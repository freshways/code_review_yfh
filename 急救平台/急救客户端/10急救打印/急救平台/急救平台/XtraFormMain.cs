﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;
using System.IO;

namespace jjpt
{
    public partial class XtraFormMain : DevExpress.XtraEditors.XtraForm
    {
        string id="0";
        System.Media.SoundPlayer pl = new System.Media.SoundPlayer("ringin.wav");
        public XtraFormMain()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                string sqlCmd = "select ID, 上报单位名称, 上报单位编码, 上报文件名, 上报单生成时间, 县中心打印时间,县中心确认时间, 县中心操作员, createTime from tb派出单转发 where 县中心确认时间 is null";
                DataTable dt = tykClass.Db.SqlHelper.ExecuteDataset(Properties.Settings.Default.connJjptDb, CommandType.Text, sqlCmd).Tables[0];

                gridControl当前任务.DataSource = dt;
                gridView1.Columns["上报单生成时间"].DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
                gridView1.Columns["县中心打印时间"].DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
                gridView1.Columns["县中心确认时间"].DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
                gridView1.BestFitColumns();      
                if (dt.Rows.Count > 0)
                {
                    gridControl当前任务.Focus();
                    id = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["ID"]).ToString();
                }
                else
                {
                    id = "0";
                }

                int jjdCount = dt.Rows.Count;
                if (jjdCount > 0)
                {
                    xtraTabPage当前任务.Select();
                    timer1.Enabled = false;
                    System.Drawing.Image image = getSfzhImage(id);
                    pl.PlayLooping();
                    XtraReport派出单 xt = new XtraReport派出单(image);
                    string dysj = tykClass.Db.SqlHelper.ExecuteScalar(Properties.Settings.Default.connJjptDb, CommandType.Text, "select 县中心打印时间 from tb派出单转发 where id=" + id).ToString();
                    if (dysj == "")
                    {
                        string sqlUpPrint = "update tb派出单转发 set 县中心打印时间=getdate() where id=" + id;
                        tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.connJjptDb, CommandType.Text, sqlUpPrint);

                        xt.CreateDocument();
                        xt.PrintingSystem.ShowMarginsWarning = false;
                        xt.PrintingSystem.ShowPrintStatusDialog = false;

                        xt.Print();
                    }
                    xt.ShowPreviewDialog();
                }
            }
            catch (Exception ex)
            {
                
                throw;
            }
            
        }


        public static System.Drawing.Image getSfzhImage(string id)
        {
            System.Drawing.Image image = null;
            try
            {
                string strSQL = "select 上报内容 from tb派出单转发 where ID=" + id;
                System.Data.SqlClient.SqlDataReader reader = TYK.Class.SqlHelper.ExecuteReader(Properties.Settings.Default.connJjptDb, CommandType.Text,
                    strSQL);
                reader.Read();
                if (reader.HasRows)
                {
                    MemoryStream ms = new MemoryStream((byte[])reader["上报内容"]);
                    image = System.Drawing.Image.FromStream(ms, true);
                }
                return image;
            }
            catch (Exception ee)
            {
                return image;
            }
        }

        private void simpleButton确定_Click(object sender, EventArgs e)
        {
            string sZfdwBm = searchLookUpEdit派遣单位.EditValue.ToString();
            string sZfdwMc = searchLookUpEdit派遣单位.Text;
            if (sZfdwMc=="" && checkEdit任务转发.Checked)
            {
               XtraMessageBox.Show("尚未选定派出单位", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;  
            }
            if (id == "0")
            {
                XtraMessageBox.Show("尚未选定急救任务", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (XtraMessageBox.Show("您确ID为" + id + "派出单吗？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
    

            string sqlCmd = "";
            if (checkEdit任务转发.Checked)
            {
                sqlCmd = "update tb派出单转发 set 县中心确认时间=GETDATE() ,转发单位编码='" + sZfdwBm + "',转发单位名称='" + sZfdwMc + "' where ID= " + id;
            }
            else
            {
                sqlCmd = "update tb派出单转发 set 县中心确认时间=GETDATE() where ID= " + id;
            }

            tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.connJjptDb, CommandType.Text, sqlCmd);
            pl.Stop();
            timer1.Enabled = true;
        }

        private void simpleButton刷新历史数据_Click(object sender, EventArgs e)
        {
            string sqlCmd = "select ID, 上报单位名称, 上报单位编码, 上报文件名, 上报单生成时间 呼救时间,县中心打印时间 派遣单打印时间,县中心确认时间 任务确认时间 from tb派出单转发 where 县中心确认时间 is not null order by id desc";
            DataTable dt = tykClass.Db.SqlHelper.ExecuteDataset(Properties.Settings.Default.connJjptDb, CommandType.Text, sqlCmd).Tables[0];
            gridControl任务记录.DataSource = dt;
            gridView2.Columns["派遣单打印时间"].DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            gridView2.Columns["呼救时间"].DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            gridView2.Columns["任务确认时间"].DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            gridView2.BestFitColumns();
        }

        private void simpleButton查看派出单_Click(object sender, EventArgs e)
        {
            string id2;
            DataTable dt = (DataTable)gridControl任务记录.DataSource;
            if (dt.Rows.Count > 0)
            {
                id2 = gridView2.GetRowCellValue(gridView2.FocusedRowHandle, gridView2.Columns["ID"]).ToString();
            }
            else
            {
                id2 = "0";
            }
            System.Drawing.Image image = getSfzhImage(id2);
            XtraReport派出单 xt = new XtraReport派出单(image);
            xt.ShowPreviewDialog();
        }

        private void checkEdit任务转发_CheckedChanged(object sender, EventArgs e)
        {
            searchLookUpEdit派遣单位.Enabled = checkEdit任务转发.Checked;           
        }

        private void XtraFormMain_Load(object sender, EventArgs e)
        {
            this.xhDwTableAdapter.Fill(this.dataSetDB.XhDw);
        }

        private void gridControl当前任务_Click(object sender, EventArgs e)
        {

        }
    }
}