﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Reflection;
using System.Collections;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;
//using DevExpress.XtraPrinting;

namespace fileWatcherToPrint
{
    public partial class FormMain : Form
    {
        System.Media.SoundPlayer pl = new System.Media.SoundPlayer("ringin.wav");
        string id = "0";
        string dwbm = "";

        private string tempUpdatePath = string.Empty;//更新临时文件路径

        public FormMain()
        {
            InitializeComponent();
        }


        private void ChangeConfiguration()
        {

            //读取程序集的配置文件
            string assemblyConfigFile = Assembly.GetEntryAssembly().Location;
            string appDomainConfigFile = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;

            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);


            //获取appSettings节点
            AppSettingsSection appSettings = (AppSettingsSection)config.GetSection("appSettings");


            //删除name，然后添加新值
            //appSettings.Settings.Remove("FolderPath");
            //appSettings.Settings.Add("FolderPath", txtFolderPath.Text);

            //保存配置文件
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }


        //string filecode;
        private void FormMain_Load(object sender, EventArgs e)
        {
            //axDoccameraOcx1.bStartPlayRotate(270);
            //axDoccameraOcx1.bSetMode(3);
            //axDoccameraOcx1.vSetResolution(3);

            if (Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "120") == false)
            {
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "120");
            }
            if (Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "log") == false)
            {
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "log");
            }

            TYK.Class.NetControl netControl = new TYK.Class.NetControl();
            ArrayList allNics = netControl.GetAllNic();
            foreach (object Nicid in allNics)
            {
                TYK.Class.zdInfo.MacAddress = netControl.GetNicAddress(Nicid.ToString());
            }

            TYK.Class.zdInfo.sAppVersion = Application.ProductVersion;
           

            try
            {
                int count = Convert.ToInt16(TYK.Class.SqlHelper.ExecuteScalar(Properties.Settings.Default.connJjptDb,
                    CommandType.Text,
                    "select count(*) from pub站点设置 where mac地址='" + TYK.Class.zdInfo.MacAddress + "'"));
                if (count == 0)
                {
                    TYK.Class.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.connJjptDb, CommandType.Text,
                    "insert into pub站点设置 (mac地址,站点名称,当前版本,是否禁用,限定版本) select '" +
                    TYK.Class.zdInfo.MacAddress + "','未注册','" +
                    TYK.Class.zdInfo.sAppVersion + "'" +
                    ",0,'1.0.0.0'");
                }
                else
                {
                    TYK.Class.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.connJjptDb, CommandType.Text,
                        "update pub站点设置 set 最近更新时间=getdate(),当前版本='" + TYK.Class.zdInfo.sAppVersion +
                        "' where mac地址='" + TYK.Class.zdInfo.MacAddress + "'");
                }

                DataTable dtClientSet = tykClass.Db.SqlHelper.ExecuteDataset(Properties.Settings.Default.connJjptDb, CommandType.Text,
                    "select folderpath,filetype,限定版本,单位编码,单位名称,是否禁用,站点名称,站点类型 from pub站点设置 where mac地址='" + TYK.Class.zdInfo.MacAddress + "'").Tables[0];
                if (dtClientSet.Rows.Count != 1)
                {
                    MessageBox.Show("获取站点配置信息失败！请联系2251685信息处理故障！站点标识："+TYK.Class.zdInfo.MacAddress);
                    this.Close();
                }
                if (dtClientSet.Rows[0]["站点类型"].ToString() == "转发")
                {
                    //xtraTabPage监控任务.PageVisible = false;
                    xtraTabPage1.PageVisible = false;

                }
                else
                {
                    //xtraTabPage监控任务.PageVisible = true;
                    xtraTabPage1.PageVisible = true;
                    if (dtClientSet.Rows[0]["FolderPath"] is DBNull || dtClientSet.Rows[0]["FileType"] is DBNull)
                    {
                        MessageBox.Show("获取站点配置信息失败！请联系2251685信息处理故障！站点标识：" + TYK.Class.zdInfo.MacAddress);
                        this.Close();
                    }
                    //txtFolderPath.Text = dtClientSet.Rows[0]["FolderPath"].ToString();
                    //txtFileType.Text = dtClientSet.Rows[0]["FileType"].ToString();
                    //btnWatcher_Click(null, null);
                }

                string vSer限定版本 = dtClientSet.Rows[0]["限定版本"].ToString();
                dwbm = dtClientSet.Rows[0]["单位编码"].ToString();
                TYK.Class.zdInfo.iDwid = Convert.ToInt32(dtClientSet.Rows[0]["单位编码"]);
                TYK.Class.zdInfo.sZdmc = dtClientSet.Rows[0]["站点名称"].ToString();
                TYK.Class.zdInfo.sDwmc = dtClientSet.Rows[0]["单位名称"].ToString();
                Boolean sfjy = Convert.ToBoolean(dtClientSet.Rows[0]["是否禁用"]);
                TYK.Class.Log.logToDb("急救平台", "正常信息", "软件启动", Properties.Settings.Default.connJjptDb);
                timer自动升级_Tick(null, null);

                btnHide_Click(null, null);
                timer心跳测试.Enabled = true;
                timer自动升级.Enabled = true;
                this.Text += "【版本号:" + TYK.Class.zdInfo.sAppVersion + ",使用单位:"+TYK.Class.zdInfo.sDwmc+"站点标识："+TYK.Class.zdInfo.MacAddress+"】";
            }
            catch (Exception ee)
            {
                TYK.Class.Log.LogToFile("log\\log", ee.Message);
                MessageBox.Show(ee.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }


        }


        /// <summary>
        /// 文件夹监控事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void fileSystemWatcher1_Created(object sender, System.IO.FileSystemEventArgs e)
        //{
        //    webBrowser1.Navigate(e.FullPath);
        //    filecode = e.Name.Remove(e.Name.LastIndexOf("."));
        //    // MessageBox.Show(filecode);

        //}

        /// <summary>
        /// html生成图片并写入数据库
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        //{
        //    try
        //    {
        //        WebSnap websnap = new WebSnap();
        //        Bitmap bmp = websnap.StartSnap(webBrowser1.Url.ToString());



        //        string bmppath = Application.StartupPath + @"\120\" + filecode + ".jpg";
        //        bmp.Save(bmppath);
        //        bmp.Dispose();

        //        using (SqlConnection cn = new SqlConnection(Properties.Settings.Default.connJjptDb))
        //        {

        //            using (SqlCommand cmd = new SqlCommand("INSERT INTO tb派出单转发(上报单位名称,上报单位编码,上报文件名,上报内容) VALUES (@dwmc,@dwbm,@wjm,@pic)", cn))
        //            {

        //                cmd.Parameters.Add("@dwmc", SqlDbType.VarChar).Value = TYK.Class.zdInfo.sDwmc;
        //                cmd.Parameters.Add("@dwbm", SqlDbType.VarChar).Value = dwbm;
        //                cmd.Parameters.Add("@wjm", SqlDbType.VarChar).Value = dwbm + "_" + filecode;
        //                cmd.Parameters.Add("@pic", SqlDbType.Image).Value = File.ReadAllBytes(bmppath);
        //                cn.Open();
        //                cmd.ExecuteNonQuery();
        //                cn.Close();
        //                //MessageBox.Show("图片插入成功！");
        //                richTextBox1.AppendText(DateTime.Now + "上传【" + filecode + "】成功。 \r\n");

        //            }
        //            string taskID_Code = webBrowser1.Document.GetElementById("taskID_Code").GetAttribute("value");
        //            string callTime = webBrowser1.Document.GetElementById("callTime").GetAttribute("value");
        //            string callPhone = webBrowser1.Document.GetElementById("callPhone").GetAttribute("value");
        //            string caller = webBrowser1.Document.GetElementById("caller").GetAttribute("value");
        //            string orgName = webBrowser1.Document.GetElementById("orgName").GetAttribute("value");
        //            string region = webBrowser1.Document.GetElementById("region").GetAttribute("value");
        //            string caseType = webBrowser1.Document.GetElementById("caseType").GetAttribute("value");
        //            string resaon = webBrowser1.Document.GetElementById("resaon").GetAttribute("value");
        //            string handleJudgement = webBrowser1.Document.GetElementById("handleJudgement").GetAttribute("value");
        //            string illState = webBrowser1.Document.GetElementById("illState").GetAttribute("value");
        //            string patientName = webBrowser1.Document.GetElementById("patientName").GetAttribute("value");
        //            string sex = webBrowser1.Document.GetElementById("sex").GetAttribute("value");
        //            string birthDate = webBrowser1.Document.GetElementById("birthDate").GetAttribute("value");
        //            string linkman = webBrowser1.Document.GetElementById("linkman").GetAttribute("value");
        //            string phone = webBrowser1.Document.GetElementById("phone").GetAttribute("value");
        //            string carType = webBrowser1.Document.GetElementById("carType").GetAttribute("value");
        //            string carNo = webBrowser1.Document.GetElementById("carNo").GetAttribute("value");
        //            string driverName = webBrowser1.Document.GetElementById("driverName").GetAttribute("value");
        //            string doctorName = webBrowser1.Document.GetElementById("doctorName").GetAttribute("value");
        //            string nurseName = webBrowser1.Document.GetElementById("nurseName").GetAttribute("value");
        //            string workerName = webBrowser1.Document.GetElementById("workerName").GetAttribute("value");
        //            string Hospital = webBrowser1.Document.GetElementById("Hospital").GetAttribute("value");
        //            string patientAddr = webBrowser1.Document.GetElementById("patientAddr").GetAttribute("value");
        //            string waitAddress = webBrowser1.Document.GetElementById("waitAddress").GetAttribute("value");
        //            string depict = webBrowser1.Document.GetElementById("depict").GetAttribute("value");
        //            string medicineInfo = webBrowser1.Document.GetElementById("medicineInfo").GetAttribute("value");
        //            string sensitiveInfo = webBrowser1.Document.GetElementById("sensitiveInfo").GetAttribute("value");
        //            string familyDisease = webBrowser1.Document.GetElementById("familyDisease").GetAttribute("value");
        //            string commandTime = webBrowser1.Document.GetElementById("commandTime").GetAttribute("value");
        //            string receiveTime = webBrowser1.Document.GetElementById("receiveTime").GetAttribute("value");
        //            string startTime = webBrowser1.Document.GetElementById("startTime").GetAttribute("value");
        //            string arriveTime = webBrowser1.Document.GetElementById("arriveTime").GetAttribute("value");
        //            string leaveTime = webBrowser1.Document.GetElementById("leaveTime").GetAttribute("value");
        //            string arriveHosTime = webBrowser1.Document.GetElementById("arriveHosTime").GetAttribute("value");
        //            string leaveHosTime = webBrowser1.Document.GetElementById("leaveHosTime").GetAttribute("value");
        //            string backTime = webBrowser1.Document.GetElementById("backTime").GetAttribute("value");
        //            string road = "";
        //            string cancel = webBrowser1.Document.GetElementById("cancel").GetAttribute("value");
        //            string cancelTime = webBrowser1.Document.GetElementById("cancelTime").GetAttribute("value");
        //            string useStretcher = webBrowser1.Document.GetElementById("useStretcher").GetAttribute("value");
        //            string executeResult = webBrowser1.Document.GetElementById("executeResult").GetAttribute("value");
        //            string cureResult = webBrowser1.Document.GetElementById("cureResult").GetAttribute("value");
        //            string memo = webBrowser1.Document.GetElementById("memo").GetAttribute("value");
        //            string userName = webBrowser1.Document.GetElementById("userName").GetAttribute("value");
        //            string PrintTime = webBrowser1.Document.GetElementById("PrintTime").GetAttribute("value");
        //            using (SqlCommand cmd = new SqlCommand("INSERT INTO tb派出单数据(上报单位名称,上报单位编码,任务单编号,呼救时间,呼救电话,机主姓名,出车单位,区域,任务类型,用车原因,病情类型,病情级别,患者姓名,性别,年龄,联系人,联系电话,车辆类型,车牌号,司机,医生,护士,担架工,接收医院,现场地址,候车地址,呼救主诉,用药史,过敏史,既往史,命令下达时间,接到命令时间,出车时间,到达现场时间,离开现场时间,到达医院时间,离开医院时间,返回急救站时间,行车路线,任务取消,取消时间,是否使用担架,执行结果,救治结果,备注,值班员,打印时间) VALUES (@sbdwmc,@sbdwbm,@taskID_Code,@callTime,@callPhone,@caller,@orgName ,@region ,@caseType ,@resaon ,@handleJudgement,@illState ,@patientName ,@sex ,@birthDate ,@linkman ,@phone ,@carType ,@carNo ,@driverName ,@doctorName ,@nurseName ,@workerName ,@Hospital ,@patientAddr ,@waitAddress ,@depict ,@medicineInfo ,@sensitiveInfo,@familyDisease,@commandTime ,@receiveTime ,@startTime ,@arriveTime ,@leaveTime ,@arriveHosTime ,@leaveHosTime ,@backTime ,@road,@cancel ,@cancelTime,@useStretcher ,@executeResult,@cureResult,@memo ,@userName ,@PrintTime)", cn))
        //            {
        //                cmd.Parameters.Add("@sbdwmc", SqlDbType.VarChar).Value = TYK.Class.zdInfo.sDwmc;
        //                cmd.Parameters.Add("@sbdwbm", SqlDbType.VarChar).Value = TYK.Class.zdInfo.iDwid.ToString();
        //                cmd.Parameters.Add("@taskID_Code", SqlDbType.VarChar).Value =TYK.Class.zdInfo.iDwid.ToString()+ "_" + taskID_Code;
        //                cmd.Parameters.Add("@callTime", SqlDbType.VarChar).Value = callTime;
        //                cmd.Parameters.Add("@callPhone", SqlDbType.VarChar).Value = callPhone;
        //                cmd.Parameters.Add("@caller", SqlDbType.VarChar).Value = caller;
        //                cmd.Parameters.Add("@orgName ", SqlDbType.VarChar).Value = orgName;
        //                cmd.Parameters.Add("@region", SqlDbType.VarChar).Value = region;
        //                cmd.Parameters.Add("@caseType ", SqlDbType.VarChar).Value = caseType;
        //                cmd.Parameters.Add("@resaon", SqlDbType.VarChar).Value = resaon;
        //                cmd.Parameters.Add("@handleJudgement", SqlDbType.VarChar).Value = handleJudgement;
        //                cmd.Parameters.Add("@illState", SqlDbType.VarChar).Value = illState;
        //                cmd.Parameters.Add("@patientName", SqlDbType.VarChar).Value = patientName;
        //                cmd.Parameters.Add("@sex", SqlDbType.VarChar).Value = sex;
        //                cmd.Parameters.Add("@birthDate", SqlDbType.VarChar).Value = birthDate;
        //                cmd.Parameters.Add("@linkman", SqlDbType.VarChar).Value = linkman;
        //                cmd.Parameters.Add("@phone", SqlDbType.VarChar).Value = phone;
        //                cmd.Parameters.Add("@carType", SqlDbType.VarChar).Value = carType;
        //                cmd.Parameters.Add("@carNo", SqlDbType.VarChar).Value = carNo;
        //                cmd.Parameters.Add("@driverName", SqlDbType.VarChar).Value = driverName;
        //                cmd.Parameters.Add("@doctorName", SqlDbType.VarChar).Value = doctorName;
        //                cmd.Parameters.Add("@nurseName", SqlDbType.VarChar).Value = nurseName;
        //                cmd.Parameters.Add("@workerName ", SqlDbType.VarChar).Value = workerName;
        //                cmd.Parameters.Add("@Hospital ", SqlDbType.VarChar).Value = Hospital;
        //                cmd.Parameters.Add("@patientAddr ", SqlDbType.VarChar).Value = patientAddr;
        //                cmd.Parameters.Add("@waitAddress ", SqlDbType.VarChar).Value = waitAddress;
        //                cmd.Parameters.Add("@depict ", SqlDbType.VarChar).Value = depict;
        //                cmd.Parameters.Add("@medicineInfo ", SqlDbType.VarChar).Value = medicineInfo;
        //                cmd.Parameters.Add("@sensitiveInfo", SqlDbType.VarChar).Value = sensitiveInfo;
        //                cmd.Parameters.Add("@familyDisease", SqlDbType.VarChar).Value = familyDisease;
        //                cmd.Parameters.Add("@commandTime ", SqlDbType.VarChar).Value = commandTime;
        //                cmd.Parameters.Add("@receiveTime ", SqlDbType.VarChar).Value = receiveTime;
        //                cmd.Parameters.Add("@startTime ", SqlDbType.VarChar).Value = startTime;
        //                cmd.Parameters.Add("@arriveTime ", SqlDbType.VarChar).Value = arriveTime;
        //                cmd.Parameters.Add("@leaveTime ", SqlDbType.VarChar).Value = leaveTime;
        //                cmd.Parameters.Add("@arriveHosTime ", SqlDbType.VarChar).Value = arriveHosTime;
        //                cmd.Parameters.Add("@leaveHosTime ", SqlDbType.VarChar).Value = leaveHosTime;
        //                cmd.Parameters.Add("@backTime ", SqlDbType.VarChar).Value = backTime;
        //                cmd.Parameters.Add("@road", SqlDbType.VarChar).Value = road;
        //                cmd.Parameters.Add("@cancel ", SqlDbType.VarChar).Value = cancel;
        //                cmd.Parameters.Add("@cancelTime", SqlDbType.VarChar).Value = cancelTime;
        //                cmd.Parameters.Add("@useStretcher ", SqlDbType.VarChar).Value = useStretcher;
        //                cmd.Parameters.Add("@executeResult", SqlDbType.VarChar).Value = executeResult;
        //                cmd.Parameters.Add("@cureResult", SqlDbType.VarChar).Value = cureResult;
        //                cmd.Parameters.Add("@memo ", SqlDbType.VarChar).Value = memo;
        //                cmd.Parameters.Add("@userName ", SqlDbType.VarChar).Value = userName;
        //                cmd.Parameters.Add("@PrintTime ", SqlDbType.VarChar).Value = PrintTime;

        //                cn.Open();
        //                cmd.ExecuteNonQuery();
        //                cn.Close();
        //                richTextBox1.AppendText(DateTime.Now + "写入数据库数据【" + filecode + "】成功。 \r\n");
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        MessageBox.Show("服务器发送失败" + ex.Message);
        //        richTextBox1.AppendText(DateTime.Now + "上传【" + filecode + "】失败，请电话通知县急救中心电话：" + ConfigurationManager.AppSettings["jjdh"] + "。 \r\n");
        //    }
        //}

   


        private void btnHide_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.WindowState = FormWindowState.Minimized;
        }

        private void 显示ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        private void 退出EToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        private void timer心跳测试_Tick(object sender, EventArgs e)
        {
            try
            {
                TYK.Class.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.connJjptDb, CommandType.Text,
                       "update pub站点设置 set 最近更新时间=getdate() where mac地址='" + TYK.Class.zdInfo.MacAddress + "'");
                TYK.Class.Log.logToDb("急救平台", "心跳测试", "", Properties.Settings.Default.connJjptDb);
            }
            catch (Exception ex)
            {
                TYK.Class.Log.LogToFile("log\\log", ex.Message);
                richTextBox1.AppendText(DateTime.Now + "更新在线状态异常\r\n" + ex.Message);
            }

        }



        private void v升级事件(string sAppVer)
        {
            string UpdateFile = "temp" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".zip";
            try
            {
                tempUpdatePath = Environment.GetEnvironmentVariable("Temp") + "\\" + "_" + UpdateFile + "_" + "y" + "_" + "x" + "_" + "m" + "_" + "\\";
                if (System.IO.Directory.Exists(tempUpdatePath)) System.IO.Directory.Delete(tempUpdatePath, true);
                System.IO.Directory.CreateDirectory(tempUpdatePath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("初始化下载目录失败!" + ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
                return;
            }
            string tempPath = tempUpdatePath + UpdateFile;

            byte[] mybyte = null;
            FileStream myfs = new FileStream(tempPath, FileMode.CreateNew);
            BinaryWriter writefile = new BinaryWriter(myfs);
            string strselect = "select filecontent from PUB_UPGRADE where appversion='" + sAppVer + "'";
            SqlConnection myconnection = new SqlConnection(Properties.Settings.Default.connJjptDb);
            myconnection.Open();
            SqlCommand mycommand = new SqlCommand(strselect, myconnection);
            SqlDataReader myreader = mycommand.ExecuteReader();
            if (myreader.HasRows == false)
            {
                string err = "获取新的升级组件失败!";
                richTextBox1.AppendText(err);
                TYK.Class.Log.logToDb("急救平台", "错误信息", err, Properties.Settings.Default.connJjptDb);
                TYK.Class.Log.LogToFile("log\\log", err);
                return;
            }
            if (myreader.Read())
            {
                mybyte = (byte[])myreader[0];
            }
            myreader.Close();
            myconnection.Close();
            writefile.Write(mybyte, 0, mybyte.Length);
            writefile.Close();
            myfs.Close();
            tykClass.Xml.XMLControl.SetConfigValue("upList.config", "fileName", tempPath);
            tykClass.Xml.XMLControl.SetConfigValue("upList.config", "mainApp", "fileWatcherToPrint.exe");
            this.Close();
            //uplistDo.Form升级等待 sjdd = new uplistDo.Form升级等待();
            //sjdd.ShowDialog();
            System.Diagnostics.Process.Start("upListDo.exe");
        }

        private void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            TYK.Class.Log.logToDb("急救平台", "异常信息", "软件人为退出", Properties.Settings.Default.connJjptDb);
        }

        private void timer自动升级_Tick(object sender, EventArgs e)
        {
            try
            {
                DataTable dtClientSet = tykClass.Db.SqlHelper.ExecuteDataset(Properties.Settings.Default.connJjptDb, CommandType.Text,
                                  "select folderpath,filetype,限定版本,单位编码,单位名称,是否禁用,站点名称 from pub站点设置 where mac地址='" + TYK.Class.zdInfo.MacAddress + "'").Tables[0];
                if (dtClientSet.Rows.Count != 1)
                {
                    MessageBox.Show("获取站点配置信息失败！请联系2251685信息处理故障！");
                    this.Close();
                }

                string vSer限定版本 = dtClientSet.Rows[0]["限定版本"].ToString();
                Boolean sfjy = Convert.ToBoolean(dtClientSet.Rows[0]["是否禁用"]);

                Version vLocal = new Version(TYK.Class.zdInfo.sAppVersion);
                if (sfjy)
                {
                    MessageBox.Show("当前站点被禁用,错误号-3", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    Version vServer限定版本 = new Version(vSer限定版本);
                    try
                    {
                        if (vLocal < vServer限定版本)
                        {
                            v升级事件(vServer限定版本.ToString());
                        }
                    }
                    catch (Exception ee)
                    {
                        TYK.Class.Log.LogToFile("log\\log", ee.Message);
                        //  MessageBox.Show(ee.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                TYK.Class.Log.LogToFile("log\\log", ex.Message);
                //  throw;
            }

        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void simpleButton确定任务_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("您确ID为" + id + "派出单吗？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string sqlCmd = "update tb派出单转发 set 转发单位确认时间=GETDATE(),转发单位派出人员='"+comboBoxEdit派出机构.Text+"."+comboBoxEdit派出人员.Text+"' where ID= " + id;
            tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.connJjptDb, CommandType.Text, sqlCmd);
            pl.Stop();
            timer任务监控.Enabled = true;
        }

        private void timer任务监控_Tick(object sender, EventArgs e)
        {
            try
            {
                string sqlCmd = "select 上报单生成时间 呼救时间,上报单位名称 急救单位,县中心确认时间 中心调度时间,转发单位打印时间,ID from tb派出单转发 where 县中心确认时间 is not null and 转发单位编码=" + TYK.Class.zdInfo.iDwid.ToString() + " and 转发单位确认时间 is null";
                DataTable dt = tykClass.Db.SqlHelper.ExecuteDataset(Properties.Settings.Default.connJjptDb, CommandType.Text, sqlCmd).Tables[0];
                gridControl急救任务.DataSource = dt;
                gridView急救任务.BestFitColumns();

                int jjdCount = dt.Rows.Count;
                if (jjdCount > 0)
                {
                    xtraTabPage急救任务.Select();
                    gridView急救任务.Focus();
                    id = gridView急救任务.GetRowCellValue(gridView急救任务.FocusedRowHandle, gridView急救任务.Columns["ID"]).ToString();

                    timer任务监控.Enabled = false;
                    System.Drawing.Image image = getSfzhImage(id);
                    pl.PlayLooping();
                    XtraReport派出单 xt = new XtraReport派出单(image);
                    string dysj = tykClass.Db.SqlHelper.ExecuteScalar(Properties.Settings.Default.connJjptDb, CommandType.Text, "select 转发单位打印时间 from tb派出单转发 where id=" + id).ToString();
                    if (dysj == "")
                    {
                        string sqlUpPrint = "update tb派出单转发 set 转发单位打印时间=getdate() where id=" + id;
                        tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.connJjptDb, CommandType.Text, sqlUpPrint);

                        xt.CreateDocument();
                        xt.PrintingSystem.ShowMarginsWarning = false;
                        xt.PrintingSystem.ShowPrintStatusDialog = false;
                        
                        xt.Print();
                    }
                    xt.ShowPreviewDialog();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public static System.Drawing.Image getSfzhImage(string id)
        {
            System.Drawing.Image image = null;
            try
            {
                string strSQL = "select 上报内容 from tb派出单转发 where ID=" + id;
                System.Data.SqlClient.SqlDataReader reader = TYK.Class.SqlHelper.ExecuteReader(Properties.Settings.Default.connJjptDb, CommandType.Text,
                    strSQL);
                reader.Read();
                if (reader.HasRows)
                {
                    MemoryStream ms = new MemoryStream((byte[])reader["上报内容"]);
                    image = System.Drawing.Image.FromStream(ms, true);
                }
                return image;
            }
            catch (Exception ee)
            {
                return image;
            }
        }

        private void simpleButton刷新_Click(object sender, EventArgs e)
        {
            string sqlCmd = "select 上报单生成时间 呼救时间,上报单位名称 急救单位,县中心确认时间 中心调度时间,转发单位打印时间,转发单位确认时间 确认时间,ID from tb派出单转发 where 转发单位编码="+TYK.Class.zdInfo.iDwid.ToString()+" and 转发单位确认时间 is not null order by id desc";
            DataTable dt = tykClass.Db.SqlHelper.ExecuteDataset(Properties.Settings.Default.connJjptDb, CommandType.Text, sqlCmd).Tables[0];
            gridControl急救记录.DataSource = dt;
            gridView急救记录.Columns["呼救时间"].DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            gridView急救记录.Columns["中心调度时间"].DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            gridView急救记录.Columns["确认时间"].DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            gridView急救记录.BestFitColumns();
        }

        private void simpleButton查看任务_Click(object sender, EventArgs e)
        {
            string id2;
            DataTable dt = (DataTable)gridControl急救记录.DataSource;
            if (dt.Rows.Count > 0)
            {
                id2 = gridView急救记录.GetRowCellValue(gridView急救记录.FocusedRowHandle, gridView急救记录.Columns["ID"]).ToString();
            }
            else
            {
                id2 = "0";
            }
            System.Drawing.Image image = getSfzhImage(id2);
            XtraReport派出单 xt = new XtraReport派出单(image);
            xt.ShowPreviewDialog();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {

            try
            {
                string filename = DateTime.Now.ToString("yyyyMMddHHmmss");
                string bmppath = Application.StartupPath + @"\120\" + filename + ".jpg";
                axDoccameraOcx1.bSaveJPG(Application.StartupPath + @"\120\", filename);
                //bmp.Save(bmppath);
                //bmp.Dispose();

                using (SqlConnection cn = new SqlConnection(Properties.Settings.Default.connJjptDb))
                {

                    using (SqlCommand cmd = new SqlCommand("INSERT INTO tb派出单转发(上报单位名称,上报单位编码,上报文件名,上报内容) VALUES (@dwmc,@dwbm,@wjm,@pic)", cn))
                    {

                        cmd.Parameters.Add("@dwmc", SqlDbType.VarChar).Value = TYK.Class.zdInfo.sDwmc;
                        cmd.Parameters.Add("@dwbm", SqlDbType.VarChar).Value = dwbm;
                        cmd.Parameters.Add("@wjm", SqlDbType.VarChar).Value = dwbm + "_" + filename;
                        cmd.Parameters.Add("@pic", SqlDbType.Image).Value = File.ReadAllBytes(bmppath);
                        cn.Open();
                        cmd.ExecuteNonQuery();
                        cn.Close();
                        richTextBox1.AppendText(DateTime.Now + "上传成功。 \r\n");
                        MessageBox.Show("上传成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("服务器发送失败" + ex.Message);
                richTextBox1.AppendText(DateTime.Now + "上传失败，请电话通知县急救中心电话：" + ConfigurationManager.AppSettings["jjdh"] + "。 \r\n");
            }
        }


        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            if (e.Page.Name == "xtraTabPage1")
            {
                axDoccameraOcx1.bStartPlayRotate(270);
                axDoccameraOcx1.bSetMode(3);
            }
            else
            {
                axDoccameraOcx1.bStopPlay();
            }
        }

        

    }
}
