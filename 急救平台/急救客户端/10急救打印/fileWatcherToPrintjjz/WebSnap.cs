﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace fileWatcherToPrint
{
    /// <summary>
    /// WebSnap ：网页抓图对象
    /// </summary>
    public class WebSnap
    {
        public WebSnap()
        {
            //
            // TODO: 在此处添加构造函数逻辑 //
        }

        /// <summary>
        /// 开始一个抓图并返回图象
        /// </summary>
        /// <param name="Url">要抓取的网页地址</param>
        /// <returns></returns>
        public Bitmap StartSnap(string Url)
        {
            WebBrowser myWB = this.GetPage(Url);
            Bitmap returnValue = this.SnapWeb(myWB);
            myWB.Dispose();
            return returnValue;
        }

        private WebBrowser GetPage(string Url)
        {
            WebBrowser myWB = new WebBrowser();
            myWB.ScrollBarsEnabled = false;
            myWB.Navigate(Url);
            while (myWB.ReadyState != WebBrowserReadyState.Complete)
            {
                System.Windows.Forms.Application.DoEvents();
            }
            return myWB;
        }

        private Bitmap SnapWeb(WebBrowser wb)
        {
            HtmlDocument hd = wb.Document;
            int height = Convert.ToInt32(hd.Body.GetAttribute("scrollHeight")) + 10;
            int width = Convert.ToInt32(hd.Body.GetAttribute("scrollWidth")) + 10;
            wb.Height = height;
            wb.Width = width;
            Bitmap bmp = new Bitmap(width, height);
            Rectangle rec = new Rectangle();
            rec.Width = width;
            rec.Height = height;
            wb.DrawToBitmap(bmp, rec);
            return bmp;
        }
    }
}
