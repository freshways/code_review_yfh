﻿namespace fileWatcherToPrint
{
    partial class Form升级列表
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button升级 = new System.Windows.Forms.Button();
            this.button取消 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // button升级
            // 
            this.button升级.Location = new System.Drawing.Point(311, 333);
            this.button升级.Name = "button升级";
            this.button升级.Size = new System.Drawing.Size(75, 23);
            this.button升级.TabIndex = 0;
            this.button升级.Text = "升级";
            this.button升级.UseVisualStyleBackColor = true;
            this.button升级.Click += new System.EventHandler(this.button升级_Click);
            // 
            // button取消
            // 
            this.button取消.Location = new System.Drawing.Point(393, 332);
            this.button取消.Name = "button取消";
            this.button取消.Size = new System.Drawing.Size(75, 23);
            this.button取消.TabIndex = 1;
            this.button取消.Text = "取消";
            this.button取消.UseVisualStyleBackColor = true;
            this.button取消.Click += new System.EventHandler(this.button取消_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(13, 13);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(475, 313);
            this.dataGridView1.TabIndex = 2;
            // 
            // Form升级列表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 363);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button取消);
            this.Controls.Add(this.button升级);
            this.Name = "Form升级列表";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form升级列表";
            this.Load += new System.EventHandler(this.Form升级列表_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button升级;
        private System.Windows.Forms.Button button取消;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}