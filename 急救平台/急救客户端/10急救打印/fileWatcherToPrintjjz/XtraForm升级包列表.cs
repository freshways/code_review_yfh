﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;
using System.Data.SqlClient;
using System.Diagnostics;

namespace ybjk.Comm
{
    public partial class XtraForm升级包列表 : DevExpress.XtraEditors.XtraForm
    {
        private string tempUpdatePath = string.Empty;//更新临时文件路径
        string UpdateFile = string.Empty;

        public XtraForm升级包列表()
        {
            InitializeComponent();
        }

        private void XtraForm升级包列表_Load(object sender, EventArgs e)
        {
            gridControl1.DataSource = tykClass.Db.SqlHelper.ExecuteDataset(
                Properties.Settings.Default.ybjkConnectionString, CommandType.Text,
                "SELECT   appversion 版本号,   uploadtime 更新时间, readme 更新说明,filename 文件名,isvalid 是否可用 FROM PUB_UPGRADE order by id desc").Tables[0];
            gridView1.Columns["更新时间"].DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            gridView1.BestFitColumns();


        }

        private void simpleButton升级_Click(object sender, EventArgs e)
        {
            string sAppVer = "";
            Boolean bIsvalid = false;
            try
            {
                
                bIsvalid = Convert.ToBoolean(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["是否可用"]).ToString());
                sAppVer = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["版本号"]).ToString();
                UpdateFile = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["文件名"]).ToString();

                if (bIsvalid==false)
                {
                    XtraMessageBox.Show("您选择的版本已经不可用", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (XtraMessageBox.Show("您确认替换版本至【" + sAppVer + "】吗？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }

                if (TYK.Class.zdInfo.sAppVersion == sAppVer)
                {
                    if (XtraMessageBox.Show("您选择的版本【" + sAppVer + "】和正在使用的版本号相同，是否继续替换？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        return;
                    }
                }
               
                //初始化临时目录，因为考虑到可能存在下载到中途以外退出问题，所以在下载配置文件之前先删除目录
                try
                {
                    tempUpdatePath = Environment.GetEnvironmentVariable("Temp") + "\\" + "_" + UpdateFile + "_" + "y" + "_" + "x" + "_" + "m" + "_" + "\\";
                    if (System.IO.Directory.Exists(tempUpdatePath)) System.IO.Directory.Delete(tempUpdatePath, true);
                    System.IO.Directory.CreateDirectory(tempUpdatePath);

                }
                catch (Exception ex)
                {
                    MessageBox.Show("初始化下载目录失败!" + ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                    return;
                }
                string tempPath = tempUpdatePath + UpdateFile;

                byte[] mybyte = null;
                FileStream myfs = new FileStream(tempPath, FileMode.CreateNew);
                BinaryWriter writefile = new BinaryWriter(myfs);
                string strselect = "select filecontent from PUB_UPGRADE where appversion='" + sAppVer + "'";
                SqlConnection myconnection = new SqlConnection(Properties.Settings.Default.ybjkConnectionString);
                myconnection.Open();
                SqlCommand mycommand = new SqlCommand(strselect, myconnection);
                SqlDataReader myreader = mycommand.ExecuteReader();
                if (myreader.Read())
                {
                    mybyte = (byte[])myreader[0];
                }
                myreader.Close();
                myconnection.Close();
                writefile.Write(mybyte, 0, mybyte.Length);
                writefile.Close();
                myfs.Close();
                tykClass.Xml.XMLControl.SetConfigValue("upList.config", "fileName", tempPath);
                //tykClass.Xml.XMLControl.SetConfigValue("upList.config", "mainApp", Process.GetCurrentProcess().MainModule.FileName);
                this.Close();
                System.Diagnostics.Process.Start("upList.exe");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //throw;
            }

        }

        private void simpleButton取消_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}