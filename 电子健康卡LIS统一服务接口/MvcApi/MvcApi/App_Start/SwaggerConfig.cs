using System.Web.Http;
using WebActivatorEx;
using MvcApi;
using Swashbuckle.Application;
using System;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace MvcApi
{
    /// <summary>
    /// 
    /// </summary>
    public class SwaggerConfig
    {
        /// <summary>
        /// 
        /// </summary>
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                    {
                        c.SingleApiVersion("v1", "MvcApi");
                        c.IncludeXmlComments(GetXmlCommentsPath());
                    })
                .EnableSwaggerUi(c =>
                    {
                        c.InjectJavaScript(thisAssembly, "MvcApi.Scripts.swaggerui.swagger_lang.js");//���İ�
                        c.DisableValidator();
                    });
        }
        private static string GetXmlCommentsPath()
        {
            return String.Format(@"{0}\bin\SwaggerMvcApi.XML", System.AppDomain.CurrentDomain.BaseDirectory);
        }
    }
}
