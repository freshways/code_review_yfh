﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Http;
using WebApi_Common;
using WebApi_DBUtility;

namespace MvcApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class LISController : ApiController
    {
        private static string LisconnString = "Data Source = 192.168.10.57; Initial Catalog = LIS2721; Persist Security Info=True;User ID = yggsuser2016; Password=yggs2016user@163.com";

        Dictionary<string, string> dic = new Dictionary<string, string>();

        string GetLisconndic(string key)
        {
            string Lisconnformat = "Data Source = 192.168.10.57; Initial Catalog = LIS{0}; Persist Security Info=True;User ID = yggsuser2016; Password=yggs2016user@163.com";
            if (dic == null || dic.Count==0)
            {
                string lis = PubConstant.GetConnectionString("LIS");
                foreach (var l in lis.Split('|'))
                {
                    string[] diclis = l.Split(',');
                    if(diclis.Length==2)
                        dic.Add(diclis[0], diclis[1]);
                }
            }
            return dic.ContainsKey(key) ? string.Format(Lisconnformat,dic[key]) : "";
        }

        /// <summary>
        /// 获取报告单列表 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPost]
        public ResponseResult Get_LisReport([FromBody]JObject value)
        {
            string sql = @"select fjy_id as reportId 
	                     ,'02' as type 
	                     ,'检验单' as title 
	                     ,fhz_name as patientName 
	                     ,sfzh as cardNo 
	                     ,fhz_sex as sex 
	                     ,cast(fhz_age as varchar) age
	                     ,case when fhz_bed='' then (select top 1 fname from WWF_DEPT where fdept_id=fhz_dept) else fhz_bed end departName
	                     ,null as doctorName 
	                     ,fjy_zt as status 
	                     ,fhz_zyh as hosUserNo 
	                     ,fapply_time as submissionTime 
	                     ,fcheck_time as reportTime 
	                     ,null as result 
	                     ,fremark as remark 
	                     ,(select top 1 fname from WWF_PERSON where fperson_id = fjy_user_id ) as checker 
	                     ,(select top 1 fname from WWF_PERSON where fperson_id = fchenk_user_id ) as auditor
	                     ,fapply_id as reportNo 
	                     ,fjy_instr as checkNo
	                     ,(select top 1 fname from SAM_INSTR where SAM_INSTR.finstr_id = fjy_instr) as checkName 
	                     ,null as checkType 
	                     ,null as checkSite 
	                     ,null as checkMethod 
	                     ,null as words 
	                     ,fjy_yb_code as sampleNo 
	                     ,'血液' as sampleType 
	                     ,convert(varchar,(select count(1) from SAM_JY_RESULT where SAM_JY_RESULT.fjy_id=SAM_JY.fjy_id)) as itemNum 
                    from dbo.SAM_JY
                    where 1=1 and fjy_zt='已审核' ";

            List<SqlParameter> SqlParamlist = new List<SqlParameter>();
            //{
            //    new SqlParameter("@startTime", startTime),
            //    new SqlParameter("@endTime", endTime),
            //    new SqlParameter("@idCardNo", idCardNo),
            //    new SqlParameter("@cardNo", cardNo)
            //};
            string cardType=string.Empty, idType=string.Empty, type=string.Empty, merchId = string.Empty;
            if (value.TryGetValue("cardNo", out var cardNo))
            {
                if (!string.IsNullOrEmpty(cardNo.ToString()))
                {
                    sql += " and f1=@cardNo";
                    SqlParamlist.Add(new SqlParameter("@cardNo", cardNo.ToString()));
                }
            }
            if (value.TryGetValue("cardType", out var cardType1))
            {
                cardType = value["cardType"].ToString();
            }
            if (value.TryGetValue("idType", out var idType1))
            {
                idType = value["idType"].ToString();
            }
            if (value.TryGetValue("idCardNo", out var idCardNo))
            {
                if (!string.IsNullOrEmpty(idCardNo.ToString()))
                {
                    sql += " and sfzh = @idCardNo";
                    SqlParamlist.Add(new SqlParameter("@idCardNo", idCardNo.ToString()));
                }
            }
            if (value.TryGetValue("startTime", out var startTime))
            {
                if (!string.IsNullOrEmpty(startTime.ToString()))
                {
                    sql += " and fjy_date>= @startTime";
                    SqlParamlist.Add(new SqlParameter("@startTime", startTime.ToString()));
                }
            }
            if (value.TryGetValue("endTime", out var endTime))
            {
                if (!string.IsNullOrEmpty(endTime.ToString()))
                {
                    sql += " and fjy_date<= @endTime";
                    SqlParamlist.Add(new SqlParameter("@endTime", endTime.ToString()));
                }
            }
            if (value.TryGetValue("type", out var type1))
            {
                type = value["type"].ToString();
                if (!type.Equals("02"))
                {
                    return ReturnResponseResult.GetJsonResult(false, "1", "传入的检验类型不对！", null);
                }
            }
            if (value.TryGetValue("merchId", out var _merchId))
            {
                //merchId = value["merchId"].ToString();
                merchId = GetLisconndic(_merchId.ToString());
                if (string.IsNullOrEmpty(merchId))
                {
                    return ReturnResponseResult.GetJsonResult(false, "1", "该医院没有配置连接！", null);
                }
            }

            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(merchId, CommandType.Text, sql, SqlParamlist.ToArray()).Tables[0];
                if (dt != null && dt.Rows.Count >= 1)
                {
                    return ReturnResponseResult.GetJsonResult(true, "", "", dt);
                }
                else
                {
                    return ReturnResponseResult.GetJsonResult(true, "", "", new string[] { });
                }
            }
            catch (System.Exception ex)
            {
                return ReturnResponseResult.GetJsonResult(false, "1", ex.Message, null);
            }
        }

        /// <summary>
        /// 获取报告单详情 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPost]
        public ResponseResult Get_LisReportDet([FromBody]JObject value)
        {
            string reportId= string.Empty, type = string.Empty , merchId=string.Empty;
            if (value.TryGetValue("reportId", out var _reportId))
            {
                reportId = value["reportId"].ToString();
            }
            if (value.TryGetValue("type", out var _type))
            {
                type = value["type"].ToString();
                if (!type.Equals("02"))
                {
                    return ReturnResponseResult.GetJsonResult(false, "1", "传入的检验类型不对！", null);
                }
            }
            if (value.TryGetValue("merchId", out var _merchId))
            {
                //merchId = value["merchId"].ToString();
                merchId = GetLisconndic(_merchId.ToString());
                if (string.IsNullOrEmpty(merchId))
                {
                    return ReturnResponseResult.GetJsonResult(false, "1", "该医院没有配置连接！", null);
                }
            }
            string sql = @"select a.fjy_id as reportId 
	                     ,'02' as type 
	                     ,'检验单' as title 
	                     ,fhz_name as patientName 
	                     ,sfzh as cardNo 
	                     ,fhz_sex as sex 
	                     ,cast(fhz_age as varchar) as age
	                     ,case when fhz_bed='' then (select top 1 fname from WWF_DEPT where fdept_id=fhz_dept) else fhz_bed end departName 
	                     ,null as doctorName 
	                     ,fjy_zt as status 
	                     ,fhz_zyh as hosUserNo 
	                     ,fapply_time as submissionTime 
	                     ,fcheck_time as reportTime 
	                     ,null as result 
	                     ,a.fremark as remark 
	                     ,(select top 1 fname from WWF_PERSON where fperson_id = fjy_user_id ) as checker 
	                     ,(select top 1 fname from WWF_PERSON where fperson_id = fchenk_user_id ) as auditor
	                     ,a.fapply_id as reportNo 
	                     ,fjy_instr as checkNo
	                     ,(select top 1 fname from SAM_INSTR where SAM_INSTR.finstr_id = fjy_instr) as checkName 
	                     ,null as checkType 
	                     ,null as checkSite 
	                     ,null as checkMethod 
	                     ,null as words 
	                     ,fjy_yb_code as sampleNo 
	                     ,'血液' as sampleType 
	                     ,convert(varchar,(select count(1) from SAM_JY_RESULT where SAM_JY_RESULT.fjy_id=a.fjy_id)) as itemNum 
                         ,null as itemList
                         ,null as medicalItem 
                    from dbo.SAM_JY a                    
                    where fjy_id= @reportId 
select b.fitem_code as itemNo 
	                     ,b.fitem_name as itemName 
	                     ,b.fvalue as resultValue 
	                     ,b.fitem_unit as unit 
	                     ,b.fitem_ref as referValue 
	                     ,b.fitem_badge as checkResult 
	                     ,fcheck_time as checkTime 
	                     ,(select top 1 fname from WWF_PERSON where fperson_id = fchenk_user_id ) as checkDoctor  
						 from dbo.SAM_JY a 
						 left join SAM_JY_RESULT b on a.fjy_id = b.fjy_id 
						 where b.fjy_id = @reportId ";
            List<SqlParameter> SqlParamlist = new List<SqlParameter>
            {
                new SqlParameter("@reportId", reportId)
            };
            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(merchId, CommandType.Text, sql, SqlParamlist.ToArray());
                
                if (ds != null && ds.Tables[0].Rows.Count >= 1)
                {
                    string json = JsonConvert.SerializeObject(ds.Tables[0]);

                    JArray jb = JArray.Parse(json);
                    //添加一个子集
                    jb[0]["itemList"] = JArray.Parse(JsonConvert.SerializeObject(ds.Tables[1]));
                    jb[0]["medicalItem"] = JArray.Parse("[]");
                    return ReturnResponseResult.GetJsonResult(true, "", "", jb);
                }
                else
                {
                    return ReturnResponseResult.GetJsonResult(true, "", "", new string[] { });
                }
            }
            catch (System.Exception ex)
            {
                return ReturnResponseResult.GetJsonResult(false, "1", ex.Message, null);
            }
        }

    }
}
