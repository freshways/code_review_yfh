﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace WebApi_Common
{
    public static class JSONHelper
    {
        #region DataTable 转换为Json 字符串
        /// <summary>
        /// DataTable 对象 转换为Json 字符串,这个代码是给母子手册接口用的，可以固定消息头
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ToJson(this DataTable dt)
        {
            try
            {
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                javaScriptSerializer.MaxJsonLength = Int32.MaxValue; //取得最大数值
                ArrayList arrayList = new ArrayList();
                foreach (DataRow dataRow in dt.Rows)
                {
                    Dictionary<string, object> dictionary = new Dictionary<string, object>();  //实例化一个参数集合
                    foreach (DataColumn dataColumn in dt.Columns)
                    {
                        dictionary.Add(dataColumn.ColumnName, dataRow[dataColumn.ColumnName].ToString());
                    }
                    arrayList.Add(dictionary); //ArrayList集合中添加键值
                }
                string result = javaScriptSerializer.Serialize(arrayList);  //返回一个json字符串

                return result;
            }
            catch (Exception ex)
            {
                return FormatJsonResult("", ex.Message);
            }

        }
        #endregion

        #region Json 字符串 转换为 DataTable数据集合
        /// <summary>
        /// Json 字符串 转换为 DataTable数据集合
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static DataTable ToDataTable(this string json)
        {
            DataTable dataTable = new DataTable();  //实例化
            DataTable result;
            try
            {
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                javaScriptSerializer.MaxJsonLength = Int32.MaxValue; //取得最大数值
                ArrayList arrayList = javaScriptSerializer.Deserialize<ArrayList>(json);
                if (arrayList.Count > 0)
                {
                    foreach (Dictionary<string, object> dictionary in arrayList)
                    {
                        if (dictionary.Keys.Count<string>() == 0)
                        {
                            result = dataTable;
                            return result;
                        }
                        if (dataTable.Columns.Count == 0)
                        {
                            foreach (string current in dictionary.Keys)
                            {
                                dataTable.Columns.Add(current, dictionary[current].GetType());
                            }
                        }
                        DataRow dataRow = dataTable.NewRow();
                        foreach (string current in dictionary.Keys)
                        {
                            dataRow[current] = dictionary[current];
                        }

                        dataTable.Rows.Add(dataRow); //循环添加行到DataTable中
                    }
                }
            }
            catch
            {
            }
            dataTable.TableName = "Table1";
            result = dataTable;
            return result;
        }
        #endregion

        #region 转换为string字符串类型
        /// <summary>
        ///  转换为string字符串类型
        /// </summary>
        /// <param name="s">获取需要转换的值</param>
        /// <param name="format">需要格式化的位数</param>
        /// <returns>返回一个新的字符串</returns>
        public static string ToStr(this object s, string format = "")
        {
            string result = "";
            try
            {
                if (format == "")
                {
                    result = s.ToString();
                }
                else
                {
                    result = string.Format("{0:" + format + "}", s);
                }
            }
            catch
            {
            }
            return result;
        }
        #endregion

        #region 临时写的一些转义方法
        /// <summary>
        /// 格式化Json数据
        /// </summary>
        /// <param name="Json"></param>
        /// <param name="Error"></param>
        /// <returns></returns>
        public static string FormatJsonResult(string Json, string Error = "")
        {
            //消息头
            string result = "";
            string header = "\"errno\":0,\"errmsg\":\"\",\"data\":{0}";
            string errorheader = "\"errno\":1,\"errmsg\":\"{0}\",\"data\":";
            try
            {
                if (string.IsNullOrEmpty(Error))
                    result = "{" + string.Format(header, Json) + "}"; //添加消息头
                else
                    result = "{" + string.Format(errorheader, Error) + "}";  //添加消息头
            }
            catch
            {
            }
            return result;

        }

        /// <summary>
        /// 处理json转换table的格式
        /// </summary>
        /// <param name="Json"></param>
        /// <returns></returns>
        public static string FormatJsonTOTable(string Json)
        {
            string result = "";
            //去最外面的大括号{}
            if (Json.IndexOf("{") == 0)
            {
                Json = Json.Substring(1, Json.Length - 2);
            }
            //截取消息头
            string header = "\"errno\":0,\"errmsg\":\"\",\"data\":";
            if (Json.Contains(header))
            {
                result = Json.Substring(header.Length);
            }
            return result;
        }
        #endregion
    }
}