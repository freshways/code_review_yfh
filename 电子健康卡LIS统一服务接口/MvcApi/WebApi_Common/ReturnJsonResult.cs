﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi_Common
{
    public class ReturnJsonResult
    {
        public static JsonResult<T> GetJsonResult<T>(int code, string msg, T data)
        {
            JsonResult<T> jsonResult = new JsonResult<T>();
            jsonResult.errno = code;
            jsonResult.errmsg = msg;
            jsonResult.data = data;
            return jsonResult;
        }
    }

    /// <summary>
    /// 定义统计返回json格式数据
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class JsonResult<T>
    {
        public int errno { get; set; }
        public string errmsg { get; set; }
        public T data { get; set; }
    }


    public class ReturnResponseResult
    {
        public static ResponseResult GetJsonResult(bool suc,string code, string msg, object data)
        {
            ResponseResult jsonResult = new ResponseResult();
            jsonResult.success = suc;
            jsonResult.errorCode = code;
            jsonResult.errorMsg = msg;
            jsonResult.content = data;
            return jsonResult;
        }
    }

    public class ResponseResult
    {
        /// <summary>
        ///表示返回的成功状态，true 为成功，否则 失败   
        /// </summary>
        public bool success { get; set; }

        /// <summary>
        /// 错误码，当返回状态 success=false 时提 供，非必需，如若需要后续需要统一制定一套状态码 
        /// </summary>
        public string errorCode { get; set; }

        /// <summary>
        /// 返回的错误信息，当返回状态 
        /// </summary>
        public string errorMsg { get; set; }

        /// <summary>
        /// 返回的结果数据，当返回状态 success=true 时提供 
        /// </summary>
        public object content { get; set; }
    }


}
