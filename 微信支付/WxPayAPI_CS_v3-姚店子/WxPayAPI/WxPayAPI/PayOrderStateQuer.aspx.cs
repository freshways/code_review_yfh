﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WxPayAPI
{
    public partial class PayOrderStateQuer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string s_单号 = Request["c_no"];

            if (string.IsNullOrEmpty(s_单号))
            {
                Response.Write("交易单号不能为空！");
                return;
            }

            string result = OrderQuery.Runs("", s_单号);//调用订单查询业务逻辑

            Log.Info(this.GetType().ToString(), "HIS微信支付订单查询结果：" + result);

            Response.Write(result);

        }
    }
}