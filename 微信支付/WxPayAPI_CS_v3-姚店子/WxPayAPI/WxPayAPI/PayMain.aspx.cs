﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WxPayAPI
{
    /**
    * 生成直接支付url，支付url有效期为2小时,模式二
    * @param c_no 商品ID
    * @param i_fee 支付金额
    * @return 模式二URL
    */
    public partial class PayMain : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string s_单号 = Request["c_no"];
            int i_金额 = Convert.ToInt32(Request["i_fee"]);
            if (string.IsNullOrEmpty(s_单号))
            {
                Response.Write("交易单号不能为空！");
                return;
            }
            Log.Info(this.GetType().ToString(), "HIS微信支付接口调用成功！单号：" + s_单号);

            NativePay nativePay = new NativePay();

            //生成扫码支付模式二url
            string url2 = nativePay.GetPayUrl(s_单号, i_金额);

            Response.Write(url2);
        }
    }
}