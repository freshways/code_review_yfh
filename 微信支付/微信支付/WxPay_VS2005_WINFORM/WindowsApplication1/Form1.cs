﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ThoughtWorks.QRCode.Codec;

namespace WxPayAPI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
           
        }
        //支付
        private void button1_Click(object sender, EventArgs e)
        {
            WxPayData result = MicroPay.Run("QQ", "1", textBox1.Text);

            //刷卡支付直接成功
            if (result.GetValue("return_code").ToString() == "SUCCESS" &&
                result.GetValue("result_code").ToString() == "SUCCESS")
            {
                MessageBox.Show("支付成功");
            }
            else 
            {
                MessageBox.Show("支付失败");
            }
           
        }
        //查单
        private void button2_Click(object sender, EventArgs e)
        {
            WxPayData result = OrderQuery.Run(textBox2.Text, "");
            MessageBox.Show(result.ToXml());
        }
        //退单
        private void button3_Click(object sender, EventArgs e)
        {
            WxPayData result = Refund.Run(textBox3.Text, textBox3.Text, "1", "1");
            //刷卡支付直接成功
            if (result.GetValue("return_code").ToString() == "SUCCESS" &&
                result.GetValue("result_code").ToString() == "SUCCESS")
            {
                MessageBox.Show("支付成功");
            }
            else
            {
                MessageBox.Show("支付失败");
            }
        }
        //退单查询
        private void button4_Click(object sender, EventArgs e)
        {
            WxPayData result = RefundQuery.Run("", "", textBox4.Text, "");
            MessageBox.Show(result.ToXml());
        }
        //账单查询
        private void button5_Click(object sender, EventArgs e)
        {
            WxPayData result = DownloadBill.Run(textBox5.Text, "ALL");

            if (result.IsSet("result"))
            {
                //
                MessageBox.Show(result.ToXml());

                //解析出来的数据
                string[] ar = result.GetValue("result").ToString().Split(new char[] { '\r', '\n'});
                string[] dtstr = ar[2].Split(new char[] { ',', '`' });
                string str = dtstr[1];
                DateTime dt = Convert.ToDateTime(str);
                //ar[1] = ""
                //ar[0]  "交易时间,公众账号ID,商户号,子商户号,设备号,微信订单号,商户订单号,用户标识,交易类型,交易状态,付款银行,货币种类,总金额,企业红包金额,微信退款单号,商户退款单号,退款金额,企业红包退款金额,退款类型,退款状态,商品名称,商户数据包,手续费,费率"
                //ar[ar.Length-3]"总交易单数,总交易额,总退款金额,总企业红包退款金额,手续费总金额"
            }
            else if (result.IsSet("return_code") && result.IsSet("return_msg"))
            {
                MessageBox.Show(result.GetValue("return_code") + "：" + result.GetValue("return_msg"));
            }

            MessageBox.Show(result.ToXml());
        }

        //扫码支付
        private void button6_Click(object sender, EventArgs e)
        {
            NativePay nativePay = new NativePay();
            //生成扫码支付模式二url
            string url2 = nativePay.GetPayUrl(1,"123456789","商品名称","商品标记","商品描述");

            //初始化二维码生成工具
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
            qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
            qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;
            qrCodeEncoder.QRCodeVersion = 0;
            qrCodeEncoder.QRCodeScale = 4;

            //将字符串生成二维码图片
            pictureBox1.Image = qrCodeEncoder.Encode(url2, Encoding.Default);
        }
    }
}