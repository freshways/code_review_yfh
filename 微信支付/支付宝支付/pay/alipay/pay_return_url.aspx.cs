﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using System.Collections.Generic;
using Com.Alipay;
using Oracle.ManagedDataAccess.Client;
using CHARGE;
using System.Reflection;

namespace CHARGE.BusinessCharge.alipay
{
    /// <summary>
    /// 功能：页面跳转同步通知页面
    /// 版本：3.3
    /// 日期：2012-07-10
    /// 说明：
    /// 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
    /// 该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
    /// 
    /// ///////////////////////页面功能说明///////////////////////
    /// 该页面可在本机电脑测试
    /// 可放入HTML等美化页面的代码、商户业务逻辑程序代码
    /// 该页面可以使用ASP.NET开发工具调试，也可以使用写文本函数LogResult进行调试
    /// </summary>
    public partial class pay_return_url : System.Web.UI.Page
    {
        private LogHelper log = LogFactory.GetLogger(MethodBase.GetCurrentMethod().DeclaringType.FullName);

        protected void Page_Load(object sender, EventArgs e)
        {
            SortedDictionary<string, string> sPara = GetRequestGet();

            if (sPara.Count > 0)//判断是否有带返回参数
            {
                Notify aliNotify = new Notify();
                bool verifyResult = aliNotify.Verify(sPara, Request.QueryString["notify_id"], Request.QueryString["sign"]);

                if (verifyResult)//验证成功
                {
                    //商户订单号
                    string out_trade_no = Request.QueryString["out_trade_no"];
                    //支付宝交易号
                    string trade_no = Request.QueryString["trade_no"];
                    //交易状态
                    string trade_status = Request.QueryString["trade_status"];

                    log.Info("订单" + out_trade_no + "接收到支付宝返回trade_status：" + trade_status);
                    // 支付成功
                    if (Request.QueryString["trade_status"] == "TRADE_FINISHED" || Request.QueryString["trade_status"] == "TRADE_SUCCESS")
                    {
                        // 根据订单状态判断是否更新订单状态、转移订单
                        PayPublic.afterAlipayReturn(out_trade_no, trade_no, "支付宝");
                    }
                    else
                    {
                        Response.Write("trade_status=" + Request.QueryString["trade_status"]);
                    }

                    //打印页面
                    Response.Write("<div style=\"margin: 100px auto;width: 700px;\"><div style=\"margin:auto;width: 50px;\"><img src=\"../../Images/warn.png\"></img><br/></div><a style=\"font-family:arial; font-size: 40px; font-weight: bold; color: red;\">支付成功，请点击写卡按钮进行写卡！</a></div>");
                }
                else//验证失败
                {
                    log.Info(Request.QueryString["notify_id"] + "验证失败");
                    Response.Write("验证失败");
                }
            }
            else
            {
                log.Info("无返回参数");
                Response.Write("无返回参数");
            }
        }

        //public void testc()
        //{
        //    string out_trade_no = "000000001958";
        //    string trade_no = "2015081400000000000000000002";
        //    List<SqlWithParams> list = new List<SqlWithParams>();
        //    OracleParameter[] parameters = null;
        //    // 1、更新t_order_Info
        //    string sql = getUpdateOrderInfoSql(out_trade_no, trade_no, ref parameters);
        //    list.Add(new SqlWithParams(sql, parameters));
        //    // 2、更新t_order_Detail
        //    sql = getUpdateOrderDetailSql(out_trade_no, ref parameters);
        //    list.Add(new SqlWithParams(sql, parameters));
        //    // 3、转移至表t_sale_info
        //    sql = getTransferIntoSaleInfoSql(out_trade_no, ref parameters);
        //    list.Add(new SqlWithParams(sql, parameters));
        //    // 4、转移至表t_sale_detail
        //    DataTable dt = DbHelperOra.Query("select id, meterId from t_order_detail t where t.orderId = '" + out_trade_no + "'").Tables[0];
        //    string id = "";
        //    string meterId = "";
        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        id = dt.Rows[i]["ID"].ToString();
        //        meterId = dt.Rows[i]["METERID"].ToString();
        //        sql = getTransferInfoSaleDetailSql(id, meterId, ref parameters);
        //        list.Add(new SqlWithParams(sql, parameters));
        //    }

        //    try
        //    {
        //        DbHelperOra.ExecuteSqlTran(list);
        //    }
        //    catch (Exception e)
        //    {
        //        //TODO 记录日志
        //        throw e;
        //    }
        //}

        /// <summary>
        /// 获取支付宝GET过来通知消息，并以“参数名=参数值”的形式组成数组
        /// </summary>
        /// <returns>request回来的信息组成的数组</returns>
        public SortedDictionary<string, string> GetRequestGet()
        {
            int i = 0;
            SortedDictionary<string, string> sArray = new SortedDictionary<string, string>();
            NameValueCollection coll;
            //Load Form variables into NameValueCollection variable.
            coll = Request.QueryString;

            // Get names of all forms into a string array.
            String[] requestItem = coll.AllKeys;

            for (i = 0; i < requestItem.Length; i++)
            {
                sArray.Add(requestItem[i], Request.QueryString[requestItem[i]]);
            }

            return sArray;
        }
    }
}