﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using System.Collections.Generic;
using Com.Alipay;
using System.Reflection;

namespace CHARGE.BusinessCharge.alipay
{
    /// <summary>
    /// 功能：服务器异步通知页面
    /// 版本：3.3
    /// 日期：2012-07-10
    /// 说明：
    /// 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
    /// 该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
    /// 
    /// ///////////////////页面功能说明///////////////////
    /// 创建该页面文件时，请留心该页面文件中无任何HTML代码及空格。
    /// 该页面不能在本机电脑测试，请到服务器上做测试。请确保外部可以访问该页面。
    /// 该页面调试工具请使用写文本函数logResult。
    /// 如果没有收到该页面返回的 success 信息，支付宝会在24小时内按一定的时间策略重发通知
    /// </summary>
    public partial class refund_notify_url : System.Web.UI.Page
    {
        private static LogHelper log = LogFactory.GetLogger(MethodBase.GetCurrentMethod().DeclaringType.FullName);
        protected void Page_Load(object sender, EventArgs e)
        {
            SortedDictionary<string, string> sPara = GetRequestPost();

            if (sPara.Count > 0)//判断是否有带返回参数
            {
                Notify aliNotify = new Notify();
                bool verifyResult = aliNotify.Verify(sPara, Request.Form["notify_id"], Request.Form["sign"]);

                if (verifyResult)//验证成功
                {
                    //批次号
                    string batch_no = Request.Form["batch_no"];

                    //批量退款数据中转账成功的笔数
                    string success_num = Request.Form["success_num"];

                    //批量退款数据中的详细信息
                    string result_details = Request.Form["result_details"];

                    log.Info("批次号batch_no：" + batch_no);
                    log.Info("success_num：" + success_num);
                    log.Info("支付宝退款通知result_details：" + result_details);

                    List<SqlWithParams> list = new List<SqlWithParams>();

                    //将result_details拆开
                    //2015091121001004610079383445^0.01^SUCCESS#2015091121001004610079294822^0.01^SUCCESS
                    string[] refundData = result_details.Split('#');
                    for (int i = 0; i < refundData.Length; i++)
                    {
                        string[] refundDetail = refundData[i].Split('^');
                        string tradeno = refundDetail[0];
                        string returnStatus = refundDetail[2];

                        string orderId = PayPublic.getOrderIdByTradeno(tradeno);
                        string status = PayPublic.orderStatus(orderId);

                        OracleParameter paramOrderID = new OracleParameter(":ORDERID", OracleDbType.Varchar2);
                        paramOrderID.Value = orderId;
                        OracleParameter[] parametersOrderID = new OracleParameter[] { paramOrderID };

                        string userid = PubClass.getUserIdsByOrderId(orderId)[0];
                        string meterIds = PubClass.getMeterIdsByOrderId(orderId)[0];
                        OracleParameter[] parameters = new OracleParameter[] { paramOrderID };
                        string content = string.Empty;
                        if (string.Equals(returnStatus, "SUCCESS"))
                        {
                            //正常(1) 8退款处理中  正常(2) 4退款成功                 正常(3) 6退款失败
                            if (string.Equals(status, "8"))
                            {
                                //添加要执行的sql
                                list.AddRange(PayPublic.refundSql(orderId));

                                content = "支付宝 通知:" + orderId + "退款成功！";
                                string sqllog = PubClass.getLogSqlWithParams("支付宝通知退款成功", orderId, content, userid, meterIds, ref parameters);
                                list.Add(new SqlWithParams(sqllog, parameters));

                                log.Info("支付宝通知退款成功, 订单(" + orderId + ")当前状态为[" + status + "] ");
                            }
                            else if (string.Equals(status, "4"))
                            {
                                //修改订单信息---该订单已经处理过了
                            }
                            else
                            {
                                content = "支付宝返回 通知:" + orderId + "退款成功,但订单状态异常！";
                                string sqllog = PubClass.getLogSqlWithParams("支付宝通知退款成功,但订单状态异常", orderId, content, userid, meterIds, ref parameters);
                                list.Add(new SqlWithParams(sqllog, parameters));
                                //异常情况  异常其他状态 -2订单取消；-1订单超时；1已下单待付款；2已付款；3已申请退款； 5退款被打回  7交易关闭
                                log.Error("订单(" + orderId + ")支付宝退款成功,返回信息,该订单当前状态为[" + status + "],状态异常");
                            }
                        }
                        else
                        {
                            if (string.Equals(status, "8"))
                            {
                                string sql = @"update t_order_info set STATUS=6 where orderid =:ORDERID";
                                list.Add(new SqlWithParams(sql, parametersOrderID));

                                content = "支付宝 通知:" + orderId + "退款失败！";
                                string sqllog = PubClass.getLogSqlWithParams("支付宝通知退款失败", orderId, content, userid, meterIds, ref parameters);
                                list.Add(new SqlWithParams(sqllog, parameters));

                                log.Info("支付宝通知退款失败, 订单(" + orderId + ")当前状态为[" + status + "] ");
                            }
                            else if (string.Equals(status, "6"))
                            {
                                //修改订单信息---该订单已经处理过了
                            }
                            else 
                            {
                                content = "支付宝 通知:" + orderId + "退款失败！";
                                string sqllog = PubClass.getLogSqlWithParams("支付宝通知退款失败", orderId, content, userid, meterIds, ref parameters);
                                list.Add(new SqlWithParams(sqllog, parameters));

                                log.Error("订单(" + orderId + ")支付宝通知退款失败,该订单当前状态为[" + status + "],状态异常");
                            }
                        }
                    }
                    if (list.Count > 0)
                    {
                        try
                        {
                            //DbHelperOra.ExecuteSql("update t_order_info t set t.status = '4', t.returndate = sysdate, t.remarks = '" + result_details + "' where t.orderid = '000000000002'");
                            DbHelperOra.ExecuteSqlTranWithParams(list);
                        }
                        catch (Exception exception)
                        {
                            log.Error("支付宝退款时,返回信息result_details:(" + result_details + "),执行数据库时失败", exception);
                            throw exception;
                        }
                    }
                    //判断是否在商户网站中已经做过了这次通知返回的处理
                    //如果没有做过处理，那么执行商户的业务程序
                    //如果有做过处理，那么不执行商户的业务程序
                    Response.Write("success");  //请不要修改或删除
                }
                else//验证失败
                {
                    Response.Write("fail");
                }
            }
            else
            {
                Response.Write("无通知参数");
            }
        }

        /// <summary>
        /// 获取支付宝POST过来通知消息，并以“参数名=参数值”的形式组成数组
        /// </summary>
        /// <returns>request回来的信息组成的数组</returns>
        public SortedDictionary<string, string> GetRequestPost()
        {
            int i = 0;
            SortedDictionary<string, string> sArray = new SortedDictionary<string, string>();
            NameValueCollection coll;
            //Load Form variables into NameValueCollection variable.
            coll = Request.Form;

            // Get names of all forms into a string array.
            String[] requestItem = coll.AllKeys;

            for (i = 0; i < requestItem.Length; i++)
            {
                sArray.Add(requestItem[i], Request.Form[requestItem[i]]);
            }
            return sArray;
        }
    }
}