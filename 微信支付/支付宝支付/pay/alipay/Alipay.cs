﻿using CHARGE;
using Com.Alipay;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;

namespace CHARGE.BusinessCharge.alipay
{
    public class Alipay
    {

        private static LogHelper log = LogFactory.GetLogger(MethodBase.GetCurrentMethod().DeclaringType.FullName);

        /// <summary>
        /// 提交订单至支付宝 调用后，Response.Write(sHtmlText);转至支付宝页面
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public static string generatorOrder(Order order)
        {
            //支付类型
            string payment_type = "1";
            //必填，不能修改
            //服务器异步通知页面路径
            string notify_url = "http://" + PubClass.webUrl + "/BusinessCharge/alipay/pay_notify_url.aspx";
            //需http://格式的完整路径，不能加?id=123这类自定义参数

            //页面跳转同步通知页面路径
            string return_url = "http://" + PubClass.webUrl + "/BusinessCharge/alipay/pay_return_url.aspx";
            //需http://格式的完整路径，不能加?id=123这类自定义参数，不能写成http://localhost/

            //商户订单号
            string out_trade_no = order.OrderId;
            //商户网站订单系统中唯一订单号，必填

            //订单名称
            string subject = order.OrderName;
            //必填

            //付款金额
            string total_fee = Convert.ToString(order.Amount);
            //必填

            //订单描述
            string body = order.Remarks;

            //商品展示地址
            string show_url = order.ShowUrl;
            //需以http://开头的完整路径，例如：http://www.商户网址.com/myorder.html

            //防钓鱼时间戳
            string anti_phishing_key = "";
            //若要使用请调用类文件submit中的query_timestamp函数

            //客户端的IP地址
            string exter_invoke_ip = "";
            //非局域网的外网IP地址，如：221.0.0.1

            //把请求参数打包成数组
            SortedDictionary<string, string> sParaTemp = new SortedDictionary<string, string>();
            sParaTemp.Add("partner", AlipayConfig.Partner);
            sParaTemp.Add("seller_email", AlipayConfig.Seller_email);
            sParaTemp.Add("_input_charset", AlipayConfig.Input_charset.ToLower());
            sParaTemp.Add("service", "create_direct_pay_by_user");
            sParaTemp.Add("payment_type", payment_type);
            sParaTemp.Add("notify_url", notify_url);
            sParaTemp.Add("return_url", return_url);
            sParaTemp.Add("out_trade_no", out_trade_no);
            sParaTemp.Add("subject", subject);
            sParaTemp.Add("total_fee", total_fee);
            sParaTemp.Add("body", body);
            sParaTemp.Add("show_url", show_url);
            sParaTemp.Add("anti_phishing_key", anti_phishing_key);
            sParaTemp.Add("exter_invoke_ip", exter_invoke_ip);

            //建立请求
            string sHtmlText = Submit.BuildRequest(sParaTemp, "get", "确认");
            //string sHtmlText = Submit.BuildRequest(sParaTemp);
            return sHtmlText;
        }

        /// <summary>
        /// 管理员查看可退款后调用退款接口，Response.Write(sHtmlText);转至支付宝页面
        /// </summary>
        /// <param name="batchNo">退款批次号</param>
        /// <param name="batchNum">退款的总笔数（可批量退款）</param>
        /// <param name="detailData">退款详细数据</param>
        /// <returns></returns>
        public static string refundOrder(string batchNo, int batchNum, string detailData)
        {
            //服务器异步通知页面路径
            string notify_url = "http://" + PubClass.webUrl + "/BusinessCharge/alipay/refund_notify_url.aspx";
            //卖家支付宝帐户
            string seller_email = AlipayConfig.Seller_email;
            //退款当天日期
            string refund_date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //必填，格式：年[4位]-月[2位]-日[2位] 小时[2位 24小时制]:分[2位]:秒[2位]，如：2007-10-01 13:13:13

            //批次号
            string batch_no = batchNo;
            //必填，格式：当天日期[8位]+序列号[3至24位]，如：201008010000001

            //退款笔数
            string batch_num = batchNum.ToString();
            //必填，参数detail_data的值中，“#”字符出现的数量加1，最大支持1000笔（即“#”字符出现的数量999个）

            //退款详细数据
            string detail_data = detailData;
            //必填，具体格式请参见接口技术文档

            //把请求参数打包成数组
            SortedDictionary<string, string> sParaTemp = new SortedDictionary<string, string>();
            sParaTemp.Add("partner", AlipayConfig.Partner);
            sParaTemp.Add("_input_charset", AlipayConfig.Input_charset.ToLower());
            sParaTemp.Add("service", "refund_fastpay_by_platform_pwd");
            sParaTemp.Add("notify_url", notify_url);
            sParaTemp.Add("seller_email", seller_email);
            sParaTemp.Add("refund_date", refund_date);
            sParaTemp.Add("batch_no", batch_no);
            sParaTemp.Add("batch_num", batch_num);
            sParaTemp.Add("detail_data", detail_data);
            
            //建立请求
            string sHtmlText = Submit.BuildRequest(sParaTemp, "get", "确认");
            return sHtmlText;
        }

        /// <summary>
        /// 批量退款时，组装退款详细数据
        /// </summary>
        /// <param name="orderIdList">需要退款的订单orderId列表</param>
        /// <returns></returns>
        public static string generatorDetailData(List<RefundDetailData> refundList)
        {
            string tradeNo = "";
            double refundAmount = 0.00;
            string refundReason = "";
            string detailData = "";
            for (int i = 0; i < refundList.Count; i++)
            {
                tradeNo = refundList[i].TradeNo;
                refundAmount = refundList[i].RefundAmount;
                refundReason = refundList[i].RefundReason;
                detailData = detailData + tradeNo + "^" + refundAmount + "^" + refundReason + "#";
            }
            return detailData.Substring(0, detailData.Length - 1);
        }

    }
}