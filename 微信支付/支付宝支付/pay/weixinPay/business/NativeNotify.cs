﻿using CHARGE;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WxPayAPI
{
    /// <summary>
    /// 扫码支付模式一回调处理类
    /// 接收微信支付后台发送的扫码结果，调用统一下单接口并将下单结果返回给微信支付后台
    /// </summary>
    public class NativeNotify:Notify
    {

        private LogHelper log = LogFactory.GetLogger(MethodBase.GetCurrentMethod().DeclaringType.FullName);

        public NativeNotify(Page page):base(page)
        {

        }

        public override void ProcessNotify()
        {
            WxPayData notifyData = GetNotifyData();

            //检查支付结果中transaction_id是否存在
            if (!notifyData.IsSet("transaction_id"))
            {
                //若transaction_id不存在，则立即返回结果给微信支付后台
                WxPayData res = new WxPayData();
                res.SetValue("return_code", "FAIL");
                res.SetValue("return_msg", "支付结果中微信订单号不存在");
                log.Error("微信通知支付结果,但是支付结果中微信订单号不存在;记录为[" + res.ToXml() + "]");
                //Log.Error(this.GetType().ToString(), "The Pay result is error : " + res.ToXml());
                page.Response.Write(res.ToXml());
                page.Response.End();
            }
            //微信交易号
            string transaction_id = notifyData.GetValue("transaction_id").ToString();
            //商户订单号
            string out_trade_no = notifyData.GetValue("out_trade_no").ToString();
            ////微信交易号
            //string trade_no = notifyData.GetValue("transaction_id").ToString();
            //交易状态
            string result_code = notifyData.GetValue("result_code").ToString();

            //查询订单，判断订单真实性
            if (!QueryOrder(transaction_id))
            {
                //若订单查询失败，则立即返回结果给微信支付后台
                WxPayData res = new WxPayData();
                res.SetValue("return_code", "FAIL");
                res.SetValue("return_msg", "订单查询失败");
                log.Error("微信通知支付结果(微信交易号为[" + transaction_id + "]),但是订单查询失败;记录为[" + res.ToXml() + "]");
                //Log.Error(this.GetType().ToString(), "Order query failure : " + res.ToXml());
                page.Response.Write(res.ToXml());
                page.Response.End();
            }
            //查询订单成功  // 根据订单状态判断是否更新订单状态、转移订单(result_code == "SUCCESS")
            else
            {
                WxPayData res = new WxPayData();
                res.SetValue("return_code", "SUCCESS");
                res.SetValue("return_msg", "OK");
                log.Info("订单" + out_trade_no + "接收到微信通知result_code：" + result_code + ";记录为[" + res.ToXml() + "]");
                //Log.Info(this.GetType().ToString(), "order query success : " + res.ToXml());
                
                PayPublic.afterAlipayReturn(out_trade_no, transaction_id, "微信");
                page.Response.Write(res.ToXml());
                page.Response.End();
            }

            #region 注释
            ////检查openid和product_id是否返回
            //if (!notifyData.IsSet("openid"))
            //{
            //    WxPayData res = new WxPayData();
            //    res.SetValue("return_code", "FAIL");
            //    res.SetValue("return_msg", "回调数据异常");
            //    log.Info("回调数据异常 : " + res.ToXml());
            //    page.Response.Write(res.ToXml());
            //    page.Response.End();
            //    return;
            //}

            ////商户订单号
            //string out_trade_no = notifyData.GetValue("out_trade_no").ToString();
            ////支付宝交易号
            //string trade_no = notifyData.GetValue("transaction_id").ToString();
            ////交易状态
            //string result_code = notifyData.GetValue("result_code").ToString();

            //log.Info("订单" + out_trade_no + "接收到微信通知result_code：" + result_code);
            //if (result_code == "SUCCESS")
            //{
            //    // 根据订单状态判断是否更新订单状态、转移订单
            //    PayPublic.afterAlipayReturn(out_trade_no, trade_no, "微信");
            //}
            //else
            //{
            //    // 付款失败

            //}

            ////调统一下单接口，获得下单结果
            //string openid = notifyData.GetValue("openid").ToString();
            ////string product_id = notifyData.GetValue("product_id").ToString();
            //string product_id = "000000000001";
            //WxPayData unifiedOrderResult = new WxPayData();
            //try
            //{
            //    unifiedOrderResult = UnifiedOrder(openid, product_id);
            //}
            //catch(Exception ex)//若在调统一下单接口时抛异常，立即返回结果给微信支付后台
            //{
            //    WxPayData res = new WxPayData();
            //    res.SetValue("return_code", "FAIL");
            //    res.SetValue("return_msg", "统一下单失败");
            //    Log.Error(this.GetType().ToString(), "UnifiedOrder failure : " + res.ToXml());
            //    page.Response.Write(res.ToXml());
            //    page.Response.End();
            //}

            //// 打印通知数据
            //foreach (KeyValuePair<string, object> pair in unifiedOrderResult.GetValues())
            //{
            //    Log.Info(this.GetType().ToString(), "返回的数据2：" + pair.Key + " = " + pair.Value);
            //}

            ////若下单失败，则立即返回结果给微信支付后台
            //if (!unifiedOrderResult.IsSet("appid") || !unifiedOrderResult.IsSet("mch_id") || !unifiedOrderResult.IsSet("prepay_id"))
            //{
            //    WxPayData res = new WxPayData();
            //    res.SetValue("return_code", "FAIL");
            //    res.SetValue("return_msg", "统一下单失败");
            //    Log.Error(this.GetType().ToString(), "UnifiedOrder failure : " + res.ToXml());
            //    page.Response.Write(res.ToXml());
            //    page.Response.End();
            //}

            ////统一下单成功,则返回成功结果给微信支付后台
            //WxPayData data = new WxPayData();
            //data.SetValue("return_code", "SUCCESS");
            //data.SetValue("return_msg", "OK");
            //data.SetValue("appid", WxPayConfig.APPID);
            //data.SetValue("mch_id", WxPayConfig.MCHID);
            //data.SetValue("nonce_str", WxPayApi.GenerateNonceStr());
            //data.SetValue("prepay_id", unifiedOrderResult.GetValue("prepay_id"));
            //data.SetValue("result_code", "SUCCESS");
            //data.SetValue("err_code_des", "OK");
            //data.SetValue("sign", data.MakeSign());

            //Log.Info(this.GetType().ToString(), "UnifiedOrder success , send data to WeChat : " + data.ToXml());
            //page.Response.Write(data.ToXml());
            //page.Response.End();
            #endregion
        }

        //查询订单
        private bool QueryOrder(string transaction_id)
        {
            WxPayData req = new WxPayData();
            req.SetValue("transaction_id", transaction_id);
            WxPayData res = WxPayApi.OrderQuery(req);
            if (res.GetValue("return_code").ToString() == "SUCCESS" &&
                res.GetValue("result_code").ToString() == "SUCCESS")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private WxPayData UnifiedOrder(string openId,string productId)
        {
            //统一下单
            WxPayData req = new WxPayData();
            req.SetValue("body", "test");
            req.SetValue("attach", "test");
            req.SetValue("out_trade_no", WxPayApi.GenerateOutTradeNo());
            req.SetValue("total_fee", 1);
            req.SetValue("time_start", DateTime.Now.ToString("yyyyMMddHHmmss"));
            req.SetValue("time_expire", DateTime.Now.AddMinutes(10).ToString("yyyyMMddHHmmss"));
            req.SetValue("goods_tag", "test");
            req.SetValue("trade_type", "NATIVE");
            req.SetValue("openid", openId);
            req.SetValue("product_id", productId);
            WxPayData result = WxPayApi.UnifiedOrder(req);
            return result;
        }
    }
}