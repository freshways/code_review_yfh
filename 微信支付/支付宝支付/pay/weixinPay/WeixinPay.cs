﻿using CHARGE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using WxPayAPI;

namespace CHARGE.BusinessCharge.weixinPay
{
    public class WeixinPay
    {

        private static LogHelper log = LogFactory.GetLogger(MethodBase.GetCurrentMethod().DeclaringType.FullName);

        /// <summary>
        /// 生成微信二维码url地址
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public static string generatorQRCodeUrl(Order order)
        {
            WxPayData data = new WxPayData();
            data.SetValue("body", order.OrderName);//商品描述
            data.SetValue("attach", order.OrderName);//附加数据
            data.SetValue("out_trade_no", order.OrderId);//订单号
            data.SetValue("total_fee", (int)(order.Amount * 100));//总金额
            // 上线时，取消此项
            //data.SetValue("total_fee", 1);//总金额

            data.SetValue("time_start", DateTime.Now.ToString("yyyyMMddHHmmss"));//交易起始时间
            data.SetValue("time_expire", DateTime.Now.AddMinutes(10).ToString("yyyyMMddHHmmss"));//交易结束时间
            data.SetValue("goods_tag", "jjj");//商品标记
            data.SetValue("trade_type", "NATIVE");//交易类型
            data.SetValue("product_id", "000000000001");//商品ID

            WxPayData result = WxPayApi.UnifiedOrder(data);//调用统一下单接口
            string url = result.GetValue("code_url").ToString();//获得统一下单接口返回的二维码链接

            log.Info("生成二维码url：" + url);
            return url;
        }



    }
}