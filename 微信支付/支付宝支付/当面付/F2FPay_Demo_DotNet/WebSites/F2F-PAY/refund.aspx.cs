﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using Com.Alipay;
using System.Threading;
using Aop.Api;
using Aop.Api.Request;
using Aop.Api.Response;
using Com.Alipay.Domain;
using Com.Alipay.Business;
using Com.Alipay.Model;

namespace F2FPay
{
    /// <summary>
    /// 功能：收单退款接口接入页
    /// 版本：3.3
    /// 日期：2012-07-05
    /// 说明：
    /// 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
    /// 该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
    /// 
    /// /////////////////注意///////////////////////////////////////////////////////////////
    /// 如果您在接口集成过程中遇到问题，可以按照下面的途径来解决
    /// 1、商户服务中心（https://b.alipay.com/support/helperApply.htm?action=consultationApply），提交申请集成协助，我们会有专业的技术工程师主动联系您协助解决
    /// 2、商户帮助中心（http://help.alipay.com/support/232511-16307/0-16307.htm?sh=Y&info_type=9）
    /// 3、支付宝论坛（http://club.alipay.com/read-htm-tid-8681712.html）
    /// 
    /// 如果不想使用扩展功能请把扩展功能参数赋空值。
    /// </summary>
    public partial class refund : System.Web.UI.Page
    {


        string result = "";

        IAlipayTradeService serviceClient = F2FBiz.CreateClientInstance(Config.serverUrl, Config.appId, Config.merchant_private_key,Config.version,
                             Config.sign_type, Config.alipay_public_key, Config.charset);

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void BtnAlipay_Click(object sender, EventArgs e)
        {
            ////////////////////////////////////////////请求参数////////////////////////////////////////////


            AlipayTradeRefundContentBuilder builder = BuildContent();

            
            AlipayF2FRefundResult refundResult  = serviceClient.tradeRefund(builder);

            //请在这里加上商户的业务逻辑程序代码
            //——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
            switch (refundResult.Status)
            {
                case ResultEnum.SUCCESS:
                    DoSuccessProcess(refundResult);
                    break;
                case ResultEnum.FAILED:
                    DoFailedProcess(refundResult);
                    break;
                case ResultEnum.UNKNOWN:
                    if (refundResult.response == null)
                    {
                        result = "配置或网络异常，请检查";
                    }
                    else
                    {
                        result = "系统异常，请走人工退款流程";
                    }
                    break;
            }
            Response.Redirect("result.aspx?Text=" + result);

        }



        private AlipayTradeRefundContentBuilder BuildContent()
        {
            AlipayTradeRefundContentBuilder builder = new AlipayTradeRefundContentBuilder();

            //支付宝交易号与商户网站订单号不能同时为空
            builder.out_trade_no =WIDout_trade_no.Text.Trim();

            //退款请求单号保持唯一性。
            builder.out_request_no = WIDout_request_no.Text.Trim();
           
            //退款金额
            builder.refund_amount = WIDrefund_amount.Text.Trim();

            builder.refund_reason = "refund reason";

            return builder;
            
        }

        private void DoSuccessProcess(AlipayF2FRefundResult refundResult)
        {

            //请添加退款成功后的处理
            result = refundResult.response.Body;
        }

        private void DoFailedProcess(AlipayF2FRefundResult refundResult)
        {

            //请添加退款失败后的处理
            result = refundResult.response.Body;
        }

    
    }
}