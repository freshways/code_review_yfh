﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices; 
namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        #region 发送底图
        [DllImport("FCSoftTerminalDLL.dll", //动态链接库的名称
        EntryPoint = "DZ_SendFormatPos")] //使用的字符集和其他
        public static extern int DZ_SendFormatPos(string strIP, int x, int y, int w, int h); // 

        [DllImport("FCSoftTerminalDLL.dll", //动态链接库的名称
        EntryPoint = "DZ_SendFormatContent")] //使用的字符集和其他
        public static extern int DZ_SendFormatContent(string strIP, string strFileName); // 
        #endregion

        #region 发送医生照片1
        [DllImport("FCSoftTerminalDLL.dll")] //使用的字符集和其他
        public static extern int DZ_SendFormatPos1(string strIP, int x, int y, int w, int h); // 

        [DllImport("FCSoftTerminalDLL.dll")] //使用的字符集和其他
        public static extern int DZ_SendFormatContent1(string strIP, string strFileName); // 
        #endregion

        #region 发送医生照片2
        [DllImport("FCSoftTerminalDLL.dll")] //使用的字符集和其他
        public static extern int DZ_SendFormatPos2(string strIP, int x, int y, int w, int h); // 

        [DllImport("FCSoftTerminalDLL.dll")] //使用的字符集和其他
        public static extern int DZ_SendFormatContent2(string strIP, string strFileName); // 

        #endregion
        #region 清除底图
        [DllImport("FCSoftTerminalDLL.dll")] //使用的字符集和其他
        public static extern int DZ_ClearFormatContent(string strIP); // 
        [DllImport("FCSoftTerminalDLL.dll")]
        public static extern int DZ_ClearFormatContent1(string strIP); //
        [DllImport("FCSoftTerminalDLL.dll")] //使用的字符集和其他
        public static extern int DZ_ClearFormatContent2(string strIP); // 
        #endregion
        #region  发送单行文本信息
        [DllImport("FCSoftTerminalDLL.dll")] //使用的字符集和其他
        public static extern int DZ_SendMultiText4(string strIP, int x, int y, int w, int h, string text, string color, int fontSize, int fontBold, int align); // 
        #endregion
        #region  清除文本信息
        [DllImport("FCSoftTerminalDLL.dll")] //使用的字符集和其他
        public static extern int DZ_ClearTxtInfo(string strIP); // 
        #endregion
         #region  发送多行文本信息
        [DllImport("FCSoftTerminalDLL.dll")] //使用的字符集和其他
        public static extern int DZ_AddWaitingInfo(int x, int y, int w, int h, string text, string color, int fontSize, int fontBold, int align); // 
        [DllImport("FCSoftTerminalDLL.dll")] //使用的字符集和其他
        public static extern int DZ_SendWaitingInfo(string strIP); // 
        #endregion
        #region  发送滚动信息
        [DllImport("FCSoftTerminalDLL.dll")] //使用的字符集和其他
        public static extern int DZ_SendMoveInfo(string strIP, int x, int y, int w, int h, string text, string color, string backcolor, int fontSize, int fontBold, int speed); // 
        [DllImport("FCSoftTerminalDLL.dll")] //使用的字符集和其他
        public static extern int DZ_ClearMoveInfo(string strIP); //       
       
        #endregion
        #region  发送日期定位信息
        [DllImport("FCSoftTerminalDLL.dll")] //使用的字符集和其他
        public static extern int DZ_SendDate(string strIP, int x, int y, int w, int h, string color, int fontSize); // 
        #endregion
        [DllImport("FCSoftTerminalDLL.dll")] //使用的字符集和其他
        public static extern int DZ_SendWav(string strIP, string text); // 
        #region  发送时间定位信息
        [DllImport("FCSoftTerminalDLL.dll")] //使用的字符集和其他
        public static extern int DZ_SendTime(string strIP, int x, int y, int w, int h, string color, int fontSize); // 
        #endregion

        #region "声明的报价器通信函数"
        [DllImport("cky95h.dll")]
        private static extern long dsbdll(int outport, string outstring);
        #endregion


        private int nResult;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            nResult = DZ_SendFormatPos(txtIP.Text, 40, 30, 200, 240);
            if (nResult==1 )
            {
                nResult = DZ_SendFormatContent(txtIP.Text, "main.jpg");
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int ii = 0;
            ii = DZ_ClearFormatContent(txtIP.Text);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            nResult = DZ_SendFormatPos1(txtIP.Text, 665, 492, 252, 220);
            if (nResult == 1)
            {
                nResult = DZ_SendFormatContent1(txtIP.Text, "1.jpg");                
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DZ_ClearFormatContent1(txtIP.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DZ_SendMultiText4(txtIP.Text, 300, 220, 300, 500, "潘军", "Blue", 32, 1, 1);
            DZ_SendMultiText4(txtIP.Text, 200, 290, 300, 500, "T108", "Red", 32, 1, 0);
            DZ_SendMultiText4(txtIP.Text, 200, 360, 300, 500, "主治医师", "Blue", 32, 1, 0);
            DZ_SendMultiText4(txtIP.Text, 580, 130, 800, 160, "消化内科 1诊室", "Blue", 80, 1, 1);
            DZ_SendMultiText4(txtIP.Text, 520, 280,900, 160, "正在就诊:001号 李喜芳", "Blue", 72, 1, 0);
            DZ_SendMultiText4(txtIP.Text, 520, 440,900, 160, "等候就诊", "Aqua", 72, 0, 1);
        }

        private void DZ_SendMultiText4(string p1, int p2, int p3, int p4, int p5, string p6, int p7, int p8, int p9, int p10)
        {
            throw new NotImplementedException();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            DZ_ClearTxtInfo(txtIP.Text);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            DZ_AddWaitingInfo( 30, 400,300,500, "潘军 主治医师\n医生擅长内科，医生擅长内科，医生擅长内科，", "Red", 24, 1, 0);
            //DZ_AddWaitingInfo(360, 280, 600, 160, "001号 李喜芳", "Red", 72, 1, 0);
            //DZ_AddWaitingInfo(360, 560, 600, 160, "002号 李喜芳", "Red", 72, 1, 0);
            //DZ_SendWaitingInfo(txtIP.Text);
            //DZ_AddWaitingInfo(30, 400, 300, 500, "潘军 主治医师\n医生擅长内科，医生擅长内科，", "Red", 24, 1, 0);
            //DZ_AddWaitingInfo(360, 280, 600, 160, "001号 李喜芳", "Red", 72, 1, 0);
            //DZ_AddWaitingInfo(360, 560, 600, 160, "002号 李喜芳", "Red", 72, 1, 0);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            DZ_SendMoveInfo(txtIP.Text, 300, 300, 1024, 60, textBox1.Text.Trim(), "red","black", 42, 1, 10);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            DZ_ClearMoveInfo(txtIP.Text);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
         //   DZ_SendMultiText4(txtIP.Text, 30, 400, 300, 500, "潘军 主治医师\n医生擅长内科，医生擅长内科，医生擅长内科，", 234, 24, 1, 0);
            //DZ_SendMultiText4(txtIP.Text, 30, 400, 300, 500, "请小心，", 234, 24, 1, 0);
        }

        private void timer2_Tick(object sender, EventArgs e)
        {            
            //DZ_AddWaitingInfo(360, 280, 600, 160, "001号 李喜芳", "Red", 72, 1, 0);
            //DZ_AddWaitingInfo(360, 560, 600, 160, "002号 李喜芳", "Red", 72, 1, 0);
            //DZ_SendWaitingInfo(txtIP.Text);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            try
            {
                DZ_SendWav(txtIP.Text, textBox2.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
                //throw ex;
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            DZ_SendDate(txtIP.Text, Convert.ToInt32(textBox3.Text),Convert.ToInt32(textBox4.Text), 600, 160, "Blue", 34);

        }

        private void button12_Click(object sender, EventArgs e)
        {
            DZ_SendTime(txtIP.Text, Convert.ToInt32(textBox5.Text), Convert.ToInt32(textBox6.Text), 600, 160, "Blue", 34);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            DZ_AddWaitingInfo(30, 400, 300, 500, "  ", "Red", 24, 1, 0);
            DZ_AddWaitingInfo(360, 280, 600, 160, " ", "Red", 72, 1, 0);
            DZ_AddWaitingInfo(360, 560, 600, 160, " ", "Red", 72, 1, 0);
            DZ_SendWaitingInfo(txtIP.Text);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            nResult = DZ_SendFormatPos2(txtIP.Text, 1081, 492, 252, 220);
            if (nResult == 1)
            {
                nResult = DZ_SendFormatContent2(txtIP.Text, "1.jpg");
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {

            dsbdll(nResult, txt指令.Text);
            //nResult = DZ_SendFormatPos1(txtIP.Text, 130, 180, 252, 260);
            //if (nResult == 1)
            //{
            //    nResult = DZ_SendFormatContent1(txtIP.Text, "147.jpg");
            //}
        }
    }
}
