﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterFaceBJQ.WxPay
{
    public enum wxPayState
    {
        /// <summary>
        /// 付款成功
        /// </summary>
        SUCCESS, 
        /// <summary>
        /// 未付款
        /// </summary>
        NOTPAY,  
        /// <summary>
        /// 退款
        /// </summary>
        REFUND, 
        /// <summary>
        /// 错误
        /// </summary>
        Error
    }
}
