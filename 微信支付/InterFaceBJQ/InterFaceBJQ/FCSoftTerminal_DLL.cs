﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using net.library;

namespace InterFaceBJQ
{
    /// <summary>
    /// 报价器接口主函数
    /// </summary>
    public class Main
    {
        private static Client _client;
        public static UserCollection allUser;

        #region 初始化通讯

        private static void OnUserChanged(UserCollection users)
        {
            allUser = users;
        }

        public static void Start()
        {
            _client = new Client();
            _client.OnUserChanged += OnUserChanged;
            if (!_client.IsOpen)
            {
                _client.Login("SendUser", "");
                _client.Start();
            }
        }

        public static void Stop()
        {
            if (_client.IsOpen)
                _client.Offline();
        }

        static void SendMessage(string msg, User user)
        {
            P2P_TalkMessage msgg = new P2P_TalkMessage(msg);
            _client.SendMessage(msgg, user);
        }

        static void SendMessage(string msg)
        {
            Start();
            if (_client.IsOpen)
            {
                SendMessage(msg, "192.168.1.100");
            }
        }
        static void SendMessage(string msg, String user)
        {
            P2S2P_TalkMessage msgg = new P2S2P_TalkMessage(msg, user);
            _client.SendMessage(msgg);
        } 
        #endregion

        #region 发送命令信息
        /// <summary>
        /// Init初始化
        /// </summary>
        public static void _s初始化()
        {
            SendMessage("&Sc$"); //清屏
            SendMessage("&C21您好,请稍等！$"); //您好
        }

        public static void _s员工信息(string s操作工号)
        {
            SendMessage("&C01|" + s操作工号 + "$"); //pic            
        }

        /// <summary>
        /// 患者信息
        /// </summary>
        public static void _s患者信息(string s姓名, string s性别, string s年龄, string 年龄单位 = "岁")
        {
            SendMessage("&C11| 姓名：" + s姓名 + " 性别：" + s性别 + " " + s年龄 + " 岁$"); //您好
            SendMessage("SFZ");
        }

        /// <summary>
        /// 患者信息+1
        /// </summary>
        /// <param name="outstring">患者信息串，姓名+空格+性别+空格+年龄+年龄单位（测试 男 11岁）</param>
        public static void _s患者信息(string outstring)
        {
            SendMessage("&C11| " + outstring + "$");
            SendMessage("SFZ");
        }

        /// <summary>
        /// 应收金额
        /// 应收同时生成付款二维码，如果有报销的需要在报销后进行重新生成
        /// </summary>
        /// <param name="s应收金额"></param>
        public static void _s应收金额(string s应收金额, string MZID = "")
        {
            SendMessage("&C21应  收| " + s应收金额 + "$"); //付款信息-应收
            SendMessage("J"); //付款信息-应收
            SendMessage("&C91|" + s应收金额 + "|" + MZID + "$");
        }

        /// <summary>
        /// 报销金额
        /// </summary>
        /// <param name="i实收金额"></param>
        public static void _s报销金额(string s报销金额)
        {
            SendMessage("&C31报  销|" + s报销金额 + "$"); //报销
            SendMessage("Y"); //付款信息-报销
        }

        /// <summary>
        /// 报销后实收金额
        /// 该调用时为了区分正常现金缴费，报销后需要重新生成付款金额二维码
        /// </summary>
        /// <param name="i实收金额"></param>
        public static void _s报销后实收金额(string s实收金额, string MZID = "")
        {
            SendMessage("&C41实  收|" + s实收金额 + "$"); //报销
            SendMessage("Y"); //付款信息-报销
            SendMessage("&C91|" + s实收金额 + "|" + MZID + "$");
        }

        /// <summary>
        /// 实收金额
        /// </summary>
        /// <param name="i实收金额"></param>
        public static void _s实收金额(string s实收金额)
        {
            SendMessage("&C31实  收|" + s实收金额 + "$"); //实收
            SendMessage("Y"); //付款信息-实收
        }

        public static void _s找零金额(string s找零金额)
        {
            SendMessage("&C41找  零|" + s找零金额 + "$"); //找零
            SendMessage("Z"); //付款信息-找零金额
            SendMessage("D"); //付款信息-找零当面点清            
        }

        public static void _SendMsg(string cmd)
        {
            SendMessage(cmd);
        } 
        #endregion

        #region 暂时不用

        /// <summary>
        /// 背景图片替换
        /// </summary>
        /// <param name="strIP">设备IP</param>
        /// <param name="strFileName">图片名称-main.jpg</param>
        /// <param name="x">横向坐标</param>
        /// <param name="y">纵向坐标</param>
        /// <param name="w">宽</param>
        /// <param name="h">高</param>
        public static void _s替换底图(string strIP, string strFileName = "main.jpg", int x = 40, int y = 30, int w = 200, int h = 240)
        {

        }

        /// <summary>
        /// 发送语音消息
        /// </summary>
        /// <param name="strIP">设备IP</param>
        /// <param name="text">语音文本</param>
        public static void _s语音(string strIP, string text)
        {

        }

        /// <summary>
        /// 滚动文本
        /// </summary>
        /// <param name="strIP">设备IP</param>
        /// <param name="text">滚动文本</param>
        /// <param name="x">坐标-横</param>
        /// <param name="y">坐标-纵</param>
        /// <param name="w">宽度</param>
        /// <param name="h">高度</param>
        /// <param name="color">颜色</param>
        /// <param name="backcolor">背景颜色</param>
        /// <param name="fontSize">字体大小</param>
        /// <param name="fontBold"></param>
        /// <param name="speed"></param>
        public static void _s滚动文本(string strIP, string text, int x = 300, int y = 300, int w = 1024, int h = 60, string color = "red",
            string backcolor = "black", int fontSize = 42, int fontBold = 1, int speed = 10)
        {

        }

        /// <summary>
        /// 微信支付图片显示
        /// </summary>
        /// <param name="strIP">报价器IP地址</param>
        /// <param name="Pic">图片名称已默认，除非重新指定，否则不用传入参数</param>
        /// <param name="x">图片坐标-横</param>
        /// <param name="y">图片坐标-纵</param>
        /// <param name="w">图片高度</param>
        /// <param name="h">图片宽度</param>
        public static void _s微信支付图片显示(string strIP, string Pic = "2.jpg", int x = 1092, int y = 522, int w = 220, int h = 220)
        {

        }

        /// <summary>
        /// 支付宝支付图片显示
        /// </summary>
        /// <param name="strIP">报价器IP地址</param>
        /// <param name="Pic">图片名称已默认，除非重新指定，否则不用传入参数</param>
        /// <param name="x">图片坐标-横</param>
        /// <param name="y">图片坐标-纵</param>
        /// <param name="w">图片高度</param>
        /// <param name="h">图片宽度</param>
        public static void _s支付宝支付图片显示(string strIP, string Pic = "1.jpg", int x = 682, int y = 522, int w = 220, int h = 220)
        {

        }

        #endregion
    }
}
