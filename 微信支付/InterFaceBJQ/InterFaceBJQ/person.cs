﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterFaceBJQ
{

    public class personMZSF : HIS.COMM.personYB
    {
        #region 微信支付变量
        private  InterFaceBJQ.WxPay.wxPayState WxPayState = WxPay.wxPayState.NOTPAY;
        public  InterFaceBJQ.WxPay.wxPayState _WxPayState
        { 
            get { return WxPayState; } 
            set { WxPayState = value; } 
        }

        public  string _s微信支付单据号 { get; set; }

        public  decimal _i微信支付金额 { get; set; }

        public  bool _WxZfState { get; set; } 
        #endregion


        #region 支付宝变量
        public  InterFaceBJQ.ZfbPay.zfbPayState _ZfbPayState { get; set; }

        public  string _s支付宝支付单据号 { get; set; }

        public  decimal _i支付宝支付金额 { get; set; }

        public  bool _ZfbZfState { get; set; } 
        #endregion
    }
}
