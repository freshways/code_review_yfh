﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Xml;

namespace InterFaceBJQ
{
    public class XMLHelper
    {

        #region 转换
        /// <summary>
        /// xml格式转换
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ConvertDataTableToEmrXml(DataTable dt)
        {
            StringBuilder strXml = new StringBuilder();
            strXml.AppendLine("<?xml version='1.0' encoding='utf-8' ?>");
            strXml.AppendLine("<GROUP>");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                strXml.AppendLine("<ITEM ");
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    strXml.AppendLine(dt.Columns[j].ColumnName + "='" + dt.Rows[i][j] + "' ");
                }
                strXml.AppendLine("/>");
            }
            strXml.AppendLine("</GROUP>");

            return strXml.ToString();
        }

        /// <summary>
        /// 解析xml返回字段
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static DataTable ConvertXmlToEmrData(string xml)
        {
            DataSet ds = ConvertXmlToDataTable(xml);
            if (ds == null || ds.Tables.Count <= 0)
                return null;
            DataTable dt = ds.Tables[0];
            return dt;
            //if (dt == null || dt.Rows.Count <= 0)
            //    return "";
            //return dt.Rows[0][0].ToString();
        }

        public static string Requestxml(string xml)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(xml);
            return xmldoc.SelectSingleNode("/xml/trade_state").InnerXml;
        }

        /// <summary>
        /// 获取订单的状态text
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static string RequestxmlText(string xml)
        {
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xml);
                return xmldoc.SelectSingleNode("/xml/trade_state").InnerText;
            }
            catch
            {
                return xml;
            }
        }

        #endregion

        #region xmlconvert
        /// <summary>
        /// 标准对称
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ConvertDataTableToXml(DataTable dt)
        {
            StringBuilder strXml = new StringBuilder();
            strXml.AppendLine("<GROUP>");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                strXml.AppendLine("<ITEM>");
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    strXml.AppendLine("<" + dt.Columns[j].ColumnName + ">" + dt.Rows[i][j] + "</" + dt.Columns[j].ColumnName + ">");
                }
                strXml.AppendLine("</ITEM>");
            }
            strXml.AppendLine("</GROUP>");

            return strXml.ToString();
        }

        private static DataSet ConvertXmlToDataTable(string xmlData)
        {
            StringReader stream = null;
            XmlTextReader reader = null;
            try
            {
                DataSet xmlDS = new DataSet();
                stream = new StringReader(xmlData);
                reader = new XmlTextReader(stream);
                xmlDS.ReadXml(reader);

                return xmlDS;
            }
            catch (Exception ex)
            {
                string strTest = ex.Message;
                return null;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
        }
        #endregion

    }
}
