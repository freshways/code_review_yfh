﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Windows.Forms;
using ThoughtWorks.QRCode.Codec;
using System.Threading;

namespace InterFaceBJQ.ZfbPay
{
    public class ZfbPayHelper
    {
        /// <summary>
        /// 获取支付url-生成支付二维码
        /// </summary>
        /// <param name="c_no">单号</param>
        /// <param name="i_fee">金额</param>
        /// <returns></returns>
        public static object GetZfbURL(string c_no, string i_fee)
        {
            string u = "http://192.168.120.254:280/PayMain.aspx?c_no=" + c_no + "&i_fee=" + i_fee; //中转服务器地址

            //初始化二维码生成工具
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
            qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
            qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;
            qrCodeEncoder.QRCodeScale = 3;
            qrCodeEncoder.QRCodeVersion = 8;

            try
            {
                //从中转服务器获取支付的二维码地址
                string url2 = SendRequest(u, Encoding.UTF8);

                //将字符串生成二维码图片
                //把生成后的图片保存到程序目录下
                string path = Application.StartupPath + "\\1.jpg";
                qrCodeEncoder.Encode(url2, Encoding.Default).Save(path);
                return qrCodeEncoder.Encode(url2, Encoding.Default);
            }
            catch
            {
                //System.Drawing.Bitmap bt = new System.Drawing.Bitmap(;
                return null ;
            }
        }

        /// <summary>
        /// 获取支付订单状态
        /// </summary>
        /// <param name="c_no">单号</param>
        /// <param name="i_fee">金额</param>
        /// <returns></returns>
        public static zfbPayState GetZfbOrderState(string c_no)
        {
            string u = "http://192.168.120.254:280/PayOrderStateQuer.aspx?c_no=" + c_no; //中转服务器地址

            zfbPayState state = new zfbPayState(); //初始化单据状态

            try
            {
                //从中转服务器获取订单状态
                string xml = SendRequest(u, Encoding.UTF8);
                switch (XMLHelper.RequestxmlText(xml))
                {
                    case "SUCCESS":
                        state = zfbPayState.SUCCESS;
                        break;
                    case "FAILED":
                        state = zfbPayState.FAILED;
                        break;
                    case "UNKNOWN":
                        state = zfbPayState.UNKNOWN;
                        break;
                    default:
                        break;
                }
            }
            catch
            {
                state = zfbPayState.UNKNOWN;
            }
            return state;
        }


        /// <summary>   
        /// Get方式获取url地址输出内容   
        /// </summary> /// <param name="url">url</param>   
        /// <param name="encoding">返回内容编码方式，例如：Encoding.UTF8</param>   
        public static String SendRequest(String url, Encoding encoding)
        {

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Method = "GET";
            HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
            StreamReader sr = new StreamReader(webResponse.GetResponseStream(), encoding);
            try
            {
                return sr.ReadLine();
            }
            finally
            {
                if (sr != null) sr.Close();
            }
        }

        /// <summary>
        /// 获取交易单号
        /// </summary>
        /// <param name="iDW">单位编码</param>
        /// <returns></returns>
        public static String GetPayNo(string iDW = "10") //默认是从姚店子开始的
        {
            //考虑到门诊收款都是一个窗口，出现重复的几率很小，默认获取单据格式是 单位编码+日期流水（yyyyMMddHHmmss）            
            return iDW + DateTime.Now.ToString("yyyyMMddHHmmss") + "00" + (new Random()).Next(100, 999).ToString(); //订单号由HIS收款时调用生成 
        }

    }
}
