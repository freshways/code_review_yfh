﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterFaceBJQ.ZfbPay
{
    public enum zfbPayState
    {
        /// <summary>
        /// 付款成功
        /// </summary>
        SUCCESS = 0,
        /// <summary>
        /// 未付款
        /// </summary>
        FAILED = 1,
        /// <summary>
        /// 网络错误
        /// </summary>
        UNKNOWN = 2,
    }
}
