﻿namespace Main
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.button语音 = new System.Windows.Forms.Button();
            this.txtIP = new System.Windows.Forms.TextBox();
            this.txtWav = new System.Windows.Forms.TextBox();
            this.button应收 = new System.Windows.Forms.Button();
            this.button实收 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button找零 = new System.Windows.Forms.Button();
            this.txt应收 = new System.Windows.Forms.TextBox();
            this.txt实收 = new System.Windows.Forms.TextBox();
            this.txt找零 = new System.Windows.Forms.TextBox();
            this.button支付宝 = new System.Windows.Forms.Button();
            this.button微信 = new System.Windows.Forms.Button();
            this.button清除 = new System.Windows.Forms.Button();
            this.button患者 = new System.Windows.Forms.Button();
            this.button滚动 = new System.Windows.Forms.Button();
            this.txt滚动 = new System.Windows.Forms.TextBox();
            this.button清除滚动 = new System.Windows.Forms.Button();
            this.button底图 = new System.Windows.Forms.Button();
            this.button稍等 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button员工 = new System.Windows.Forms.Button();
            this.button一键 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonTest = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button语音
            // 
            this.button语音.Location = new System.Drawing.Point(33, 98);
            this.button语音.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button语音.Name = "button语音";
            this.button语音.Size = new System.Drawing.Size(123, 36);
            this.button语音.TabIndex = 0;
            this.button语音.Text = "发送语音文本";
            this.button语音.UseVisualStyleBackColor = true;
            this.button语音.Click += new System.EventHandler(this.button语音_Click);
            // 
            // txtIP
            // 
            this.txtIP.Location = new System.Drawing.Point(84, 35);
            this.txtIP.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtIP.Name = "txtIP";
            this.txtIP.Size = new System.Drawing.Size(176, 25);
            this.txtIP.TabIndex = 1;
            this.txtIP.Text = "192.168.0.35";
            // 
            // txtWav
            // 
            this.txtWav.Location = new System.Drawing.Point(188, 104);
            this.txtWav.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtWav.Name = "txtWav";
            this.txtWav.Size = new System.Drawing.Size(313, 25);
            this.txtWav.TabIndex = 2;
            // 
            // button应收
            // 
            this.button应收.Location = new System.Drawing.Point(33, 322);
            this.button应收.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button应收.Name = "button应收";
            this.button应收.Size = new System.Drawing.Size(123, 36);
            this.button应收.TabIndex = 3;
            this.button应收.Text = "应收";
            this.button应收.UseVisualStyleBackColor = true;
            this.button应收.Click += new System.EventHandler(this.button应收_Click);
            // 
            // button实收
            // 
            this.button实收.Location = new System.Drawing.Point(33, 372);
            this.button实收.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button实收.Name = "button实收";
            this.button实收.Size = new System.Drawing.Size(123, 36);
            this.button实收.TabIndex = 3;
            this.button实收.Text = "实收";
            this.button实收.UseVisualStyleBackColor = true;
            this.button实收.Click += new System.EventHandler(this.button实收_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 39);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "IP:";
            // 
            // button找零
            // 
            this.button找零.Location = new System.Drawing.Point(33, 422);
            this.button找零.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button找零.Name = "button找零";
            this.button找零.Size = new System.Drawing.Size(123, 36);
            this.button找零.TabIndex = 3;
            this.button找零.Text = "找零";
            this.button找零.UseVisualStyleBackColor = true;
            this.button找零.Click += new System.EventHandler(this.button找零_Click);
            // 
            // txt应收
            // 
            this.txt应收.Location = new System.Drawing.Point(188, 329);
            this.txt应收.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt应收.Name = "txt应收";
            this.txt应收.Size = new System.Drawing.Size(103, 25);
            this.txt应收.TabIndex = 2;
            this.txt应收.Text = "12";
            // 
            // txt实收
            // 
            this.txt实收.Location = new System.Drawing.Point(187, 379);
            this.txt实收.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt实收.Name = "txt实收";
            this.txt实收.Size = new System.Drawing.Size(103, 25);
            this.txt实收.TabIndex = 2;
            this.txt实收.Text = "15";
            // 
            // txt找零
            // 
            this.txt找零.Location = new System.Drawing.Point(187, 429);
            this.txt找零.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt找零.Name = "txt找零";
            this.txt找零.Size = new System.Drawing.Size(103, 25);
            this.txt找零.TabIndex = 2;
            this.txt找零.Text = "3";
            // 
            // button支付宝
            // 
            this.button支付宝.Location = new System.Drawing.Point(33, 210);
            this.button支付宝.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button支付宝.Name = "button支付宝";
            this.button支付宝.Size = new System.Drawing.Size(123, 36);
            this.button支付宝.TabIndex = 3;
            this.button支付宝.Text = "支付宝图片";
            this.button支付宝.UseVisualStyleBackColor = true;
            this.button支付宝.Click += new System.EventHandler(this.button支付宝_Click);
            // 
            // button微信
            // 
            this.button微信.Location = new System.Drawing.Point(187, 210);
            this.button微信.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button微信.Name = "button微信";
            this.button微信.Size = new System.Drawing.Size(123, 36);
            this.button微信.TabIndex = 3;
            this.button微信.Text = "微信图片";
            this.button微信.UseVisualStyleBackColor = true;
            this.button微信.Click += new System.EventHandler(this.button微信_Click);
            // 
            // button清除
            // 
            this.button清除.Location = new System.Drawing.Point(344, 210);
            this.button清除.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button清除.Name = "button清除";
            this.button清除.Size = new System.Drawing.Size(123, 36);
            this.button清除.TabIndex = 3;
            this.button清除.Text = "清除图片";
            this.button清除.UseVisualStyleBackColor = true;
            this.button清除.Click += new System.EventHandler(this.button清除_Click);
            // 
            // button患者
            // 
            this.button患者.Location = new System.Drawing.Point(33, 268);
            this.button患者.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button患者.Name = "button患者";
            this.button患者.Size = new System.Drawing.Size(123, 36);
            this.button患者.TabIndex = 3;
            this.button患者.Text = "患者信息";
            this.button患者.UseVisualStyleBackColor = true;
            this.button患者.Click += new System.EventHandler(this.button患者_Click);
            // 
            // button滚动
            // 
            this.button滚动.Location = new System.Drawing.Point(33, 152);
            this.button滚动.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button滚动.Name = "button滚动";
            this.button滚动.Size = new System.Drawing.Size(123, 36);
            this.button滚动.TabIndex = 3;
            this.button滚动.Text = "滚动文本";
            this.button滚动.UseVisualStyleBackColor = true;
            this.button滚动.Click += new System.EventHandler(this.button滚动_Click);
            // 
            // txt滚动
            // 
            this.txt滚动.Location = new System.Drawing.Point(188, 159);
            this.txt滚动.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt滚动.Name = "txt滚动";
            this.txt滚动.Size = new System.Drawing.Size(239, 25);
            this.txt滚动.TabIndex = 2;
            this.txt滚动.Text = "找零请当面点清！欢迎使用移动支付功能！";
            // 
            // button清除滚动
            // 
            this.button清除滚动.Location = new System.Drawing.Point(436, 152);
            this.button清除滚动.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button清除滚动.Name = "button清除滚动";
            this.button清除滚动.Size = new System.Drawing.Size(67, 36);
            this.button清除滚动.TabIndex = 3;
            this.button清除滚动.Text = "清除";
            this.button清除滚动.UseVisualStyleBackColor = true;
            this.button清除滚动.Click += new System.EventHandler(this.button清除滚动_Click);
            // 
            // button底图
            // 
            this.button底图.Location = new System.Drawing.Point(269, 29);
            this.button底图.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button底图.Name = "button底图";
            this.button底图.Size = new System.Drawing.Size(99, 36);
            this.button底图.TabIndex = 0;
            this.button底图.Text = "替换底图";
            this.button底图.UseVisualStyleBackColor = true;
            this.button底图.Click += new System.EventHandler(this.button底图_Click);
            // 
            // button稍等
            // 
            this.button稍等.Location = new System.Drawing.Point(380, 29);
            this.button稍等.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button稍等.Name = "button稍等";
            this.button稍等.Size = new System.Drawing.Size(123, 36);
            this.button稍等.TabIndex = 0;
            this.button稍等.Text = "您好请稍等";
            this.button稍等.UseVisualStyleBackColor = true;
            this.button稍等.Click += new System.EventHandler(this.button稍等_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(187, 268);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(123, 36);
            this.button2.TabIndex = 3;
            this.button2.Text = "付款信息";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button员工
            // 
            this.button员工.Location = new System.Drawing.Point(344, 268);
            this.button员工.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button员工.Name = "button员工";
            this.button员工.Size = new System.Drawing.Size(123, 36);
            this.button员工.TabIndex = 3;
            this.button员工.Text = "员工信息";
            this.button员工.UseVisualStyleBackColor = true;
            this.button员工.Click += new System.EventHandler(this.button员工_Click);
            // 
            // button一键
            // 
            this.button一键.Location = new System.Drawing.Point(344, 322);
            this.button一键.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button一键.Name = "button一键";
            this.button一键.Size = new System.Drawing.Size(123, 36);
            this.button一键.TabIndex = 5;
            this.button一键.Text = "一键测试";
            this.button一键.UseVisualStyleBackColor = true;
            this.button一键.Click += new System.EventHandler(this.button一键_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(527, 282);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(260, 200);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(527, 216);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(259, 25);
            this.textBox1.TabIndex = 7;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(593, 180);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(125, 29);
            this.button1.TabIndex = 8;
            this.button1.Text = "wx订单查询";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonTest
            // 
            this.buttonTest.Location = new System.Drawing.Point(344, 422);
            this.buttonTest.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonTest.Name = "buttonTest";
            this.buttonTest.Size = new System.Drawing.Size(123, 36);
            this.buttonTest.TabIndex = 9;
            this.buttonTest.Text = "找零点清";
            this.buttonTest.UseVisualStyleBackColor = true;
            this.buttonTest.Click += new System.EventHandler(this.buttonTest_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(527, 140);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(259, 25);
            this.textBox2.TabIndex = 7;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(593, 104);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(125, 29);
            this.button3.TabIndex = 8;
            this.button3.Text = "zfb订单查询";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(803, 498);
            this.Controls.Add(this.buttonTest);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button一键);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button找零);
            this.Controls.Add(this.button实收);
            this.Controls.Add(this.button清除);
            this.Controls.Add(this.button微信);
            this.Controls.Add(this.button清除滚动);
            this.Controls.Add(this.button滚动);
            this.Controls.Add(this.button员工);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button患者);
            this.Controls.Add(this.button支付宝);
            this.Controls.Add(this.button应收);
            this.Controls.Add(this.txt找零);
            this.Controls.Add(this.txt实收);
            this.Controls.Add(this.txt应收);
            this.Controls.Add(this.txt滚动);
            this.Controls.Add(this.txtWav);
            this.Controls.Add(this.txtIP);
            this.Controls.Add(this.button稍等);
            this.Controls.Add(this.button底图);
            this.Controls.Add(this.button语音);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button语音;
        private System.Windows.Forms.TextBox txtIP;
        private System.Windows.Forms.TextBox txtWav;
        private System.Windows.Forms.Button button应收;
        private System.Windows.Forms.Button button实收;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button找零;
        private System.Windows.Forms.TextBox txt应收;
        private System.Windows.Forms.TextBox txt实收;
        private System.Windows.Forms.TextBox txt找零;
        private System.Windows.Forms.Button button支付宝;
        private System.Windows.Forms.Button button微信;
        private System.Windows.Forms.Button button清除;
        private System.Windows.Forms.Button button患者;
        private System.Windows.Forms.Button button滚动;
        private System.Windows.Forms.TextBox txt滚动;
        private System.Windows.Forms.Button button清除滚动;
        private System.Windows.Forms.Button button底图;
        private System.Windows.Forms.Button button稍等;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button员工;
        private System.Windows.Forms.Button button一键;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonTest;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button3;
    }
}

