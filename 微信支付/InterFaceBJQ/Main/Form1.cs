﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.IO;
using InterFaceBJQ;
using System.Runtime.InteropServices;

namespace Main
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private int nResult;

        private void button语音_Click(object sender, EventArgs e)
        {
            InterFaceBJQ.Main._s语音(txtIP.Text, txtWav.Text);
            //FCSoftTerminal_DLL.DZ_SendWav(txtIP.Text, txtWav.Text);
        }
        
        private void button滚动_Click(object sender, EventArgs e)
        {
            InterFaceBJQ.Main._s滚动文本(txtIP.Text, txt滚动.Text.Trim());
            //FCSoftTerminal_DLL.DZ_SendMoveInfo(txtIP.Text, 300, 300, 1024, 60, txt滚动.Text.Trim(), "red", "black", 42, 1, 10);
        }

        private void button清除滚动_Click(object sender, EventArgs e)
        {
            FCSoftTerminal_DLL.DZ_ClearMoveInfo(txtIP.Text);
        }

        private void button支付宝_Click(object sender, EventArgs e)
        {
            string cno = InterFaceBJQ.ZfbPay.ZfbPayHelper.GetPayNo();
            this.textBox2.Text = cno;
            if (!string.IsNullOrEmpty(cno))
                pictureBox1.Image = (Image)InterFaceBJQ.ZfbPay.ZfbPayHelper.GetZfbURL(cno, "0.01");
            //InterFaceBJQ.Main._s支付宝支付图片显示(txtIP.Text, "1.jpg");

            //nResult = FCSoftTerminal_DLL.DZ_SendFormatPos1(txtIP.Text, 682, 522, 220, 220);
            //if (nResult == 1)
            //{
            //    nResult = FCSoftTerminal_DLL.DZ_SendFormatContent1(txtIP.Text, "1.jpg");
            //}
        }

        private void button微信_Click(object sender, EventArgs e)
        {
            string cno = InterFaceBJQ.WxPay.wxPayHelper.GetPayNo();
            this.textBox1.Text = cno;
            if (!string.IsNullOrEmpty(cno))
                pictureBox1.Image = (Image)InterFaceBJQ.WxPay.wxPayHelper.GetWxURL(cno, 1);

            InterFaceBJQ.Main._s微信支付图片显示(txtIP.Text, "2.jpg");

            #region MyRegion
            //string path  = InterFaceBJQ.WxPay.wxPayHelper.GetWxURL("12345", 2);

            //pictureBox1.Image = Image.FromFile(path);

            //string url = @"http://paysdk.weixin.qq.com/example/MakeQRCode.aspx?data=weixin%3a%2f%2fwxpay%2fbizpayurl%3fpr%3dqfZ5DXT";

            //DoGetImage(url, path);

            //nResult = FCSoftTerminal_DLL.DZ_SendFormatPos2(txtIP.Text, 1092, 522, 220, 220);
            //if (nResult == 1)
            //{
            //    nResult = FCSoftTerminal_DLL.DZ_SendFormatContent2(txtIP.Text, "2.jpg");
            //} 
            #endregion
        }

        private void button清除_Click(object sender, EventArgs e)
        {
            //FCSoftTerminal_DLL.DZ_ClearFormatContent1(txtIP.Text);
            FCSoftTerminal_DLL.dsbdll(nResult, "F");
        }

        private void button患者_Click(object sender, EventArgs e)
        {
            InterFaceBJQ.Main._s患者信息("张伟 男 11岁");
            //Main._s患者信息("测试", "男", "11", "月");
            //FCSoftTerminal_DLL.dsbdll(nResult, "#您的姓名是：测试 男 11岁#"); //
            //FCSoftTerminal_DLL.dsbdll(nResult, "#病人性别：男#");
            //FCSoftTerminal_DLL.dsbdll(nResult, "#病人年龄：12#"); 
        }

        private void button底图_Click(object sender, EventArgs e)
        {
            InterFaceBJQ.Main._s替换底图(txtIP.Text);
            //nResult = FCSoftTerminal_DLL.DZ_SendFormatPos(txtIP.Text, 40, 30, 200, 240);
            //if (nResult == 1)
            //{
            //    nResult = FCSoftTerminal_DLL.DZ_SendFormatContent(txtIP.Text, "main.jpg");
            //}
        }

        private void button稍等_Click(object sender, EventArgs e)
        {
            InterFaceBJQ.Main._s初始化();
            //FCSoftTerminal_DLL.dsbdll(nResult, "W"); //稍等就是初始化
            //FCSoftTerminal_DLL.dsbdll(nResult, "F"); //初始化后为了把支付图片清除
        }

        private void button应收_Click(object sender, EventArgs e)
        {
            InterFaceBJQ.Main._s应收金额("ZQ04zje=100,bxzf=80,grxj=20"); //ZQ04zje=100,bxzf=80,grxj=20
            //Main._s应收金额(txt应收.Text);
            //FCSoftTerminal_DLL.dsbdll(1,"ZQ04zje=100,bxzf=80,grxj=20");
            //FCSoftTerminal_DLL.dsbdll(nResult, txt应收.Text+"J");
        }


        private void button实收_Click(object sender, EventArgs e)
        {
            InterFaceBJQ.Main._s实收金额(txt实收.Text);
            //FCSoftTerminal_DLL.dsbdll(nResult, "F");
            //FCSoftTerminal_DLL.dsbdll(nResult, txt实收.Text+"Y");
        }

        private void button找零_Click(object sender, EventArgs e)
        {
            InterFaceBJQ.Main._s找零金额(txt找零.Text);
            //Thread.Sleep(5000);
            //Main._s普通命令执行("D");
            //FCSoftTerminal_DLL.dsbdll(nResult,txt找零.Text+"Z");
            //Thread.Sleep(5000);
            //FCSoftTerminal_DLL.dsbdll(nResult, "D");
        }

        private void button员工_Click(object sender, EventArgs e)
        {
            InterFaceBJQ.Main._s员工信息("232");
            //FCSoftTerminal_DLL.dsbdll(nResult, "#今天当班操作员是:206#");
        }

        private void button一键_Click(object sender, EventArgs e)
        {
            //第一步：发送患者信息
            FCSoftTerminal_DLL.dsbdll(nResult, "#您的姓名是：张伟 男 31岁#"); //
            //第二步：发送付款信息
            FCSoftTerminal_DLL.dsbdll(nResult, txt应收.Text + "J");
            //第三步：发送支付图片
            Thread.Sleep(1000);
            nResult = FCSoftTerminal_DLL.DZ_SendFormatPos1(txtIP.Text, 665, 492, 262, 220); //支付宝图片
            if (nResult == 1)
            {
                nResult = FCSoftTerminal_DLL.DZ_SendFormatContent1(txtIP.Text, "1.jpg");
            }
            Thread.Sleep(500);
            nResult = FCSoftTerminal_DLL.DZ_SendFormatPos2(txtIP.Text, 1081, 492, 262, 220);//微信支付图片
            if (nResult == 1)
            {
                nResult = FCSoftTerminal_DLL.DZ_SendFormatContent2(txtIP.Text, "1.jpg");
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //this.pictureBox1.Image = Image.FromStream(System.Net.WebRequest.Create(
            //    @"http://paysdk.weixin.qq.com/example/MakeQRCode.aspx?data=weixin%3a%2f%2fwxpay%2fbizpayurl%3fpr%3dqfZ5DXT").GetResponse().GetResponseStream());
                        
        }

        public void DoGetImage(string url, string path)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);

            req.ServicePoint.Expect100Continue = false;
            req.Method = "GET";
            req.KeepAlive = true;

            req.ContentType = "image/jpg";
            HttpWebResponse rsp = (HttpWebResponse)req.GetResponse();

            System.IO.Stream stream = null;

            try
            {
                // 以字符流的方式读取HTTP响应
                stream = rsp.GetResponseStream();
                Image.FromStream(stream).Save(path);
            }
            finally
            {
                // 释放资源
                if (stream != null) stream.Close();
                if (rsp != null) rsp.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.textBox1.Text)) return;
            InterFaceBJQ.WxPay.wxPayState state = InterFaceBJQ.WxPay.wxPayHelper.GetWxOrderState(this.textBox1.Text);
            
            if (state == InterFaceBJQ.WxPay.wxPayState.SUCCESS)
                MessageBox.Show("订单交易成功");
            else if (state == InterFaceBJQ.WxPay.wxPayState.NOTPAY)
                MessageBox.Show("订单未付款");
            else if (state == InterFaceBJQ.WxPay.wxPayState.REFUND)
                MessageBox.Show("订单已退款");
            else
                MessageBox.Show("订单查询失败");
        }

        private void buttonTest_Click(object sender, EventArgs e)
        {
            InterFaceBJQ.Main._s普通命令执行("D");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            BeepUp.Beep(500, 700);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.textBox2.Text)) return;
            InterFaceBJQ.ZfbPay.zfbPayState state = InterFaceBJQ.ZfbPay.ZfbPayHelper.GetZfbOrderState(this.textBox2.Text);

            if (state == InterFaceBJQ.ZfbPay.zfbPayState.SUCCESS)
                MessageBox.Show("订单交易成功");
            else if (state == InterFaceBJQ.ZfbPay.zfbPayState.FAILED)
                MessageBox.Show("订单未付款");
            else if (state == InterFaceBJQ.ZfbPay.zfbPayState.UNKNOWN)
                MessageBox.Show("网络错误");
            else
                MessageBox.Show("订单查询失败");
        }

    }

    // 声明  
    public class BeepUp  //新建一个类
    {
        /// <param name="iFrequency">声音频率（从37Hz到32767Hz）。在windows95中忽略</param>  
        /// <param name="iDuration">声音的持续时间，以毫秒为单位。</param>  
        [DllImport("Kernel32.dll")] //引入命名空间 using System.Runtime.InteropServices;  
        public static extern bool Beep(int frequency, int duration);
    }  
}
