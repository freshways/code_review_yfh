﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DotNet.Model
{
	///名称：WWF_DEPT
	///作者：yufh 
	///创建时间：2020-09-27 15:30 
	///功能描述：WWF_DEPT	实体类
	[Serializable]
	public class WWF_DEPT
	{
		#region Model

		/// <summary>
		/// fdept_id
		/// </summary>
		[DisplayName("fdept_id")]
		[Key]
		public string fdept_id { get; set; }

		/// <summary>
		/// fp_id
		/// </summary>
		[DisplayName("fp_id")]
		public string fp_id { get; set; }

		/// <summary>
		/// fname
		/// </summary>
		[DisplayName("fname")]
		public string fname { get; set; }

		/// <summary>
		/// fname_e
		/// </summary>
		[DisplayName("fname_e")]
		public string fname_e { get; set; }

		/// <summary>
		/// forder_by
		/// </summary>
		[DisplayName("forder_by")]
		public string forder_by { get; set; }

		/// <summary>
		/// fuse_flag
		/// </summary>
		[DisplayName("fuse_flag")]
		public int? fuse_flag { get; set; }

		/// <summary>
		/// fremark
		/// </summary>
		[DisplayName("fremark")]
		public string fremark { get; set; }
		#endregion Model
	}
}
