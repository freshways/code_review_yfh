﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DotNet.Model
{
	///名称：SAM_JY
	///作者：yufh 
	///创建时间：2020-09-25 15:07 
	///功能描述：SAM_JY	实体类
	[Serializable]
	public class SAM_JY
	{
		#region Model

		/// <summary>
		/// fjy_id
		/// </summary>
		[DisplayName("fjy_id")]
		[Key]
		public string fjy_id { get; set; }

		/// <summary>
		/// fjy_date
		/// </summary>
		[DisplayName("fjy_date")]
		public string fjy_date { get; set; }

		/// <summary>
		/// fjy_instr
		/// </summary>
		[DisplayName("fjy_instr")]
		public string fjy_instr { get; set; }

		/// <summary>
		/// fjy_yb_code
		/// </summary>
		[DisplayName("fjy_yb_code")]
		public string fjy_yb_code { get; set; }

		/// <summary>
		/// fjy_yb_tm
		/// </summary>
		[DisplayName("fjy_yb_tm")]
		public string fjy_yb_tm { get; set; }

		/// <summary>
		/// fjy_yb_type
		/// </summary>
		[DisplayName("fjy_yb_type")]
		public string fjy_yb_type { get; set; }

		/// <summary>
		/// fjy_sf_type
		/// </summary>
		[DisplayName("fjy_sf_type")]
		public string fjy_sf_type { get; set; }

		/// <summary>
		/// fjy_f
		/// </summary>
		[DisplayName("fjy_f")]
		public decimal? fjy_f { get; set; }

		/// <summary>
		/// fjy_f_ok
		/// </summary>
		[DisplayName("fjy_f_ok")]
		public string fjy_f_ok { get; set; }

		/// <summary>
		/// fjy_zt
		/// </summary>
		[DisplayName("fjy_zt")]
		public string fjy_zt { get; set; }

		/// <summary>
		/// fjy_md
		/// </summary>
		[DisplayName("fjy_md")]
		public string fjy_md { get; set; }

		/// <summary>
		/// fjy_lczd
		/// </summary>
		[DisplayName("fjy_lczd")]
		public string fjy_lczd { get; set; }

		/// <summary>
		/// fhz_id
		/// </summary>
		[DisplayName("fhz_id")]
		public string fhz_id { get; set; }

		/// <summary>
		/// fhz_type_id
		/// </summary>
		[DisplayName("fhz_type_id")]
		public string fhz_type_id { get; set; }

		/// <summary>
		/// fhz_zyh
		/// </summary>
		[DisplayName("fhz_zyh")]
		public string fhz_zyh { get; set; }

		/// <summary>
		/// fhz_name
		/// </summary>
		[DisplayName("fhz_name")]
		public string fhz_name { get; set; }

		/// <summary>
		/// fhz_sex
		/// </summary>
		[DisplayName("fhz_sex")]
		public string fhz_sex { get; set; }

		/// <summary>
		/// fhz_age
		/// </summary>
		[DisplayName("fhz_age")]
		public decimal? fhz_age { get; set; }

		/// <summary>
		/// fhz_age_unit
		/// </summary>
		[DisplayName("fhz_age_unit")]
		public string fhz_age_unit { get; set; }

		/// <summary>
		/// fhz_age_date
		/// </summary>
		[DisplayName("fhz_age_date")]
		public string fhz_age_date { get; set; }

		/// <summary>
		/// fhz_bq
		/// </summary>
		[DisplayName("fhz_bq")]
		public string fhz_bq { get; set; }

		/// <summary>
		/// fhz_dept
		/// </summary>
		[DisplayName("fhz_dept")]
		public string fhz_dept { get; set; }

		/// <summary>
		/// fhz_room
		/// </summary>
		[DisplayName("fhz_room")]
		public string fhz_room { get; set; }

		/// <summary>
		/// fhz_bed
		/// </summary>
		[DisplayName("fhz_bed")]
		public string fhz_bed { get; set; }

		/// <summary>
		/// fhz_volk
		/// </summary>
		[DisplayName("fhz_volk")]
		public string fhz_volk { get; set; }

		/// <summary>
		/// fhz_hy
		/// </summary>
		[DisplayName("fhz_hy")]
		public string fhz_hy { get; set; }

		/// <summary>
		/// fhz_job
		/// </summary>
		[DisplayName("fhz_job")]
		public string fhz_job { get; set; }

		/// <summary>
		/// fhz_gms
		/// </summary>
		[DisplayName("fhz_gms")]
		public string fhz_gms { get; set; }

		/// <summary>
		/// fhz_add
		/// </summary>
		[DisplayName("fhz_add")]
		public string fhz_add { get; set; }

		/// <summary>
		/// fhz_tel
		/// </summary>
		[DisplayName("fhz_tel")]
		public string fhz_tel { get; set; }

		/// <summary>
		/// fapply_id
		/// </summary>
		[DisplayName("fapply_id")]
		public string fapply_id { get; set; }

		/// <summary>
		/// fapply_user_id
		/// </summary>
		[DisplayName("fapply_user_id")]
		public string fapply_user_id { get; set; }

		/// <summary>
		/// fapply_dept_id
		/// </summary>
		[DisplayName("fapply_dept_id")]
		public string fapply_dept_id { get; set; }

		/// <summary>
		/// fapply_time
		/// </summary>
		[DisplayName("fapply_time")]
		public string fapply_time { get; set; }

		/// <summary>
		/// fsampling_user_id
		/// </summary>
		[DisplayName("fsampling_user_id")]
		public string fsampling_user_id { get; set; }

		/// <summary>
		/// fsampling_time
		/// </summary>
		[DisplayName("fsampling_time")]
		public string fsampling_time { get; set; }

		/// <summary>
		/// fjy_user_id
		/// </summary>
		[DisplayName("fjy_user_id")]
		public string fjy_user_id { get; set; }

		/// <summary>
		/// fchenk_user_id
		/// </summary>
		[DisplayName("fchenk_user_id")]
		public string fchenk_user_id { get; set; }

		/// <summary>
		/// fcheck_time
		/// </summary>
		[DisplayName("fcheck_time")]
		public string fcheck_time { get; set; }

		/// <summary>
		/// freport_time
		/// </summary>
		[DisplayName("freport_time")]
		public string freport_time { get; set; }

		/// <summary>
		/// fprint_time
		/// </summary>
		[DisplayName("fprint_time")]
		public string fprint_time { get; set; }

		/// <summary>
		/// fprint_zt
		/// </summary>
		[DisplayName("fprint_zt")]
		public string fprint_zt { get; set; }

		/// <summary>
		/// fprint_count
		/// </summary>
		[DisplayName("fprint_count")]
		public int? fprint_count { get; set; }

		/// <summary>
		/// fsys_user
		/// </summary>
		[DisplayName("fsys_user")]
		public string fsys_user { get; set; }

		/// <summary>
		/// fsys_time
		/// </summary>
		[DisplayName("fsys_time")]
		public string fsys_time { get; set; }

		/// <summary>
		/// fsys_dept
		/// </summary>
		[DisplayName("fsys_dept")]
		public string fsys_dept { get; set; }

		/// <summary>
		/// fremark
		/// </summary>
		[DisplayName("fremark")]
		public string fremark { get; set; }

		/// <summary>
		/// finstr_result_id
		/// </summary>
		[DisplayName("finstr_result_id")]
		public string finstr_result_id { get; set; }

		/// <summary>
		/// f1
		/// </summary>
		[DisplayName("f1")]
		public string f1 { get; set; }

		/// <summary>
		/// f2
		/// </summary>
		[DisplayName("f2")]
		public string f2 { get; set; }

		/// <summary>
		/// f3
		/// </summary>
		[DisplayName("f3")]
		public string f3 { get; set; }

		/// <summary>
		/// f4
		/// </summary>
		[DisplayName("f4")]
		public string f4 { get; set; }

		/// <summary>
		/// f5
		/// </summary>
		[DisplayName("f5")]
		public string f5 { get; set; }

		/// <summary>
		/// f6
		/// </summary>
		[DisplayName("f6")]
		public string f6 { get; set; }

		/// <summary>
		/// f7
		/// </summary>
		[DisplayName("f7")]
		public string f7 { get; set; }

		/// <summary>
		/// f8
		/// </summary>
		[DisplayName("f8")]
		public string f8 { get; set; }

		/// <summary>
		/// f9
		/// </summary>
		[DisplayName("f9")]
		public string f9 { get; set; }

		/// <summary>
		/// f10
		/// </summary>
		[DisplayName("f10")]
		public string f10 { get; set; }

		/// <summary>
		/// sfzh
		/// </summary>
		[DisplayName("sfzh")]
		public string sfzh { get; set; }
		#endregion Model
	}
}
