﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DotNet.Model
{
    public class SAM_JYSearcher
    {
        /*
         {
 "idCard":"6f0091df97702a28c1ffdc4c420ccfdd2f99d48cb8d532229269cb25fb777228",
 "startNum":"1",
 "endNum":"1000",
 "random":"b48861793cd14869a82c5b71cd65b098",
 "sign":"89a7b9d740b7e3755459aa430cdede95",
 "appKey":"274b4345c0929ee10bb2be7d0b1c1456"
}
         */
        [Display(Name = "身份证号")]
        public string IDCard { get; set; }

        [Display(Name = "开始序号")]
        public string startNum { get; set; }

        [Display(Name = "结束序号")]
        public string endNum { get; set; }

        [Display(Name = "开始日期")]
        public string startDate { get; set; }

        [Display(Name = "结束日期")]
        public string endDate { get; set; }

        [Display(Name = "随机值")]
        public string random { get; set; }
        [Display(Name = "机构标识")]
        public string appKey { get; set; }
        [Display(Name = "验证")]
        public string sign { get; set; }
    }


    public class SAM_JYRes
    {
        /// <summary>
        /// 
        /// </summary>
        public string reportNum { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string idCard { get; set; }
        /// <summary>
        /// 刘敏
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string doctorId { get; set; }
        /// <summary>
        /// 黄华
        /// </summary>
        public string doctorName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string deptId { get; set; }
        /// <summary>
        /// 检验科
        /// </summary>
        public string deptName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string examCode { get; set; }
        /// <summary>
        /// 血常规（五分类）(静脉血)
        /// </summary>
        public string examName { get; set; }
        /// <summary>
        /// 检验项目集
        /// </summary>
        public List<DetailsItem> details { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string examDate { get; set; }
    }

    /// <summary>
    /// 检验项目集
    /// </summary>
    public class DetailsItem
    {
        /// <summary>
        /// 
        /// </summary>
        public string itemId { get; set; }
        /// <summary>
        /// 白细胞计数
        /// </summary>
        public string itemName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string result { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string unit { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string referValueUpper { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string referValueLower { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string attachedResult { get; set; }
    }

}
