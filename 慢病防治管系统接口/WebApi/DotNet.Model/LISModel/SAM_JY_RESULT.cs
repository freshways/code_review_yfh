﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DotNet.Model
{
	///名称：SAM_JY_RESULT
	///作者：yufh 
	///创建时间：2020-09-25 15:38 
	///功能描述：SAM_JY_RESULT	实体类
	[Serializable]
	public class SAM_JY_RESULT
	{
		#region Model

		/// <summary>
		/// fresult_id
		/// </summary>
		[DisplayName("fresult_id")]
		[Key]
		public string fresult_id { get; set; }

		/// <summary>
		/// fjy_id
		/// </summary>
		[DisplayName("fjy_id")]
		public string fjy_id { get; set; }

		/// <summary>
		/// fapply_id
		/// </summary>
		[DisplayName("fapply_id")]
		public string fapply_id { get; set; }

		/// <summary>
		/// fitem_id
		/// </summary>
		[DisplayName("fitem_id")]
		public string fitem_id { get; set; }

		/// <summary>
		/// fitem_code
		/// </summary>
		[DisplayName("fitem_code")]
		public string fitem_code { get; set; }

		/// <summary>
		/// fitem_name
		/// </summary>
		[DisplayName("fitem_name")]
		public string fitem_name { get; set; }

		/// <summary>
		/// fitem_unit
		/// </summary>
		[DisplayName("fitem_unit")]
		public string fitem_unit { get; set; }

		/// <summary>
		/// fitem_ref
		/// </summary>
		[DisplayName("fitem_ref")]
		public string fitem_ref { get; set; }

		/// <summary>
		/// fitem_badge
		/// </summary>
		[DisplayName("fitem_badge")]
		public string fitem_badge { get; set; }

		/// <summary>
		/// forder_by
		/// </summary>
		[DisplayName("forder_by")]
		public string forder_by { get; set; }

		/// <summary>
		/// fvalue
		/// </summary>
		[DisplayName("fvalue")]
		public string fvalue { get; set; }

		/// <summary>
		/// fod
		/// </summary>
		[DisplayName("fod")]
		public string fod { get; set; }

		/// <summary>
		/// fcutoff
		/// </summary>
		[DisplayName("fcutoff")]
		public string fcutoff { get; set; }

		/// <summary>
		/// fremark
		/// </summary>
		[DisplayName("fremark")]
		public string fremark { get; set; }
		#endregion Model
	}
}
