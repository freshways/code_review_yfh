﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DotNet.Model
{
	///名称：SAM_INSTR
	///作者：yufh 
	///创建时间：2020-10-30 08:35 
	///功能描述：SAM_INSTR	实体类
	[Serializable]
	public class SAM_INSTR
	{
		#region Model

		/// <summary>
		/// finstr_id
		/// </summary>
		[DisplayName("finstr_id")]
		[Key]
		public string finstr_id { get; set; }

		/// <summary>
		/// ftype_id
		/// </summary>
		[DisplayName("ftype_id")]
		public string ftype_id { get; set; }

		/// <summary>
		/// fjygroup_id
		/// </summary>
		[DisplayName("fjygroup_id")]
		public string fjygroup_id { get; set; }

		/// <summary>
		/// fjytype_id
		/// </summary>
		[DisplayName("fjytype_id")]
		public string fjytype_id { get; set; }

		/// <summary>
		/// fuse_if
		/// </summary>
		[DisplayName("fuse_if")]
		public int? fuse_if { get; set; }

		/// <summary>
		/// fcode
		/// </summary>
		[DisplayName("fcode")]
		public string fcode { get; set; }

		/// <summary>
		/// fname
		/// </summary>
		[DisplayName("fname")]
		public string fname { get; set; }

		/// <summary>
		/// fname_e
		/// </summary>
		[DisplayName("fname_e")]
		public string fname_e { get; set; }

		/// <summary>
		/// forder_by
		/// </summary>
		[DisplayName("forder_by")]
		public string forder_by { get; set; }

		/// <summary>
		/// fremark
		/// </summary>
		[DisplayName("fremark")]
		public string fremark { get; set; }

		/// <summary>
		/// fbody_no
		/// </summary>
		[DisplayName("fbody_no")]
		public string fbody_no { get; set; }

		/// <summary>
		/// fnew_old
		/// </summary>
		[DisplayName("fnew_old")]
		public string fnew_old { get; set; }

		/// <summary>
		/// fprice
		/// </summary>
		[DisplayName("fprice")]
		public decimal? fprice { get; set; }

		/// <summary>
		/// fimg
		/// </summary>
		[DisplayName("fimg")]
		public string fimg { get; set; }

		/// <summary>
		/// funit
		/// </summary>
		[DisplayName("funit")]
		public string funit { get; set; }

		/// <summary>
		/// fy_value
		/// </summary>
		[DisplayName("fy_value")]
		public decimal? fy_value { get; set; }

		/// <summary>
		/// fj_value
		/// </summary>
		[DisplayName("fj_value")]
		public decimal? fj_value { get; set; }

		/// <summary>
		/// fzjl
		/// </summary>
		[DisplayName("fzjl")]
		public decimal? fzjl { get; set; }

		/// <summary>
		/// fuse_year
		/// </summary>
		[DisplayName("fuse_year")]
		public int? fuse_year { get; set; }

		/// <summary>
		/// fout_date
		/// </summary>
		[DisplayName("fout_date")]
		public string fout_date { get; set; }

		/// <summary>
		/// fdh_date
		/// </summary>
		[DisplayName("fdh_date")]
		public string fdh_date { get; set; }

		/// <summary>
		/// fty_date
		/// </summary>
		[DisplayName("fty_date")]
		public string fty_date { get; set; }

		/// <summary>
		/// fgys_id
		/// </summary>
		[DisplayName("fgys_id")]
		public string fgys_id { get; set; }

		/// <summary>
		/// fcj_id
		/// </summary>
		[DisplayName("fcj_id")]
		public string fcj_id { get; set; }

		/// <summary>
		/// fwz
		/// </summary>
		[DisplayName("fwz")]
		public string fwz { get; set; }

		/// <summary>
		/// fgb
		/// </summary>
		[DisplayName("fgb")]
		public string fgb { get; set; }

		/// <summary>
		/// fsz_date
		/// </summary>
		[DisplayName("fsz_date")]
		public string fsz_date { get; set; }

		/// <summary>
		/// fdah
		/// </summary>
		[DisplayName("fdah")]
		public string fdah { get; set; }

		/// <summary>
		/// fjfr
		/// </summary>
		[DisplayName("fjfr")]
		public string fjfr { get; set; }

		/// <summary>
		/// fuser
		/// </summary>
		[DisplayName("fuser")]
		public string fuser { get; set; }

		/// <summary>
		/// fdate
		/// </summary>
		[DisplayName("fdate")]
		public string fdate { get; set; }

		/// <summary>
		/// flx
		/// </summary>
		[DisplayName("flx")]
		public int? flx { get; set; }

		/// <summary>
		/// fzjzz_date
		/// </summary>
		[DisplayName("fzjzz_date")]
		public string fzjzz_date { get; set; }

		/// <summary>
		/// fspec
		/// </summary>
		[DisplayName("fspec")]
		public string fspec { get; set; }

		/// <summary>
		/// fhelp_code
		/// </summary>
		[DisplayName("fhelp_code")]
		public string fhelp_code { get; set; }

		/// <summary>
		/// fxlzb
		/// </summary>
		[DisplayName("fxlzb")]
		public string fxlzb { get; set; }
		#endregion Model
	}
}
