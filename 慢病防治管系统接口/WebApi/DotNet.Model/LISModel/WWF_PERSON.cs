﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DotNet.Model
{
	///名称：WWF_PERSON
	///作者：yufh 
	///创建时间：2020-09-27 15:27 
	///功能描述：WWF_PERSON	实体类
	[Serializable]
	public class WWF_PERSON
	{
		#region Model

		/// <summary>
		/// fperson_id
		/// </summary>
		[DisplayName("fperson_id")]
		[Key]
		public string fperson_id { get; set; }

		/// <summary>
		/// fdept_id
		/// </summary>
		[DisplayName("fdept_id")]
		public string fdept_id { get; set; }

		/// <summary>
		/// fname
		/// </summary>
		[DisplayName("fname")]
		public string fname { get; set; }

		/// <summary>
		/// fname_e
		/// </summary>
		[DisplayName("fname_e")]
		public string fname_e { get; set; }

		/// <summary>
		/// fpass
		/// </summary>
		[DisplayName("fpass")]
		public string fpass { get; set; }

		/// <summary>
		/// forder_by
		/// </summary>
		[DisplayName("forder_by")]
		public string forder_by { get; set; }

		/// <summary>
		/// fuse_flag
		/// </summary>
		[DisplayName("fuse_flag")]
		public int? fuse_flag { get; set; }

		/// <summary>
		/// fremark
		/// </summary>
		[DisplayName("fremark")]
		public string fremark { get; set; }

		/// <summary>
		/// ftype
		/// </summary>
		[DisplayName("ftype")]
		public string ftype { get; set; }

		/// <summary>
		/// fhelp_code
		/// </summary>
		[DisplayName("fhelp_code")]
		public string fhelp_code { get; set; }

		/// <summary>
		/// fshow_name
		/// </summary>
		[DisplayName("fshow_name")]
		public string fshow_name { get; set; }
		#endregion Model
	}
}
