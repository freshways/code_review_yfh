﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DotNet.Model
{
	///名称：SAM_APPLY
	///作者：yufh 
	///创建时间：2020-09-25 15:48 
	///功能描述：SAM_APPLY	实体类
	[Serializable]
	public class SAM_APPLY
	{
		#region Model

		/// <summary>
		/// fapply_id
		/// </summary>
		[DisplayName("fapply_id")]
		[Key]
		public string fapply_id { get; set; }

		/// <summary>
		/// fjytype_id
		/// </summary>
		[DisplayName("fjytype_id")]
		public string fjytype_id { get; set; }

		/// <summary>
		/// fsample_type_id
		/// </summary>
		[DisplayName("fsample_type_id")]
		public string fsample_type_id { get; set; }

		/// <summary>
		/// fjz_flag
		/// </summary>
		[DisplayName("fjz_flag")]
		public int? fjz_flag { get; set; }

		/// <summary>
		/// fcharge_flag
		/// </summary>
		[DisplayName("fcharge_flag")]
		public int? fcharge_flag { get; set; }

		/// <summary>
		/// fcharge_id
		/// </summary>
		[DisplayName("fcharge_id")]
		public string fcharge_id { get; set; }

		/// <summary>
		/// fstate
		/// </summary>
		[DisplayName("fstate")]
		public string fstate { get; set; }

		/// <summary>
		/// fjyf
		/// </summary>
		[DisplayName("fjyf")]
		public decimal? fjyf { get; set; }

		/// <summary>
		/// fapply_user_id
		/// </summary>
		[DisplayName("fapply_user_id")]
		public string fapply_user_id { get; set; }

		/// <summary>
		/// fapply_dept_id
		/// </summary>
		[DisplayName("fapply_dept_id")]
		public string fapply_dept_id { get; set; }

		/// <summary>
		/// fapply_time
		/// </summary>
		[DisplayName("fapply_time")]
		public string fapply_time { get; set; }

		/// <summary>
		/// ftype_id
		/// </summary>
		[DisplayName("ftype_id")]
		public string ftype_id { get; set; }

		/// <summary>
		/// fhz_id
		/// </summary>
		[DisplayName("fhz_id")]
		public string fhz_id { get; set; }

		/// <summary>
		/// fhz_zyh
		/// </summary>
		[DisplayName("fhz_zyh")]
		public string fhz_zyh { get; set; }

		/// <summary>
		/// fsex
		/// </summary>
		[DisplayName("fsex")]
		public string fsex { get; set; }

		/// <summary>
		/// fname
		/// </summary>
		[DisplayName("fname")]
		public string fname { get; set; }

		/// <summary>
		/// fvolk
		/// </summary>
		[DisplayName("fvolk")]
		public string fvolk { get; set; }

		/// <summary>
		/// fhymen
		/// </summary>
		[DisplayName("fhymen")]
		public string fhymen { get; set; }

		/// <summary>
		/// fage
		/// </summary>
		[DisplayName("fage")]
		public int? fage { get; set; }

		/// <summary>
		/// fjob
		/// </summary>
		[DisplayName("fjob")]
		public string fjob { get; set; }

		/// <summary>
		/// fgms
		/// </summary>
		[DisplayName("fgms")]
		public string fgms { get; set; }

		/// <summary>
		/// fjob_org
		/// </summary>
		[DisplayName("fjob_org")]
		public string fjob_org { get; set; }

		/// <summary>
		/// fadd
		/// </summary>
		[DisplayName("fadd")]
		public string fadd { get; set; }

		/// <summary>
		/// ftel
		/// </summary>
		[DisplayName("ftel")]
		public string ftel { get; set; }

		/// <summary>
		/// fage_unit
		/// </summary>
		[DisplayName("fage_unit")]
		public string fage_unit { get; set; }

		/// <summary>
		/// fage_year
		/// </summary>
		[DisplayName("fage_year")]
		public int? fage_year { get; set; }

		/// <summary>
		/// fage_month
		/// </summary>
		[DisplayName("fage_month")]
		public int? fage_month { get; set; }

		/// <summary>
		/// fage_day
		/// </summary>
		[DisplayName("fage_day")]
		public int? fage_day { get; set; }

		/// <summary>
		/// fward_num
		/// </summary>
		[DisplayName("fward_num")]
		public string fward_num { get; set; }

		/// <summary>
		/// froom_num
		/// </summary>
		[DisplayName("froom_num")]
		public string froom_num { get; set; }

		/// <summary>
		/// fbed_num
		/// </summary>
		[DisplayName("fbed_num")]
		public string fbed_num { get; set; }

		/// <summary>
		/// fblood_abo
		/// </summary>
		[DisplayName("fblood_abo")]
		public string fblood_abo { get; set; }

		/// <summary>
		/// fblood_rh
		/// </summary>
		[DisplayName("fblood_rh")]
		public string fblood_rh { get; set; }

		/// <summary>
		/// fjymd
		/// </summary>
		[DisplayName("fjymd")]
		public string fjymd { get; set; }

		/// <summary>
		/// fdiagnose
		/// </summary>
		[DisplayName("fdiagnose")]
		public string fdiagnose { get; set; }

		/// <summary>
		/// fheat
		/// </summary>
		[DisplayName("fheat")]
		public decimal? fheat { get; set; }

		/// <summary>
		/// fxyld
		/// </summary>
		[DisplayName("fxyld")]
		public decimal? fxyld { get; set; }

		/// <summary>
		/// fcyy
		/// </summary>
		[DisplayName("fcyy")]
		public string fcyy { get; set; }

		/// <summary>
		/// fxhdb
		/// </summary>
		[DisplayName("fxhdb")]
		public int? fxhdb { get; set; }

		/// <summary>
		/// fcreate_user_id
		/// </summary>
		[DisplayName("fcreate_user_id")]
		public string fcreate_user_id { get; set; }

		/// <summary>
		/// fcreate_time
		/// </summary>
		[DisplayName("fcreate_time")]
		public string fcreate_time { get; set; }

		/// <summary>
		/// fupdate_user_id
		/// </summary>
		[DisplayName("fupdate_user_id")]
		public string fupdate_user_id { get; set; }

		/// <summary>
		/// fupdate_time
		/// </summary>
		[DisplayName("fupdate_time")]
		public string fupdate_time { get; set; }

		/// <summary>
		/// fitem_group
		/// </summary>
		[DisplayName("fitem_group")]
		public string fitem_group { get; set; }

		/// <summary>
		/// fremark
		/// </summary>
		[DisplayName("fremark")]
		public string fremark { get; set; }

		/// <summary>
		/// his_recordid
		/// </summary>
		[DisplayName("his_recordid")]
		public string his_recordid { get; set; }

		/// <summary>
		/// sfzh
		/// </summary>
		[DisplayName("sfzh")]
		public string sfzh { get; set; }
		#endregion Model
	}
}
