using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace DotNet.Model
{
    /// <summary>
    /// 用户登录信息
    /// </summary>
    public class AccountInfo
    {
        /// <summary>
        /// 账户ID
        /// </summary>
        /// <returns></returns>
        [Key]
        public string AccountID { get; set; }
 
        /// <summary>
        /// 登陆账号
        /// </summary>
        /// <returns></returns>
        public string LoginID { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        /// <returns></returns>
        public string PassWord { get; set; }
 
        /// <summary>
        /// 用户姓名
        /// </summary>
        /// <returns></returns>
        public string UserName { get; set; }
 
        /// <summary>
        /// 创建时间
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
 
        /// <summary>
        /// 修改时间
        /// </summary>
        /// <returns></returns>
        public DateTime? ModifyTime { get; set; }
    }
}