﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DotNet.Model
{
    /// <summary>
    /// 菜单
    /// </summary>
    public class Menus
    {
        /// <summary>
        /// 首页
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string url { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public string id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string pId { get; set; }
    }
}
