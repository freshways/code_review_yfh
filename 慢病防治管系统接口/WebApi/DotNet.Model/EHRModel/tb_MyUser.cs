﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DotNet.Model
{
	///名称：tb_MyUser
	///作者：yufh 
	///创建时间：2020-09-24 14:41 
	///功能描述：tb_MyUser	实体类
	[Serializable]
	public class tb_MyUser
	{
		#region Model

		/// <summary>
		/// isid
		/// </summary>
		[DisplayName("isid")]
		[Key]
		public int isid { get; set; }

		/// <summary>
		/// Account
		/// </summary>
		[DisplayName("Account")]
		public string Account { get; set; }

		/// <summary>
		/// NovellAccount
		/// </summary>
		[DisplayName("NovellAccount")]
		public string NovellAccount { get; set; }

		/// <summary>
		/// DomainName
		/// </summary>
		[DisplayName("DomainName")]
		public string DomainName { get; set; }

		/// <summary>
		/// 用户编码
		/// </summary>
		[DisplayName("用户编码")]
		public string 用户编码 { get; set; }

		/// <summary>
		/// UserName
		/// </summary>
		[DisplayName("UserName")]
		public string UserName { get; set; }

		/// <summary>
		/// Address
		/// </summary>
		[DisplayName("Address")]
		public string Address { get; set; }

		/// <summary>
		/// Tel
		/// </summary>
		[DisplayName("Tel")]
		public string Tel { get; set; }

		/// <summary>
		/// Email
		/// </summary>
		[DisplayName("Email")]
		public string Email { get; set; }

		/// <summary>
		/// Password
		/// </summary>
		[DisplayName("Password")]
		public string Password { get; set; }

		/// <summary>
		/// LastLoginTime
		/// </summary>
		[DisplayName("LastLoginTime")]
		public DateTime? LastLoginTime { get; set; }

		/// <summary>
		/// LastLogoutTime
		/// </summary>
		[DisplayName("LastLogoutTime")]
		public DateTime? LastLogoutTime { get; set; }

		/// <summary>
		/// IsLocked
		/// </summary>
		[DisplayName("IsLocked")]
		public int? IsLocked { get; set; }

		/// <summary>
		/// CreateTime
		/// </summary>
		[DisplayName("CreateTime")]
		public DateTime? CreateTime { get; set; }

		/// <summary>
		/// FlagAdmin
		/// </summary>
		[DisplayName("FlagAdmin")]
		public string FlagAdmin { get; set; }

		/// <summary>
		/// FlagOnline
		/// </summary>
		[DisplayName("FlagOnline")]
		public string FlagOnline { get; set; }

		/// <summary>
		/// LoginCounter
		/// </summary>
		[DisplayName("LoginCounter")]
		public int? LoginCounter { get; set; }

		/// <summary>
		/// DataSets
		/// </summary>
		[DisplayName("DataSets")]
		public string DataSets { get; set; }

		/// <summary>
		/// 所属机构
		/// </summary>
		[DisplayName("所属机构")]
		public string 所属机构 { get; set; }

		/// <summary>
		/// 身份证
		/// </summary>
		[DisplayName("身份证")]
		public string 身份证 { get; set; }

		/// <summary>
		/// 手签
		/// </summary>
		[DisplayName("手签")]
		public string 手签 { get; set; }

		/// <summary>
		/// 手签时间
		/// </summary>
		[DisplayName("手签时间")]
		public DateTime? 手签时间 { get; set; }

		/// <summary>
		/// 手机app权限标记
		/// </summary>
		[DisplayName("手机app权限标记")]
		public string 手机app权限标记 { get; set; }
		#endregion Model
	}
}
