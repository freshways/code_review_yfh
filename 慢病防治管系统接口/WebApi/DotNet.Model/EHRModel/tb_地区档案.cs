﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DotNet.Model
{
	///名称：tb_地区档案
	///作者：yufh 
	///创建时间：2020-09-22 10:07 
	///功能描述：tb_地区档案	实体类
	[Serializable]
	public class tb_地区档案
	{
		#region Model

		/// <summary>
		/// ID
		/// </summary>
		[DisplayName("ID")]
		public int ID { get; set; }

		/// <summary>
		/// 地区编码
		/// </summary>
		[DisplayName("地区编码")]
		public string 地区编码 { get; set; }

		/// <summary>
		/// 地区名称
		/// </summary>
		[DisplayName("地区名称")]
		public string 地区名称 { get; set; }

		/// <summary>
		/// 上级编码
		/// </summary>
		[DisplayName("上级编码")]
		public string 上级编码 { get; set; }

		/// <summary>
		/// 备注
		/// </summary>
		[DisplayName("备注")]
		public string 备注 { get; set; }

		/// <summary>
		/// 创建人
		/// </summary>
		[DisplayName("创建人")]
		public string 创建人 { get; set; }

		/// <summary>
		/// 创建时间
		/// </summary>
		[DisplayName("创建时间")]
		public DateTime? 创建时间 { get; set; }

		/// <summary>
		/// 详细地区名称
		/// </summary>
		[DisplayName("详细地区名称")]
		public string 详细地区名称 { get; set; }

		/// <summary>
		/// 地区人口
		/// </summary>
		[DisplayName("地区人口")]
		public string 地区人口 { get; set; }

		/// <summary>
		/// 是否城市
		/// </summary>
		[DisplayName("是否城市")]
		public string 是否城市 { get; set; }

		/// <summary>
		/// 所属机构
		/// </summary>
		[DisplayName("所属机构")]
		public string 所属机构 { get; set; }

		/// <summary>
		/// 上级机构
		/// </summary>
		[DisplayName("上级机构")]
		public string 上级机构 { get; set; }

		/// <summary>
		/// 邮编
		/// </summary>
		[DisplayName("邮编")]
		public string 邮编 { get; set; }
		#endregion Model
	}
}
