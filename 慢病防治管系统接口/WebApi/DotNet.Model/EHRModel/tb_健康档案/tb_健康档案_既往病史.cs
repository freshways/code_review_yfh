﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DotNet.Model
{
	///名称：tb_健康档案_既往病史
	///作者：yufh 
	///创建时间：2020-09-22 09:37 
	///功能描述：tb_健康档案_既往病史	实体类
	[Serializable]
	public class tb_健康档案_既往病史
	{
		#region Model

		/// <summary>
		/// ID
		/// </summary>
		[DisplayName("ID")]
		public long ID { get; set; }

		/// <summary>
		/// 个人档案编号
		/// </summary>
		[DisplayName("个人档案编号")]
		public string 个人档案编号 { get; set; }

		/// <summary>
		/// 疾病名称
		/// </summary>
		[DisplayName("疾病名称")]
		public string 疾病名称 { get; set; }

		/// <summary>
		/// D_JBBM
		/// </summary>
		[DisplayName("D_JBBM")]
		public string D_JBBM { get; set; }

		/// <summary>
		/// 日期
		/// </summary>
		[DisplayName("日期")]
		public string 日期 { get; set; }

		/// <summary>
		/// D_ZDDW
		/// </summary>
		[DisplayName("D_ZDDW")]
		public string D_ZDDW { get; set; }

		/// <summary>
		/// D_CLQK
		/// </summary>
		[DisplayName("D_CLQK")]
		public string D_CLQK { get; set; }

		/// <summary>
		/// D_ZGQK
		/// </summary>
		[DisplayName("D_ZGQK")]
		public string D_ZGQK { get; set; }

		/// <summary>
		/// 疾病名称其他
		/// </summary>
		[DisplayName("疾病名称其他")]
		public string 疾病名称其他 { get; set; }

		/// <summary>
		/// 疾病类型
		/// </summary>
		[DisplayName("疾病类型")]
		public string 疾病类型 { get; set; }

		/// <summary>
		/// 恶性肿瘤
		/// </summary>
		[DisplayName("恶性肿瘤")]
		public string 恶性肿瘤 { get; set; }

		/// <summary>
		/// 疾病其他
		/// </summary>
		[DisplayName("疾病其他")]
		public string 疾病其他 { get; set; }

		/// <summary>
		/// 职业病其他
		/// </summary>
		[DisplayName("职业病其他")]
		public string 职业病其他 { get; set; }
		#endregion Model
	}
}
