﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DotNet.Model
{
	///名称：tb_健康档案_个人健康特征
	///作者：yufh 
	///创建时间：2020-09-22 08:47 
	///功能描述：tb_健康档案_个人健康特征	实体类
	[Serializable]
	public class tb_健康档案_个人健康特征
	{
		#region Model

		/// <summary>
		/// ID
		/// </summary>
		[DisplayName("ID")]
		public long ID { get; set; }

		/// <summary>
		/// 个人档案编号
		/// </summary>
		[DisplayName("个人档案编号")]
		public string 个人档案编号 { get; set; }

		/// <summary>
		/// 与户主关系
		/// </summary>
		[DisplayName("与户主关系")]
		public string 与户主关系 { get; set; }

		/// <summary>
		/// 个人基本信息表
		/// </summary>
		[DisplayName("个人基本信息表")]
		public string 个人基本信息表 { get; set; }

		/// <summary>
		/// 家庭档案
		/// </summary>
		[DisplayName("家庭档案")]
		public string 家庭档案 { get; set; }

		/// <summary>
		/// 健康体检
		/// </summary>
		[DisplayName("健康体检")]
		public string 健康体检 { get; set; }

		/// <summary>
		/// 接诊记录
		/// </summary>
		[DisplayName("接诊记录")]
		public string 接诊记录 { get; set; }

		/// <summary>
		/// 会诊记录
		/// </summary>
		[DisplayName("会诊记录")]
		public string 会诊记录 { get; set; }

		/// <summary>
		/// 儿童基本信息
		/// </summary>
		[DisplayName("儿童基本信息")]
		public string 儿童基本信息 { get; set; }

		/// <summary>
		/// 新生儿随访记录
		/// </summary>
		[DisplayName("新生儿随访记录")]
		public string 新生儿随访记录 { get; set; }

		/// <summary>
		/// 儿童健康检查记录表满月
		/// </summary>
		[DisplayName("儿童健康检查记录表满月")]
		public string 儿童健康检查记录表满月 { get; set; }

		/// <summary>
		/// 儿童健康检查记录表3月
		/// </summary>
		[DisplayName("儿童健康检查记录表3月")]
		public string 儿童健康检查记录表3月 { get; set; }

		/// <summary>
		/// 儿童健康检查记录表6月
		/// </summary>
		[DisplayName("儿童健康检查记录表6月")]
		public string 儿童健康检查记录表6月 { get; set; }

		/// <summary>
		/// 儿童健康检查记录表8月
		/// </summary>
		[DisplayName("儿童健康检查记录表8月")]
		public string 儿童健康检查记录表8月 { get; set; }

		/// <summary>
		/// 儿童健康检查记录表12月
		/// </summary>
		[DisplayName("儿童健康检查记录表12月")]
		public string 儿童健康检查记录表12月 { get; set; }

		/// <summary>
		/// 儿童健康检查记录表18月
		/// </summary>
		[DisplayName("儿童健康检查记录表18月")]
		public string 儿童健康检查记录表18月 { get; set; }

		/// <summary>
		/// 儿童健康检查记录表2岁
		/// </summary>
		[DisplayName("儿童健康检查记录表2岁")]
		public string 儿童健康检查记录表2岁 { get; set; }

		/// <summary>
		/// 儿童健康检查记录表2岁半
		/// </summary>
		[DisplayName("儿童健康检查记录表2岁半")]
		public string 儿童健康检查记录表2岁半 { get; set; }

		/// <summary>
		/// 儿童健康检查记录表3岁
		/// </summary>
		[DisplayName("儿童健康检查记录表3岁")]
		public string 儿童健康检查记录表3岁 { get; set; }

		/// <summary>
		/// 儿童健康检查记录表4岁
		/// </summary>
		[DisplayName("儿童健康检查记录表4岁")]
		public string 儿童健康检查记录表4岁 { get; set; }

		/// <summary>
		/// 儿童健康检查记录表5岁
		/// </summary>
		[DisplayName("儿童健康检查记录表5岁")]
		public string 儿童健康检查记录表5岁 { get; set; }

		/// <summary>
		/// 儿童健康检查记录表6岁
		/// </summary>
		[DisplayName("儿童健康检查记录表6岁")]
		public string 儿童健康检查记录表6岁 { get; set; }

		/// <summary>
		/// 儿童入托信息表
		/// </summary>
		[DisplayName("儿童入托信息表")]
		public string 儿童入托信息表 { get; set; }

		/// <summary>
		/// 妇女保健检查
		/// </summary>
		[DisplayName("妇女保健检查")]
		public string 妇女保健检查 { get; set; }

		/// <summary>
		/// 妇女更年期保健检查
		/// </summary>
		[DisplayName("妇女更年期保健检查")]
		public string 妇女更年期保健检查 { get; set; }

		/// <summary>
		/// 孕产妇基本信息表
		/// </summary>
		[DisplayName("孕产妇基本信息表")]
		public string 孕产妇基本信息表 { get; set; }

		/// <summary>
		/// 第一次产前检查表
		/// </summary>
		[DisplayName("第一次产前检查表")]
		public string 第一次产前检查表 { get; set; }

		/// <summary>
		/// 第二次产前检查表
		/// </summary>
		[DisplayName("第二次产前检查表")]
		public string 第二次产前检查表 { get; set; }

		/// <summary>
		/// 第三次产前检查表
		/// </summary>
		[DisplayName("第三次产前检查表")]
		public string 第三次产前检查表 { get; set; }

		/// <summary>
		/// 第四次产前检查表
		/// </summary>
		[DisplayName("第四次产前检查表")]
		public string 第四次产前检查表 { get; set; }

		/// <summary>
		/// 第五次产前检查表
		/// </summary>
		[DisplayName("第五次产前检查表")]
		public string 第五次产前检查表 { get; set; }

		/// <summary>
		/// 孕产妇产后访视
		/// </summary>
		[DisplayName("孕产妇产后访视")]
		public string 孕产妇产后访视 { get; set; }

		/// <summary>
		/// 孕产妇产后42天
		/// </summary>
		[DisplayName("孕产妇产后42天")]
		public string 孕产妇产后42天 { get; set; }

		/// <summary>
		/// 老年人随访
		/// </summary>
		[DisplayName("老年人随访")]
		public string 老年人随访 { get; set; }

		/// <summary>
		/// 高血压管理卡
		/// </summary>
		[DisplayName("高血压管理卡")]
		public string 高血压管理卡 { get; set; }

		/// <summary>
		/// 高血压随访表
		/// </summary>
		[DisplayName("高血压随访表")]
		public string 高血压随访表 { get; set; }

		/// <summary>
		/// 糖尿病管理卡
		/// </summary>
		[DisplayName("糖尿病管理卡")]
		public string 糖尿病管理卡 { get; set; }

		/// <summary>
		/// 糖尿病随访表
		/// </summary>
		[DisplayName("糖尿病随访表")]
		public string 糖尿病随访表 { get; set; }

		/// <summary>
		/// 脑卒中管理卡
		/// </summary>
		[DisplayName("脑卒中管理卡")]
		public string 脑卒中管理卡 { get; set; }

		/// <summary>
		/// 脑卒中随访表
		/// </summary>
		[DisplayName("脑卒中随访表")]
		public string 脑卒中随访表 { get; set; }

		/// <summary>
		/// 冠心病管理卡
		/// </summary>
		[DisplayName("冠心病管理卡")]
		public string 冠心病管理卡 { get; set; }

		/// <summary>
		/// 冠心病随访表
		/// </summary>
		[DisplayName("冠心病随访表")]
		public string 冠心病随访表 { get; set; }

		/// <summary>
		/// 精神疾病信息补充表
		/// </summary>
		[DisplayName("精神疾病信息补充表")]
		public string 精神疾病信息补充表 { get; set; }

		/// <summary>
		/// 精神疾病随访表
		/// </summary>
		[DisplayName("精神疾病随访表")]
		public string 精神疾病随访表 { get; set; }

		/// <summary>
		/// 听力言语残疾随访表
		/// </summary>
		[DisplayName("听力言语残疾随访表")]
		public string 听力言语残疾随访表 { get; set; }

		/// <summary>
		/// 肢体残疾随访表
		/// </summary>
		[DisplayName("肢体残疾随访表")]
		public string 肢体残疾随访表 { get; set; }

		/// <summary>
		/// 智力残疾随访表
		/// </summary>
		[DisplayName("智力残疾随访表")]
		public string 智力残疾随访表 { get; set; }

		/// <summary>
		/// 视力残疾随访表
		/// </summary>
		[DisplayName("视力残疾随访表")]
		public string 视力残疾随访表 { get; set; }

		/// <summary>
		/// 是否高血压
		/// </summary>
		[DisplayName("是否高血压")]
		public string 是否高血压 { get; set; }

		/// <summary>
		/// 是否糖尿病
		/// </summary>
		[DisplayName("是否糖尿病")]
		public string 是否糖尿病 { get; set; }

		/// <summary>
		/// 是否冠心病
		/// </summary>
		[DisplayName("是否冠心病")]
		public string 是否冠心病 { get; set; }

		/// <summary>
		/// 是否脑卒中
		/// </summary>
		[DisplayName("是否脑卒中")]
		public string 是否脑卒中 { get; set; }

		/// <summary>
		/// 是否空腹血糖
		/// </summary>
		[DisplayName("是否空腹血糖")]
		public string 是否空腹血糖 { get; set; }

		/// <summary>
		/// 是否血脂
		/// </summary>
		[DisplayName("是否血脂")]
		public string 是否血脂 { get; set; }

		/// <summary>
		/// 是否餐后血糖
		/// </summary>
		[DisplayName("是否餐后血糖")]
		public string 是否餐后血糖 { get; set; }

		/// <summary>
		/// 是否肥胖
		/// </summary>
		[DisplayName("是否肥胖")]
		public string 是否肥胖 { get; set; }

		/// <summary>
		/// 是否重度吸烟
		/// </summary>
		[DisplayName("是否重度吸烟")]
		public string 是否重度吸烟 { get; set; }

		/// <summary>
		/// 是否超重肥胖
		/// </summary>
		[DisplayName("是否超重肥胖")]
		public string 是否超重肥胖 { get; set; }

		/// <summary>
		/// 是否肿瘤
		/// </summary>
		[DisplayName("是否肿瘤")]
		public string 是否肿瘤 { get; set; }

		/// <summary>
		/// 是否慢阻肺
		/// </summary>
		[DisplayName("是否慢阻肺")]
		public string 是否慢阻肺 { get; set; }

		/// <summary>
		/// 所属机构
		/// </summary>
		[DisplayName("所属机构")]
		public string 所属机构 { get; set; }

		/// <summary>
		/// 家庭档案编号
		/// </summary>
		[DisplayName("家庭档案编号")]
		public string 家庭档案编号 { get; set; }

		/// <summary>
		/// 残疾人康复服务随访记录表
		/// </summary>
		[DisplayName("残疾人康复服务随访记录表")]
		public string 残疾人康复服务随访记录表 { get; set; }

		/// <summary>
		/// 言语残疾随访表
		/// </summary>
		[DisplayName("言语残疾随访表")]
		public string 言语残疾随访表 { get; set; }
		#endregion Model
	}
}
