﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DotNet.Model
{
	///名称：tb_健康档案_家族病史
	///作者：yufh 
	///创建时间：2020-09-22 10:43 
	///功能描述：tb_健康档案_家族病史	实体类
	[Serializable]
	public class tb_健康档案_家族病史
	{
		#region Model

		/// <summary>
		/// ID
		/// </summary>
		[DisplayName("ID")]
		public long ID { get; set; }

		/// <summary>
		/// 个人档案编号
		/// </summary>
		[DisplayName("个人档案编号")]
		public string 个人档案编号 { get; set; }

		/// <summary>
		/// 家族关系
		/// </summary>
		[DisplayName("家族关系")]
		public string 家族关系 { get; set; }

		/// <summary>
		/// 家族病史
		/// </summary>
		[DisplayName("家族病史")]
		public string 家族病史 { get; set; }

		/// <summary>
		/// D_JBBM
		/// </summary>
		[DisplayName("D_JBBM")]
		public string D_JBBM { get; set; }

		/// <summary>
		/// 其他疾病
		/// </summary>
		[DisplayName("其他疾病")]
		public string 其他疾病 { get; set; }

		/// <summary>
		/// D_QTJBBM
		/// </summary>
		[DisplayName("D_QTJBBM")]
		public string D_QTJBBM { get; set; }

		/// <summary>
		/// 家族关系名称
		/// </summary>
		[DisplayName("家族关系名称")]
		public string 家族关系名称 { get; set; }
		#endregion Model
	}
}
