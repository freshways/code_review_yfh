﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DotNet.Model
{
    public class tb_健康档案Searcher
    {
        /*
         {
 "idCard":"6f0091df97702a28c1ffdc4c420ccfdd2f99d48cb8d532229269cb25fb777228",
 "startNum":"1",
 "endNum":"1000",
 "random":"b48861793cd14869a82c5b71cd65b098",
 "sign":"89a7b9d740b7e3755459aa430cdede95",
 "appKey":"274b4345c0929ee10bb2be7d0b1c1456"
}
         */
        [Display(Name = "身份证号")]
        public string IDCard { get; set; }

        [Display(Name = "开始序号")]
        public string startNum { get; set; }

        [Display(Name = "结束序号")]
        public string endNum { get; set; }

        [Display(Name = "开始日期")]
        public string startDate { get; set; }

        [Display(Name = "结束日期")]
        public string endDate { get; set; }

        [Display(Name = "随机值")]
        public string random { get; set; }
        [Display(Name = "机构标识")]
        public string appKey { get; set; }
        [Display(Name = "验证")]
        public string sign { get; set; }
    }

    public class tb_健康档案Res
    {
        /// <summary>
        /// 患者身份证 
        /// </summary>
        public string idCard { get; set; }
        /// <summary>
        /// 公卫健康档案编号
        /// </summary>
        public string customerId { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public string customerName { get; set; }
        /// <summary>
        /// 本人电话
        /// </summary>
        public string telphon { get; set; }
        /// <summary>
        /// 联系人姓名
        /// </summary>
        public string familyName { get; set; }
        /// <summary>
        /// 联系人电话
        /// </summary>
        public string familyTel { get; set; }
        /// <summary>
        /// 性别编号
        /// </summary>
        public string sex { get; set; }
        /// <summary>
        /// 血型编号
        /// </summary>
        public string blood { get; set; }
        /// <summary>
        /// 出生日期
        /// </summary>
        public string birthday { get; set; }
        /// <summary>
        /// 现住地址
        /// </summary>
        public string address { get; set; }

        /// <summary>
        /// 户籍地址
        /// </summary>
        public string regAddress { get; set; }

        /// <summary>
        /// 药物过敏史
        /// </summary>
        public string drugEllergies { get; set; }

        /// <summary>
        /// 暴露史史
        /// </summary>
        public string occupation { get; set; }
        /// <summary>
        /// 既往史
        /// </summary>
        public List<diseaseList> diseaseList { get; set; }
        /// <summary>
        /// 家族史
        /// </summary>
        public List<familyDiseaseList> familyDiseaseList { get; set; }
        /// <summary>
        /// 手术
        /// </summary>
        public List<operationList> operationList { get; set; }
        /// <summary>
        /// 外伤
        /// </summary>
        public List<traumaList> traumaList { get; set; }
        /// <summary>
        /// 输血
        /// </summary>
        public List<transfusionList> transfusionList { get; set; }
        /// <summary>
        /// 遗传病史
        /// </summary>
        public string genetic { get; set; }

        /// <summary>
        /// 所属机构Id
        /// </summary>
        public string organizationId { get; set; }
        /// <summary>
        /// 所属机构名称
        /// </summary>
        public string organizationName { get; set; }
        
        /// <summary>
        /// 建档日期
        /// </summary>
        public string createTime { get; set; }
        /// <summary>
        /// 最后更新时间
        /// </summary>
        public string updateTime { get; set; }
    }
    /// <summary>
    /// 既往史
    /// </summary>
    public class diseaseList {

        /// <summary>
        /// 疾病名称
        /// </summary>
        public string diseaseName { get; set; }
        /// <summary>
        /// 确诊日期
        /// </summary>
        public string diseaseDate { get; set; }
    }

    /// <summary>
    /// 家族史
    /// </summary>
    public class familyDiseaseList
    {
        /// <summary>
        /// 关系名称
        /// </summary>
        public string MyProperty { get; set; }
        /// <summary>
        /// 疾病名称
        /// </summary>
        public string diseaseName { get; set; }
    }

    /// <summary>
    /// 手术史
    /// </summary>
    public class operationList
    {
        /// <summary>
        /// 手术名称
        /// </summary>
        public string operationName { get; set; }
        /// <summary>
        /// 手术日期
        /// </summary>
        public string operationDate { get; set; }

    }
    /// <summary>
    /// 外伤
    /// </summary>
    public class traumaList
    {
        /// <summary>
        /// 外伤名称
        /// </summary>
        public string traumaName { get; set; }

        /// <summary>
        /// 外伤日期
        /// </summary>
        public string traumaDate { get; set; }
    }

    /// <summary>
    /// 输血
    /// </summary>
    public class transfusionList
    {
        /// <summary>
        /// 输血原因
        /// </summary>
        public string transReason { get; set; }
        /// <summary>
        /// 输血日期
        /// </summary>
        public string MyProperty { get; set; }
    }
}
