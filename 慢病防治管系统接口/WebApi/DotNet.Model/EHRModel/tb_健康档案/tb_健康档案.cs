﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DotNet.Model
{
    [Serializable]
    public class tb_健康档案
    {
        /// <summary>
        /// ID
        /// </summary>
        [DisplayName("ID")]
        public long ID { get; set; }

        /// <summary>
        /// 个人档案编号
        /// </summary>
        [DisplayName("个人档案编号")]
        public string 个人档案编号 { get; set; }

        /// <summary>
        /// 家庭档案编号
        /// </summary>
        [DisplayName("家庭档案编号")]
        public string 家庭档案编号 { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        [DisplayName("姓名")]
        public string 姓名 { get; set; }

        /// <summary>
        /// 身份证号
        /// </summary>
        [DisplayName("身份证号")]
        public string 身份证号 { get; set; }

        /// <summary>
        /// 拼音简码
        /// </summary>
        [DisplayName("拼音简码")]
        public string 拼音简码 { get; set; }

        /// <summary>
        /// 与户主关系
        /// </summary>
        [DisplayName("与户主关系")]
        public string 与户主关系 { get; set; }

        /// <summary>
        /// 工作单位
        /// </summary>
        [DisplayName("工作单位")]
        public string 工作单位 { get; set; }

        /// <summary>
        /// 本人电话
        /// </summary>
        [DisplayName("本人电话")]
        public string 本人电话 { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        [DisplayName("邮箱")]
        public string 邮箱 { get; set; }

        /// <summary>
        /// 省
        /// </summary>
        [DisplayName("省")]
        public string 省 { get; set; }

        /// <summary>
        /// 市
        /// </summary>
        [DisplayName("市")]
        public string 市 { get; set; }

        /// <summary>
        /// 区
        /// </summary>
        [DisplayName("区")]
        public string 区 { get; set; }

        /// <summary>
        /// 街道
        /// </summary>
        [DisplayName("街道")]
        public string 街道 { get; set; }

        /// <summary>
        /// 居委会
        /// </summary>
        [DisplayName("居委会")]
        public string 居委会 { get; set; }

        /// <summary>
        /// 居住地址
        /// </summary>
        [DisplayName("居住地址")]
        public string 居住地址 { get; set; }

        /// <summary>
        /// 所属片区
        /// </summary>
        [DisplayName("所属片区")]
        public string 所属片区 { get; set; }

        /// <summary>
        /// 常住类型
        /// </summary>
        [DisplayName("常住类型")]
        public string 常住类型 { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        [DisplayName("性别")]
        public string 性别 { get; set; }

        /// <summary>
        /// 出生日期
        /// </summary>
        [DisplayName("出生日期")]
        public string 出生日期 { get; set; }

        /// <summary>
        /// 民族
        /// </summary>
        [DisplayName("民族")]
        public string 民族 { get; set; }

        /// <summary>
        /// 文化程度
        /// </summary>
        [DisplayName("文化程度")]
        public string 文化程度 { get; set; }

        /// <summary>
        /// 职业
        /// </summary>
        [DisplayName("职业")]
        public string 职业 { get; set; }

        /// <summary>
        /// 婚姻状况
        /// </summary>
        [DisplayName("婚姻状况")]
        public string 婚姻状况 { get; set; }

        /// <summary>
        /// 医疗费支付类型
        /// </summary>
        [DisplayName("医疗费支付类型")]
        public string 医疗费支付类型 { get; set; }

        /// <summary>
        /// 医疗保险号
        /// </summary>
        [DisplayName("医疗保险号")]
        public string 医疗保险号 { get; set; }

        /// <summary>
        /// 新农合号
        /// </summary>
        [DisplayName("新农合号")]
        public string 新农合号 { get; set; }

        /// <summary>
        /// 所属机构
        /// </summary>
        [DisplayName("所属机构")]
        public string 所属机构 { get; set; }

        /// <summary>
        /// 调查时间
        /// </summary>
        [DisplayName("调查时间")]
        public string 调查时间 { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DisplayName("创建时间")]
        public string 创建时间 { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        [DisplayName("修改时间")]
        public string 修改时间 { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        [DisplayName("创建人")]
        public string 创建人 { get; set; }

        /// <summary>
        /// 修改人
        /// </summary>
        [DisplayName("修改人")]
        public string 修改人 { get; set; }

        /// <summary>
        /// 创建机构
        /// </summary>
        [DisplayName("创建机构")]
        public string 创建机构 { get; set; }

        /// <summary>
        /// PASSWORD
        /// </summary>
        [DisplayName("PASSWORD")]
        public string PASSWORD { get; set; }

        /// <summary>
        /// D_ZHUXIAO
        /// </summary>
        [DisplayName("D_ZHUXIAO")]
        public string D_ZHUXIAO { get; set; }

        /// <summary>
        /// 联系人姓名
        /// </summary>
        [DisplayName("联系人姓名")]
        public string 联系人姓名 { get; set; }

        /// <summary>
        /// 联系人电话
        /// </summary>
        [DisplayName("联系人电话")]
        public string 联系人电话 { get; set; }

        /// <summary>
        /// 医疗费用支付类型其他
        /// </summary>
        [DisplayName("医疗费用支付类型其他")]
        public string 医疗费用支付类型其他 { get; set; }

        /// <summary>
        /// 怀孕情况
        /// </summary>
        [DisplayName("怀孕情况")]
        public string 怀孕情况 { get; set; }

        /// <summary>
        /// 孕次
        /// </summary>
        [DisplayName("孕次")]
        public string 孕次 { get; set; }

        /// <summary>
        /// 产次
        /// </summary>
        [DisplayName("产次")]
        public string 产次 { get; set; }

        /// <summary>
        /// 缺项
        /// </summary>
        [DisplayName("缺项")]
        public string 缺项 { get; set; }

        /// <summary>
        /// 档案类别
        /// </summary>
        [DisplayName("档案类别")]
        public string 档案类别 { get; set; }

        /// <summary>
        /// 档案状态
        /// </summary>
        [DisplayName("档案状态")]
        public string 档案状态 { get; set; }

        /// <summary>
        /// D_DAZTYY
        /// </summary>
        [DisplayName("D_DAZTYY")]
        public string D_DAZTYY { get; set; }

        /// <summary>
        /// 证件类型
        /// </summary>
        [DisplayName("证件类型")]
        public string 证件类型 { get; set; }

        /// <summary>
        /// D_ZJHQT
        /// </summary>
        [DisplayName("D_ZJHQT")]
        public string D_ZJHQT { get; set; }

        /// <summary>
        /// 厨房排气设施
        /// </summary>
        [DisplayName("厨房排气设施")]
        public string 厨房排气设施 { get; set; }

        /// <summary>
        /// 燃料类型
        /// </summary>
        [DisplayName("燃料类型")]
        public string 燃料类型 { get; set; }

        /// <summary>
        /// 饮水
        /// </summary>
        [DisplayName("饮水")]
        public string 饮水 { get; set; }

        /// <summary>
        /// 厕所
        /// </summary>
        [DisplayName("厕所")]
        public string 厕所 { get; set; }

        /// <summary>
        /// 禽畜栏
        /// </summary>
        [DisplayName("禽畜栏")]
        public string 禽畜栏 { get; set; }

        /// <summary>
        /// 完整度
        /// </summary>
        [DisplayName("完整度")]
        public string 完整度 { get; set; }

        /// <summary>
        /// D_GRDABH_17
        /// </summary>
        [DisplayName("D_GRDABH_17")]
        public string D_GRDABH_17 { get; set; }

        /// <summary>
        /// D_GRDABH_SHOW
        /// </summary>
        [DisplayName("D_GRDABH_SHOW")]
        public string D_GRDABH_SHOW { get; set; }

        /// <summary>
        /// 劳动强度
        /// </summary>
        [DisplayName("劳动强度")]
        public string 劳动强度 { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        [DisplayName("是否显示")]
        public string 是否显示 { get; set; }

        /// <summary>
        /// 是否贫困人口
        /// </summary>
        [DisplayName("是否贫困人口")]
        public string 是否贫困人口 { get; set; }

        /// <summary>
        /// 是否流动人口
        /// </summary>
        [DisplayName("是否流动人口")]
        public string 是否流动人口 { get; set; }

        /// <summary>
        /// 是否签约
        /// </summary>
        [DisplayName("是否签约")]
        public string 是否签约 { get; set; }

        /// <summary>
        /// 外出情况
        /// </summary>
        [DisplayName("外出情况")]
        public string 外出情况 { get; set; }

        /// <summary>
        /// 外出地址
        /// </summary>
        [DisplayName("外出地址")]
        public string 外出地址 { get; set; }

        /// <summary>
        /// 档案位置
        /// </summary>
        [DisplayName("档案位置")]
        public string 档案位置 { get; set; }

        /// <summary>
        /// 户主姓名
        /// </summary>
        [DisplayName("户主姓名")]
        public string 户主姓名 { get; set; }

        /// <summary>
        /// 户主身份证号
        /// </summary>
        [DisplayName("户主身份证号")]
        public string 户主身份证号 { get; set; }

        /// <summary>
        /// 家庭人口数
        /// </summary>
        [DisplayName("家庭人口数")]
        public string 家庭人口数 { get; set; }

        /// <summary>
        /// 家庭结构
        /// </summary>
        [DisplayName("家庭结构")]
        public string 家庭结构 { get; set; }

        /// <summary>
        /// 居住情况
        /// </summary>
        [DisplayName("居住情况")]
        public string 居住情况 { get; set; }

        /// <summary>
        /// 职工医疗保险卡号
        /// </summary>
        [DisplayName("职工医疗保险卡号")]
        public string 职工医疗保险卡号 { get; set; }

        /// <summary>
        /// 居民医疗保险卡号
        /// </summary>
        [DisplayName("居民医疗保险卡号")]
        public string 居民医疗保险卡号 { get; set; }

        /// <summary>
        /// 本人或家属签字
        /// </summary>
        [DisplayName("本人或家属签字")]
        public string 本人或家属签字 { get; set; }

        /// <summary>
        /// 签字时间
        /// </summary>
        [DisplayName("签字时间")]
        public string 签字时间 { get; set; }

        /// <summary>
        /// 贫困救助卡号
        /// </summary>
        [DisplayName("贫困救助卡号")]
        public string 贫困救助卡号 { get; set; }

        /// <summary>
        /// 复核标记
        /// </summary>
        [DisplayName("复核标记")]
        public string 复核标记 { get; set; }

        /// <summary>
        /// 复核备注
        /// </summary>
        [DisplayName("复核备注")]
        public string 复核备注 { get; set; }

        /// <summary>
        /// 查阅_修改内容
        /// </summary>
        [DisplayName("查阅_修改内容")]
        public string 查阅_修改内容 { get; set; }

        /// <summary>
        /// 查阅_修改时间
        /// </summary>
        [DisplayName("查阅_修改时间")]
        public string 查阅_修改时间 { get; set; }

        /// <summary>
        /// 二次复核
        /// </summary>
        [DisplayName("二次复核")]
        public string 二次复核 { get; set; }

        /// <summary>
        /// 二次复核时间
        /// </summary>
        [DisplayName("二次复核时间")]
        public string 二次复核时间 { get; set; }

    }
}
