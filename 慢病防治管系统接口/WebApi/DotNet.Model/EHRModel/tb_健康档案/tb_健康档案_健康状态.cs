﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DotNet.Model
{
	///名称：tb_健康档案_健康状态
	///作者：yufh 
	///创建时间：2020-09-22 10:03 
	///功能描述：tb_健康档案_健康状态	实体类
	[Serializable]
	public class tb_健康档案_健康状态
	{
		#region Model

		/// <summary>
		/// ID
		/// </summary>
		[DisplayName("ID")]
		public long ID { get; set; }

		/// <summary>
		/// 个人档案编号
		/// </summary>
		[DisplayName("个人档案编号")]
		public string 个人档案编号 { get; set; }

		/// <summary>
		/// 血型
		/// </summary>
		[DisplayName("血型")]
		public string 血型 { get; set; }

		/// <summary>
		/// RH
		/// </summary>
		[DisplayName("RH")]
		public string RH { get; set; }

		/// <summary>
		/// 过敏史有无
		/// </summary>
		[DisplayName("过敏史有无")]
		public string 过敏史有无 { get; set; }

		/// <summary>
		/// 药物过敏史
		/// </summary>
		[DisplayName("药物过敏史")]
		public string 药物过敏史 { get; set; }

		/// <summary>
		/// 过敏史其他
		/// </summary>
		[DisplayName("过敏史其他")]
		public string 过敏史其他 { get; set; }

		/// <summary>
		/// 暴露史
		/// </summary>
		[DisplayName("暴露史")]
		public string 暴露史 { get; set; }

		/// <summary>
		/// D_YBLS
		/// </summary>
		[DisplayName("D_YBLS")]
		public string D_YBLS { get; set; }

		/// <summary>
		/// 暴露史化学品
		/// </summary>
		[DisplayName("暴露史化学品")]
		public string 暴露史化学品 { get; set; }

		/// <summary>
		/// 暴露史毒物
		/// </summary>
		[DisplayName("暴露史毒物")]
		public string 暴露史毒物 { get; set; }

		/// <summary>
		/// 暴露史射线
		/// </summary>
		[DisplayName("暴露史射线")]
		public string 暴露史射线 { get; set; }

		/// <summary>
		/// 遗传病史有无
		/// </summary>
		[DisplayName("遗传病史有无")]
		public string 遗传病史有无 { get; set; }

		/// <summary>
		/// 遗传病史
		/// </summary>
		[DisplayName("遗传病史")]
		public string 遗传病史 { get; set; }

		/// <summary>
		/// 有无残疾
		/// </summary>
		[DisplayName("有无残疾")]
		public string 有无残疾 { get; set; }

		/// <summary>
		/// 有无疾病
		/// </summary>
		[DisplayName("有无疾病")]
		public string 有无疾病 { get; set; }

		/// <summary>
		/// 有无既往史
		/// </summary>
		[DisplayName("有无既往史")]
		public string 有无既往史 { get; set; }

		/// <summary>
		/// 残疾情况
		/// </summary>
		[DisplayName("残疾情况")]
		public string 残疾情况 { get; set; }

		/// <summary>
		/// 调查时间
		/// </summary>
		[DisplayName("调查时间")]
		public string 调查时间 { get; set; }

		/// <summary>
		/// 创建人
		/// </summary>
		[DisplayName("创建人")]
		public string 创建人 { get; set; }

		/// <summary>
		/// 修改人
		/// </summary>
		[DisplayName("修改人")]
		public string 修改人 { get; set; }

		/// <summary>
		/// 创建时间
		/// </summary>
		[DisplayName("创建时间")]
		public string 创建时间 { get; set; }

		/// <summary>
		/// 残疾其他
		/// </summary>
		[DisplayName("残疾其他")]
		public string 残疾其他 { get; set; }
		#endregion Model
	}
}
