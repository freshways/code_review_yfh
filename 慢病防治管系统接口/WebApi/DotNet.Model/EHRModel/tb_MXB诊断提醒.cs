﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DotNet.Model
{
	///名称：tb_MXB诊断提醒
	///作者：yufh 
	///创建时间：2020-10-09 10:44 
	///功能描述：tb_MXB诊断提醒	实体类
	[Serializable]
	public class tb_MXB诊断提醒
	{
		#region Model

		/// <summary>
		/// ID
		/// </summary>
		[DisplayName("ID")]
		public int ID { get; set; }

		/// <summary>
		/// 患者身份证
		/// </summary>
		[DisplayName("患者身份证")]
		public string 患者身份证 { get; set; }

		/// <summary>
		/// 患者姓名
		/// </summary>
		[DisplayName("患者姓名")]
		public string 患者姓名 { get; set; }

		/// <summary>
		/// 电话
		/// </summary>
		[DisplayName("电话")]
		public string 电话 { get; set; }

		/// <summary>
		/// 居住地址
		/// </summary>
		[DisplayName("居住地址")]
		public string 居住地址 { get; set; }

		/// <summary>
		/// 诊断名称
		/// </summary>
		[DisplayName("诊断名称")]
		public string 诊断名称 { get; set; }

		/// <summary>
		/// 诊断日期
		/// </summary>
		[DisplayName("诊断日期")]
		public string 诊断日期 { get; set; }

		/// <summary>
		/// 诊断医院
		/// </summary>
		[DisplayName("诊断医院")]
		public string 诊断医院 { get; set; }

		/// <summary>
		/// 诊断科室
		/// </summary>
		[DisplayName("诊断科室")]
		public string 诊断科室 { get; set; }

		/// <summary>
		/// 诊断医生
		/// </summary>
		[DisplayName("诊断医生")]
		public string 诊断医生 { get; set; }

		/// <summary>
		/// 机构标识
		/// </summary>
		[DisplayName("appKey")]
		public string appKey { get; set; }

		/// <summary>
		/// 所属机构
		/// </summary>
		[DisplayName("所属机构")]
		public string 所属机构 { get; set; }

		/// <summary>
		/// random
		/// </summary>
		[DisplayName("random")]
		public string random { get; set; }

		/// <summary>
		/// sign
		/// </summary>
		[DisplayName("sign")]
		public string sign { get; set; }
		#endregion Model
	}

	public class tb_MXB诊断提醒Input
	{
		/// <summary>
		/// 患者身份证
		/// </summary>
		public string idCard { get; set; }
		/// <summary>
		/// 患者姓名
		/// </summary>
		public string name { get; set; }
		/// <summary>
		/// 电话
		/// </summary>
		public string telphone { get; set; }
		/// <summary>
		/// 居住地址
		/// </summary>
		public string address { get; set; }
		/// <summary>
		/// 诊断名称
		/// </summary>
		public string diagnosisName { get; set; }
		/// <summary>
		/// 诊断日期
		/// </summary>
		public string diagnosisDate { get; set; }
		/// <summary>
		/// 诊断医院
		/// </summary>
		public string hospitalName { get; set; }
		/// <summary>
		/// 诊断科室
		/// </summary>
		public string deptName { get; set; }
		/// <summary>
		/// 诊断医生
		/// </summary>
		public string doctorName { get; set; }
		/// <summary>
		/// 机构标识
		/// </summary>
		public string appKey { get; set; }
		/// <summary>
		/// 随机值
		/// </summary>
		public string random { get; set; }
		/// <summary>
		/// 校验值
		/// </summary>
		public string sign { get; set; }
	}

}
