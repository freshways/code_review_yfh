﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DotNet.Model
{
	///名称：tb_MXB糖尿病随访表_用药调整
	///作者：yufh 
	///创建时间：2020-09-30 09:18 
	///功能描述：tb_MXB糖尿病随访表_用药调整	实体类
	[Serializable]
	public class tb_MXB糖尿病随访表_用药调整
	{
		#region Model

		/// <summary>
		/// Y_ID
		/// </summary>
		[DisplayName("Y_ID")]
		[Key]
		public long Y_ID { get; set; }

		/// <summary>
		/// 个人档案编号
		/// </summary>
		[DisplayName("个人档案编号")]
		public string 个人档案编号 { get; set; }

		/// <summary>
		/// 药物名称
		/// </summary>
		[DisplayName("药物名称")]
		public string 药物名称 { get; set; }

		/// <summary>
		/// 用法
		/// </summary>
		[DisplayName("用法")]
		public string 用法 { get; set; }

		/// <summary>
		/// 创建时间
		/// </summary>
		[DisplayName("创建时间")]
		public string 创建时间 { get; set; }
		#endregion Model
	}
}
