﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNet.Model
{
    public class tb_MXB高血压随访表Searcher : tb_健康档案Searcher
    {
    }

    /// <summary>
    /// 继承返回实体类
    /// </summary>
    public class tb_MXB高血压随访表Input : tb_MXB高血压随访表Res
    {
        /// <summary>
        /// 机构标识
        /// </summary>
        public string appKey { get; set; }

        /// <summary>
        /// 随机值
        /// </summary>
        public string random { get; set; }

        /// <summary>
        /// 校验值
        /// </summary>
        public string sign { get; set; }
    }

    /// <summary>
    /// 高血压随访返回信息
    /// </summary>
    public class tb_MXB高血压随访表Res
    {
        /// <summary>
        /// 
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// 公卫健康档案编号
        /// </summary>
        public string customerId { get; set; }
        /// <summary>
        /// 身份证号
        /// </summary>
        public string idCard { get; set; }
        /// <summary>
        /// 患者姓名
        /// </summary>
        public string patientName { get; set; }
        /// <summary>
        /// 随访单编号
        /// </summary>
        public string recordDetailHbpmsNo { get; set; }
        /// <summary>
        /// 失访原因
        /// </summary>
        public string offFollowReason { get; set; }
        /// <summary>
        /// 随访日期
        /// </summary>
        public string followTime { get; set; }
        /// <summary>
        /// 随访方式
        /// </summary>
        public string hbpType { get; set; }
        /// <summary>
        /// 随访表单主体
        /// </summary>
        public Gxy_MainItem main { get; set; }
        /// <summary>
        /// 下次随访日期
        /// </summary>
        public string nextPlanTime { get; set; }
        /// <summary>
        /// 随访医生公卫ID
        /// </summary>
        public string followUserId { get; set; }
        /// <summary>
        /// 随访医生签名
        /// </summary>
        public string followUserSign { get; set; }
        /// <summary>
        /// 随访创建机构公卫id
        /// </summary>
        public string organizationId { get; set; }
        /// <summary>
        /// 随访创建机构公卫名称
        /// </summary>
        public string organizationName { get; set; }
        /// <summary>
        /// 居民签名
        /// </summary>
        public string customerSign { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string remark { get; set; }
        /// <summary>
        /// 随访记录创建时间
        /// </summary>
        public string creatTime { get; set; }
        /// <summary>
        /// 区县编码
        /// </summary>
        public string areaCode { get; set; }
    }

    /// <summary>
    /// 随访表单主体
    /// </summary>
    public class Gxy_MainItem
    {
        /// <summary>
        /// 症状代码
        /// </summary>
        public string curSymptom { get; set; }
        /// <summary>
        /// 其他症状描述
        /// </summary>
        public string curSymptomDescription { get; set; }
        /// <summary>
        /// 收缩压
        /// </summary>
        public string sbp { get; set; }
        /// <summary>
        /// 舒张压
        /// </summary>
        public string dbp { get; set; }
        /// <summary>
        /// 身高
        /// </summary>
        public string height { get; set; }
        /// <summary>
        /// 体重
        /// </summary>
        public string weight { get; set; }
        /// <summary>
        /// 目标体重
        /// </summary>
        public string target_weight { get; set; }
        /// <summary>
        /// 体质指数
        /// </summary>
        public string bmi { get; set; }
        /// <summary>
        /// 目标体质指数
        /// </summary>
        public string target_bmi { get; set; }
        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; }
        /// <summary>
        /// 是否吸烟
        /// </summary>
        public string isSmoke { get; set; }
        /// <summary>
        /// 日吸烟量
        /// </summary>
        public string smokingQuantity { get; set; }
        /// <summary>
        /// 目标日吸烟量
        /// </summary>
        public string targetSmokingQuantity { get; set; }
        /// <summary>
        /// 是否饮酒
        /// </summary>
        public string isDrink { get; set; }
        /// <summary>
        /// 饮酒量
        /// </summary>
        public string drinkingQuantity { get; set; }
        /// <summary>
        /// 目标饮酒量
        /// </summary>
        public string targetDrinkingQuantity { get; set; }
        /// <summary>
        /// 运动频次
        /// </summary>
        public string sportTimes { get; set; }
        /// <summary>
        /// 目标运动频次
        /// </summary>
        public string targetSportTimes { get; set; }
        /// <summary>
        /// 运动时间
        /// </summary>
        public string sportQuantity { get; set; }
        /// <summary>
        /// 目标运动时间
        /// </summary>
        public string targetSportQuantity { get; set; }
        /// <summary>
        /// 摄入盐量
        /// </summary>
        public string saltQuantity { get; set; }
        /// <summary>
        /// 目标摄入盐量
        /// </summary>
        public string targetSaltQuantity { get; set; }
        /// <summary>
        /// 心里调整
        /// </summary>
        public string mentality { get; set; }
        /// <summary>
        /// 遵医行为
        /// </summary>
        public string compiance { get; set; }
        /// <summary>
        /// 辅助检查描述
        /// </summary>
        public string accessoryExaminatioDes { get; set; }
        /// <summary>
        /// 当前用药
        /// </summary>
        public List<Gxy_DrugsListItem> drugsList { get; set; }
        /// <summary>
        /// 服药依从性
        /// </summary>
        public string medicationCompliance { get; set; }
        /// <summary>
        /// 药物不良反应
        /// </summary>
        public string adverseReactions { get; set; }
        /// <summary>
        /// 随访结果
        /// </summary>
        public string resultType { get; set; }
        /// <summary>
        /// 下一步管理措施
        /// </summary>
        public string nextManage { get; set; }
        /// <summary>
        /// 用药调整意见
        /// </summary>
        public List<Gxy_DrugsAdviceListItem> drugsAdviceList { get; set; }
        /// <summary>
        /// 转诊原因
        /// </summary>
        public string nsferTreatmentReson { get; set; }
        /// <summary>
        /// 转诊机构科别
        /// </summary>
        public string nsferTreatmentOrganization { get; set; }
        /// <summary>
        /// 转诊联系人姓名
        /// </summary>
        public string nsferTreatmentContact { get; set; }
        /// <summary>
        /// 转诊联系人电话
        /// </summary>
        public string nsferTreatmentContactPho { get; set; }
        /// <summary>
        /// 转诊结果
        /// </summary>
        public string nsferTreatmentResult { get; set; }
    }

    /// <summary>
    /// 当前用药
    /// </summary>
    public class Gxy_DrugsListItem
    {
        /// <summary>
        /// 药品名称
        /// </summary>
        public string drugName { get; set; }
        /// <summary>
        /// 用药来源
        /// </summary>
        public string drugSource { get; set; }
        /// <summary>
        /// 每天用药次数
        /// </summary>
        public string drugUsage { get; set; }
        /// <summary>
        /// 每次用量
        /// </summary>
        public string drugDose { get; set; }
        /// <summary>
        /// 每次用量单位
        /// </summary>
        public string drugDoseUnit { get; set; }
    }

    /// <summary>
    /// 用药调整意见
    /// </summary>
    public class Gxy_DrugsAdviceListItem
    {
        /// <summary>
        /// 药品名称
        /// </summary>
        public string drugName { get; set; }
        /// <summary>
        /// 用药来源
        /// </summary>
        public string drugSource { get; set; }
        /// <summary>
        /// 每天用药次数
        /// </summary>
        public string drugUsage { get; set; }
        /// <summary>
        /// 每次用量
        /// </summary>
        public string drugDose { get; set; }
        /// <summary>
        /// 每次用量单位
        /// </summary>
        public string drugDoseUnit { get; set; }
    }

}
