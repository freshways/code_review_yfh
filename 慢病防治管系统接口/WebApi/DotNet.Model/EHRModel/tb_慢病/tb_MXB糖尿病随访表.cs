﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DotNet.Model
{
	///名称：tb_MXB糖尿病随访表
	///作者：yufh 
	///创建时间：2020-09-30 09:02 
	///功能描述：tb_MXB糖尿病随访表	实体类
	[Serializable]
	public class tb_MXB糖尿病随访表
	{
		#region Model

		/// <summary>
		/// ID
		/// </summary>
		[DisplayName("ID")]
		public long ID { get; set; }

		/// <summary>
		/// 个人档案编号
		/// </summary>
		[DisplayName("个人档案编号")]
		public string 个人档案编号 { get; set; }

		/// <summary>
		/// 姓名
		/// </summary>
		[DisplayName("姓名")]
		public string 姓名 { get; set; }

		/// <summary>
		/// 性别
		/// </summary>
		[DisplayName("性别")]
		public string 性别 { get; set; }

		/// <summary>
		/// 身份证号
		/// </summary>
		[DisplayName("身份证号")]
		public string 身份证号 { get; set; }

		/// <summary>
		/// 出生日期
		/// </summary>
		[DisplayName("出生日期")]
		public string 出生日期 { get; set; }

		/// <summary>
		/// 民族
		/// </summary>
		[DisplayName("民族")]
		public string 民族 { get; set; }

		/// <summary>
		/// 文化程度
		/// </summary>
		[DisplayName("文化程度")]
		public string 文化程度 { get; set; }

		/// <summary>
		/// 常住类型
		/// </summary>
		[DisplayName("常住类型")]
		public string 常住类型 { get; set; }

		/// <summary>
		/// 邮箱
		/// </summary>
		[DisplayName("邮箱")]
		public string 邮箱 { get; set; }

		/// <summary>
		/// 工作单位
		/// </summary>
		[DisplayName("工作单位")]
		public string 工作单位 { get; set; }

		/// <summary>
		/// 婚姻状况
		/// </summary>
		[DisplayName("婚姻状况")]
		public string 婚姻状况 { get; set; }

		/// <summary>
		/// 职业
		/// </summary>
		[DisplayName("职业")]
		public string 职业 { get; set; }

		/// <summary>
		/// 联系电话
		/// </summary>
		[DisplayName("联系电话")]
		public string 联系电话 { get; set; }

		/// <summary>
		/// 省
		/// </summary>
		[DisplayName("省")]
		public string 省 { get; set; }

		/// <summary>
		/// 市
		/// </summary>
		[DisplayName("市")]
		public string 市 { get; set; }

		/// <summary>
		/// 区
		/// </summary>
		[DisplayName("区")]
		public string 区 { get; set; }

		/// <summary>
		/// 街道
		/// </summary>
		[DisplayName("街道")]
		public string 街道 { get; set; }

		/// <summary>
		/// 居委会
		/// </summary>
		[DisplayName("居委会")]
		public string 居委会 { get; set; }

		/// <summary>
		/// 所属片区
		/// </summary>
		[DisplayName("所属片区")]
		public string 所属片区 { get; set; }

		/// <summary>
		/// 居住地址
		/// </summary>
		[DisplayName("居住地址")]
		public string 居住地址 { get; set; }

		/// <summary>
		/// 医疗费支付类型
		/// </summary>
		[DisplayName("医疗费支付类型")]
		public string 医疗费支付类型 { get; set; }

		/// <summary>
		/// 医疗保险号
		/// </summary>
		[DisplayName("医疗保险号")]
		public string 医疗保险号 { get; set; }

		/// <summary>
		/// 新农合号
		/// </summary>
		[DisplayName("新农合号")]
		public string 新农合号 { get; set; }

		/// <summary>
		/// D_JDJG
		/// </summary>
		[DisplayName("D_JDJG")]
		public string D_JDJG { get; set; }

		/// <summary>
		/// D_JDSJ
		/// </summary>
		[DisplayName("D_JDSJ")]
		public string D_JDSJ { get; set; }

		/// <summary>
		/// D_JDR
		/// </summary>
		[DisplayName("D_JDR")]
		public string D_JDR { get; set; }

		/// <summary>
		/// D_JZS
		/// </summary>
		[DisplayName("D_JZS")]
		public string D_JZS { get; set; }

		/// <summary>
		/// 吸烟数量
		/// </summary>
		[DisplayName("吸烟数量")]
		public int? 吸烟数量 { get; set; }

		/// <summary>
		/// 饮酒数量
		/// </summary>
		[DisplayName("饮酒数量")]
		public decimal? 饮酒数量 { get; set; }

		/// <summary>
		/// 运动频率
		/// </summary>
		[DisplayName("运动频率")]
		public int? 运动频率 { get; set; }

		/// <summary>
		/// 运动持续时间
		/// </summary>
		[DisplayName("运动持续时间")]
		public decimal? 运动持续时间 { get; set; }

		/// <summary>
		/// 身高
		/// </summary>
		[DisplayName("身高")]
		public decimal? 身高 { get; set; }

		/// <summary>
		/// 体重
		/// </summary>
		[DisplayName("体重")]
		public decimal? 体重 { get; set; }

		/// <summary>
		/// BMI
		/// </summary>
		[DisplayName("BMI")]
		public decimal? BMI { get; set; }

		/// <summary>
		/// 腰围
		/// </summary>
		[DisplayName("腰围")]
		public decimal? 腰围 { get; set; }

		/// <summary>
		/// 收缩压
		/// </summary>
		[DisplayName("收缩压")]
		public int? 收缩压 { get; set; }

		/// <summary>
		/// 舒张压
		/// </summary>
		[DisplayName("舒张压")]
		public int? 舒张压 { get; set; }

		/// <summary>
		/// 空腹血糖
		/// </summary>
		[DisplayName("空腹血糖")]
		public decimal? 空腹血糖 { get; set; }

		/// <summary>
		/// 餐后血糖
		/// </summary>
		[DisplayName("餐后血糖")]
		public decimal? 餐后血糖 { get; set; }

		/// <summary>
		/// 高密度蛋白
		/// </summary>
		[DisplayName("高密度蛋白")]
		public decimal? 高密度蛋白 { get; set; }

		/// <summary>
		/// 低密度蛋白
		/// </summary>
		[DisplayName("低密度蛋白")]
		public decimal? 低密度蛋白 { get; set; }

		/// <summary>
		/// 甘油三脂
		/// </summary>
		[DisplayName("甘油三脂")]
		public decimal? 甘油三脂 { get; set; }

		/// <summary>
		/// 总胆固醇
		/// </summary>
		[DisplayName("总胆固醇")]
		public decimal? 总胆固醇 { get; set; }

		/// <summary>
		/// 糖化血红蛋白
		/// </summary>
		[DisplayName("糖化血红蛋白")]
		public decimal? 糖化血红蛋白 { get; set; }

		/// <summary>
		/// 随访方式
		/// </summary>
		[DisplayName("随访方式")]
		public string 随访方式 { get; set; }

		/// <summary>
		/// 随访方式其他
		/// </summary>
		[DisplayName("随访方式其他")]
		public string 随访方式其他 { get; set; }

		/// <summary>
		/// 目前症状
		/// </summary>
		[DisplayName("目前症状")]
		public string 目前症状 { get; set; }

		/// <summary>
		/// 目前症状其他
		/// </summary>
		[DisplayName("目前症状其他")]
		public string 目前症状其他 { get; set; }

		/// <summary>
		/// T_YDS
		/// </summary>
		[DisplayName("T_YDS")]
		public string T_YDS { get; set; }

		/// <summary>
		/// T_YDSYL
		/// </summary>
		[DisplayName("T_YDSYL")]
		public string T_YDSYL { get; set; }

		/// <summary>
		/// T_KFJTY
		/// </summary>
		[DisplayName("T_KFJTY")]
		public string T_KFJTY { get; set; }

		/// <summary>
		/// T_YYMC1
		/// </summary>
		[DisplayName("T_YYMC1")]
		public string T_YYMC1 { get; set; }

		/// <summary>
		/// T_YF1
		/// </summary>
		[DisplayName("T_YF1")]
		public string T_YF1 { get; set; }

		/// <summary>
		/// T_YYMC2
		/// </summary>
		[DisplayName("T_YYMC2")]
		public string T_YYMC2 { get; set; }

		/// <summary>
		/// T_YF2
		/// </summary>
		[DisplayName("T_YF2")]
		public string T_YF2 { get; set; }

		/// <summary>
		/// T_YYMC3
		/// </summary>
		[DisplayName("T_YYMC3")]
		public string T_YYMC3 { get; set; }

		/// <summary>
		/// T_YF3
		/// </summary>
		[DisplayName("T_YF3")]
		public string T_YF3 { get; set; }

		/// <summary>
		/// 服药依从性
		/// </summary>
		[DisplayName("服药依从性")]
		public string 服药依从性 { get; set; }

		/// <summary>
		/// 药物副作用
		/// </summary>
		[DisplayName("药物副作用")]
		public string 药物副作用 { get; set; }

		/// <summary>
		/// 副作用详述
		/// </summary>
		[DisplayName("副作用详述")]
		public string 副作用详述 { get; set; }

		/// <summary>
		/// T_ZZYY
		/// </summary>
		[DisplayName("T_ZZYY")]
		public string T_ZZYY { get; set; }

		/// <summary>
		/// 转诊科别
		/// </summary>
		[DisplayName("转诊科别")]
		public string 转诊科别 { get; set; }

		/// <summary>
		/// 转诊原因
		/// </summary>
		[DisplayName("转诊原因")]
		public string 转诊原因 { get; set; }

		/// <summary>
		/// T_FYWZLCS
		/// </summary>
		[DisplayName("T_FYWZLCS")]
		public string T_FYWZLCS { get; set; }

		/// <summary>
		/// 本次随访分类
		/// </summary>
		[DisplayName("本次随访分类")]
		public string 本次随访分类 { get; set; }

		/// <summary>
		/// 随访医生建议
		/// </summary>
		[DisplayName("随访医生建议")]
		public string 随访医生建议 { get; set; }

		/// <summary>
		/// 随访医生
		/// </summary>
		[DisplayName("随访医生")]
		public string 随访医生 { get; set; }

		/// <summary>
		/// 创建机构
		/// </summary>
		[DisplayName("创建机构")]
		public string 创建机构 { get; set; }

		/// <summary>
		/// 创建时间
		/// </summary>
		[DisplayName("创建时间")]
		public string 创建时间 { get; set; }

		/// <summary>
		/// 创建人
		/// </summary>
		[DisplayName("创建人")]
		public string 创建人 { get; set; }

		/// <summary>
		/// 修改时间
		/// </summary>
		[DisplayName("修改时间")]
		public string 修改时间 { get; set; }

		/// <summary>
		/// 修改人
		/// </summary>
		[DisplayName("修改人")]
		public string 修改人 { get; set; }

		/// <summary>
		/// 发生时间
		/// </summary>
		[DisplayName("发生时间")]
		public string 发生时间 { get; set; }

		/// <summary>
		/// 所属机构
		/// </summary>
		[DisplayName("所属机构")]
		public string 所属机构 { get; set; }

		/// <summary>
		/// T_BFZQK
		/// </summary>
		[DisplayName("T_BFZQK")]
		public string T_BFZQK { get; set; }

		/// <summary>
		/// M_NWLDB
		/// </summary>
		[DisplayName("M_NWLDB")]
		public string M_NWLDB { get; set; }

		/// <summary>
		/// D_PYJM
		/// </summary>
		[DisplayName("D_PYJM")]
		public string D_PYJM { get; set; }

		/// <summary>
		/// 下次随访时间
		/// </summary>
		[DisplayName("下次随访时间")]
		public string 下次随访时间 { get; set; }

		/// <summary>
		/// 适合主食
		/// </summary>
		[DisplayName("适合主食")]
		public string 适合主食 { get; set; }

		/// <summary>
		/// 心理调整
		/// </summary>
		[DisplayName("心理调整")]
		public string 心理调整 { get; set; }

		/// <summary>
		/// 遵医行为
		/// </summary>
		[DisplayName("遵医行为")]
		public string 遵医行为 { get; set; }

		/// <summary>
		/// 足背动脉搏动
		/// </summary>
		[DisplayName("足背动脉搏动")]
		public string 足背动脉搏动 { get; set; }

		/// <summary>
		/// 体征其他
		/// </summary>
		[DisplayName("体征其他")]
		public string 体征其他 { get; set; }

		/// <summary>
		/// 辅助检查日期
		/// </summary>
		[DisplayName("辅助检查日期")]
		public string 辅助检查日期 { get; set; }

		/// <summary>
		/// D_YDS
		/// </summary>
		[DisplayName("D_YDS")]
		public string D_YDS { get; set; }

		/// <summary>
		/// D_YDSYF
		/// </summary>
		[DisplayName("D_YDSYF")]
		public string D_YDSYF { get; set; }

		/// <summary>
		/// 低血糖反应
		/// </summary>
		[DisplayName("低血糖反应")]
		public string 低血糖反应 { get; set; }

		/// <summary>
		/// 缺项
		/// </summary>
		[DisplayName("缺项")]
		public string 缺项 { get; set; }

		/// <summary>
		/// 慢病并发症
		/// </summary>
		[DisplayName("慢病并发症")]
		public string 慢病并发症 { get; set; }

		/// <summary>
		/// 降压药
		/// </summary>
		[DisplayName("降压药")]
		public string 降压药 { get; set; }

		/// <summary>
		/// 转诊情况
		/// </summary>
		[DisplayName("转诊情况")]
		public string 转诊情况 { get; set; }

		/// <summary>
		/// 完整度
		/// </summary>
		[DisplayName("完整度")]
		public string 完整度 { get; set; }

		/// <summary>
		/// SFCS
		/// </summary>
		[DisplayName("SFCS")]
		public string SFCS { get; set; }

		/// <summary>
		/// 体重2
		/// </summary>
		[DisplayName("体重2")]
		public decimal? 体重2 { get; set; }

		/// <summary>
		/// BMI2
		/// </summary>
		[DisplayName("BMI2")]
		public decimal? BMI2 { get; set; }

		/// <summary>
		/// 吸烟数量2
		/// </summary>
		[DisplayName("吸烟数量2")]
		public int? 吸烟数量2 { get; set; }

		/// <summary>
		/// 饮酒数量2
		/// </summary>
		[DisplayName("饮酒数量2")]
		public decimal? 饮酒数量2 { get; set; }

		/// <summary>
		/// 运动频率2
		/// </summary>
		[DisplayName("运动频率2")]
		public int? 运动频率2 { get; set; }

		/// <summary>
		/// 运动持续时间2
		/// </summary>
		[DisplayName("运动持续时间2")]
		public decimal? 运动持续时间2 { get; set; }

		/// <summary>
		/// 适合主食2
		/// </summary>
		[DisplayName("适合主食2")]
		public string 适合主食2 { get; set; }

		/// <summary>
		/// 随机血糖
		/// </summary>
		[DisplayName("随机血糖")]
		public string 随机血糖 { get; set; }

		/// <summary>
		/// 餐后2h血糖
		/// </summary>
		[DisplayName("餐后2h血糖")]
		public string 餐后2h血糖 { get; set; }

		/// <summary>
		/// 下一步管理措施
		/// </summary>
		[DisplayName("下一步管理措施")]
		public string 下一步管理措施 { get; set; }

		/// <summary>
		/// 转诊联系人
		/// </summary>
		[DisplayName("转诊联系人")]
		public string 转诊联系人 { get; set; }

		/// <summary>
		/// 转诊联系电话
		/// </summary>
		[DisplayName("转诊联系电话")]
		public string 转诊联系电话 { get; set; }

		/// <summary>
		/// 居民签名
		/// </summary>
		[DisplayName("居民签名")]
		public string 居民签名 { get; set; }

		/// <summary>
		/// 备注
		/// </summary>
		[DisplayName("备注")]
		public string 备注 { get; set; }

		/// <summary>
		/// 转诊结果
		/// </summary>
		[DisplayName("转诊结果")]
		public string 转诊结果 { get; set; }

		/// <summary>
		/// 胰岛素种类
		/// </summary>
		[DisplayName("胰岛素种类")]
		public string 胰岛素种类 { get; set; }

		/// <summary>
		/// 胰岛素用法用量
		/// </summary>
		[DisplayName("胰岛素用法用量")]
		public string 胰岛素用法用量 { get; set; }

		/// <summary>
		/// 调整胰岛素种类
		/// </summary>
		[DisplayName("调整胰岛素种类")]
		public string 调整胰岛素种类 { get; set; }

		/// <summary>
		/// 调整胰岛素用法用量
		/// </summary>
		[DisplayName("调整胰岛素用法用量")]
		public string 调整胰岛素用法用量 { get; set; }

		/// <summary>
		/// 用药调整意见
		/// </summary>
		[DisplayName("用药调整意见")]
		public string 用药调整意见 { get; set; }

		/// <summary>
		/// 访视情况
		/// </summary>
		[DisplayName("访视情况")]
		public string 访视情况 { get; set; }

		/// <summary>
		/// 失访原因
		/// </summary>
		[DisplayName("失访原因")]
		public string 失访原因 { get; set; }

		/// <summary>
		/// 死亡日期
		/// </summary>
		[DisplayName("死亡日期")]
		public string 死亡日期 { get; set; }

		/// <summary>
		/// 死亡原因
		/// </summary>
		[DisplayName("死亡原因")]
		public string 死亡原因 { get; set; }

        #endregion Model
    }
}
