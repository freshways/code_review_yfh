﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DotNet.Model
{
	///名称：tb_MXB高血压随访表
	///作者：yufh 
	///创建时间：2020-09-23 17:30 
	///功能描述：tb_MXB高血压随访表	实体类
	[Serializable]
	public class tb_MXB高血压随访表
	{
		#region Model

		/// <summary>
		/// ID
		/// </summary>
		[DisplayName("ID")]
		public long ID { get; set; }

		/// <summary>
		/// 个人档案编号
		/// </summary>
		[DisplayName("个人档案编号")]
		public string 个人档案编号 { get; set; }

		/// <summary>
		/// 姓名
		/// </summary>
		[DisplayName("姓名")]
		public string 姓名 { get; set; }

		/// <summary>
		/// 性别
		/// </summary>
		[DisplayName("性别")]
		public string 性别 { get; set; }

		/// <summary>
		/// 身份证号
		/// </summary>
		[DisplayName("身份证号")]
		public string 身份证号 { get; set; }

		/// <summary>
		/// 出生日期
		/// </summary>
		[DisplayName("出生日期")]
		public string 出生日期 { get; set; }

		/// <summary>
		/// 民族
		/// </summary>
		[DisplayName("民族")]
		public string 民族 { get; set; }

		/// <summary>
		/// 文化程度
		/// </summary>
		[DisplayName("文化程度")]
		public string 文化程度 { get; set; }

		/// <summary>
		/// 常住类型
		/// </summary>
		[DisplayName("常住类型")]
		public string 常住类型 { get; set; }

		/// <summary>
		/// 邮箱
		/// </summary>
		[DisplayName("邮箱")]
		public string 邮箱 { get; set; }

		/// <summary>
		/// 工作单位
		/// </summary>
		[DisplayName("工作单位")]
		public string 工作单位 { get; set; }

		/// <summary>
		/// 婚姻状况
		/// </summary>
		[DisplayName("婚姻状况")]
		public string 婚姻状况 { get; set; }

		/// <summary>
		/// 职业
		/// </summary>
		[DisplayName("职业")]
		public string 职业 { get; set; }

		/// <summary>
		/// 联系电话
		/// </summary>
		[DisplayName("联系电话")]
		public string 联系电话 { get; set; }

		/// <summary>
		/// 省
		/// </summary>
		[DisplayName("省")]
		public string 省 { get; set; }

		/// <summary>
		/// 市
		/// </summary>
		[DisplayName("市")]
		public string 市 { get; set; }

		/// <summary>
		/// 区
		/// </summary>
		[DisplayName("区")]
		public string 区 { get; set; }

		/// <summary>
		/// 街道
		/// </summary>
		[DisplayName("街道")]
		public string 街道 { get; set; }

		/// <summary>
		/// 居委会
		/// </summary>
		[DisplayName("居委会")]
		public string 居委会 { get; set; }

		/// <summary>
		/// 所属片区
		/// </summary>
		[DisplayName("所属片区")]
		public string 所属片区 { get; set; }

		/// <summary>
		/// 居住地址
		/// </summary>
		[DisplayName("居住地址")]
		public string 居住地址 { get; set; }

		/// <summary>
		/// 医疗费支付类型
		/// </summary>
		[DisplayName("医疗费支付类型")]
		public string 医疗费支付类型 { get; set; }

		/// <summary>
		/// 医疗保险号
		/// </summary>
		[DisplayName("医疗保险号")]
		public string 医疗保险号 { get; set; }

		/// <summary>
		/// 新农合号
		/// </summary>
		[DisplayName("新农合号")]
		public string 新农合号 { get; set; }

		/// <summary>
		/// 建档机构
		/// </summary>
		[DisplayName("建档机构")]
		public string 建档机构 { get; set; }

		/// <summary>
		/// 建档时间
		/// </summary>
		[DisplayName("建档时间")]
		public string 建档时间 { get; set; }

		/// <summary>
		/// 建档人
		/// </summary>
		[DisplayName("建档人")]
		public string 建档人 { get; set; }

		/// <summary>
		/// 吸烟数量
		/// </summary>
		[DisplayName("吸烟数量")]
		public int? 吸烟数量 { get; set; }

		/// <summary>
		/// 饮酒数量
		/// </summary>
		[DisplayName("饮酒数量")]
		public decimal? 饮酒数量 { get; set; }

		/// <summary>
		/// 运动频率
		/// </summary>
		[DisplayName("运动频率")]
		public int? 运动频率 { get; set; }

		/// <summary>
		/// 运动持续时间
		/// </summary>
		[DisplayName("运动持续时间")]
		public decimal? 运动持续时间 { get; set; }

		/// <summary>
		/// 身高
		/// </summary>
		[DisplayName("身高")]
		public decimal? 身高 { get; set; }

		/// <summary>
		/// 体重
		/// </summary>
		[DisplayName("体重")]
		public decimal? 体重 { get; set; }

		/// <summary>
		/// BMI
		/// </summary>
		[DisplayName("BMI")]
		public decimal? BMI { get; set; }

		/// <summary>
		/// 腰围
		/// </summary>
		[DisplayName("腰围")]
		public decimal? 腰围 { get; set; }

		/// <summary>
		/// 收缩压
		/// </summary>
		[DisplayName("收缩压")]
		public int? 收缩压 { get; set; }

		/// <summary>
		/// 舒张压
		/// </summary>
		[DisplayName("舒张压")]
		public int? 舒张压 { get; set; }

		/// <summary>
		/// 空腹血糖
		/// </summary>
		[DisplayName("空腹血糖")]
		public decimal? 空腹血糖 { get; set; }

		/// <summary>
		/// 餐后血糖
		/// </summary>
		[DisplayName("餐后血糖")]
		public decimal? 餐后血糖 { get; set; }

		/// <summary>
		/// 高密度蛋白
		/// </summary>
		[DisplayName("高密度蛋白")]
		public decimal? 高密度蛋白 { get; set; }

		/// <summary>
		/// 低密度蛋白
		/// </summary>
		[DisplayName("低密度蛋白")]
		public decimal? 低密度蛋白 { get; set; }

		/// <summary>
		/// 甘油三脂
		/// </summary>
		[DisplayName("甘油三脂")]
		public decimal? 甘油三脂 { get; set; }

		/// <summary>
		/// 总胆固醇
		/// </summary>
		[DisplayName("总胆固醇")]
		public decimal? 总胆固醇 { get; set; }

		/// <summary>
		/// EKG
		/// </summary>
		[DisplayName("EKG")]
		public string EKG { get; set; }

		/// <summary>
		/// 随访方式
		/// </summary>
		[DisplayName("随访方式")]
		public string 随访方式 { get; set; }

		/// <summary>
		/// 随访方式其他
		/// </summary>
		[DisplayName("随访方式其他")]
		public string 随访方式其他 { get; set; }

		/// <summary>
		/// 目前症状
		/// </summary>
		[DisplayName("目前症状")]
		public string 目前症状 { get; set; }

		/// <summary>
		/// 目前症状其他
		/// </summary>
		[DisplayName("目前症状其他")]
		public string 目前症状其他 { get; set; }

		/// <summary>
		/// G_BFZQK
		/// </summary>
		[DisplayName("G_BFZQK")]
		public string G_BFZQK { get; set; }

		/// <summary>
		/// 降压药
		/// </summary>
		[DisplayName("降压药")]
		public string 降压药 { get; set; }

		/// <summary>
		/// 药物名称1
		/// </summary>
		[DisplayName("药物名称1")]
		public string 药物名称1 { get; set; }

		/// <summary>
		/// 使用方法1
		/// </summary>
		[DisplayName("使用方法1")]
		public string 使用方法1 { get; set; }

		/// <summary>
		/// 药物名称2
		/// </summary>
		[DisplayName("药物名称2")]
		public string 药物名称2 { get; set; }

		/// <summary>
		/// 使用方法2
		/// </summary>
		[DisplayName("使用方法2")]
		public string 使用方法2 { get; set; }

		/// <summary>
		/// 药物名称3
		/// </summary>
		[DisplayName("药物名称3")]
		public string 药物名称3 { get; set; }

		/// <summary>
		/// 使用方法3
		/// </summary>
		[DisplayName("使用方法3")]
		public string 使用方法3 { get; set; }

		/// <summary>
		/// 服药依从性
		/// </summary>
		[DisplayName("服药依从性")]
		public string 服药依从性 { get; set; }

		/// <summary>
		/// 药物副作用
		/// </summary>
		[DisplayName("药物副作用")]
		public string 药物副作用 { get; set; }

		/// <summary>
		/// G_ZZYY
		/// </summary>
		[DisplayName("G_ZZYY")]
		public string G_ZZYY { get; set; }

		/// <summary>
		/// 转诊科别
		/// </summary>
		[DisplayName("转诊科别")]
		public string 转诊科别 { get; set; }

		/// <summary>
		/// 转诊原因
		/// </summary>
		[DisplayName("转诊原因")]
		public string 转诊原因 { get; set; }

		/// <summary>
		/// G_FYYZLCS
		/// </summary>
		[DisplayName("G_FYYZLCS")]
		public string G_FYYZLCS { get; set; }

		/// <summary>
		/// 本次随访分类
		/// </summary>
		[DisplayName("本次随访分类")]
		public string 本次随访分类 { get; set; }

		/// <summary>
		/// 随访医生建议
		/// </summary>
		[DisplayName("随访医生建议")]
		public string 随访医生建议 { get; set; }

		/// <summary>
		/// 随访医生
		/// </summary>
		[DisplayName("随访医生")]
		public string 随访医生 { get; set; }

		/// <summary>
		/// 创建机构
		/// </summary>
		[DisplayName("创建机构")]
		public string 创建机构 { get; set; }

		/// <summary>
		/// 创建时间
		/// </summary>
		[DisplayName("创建时间")]
		public string 创建时间 { get; set; }

		/// <summary>
		/// 创建人
		/// </summary>
		[DisplayName("创建人")]
		public string 创建人 { get; set; }

		/// <summary>
		/// 修改时间
		/// </summary>
		[DisplayName("修改时间")]
		public string 修改时间 { get; set; }

		/// <summary>
		/// 修改人
		/// </summary>
		[DisplayName("修改人")]
		public string 修改人 { get; set; }

		/// <summary>
		/// 发生时间
		/// </summary>
		[DisplayName("发生时间")]
		public string 发生时间 { get; set; }

		/// <summary>
		/// 所属机构
		/// </summary>
		[DisplayName("所属机构")]
		public string 所属机构 { get; set; }

		/// <summary>
		/// 副作用详述
		/// </summary>
		[DisplayName("副作用详述")]
		public string 副作用详述 { get; set; }

		/// <summary>
		/// G_BFZQKQT
		/// </summary>
		[DisplayName("G_BFZQKQT")]
		public string G_BFZQKQT { get; set; }

		/// <summary>
		/// D_PYJM
		/// </summary>
		[DisplayName("D_PYJM")]
		public string D_PYJM { get; set; }

		/// <summary>
		/// 下次随访时间
		/// </summary>
		[DisplayName("下次随访时间")]
		public string 下次随访时间 { get; set; }

		/// <summary>
		/// 心理调整
		/// </summary>
		[DisplayName("心理调整")]
		public string 心理调整 { get; set; }

		/// <summary>
		/// 遵医行为
		/// </summary>
		[DisplayName("遵医行为")]
		public string 遵医行为 { get; set; }

		/// <summary>
		/// 摄盐情况
		/// </summary>
		[DisplayName("摄盐情况")]
		public string 摄盐情况 { get; set; }

		/// <summary>
		/// 体征其他
		/// </summary>
		[DisplayName("体征其他")]
		public string 体征其他 { get; set; }

		/// <summary>
		/// G_YYQT
		/// </summary>
		[DisplayName("G_YYQT")]
		public string G_YYQT { get; set; }

		/// <summary>
		/// G_SYQT
		/// </summary>
		[DisplayName("G_SYQT")]
		public string G_SYQT { get; set; }

		/// <summary>
		/// 辅助检查
		/// </summary>
		[DisplayName("辅助检查")]
		public string 辅助检查 { get; set; }

		/// <summary>
		/// G_DXL
		/// </summary>
		[DisplayName("G_DXL")]
		public string G_DXL { get; set; }

		/// <summary>
		/// 心率
		/// </summary>
		[DisplayName("心率")]
		public string 心率 { get; set; }

		/// <summary>
		/// 缺项
		/// </summary>
		[DisplayName("缺项")]
		public string 缺项 { get; set; }

		/// <summary>
		/// 慢病并发症
		/// </summary>
		[DisplayName("慢病并发症")]
		public string 慢病并发症 { get; set; }

		/// <summary>
		/// 转诊情况
		/// </summary>
		[DisplayName("转诊情况")]
		public string 转诊情况 { get; set; }

		/// <summary>
		/// 完整度
		/// </summary>
		[DisplayName("完整度")]
		public string 完整度 { get; set; }

		/// <summary>
		/// SFCS
		/// </summary>
		[DisplayName("SFCS")]
		public string SFCS { get; set; }

		/// <summary>
		/// 体重2
		/// </summary>
		[DisplayName("体重2")]
		public decimal? 体重2 { get; set; }

		/// <summary>
		/// BMI2
		/// </summary>
		[DisplayName("BMI2")]
		public decimal? BMI2 { get; set; }

		/// <summary>
		/// 吸烟数量2
		/// </summary>
		[DisplayName("吸烟数量2")]
		public int? 吸烟数量2 { get; set; }

		/// <summary>
		/// 饮酒数量2
		/// </summary>
		[DisplayName("饮酒数量2")]
		public decimal? 饮酒数量2 { get; set; }

		/// <summary>
		/// 运动频率2
		/// </summary>
		[DisplayName("运动频率2")]
		public int? 运动频率2 { get; set; }

		/// <summary>
		/// 运动持续时间2
		/// </summary>
		[DisplayName("运动持续时间2")]
		public decimal? 运动持续时间2 { get; set; }

		/// <summary>
		/// 摄盐情况2
		/// </summary>
		[DisplayName("摄盐情况2")]
		public string 摄盐情况2 { get; set; }

		/// <summary>
		/// 下一步管理措施
		/// </summary>
		[DisplayName("下一步管理措施")]
		public string 下一步管理措施 { get; set; }

		/// <summary>
		/// 转诊联系人
		/// </summary>
		[DisplayName("转诊联系人")]
		public string 转诊联系人 { get; set; }

		/// <summary>
		/// 转诊联系电话
		/// </summary>
		[DisplayName("转诊联系电话")]
		public string 转诊联系电话 { get; set; }

		/// <summary>
		/// 居民签名
		/// </summary>
		[DisplayName("居民签名")]
		public string 居民签名 { get; set; }

		/// <summary>
		/// 备注
		/// </summary>
		[DisplayName("备注")]
		public string 备注 { get; set; }

		/// <summary>
		/// 转诊结果
		/// </summary>
		[DisplayName("转诊结果")]
		public string 转诊结果 { get; set; }

		/// <summary>
		/// 用药调整意见
		/// </summary>
		[DisplayName("用药调整意见")]
		public string 用药调整意见 { get; set; }

		/// <summary>
		/// 访视情况
		/// </summary>
		[DisplayName("访视情况")]
		public string 访视情况 { get; set; }

		/// <summary>
		/// 失访原因
		/// </summary>
		[DisplayName("失访原因")]
		public string 失访原因 { get; set; }

		/// <summary>
		/// 死亡日期
		/// </summary>
		[DisplayName("死亡日期")]
		public string 死亡日期 { get; set; }

		/// <summary>
		/// 死亡原因
		/// </summary>
		[DisplayName("死亡原因")]
		public string 死亡原因 { get; set; }

        #endregion Model
    }
}
