﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DotNet.Model
{
	///名称：InterFace_MB_RGKEY
	///作者：yufh 
	///创建时间：2020-09-22 15:47 
	///功能描述：InterFace_MB_RGKEY	实体类
	[Serializable]
	public class InterFace_MB_RGKEY
	{
		#region Model

		/// <summary>
		/// ID
		/// </summary>
		[DisplayName("ID")]
		public int ID { get; set; }

		/// <summary>
		/// rgcode
		/// </summary>
		[DisplayName("rgcode")]
		public string rgcode { get; set; }

		/// <summary>
		/// rgname
		/// </summary>
		[DisplayName("rgname")]
		public string rgname { get; set; }

		/// <summary>
		/// appKey
		/// </summary>
		[DisplayName("appKey")]
		public string appKey { get; set; }

		/// <summary>
		/// secret
		/// </summary>
		[DisplayName("secret")]
		public string secret { get; set; }

		/// <summary>
		/// liscode
		/// </summary>
		[DisplayName("liscode")]
		public string liscode { get; set; }
		#endregion Model
	}
}
