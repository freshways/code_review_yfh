﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNet.Model
{
    public class tb_健康档案2
    {
        /// <summary>
        /// 
        /// </summary>
        public string 个人档案编号 { get; set; }
        /// <summary>
        /// 测试
        /// </summary>
        public string 姓名 { get; set; }
        /// <summary>
        /// 女
        /// </summary>
        public string 性别 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string 出生日期 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string 身份证号 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string 本人电话 { get; set; }
        /// <summary>
        /// 沂水县夏蔚镇东村
        /// </summary>
        public string 居住地址 { get; set; }
        /// <summary>
        /// 高血压
        /// </summary>
        public string 是否高血压 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string 高血压下次随访时间 { get; set; }
        /// <summary>
        /// 糖尿病
        /// </summary>
        public string 是否糖尿病 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string 糖尿病下次随访时间 { get; set; }
    }
}
