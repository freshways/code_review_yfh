﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DotNet.Model
{
	///名称：tb_机构信息
	///作者：yufh 
	///创建时间：2020-09-22 08:41 
	///功能描述：tb_机构信息	实体类
	[Serializable]
	public class tb_机构信息
	{
		#region Model

		/// <summary>
		/// ID
		/// </summary>
		[DisplayName("ID")]
		public long ID { get; set; }

		/// <summary>
		/// 机构编号
		/// </summary>
		[DisplayName("机构编号")]
		public string 机构编号 { get; set; }

		/// <summary>
		/// 机构名称
		/// </summary>
		[DisplayName("机构名称")]
		public string 机构名称 { get; set; }

		/// <summary>
		/// P_RGID_MARKER
		/// </summary>
		[DisplayName("P_RGID_MARKER")]
		public string P_RGID_MARKER { get; set; }

		/// <summary>
		/// 负责人
		/// </summary>
		[DisplayName("负责人")]
		public string 负责人 { get; set; }

		/// <summary>
		/// 联系人
		/// </summary>
		[DisplayName("联系人")]
		public string 联系人 { get; set; }

		/// <summary>
		/// 联系电话
		/// </summary>
		[DisplayName("联系电话")]
		public string 联系电话 { get; set; }

		/// <summary>
		/// 上级机构
		/// </summary>
		[DisplayName("上级机构")]
		public string 上级机构 { get; set; }

		/// <summary>
		/// 机构级别
		/// </summary>
		[DisplayName("机构级别")]
		public string 机构级别 { get; set; }

		/// <summary>
		/// 创建人
		/// </summary>
		[DisplayName("创建人")]
		public string 创建人 { get; set; }

		/// <summary>
		/// 创建时间
		/// </summary>
		[DisplayName("创建时间")]
		public DateTime? 创建时间 { get; set; }

		/// <summary>
		/// 状态
		/// </summary>
		[DisplayName("状态")]
		public string 状态 { get; set; }

		/// <summary>
		/// P_QYDW
		/// </summary>
		[DisplayName("P_QYDW")]
		public string P_QYDW { get; set; }

		/// <summary>
		/// 辖区人口
		/// </summary>
		[DisplayName("辖区人口")]
		public string 辖区人口 { get; set; }
		#endregion Model
	}
}
