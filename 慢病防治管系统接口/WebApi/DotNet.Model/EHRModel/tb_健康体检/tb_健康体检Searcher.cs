﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNet.Model
{
    public class tb_健康体检Searcher : tb_健康档案Searcher
    {
    }

    public class tb_健康体检Res
    {
        /// <summary>
        /// 患者身份证
        /// </summary>
        public string idCard { get; set; }
        /// <summary>
        /// 公卫健康档案编号
        /// </summary>
        public string customerId { get; set; }
        /// <summary>
        /// 症状
        /// </summary>
        public string curSymptom { get; set; }
        /// <summary>
        /// 身高
        /// </summary>
        public string height { get; set; }
        /// <summary>
        /// 体重
        /// </summary>
        public string weight { get; set; }
        /// <summary>
        /// 腰围
        /// </summary>
        public string waist { get; set; }
        /// <summary>
        /// 臀围
        /// </summary>
        public string hipLine { get; set; }
        /// <summary>
        /// 锻炼频率
        /// </summary>
        public string exerciseFre { get; set; }
        /// <summary>
        /// 每次锻炼时间
        /// </summary>
        public string exerciseTime { get; set; }
        /// <summary>
        /// 锻炼方式
        /// </summary>
        public string exerciseType { get; set; }
        /// <summary>
        /// 饮食习惯
        /// </summary>
        public string diet { get; set; }
        /// <summary>
        /// 吸烟状况
        /// </summary>
        public string smokeStatus { get; set; }
        /// <summary>
        /// 饮酒频率
        /// </summary>
        public string drinkStatus { get; set; }
        /// <summary>
        /// 眼底异常
        /// </summary>
        public string eyeAbnormal { get; set; }
        /// <summary>
        /// 脉搏
        /// </summary>
        public string pulse { get; set; }
        /// <summary>
        /// 心律
        /// </summary>
        public string heartrate { get; set; }
        /// <summary>
        /// 下肢水肿
        /// </summary>
        public string legEdema { get; set; }
        /// <summary>
        /// 足背动脉搏动
        /// </summary>
        public string foodDorsum { get; set; }
        /// <summary>
        /// 空腹血糖值
        /// </summary>
        public string fbg { get; set; }
        /// <summary>
        /// 餐后2小时血糖值
        /// </summary>
        public string sugar2H { get; set; }
        /// <summary>
        /// 收缩压
        /// </summary>
        public string sbp { get; set; }
        /// <summary>
        /// 舒张压
        /// </summary>
        public string dbp { get; set; }

        /// <summary>
        /// 现存主要健康问题
        /// </summary>
        public List<healthList> healthList { get; set; }

        /// <summary>
        /// 主要用药情况
        /// </summary>
        public List<drugList> drugList { get; set; }
    }

    /// <summary>
    /// 现存主要健康问题
    /// </summary>
    public class healthList
    {
        /// <summary>
        /// 脑血管疾病
        /// </summary>
        public string brainDisease { get; set; }
        /// <summary>
        /// 肾脏疾病
        /// </summary>
        public string kidneyDisease { get; set; }
        /// <summary>
        /// 心血管疾病
        /// </summary>
        public string cardioDisease { get; set; }
        /// <summary>
        /// 眼部疾病
        /// </summary>
        public string eyeDisease { get; set; }
        /// <summary>
        /// 神经系统其他疾病
        /// </summary>
        public string nervousDisease { get; set; }
        /// <summary>
        /// 其他系统疾病
        /// </summary>
        public string otherDisease { get; set; }
    }

    /// <summary>
    /// 主要用药情况
    /// </summary>
    public class drugList
    {
        /// <summary>
        /// 药物名称
        /// </summary>
        public string drugName { get; set; }
        /// <summary>
        /// 用法
        /// </summary>
        public string drugUse { get; set; }
        /// <summary>
        /// 用量
        /// </summary>
        public string drugDose { get; set; }
        /// <summary>
        /// 用药时间
        /// </summary>
        public string drugDate { get; set; }
        /// <summary>
        /// 服药依从性
        /// </summary>
        public string drugCompliance { get; set; }

    }
}
