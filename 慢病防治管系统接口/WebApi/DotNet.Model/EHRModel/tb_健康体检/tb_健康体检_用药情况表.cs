﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DotNet.Model
{
	///名称：tb_健康体检_用药情况表
	///作者：yufh 
	///创建时间：2020-09-23 09:29 
	///功能描述：tb_健康体检_用药情况表	实体类
	[Serializable]
	public class tb_健康体检_用药情况表
	{
		#region Model

		/// <summary>
		/// ID
		/// </summary>
		[DisplayName("ID")]
		public long ID { get; set; }

		/// <summary>
		/// 个人档案编号
		/// </summary>
		[DisplayName("个人档案编号")]
		public string 个人档案编号 { get; set; }

		/// <summary>
		/// 药物名称
		/// </summary>
		[DisplayName("药物名称")]
		public string 药物名称 { get; set; }

		/// <summary>
		/// 用法
		/// </summary>
		[DisplayName("用法")]
		public string 用法 { get; set; }

		/// <summary>
		/// 用量
		/// </summary>
		[DisplayName("用量")]
		public string 用量 { get; set; }

		/// <summary>
		/// 用药时间
		/// </summary>
		[DisplayName("用药时间")]
		public string 用药时间 { get; set; }

		/// <summary>
		/// 服药依从性
		/// </summary>
		[DisplayName("服药依从性")]
		public string 服药依从性 { get; set; }

		/// <summary>
		/// 创建时间
		/// </summary>
		[DisplayName("创建时间")]
		public string 创建时间 { get; set; }
		#endregion Model
	}
}
