﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DotNet.Model
{
	///名称：tb_健康体检
	///作者：yufh 
	///创建时间：2020-09-23 09:29 
	///功能描述：tb_健康体检	实体类
	[Serializable]
	public class tb_健康体检
	{
		#region Model

		/// <summary>
		/// ID
		/// </summary>
		[DisplayName("ID")]
		public long ID { get; set; }

		/// <summary>
		/// 症状
		/// </summary>
		[DisplayName("症状")]
		public string 症状 { get; set; }

		/// <summary>
		/// 体温
		/// </summary>
		[DisplayName("体温")]
		public decimal? 体温 { get; set; }

		/// <summary>
		/// 呼吸
		/// </summary>
		[DisplayName("呼吸")]
		public int? 呼吸 { get; set; }

		/// <summary>
		/// 脉搏
		/// </summary>
		[DisplayName("脉搏")]
		public int? 脉搏 { get; set; }

		/// <summary>
		/// 血压右侧1
		/// </summary>
		[DisplayName("血压右侧1")]
		public string 血压右侧1 { get; set; }

		/// <summary>
		/// 血压右侧2
		/// </summary>
		[DisplayName("血压右侧2")]
		public string 血压右侧2 { get; set; }

		/// <summary>
		/// 血压左侧1
		/// </summary>
		[DisplayName("血压左侧1")]
		public string 血压左侧1 { get; set; }

		/// <summary>
		/// 血压左侧2
		/// </summary>
		[DisplayName("血压左侧2")]
		public string 血压左侧2 { get; set; }

		/// <summary>
		/// 身高
		/// </summary>
		[DisplayName("身高")]
		public decimal? 身高 { get; set; }

		/// <summary>
		/// 腰围
		/// </summary>
		[DisplayName("腰围")]
		public decimal? 腰围 { get; set; }

		/// <summary>
		/// 体重
		/// </summary>
		[DisplayName("体重")]
		public decimal? 体重 { get; set; }

		/// <summary>
		/// 体重指数
		/// </summary>
		[DisplayName("体重指数")]
		public string 体重指数 { get; set; }

		/// <summary>
		/// 老年人认知
		/// </summary>
		[DisplayName("老年人认知")]
		public string 老年人认知 { get; set; }

		/// <summary>
		/// 老年人情感
		/// </summary>
		[DisplayName("老年人情感")]
		public string 老年人情感 { get; set; }

		/// <summary>
		/// 老年人认知分
		/// </summary>
		[DisplayName("老年人认知分")]
		public string 老年人认知分 { get; set; }

		/// <summary>
		/// 老年人情感分
		/// </summary>
		[DisplayName("老年人情感分")]
		public string 老年人情感分 { get; set; }

		/// <summary>
		/// 左眼视力
		/// </summary>
		[DisplayName("左眼视力")]
		public decimal? 左眼视力 { get; set; }

		/// <summary>
		/// 右眼视力
		/// </summary>
		[DisplayName("右眼视力")]
		public decimal? 右眼视力 { get; set; }

		/// <summary>
		/// 左眼矫正
		/// </summary>
		[DisplayName("左眼矫正")]
		public decimal? 左眼矫正 { get; set; }

		/// <summary>
		/// 右眼矫正
		/// </summary>
		[DisplayName("右眼矫正")]
		public string 右眼矫正 { get; set; }

		/// <summary>
		/// 听力
		/// </summary>
		[DisplayName("听力")]
		public string 听力 { get; set; }

		/// <summary>
		/// 运动功能
		/// </summary>
		[DisplayName("运动功能")]
		public string 运动功能 { get; set; }

		/// <summary>
		/// 皮肤
		/// </summary>
		[DisplayName("皮肤")]
		public string 皮肤 { get; set; }

		/// <summary>
		/// 淋巴结
		/// </summary>
		[DisplayName("淋巴结")]
		public string 淋巴结 { get; set; }

		/// <summary>
		/// 淋巴结其他
		/// </summary>
		[DisplayName("淋巴结其他")]
		public string 淋巴结其他 { get; set; }

		/// <summary>
		/// 桶状胸
		/// </summary>
		[DisplayName("桶状胸")]
		public string 桶状胸 { get; set; }

		/// <summary>
		/// 呼吸音
		/// </summary>
		[DisplayName("呼吸音")]
		public string 呼吸音 { get; set; }

		/// <summary>
		/// 呼吸音异常
		/// </summary>
		[DisplayName("呼吸音异常")]
		public string 呼吸音异常 { get; set; }

		/// <summary>
		/// 罗音
		/// </summary>
		[DisplayName("罗音")]
		public string 罗音 { get; set; }

		/// <summary>
		/// 罗音异常
		/// </summary>
		[DisplayName("罗音异常")]
		public string 罗音异常 { get; set; }

		/// <summary>
		/// 心率
		/// </summary>
		[DisplayName("心率")]
		public int? 心率 { get; set; }

		/// <summary>
		/// 心律
		/// </summary>
		[DisplayName("心律")]
		public string 心律 { get; set; }

		/// <summary>
		/// 杂音
		/// </summary>
		[DisplayName("杂音")]
		public string 杂音 { get; set; }

		/// <summary>
		/// 杂音有
		/// </summary>
		[DisplayName("杂音有")]
		public string 杂音有 { get; set; }

		/// <summary>
		/// 压痛
		/// </summary>
		[DisplayName("压痛")]
		public string 压痛 { get; set; }

		/// <summary>
		/// 压痛有
		/// </summary>
		[DisplayName("压痛有")]
		public string 压痛有 { get; set; }

		/// <summary>
		/// 包块
		/// </summary>
		[DisplayName("包块")]
		public string 包块 { get; set; }

		/// <summary>
		/// 包块有
		/// </summary>
		[DisplayName("包块有")]
		public string 包块有 { get; set; }

		/// <summary>
		/// 肝大
		/// </summary>
		[DisplayName("肝大")]
		public string 肝大 { get; set; }

		/// <summary>
		/// 肝大有
		/// </summary>
		[DisplayName("肝大有")]
		public string 肝大有 { get; set; }

		/// <summary>
		/// 脾大
		/// </summary>
		[DisplayName("脾大")]
		public string 脾大 { get; set; }

		/// <summary>
		/// 脾大有
		/// </summary>
		[DisplayName("脾大有")]
		public string 脾大有 { get; set; }

		/// <summary>
		/// 浊音
		/// </summary>
		[DisplayName("浊音")]
		public string 浊音 { get; set; }

		/// <summary>
		/// 浊音有
		/// </summary>
		[DisplayName("浊音有")]
		public string 浊音有 { get; set; }

		/// <summary>
		/// 下肢水肿
		/// </summary>
		[DisplayName("下肢水肿")]
		public string 下肢水肿 { get; set; }

		/// <summary>
		/// 肛门指诊
		/// </summary>
		[DisplayName("肛门指诊")]
		public string 肛门指诊 { get; set; }

		/// <summary>
		/// 肛门指诊异常
		/// </summary>
		[DisplayName("肛门指诊异常")]
		public string 肛门指诊异常 { get; set; }

		/// <summary>
		/// G_QLX
		/// </summary>
		[DisplayName("G_QLX")]
		public string G_QLX { get; set; }

		/// <summary>
		/// 查体其他
		/// </summary>
		[DisplayName("查体其他")]
		public string 查体其他 { get; set; }

		/// <summary>
		/// 白细胞
		/// </summary>
		[DisplayName("白细胞")]
		public string 白细胞 { get; set; }

		/// <summary>
		/// 血红蛋白
		/// </summary>
		[DisplayName("血红蛋白")]
		public string 血红蛋白 { get; set; }

		/// <summary>
		/// 血小板
		/// </summary>
		[DisplayName("血小板")]
		public string 血小板 { get; set; }

		/// <summary>
		/// 血常规其他
		/// </summary>
		[DisplayName("血常规其他")]
		public string 血常规其他 { get; set; }

		/// <summary>
		/// 尿蛋白
		/// </summary>
		[DisplayName("尿蛋白")]
		public string 尿蛋白 { get; set; }

		/// <summary>
		/// 尿糖
		/// </summary>
		[DisplayName("尿糖")]
		public string 尿糖 { get; set; }

		/// <summary>
		/// 尿酮体
		/// </summary>
		[DisplayName("尿酮体")]
		public string 尿酮体 { get; set; }

		/// <summary>
		/// 尿潜血
		/// </summary>
		[DisplayName("尿潜血")]
		public string 尿潜血 { get; set; }

		/// <summary>
		/// 尿常规其他
		/// </summary>
		[DisplayName("尿常规其他")]
		public string 尿常规其他 { get; set; }

		/// <summary>
		/// 大便潜血
		/// </summary>
		[DisplayName("大便潜血")]
		public string 大便潜血 { get; set; }

		/// <summary>
		/// 血清谷丙转氨酶
		/// </summary>
		[DisplayName("血清谷丙转氨酶")]
		public string 血清谷丙转氨酶 { get; set; }

		/// <summary>
		/// 血清谷草转氨酶
		/// </summary>
		[DisplayName("血清谷草转氨酶")]
		public string 血清谷草转氨酶 { get; set; }

		/// <summary>
		/// 白蛋白
		/// </summary>
		[DisplayName("白蛋白")]
		public string 白蛋白 { get; set; }

		/// <summary>
		/// 总胆红素
		/// </summary>
		[DisplayName("总胆红素")]
		public string 总胆红素 { get; set; }

		/// <summary>
		/// 结合胆红素
		/// </summary>
		[DisplayName("结合胆红素")]
		public string 结合胆红素 { get; set; }

		/// <summary>
		/// 血清肌酐
		/// </summary>
		[DisplayName("血清肌酐")]
		public string 血清肌酐 { get; set; }

		/// <summary>
		/// 血尿素氮
		/// </summary>
		[DisplayName("血尿素氮")]
		public string 血尿素氮 { get; set; }

		/// <summary>
		/// 总胆固醇
		/// </summary>
		[DisplayName("总胆固醇")]
		public string 总胆固醇 { get; set; }

		/// <summary>
		/// 甘油三酯
		/// </summary>
		[DisplayName("甘油三酯")]
		public string 甘油三酯 { get; set; }

		/// <summary>
		/// 血清低密度脂蛋白胆固醇
		/// </summary>
		[DisplayName("血清低密度脂蛋白胆固醇")]
		public string 血清低密度脂蛋白胆固醇 { get; set; }

		/// <summary>
		/// 血清高密度脂蛋白胆固醇
		/// </summary>
		[DisplayName("血清高密度脂蛋白胆固醇")]
		public string 血清高密度脂蛋白胆固醇 { get; set; }

		/// <summary>
		/// 空腹血糖
		/// </summary>
		[DisplayName("空腹血糖")]
		public string 空腹血糖 { get; set; }

		/// <summary>
		/// 乙型肝炎表面抗原
		/// </summary>
		[DisplayName("乙型肝炎表面抗原")]
		public string 乙型肝炎表面抗原 { get; set; }

		/// <summary>
		/// 眼底
		/// </summary>
		[DisplayName("眼底")]
		public string 眼底 { get; set; }

		/// <summary>
		/// 眼底异常
		/// </summary>
		[DisplayName("眼底异常")]
		public string 眼底异常 { get; set; }

		/// <summary>
		/// 心电图
		/// </summary>
		[DisplayName("心电图")]
		public string 心电图 { get; set; }

		/// <summary>
		/// 心电图异常
		/// </summary>
		[DisplayName("心电图异常")]
		public string 心电图异常 { get; set; }

		/// <summary>
		/// 胸部X线片
		/// </summary>
		[DisplayName("胸部X线片")]
		public string 胸部X线片 { get; set; }

		/// <summary>
		/// 胸部X线片异常
		/// </summary>
		[DisplayName("胸部X线片异常")]
		public string 胸部X线片异常 { get; set; }

		/// <summary>
		/// B超
		/// </summary>
		[DisplayName("B超")]
		public string B超 { get; set; }

		/// <summary>
		/// B超其他
		/// </summary>
		[DisplayName("B超其他")]
		public string B超其他 { get; set; }

		/// <summary>
		/// 辅助检查其他
		/// </summary>
		[DisplayName("辅助检查其他")]
		public string 辅助检查其他 { get; set; }

		/// <summary>
		/// G_JLJJY
		/// </summary>
		[DisplayName("G_JLJJY")]
		public string G_JLJJY { get; set; }

		/// <summary>
		/// 个人档案编号
		/// </summary>
		[DisplayName("个人档案编号")]
		public string 个人档案编号 { get; set; }

		/// <summary>
		/// 创建机构
		/// </summary>
		[DisplayName("创建机构")]
		public string 创建机构 { get; set; }

		/// <summary>
		/// 创建时间
		/// </summary>
		[DisplayName("创建时间")]
		public string 创建时间 { get; set; }

		/// <summary>
		/// 创建人
		/// </summary>
		[DisplayName("创建人")]
		public string 创建人 { get; set; }

		/// <summary>
		/// 修改时间
		/// </summary>
		[DisplayName("修改时间")]
		public string 修改时间 { get; set; }

		/// <summary>
		/// 修改人
		/// </summary>
		[DisplayName("修改人")]
		public string 修改人 { get; set; }

		/// <summary>
		/// 体检日期
		/// </summary>
		[DisplayName("体检日期")]
		public string 体检日期 { get; set; }

		/// <summary>
		/// 所属机构
		/// </summary>
		[DisplayName("所属机构")]
		public string 所属机构 { get; set; }

		/// <summary>
		/// FIELD1
		/// </summary>
		[DisplayName("FIELD1")]
		public string FIELD1 { get; set; }

		/// <summary>
		/// FIELD2
		/// </summary>
		[DisplayName("FIELD2")]
		public string FIELD2 { get; set; }

		/// <summary>
		/// FIELD3
		/// </summary>
		[DisplayName("FIELD3")]
		public string FIELD3 { get; set; }

		/// <summary>
		/// FIELD4
		/// </summary>
		[DisplayName("FIELD4")]
		public string FIELD4 { get; set; }

		/// <summary>
		/// WBC_SUP
		/// </summary>
		[DisplayName("WBC_SUP")]
		public int? WBC_SUP { get; set; }

		/// <summary>
		/// PLT_SUP
		/// </summary>
		[DisplayName("PLT_SUP")]
		public int? PLT_SUP { get; set; }

		/// <summary>
		/// G_TUNWEI
		/// </summary>
		[DisplayName("G_TUNWEI")]
		public string G_TUNWEI { get; set; }

		/// <summary>
		/// G_YTWBZ
		/// </summary>
		[DisplayName("G_YTWBZ")]
		public string G_YTWBZ { get; set; }

		/// <summary>
		/// 锻炼频率
		/// </summary>
		[DisplayName("锻炼频率")]
		public string 锻炼频率 { get; set; }

		/// <summary>
		/// 每次锻炼时间
		/// </summary>
		[DisplayName("每次锻炼时间")]
		public string 每次锻炼时间 { get; set; }

		/// <summary>
		/// 坚持锻炼时间
		/// </summary>
		[DisplayName("坚持锻炼时间")]
		public string 坚持锻炼时间 { get; set; }

		/// <summary>
		/// 锻炼方式
		/// </summary>
		[DisplayName("锻炼方式")]
		public string 锻炼方式 { get; set; }

		/// <summary>
		/// 饮食习惯
		/// </summary>
		[DisplayName("饮食习惯")]
		public string 饮食习惯 { get; set; }

		/// <summary>
		/// 吸烟状况
		/// </summary>
		[DisplayName("吸烟状况")]
		public string 吸烟状况 { get; set; }

		/// <summary>
		/// 日吸烟量
		/// </summary>
		[DisplayName("日吸烟量")]
		public string 日吸烟量 { get; set; }

		/// <summary>
		/// 开始吸烟年龄
		/// </summary>
		[DisplayName("开始吸烟年龄")]
		public string 开始吸烟年龄 { get; set; }

		/// <summary>
		/// 戒烟年龄
		/// </summary>
		[DisplayName("戒烟年龄")]
		public string 戒烟年龄 { get; set; }

		/// <summary>
		/// 饮酒频率
		/// </summary>
		[DisplayName("饮酒频率")]
		public string 饮酒频率 { get; set; }

		/// <summary>
		/// 日饮酒量
		/// </summary>
		[DisplayName("日饮酒量")]
		public string 日饮酒量 { get; set; }

		/// <summary>
		/// 是否戒酒
		/// </summary>
		[DisplayName("是否戒酒")]
		public string 是否戒酒 { get; set; }

		/// <summary>
		/// 戒酒年龄
		/// </summary>
		[DisplayName("戒酒年龄")]
		public string 戒酒年龄 { get; set; }

		/// <summary>
		/// 开始饮酒年龄
		/// </summary>
		[DisplayName("开始饮酒年龄")]
		public string 开始饮酒年龄 { get; set; }

		/// <summary>
		/// 近一年内是否曾醉酒
		/// </summary>
		[DisplayName("近一年内是否曾醉酒")]
		public string 近一年内是否曾醉酒 { get; set; }

		/// <summary>
		/// 饮酒种类
		/// </summary>
		[DisplayName("饮酒种类")]
		public string 饮酒种类 { get; set; }

		/// <summary>
		/// 饮酒种类其它
		/// </summary>
		[DisplayName("饮酒种类其它")]
		public string 饮酒种类其它 { get; set; }

		/// <summary>
		/// 有无职业病
		/// </summary>
		[DisplayName("有无职业病")]
		public string 有无职业病 { get; set; }

		/// <summary>
		/// 具体职业
		/// </summary>
		[DisplayName("具体职业")]
		public string 具体职业 { get; set; }

		/// <summary>
		/// 从业时间
		/// </summary>
		[DisplayName("从业时间")]
		public string 从业时间 { get; set; }

		/// <summary>
		/// 化学物质
		/// </summary>
		[DisplayName("化学物质")]
		public string 化学物质 { get; set; }

		/// <summary>
		/// 化学物质防护
		/// </summary>
		[DisplayName("化学物质防护")]
		public string 化学物质防护 { get; set; }

		/// <summary>
		/// 化学物质具体防护
		/// </summary>
		[DisplayName("化学物质具体防护")]
		public string 化学物质具体防护 { get; set; }

		/// <summary>
		/// 毒物
		/// </summary>
		[DisplayName("毒物")]
		public string 毒物 { get; set; }

		/// <summary>
		/// G_DWFHCS
		/// </summary>
		[DisplayName("G_DWFHCS")]
		public string G_DWFHCS { get; set; }

		/// <summary>
		/// G_DWFHCSQT
		/// </summary>
		[DisplayName("G_DWFHCSQT")]
		public string G_DWFHCSQT { get; set; }

		/// <summary>
		/// 放射物质
		/// </summary>
		[DisplayName("放射物质")]
		public string 放射物质 { get; set; }

		/// <summary>
		/// 放射物质防护措施有无
		/// </summary>
		[DisplayName("放射物质防护措施有无")]
		public string 放射物质防护措施有无 { get; set; }

		/// <summary>
		/// 放射物质防护措施其他
		/// </summary>
		[DisplayName("放射物质防护措施其他")]
		public string 放射物质防护措施其他 { get; set; }

		/// <summary>
		/// 口唇
		/// </summary>
		[DisplayName("口唇")]
		public string 口唇 { get; set; }

		/// <summary>
		/// 齿列
		/// </summary>
		[DisplayName("齿列")]
		public string 齿列 { get; set; }

		/// <summary>
		/// 咽部
		/// </summary>
		[DisplayName("咽部")]
		public string 咽部 { get; set; }

		/// <summary>
		/// 皮肤其他
		/// </summary>
		[DisplayName("皮肤其他")]
		public string 皮肤其他 { get; set; }

		/// <summary>
		/// 巩膜
		/// </summary>
		[DisplayName("巩膜")]
		public string 巩膜 { get; set; }

		/// <summary>
		/// 巩膜其他
		/// </summary>
		[DisplayName("巩膜其他")]
		public string 巩膜其他 { get; set; }

		/// <summary>
		/// 足背动脉搏动
		/// </summary>
		[DisplayName("足背动脉搏动")]
		public string 足背动脉搏动 { get; set; }

		/// <summary>
		/// 乳腺
		/// </summary>
		[DisplayName("乳腺")]
		public string 乳腺 { get; set; }

		/// <summary>
		/// 乳腺其他
		/// </summary>
		[DisplayName("乳腺其他")]
		public string 乳腺其他 { get; set; }

		/// <summary>
		/// 外阴
		/// </summary>
		[DisplayName("外阴")]
		public string 外阴 { get; set; }

		/// <summary>
		/// 外阴异常
		/// </summary>
		[DisplayName("外阴异常")]
		public string 外阴异常 { get; set; }

		/// <summary>
		/// 阴道
		/// </summary>
		[DisplayName("阴道")]
		public string 阴道 { get; set; }

		/// <summary>
		/// 阴道异常
		/// </summary>
		[DisplayName("阴道异常")]
		public string 阴道异常 { get; set; }

		/// <summary>
		/// 宫颈
		/// </summary>
		[DisplayName("宫颈")]
		public string 宫颈 { get; set; }

		/// <summary>
		/// 宫颈异常
		/// </summary>
		[DisplayName("宫颈异常")]
		public string 宫颈异常 { get; set; }

		/// <summary>
		/// 宫体
		/// </summary>
		[DisplayName("宫体")]
		public string 宫体 { get; set; }

		/// <summary>
		/// 宫体异常
		/// </summary>
		[DisplayName("宫体异常")]
		public string 宫体异常 { get; set; }

		/// <summary>
		/// 附件
		/// </summary>
		[DisplayName("附件")]
		public string 附件 { get; set; }

		/// <summary>
		/// 附件异常
		/// </summary>
		[DisplayName("附件异常")]
		public string 附件异常 { get; set; }

		/// <summary>
		/// 血钾浓度
		/// </summary>
		[DisplayName("血钾浓度")]
		public string 血钾浓度 { get; set; }

		/// <summary>
		/// 血钠浓度
		/// </summary>
		[DisplayName("血钠浓度")]
		public string 血钠浓度 { get; set; }

		/// <summary>
		/// 糖化血红蛋白
		/// </summary>
		[DisplayName("糖化血红蛋白")]
		public string 糖化血红蛋白 { get; set; }

		/// <summary>
		/// 宫颈涂片
		/// </summary>
		[DisplayName("宫颈涂片")]
		public string 宫颈涂片 { get; set; }

		/// <summary>
		/// 宫颈涂片异常
		/// </summary>
		[DisplayName("宫颈涂片异常")]
		public string 宫颈涂片异常 { get; set; }

		/// <summary>
		/// 平和质
		/// </summary>
		[DisplayName("平和质")]
		public string 平和质 { get; set; }

		/// <summary>
		/// 气虚质
		/// </summary>
		[DisplayName("气虚质")]
		public string 气虚质 { get; set; }

		/// <summary>
		/// 阳虚质
		/// </summary>
		[DisplayName("阳虚质")]
		public string 阳虚质 { get; set; }

		/// <summary>
		/// 阴虚质
		/// </summary>
		[DisplayName("阴虚质")]
		public string 阴虚质 { get; set; }

		/// <summary>
		/// 痰湿质
		/// </summary>
		[DisplayName("痰湿质")]
		public string 痰湿质 { get; set; }

		/// <summary>
		/// 湿热质
		/// </summary>
		[DisplayName("湿热质")]
		public string 湿热质 { get; set; }

		/// <summary>
		/// 血瘀质
		/// </summary>
		[DisplayName("血瘀质")]
		public string 血瘀质 { get; set; }

		/// <summary>
		/// 气郁质
		/// </summary>
		[DisplayName("气郁质")]
		public string 气郁质 { get; set; }

		/// <summary>
		/// 特禀质
		/// </summary>
		[DisplayName("特禀质")]
		public string 特禀质 { get; set; }

		/// <summary>
		/// 脑血管疾病
		/// </summary>
		[DisplayName("脑血管疾病")]
		public string 脑血管疾病 { get; set; }

		/// <summary>
		/// 脑血管疾病其他
		/// </summary>
		[DisplayName("脑血管疾病其他")]
		public string 脑血管疾病其他 { get; set; }

		/// <summary>
		/// 肾脏疾病
		/// </summary>
		[DisplayName("肾脏疾病")]
		public string 肾脏疾病 { get; set; }

		/// <summary>
		/// 肾脏疾病其他
		/// </summary>
		[DisplayName("肾脏疾病其他")]
		public string 肾脏疾病其他 { get; set; }

		/// <summary>
		/// 心脏疾病
		/// </summary>
		[DisplayName("心脏疾病")]
		public string 心脏疾病 { get; set; }

		/// <summary>
		/// 心脏疾病其他
		/// </summary>
		[DisplayName("心脏疾病其他")]
		public string 心脏疾病其他 { get; set; }

		/// <summary>
		/// 血管疾病
		/// </summary>
		[DisplayName("血管疾病")]
		public string 血管疾病 { get; set; }

		/// <summary>
		/// 血管疾病其他
		/// </summary>
		[DisplayName("血管疾病其他")]
		public string 血管疾病其他 { get; set; }

		/// <summary>
		/// 眼部疾病
		/// </summary>
		[DisplayName("眼部疾病")]
		public string 眼部疾病 { get; set; }

		/// <summary>
		/// 眼部疾病其他
		/// </summary>
		[DisplayName("眼部疾病其他")]
		public string 眼部疾病其他 { get; set; }

		/// <summary>
		/// 神经系统疾病
		/// </summary>
		[DisplayName("神经系统疾病")]
		public string 神经系统疾病 { get; set; }

		/// <summary>
		/// 神经系统疾病其他
		/// </summary>
		[DisplayName("神经系统疾病其他")]
		public string 神经系统疾病其他 { get; set; }

		/// <summary>
		/// 其他系统疾病
		/// </summary>
		[DisplayName("其他系统疾病")]
		public string 其他系统疾病 { get; set; }

		/// <summary>
		/// 其他系统疾病其他
		/// </summary>
		[DisplayName("其他系统疾病其他")]
		public string 其他系统疾病其他 { get; set; }

		/// <summary>
		/// 健康评价
		/// </summary>
		[DisplayName("健康评价")]
		public string 健康评价 { get; set; }

		/// <summary>
		/// 健康评价异常1
		/// </summary>
		[DisplayName("健康评价异常1")]
		public string 健康评价异常1 { get; set; }

		/// <summary>
		/// 健康评价异常2
		/// </summary>
		[DisplayName("健康评价异常2")]
		public string 健康评价异常2 { get; set; }

		/// <summary>
		/// 健康评价异常3
		/// </summary>
		[DisplayName("健康评价异常3")]
		public string 健康评价异常3 { get; set; }

		/// <summary>
		/// 健康评价异常4
		/// </summary>
		[DisplayName("健康评价异常4")]
		public string 健康评价异常4 { get; set; }

		/// <summary>
		/// 健康指导
		/// </summary>
		[DisplayName("健康指导")]
		public string 健康指导 { get; set; }

		/// <summary>
		/// 危险因素控制
		/// </summary>
		[DisplayName("危险因素控制")]
		public string 危险因素控制 { get; set; }

		/// <summary>
		/// 危险因素控制体重
		/// </summary>
		[DisplayName("危险因素控制体重")]
		public string 危险因素控制体重 { get; set; }

		/// <summary>
		/// 危险因素控制疫苗
		/// </summary>
		[DisplayName("危险因素控制疫苗")]
		public string 危险因素控制疫苗 { get; set; }

		/// <summary>
		/// 危险因素控制其他
		/// </summary>
		[DisplayName("危险因素控制其他")]
		public string 危险因素控制其他 { get; set; }

		/// <summary>
		/// FIELD5
		/// </summary>
		[DisplayName("FIELD5")]
		public string FIELD5 { get; set; }

		/// <summary>
		/// 症状其他
		/// </summary>
		[DisplayName("症状其他")]
		public string 症状其他 { get; set; }

		/// <summary>
		/// G_XYYC
		/// </summary>
		[DisplayName("G_XYYC")]
		public string G_XYYC { get; set; }

		/// <summary>
		/// G_XYZC
		/// </summary>
		[DisplayName("G_XYZC")]
		public string G_XYZC { get; set; }

		/// <summary>
		/// G_QTZHZH
		/// </summary>
		[DisplayName("G_QTZHZH")]
		public string G_QTZHZH { get; set; }

		/// <summary>
		/// 缺项
		/// </summary>
		[DisplayName("缺项")]
		public string 缺项 { get; set; }

		/// <summary>
		/// 口唇其他
		/// </summary>
		[DisplayName("口唇其他")]
		public string 口唇其他 { get; set; }

		/// <summary>
		/// 齿列其他
		/// </summary>
		[DisplayName("齿列其他")]
		public string 齿列其他 { get; set; }

		/// <summary>
		/// 咽部其他
		/// </summary>
		[DisplayName("咽部其他")]
		public string 咽部其他 { get; set; }

		/// <summary>
		/// YDGNQT
		/// </summary>
		[DisplayName("YDGNQT")]
		public string YDGNQT { get; set; }

		/// <summary>
		/// 餐后2H血糖
		/// </summary>
		[DisplayName("餐后2H血糖")]
		public string 餐后2H血糖 { get; set; }

		/// <summary>
		/// 老年人状况评估
		/// </summary>
		[DisplayName("老年人状况评估")]
		public string 老年人状况评估 { get; set; }

		/// <summary>
		/// 老年人自理评估
		/// </summary>
		[DisplayName("老年人自理评估")]
		public string 老年人自理评估 { get; set; }

		/// <summary>
		/// 粉尘
		/// </summary>
		[DisplayName("粉尘")]
		public string 粉尘 { get; set; }

		/// <summary>
		/// 物理因素
		/// </summary>
		[DisplayName("物理因素")]
		public string 物理因素 { get; set; }

		/// <summary>
		/// 职业病其他
		/// </summary>
		[DisplayName("职业病其他")]
		public string 职业病其他 { get; set; }

		/// <summary>
		/// 粉尘防护有无
		/// </summary>
		[DisplayName("粉尘防护有无")]
		public string 粉尘防护有无 { get; set; }

		/// <summary>
		/// 物理防护有无
		/// </summary>
		[DisplayName("物理防护有无")]
		public string 物理防护有无 { get; set; }

		/// <summary>
		/// 其他防护有无
		/// </summary>
		[DisplayName("其他防护有无")]
		public string 其他防护有无 { get; set; }

		/// <summary>
		/// 粉尘防护措施
		/// </summary>
		[DisplayName("粉尘防护措施")]
		public string 粉尘防护措施 { get; set; }

		/// <summary>
		/// 物理防护措施
		/// </summary>
		[DisplayName("物理防护措施")]
		public string 物理防护措施 { get; set; }

		/// <summary>
		/// 其他防护措施
		/// </summary>
		[DisplayName("其他防护措施")]
		public string 其他防护措施 { get; set; }

		/// <summary>
		/// TNBFXJF
		/// </summary>
		[DisplayName("TNBFXJF")]
		public string TNBFXJF { get; set; }

		/// <summary>
		/// 左侧原因
		/// </summary>
		[DisplayName("左侧原因")]
		public string 左侧原因 { get; set; }

		/// <summary>
		/// 右侧原因
		/// </summary>
		[DisplayName("右侧原因")]
		public string 右侧原因 { get; set; }

		/// <summary>
		/// 尿微量白蛋白
		/// </summary>
		[DisplayName("尿微量白蛋白")]
		public string 尿微量白蛋白 { get; set; }

		/// <summary>
		/// 完整度
		/// </summary>
		[DisplayName("完整度")]
		public string 完整度 { get; set; }

		/// <summary>
		/// 齿列缺齿
		/// </summary>
		[DisplayName("齿列缺齿")]
		public string 齿列缺齿 { get; set; }

		/// <summary>
		/// 齿列龋齿
		/// </summary>
		[DisplayName("齿列龋齿")]
		public string 齿列龋齿 { get; set; }

		/// <summary>
		/// 齿列义齿
		/// </summary>
		[DisplayName("齿列义齿")]
		public string 齿列义齿 { get; set; }

		/// <summary>
		/// 同型半胱氨酸
		/// </summary>
		[DisplayName("同型半胱氨酸")]
		public string 同型半胱氨酸 { get; set; }

		/// <summary>
		/// 空腹血糖2
		/// </summary>
		[DisplayName("空腹血糖2")]
		public string 空腹血糖2 { get; set; }

		/// <summary>
		/// 其他B超
		/// </summary>
		[DisplayName("其他B超")]
		public string 其他B超 { get; set; }

		/// <summary>
		/// 其他B超异常
		/// </summary>
		[DisplayName("其他B超异常")]
		public string 其他B超异常 { get; set; }

		/// <summary>
		/// 本人签字
		/// </summary>
		[DisplayName("本人签字")]
		public string 本人签字 { get; set; }

		/// <summary>
		/// 家属签字
		/// </summary>
		[DisplayName("家属签字")]
		public string 家属签字 { get; set; }

		/// <summary>
		/// 反馈人签字
		/// </summary>
		[DisplayName("反馈人签字")]
		public string 反馈人签字 { get; set; }

		/// <summary>
		/// 反馈时间
		/// </summary>
		[DisplayName("反馈时间")]
		public string 反馈时间 { get; set; }

		/// <summary>
		/// 医师签字
		/// </summary>
		[DisplayName("医师签字")]
		public string 医师签字 { get; set; }

		/// <summary>
		/// 是否反馈
		/// </summary>
		[DisplayName("是否反馈")]
		public string 是否反馈 { get; set; }
		#endregion Model
	}
}
