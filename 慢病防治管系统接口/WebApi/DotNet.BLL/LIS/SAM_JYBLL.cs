﻿using DotNet.DAL;
using DotNet.Utilities;
using DotNet.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace DotNet.BLL
{
    public class SAM_JYBLL
    {
        private static readonly SAM_JYDAL DAL = new SAM_JYDAL();

        /// <summary>
        /// 根据查询条件获取数据
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public static MBJsonResult<List<SAM_JYRes>> GetListByWhere(SAM_JYSearcher Searcher)
        {
            try
            {
                var accountList = DAL.GetListByWhere(Searcher);

                return ReturnMBResult.GetJsonResult(true, "", accountList);
            }
            catch (Exception ex)
            {
                return ReturnMBResult.GetJsonResult(false, ex.Message, new List<SAM_JYRes>());
            }
        }
    }
}
