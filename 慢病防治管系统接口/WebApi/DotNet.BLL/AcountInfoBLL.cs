using System;
using DotNet.Utilities;
using DotNet.Model;
using DotNet.DAL;
using System.Collections.Generic;

namespace DotNet.BLL
{
    public class AccountInfoBLL
    {
        private static readonly AccountInfoDAL accountInfoDAL = new AccountInfoDAL();

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="loginID"></param>
        /// <param name="passWord"></param>
        /// <returns></returns>
        public static string Login(string loginID, string passWord)
        {
            return "1";
        }

        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <returns></returns>
        public static JsonResult<List<AccountInfo>> GetList()
        {
            var accountList = accountInfoDAL.GetList();
            return ReturnJsonResult.GetJsonResult(0, "", accountList);
            //return (accountList != null) ? JsonHelperT.ToJson(accountList) : "";
        }
 
        /// <summary>
        /// 根据查询条件获取数据
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public static JsonResult<List<AccountInfo>> GetListByWhere(string where)
        {
            var accountList = accountInfoDAL.GetListByWhere(where);

            return ReturnJsonResult.GetJsonResult(0, "", accountList);
            //return (accountList != null) ? JsonHelperT.ToJson(accountList) : "";
        }
 
        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="currentPage">当前页码</param>
        /// <param name="pageSize">每页数据条数</param>
        /// <param name="where">查询条件</param>
        /// <param name="totalCount">总数据条数</param>
        /// <param name="pageCount">页码数量</param>
        /// <returns></returns>
        public static string GetPageList(int currentPage, int pageSize, string where)
        {
            int totalCount = 0;
            int pageCount = 0;
            var accountList = accountInfoDAL.GetPageList(currentPage, pageSize, where, out totalCount, out pageCount);
            if (accountList != null)
            {
                var accountinfo = new
                {
                    totalcount = totalCount,
                    pagecount = pageCount,
                    mainList = accountList
                };
                return totalCount.ToString();//JsonHelperT.ToJson(accountinfo);
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string Save(AccountInfo item)
        {
            string isOK = "success";
            if (!accountInfoDAL.Save(item))
            {
                isOK = "fail";
            }
            var mesg = new
            {
                message = isOK
            };
            return isOK;// JsonHelperT.ToJson(mesg);
        }
 
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="accountID"></param>
        /// <returns></returns>
        public static string Delete(string accountID)
        {
            string isOK = "success";
            if (!accountInfoDAL.Delete(accountID))
            {
                isOK = "fail";
            }
            var mesg = new
            {
                message = isOK
            };
            return isOK;// JsonHelperT.ToJson(mesg);
        }
    }
}