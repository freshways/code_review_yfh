﻿using DotNet.DAL;
using DotNet.Utilities;
using DotNet.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace DotNet.BLL
{
    public class tb_健康体检BLL
    {
        private static readonly tb_健康体检DAL DAL = new tb_健康体检DAL();

        /// <summary>
        /// 根据查询条件获取数据
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public static MBJsonResult<List<tb_健康体检Res>> GetListByWhere(tb_健康体检Searcher Searcher)
        {
            try
            {
                var accountList = DAL.GetListByWhere(Searcher);

                return ReturnMBResult.GetJsonResult(true, "", accountList);
            }
            catch (Exception ex)
            {
                return ReturnMBResult.GetJsonResult(false, ex.Message, new List<tb_健康体检Res>());
            }
        }
    }
}
