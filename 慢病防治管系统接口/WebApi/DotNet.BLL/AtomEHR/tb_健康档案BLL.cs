﻿using DotNet.DAL;
using DotNet.Utilities;
using DotNet.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace DotNet.BLL
{
    public class tb_健康档案BLL
    {
        private static readonly tb_健康档案DAL tb_健康档案DAL = new tb_健康档案DAL();

        /// <summary>
        /// 根据查询条件获取数据
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public static MBJsonResult<List<tb_健康档案Res>> GetListByWhere(tb_健康档案Searcher Searcher)
        {
            try
            {
                var accountList = tb_健康档案DAL.GetListByWhere(Searcher);

                return ReturnMBResult.GetJsonResult(true, "", accountList);
            }
            catch (Exception ex)
            {
                return ReturnMBResult.GetJsonResult(false, ex.Message, new List<tb_健康档案Res>());
            }
        }

        /// <summary>
        /// 根据身份证返回是否
        /// </summary>
        /// <param name="IDCard"></param>
        /// <returns></returns>
        public static JsonResult<bool> GetGRDNForID(string IDCard)
        {
            try
            {
                if (string.IsNullOrEmpty(IDCard))
                { 
                    return ReturnJsonResult.GetJsonResult(-1, "身份证号为空！", false); 
                }
                var state = tb_健康档案DAL.GetGRDNForID(IDCard);
                if (state)
                {
                    return ReturnJsonResult.GetJsonResult(0, "", state);
                }
                else
                {
                    return ReturnJsonResult.GetJsonResult(-1, "未找到身份信息！", state);
                }
            }
            catch (Exception ex)
            {
                return ReturnJsonResult.GetJsonResult(-1, ex.Message, false);
            }
        }
    }
}
