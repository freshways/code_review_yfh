﻿using DotNet.DAL;
using DotNet.Utilities;
using DotNet.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace DotNet.BLL
{
    public class tb_MXB高血压随访表BLL
    {
        private static readonly tb_MXB高血压随访表DAL DAL = new tb_MXB高血压随访表DAL();

        /// <summary>
        /// 根据查询条件获取数据
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public static MBJsonResult<List<tb_MXB高血压随访表Res>> GetListByWhere(tb_MXB高血压随访表Searcher Searcher)
        {
            try
            {
                var accountList = DAL.GetListByWhere(Searcher);

                return ReturnMBResult.GetJsonResult(true, "", accountList);
            }
            catch (Exception ex)
            {
                return ReturnMBResult.GetJsonResult(false, ex.Message, new List<tb_MXB高血压随访表Res>());
            }
        }

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static MBJsonResult<string> Save(tb_MXB高血压随访表Input item)
        {
            return DAL.Save(item);
        }
    }
}
