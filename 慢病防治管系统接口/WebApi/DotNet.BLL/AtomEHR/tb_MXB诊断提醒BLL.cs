﻿using DotNet.DAL;
using DotNet.Utilities;
using DotNet.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace DotNet.BLL
{
    public class tb_MXB诊断提醒BLL
    {
        private static readonly tb_MXB诊断提醒DAL DAL = new tb_MXB诊断提醒DAL();

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static MBJsonResult<string> Save(tb_MXB诊断提醒Input item)
        {            
            return DAL.Save(item);
        }
    }
}
