﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNet.Utilities
{
    public class ReturnJsonResult
    {
        public static JsonResult<T> GetJsonResult<T>(int code, string msg, T data)
        {
            JsonResult<T> jsonResult = new JsonResult<T>();
            jsonResult.Errno = code;
            jsonResult.Errmsg = msg;
            jsonResult.Data = data;
            return jsonResult;
        }
    }

    /// <summary>
    /// 定义统计返回json格式数据
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class JsonResult<T>
    {
        public int Errno { get; set; }
        public string Errmsg { get; set; }
        public T Data { get; set; }
    }

    /// <summary>
    /// 慢病接口返回消息格式
    /// </summary>
    public class ReturnMBResult
    {
        public static MBJsonResult<T> GetJsonResult<T>(bool code, string msg, T data)
        {
            MBJsonResult<T> jsonResult = new MBJsonResult<T>();
            jsonResult.success = code;
            jsonResult.msg = msg;
            jsonResult.data = data;
            return jsonResult;
        }
    }

    /// <summary>
    /// 定义统计返回json格式数据
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MBJsonResult<T>
    {
        public bool success { get; set; }
        public string msg { get; set; }
        public T data { get; set; }
    }
}
