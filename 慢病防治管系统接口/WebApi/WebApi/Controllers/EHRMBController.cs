﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using DotNet.BLL;
using DotNet.Model;
using DotNet.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EHRMBController : ControllerBase
    {
        /// <summary>
        /// 获取居民档案
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("Get_GRDA")]
        public MBJsonResult<List<tb_健康档案Res>> Get_GRDA([FromBody] tb_健康档案Searcher Searcher)
        {
            //string IDCard = string.Empty, startNum = string.Empty, endNum = string.Empty;
            //if (!string.IsNullOrEmpty(Searcher.IDCard))
            //{
            //    SM4Utils sm4 = new SM4Utils();
            //    string resam4 = sm4.Decrypt_ECB(Searcher.IDCard, true,"c89bed27dfde4d0291885c3ea3e5a89e");
            //    Searcher.IDCard = resam4;
            //    //return ReturnMBResult.GetJsonResult(false, "身份证不能为空！", new List<tb_健康档案Res>());
            //}
            try
            {
                return tb_健康档案BLL.GetListByWhere(Searcher);
            }
            catch (Exception ex)
            {
                return ReturnMBResult.GetJsonResult(false, ex.Message, new List<tb_健康档案Res>());
            }
        }


        /// <summary>
        /// 获取健康体检
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("Get_JKTJ")]
        public MBJsonResult<List<tb_健康体检Res>> Get_JKTJ([FromBody] tb_健康体检Searcher Searcher)
        {
            try
            {
                return tb_健康体检BLL.GetListByWhere(Searcher);
            }
            catch (Exception ex)
            {
                return ReturnMBResult.GetJsonResult(false, ex.Message, new List<tb_健康体检Res>());
            }
        }

        /// <summary>
        /// 获取高血压随访记录
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("Get_GXYSF")]
        public MBJsonResult<List<tb_MXB高血压随访表Res>> Get_GXYSF([FromBody] tb_MXB高血压随访表Searcher Searcher)
        {
            try
            {
                return tb_MXB高血压随访表BLL.GetListByWhere(Searcher);
            }
            catch (Exception ex)
            {
                return ReturnMBResult.GetJsonResult(false, ex.Message, new List<tb_MXB高血压随访表Res>());
            }
        }

        /// <summary>
        /// 高血压随访同步
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Input_GXYSF")]
        public MBJsonResult<string> Input_GXYSF(tb_MXB高血压随访表Input Value)
        {
            return tb_MXB高血压随访表BLL.Save(Value);
        }

        /// <summary>
        /// 获取糖尿病随访记录
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("Get_TNBSF")]
        public Task<MBJsonResult<List<tb_MXB糖尿病随访表Res>>> Get_TNBSF([FromBody] tb_MXB糖尿病随访表Searcher Searcher)
        {
            try
            {
                return Task.Run(function: () => tb_MXB糖尿病随访表BLL.GetListByWhere(Searcher));
            }
            catch (Exception ex)
            {
                return Task.Run(function: () => ReturnMBResult.GetJsonResult(false, ex.Message, new List<tb_MXB糖尿病随访表Res>()));
            }
        }

        /// <summary>
        /// 糖尿病随访同步
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Input_TNBSF")]
        public MBJsonResult<string> Input_TNBSF(tb_MXB糖尿病随访表Input Value)
        {
            return tb_MXB糖尿病随访表BLL.Save(Value);
        }

        /// <summary>
        /// 诊断提醒
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("Input_ZDTX")]
        public Task<MBJsonResult<string>> Input_ZDTX(tb_MXB诊断提醒Input Value)
        {
            try
            {
                return Task.Run(function:()=> tb_MXB诊断提醒BLL.Save(Value));
            }
            catch (Exception ex)
            {
                return Task.Run(function: () => ReturnMBResult.GetJsonResult(false, ex.Message, ""));
            }
        }

        [HttpGet]
        [Route("Get_Encrypt_ECB")]
        public ActionResult<string> Get_Encrypt_ECB(string plainText)
        {
            SM4Utils sm4 = new SM4Utils();
            //sm4.secretKey = "c89bed27dfde4d0291885c3ea3e5a89e";
            //sm4.hexString = true;
            string resam4 = sm4.Encrypt_ECB(plainText, true, "c89bed27dfde4d0291885c3ea3e5a89e");
            return resam4;
        }

        [HttpGet]
        [Route("Get_Decrypt_ECB")]
        public string Get_Decrypt_ECB(string cipherText)
        {
            SM4Utils sm4 = new SM4Utils();
            //sm4.secretKey = "c89bed27dfde4d0291885c3ea3e5a89e";
            //sm4.hexString = true;
            string resam4 = sm4.Decrypt_ECB(cipherText, true, "c89bed27dfde4d0291885c3ea3e5a89e");
            return resam4;
        }


        [HttpGet]
        [Route("Get_sign")]
        public string Get_sign(string random)
        {
            string random2 = Guid.NewGuid().ToString().Replace("-", "");
            random2 = random + "c89bed27dfde4d0291885c3ea3e5a89e";
            string[] ss = new string[random2.Length];
            for (int i = 0; i < random2.Length; i++)
            {
                ss[i] = random2.Substring(i, 1);
            }
            //ss = ss.OrderBy(p => p).ToArray();
            Array.Sort(ss);//sort:是排序的意思，对这个一维数组进行排序
            
            string res = string.Join("", ss);
            return MD5Encrypt.UserMd5(res);
        }

        [HttpGet]
        [Route("Get_GRDNforID")]
        public Task<JsonResult<bool>> Get_GRDNforID(string IDCard)
        {
            //string SQL = "select 个人档案编号,姓名 from tb_健康档案 where 身份证号='371323198010150531' ";
            //return new JsonResult(SQL);

            return Task.Run(function: () =>  tb_健康档案BLL.GetGRDNForID(IDCard));
        }
    }
}
