﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using DotNet.BLL;
using DotNet.Model;
using DotNet.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LISMBController : ControllerBase
    {
        /// <summary>
        /// 获取患者检验数据
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("Get_JYList")]
        public MBJsonResult<List<SAM_JYRes>> Get_JYList([FromBody] SAM_JYSearcher Searcher)
        {
            try
            {
                return SAM_JYBLL.GetListByWhere(Searcher);
            }
            catch (Exception ex)
            {
                return ReturnMBResult.GetJsonResult(false, ex.Message, new List<SAM_JYRes>());
            }
        }

    }
}
