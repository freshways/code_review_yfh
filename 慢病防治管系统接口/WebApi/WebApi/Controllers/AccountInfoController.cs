using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using DotNet.Utilities;
using DotNet.Model;
using DotNet.BLL;
using System.Data;
using Newtonsoft.Json.Linq;

namespace WebAPI.Controllers
{
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountInfoController : ControllerBase
    {
        private HttpResponseMessage WriteMsg(string Msg)
        {
            return new HttpResponseMessage { Content = new StringContent(Msg, System.Text.Encoding.UTF8, "application/json") };
        }
        /// <summary>
        /// 分页数据参数
        /// </summary>
        public class InputModel
        {
            public int CurretPage { get; set; }
            public int PageSize { get; set; }
            //public List<SearchModel> SearchList { get; set; }
        }
 
        /// <summary>
        /// 登录参数
        /// </summary>
        public class LoginModel
        {
            public string LoginID { get; set; }
            public string PassWord { get; set; }
        }
 
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Login")]
        public string Login([FromBody]LoginModel login)
        {
            return AccountInfoBLL.Login(login.LoginID, login.PassWord);
        }
 
        /// <summary>
        /// 获取数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetList")]
        public JsonResult<List<AccountInfo>> GetList()
        {
            return AccountInfoBLL.GetList();
        }


        /*原作者没有提到Utilities的方法，所以里面的错误暂时注释
        /// <summary>
        /// 根据条件获取数据
        /// </summary>
        /// <param name="searchList"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetListByWhere")]
        public string GetListByWhere([FromBody]List<SearchModel> searchList)
        {
            return AccountInfoBLL.GetListByWhere(SearchHelper.GetSarchCondition(searchList));
        }
 
        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="inputValue"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetPageList")]
        public string GetPageList([FromBody]InputModel inputValue)
        {
            string where = SearchHelper.GetSarchCondition(inputValue.SearchList);
            return AccountInfoBLL.GetPageList(inputValue.CurretPage, inputValue.PageSize, where);
        }

        */

        ///// <summary>
        ///// 保存
        ///// </summary>
        ///// <param name="item"></param>
        ///// <returns></returns>
        //[HttpPost]
        //[Route("Save")]
        //public string Save([FromBody]AccountInfo item)
        //{
        //    return AccountInfoBLL.Save(item);
        //}
 
        ///// <summary>
        ///// 删除
        ///// </summary>
        ///// <param name="accountID"></param>
        ///// <returns></returns>
        //[HttpDelete]
        //[Route("Delete")]
        //public string Delete([FromBody]string accountID)
        //{
        //    return AccountInfoBLL.Delete(accountID);
        //}

    }


    
}
