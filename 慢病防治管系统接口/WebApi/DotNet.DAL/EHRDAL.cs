﻿using DotNet.DAL;
using DotNet.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace DotNet.DAL
{
    public class EHRDAL
    {
        AtomEHRDbContent dbContent;

        public EHRDAL()
        {
            dbContent = new AtomEHRDbContent();
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns></returns>
        public List<tb_健康档案> GetList()
        {
            try
            {
                var accountList = dbContent.tb_健康档案.OrderByDescending(s => s.ID);
                return (accountList.Any()) ? accountList.ToList() : null;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 根据查询条件获取数据
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public List<tb_健康档案> GetListByWhere(string where)
        {
            try
            {
                //tb_健康档案 tb1 = new tb_健康档案();

                //dbContent.Add(tb1);
                //dbContent.Update(tb1);
                //dbContent.SaveChanges();                

                var accountList = dbContent.tb_健康档案.Where(c => c.个人档案编号 == where).OrderByDescending(s => s.ID);
                return (accountList.Any()) ? accountList.ToList() : null;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 根据SQL
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public List<tb_健康档案2> GetListBySQL(string IDCard)
        {
            try
            {
                string SQL个人档案 = @"select 个人档案编号,姓名,性别,出生日期,身份证号,本人电话,区+街道+居委会 as 居住地址
,case when 是否高血压='1' then '高血压' end 是否高血压
,(select top 1 下次随访时间 from tb_MXB高血压随访表 gxy where a.个人档案编号 = gxy.个人档案编号 order by 发生时间 desc) 高血压下次随访时间
,case when 是否糖尿病 = '1' then '糖尿病' end 是否糖尿病
  ,(select top 1 下次随访时间 from tb_MXB糖尿病随访表 tnb where a.个人档案编号 = tnb.个人档案编号 order by 发生时间 desc) 糖尿病下次随访时间
  from View_个人信息表 a where 个人档案编号 = @个人档案编号 ";
                var list = dbContent.Query<tb_健康档案2>(new DbContextSqlQueryCommands
                {
                    //调用
                    Sql = new DbContextSqlQueryCommand(SQL个人档案, new
                    {
                        个人档案编号 = string.Join(",", IDCard),
                    })
                });
                return list.ToList();
                //var accountList = dbContent.Database.(SQL);
                //return (accountList.Any()) ? accountList.ToList() : null;
            }
            catch
            {
                return null;
            }
        }
    }
}
