using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using DotNet.Model;
 
namespace DotNet.DAL
{
    public class AccountInfoDAL
    {
        AngularDbContent dbContent;
 
        public AccountInfoDAL()
        {
            dbContent = new AngularDbContent();
        }

        public string GetMessage()
        {
            return "";
        }
 
        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <returns></returns>
        public List<AccountInfo> GetList()
        {
            try
            {
                var accountList = dbContent.accountInfo.OrderByDescending(s => s.CreateTime);
                return (accountList.Any()) ? accountList.ToList() : null;
            }
            catch
            {
                return null;
            }
        }
 
        /// <summary>
        /// 根据查询条件获取数据
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public List<AccountInfo> GetListByWhere(string where)
        {
            try
            {
                var accountList = dbContent.accountInfo.Where(where).OrderByDescending(s => s.CreateTime);
                return (accountList.Any()) ? accountList.ToList() : null;
            }
            catch
            {
                return null;
            }
        }
 
        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="currentPage">当前页码</param>
        /// <param name="pageSize">每页数据条数</param>
        /// <param name="where">查询条件</param>
        /// <param name="totalCount">总数据条数</param>
        /// <param name="pageCount">页码数量</param>
        /// <returns></returns>
        public List<AccountInfo> GetPageList(int currentPage, int pageSize, string where, out int totalCount, out int pageCount)
        {
            int skipCount = (currentPage - 1) * pageSize;
            pageCount = 0;
            totalCount = 0;
            try
            {
                totalCount = dbContent.accountInfo.Where(where).Count();
                if ((totalCount % pageSize) > 0)
                {
                    pageCount = (totalCount / pageSize) + 1;
                }
                else
                {
                    pageCount = totalCount / pageSize;
                }
                var accountList = dbContent.accountInfo.Where(where).OrderByDescending(s => s.CreateTime)
                .Skip(skipCount).Take(pageSize);
                return (accountList.Any()) ? accountList.ToList() : null;
            }
            catch
            {
                return null;
            }
        }
 
        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Save(AccountInfo item)
        {
            if (item == null)
            {
                return false;
            }
            int rowsAffected = 0;//受影响行数
            string accountID = (!string.IsNullOrWhiteSpace(item.AccountID)) ? item.AccountID : Guid.NewGuid().ToString();
            try
            {
                var accountModel = dbContent.accountInfo.Where(s => s.AccountID.Equals(accountID)).FirstOrDefault();
                if (accountModel != null)
                {
                    accountModel.LoginID = item.LoginID;
                    accountModel.PassWord = item.PassWord;
                    accountModel.UserName = item.UserName;
                    accountModel.ModifyTime = System.DateTime.Now;
                    rowsAffected = dbContent.SaveChanges();
                }
                else
                {
                    item.AccountID = accountID;
                    item.CreateTime = System.DateTime.Now;
                    dbContent.accountInfo.Add(item);
                    rowsAffected = dbContent.SaveChanges();
                }
                return (rowsAffected > 0) ? true : false;
            }
            catch
            {
                return false;
            }
        }
 
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="accountID"></param>
        /// <returns></returns>
        public bool Delete(string accountID)
        {
            if (string.IsNullOrWhiteSpace(accountID))
            {
                return false;
            }
            try
            {
                var accountModel = dbContent.accountInfo.Where(s => s.AccountID.Equals(accountID)).FirstOrDefault();
                dbContent.Remove(accountModel);
                int rowsAffected = dbContent.SaveChanges();
                return (rowsAffected > 0) ? true : false;
            }
            catch
            {
                return false;
            }
        }
    }
}