﻿using DotNet.Model;
using DotNet.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotNet.DAL
{
    public class SAM_JYDAL
    {
        LISDbContent dbContent;

        public SAM_JYDAL()
        {
            //dbContent = new LISDbContent();
        }

        //public 
        /// <summary>
        /// 根据查询条件获取数据
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public List<SAM_JYRes> GetListByWhere(SAM_JYSearcher where)
        {
            try
            {
                #region 初始化
                int pag = 1;
                int top = 1;
                pag = string.IsNullOrEmpty(where.startNum) ? 1 : Convert.ToInt32(where.startNum);
                top = string.IsNullOrEmpty(where.endNum) ? 1 : Convert.ToInt32(where.endNum);

                SM4Utils sm4 = new SM4Utils();
                string _secretKey = string.Empty;
                string _liscode = string.Empty;
                if (!string.IsNullOrEmpty(where.appKey))
                {
                    AtomEHRDbContent gwdbContent = new AtomEHRDbContent();
                    InterFace_MB_RGKEY rg = gwdbContent.InterFace_MB_RGKEY.Where(x => x.appKey == where.appKey).FirstOrDefault();
                    _secretKey = rg.secret;
                    _liscode = rg.liscode;
                }
                #endregion

                if (!string.IsNullOrEmpty(_liscode))
                {
                    //获取一下所属辖区机构,预处理
                    string LISConnection = $"Server = 192.168.10.57;Database=LIS{_liscode};User ID =yggsuser2020;Password=yggsuser@123.com;";
                    dbContent = new LISDbContent(LISConnection);
                }

                var List = dbContent.SAM_JY.Where(x=>x.fjy_zt== "已审核" && x.sfzh!="");
                if (!string.IsNullOrEmpty(where.IDCard))
                {
                    //解密身份证号
                    string s身份证号 = sm4.Decrypt_ECB(where.IDCard, true, _secretKey);
                    List = List.Where(x => x.sfzh == s身份证号);
                }
                if (!string.IsNullOrEmpty(where.startDate))
                {
                    List = List.Where(x => string.Compare(x.freport_time, where.startDate) >= 0);
                }
                if (!string.IsNullOrEmpty(where.endDate))
                {
                    //处理日期+1天
                    string endDate = endDate = Convert.ToDateTime(where.endDate).AddDays(1).ToString("yyyy-MM-dd");
                    List = List.Where(x => string.Compare(x.freport_time, endDate) <= 0);
                }
                //拼接开始和结束行
                List = List.OrderByDescending(x => x.fjy_date).ThenBy(c=>c.fjy_yb_code).Skip(pag <= 0 ? 0 : pag - 1).Take(top - pag + 1);

                var accountList = List
                    .Select(x=> new SAM_JYRes
                    {
                        reportNum = x.fjy_id,
                        idCard = sm4.Encrypt_ECB(x.sfzh, true, _secretKey),
                        name = x.fhz_name,
                        doctorId = x.fjy_user_id,
                        doctorName = dbContent.WWF_PERSON.Where(a=>a.fperson_id == x.fjy_user_id).Select(y=>y.fname).FirstOrDefault(),
                        deptId = x.fhz_dept,
                        deptName = dbContent.WWF_DEPT.Where(a=>a.fdept_id == x.fhz_dept).Select(y=>y.fname).FirstOrDefault(),
                        examCode = x.fjy_instr,//dbContent.SAM_APPLY.Where(a=> a.fapply_id == x.fapply_id).Select(y=>y.his_recordid).FirstOrDefault(),
                        examName = dbContent.SAM_INSTR.Where(a=>a.finstr_id == x.fjy_instr).Select(y=>y.fname).FirstOrDefault(),//dbContent.SAM_APPLY.Where(a => a.fapply_id == x.fapply_id).Select(y => y.fitem_group).FirstOrDefault(),
                        details = dbContent.SAM_JY_RESULT.Where(a=>a.fjy_id == x.fjy_id).Select(y=> new DetailsItem
                        {
                            itemId = y.fitem_id,
                            itemName = y.fitem_name,
                            result = y.fvalue,
                            unit = y.fitem_unit,
                            referValueUpper = y.fitem_ref.Contains("--")? y.fitem_ref.Split("--",StringSplitOptions.RemoveEmptyEntries)[1]:"",
                            referValueLower = y.fitem_ref.Contains("--") ? y.fitem_ref.Split("--", StringSplitOptions.RemoveEmptyEntries)[0] : "",
                            attachedResult = y.fvalue=="阳性"?"4" : y.fitem_badge.Replace("↑↑", "6").Replace("↑", "3").Replace("↓↓", "5").Replace("↓", "2").Replace("","1")
                        }).ToList(),
                        examDate = x.fcheck_time
                    }).ToList();

                return accountList;//(accountList.Any()) ? accountList.ToList() : null;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
