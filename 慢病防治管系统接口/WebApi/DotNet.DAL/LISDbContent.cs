using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using DotNet.Model;
 
namespace DotNet.DAL
{
    public class LISDbContent : DbContext
    {
        string LisConn = string.Empty;
        public LISDbContent(string conn="") : base() 
        {
            if (!string.IsNullOrEmpty(conn))
            {
                //base.Database.GetDbConnection().ConnectionString = conn;
                LisConn = conn;
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            try
            {               
                var config = new ConfigurationBuilder()
                                .SetBasePath(System.Environment.CurrentDirectory)//System.IO.Directory.GetCurrentDirectory()
                                .AddJsonFile("appsettings.json")
                                .Build();
                
                optionsBuilder.UseSqlServer(config.GetConnectionString("LISConnection"), b => b.UseRowNumberForPaging());

                //重定向数据库连接
                if (!string.IsNullOrEmpty(LisConn))
                {
                    optionsBuilder.UseSqlServer(LisConn, b => b.UseRowNumberForPaging());
                }

            }
            catch (System.Exception ex)
            {                
                throw new Exception(ex.Message);
            }
        }
        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<WWF_DEPT>().HasKey(c => new { c.fdept_id });
        //}

        public DbSet<WWF_DEPT> WWF_DEPT { get; set; }

        public DbSet<WWF_PERSON> WWF_PERSON { get; set; }     

        public DbSet<SAM_APPLY> SAM_APPLY { get; set; }

        public DbSet<SAM_INSTR> SAM_INSTR { get; set; }

        public DbSet<SAM_JY> SAM_JY { get; set; }

        public DbSet<SAM_JY_RESULT> SAM_JY_RESULT { get; set; }
    }
}