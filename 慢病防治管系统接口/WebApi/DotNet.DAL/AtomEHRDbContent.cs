using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using DotNet.Model;
 
namespace DotNet.DAL
{
    public class AtomEHRDbContent : DbContext
    {
        public AtomEHRDbContent() : base() { }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            try
            {               
                var config = new ConfigurationBuilder()
                                .SetBasePath(System.Environment.CurrentDirectory)//System.IO.Directory.GetCurrentDirectory()
                                .AddJsonFile("appsettings.json")
                                .Build();
    
                optionsBuilder.UseSqlServer(config.GetConnectionString("AtomEHR")); 
            }
            catch (System.Exception ex)
            {                
                throw new Exception(ex.Message);
            }
        }
        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity <tb_健康档案> ().HasKey(c => new { c.ID });
        //}

        public DbSet<tb_MyUser> tb_MyUser { get; set; }

        public DbSet<tb_机构信息> tb_机构信息 { get; set; }

        public DbSet<tb_地区档案> tb_地区档案 { get; set; }

        public DbSet<InterFace_MB_RGKEY> InterFace_MB_RGKEY { get; set; }

        //
        public DbSet<tb_健康档案> tb_健康档案 { get; set; }

        public DbSet<tb_健康档案_个人健康特征> tb_健康档案_个人健康特征 { get; set; }

        public DbSet<tb_健康档案_既往病史> tb_健康档案_既往病史 { get; set; }

        public DbSet<tb_健康档案_家族病史> tb_健康档案_家族病史 { get; set; }

        public DbSet<tb_健康档案_健康状态> tb_健康档案_健康状态 { get; set; }

        //
        public DbSet<tb_健康体检> tb_健康体检 { get; set; }

        public DbSet<tb_健康体检_用药情况表> tb_健康体检_用药情况表 { get; set; }

        //
        public DbSet<tb_MXB高血压随访表> tb_MXB高血压随访表 { get; set; }

        public DbSet<tb_MXB高血压随访表_用药情况> tb_MXB高血压随访表_用药情况 { get; set; }

        public DbSet<tb_MXB高血压随访表_用药调整> tb_MXB高血压随访表_用药调整 { get; set; }

        //
        public DbSet<tb_MXB糖尿病随访表> tb_MXB糖尿病随访表 { get; set; }

        public DbSet<tb_MXB糖尿病随访表_用药情况> tb_MXB糖尿病随访表_用药情况 { get; set; }

        public DbSet<tb_MXB糖尿病随访表_用药调整> tb_MXB糖尿病随访表_用药调整 { get; set; }
        
        //
        public DbSet<tb_MXB诊断提醒> tb_MXB诊断提醒 { get; set; }
    }
}