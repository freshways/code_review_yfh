using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using DotNet.Model;
 
namespace DotNet.DAL
{
    public class AngularDbContent : DbContext
    {
        public AngularDbContent() : base() { }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            try
            {               
                var config = new ConfigurationBuilder()
                                .SetBasePath(System.Environment.CurrentDirectory)//System.IO.Directory.GetCurrentDirectory()
                                .AddJsonFile("appsettings.json")
                                .Build();
    
                optionsBuilder.UseSqlServer(config.GetConnectionString("SqlServerConnection")); 
            }
            catch (System.Exception ex)
            {                
                throw new Exception(ex.Message);
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccountInfo>().HasKey(c => new { c.AccountID });
        }
 
        public DbSet<AccountInfo> accountInfo { get; set; }
    }
}