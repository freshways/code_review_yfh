﻿using DotNet.Model;
using DotNet.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace DotNet.DAL
{
    public class tb_MXB糖尿病随访表DAL
    {
        AtomEHRDbContent dbContent;

        public tb_MXB糖尿病随访表DAL()
        {
            dbContent = new AtomEHRDbContent();
        }

        /// <summary>
        /// 根据查询条件获取数据
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public List<tb_MXB糖尿病随访表Res> GetListByWhere(tb_MXB糖尿病随访表Searcher where)
        {
            try
            {
                #region 预处理
                int pag = 1;
                int top = 1;
                pag = string.IsNullOrEmpty(where.startNum) ? 1 : Convert.ToInt32(where.startNum);
                top = string.IsNullOrEmpty(where.endNum) ? 1 : Convert.ToInt32(where.endNum);

                SM4Utils sm4 = new SM4Utils();
                InterFace_MB_RGKEY rg  = dbContent.InterFace_MB_RGKEY.Where(x => x.appKey == where.appKey).FirstOrDefault();
                string _secretKey = rg.secret;
                string _rgcode = rg.rgcode;                
                
                #endregion
                var List = dbContent.tb_MXB糖尿病随访表.Join(dbContent.tb_健康档案, a => a.个人档案编号, b => b.个人档案编号, (a, b) => new { a, b.姓名, b.身份证号 });
                if (!string.IsNullOrEmpty(where.IDCard))
                {
                    //解密身份证号
                    string s身份证号 = sm4.Decrypt_ECB(where.IDCard, true, _secretKey);
                    List = List.Where(x => x.身份证号 == s身份证号);
                }
                if (!string.IsNullOrEmpty(_rgcode))
                {
                    //获取一下所属辖区机构,预处理
                    //var list机构 = dbContent.tb_机构信息.Where(a => a.机构编号 == _rgcode || a.上级机构 == _rgcode).Select(y => y.机构编号).ToList();
                    List = List.Where(x => dbContent.tb_机构信息.Where(a => a.机构编号 == _rgcode || a.上级机构 == _rgcode).Select(y => y.机构编号).Contains(x.a.所属机构));
                }
                if (!string.IsNullOrEmpty(where.startDate))
                {
                    List = List.Where(x => string.Compare(x.a.创建时间, where.startDate) >= 0);
                }
                if (!string.IsNullOrEmpty(where.endDate))
                {
                    //处理日期+1天
                    string endDate = endDate = Convert.ToDateTime(where.endDate).AddDays(1).ToString("yyyy-MM-dd");
                    List = List.Where(x => string.Compare(x.a.创建时间, endDate) <= 0);
                }
                //拼接开始和结束行
                List = List.OrderBy(x => x.a.ID).Skip(pag <= 0 ? 0 : pag - 1).Take(top - pag + 1);

                var accountList = List.Select(x => new tb_MXB糖尿病随访表Res
                {
                    id = x.a.ID.ToString(),
                    customerId = x.a.个人档案编号,
                    idCard = sm4.Encrypt_ECB(x.身份证号, true, _secretKey),
                    patientName = x.姓名,
                    //recordDetailHbpmsNo="",
                    offFollowReason = x.a.失访原因,
                    visitDate = x.a.发生时间,
                    visitWay = x.a.随访方式,
                    main = new Tnb_MainItem
                    {
                        curSymptom = x.a.目前症状,
                        curSymptomDescription = x.a.目前症状其他,
                        sbp = x.a.收缩压.ToString(),
                        dbp = x.a.舒张压.ToString(),
                        height = x.a.身高.ToString(),
                        weight = x.a.体重.ToString(),
                        targetWeight = x.a.体重2.ToString(),
                        bmi = x.a.BMI.ToString(),
                        targetBmi = x.a.BMI2.ToString(),
                        //heartRate = x.a.心率,
                        arteriopalmus = x.a.足背动脉搏动,
                        //arteriopalmusAddition = x.a.足背动脉搏动,
                        isSmoke = x.a.吸烟数量>0?"1":"0",
                        smokingQuantity = x.a.吸烟数量.ToString(),
                        targetSmokingQuantity = x.a.吸烟数量2.ToString(),
                        isDrink = x.a.饮酒数量>0?"1":"0",
                        drink = x.a.饮酒数量.ToString(),
                        targetDrink = x.a.饮酒数量2.ToString(),
                        training = x.a.运动频率.ToString(),
                        targetTraining = x.a.运动频率2.ToString(),
                        trainingMin = x.a.运动持续时间.ToString(),
                        targetTrainingMin = x.a.运动持续时间2.ToString(),
                        //rice = x.a.摄盐情况,
                        //targetRice = x.a.摄盐情况2,
                        mentality= x.a.心理调整,
                        compiance = x.a.遵医行为,
                        fbg = x.a.空腹血糖.ToString(),
                        hgb = x.a.糖化血红蛋白.ToString(),
                        res2 = x.a.辅助检查日期,
                        res3 = x.a.辅助检查日期,
                        //subCheck = x.a.辅助检查,
                        //drugsList = dbContent.tb_MXB糖尿病随访表_用药情况.Where(a => a.个人档案编号 == x.a.个人档案编号 && a.创建时间 == x.a.创建时间).Select(y=> new Tnb_DrugsListItem { drugName = y.药物名称,usage=y.用法}).ToList(),
                        medicationCompliance = x.a.服药依从性,
                        sideEffects = x.a.药物副作用,
                        effectsState = x.a.副作用详述,
                        lowEffects ="1",
                        resultType =x.a.本次随访分类,
                        nextManage = x.a.下一步管理措施,
                        //drugsAdviceList = dbContent.tb_MXB糖尿病随访表_用药调整.Where(a => a.个人档案编号 == x.a.个人档案编号 && a.创建时间 == x.a.创建时间).Select(y=>new Tnb_DrugsAdviceListItem { drugName = y.药物名称, usage = y.用法 }).ToList(),
                        nsferTreatmentReson = x.a.转诊原因,
                        nsferTreatmentOrganization = x.a.转诊科别,
                        nsferTreatmentContact = x.a.转诊联系人,
                        nsferTreatmentContactPho = x.a.转诊联系电话,
                        nsferTreatmentResult = x.a.转诊结果
                    },
                    nextVisitDate = x.a.下次随访时间,
                    followUserId = x.a.创建人,
                    followUserSign = x.a.随访医生,
                    inputOrganCode = x.a.所属机构,
                    inputOrgan = dbContent.tb_机构信息.Where(a => x.a.所属机构 == a.机构编号).Select(y => y.机构名称).FirstOrDefault(),
                    customerSign = x.姓名,
                    remarks = x.a.备注,
                    creattime = x.a.创建时间,
                    areaCode = x.a.所属机构.Substring(0,6)
                }).ToList();

                foreach (var item in accountList)
                {
                    //item.main.drugsList = new List<DrugsListItem> { new DrugsListItem { drugName = "test" } };
                    item.main.drugsList = dbContent.tb_MXB糖尿病随访表_用药情况.Where(a => a.个人档案编号 == item.customerId && a.创建时间 == item.creattime).Select(y => new Tnb_DrugsListItem { drugName = y.药物名称, usage = y.用法 }).ToList();

                    item.main.drugsAdviceList = dbContent.tb_MXB糖尿病随访表_用药调整.Where(a => a.个人档案编号 == item.customerId && a.创建时间 == item.creattime).Select(y => new Tnb_DrugsAdviceListItem { drugName = y.药物名称, usage = y.用法 }).ToList();
                }
                return accountList;
                //return accountList.Any() ? accountList.ToList() : null;

            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public MBJsonResult<string> Save(tb_MXB糖尿病随访表Input item)
        {
            if (item == null)
            {
                return ReturnMBResult.GetJsonResult(false, "传入数据为空！", "");
            }

            int rowsAffected = 0;//受影响行数
            try
            {
                SM4Utils sm4 = new SM4Utils();
                InterFace_MB_RGKEY rg = dbContent.InterFace_MB_RGKEY.Where(x => x.appKey == item.appKey).FirstOrDefault();
                string _secretKey = rg.secret;
                string _rgcode = rg.rgcode;
                if (!string.IsNullOrEmpty(item.idCard))
                {
                    //解密身份证号
                    item.idCard = sm4.Decrypt_ECB(item.idCard, true, _secretKey);
                }

                var accountModel = dbContent.tb_MXB糖尿病随访表.Where(s => s.个人档案编号.Equals(item.customerId) && s.发生时间.Equals(item.visitDate)).FirstOrDefault();
                if (accountModel != null)
                {
                    return ReturnMBResult.GetJsonResult(false, "传入数据已存在！", "");
                }
                else
                {
                    accountModel = new tb_MXB糖尿病随访表()
                    {
                        个人档案编号 = item.customerId,
                        身份证号 = item.idCard,
                        姓名 = item.patientName,
                        失访原因 = item.offFollowReason,
                        发生时间 = item.visitDate,
                        随访方式 = item.visitWay,
                        //main
                        目前症状 = item.main.curSymptom,
                        目前症状其他 = item.main.curSymptomDescription,
                        收缩压 = Convert.ToInt32(item.main.sbp),
                        舒张压 = Convert.ToInt32(item.main.dbp),
                        身高 = (decimal?)Convert.ToDouble(item.main.height),
                        体重 = (decimal?)Convert.ToDouble(item.main.weight),
                        体重2 = (decimal?)Convert.ToDouble(item.main.targetWeight),
                        BMI = (decimal?)Convert.ToDouble(item.main.bmi),
                        BMI2 = (decimal?)Convert.ToDouble(item.main.targetBmi),
                        //心率 = item.main.heartRate,
                        吸烟数量 = Convert.ToInt32(Convert.ToDouble(item.main.smokingQuantity)),
                        吸烟数量2 = Convert.ToInt32(Convert.ToDouble(item.main.targetSmokingQuantity)),
                        饮酒数量 = (decimal?)Convert.ToDouble(item.main.drink),
                        饮酒数量2 = (decimal?)Convert.ToDouble(item.main.targetDrink),
                        运动频率 = string.IsNullOrEmpty(item.main.training) ? 0 : Convert.ToInt32(item.main.training),
                        运动频率2 = string.IsNullOrEmpty(item.main.targetTraining) ? 0 : Convert.ToInt32(item.main.targetTraining),
                        运动持续时间 = string.IsNullOrEmpty(item.main.trainingMin) ? 0 : (decimal?)Convert.ToDouble(item.main.trainingMin),
                        运动持续时间2 = string.IsNullOrEmpty(item.main.targetTrainingMin) ? 0 : (decimal?)Convert.ToDouble(item.main.targetTrainingMin),
                        //摄盐情况 = item.main.saltQuantity,
                        //摄盐情况2 = item.main.targetSaltQuantity,
                        心理调整 = item.main.mentality,
                        遵医行为 = item.main.compiance,
                        空腹血糖 = string.IsNullOrEmpty(item.main.fbg) ? 0 : (decimal?)Convert.ToDouble(item.main.fbg),
                        糖化血红蛋白 = string.IsNullOrEmpty(item.main.hgb) ? 0 : (decimal?)Convert.ToDouble(item.main.hgb),                        
                        辅助检查日期 = item.main.res2,
                        降压药 = item.main.drugsList.Count >= 1 ? "1" : "2",
                        服药依从性 = item.main.medicationCompliance,
                        药物副作用 = item.main.sideEffects,
                        副作用详述 = item.main.effectsState,
                        本次随访分类 = item.main.resultType,
                        下一步管理措施 = item.main.nextManage,
                        用药调整意见 = item.main.drugsAdviceList.Count >= 1 ? "1" : "2",
                        转诊原因 = item.main.nsferTreatmentReson,
                        转诊科别 = item.main.nsferTreatmentOrganization,
                        转诊联系人 = item.main.nsferTreatmentContact,
                        转诊联系电话 = item.main.nsferTreatmentContactPho,
                        转诊结果 = item.main.nsferTreatmentResult,

                        下次随访时间 = item.nextVisitDate,
                        创建人 = item.followUserId,
                        随访医生 = item.followUserSign,
                        所属机构 = _rgcode,
                        创建机构 = _rgcode,
                        居民签名 = item.customerSign,
                        备注 = item.remarks,
                        创建时间 = item.creattime,
                        修改时间 = item.creattime,
                    };

                    dbContent.tb_MXB糖尿病随访表.Add(accountModel);
                    rowsAffected = dbContent.SaveChanges();
                    if (rowsAffected >= 1)
                    {
                        foreach (var list in item.main.drugsList)
                        {
                            var yy = new tb_MXB糖尿病随访表_用药情况()
                            {
                                个人档案编号 = item.customerId,
                                药物名称 = list.drugName,
                                用法 = "每日" + list.usage + "次， 每次" + list.dosage + "mg,口服",
                                创建时间 = item.creattime,
                            };


                            dbContent.tb_MXB糖尿病随访表_用药情况.Add(yy);
                        }
                        foreach (var list in item.main.drugsAdviceList)
                        {
                            var yy = new tb_MXB糖尿病随访表_用药调整()
                            {
                                个人档案编号 = item.customerId,
                                药物名称 = list.drugName,
                                用法 = "每日" + list.usage + "次， 每次" + list.dosage + "mg,口服",
                                创建时间 = item.creattime,
                            };


                            dbContent.tb_MXB糖尿病随访表_用药调整.Add(yy);
                        }
                        var row = dbContent.SaveChanges();
                    }
                }
                return (rowsAffected > 0) ? ReturnMBResult.GetJsonResult(true, "保存成功！", accountModel.ID.ToString()) : ReturnMBResult.GetJsonResult(false, "保存失败！", ""); ;
            }
            catch (Exception ex)
            {
                return ReturnMBResult.GetJsonResult(false, ex.Message, "");
            }
        }
    }
}
