﻿using DotNet.Model;
using DotNet.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace DotNet.DAL
{
    public class tb_MXB诊断提醒DAL
    {
        AtomEHRDbContent dbContent;

        public tb_MXB诊断提醒DAL()
        {
            dbContent = new AtomEHRDbContent();
        }

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public MBJsonResult<string> Save(tb_MXB诊断提醒Input item)
        {
            if (item == null)
            {
                return ReturnMBResult.GetJsonResult(false, "传入数据为空！", "");
            }

            int rowsAffected = 0;//受影响行数
            //string accountID = (!string.IsNullOrWhiteSpace(item.AccountID)) ? item.AccountID : Guid.NewGuid().ToString();
            try
            {
                SM4Utils sm4 = new SM4Utils();
                InterFace_MB_RGKEY rg = dbContent.InterFace_MB_RGKEY.Where(x => x.appKey == item.appKey).FirstOrDefault();
                string _secretKey = rg.secret;
                string _rgcode = rg.rgcode;
                if (!string.IsNullOrEmpty(item.idCard))
                {
                    //解密身份证号
                    item.idCard = sm4.Decrypt_ECB(item.idCard, true, _secretKey);
                }

                var accountModel = dbContent.tb_MXB诊断提醒.Where(s => s.患者身份证.Equals(item.idCard) && s.诊断日期.Equals(item.diagnosisDate) && s.诊断医院.Equals(item.hospitalName)).FirstOrDefault();
                if (accountModel != null)
                {
                    accountModel.患者姓名 = item.name;
                    accountModel.电话 = item.telphone;
                    accountModel.居住地址 = item.address;
                    accountModel.诊断名称 = item.diagnosisName;
                    accountModel.诊断科室 = item.deptName;
                    accountModel.诊断医生 = item.doctorName;

                    rowsAffected = dbContent.SaveChanges();
                }
                else
                {
                    accountModel = new tb_MXB诊断提醒();
                    accountModel.患者身份证 = item.idCard;
                    accountModel.患者姓名 = item.name;
                    accountModel.电话 = item.telphone;
                    accountModel.居住地址 = item.address;
                    accountModel.诊断名称 = item.diagnosisName;
                    accountModel.诊断日期 = item.diagnosisDate;
                    accountModel.诊断医院 = item.hospitalName;
                    accountModel.诊断科室 = item.deptName;
                    accountModel.诊断医生 = item.doctorName;
                    accountModel.appKey = item.appKey;
                    accountModel.所属机构 = _rgcode;
                    accountModel.random = item.random;
                    accountModel.sign = item.sign;

                    dbContent.tb_MXB诊断提醒.Add(accountModel);
                    rowsAffected = dbContent.SaveChanges();
                }
                return (rowsAffected > 0) ? ReturnMBResult.GetJsonResult(true, "保存成功！", "") : ReturnMBResult.GetJsonResult(false, "保存失败！", ""); ;
            }
            catch(Exception ex)
            {
                return ReturnMBResult.GetJsonResult(false, ex.Message, "");
            }
        }
    }
}
