﻿using DotNet.Model;
using DotNet.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotNet.DAL
{
    public class tb_健康档案DAL
    {
        AtomEHRDbContent dbContent;

        public tb_健康档案DAL()
        {
            dbContent = new AtomEHRDbContent();
        }

        /// <summary>
        /// 根据查询条件获取数据
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public List<tb_健康档案Res> GetListByWhere(tb_健康档案Searcher where)
        {
            try
            {
                #region 初始化
                int pag = 1;
                int top = 1;
                pag = string.IsNullOrEmpty(where.startNum) ? 1 : Convert.ToInt32(where.startNum);
                top = string.IsNullOrEmpty(where.endNum) ? 1 : Convert.ToInt32(where.endNum);

                SM4Utils sm4 = new SM4Utils();
                string _secretKey = string.Empty;
                string _rgcode = string.Empty;
                if (!string.IsNullOrEmpty(where.appKey))
                {
                    InterFace_MB_RGKEY rg = dbContent.InterFace_MB_RGKEY.Where(x => x.appKey == where.appKey).FirstOrDefault();
                    _secretKey = rg.secret;
                     _rgcode = rg.rgcode;
                }
                ////解密身份证号
                //string s身份证号 = string.Empty;
                //if (!string.IsNullOrEmpty(where.IDCard))
                //{
                //    s身份证号 = sm4.Decrypt_ECB(where.IDCard, true, _secretKey);
                //}
                ////获取一下所属辖区机构,预处理
                //var list机构 = dbContent.tb_机构信息.Where(a => a.机构编号 == _rgcode || a.上级机构 == _rgcode).Select(y => y.机构编号).ToList();
                ////处理日期+1天
                //string endDate = string.Empty;
                //if (!string.IsNullOrEmpty(where.endDate))
                //{
                //    endDate = Convert.ToDateTime(where.endDate).AddDays(1).ToString("yyyy-MM-dd");
                //}
                #endregion

                var List = dbContent.tb_健康档案.Where(c => c.档案状态 == "1" && dbContent.tb_健康档案_个人健康特征.Where(y => y.是否高血压 == "1" || y.是否糖尿病=="1").Select(y => y.个人档案编号).Contains(c.个人档案编号));
                if (!string.IsNullOrEmpty(where.IDCard))
                {
                    //解密身份证号
                    string s身份证号 = sm4.Decrypt_ECB(where.IDCard, true, _secretKey);
                    List = List.Where(x => x.个人档案编号 == dbContent.tb_健康档案.Where(a => a.身份证号 == s身份证号).Select(y => y.个人档案编号).FirstOrDefault());
                }
                if (!string.IsNullOrEmpty(_rgcode))
                {
                    //获取一下所属辖区机构,预处理
                    //var list机构 = dbContent.tb_机构信息.Where(a => a.机构编号 == _rgcode || a.上级机构 == _rgcode).Select(y => y.机构编号).ToList();
                    List = List.Where(x => dbContent.tb_机构信息.Where(a => a.机构编号 == _rgcode || a.上级机构 == _rgcode).Select(y => y.机构编号).Contains(x.所属机构));
                }
                if (!string.IsNullOrEmpty(where.startDate))
                {
                    List = List.Where(x => string.Compare(x.创建时间, where.startDate) >= 0);
                }
                if (!string.IsNullOrEmpty(where.endDate))
                {
                    //处理日期+1天
                    string endDate = endDate = Convert.ToDateTime(where.endDate).AddDays(1).ToString("yyyy-MM-dd");
                    List = List.Where(x => string.Compare(x.创建时间, endDate) <= 0);
                }
                //拼接开始和结束行
                List = List.OrderBy(x => x.ID).Skip(pag <= 0 ? 0 : pag - 1).Take(top - pag + 1);

                var accountList = List
                    //.Where(c=> c.档案状态=="1" && dbContent.tb_健康档案_个人健康特征.Where(y=>y.是否高血压=="1").Select(y=>y.个人档案编号).Contains(c.个人档案编号))
                    //.Where(c => string.IsNullOrEmpty(s身份证号) ? true : (c.身份证号 == s身份证号))
                    //.Where(c => list机构.Count==0 ? true : list机构.Contains(c.所属机构))
                    //.Where(x => string.IsNullOrEmpty(where.startDate) ? true : string.Compare(x.修改时间, where.startDate) >= 0)
                    //.Where(x => string.IsNullOrEmpty(where.endDate) ? true : string.Compare(x.修改时间, endDate) <= 0)
                    //.OrderBy(x=>x.ID)
                    //.Skip(pag - 1)
                    //.Take(top == 1 ? 1 : top - pag)
                    .Select(x=> new tb_健康档案Res
                    {
                        idCard = sm4.Encrypt_ECB( x.身份证号,true, _secretKey),
                        customerId = x.个人档案编号,
                        customerName = x.姓名,
                        telphon = x.本人电话,
                        familyName = x.联系人姓名,
                        familyTel = x.联系人电话,
                        sex = x.性别,
                        blood = dbContent.tb_健康档案_健康状态.Where(a => x.个人档案编号 == a.个人档案编号).Select(y => y.血型=="6"?"5": y.血型).FirstOrDefault(),
                        birthday = x.出生日期,
                        address = dbContent.tb_地区档案.Where(a=>a.地区编码==x.省).Select(y=>y.地区名称).FirstOrDefault()+ dbContent.tb_地区档案.Where(a => a.地区编码 == x.市).Select(y => y.地区名称).FirstOrDefault() + dbContent.tb_地区档案.Where(a => a.地区编码 == x.区).Select(y => y.地区名称).FirstOrDefault() + dbContent.tb_地区档案.Where(a => a.地区编码 == x.街道).Select(y => y.地区名称).FirstOrDefault() + x.居住地址,
                        regAddress = x.居住地址,
                        drugEllergies = dbContent.tb_健康档案_健康状态.Where(a => x.个人档案编号 == a.个人档案编号).Select(y => y.过敏史有无).FirstOrDefault(),
                        occupation = dbContent.tb_健康档案_健康状态.Where(a => x.个人档案编号 == a.个人档案编号).Select(y => y.暴露史).FirstOrDefault(),
                        diseaseList = dbContent.tb_健康档案_既往病史
                                    .Where(a => x.个人档案编号 == a.个人档案编号 && a.疾病类型== "疾病" && a.D_JBBM !=null)
                                    .Select(y => new diseaseList 
                                    { 
                                        diseaseName = y.D_JBBM , 
                                        diseaseDate =y.日期 + "-01"
                                    }).ToList(),
                        familyDiseaseList = dbContent.tb_健康档案_家族病史
                                    .Where(a => x.个人档案编号 == a.个人档案编号 )
                                    .Select(y => new familyDiseaseList 
                                    { 
                                        MyProperty = y.家族关系, 
                                        diseaseName = y.D_JBBM 
                                    }).ToList(),
                        operationList = dbContent.tb_健康档案_既往病史
                                    .Where(a => x.个人档案编号 == a.个人档案编号 && a.疾病类型 == "手术")
                                    .Select(y => new operationList 
                                    { 
                                        operationName = y.D_JBBM, 
                                        operationDate = y.日期 + "-01"
                                    }).ToList(),
                        traumaList = dbContent.tb_健康档案_既往病史
                                    .Where(a => x.个人档案编号 == a.个人档案编号 && a.疾病类型 == "外伤")
                                    .Select(y => new traumaList 
                                    { 
                                        traumaName = y.D_JBBM, 
                                        traumaDate = y.日期 + "-01"
                                    }).ToList(),
                        transfusionList = dbContent.tb_健康档案_既往病史
                                    .Where(a => x.个人档案编号 == a.个人档案编号 && a.疾病类型 == "输血")
                                    .Select(y => new transfusionList 
                                    { 
                                        transReason = y.D_JBBM, 
                                        MyProperty = y.日期 + "-01"
                                    }).ToList(),
                        genetic = dbContent.tb_健康档案_健康状态.Where(a => x.个人档案编号 == a.个人档案编号).Select(y => y.遗传病史有无).FirstOrDefault(),
                        organizationId = x.所属机构,
                        organizationName = dbContent.tb_机构信息.Where(a => x.所属机构 == a.机构编号).Select(y => y.机构名称).FirstOrDefault(),
                        createTime = x.创建时间,
                        updateTime = x.修改时间

                    });
                return (accountList.Any()) ? accountList.ToList() : null;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        //public 
        /// <summary>
        /// 根据查询条件获取数据
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public List<tb_健康档案Res> GetListByWhere2(tb_健康档案Searcher where)
        {
            try
            {
                string SQL个人档案 = @"select top 1000 身份证号 as idCard,个人档案编号 as customerId,姓名 as customerName,本人电话 as telphon,联系人姓名 as familyName,联系人电话 as familyTel,性别编码 as sex,血型 as blood,出生日期 as birthday,(省+市+区+街道+居住地址) as address,居住地址 as regAddress,'无' as drugEllergies,'无' as occupation,'' as genetic,所属机构 as organizationId,所属机构名称 as organizationName,创建时间 as createTime,修改时间 as updateTime from View_个人信息表 where 身份证号=@IDCard  ";
                var list = dbContent.Query<tb_健康档案Res>(new DbContextSqlQueryCommands
                {
                    //调用
                    Sql = new DbContextSqlQueryCommand(SQL个人档案, new
                    {
                        IDCard = string.Join(",", where.IDCard),
                    })
                });
                foreach (var tb in list)
                {
                    tb.diseaseList = dbContent.tb_健康档案_既往病史.Where(a => tb.customerId == a.个人档案编号 && a.疾病类型 == "疾病").Select(y => new diseaseList { diseaseName = y.D_JBBM, diseaseDate = y.日期 }).ToList();
                    tb.familyDiseaseList = dbContent.tb_健康档案_家族病史.Where(a => tb.customerId == a.个人档案编号).Select(y => new familyDiseaseList { MyProperty = y.家族关系, diseaseName = y.D_JBBM }).ToList();
                    tb.operationList = dbContent.tb_健康档案_既往病史.Where(a => tb.customerId == a.个人档案编号 && a.疾病类型 == "手术").Select(y => new operationList { operationName = y.D_JBBM, operationDate = y.日期 }).ToList();
                    tb.traumaList = dbContent.tb_健康档案_既往病史.Where(a => tb.customerId == a.个人档案编号 && a.疾病类型 == "外伤").Select(y => new traumaList { traumaName = y.D_JBBM, traumaDate = y.日期 }).ToList();
                    tb.transfusionList = dbContent.tb_健康档案_既往病史.Where(a => tb.customerId == a.个人档案编号 && a.疾病类型 == "输血").Select(y => new transfusionList { transReason = y.D_JBBM, MyProperty = y.日期 }).ToList();
                }
                return (list.Any()) ? list.ToList() : null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        /// <summary>
        /// 根据身份证返回是否
        /// </summary>
        /// <param name="IDCard"></param>
        /// <returns></returns>
        public bool GetGRDNForID(string IDCard)
        {
            var List = dbContent.tb_健康档案.Where(c => c.身份证号 == IDCard).ToList();
            return List.Count > 0 ? true : false;
        }
    }
}
