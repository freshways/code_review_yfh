﻿using DotNet.Model;
using DotNet.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace DotNet.DAL
{
    public class tb_健康体检DAL
    {
        AtomEHRDbContent dbContent;

        public tb_健康体检DAL()
        {
            dbContent = new AtomEHRDbContent();
        }


        /// <summary>
        /// 根据查询条件获取数据
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public List<tb_健康体检Res> GetListByWhere(tb_健康体检Searcher where)
        {
            try
            {
                #region 预处理
                int pag = 1;
                int top = 1;
                pag = string.IsNullOrEmpty(where.startNum) ? 1 : Convert.ToInt32(where.startNum);
                top = string.IsNullOrEmpty(where.endNum) ? 1 : Convert.ToInt32(where.endNum);

                SM4Utils sm4 = new SM4Utils();
                InterFace_MB_RGKEY rg = dbContent.InterFace_MB_RGKEY.Where(x => x.appKey == where.appKey).FirstOrDefault();
                string _secretKey = rg.secret;
                string _rgcode = rg.rgcode;
                #endregion
                var List = dbContent.tb_健康体检.Where(x=>true) ;
                if (!string.IsNullOrEmpty(where.IDCard))
                {
                    //解密身份证号
                    string s身份证号 = sm4.Decrypt_ECB(where.IDCard, true, _secretKey);
                    List = List.Where(x => x.个人档案编号 == dbContent.tb_健康档案.Where(a => a.身份证号 == s身份证号).Select(y => y.个人档案编号).FirstOrDefault());
                }
                if (!string.IsNullOrEmpty(_rgcode))
                {
                    //获取一下所属辖区机构,预处理
                    //var list机构 = dbContent.tb_机构信息.Where(a => a.机构编号 == _rgcode || a.上级机构 == _rgcode).Select(y => y.机构编号).ToList();
                    List = List.Where(x => dbContent.tb_机构信息.Where(a => a.机构编号 == _rgcode || a.上级机构 == _rgcode).Select(y => y.机构编号).Contains(x.所属机构));
                }
                if (!string.IsNullOrEmpty(where.startDate))
                {
                    List = List.Where(x => string.Compare(x.创建时间, where.startDate) >= 0);
                }
                if (!string.IsNullOrEmpty(where.endDate))
                {
                    //处理日期+1天
                    string endDate = endDate = Convert.ToDateTime(where.endDate).AddDays(1).ToString("yyyy-MM-dd");
                    List = List.Where(x => string.Compare(x.创建时间, endDate) <= 0);
                }
                //拼接开始和结束行
                List = List.OrderBy(x => x.ID).Skip(pag <= 0 ? 0 : pag - 1).Take(top - pag + 1);

                var accountList = List.Select(x => new tb_健康体检Res
                {
                    idCard = sm4.Encrypt_ECB(dbContent.tb_健康档案.Where(a => a.个人档案编号 == x.个人档案编号).Select(y => y.身份证号).FirstOrDefault(), true, _secretKey),
                    customerId = x.个人档案编号,
                    curSymptom = x.症状,
                    height = x.身高.ToString(),
                    weight = x.体重.ToString(),
                    waist = x.腰围.ToString(),
                    hipLine = "",
                    exerciseFre = x.锻炼频率,
                    exerciseTime = x.每次锻炼时间,
                    exerciseType = x.锻炼方式,
                    diet = x.饮食习惯,
                    smokeStatus = x.吸烟状况,
                    drinkStatus = x.饮酒频率,
                    eyeAbnormal = x.眼底异常,
                    pulse = x.脉搏.ToString(),
                    heartrate = x.心律,
                    legEdema = x.下肢水肿,
                    foodDorsum = x.足背动脉搏动,
                    fbg = x.空腹血糖,
                    sugar2H = x.餐后2H血糖,
                    healthList = new List<healthList> { new healthList
                    {
                        brainDisease = x.脑血管疾病,
                        kidneyDisease = x.肾脏疾病,
                        cardioDisease = x.心脏疾病,
                        eyeDisease = x.眼部疾病,
                        nervousDisease = x.神经系统疾病,
                        otherDisease = x.其他系统疾病
                    } },
                    drugList = dbContent.tb_健康体检_用药情况表.Where(a => a.个人档案编号 == x.个人档案编号 && a.创建时间.Substring(0,10)==x.创建时间.Substring(0,10))
                    .Select(y => new drugList 
                    { 
                        drugName = y.药物名称, 
                        drugUse = y.用法, 
                        drugDose = y.用量, 
                        drugDate = "", 
                        drugCompliance = y.服药依从性 
                    }).ToList()

                });
                return accountList.ToList();

                #region 上线运行后删除本代码
                ////解密身份证号
                //string s身份证号 = string.Empty;
                //if (!string.IsNullOrEmpty(where.IDCard))
                //{
                //    s身份证号 = sm4.Decrypt_ECB(where.IDCard, true, _secretKey);
                //}
                ////由于联合个人档案查询比较慢,所以有身份证的时候,先取一下档案号
                //string s个人档案编号 = string.Empty;
                //if (!string.IsNullOrEmpty(where.IDCard))
                //{
                //    s个人档案编号 = dbContent.tb_健康档案.Where(a => a.身份证号 == s身份证号).Select(y => y.个人档案编号).FirstOrDefault();
                //}
                ////获取一下所属辖区机构,预处理
                //var list机构 = dbContent.tb_机构信息.Where(a => a.机构编号 == _rgcode || a.上级机构 == _rgcode).Select(y => y.机构编号).ToList();
                ////处理日期+1天
                //string endDate = string.Empty;
                //if (!string.IsNullOrEmpty(where.endDate))
                //{
                //    endDate = Convert.ToDateTime(where.endDate).AddDays(1).ToString("yyyy-MM-dd");
                //}
                //var accountList = dbContent.tb_健康体检
                //    .Where(c => string.IsNullOrEmpty(s个人档案编号) ? true : c.个人档案编号 == s个人档案编号)
                //    .Where(c => list机构.Count<=0 ? true : list机构.Contains(c.所属机构))
                //    .Where(x=> string.IsNullOrEmpty(where.startDate) ? true : string.Compare(x.创建时间, where.startDate) >= 0)
                //    .Where(x=> string.IsNullOrEmpty(where.endDate) ? true : string.Compare(x.创建时间, endDate) <= 0)
                //    .OrderBy(x=>x.ID)
                //    .Skip(pag - 1)
                //    .Take(top == 1 ? 1 : top - pag)
                //    .Select(x=> new tb_健康体检Res
                //    {
                //        idCard = sm4.Encrypt_ECB(dbContent.tb_健康档案.Where(a => a.个人档案编号 == x.个人档案编号).Select(y => y.身份证号).FirstOrDefault(),true, _secretKey),
                //        customerId = x.个人档案编号,
                //        curSymptom = x.症状,
                //        height = x.身高.ToString(),
                //        weight = x.体重.ToString(),
                //        waist = x.腰围.ToString(),
                //        hipLine = "",
                //        exerciseFre = x.锻炼频率,
                //        exerciseTime = x.每次锻炼时间,
                //        exerciseType = x.锻炼方式,
                //        diet = x.饮食习惯,
                //        smokeStatus = x.吸烟状况,
                //        drinkStatus = x.饮酒频率,
                //        eyeAbnormal = x.眼底异常,
                //        pulse = x.脉搏.ToString(),
                //        heartrate = x.心律,
                //        legEdema = x.下肢水肿,
                //        foodDorsum = x.足背动脉搏动,
                //        fbg = x.空腹血糖,
                //        sugar2H = x.餐后2H血糖,
                //        healthList= new List<healthList> { new healthList { brainDisease = x.脑血管疾病, kidneyDisease = x.肾脏疾病, cardioDisease = x.心脏疾病, eyeDisease = x.眼部疾病, nervousDisease = x.神经系统疾病, otherDisease = x.其他系统疾病 } },
                //        drugList = dbContent.tb_健康体检_用药情况表.Where(a => a.个人档案编号 == x.个人档案编号 && a.创建时间 == x.创建时间).Select(y => new drugList { drugName = y.药物名称, drugUse = y.用法, drugDose = y.用量, drugDate = "", drugCompliance = y.服药依从性 }).ToList()

                //    });
                //return (accountList.Any()) ? accountList.ToList() : null; 
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        //public 
        /// <summary>
        /// 根据查询条件获取数据
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public List<tb_健康体检Res> GetListByWhere2(tb_健康体检Searcher where)
        {
            try
            {
                string SQL = @"SELECT TOP (1) ID AS idCard ,个人档案编号 AS customerId ,症状 AS curSymptom ,身高 AS height ,体重 AS weight ,腰围 AS waist ,'臀围' AS hipLine ,锻炼频率 AS exerciseFre ,每次锻炼时间 AS exerciseTime ,锻炼方式 AS exerciseType ,饮食习惯 AS diet ,吸烟状况 AS smokeStatus ,饮酒频率 AS drinkStatus ,眼底 AS eyeAbnormal ,脉搏 AS pulse ,心律 AS heartrate ,下肢水肿 AS legEdema ,足背动脉搏动 AS foodDorsum ,空腹血糖 AS fbg ,餐后2H血糖 AS sugar2H ,血压右侧1 AS sbp ,血压右侧2 AS dbp ,脑血管疾病 AS brainDisease ,肾脏疾病 AS kidneyDisease ,心脏疾病 AS cardioDisease ,眼部疾病 AS eyeDisease ,神经系统疾病 AS nervousDisease ,其他系统疾病 AS otherDisease FROM tb_健康体检  where 身份证号=@IDCard  ";
                var list = dbContent.Query<tb_健康体检Res>(new DbContextSqlQueryCommands
                {
                    //调用
                    Sql = new DbContextSqlQueryCommand(SQL, new
                    {
                        IDCard = string.Join(",", where.IDCard),
                    })
                });
                //foreach (var tb in list)
                //{
                //}
                return (list.Any()) ? list.ToList() : null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
