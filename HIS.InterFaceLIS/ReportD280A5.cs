﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.IO;
//using lisbll;

namespace HIS.InterFaceLIS
{
    public partial class ReportD280A5 : DevExpress.XtraReports.UI.XtraReport
    {
        //public ReportD280A5()
        //{
        //    InitializeComponent();
        //}

        private string strJYid = string.Empty;

        private bool printSuccess = false;

        public bool GetPrintResult()
        {
            return printSuccess;
        }
        //public ReportD280A5(string strid, DataTable dtDataSource)
        public ReportD280A5(string strid, DataTable dtReport, DataTable dtDetail)
        {
            InitializeComponent();

            strJYid = strid;
            string brnl = "0", brxm = "", brxb = "";
            brxm = dtReport.Rows[0]["姓名"].ToString();
            brxb = dtReport.Rows[0]["性别"].ToString();
            string sql = @"select dbo.fn_getage(出生日期) 年龄,病人姓名,性别 from ZY病人信息 where 住院号码='" + dtReport.Rows[0]["住院号"].ToString() + "' ";
            try
            {
                DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataTable(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, sql);
                if (dt != null && dt.Rows.Count > 0)
                {
                    brnl = dt.Rows[0]["年龄"].ToString();
                    brxm = dt.Rows[0]["病人姓名"].ToString();
                    brxb = dt.Rows[0]["性别"].ToString();
                }
            }
            catch
            {
                brnl = dtReport.Rows[0]["年龄"].ToString();
            }
            //设置报告头信息
            xrLabelTitle.Text = dtReport.Rows[0]["医院名称"].ToString() + dtReport.Rows[0]["报表名称"].ToString();
            xrLabel姓名.Text = brxm;//dtReport.Rows[0]["姓名"].ToString();
            xrLabel性别.Text = brxb;// dtReport.Rows[0]["性别"].ToString();
            xrLabel年龄.Text = brnl + dtReport.Rows[0]["年龄单位"].ToString();//dtReport.Rows[0]["年龄"].ToString() 
            xrLabel科室.Text = dtReport.Rows[0]["科室"].ToString();
            xrLabel床号.Text = dtReport.Rows[0]["床号"].ToString();
            xrLabel样本类型.Text = dtReport.Rows[0]["样本类型"].ToString();
            xrLabel患者类别.Text = dtReport.Rows[0]["病人类别"].ToString();
            xrLabel病历号.Text = dtReport.Rows[0]["住院号"].ToString();
            xrLabel临床诊断.Text = dtReport.Rows[0]["疾病名称"].ToString();
            xrLabel样本号.Text = dtReport.Rows[0]["样本号"].ToString();
            xrLabel条码.Text = dtReport.Rows[0]["条码"].ToString();
            xrLabel送检医师.Text = dtReport.Rows[0]["申请医师"].ToString();

            xrLabel备注.Text = dtReport.Rows[0]["备注"].ToString();
            xrLabel检验时间.Text = dtReport.Rows[0]["送检日期"].ToString();
            xrLabel检验者.Text = dtReport.Rows[0]["检验医师"].ToString();

            xrLabel报告时间.Text = dtReport.Rows[0]["报告时间"].ToString();
            xrLabel审核者.Text = dtReport.Rows[0]["审核者"].ToString();

            xrTableCell序号.DataBindings.Add("Text", null, "forder_by");
            xrTableCell项目代码.DataBindings.Add("Text", null, "fitem_code");
            xrTableCell项目名称.DataBindings.Add("Text", null, "fitem_name");
            xrTableCell结果.DataBindings.Add("Text", null, "fvalue");
            xrTableCell单位.DataBindings.Add("Text", null, "fitem_unit");
            xrTableCell标记.DataBindings.Add("Text", null, "fitem_badge");
            xrTableCell参考值.DataBindings.Add("Text", null, "fitem_ref");

            #region 取审核人的手签图片
            try
            {
                string strPIC = "select  top 1 手签照片 from pubUser where  (用户名='" + xrLabel审核者.Text + "') ";
                Image _image = null;
                System.Data.SqlClient.SqlDataReader reader = HIS.Model.Dal.SqlHelper.ExecuteReader(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, strPIC);

                reader.Read();
                if (reader.HasRows)
                {
                    MemoryStream ms = new MemoryStream((byte[])reader["手签照片"]);
                    _image = System.Drawing.Image.FromStream(ms, true);
                }

                if (_image == null)
                { //如果没取到手签图片则隐藏，显示系统默认的审核人
                    xrLabel审核者.Visible = true;
                    xrPic审核者.Visible = false;
                }
                else
                    xrPic审核者.Image = _image;
            }
            catch
            {//如果没取到手签图片则隐藏，显示系统默认的审核人
                xrLabel审核者.Visible = true;
                xrPic审核者.Visible = false;
            }
            #endregion

            this.DataSource = dtDetail;
        }


        private void ReportD280A5_PrintProgress(object sender, DevExpress.XtraPrinting.PrintProgressEventArgs e)
        {
            if (!printSuccess && !(string.IsNullOrWhiteSpace(strJYid)))
            {
                ApplyBLL bllReport = new ApplyBLL();
                //ww.lis.lisbll.sam.jybll bllReport = new ww.lis.lisbll.sam.jybll();
                bllReport.BllReportUpdatePrintTimeNew(strJYid);
            }
            printSuccess = true;
        }

    }
}
