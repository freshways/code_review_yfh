using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
//using ww.wwf.dao;
//using ww.wwf.wwfbll;
//using ww.wwf.com;
using System.Collections;
using System.Data.SqlClient;

namespace HIS.InterFaceLIS
{
 
    /// <summary>
    /// 检验申请 规则
    /// </summary>
    public class ApplyBLL : DAOWWF
    {
        string table_SAM_APPLY="";
        string table_SAM_JY_RESULT = "";
        string table_SAM_SAMPLE = "";
        public ApplyBLL()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //           
            table_SAM_APPLY = "SAM_APPLY";
            table_SAM_JY_RESULT = "SAM_JY_RESULT";
            table_SAM_SAMPLE = "SAM_SAMPLE";
        }       

        #region  申请
        /// <summary>
        /// 申请 增加
        /// </summary>
        /// <param name="rowApply">主表</param>
        /// <param name="dtResult">结果表</param>
        /// <returns>返回true表示成功</returns>
        public string BllApplyAdd(ApplyModel model, DataTable dtResult, string fitem_group)
        {
            string strReturn = string.Empty;
            IList addList = new ArrayList();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into " + table_SAM_APPLY + "(");
            strSql.Append("fapply_id,fjytype_id,fsample_type_id,fjz_flag,fcharge_flag,fcharge_id,fstate,fjyf,fapply_user_id,fapply_dept_id,fapply_time,ftype_id,fhz_id,fhz_zyh,fsex,fname,fvolk,fhymen,fage,fjob,fgms,fjob_org,fadd,ftel,fage_unit,fage_year,fage_month,fage_day,fward_num,froom_num,fbed_num,fblood_abo,fblood_rh,fjymd,fdiagnose,fheat,fxyld,fcyy,fxhdb,fcreate_user_id,fcreate_time,fupdate_user_id,fupdate_time,fitem_group,fremark");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + model.fapply_id + "',");
            strSql.Append("'" + model.fjytype_id + "',");
            strSql.Append("'" + model.fsample_type_id + "',");
            strSql.Append("" + model.fjz_flag + ",");
            strSql.Append("" + model.fcharge_flag + ",");
            strSql.Append("'" + model.fcharge_id + "',");
            strSql.Append("'" + model.fstate + "',");
            strSql.Append("" + model.fjyf + ",");
            strSql.Append("'" + model.fapply_user_id + "',");
            strSql.Append("'" + model.fapply_dept_id + "',");
            strSql.Append("'" + model.fapply_time + "',");
            strSql.Append("'" + model.ftype_id + "',");
            strSql.Append("'" + model.fhz_id + "',");
            strSql.Append("'" + model.fhz_zyh + "',");
            strSql.Append("'" + model.fsex + "',");
            strSql.Append("'" + model.fname + "',");
            strSql.Append("'" + model.fvolk + "',");
            strSql.Append("'" + model.fhymen + "',");
            strSql.Append("" + model.fage + ",");
            strSql.Append("'" + model.fjob + "',");
            strSql.Append("'" + model.fgms + "',");
            strSql.Append("'" + model.fjob_org + "',");
            strSql.Append("'" + model.fadd + "',");
            strSql.Append("'" + model.ftel + "',");
            strSql.Append("'" + model.fage_unit + "',");
            strSql.Append("" + model.fage_year + ",");
            strSql.Append("" + model.fage_month + ",");
            strSql.Append("" + model.fage_day + ",");
            strSql.Append("'" + model.fward_num + "',");
            strSql.Append("'" + model.froom_num + "',");
            strSql.Append("'" + model.fbed_num + "',");
            strSql.Append("'" + model.fblood_abo + "',");
            strSql.Append("'" + model.fblood_rh + "',");
            strSql.Append("'" + model.fjymd + "',");
            strSql.Append("'" + model.fdiagnose + "',");
            strSql.Append("" + model.fheat + ",");
            strSql.Append("" + model.fxyld + ",");
            strSql.Append("'" + model.fcyy + "',");
            strSql.Append("" + model.fxhdb + ",");
            strSql.Append("'" + model.fcreate_user_id + "',");
            strSql.Append("'" + model.fcreate_time + "',");
            strSql.Append("'" + model.fupdate_user_id + "',");
            strSql.Append("'" + model.fupdate_time + "',");
            strSql.Append("'" + fitem_group + "',");
            strSql.Append("'" + model.fremark + "'");
            strSql.Append(")");
            addList.Add(strSql.ToString());
            StringBuilder strSqlSam = new StringBuilder();
            strSqlSam.Append("insert into " + table_SAM_SAMPLE + "(");
            strSqlSam.Append("fsample_id,fapply_id,fsample_barcode,fexamine_flag,fprint_flag,fprint_count,fread_flag,fstate");
            strSqlSam.Append(")");
            strSqlSam.Append(" values (");
            strSqlSam.Append("'" + this.DbGuid() + "',");
            strSqlSam.Append("'" + model.fapply_id + "',");
            strSqlSam.Append("'" + model.fapply_id + "',");
            strSqlSam.Append("0,");
            strSqlSam.Append("0,");
            strSqlSam.Append("0,");
            strSqlSam.Append("0,");      
            strSqlSam.Append("'" + model.fstate + "'");
            strSqlSam.Append(")");
            addList.Add(strSqlSam.ToString());

            for (int i = 0; i < dtResult.Rows.Count; i++)
            {
                StringBuilder strSql_Result = new StringBuilder();
                strSql_Result.Append("insert into " + table_SAM_JY_RESULT + "(");
                strSql_Result.Append("fresult_id,fapply_id,fitem_id,fitem_code,fitem_name,fitem_unit,forder_by");
                strSql_Result.Append(")");
                strSql_Result.Append(" values (");
                strSql_Result.Append("'" + this.DbGuid() + "',");
                strSql_Result.Append("'" + model.fapply_id + "',");
                strSql_Result.Append("'" + dtResult.Rows[i]["fitem_id"] + "',");
                strSql_Result.Append("'" + dtResult.Rows[i]["fitem_code"] + "',");
                strSql_Result.Append("'" + dtResult.Rows[i]["fitem_name"] + "',");
                strSql_Result.Append("'" + dtResult.Rows[i]["fitem_unit"] + "',");
                strSql_Result.Append("'" + dtResult.Rows[i]["forder_by"] + "'");
                strSql_Result.Append(")");
                addList.Add(strSql_Result.ToString());
            }
            addList.Add("DELETE FROM SAM_APPLY_BCODE WHERE  (fapply_id = '" + model.fapply_id + "')");

            try
            {
                int irow = HIS.Model.Dal.SqlHelper.ExecuteNonQuery(HIS.COMM.ClassDBConnstring.sLISConn, CommandType.Text, addList.ToString());
                if (irow > 0)
                {
                    strReturn = "true";
                }
            }
            catch (Exception ex)
            {
                strReturn = "发生异常。\n" + ex.ToString();
            }
            return strReturn;
        }

        /// <summary>
        /// 申请 增加 本方法于20150623创建  与上面方法BllApplyAdd的区别：
        /// ①删除对SAM_JY_RESULT、SAM_APPLY_BCODE两个数据表的操作
        /// </summary>
        /// <param name="rowApply">主表</param>
        /// <param name="dtResult">结果表</param>
        /// <returns>返回true表示成功</returns>
        public string BllApplyAdd_New(ApplyModel model)
        {
            string strReturn = string.Empty;
            IList addList = new ArrayList();
            StringBuilder strSql = new StringBuilder();
//            strSql.Append(@"declare @applyid varchar(32) 
//EXEC [GetApplyNo] '" + model.ftype_id + "','" + DateTime.Now.ToString("yyMMdd") + @"',@applyid output 
            strSql.Append("insert into " + table_SAM_APPLY + "(");
            strSql.Append(@"fapply_id,fjytype_id,fsample_type_id,fjz_flag,fcharge_flag,fcharge_id,fstate,fjyf,fapply_user_id,fapply_dept_id,
fapply_time,ftype_id,fhz_id,fhz_zyh,fsex,fname,fvolk,fhymen,fage,fjob,fgms,fjob_org,fadd,ftel,fage_unit,fage_year,fage_month,fage_day,fward_num,
froom_num,fbed_num,fblood_abo,fblood_rh,fjymd,fdiagnose,fheat,fxyld,fcyy,fxhdb,fcreate_user_id,fcreate_time,fupdate_user_id,fupdate_time,fitem_group,fremark,his_recordid,sfzh");//changed by wjz 20160122 追加了身份证号字段
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + model.fapply_id + "',");
            strSql.Append("'" + model.fjytype_id + "',");
            strSql.Append("'" + model.fsample_type_id + "',");
            strSql.Append("" + model.fjz_flag + ",");
            strSql.Append("" + model.fcharge_flag + ",");
            strSql.Append("'" + model.fcharge_id + "',");
            strSql.Append("'" + model.fstate + "',");
            strSql.Append("" + model.fjyf + ",");
            strSql.Append("'" + model.fapply_user_id + "',");
            strSql.Append("'" + model.fapply_dept_id + "',");
            strSql.Append("'" + model.fapply_time + "',");
            strSql.Append("'" + model.ftype_id + "',");
            strSql.Append("'" + model.fhz_id + "',");
            strSql.Append("'" + model.fhz_zyh + "',");
            strSql.Append("'" + model.fsex + "',");
            strSql.Append("'" + model.fname + "',");
            strSql.Append("'" + model.fvolk + "',");
            strSql.Append("'" + model.fhymen + "',");
            strSql.Append("" + model.fage + ",");
            strSql.Append("'" + model.fjob + "',");
            strSql.Append("'" + model.fgms + "',");
            strSql.Append("'" + model.fjob_org + "',");
            strSql.Append("'" + model.fadd + "',");
            strSql.Append("'" + model.ftel + "',");
            strSql.Append("'" + model.fage_unit + "',");
            strSql.Append("" + model.fage_year + ",");
            strSql.Append("" + model.fage_month + ",");
            strSql.Append("" + model.fage_day + ",");
            strSql.Append("'" + model.fward_num + "',");
            strSql.Append("'" + model.froom_num + "',");
            strSql.Append("'" + model.fbed_num + "',");
            strSql.Append("'" + model.fblood_abo + "',");
            strSql.Append("'" + model.fblood_rh + "',");
            strSql.Append("'" + model.fjymd + "',");
            strSql.Append("'" + model.fdiagnose + "',");
            strSql.Append("" + model.fheat + ",");
            strSql.Append("" + model.fxyld + ",");
            strSql.Append("'" + model.fcyy + "',");
            strSql.Append("" + model.fxhdb + ",");
            strSql.Append("'" + model.fcreate_user_id + "',");
            strSql.Append("convert(varchar(20), getdate(),20),");
            strSql.Append("'" + model.fupdate_user_id + "',");
            strSql.Append("convert(varchar(20), getdate(),20),");
            strSql.Append("'" + model.F检验项目名称 + "',");
            strSql.Append("'" + model.fremark + "',");
            strSql.Append("'" + model.His申请单号 + "'");
            //add by wjz 20160122 追加身份证号 ▽
            strSql.Append(",'" + model.Sfzh + "'");
            //add by wjz 20160122 追加身份证号 △
            strSql.Append(")");
            addList.Add(strSql.ToString());
            
            StringBuilder strSqlSam = new StringBuilder();
            strSqlSam.Append("insert into " + table_SAM_SAMPLE + "(");
            strSqlSam.Append("fsample_id,fapply_id,fsample_barcode,fexamine_flag,fprint_flag,fprint_count,fread_flag,fstate");
            strSqlSam.Append(")");
            strSqlSam.Append(" values (");
            strSqlSam.Append("'" + this.DbGuid() + "',");
            strSqlSam.Append("'" + model.fapply_id + "',");
            strSqlSam.Append("'" + model.fapply_id + "',");
            strSqlSam.Append("0,");
            strSqlSam.Append("0,");
            strSqlSam.Append("0,");
            strSqlSam.Append("0,");
            strSqlSam.Append("'" + model.fstate + "'");
            strSqlSam.Append(") ");
            addList.Add(strSqlSam.ToString());

            try
            {
                using(SqlConnection con = new SqlConnection(HIS.COMM.ClassDBConnstring.sLISConn))
                {
                    con.Open();
                    SqlTransaction Tcon = con.BeginTransaction();
                    int iCnt = HIS.Model.Dal.SqlHelper.ExecuteNonQuery(Tcon, CommandType.Text, strSql.ToString());
                    if(iCnt>0)
                        HIS.Model.Dal.SqlHelper.ExecuteNonQuery(Tcon, CommandType.Text, strSqlSam.ToString());
                    Tcon.Commit();

                    if (iCnt > 0)
                    {
                        strReturn = "true";
                        //2015-08-21 09:58:10 yufh 修改同步检验单状态存储
                        string strProc = @" 
                    exec dbo.[uSp_化验申请单状态更新] '打印','" + model.fapply_id + "','" + model.His申请单号 + "'";
                        HIS.Model.Dal.SqlHelper.ExecuteNonQuery(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, strProc);
                    }
                }                
            }
            catch (Exception ex)
            {
                strReturn = "发生异常。\n" + ex.ToString();
            }
            
            return strReturn;
        }

        #endregion

        public DataTable BllSamplingMainDTFromLis(string strWhere)
        {
            string strSql = "select fapply_id, fstate, ftype_id, fhz_id, fhz_zyh,his_recordid from SAM_APPLY where 1=1 ";
            //if (!(string.IsNullOrWhiteSpace(strWhere)))
            //{
            strSql += " and " + strWhere;
            //}
            return HIS.Model.Dal.SqlHelper.ExecuteDataTable(HIS.COMM.ClassDBConnstring.sLISConn, CommandType.Text, strSql);
            //return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql).Tables[0];
        }

        /// <summary>
        /// 保存检验申请条码号
        /// </summary>
        /// <param name="strfapply_id"></param>
        /// <param name="imgfbcode"></param>
        /// <returns></returns>
        public int BllApplyBCodeAdd(String strfapply_id, Byte[] imgfbcode)
        {
            int intR = 0;
            //ww.wwf.wwfbll.WWFInit.strDBConn
            //intR = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbApplyBcodeAdd(ww.wwf.wwfbll.WWFInit.strDBConn, "SAM_APPLY_BCODE_ADD", strfapply_id, imgfbcode);
            return intR;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataTable GetListByJYID(string jyid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select '" + WEISHENG.COMM.zdInfo.Model单位信息.sDwmc + "'医院名称,'' 报表名称,fhz_name 姓名,fhz_age 年龄1,fjy_yb_code 样本号,fhz_zyh 住院号,fhz_bed 床号,fapply_id 条码,fremark 备注,fjy_date 检验日期,fapply_time 送检日期,freport_time 报告时间,(SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '病人类别' and z.fcode=t.fhz_type_id)) as 病人类别,(SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '性别' and z.fcode=t.fhz_sex)) as 性别,(SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '年龄单位' and z.fcode=t.fhz_age_unit)) as 年龄单位,(SELECT top 1 z.fname FROM WWF_DEPT z WHERE (z.fdept_id=t.fhz_dept)) as 科室,(SELECT top 1 z.fname FROM SAM_SAMPLE_TYPE z WHERE (z.fsample_type_id=t.fjy_yb_type)) as 样本类型,(SELECT top 1 z.fname FROM WWF_PERSON z WHERE (z.fperson_id=t.fjy_user_id)) as 检验医师,(SELECT top 1 z.fname FROM WWF_PERSON z WHERE (z.fperson_id=t.fapply_user_id)) as 申请医师,(select top 1 z.fname from SAM_DISEASE z where z.fcode = t.fjy_lczd) as 疾病名称,(select top 1 z.fname from WWF_PERSON Z WHERE (Z.fperson_id=t.fchenk_user_id)) 审核者 ");
            strSql.Append(" ,case when DatePart(dy, getdate())-DatePart(dy, fhz_age_date)<0  then datediff(year,fhz_age_date,getdate())-1 when DatePart(dy, getdate())-DatePart(dy, fhz_age_date)>=0 then datediff(year,fhz_age_date,getdate()) end 年龄");
            strSql.Append(" FROM SAM_JY t where t.fjy_id=@jyid ");
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@jyid", jyid));

            DataSet ds = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.sLISConn, CommandType.Text, strSql.ToString(), paramList.ToArray());
            return ds.Tables[0];
        }

        /// <summary>
        /// 获得化验结果数据列表
        /// </summary>
        public DataTable GetJYDetailByJYID(string jyid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select t.* ");
            strSql.Append(" FROM SAM_JY_RESULT t where t.fjy_id=@jyid  order by cast(forder_by as int) ");
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@jyid", jyid));
            DataSet ds = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.sLISConn, CommandType.Text, strSql.ToString(), paramList.ToArray());
            return ds.Tables[0];
        }

        //打印状态更新
        public int BllReportUpdatePrintTimeNew(string jyid)
        {
            string sql = "update SAM_JY set fprint_time=(case when fprint_zt='未打印' then convert(varchar(20),getdate(),23) else fprint_time end), fprint_count=fprint_count+1,fprint_zt='已打印' where fjy_id=@fjyid";
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@fjyid", jyid));

            return HIS.Model.Dal.SqlHelper.ExecuteNonQuery(HIS.COMM.ClassDBConnstring.sLISConn, CommandType.Text, sql, paramList.ToArray());
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataTable GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from (select fhz_name 姓名,fhz_age 年龄1,fjy_yb_code 样本号,fhz_zyh 住院号,fhz_bed 床号,fapply_id 条码,fremark 备注,fjy_date 检验日期,fapply_time 送检日期,freport_time 报告时间,fjy_instr 化验设备ID,(SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '病人类别' and z.fcode=t.fhz_type_id)) as 病人类别,(SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '性别' and z.fcode=t.fhz_sex)) as 性别,(SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '年龄单位' and z.fcode=t.fhz_age_unit)) as 年龄单位,(SELECT top 1 z.fname FROM WWF_DEPT z WHERE (z.fdept_id=t.fhz_dept)) as 科室,(SELECT top 1 z.fname FROM SAM_SAMPLE_TYPE z WHERE (z.fsample_type_id=t.fjy_yb_type)) as 样本类型,(SELECT top 1 z.fname FROM WWF_PERSON z WHERE (z.fperson_id=t.fjy_user_id)) as 检验医师,(SELECT top 1 z.fname FROM WWF_PERSON z WHERE (z.fperson_id=t.fapply_user_id)) as 申请医师,(select top 1 z.fname from SAM_DISEASE z where z.fcode = t.fjy_lczd) as 疾病名称,(select top 1 z.fname from WWF_PERSON Z WHERE (Z.fperson_id=t.fchenk_user_id)) 审核者,(select ins.fname from SAM_INSTR ins where ins.finstr_id = t.fjy_instr ) 检验设备,(select app.fitem_group from SAM_APPLY app where app.fapply_id = t.fapply_id) 化验项目,t.fjy_id,fprint_zt 打印状态 ");
            strSql.Append(" ,case when DatePart(dy, getdate())-DatePart(dy, fhz_age_date)<0  then datediff(year,fhz_age_date,getdate())-1 when DatePart(dy, getdate())-DatePart(dy, fhz_age_date)>=0 then datediff(year,fhz_age_date,getdate()) end 年龄");
            strSql.Append(" FROM SAM_JY t) a  where 1=1 ");

            if (strWhere.Trim() != "")
            {
                strSql.Append(strWhere);
            }
            //strSql.Append(" order by 样本号 ");
            DataSet ds = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.sLISConn, CommandType.Text, strSql.ToString());
            return ds.Tables[0];
        }

    }
}
