﻿namespace HIS.InterFaceLIS
{
    partial class ApplyQueryNewForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ApplyQueryNewForm));
            this.bindingSourceApply = new System.Windows.Forms.BindingSource(this.components);
            this.sam_typeBindingSource_fstate = new System.Windows.Forms.BindingSource(this.components);
            this.panel5 = new System.Windows.Forms.Panel();
            this.labelNotify = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxSFZH = new System.Windows.Forms.TextBox();
            this.textBox门诊住院号 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btn生成条码 = new System.Windows.Forms.Button();
            this.btn查询 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txt申请科室 = new System.Windows.Forms.ComboBox();
            this.comboBox病人类型 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.fjy_dateDateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.fjy_dateDateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridViewApply = new System.Windows.Forms.DataGridView();
            this.fstate = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridViewItem = new System.Windows.Forms.DataGridView();
            this.bN = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.tb查询 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.tb打印 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.tb打印预览 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.tb打印全部 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonClose = new System.Windows.Forms.ToolStripButton();
            this.ck已打印 = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceApply)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sam_typeBindingSource_fstate)).BeginInit();
            this.panel5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewApply)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).BeginInit();
            this.bN.SuspendLayout();
            this.SuspendLayout();
            // 
            // bindingSourceApply
            // 
            this.bindingSourceApply.PositionChanged += new System.EventHandler(this.bindingSourceApply_PositionChanged);
            // 
            // panel5
            // 
            this.panel5.AutoScroll = true;
            this.panel5.Controls.Add(this.ck已打印);
            this.panel5.Controls.Add(this.labelNotify);
            this.panel5.Controls.Add(this.textBoxName);
            this.panel5.Controls.Add(this.textBoxSFZH);
            this.panel5.Controls.Add(this.textBox门诊住院号);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.btn生成条码);
            this.panel5.Controls.Add(this.btn查询);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.txt申请科室);
            this.panel5.Controls.Add(this.comboBox病人类型);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.fjy_dateDateTimePicker2);
            this.panel5.Controls.Add(this.fjy_dateDateTimePicker1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 43);
            this.panel5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1342, 103);
            this.panel5.TabIndex = 134;
            // 
            // labelNotify
            // 
            this.labelNotify.AutoSize = true;
            this.labelNotify.Location = new System.Drawing.Point(665, 83);
            this.labelNotify.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNotify.Name = "labelNotify";
            this.labelNotify.Size = new System.Drawing.Size(0, 17);
            this.labelNotify.TabIndex = 26;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(476, 48);
            this.textBoxName.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(148, 27);
            this.textBoxName.TabIndex = 25;
            this.textBoxName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox门诊住院号_KeyPress);
            // 
            // textBoxSFZH
            // 
            this.textBoxSFZH.Location = new System.Drawing.Point(108, 51);
            this.textBoxSFZH.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxSFZH.Name = "textBoxSFZH";
            this.textBoxSFZH.Size = new System.Drawing.Size(257, 27);
            this.textBoxSFZH.TabIndex = 25;
            this.textBoxSFZH.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox门诊住院号_KeyPress);
            // 
            // textBox门诊住院号
            // 
            this.textBox门诊住院号.Location = new System.Drawing.Point(767, 49);
            this.textBox门诊住院号.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox门诊住院号.Name = "textBox门诊住院号";
            this.textBox门诊住院号.Size = new System.Drawing.Size(148, 27);
            this.textBox门诊住院号.TabIndex = 25;
            this.textBox门诊住院号.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox门诊住院号_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(648, 54);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 17);
            this.label3.TabIndex = 24;
            this.label3.Text = "门诊/住院号:";
            // 
            // btn生成条码
            // 
            this.btn生成条码.Location = new System.Drawing.Point(1217, 10);
            this.btn生成条码.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn生成条码.Name = "btn生成条码";
            this.btn生成条码.Size = new System.Drawing.Size(112, 59);
            this.btn生成条码.TabIndex = 23;
            this.btn生成条码.Text = "生成条码";
            this.btn生成条码.UseVisualStyleBackColor = true;
            this.btn生成条码.Visible = false;
            this.btn生成条码.Click += new System.EventHandler(this.btn生成条码_Click);
            // 
            // btn查询
            // 
            this.btn查询.BackColor = System.Drawing.Color.Transparent;
            this.btn查询.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btn查询.Location = new System.Drawing.Point(1081, 10);
            this.btn查询.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn查询.Name = "btn查询";
            this.btn查询.Size = new System.Drawing.Size(128, 59);
            this.btn查询.TabIndex = 22;
            this.btn查询.Text = "普通查询";
            this.btn查询.UseVisualStyleBackColor = false;
            this.btn查询.Visible = false;
            this.btn查询.Click += new System.EventHandler(this.btn查询_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(385, 54);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 17);
            this.label5.TabIndex = 20;
            this.label5.Text = "病人姓名:";
            // 
            // txt申请科室
            // 
            this.txt申请科室.DisplayMember = "fname";
            this.txt申请科室.FormattingEnabled = true;
            this.txt申请科室.Location = new System.Drawing.Point(767, 12);
            this.txt申请科室.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt申请科室.Name = "txt申请科室";
            this.txt申请科室.Size = new System.Drawing.Size(148, 25);
            this.txt申请科室.TabIndex = 21;
            this.txt申请科室.ValueMember = "fname";
            // 
            // comboBox病人类型
            // 
            this.comboBox病人类型.DisplayMember = "fname";
            this.comboBox病人类型.FormattingEnabled = true;
            this.comboBox病人类型.Location = new System.Drawing.Point(477, 12);
            this.comboBox病人类型.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.comboBox病人类型.Name = "comboBox病人类型";
            this.comboBox病人类型.Size = new System.Drawing.Size(148, 25);
            this.comboBox病人类型.TabIndex = 21;
            this.comboBox病人类型.ValueMember = "fname";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 54);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 17);
            this.label4.TabIndex = 20;
            this.label4.Text = "身份证号:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(665, 18);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 17);
            this.label6.TabIndex = 20;
            this.label6.Text = "部门/科室:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(385, 17);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 17);
            this.label2.TabIndex = 20;
            this.label2.Text = "病人类型:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 17);
            this.label1.TabIndex = 19;
            this.label1.Text = "日期范围:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(228, 15);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 17);
            this.label11.TabIndex = 18;
            this.label11.Text = "-";
            // 
            // fjy_dateDateTimePicker2
            // 
            this.fjy_dateDateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker2.Location = new System.Drawing.Point(250, 10);
            this.fjy_dateDateTimePicker2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.fjy_dateDateTimePicker2.Name = "fjy_dateDateTimePicker2";
            this.fjy_dateDateTimePicker2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker2.Size = new System.Drawing.Size(115, 27);
            this.fjy_dateDateTimePicker2.TabIndex = 17;
            // 
            // fjy_dateDateTimePicker1
            // 
            this.fjy_dateDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker1.Location = new System.Drawing.Point(108, 10);
            this.fjy_dateDateTimePicker1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.fjy_dateDateTimePicker1.Name = "fjy_dateDateTimePicker1";
            this.fjy_dateDateTimePicker1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker1.Size = new System.Drawing.Size(115, 27);
            this.fjy_dateDateTimePicker1.TabIndex = 15;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridViewApply);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 146);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Size = new System.Drawing.Size(887, 443);
            this.groupBox1.TabIndex = 142;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "申请单";
            // 
            // dataGridViewApply
            // 
            this.dataGridViewApply.AllowUserToAddRows = false;
            this.dataGridViewApply.AllowUserToResizeRows = false;
            this.dataGridViewApply.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewApply.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewApply.ColumnHeadersHeight = 25;
            this.dataGridViewApply.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fstate});
            this.dataGridViewApply.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewApply.EnableHeadersVisualStyles = false;
            this.dataGridViewApply.Location = new System.Drawing.Point(4, 23);
            this.dataGridViewApply.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dataGridViewApply.MultiSelect = false;
            this.dataGridViewApply.Name = "dataGridViewApply";
            this.dataGridViewApply.RowHeadersVisible = false;
            this.dataGridViewApply.RowHeadersWidth = 35;
            this.dataGridViewApply.RowTemplate.Height = 23;
            this.dataGridViewApply.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewApply.Size = new System.Drawing.Size(879, 417);
            this.dataGridViewApply.TabIndex = 137;
            // 
            // fstate
            // 
            this.fstate.DataPropertyName = "fstate";
            this.fstate.DataSource = this.sam_typeBindingSource_fstate;
            this.fstate.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.fstate.HeaderText = "fstate";
            this.fstate.Name = "fstate";
            this.fstate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fstate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridViewItem);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox2.Location = new System.Drawing.Point(887, 146);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox2.Size = new System.Drawing.Size(455, 443);
            this.groupBox2.TabIndex = 143;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "化验项目";
            this.groupBox2.Visible = false;
            // 
            // dataGridViewItem
            // 
            this.dataGridViewItem.AllowUserToAddRows = false;
            this.dataGridViewItem.AllowUserToResizeRows = false;
            this.dataGridViewItem.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewItem.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewItem.ColumnHeadersHeight = 21;
            this.dataGridViewItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewItem.EnableHeadersVisualStyles = false;
            this.dataGridViewItem.Location = new System.Drawing.Point(4, 23);
            this.dataGridViewItem.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dataGridViewItem.MultiSelect = false;
            this.dataGridViewItem.Name = "dataGridViewItem";
            this.dataGridViewItem.RowHeadersVisible = false;
            this.dataGridViewItem.RowHeadersWidth = 35;
            this.dataGridViewItem.RowTemplate.Height = 23;
            this.dataGridViewItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewItem.Size = new System.Drawing.Size(447, 417);
            this.dataGridViewItem.TabIndex = 136;
            // 
            // bN
            // 
            this.bN.AddNewItem = null;
            this.bN.AutoSize = false;
            this.bN.CountItem = this.bindingNavigatorCountItem;
            this.bN.DeleteItem = null;
            this.bN.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.bN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.toolStripSeparator8,
            this.tb查询,
            this.toolStripSeparator6,
            this.tb打印,
            this.toolStripSeparator9,
            this.tb打印预览,
            this.toolStripSeparator11,
            this.tb打印全部,
            this.toolStripSeparator10,
            this.toolStripButtonClose});
            this.bN.Location = new System.Drawing.Point(0, 0);
            this.bN.MoveFirstItem = null;
            this.bN.MoveLastItem = null;
            this.bN.MoveNextItem = null;
            this.bN.MovePreviousItem = null;
            this.bN.Name = "bN";
            this.bN.PositionItem = this.bindingNavigatorPositionItem;
            this.bN.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bN.Size = new System.Drawing.Size(1342, 43);
            this.bN.TabIndex = 139;
            this.bN.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(38, 40);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(44, 27);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 43);
            // 
            // tb查询
            // 
            this.tb查询.Image = ((System.Drawing.Image)(resources.GetObject("tb查询.Image")));
            this.tb查询.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tb查询.Name = "tb查询";
            this.tb查询.Size = new System.Drawing.Size(98, 40);
            this.tb查询.Text = "查询(F8)  ";
            this.tb查询.Click += new System.EventHandler(this.tb查询_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 43);
            // 
            // tb打印
            // 
            this.tb打印.Image = ((System.Drawing.Image)(resources.GetObject("tb打印.Image")));
            this.tb打印.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tb打印.Name = "tb打印";
            this.tb打印.Size = new System.Drawing.Size(106, 40);
            this.tb打印.Text = "打印(F3)    ";
            this.tb打印.ToolTipText = "单个打印(F3)    ";
            this.tb打印.Click += new System.EventHandler(this.tb打印_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 43);
            // 
            // tb打印预览
            // 
            this.tb打印预览.Image = ((System.Drawing.Image)(resources.GetObject("tb打印预览.Image")));
            this.tb打印预览.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tb打印预览.Name = "tb打印预览";
            this.tb打印预览.Size = new System.Drawing.Size(93, 40);
            this.tb打印预览.Text = "打印预览";
            this.tb打印预览.ToolTipText = "单个预览";
            this.tb打印预览.Click += new System.EventHandler(this.tb打印_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 43);
            // 
            // tb打印全部
            // 
            this.tb打印全部.Image = ((System.Drawing.Image)(resources.GetObject("tb打印全部.Image")));
            this.tb打印全部.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tb打印全部.Name = "tb打印全部";
            this.tb打印全部.Size = new System.Drawing.Size(153, 40);
            this.tb打印全部.Text = "打印病人全部条码";
            this.tb打印全部.ToolTipText = "打印某个病人的全部条码";
            this.tb打印全部.Click += new System.EventHandler(this.tb打印全部_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 43);
            // 
            // toolStripButtonClose
            // 
            this.toolStripButtonClose.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonClose.Image")));
            this.toolStripButtonClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonClose.Name = "toolStripButtonClose";
            this.toolStripButtonClose.Size = new System.Drawing.Size(71, 40);
            this.toolStripButtonClose.Text = "关闭  ";
            this.toolStripButtonClose.Click += new System.EventHandler(this.toolStripButtonClose_Click);
            // 
            // ck已打印
            // 
            this.ck已打印.AutoSize = true;
            this.ck已打印.Location = new System.Drawing.Point(939, 12);
            this.ck已打印.Name = "ck已打印";
            this.ck已打印.Size = new System.Drawing.Size(115, 21);
            this.ck已打印.TabIndex = 27;
            this.ck已打印.Text = "包含已打印";
            this.ck已打印.UseVisualStyleBackColor = true;
            // 
            // ApplyQueryNewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1342, 589);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.bN);
            this.Font = new System.Drawing.Font("宋体", 10F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "ApplyQueryNewForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "申请查询基础窗口";
            this.Load += new System.EventHandler(this.ApplyQueryNewForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceApply)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sam_typeBindingSource_fstate)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewApply)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).EndInit();
            this.bN.ResumeLayout(false);
            this.bN.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.BindingNavigator bN;
        protected System.Windows.Forms.ToolStripButton tb查询;
        private System.Windows.Forms.BindingSource sam_typeBindingSource_fstate;
        //private ww.form.lis.sam.samDataSet samDataSet;
        private System.Windows.Forms.BindingSource bindingSourceApply;
        protected System.Windows.Forms.ToolStripButton toolStripButtonClose;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        protected System.Windows.Forms.GroupBox groupBox2;
        protected System.Windows.Forms.GroupBox groupBox1;
        protected System.Windows.Forms.DataGridView dataGridViewApply;
        protected System.Windows.Forms.DataGridView dataGridViewItem;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.DataGridViewComboBoxColumn fstate;
        private System.Windows.Forms.ToolStripButton tb打印;
        protected System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        protected System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker2;
        protected System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker1;
        private System.Windows.Forms.ComboBox comboBox病人类型;
        private System.Windows.Forms.Button btn生成条码;
        private System.Windows.Forms.Button btn查询;
        private System.Windows.Forms.TextBox textBox门诊住院号;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripButton tb打印预览;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripButton tb打印全部;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxSFZH;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox txt申请科室;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelNotify;
        private System.Windows.Forms.CheckBox ck已打印;
    }
}