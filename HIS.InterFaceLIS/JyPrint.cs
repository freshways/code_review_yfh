﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
//using ww.form.wwf.print;
//using ww.wwf.wwfbll;
//using ww.lis.lisbll.sam;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
//using Common;

namespace HIS.InterFaceLIS
{
    public class JyPrint
    { 
        /// <summary>
        /// 报告规则
        /// </summary>
        private ApplyBLL bllReport = new ApplyBLL();
        //private lisbll.InstrBLL bllInstr = new lisbll.InstrBLL();

        public JyPrint()
        {
        }

        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="jyid">样本ID</param>
        /// <param name="printType">false：打印预览；true：打印</param>
        public bool BllPrintViewer(string jyid, bool printType)
        {
            bool printSuccess = false;
            try
            {
                DataTable dtReport = this.bllReport.GetListByJYID(jyid);
                if (dtReport.Rows.Count > 0)
                {
                    DataTable dtDetail = this.bllReport.GetJYDetailByJYID(jyid);

                    int intResultCount = dtDetail.Rows.Count;//结果记录数
                    if (intResultCount > 0)
                    {
                        //printSuccess = WWPrintViewer(strfsample_id, strReportName, dtPrintDataTable);"检验报告单"
                        printSuccess = WWPrintViewer(jyid, dtReport, dtDetail, printType);
                    }
                    else
                    {
                        MessageBox.Show("报告结果为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    WWMessage.MessageShowWarning("报告为空，操作失败！请选择报告后重试。");
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            return printSuccess;
        }

        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="strReportName">报表数据源名</param>
        /// <param name="strReportPath">报表文档</param>
        private bool WWPrintViewer(string fjy_id, DataTable dtJYReport, DataTable dtJYDetail,bool print=false) 
        {
            bool printSuccess = false;
            try
            {
                ReportD280A5 rep = new ReportD280A5(fjy_id,dtJYReport, dtJYDetail);
                rep.ShowPrintStatusDialog = false;
                if (print)
                    rep.Print();
                else
                    rep.ShowPreview(); // 调试预览打印效果

                ///TODO: 这部分需要根据打印状态进行更新
                printSuccess = rep.GetPrintResult();

                if (printSuccess && !(string.IsNullOrWhiteSpace(fjy_id)))
                {
                    //lisbll.jybll bllReport = new lisbll.jybll();
                    bllReport.BllReportUpdatePrintTimeNew(fjy_id);
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            return printSuccess;
        }
    }
}
