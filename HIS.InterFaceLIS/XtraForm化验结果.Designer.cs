﻿namespace HIS.InterFaceLIS
{
    partial class XtraForm化验结果
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraForm化验结果));
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.col打印状态 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col申请医生 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.txt病人来源 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton打印全部 = new DevExpress.XtraEditors.SimpleButton();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.gc化验明细 = new DevExpress.XtraGrid.GridControl();
            this.gv化验明细 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton查看 = new DevExpress.XtraEditors.SimpleButton();
            this.gc化验列表 = new DevExpress.XtraGrid.GridControl();
            this.gv化验列表 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col申请单号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col检验日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col样本号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col病人来源 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col住院号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col床号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col病人姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col性别 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col年龄 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col身份证 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col化验项目 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col化验设备 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col化验设备ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colfjy_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.textEdit截止时间 = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton刷新 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit开始时间 = new DevExpress.XtraEditors.TextEdit();
            this.txt申请科室 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.col检查费 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt病人来源.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc化验明细)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv化验明细)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc化验列表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv化验列表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit截止时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit开始时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt申请科室.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            this.SuspendLayout();
            // 
            // col打印状态
            // 
            this.col打印状态.Caption = "打印状态";
            this.col打印状态.FieldName = "打印状态";
            this.col打印状态.Name = "col打印状态";
            this.col打印状态.OptionsColumn.AllowEdit = false;
            this.col打印状态.Visible = true;
            this.col打印状态.VisibleIndex = 8;
            // 
            // col申请医生
            // 
            this.col申请医生.Caption = "申请医生";
            this.col申请医生.FieldName = "申请医生";
            this.col申请医生.Name = "col申请医生";
            this.col申请医生.OptionsColumn.AllowEdit = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.txt病人来源;
            this.layoutControlItem8.CustomizationFormText = "病人来源";
            this.layoutControlItem8.Location = new System.Drawing.Point(546, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(136, 26);
            this.layoutControlItem8.Text = "病人来源";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(52, 14);
            // 
            // txt病人来源
            // 
            this.txt病人来源.EditValue = "住院";
            this.txt病人来源.Location = new System.Drawing.Point(613, 12);
            this.txt病人来源.Name = "txt病人来源";
            this.txt病人来源.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt病人来源.Properties.Items.AddRange(new object[] {
            "住院",
            "门诊"});
            this.txt病人来源.Size = new System.Drawing.Size(77, 20);
            this.txt病人来源.StyleController = this.layoutControl1;
            this.txt病人来源.TabIndex = 29;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.simpleButton打印全部);
            this.layoutControl1.Controls.Add(this.splitterControl1);
            this.layoutControl1.Controls.Add(this.gc化验明细);
            this.layoutControl1.Controls.Add(this.txt病人来源);
            this.layoutControl1.Controls.Add(this.simpleButton查看);
            this.layoutControl1.Controls.Add(this.gc化验列表);
            this.layoutControl1.Controls.Add(this.textEdit截止时间);
            this.layoutControl1.Controls.Add(this.simpleButton刷新);
            this.layoutControl1.Controls.Add(this.textEdit开始时间);
            this.layoutControl1.Controls.Add(this.txt申请科室);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(241, 249, 250, 350);
            this.layoutControl1.OptionsView.UseDefaultDragAndDropRendering = false;
            this.layoutControl1.Root = this.layoutControlGroup2;
            this.layoutControl1.Size = new System.Drawing.Size(1063, 476);
            this.layoutControl1.TabIndex = 2;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // simpleButton打印全部
            // 
            this.simpleButton打印全部.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton打印全部.Image")));
            this.simpleButton打印全部.Location = new System.Drawing.Point(913, 12);
            this.simpleButton打印全部.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.simpleButton打印全部.Name = "simpleButton打印全部";
            this.simpleButton打印全部.Size = new System.Drawing.Size(124, 22);
            this.simpleButton打印全部.StyleController = this.layoutControl1;
            this.simpleButton打印全部.TabIndex = 32;
            this.simpleButton打印全部.Text = "打印全部(未打印)";
            this.simpleButton打印全部.ToolTip = "只打未打印的单据，已打印的不再打印！";
            this.simpleButton打印全部.Click += new System.EventHandler(this.simpleButton打印全部_Click);
            // 
            // splitterControl1
            // 
            this.splitterControl1.Location = new System.Drawing.Point(658, 214);
            this.splitterControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(5, 2);
            this.splitterControl1.TabIndex = 31;
            this.splitterControl1.TabStop = false;
            // 
            // gc化验明细
            // 
            this.gc化验明细.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gc化验明细.Location = new System.Drawing.Point(672, 38);
            this.gc化验明细.MainView = this.gv化验明细;
            this.gc化验明细.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gc化验明细.Name = "gc化验明细";
            this.gc化验明细.Size = new System.Drawing.Size(379, 426);
            this.gc化验明细.TabIndex = 30;
            this.gc化验明细.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv化验明细});
            // 
            // gv化验明细
            // 
            this.gv化验明细.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4});
            this.gv化验明细.GridControl = this.gc化验明细;
            this.gv化验明细.Name = "gv化验明细";
            this.gv化验明细.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "检查项目";
            this.gridColumn1.FieldName = "检验名称";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "结果";
            this.gridColumn2.FieldName = "检验值";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "标记";
            this.gridColumn3.FieldName = "升降";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "参考值";
            this.gridColumn4.FieldName = "参考值";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // simpleButton查看
            // 
            this.simpleButton查看.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton查看.Image")));
            this.simpleButton查看.Location = new System.Drawing.Point(817, 12);
            this.simpleButton查看.Name = "simpleButton查看";
            this.simpleButton查看.Size = new System.Drawing.Size(92, 22);
            this.simpleButton查看.StyleController = this.layoutControl1;
            this.simpleButton查看.TabIndex = 28;
            this.simpleButton查看.Text = "打印结果";
            this.simpleButton查看.Click += new System.EventHandler(this.simpleButton查看_Click);
            // 
            // gc化验列表
            // 
            this.gc化验列表.Location = new System.Drawing.Point(12, 38);
            this.gc化验列表.MainView = this.gv化验列表;
            this.gc化验列表.Name = "gc化验列表";
            this.gc化验列表.Size = new System.Drawing.Size(656, 426);
            this.gc化验列表.TabIndex = 7;
            this.gc化验列表.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv化验列表});
            this.gc化验列表.Click += new System.EventHandler(this.gc化验列表_Click);
            // 
            // gv化验列表
            // 
            this.gv化验列表.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col申请单号,
            this.col检验日期,
            this.col样本号,
            this.col病人来源,
            this.col住院号,
            this.col床号,
            this.col病人姓名,
            this.col性别,
            this.col年龄,
            this.col身份证,
            this.col化验项目,
            this.col化验设备,
            this.col化验设备ID,
            this.col打印状态,
            this.colfjy_id});
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.LightSkyBlue;
            styleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.col打印状态;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = "已打印";
            styleFormatCondition1.Value2 = "未打印";
            this.gv化验列表.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gv化验列表.GridControl = this.gc化验列表;
            this.gv化验列表.Name = "gv化验列表";
            this.gv化验列表.OptionsView.ShowGroupPanel = false;
            // 
            // col申请单号
            // 
            this.col申请单号.Caption = "申请单号";
            this.col申请单号.FieldName = "条码";
            this.col申请单号.Name = "col申请单号";
            this.col申请单号.OptionsColumn.AllowEdit = false;
            this.col申请单号.Visible = true;
            this.col申请单号.VisibleIndex = 0;
            // 
            // col检验日期
            // 
            this.col检验日期.Caption = "检验日期";
            this.col检验日期.FieldName = "检验日期";
            this.col检验日期.Name = "col检验日期";
            this.col检验日期.OptionsColumn.AllowEdit = false;
            this.col检验日期.Visible = true;
            this.col检验日期.VisibleIndex = 1;
            // 
            // col样本号
            // 
            this.col样本号.Caption = "样本号";
            this.col样本号.FieldName = "样本号";
            this.col样本号.Name = "col样本号";
            this.col样本号.OptionsColumn.AllowEdit = false;
            this.col样本号.Visible = true;
            this.col样本号.VisibleIndex = 2;
            // 
            // col病人来源
            // 
            this.col病人来源.Caption = "病人来源";
            this.col病人来源.FieldName = "病人类别";
            this.col病人来源.Name = "col病人来源";
            this.col病人来源.OptionsColumn.AllowEdit = false;
            // 
            // col住院号
            // 
            this.col住院号.Caption = "住院号/门诊号";
            this.col住院号.FieldName = "住院号";
            this.col住院号.Name = "col住院号";
            this.col住院号.OptionsColumn.AllowEdit = false;
            this.col住院号.Visible = true;
            this.col住院号.VisibleIndex = 3;
            // 
            // col床号
            // 
            this.col床号.Caption = "床号";
            this.col床号.FieldName = "床号";
            this.col床号.Name = "col床号";
            this.col床号.OptionsColumn.AllowEdit = false;
            this.col床号.Visible = true;
            this.col床号.VisibleIndex = 4;
            // 
            // col病人姓名
            // 
            this.col病人姓名.Caption = "病人姓名";
            this.col病人姓名.FieldName = "姓名";
            this.col病人姓名.Name = "col病人姓名";
            this.col病人姓名.OptionsColumn.AllowEdit = false;
            this.col病人姓名.Visible = true;
            this.col病人姓名.VisibleIndex = 5;
            // 
            // col性别
            // 
            this.col性别.Caption = "性别";
            this.col性别.FieldName = "性别";
            this.col性别.Name = "col性别";
            this.col性别.OptionsColumn.AllowEdit = false;
            // 
            // col年龄
            // 
            this.col年龄.Caption = "年龄";
            this.col年龄.FieldName = "年龄";
            this.col年龄.Name = "col年龄";
            this.col年龄.OptionsColumn.AllowEdit = false;
            // 
            // col身份证
            // 
            this.col身份证.Caption = "身份证";
            this.col身份证.FieldName = "身份证";
            this.col身份证.Name = "col身份证";
            this.col身份证.OptionsColumn.AllowEdit = false;
            // 
            // col化验项目
            // 
            this.col化验项目.Caption = "化验项目";
            this.col化验项目.FieldName = "化验项目";
            this.col化验项目.Name = "col化验项目";
            this.col化验项目.OptionsColumn.AllowEdit = false;
            this.col化验项目.Visible = true;
            this.col化验项目.VisibleIndex = 6;
            // 
            // col化验设备
            // 
            this.col化验设备.Caption = "化验设备";
            this.col化验设备.FieldName = "检验设备";
            this.col化验设备.Name = "col化验设备";
            this.col化验设备.OptionsColumn.AllowEdit = false;
            this.col化验设备.Visible = true;
            this.col化验设备.VisibleIndex = 7;
            // 
            // col化验设备ID
            // 
            this.col化验设备ID.Caption = "化验设备ID";
            this.col化验设备ID.FieldName = "化验设备ID";
            this.col化验设备ID.Name = "col化验设备ID";
            this.col化验设备ID.OptionsColumn.AllowEdit = false;
            this.col化验设备ID.Visible = true;
            this.col化验设备ID.VisibleIndex = 9;
            // 
            // colfjy_id
            // 
            this.colfjy_id.Caption = "检验ID";
            this.colfjy_id.FieldName = "fjy_id";
            this.colfjy_id.Name = "colfjy_id";
            this.colfjy_id.OptionsColumn.AllowEdit = false;
            // 
            // textEdit截止时间
            // 
            this.textEdit截止时间.Location = new System.Drawing.Point(267, 12);
            this.textEdit截止时间.Name = "textEdit截止时间";
            this.textEdit截止时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.textEdit截止时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.textEdit截止时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEdit截止时间.Size = new System.Drawing.Size(141, 20);
            this.textEdit截止时间.StyleController = this.layoutControl1;
            this.textEdit截止时间.TabIndex = 20;
            // 
            // simpleButton刷新
            // 
            this.simpleButton刷新.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton刷新.Image")));
            this.simpleButton刷新.Location = new System.Drawing.Point(723, 12);
            this.simpleButton刷新.Name = "simpleButton刷新";
            this.simpleButton刷新.Size = new System.Drawing.Size(90, 22);
            this.simpleButton刷新.StyleController = this.layoutControl1;
            this.simpleButton刷新.TabIndex = 6;
            this.simpleButton刷新.Text = "刷新";
            this.simpleButton刷新.Click += new System.EventHandler(this.simpleButton刷新_Click);
            // 
            // textEdit开始时间
            // 
            this.textEdit开始时间.Location = new System.Drawing.Point(67, 12);
            this.textEdit开始时间.Name = "textEdit开始时间";
            this.textEdit开始时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.textEdit开始时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.textEdit开始时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEdit开始时间.Size = new System.Drawing.Size(141, 20);
            this.textEdit开始时间.StyleController = this.layoutControl1;
            this.textEdit开始时间.TabIndex = 19;
            // 
            // txt申请科室
            // 
            this.txt申请科室.Location = new System.Drawing.Point(467, 12);
            this.txt申请科室.Name = "txt申请科室";
            this.txt申请科室.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt申请科室.Properties.NullText = "请选择...";
            this.txt申请科室.Properties.PopupSizeable = true;
            this.txt申请科室.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txt申请科室.Size = new System.Drawing.Size(87, 20);
            this.txt申请科室.StyleController = this.layoutControl1;
            this.txt申请科室.TabIndex = 21;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.layoutControlItem9,
            this.emptySpaceItem2,
            this.layoutControlItem8,
            this.layoutControlItem10,
            this.layoutControlItem11});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1063, 476);
            this.layoutControlGroup2.Text = "Root";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEdit开始时间;
            this.layoutControlItem2.CustomizationFormText = "开始时间:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem2.Name = "layoutControlItem3";
            this.layoutControlItem2.Size = new System.Drawing.Size(200, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "开始时间:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(52, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.simpleButton刷新;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem5.Location = new System.Drawing.Point(711, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(94, 0);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(94, 26);
            this.layoutControlItem5.Name = "layoutControlItem2";
            this.layoutControlItem5.Size = new System.Drawing.Size(94, 26);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem2";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEdit截止时间;
            this.layoutControlItem6.CustomizationFormText = "截止时间:";
            this.layoutControlItem6.Location = new System.Drawing.Point(200, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem6.Name = "layoutControlItem4";
            this.layoutControlItem6.Size = new System.Drawing.Size(200, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "截止时间:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(52, 14);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txt申请科室;
            this.layoutControlItem7.CustomizationFormText = "病区选择:";
            this.layoutControlItem7.Location = new System.Drawing.Point(400, 0);
            this.layoutControlItem7.Name = "layoutControlItem5";
            this.layoutControlItem7.Size = new System.Drawing.Size(146, 26);
            this.layoutControlItem7.Text = "申请科室";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(52, 14);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gc化验列表;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(660, 430);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(682, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(29, 26);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.simpleButton查看;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(805, 0);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(96, 26);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(96, 26);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(96, 26);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(1029, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(14, 26);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.gc化验明细;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(660, 26);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(383, 430);
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.simpleButton打印全部;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(901, 0);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(128, 26);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(128, 26);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(128, 26);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.CustomizationFormText = "开始时间:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(233, 26);
            this.layoutControlItem3.Text = "开始时间:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(52, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.CustomizationFormText = "截止时间:";
            this.layoutControlItem4.Location = new System.Drawing.Point(233, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(229, 26);
            this.layoutControlItem4.Text = "截止时间:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(52, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(895, 497);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // col检查费
            // 
            this.col检查费.Caption = "检查费";
            this.col检查费.FieldName = "检查费";
            this.col检查费.Name = "col检查费";
            this.col检查费.OptionsColumn.AllowEdit = false;
            // 
            // XtraForm化验结果
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1063, 476);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "XtraForm化验结果";
            this.Text = "XtraForm化验结果";
            this.Load += new System.EventHandler(this.XtraForm化验结果_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt病人来源.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc化验明细)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv化验明细)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc化验列表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv化验列表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit截止时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit开始时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt申请科室.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn col申请医生;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.ComboBoxEdit txt病人来源;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton查看;
        private DevExpress.XtraGrid.GridControl gc化验列表;
        private DevExpress.XtraGrid.Views.Grid.GridView gv化验列表;
        private DevExpress.XtraGrid.Columns.GridColumn col申请单号;
        private DevExpress.XtraGrid.Columns.GridColumn col检验日期;
        private DevExpress.XtraGrid.Columns.GridColumn col样本号;
        private DevExpress.XtraGrid.Columns.GridColumn col病人来源;
        private DevExpress.XtraGrid.Columns.GridColumn col住院号;
        private DevExpress.XtraGrid.Columns.GridColumn col病人姓名;
        private DevExpress.XtraGrid.Columns.GridColumn col性别;
        private DevExpress.XtraGrid.Columns.GridColumn col年龄;
        private DevExpress.XtraGrid.Columns.GridColumn col身份证;
        private DevExpress.XtraGrid.Columns.GridColumn col化验项目;
        private DevExpress.XtraEditors.TextEdit textEdit截止时间;
        private DevExpress.XtraEditors.SimpleButton simpleButton刷新;
        private DevExpress.XtraEditors.TextEdit textEdit开始时间;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.Columns.GridColumn col检查费;
        private DevExpress.XtraEditors.ComboBoxEdit txt申请科室;
        private DevExpress.XtraGrid.Columns.GridColumn col化验设备;
        private DevExpress.XtraGrid.GridControl gc化验明细;
        private DevExpress.XtraGrid.Views.Grid.GridView gv化验明细;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraGrid.Columns.GridColumn colfjy_id;
        private DevExpress.XtraGrid.Columns.GridColumn col化验设备ID;
        private DevExpress.XtraGrid.Columns.GridColumn col床号;
        private DevExpress.XtraGrid.Columns.GridColumn col打印状态;
        private DevExpress.XtraEditors.SimpleButton simpleButton打印全部;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
    }
}