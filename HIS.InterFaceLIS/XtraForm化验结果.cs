﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HIS.InterFaceLIS
{
    public partial class XtraForm化验结果 : DevExpress.XtraEditors.XtraForm
    {
        public XtraForm化验结果()
        {
            InitializeComponent();
        }
        ApplyBLL bll = new ApplyBLL();
        private void XtraForm化验结果_Load(object sender, EventArgs e)
        {
            this.textEdit开始时间.Text = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            this.textEdit截止时间.Text = DateTime.Now.ToString("yyyy-MM-dd");
            this.txt病人来源.SelectedIndex = 0;
            Get病人病区();
            txt申请科室.Text = WEISHENG.COMM.zdInfo.ModelUserInfo.科室名称;
        }
        private void Get病人病区()
        {
            DataTable dt = new DataTable();
            dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text,
                "select 科室名称,科室编码 from GY科室设置 where 是否住院科室=1 ").Tables[0];

            txt申请科室.Properties.Items.Clear();

            foreach (DataRow row in dt.Rows)
            {
                txt申请科室.Properties.Items.Add(row["科室名称"]);
            }
        }

        private void simpleButton刷新_Click(object sender, EventArgs e)
        {
            _Binding检查结果();
        }

        private void simpleButton查看_Click(object sender, EventArgs e)
        {
            try
            {
                if (gv化验列表.GetSelectedRows().Length == 0) return;
                string sMZID = gv化验列表.GetDataRow(gv化验列表.FocusedRowHandle)["住院号"].ToString();
                string s检验日期 = gv化验列表.GetDataRow(gv化验列表.FocusedRowHandle)["检验日期"].ToString();
                string s样本号 = gv化验列表.GetDataRow(gv化验列表.FocusedRowHandle)["样本号"].ToString();
                string s检验设备 = gv化验列表.GetDataRow(gv化验列表.FocusedRowHandle)["化验设备ID"].ToString();
                string s病人来源 = gv化验列表.GetDataRow(gv化验列表.FocusedRowHandle)["病人类别"].ToString();

                bool tru = InterFaceLIS.ClassLIS._s报告单打印(sMZID, s检验日期, s样本号, s检验设备, s病人来源,false);
                if (tru)
                {
                    gv化验列表.GetDataRow(gv化验列表.FocusedRowHandle)["打印状态"] = "已打印";
                }
                else
                {
                    MessageBox.Show("该化验结果【未审核】或【没有可打印的内容】！");
                }
            }
            catch (Exception)
            {
                _Binding检查结果();
            }
        }

        DataTable dtMain;
        private void _Binding检查结果()
        {
//            string sql = @"select distinct a.申请单号,检验日期,b.HIS组合名称,住院号,a.病人姓名,年龄,身份证,a.病人ID, b.病人类型 as 病人来源,样本号,检验设备 
//                                    from JY检验结果 a,JY申请单摘要 b where a.申请单号=convert(varchar,b.申请单号) ";
            string sql = "";
            if (textEdit开始时间.Text != "")
                sql += " and 检验日期>='" + textEdit开始时间.Text + "' ";
            if (textEdit截止时间.Text != "")
                sql += " and 检验日期<='" + textEdit截止时间.Text + "' ";
            if (txt病人来源.Text != "" && txt病人来源.Text == "门诊")
                sql += " and 病人类别='门诊' ";
            else if (txt病人来源.Text != "" && txt病人来源.Text == "住院")
                sql += " and 病人类别='住院' ";
            if (txt申请科室.Text != "")
                sql += " and 科室='" + txt申请科室.Text + "' ";
            sql += " order by 住院号 asc,检验日期,cast(样本号 as int) asc  ";
            dtMain = new DataTable();
            dtMain = bll.GetList(sql);
           // dtMain = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, sql).Tables[0];

            gc化验列表.DataSource = dtMain;
            this.gv化验列表.BestFitColumns();
            if (dtMain != null & dtMain.Rows.Count > 0)
            {
                this.gv化验列表.FocusedRowHandle = 0;
                gc化验列表_Click(null, null);
            }
            else
            {
                gc化验列表.DataSource = null;
                gc化验明细.DataSource = null;
            }
        }

        private void gc化验列表_Click(object sender, EventArgs e)
        {
            if (gv化验列表.GetSelectedRows().Length == 0) return;
            string sMZID = gv化验列表.GetDataRow(gv化验列表.FocusedRowHandle)["住院号"].ToString();
            string s检验日期 = gv化验列表.GetDataRow(gv化验列表.FocusedRowHandle)["检验日期"].ToString();
            string s样本号 = gv化验列表.GetDataRow(gv化验列表.FocusedRowHandle)["样本号"].ToString();
            string s检验设备 = gv化验列表.GetDataRow(gv化验列表.FocusedRowHandle)["化验设备ID"].ToString();
            string s病人来源 = gv化验列表.GetDataRow(gv化验列表.FocusedRowHandle)["病人类别"].ToString();
            string s检验ID = gv化验列表.GetDataRow(gv化验列表.FocusedRowHandle)["fjy_id"].ToString();

            //string sql = "select 检验编码,检验名称,单位,参考值,升降,检验值,排序,样本号 from JY检验结果 where 1=1" +
            //    "and 住院号='"+sMZID+"' and 检验日期='"+s检验日期+"' and 检验设备='"+s检验设备+"' and 样本号='"+s样本号+"' "+
            //    " order by convert(int,排序) ";

            string sql = "select fitem_code 检验编码,fitem_name 检验名称,fitem_unit 单位,fitem_ref 参考值,fitem_badge 升降,fvalue 检验值,forder_by 排序  FROM SAM_JY_RESULT t where t.fjy_id='" + s检验ID + "'  order by cast(forder_by as int) ";

            DataTable dtDetail = new DataTable();
            dtDetail = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.sLISConn, CommandType.Text, sql).Tables[0];
            this.gc化验明细.DataSource = dtDetail;
            this.gv化验明细.BestFitColumns();
            //InterFaceLIS.ClassLIS._s显示化验结果(sMZID, s检验日期, s样本号, s检验设备, s病人来源);
        }

        private void simpleButton打印全部_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtMain == null) return;
                foreach (DataRow row in dtMain.Rows)
                {
                    if (row["打印状态"].ToString() == "已打印") continue;
                    string sMZID = row["住院号"].ToString();
                    string s检验日期 = row["检验日期"].ToString();
                    string s样本号 = row["样本号"].ToString(); 
                    string s检验设备 = row["化验设备ID"].ToString();
                    string s病人来源 = row["病人类别"].ToString(); 

                    bool tru = InterFaceLIS.ClassLIS._s报告单打印(sMZID, s检验日期, s样本号, s检验设备, s病人来源, true);
                    if (tru)
                    {
                        row["打印状态"] = "已打印";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                _Binding检查结果();
            }
        }
    }
}
