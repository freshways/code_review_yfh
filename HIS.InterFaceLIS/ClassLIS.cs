﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DevExpress.XtraReports.UI;
using System.Windows.Forms;
using HIS.Model;
using HIS.Model.Pojo;
using HIS.COMM;

namespace HIS.InterFaceLIS
{
    public class ClassLIS
    {
        public static bool _s报告单打印(string s住院号或门诊号, string s检验日期, string s样本号, string s检验设备, string s病人来源, bool 直接打印)
        {
            string SQL = "SELECT fjy_id ,fjy_date ,fjy_instr ,fjy_yb_code ,fhz_name ,fprint_zt ,fhz_zyh ,fapply_id " +
                            "FROM [SAM_JY] aa where fjy_zt='已审核' " +
                            " and fjy_date='" + s检验日期 + "' and fjy_yb_code='" + s样本号 + "' and fjy_instr='" + s检验设备 + "' " +
                            " and fhz_zyh='" + s住院号或门诊号 + "' ";
            //获取一条病人的化验信息
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.sLISConn, CommandType.Text, SQL).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                JyPrint print = new JyPrint();
                //print.
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    print.BllPrintViewer(dt.Rows[index]["fjy_id"].ToString(), 直接打印);
                }

                return true;
            }
            else
                return false;
        }
        public static bool _s显示化验结果(string s住院号或门诊号, string s检验日期, string s样本号, string s检验设备, string s病人来源)
        {
            string SQL基本信息 = "";
            DataTable dt病人信息 = null;
            if (s病人来源 == "住院")
            {
                SQL基本信息 =
                    "select '" + WEISHENG.COMM.zdInfo.ModelUserInfo.单位编码 + "' 医院名称,'' 报表名称,住院号码, 病人姓名 姓名, 性别,'' 年龄,'' 年龄单位, bb.[科室名称] 送检科室, '' 床号," + "\r\n" +
                    "       '' 样本类别, '' 检验类别, '' 诊断, '' 样本号, '' 样本条码号, bb.[用户名] 申请者," + "\r\n" +
                    "       '' 备注, '' 检验时间, '' 检验者, '' 医院地址, '' 审核时间, '' 检验实验室," + "\r\n" +
                    "       '' 医院联系电话" + "\r\n" +
                    "from   [zy病人信息] aa left outer join pubUser bb on aa.主治医生编码 = bb.[用户编码]" + "\r\n" +
                    "where  aa.住院号码 = " + s住院号或门诊号;
            }
            else if (s病人来源 == "门诊")
            {
                SQL基本信息 =
                     "select '" + WEISHENG.COMM.zdInfo.ModelUserInfo.单位编码 + "' 医院名称,'' 报表名称,住院号码, 病人姓名 姓名, 性别, 年龄, 年龄单位, bb.[科室名称] 送检科室, '' 床号," + "\r\n" +
                     "       '' 样本类别, '' 检验类别, 临床诊断 诊断, '' 样本号, '' 样本条码号, bb.[用户名] 申请者," + "\r\n" +
                     "       '' 备注, '' 检验时间, '' 检验者, '' 医院地址, '' 审核时间, '' 检验实验室," + "\r\n" +
                     "       '' 医院联系电话" + "\r\n" +
                     "from   [MF门诊摘要] aa left outer join pubUser bb on aa.医生编码 = bb.[用户编码]" + "\r\n" +
                     "where  MZID = " + s住院号或门诊号;
            }

            dt病人信息 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL基本信息).Tables[0];

            string SQL检验结果 =
                "SELECT [申请单号], [检验日期], [检验设备], [病人ID], [住院号], [病人姓名], [年龄], [身份证], [检验ID]," + "\r\n" +
                "       [检验编码], [检验名称], [单位], [参考值], [升降], [检验值], [排序], [样本号]" + "\r\n" +
                "FROM   [JY检验结果]" + "\r\n" +
                "WHERE  [住院号] = '" + s住院号或门诊号 + "' and [检验日期]='" + s检验日期 + "' and [样本号]='" + s样本号 + "' and [检验设备]='" + s检验设备 + "' order by convert(int,排序)  ";
            DataTable dt检验结果 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL检验结果).Tables[0];

            InterFaceLIS.ReportComLittle report = new InterFaceLIS.ReportComLittle(dt病人信息, dt检验结果);
            report.ShowPreviewDialog();//软件暂停，等用户响应
            return true;
        }
        public static bool b显示住院结果(string _sLISID, Pojo病人信息 _person)
        {
            string fapply_id = s用申请唯一标识获取申请单号(_sLISID);
            if (string.IsNullOrEmpty(fapply_id)) return false;
            string SQL = "SELECT fjy_id ,fjy_date ,fjy_instr ,fjy_yb_code ,fhz_name ,fprint_zt ,fhz_zyh ,fapply_id " +
                            "FROM [SAM_JY] aa where fjy_zt='已审核' " +
                            " and fapply_id='" + s用申请唯一标识获取申请单号(_sLISID) + "' ";
            //获取一条病人的化验信息
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.sLISConn, CommandType.Text, SQL).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                JyPrint print = new JyPrint();
                //print.
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    print.BllPrintViewer(dt.Rows[index]["fjy_id"].ToString(), false);
                }

                return true;
            }
            else
                return false;
            //try
            //{
            //    string SQL基本信息 =
            // "select '" + WEISHENG.COMM.zdInfo.sDwmc + "' 医院名称,'' 报表名称,住院号码, 病人姓名 姓名, 性别,'" + _person.S年龄 + "' 年龄,'" + _person.S年龄单位 + "' 年龄单位, bb.[科室名称] 送检科室, 病床 床号," + "\r\n" +
            // "       '血清' 样本类别, '住院' 检验类别, '"+_person.s疾病名称+"' 诊断, '' 样本号, '' 样本条码号, bb.[用户名] 申请者," + "\r\n" +
            // "       '' 备注, '' 检验时间, '' 检验者, '' 医院地址, '' 审核时间, '' 检验实验室," + "\r\n" +
            // "       '' 医院联系电话" + "\r\n" +
            // "from   [zy病人信息] aa left outer join pubUser bb on aa.主治医生编码 = bb.[用户编码]" + "\r\n" +
            // "where  zyid = " + _person.sZYID父;

            //    DataTable dt病人信息 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL基本信息).Tables[0];

            //    string SQL检验结果 =
            //    "SELECT [申请单号], [检验日期], [检验设备], [病人ID], [住院号], [病人姓名], [年龄], [身份证], [检验ID]," + "\r\n" +
            //    "       [检验编码], [检验名称], [单位], [参考值], [升降], [检验值], [排序], [样本号]" + "\r\n" +
            //    "FROM   [JY检验结果]" + "\r\n" +
            //    "WHERE  病人ID='" + _person.sZYID父 + "' and [申请单号] = '" + s用申请唯一标识获取申请单号(_sLISID) +
            //    "'  order by 检验设备,样本号,convert(int,排序) ";
            //    DataTable dt检验结果 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL检验结果).Tables[0];

            //    InterFaceLIS.ReportComLittle report = new InterFaceLIS.ReportComLittle(dt病人信息, dt检验结果);
            //    report.ShowPreviewDialog();//软件暂停，等用户响应
            //    return true;
            //}
            //catch (Exception ex)
            //{
            //    return false;
            //    throw ex;
            //}
        }

        public static string s用申请唯一标识获取申请单号(string _sID)
        {
            try
            {
                string sql = "select 申请单号 from dbo.JY申请单摘要 where 申请唯一标识='" + _sID + "'";
                return HIS.Model.Dal.SqlHelper.ExecuteScalar(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, sql).ToString();
            }
            catch (Exception)
            {
                return "";
            }

        }
        public static bool b显示门诊结果(string _sMZID)
        {
            try
            {
                string SQL基本信息 =
                     "select '" + WEISHENG.COMM.zdInfo.ModelUserInfo.单位编码 + "' 医院名称,'' 报表名称, 病人姓名 姓名, 性别, 年龄, 年龄单位, bb.[科室名称] 送检科室, '' 床号," + "\r\n" +
                     "       '' 样本类别, '' 检验类别, 临床诊断 诊断, '' 样本号, '' 样本条码号, bb.[用户名] 申请者," + "\r\n" +
                     "       '' 备注, '' 检验时间, '' 检验者, '' 医院地址, '' 审核时间, '' 检验实验室," + "\r\n" +
                     "       '' 医院联系电话" + "\r\n" +
                     "from   [MF门诊摘要] aa left outer join pubUser bb on aa.医生编码 = bb.[用户编码]" + "\r\n" +
                     "where  MZID = " + _sMZID;

                DataTable dt病人信息 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL基本信息).Tables[0];

                string SQL检验结果 =
                "SELECT [申请单号], [检验日期], [检验设备], [病人ID], [住院号], [病人姓名], [年龄], [身份证], [检验ID]," + "\r\n" +
                "       [检验编码], [检验名称], [单位], [参考值], [升降], [检验值], [排序], [样本号]" + "\r\n" +
                "FROM   [JY检验结果]" + "\r\n" +
                "WHERE  [申请单号] = '" + s用申请唯一标识获取申请单号(_sMZID) + "'";
                DataTable dt检验结果 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL检验结果).Tables[0];

                InterFaceLIS.ReportComLittle report = new InterFaceLIS.ReportComLittle(dt病人信息, dt检验结果);
                report.ShowPreviewDialog();//软件暂停，等用户响应
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public static bool b更新门诊申请单状态(string _sMZID, enLIS申请单状态 _en状态, SqlTransaction connT)
        {
            try
            {
                string SQL =
                "update [JY申请单摘要]" + "\r\n" +
                "set [状态]='" + _en状态.ToString() + "'" + "\r\n" +
                "where 申请唯一标识='" + _sMZID + "'";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(connT, CommandType.Text, SQL);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public static bool b生成住院申请单(CHISEntities chis, ZY病人信息 inpatient, HIS.Model.Pojo.Pojo医嘱执行 pojo父医嘱申请摘要, List<HIS.Model.Pojo.Pojo医嘱执行> pojo子医嘱申请明细)
        {
            try
            {
                //bool b是否提交 = true;
                //DataTable dt申请明细 = dt电子医嘱;
                //DataTable dt申请序列 = dt申请明细.DefaultView.ToTable(true, "lisID");
                //var lis申请序列 = pojo子医嘱申请明细.Select(p => new { lisID = p.lisID, HIS组合名称 = p.项目名称 }).Distinct().ToList();


                //foreach (var row摘要 in lis申请序列)
                //{
                string s申请单号 = s生成申请单号(InterFaceLIS.ClassLIS.enLIS申请类型.住院);
                var inpatient2 = chis.ZY病人信息.Where(c => c.ZYID == inpatient.ZYID).FirstOrDefault();
                //string _sHIS组合名称 = HIS.Model.Dal.SqlHelper.ExecuteScalar(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text,
                //    "select 项目名称 from [YS住院医嘱] where id=" + row摘要["lisID"].ToString()).ToString();
                chis.JY申请单摘要.Add(new JY申请单摘要()
                {
                    申请类别 = "检验",
                    申请单号 = Convert.ToDecimal(s申请单号),
                    病人ID = Convert.ToInt32(inpatient2.ZYID),
                    病历号 = inpatient2.住院号码,
                    病人类型 = "住院",
                    病人姓名 = inpatient2.病人姓名,
                    性别 = inpatient2.性别,
                    病人年龄 = HIS.COMM.Helper.ageHelper.GetAgeByBirthdate(Convert.ToDateTime(inpatient2.出生日期)).ToString(),
                    收费类型 = inpatient2.医保类型,
                    临床诊断 = inpatient2.疾病名称,
                    病人病区 = Convert.ToString(inpatient2.病区),
                    床号 = inpatient2.病床,
                    申请科室 = chis.GY科室设置.Where(c => c.科室编码 == inpatient2.科室).SingleOrDefault().科室名称,
                    申请医生 = inpatient2.主治医生编码.ToString(),
                    申请时间 = DateTime.Now,
                    加急标志 = "0",
                    标本类型 = "标本类型",
                    执行科室 = HIS.COMM.baseInfo.sLIS执行科室名称,
                    申请唯一标识 = pojo父医嘱申请摘要.lisID.ToString(),
                    身份证号 = inpatient2.身份证号,
                    健康档案号 = "",
                    状态 = "已申请",
                    HIS组合名称 = pojo父医嘱申请摘要.项目名称,
                });
                //string SQL申请摘要 =
                //    "INSERT INTO [JY申请单摘要]([申请类别], [申请单号], [病人ID], [病历号], [病人类型]," + "\r\n" +
                //    "              [病人姓名], [性别],  [病人年龄], [收费类型]," + "\r\n" +
                //    "              [临床诊断], [病人病区], [床号], [申请科室], [申请医生]," + "\r\n" +
                //    "              [申请时间], [加急标志], [标本类型], [执行科室], [申请唯一标识]," + "\r\n" +
                //    "              [身份证号], [健康档案号], [状态], [HIS组合名称]) " + "\r\n" +
                //    "VALUES      ('检验', " + s申请单号 + ", " + zyPerson.sZYID父 + ", '" + zyPerson.住院号码 + "', '住院', '" + zyPerson.S姓名 + "', '" + zyPerson.S性别 + "'," + "\r\n" +
                //    "              '" + zyPerson.S年龄 + "', '" + zyPerson.S医保类型 + "', '" + zyPerson.s疾病名称 + "', '', '', '" + zyPerson.S科室名称 + "'," + "\r\n" +
                //    "             '" + zyPerson.主治医生编码 + "', '" + DateTime.Now.ToString() + "', '0', '标本类型', '" + HIS.COMM.baseInfo.sLIS执行科室名称 + "', '" + row摘要["lisID"].ToString() + "', '" + zyPerson.身份证号 + "'," + "\r\n" +
                //    "             '', '已申请', '" + _sHIS组合名称 + "')";
                //int reSult = HIS.Model.Dal.SqlHelper.ExecuteNonQuery(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL申请摘要);
                //if (reSult != 1)
                //{
                //    b是否提交 = false;
                //}

                //DataTable dt本序列明细 = HIS.Model.Dal.autoDb.GetNewDataTable(dt申请明细, "lisID=" + row摘要["lisID"].ToString());
                //var lis序列明细 = pojo子医嘱申请明细.Where(c => c.lisID == pojo父医嘱申请摘要.lisID).ToList();
                foreach (var row明细 in pojo子医嘱申请明细)
                {
                    var _sLIS条目 = HIS.Model.CacheData.Lis_HisLis项目对照.Where(c => c.his编码 == row明细.项目编码).FirstOrDefault();
                    if (_sLIS条目 == null)
                    {
                        WEISHENG.COMM.msgHelper.ShowInformation(row明细.项目名称 + "获取LIS对照失败，请联系管理员进行设置");
                        return false;
                    }
                    //strLIS条目 _sLIS条目 = sGetLis对照编码(row明细.项目编码.ToString());
                    //if (_sLIS条目.sLIS编码 != null)
                    //{
                    chis.JY申请单明细.Add(new JY申请单明细()
                    {
                        申请单号 = s申请单号,
                        lis申请项目ID = _sLIS条目.lisID,
                        lis申请项目代码 = _sLIS条目.lis编码,
                        lis申请项目名称 = _sLIS条目.lis名称,
                        his申请项目单价 = 0,
                        his申请项目数量 = 1,
                        计价标志 = "0",
                        医嘱ID = pojo父医嘱申请摘要.lisID.ToString(),
                        计费ID = "-1",
                        执行科室 = HIS.COMM.baseInfo.sLIS执行科室名称

                    });
                    //string SQL申请明细 =
                    // "INSERT INTO [JY申请单明细]([申请单号], [lis申请项目ID], [lis申请项目代码], [lis申请项目名称]," + "\r\n" +
                    // "              [his申请项目单价], [his申请项目数量], [计价标志], [医嘱ID]," + "\r\n" +
                    // "              [计费ID], [执行科室])" + "\r\n" +
                    // "VALUES      ('" + s申请单号 + "', '" + _sLIS条目.sLISID + "', '" + _sLIS条目.sLIS编码 + "', '" + _sLIS条目.sLIS名称 + "', 0," + "\r\n" +
                    // "             1, '0', '-1', '-1', '" + HIS.COMM.baseInfo.sLIS执行科室名称 + "')";
                    //int reSult2 = HIS.Model.Dal.SqlHelper.ExecuteNonQuery(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL申请明细);
                    //if (reSult2 != 1)
                    //{
                    //    b是否提交 = false;
                    //}
                    //}
                }
                //}

                return true;

            }
            catch (Exception ex)
            {
                msgHelper.ShowInformation(ex.Message);
                return false;
            }
        }

        public static bool b生成门诊申请单(HIS.COMM.Person门诊病人 mzPerson, DataTable dt电子处方)
        {
            try
            {
                //bool b是否提交 = true;
                //DataTable dt检验申请 = HIS.Model.Dal.autoDb.GetNewDataTable(dt电子处方, "YPID=0 and LIS检查项=1");
                //DataTable dt申请序列 = dt检验申请.DefaultView.ToTable(true, "套餐编号", "套餐名称");
                //foreach (DataRow row摘要 in dt申请序列.Rows)
                //{
                //    string s申请单号 = s生成申请单号(InterFaceLIS.ClassLIS.enLIS申请类型.门诊);
                //    string SQL申请摘要 =
                //        "INSERT INTO [JY申请单摘要]([申请类别], [申请单号], [病人ID], [病历号], [病人类型]," + "\r\n" +
                //        "              [病人姓名], [性别],  [病人年龄], [收费类型]," + "\r\n" +
                //        "              [临床诊断], [病人病区], [床号], [申请科室], [申请医生]," + "\r\n" +
                //        "              [申请时间], [加急标志], [标本类型], [执行科室], [申请唯一标识]," + "\r\n" +
                //        "              [身份证号], [健康档案号], [状态], [HIS组合名称])" + "\r\n" +
                //        "VALUES      ('检验', " + s申请单号 + ", -1, '', '门诊', '" + mzPerson.S姓名 + "', '" + mzPerson.S性别 + "'," + "\r\n" +
                //        "              '" + mzPerson.S年龄 + "', '" + mzPerson.En医保类型.ToString() + "', '" + mzPerson.S临床诊断 + "', '', '', '" + WEISHENG.COMM.zdInfo.ModelUserInfo.科室名称 + "'," + "\r\n" +
                //        "             '" + mzPerson.S医生编码 + "', '" + DateTime.Now.ToString() + "', '0', '标本类型', '" + HIS.COMM.baseInfo.sLIS执行科室名称 + "', '" + mzPerson.sMZID + "', '" + mzPerson.S身份证号 + "'," + "\r\n" +
                //        "             '', '已申请', '" + row摘要["套餐名称"].ToString() + "')";
                //    int reSult = HIS.Model.Dal.SqlHelper.ExecuteNonQuery(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL申请摘要);
                //    if (reSult != 1)
                //    {
                //        MessageBox.Show("写申请摘要影响行数:" + reSult.ToString(), "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //        b是否提交 = false;
                //    }
                //    DataTable dt本序列明细 = HIS.Model.Dal.autoDb.GetNewDataTable(dt检验申请, "套餐名称='" + row摘要["套餐名称"].ToString() + "'");
                //    foreach (DataRow row in dt本序列明细.Rows)
                //    {
                //        strLIS条目 _sLIS条目 = sGetLis对照编码(row["费用序号"].ToString());
                //        if (_sLIS条目.sLIS编码 != null)
                //        {
                //            string SQL申请明细 =
                //             "INSERT INTO [JY申请单明细]([申请单号], [lis申请项目ID], [lis申请项目代码], [lis申请项目名称]," + "\r\n" +
                //             "              [his申请项目单价], [his申请项目数量], [计价标志], [医嘱ID]," + "\r\n" +
                //             "              [计费ID], [执行科室])" + "\r\n" +
                //             "VALUES      ('" + s申请单号 + "', '" + _sLIS条目.sLISID + "', '" + _sLIS条目.sLIS编码 + "', '" + _sLIS条目.sLIS名称 + "', 0," + "\r\n" +
                //             "             1, '0', '-1', '-1', '" + HIS.COMM.baseInfo.sLIS执行科室名称 + "')";
                //            int reSult2 = HIS.Model.Dal.SqlHelper.ExecuteNonQuery(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL申请明细);
                //            if (reSult2 != 1)
                //            {
                //                MessageBox.Show("写申请摘要影响行数:" + reSult2.ToString(), "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //                b是否提交 = false;
                //            }
                //        }
                //    }
                //}
                //if (b是否提交)
                //{
                //    return true;
                //}
                //else
                //{
                return false;
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }

        private static string s生成申请单号(enLIS申请类型 _en申请类型)
        {

            decimal dec数据库号 = Convert.ToDecimal(HIS.Model.Dal.SqlHelper.ExecuteScalar(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text,
              "select isnull(max(申请单号),0) from JY申请单摘要 where left(申请单号,1)=" + ((int)_en申请类型).ToString()));
            decimal dec计算号 = Convert.ToDecimal(((int)_en申请类型).ToString() + DateTime.Now.ToString("yyyyMMdd").Substring(2, 6) + "00000");
            if (dec计算号 >= dec数据库号)
            {
                return (dec计算号 + 1).ToString();
            }
            else
            {
                return (dec数据库号 + 1).ToString();
            }
        }

        public static strLIS条目 sGetLis对照编码(string sHIS编码)
        {
            DataTable dt对照表 = HIS.COMM.baseInfo.dtHISLIS项目对照表;
            strLIS条目 resultStr = new strLIS条目();
            try
            {
                DataRow[] rows = dt对照表.Select("HIS编码='" + sHIS编码 + "'");
                if (rows.Length == 0)
                {
                    //resultStr = null;
                }
                else
                {
                    resultStr.sLIS编码 = rows[0]["LIS编码"].ToString();
                    resultStr.sLIS名称 = rows[0]["LIS名称"].ToString();
                    resultStr.s设备名称 = rows[0]["LIS设备"].ToString();
                    resultStr.sLISID = rows[0]["LISID"].ToString();
                }
            }
            catch (Exception ee)
            {
                throw ee;
            }
            return resultStr;
        }

        public struct strLIS条目
        {
            public string sLIS编码;
            public string sLIS名称;
            public string sLISID;
            public string s设备名称;
        }

        public enum enLIS申请类型
        {
            住院 = 1,
            门诊 = 2,
            体检 = 3
        }
        public enum enLIS申请单状态
        {
            已申请,//医生开具
            已收款,//收款确认
            已确认
        }
        //CREATE=生成条码 PRINT=打印 DELETE=删除/作废 COLLECT=采样确认 SEND=送出确认 RECIEVE=接收标本 ROLLBACK=剔回 FINISH=报告审核完成 CALLBACK=解除审核召回报告；ADDPRELISREQ=预制条码LIS开单完成；DELPRELISREQ=预制条码LIS开单作废；CANCELRECIEVE=取消签收（和剔回不同）

    }
}
