﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace HIS.InterFaceLIS
{
    public partial class ReportComLittle : DevExpress.XtraReports.UI.XtraReport
    {
        //public ReportComLittle()
        //{
        //    InitializeComponent();
        //}

        //DataTable dr基本信息包含的列：
        //      【列名】----【说明】
        //      医院名称
        //      报表名称----“检验报告单”
        //      姓名    ----病人姓名
        //      性别    ----病人性别
        //      年龄
        //      年龄单位
        //      送检科室----开出化验单的科室
        //      床号    ----针对住院病人，门诊病人设置为空字符
        //      样本类别----例如血清、尿液等
        //      检验类别----病人类别;门诊、住院、急诊等
        //      诊断    ----病人的疾病名称
        //      样本号  
        //      样本条码号
        //      申请者  ----开出化验申请单的医生
        //      备注
        //      检验时间
        //      检验者
        //      医院地址
        //      审核时间
        //      检验实验室
        //      医院联系电话

        //DataTable  dt检验项目 列名如下：
        //      结果序号   --根据“JY检验结果”表中的“排序”字段进行排序
		//		[检验编码]
		//	  	[检验名称]
		//	  	[检验值]
		//	  	[单位]
		//	  	[升降]
		//	  	[参考值]

        public ReportComLittle(DataTable dr基本信息, DataTable dt检验项目)
        {
            InitializeComponent();

            //strJYid = strid;

            //设置报告头信息
            xrLabelTitle.Text = dr基本信息.Rows[0]["医院名称"].ToString() + dr基本信息.Rows[0]["报表名称"].ToString();
            xrLabel姓名.Text = dr基本信息.Rows[0]["姓名"].ToString();
            xrLabel性别.Text = dr基本信息.Rows[0]["性别"].ToString();
            xrLabel年龄.Text = dr基本信息.Rows[0]["年龄"].ToString() + dr基本信息.Rows[0]["年龄单位"].ToString();
            xrLabel科室.Text = dr基本信息.Rows[0]["送检科室"].ToString();
            xrLabel床号.Text = dr基本信息.Rows[0]["床号"].ToString();
            xrLabel样本类型.Text = dr基本信息.Rows[0]["样本类别"].ToString();
            xrLabel患者类别.Text = dr基本信息.Rows[0]["检验类别"].ToString();
            xrLabel病历号.Text = dr基本信息.Rows[0]["住院号码"].ToString();
            xrLabel临床诊断.Text = dr基本信息.Rows[0]["诊断"].ToString();
            //xrLabel样本号.Text = dr基本信息.Rows[0]["样本号"].ToString();
            xrLabel条码.Text = dr基本信息.Rows[0]["样本条码号"].ToString();
            xrLabel送检医师.Text = dr基本信息.Rows[0]["申请者"].ToString();

            xrLabel备注.Text = dr基本信息.Rows[0]["备注"].ToString();
            //xrLabel检验时间.Text = dr基本信息.Rows[0]["检验时间"].ToString();
            xrLabel检验者.Text = dr基本信息.Rows[0]["检验者"].ToString();
            xrLabel医院地址.Text = dr基本信息.Rows[0]["医院地址"].ToString();
            //xrLabel检验时间安排.Text = dtDataSource.Rows[0]["检验时间安排"].ToString();
            //xrLabel采样地点.Text = dtDataSource.Rows[0]["采样地点"].ToString();

            xrLabel报告时间.Text = dr基本信息.Rows[0]["审核时间"].ToString();
            //xrLabel审核者.Text = dtDataSource.Rows[0]["审核者"].ToString();

            //xrLabel打印时间.Text = dtDataSource.Rows[0]["打印时间"].ToString();
            xrLabel打印时间.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            xrLabel实验室.Text = dr基本信息.Rows[0]["检验实验室"].ToString();
            xrLabel联系电话.Text = dr基本信息.Rows[0]["医院联系电话"].ToString();

            xrLabel样本号.DataBindings.Add("Text", null, "样本号");
            xrLabel检验时间.DataBindings.Add("Text", null, "检验日期");
            xrTableCell序号.DataBindings.Add("Text", null, "排序");
            xrTableCell项目代码.DataBindings.Add("Text", null, "检验编码");
            xrTableCell项目名称.DataBindings.Add("Text", null, "检验名称");
            xrTableCell结果.DataBindings.Add("Text", null, "检验值");
            xrTableCell单位.DataBindings.Add("Text", null, "单位");
            xrTableCell标记.DataBindings.Add("Text", null, "升降");
            xrTableCell参考值.DataBindings.Add("Text", null, "参考值");

            //printRow["结果序号"] = int序号;
            //printRow["结果项目代码"] = dtResult.Rows[i]["fitem_code"];
            //printRow["结果项目名称"] = dtResult.Rows[i]["fitem_name"];
            //printRow["结果值"] = dtResult.Rows[i]["fvalue"];
            //printRow["结果项目单位"] = dtResult.Rows[i]["fitem_unit"];
            //printRow["结果标记"] = dtResult.Rows[i]["fitem_badge"];
            //printRow["结果参考值"] = dtResult.Rows[i]["fitem_ref"];
            this.DataSource = dt检验项目;
        }
    }
}
