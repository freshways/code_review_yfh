﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
//using ww.form.wwf;
//using ww.lis.lisbll.sam;
//using ww.wwf.wwfbll;
using System.Collections;
using TableXtraReport;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;

namespace HIS.InterFaceLIS
{
    public partial class ApplyQueryNewForm : Form
    {
        //protected ww.wwf.wwfbll.UserdefinedFieldUpdateForm showApply = null;
        //protected ww.wwf.wwfbll.UserdefinedFieldUpdateForm showResult = null;
        //protected PatientBLL bllPatient = new PatientBLL();
        //protected TypeBLL bllType = new TypeBLL();//公共类型逻辑  
        protected ApplyBLL bllApply = new ApplyBLL();//申请规则
        //protected string strHelpID = "";//帮助ID

        protected DataTable dtMain;
        protected DataTable dtResult = new DataTable();

        string str申请单id = "";

        public ApplyQueryNewForm()
        {
            InitializeComponent();
            dataGridViewApply.AutoGenerateColumns = false;
            this.dataGridViewItem.AutoGenerateColumns = false;
        }


        private void ApplyQueryNewForm_Load(object sender, EventArgs e)
        {
            this.WWComTypeList();
            //this.WWGetTreeHZ();
            this.WWSetShowApply("lis_sam_apply_query_apply");
            this.Set病人类型();
            Get病人病区();
            txt申请科室.Text = WEISHENG.COMM.zdInfo.ModelUserInfo.科室名称;
        }
        private void Get病人病区()
        {
            DataTable dt = new DataTable();
            dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text,
                "select 科室名称,科室编码 from GY科室设置 where 是否住院科室=1 ").Tables[0];

            txt申请科室.Items.Clear();

            foreach (DataRow row in dt.Rows)
            {
                txt申请科室.Items.Add(row["科室名称"]);
            }
        }

        private void Set病人类型()
        {
            string sql = "SELECT * FROM sam_type where (ftype='病人类别' and fuse_if='1')  ORDER BY forder_by ";
            //
            DataTable dt = //this.bllType.BllComTypeDT("病人类别", 1, "");
            HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.sLISConn, CommandType.Text, sql).Tables[0];
            this.comboBox病人类型.DataSource = dt;
        }

        #region        用户自定义方法
        protected void WWComTypeList()
        {
            try
            {
                string sql = "SELECT * FROM sam_type where (ftype='样本状态' and fuse_if='1')  ORDER BY forder_by ";

                this.sam_typeBindingSource_fstate.DataSource = //this.bllType.BllComTypeDT("样本状态", 1, "");
                    HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.sLISConn, CommandType.Text, sql).Tables[0];
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        /// <summary>
        /// 设置风格 申请
        /// </summary>
        /// <param name="strID"></param>
        protected void WWSetShowApply(string strID)
        {
            try
            {
                dtMain = new DataTable();

                dtMain = this.BllSamplingMainDTFromHis(" fapply_time >'" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' ");               

                bindingSourceApply.DataSource = dtMain;
                dataGridViewApply.DataSource = bindingSourceApply;


                string sql = "SELECT * FROM wwf_columns where ftable_id='" + strID + "' ORDER BY forder_by";
                dtCurr = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.sLISConn, CommandType.Text, sql).Tables[0];


                this.DataGridViewSetStyleNew(this.dataGridViewApply);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        /// <summary>
        /// 获取申请列表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <param name="ChisDBName"></param>
        /// <returns></returns>
        private DataTable BllSamplingMainDTFromHis(string strWhere)
        {
            //" + WEISHENG.COMM.zdInfo.iDwid + "
            string strsql = "select isnull(t2.分类名称,'') 分类名称,t1.* from CHIS.dbo.vw_lis_request_inp t1 left join vw_检验项目分类 t2 on t1.fitem_group = t2.HIS检查名称 where 1=1 ";
            if (!(string.IsNullOrWhiteSpace(strWhere)))
            {
                strsql += " and " + strWhere;
            }
            if (!ck已打印.Checked)
                strsql += " and not exists (select 1 from SAM_APPLY ap where t1.fapply_id = ap.fapply_id)"; //排除本地已有的申请单
            strsql += " order by t1.fhz_id,t2.分类名称,t1.his_recordid  ";

            return HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.sLISConn, CommandType.Text, strsql).Tables[0];
        }

        DataTable dtCurr = new DataTable();
        /// <summary>
        /// 定义Grid格式
        /// </summary>
        /// <param name="dgvShow"></param>
        public void DataGridViewSetStyleNew(DataGridView dgvShow)
        {
            try
            {
                DataRow[] colList_OK = dtCurr.Select("fshow_flag>=0", "forder_by");
                int intOrder = 0;
                int fread_flage = 0;
                if (colList_OK.Length > 0)
                {
                    foreach (DataRow drC in colList_OK)
                    {
                        if (drC["fread_flage"].ToString() != null || drC["fread_flage"].ToString() != "")
                        {
                            fread_flage = Convert.ToInt32(drC["fread_flage"].ToString());
                        }
                        if (drC["forder_by"].ToString() != null || drC["forder_by"].ToString() != "")
                        {
                            intOrder = Convert.ToInt32(drC["forder_by"].ToString());
                            if (intOrder >= colList_OK.Length)
                                intOrder = colList_OK.Length - 1;
                        }
                        else
                            intOrder = colList_OK.Length - 1;

                        DataGridViewTextBoxColumn newCol = new DataGridViewTextBoxColumn();
                        string strCName_OK = drC["fcode"].ToString();
                        string strfvalue_type = drC["fvalue_type"].ToString();
                        int intfshow_flag = Convert.ToInt32(drC["fshow_flag"].ToString());
                        if (strfvalue_type == "预定义")
                        {
                            dgvShow.Columns[strCName_OK].Width = Convert.ToInt32(drC["fshow_width"].ToString());

                            dgvShow.Columns[strCName_OK].DisplayIndex = intOrder;
                            dgvShow.Columns[strCName_OK].HeaderText = drC["fname"].ToString();
                            if (fread_flage == 0)
                                dgvShow.Columns[strCName_OK].ReadOnly = false;
                            else
                                dgvShow.Columns[strCName_OK].ReadOnly = true;
                        }
                        else
                        {
                            newCol.DataPropertyName = strCName_OK;
                            newCol.Name = strCName_OK;
                            if (drC["fshow_width"].ToString() != "" || drC["fshow_width"].ToString() != null)
                                newCol.Width = Convert.ToInt32(drC["fshow_width"].ToString());
                            if (drC["fname"].ToString() != null || drC["fname"].ToString() != "")
                                newCol.HeaderText = drC["fname"].ToString();
                            newCol.DisplayIndex = intOrder;

                            if (fread_flage == 0)
                                newCol.ReadOnly = false;
                            else
                                newCol.ReadOnly = true;

                            dgvShow.Columns.AddRange(new DataGridViewColumn[] { newCol });
                        }
                        if ((intfshow_flag == 0) || (strfvalue_type == "主键"))
                            dgvShow.Columns[strCName_OK].Visible = false;
                        else
                            dgvShow.Columns[strCName_OK].Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        /// <summary>
        /// 取得申请列表
        /// </summary>
        private void WWGetApplyList()
        {
            labelNotify.ForeColor = Color.Black;
            try
            {
                dtMain = new DataTable();
                string fjy_date1 = this.fjy_dateDateTimePicker1.Value.ToString("yyyy-MM-dd");
                string fjy_date2 = this.fjy_dateDateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59");
                string fjy_病人类型 = this.comboBox病人类型.SelectedValue.ToString();

                //20150629 add wjz
                string fjy_门诊住院号 = this.textBox门诊住院号.Text;
                //add by wjz 20160125 修改查询条件，如果门诊主要号输入的数据是四位，则将其转换为门诊号 ▽
                if(string.IsNullOrWhiteSpace(this.textBox门诊住院号.Text))
                {
                }
                else if(this.textBox门诊住院号.Text.Trim().Length == 4)
                {
                    this.textBox门诊住院号.Text = DateTime.Now.ToString("yyyyMMdd")+"."+ this.textBox门诊住院号.Text.Trim();
                    fjy_门诊住院号 = this.textBox门诊住院号.Text;
                }
                //add by wjz 20160125 修改查询条件，如果门诊主要号输入的数据是四位，则将其转换为门诊号 △
                string strDateif = " cast(fapply_time as datetime)>='" + fjy_date1 + "' and cast(fapply_time as datetime)<='" + fjy_date2 + "' and ftype_id='" + fjy_病人类型 + "' and fhz_zyh like '%" + fjy_门诊住院号 + "%'";

                //设置查询条件
                if(!string.IsNullOrWhiteSpace(textBoxName.Text))
                { 
                    strDateif += " and fname='"+textBoxName.Text+"'";
                }

                if(!string.IsNullOrWhiteSpace(textBoxSFZH.Text))
                {
                    strDateif += " and sfzh='"+textBoxSFZH.Text+"' ";
                }
                if (!string.IsNullOrEmpty(txt申请科室.Text))
                {
                    strDateif += " and fapply_dept_id='" + txt申请科室.Text + "' ";                    
                }

                //add by wjz 20160125 修改门诊申请单的查询条件，对于门诊，只查询“已收款”的申请单信息。此处需要修改HIS视图 ▽
                if (fjy_病人类型.Trim() == "门诊")
                {
                    strDateif += " and 状态='已收款'";
                }
                //add by wjz 20160125 修改门诊申请单的查询条件，对于门诊，只查询“已收款”的申请单信息。此处需要修改HIS视图 △
                dtMain = this.BllSamplingMainDTFromHis(strDateif);

                //add by wjz 20160428 添加查询提示，提示是否查到了数据 ▽
                if (dtMain == null || dtMain.Rows.Count == 0)
                {
                    labelNotify.ForeColor = Color.Red;
                    labelNotify.Text = "未查到任何数据";
                    this.labelNotify.Visible = true;
                }
                else
                {
                    this.labelNotify.Visible = false;
                }
                //add by wjz 20160428 添加查询提示，提示是否查到了数据 △
                
                this.bindingSourceApply.DataSource = dtMain;
                dataGridViewApply.DataSource = bindingSourceApply;
                this.bN.BindingSource = bindingSourceApply;
                int intfstate = 0;
                for (int intGrid = 0; intGrid < this.dataGridViewApply.Rows.Count; intGrid++)
                {
                    intfstate = 0;
                    //修改此部分的代码，基本逻辑没有变化 20150624 wjz begin
                    if (this.dataGridViewApply.Rows[intGrid].Cells["fstate"].Value != null)
                    {
                        try
                        {
                            if (this.dataGridViewApply.Rows[intGrid].Cells["fstate"].Value.ToString() != "")
                                intfstate = Convert.ToInt32(this.dataGridViewApply.Rows[intGrid].Cells["fstate"].Value.ToString());
                        }
                        catch
                        { }
                    }
                    //if (intfstate == 1)//已执行申请
                    //this.dataGridViewApply.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Red;
                    if (intfstate == 2)//已打印条码
                    {
                        this.dataGridViewApply.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Blue;
                    }
                    else if (intfstate == 3)//已打印条码
                    {
                        this.dataGridViewApply.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Red;
                    }
                    //修改此部分的代码，基本逻辑没有变化 20150624 wjz end
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        /// <summary>
        /// 快捷键 设置
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref   System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {

            if (keyData == (Keys.F8))
            {
                tb查询.PerformClick();
                return true;
            }
            if (keyData == (Keys.F3))
            {
                tb打印.PerformClick();
                return true;
            }
            return base.ProcessCmdKey(ref   msg, keyData);
        }
        #endregion


        private void bindingSourceApply_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                DataRowView rowCurrent = (DataRowView)this.bindingSourceApply.Current;
                if (this.dtResult.Rows.Count > 0)
                    this.dtResult.Clear();
                if (rowCurrent != null)
                {
                    string strfapply_id = rowCurrent["fapply_id"].ToString();

                    str申请单id = strfapply_id;
                    //WWGetResultList(strfapply_id); //20150625 wjz del
                }

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void tb查询_Click(object sender, EventArgs e)
        {
            WWGetApplyList();
        }

        private void tb打印_Click(object sender, EventArgs e)
        {
            ToolStripButton but = (ToolStripButton)sender;// 打印完成不显示
            try
            {
                string s_打印 = dataGridViewApply.CurrentRow.Cells["状态"].Value.ToString();
                if (s_打印.Equals("打印"))
                {
                    if(!WWMessage.MessageDialogResult("该条码已打印，是否重新打印？"))
                        return;
                }
                str申请单id = dataGridViewApply.CurrentRow.Cells["fapply_id"].Value.ToString();
                string s分类 = dataGridViewApply.CurrentRow.Cells["分类名称"].Value.ToString();
                string s住院号 = dataGridViewApply.CurrentRow.Cells["fhz_zyh"].Value.ToString();
                string s检查项目 = dataGridViewApply.CurrentRow.Cells["fitem_group"].Value.ToString();
                if (!string.IsNullOrEmpty(s分类))
                {
                    //如果分类是空的则不处理，使用当前行的申请单id和检查项目，否则重新根据分类获取、、2015-11-9 09:52:41 yufh添加
                    //如果分类是空的情况下，不进行任何操作
                    //按照住院号和检验分类进行筛选数据，组织打印项目
                    DataRow[] drs = dtMain.Select("fhz_zyh='" + s住院号 + "' and 分类名称='" + s分类 + "'", "fapply_id asc");
                    //重新排序后，取第一个申请单号作为lis申请单号，这样就可以不用重新生成
                    str申请单id = drs[0]["fapply_id"].ToString();
                    s检查项目 = "";
                    foreach (DataRow dr in drs)
                        s检查项目 += dr["fitem_group"].ToString() + ",";
                    s检查项目 = s检查项目.Substring(0, s检查项目.Length - 1);
                }
                if (!(string.IsNullOrWhiteSpace(str申请单id)))
                {
                    int index = dataGridViewApply.CurrentRow.Index;
                    
                    string s姓名 = dataGridViewApply.Rows[index].Cells["fname"].Value.ToString(); //rowCurrent["fname"].ToString();
                    string s性别 = dataGridViewApply.Rows[index].Cells["fsex"].Value.ToString();
                    string s年龄 = dataGridViewApply.Rows[index].Cells["fage"].Value.ToString();
                    string s类型 = dataGridViewApply.Rows[index].Cells["ftype_id"].Value.ToString();
                    string s床号 = dataGridViewApply.Rows[index].Cells["fbed_num"].Value.ToString();
                    //string s检查 = dataGridViewApply.Rows[index].Cells["fitem_group"].Value.ToString();
                    string s申请医生 = HIS.Model.Dal.SqlHelper.ExecuteScalar(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, "select 用户名 from pubUser where 用户编码='" + dataGridViewApply.Rows[index].Cells["req_docno"].Value.ToString() + "'").ToString();
                    string s病人信息 = "";
                    if(s类型=="住院")
                        s病人信息 = s姓名 + "/" + s性别 + "/" + s年龄 + "  " + s类型 + "/" + s床号 + "/" + s申请医生;
                    else
                        s病人信息 = s姓名 + "/" + s性别 + "/" + s年龄 + "  " + s类型 + "/" + s申请医生;
                    try
                    {
                        if (but.Name == "tb打印预览")
                            Print(true, s病人信息, s检查项目, str申请单id);
                        else
                            Print(false, s病人信息, s检查项目, str申请单id);

                        //add by wjz 打印后刷新查询页面▽
                        WWGetApplyList();
                        //add by wjz 打印后刷新查询页面△

                    }
                    catch (Exception ex)
                    {
                        WWMessage.MessageShowWarning("打印失败！" + ex.Message);
                    }
                }
                else
                {
                    WWMessage.MessageShowWarning("所要打印的申请项目没有申请号。");
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowWarning("打印过程出现异常，异常信息如下：\n" + ex.Message);
            }
        }


        //add by wjz 20160504 打印时将该病人所有的化验项目都打印出来 ▽
        private void tb打印全部_Click(object sender, EventArgs e)
        {
            PrintAll(false);
        }

        private void PrintAll(bool b是否预览)
        {
            if (dataGridViewApply.CurrentRow == null)
            {
                WWMessage.MessageShowWarning("没有选择任何行！");
                return;
            }

            #region 获取病人基本信息
            string s住院号 = dataGridViewApply.CurrentRow.Cells["fhz_zyh"].Value.ToString();
            string s姓名 = dataGridViewApply.CurrentRow.Cells["fname"].Value.ToString();
            string s性别 = dataGridViewApply.CurrentRow.Cells["fsex"].Value.ToString();
            string s年龄 = dataGridViewApply.CurrentRow.Cells["fage"].Value.ToString();
            string s类型 = dataGridViewApply.CurrentRow.Cells["ftype_id"].Value.ToString();
            string s床号 = dataGridViewApply.CurrentRow.Cells["fbed_num"].Value.ToString();
            string s申请医生 = HIS.Model.Dal.SqlHelper.ExecuteScalar(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, "select 用户名 from pubUser where 用户编码='" + dataGridViewApply.CurrentRow.Cells["req_docno"].Value.ToString() + "'").ToString();
            #endregion

            //获取医师列表的代码已经注释
            #region 获取医生列表
            DataTable dtDoctors = null;
            try
            {
                dtDoctors = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.sLISConn, "SELECT [fperson_id],fname FROM WWF_PERSON").Tables[0];
            }
            catch
            {
                //防止后续使用此DataTable时，程序报错。
                dtDoctors = new DataTable();
                dtDoctors.Columns.Add("fperson_id");
                dtDoctors.Columns.Add("fname");
            }
            #endregion

            DataRow[] drs = dtMain.Select("fhz_zyh='" + s住院号 + "'", "分类名称,fitem_group");
            //----List<string> applyidList = new List<string>();//目的：用于存放“fitem_group”不为空时的fapply_id。HIS异常时可能会生成化验项目为空的申请信息
            Dictionary<String, String> applyiddic = new Dictionary<string, string>();
            TableXReport report = new TableXReport();

            string s病人信息 = "";
            if (s类型 == "住院")
                s病人信息 = s姓名 + "/" + s性别 + "/" + s年龄 + "  " + s类型 + "/" + s床号 + "/" + s申请医生;
            else
                s病人信息 = s姓名 + "/" + s性别 + "/" + s年龄 + "  " + s类型 + "/" + s申请医生;
            //add by wjz 20160620 为自助打印机添加条码打印 ▽
            if (s类型.Contains("门诊"))
            {
                AppendReport(ref report, s病人信息, "自助打印", drs[0]["fapply_id"].ToString(), s住院号, "");
            }
            //add by wjz 20160620 为自助打印机添加条码打印 △

            #region 目的：记录申请项目不为空的申请号,同时将申请项目按照“分类名称”进行汇总
            for (int index = 0; index < drs.Length; index++)
            {
                //检查项目是空的情况下，直接跳过此项
                if (string.IsNullOrWhiteSpace(drs[index]["fitem_group"].ToString()))
                {
                    continue;
                }

                #region 获取基本信息
                string s申请单号 = drs[index]["fapply_id"].ToString();
                string s检查 = drs[index]["fitem_group"].ToString();

                //applyidList.Add(drs[index]["fapply_id"].ToString());//记录fapply_id，方便记录申请单信息
                applyiddic.Add(s申请单号, s申请单号);

                //打印的条码中，不再显示医师姓名
                DataRow[] docs = dtDoctors.Select("fperson_id='" + drs[index]["req_docno"].ToString() + "'");
                //string s申请医生 = docs.Length > 0 ? docs[0]["fname"].ToString() : "";
                #endregion

                //分类名称是空的，每个单独汇总
                if (string.IsNullOrWhiteSpace(drs[index]["分类名称"].ToString()))
                {
                    //追加报表，修改记录
                    AppendReport(ref report, s病人信息, s检查, s申请单号, s住院号, s申请医生);
                }
                else
                {   //分类不是空的，需要找到相同分类的化验项目
                    string s分类Temp = drs[index]["分类名称"].ToString();

                    #region 找到相同分类的项目
                    index++;
                    while (index < drs.Length)
                    {
                        if (s分类Temp == drs[index]["分类名称"].ToString())
                        {
                            s检查 += "," + drs[index]["fitem_group"].ToString();
                            //applyidList.Add(drs[index]["fapply_id"].ToString());
                            applyiddic.Add(drs[index]["fapply_id"].ToString(), s申请单号);
                        }
                        else
                        {
                            break;
                        }
                        index++;
                    }
                    index--;
                    #endregion

                    //追加报表，修改记录
                    AppendReport(ref report, s病人信息, s检查, s申请单号, s住院号, s申请医生);
                }
            }
            #endregion

            #region 打印操作
            bool bPrintSuccess = false;
            report.PrintProgress += (sender, e) => { bPrintSuccess = true; };

            if (b是否预览)
            {
                report.PrinterName = HIS.COMM.baseInfo.S默认条码打印;
                report.ShowPreviewDialog();
            }
            else
            {
                report.Print(HIS.COMM.baseInfo.S默认条码打印);
            }
            #endregion

            if (bPrintSuccess)
            {
                //applyidList记录了已经打印出来的检验项目的申请号。接下来的操作：根据申请号向LIS中写入申请信息
                foreach (var item in applyiddic)
                {
                    SaveHisApplyNew(item.Key, item.Value);
                }

                //刷新查询结果
                WWGetApplyList();
            }
            else
            {
                WWMessage.MessageShowWarning("打印失败！");
            }
        }

        private void toolStripButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void btn查询_Click(object sender, EventArgs e)
        {
            WWGetApplyList();
        }

        private void btn生成条码_Click(object sender, EventArgs e)
        {
            try
            {
                SaveHisApply();
            }
            catch(Exception ex)
            {
                WWMessage.MessageShowWarning("打印过程出现异常，异常信息如下：\n" + ex.Message);
            }
            
            try
            {
                //刷新
                WWGetApplyList();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowWarning("刷新列表时出现异常，异常信息如下：\n" + ex.Message);
            }
        }

        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="是否预览">true or false</param>
        /// <param name="s病人信息">包含姓名、性别、年龄等组合字符串</param>
        /// <param name="s检查项目">检查项目</param>
        /// <param name="s申请单号">申请单号==>条码</param>
        private void Print(bool 是否预览, string s病人信息, string s检查项目, string s申请单号)
        {
            #region add by wjz 修改此方法的实现，化验项目如果在一个条码纸上打不开的话，可以用分页打印 ▽

            Font ft = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            Font ft7 = new System.Drawing.Font("宋体", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            int size = 20;

            //add by wjz 20160122 添加检查项目的排序 ▽
            string[] sub检查项目 = s检查项目.Split(',');
            if (sub检查项目.Length == 1)
            { }
            else
            {
                Array.Sort(sub检查项目);
                s检查项目 = sub检查项目[0];
                for(int index = 1; index < sub检查项目.Length;index++)
                {
                    s检查项目 += "," + sub检查项目[index];
                }
            }
            //add by wjz 20160122 添加检查项目的排序 △

            TableXReport xtr = new TableXReport();

            if (s检查项目.Length < 2 * size)
            {
                xtr.SetReportTitle("打印时间 " + System.DateTime.Now.ToString(), true, ft, TextAlignment.TopLeft, Size.Empty);
                xtr.SetReportTitle(s病人信息, true, ft, TextAlignment.TopLeft, Size.Empty);


                if (s检查项目.Length > size)
                {
                    xtr.SetReportTitle(s检查项目.Substring(0, size), true, ft7, TextAlignment.TopLeft, Size.Empty);
                    xtr.SetReportTitle(s检查项目.Substring(size), true, ft7, TextAlignment.TopLeft, Size.Empty);
                }
                else
                {
                    xtr.SetReportTitle(s检查项目, true, ft, TextAlignment.TopLeft, Size.Empty);
                }

                xtr.SetReportBarCode(s申请单号, new Point(5, 150), new Size(400, 125), null, true);
                xtr.SetReportMain(0, 0, 1, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(10, 10, 10, 10), System.Drawing.Printing.PaperKind.Custom, new Size(500, 315), 0, 0, "");
            }
            else
            {
                string[] arr检查 = s检查项目.Split(',');
                List<string> list检验 = new List<string>();

                list检验.Add(arr检查[0]);
                for (int index = 1; index < arr检查.Length; index++)
                {
                    if (list检验[list检验.Count - 1].Length + arr检查[index].Length < size * 2)
                    {
                        list检验[list检验.Count - 1] = list检验[list检验.Count - 1] + "," + arr检查[index];
                    }
                    else
                    {
                        list检验.Add(arr检查[index]);
                    }
                }

                xtr.SetReportTitle("打印时间 " + System.DateTime.Now.ToString(), true, ft, TextAlignment.TopLeft, Size.Empty);
                xtr.SetReportTitle(s病人信息, true, ft, TextAlignment.TopLeft, Size.Empty);


                if (list检验[0].Length > size)
                {
                    xtr.SetReportTitle(list检验[0].Substring(0, size), true, ft7, TextAlignment.TopLeft, Size.Empty);
                    xtr.SetReportTitle(list检验[0].Substring(size), true, ft7, TextAlignment.TopLeft, Size.Empty);
                }
                else
                {
                    xtr.SetReportTitle(list检验[0], true, ft, TextAlignment.TopLeft, Size.Empty);
                }

                xtr.SetReportBarCode(s申请单号, new Point(5, 150), new Size(400, 125), null, true);
                xtr.SetReportMain(0, 0, 1, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(10, 10, 10, 10), System.Drawing.Printing.PaperKind.Custom, new Size(500, 315), 0, 0, "");

                xtr.CreateDocument();

                for (int index = 1; index < list检验.Count; index++)
                {
                    CombineReports(xtr, ft, ft7, s病人信息, list检验[index], s申请单号);
                }

                xtr.PrintingSystem.ContinuousPageNumbering = true;
            }
            #endregion

            xtr.PrintProgress += new PrintProgressEventHandler(xtr_PrintProgress);

            if (是否预览)
            {
                #region 隐藏打印按钮
                //创建报表示例，指定到一个打印工具
                ReportPrintTool pt = new ReportPrintTool(xtr);

                // 获取打印工具的PrintingSystem
                PrintingSystemBase ps = pt.PrintingSystem;

                //// 隐藏Watermark 工具按钮和菜单项.
                //if (ps.GetCommandVisibility(PrintingSystemCommand.Watermark) != CommandVisibility.None)
                //{
                //    ps.SetCommandVisibility(PrintingSystemCommand.Watermark,CommandVisibility.None);                    
                //}
                //// 显示Document Map工具按钮和菜单项
                //ps.SetCommandVisibility(PrintingSystemCommand.DocumentMap, CommandVisibility.All);

                //隐藏打印按钮
                ps.SetCommandVisibility(new DevExpress.XtraPrinting.PrintingSystemCommand[] 
    { 
        DevExpress.XtraPrinting.PrintingSystemCommand.Background , 
        DevExpress.XtraPrinting.PrintingSystemCommand.ClosePreview ,
            DevExpress.XtraPrinting.PrintingSystemCommand.Customize ,
            DevExpress.XtraPrinting.PrintingSystemCommand.DocumentMap ,
            DevExpress.XtraPrinting.PrintingSystemCommand.File ,
            DevExpress.XtraPrinting.PrintingSystemCommand.FillBackground ,
            DevExpress.XtraPrinting.PrintingSystemCommand.Open ,
            DevExpress.XtraPrinting.PrintingSystemCommand.PageSetup ,
            DevExpress.XtraPrinting.PrintingSystemCommand.Print ,
            DevExpress.XtraPrinting.PrintingSystemCommand.PrintDirect ,
            DevExpress.XtraPrinting.PrintingSystemCommand.Save,
            DevExpress.XtraPrinting.PrintingSystemCommand.Watermark,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportCsv,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportFile,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportGraphic,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportHtm,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportMht,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportPdf,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportRtf,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportTxt,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportXls,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportXlsx,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportXps,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendCsv,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendFile,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendGraphic,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendMht,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendPdf,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendRtf,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendTxt,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendXls,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendXlsx,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendXps,
            DevExpress.XtraPrinting.PrintingSystemCommand.SubmitParameters
        }, CommandVisibility.None);

                #endregion
                //显示报表预览.
                pt.ShowPreview();
            }
            else
                xtr.Print(HIS.COMM.baseInfo.S默认条码打印);
        }

        //add by wjz 20151226 一个条码纸上可能无法将所有的项目全部打印出来，添加此方法的目的：实现分页打印 ▽
        private void CombineReports(TableXReport firstRep, Font ft9, Font ft7,string s病人信息, string sub检查项目, string s申请单号)
        {
            TableXReport subRep = new TableXReport(); 
            subRep.SetReportTitle("打印时间 " + System.DateTime.Now.ToString(), true, ft9, TextAlignment.TopLeft, Size.Empty);
            subRep.SetReportTitle(s病人信息, true, ft9, TextAlignment.TopLeft, Size.Empty);

            //xtr.SetReportTitle(s检查, true, ft, TextAlignment.TopLeft, Size.Empty);
            int size = 20;
            if (sub检查项目.Length > size)
            {
                subRep.SetReportTitle(sub检查项目.Substring(0, size), true, ft7, TextAlignment.TopLeft, Size.Empty);
                subRep.SetReportTitle(sub检查项目.Substring(size), true, ft7, TextAlignment.TopLeft, Size.Empty);
            }
            else
            {
                subRep.SetReportTitle(sub检查项目, true, ft9, TextAlignment.TopLeft, Size.Empty);
            }

            subRep.SetReportBarCode(s申请单号, new Point(5, 150), new Size(400, 125), null, true);
            subRep.SetReportMain(0, 0, 1, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(10, 10, 10, 10), System.Drawing.Printing.PaperKind.Custom, new Size(500, 315), 0, 0, "");

            subRep.CreateDocument();

            firstRep.Pages.AddRange(subRep.Pages);
        }
        //add by wjz 20151226 一个条码纸上可能无法将所有的项目全部打印出来，添加此方法的目的：实现分页打印 △

        private void xtr_PrintProgress(object sender, PrintProgressEventArgs e)
        {
            #region HIS负责条码的生成，LIS段负责打印条码后，将申请信息写入LIS数据库，这部分代码是新增的
            string strUpdate = SaveHisApply();
            //更新显示状态
            DataGridViewRow row = this.dataGridViewApply.CurrentRow;
            if (strUpdate.Equals("true") && row != null)
            {
                //this.dtMain.Rows[applySelectedIndex]["fstate"] = 2;
                row.Cells["fstate"].Value = "2";
                row.DefaultCellStyle.ForeColor = Color.Blue;
            }
            #endregion
        }

        /// <summary>
        /// 此方法负责保存从HIS中取出的化验申请，申请条码的生成由HIS段负责（Lis段不再操作申请条码）
        /// 2015-08-20 18:49:48 yufh 调整按照分类进行生成条码，打印并保存
        /// </summary>
        private string SaveHisApply()
        {
            DataGridViewRow row = this.dataGridViewApply.CurrentRow;
            int selectedIndex = this.dataGridViewApply.CurrentRow.Index;

            if (row == null)
            {
                WWMessage.MessageShowWarning("没有选中任何行");
                return "false";
            }
            //string hisRecordid = row.Cells["his_recordid"].Value.ToString();         
            string s检查分类 = row.Cells["分类名称"].Value.ToString();
            string s住院号 = row.Cells["fhz_zyh"].Value.ToString();

            //changed by wjz 20151109 条码打印时，分类名称是空白的项目，回打到一个条码上，修改此bug ▽
            #region old code
            //DataRow[] drs = dtMain.Select("fhz_zyh='" + s住院号 + "' and 分类名称='" + s检查分类 + "'");
            #endregion
            string strID = row.Cells["fapply_id"].Value.ToString();

            //
            DataRow[] drs;
            if (s检查分类 == null || s检查分类 == "")
            {
                drs = dtMain.Select("fapply_id='" + strID + "'");
            }
            else
            {
                drs = dtMain.Select("fhz_zyh='" + s住院号 + "' and 分类名称='" + s检查分类 + "'");
            }
            //changed by wjz 20151109 条码打印时，分类名称是空白的项目，回打到一个条码上，修改此bug △

            this.str申请单id = drs[0]["fapply_id"].ToString();

            //循环保存单据
            string 执行状态 = "false";
            foreach (DataRow dr in drs)
            {
                DataRow SelectedRow = dr;//dtMain.Rows[selectedIndex];

                if (SelectedRow == null)
                {
                    WWMessage.MessageShowWarning("操作数据出现异常。");
                    return "false";
                }

                //验证Lis数据库中是不是有这个申请单号的信息，如果有，则不再向LIS数据库中插入这条申请的信息 add 20150630 wjz
                string strSql = "select fapply_id, fstate, ftype_id, fhz_id, fhz_zyh,his_recordid from SAM_APPLY where 1=1 ";                
                           strSql += " and fapply_id='" + SelectedRow["fapply_id"].ToString() + "'";

                           DataTable dtFromLis = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.sLISConn,CommandType.Text, strSql).Tables[0];
                if (dtFromLis != null && dtFromLis.Rows.Count > 0)
                {
                    //return "false";
                    continue;
                }
                                
                ApplyModel applyModel = new ApplyModel();

                //申请单号：使用存储过程生成的申请单号
                applyModel.fapply_id = SelectedRow["fapply_id"].ToString();

                //判断分类是否为空！如果是空（没有分类的情况）则需要单独生成一个申请 2015-11-9 09:44:38 yufh 添加
                if (s检查分类 == "" || s检查分类 == null)
                    applyModel.His申请单号 = SelectedRow["fapply_id"].ToString();
                else
                    applyModel.His申请单号 = this.str申请单id;

                applyModel.fjytype_id = "";//暂时设成空值

                applyModel.fsample_type_id = "";//暂时设成空值

                applyModel.fjz_flag = 1; //急诊否

                applyModel.fcharge_flag = 0;//收费否
                applyModel.fstate = "2";//状态
                applyModel.fapply_user_id = SelectedRow["req_docno"].ToString();

                applyModel.fapply_dept_id = SelectedRow["fapply_dept_id"].ToString(); //申请部门

                applyModel.fapply_time = SelectedRow["fapply_time"].ToString(); //申请时间

                applyModel.ftype_id = SelectedRow["ftype_id"].ToString(); //病人类型

                applyModel.fhz_id = SelectedRow["fhz_id"].ToString(); //患者id

                applyModel.fhz_zyh = SelectedRow["fhz_zyh"].ToString(); //住院号

                applyModel.fsex = SelectedRow["fsex"].ToString(); //性别

                applyModel.fname = SelectedRow["fname"].ToString(); //姓名

                //add by wjz 20160122 添加身份证号 ▽
                applyModel.Sfzh = SelectedRow["sfzh"].ToString();
                //add by wjz 20160122 添加身份证号 △

                //changed by wjz 20160122 修改年龄的计算方式 ▽
                //applyModel.fage = Convert.ToInt32(SelectedRow["fage"]); //年龄
                //applyModel.fage_unit = "岁";

                try
                {
                    if (string.IsNullOrWhiteSpace(applyModel.Sfzh) || applyModel.Sfzh.Length != 18)
                    {
                        applyModel.fage = Convert.ToInt32(SelectedRow["fage"]); //年龄
                        applyModel.fage_unit = SelectedRow["fage_unit"].ToString();
                        if (string.IsNullOrWhiteSpace(applyModel.fage_unit))//如果没有取到年龄，则默认为“岁”
                        {
                            applyModel.fage_unit = "岁";
                        }
                    }
                    else
                    {
                        string birthday = applyModel.Sfzh.Substring(6, 4) + "-"
                                        + applyModel.Sfzh.Substring(10, 2) + "-"
                                        + applyModel.Sfzh.Substring(12, 2);

                        string ages = DAOWWF.BllPatientAge(birthday);
                        string[] arrage = ages.Split(':');

                        applyModel.fage = Convert.ToInt32(arrage[1]);
                        applyModel.fage_unit = arrage[0];
                    }
                }
                catch
                {
                    applyModel.fage = Convert.ToInt32(SelectedRow["fage"]); //年龄
                    applyModel.fage_unit = SelectedRow["fage_unit"].ToString();
                    if (string.IsNullOrWhiteSpace(applyModel.fage_unit))//如果没有取到年龄，则默认为“岁”
                    {
                        applyModel.fage_unit = "岁";
                    }
                }
                //changed by wjz 20160122 修改年龄的计算方式 △
                //岁	1
                //月	2
                //天	3

                applyModel.froom_num = "";

                applyModel.fbed_num = SelectedRow["fbed_num"].ToString();

                applyModel.fdiagnose = SelectedRow["fdiagnose"].ToString();
                applyModel.fcreate_user_id = WEISHENG.COMM.zdInfo.ModelUserInfo.用户编码.ToString();//LoginBLL.strPersonID;
                //applyModel.fcreate_time = strTime;
                applyModel.fupdate_user_id = WEISHENG.COMM.zdInfo.ModelUserInfo.用户编码.ToString();  //LoginBLL.strPersonID;
                //applyModel.fupdate_time = strTime;
                //
                applyModel.fxhdb = 0;
                applyModel.fxyld = 0;
                applyModel.fheat = 0;
                applyModel.fage_day = 0;
                applyModel.fage_month = 0;
                applyModel.fage_year = 0;
                applyModel.fjyf = 0;//检验费

                applyModel.F检验项目名称 = SelectedRow["fitem_group"].ToString();
                applyModel.fremark = SelectedRow["分类名称"].ToString();


                string strSave = this.bllApply.BllApplyAdd_New(applyModel);
                if (strSave == "true")
                {
                    执行状态 = strSave;
                }
            }
            return 执行状态;
        }
        
        private void textBox门诊住院号_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == (char)Keys.Enter)
            {
                WWGetApplyList();
            }
        }


        //Print(bool 是否预览, string s姓名, string s性别, string s年龄, string s类型, string s检查, string s申请单号, string s申请医生)
        private void AppendReport(ref TableXReport report, string s病人信息, string s检查, string s申请单号, string s住院号, string s申请医生)
        {
            TableXReport rep = GetSubReport(s病人信息, s检查, s申请单号, s住院号, s申请医生);

            report.Pages.AddRange(rep.Pages);
        }

        private TableXReport GetSubReport(string s病人信息, string s检查, string s申请单号, string s住院号, string s申请医生)
        {
            Font ft = new System.Drawing.Font("宋体", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            Font ft7 = new System.Drawing.Font("宋体", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            int size = 20;

            TableXReport xtr = new TableXReport();

            if (s检查.Length < 2 * size)
            {
                SetFirstReport(ref xtr, ft, ft7, size, s病人信息, s检查, s申请单号, s住院号, s申请医生);
            }
            else
            {
                List<string> list检验 = new List<string>();

                SplitItems(ref list检验, s检查, size);

                SetFirstReport(ref xtr, ft, ft7, size, s病人信息, list检验[0], s申请单号, s住院号, s申请医生);

                for (int index = 1; index < list检验.Count; index++)
                {
                    CombineReportsNew(xtr, ft, ft7, size, s病人信息, list检验[index], s申请单号, s住院号, s申请医生);
                }
            }
            return xtr;
        }

        private void CombineReportsNew(TableXReport firstRep, Font ft9, Font ft7, int colMaxSize,
                                    string s病人信息, string sub检查, string s申请单号, string s住院号, string s申请医生)
        {
            TableXReport subRep = new TableXReport();
            
            SetFirstReport(ref subRep, ft9, ft7, colMaxSize, s病人信息, sub检查, s申请单号, s住院号, s申请医生);

            firstRep.Pages.AddRange(subRep.Pages);
        }

        private void SplitItems(ref List<string> list检验, string s检查, int colMaxSize)
        {
            string[] arr检查 = s检查.Split(',');

            list检验.Add(arr检查[0]);
            for (int index = 1; index < arr检查.Length; index++)
            {
                if (list检验[list检验.Count - 1].Length + arr检查[index].Length < colMaxSize * 2)
                {
                    list检验[list检验.Count - 1] = list检验[list检验.Count - 1] + "," + arr检查[index];
                }
                else
                {
                    list检验.Add(arr检查[index]);
                }
            }
        }

        private void SetFirstReport(ref TableXReport xtr, Font ft, Font ft7, int colMaxSize, string s病人信息, string s检查, string s申请单号, string s住院号, string s申请医生)
        {           

            xtr.SetReportTitle("打印时间" + System.DateTime.Now.ToString() + " " + s申请医生, true, ft, TextAlignment.TopLeft, Size.Empty);
            xtr.SetReportTitle(s病人信息, true, ft, TextAlignment.TopLeft, Size.Empty);


            if (s检查.Length > colMaxSize)
            {
                xtr.SetReportTitle(s检查.Substring(0, colMaxSize), true, ft7, TextAlignment.TopLeft, Size.Empty);
                xtr.SetReportTitle(s检查.Substring(colMaxSize), true, ft7, TextAlignment.TopLeft, Size.Empty);
            }
            else
            {
                xtr.SetReportTitle(s检查, true, ft7, TextAlignment.TopLeft, Size.Empty);
            }

            xtr.SetReportBarCode(s申请单号, new Point(5, 150), new Size(400, 125), null, true);
            xtr.SetReportMain(0, 0, 1, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(10, 10, 10, 10), System.Drawing.Printing.PaperKind.Custom, new Size(500, 315), 0, 0, "");

            xtr.CreateDocument();//如果不加这一行，report可能不能显示
        }

        private string SaveHisApplyNew(string strID, string str条码)
        {
            DataRow[] drs = dtMain.Select("fapply_id='" + strID + "'");

            //循环保存单据
            string 执行状态 = "false";

            DataRow SelectedRow;
            if (drs.Length > 0)
            {
                SelectedRow = drs[0];
            }
            else
            {
                return 执行状态;
            }

            //验证Lis数据库中是不是有这个申请单号的信息，如果有，则不再向LIS数据库中插入这条申请的信息 add 20150630 wjz
            DataTable dtFromLis = this.bllApply.BllSamplingMainDTFromLis("fapply_id='" + SelectedRow["fapply_id"].ToString() + "'");
            if (dtFromLis != null && dtFromLis.Rows.Count > 0)
            {
                return "false";
            }

            //string strTime = this.bllApply.DbServerDateTim();
            ApplyModel applyModel = new ApplyModel();

            //申请单号：使用存储过程生成的申请单号
            applyModel.fapply_id = SelectedRow["fapply_id"].ToString();

            //判断分类是否为空！如果是空（没有分类的情况）则需要单独生成一个申请 2015-11-9 09:44:38 yufh 添加
            //if (s检查分类 == "" || s检查分类 == null)
            //    applyModel.His申请单号 = SelectedRow["fapply_id"].ToString();
            //else
            //    applyModel.His申请单号 = this.str申请单id;
            applyModel.His申请单号 = str条码;

            applyModel.fjytype_id = "";//暂时设成空值

            applyModel.fsample_type_id = "";//暂时设成空值

            applyModel.fjz_flag = 1; //急诊否

            applyModel.fcharge_flag = 0;//收费否
            applyModel.fstate = "2";//状态
            applyModel.fapply_user_id = SelectedRow["req_docno"].ToString();

            applyModel.fapply_dept_id = SelectedRow["fapply_dept_id"].ToString(); //申请部门

            applyModel.fapply_time = SelectedRow["fapply_time"].ToString(); //申请时间

            applyModel.ftype_id = SelectedRow["ftype_id"].ToString(); //病人类型

            applyModel.fhz_id = SelectedRow["fhz_id"].ToString(); //患者id

            applyModel.fhz_zyh = SelectedRow["fhz_zyh"].ToString(); //住院号

            applyModel.fsex = SelectedRow["fsex"].ToString(); //性别

            applyModel.fname = SelectedRow["fname"].ToString(); //姓名

            //add by wjz 20160122 添加身份证号 ▽
            applyModel.Sfzh = SelectedRow["sfzh"].ToString();
            //add by wjz 20160122 添加身份证号 △

            //changed by wjz 20160122 修改年龄的计算方式 ▽
            //applyModel.fage = Convert.ToInt32(SelectedRow["fage"]); //年龄
            //applyModel.fage_unit = "岁";
            try
            {
                if (string.IsNullOrWhiteSpace(applyModel.Sfzh) || applyModel.Sfzh.Length != 18)
                {
                    applyModel.fage = Convert.ToInt32(SelectedRow["fage"]); //年龄
                    applyModel.fage_unit = SelectedRow["fage_unit"].ToString();
                    if (string.IsNullOrWhiteSpace(applyModel.fage_unit))//如果没有取到年龄，则默认为“岁”
                    {
                        applyModel.fage_unit = "岁";
                    }
                }
                else
                {
                    string birthday = applyModel.Sfzh.Substring(6, 4) + "-"
                                    + applyModel.Sfzh.Substring(10, 2) + "-"
                                    + applyModel.Sfzh.Substring(12, 2);

                    string ages = DAOWWF.BllPatientAge(birthday);
                    string[] arrage = ages.Split(':');

                    applyModel.fage = Convert.ToInt32(arrage[1]);
                    applyModel.fage_unit = arrage[0];
                }
            }
            catch
            {
                applyModel.fage = Convert.ToInt32(SelectedRow["fage"]); //年龄
                applyModel.fage_unit = SelectedRow["fage_unit"].ToString();
                if (string.IsNullOrWhiteSpace(applyModel.fage_unit))//如果没有取到年龄，则默认为“岁”
                {
                    applyModel.fage_unit = "岁";
                }
            }
            //changed by wjz 20160122 修改年龄的计算方式 △
            //岁	1
            //月	2
            //天	3

            applyModel.froom_num = "";
            applyModel.fbed_num = SelectedRow["fbed_num"].ToString();

            applyModel.fdiagnose = SelectedRow["fdiagnose"].ToString();
            applyModel.fcreate_user_id = WEISHENG.COMM.zdInfo.ModelUserInfo.用户编码.ToString();//LoginBLL.strPersonID;
            //applyModel.fcreate_time = strTime;
            applyModel.fupdate_user_id = WEISHENG.COMM.zdInfo.ModelUserInfo.用户编码.ToString();//LoginBLL.strPersonID;
            //applyModel.fupdate_time = strTime;
            //
            applyModel.fxhdb = 0;
            applyModel.fxyld = 0;
            applyModel.fheat = 0;
            applyModel.fage_day = 0;
            applyModel.fage_month = 0;
            applyModel.fage_year = 0;
            applyModel.fjyf = 0;//检验费

            applyModel.F检验项目名称 = SelectedRow["fitem_group"].ToString();
            applyModel.fremark = SelectedRow["分类名称"].ToString();

            string strSave = this.bllApply.BllApplyAdd_New(applyModel);
            if (strSave == "true")
            {
                执行状态 = strSave;
            }

            return 执行状态;
        }

    }
}