﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;

namespace ATOMEHR_LeaveClient
{
    class Reg
    {
        /// <summary>
        /// 获取CPU编号
        /// </summary>
        /// <returns></returns>
        public string GetCPUID()
        {
            try
            {
                ManagementClass mc = new ManagementClass("win32_Processor");
                ManagementObjectCollection moc = mc.GetInstances();
                string strCPUID = string.Empty;

                foreach (ManagementObject item in moc)
                {

                    strCPUID = item.Properties["Processorid"].Value as string;
                    break;
                }

                return strCPUID;
            }
            catch (Exception)
            {
                return "";
            }
        }

        public string GetHardDiskID()
        {
            string HDid = "";
            try
            {
                ManagementClass mc = new ManagementClass("Win32_DiskDrive");
                ManagementObjectCollection moc = mc.GetInstances();

                foreach (ManagementObject item in moc)
                {
                    HDid = (string)item.Properties["Model"].Value;
                }

                return HDid;
            }
            catch (Exception)
            {
                return "";
            }
        }
        /// <summary>
        /// 获取硬盘的序列号
        /// </summary>
        /// <returns></returns>
        public string GetDiskVolumeSerialNumber()
        {
            ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObject disk = new ManagementObject("win32_logicaldisk.deviceid=\"d:\"");
            disk.Get();
            return disk.GetPropertyValue("VolumeSerialNumber").ToString();
        }
        public string getMD5(string md5)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider md = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] value, hash;

            value = System.Text.Encoding.UTF8.GetBytes(md5);
            hash = md.ComputeHash(value);
            md.Clear();
            string temp = "";
            for (int i = 0; i < hash.Length; i++)
            {
                temp += hash[i].ToString("x").PadLeft(2, '0');
            }

            return temp;
        }
    }
}
