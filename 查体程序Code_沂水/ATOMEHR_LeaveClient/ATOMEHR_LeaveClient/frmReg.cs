﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ATOMEHR_LeaveClient
{
    public partial class frmRegSoft : Form
    {
        string _cpuid;

        public string Cpuid
        {
            get { return _cpuid; }
            set { _cpuid = value; }
        }
        public frmRegSoft()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Reg reg = new Reg();
            //this.textEdit2.Text = reg.GetCPUID();
            string sysFolder = System.Environment.CurrentDirectory;
            if (txtRegCode.Text.Trim() != reg.getMD5(_cpuid))
            {
                MessageBox.Show("注册码失误！");
                this.DialogResult = System.Windows.Forms.DialogResult.Retry;
            }
            else
            {
                MessageBox.Show("注册成功！");
                File.WriteAllText(sysFolder + "\\sixi.ini", reg.getMD5(_cpuid));
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void frmReg_Load(object sender, EventArgs e)
        {
            this.txtMachCode.Text = _cpuid;
        }

        private void btnRegSoft_Click(object sender, EventArgs e)
        {
            Reg reg = new Reg();
            //this.textEdit2.Text = reg.GetCPUID();
            string sysFolder = System.Environment.CurrentDirectory;
            if (txtRegCode.Text.Trim() != reg.getMD5(_cpuid))
            {
                MessageBox.Show("注册码失误！");
                this.DialogResult = System.Windows.Forms.DialogResult.Retry;
            }
            else
            {
                MessageBox.Show("恭喜您，注册成功！");
                File.WriteAllText(sysFolder + "\\atomclient.ini", reg.getMD5(_cpuid));
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            Reg reg = new Reg();
            this.txtMachCode.Text = reg.GetCPUID();
        }
    }
}
