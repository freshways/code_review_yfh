﻿using ATOMEHR_LeaveClient.SqlSugar_Models;
using ATOMEHR_LeaveClient.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATOMEHR_LeaveClient.SqlSugar_DALs
{
    class dal_B超报告模版_Lite
    {
        SqlSugarConn conn = new SqlSugarConn(DBConn.connSQLite);

        public List<tb_B超报告模版> Get检查部位()
        {
            string sql = "select 检查部位 from tb_B超报告模版 group by 检查部位";
            return conn.db.Ado.SqlQuery<tb_B超报告模版>(sql);
        }

        public int SetB超报告模版(List<tb_B超报告模版> ls)
        {
            //Begin WXF 2019-01-14 | 18:53
            //SqlSugar插入sqlite的时候如果单次数据量过大会造成out of memory(内存溢出),故改为分批插入 
            int iCount = 0;
            List<tb_B超报告模版> lsTemp = new List<tb_B超报告模版>();

            while (ls.Count > 0)
            {
                lsTemp = ls.Take<tb_B超报告模版>(5000).ToList<tb_B超报告模版>();
                ls.RemoveRange(0, lsTemp.Count);
                iCount += conn.db.Insertable<tb_B超报告模版>(lsTemp).ExecuteCommand();
                lsTemp.Clear();
            }

            return iCount;
            //End
        }

        public bool Delete(string sqlWhere)
        {
            bool rst = false;
            string sqlStr = "DELETE FROM tb_B超报告模版 WHERE 1=1 ";
            if (!string.IsNullOrEmpty(sqlWhere))
            {
                sqlStr += sqlWhere;
            }
            int rows = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }

        public List<tb_B超报告模版> GetB超报告模版(string str部位)
        {
            string sql = "select * from tb_B超报告模版 where 检查部位=@检查部位";
            return conn.db.Ado.SqlQuery<tb_B超报告模版>(sql, new { 检查部位 = str部位 });
        }
    }
}
