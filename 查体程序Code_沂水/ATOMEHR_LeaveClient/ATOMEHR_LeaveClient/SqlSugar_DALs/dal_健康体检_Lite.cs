﻿using ATOMEHR_LeaveClient.SqlSugar_Models;
using ATOMEHR_LeaveClient.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATOMEHR_LeaveClient.SqlSugar_DALs
{
    class dal_健康体检_Lite
    {
        SqlSugarConn conn = new SqlSugarConn(DBConn.connSQLite);

        /// <summary>
        /// 从sqlite获取某个档案号当天的体检数据(正常是一条)
        /// 没有返回空的实体对象
        /// </summary>
        /// <param name="recordNum"></param>
        /// <returns></returns>
        public tb_健康体检_Lite Get_健康体检Today(string recordNum)
        {
            string DateNow = DateTime.Now.ToString("yyyy-MM-dd");
            tb_健康体检_Lite result = conn.db.Queryable<tb_健康体检_Lite>().Where(p => p.个人档案编号.Equals(recordNum) && p.体检日期.Equals(DateNow)).First();

            if (result == null)
            {
                return new tb_健康体检_Lite();
            }
            else
            {
                return result;
            }
        }

    }
}
