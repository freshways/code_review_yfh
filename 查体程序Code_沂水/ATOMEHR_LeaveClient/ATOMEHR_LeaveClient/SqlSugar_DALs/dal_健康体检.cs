﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATOMEHR_LeaveClient.SqlSugar_Models;
using ATOMEHR_LeaveClient.Util;

namespace ATOMEHR_LeaveClient.SqlSugar_DALs
{
    class dal_健康体检
    {
        SqlSugarConn conn = new SqlSugarConn(DBConn.connGW);

        /// <summary>
        /// 获取参照数据
        /// </summary>
        /// <returns></returns>
        public List<tb_健康体检_Lite> Get参照数据(string rgid)
        {
            string sql = @"select a.* from 
                          tb_健康体检 a left join tb_健康档案 c on a.个人档案编号 = c.个人档案编号 where 1=1
                          and a.体检日期 like ''+CONVERT(varchar(4),DATEADD(YEAR,-1,GETDATE()),120)+'%'
                          and c.档案状态='1'
                          and ISNULL(c.身份证号,'')!=''
                          --and c.出生日期>=CONVERT(varchar(4),DATEADD(YEAR,-65,GETDATE()),120)
                          and c.所属机构 in(select 机构编号 from tb_机构信息 where 机构编号=@rgid or 上级机构=@rgid)
                          and EXISTS(select 1 from tb_健康体检 b
                          group by b.个人档案编号 having a.个人档案编号=b.个人档案编号 and a.体检日期=MAX(b.体检日期))";

            return conn.db.Ado.SqlQuery<tb_健康体检_Lite>(sql, new { rgid = rgid });
        }

        public string Get医师签字(string recordNum,string orgID,string dateTJ)
        {
            var result = conn.db.Queryable<tb_健康体检>().Where(p => p.个人档案编号 == recordNum && p.所属机构 == orgID && p.体检日期 == dateTJ).First();

            if (result == null)
            {
                return "";
            }
            else
            {
                return result.医师签字;
            }
        }
    }
}
