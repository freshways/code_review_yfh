﻿using ATOMEHR_LeaveClient.SqlSugar_Models;
using ATOMEHR_LeaveClient.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATOMEHR_LeaveClient.SqlSugar_DALs
{
    class dal_健康体检_B超
    {
        SqlSugarConn conn = new SqlSugarConn(DBConn.connGW);

        public int InsertB超报告(tb_健康体检_B超 tb_B超)
        {
            int count = conn.db.Insertable(tb_B超).InsertColumns(p => new { p.个人档案编号, p.超声所见, p.超声提示, p.备注, p.所属机构, p.检查日期, p.创建日期, p.创建人 }).ExecuteReturnIdentity();

            if (count > 0) { return 1; } else { return 0; }
        }

        public int UpdateB超报告(tb_健康体检_B超 tb_B超)
        {
            int count = conn.db.Updateable(tb_B超).UpdateColumns(p => new { p.个人档案编号, p.超声所见, p.超声提示, p.备注, p.所属机构, p.检查日期, p.创建日期, p.创建人 }).Where(p => p.个人档案编号 == tb_B超.个人档案编号 && p.创建日期 == tb_B超.创建日期).ExecuteCommand();

            if (count > 0) { return 1; } else { return 0; }
        }

        public bool isExistB超报告(tb_健康体检_B超 tb_B超)
        {
            return conn.db.Queryable<tb_健康体检_B超>().Any(p => p.个人档案编号 == tb_B超.个人档案编号 && p.创建日期 == tb_B超.创建日期);
        }
    }
}
