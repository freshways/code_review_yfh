﻿using ATOMEHR_LeaveClient.SqlSugar_Models;
using ATOMEHR_LeaveClient.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATOMEHR_LeaveClient.SqlSugar_DALs
{
    class dal_健康体检_B超_Lite
    {
        SqlSugarConn conn = new SqlSugarConn(DBConn.connSQLite);

        public List<tb_健康体检_B超_Lite> Get未上传体检B超()
        {
            string sql = " select * from tb_健康体检_B超 where RowState ISNULL or RowState='' ";
            return conn.db.Ado.SqlQuery<tb_健康体检_B超_Lite>(sql);
        }

        public int Set体检B超上传状态(List<tb_健康体检_B超_Lite> ls)
        {
            return conn.db.Updateable(ls).UpdateColumns(p => new { p.RowState }).ExecuteCommand();
        }
    }
}