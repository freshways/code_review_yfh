﻿using ATOMEHR_LeaveClient.SqlSugar_Models;
using ATOMEHR_LeaveClient.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATOMEHR_LeaveClient.SqlSugar_DALs
{
    class dal_老年人生活自理能力评估_Reference
    {
        SqlSugarConn conn = new SqlSugarConn(DBConn.connSQLite);

        /// <summary>
        /// 从sqlserver老年人生活自理能力评估
        /// </summary>
        /// <param name="rgid"></param>
        /// <returns></returns>
        public List<tb_老年人生活自理能力评估_Reference> Get自理评估FromSS(string rgid)
        {
            SqlSugarConn connSS = new SqlSugarConn(DBConn.connGW);

            string sql = @"select a.[个人档案编号]
                                 ,a.[进餐评分]
                                 ,a.[梳洗评分]
                                 ,a.[穿衣评分]
                                 ,a.[如厕评分]
                                 ,a.[活动评分]
                                 ,a.[总评分]
                                 ,a.[随访次数]
                                 ,a.[下次随访目标]
                                 ,a.[下次随访日期]
                                 ,a.[随访医生]
                                 ,a.[创建机构]
                                 ,a.[创建人]
                                 ,a.[更新人]
                                 ,a.[所属机构]
                                 ,a.[创建时间]
                                 ,a.[更新时间]
                                 ,a.[随访日期]
                                 from 
                                 tb_老年人随访 a left join tb_健康档案 c on a.个人档案编号 = c.个人档案编号 where 1=1
                                 and a.随访日期 like ''+CONVERT(varchar(4),DATEADD(YEAR,-1,GETDATE()),120)+'%'
                                 and c.档案状态='1'
                                 and ISNULL(c.身份证号,'')!=''
                                 and c.所属机构 in(select 机构编号 from tb_机构信息 where 机构编号=@rgid or 上级机构=@rgid)
                                 and EXISTS(select 1 from tb_老年人随访 b
                                 group by b.个人档案编号 having a.个人档案编号=b.个人档案编号 and a.随访日期=MAX(b.随访日期))";

            return connSS.db.Ado.SqlQuery<tb_老年人生活自理能力评估_Reference>(sql, new { rgid = rgid });
        }

        /// <summary>
        /// 写入参照数据
        /// </summary>
        /// <param name="ls"></param>
        /// <returns></returns>
        public int Set参照数据(List<tb_老年人生活自理能力评估_Reference> ls)
        {
            //Begin WXF 2019-01-14 | 18:53
            //SqlSugar插入sqlite的时候如果单次数据量过大会造成out of memory(内存溢出),故改为分批插入 
            int iCount = 0;
            List<tb_老年人生活自理能力评估_Reference> lsTemp = new List<tb_老年人生活自理能力评估_Reference>();

            while (ls.Count > 0)
            {
                lsTemp = ls.Take<tb_老年人生活自理能力评估_Reference>(5000).ToList<tb_老年人生活自理能力评估_Reference>();
                ls.RemoveRange(0, lsTemp.Count);
                iCount += conn.db.Insertable<tb_老年人生活自理能力评估_Reference>(lsTemp).ExecuteCommand();
                lsTemp.Clear();
            }

            return iCount;
            //End
        }

        /// <summary>
        /// 获取表总行数
        /// </summary>
        /// <returns></returns>
        public int GetTableRows()
        {
            return conn.db.Queryable<tb_老年人生活自理能力评估_Reference>().Count();
        }

        /// <summary>
        /// 获取表是否为空
        /// </summary>
        /// <returns></returns>
        public bool GetTableIsNull()
        {
            if (GetTableRows() > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 清空表
        /// </summary>
        /// <returns></returns>
        public int DeleteAll()
        {
            return conn.db.Deleteable<tb_老年人生活自理能力评估_Reference>().ExecuteCommand();
        }

        /// <summary>
        /// 根据档案号获取一条参照数据
        /// </summary>
        /// <param name="recordNum"></param>
        /// <returns></returns>
        public tb_老年人生活自理能力评估_Reference GetOneRow(string recordNum)
        {
            return conn.db.Queryable<tb_老年人生活自理能力评估_Reference>().Where(p => p.个人档案编号.Equals(recordNum)).First();
        }
    }
}
