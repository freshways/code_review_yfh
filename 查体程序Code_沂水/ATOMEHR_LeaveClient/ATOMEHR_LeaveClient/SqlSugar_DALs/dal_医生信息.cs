﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATOMEHR_LeaveClient.Util;
using ATOMEHR_LeaveClient.SqlSugar_Models;
using System.Data;

namespace ATOMEHR_LeaveClient.SqlSugar_DALs
{
    class dal_医生信息
    {
        SqlSugarConn conn = new SqlSugarConn(DBConn.connSQLite);

        /// <summary>
        /// 从sqlserver获取医生信息
        /// 此表有image字段,为了节约性能不再新建实体,与sqlite共用一个dal
        /// </summary>
        /// <param name="rgid"></param>
        /// <returns></returns>
        public List<tb_医生信息_Lite> Get医生信息FromSS(string rgid)
        {
            SqlSugarConn connSS = new SqlSugarConn(DBConn.connGW);

            string sql = @"SELECT [ID]
                          ,[b编码]
                          ,[x医生姓名]
                          ,[x性别]
                          ,[c出生日期]
                          ,[z职务]
                          ,[k科室]
                          ,[s身份证号]
                          ,[c查体模块]
                          ,[s是否停用]
                          ,[s所属机构]
                           FROM [dbo].[tb_医生信息]
                           WHERE s所属机构 in(select 机构编号 from tb_机构信息 where 机构编号=@rgid or 上级机构=@rgid)";

            return connSS.db.Ado.SqlQuery<tb_医生信息_Lite>(sql, new { rgid = rgid });
        }

        /// <summary>
        /// 医生信息存入sqlite
        /// </summary>
        /// <param name="ls"></param>
        /// <returns></returns>
        public int Set医生信息ToLite(List<tb_医生信息_Lite> ls)
        {
            return conn.db.Insertable<tb_医生信息_Lite>(ls).ExecuteCommand();
        }

        /// <summary>
        /// 从sqlite获取医生信息返回List<>
        /// </summary>
        /// <returns></returns>
        public List<tb_医生信息_Lite> Get医生信息FromLite_List()
        {
            return conn.db.Queryable<tb_医生信息_Lite>().ToList();
        }

        /// <summary>
        /// 从sqlite获取医生信息返回DataTable
        /// </summary>
        /// <returns></returns>
        public DataTable Get医生信息FromLite_DT()
        {
            return conn.db.Queryable<tb_医生信息_Lite>().ToDataTable();
        }
        

        public DataTable Get医生信息ToLookUp()
        {
            DataTable dt = new DataTable();

            dt = Get医生信息FromLite_DT().DefaultView.ToTable(false, new string[] { "b编码", "x医生姓名" });

            DataRow dr = dt.Rows.Add();
            dr["b编码"] = "请选择";
            dr["x医生姓名"] = "请选择";

            dt.DefaultView.Sort = "b编码 DESC";
            dt = dt.DefaultView.ToTable();

            return dt;
        }

        /// <summary>
        /// 获取表总行数
        /// </summary>
        /// <returns></returns>
        public int GetTableRows()
        {
            return conn.db.Queryable<tb_医生信息_Lite>().Count();
        }

        /// <summary>
        /// 获取表是否为空
        /// </summary>
        /// <returns></returns>
        public bool GetTableIsNull()
        {
            if (GetTableRows() > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 清空表
        /// </summary>
        /// <returns></returns>
        public int DeleteAll()
        {
            return conn.db.Deleteable<tb_医生信息_Lite>().ExecuteCommand();
        }
    }
}
