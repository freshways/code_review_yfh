﻿using ATOMEHR_LeaveClient.SqlSugar_Models;
using ATOMEHR_LeaveClient.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATOMEHR_LeaveClient.SqlSugar_DALs
{
    class dal_健康体检_用药情况_Reference
    {
        SqlSugarConn conn = new SqlSugarConn(DBConn.connSQLite);
        SqlSugarConn connSS = new SqlSugarConn(DBConn.connGW);

        #region 获取一个机构体检表,高血压随访,糖尿病随访,冠心病随访,脑卒中随访,精神病随访的最近一次随访的用药情况
        public List<tb_健康体检_用药情况_Reference> Get体检表用药(string rgid)
        {
            string sql = @"select d.个人档案编号,d.药物名称,d.用法,d.用量,d.用药时间,d.服药依从性,d.创建时间,'tb_健康体检_用药情况表' as 数据来源 from
                           tb_健康体检 a 
                           left join tb_健康档案 c on a.个人档案编号=c.个人档案编号 
                           left join tb_健康体检_用药情况表 d on a.个人档案编号=d.个人档案编号 and a.创建时间=d.创建时间
                           where 1=1
                           and d.ID is not null
                           and c.档案状态='1'
                           and ISNULL(c.身份证号,'')!=''
                           and c.所属机构 in(select 机构编号 from tb_机构信息 where 机构编号=@rgid or 上级机构=@rgid)
                           and EXISTS(select 1 from tb_健康体检 b
                           group by b.个人档案编号 having a.个人档案编号=b.个人档案编号 and a.体检日期=MAX(b.体检日期))";

            return connSS.db.Ado.SqlQuery<tb_健康体检_用药情况_Reference>(sql, new { rgid = rgid });
        }

        public List<tb_健康体检_用药情况_Reference> Get高血压随访用药(string rgid)
        {
            string sql = @"select d.个人档案编号,d.药物名称,d.用法,'' as 用量,'' as 用药时间,'' as 服药依从性,d.创建时间,'tb_MXB高血压随访表_用药情况' as 数据来源 from 
                           tb_MXB高血压随访表 a 
                           left join tb_健康档案 c on a.个人档案编号=c.个人档案编号 
                           left join tb_MXB高血压随访表_用药情况 d on a.个人档案编号=d.个人档案编号 and a.创建时间=d.创建时间
                           where 1=1
                           and d.Y_ID is not null
                           and c.档案状态='1'
                           and ISNULL(c.身份证号,'')!=''
                           and c.所属机构 in(select 机构编号 from tb_机构信息 where 机构编号=@rgid or 上级机构=@rgid)
                           and EXISTS(select 1 from tb_MXB高血压随访表 b
                           group by b.个人档案编号 having a.个人档案编号=b.个人档案编号 and a.发生时间=MAX(b.发生时间))";

            return connSS.db.Ado.SqlQuery<tb_健康体检_用药情况_Reference>(sql, new { rgid = rgid });
        }

        public List<tb_健康体检_用药情况_Reference> Get糖尿病随访用药(string rgid)
        {
            string sql = @"select d.个人档案编号,d.药物名称,d.用法,'' as 用量,'' as 用药时间,'' as 服药依从性,d.创建时间,'tb_MXB糖尿病随访表_用药情况' as 数据来源 from 
                           tb_MXB糖尿病随访表 a 
                           left join tb_健康档案 c on a.个人档案编号=c.个人档案编号 
                           left join tb_MXB糖尿病随访表_用药情况 d on a.个人档案编号=d.个人档案编号 and a.创建时间=d.创建时间
                           where 1=1
                           and d.Y_ID is not null
                           and c.档案状态='1'
                           and ISNULL(c.身份证号,'')!=''
                           and c.所属机构 in(select 机构编号 from tb_机构信息 where 机构编号=@rgid or 上级机构=@rgid)
                           and EXISTS(select 1 from tb_MXB糖尿病随访表 b
                           group by b.个人档案编号 having a.个人档案编号=b.个人档案编号 and a.发生时间=MAX(b.发生时间))";

            return connSS.db.Ado.SqlQuery<tb_健康体检_用药情况_Reference>(sql, new { rgid = rgid });
        }

        public List<tb_健康体检_用药情况_Reference> Get冠心病随访用药(string rgid)
        {
            string sql = @"select d.个人档案编号,d.药物名称,d.用法,'' as 用量,'' as 用药时间,'' as 服药依从性,d.创建时间,'tb_MXB冠心病随访表_用药情况' as 数据来源 from 
                           tb_MXB冠心病随访表 a 
                           left join tb_健康档案 c on a.个人档案编号=c.个人档案编号 
                           left join tb_MXB冠心病随访表_用药情况 d on a.个人档案编号=d.个人档案编号 and a.创建时间=d.创建时间
                           where 1=1
                           and d.Y_ID is not null
                           and c.档案状态='1'
                           and ISNULL(c.身份证号,'')!=''
                           and c.所属机构 in(select 机构编号 from tb_机构信息 where 机构编号=@rgid or 上级机构=@rgid)
                           and EXISTS(select 1 from tb_MXB冠心病随访表 b
                           group by b.个人档案编号 having a.个人档案编号=b.个人档案编号 and a.发生时间=MAX(b.发生时间))";

            return connSS.db.Ado.SqlQuery<tb_健康体检_用药情况_Reference>(sql, new { rgid = rgid });
        }

        public List<tb_健康体检_用药情况_Reference> Get脑卒中随访用药(string rgid)
        {
            string sql = @"select d.个人档案编号,d.药物名称,d.用法,'' as 用量,'' as 用药时间,'' as 服药依从性,d.创建时间,'tb_MXB脑卒中随访表_用药情况' as 数据来源 from 
                           tb_MXB脑卒中随访表 a 
                           left join tb_健康档案 c on a.个人档案编号=c.个人档案编号 
                           left join tb_MXB脑卒中随访表_用药情况 d on a.个人档案编号=d.个人档案编号 and a.创建时间=d.创建时间
                           where 1=1
                           and d.Y_ID is not null
                           and c.档案状态='1'
                           and ISNULL(c.身份证号,'')!=''
                           and c.所属机构 in(select 机构编号 from tb_机构信息 where 机构编号=@rgid or 上级机构=@rgid)
                           and EXISTS(select 1 from tb_MXB脑卒中随访表 b
                           group by b.个人档案编号 having a.个人档案编号=b.个人档案编号 and a.发生时间=MAX(b.发生时间))";

            return connSS.db.Ado.SqlQuery<tb_健康体检_用药情况_Reference>(sql, new { rgid = rgid });
        }

        public List<tb_健康体检_用药情况_Reference> Get重症精神疾病随访用药(string rgid)
        {
            string sql = @"select a.个人档案编号,d.药物名称,d.用法,d.剂量 as 用量,'' as 用药时间,'' as 服药依从性,a.创建时间,'tb_精神病_用药情况' as 数据来源 from 
                           tb_精神病随访记录 a
                           left join tb_健康档案 c on a.个人档案编号=c.个人档案编号 
                           left join tb_精神病_用药情况 d on a.ID=d.C_JRID
                           where 1=1
                           and d.ID is not null
                           and c.档案状态='1'
                           and ISNULL(c.身份证号,'')!=''
                           and c.所属机构 in(select 机构编号 from tb_机构信息 where 机构编号=@rgid or 上级机构=@rgid)
                           and EXISTS(select 1 from tb_精神病随访记录 b
                           group by b.个人档案编号 having a.个人档案编号=b.个人档案编号 and a.随访日期=MAX(b.随访日期))";

            return connSS.db.Ado.SqlQuery<tb_健康体检_用药情况_Reference>(sql, new { rgid = rgid });
        }
        #endregion

        /// <summary>
        /// 写入参照数据
        /// </summary>
        /// <param name="ls"></param>
        /// <returns></returns>
        public int Set参照数据(List<tb_健康体检_用药情况_Reference> ls)
        {
            //Begin WXF 2019-01-14 | 18:53
            //SqlSugar插入sqlite的时候如果单次数据量过大会造成out of memory(内存溢出),故改为分批插入 
            int iCount = 0;
            List<tb_健康体检_用药情况_Reference> lsTemp = new List<tb_健康体检_用药情况_Reference>();

            while (ls.Count > 0)
            {
                lsTemp = ls.Take<tb_健康体检_用药情况_Reference>(5000).ToList<tb_健康体检_用药情况_Reference>();
                ls.RemoveRange(0, lsTemp.Count);
                iCount += conn.db.Insertable<tb_健康体检_用药情况_Reference>(lsTemp).ExecuteCommand();
                lsTemp.Clear();
            }

            return iCount;
            //End
        }

        /// <summary>
        /// 获取表总行数
        /// </summary>
        /// <returns></returns>
        public int GetTableRows()
        {
            return conn.db.Queryable<tb_健康体检_用药情况_Reference>().Count();
        }

        /// <summary>
        /// 获取表是否为空
        /// </summary>
        /// <returns></returns>
        public bool GetTableIsNull()
        {
            if (GetTableRows() > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 清空表
        /// </summary>
        /// <returns></returns>
        public int DeleteAll()
        {
            return conn.db.Deleteable<tb_健康体检_用药情况_Reference>().ExecuteCommand();
        }

        /// <summary>
        /// 获取某个人的体检用药参照集
        /// </summary>
        /// <param name="recordNum"></param>
        /// <returns></returns>
        public List<tb_健康体检_用药情况_Reference> Get体检用药参照(string recordNum)
        {
            return conn.db.Queryable<tb_健康体检_用药情况_Reference>().Where(p => p.个人档案编号 == recordNum && p.数据来源 == "tb_健康体检_用药情况表").ToList();
        }

        /// <summary>
        /// 获取某个人的其他用药参照集
        /// </summary>
        /// <param name="recordNum"></param>
        /// <returns></returns>
        public List<tb_健康体检_用药情况_Reference> Get其他用药参照(string recordNum)
        {
            return conn.db.Queryable<tb_健康体检_用药情况_Reference>().Where(p => p.个人档案编号 == recordNum && p.数据来源 != "tb_健康体检_用药情况表").ToList();
        }
    }
}
