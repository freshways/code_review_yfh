﻿using ATOMEHR_LeaveClient.SqlSugar_Models;
using ATOMEHR_LeaveClient.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATOMEHR_LeaveClient.SqlSugar_DALs
{
    class dal_B超报告模版
    {
        SqlSugarConn conn = new SqlSugarConn(DBConn.connGW);

        public List<tb_B超报告模版> Get_B超报告模版(string rgid)
        {
            //Begin WXF 2019-04-22 | 11:06
            //沂水的所属机构无效 
            //string sql = "select 检查部位,超声提示,超声所见,备注 from tb_B超报告模板 where 所属机构=@rgid";
            //return conn.db.Ado.SqlQuery<tb_B超报告模版>(sql, new { rgid = rgid });
            //End

            string sql = "select 检查部位,超声提示,超声所见,备注 from tb_B超报告模板";
            return conn.db.Ado.SqlQuery<tb_B超报告模版>(sql);
        }
    }
}
