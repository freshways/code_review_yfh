﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATOMEHR_LeaveClient.SqlSugar_Models;
using ATOMEHR_LeaveClient.Util;

namespace ATOMEHR_LeaveClient.SqlSugar_DALs
{
    class dal_健康体检_Reference
    {
        SqlSugarConn conn = new SqlSugarConn(DBConn.connSQLite);

        /// <summary>
        /// 写入参照数据
        /// </summary>
        /// <param name="ls"></param>
        /// <returns></returns>
        public int Set参照数据(List<tb_健康体检_Reference> ls)
        {
            //Begin WXF 2019-01-14 | 18:53
            //SqlSugar插入sqlite的时候如果单次数据量过大会造成out of memory(内存溢出),故改为分批插入 
            int iCount = 0;
            List<tb_健康体检_Reference> lsTemp = new List<tb_健康体检_Reference>();

            while (ls.Count > 0)
            {
                lsTemp = ls.Take<tb_健康体检_Reference>(5000).ToList<tb_健康体检_Reference>();
                ls.RemoveRange(0, lsTemp.Count);
                iCount += conn.db.Insertable<tb_健康体检_Reference>(lsTemp).ExecuteCommand();
                lsTemp.Clear();
            }

            return iCount;
            //End
        }

        /// <summary>
        /// 获取表总行数
        /// </summary>
        /// <returns></returns>
        public int GetTableRows()
        {
            return conn.db.Queryable<tb_健康体检_Reference>().Count();
        }

        /// <summary>
        /// 获取表是否为空
        /// </summary>
        /// <returns></returns>
        public bool GetTableIsNull()
        {
            if (GetTableRows() > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 清空表
        /// </summary>
        /// <returns></returns>
        public int DeleteAll()
        {
            return conn.db.Deleteable<tb_健康体检_Reference>().ExecuteCommand();
        }

        /// <summary>
        /// 根据档案号获取一条参照数据
        /// </summary>
        /// <param name="recordNum"></param>
        /// <returns></returns>
        public tb_健康体检_Reference GetOneRow(string recordNum)
        {
            return conn.db.Queryable<tb_健康体检_Reference>().Where(p => p.个人档案编号.Equals(recordNum)).First();
        }
    }
}
