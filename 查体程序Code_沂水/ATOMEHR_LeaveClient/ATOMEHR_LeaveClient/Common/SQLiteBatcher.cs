﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;

namespace ATOMEHR_LeaveClient.Common
{
    public sealed  class SQLiteBatcher
    {
        /// <summary>
        /// 获取或设置提供者服务的上下文。
        /// </summary>
        //public ServiceContext ServiceContext { get; set; }

        /// <summary>
        /// 将 <see cref="DataTable"/> 的数据批量插入到数据库中。
        /// </summary>
        /// <param name="dataTable">要批量插入的 <see cref="DataTable"/>。</param>
        /// <param name="batchSize">每批次写入的数据量。</param>
        public void Insert(DataTable dataTable, int batchSize = 10000)
        {
            //Checker.ArgumentNull(dataTable, "dataTable");
            if (dataTable.Rows.Count == 0)
            {
                return;
            }
            using (var connection = ServiceContext.Database.CreateConnection())
            {
                DbTransaction transcation = null;
                try
                {
                    connection.TryOpen();
                    transcation = connection.BeginTransaction();
                    using (var command = ServiceContext.Database.Provider.DbProviderFactory.CreateCommand())
                    {
                        if (command == null)
                        {
                            throw new Exception("command");
                        }
                        command.Connection = connection;

                        command.CommandText = GenerateInserSql(ServiceContext.Database, dataTable);
                        if (command.CommandText == string.Empty)
                        {
                            return;
                        }

                        var flag = new AssertFlag();
                        dataTable.EachRow(row =>
                        {
                            var first = flag.AssertTrue();
                            ProcessCommandParameters(dataTable, command, row, first);
                            command.ExecuteNonQuery();
                        });
                    }
                    transcation.Commit();
                }
                catch (Exception exp)
                {
                    if (transcation != null)
                    {
                        transcation.Rollback();
                    }
                    throw new Exception(exp.Message);
                }
                finally
                {
                    connection.TryClose();
                }
            }
        }

        private void ProcessCommandParameters(DataTable dataTable, DbCommand command, DataRow row, bool first)
        {
            for (var c = 0; c < dataTable.Columns.Count; c++)
            {
                DbParameter parameter;
                //首次创建参数，是为了使用缓存
                if (first)
                {
                    parameter = ServiceContext.Database.Provider.DbProviderFactory.CreateParameter();
                    parameter.ParameterName = dataTable.Columns[c].ColumnName;
                    command.Parameters.Add(parameter);
                }
                else
                {
                    parameter = command.Parameters[c];
                }
                parameter.Value = row[c];
            }
        }

        /// <summary>
        /// 生成插入数据的sql语句。
        /// </summary>
        /// <param name="database"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        private string GenerateInserSql(IDatabase database, DataTable table)
        {
            var syntax = database.Provider.GetService<ISyntaxProvider>();
            var names = new StringBuilder();
            var values = new StringBuilder();
            var flag = new AssertFlag();
            table.EachColumn(column =>
            {
                if (!flag.AssertTrue())
                {
                    names.Append(",");
                    values.Append(",");
                }
                names.Append(DbUtility.FormatByQuote(syntax, column.ColumnName));
                values.AppendFormat("{0}{1}", syntax.ParameterPrefix, column.ColumnName);
            });
            return string.Format("INSERT INTO {0}({1}) VALUES ({2})", DbUtility.FormatByQuote(syntax, table.TableName), names, values);
        }
    }
}
