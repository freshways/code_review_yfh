﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace ATOMEHR_LeaveClient
{
    static class Program
    {
        #region 变量（暂时先放着吧，以后使用率高了再优化）
        public static User currentUser = new User();
        public static string _currentTime 
        { 
            get 
            { 
                return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"); 
            } 
        }//= DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        public static readonly string toAppPath = Application.StartupPath;
        public static readonly string iniName = "BlueToothSet.ini";//ini文件
        public static readonly string iniSys = "SysIni.ini";//ini文件
        public static string BlueName { get; set; }//ini文件
        public static string BlueAddr { get; set; }//ini文件
        public static string BlueType { get; set; }//ini文件
        public static string IDCardName { get; set; }//ini文件
        public static string IDCardAddr { get; set; }//ini文件
        /// <summary>
        /// 2016年6月29日14:13:15 yufh 添加 存入SysIni.ini文件
        /// </summary>
        public static string _bRGID { get; set; }//机构代码
        public static string _bRGName { get; set; }//机构名称
        public static string _bCreateuser { get; set; }//创建人
        public static string _bSerVer { get; set; }//服务器IP
        public static string _bDoctor { get; set; }//接诊医生 

        public static string _bLoginType { get; set; }//判断登陆
        public static string _uploadModel { get; set; }//上传模式1:上传到公卫;2:上传到主机;3:上传到公卫和主机
        #endregion
        public static string Cardreader { get; set; }
        public static string ThermometerGun { get; set; }
        public static string Sphygmomanometer { get; set; }
        public static string HeightWeightScale { get; set; }
        public static string ThermometerGunCom { get; set; }
        public static string SphygmomanometerCom { get; set; }
        public static string HeightWeightScaleCom { get; set; }

        //Begin WXF 2019-03-27 | 12:16
        //设置查体设备的连接方式 

        //End
											 

        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Boolean createdNew; //返回是否赋予了使用线程的互斥体初始所属权
            Mutex instance = new Mutex(true, "ATOMEHR_LeaveClient", out createdNew); //同步基元变量
            if (createdNew) //首次使用互斥体
            {
                instance.ReleaseMutex();
            }
            else
            {
                MessageBox.Show("已经启动了一个程序，请先退出！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                Application.Exit();
                return;
            }

            BindingIni(); //初始化配置

            Form objFrm = null;

            if(_bLoginType.Equals("刷卡"))
                objFrm = new frmLogin();
            else if (_bLoginType.Equals("扫码"))
                objFrm = new frmEnterIDNO();
            else
                objFrm = new frmLogin();

            objFrm.WindowState = FormWindowState.Maximized;
            objFrm.MaximizeBox = false;
            objFrm.MinimizeBox = false;
            Application.Run(objFrm);

            //frmLogin login = new frmLogin();
            //if (login.ShowDialog() == DialogResult.OK)
            //{
            //    frmMain frm = new frmMain();
            //    frm.WindowState = FormWindowState.Maximized;
            //    frm.MaximizeBox = false;
            //    frm.MinimizeBox = false;
            //    Application.Run(frm);
            //}


            #region 注册码验证

            //string sysFolder = System.Environment.CurrentDirectory;//获取系统安装目录，如c:\windows\system32

            //Reg reg = new Reg();
            //frmRegSoft frmreg = null;

            //Form objFrm = null;

            //string cupid = reg.GetCPUID();

            //if (!File.Exists(sysFolder + "\\atomclient.ini"))
            //{
            //    //如果注册文件不存在，注册失败
            //    //创建注册文件
            //    File.Create(sysFolder + "\\atomclient.ini").Dispose();
            //    if (DialogResult.OK == MessageBox.Show("软件尚未注册，请联系开发商进行注册！", "注册", MessageBoxButtons.OKCancel))
            //    {
            //        frmreg = new frmRegSoft();
            //        frmreg.Cpuid = cupid;
            //        if (DialogResult.OK == frmreg.ShowDialog())
            //        {
            //            if(_bLoginType.Equals("刷卡"))
            //                objFrm = new frmLogin();
            //            else if (_bLoginType.Equals("扫码"))
            //                objFrm = new frmEnterIDNO();
            //            else
            //                objFrm = new frmLogin();
            //        }
            //    }
            //}
            //else
            //{
            //    //如果注册文件存在，读取文件内容跟密码比较
            //    byte[] array = new byte[32];
            //    string str = "";
            //    FileInfo fi = new FileInfo(sysFolder + "\\atomclient.ini");
            //    FileStream fs = fi.OpenRead();
            //    int i = fs.Read(array, 0, 32);
            //    fs.Close();
            //    str = System.Text.Encoding.ASCII.GetString(array);

            //    if (str == reg.getMD5(cupid).Trim())//如果注册文件里的字符串和经过MD5运算过的注册码相同，则注册成功
            //    {
            //        if (_bLoginType.Equals("刷卡"))
            //            objFrm = new frmLogin();
            //        else if (_bLoginType.Equals("扫码"))
            //            objFrm = new frmEnterIDNO();
            //        else
            //            objFrm = new frmLogin();
            //    }
            //    else
            //    {
            //        if (DialogResult.OK == MessageBox.Show("软件尚未注册，请进行注册！", "注册", MessageBoxButtons.OKCancel))
            //        {
            //            frmreg = new frmRegSoft();
            //            frmreg.Cpuid = cupid;
            //            if (DialogResult.OK == frmreg.ShowDialog())
            //            {
            //                if (_bLoginType.Equals("刷卡"))
            //                    objFrm = new frmLogin();
            //                else if (_bLoginType.Equals("扫码"))
            //                    objFrm = new frmEnterIDNO();
            //                else
            //                    objFrm = new frmLogin();
            //            }
            //        }
            //    }
            //}

            //if (objFrm != null && objFrm.ShowDialog() == DialogResult.OK)
            //{
            //    frmMain frm = new frmMain();
            //    frm.WindowState = FormWindowState.Maximized;
            //    frm.MaximizeBox = false;
            //    frm.MinimizeBox = false;
            //    Application.Run(frm);
            //}
            #endregion

        }

        public static void BindingIni()
        {
            //设备配置
            Program.BlueName = ControlsHelper.IniReadValue("BlueTooth", "Name", Path.Combine(Program.toAppPath, Program.iniName));
            Program.BlueAddr = ControlsHelper.IniReadValue("BlueTooth", "Addr", Path.Combine(Program.toAppPath, Program.iniName));
            Program.BlueType = ControlsHelper.IniReadValue("BlueTooth", "Type", Path.Combine(Program.toAppPath, Program.iniName));
            Program.IDCardName = ControlsHelper.IniReadValue("BlueTooth", "IDCardName", Path.Combine(Program.toAppPath, Program.iniName));
            Program.IDCardAddr = ControlsHelper.IniReadValue("BlueTooth", "IDCardAddr", Path.Combine(Program.toAppPath, Program.iniName));
            //档案配置信息
            Program._bRGID = ControlsHelper.IniReadValue("BlueTooth", "RGID", Path.Combine(Program.toAppPath, Program.iniName));
            Program._bRGName = ControlsHelper.IniReadValue("BlueTooth", "RGName", Path.Combine(Program.toAppPath, Program.iniName));
            Program._bCreateuser = ControlsHelper.IniReadValue("BlueTooth", "UserID", Path.Combine(Program.toAppPath, Program.iniName));
            Program._bSerVer = ControlsHelper.IniReadValue("BlueTooth", "SerVer", Path.Combine(Program.toAppPath, Program.iniName));
            Program._bDoctor = ControlsHelper.IniReadValue("BlueTooth", "Doctor", Path.Combine(Program.toAppPath, Program.iniName));
            Program._bLoginType = ControlsHelper.IniReadValue("BlueTooth", "DefaultLogin", Path.Combine(Program.toAppPath, Program.iniName));
            // add by lid---start
            Program._uploadModel = ControlsHelper.IniReadValue("BlueTooth", "UploadModel", Path.Combine(Program.toAppPath, Program.iniName));
            // add by lid---end

            //Begin WXF 2019-03-27 | 12:19
            //设置查体设备的连接方式 
            Program.Cardreader = ControlsHelper.IniReadValue("Connection", "Cardreader", Path.Combine(Program.toAppPath, Program.iniName));
            Program.ThermometerGun = ControlsHelper.IniReadValue("Connection", "ThermometerGun", Path.Combine(Program.toAppPath, Program.iniName));
            Program.Sphygmomanometer = ControlsHelper.IniReadValue("Connection", "Sphygmomanometer", Path.Combine(Program.toAppPath, Program.iniName));
            Program.HeightWeightScale = ControlsHelper.IniReadValue("Connection", "HeightWeightScale", Path.Combine(Program.toAppPath, Program.iniName));
            //End
            Program.ThermometerGunCom = ControlsHelper.IniReadValue("Connection", "ThermometerGunCom", Path.Combine(Program.toAppPath, Program.iniName));
            Program.SphygmomanometerCom = ControlsHelper.IniReadValue("Connection", "SphygmomanometerCom", Path.Combine(Program.toAppPath, Program.iniName));
            Program.HeightWeightScaleCom = ControlsHelper.IniReadValue("Connection", "HeightWeightScaleCom", Path.Combine(Program.toAppPath, Program.iniName));
											 
        }
    }
}
