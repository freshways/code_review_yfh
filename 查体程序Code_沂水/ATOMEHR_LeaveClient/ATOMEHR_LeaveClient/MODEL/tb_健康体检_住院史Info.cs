﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATOMEHR_LeaveClient
{
    public class tb_健康体检_住院史Info
    {
        private string _ID;
        /// <summary>
        /// 获取或设置 ID 的值
        /// </summary>
        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private string _身份证号;
        /// <summary>
        /// 获取或设置 身份证号 的值
        /// </summary>
        public string 身份证号
        {
            get { return _身份证号; }
            set { _身份证号 = value; }
        }

        private string _个人档案编号;
        /// <summary>
        /// 获取或设置 个人档案编号 的值
        /// </summary>
        public string 个人档案编号
        {
            get { return _个人档案编号; }
            set { _个人档案编号 = value; }
        }

        private string _类型;
        /// <summary>
        /// 获取或设置 类型 的值
        /// </summary>
        public string 类型
        {
            get { return _类型; }
            set { _类型 = value; }
        }

        private string _入院日期;
        /// <summary>
        /// 获取或设置 入院日期 的值
        /// </summary>
        public string 入院日期
        {
            get { return _入院日期; }
            set { _入院日期 = value; }
        }

        private string _出院日期;
        /// <summary>
        /// 获取或设置 出院日期 的值
        /// </summary>
        public string 出院日期
        {
            get { return _出院日期; }
            set { _出院日期 = value; }
        }

        private string _原因;
        /// <summary>
        /// 获取或设置 原因 的值
        /// </summary>
        public string 原因
        {
            get { return _原因; }
            set { _原因 = value; }
        }

        private string _医疗机构名称;
        /// <summary>
        /// 获取或设置 医疗机构名称 的值
        /// </summary>
        public string 医疗机构名称
        {
            get { return _医疗机构名称; }
            set { _医疗机构名称 = value; }
        }

        private string _病案号;
        /// <summary>
        /// 获取或设置 病案号 的值
        /// </summary>
        public string 病案号
        {
            get { return _病案号; }
            set { _病案号 = value; }
        }

        private string _创建日期;
        /// <summary>
        /// 获取或设置 创建日期 的值
        /// </summary>
        public string 创建日期
        {
            get { return _创建日期; }
            set { _创建日期 = value; }
        }

        public string BlueName { get; set; }

        public string BlueAddr { get; set; }

        public string BlueType { get; set; }
    }
}
