﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATOMEHR_LeaveClient
{
    public class tb_导出用户Info
    {
        private string _ID;
        /// <summary>
        /// 获取或设置 ID 的值
        /// </summary>
        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private string _身份证号;
        /// <summary>
        /// 获取或设置 身份证号 的值
        /// </summary>
        public string 身份证号
        {
            get { return _身份证号; }
            set { _身份证号 = value; }
        }

        private string _姓名;
        /// <summary>
        /// 获取或设置 姓名 的值
        /// </summary>
        public string 姓名
        {
            get { return _姓名; }
            set { _姓名 = value; }
        }

        private string _性别;
        /// <summary>
        /// 获取或设置 性别 的值
        /// </summary>
        public string 性别
        {
            get { return _性别; }
            set { _性别 = value; }
        }

        private string _居住地址;
        /// <summary>
        /// 获取或设置 居住地址 的值
        /// </summary>
        public string 居住地址
        {
            get { return _居住地址; }
            set { _居住地址 = value; }
        }

        private string _出生日期;
        /// <summary>
        /// 获取或设置 出生日期 的值
        /// </summary>
        public string 出生日期
        {
            get { return _出生日期; }
            set { _出生日期 = value; }
        }

        public string 个人档案编号 { get; set; }
        public string 所属机构 { get; set; }
    }
}
