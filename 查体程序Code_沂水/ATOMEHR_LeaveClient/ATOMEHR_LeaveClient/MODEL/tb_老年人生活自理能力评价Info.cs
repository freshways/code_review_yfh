﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATOMEHR_LeaveClient
{
    public class tb_老年人生活自理能力评价Info
    {
        public int RowID { get; set; }

        private string _ID;
        /// <summary>
        /// 获取或设置 ID 的值
        /// </summary>
        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private string _身份证号;
        /// <summary>
        /// 获取或设置 身份证号 的值
        /// </summary>
        public string 身份证号
        {
            get { return _身份证号; }
            set { _身份证号 = value; }
        }

        private string _姓名;
        /// <summary>
        /// 获取或设置 姓名 的值
        /// </summary>
        public string 姓名
        {
            get { return _姓名; }
            set { _姓名 = value; }
        }

        private string _性别;
        /// <summary>
        /// 获取或设置 性别 的值
        /// </summary>
        public string 性别
        {
            get { return _性别; }
            set { _性别 = value; }
        }

        private string _出生日期;
        /// <summary>
        /// 获取或设置 出生日期 的值
        /// </summary>
        public string 出生日期
        {
            get { return _出生日期; }
            set { _出生日期 = value; }
        }

        private string _居住地址;
        /// <summary>
        /// 获取或设置 居住地址 的值
        /// </summary>
        public string 居住地址
        {
            get { return _居住地址; }
            set { _居住地址 = value; }
        }

        private string _个人档案编号;
        /// <summary>
        /// 获取或设置 个人档案编号 的值
        /// </summary>
        public string 个人档案编号
        {
            get { return _个人档案编号; }
            set { _个人档案编号 = value; }
        }

        private string _G_ZZBS;
        /// <summary>
        /// 获取或设置 G_ZZBS 的值
        /// </summary>
        public string G_ZZBS
        {
            get { return _G_ZZBS; }
            set { _G_ZZBS = value; }
        }

        private string _G_ZZXZZ;
        /// <summary>
        /// 获取或设置 G_ZZXZZ 的值
        /// </summary>
        public string G_ZZXZZ
        {
            get { return _G_ZZXZZ; }
            set { _G_ZZXZZ = value; }
        }

        private string _G_ZZCX;
        /// <summary>
        /// 获取或设置 G_ZZCX 的值
        /// </summary>
        public string G_ZZCX
        {
            get { return _G_ZZCX; }
            set { _G_ZZCX = value; }
        }

        private string _G_ZZZZ;
        /// <summary>
        /// 获取或设置 G_ZZZZ 的值
        /// </summary>
        public string G_ZZZZ
        {
            get { return _G_ZZZZ; }
            set { _G_ZZZZ = value; }
        }

        private string _G_ZTH;
        /// <summary>
        /// 获取或设置 G_ZTH 的值
        /// </summary>
        public string G_ZTH
        {
            get { return _G_ZTH; }
            set { _G_ZTH = value; }
        }

        private string _G_ZTYY;
        /// <summary>
        /// 获取或设置 G_ZTYY 的值
        /// </summary>
        public string G_ZTYY
        {
            get { return _G_ZTYY; }
            set { _G_ZTYY = value; }
        }

        private string _G_ZTZD;
        /// <summary>
        /// 获取或设置 G_ZTZD 的值
        /// </summary>
        public string G_ZTZD
        {
            get { return _G_ZTZD; }
            set { _G_ZTZD = value; }
        }

        private string _G_ZTZZ;
        /// <summary>
        /// 获取或设置 G_ZTZZ 的值
        /// </summary>
        public string G_ZTZZ
        {
            get { return _G_ZTZZ; }
            set { _G_ZTZZ = value; }
        }

        private decimal _G_SFTZ;
        /// <summary>
        /// 获取或设置 G_SFTZ 的值
        /// </summary>
        public decimal G_SFTZ
        {
            get { return _G_SFTZ; }
            set { _G_SFTZ = value; }
        }

        private int _G_RXY;
        /// <summary>
        /// 获取或设置 G_RXY 的值
        /// </summary>
        public int G_RXY
        {
            get { return _G_RXY; }
            set { _G_RXY = value; }
        }

        private decimal _G_RYJ;
        /// <summary>
        /// 获取或设置 G_RYJ 的值
        /// </summary>
        public decimal G_RYJ
        {
            get { return _G_RYJ; }
            set { _G_RYJ = value; }
        }

        private int _G_ZYD;
        /// <summary>
        /// 获取或设置 G_ZYD 的值
        /// </summary>
        public int G_ZYD
        {
            get { return _G_ZYD; }
            set { _G_ZYD = value; }
        }

        private decimal _G_YDSJ;
        /// <summary>
        /// 获取或设置 G_YDSJ 的值
        /// </summary>
        public decimal G_YDSJ
        {
            get { return _G_YDSJ; }
            set { _G_YDSJ = value; }
        }

        private string _G_YS;
        /// <summary>
        /// 获取或设置 G_YS 的值
        /// </summary>
        public string G_YS
        {
            get { return _G_YS; }
            set { _G_YS = value; }
        }

        private string _G_XLTZ;
        /// <summary>
        /// 获取或设置 G_XLTZ 的值
        /// </summary>
        public string G_XLTZ
        {
            get { return _G_XLTZ; }
            set { _G_XLTZ = value; }
        }

        private string _G_ZYXW;
        /// <summary>
        /// 获取或设置 G_ZYXW 的值
        /// </summary>
        public string G_ZYXW
        {
            get { return _G_ZYXW; }
            set { _G_ZYXW = value; }
        }

        private string _G_YMJZ;
        /// <summary>
        /// 获取或设置 G_YMJZ 的值
        /// </summary>
        public string G_YMJZ
        {
            get { return _G_YMJZ; }
            set { _G_YMJZ = value; }
        }

        private string _G_GXBYF;
        /// <summary>
        /// 获取或设置 G_GXBYF 的值
        /// </summary>
        public string G_GXBYF
        {
            get { return _G_GXBYF; }
            set { _G_GXBYF = value; }
        }

        private string _G_GZSSYF;
        /// <summary>
        /// 获取或设置 G_GZSSYF 的值
        /// </summary>
        public string G_GZSSYF
        {
            get { return _G_GZSSYF; }
            set { _G_GZSSYF = value; }
        }

        private string _下次随访目标;
        /// <summary>
        /// 获取或设置 下次随访目标 的值
        /// </summary>
        public string 下次随访目标
        {
            get { return _下次随访目标; }
            set { _下次随访目标 = value; }
        }

        private string _下次随访日期;
        /// <summary>
        /// 获取或设置 下次随访日期 的值
        /// </summary>
        public string 下次随访日期
        {
            get { return _下次随访日期; }
            set { _下次随访日期 = value; }
        }

        private string _随访医生;
        /// <summary>
        /// 获取或设置 随访医生 的值
        /// </summary>
        public string 随访医生
        {
            get { return _随访医生; }
            set { _随访医生 = value; }
        }

        private string _创建机构;
        /// <summary>
        /// 获取或设置 创建机构 的值
        /// </summary>
        public string 创建机构
        {
            get { return _创建机构; }
            set { _创建机构 = value; }
        }

        private string _创建人;
        /// <summary>
        /// 获取或设置 创建人 的值
        /// </summary>
        public string 创建人
        {
            get { return _创建人; }
            set { _创建人 = value; }
        }

        private string _更新人;
        /// <summary>
        /// 获取或设置 更新人 的值
        /// </summary>
        public string 更新人
        {
            get { return _更新人; }
            set { _更新人 = value; }
        }

        private string _所属机构;
        /// <summary>
        /// 获取或设置 所属机构 的值
        /// </summary>
        public string 所属机构
        {
            get { return _所属机构; }
            set { _所属机构 = value; }
        }

        private string _创建时间;
        /// <summary>
        /// 获取或设置 创建时间 的值
        /// </summary>
        public string 创建时间
        {
            get { return _创建时间; }
            set { _创建时间 = value; }
        }

        private string _更新时间;
        /// <summary>
        /// 获取或设置 更新时间 的值
        /// </summary>
        public string 更新时间
        {
            get { return _更新时间; }
            set { _更新时间 = value; }
        }

        private string _随访日期;
        /// <summary>
        /// 获取或设置 随访日期 的值
        /// </summary>
        public string 随访日期
        {
            get { return _随访日期; }
            set { _随访日期 = value; }
        }

        private string _缺项;
        /// <summary>
        /// 获取或设置 缺项 的值
        /// </summary>
        public string 缺项
        {
            get { return _缺项; }
            set { _缺项 = value; }
        }

        private string _进餐评分;
        /// <summary>
        /// 获取或设置 进餐评分 的值
        /// </summary>
        public string 进餐评分
        {
            get { return _进餐评分; }
            set { _进餐评分 = value; }
        }

        private string _梳洗评分;
        /// <summary>
        /// 获取或设置 梳洗评分 的值
        /// </summary>
        public string 梳洗评分
        {
            get { return _梳洗评分; }
            set { _梳洗评分 = value; }
        }

        private string _如厕评分;
        /// <summary>
        /// 获取或设置 如厕评分 的值
        /// </summary>
        public string 如厕评分
        {
            get { return _如厕评分; }
            set { _如厕评分 = value; }
        }

        private string _活动评分;
        /// <summary>
        /// 获取或设置 活动评分 的值
        /// </summary>
        public string 活动评分
        {
            get { return _活动评分; }
            set { _活动评分 = value; }
        }

        private string _总评分;
        /// <summary>
        /// 获取或设置 总评分 的值
        /// </summary>
        public string 总评分
        {
            get { return _总评分; }
            set { _总评分 = value; }
        }

        private string _穿衣评分;
        /// <summary>
        /// 获取或设置 穿衣评分 的值
        /// </summary>
        public string 穿衣评分
        {
            get { return _穿衣评分; }
            set { _穿衣评分 = value; }
        }

        private string _随访次数;
        /// <summary>
        /// 获取或设置 随访次数 的值
        /// </summary>
        public string 随访次数
        {
            get { return _随访次数; }
            set { _随访次数 = value; }
        }

        private string _完整度;
        /// <summary>
        /// 获取或设置 完整度 的值
        /// </summary>
        public string 完整度
        {
            get { return _完整度; }
            set { _完整度 = value; }
        }


        public string BlueName { get; set; }

        public string BlueAddr { get; set; }

        public string BlueType { get; set; }

        public string RowState { get; set; }
    }
}
