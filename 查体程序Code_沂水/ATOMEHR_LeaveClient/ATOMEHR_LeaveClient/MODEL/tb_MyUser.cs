﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ATOMEHR_LeaveClient
{
    [Serializable]
    public class tb_MyUser
    {
        /// <summary>
        /// isid
        /// </summary>
        [DisplayName("isid")]
        public int isid { get; set; }

        /// <summary>
        /// 登录帐号
        /// </summary>
        [DisplayName("登录帐号")]
        public string Account { get; set; }

        /// <summary>
        /// Novell网帐号(全名,如.Jonny.EDP.CKG.MO)
        /// </summary>
        [DisplayName("Novell网帐号(全名,如.Jonny.EDP.CKG.MO)")]
        public string NovellAccount { get; set; }

        /// <summary>
        /// Windows Domain Account(如:jonny@ckg.mo)
        /// </summary>
        [DisplayName("Windows Domain Account(如:jonny@ckg.mo)")]
        public string DomainName { get; set; }

        /// <summary>
        /// 用户编码
        /// </summary>
        [DisplayName("用户编码")]
        public string 用户编码 { get; set; }

        /// <summary>
        /// 中文名称
        /// </summary>
        [DisplayName("中文名称")]
        public string UserName { get; set; }

        /// <summary>
        /// Address
        /// </summary>
        [DisplayName("Address")]
        public string Address { get; set; }

        /// <summary>
        /// Tel
        /// </summary>
        [DisplayName("Tel")]
        public string Tel { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [DisplayName("Email")]
        public string Email { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        [DisplayName("Password")]
        public string Password { get; set; }

        /// <summary>
        /// LastLoginTime
        /// </summary>
        [DisplayName("LastLoginTime")]
        public DateTime LastLoginTime { get; set; }

        /// <summary>
        /// LastLogoutTime
        /// </summary>
        [DisplayName("LastLogoutTime")]
        public DateTime? LastLogoutTime { get; set; }

        /// <summary>
        /// IsLocked
        /// </summary>
        [DisplayName("IsLocked")]
        public short IsLocked { get; set; }

        /// <summary>
        /// CreateTime
        /// </summary>
        [DisplayName("CreateTime")]
        public DateTime? CreateTime { get; set; }

        /// <summary>
        /// FlagAdmin
        /// </summary>
        [DisplayName("FlagAdmin")]
        public string FlagAdmin { get; set; }

        /// <summary>
        /// FlagOnline
        /// </summary>
        [DisplayName("FlagOnline")]
        public string FlagOnline { get; set; }

        /// <summary>
        /// LoginCounter
        /// </summary>
        [DisplayName("LoginCounter")]
        public int? LoginCounter { get; set; }

        /// <summary>
        /// DataSets
        /// </summary>
        [DisplayName("DataSets")]
        public string DataSets { get; set; }

        /// <summary>
        /// 所属机构
        /// </summary>
        [DisplayName("所属机构")]
        public string 所属机构 { get; set; }

        /// <summary>
        /// 身份证
        /// </summary>
        [DisplayName("身份证")]
        public string 身份证 { get; set; }

    }
}
