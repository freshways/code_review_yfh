﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATOMEHR_LeaveClient
{
    public class tb_健康体检_非免疫规划预防接种史Info
    {
        private string _ID;
        /// <summary>
        /// 获取或设置 ID 的值
        /// </summary>
        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private string _身份证号;
        /// <summary>
        /// 获取或设置 身份证号 的值
        /// </summary>
        public string 身份证号
        {
            get { return _身份证号; }
            set { _身份证号 = value; }
        }

        private string _个人档案编号;
        /// <summary>
        /// 获取或设置 个人档案编号 的值
        /// </summary>
        public string 个人档案编号
        {
            get { return _个人档案编号; }
            set { _个人档案编号 = value; }
        }

        private string _接种名称;
        /// <summary>
        /// 获取或设置 接种名称 的值
        /// </summary>
        public string 接种名称
        {
            get { return _接种名称; }
            set { _接种名称 = value; }
        }

        private string _接种日期;
        /// <summary>
        /// 获取或设置 接种日期 的值
        /// </summary>
        public string 接种日期
        {
            get { return _接种日期; }
            set { _接种日期 = value; }
        }

        private string _接种机构;
        /// <summary>
        /// 获取或设置 接种机构 的值
        /// </summary>
        public string 接种机构
        {
            get { return _接种机构; }
            set { _接种机构 = value; }
        }

        private string _创建日期;
        /// <summary>
        /// 获取或设置 创建日期 的值
        /// </summary>
        public string 创建日期
        {
            get { return _创建日期; }
            set { _创建日期 = value; }
        }

        public string BlueType { get; set; }

        public string BlueAddr { get; set; }

        public string BlueName { get; set; }
    }
}
