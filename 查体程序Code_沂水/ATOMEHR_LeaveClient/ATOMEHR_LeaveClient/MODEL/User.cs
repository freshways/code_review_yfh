﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATOMEHR_LeaveClient
{
    public class User
    {
        string name;//姓名
        string id;//身份证号
        string gender;//性别
        int age;//年龄
        string spo2;//血氧饱和度
        string pr;//脉率
        string pi;//灌注指数
        string birth;//出生日期
        string addr;//居住地址
        string photoPath;//头像地址
        string docNo;//个人档案编号
        string prgid;//所属机构

        public string Prgid
        {
            get { return prgid; }
            set { prgid = value; }
        }
        tb_老年人中医药特征管理Info tab_中医辨识体质;//对应的中医辨识体质
        tb_健康体检Info tab_健康体检;//对应的中医辨识体质
        tb_老年人生活自理能力评价Info tab_生活自理能力评价;//生活自理能力评价
        tb_健康体检_非免疫规划预防接种史Info tab_预防接种史;
        tb_健康体检_用药情况表Info tab_用药情况表;
        tb_健康体检_住院史Info tab_住院史;

        public string DocNo
        {
            get { return docNo; }
            set { docNo = value; }
        }
        public string 创建人
        {
            get 
            {
                return Program._bCreateuser == "" ? "371323B100190001" : Program._bCreateuser; //如果该参数是空默认姚店子
            }
        }
        public string PhotoPath
        {
            get { return photoPath; }
            set { photoPath = value; }
        }

        public tb_老年人生活自理能力评价Info Tab_生活自理能力评价
        {
            get { return tab_生活自理能力评价; }
            set { tab_生活自理能力评价 = value; }
        }

        public tb_健康体检Info Tab_健康体检
        {
            get { return tab_健康体检; }
            set { tab_健康体检 = value; }
        }

        public tb_老年人中医药特征管理Info Tab_中医辨识体质
        {
            get { return tab_中医辨识体质; }
            set { tab_中医辨识体质 = value; }
        }

        public string Addr
        {
            get { return addr; }
            set { addr = value; }
        }

        public string Birth
        {
            get { return birth; }
            set { birth = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string ID
        {
            get { return id; }
            set { id = value; }
        }

        public string Gender
        {
            get { return gender; }
            set { gender = value; }
        }

        public int Age
        {
            get { return age; }
            set { age = value; }
        }

        public string Spo2
        {
            get { return spo2; }
            set { spo2 = value; }
        }


        public string PR
        {
            get { return pr; }
            set { pr = value; }
        }

        public string PI
        {
            get { return pi; }
            set { pi = value; }
        }
    }
}
