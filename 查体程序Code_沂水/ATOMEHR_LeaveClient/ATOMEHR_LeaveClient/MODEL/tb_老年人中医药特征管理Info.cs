﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace ATOMEHR_LeaveClient
{


    public class tb_老年人中医药特征管理Info
    {
        public int RowID { get; set; }

        private string _ID;
        /// <summary>
        /// 获取或设置 ID 的值
        /// </summary>
        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public string 个人档案编号 { get; set; }

        private string _身份证号;
        /// <summary>
        /// 获取或设置 身份证号 的值
        /// </summary>
        public string 身份证号
        {
            get { return _身份证号; }
            set { _身份证号 = value; }
        }

        private string _姓名;
        /// <summary>
        /// 获取或设置 姓名 的值
        /// </summary>
        public string 姓名
        {
            get { return _姓名; }
            set { _姓名 = value; }
        }

        private string _性别;
        /// <summary>
        /// 获取或设置 性别 的值
        /// </summary>
        public string 性别
        {
            get { return _性别; }
            set { _性别 = value; }
        }

        private string _出生日期;
        /// <summary>
        /// 获取或设置 出生日期 的值
        /// </summary>
        public string 出生日期
        {
            get { return _出生日期; }
            set { _出生日期 = value; }
        }

        private string _居住地址;
        /// <summary>
        /// 获取或设置 居住地址 的值
        /// </summary>
        public string 居住地址
        {
            get { return _居住地址; }
            set { _居住地址 = value; }
        }

        private string _创建机构;
        /// <summary>
        /// 获取或设置 创建机构 的值
        /// </summary>
        public string 创建机构
        {
            get { return _创建机构; }
            set { _创建机构 = value; }
        }

        private string _创建人;
        /// <summary>
        /// 获取或设置 创建人 的值
        /// </summary>
        public string 创建人
        {
            get { return _创建人; }
            set { _创建人 = value; }
        }

        private string _修改人;
        /// <summary>
        /// 获取或设置 修改人 的值
        /// </summary>
        public string 修改人
        {
            get { return _修改人; }
            set { _修改人 = value; }
        }

        private string _所属机构;
        /// <summary>
        /// 获取或设置 所属机构 的值
        /// </summary>
        public string 所属机构
        {
            get { return _所属机构; }
            set { _所属机构 = value; }
        }

        private string _创建时间;
        /// <summary>
        /// 获取或设置 创建时间 的值
        /// </summary>
        public string 创建时间
        {
            get { return _创建时间; }
            set { _创建时间 = value; }
        }

        private string _修改时间;
        /// <summary>
        /// 获取或设置 修改时间 的值
        /// </summary>
        public string 修改时间
        {
            get { return _修改时间; }
            set { _修改时间 = value; }
        }

        private int _特征1;
        /// <summary>
        /// 获取或设置 特征1 的值
        /// </summary>
        public int 特征1
        {
            get { return _特征1; }
            set { _特征1 = value; }
        }

        private int _特征2;
        /// <summary>
        /// 获取或设置 特征2 的值
        /// </summary>
        public int 特征2
        {
            get { return _特征2; }
            set { _特征2 = value; }
        }

        private int _特征3;
        /// <summary>
        /// 获取或设置 特征3 的值
        /// </summary>
        public int 特征3
        {
            get { return _特征3; }
            set { _特征3 = value; }
        }

        private int _特征4;
        /// <summary>
        /// 获取或设置 特征4 的值
        /// </summary>
        public int 特征4
        {
            get { return _特征4; }
            set { _特征4 = value; }
        }

        private int _特征5;
        /// <summary>
        /// 获取或设置 特征5 的值
        /// </summary>
        public int 特征5
        {
            get { return _特征5; }
            set { _特征5 = value; }
        }

        private int _特征6;
        /// <summary>
        /// 获取或设置 特征6 的值
        /// </summary>
        public int 特征6
        {
            get { return _特征6; }
            set { _特征6 = value; }
        }

        private int _特征7;
        /// <summary>
        /// 获取或设置 特征7 的值
        /// </summary>
        public int 特征7
        {
            get { return _特征7; }
            set { _特征7 = value; }
        }

        private int _特征8;
        /// <summary>
        /// 获取或设置 特征8 的值
        /// </summary>
        public int 特征8
        {
            get { return _特征8; }
            set { _特征8 = value; }
        }

        private int _特征9;
        /// <summary>
        /// 获取或设置 特征9 的值
        /// </summary>
        public int 特征9
        {
            get { return _特征9; }
            set { _特征9 = value; }
        }

        private int _特征10;
        /// <summary>
        /// 获取或设置 特征10 的值
        /// </summary>
        public int 特征10
        {
            get { return _特征10; }
            set { _特征10 = value; }
        }

        private int _特征11;
        /// <summary>
        /// 获取或设置 特征11 的值
        /// </summary>
        public int 特征11
        {
            get { return _特征11; }
            set { _特征11 = value; }
        }

        private int _特征12;
        /// <summary>
        /// 获取或设置 特征12 的值
        /// </summary>
        public int 特征12
        {
            get { return _特征12; }
            set { _特征12 = value; }
        }

        private int _特征13;
        /// <summary>
        /// 获取或设置 特征13 的值
        /// </summary>
        public int 特征13
        {
            get { return _特征13; }
            set { _特征13 = value; }
        }

        private int _特征14;
        /// <summary>
        /// 获取或设置 特征14 的值
        /// </summary>
        public int 特征14
        {
            get { return _特征14; }
            set { _特征14 = value; }
        }

        private int _特征15;
        /// <summary>
        /// 获取或设置 特征15 的值
        /// </summary>
        public int 特征15
        {
            get { return _特征15; }
            set { _特征15 = value; }
        }

        private int _特征16;
        /// <summary>
        /// 获取或设置 特征16 的值
        /// </summary>
        public int 特征16
        {
            get { return _特征16; }
            set { _特征16 = value; }
        }

        private int _特征17;
        /// <summary>
        /// 获取或设置 特征17 的值
        /// </summary>
        public int 特征17
        {
            get { return _特征17; }
            set { _特征17 = value; }
        }

        private int _特征18;
        /// <summary>
        /// 获取或设置 特征18 的值
        /// </summary>
        public int 特征18
        {
            get { return _特征18; }
            set { _特征18 = value; }
        }

        private int _特征19;
        /// <summary>
        /// 获取或设置 特征19 的值
        /// </summary>
        public int 特征19
        {
            get { return _特征19; }
            set { _特征19 = value; }
        }

        private int _特征20;
        /// <summary>
        /// 获取或设置 特征20 的值
        /// </summary>
        public int 特征20
        {
            get { return _特征20; }
            set { _特征20 = value; }
        }

        private int _特征21;
        /// <summary>
        /// 获取或设置 特征21 的值
        /// </summary>
        public int 特征21
        {
            get { return _特征21; }
            set { _特征21 = value; }
        }

        private int _特征22;
        /// <summary>
        /// 获取或设置 特征22 的值
        /// </summary>
        public int 特征22
        {
            get { return _特征22; }
            set { _特征22 = value; }
        }

        private int _特征23;
        /// <summary>
        /// 获取或设置 特征23 的值
        /// </summary>
        public int 特征23
        {
            get { return _特征23; }
            set { _特征23 = value; }
        }

        private int _特征24;
        /// <summary>
        /// 获取或设置 特征24 的值
        /// </summary>
        public int 特征24
        {
            get { return _特征24; }
            set { _特征24 = value; }
        }

        private int _特征25;
        /// <summary>
        /// 获取或设置 特征25 的值
        /// </summary>
        public int 特征25
        {
            get { return _特征25; }
            set { _特征25 = value; }
        }

        private int _特征26;
        /// <summary>
        /// 获取或设置 特征26 的值
        /// </summary>
        public int 特征26
        {
            get { return _特征26; }
            set { _特征26 = value; }
        }

        private int _特征27;
        /// <summary>
        /// 获取或设置 特征27 的值
        /// </summary>
        public int 特征27
        {
            get { return _特征27; }
            set { _特征27 = value; }
        }

        private int _特征28;
        /// <summary>
        /// 获取或设置 特征28 的值
        /// </summary>
        public int 特征28
        {
            get { return _特征28; }
            set { _特征28 = value; }
        }

        private int _特征29;
        /// <summary>
        /// 获取或设置 特征29 的值
        /// </summary>
        public int 特征29
        {
            get { return _特征29; }
            set { _特征29 = value; }
        }

        private int _特征30;
        /// <summary>
        /// 获取或设置 特征30 的值
        /// </summary>
        public int 特征30
        {
            get { return _特征30; }
            set { _特征30 = value; }
        }

        private int _特征31;
        /// <summary>
        /// 获取或设置 特征31 的值
        /// </summary>
        public int 特征31
        {
            get { return _特征31; }
            set { _特征31 = value; }
        }

        private int _特征32;
        /// <summary>
        /// 获取或设置 特征32 的值
        /// </summary>
        public int 特征32
        {
            get { return _特征32; }
            set { _特征32 = value; }
        }

        private int _特征33;
        /// <summary>
        /// 获取或设置 特征33 的值
        /// </summary>
        public int 特征33
        {
            get { return _特征33; }
            set { _特征33 = value; }
        }

        private int _气虚质得分;
        /// <summary>
        /// 获取或设置 气虚质得分 的值
        /// </summary>
        public int 气虚质得分
        {
            get { return _气虚质得分; }
            set { _气虚质得分 = value; }
        }

        private string _气虚质辨识;
        /// <summary>
        /// 获取或设置 气虚质辨识 的值
        /// </summary>
        public string 气虚质辨识
        {
            get { return _气虚质辨识; }
            set { _气虚质辨识 = value; }
        }

        private string _气虚质指导;
        /// <summary>
        /// 获取或设置 气虚质指导 的值
        /// </summary>
        public string 气虚质指导
        {
            get { return _气虚质指导; }
            set { _气虚质指导 = value; }
        }

        private string _气虚质其他;
        /// <summary>
        /// 获取或设置 气虚质其他 的值
        /// </summary>
        public string 气虚质其他
        {
            get { return _气虚质其他; }
            set { _气虚质其他 = value; }
        }

        private int _阳虚质得分;
        /// <summary>
        /// 获取或设置 阳虚质得分 的值
        /// </summary>
        public int 阳虚质得分
        {
            get { return _阳虚质得分; }
            set { _阳虚质得分 = value; }
        }

        private string _阳虚质辨识;
        /// <summary>
        /// 获取或设置 阳虚质辨识 的值
        /// </summary>
        public string 阳虚质辨识
        {
            get { return _阳虚质辨识; }
            set { _阳虚质辨识 = value; }
        }

        private string _阳虚质指导;
        /// <summary>
        /// 获取或设置 阳虚质指导 的值
        /// </summary>
        public string 阳虚质指导
        {
            get { return _阳虚质指导; }
            set { _阳虚质指导 = value; }
        }

        private string _阳虚质其他;
        /// <summary>
        /// 获取或设置 阳虚质其他 的值
        /// </summary>
        public string 阳虚质其他
        {
            get { return _阳虚质其他; }
            set { _阳虚质其他 = value; }
        }

        private int _阴虚质得分;
        /// <summary>
        /// 获取或设置 阴虚质得分 的值
        /// </summary>
        public int 阴虚质得分
        {
            get { return _阴虚质得分; }
            set { _阴虚质得分 = value; }
        }

        private string _阴虚质辨识;
        /// <summary>
        /// 获取或设置 阴虚质辨识 的值
        /// </summary>
        public string 阴虚质辨识
        {
            get { return _阴虚质辨识; }
            set { _阴虚质辨识 = value; }
        }

        private string _阴虚质指导;
        /// <summary>
        /// 获取或设置 阴虚质指导 的值
        /// </summary>
        public string 阴虚质指导
        {
            get { return _阴虚质指导; }
            set { _阴虚质指导 = value; }
        }

        private string _阴虚质其他;
        /// <summary>
        /// 获取或设置 阴虚质其他 的值
        /// </summary>
        public string 阴虚质其他
        {
            get { return _阴虚质其他; }
            set { _阴虚质其他 = value; }
        }

        private int _痰湿质得分;
        /// <summary>
        /// 获取或设置 痰湿质得分 的值
        /// </summary>
        public int 痰湿质得分
        {
            get { return _痰湿质得分; }
            set { _痰湿质得分 = value; }
        }

        private string _痰湿质辨识;
        /// <summary>
        /// 获取或设置 痰湿质辨识 的值
        /// </summary>
        public string 痰湿质辨识
        {
            get { return _痰湿质辨识; }
            set { _痰湿质辨识 = value; }
        }

        private string _痰湿质指导;
        /// <summary>
        /// 获取或设置 痰湿质指导 的值
        /// </summary>
        public string 痰湿质指导
        {
            get { return _痰湿质指导; }
            set { _痰湿质指导 = value; }
        }

        private string _痰湿质其他;
        /// <summary>
        /// 获取或设置 痰湿质其他 的值
        /// </summary>
        public string 痰湿质其他
        {
            get { return _痰湿质其他; }
            set { _痰湿质其他 = value; }
        }

        private int _湿热质得分;
        /// <summary>
        /// 获取或设置 湿热质得分 的值
        /// </summary>
        public int 湿热质得分
        {
            get { return _湿热质得分; }
            set { _湿热质得分 = value; }
        }

        private string _湿热质辨识;
        /// <summary>
        /// 获取或设置 湿热质辨识 的值
        /// </summary>
        public string 湿热质辨识
        {
            get { return _湿热质辨识; }
            set { _湿热质辨识 = value; }
        }

        private string _湿热质指导;
        /// <summary>
        /// 获取或设置 湿热质指导 的值
        /// </summary>
        public string 湿热质指导
        {
            get { return _湿热质指导; }
            set { _湿热质指导 = value; }
        }

        private string _湿热质其他;
        /// <summary>
        /// 获取或设置 湿热质其他 的值
        /// </summary>
        public string 湿热质其他
        {
            get { return _湿热质其他; }
            set { _湿热质其他 = value; }
        }

        private int _血瘀质得分;
        /// <summary>
        /// 获取或设置 血瘀质得分 的值
        /// </summary>
        public int 血瘀质得分
        {
            get { return _血瘀质得分; }
            set { _血瘀质得分 = value; }
        }

        private string _血瘀质辨识;
        /// <summary>
        /// 获取或设置 血瘀质辨识 的值
        /// </summary>
        public string 血瘀质辨识
        {
            get { return _血瘀质辨识; }
            set { _血瘀质辨识 = value; }
        }

        private string _血瘀质指导;
        /// <summary>
        /// 获取或设置 血瘀质指导 的值
        /// </summary>
        public string 血瘀质指导
        {
            get { return _血瘀质指导; }
            set { _血瘀质指导 = value; }
        }

        private string _血瘀质其他;
        /// <summary>
        /// 获取或设置 血瘀质其他 的值
        /// </summary>
        public string 血瘀质其他
        {
            get { return _血瘀质其他; }
            set { _血瘀质其他 = value; }
        }

        private int _气郁质得分;
        /// <summary>
        /// 获取或设置 气郁质得分 的值
        /// </summary>
        public int 气郁质得分
        {
            get { return _气郁质得分; }
            set { _气郁质得分 = value; }
        }

        private string _气郁质辨识;
        /// <summary>
        /// 获取或设置 气郁质辨识 的值
        /// </summary>
        public string 气郁质辨识
        {
            get { return _气郁质辨识; }
            set { _气郁质辨识 = value; }
        }

        private string _气郁质指导;
        /// <summary>
        /// 获取或设置 气郁质指导 的值
        /// </summary>
        public string 气郁质指导
        {
            get { return _气郁质指导; }
            set { _气郁质指导 = value; }
        }

        private string _气郁质其他;
        /// <summary>
        /// 获取或设置 气郁质其他 的值
        /// </summary>
        public string 气郁质其他
        {
            get { return _气郁质其他; }
            set { _气郁质其他 = value; }
        }

        private int _特禀质得分;
        /// <summary>
        /// 获取或设置 特禀质得分 的值
        /// </summary>
        public int 特禀质得分
        {
            get { return _特禀质得分; }
            set { _特禀质得分 = value; }
        }

        private string _特禀质辨识;
        /// <summary>
        /// 获取或设置 特禀质辨识 的值
        /// </summary>
        public string 特禀质辨识
        {
            get { return _特禀质辨识; }
            set { _特禀质辨识 = value; }
        }

        private string _特禀质指导;
        /// <summary>
        /// 获取或设置 特禀质指导 的值
        /// </summary>
        public string 特禀质指导
        {
            get { return _特禀质指导; }
            set { _特禀质指导 = value; }
        }

        private string _特禀质其他;
        /// <summary>
        /// 获取或设置 特禀质其他 的值
        /// </summary>
        public string 特禀质其他
        {
            get { return _特禀质其他; }
            set { _特禀质其他 = value; }
        }

        private int _平和质得分;
        /// <summary>
        /// 获取或设置 平和质得分 的值
        /// </summary>
        public int 平和质得分
        {
            get { return _平和质得分; }
            set { _平和质得分 = value; }
        }

        private string _平和质辨识;
        /// <summary>
        /// 获取或设置 平和质辨识 的值
        /// </summary>
        public string 平和质辨识
        {
            get { return _平和质辨识; }
            set { _平和质辨识 = value; }
        }

        private string _平和质指导;
        /// <summary>
        /// 获取或设置 平和质指导 的值
        /// </summary>
        public string 平和质指导
        {
            get { return _平和质指导; }
            set { _平和质指导 = value; }
        }

        private string _平和质其他;
        /// <summary>
        /// 获取或设置 平和质其他 的值
        /// </summary>
        public string 平和质其他
        {
            get { return _平和质其他; }
            set { _平和质其他 = value; }
        }

        private string _发生时间;
        /// <summary>
        /// 获取或设置 发生时间 的值
        /// </summary>
        public string 发生时间
        {
            get { return _发生时间; }
            set { _发生时间 = value; }
        }

        private string _医生签名;
        /// <summary>
        /// 获取或设置 医生签名 的值
        /// </summary>
        public string 医生签名
        {
            get { return _医生签名; }
            set { _医生签名 = value; }
        }


        public string BlueName { get; set; }

        public string BlueAddr { get; set; }

        public string BlueType { get; set; }

        public string RowState { get; set; }
    }
}