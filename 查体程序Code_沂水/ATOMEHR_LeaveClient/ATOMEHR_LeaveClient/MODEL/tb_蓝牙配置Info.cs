﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATOMEHR_LeaveClient
{
    public class tb_蓝牙配置Info
    {

        private string _ID;
        /// <summary>
        /// 获取或设置 ID 的值
        /// </summary>
        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private string _蓝牙名称;
        /// <summary>
        /// 获取或设置 蓝牙名称 的值
        /// </summary>
        public string 蓝牙名称
        {
            get { return _蓝牙名称; }
            set { _蓝牙名称 = value; }
        }

        private string _类别;
        /// <summary>
        /// 获取或设置 类别 的值
        /// </summary>
        public string 类别
        {
            get { return _类别; }
            set { _类别 = value; }
        }
    }
}
