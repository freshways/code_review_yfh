﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATOMEHR_LeaveClient
{
    public class tb_健康体检_用药情况表Info
    {
        private string _ID;
        /// <summary>
        /// 获取或设置 ID 的值
        /// </summary>
        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private string _身份证号;
        /// <summary>
        /// 获取或设置 身份证号 的值
        /// </summary>
        public string 身份证号
        {
            get { return _身份证号; }
            set { _身份证号 = value; }
        }

        private string _个人档案编号;
        /// <summary>
        /// 获取或设置 个人档案编号 的值
        /// </summary>
        public string 个人档案编号
        {
            get { return _个人档案编号; }
            set { _个人档案编号 = value; }
        }

        private string _药物名称;
        /// <summary>
        /// 获取或设置 药物名称 的值
        /// </summary>
        public string 药物名称
        {
            get { return _药物名称; }
            set { _药物名称 = value; }
        }

        private string _用法;
        /// <summary>
        /// 获取或设置 用法 的值
        /// </summary>
        public string 用法
        {
            get { return _用法; }
            set { _用法 = value; }
        }

        private string _用量;
        /// <summary>
        /// 获取或设置 用量 的值
        /// </summary>
        public string 用量
        {
            get { return _用量; }
            set { _用量 = value; }
        }

        private string _用药时间;
        /// <summary>
        /// 获取或设置 用药时间 的值
        /// </summary>
        public string 用药时间
        {
            get { return _用药时间; }
            set { _用药时间 = value; }
        }

        private string _服药依从性;
        /// <summary>
        /// 获取或设置 服药依从性 的值
        /// </summary>
        public string 服药依从性
        {
            get { return _服药依从性; }
            set { _服药依从性 = value; }
        }

        private string _创建时间;
        /// <summary>
        /// 获取或设置 创建时间 的值
        /// </summary>
        public string 创建时间
        {
            get { return _创建时间; }
            set { _创建时间 = value; }
        }

        public string BlueName { get; set; }

        public string BlueAddr { get; set; }

        public string BlueType { get; set; }
    }
}
