USE [master]
GO
/****** Object:  Database [YSDB_Leave]    Script Date: 04/25/2016 11:58:03 ******/
CREATE DATABASE [YSDB_Leave] ON  PRIMARY 
( NAME = N'YSDB_Leave', FILENAME = N'G:\数据库备份\YSDB_Leave\YSDB_Leave.mdf' , SIZE = 80896KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'YSDB_Leave_log', FILENAME = N'G:\数据库备份\YSDB_Leave\YSDB_Leave_log.ldf' , SIZE = 199296KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [YSDB_Leave].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [YSDB_Leave] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [YSDB_Leave] SET ANSI_NULLS OFF
GO
ALTER DATABASE [YSDB_Leave] SET ANSI_PADDING OFF
GO
ALTER DATABASE [YSDB_Leave] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [YSDB_Leave] SET ARITHABORT OFF
GO
ALTER DATABASE [YSDB_Leave] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [YSDB_Leave] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [YSDB_Leave] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [YSDB_Leave] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [YSDB_Leave] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [YSDB_Leave] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [YSDB_Leave] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [YSDB_Leave] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [YSDB_Leave] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [YSDB_Leave] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [YSDB_Leave] SET  DISABLE_BROKER
GO
ALTER DATABASE [YSDB_Leave] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [YSDB_Leave] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [YSDB_Leave] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [YSDB_Leave] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [YSDB_Leave] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [YSDB_Leave] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [YSDB_Leave] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [YSDB_Leave] SET  READ_WRITE
GO
ALTER DATABASE [YSDB_Leave] SET RECOVERY FULL
GO
ALTER DATABASE [YSDB_Leave] SET  MULTI_USER
GO
ALTER DATABASE [YSDB_Leave] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [YSDB_Leave] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'YSDB_Leave', N'ON'
GO
USE [YSDB_Leave]
GO
/****** Object:  Table [dbo].[tb_老年人中医药特征管理]    Script Date: 04/25/2016 11:58:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_老年人中医药特征管理](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[个人档案编号] [varchar](20) NULL,
	[身份证号] [varchar](18) NOT NULL,
	[姓名] [nvarchar](20) NOT NULL,
	[性别] [nvarchar](2) NOT NULL,
	[出生日期] [varchar](20) NULL,
	[居住地址] [nvarchar](200) NULL,
	[创建机构] [varchar](20) NULL,
	[创建人] [varchar](20) NULL,
	[修改人] [varchar](20) NULL,
	[所属机构] [varchar](20) NULL,
	[创建时间] [varchar](20) NULL,
	[修改时间] [varchar](20) NULL,
	[特征1] [int] NULL,
	[特征2] [int] NULL,
	[特征3] [int] NULL,
	[特征4] [int] NULL,
	[特征5] [int] NULL,
	[特征6] [int] NULL,
	[特征7] [int] NULL,
	[特征8] [int] NULL,
	[特征9] [int] NULL,
	[特征10] [int] NULL,
	[特征11] [int] NULL,
	[特征12] [int] NULL,
	[特征13] [int] NULL,
	[特征14] [int] NULL,
	[特征15] [int] NULL,
	[特征16] [int] NULL,
	[特征17] [int] NULL,
	[特征18] [int] NULL,
	[特征19] [int] NULL,
	[特征20] [int] NULL,
	[特征21] [int] NULL,
	[特征22] [int] NULL,
	[特征23] [int] NULL,
	[特征24] [int] NULL,
	[特征25] [int] NULL,
	[特征26] [int] NULL,
	[特征27] [int] NULL,
	[特征28] [int] NULL,
	[特征29] [int] NULL,
	[特征30] [int] NULL,
	[特征31] [int] NULL,
	[特征32] [int] NULL,
	[特征33] [int] NULL,
	[气虚质得分] [int] NULL,
	[气虚质辨识] [varchar](2) NULL,
	[气虚质指导] [varchar](20) NULL,
	[气虚质其他] [varchar](200) NULL,
	[阳虚质得分] [int] NULL,
	[阳虚质辨识] [varchar](2) NULL,
	[阳虚质指导] [varchar](20) NULL,
	[阳虚质其他] [varchar](200) NULL,
	[阴虚质得分] [int] NULL,
	[阴虚质辨识] [varchar](2) NULL,
	[阴虚质指导] [varchar](20) NULL,
	[阴虚质其他] [varchar](200) NULL,
	[痰湿质得分] [int] NULL,
	[痰湿质辨识] [varchar](2) NULL,
	[痰湿质指导] [varchar](20) NULL,
	[痰湿质其他] [varchar](200) NULL,
	[湿热质得分] [int] NULL,
	[湿热质辨识] [varchar](2) NULL,
	[湿热质指导] [varchar](20) NULL,
	[湿热质其他] [varchar](200) NULL,
	[血瘀质得分] [int] NULL,
	[血瘀质辨识] [varchar](2) NULL,
	[血瘀质指导] [varchar](20) NULL,
	[血瘀质其他] [varchar](200) NULL,
	[气郁质得分] [int] NULL,
	[气郁质辨识] [varchar](2) NULL,
	[气郁质指导] [varchar](20) NULL,
	[气郁质其他] [varchar](200) NULL,
	[特禀质得分] [int] NULL,
	[特禀质辨识] [varchar](2) NULL,
	[特禀质指导] [varchar](20) NULL,
	[特禀质其他] [varchar](200) NULL,
	[平和质得分] [int] NULL,
	[平和质辨识] [varchar](2) NULL,
	[平和质指导] [varchar](20) NULL,
	[平和质其他] [varchar](200) NULL,
	[发生时间] [varchar](20) NOT NULL,
	[医生签名] [varchar](20) NULL,
	[RowState] [varchar](2) NULL,
	[BlueAddr] [varchar](50) NULL,
	[BlueType] [varchar](20) NULL,
	[BlueName] [varchar](20) NULL,
 CONSTRAINT [PK_tb_老年人中医药特征管理] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_老年人生活自理能力评价]    Script Date: 04/25/2016 11:58:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_老年人生活自理能力评价](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[身份证号] [varchar](20) NOT NULL,
	[姓名] [varchar](20) NOT NULL,
	[性别] [varchar](2) NOT NULL,
	[出生日期] [varchar](20) NOT NULL,
	[居住地址] [nvarchar](200) NOT NULL,
	[个人档案编号] [varchar](20) NULL,
	[G_ZZBS] [varchar](50) NULL,
	[G_ZZXZZ] [varchar](50) NULL,
	[G_ZZCX] [varchar](50) NULL,
	[G_ZZZZ] [varchar](50) NULL,
	[G_ZTH] [varchar](50) NULL,
	[G_ZTYY] [varchar](50) NULL,
	[G_ZTZD] [varchar](50) NULL,
	[G_ZTZZ] [varchar](50) NULL,
	[G_SFTZ] [decimal](5, 2) NULL,
	[G_RXY] [int] NULL,
	[G_RYJ] [decimal](5, 2) NULL,
	[G_ZYD] [int] NULL,
	[G_YDSJ] [decimal](5, 2) NULL,
	[G_YS] [varchar](20) NULL,
	[G_XLTZ] [varchar](100) NULL,
	[G_ZYXW] [varchar](20) NULL,
	[G_YMJZ] [varchar](100) NULL,
	[G_GXBYF] [varchar](100) NULL,
	[G_GZSSYF] [varchar](100) NULL,
	[下次随访目标] [varchar](100) NULL,
	[下次随访日期] [varchar](80) NULL,
	[随访医生] [varchar](20) NULL,
	[创建机构] [varchar](20) NULL,
	[创建人] [varchar](20) NULL,
	[更新人] [varchar](20) NULL,
	[所属机构] [varchar](20) NULL,
	[创建时间] [varchar](20) NULL,
	[更新时间] [varchar](20) NULL,
	[随访日期] [varchar](20) NULL,
	[缺项] [varchar](4) NULL,
	[进餐评分] [varchar](20) NULL,
	[梳洗评分] [varchar](20) NULL,
	[如厕评分] [varchar](20) NULL,
	[活动评分] [varchar](20) NULL,
	[总评分] [varchar](20) NULL,
	[穿衣评分] [varchar](20) NULL,
	[随访次数] [varchar](20) NULL,
	[完整度] [varchar](20) NULL,
	[RowState] [varchar](2) NULL,
	[BlueAddr] [varchar](50) NULL,
	[BlueType] [varchar](20) NULL,
	[BlueName] [varchar](20) NULL,
 CONSTRAINT [PK_tb_老年人随访] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_蓝牙配置]    Script Date: 04/25/2016 11:58:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_蓝牙配置](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[蓝牙名称] [nvarchar](50) NOT NULL,
	[类别] [varchar](10) NOT NULL,
 CONSTRAINT [PK_tb_蓝牙配置] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_健康体检_住院史]    Script Date: 04/25/2016 11:58:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_健康体检_住院史](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[身份证号] [varchar](20) NULL,
	[个人档案编号] [varchar](20) NULL,
	[类型] [varchar](20) NULL,
	[入院日期] [varchar](20) NULL,
	[出院日期] [varchar](20) NULL,
	[原因] [varchar](200) NULL,
	[医疗机构名称] [varchar](100) NULL,
	[病案号] [varchar](40) NULL,
	[创建日期] [varchar](40) NULL,
	[RowState] [varchar](2) NULL,
	[BlueAddr] [varchar](50) NULL,
	[BlueType] [varchar](20) NULL,
	[BlueName] [varchar](20) NULL,
 CONSTRAINT [PK_tb_健康体检_住院史] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_健康体检_用药情况表]    Script Date: 04/25/2016 11:58:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_健康体检_用药情况表](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[身份证号] [varchar](20) NULL,
	[个人档案编号] [varchar](20) NULL,
	[药物名称] [varchar](100) NULL,
	[用法] [varchar](100) NULL,
	[用量] [varchar](20) NULL,
	[用药时间] [varchar](20) NULL,
	[服药依从性] [varchar](100) NULL,
	[创建时间] [varchar](40) NULL,
	[RowState] [varchar](2) NULL,
	[BlueAddr] [varchar](50) NULL,
	[BlueType] [varchar](20) NULL,
	[BlueName] [varchar](20) NULL,
 CONSTRAINT [PK_tb_健康体检_用药情况表] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_健康体检_非免疫规划预防接种史]    Script Date: 04/25/2016 11:58:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_健康体检_非免疫规划预防接种史](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[身份证号] [varchar](20) NULL,
	[个人档案编号] [varchar](20) NULL,
	[接种名称] [varchar](100) NULL,
	[接种日期] [varchar](20) NULL,
	[接种机构] [varchar](40) NULL,
	[创建日期] [varchar](40) NULL,
	[RowState] [varchar](2) NULL,
	[BlueAddr] [varchar](50) NULL,
	[BlueType] [varchar](20) NULL,
	[BlueName] [varchar](20) NULL,
 CONSTRAINT [PK_tb_健康体检_非免疫规划预防接种史] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_健康体检]    Script Date: 04/25/2016 11:58:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_健康体检](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[身份证号] [varchar](18) NOT NULL,
	[姓名] [nvarchar](20) NOT NULL,
	[性别] [nvarchar](2) NOT NULL,
	[出生日期] [varchar](20) NULL,
	[症状] [varchar](150) NULL,
	[体温] [decimal](5, 2) NULL,
	[呼吸] [int] NULL,
	[脉搏] [int] NULL,
	[血压右侧1] [varchar](20) NULL,
	[血压右侧2] [varchar](20) NULL,
	[血压左侧1] [varchar](20) NULL,
	[血压左侧2] [varchar](20) NULL,
	[身高] [decimal](5, 2) NULL,
	[腰围] [decimal](5, 2) NULL,
	[体重] [decimal](5, 2) NULL,
	[体重指数] [varchar](20) NULL,
	[老年人认知] [varchar](20) NULL,
	[老年人情感] [varchar](20) NULL,
	[老年人认知分] [varchar](20) NULL,
	[老年人情感分] [varchar](20) NULL,
	[左眼视力] [decimal](5, 2) NULL,
	[右眼视力] [decimal](5, 2) NULL,
	[左眼矫正] [varchar](20) NULL,
	[右眼矫正] [varchar](20) NULL,
	[听力] [varchar](150) NULL,
	[运动功能] [varchar](150) NULL,
	[皮肤] [varchar](20) NULL,
	[淋巴结] [varchar](20) NULL,
	[淋巴结其他] [varchar](150) NULL,
	[桶状胸] [varchar](20) NULL,
	[呼吸音] [varchar](20) NULL,
	[呼吸音异常] [varchar](150) NULL,
	[罗音] [varchar](20) NULL,
	[罗音异常] [varchar](150) NULL,
	[心率] [int] NULL,
	[心律] [varchar](20) NULL,
	[杂音] [varchar](20) NULL,
	[杂音有] [varchar](150) NULL,
	[压痛] [varchar](20) NULL,
	[压痛有] [varchar](150) NULL,
	[包块] [varchar](20) NULL,
	[包块有] [varchar](150) NULL,
	[肝大] [varchar](20) NULL,
	[肝大有] [varchar](150) NULL,
	[脾大] [varchar](20) NULL,
	[脾大有] [varchar](150) NULL,
	[浊音] [varchar](20) NULL,
	[浊音有] [varchar](150) NULL,
	[下肢水肿] [varchar](20) NULL,
	[肛门指诊] [varchar](20) NULL,
	[肛门指诊异常] [varchar](150) NULL,
	[G_QLX] [varchar](20) NULL,
	[查体其他] [varchar](150) NULL,
	[白细胞] [varchar](20) NULL,
	[血红蛋白] [varchar](20) NULL,
	[血小板] [varchar](20) NULL,
	[血常规其他] [varchar](150) NULL,
	[尿蛋白] [varchar](20) NULL,
	[尿糖] [varchar](20) NULL,
	[尿酮体] [varchar](20) NULL,
	[尿潜血] [varchar](20) NULL,
	[尿常规其他] [varchar](150) NULL,
	[大便潜血] [varchar](20) NULL,
	[血清谷丙转氨酶] [varchar](20) NULL,
	[血清谷草转氨酶] [varchar](20) NULL,
	[白蛋白] [varchar](20) NULL,
	[总胆红素] [varchar](20) NULL,
	[结合胆红素] [varchar](20) NULL,
	[血清肌酐] [varchar](20) NULL,
	[血尿素氮] [varchar](20) NULL,
	[总胆固醇] [varchar](20) NULL,
	[甘油三酯] [varchar](20) NULL,
	[血清低密度脂蛋白胆固醇] [varchar](20) NULL,
	[血清高密度脂蛋白胆固醇] [varchar](20) NULL,
	[空腹血糖] [varchar](20) NULL,
	[乙型肝炎表面抗原] [varchar](20) NULL,
	[眼底] [varchar](8) NULL,
	[眼底异常] [varchar](150) NULL,
	[心电图] [varchar](8) NULL,
	[心电图异常] [varchar](300) NULL,
	[胸部X线片] [varchar](8) NULL,
	[胸部X线片异常] [varchar](300) NULL,
	[B超] [varchar](8) NULL,
	[B超其他] [varchar](300) NULL,
	[辅助检查其他] [varchar](150) NULL,
	[G_JLJJY] [varchar](150) NULL,
	[个人档案编号] [varchar](20) NULL,
	[创建机构] [varchar](20) NULL,
	[创建时间] [varchar](20) NULL,
	[创建人] [varchar](20) NULL,
	[修改时间] [varchar](20) NULL,
	[修改人] [varchar](20) NULL,
	[体检日期] [varchar](20) NULL,
	[所属机构] [varchar](20) NULL,
	[FIELD1] [varchar](20) NULL,
	[FIELD2] [varchar](20) NULL,
	[FIELD3] [varchar](20) NULL,
	[FIELD4] [varchar](20) NULL,
	[WBC_SUP] [int] NULL,
	[PLT_SUP] [int] NULL,
	[G_TUNWEI] [varchar](20) NULL,
	[G_YTWBZ] [varchar](20) NULL,
	[锻炼频率] [varchar](20) NULL,
	[每次锻炼时间] [varchar](20) NULL,
	[坚持锻炼时间] [varchar](20) NULL,
	[锻炼方式] [varchar](150) NULL,
	[饮食习惯] [varchar](20) NULL,
	[吸烟状况] [varchar](20) NULL,
	[日吸烟量] [varchar](20) NULL,
	[开始吸烟年龄] [varchar](20) NULL,
	[戒烟年龄] [varchar](20) NULL,
	[饮酒频率] [varchar](20) NULL,
	[日饮酒量] [varchar](20) NULL,
	[是否戒酒] [varchar](20) NULL,
	[戒酒年龄] [varchar](20) NULL,
	[开始饮酒年龄] [varchar](20) NULL,
	[近一年内是否曾醉酒] [varchar](20) NULL,
	[饮酒种类] [varchar](20) NULL,
	[饮酒种类其它] [varchar](20) NULL,
	[有无职业病] [varchar](20) NULL,
	[具体职业] [varchar](20) NULL,
	[从业时间] [varchar](20) NULL,
	[化学物质] [varchar](150) NULL,
	[化学物质防护] [varchar](20) NULL,
	[化学物质具体防护] [varchar](150) NULL,
	[毒物] [varchar](150) NULL,
	[G_DWFHCS] [varchar](20) NULL,
	[G_DWFHCSQT] [varchar](150) NULL,
	[放射物质] [varchar](150) NULL,
	[放射物质防护措施有无] [varchar](20) NULL,
	[放射物质防护措施其他] [varchar](150) NULL,
	[口唇] [varchar](20) NULL,
	[齿列] [varchar](20) NULL,
	[咽部] [varchar](20) NULL,
	[皮肤其他] [varchar](150) NULL,
	[巩膜] [varchar](20) NULL,
	[巩膜其他] [varchar](150) NULL,
	[足背动脉搏动] [varchar](20) NULL,
	[乳腺] [varchar](100) NULL,
	[乳腺其他] [varchar](150) NULL,
	[外阴] [varchar](20) NULL,
	[外阴异常] [varchar](150) NULL,
	[阴道] [varchar](20) NULL,
	[阴道异常] [varchar](150) NULL,
	[宫颈] [varchar](20) NULL,
	[宫颈异常] [varchar](150) NULL,
	[宫体] [varchar](20) NULL,
	[宫体异常] [varchar](150) NULL,
	[附件] [varchar](20) NULL,
	[附件异常] [varchar](150) NULL,
	[血钾浓度] [varchar](20) NULL,
	[血钠浓度] [varchar](20) NULL,
	[糖化血红蛋白] [varchar](20) NULL,
	[宫颈涂片] [varchar](20) NULL,
	[宫颈涂片异常] [varchar](300) NULL,
	[平和质] [varchar](20) NULL,
	[气虚质] [varchar](20) NULL,
	[阳虚质] [varchar](20) NULL,
	[阴虚质] [varchar](20) NULL,
	[痰湿质] [varchar](20) NULL,
	[湿热质] [varchar](20) NULL,
	[血瘀质] [varchar](20) NULL,
	[气郁质] [varchar](20) NULL,
	[特禀质] [varchar](20) NULL,
	[脑血管疾病] [varchar](20) NULL,
	[脑血管疾病其他] [varchar](150) NULL,
	[肾脏疾病] [varchar](20) NULL,
	[肾脏疾病其他] [varchar](150) NULL,
	[心脏疾病] [varchar](20) NULL,
	[心脏疾病其他] [varchar](150) NULL,
	[血管疾病] [varchar](20) NULL,
	[血管疾病其他] [varchar](150) NULL,
	[眼部疾病] [varchar](20) NULL,
	[眼部疾病其他] [varchar](150) NULL,
	[神经系统疾病] [varchar](20) NULL,
	[神经系统疾病其他] [varchar](150) NULL,
	[其他系统疾病] [varchar](20) NULL,
	[其他系统疾病其他] [varchar](150) NULL,
	[健康评价] [varchar](20) NULL,
	[健康评价异常1] [varchar](150) NULL,
	[健康评价异常2] [varchar](150) NULL,
	[健康评价异常3] [varchar](150) NULL,
	[健康评价异常4] [varchar](150) NULL,
	[健康指导] [varchar](20) NULL,
	[危险因素控制] [varchar](100) NULL,
	[危险因素控制体重] [varchar](150) NULL,
	[危险因素控制疫苗] [varchar](150) NULL,
	[危险因素控制其他] [varchar](150) NULL,
	[FIELD5] [varchar](20) NULL,
	[症状其他] [varchar](40) NULL,
	[G_XYYC] [varchar](20) NULL,
	[G_XYZC] [varchar](20) NULL,
	[G_QTZHZH] [varchar](20) NULL,
	[缺项] [varchar](4) NULL,
	[口唇其他] [varchar](150) NULL,
	[齿列其他] [varchar](150) NULL,
	[咽部其他] [varchar](150) NULL,
	[YDGNQT] [varchar](50) NULL,
	[餐后2H血糖] [varchar](20) NULL,
	[老年人状况评估] [varchar](50) NULL,
	[老年人自理评估] [varchar](50) NULL,
	[粉尘] [varchar](50) NULL,
	[物理因素] [varchar](50) NULL,
	[职业病其他] [varchar](50) NULL,
	[粉尘防护有无] [varchar](4) NULL,
	[物理防护有无] [varchar](4) NULL,
	[其他防护有无] [varchar](4) NULL,
	[粉尘防护措施] [varchar](50) NULL,
	[物理防护措施] [varchar](50) NULL,
	[其他防护措施] [varchar](50) NULL,
	[TNBFXJF] [varchar](10) NULL,
	[左侧原因] [varchar](20) NULL,
	[右侧原因] [varchar](20) NULL,
	[尿微量白蛋白] [varchar](20) NULL,
	[完整度] [varchar](20) NULL,
	[齿列缺齿] [varchar](100) NULL,
	[齿列龋齿] [varchar](100) NULL,
	[齿列义齿] [varchar](100) NULL,
	[RowState] [varchar](2) NULL,
	[BlueAddr] [varchar](50) NULL,
	[BlueType] [varchar](20) NULL,
	[BlueName] [varchar](20) NULL,
 CONSTRAINT [PK_tb_健康体检] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_机构信息]    Script Date: 04/25/2016 11:58:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_机构信息](
	[ID] [bigint] NOT NULL,
	[机构编号] [varchar](20) NOT NULL,
	[机构名称] [varchar](150) NULL,
	[P_RGID_MARKER] [varchar](20) NULL,
	[负责人] [varchar](20) NULL,
	[联系人] [varchar](20) NULL,
	[联系电话] [varchar](20) NULL,
	[上级机构] [varchar](50) NULL,
	[机构级别] [varchar](20) NULL,
	[创建人] [varchar](20) NULL,
	[创建时间] [datetime] NULL,
	[状态] [varchar](4) NULL,
	[P_QYDW] [varchar](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_导出用户]    Script Date: 04/25/2016 11:58:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_导出用户](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[身份证号] [varchar](20) NULL,
	[姓名] [varchar](300) NULL,
	[性别] [varchar](2) NULL,
	[居住地址] [nvarchar](200) NULL,
	[出生日期] [varchar](20) NULL,
	[个人档案编号] [varchar](20) NULL,
	[所属机构] [varchar](20) NULL,
 CONSTRAINT [PK_tb_导出用户] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_常用字典]    Script Date: 04/25/2016 11:58:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_常用字典](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[P_FUN_CODE] [varchar](20) NULL,
	[P_FUN_DESC] [varchar](80) NULL,
	[P_CODE] [varchar](20) NULL,
	[P_DESC_CODE] [varchar](20) NULL,
	[P_DESC] [varchar](80) NULL,
	[P_BEIZHU] [varchar](80) NULL,
	[P_FLAG] [int] NULL,
 CONSTRAINT [PK_tb_常用字典] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_MyUser]    Script Date: 04/25/2016 11:58:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_MyUser](
	[isid] [int] NOT NULL,
	[Account] [nvarchar](30) NOT NULL,
	[NovellAccount] [nvarchar](100) NULL,
	[DomainName] [nvarchar](100) NULL,
	[用户编码] [varchar](50) NULL,
	[UserName] [nvarchar](20) NOT NULL,
	[Address] [nvarchar](50) NULL,
	[Tel] [nvarchar](50) NULL,
	[Email] [nvarchar](40) NULL,
	[Password] [nvarchar](100) NULL,
	[LastLoginTime] [datetime] NULL,
	[LastLogoutTime] [datetime] NULL,
	[IsLocked] [smallint] NULL,
	[CreateTime] [datetime] NULL,
	[FlagAdmin] [nchar](1) NULL,
	[FlagOnline] [nchar](1) NULL,
	[LoginCounter] [int] NULL,
	[DataSets] [varchar](250) NULL,
	[所属机构] [varchar](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[USP_健康体检]    Script Date: 04/25/2016 11:58:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_健康体检]
	@身份证号 VARCHAR(18) , -- 
  @姓名 NVARCHAR(40) , -- 
  @性别 NVARCHAR(4) , -- 
  @出生日期 VARCHAR(20) , -- 
  @症状 VARCHAR(150) =  '1' , --无症状 
  @体温 DECIMAL(5) =  NULL , -- 
  @呼吸 INT =  NULL  , -- 
  @脉搏 INT  =  NULL , -- 
  @血压右侧1 VARCHAR(20)=  NULL  , -- 
  @血压右侧2 VARCHAR(20)=  NULL  , -- 
  @血压左侧1 VARCHAR(20)=  NULL  , -- 
  @血压左侧2 VARCHAR(20)=  NULL  , -- 
  @身高 DECIMAL(5)=  NULL  , -- 
  @腰围 DECIMAL(5)=  NULL  , -- 
  @体重 DECIMAL(5)=  NULL  , -- 
  @体重指数 VARCHAR(20) =  NULL , -- 
  @老年人认知 VARCHAR(20)=  NULL  , -- 
  @老年人情感 VARCHAR(20)=  NULL  , -- 
  @老年人认知分 VARCHAR(20) =  NULL , -- 
  @老年人情感分 VARCHAR(20) =  NULL , -- 
  @左眼视力 DECIMAL(5)=  NULL  , -- 
  @右眼视力 DECIMAL(5) =  NULL , -- 
  @左眼矫正 VARCHAR(20) =  NULL , -- 
  @右眼矫正 VARCHAR(20) =  NULL , -- 
  @听力 VARCHAR(150) =  '1' , -- 
  @运动功能 VARCHAR(150) =  '1' , -- 
  @皮肤 VARCHAR(20) =  '1' , -- 
  @淋巴结 VARCHAR(20) =  '1' , -- 
  @淋巴结其他 VARCHAR(150) =  NULL , -- 
  @桶状胸 VARCHAR(20)=  '2'  , -- 
  @呼吸音 VARCHAR(20)=  '1'  , -- 
  @呼吸音异常 VARCHAR(150)=  NULL  , -- 
  @罗音 VARCHAR(20)=  '1'  , -- 
  @罗音异常 VARCHAR(150) =  NULL , -- 
  @心率 INT =  NULL , -- 
  @心律 VARCHAR(20)=  '1'  , -- 
  @杂音 VARCHAR(20)=  '1'  , -- 
  @杂音有 VARCHAR(150) =  NULL , -- 
  @压痛 VARCHAR(20)=  '1'  , -- 
  @压痛有 VARCHAR(150) =  NULL , -- 
  @包块 VARCHAR(20) =  '1' , -- 
  @包块有 VARCHAR(150) =  NULL , -- 
  @肝大 VARCHAR(20)=  '1'  , -- 
  @肝大有 VARCHAR(150)=  NULL  , -- 
  @脾大 VARCHAR(20)=  '1'  , -- 
  @脾大有 VARCHAR(150) =  NULL , -- 
  @浊音 VARCHAR(20) =  '1' , -- 
  @浊音有 VARCHAR(150) =  NULL , -- 
  @下肢水肿 VARCHAR(20)=  '1'  , -- 
  @肛门指诊 VARCHAR(20) =  NULL , -- 
  @肛门指诊异常 VARCHAR(150)=  NULL  , -- 
  @G_QLX VARCHAR(20)=  NULL  , -- 
  @查体其他 VARCHAR(150)=  NULL  , -- 
  @白细胞 VARCHAR(20) =  NULL , -- 
  @血红蛋白 VARCHAR(20)=  NULL  , -- 
  @血小板 VARCHAR(20)=  NULL  , -- 
  @血常规其他 VARCHAR(150)=  NULL  , -- 
  @尿蛋白 VARCHAR(20)=  NULL  , -- 
  @尿糖 VARCHAR(20) =  NULL , -- 
  @尿酮体 VARCHAR(20)=  NULL  , -- 
  @尿潜血 VARCHAR(20)=  NULL  , -- 
  @尿常规其他 VARCHAR(150)=  NULL  , -- 
  @大便潜血 VARCHAR(20)=  NULL  , -- 
  @血清谷丙转氨酶 VARCHAR(20)=  NULL , -- 
  @血清谷草转氨酶 VARCHAR(20)=  NULL , -- 
  @白蛋白 VARCHAR(20)=  NULL  , -- 
  @总胆红素 VARCHAR(20)=  NULL  , -- 
  @结合胆红素 VARCHAR(20)=  NULL  , -- 
  @血清肌酐 VARCHAR(20)=  NULL  , -- 
  @血尿素氮 VARCHAR(20)=  NULL  , -- 
  @总胆固醇 VARCHAR(20)=  NULL  , -- 
  @甘油三酯 VARCHAR(20)=  NULL  , -- 
  @血清低密度脂蛋白胆固醇 VARCHAR(20)=  NULL , -- 
  @血清高密度脂蛋白胆固醇 VARCHAR(20)=  NULL , -- 
  @空腹血糖 VARCHAR(20)=  NULL  , -- 
  @乙型肝炎表面抗原 VARCHAR(20)=  NULL  , -- 
  @眼底 VARCHAR(8) =  NULL , -- 
  @眼底异常 VARCHAR(150)=  NULL  , -- 
  @心电图 VARCHAR(8)=  NULL  , -- 
  @心电图异常 VARCHAR(300) =  NULL , -- 
  @胸部X线片 VARCHAR(8) =  NULL , -- 
  @胸部X线片异常 VARCHAR(300) =  NULL , -- 
  @B超 VARCHAR(8) =  NULL , -- 
  @B超其他 VARCHAR(300)=  NULL  , -- 
  @辅助检查其他 VARCHAR(150)=  NULL  , -- 
  @G_JLJJY VARCHAR(150) =  NULL , -- 
  @个人档案编号 VARCHAR(20)=  NULL  , -- 
  @创建机构 VARCHAR(20) =  NULL , -- 
  @创建时间 VARCHAR(20) =  NULL , -- 
  @创建人 VARCHAR(20) =  NULL , -- 
  @修改时间 VARCHAR(20)=  NULL  , -- 
  @修改人 VARCHAR(20)=  NULL  , -- 
  @体检日期 VARCHAR(20) =  NULL , -- 
  @所属机构 VARCHAR(20)=  NULL  , -- 
  @FIELD1 VARCHAR(20) =  NULL , -- 
  @FIELD2 VARCHAR(20) =  NULL , -- 
  @FIELD3 VARCHAR(20) =  NULL , -- 
  @FIELD4 VARCHAR(20) =  NULL , -- 
  @WBC_SUP INT =  NULL , -- 
  @PLT_SUP INT =  NULL , -- 
  @G_TUNWEI VARCHAR(20) =  NULL , -- 
  @G_YTWBZ VARCHAR(20)=  NULL  , -- 
  @锻炼频率 VARCHAR(20) =  '4' , -- 
  @每次锻炼时间 VARCHAR(20)=  NULL  , -- 
  @坚持锻炼时间 VARCHAR(20)=  NULL  , -- 
  @锻炼方式 VARCHAR(150)=  NULL  , -- 
  @饮食习惯 VARCHAR(20)=  '1' , -- 
  @吸烟状况 VARCHAR(20)=  '1' , -- 
  @日吸烟量 VARCHAR(20)=  NULL , -- 
  @开始吸烟年龄 VARCHAR(20) =  NULL , -- 
  @戒烟年龄 VARCHAR(20) =  NULL , -- 
  @饮酒频率 VARCHAR(20) =  '1' , -- 
  @日饮酒量 VARCHAR(20) =  NULL , -- 
  @是否戒酒 VARCHAR(20) =  '1' , -- 
  @戒酒年龄 VARCHAR(20)=  NULL  , -- 
  @开始饮酒年龄 VARCHAR(20)=  NULL  , -- 
  @近一年内是否曾醉酒 VARCHAR(20) =  NULL , -- 
  @饮酒种类 VARCHAR(20) =  NULL , -- 
  @饮酒种类其它 VARCHAR(20)=  NULL  , -- 
  @有无职业病 VARCHAR(20)=  '1'  , -- 
  @具体职业 VARCHAR(20) =  NULL , -- 
  @从业时间 VARCHAR(20) =  NULL , -- 
  @化学物质 VARCHAR(150) =  NULL , -- 
  @化学物质防护 VARCHAR(20) =  NULL , -- 
  @化学物质具体防护 VARCHAR(150)=  NULL  , -- 
  @毒物 VARCHAR(150) =  NULL , -- 
  @G_DWFHCS VARCHAR(20) =  NULL , -- 
  @G_DWFHCSQT VARCHAR(150) =  NULL , -- 
  @放射物质 VARCHAR(150) =  NULL , -- 
  @放射物质防护措施有无 VARCHAR(20)=  NULL  , -- 
  @放射物质防护措施其他 VARCHAR(150) =  NULL , -- 
  @口唇 VARCHAR(20)=  '1' , -- 
  @齿列 VARCHAR(20)=  '1' , -- 
  @咽部 VARCHAR(20)=  '1' , -- 
  @皮肤其他 VARCHAR(150)=  NULL  , -- 
  @巩膜 VARCHAR(20) =  '1' , -- 
  @巩膜其他 VARCHAR(150)=  NULL  , -- 
  @足背动脉搏动 VARCHAR(20)=  '2'  , -- 
  @乳腺 VARCHAR(100)=  NULL  , -- 
  @乳腺其他 VARCHAR(150)=  NULL  , -- 
  @外阴 VARCHAR(20) =  NULL , -- 
  @外阴异常 VARCHAR(150)=  NULL  , -- 
  @阴道 VARCHAR(20)=  NULL  , -- 
  @阴道异常 VARCHAR(150) =  NULL , -- 
  @宫颈 VARCHAR(20)=  NULL  , -- 
  @宫颈异常 VARCHAR(150)=  NULL  , -- 
  @宫体 VARCHAR(20) =  NULL , -- 
  @宫体异常 VARCHAR(150) =  NULL , -- 
  @附件 VARCHAR(20) =  NULL , -- 
  @附件异常 VARCHAR(150)=  NULL  , -- 
  @血钾浓度 VARCHAR(20)=  NULL  , -- 
  @血钠浓度 VARCHAR(20)=  NULL  , -- 
  @糖化血红蛋白 VARCHAR(20) =  NULL , -- 
  @宫颈涂片 VARCHAR(20) =  NULL , -- 
  @宫颈涂片异常 VARCHAR(300) =  NULL , -- 
  @平和质 VARCHAR(20)=  NULL , -- 
  @气虚质 VARCHAR(20)=  NULL , -- 
  @阳虚质 VARCHAR(20)=  NULL , -- 
  @阴虚质 VARCHAR(20)=  NULL , -- 
  @痰湿质 VARCHAR(20)=  NULL , -- 
  @湿热质 VARCHAR(20)=  NULL , -- 
  @血瘀质 VARCHAR(20)=  NULL , -- 
  @气郁质 VARCHAR(20)=  NULL , -- 
  @特禀质 VARCHAR(20)=  NULL , -- 
  @脑血管疾病 VARCHAR(20) =  '1' , -- 
  @脑血管疾病其他 VARCHAR(150) =  NULL , -- 
  @肾脏疾病 VARCHAR(20)=  '1'  , -- 
  @肾脏疾病其他 VARCHAR(150)=  NULL  , -- 
  @心脏疾病 VARCHAR(20) =  '1' , -- 
  @心脏疾病其他 VARCHAR(150)=  NULL  , -- 
  @血管疾病 VARCHAR(20) =  '1' , -- 
  @血管疾病其他 VARCHAR(150) =  NULL , -- 
  @眼部疾病 VARCHAR(20) =  '1' , -- 
  @眼部疾病其他 VARCHAR(150) =  NULL , -- 
  @神经系统疾病 VARCHAR(20) =  '1' , -- 
  @神经系统疾病其他 VARCHAR(150) =  NULL , -- 
  @其他系统疾病 VARCHAR(20) =  '1' , -- 
  @其他系统疾病其他 VARCHAR(150)=  NULL  , -- 
  @健康评价 VARCHAR(20) =  '1' , -- 
  @健康评价异常1 VARCHAR(150)=  NULL  , -- 
  @健康评价异常2 VARCHAR(150)=  NULL  , -- 
  @健康评价异常3 VARCHAR(150)=  NULL  , -- 
  @健康评价异常4 VARCHAR(150)=  NULL  , -- 
  @健康指导 VARCHAR(20) =  NULL , -- 
  @危险因素控制 VARCHAR(100)=  NULL  , -- 
  @危险因素控制体重 VARCHAR(150)=  NULL  , -- 
  @危险因素控制疫苗 VARCHAR(150)=  NULL  , -- 
  @危险因素控制其他 VARCHAR(150)=  NULL  , -- 
  @FIELD5 VARCHAR(20) =  NULL , -- 
  @症状其他 VARCHAR(40)=  NULL  , -- 
  @G_XYYC VARCHAR(20) =  NULL , -- 
  @G_XYZC VARCHAR(20) =  NULL , -- 
  @G_QTZHZH VARCHAR(20)=  NULL  , -- 
  @缺项 VARCHAR(4) =  NULL , -- 
  @口唇其他 VARCHAR(150)=  NULL , -- 
  @齿列其他 VARCHAR(150)=  NULL , -- 
  @咽部其他 VARCHAR(150)=  NULL , -- 
  @YDGNQT VARCHAR(50)=  NULL  , -- 
  @餐后2H血糖 VARCHAR(20)=  NULL  , -- 
  @老年人状况评估 VARCHAR(50)=  NULL  , -- 
  @老年人自理评估 VARCHAR(50)=  NULL  , -- 
  @粉尘 VARCHAR(50) =  NULL , -- 
  @物理因素 VARCHAR(50)=  NULL  , -- 
  @职业病其他 VARCHAR(50) =  NULL , -- 
  @粉尘防护有无 VARCHAR(4)=  NULL  , -- 
  @物理防护有无 VARCHAR(4)=  NULL  , -- 
  @其他防护有无 VARCHAR(4)=  NULL  , -- 
  @粉尘防护措施 VARCHAR(50)=  NULL , -- 
  @物理防护措施 VARCHAR(50)=  NULL , -- 
  @其他防护措施 VARCHAR(50)=  NULL , -- 
  @TNBFXJF VARCHAR(10) =  NULL , -- 
  @左侧原因 VARCHAR(20)=  NULL  , -- 
  @右侧原因 VARCHAR(20)=  NULL  , -- 
  @尿微量白蛋白 VARCHAR(20) =  NULL , -- 
  @完整度 VARCHAR(20) =  NULL , -- 
  @齿列缺齿 VARCHAR(100)=  NULL  , -- 
  @齿列龋齿 VARCHAR(100)=  NULL  , -- 
  @齿列义齿 VARCHAR(100)=  NULL   --  
	  ,@_BlueAddr		varchar(20)
	  ,@_BlueType		varchar(20)
      ,@_BlueName		varchar(20)

AS
BEGIN
	IF NOT EXISTS (SELECT 1 FROM tb_健康体检 WHERE 身份证号 = @身份证号)--不存在，插入
	BEGIN
		  BEGIN TRANSACTION 
 
			  INSERT INTO tb_健康体检 
			  ( 
				身份证号 , --  
				姓名 , --  
				性别 , --  
				出生日期 , --  
				症状 , --  
				体温 , --  
				呼吸 , --  
				脉搏 , --  
				血压右侧1 , --  
				血压右侧2 , --  
				血压左侧1 , --  
				血压左侧2 , --  
				身高 , --  
				腰围 , --  
				体重 , --  
				体重指数 , --  
				老年人认知 , --  
				老年人情感 , --  
				老年人认知分 , --  
				老年人情感分 , --  
				左眼视力 , --  
				右眼视力 , --  
				左眼矫正 , --  
				右眼矫正 , --  
				听力 , --  
				运动功能 , --  
				皮肤 , --  
				淋巴结 , --  
				淋巴结其他 , --  
				桶状胸 , --  
				呼吸音 , --  
				呼吸音异常 , --  
				罗音 , --  
				罗音异常 , --  
				心率 , --  
				心律 , --  
				杂音 , --  
				杂音有 , --  
				压痛 , --  
				压痛有 , --  
				包块 , --  
				包块有 , --  
				肝大 , --  
				肝大有 , --  
				脾大 , --  
				脾大有 , --  
				浊音 , --  
				浊音有 , --  
				下肢水肿 , --  
				肛门指诊 , --  
				肛门指诊异常 , --  
				G_QLX , --  
				查体其他 , --  
				白细胞 , --  
				血红蛋白 , --  
				血小板 , --  
				血常规其他 , --  
				尿蛋白 , --  
				尿糖 , --  
				尿酮体 , --  
				尿潜血 , --  
				尿常规其他 , --  
				大便潜血 , --  
				血清谷丙转氨酶 , --  
				血清谷草转氨酶 , --  
				白蛋白 , --  
				总胆红素 , --  
				结合胆红素 , --  
				血清肌酐 , --  
				血尿素氮 , --  
				总胆固醇 , --  
				甘油三酯 , --  
				血清低密度脂蛋白胆固醇 , --  
				血清高密度脂蛋白胆固醇 , --  
				空腹血糖 , --  
				乙型肝炎表面抗原 , --  
				眼底 , --  
				眼底异常 , --  
				心电图 , --  
				心电图异常 , --  
				胸部X线片 , --  
				胸部X线片异常 , --  
				B超 , --  
				B超其他 , --  
				辅助检查其他 , --  
				G_JLJJY , --  
				个人档案编号 , --  
				创建机构 , --  
				创建时间 , --  
				创建人 , --  
				修改时间 , --  
				修改人 , --  
				体检日期 , --  
				所属机构 , --  
				FIELD1 , --  
				FIELD2 , --  
				FIELD3 , --  
				FIELD4 , --  
				WBC_SUP , --  
				PLT_SUP , --  
				G_TUNWEI , --  
				G_YTWBZ , --  
				锻炼频率 , --  
				每次锻炼时间 , --  
				坚持锻炼时间 , --  
				锻炼方式 , --  
				饮食习惯 , --  
				吸烟状况 , --  
				日吸烟量 , --  
				开始吸烟年龄 , --  
				戒烟年龄 , --  
				饮酒频率 , --  
				日饮酒量 , --  
				是否戒酒 , --  
				戒酒年龄 , --  
				开始饮酒年龄 , --  
				近一年内是否曾醉酒 , --  
				饮酒种类 , --  
				饮酒种类其它 , --  
				有无职业病 , --  
				具体职业 , --  
				从业时间 , --  
				化学物质 , --  
				化学物质防护 , --  
				化学物质具体防护 , --  
				毒物 , --  
				G_DWFHCS , --  
				G_DWFHCSQT , --  
				放射物质 , --  
				放射物质防护措施有无 , --  
				放射物质防护措施其他 , --  
				口唇 , --  
				齿列 , --  
				咽部 , --  
				皮肤其他 , --  
				巩膜 , --  
				巩膜其他 , --  
				足背动脉搏动 , --  
				乳腺 , --  
				乳腺其他 , --  
				外阴 , --  
				外阴异常 , --  
				阴道 , --  
				阴道异常 , --  
				宫颈 , --  
				宫颈异常 , --  
				宫体 , --  
				宫体异常 , --  
				附件 , --  
				附件异常 , --  
				血钾浓度 , --  
				血钠浓度 , --  
				糖化血红蛋白 , --  
				宫颈涂片 , --  
				宫颈涂片异常 , --  
				平和质 , --  
				气虚质 , --  
				阳虚质 , --  
				阴虚质 , --  
				痰湿质 , --  
				湿热质 , --  
				血瘀质 , --  
				气郁质 , --  
				特禀质 , --  
				脑血管疾病 , --  
				脑血管疾病其他 , --  
				肾脏疾病 , --  
				肾脏疾病其他 , --  
				心脏疾病 , --  
				心脏疾病其他 , --  
				血管疾病 , --  
				血管疾病其他 , --  
				眼部疾病 , --  
				眼部疾病其他 , --  
				神经系统疾病 , --  
				神经系统疾病其他 , --  
				其他系统疾病 , --  
				其他系统疾病其他 , --  
				健康评价 , --  
				健康评价异常1 , --  
				健康评价异常2 , --  
				健康评价异常3 , --  
				健康评价异常4 , --  
				健康指导 , --  
				危险因素控制 , --  
				危险因素控制体重 , --  
				危险因素控制疫苗 , --  
				危险因素控制其他 , --  
				FIELD5 , --  
				症状其他 , --  
				G_XYYC , --  
				G_XYZC , --  
				G_QTZHZH , --  
				缺项 , --  
				口唇其他 , --  
				齿列其他 , --  
				咽部其他 , --  
				YDGNQT , --  
				餐后2H血糖 , --  
				老年人状况评估 , --  
				老年人自理评估 , --  
				粉尘 , --  
				物理因素 , --  
				职业病其他 , --  
				粉尘防护有无 , --  
				物理防护有无 , --  
				其他防护有无 , --  
				粉尘防护措施 , --  
				物理防护措施 , --  
				其他防护措施 , --  
				TNBFXJF , --  
				左侧原因 , --  
				右侧原因 , --  
				尿微量白蛋白 , --  
				完整度 , --  
				齿列缺齿 , --  
				齿列龋齿 , --  
				齿列义齿   --  
				  ,BlueAddr	
				  ,BlueType	
				  ,BlueName	
			  ) 
			  VALUES 
			  ( 
				@身份证号 , 
				@姓名 , 
				@性别 , 
				@出生日期 , 
				@症状 , 
				@体温 , 
				@呼吸 , 
				@脉搏 , 
				@血压右侧1 , 
				@血压右侧2 , 
				@血压左侧1 , 
				@血压左侧2 , 
				@身高 , 
				@腰围 , 
				@体重 , 
				@体重指数 , 
				@老年人认知 , 
				@老年人情感 , 
				@老年人认知分 , 
				@老年人情感分 , 
				@左眼视力 , 
				@右眼视力 , 
				@左眼矫正 , 
				@右眼矫正 , 
				@听力 , 
				@运动功能 , 
				@皮肤 , 
				@淋巴结 , 
				@淋巴结其他 , 
				@桶状胸 , 
				@呼吸音 , 
				@呼吸音异常 , 
				@罗音 , 
				@罗音异常 , 
				@心率 , 
				@心律 , 
				@杂音 , 
				@杂音有 , 
				@压痛 , 
				@压痛有 , 
				@包块 , 
				@包块有 , 
				@肝大 , 
				@肝大有 , 
				@脾大 , 
				@脾大有 , 
				@浊音 , 
				@浊音有 , 
				@下肢水肿 , 
				@肛门指诊 , 
				@肛门指诊异常 , 
				@G_QLX , 
				@查体其他 , 
				@白细胞 , 
				@血红蛋白 , 
				@血小板 , 
				@血常规其他 , 
				@尿蛋白 , 
				@尿糖 , 
				@尿酮体 , 
				@尿潜血 , 
				@尿常规其他 , 
				@大便潜血 , 
				@血清谷丙转氨酶 , 
				@血清谷草转氨酶 , 
				@白蛋白 , 
				@总胆红素 , 
				@结合胆红素 , 
				@血清肌酐 , 
				@血尿素氮 , 
				@总胆固醇 , 
				@甘油三酯 , 
				@血清低密度脂蛋白胆固醇 , 
				@血清高密度脂蛋白胆固醇 , 
				@空腹血糖 , 
				@乙型肝炎表面抗原 , 
				@眼底 , 
				@眼底异常 , 
				@心电图 , 
				@心电图异常 , 
				@胸部X线片 , 
				@胸部X线片异常 , 
				@B超 , 
				@B超其他 , 
				@辅助检查其他 , 
				@G_JLJJY , 
				@个人档案编号 , 
				@创建机构 , 
				@创建时间 , 
				@创建人 , 
				@修改时间 , 
				@修改人 , 
				case when isnull(@体检日期,'') = '' then convert(varchar ,getdate(),120) else @体检日期 end, 
				@所属机构 , 
				@FIELD1 , 
				@FIELD2 , 
				@FIELD3 , 
				@FIELD4 , 
				@WBC_SUP , 
				@PLT_SUP , 
				@G_TUNWEI , 
				@G_YTWBZ , 
				@锻炼频率 , 
				@每次锻炼时间 , 
				@坚持锻炼时间 , 
				@锻炼方式 , 
				@饮食习惯 , 
				@吸烟状况 , 
				@日吸烟量 , 
				@开始吸烟年龄 , 
				@戒烟年龄 , 
				@饮酒频率 , 
				@日饮酒量 , 
				@是否戒酒 , 
				@戒酒年龄 , 
				@开始饮酒年龄 , 
				@近一年内是否曾醉酒 , 
				@饮酒种类 , 
				@饮酒种类其它 , 
				@有无职业病 , 
				@具体职业 , 
				@从业时间 , 
				@化学物质 , 
				@化学物质防护 , 
				@化学物质具体防护 , 
				@毒物 , 
				@G_DWFHCS , 
				@G_DWFHCSQT , 
				@放射物质 , 
				@放射物质防护措施有无 , 
				@放射物质防护措施其他 , 
				@口唇 , 
				@齿列 , 
				@咽部 , 
				@皮肤其他 , 
				@巩膜 , 
				@巩膜其他 , 
				@足背动脉搏动 , 
				@乳腺 , 
				@乳腺其他 , 
				@外阴 , 
				@外阴异常 , 
				@阴道 , 
				@阴道异常 , 
				@宫颈 , 
				@宫颈异常 , 
				@宫体 , 
				@宫体异常 , 
				@附件 , 
				@附件异常 , 
				@血钾浓度 , 
				@血钠浓度 , 
				@糖化血红蛋白 , 
				@宫颈涂片 , 
				@宫颈涂片异常 , 
				@平和质 , 
				@气虚质 , 
				@阳虚质 , 
				@阴虚质 , 
				@痰湿质 , 
				@湿热质 , 
				@血瘀质 , 
				@气郁质 , 
				@特禀质 , 
				@脑血管疾病 , 
				@脑血管疾病其他 , 
				@肾脏疾病 , 
				@肾脏疾病其他 , 
				@心脏疾病 , 
				@心脏疾病其他 , 
				@血管疾病 , 
				@血管疾病其他 , 
				@眼部疾病 , 
				@眼部疾病其他 , 
				@神经系统疾病 , 
				@神经系统疾病其他 , 
				@其他系统疾病 , 
				@其他系统疾病其他 , 
				@健康评价 , 
				@健康评价异常1 , 
				@健康评价异常2 , 
				@健康评价异常3 , 
				@健康评价异常4 , 
				@健康指导 , 
				@危险因素控制 , 
				@危险因素控制体重 , 
				@危险因素控制疫苗 , 
				@危险因素控制其他 , 
				@FIELD5 , 
				@症状其他 , 
				@G_XYYC , 
				@G_XYZC , 
				@G_QTZHZH , 
				@缺项 , 
				@口唇其他 , 
				@齿列其他 , 
				@咽部其他 , 
				@YDGNQT , 
				@餐后2H血糖 , 
				@老年人状况评估 , 
				@老年人自理评估 , 
				@粉尘 , 
				@物理因素 , 
				@职业病其他 , 
				@粉尘防护有无 , 
				@物理防护有无 , 
				@其他防护有无 , 
				@粉尘防护措施 , 
				@物理防护措施 , 
				@其他防护措施 , 
				@TNBFXJF , 
				@左侧原因 , 
				@右侧原因 , 
				@尿微量白蛋白 , 
				@完整度 , 
				@齿列缺齿 , 
				@齿列龋齿 , 
				@齿列义齿  
				  ,@_BlueAddr
				  ,@_BlueType
				  ,@_BlueName
			  ); 
 
		 IF @@error = 0 BEGIN 
			 COMMIT TRANSACTION 
			 SELECT 0 , '执行成功' 
			 RETURN 
		 END 
		 ELSE BEGIN 
			 ROLLBACK TRANSACTION 
			 SELECT 1 , '执行失败' 
			 RETURN 
		 END 
	END
	ELSE
	BEGIN

		BEGIN TRANSACTION 
 
			  UPDATE tb_健康体检 SET 
				症状 = CASE WHEN ISNULL( @症状,'')= '1' THEN  症状 ELSE @症状 END , --  
				体温 = CASE WHEN ISNULL( @体温,0.00)= 0.00 THEN  体温 ELSE @体温 END , --   
				呼吸 = CASE WHEN ISNULL( @呼吸,0)= 0 THEN  呼吸 ELSE @呼吸 END , --   
				脉搏 = CASE WHEN ISNULL( @脉搏,'')= '' THEN  脉搏 ELSE @脉搏 END , --   
				血压右侧1 = CASE WHEN ISNULL( @血压右侧1,'')= '' THEN  血压右侧1 ELSE @血压右侧1 END , --  
				血压右侧2 = CASE WHEN ISNULL( @血压右侧2,'')= '' THEN  血压右侧2 ELSE @血压右侧2 END , --  
				血压左侧1 = CASE WHEN ISNULL( @血压左侧1,'')= '' THEN  血压左侧1 ELSE @血压左侧1 END , --  
				血压左侧2 = CASE WHEN ISNULL( @血压左侧2,'')= '' THEN  血压左侧2 ELSE @血压左侧2 END , --  
				身高 = CASE WHEN ISNULL( @身高,0.00)= 0.00 THEN  身高 ELSE @身高 END , --  
				腰围 = CASE WHEN ISNULL( @腰围,0.00)= 0.00 THEN  腰围 ELSE @腰围 END , --  
				体重 = CASE WHEN ISNULL( @体重,0.00)= 0.00 THEN  体重 ELSE @体重 END , --  
				体重指数 = CASE WHEN ISNULL( @体重指数,'')= '' THEN  体重指数 ELSE @体重指数 END , --  
				老年人认知 = CASE WHEN ISNULL( @老年人认知,'')= '' THEN  老年人认知 ELSE @老年人认知 END , --  
				老年人情感 = CASE WHEN ISNULL( @老年人情感,'')= '' THEN  老年人情感 ELSE @老年人情感 END , --  
				老年人认知分 = CASE WHEN ISNULL( @老年人认知分,'')= '' THEN  老年人认知分 ELSE @老年人认知分 END , --  
				老年人情感分 = CASE WHEN ISNULL( @老年人情感分,'')= '' THEN  老年人情感分 ELSE @老年人情感分 END , --  
				左眼视力 =CASE WHEN ISNULL( @左眼视力,0.00)= 0.00 THEN  左眼视力 ELSE @左眼视力 END , --  
				右眼视力 =CASE WHEN ISNULL( @右眼视力,0.00)= 0.00 THEN  右眼视力 ELSE @右眼视力 END , --  
				左眼矫正 =CASE WHEN ISNULL( @左眼矫正,'')= '' THEN  左眼矫正 ELSE @左眼矫正 END , --  
				右眼矫正 =CASE WHEN ISNULL( @右眼矫正,'')= '' THEN  右眼矫正 ELSE @右眼矫正 END , --  
				听力 = CASE WHEN ISNULL( @听力,'')= '1' THEN  听力 ELSE @听力 END , --   
				运动功能 = CASE WHEN ISNULL( @运动功能,'')= '1' THEN  运动功能 ELSE @运动功能 END , --  
				皮肤 = CASE WHEN ISNULL( @皮肤,'')= '1' THEN  皮肤 ELSE @皮肤 END , --  
				淋巴结 = CASE WHEN ISNULL( @淋巴结,'')= '1' THEN  淋巴结 ELSE @淋巴结 END , --    
				淋巴结其他 = CASE WHEN ISNULL( @淋巴结其他,'')= '' THEN  淋巴结其他 ELSE @淋巴结其他 END , --  
				桶状胸 = CASE WHEN ISNULL( @桶状胸,'')= '2' THEN  桶状胸 ELSE @桶状胸 END , --  
				呼吸音 = CASE WHEN ISNULL( @呼吸音,'')= '1' THEN  呼吸音 ELSE @呼吸音 END , --  
				呼吸音异常 = CASE WHEN ISNULL( @呼吸音异常,'')= '' THEN  呼吸音异常 ELSE @呼吸音异常 END , --  
				罗音 = CASE WHEN ISNULL( @罗音,'')= '1' THEN  罗音 ELSE @罗音 END , --  
				罗音异常 = CASE WHEN ISNULL( @罗音异常,'')= '' THEN  罗音异常 ELSE @罗音异常 END , --  
				心率 = CASE WHEN ISNULL( @心率,0)= 0 THEN  心率 ELSE @心率 END , --  
				心律 = CASE WHEN ISNULL( @心律,'')= '1' THEN  心律 ELSE @心律 END , --  
				杂音 = CASE WHEN ISNULL( @杂音,'')= '1' THEN  杂音 ELSE @杂音 END , --  
				杂音有 = CASE WHEN ISNULL( @杂音有,'')= '' THEN  杂音有 ELSE @杂音有 END , --  
				压痛 = CASE WHEN ISNULL( @压痛,'')= '1' THEN  压痛 ELSE @压痛 END , --  
				压痛有 = CASE WHEN ISNULL( @压痛有,'')= '' THEN  压痛有 ELSE @压痛有 END , --  
				包块 = CASE WHEN ISNULL( @包块,'')= '1' THEN  包块 ELSE @包块 END , --  
				包块有 = CASE WHEN ISNULL( @包块有,'')=''  THEN  包块有 ELSE @包块有 END , --  
				肝大 = CASE WHEN ISNULL( @肝大,'')='1' THEN  肝大 ELSE @肝大 END , --  
				肝大有 = CASE WHEN ISNULL( @肝大有,'')= '' THEN  肝大有 ELSE @肝大有 END , --    
				脾大 =CASE WHEN ISNULL( @脾大,'')= '1' THEN  脾大 ELSE @脾大 END , --  
				脾大有 = CASE WHEN ISNULL( @脾大有,'')= '' THEN  脾大有 ELSE @脾大有 END , --  
				浊音 = CASE WHEN ISNULL( @浊音,'')= '1' THEN  浊音 ELSE @浊音 END , --  
				浊音有 = CASE WHEN ISNULL( @浊音有,'')= '' THEN  浊音有 ELSE @浊音有 END , --  
				下肢水肿 = CASE WHEN ISNULL( @下肢水肿,'')= '1' THEN  下肢水肿 ELSE @下肢水肿 END , --  
				肛门指诊 = CASE WHEN ISNULL( @肛门指诊,'')= '' THEN  肛门指诊 ELSE @肛门指诊 END , --  
				肛门指诊异常 = CASE WHEN ISNULL( @肛门指诊异常,'')= '' THEN  肛门指诊异常 ELSE @肛门指诊异常 END , --  
				G_QLX = CASE WHEN ISNULL( @G_QLX,'')= '' THEN  G_QLX ELSE @G_QLX END , --  
				查体其他 = CASE WHEN ISNULL( @查体其他,'')= '' THEN  查体其他 ELSE @查体其他 END , --    
				白细胞 = CASE WHEN ISNULL( @白细胞,'')= '' THEN  白细胞 ELSE @白细胞 END , --   
				血红蛋白 = CASE WHEN ISNULL( @血红蛋白,'')= '' THEN  血红蛋白 ELSE @血红蛋白 END , --  
				血小板 = CASE WHEN ISNULL( @血小板,'')= '' THEN  血小板 ELSE @血小板 END , --   
				血常规其他 = CASE WHEN ISNULL( @血常规其他,'')= '' THEN  血常规其他 ELSE @血常规其他 END , --  
				尿蛋白 = CASE WHEN ISNULL( @尿蛋白,'')= '' THEN  尿蛋白 ELSE @尿蛋白 END , --  
				尿糖 = CASE WHEN ISNULL( @尿糖,'')= '' THEN  尿糖 ELSE @尿糖 END , --  
				尿酮体 = CASE WHEN ISNULL( @尿酮体,'')= '' THEN  尿酮体 ELSE @尿酮体 END , --  
				尿潜血 = CASE WHEN ISNULL( @尿潜血,'')= '' THEN  尿潜血 ELSE @尿潜血 END , --  
				尿常规其他 = CASE WHEN ISNULL( @尿常规其他,'')= '' THEN  尿常规其他 ELSE @尿常规其他 END , --  
				大便潜血 =CASE WHEN ISNULL( @大便潜血,'')= '' THEN  大便潜血 ELSE @大便潜血 END , --  
				血清谷丙转氨酶 = CASE WHEN ISNULL( @血清谷丙转氨酶,'')='' THEN  血清谷丙转氨酶 ELSE @血清谷丙转氨酶 END , --   
				血清谷草转氨酶 = CASE WHEN ISNULL( @血清谷草转氨酶,'')='' THEN  血清谷草转氨酶 ELSE @血清谷草转氨酶 END , --   
				白蛋白 = CASE WHEN ISNULL( @白蛋白,'')= '' THEN  白蛋白 ELSE @白蛋白 END , --  
				总胆红素 = CASE WHEN ISNULL( @总胆红素,'')= '' THEN  总胆红素 ELSE @总胆红素 END , --  
				结合胆红素 = CASE WHEN ISNULL( @结合胆红素,'')= '' THEN  结合胆红素 ELSE @结合胆红素 END , --  
				血清肌酐 = CASE WHEN ISNULL( @血清肌酐,'')= '' THEN  血清肌酐 ELSE @血清肌酐 END , --  
				血尿素氮 = CASE WHEN ISNULL( @血尿素氮,'')= '' THEN  血尿素氮 ELSE @血尿素氮 END , --  
				总胆固醇 = CASE WHEN ISNULL( @总胆固醇,'')= '' THEN  总胆固醇 ELSE @总胆固醇 END , --  
				甘油三酯 = CASE WHEN ISNULL( @甘油三酯,'')= '' THEN  甘油三酯 ELSE @甘油三酯 END , --  
				血清低密度脂蛋白胆固醇 = CASE WHEN ISNULL( @血清低密度脂蛋白胆固醇,'')= '' THEN  血清低密度脂蛋白胆固醇 ELSE @血清低密度脂蛋白胆固醇 END , --  
				血清高密度脂蛋白胆固醇 = CASE WHEN ISNULL( @血清高密度脂蛋白胆固醇,'')= '' THEN  血清高密度脂蛋白胆固醇 ELSE @血清高密度脂蛋白胆固醇 END , --  
				空腹血糖 = CASE WHEN ISNULL( @空腹血糖,'')= '' THEN  空腹血糖 ELSE @空腹血糖 END , --  
				乙型肝炎表面抗原 = CASE WHEN ISNULL( @乙型肝炎表面抗原,'')= '' THEN  乙型肝炎表面抗原 ELSE @乙型肝炎表面抗原 END , --  
				眼底 = CASE WHEN ISNULL( @眼底,'')= '' THEN  眼底 ELSE @眼底 END , --  
				眼底异常 = CASE WHEN ISNULL( @眼底异常,'')= '' THEN  眼底异常 ELSE @眼底异常 END , --   
				心电图 = CASE WHEN ISNULL( @心电图,'')= '' THEN  心电图 ELSE @心电图 END , --   
				心电图异常 = CASE WHEN ISNULL( @心电图异常,'')= '' THEN  心电图异常 ELSE @心电图异常 END , --  
				胸部X线片 =CASE WHEN ISNULL( @胸部X线片,'')= '' THEN  胸部X线片 ELSE @胸部X线片 END , --  
				胸部X线片异常 = CASE WHEN ISNULL( @胸部X线片异常,'')= '' THEN  胸部X线片异常 ELSE @胸部X线片异常 END , --  
				B超 = CASE WHEN ISNULL( @B超,'')= '' THEN  B超 ELSE @B超 END , --  
				B超其他 = CASE WHEN ISNULL( @B超其他,'')= '' THEN  B超其他 ELSE @B超其他 END , --  
				辅助检查其他 = CASE WHEN ISNULL( @辅助检查其他,'')= '' THEN  辅助检查其他 ELSE @辅助检查其他 END , --  
				G_JLJJY = CASE WHEN ISNULL( @G_JLJJY,'')= '' THEN  G_JLJJY ELSE @G_JLJJY END , --  
				个人档案编号 = CASE WHEN ISNULL( @个人档案编号,'')= '' THEN  个人档案编号 ELSE @个人档案编号 END , --   
				创建机构 = CASE WHEN ISNULL( @创建机构,'')= '' THEN  创建机构 ELSE @创建机构 END , --  
				创建时间 = CASE WHEN ISNULL( @创建时间,'')= '' THEN  创建时间 ELSE @创建时间 END , --  
				创建人 = CASE WHEN ISNULL( @创建人,'')= '' THEN  创建人 ELSE @创建人 END , --   
				修改时间 = CASE WHEN ISNULL( @修改时间,'')= '' THEN  修改时间 ELSE @修改时间 END , --   
				修改人 = CASE WHEN ISNULL( @修改人,'')= '' THEN  修改人 ELSE @修改人 END , --   
				体检日期 = CASE WHEN ISNULL( @体检日期,'')= '' THEN  体检日期 ELSE @体检日期 END , --  
				所属机构 = CASE WHEN ISNULL( @所属机构,'')= '' THEN  所属机构 ELSE @所属机构 END , --  
				FIELD1 = CASE WHEN ISNULL( @FIELD1,'')= '' THEN  FIELD1 ELSE @FIELD1 END , --  
				FIELD2 = CASE WHEN ISNULL( @FIELD2,'')= '' THEN  FIELD2 ELSE @FIELD2 END , --  
				FIELD3 = CASE WHEN ISNULL( @FIELD3,'')= '' THEN  FIELD3 ELSE @FIELD3 END , --  
				FIELD4 = CASE WHEN ISNULL( @FIELD4,'')= '' THEN  FIELD4 ELSE @FIELD4 END , --  
				WBC_SUP = CASE WHEN ISNULL( @WBC_SUP,'')= '' THEN  WBC_SUP ELSE @WBC_SUP END , --   
				PLT_SUP = CASE WHEN ISNULL( @PLT_SUP,'')= '' THEN  PLT_SUP ELSE @PLT_SUP END , --   
				G_TUNWEI = CASE WHEN ISNULL( @G_TUNWEI,'')= '' THEN  G_TUNWEI ELSE @G_TUNWEI END , --  
				G_YTWBZ = CASE WHEN ISNULL( @G_YTWBZ,'')= '' THEN  G_YTWBZ ELSE @G_YTWBZ END , --  
				锻炼频率 = CASE WHEN ISNULL( @锻炼频率,'')= '4' THEN  锻炼频率 ELSE @锻炼频率 END , --  
				每次锻炼时间 = CASE WHEN ISNULL( @每次锻炼时间,'')='' THEN  每次锻炼时间 ELSE @每次锻炼时间 END , --  
				坚持锻炼时间 = CASE WHEN ISNULL( @坚持锻炼时间,'')='' THEN  坚持锻炼时间 ELSE @坚持锻炼时间 END , --  
				锻炼方式 = CASE WHEN ISNULL( @锻炼方式,'')= '' THEN  锻炼方式 ELSE @锻炼方式 END , --  
				饮食习惯 = CASE WHEN ISNULL( @饮食习惯,'')= '1' THEN  饮食习惯 ELSE @饮食习惯 END , --  
				吸烟状况 = CASE WHEN ISNULL( @吸烟状况,'')= '1' THEN  吸烟状况 ELSE @吸烟状况 END , --  
				日吸烟量 = CASE WHEN ISNULL( @日吸烟量,'')= '' THEN  日吸烟量 ELSE @日吸烟量 END , --  
				开始吸烟年龄 = CASE WHEN ISNULL( @开始吸烟年龄,'')= '' THEN  开始吸烟年龄 ELSE @开始吸烟年龄 END , --   
				戒烟年龄 = CASE WHEN ISNULL( @戒烟年龄,'')='' THEN  戒烟年龄 ELSE @戒烟年龄 END , --  
				饮酒频率 = CASE WHEN ISNULL( @饮酒频率,'')='1' THEN  饮酒频率 ELSE @饮酒频率 END , --  
				日饮酒量 = CASE WHEN ISNULL( @日饮酒量,'')='' THEN  日饮酒量 ELSE @日饮酒量 END , --  
				是否戒酒 = CASE WHEN ISNULL( @是否戒酒,'')='1' THEN  是否戒酒 ELSE @是否戒酒 END , --  
				戒酒年龄 = CASE WHEN ISNULL( @戒酒年龄,'')='' THEN  戒酒年龄 ELSE @戒酒年龄 END , --  
				开始饮酒年龄 = CASE WHEN ISNULL( @开始饮酒年龄,'')= '' THEN  开始饮酒年龄 ELSE @开始饮酒年龄 END , --  
				近一年内是否曾醉酒 =CASE WHEN ISNULL( @近一年内是否曾醉酒,'')= '' THEN  近一年内是否曾醉酒 ELSE @近一年内是否曾醉酒 END , --  
				饮酒种类 = CASE WHEN ISNULL( @饮酒种类,'')= '' THEN  饮酒种类 ELSE @饮酒种类 END , --  
				饮酒种类其它 = CASE WHEN ISNULL( @饮酒种类其它,'')= '' THEN  饮酒种类其它 ELSE @饮酒种类其它 END , --  
				有无职业病 = CASE WHEN ISNULL( @有无职业病,'')= '1' THEN  有无职业病 ELSE @有无职业病 END , --  
				具体职业 =CASE WHEN ISNULL( @具体职业,'')= '' THEN  具体职业 ELSE @具体职业 END , --  
				从业时间 =CASE WHEN ISNULL( @从业时间,'')= '' THEN  从业时间 ELSE @从业时间 END , --  
				化学物质 =CASE WHEN ISNULL( @化学物质,'')= '' THEN  化学物质 ELSE @化学物质 END , --  
				化学物质防护 = CASE WHEN ISNULL( @化学物质防护,'')= '' THEN  化学物质防护 ELSE @化学物质防护 END , --  
				化学物质具体防护 = CASE WHEN ISNULL( @化学物质具体防护,'')= '' THEN  化学物质具体防护 ELSE @化学物质具体防护 END , --  
				毒物 = CASE WHEN ISNULL( @毒物,'')= '' THEN  毒物 ELSE @毒物 END , --   
				G_DWFHCS = CASE WHEN ISNULL( @G_DWFHCS,'')= '' THEN  G_DWFHCS ELSE @G_DWFHCS END , --   
				G_DWFHCSQT = CASE WHEN ISNULL( @G_DWFHCSQT,'')= '' THEN  G_DWFHCSQT ELSE @G_DWFHCSQT END , --   
				放射物质 =CASE WHEN ISNULL( @放射物质,'')= '' THEN  放射物质 ELSE @放射物质 END , --    
				放射物质防护措施有无 = CASE WHEN ISNULL( @放射物质防护措施有无,'')= '' THEN  放射物质防护措施有无 ELSE @放射物质防护措施有无 END , --   
				放射物质防护措施其他 = CASE WHEN ISNULL( @放射物质防护措施其他,'')= '' THEN  放射物质防护措施其他 ELSE @放射物质防护措施其他 END , --   
				口唇 =CASE WHEN ISNULL( @口唇,'')= '1' THEN  口唇 ELSE @口唇 END , --   
				齿列 =CASE WHEN ISNULL( @齿列,'')= '1' THEN  齿列 ELSE @齿列 END , --   
				咽部 =CASE WHEN ISNULL( @咽部,'')= '1' THEN  咽部 ELSE @咽部 END , --   
				皮肤其他 = CASE WHEN ISNULL( @皮肤其他,'')= '' THEN  皮肤其他 ELSE @皮肤其他 END , --   
				巩膜 = CASE WHEN ISNULL( @巩膜,'')= '1' THEN  巩膜 ELSE @巩膜 END , --   
				巩膜其他 = CASE WHEN ISNULL( @巩膜其他,'')= '' THEN  巩膜其他 ELSE @巩膜其他 END , --   
				足背动脉搏动 = CASE WHEN ISNULL( @足背动脉搏动,'')= '2' THEN  足背动脉搏动 ELSE @足背动脉搏动 END , --   
				乳腺 = CASE WHEN ISNULL( @乳腺,'')= '' THEN  乳腺 ELSE @乳腺 END , --   
				乳腺其他 = CASE WHEN ISNULL( @乳腺其他,'')= '' THEN  乳腺其他 ELSE @乳腺其他 END , --   
				外阴 =  CASE WHEN ISNULL( @外阴,'')= '' THEN  外阴 ELSE @外阴 END , --   
				外阴异常 = CASE WHEN ISNULL( @外阴异常,'')= '' THEN  外阴异常 ELSE @外阴异常 END , --   
				阴道 =CASE WHEN ISNULL( @阴道,'')= '' THEN  阴道 ELSE @阴道 END , --   
				阴道异常 = CASE WHEN ISNULL( @阴道异常,'')= '' THEN  阴道异常 ELSE @阴道异常 END , --   
				宫颈 = CASE WHEN ISNULL( @宫颈,'')= '' THEN  宫颈 ELSE @宫颈 END , --    
				宫颈异常 = CASE WHEN ISNULL( @宫颈异常,'')= '' THEN  宫颈异常 ELSE @宫颈异常 END , --   
				宫体 = CASE WHEN ISNULL( @宫体,'')= '' THEN  宫体 ELSE @宫体 END , --   
				宫体异常 = CASE WHEN ISNULL( @宫体异常,'')= '' THEN  宫体异常 ELSE @宫体异常 END , --   
				附件 = CASE WHEN ISNULL( @附件,'')= '' THEN  附件 ELSE @附件 END , --   
				附件异常 = CASE WHEN ISNULL( @附件异常,'')= '' THEN  附件异常 ELSE @附件异常 END , --   
				血钾浓度 = CASE WHEN ISNULL( @血钾浓度,'')= '' THEN  血钾浓度 ELSE @血钾浓度 END , --   
				血钠浓度 = CASE WHEN ISNULL( @血钠浓度,'')= '' THEN  血钠浓度 ELSE @血钠浓度 END , --   
				糖化血红蛋白 = CASE WHEN ISNULL( @糖化血红蛋白,'')= '' THEN  糖化血红蛋白 ELSE @糖化血红蛋白 END , --    
				宫颈涂片 = CASE WHEN ISNULL( @宫颈涂片,'')= '' THEN  宫颈涂片 ELSE @宫颈涂片 END , --   
				宫颈涂片异常 = CASE WHEN ISNULL( @宫颈涂片异常,'')= '' THEN  宫颈涂片异常 ELSE @宫颈涂片异常 END , --   
				平和质 =CASE WHEN ISNULL( @平和质,'')= '' THEN  平和质 ELSE @平和质 END , --   
				气虚质 =CASE WHEN ISNULL( @气虚质,'')= '' THEN  气虚质 ELSE @气虚质 END , --   
				阳虚质 =CASE WHEN ISNULL( @阳虚质,'')= '' THEN  阳虚质 ELSE @阳虚质 END , --   
				阴虚质 =CASE WHEN ISNULL( @阴虚质,'')= '' THEN  阴虚质 ELSE @阴虚质 END , --   
				痰湿质 =CASE WHEN ISNULL( @痰湿质,'')= '' THEN  痰湿质 ELSE @痰湿质 END , --   
				湿热质 =CASE WHEN ISNULL( @湿热质,'')= '' THEN  湿热质 ELSE @湿热质 END , --   
				血瘀质 =CASE WHEN ISNULL( @血瘀质,'')= '' THEN  血瘀质 ELSE @血瘀质 END , --   
				气郁质 =CASE WHEN ISNULL( @气郁质,'')= '' THEN  气郁质 ELSE @气郁质 END , --   
				特禀质 =CASE WHEN ISNULL( @特禀质,'')= '' THEN  特禀质 ELSE @特禀质 END , --   
				脑血管疾病 = CASE WHEN ISNULL( @脑血管疾病,'')= '1' THEN  脑血管疾病 ELSE @脑血管疾病 END , --   
				脑血管疾病其他 = CASE WHEN ISNULL( @脑血管疾病其他,'')= '' THEN  脑血管疾病其他 ELSE @脑血管疾病其他 END , --   
				肾脏疾病 = CASE WHEN ISNULL( @肾脏疾病,'')= '1' THEN  肾脏疾病 ELSE @肾脏疾病 END , --   
				肾脏疾病其他 = CASE WHEN ISNULL( @肾脏疾病其他,'')= '' THEN  肾脏疾病其他 ELSE @肾脏疾病其他 END , --   
				心脏疾病 = CASE WHEN ISNULL( @心脏疾病,'')= '1' THEN  心脏疾病 ELSE @心脏疾病 END , --   
				心脏疾病其他 = CASE WHEN ISNULL( @心脏疾病其他,'')= '' THEN  心脏疾病其他 ELSE @心脏疾病其他 END , --   
				血管疾病 = CASE WHEN ISNULL( @血管疾病,'')= '1' THEN  血管疾病 ELSE @血管疾病 END , --   
				血管疾病其他 = CASE WHEN ISNULL( @血管疾病其他,'')= '' THEN  血管疾病其他 ELSE @血管疾病其他 END , --   
				眼部疾病 = CASE WHEN ISNULL( @眼部疾病,'')= '1' THEN  眼部疾病 ELSE @眼部疾病 END , --   
				眼部疾病其他 = CASE WHEN ISNULL( @眼部疾病其他,'')='' THEN  眼部疾病其他 ELSE @眼部疾病其他 END , --   
				神经系统疾病 = CASE WHEN ISNULL( @神经系统疾病,'')='1' THEN  神经系统疾病 ELSE @神经系统疾病 END , --   
				神经系统疾病其他 = CASE WHEN ISNULL( @神经系统疾病其他,'')= '' THEN  神经系统疾病其他 ELSE @神经系统疾病其他 END , --     
				其他系统疾病 = CASE WHEN ISNULL( @其他系统疾病,'')= '1' THEN  其他系统疾病 ELSE @其他系统疾病 END , --   
				其他系统疾病其他 = CASE WHEN ISNULL( @其他系统疾病其他,'')= '' THEN  其他系统疾病其他 ELSE @其他系统疾病其他 END , --   
				健康评价 = CASE WHEN ISNULL( @健康评价,'')= '1' THEN  健康评价 ELSE @健康评价 END , --   
				健康评价异常1 = CASE WHEN ISNULL(@健康评价异常1,'')= '' THEN  健康评价异常1 ELSE @健康评价异常1 END , --   
				健康评价异常2 = CASE WHEN ISNULL(@健康评价异常2,'')= '' THEN  健康评价异常2 ELSE @健康评价异常2 END , --   
				健康评价异常3 = CASE WHEN ISNULL(@健康评价异常3,'')= '' THEN  健康评价异常3 ELSE @健康评价异常3 END , --   
				健康评价异常4 = CASE WHEN ISNULL(@健康评价异常4,'')= '' THEN  健康评价异常4 ELSE @健康评价异常4 END , --   
				健康指导 = CASE WHEN ISNULL( @健康指导,'')= '' THEN  健康指导 ELSE @健康指导 END , --   
				危险因素控制 = CASE WHEN ISNULL( @危险因素控制,'')= '' THEN  危险因素控制 ELSE @危险因素控制 END , --   
				危险因素控制体重 = CASE WHEN ISNULL( @危险因素控制体重,'')= '' THEN  危险因素控制体重 ELSE @危险因素控制体重 END , --   
				危险因素控制疫苗 = CASE WHEN ISNULL( @危险因素控制疫苗,'')= '' THEN  危险因素控制疫苗 ELSE @危险因素控制疫苗 END , --   
				危险因素控制其他 = CASE WHEN ISNULL( @危险因素控制其他,'')= '' THEN  危险因素控制其他 ELSE @危险因素控制其他 END , --   
				FIELD5 = CASE WHEN ISNULL( @FIELD5,'')= '' THEN  FIELD5 ELSE @FIELD5 END , --   
				症状其他 = CASE WHEN ISNULL( @症状其他,'')= '' THEN  症状其他 ELSE @症状其他 END , --   
				G_XYYC = CASE WHEN ISNULL( @G_XYYC,'')= '' THEN  G_XYYC ELSE @G_XYYC END , --   
				G_XYZC = CASE WHEN ISNULL( @G_XYZC,'')= '' THEN  G_XYZC ELSE @G_XYZC END , --   
				G_QTZHZH = CASE WHEN ISNULL( @G_QTZHZH,'')= '' THEN  G_QTZHZH ELSE @G_QTZHZH END , --   
				缺项 = CASE WHEN ISNULL( @缺项,'')= '' THEN  缺项 ELSE @缺项 END , --   
				口唇其他 =CASE WHEN ISNULL( @口唇其他,'')= '' THEN  口唇其他 ELSE @口唇其他 END , --   
				齿列其他 =CASE WHEN ISNULL( @齿列其他,'')= '' THEN  齿列其他 ELSE @齿列其他 END , --   
				咽部其他 =CASE WHEN ISNULL( @咽部其他,'')= '' THEN  咽部其他 ELSE @咽部其他 END , --   
				YDGNQT = CASE WHEN ISNULL( @YDGNQT,'')= '' THEN  YDGNQT ELSE @YDGNQT END , --   
				餐后2H血糖 = CASE WHEN ISNULL( @餐后2H血糖,'')= '' THEN  餐后2H血糖 ELSE @餐后2H血糖 END , --   
				老年人状况评估 = CASE WHEN ISNULL( @老年人状况评估,'')= '' THEN  老年人状况评估 ELSE @老年人状况评估 END , --   
				老年人自理评估 = CASE WHEN ISNULL( @老年人自理评估,'')= '' THEN  老年人自理评估 ELSE @老年人自理评估 END , --   
				粉尘 =CASE WHEN ISNULL( @粉尘,'')= '' THEN  粉尘 ELSE @粉尘 END , --   
				物理因素 =CASE WHEN ISNULL( @物理因素,'')= '' THEN  物理因素 ELSE @物理因素 END , --   
				职业病其他 = CASE WHEN ISNULL( @职业病其他,'')= '' THEN  职业病其他 ELSE @职业病其他 END , --   
				粉尘防护有无 = CASE WHEN ISNULL( @粉尘防护有无,'')= '' THEN  粉尘防护有无 ELSE @粉尘防护有无 END , --     
				物理防护有无 = CASE WHEN ISNULL( @物理防护有无,'')= '' THEN  物理防护有无 ELSE @物理防护有无 END , --     
				其他防护有无 = CASE WHEN ISNULL( @其他防护有无,'')= '' THEN  其他防护有无 ELSE @其他防护有无 END , --     
				粉尘防护措施 = CASE WHEN ISNULL( @粉尘防护措施,'')= '' THEN  粉尘防护措施 ELSE @粉尘防护措施 END , --     
				物理防护措施 = CASE WHEN ISNULL( @物理防护措施,'')= '' THEN  物理防护措施 ELSE @物理防护措施 END , --     
				其他防护措施 = CASE WHEN ISNULL( @其他防护措施,'')= '' THEN  其他防护措施 ELSE @其他防护措施 END , --     
				TNBFXJF =CASE WHEN ISNULL( @TNBFXJF,'')= '' THEN  TNBFXJF ELSE @TNBFXJF END , --   
				左侧原因 = CASE WHEN ISNULL( @左侧原因,'')= '' THEN  左侧原因 ELSE @左侧原因 END , --   
				右侧原因 = CASE WHEN ISNULL( @右侧原因,'')= '' THEN  右侧原因 ELSE @右侧原因 END , --   
				尿微量白蛋白 = CASE WHEN ISNULL( @尿微量白蛋白,'')= '' THEN  尿微量白蛋白 ELSE @尿微量白蛋白 END , --   
				完整度 = CASE WHEN ISNULL( @完整度,'')= '' THEN  完整度 ELSE @完整度 END , --   
				齿列缺齿 = CASE WHEN ISNULL( @齿列缺齿,'')= '' THEN  齿列缺齿 ELSE @齿列缺齿 END , --   
				齿列龋齿 = CASE WHEN ISNULL( @齿列龋齿,'')= '' THEN  齿列龋齿 ELSE @齿列龋齿 END , --   
				齿列义齿 = CASE WHEN ISNULL( @齿列义齿,'')= '' THEN  齿列义齿 ELSE @齿列义齿 END 
			 WHERE 身份证号 = @身份证号 ; 
 
			 IF @@error = 0 BEGIN 
				 COMMIT TRANSACTION 
				 SELECT 0 , '执行成功' 
				 RETURN 
			 END 
			 ELSE BEGIN 
				 ROLLBACK TRANSACTION 
				 SELECT 1 , '执行失败' 
				 RETURN 
			 END 
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_SAVE中医药体质辨识]    Script Date: 04/25/2016 11:58:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_SAVE中医药体质辨识]
	@身份证号	varchar(18),
	@姓名		nvarchar(20),
	@性别		nvarchar(2),
	@出生日期	varchar(20),
	@居住地址	NVARCHAR(200),
	@个人档案编号	VARCHAR(20),
	@创建机构	VARCHAR(20)			=	NULL,
	@创建人		VARCHAR(20)			=	NULL,
	@修改人		VARCHAR(20)			=	NULL,
	@所属机构		VARCHAR(20)			=	NULL,
	@创建时间		VARCHAR(20)			=	NULL,
	@修改时间		VARCHAR(20)			=	NULL,
	@特征1		INT			=	NULL,
	@特征2		INT			=	NULL,
	@特征3		INT			=	NULL,
	@特征4		INT			=	NULL,
	@特征5		INT			=	NULL,
	@特征6		INT			=	NULL,
	@特征7		INT			=	NULL,
	@特征8		INT			=	NULL,
	@特征9		INT			=	NULL,
	@特征10		INT			=	NULL,
	@特征11		INT			=	NULL,
	@特征12		INT			=	NULL,
	@特征13		INT			=	NULL,
	@特征14		INT			=	NULL,
	@特征15		INT			=	NULL,
	@特征16		INT			=	NULL,
	@特征17		INT			=	NULL,
	@特征18		INT			=	NULL,
	@特征19		INT			=	NULL,
	@特征20		INT			=	NULL,
	@特征21		INT			=	NULL,
	@特征22		INT			=	NULL,
	@特征23		INT			=	NULL,
	@特征24		INT			=	NULL,
	@特征25		INT			=	NULL,
	@特征26		INT			=	NULL,
	@特征27		INT			=	NULL,
	@特征28		INT			=	NULL,
	@特征29		INT			=	NULL,
	@特征30		INT			=	NULL,
	@特征31		INT			=	NULL,
	@特征32		INT			=	NULL,
	@特征33		INT			=	NULL,
	@气虚质得分	INT			=	NULL,
	@气虚质辨识	varchar(2)			=	NULL,
	@气虚质指导	VARCHAR(20)			=	NULL,
	@气虚质其他	VARCHAR(200)			=	NULL,
	@阳虚质得分	INT			=	NULL,
	@阳虚质辨识	varchar(2)			=	NULL,
	@阳虚质指导	VARCHAR(20)			=	NULL,
	@阳虚质其他	VARCHAR(200)			=	NULL,
	@阴虚质得分	INT			=	NULL,
	@阴虚质辨识	varchar(2)			=	NULL,
	@阴虚质指导	VARCHAR(20)			=	NULL,
	@阴虚质其他	VARCHAR(200)			=	NULL,
	@痰湿质得分	INT			=	NULL,
	@痰湿质辨识	varchar(2)			=	NULL,
	@痰湿质指导	VARCHAR(20)			=	NULL,
	@痰湿质其他	VARCHAR(200)			=	NULL,
	@湿热质得分	INT			=	NULL,
	@湿热质辨识	varchar(2)			=	NULL,
	@湿热质指导	VARCHAR(20)			=	NULL,
	@湿热质其他	VARCHAR(200)			=	NULL,
	@血瘀质得分	INT			=	NULL,
	@血瘀质辨识	varchar(2)			=	NULL,
	@血瘀质指导	VARCHAR(20)			=	NULL,
	@血瘀质其他	VARCHAR(200)			=	NULL,
	@气郁质得分	INT			=	NULL,
	@气郁质辨识	varchar(2)			=	NULL,
	@气郁质指导	VARCHAR(20)			=	NULL,
	@气郁质其他	VARCHAR(200)			=	NULL,
	@特禀质得分	INT			=	NULL,
	@特禀质辨识	varchar(2)			=	NULL,
	@特禀质指导	VARCHAR(20)			=	NULL,
	@特禀质其他	VARCHAR(200)			=	NULL,
	@平和质得分	INT			=	NULL,
	@平和质辨识	varchar(2)			=	NULL,
	@平和质指导	VARCHAR(20)			=	NULL,
	@平和质其他	VARCHAR(200)			=	NULL,
	@发生时间	VARCHAR(20)			=	NULL,
	@医生签名	VARCHAR(20)			=	NULL
	  ,@_BlueAddr		varchar(20)
	  ,@_BlueType		varchar(20)
      ,@_BlueName		varchar(20)
AS	
BEGIN
	SET NOCOUNT ON;
	IF NOT EXISTS(SELECT 1 FROM tb_老年人中医药特征管理 WHERE 身份证号 = @身份证号)--不存在，插入
	BEGIN
		INSERT INTO tb_老年人中医药特征管理
			(
			身份证号	,
			姓名		,
			性别		,
			出生日期	,
			居住地址	,
			个人档案编号	,
			创建机构	,
			创建人		,
			修改人		,
			所属机构	,
			创建时间	,
			修改时间	,
			特征1	,
			特征2	,
			特征3	,
			特征4	,
			特征5	,
			特征6	,
			特征7	,
			特征8	,
			特征9	,
			特征10	,
			特征11	,
			特征12	,
			特征13	,
			特征14	,
			特征15	,
			特征16	,
			特征17	,
			特征18	,
			特征19	,
			特征20	,
			特征21	,
			特征22	,
			特征23	,
			特征24	,
			特征25	,
			特征26	,
			特征27	,
			特征28	,
			特征29	,
			特征30	,
			特征31	,
			特征32	,
			特征33	,
			气虚质得分	,
			气虚质辨识	,
			气虚质指导	,
			气虚质其他	,
			阳虚质得分	,
			阳虚质辨识	,
			阳虚质指导	,
			阳虚质其他	,
			阴虚质得分	,
			阴虚质辨识	,
			阴虚质指导	,
			阴虚质其他	,
			痰湿质得分	,
			痰湿质辨识	,
			痰湿质指导	,
			痰湿质其他	,
			湿热质得分	,
			湿热质辨识	,
			湿热质指导	,
			湿热质其他	,
			血瘀质得分	,
			血瘀质辨识	,
			血瘀质指导	,
			血瘀质其他	,
			气郁质得分	,
			气郁质辨识	,
			气郁质指导	,
			气郁质其他	,
			特禀质得分	,
			特禀质辨识	,
			特禀质指导	,
			特禀质其他	,
			平和质得分	,
			平和质辨识	,
			平和质指导	,
			平和质其他	,
			发生时间	,
			医生签名	
			  ,BlueAddr	
			  ,BlueType	
			  ,BlueName	
			)
		VALUES (
			@身份证号	,
			@姓名		,
			@性别		,
			@出生日期	,
			@居住地址	,
			@个人档案编号	,
			@创建机构	,
			@创建人		,
			@修改人		,
			@所属机构	,
			@创建时间	,
			@修改时间	,
			@特征1	,
			@特征2	,
			@特征3	,
			@特征4	,
			@特征5	,
			@特征6	,
			@特征7	,
			@特征8	,
			@特征9	,
			@特征10	,
			@特征11	,
			@特征12	,
			@特征13	,
			@特征14	,
			@特征15	,
			@特征16	,
			@特征17	,
			@特征18	,
			@特征19	,
			@特征20	,
			@特征21	,
			@特征22	,
			@特征23	,
			@特征24	,
			@特征25	,
			@特征26	,
			@特征27	,
			@特征28	,
			@特征29	,
			@特征30	,
			@特征31	,
			@特征32	,
			@特征33	,
			@气虚质得分	,
			@气虚质辨识	,
			@气虚质指导	,
			@气虚质其他	,
			@阳虚质得分	,
			@阳虚质辨识	,
			@阳虚质指导	,
			@阳虚质其他	,
			@阴虚质得分	,
			@阴虚质辨识	,
			@阴虚质指导	,
			@阴虚质其他	,
			@痰湿质得分	,
			@痰湿质辨识	,
			@痰湿质指导	,
			@痰湿质其他	,
			@湿热质得分	,
			@湿热质辨识	,
			@湿热质指导	,
			@湿热质其他	,
			@血瘀质得分	,
			@血瘀质辨识	,
			@血瘀质指导	,
			@血瘀质其他	,
			@气郁质得分	,
			@气郁质辨识	,
			@气郁质指导	,
			@气郁质其他	,
			@特禀质得分	,
			@特禀质辨识	,
			@特禀质指导	,
			@特禀质其他	,
			@平和质得分	,
			@平和质辨识	,
			@平和质指导	,
			@平和质其他	,
			@发生时间	,
			@医生签名	
			  ,@_BlueAddr		
			  ,@_BlueType		
			  ,@_BlueName		
			)
	END
	ELSE --//更新
	BEGIN
		UPDATE A SET 
				A.身份证号=	@身份证号	,
				A.姓名		=	@姓名		,
				A.性别		=	@性别		,
				A.出生日期	=	@出生日期	,
				A.居住地址	=	@居住地址	,
				A.个人档案编号	=	@个人档案编号	,
				A.创建机构	=	@创建机构	,
				A.创建人	=		@创建人		,
				A.修改人	=		@修改人		,
				A.所属机构	=	@所属机构	,
				A.创建时间	=	@创建时间	,
				A.修改时间	=	@修改时间	,
				A.特征1	=	@特征1	,
				A.特征2	=	@特征2	,
				A.特征3	=	@特征3	,
				A.特征4	=	@特征4	,
				A.特征5	=	@特征5	,
				A.特征6	=	@特征6	,
				A.特征7	=	@特征7	,
				A.特征8	=	@特征8	,
				A.特征9	=	@特征9	,
				A.特征10	=	@特征10	,
				A.特征11	=	@特征11	,
				A.特征12	=	@特征12	,
				A.特征13	=	@特征13	,
				A.特征14	=	@特征14	,
				A.特征15	=	@特征15	,
				A.特征16	=	@特征16	,
				A.特征17	=	@特征17	,
				A.特征18	=	@特征18	,
				A.特征19	=	@特征19	,
				A.特征20	=	@特征20	,
				A.特征21	=	@特征21	,
				A.特征22	=	@特征22	,
				A.特征23	=	@特征23	,
				A.特征24	=	@特征24	,
				A.特征25	=	@特征25	,
				A.特征26	=	@特征26	,
				A.特征27	=	@特征27	,
				A.特征28	=	@特征28	,
				A.特征29	=	@特征29	,
				A.特征30	=	@特征30	,
				A.特征31	=	@特征31	,
				A.特征32	=	@特征32	,
				A.特征33	=	@特征33	,
				A.气虚质得分	=	@气虚质得分	,
				A.气虚质辨识	=	@气虚质辨识	,
				A.气虚质指导	=	@气虚质指导	,
				A.气虚质其他	=	@气虚质其他	,
				A.阳虚质得分	=	@阳虚质得分	,
				A.阳虚质辨识	=	@阳虚质辨识	,
				A.阳虚质指导	=	@阳虚质指导	,
				A.阳虚质其他	=	@阳虚质其他	,
				A.阴虚质得分	=	@阴虚质得分	,
				A.阴虚质辨识	=	@阴虚质辨识	,
				A.阴虚质指导	=	@阴虚质指导	,
				A.阴虚质其他	=	@阴虚质其他	,
				A.痰湿质得分	=	@痰湿质得分	,
				A.痰湿质辨识	=	@痰湿质辨识	,
				A.痰湿质指导	=	@痰湿质指导	,
				A.痰湿质其他	=	@痰湿质其他	,
				A.湿热质得分	=	@湿热质得分	,
				A.湿热质辨识	=	@湿热质辨识	,
				A.湿热质指导	=	@湿热质指导	,
				A.湿热质其他	=	@湿热质其他	,
				A.血瘀质得分	=	@血瘀质得分	,
				A.血瘀质辨识	=	@血瘀质辨识	,
				A.血瘀质指导	=	@血瘀质指导	,
				A.血瘀质其他	=	@血瘀质其他	,
				A.气郁质得分	=	@气郁质得分	,
				A.气郁质辨识	=	@气郁质辨识	,
				A.气郁质指导	=	@气郁质指导	,
				A.气郁质其他	=	@气郁质其他	,
				A.特禀质得分	=	@特禀质得分	,
				A.特禀质辨识	=	@特禀质辨识	,
				A.特禀质指导	=	@特禀质指导	,
				A.特禀质其他	=	@特禀质其他	,
				A.平和质得分	=	@平和质得分	,
				A.平和质辨识	=	@平和质辨识	,
				A.平和质指导	=	@平和质指导	,
				A.平和质其他	=	@平和质其他	,
				A.发生时间	=	@发生时间	,
				A.医生签名	=	@医生签名	
		FROM tb_老年人中医药特征管理 A
		WHERE A.身份证号 = @身份证号

		END
	END
GO
/****** Object:  StoredProcedure [dbo].[USP_SAVE_tb_老年人生活自理能力评价]    Script Date: 04/25/2016 11:58:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------------------------------------ 
--功能：添加或更新【tb_老年人生活自理能力评价】表中的数据 
--说明： 
--创建日期：【2016-04-14 14:20:36】 
------------------------------------ 
CREATE PROCEDURE [dbo].[USP_SAVE_tb_老年人生活自理能力评价]
( 
  @身份证号 VARCHAR(20) , -- 
  @姓名 VARCHAR(20) , -- 
  @性别 VARCHAR(2) , -- 
  @出生日期 VARCHAR(20) , -- 
  @居住地址 VARCHAR(20) , -- 
  @个人档案编号 VARCHAR(20) = NULL, -- 
  @G_ZZBS VARCHAR(50) = NULL, -- 
  @G_ZZXZZ VARCHAR(50)= NULL , -- 
  @G_ZZCX VARCHAR(50) = NULL, -- 
  @G_ZZZZ VARCHAR(50) = NULL, -- 
  @G_ZTH VARCHAR(50) = NULL, -- 
  @G_ZTYY VARCHAR(50)= NULL , -- 
  @G_ZTZD VARCHAR(50)= NULL , -- 
  @G_ZTZZ VARCHAR(50)= NULL , -- 
  @G_SFTZ DECIMAL(5) = NULL, -- 
  @G_RXY INT = NULL, -- 
  @G_RYJ DECIMAL(5) = NULL, -- 
  @G_ZYD INT = NULL, -- 
  @G_YDSJ DECIMAL(5) = NULL, -- 
  @G_YS VARCHAR(20) = NULL, -- 
  @G_XLTZ VARCHAR(100) = NULL, -- 
  @G_ZYXW VARCHAR(20)= NULL , -- 
  @G_YMJZ VARCHAR(100)= NULL , -- 
  @G_GXBYF VARCHAR(100)= NULL , -- 
  @G_GZSSYF VARCHAR(100)= NULL , -- 
  @下次随访目标 VARCHAR(100) = NULL, -- 
  @下次随访日期 VARCHAR(80)= NULL , -- 
  @随访医生 VARCHAR(20) = NULL, -- 
  @创建机构 VARCHAR(20)= NULL , -- 
  @创建人 VARCHAR(20)= NULL, -- 
  @更新人 VARCHAR(20)= NULL, -- 
  @所属机构 VARCHAR(20)= NULL , -- 
  @创建时间 VARCHAR(20)= NULL , -- 
  @更新时间 VARCHAR(20)= NULL , -- 
  @随访日期 VARCHAR(20)= NULL , -- 
  @缺项 VARCHAR(4) = NULL, -- 
  @进餐评分 VARCHAR(20) = NULL, -- 
  @梳洗评分 VARCHAR(20) = NULL, -- 
  @如厕评分 VARCHAR(20) = NULL, -- 
  @活动评分 VARCHAR(20) = NULL, -- 
  @总评分 VARCHAR(20) = NULL, -- 
  @穿衣评分 VARCHAR(20) = NULL, -- 
  @随访次数 VARCHAR(20) = NULL, -- 
  @完整度 VARCHAR(20)  = NULL--  
    ,@_BlueAddr		varchar(20)
	  ,@_BlueType		varchar(20)
      ,@_BlueName		varchar(20)
) 
AS 
SET NOCOUNT ON 
	IF NOT EXISTS(SELECT 1 FROM [dbo].[tb_老年人生活自理能力评价] WHERE 身份证号 = @身份证号)
	begin
	
   BEGIN TRANSACTION 
 
  INSERT INTO tb_老年人生活自理能力评价 
  ( 
    身份证号 , --  
    姓名 , --  
    性别 , --  
    出生日期 , --  
    居住地址 , --  
    个人档案编号 , --  
    G_ZZBS , --  
    G_ZZXZZ , --  
    G_ZZCX , --  
    G_ZZZZ , --  
    G_ZTH , --  
    G_ZTYY , --  
    G_ZTZD , --  
    G_ZTZZ , --  
    G_SFTZ , --  
    G_RXY , --  
    G_RYJ , --  
    G_ZYD , --  
    G_YDSJ , --  
    G_YS , --  
    G_XLTZ , --  
    G_ZYXW , --  
    G_YMJZ , --  
    G_GXBYF , --  
    G_GZSSYF , --  
    下次随访目标 , --  
    下次随访日期 , --  
    随访医生 , --  
    创建机构 , --  
    创建人 , --  
    更新人 , --  
    所属机构 , --  
    创建时间 , --  
    更新时间 , --  
    随访日期 , --  
    缺项 , --  
    进餐评分 , --  
    梳洗评分 , --  
    如厕评分 , --  
    活动评分 , --  
    总评分 , --  
    穿衣评分 , --  
    随访次数 , --  
    完整度   --  
	  ,BlueAddr	
	  ,BlueType	
      ,BlueName	
  ) 
  VALUES 
  ( 
    @身份证号 , 
    @姓名 , 
    @性别 , 
    @出生日期 , 
    @居住地址 , 
    @个人档案编号 , 
    @G_ZZBS , 
    @G_ZZXZZ , 
    @G_ZZCX , 
    @G_ZZZZ , 
    @G_ZTH , 
    @G_ZTYY , 
    @G_ZTZD , 
    @G_ZTZZ , 
    @G_SFTZ , 
    @G_RXY , 
    @G_RYJ , 
    @G_ZYD , 
    @G_YDSJ , 
    @G_YS , 
    @G_XLTZ , 
    @G_ZYXW , 
    @G_YMJZ , 
    @G_GXBYF , 
    @G_GZSSYF , 
    @下次随访目标 , 
    @下次随访日期 , 
    @随访医生 , 
    @创建机构 , 
    @创建人 , 
    @更新人 , 
    @所属机构 , 
    @创建时间 , 
    @更新时间 , 
    @随访日期 , 
    @缺项 , 
    @进餐评分 , 
    @梳洗评分 , 
    @如厕评分 , 
    @活动评分 , 
    @总评分 , 
    @穿衣评分 , 
    @随访次数 , 
    @完整度  
	  ,@_BlueAddr		
	  ,@_BlueType		
      ,@_BlueName		
  ); 
 
     IF @@error = 0 BEGIN 
         COMMIT TRANSACTION 
         SELECT 0 , '执行成功' 
         RETURN 
     END 
     ELSE BEGIN 
         ROLLBACK TRANSACTION 
         SELECT 1 , '执行失败' 
         RETURN 
     END 
end
else --update
begin

	 BEGIN TRANSACTION 
 
  UPDATE tb_老年人生活自理能力评价 SET 
    身份证号 = @身份证号 , --  
    姓名 = @姓名 , --  
    性别 = @性别 , --  
    出生日期 = @出生日期 , --  
    居住地址 = @居住地址 , --  
    个人档案编号 = @个人档案编号 , --  
    G_ZZBS = @G_ZZBS , --  
    G_ZZXZZ = @G_ZZXZZ , --  
    G_ZZCX = @G_ZZCX , --  
    G_ZZZZ = @G_ZZZZ , --  
    G_ZTH = @G_ZTH , --  
    G_ZTYY = @G_ZTYY , --  
    G_ZTZD = @G_ZTZD , --  
    G_ZTZZ = @G_ZTZZ , --  
    G_SFTZ = @G_SFTZ , --  
    G_RXY = @G_RXY , --  
    G_RYJ = @G_RYJ , --  
    G_ZYD = @G_ZYD , --  
    G_YDSJ = @G_YDSJ , --  
    G_YS = @G_YS , --  
    G_XLTZ = @G_XLTZ , --  
    G_ZYXW = @G_ZYXW , --  
    G_YMJZ = @G_YMJZ , --  
    G_GXBYF = @G_GXBYF , --  
    G_GZSSYF = @G_GZSSYF , --  
    下次随访目标 = @下次随访目标 , --  
    下次随访日期 = @下次随访日期 , --  
    随访医生 = @随访医生 , --  
    创建机构 = @创建机构 , --  
    创建人 = @创建人 , --  
    更新人 = @更新人 , --  
    所属机构 = @所属机构 , --  
    创建时间 = @创建时间 , --  
    更新时间 = @更新时间 , --  
    随访日期 = @随访日期 , --  
    缺项 = @缺项 , --  
    进餐评分 = @进餐评分 , --  
    梳洗评分 = @梳洗评分 , --  
    如厕评分 = @如厕评分 , --  
    活动评分 = @活动评分 , --  
    总评分 = @总评分 , --  
    穿衣评分 = @穿衣评分 , --  
    随访次数 = @随访次数 , --  
    完整度 = @完整度   --  
 WHERE 身份证号 = @身份证号 ; 
 
     IF @@error = 0 BEGIN 
         COMMIT TRANSACTION 
         SELECT 0 , '执行成功' 
         RETURN 
     END 
     ELSE BEGIN 
         ROLLBACK TRANSACTION 
         SELECT 1 , '执行失败' 
         RETURN 
     END 
end
 RETURN
GO
/****** Object:  StoredProcedure [dbo].[USP_import中医药体质辨识TO121]    Script Date: 04/25/2016 11:58:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_import中医药体质辨识TO121]
	@个人档案编号	varchar(20),
	--@身份证号	varchar(18),
	--@姓名		nvarchar(20),
	--@性别		nvarchar(2),
	--@出生日期	varchar(20),
	--@居住地址	NVARCHAR(200),
	@创建机构	VARCHAR(20)			=	NULL,
	@创建人		VARCHAR(20)			=	NULL,
	@修改人		VARCHAR(20)			=	NULL,
	@所属机构		VARCHAR(20)			=	NULL,
	@创建时间		VARCHAR(20)			=	NULL,
	@修改时间		VARCHAR(20)			=	NULL,
	@特征1		INT			=	NULL,
	@特征2		INT			=	NULL,
	@特征3		INT			=	NULL,
	@特征4		INT			=	NULL,
	@特征5		INT			=	NULL,
	@特征6		INT			=	NULL,
	@特征7		INT			=	NULL,
	@特征8		INT			=	NULL,
	@特征9		INT			=	NULL,
	@特征10		INT			=	NULL,
	@特征11		INT			=	NULL,
	@特征12		INT			=	NULL,
	@特征13		INT			=	NULL,
	@特征14		INT			=	NULL,
	@特征15		INT			=	NULL,
	@特征16		INT			=	NULL,
	@特征17		INT			=	NULL,
	@特征18		INT			=	NULL,
	@特征19		INT			=	NULL,
	@特征20		INT			=	NULL,
	@特征21		INT			=	NULL,
	@特征22		INT			=	NULL,
	@特征23		INT			=	NULL,
	@特征24		INT			=	NULL,
	@特征25		INT			=	NULL,
	@特征26		INT			=	NULL,
	@特征27		INT			=	NULL,
	@特征28		INT			=	NULL,
	@特征29		INT			=	NULL,
	@特征30		INT			=	NULL,
	@特征31		INT			=	NULL,
	@特征32		INT			=	NULL,
	@特征33		INT			=	NULL,
	@气虚质得分	INT			=	NULL,
	@气虚质辨识	varchar(2)			=	NULL,
	@气虚质指导	VARCHAR(20)			=	NULL,
	@气虚质其他	VARCHAR(200)			=	NULL,
	@阳虚质得分	INT			=	NULL,
	@阳虚质辨识	varchar(2)			=	NULL,
	@阳虚质指导	VARCHAR(20)			=	NULL,
	@阳虚质其他	VARCHAR(200)			=	NULL,
	@阴虚质得分	INT			=	NULL,
	@阴虚质辨识	varchar(2)			=	NULL,
	@阴虚质指导	VARCHAR(20)			=	NULL,
	@阴虚质其他	VARCHAR(200)			=	NULL,
	@痰湿质得分	INT			=	NULL,
	@痰湿质辨识	varchar(2)			=	NULL,
	@痰湿质指导	VARCHAR(20)			=	NULL,
	@痰湿质其他	VARCHAR(200)			=	NULL,
	@湿热质得分	INT			=	NULL,
	@湿热质辨识	varchar(2)			=	NULL,
	@湿热质指导	VARCHAR(20)			=	NULL,
	@湿热质其他	VARCHAR(200)			=	NULL,
	@血瘀质得分	INT			=	NULL,
	@血瘀质辨识	varchar(2)			=	NULL,
	@血瘀质指导	VARCHAR(20)			=	NULL,
	@血瘀质其他	VARCHAR(200)			=	NULL,
	@气郁质得分	INT			=	NULL,
	@气郁质辨识	varchar(2)			=	NULL,
	@气郁质指导	VARCHAR(20)			=	NULL,
	@气郁质其他	VARCHAR(200)			=	NULL,
	@特禀质得分	INT			=	NULL,
	@特禀质辨识	varchar(2)			=	NULL,
	@特禀质指导	VARCHAR(20)			=	NULL,
	@特禀质其他	VARCHAR(200)			=	NULL,
	@平和质得分	INT			=	NULL,
	@平和质辨识	varchar(2)			=	NULL,
	@平和质指导	VARCHAR(20)			=	NULL,
	@平和质其他	VARCHAR(200)			=	NULL,
	@发生时间	VARCHAR(20)			=	NULL,
	@医生签名	VARCHAR(20)			=	NULL
AS	
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT 1 FROM  [192.168.10.121].[AtomEHR.YSDB01].[DBO].[tb_老年人中医药特征管理] 
		WHERE 个人档案编号 = @个人档案编号 and substring(创建时间,1,10) =SUBSTRING( @创建时间,1,10))--不存在，插入
	BEGIN
		INSERT INTO [192.168.10.121].[AtomEHR.YSDB01].[DBO].[tb_老年人中医药特征管理] 
			(
			个人档案编号	,
			创建机构	,
			创建人		,
			修改人		,
			所属机构	,
			创建时间	,
			修改时间	,
			特征1	,
			特征2	,
			特征3	,
			特征4	,
			特征5	,
			特征6	,
			特征7	,
			特征8	,
			特征9	,
			特征10	,
			特征11	,
			特征12	,
			特征13	,
			特征14	,
			特征15	,
			特征16	,
			特征17	,
			特征18	,
			特征19	,
			特征20	,
			特征21	,
			特征22	,
			特征23	,
			特征24	,
			特征25	,
			特征26	,
			特征27	,
			特征28	,
			特征29	,
			特征30	,
			特征31	,
			特征32	,
			特征33	,
			气虚质得分	,
			气虚质辨识	,
			气虚质指导	,
			气虚质其他	,
			阳虚质得分	,
			阳虚质辨识	,
			阳虚质指导	,
			阳虚质其他	,
			阴虚质得分	,
			阴虚质辨识	,
			阴虚质指导	,
			阴虚质其他	,
			痰湿质得分	,
			痰湿质辨识	,
			痰湿质指导	,
			痰湿质其他	,
			湿热质得分	,
			湿热质辨识	,
			湿热质指导	,
			湿热质其他	,
			血瘀质得分	,
			血瘀质辨识	,
			血瘀质指导	,
			血瘀质其他	,
			气郁质得分	,
			气郁质辨识	,
			气郁质指导	,
			气郁质其他	,
			特禀质得分	,
			特禀质辨识	,
			特禀质指导	,
			特禀质其他	,
			平和质得分	,
			平和质辨识	,
			平和质指导	,
			平和质其他	,
			发生时间	,
			医生签名	)
		VALUES (
			@个人档案编号	,
			@创建机构	,
			@创建人		,
			@修改人		,
			@所属机构	,
			@创建时间	,
			@修改时间	,
			@特征1	,
			@特征2	,
			@特征3	,
			@特征4	,
			@特征5	,
			@特征6	,
			@特征7	,
			@特征8	,
			@特征9	,
			@特征10	,
			@特征11	,
			@特征12	,
			@特征13	,
			@特征14	,
			@特征15	,
			@特征16	,
			@特征17	,
			@特征18	,
			@特征19	,
			@特征20	,
			@特征21	,
			@特征22	,
			@特征23	,
			@特征24	,
			@特征25	,
			@特征26	,
			@特征27	,
			@特征28	,
			@特征29	,
			@特征30	,
			@特征31	,
			@特征32	,
			@特征33	,
			@气虚质得分	,
			@气虚质辨识	,
			@气虚质指导	,
			@气虚质其他	,
			@阳虚质得分	,
			@阳虚质辨识	,
			@阳虚质指导	,
			@阳虚质其他	,
			@阴虚质得分	,
			@阴虚质辨识	,
			@阴虚质指导	,
			@阴虚质其他	,
			@痰湿质得分	,
			@痰湿质辨识	,
			@痰湿质指导	,
			@痰湿质其他	,
			@湿热质得分	,
			@湿热质辨识	,
			@湿热质指导	,
			@湿热质其他	,
			@血瘀质得分	,
			@血瘀质辨识	,
			@血瘀质指导	,
			@血瘀质其他	,
			@气郁质得分	,
			@气郁质辨识	,
			@气郁质指导	,
			@气郁质其他	,
			@特禀质得分	,
			@特禀质辨识	,
			@特禀质指导	,
			@特禀质其他	,
			@平和质得分	,
			@平和质辨识	,
			@平和质指导	,
			@平和质其他	,
			@发生时间	,
			@医生签名	)
			--更新 中医辨识体质
			update [192.168.10.121].[AtomEHR.YSDB01].[DBO].[tb_健康体检] 
			set 平和质 = @平和质辨识,气虚质 = @气虚质辨识
			,阳虚质 = @阳虚质辨识,阴虚质 = @阴虚质辨识,痰湿质 = @痰湿质辨识,湿热质 = @湿热质辨识
			,血瘀质 = @血瘀质辨识,气郁质 = @气郁质辨识,特禀质 = @特禀质辨识
			where 个人档案编号 = @个人档案编号 and substring(创建时间,1,10) =SUBSTRING( @创建时间,1,10)

			update tb_老年人中医药特征管理 
			set RowState = '1' where 个人档案编号 = @个人档案编号 and substring(创建时间,1,10) =SUBSTRING( @创建时间,1,10)
	END
	ELSE --//更新
	BEGIN
		UPDATE A SET 
				A.创建机构	=	@创建机构	,
				A.创建人	=		@创建人		,
				A.修改人	=		@修改人		,
				A.所属机构	=	@所属机构	,
				A.创建时间	=	@创建时间	,
				A.修改时间	=	@修改时间	,
				A.特征1	=	@特征1	,
				A.特征2	=	@特征2	,
				A.特征3	=	@特征3	,
				A.特征4	=	@特征4	,
				A.特征5	=	@特征5	,
				A.特征6	=	@特征6	,
				A.特征7	=	@特征7	,
				A.特征8	=	@特征8	,
				A.特征9	=	@特征9	,
				A.特征10	=	@特征10	,
				A.特征11	=	@特征11	,
				A.特征12	=	@特征12	,
				A.特征13	=	@特征13	,
				A.特征14	=	@特征14	,
				A.特征15	=	@特征15	,
				A.特征16	=	@特征16	,
				A.特征17	=	@特征17	,
				A.特征18	=	@特征18	,
				A.特征19	=	@特征19	,
				A.特征20	=	@特征20	,
				A.特征21	=	@特征21	,
				A.特征22	=	@特征22	,
				A.特征23	=	@特征23	,
				A.特征24	=	@特征24	,
				A.特征25	=	@特征25	,
				A.特征26	=	@特征26	,
				A.特征27	=	@特征27	,
				A.特征28	=	@特征28	,
				A.特征29	=	@特征29	,
				A.特征30	=	@特征30	,
				A.特征31	=	@特征31	,
				A.特征32	=	@特征32	,
				A.特征33	=	@特征33	,
				A.气虚质得分	=	@气虚质得分	,
				A.气虚质辨识	=	@气虚质辨识	,
				A.气虚质指导	=	@气虚质指导	,
				A.气虚质其他	=	@气虚质其他	,
				A.阳虚质得分	=	@阳虚质得分	,
				A.阳虚质辨识	=	@阳虚质辨识	,
				A.阳虚质指导	=	@阳虚质指导	,
				A.阳虚质其他	=	@阳虚质其他	,
				A.阴虚质得分	=	@阴虚质得分	,
				A.阴虚质辨识	=	@阴虚质辨识	,
				A.阴虚质指导	=	@阴虚质指导	,
				A.阴虚质其他	=	@阴虚质其他	,
				A.痰湿质得分	=	@痰湿质得分	,
				A.痰湿质辨识	=	@痰湿质辨识	,
				A.痰湿质指导	=	@痰湿质指导	,
				A.痰湿质其他	=	@痰湿质其他	,
				A.湿热质得分	=	@湿热质得分	,
				A.湿热质辨识	=	@湿热质辨识	,
				A.湿热质指导	=	@湿热质指导	,
				A.湿热质其他	=	@湿热质其他	,
				A.血瘀质得分	=	@血瘀质得分	,
				A.血瘀质辨识	=	@血瘀质辨识	,
				A.血瘀质指导	=	@血瘀质指导	,
				A.血瘀质其他	=	@血瘀质其他	,
				A.气郁质得分	=	@气郁质得分	,
				A.气郁质辨识	=	@气郁质辨识	,
				A.气郁质指导	=	@气郁质指导	,
				A.气郁质其他	=	@气郁质其他	,
				A.特禀质得分	=	@特禀质得分	,
				A.特禀质辨识	=	@特禀质辨识	,
				A.特禀质指导	=	@特禀质指导	,
				A.特禀质其他	=	@特禀质其他	,
				A.平和质得分	=	@平和质得分	,
				A.平和质辨识	=	@平和质辨识	,
				A.平和质指导	=	@平和质指导	,
				A.平和质其他	=	@平和质其他	,
				A.发生时间	=	@发生时间	,
				A.医生签名	=	@医生签名	
		FROM [192.168.10.121].[AtomEHR.YSDB01].[DBO].[tb_老年人中医药特征管理]  A
		WHERE A.个人档案编号 = @个人档案编号
				--更新 中医辨识体质
			update [192.168.10.121].[AtomEHR.YSDB01].[DBO].[tb_健康体检] 
			set 平和质 = @平和质辨识,气虚质 = @气虚质辨识
			,阳虚质 = @阳虚质辨识,阴虚质 = @阴虚质辨识,痰湿质 = @痰湿质辨识,湿热质 = @湿热质辨识
			,血瘀质 = @血瘀质辨识,气郁质 = @气郁质辨识,特禀质 = @特禀质辨识
			where 个人档案编号 = @个人档案编号 and substring(创建时间,1,10) =SUBSTRING( @创建时间,1,10)
			update tb_老年人中医药特征管理 

			set RowState = '1' where 个人档案编号 = @个人档案编号 and substring(创建时间,1,10) =SUBSTRING( @创建时间,1,10)
		END
	END
GO
/****** Object:  StoredProcedure [dbo].[USP_IMPORT健康体检TO121]    Script Date: 04/25/2016 11:58:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_IMPORT健康体检TO121]
	--@身份证号 VARCHAR(18) , -- 
 -- @姓名 NVARCHAR(40) , -- 
 -- @性别 NVARCHAR(4) , -- 
 -- @出生日期 VARCHAR(20) , -- 
  @症状 VARCHAR(150) =  '1' , --无症状 
  @体温 DECIMAL(5) =  NULL , -- 
  @呼吸 INT =  NULL  , -- 
  @脉搏 INT  =  NULL , -- 
  @血压右侧1 VARCHAR(20)=  NULL  , -- 
  @血压右侧2 VARCHAR(20)=  NULL  , -- 
  @血压左侧1 VARCHAR(20)=  NULL  , -- 
  @血压左侧2 VARCHAR(20)=  NULL  , -- 
  @身高 DECIMAL(5)=  NULL  , -- 
  @腰围 DECIMAL(5)=  NULL  , -- 
  @体重 DECIMAL(5)=  NULL  , -- 
  @体重指数 VARCHAR(20) =  NULL , -- 
  @老年人认知 VARCHAR(20)=  NULL  , -- 
  @老年人情感 VARCHAR(20)=  NULL  , -- 
  @老年人认知分 VARCHAR(20) =  NULL , -- 
  @老年人情感分 VARCHAR(20) =  NULL , -- 
  @左眼视力 DECIMAL(5)=  NULL  , -- 
  @右眼视力 DECIMAL(5) =  NULL , -- 
  @左眼矫正 VARCHAR(20) =  NULL , -- 
  @右眼矫正 VARCHAR(20) =  NULL , -- 
  @听力 VARCHAR(150) =  '1' , -- 
  @运动功能 VARCHAR(150) =  '1' , -- 
  @皮肤 VARCHAR(20) =  '1' , -- 
  @淋巴结 VARCHAR(20) =  '1' , -- 
  @淋巴结其他 VARCHAR(150) =  NULL , -- 
  @桶状胸 VARCHAR(20)=  '2'  , -- 
  @呼吸音 VARCHAR(20)=  '1'  , -- 
  @呼吸音异常 VARCHAR(150)=  NULL  , -- 
  @罗音 VARCHAR(20)=  '1'  , -- 
  @罗音异常 VARCHAR(150) =  NULL , -- 
  @心率 INT =  NULL , -- 
  @心律 VARCHAR(20)=  '1'  , -- 
  @杂音 VARCHAR(20)=  '1'  , -- 
  @杂音有 VARCHAR(150) =  NULL , -- 
  @压痛 VARCHAR(20)=  '1'  , -- 
  @压痛有 VARCHAR(150) =  NULL , -- 
  @包块 VARCHAR(20) =  '1' , -- 
  @包块有 VARCHAR(150) =  NULL , -- 
  @肝大 VARCHAR(20)=  '1'  , -- 
  @肝大有 VARCHAR(150)=  NULL  , -- 
  @脾大 VARCHAR(20)=  '1'  , -- 
  @脾大有 VARCHAR(150) =  NULL , -- 
  @浊音 VARCHAR(20) =  '1' , -- 
  @浊音有 VARCHAR(150) =  NULL , -- 
  @下肢水肿 VARCHAR(20)=  '1'  , -- 
  @肛门指诊 VARCHAR(20) =  NULL , -- 
  @肛门指诊异常 VARCHAR(150)=  NULL  , -- 
  @G_QLX VARCHAR(20)=  NULL  , -- 
  @查体其他 VARCHAR(150)=  NULL  , -- 
  @白细胞 VARCHAR(20) =  NULL , -- 
  @血红蛋白 VARCHAR(20)=  NULL  , -- 
  @血小板 VARCHAR(20)=  NULL  , -- 
  @血常规其他 VARCHAR(150)=  NULL  , -- 
  @尿蛋白 VARCHAR(20)=  NULL  , -- 
  @尿糖 VARCHAR(20) =  NULL , -- 
  @尿酮体 VARCHAR(20)=  NULL  , -- 
  @尿潜血 VARCHAR(20)=  NULL  , -- 
  @尿常规其他 VARCHAR(150)=  NULL  , -- 
  @大便潜血 VARCHAR(20)=  NULL  , -- 
  @血清谷丙转氨酶 VARCHAR(20)=  NULL , -- 
  @血清谷草转氨酶 VARCHAR(20)=  NULL , -- 
  @白蛋白 VARCHAR(20)=  NULL  , -- 
  @总胆红素 VARCHAR(20)=  NULL  , -- 
  @结合胆红素 VARCHAR(20)=  NULL  , -- 
  @血清肌酐 VARCHAR(20)=  NULL  , -- 
  @血尿素氮 VARCHAR(20)=  NULL  , -- 
  @总胆固醇 VARCHAR(20)=  NULL  , -- 
  @甘油三酯 VARCHAR(20)=  NULL  , -- 
  @血清低密度脂蛋白胆固醇 VARCHAR(20)=  NULL , -- 
  @血清高密度脂蛋白胆固醇 VARCHAR(20)=  NULL , -- 
  @空腹血糖 VARCHAR(20)=  NULL  , -- 
  @乙型肝炎表面抗原 VARCHAR(20)=  NULL  , -- 
  @眼底 VARCHAR(8) =  NULL , -- 
  @眼底异常 VARCHAR(150)=  NULL  , -- 
  @心电图 VARCHAR(8)=  NULL  , -- 
  @心电图异常 VARCHAR(300) =  NULL , -- 
  @胸部X线片 VARCHAR(8) =  NULL , -- 
  @胸部X线片异常 VARCHAR(300) =  NULL , -- 
  @B超 VARCHAR(8) =  NULL , -- 
  @B超其他 VARCHAR(300)=  NULL  , -- 
  @辅助检查其他 VARCHAR(150)=  NULL  , -- 
  @G_JLJJY VARCHAR(150) =  NULL , -- 
  @个人档案编号 VARCHAR(20)=  NULL  , -- 
  @创建机构 VARCHAR(20) =  NULL , -- 
  @创建时间 VARCHAR(20) =  NULL , -- 
  @创建人 VARCHAR(20) =  NULL , -- 
  @修改时间 VARCHAR(20)=  NULL  , -- 
  @修改人 VARCHAR(20)=  NULL  , -- 
  @体检日期 VARCHAR(20) =  NULL , -- 
  @所属机构 VARCHAR(20)=  NULL  , -- 
  @FIELD1 VARCHAR(20) =  NULL , -- 
  @FIELD2 VARCHAR(20) =  NULL , -- 
  @FIELD3 VARCHAR(20) =  NULL , -- 
  @FIELD4 VARCHAR(20) =  NULL , -- 
  @WBC_SUP INT =  NULL , -- 
  @PLT_SUP INT =  NULL , -- 
  @G_TUNWEI VARCHAR(20) =  NULL , -- 
  @G_YTWBZ VARCHAR(20)=  NULL  , -- 
  @锻炼频率 VARCHAR(20) =  '4' , -- 
  @每次锻炼时间 VARCHAR(20)=  NULL  , -- 
  @坚持锻炼时间 VARCHAR(20)=  NULL  , -- 
  @锻炼方式 VARCHAR(150)=  NULL  , -- 
  @饮食习惯 VARCHAR(20)=  '1' , -- 
  @吸烟状况 VARCHAR(20)=  '1' , -- 
  @日吸烟量 VARCHAR(20)=  NULL , -- 
  @开始吸烟年龄 VARCHAR(20) =  NULL , -- 
  @戒烟年龄 VARCHAR(20) =  NULL , -- 
  @饮酒频率 VARCHAR(20) =  '1' , -- 
  @日饮酒量 VARCHAR(20) =  NULL , -- 
  @是否戒酒 VARCHAR(20) =  '1' , -- 
  @戒酒年龄 VARCHAR(20)=  NULL  , -- 
  @开始饮酒年龄 VARCHAR(20)=  NULL  , -- 
  @近一年内是否曾醉酒 VARCHAR(20) =  NULL , -- 
  @饮酒种类 VARCHAR(20) =  NULL , -- 
  @饮酒种类其它 VARCHAR(20)=  NULL  , -- 
  @有无职业病 VARCHAR(20)=  '1'  , -- 
  @具体职业 VARCHAR(20) =  NULL , -- 
  @从业时间 VARCHAR(20) =  NULL , -- 
  @化学物质 VARCHAR(150) =  NULL , -- 
  @化学物质防护 VARCHAR(20) =  NULL , -- 
  @化学物质具体防护 VARCHAR(150)=  NULL  , -- 
  @毒物 VARCHAR(150) =  NULL , -- 
  @G_DWFHCS VARCHAR(20) =  NULL , -- 
  @G_DWFHCSQT VARCHAR(150) =  NULL , -- 
  @放射物质 VARCHAR(150) =  NULL , -- 
  @放射物质防护措施有无 VARCHAR(20)=  NULL  , -- 
  @放射物质防护措施其他 VARCHAR(150) =  NULL , -- 
  @口唇 VARCHAR(20)=  '1' , -- 
  @齿列 VARCHAR(20)=  '1' , -- 
  @咽部 VARCHAR(20)=  '1' , -- 
  @皮肤其他 VARCHAR(150)=  NULL  , -- 
  @巩膜 VARCHAR(20) =  '1' , -- 
  @巩膜其他 VARCHAR(150)=  NULL  , -- 
  @足背动脉搏动 VARCHAR(20)=  '2'  , -- 
  @乳腺 VARCHAR(100)=  NULL  , -- 
  @乳腺其他 VARCHAR(150)=  NULL  , -- 
  @外阴 VARCHAR(20) =  NULL , -- 
  @外阴异常 VARCHAR(150)=  NULL  , -- 
  @阴道 VARCHAR(20)=  NULL  , -- 
  @阴道异常 VARCHAR(150) =  NULL , -- 
  @宫颈 VARCHAR(20)=  NULL  , -- 
  @宫颈异常 VARCHAR(150)=  NULL  , -- 
  @宫体 VARCHAR(20) =  NULL , -- 
  @宫体异常 VARCHAR(150) =  NULL , -- 
  @附件 VARCHAR(20) =  NULL , -- 
  @附件异常 VARCHAR(150)=  NULL  , -- 
  @血钾浓度 VARCHAR(20)=  NULL  , -- 
  @血钠浓度 VARCHAR(20)=  NULL  , -- 
  @糖化血红蛋白 VARCHAR(20) =  NULL , -- 
  @宫颈涂片 VARCHAR(20) =  NULL , -- 
  @宫颈涂片异常 VARCHAR(300) =  NULL , -- 
  @平和质 VARCHAR(20)=  NULL , -- 
  @气虚质 VARCHAR(20)=  NULL , -- 
  @阳虚质 VARCHAR(20)=  NULL , -- 
  @阴虚质 VARCHAR(20)=  NULL , -- 
  @痰湿质 VARCHAR(20)=  NULL , -- 
  @湿热质 VARCHAR(20)=  NULL , -- 
  @血瘀质 VARCHAR(20)=  NULL , -- 
  @气郁质 VARCHAR(20)=  NULL , -- 
  @特禀质 VARCHAR(20)=  NULL , -- 
  @脑血管疾病 VARCHAR(20) =  '1' , -- 
  @脑血管疾病其他 VARCHAR(150) =  NULL , -- 
  @肾脏疾病 VARCHAR(20)=  '1'  , -- 
  @肾脏疾病其他 VARCHAR(150)=  NULL  , -- 
  @心脏疾病 VARCHAR(20) =  '1' , -- 
  @心脏疾病其他 VARCHAR(150)=  NULL  , -- 
  @血管疾病 VARCHAR(20) =  '1' , -- 
  @血管疾病其他 VARCHAR(150) =  NULL , -- 
  @眼部疾病 VARCHAR(20) =  '1' , -- 
  @眼部疾病其他 VARCHAR(150) =  NULL , -- 
  @神经系统疾病 VARCHAR(20) =  '1' , -- 
  @神经系统疾病其他 VARCHAR(150) =  NULL , -- 
  @其他系统疾病 VARCHAR(20) =  '1' , -- 
  @其他系统疾病其他 VARCHAR(150)=  NULL  , -- 
  @健康评价 VARCHAR(20) =  '1' , -- 
  @健康评价异常1 VARCHAR(150)=  NULL  , -- 
  @健康评价异常2 VARCHAR(150)=  NULL  , -- 
  @健康评价异常3 VARCHAR(150)=  NULL  , -- 
  @健康评价异常4 VARCHAR(150)=  NULL  , -- 
  @健康指导 VARCHAR(20) =  NULL , -- 
  @危险因素控制 VARCHAR(100)=  NULL  , -- 
  @危险因素控制体重 VARCHAR(150)=  NULL  , -- 
  @危险因素控制疫苗 VARCHAR(150)=  NULL  , -- 
  @危险因素控制其他 VARCHAR(150)=  NULL  , -- 
  @FIELD5 VARCHAR(20) =  NULL , -- 
  @症状其他 VARCHAR(40)=  NULL  , -- 
  @G_XYYC VARCHAR(20) =  NULL , -- 
  @G_XYZC VARCHAR(20) =  NULL , -- 
  @G_QTZHZH VARCHAR(20)=  NULL  , -- 
  @缺项 VARCHAR(4) =  NULL , -- 
  @口唇其他 VARCHAR(150)=  NULL , -- 
  @齿列其他 VARCHAR(150)=  NULL , -- 
  @咽部其他 VARCHAR(150)=  NULL , -- 
  @YDGNQT VARCHAR(50)=  NULL  , -- 
  @餐后2H血糖 VARCHAR(20)=  NULL  , -- 
  @老年人状况评估 VARCHAR(50)=  NULL  , -- 
  @老年人自理评估 VARCHAR(50)=  NULL  , -- 
  @粉尘 VARCHAR(50) =  NULL , -- 
  @物理因素 VARCHAR(50)=  NULL  , -- 
  @职业病其他 VARCHAR(50) =  NULL , -- 
  @粉尘防护有无 VARCHAR(4)=  NULL  , -- 
  @物理防护有无 VARCHAR(4)=  NULL  , -- 
  @其他防护有无 VARCHAR(4)=  NULL  , -- 
  @粉尘防护措施 VARCHAR(50)=  NULL , -- 
  @物理防护措施 VARCHAR(50)=  NULL , -- 
  @其他防护措施 VARCHAR(50)=  NULL , -- 
  @TNBFXJF VARCHAR(10) =  NULL , -- 
  @左侧原因 VARCHAR(20)=  NULL  , -- 
  @右侧原因 VARCHAR(20)=  NULL  , -- 
  @尿微量白蛋白 VARCHAR(20) =  NULL , -- 
  @完整度 VARCHAR(20) =  NULL , -- 
  @齿列缺齿 VARCHAR(100)=  NULL  , -- 
  @齿列龋齿 VARCHAR(100)=  NULL  , -- 
  @齿列义齿 VARCHAR(100)=  NULL   --  

AS
BEGIN

--1. 体检表的保存 不存在 则插入进表；存在则更新
	IF NOT EXISTS (SELECT 1 FROM  [192.168.10.121].[AtomEHR.YSDB01].[DBO].[tb_健康体检] 
		WHERE 个人档案编号 = @个人档案编号 and substring(创建时间,1,10) =SUBSTRING( @创建时间,1,10))--不存在，插入
	BEGIN
		  BEGIN TRANSACTION 
			
				INSERT INTO [192.168.10.121].[AtomEHR.YSDB01].[DBO].[tb_健康体检] 
			  ( 
				症状 , --  
				体温 , --  
				呼吸 , --  
				脉搏 , --  
				血压右侧1 , --  
				血压右侧2 , --  
				血压左侧1 , --  
				血压左侧2 , --  
				身高 , --  
				腰围 , --  
				体重 , --  
				体重指数 , --  
				老年人认知 , --  
				老年人情感 , --  
				老年人认知分 , --  
				老年人情感分 , --  
				左眼视力 , --  
				右眼视力 , --  
				左眼矫正 , --  
				右眼矫正 , --  
				听力 , --  
				运动功能 , --  
				皮肤 , --  
				淋巴结 , --  
				淋巴结其他 , --  
				桶状胸 , --  
				呼吸音 , --  
				呼吸音异常 , --  
				罗音 , --  
				罗音异常 , --  
				心率 , --  
				心律 , --  
				杂音 , --  
				杂音有 , --  
				压痛 , --  
				压痛有 , --  
				包块 , --  
				包块有 , --  
				肝大 , --  
				肝大有 , --  
				脾大 , --  
				脾大有 , --  
				浊音 , --  
				浊音有 , --  
				下肢水肿 , --  
				肛门指诊 , --  
				肛门指诊异常 , --  
				G_QLX , --  
				查体其他 , --  
				白细胞 , --  
				血红蛋白 , --  
				血小板 , --  
				血常规其他 , --  
				尿蛋白 , --  
				尿糖 , --  
				尿酮体 , --  
				尿潜血 , --  
				尿常规其他 , --  
				大便潜血 , --  
				血清谷丙转氨酶 , --  
				血清谷草转氨酶 , --  
				白蛋白 , --  
				总胆红素 , --  
				结合胆红素 , --  
				血清肌酐 , --  
				血尿素氮 , --  
				总胆固醇 , --  
				甘油三酯 , --  
				血清低密度脂蛋白胆固醇 , --  
				血清高密度脂蛋白胆固醇 , --  
				空腹血糖 , --  
				乙型肝炎表面抗原 , --  
				眼底 , --  
				眼底异常 , --  
				心电图 , --  
				心电图异常 , --  
				胸部X线片 , --  
				胸部X线片异常 , --  
				B超 , --  
				B超其他 , --  
				辅助检查其他 , --  
				G_JLJJY , --  
				个人档案编号 , --  
				创建机构 , --  
				创建时间 , --  
				创建人 , --  
				修改时间 , --  
				修改人 , --  
				体检日期 , --  
				所属机构 , --  
				FIELD1 , --  
				FIELD2 , --  
				FIELD3 , --  
				FIELD4 , --  
				WBC_SUP , --  
				PLT_SUP , --  
				G_TUNWEI , --  
				G_YTWBZ , --  
				锻炼频率 , --  
				每次锻炼时间 , --  
				坚持锻炼时间 , --  
				锻炼方式 , --  
				饮食习惯 , --  
				吸烟状况 , --  
				日吸烟量 , --  
				开始吸烟年龄 , --  
				戒烟年龄 , --  
				饮酒频率 , --  
				日饮酒量 , --  
				是否戒酒 , --  
				戒酒年龄 , --  
				开始饮酒年龄 , --  
				近一年内是否曾醉酒 , --  
				饮酒种类 , --  
				饮酒种类其它 , --  
				有无职业病 , --  
				具体职业 , --  
				从业时间 , --  
				化学物质 , --  
				化学物质防护 , --  
				化学物质具体防护 , --  
				毒物 , --  
				G_DWFHCS , --  
				G_DWFHCSQT , --  
				放射物质 , --  
				放射物质防护措施有无 , --  
				放射物质防护措施其他 , --  
				口唇 , --  
				齿列 , --  
				咽部 , --  
				皮肤其他 , --  
				巩膜 , --  
				巩膜其他 , --  
				足背动脉搏动 , --  
				乳腺 , --  
				乳腺其他 , --  
				外阴 , --  
				外阴异常 , --  
				阴道 , --  
				阴道异常 , --  
				宫颈 , --  
				宫颈异常 , --  
				宫体 , --  
				宫体异常 , --  
				附件 , --  
				附件异常 , --  
				血钾浓度 , --  
				血钠浓度 , --  
				糖化血红蛋白 , --  
				宫颈涂片 , --  
				宫颈涂片异常 , --  
				平和质 , --  
				气虚质 , --  
				阳虚质 , --  
				阴虚质 , --  
				痰湿质 , --  
				湿热质 , --  
				血瘀质 , --  
				气郁质 , --  
				特禀质 , --  
				脑血管疾病 , --  
				脑血管疾病其他 , --  
				肾脏疾病 , --  
				肾脏疾病其他 , --  
				心脏疾病 , --  
				心脏疾病其他 , --  
				血管疾病 , --  
				血管疾病其他 , --  
				眼部疾病 , --  
				眼部疾病其他 , --  
				神经系统疾病 , --  
				神经系统疾病其他 , --  
				其他系统疾病 , --  
				其他系统疾病其他 , --  
				健康评价 , --  
				健康评价异常1 , --  
				健康评价异常2 , --  
				健康评价异常3 , --  
				健康评价异常4 , --  
				健康指导 , --  
				危险因素控制 , --  
				危险因素控制体重 , --  
				危险因素控制疫苗 , --  
				危险因素控制其他 , --  
				FIELD5 , --  
				症状其他 , --  
				G_XYYC , --  
				G_XYZC , --  
				G_QTZHZH , --  
				缺项 , --  
				口唇其他 , --  
				齿列其他 , --  
				咽部其他 , --  
				YDGNQT , --  
				餐后2H血糖 , --  
				老年人状况评估 , --  
				老年人自理评估 , --  
				粉尘 , --  
				物理因素 , --  
				职业病其他 , --  
				粉尘防护有无 , --  
				物理防护有无 , --  
				其他防护有无 , --  
				粉尘防护措施 , --  
				物理防护措施 , --  
				其他防护措施 , --  
				TNBFXJF , --  
				左侧原因 , --  
				右侧原因 , --  
				尿微量白蛋白 , --  
				完整度 , --  
				齿列缺齿 , --  
				齿列龋齿 , --  
				齿列义齿   --  
			  ) 
			  VALUES 
			  ( 
				@症状 , 
				@体温 , 
				@呼吸 , 
				@脉搏 , 
				@血压右侧1 , 
				@血压右侧2 , 
				@血压左侧1 , 
				@血压左侧2 , 
				@身高 , 
				@腰围 , 
				@体重 , 
				@体重指数 , 
				@老年人认知 , 
				@老年人情感 , 
				@老年人认知分 , 
				@老年人情感分 , 
				@左眼视力 , 
				@右眼视力 , 
				@左眼矫正 , 
				@右眼矫正 , 
				@听力 , 
				@运动功能 , 
				@皮肤 , 
				@淋巴结 , 
				@淋巴结其他 , 
				@桶状胸 , 
				@呼吸音 , 
				@呼吸音异常 , 
				@罗音 , 
				@罗音异常 , 
				@心率 , 
				@心律 , 
				@杂音 , 
				@杂音有 , 
				@压痛 , 
				@压痛有 , 
				@包块 , 
				@包块有 , 
				@肝大 , 
				@肝大有 , 
				@脾大 , 
				@脾大有 , 
				@浊音 , 
				@浊音有 , 
				@下肢水肿 , 
				@肛门指诊 , 
				@肛门指诊异常 , 
				@G_QLX , 
				@查体其他 , 
				@白细胞 , 
				@血红蛋白 , 
				@血小板 , 
				@血常规其他 , 
				@尿蛋白 , 
				@尿糖 , 
				@尿酮体 , 
				@尿潜血 , 
				@尿常规其他 , 
				@大便潜血 , 
				@血清谷丙转氨酶 , 
				@血清谷草转氨酶 , 
				@白蛋白 , 
				@总胆红素 , 
				@结合胆红素 , 
				@血清肌酐 , 
				@血尿素氮 , 
				@总胆固醇 , 
				@甘油三酯 , 
				@血清低密度脂蛋白胆固醇 , 
				@血清高密度脂蛋白胆固醇 , 
				@空腹血糖 , 
				@乙型肝炎表面抗原 , 
				@眼底 , 
				@眼底异常 , 
				@心电图 , 
				@心电图异常 , 
				@胸部X线片 , 
				@胸部X线片异常 , 
				@B超 , 
				@B超其他 , 
				@辅助检查其他 , 
				@G_JLJJY , 
				@个人档案编号 , 
				@创建机构 , 
				@创建时间 , 
				@创建人 , 
				@修改时间 , 
				@修改人 , 
				case when isnull(@体检日期,'') = '' then convert(varchar ,getdate(),120) else @体检日期 end, 
				@所属机构 , 
				@FIELD1 , 
				@FIELD2 , 
				@FIELD3 , 
				@FIELD4 , 
				@WBC_SUP , 
				@PLT_SUP , 
				@G_TUNWEI , 
				@G_YTWBZ , 
				@锻炼频率 , 
				@每次锻炼时间 , 
				@坚持锻炼时间 , 
				@锻炼方式 , 
				@饮食习惯 , 
				@吸烟状况 , 
				@日吸烟量 , 
				@开始吸烟年龄 , 
				@戒烟年龄 , 
				@饮酒频率 , 
				@日饮酒量 , 
				@是否戒酒 , 
				@戒酒年龄 , 
				@开始饮酒年龄 , 
				@近一年内是否曾醉酒 , 
				@饮酒种类 , 
				@饮酒种类其它 , 
				@有无职业病 , 
				@具体职业 , 
				@从业时间 , 
				@化学物质 , 
				@化学物质防护 , 
				@化学物质具体防护 , 
				@毒物 , 
				@G_DWFHCS , 
				@G_DWFHCSQT , 
				@放射物质 , 
				@放射物质防护措施有无 , 
				@放射物质防护措施其他 , 
				@口唇 , 
				@齿列 , 
				@咽部 , 
				@皮肤其他 , 
				@巩膜 , 
				@巩膜其他 , 
				@足背动脉搏动 , 
				@乳腺 , 
				@乳腺其他 , 
				@外阴 , 
				@外阴异常 , 
				@阴道 , 
				@阴道异常 , 
				@宫颈 , 
				@宫颈异常 , 
				@宫体 , 
				@宫体异常 , 
				@附件 , 
				@附件异常 , 
				@血钾浓度 , 
				@血钠浓度 , 
				@糖化血红蛋白 , 
				@宫颈涂片 , 
				@宫颈涂片异常 , 
				@平和质 , 
				@气虚质 , 
				@阳虚质 , 
				@阴虚质 , 
				@痰湿质 , 
				@湿热质 , 
				@血瘀质 , 
				@气郁质 , 
				@特禀质 , 
				@脑血管疾病 , 
				@脑血管疾病其他 , 
				@肾脏疾病 , 
				@肾脏疾病其他 , 
				@心脏疾病 , 
				@心脏疾病其他 , 
				@血管疾病 , 
				@血管疾病其他 , 
				@眼部疾病 , 
				@眼部疾病其他 , 
				@神经系统疾病 , 
				@神经系统疾病其他 , 
				@其他系统疾病 , 
				@其他系统疾病其他 , 
				@健康评价 , 
				@健康评价异常1 , 
				@健康评价异常2 , 
				@健康评价异常3 , 
				@健康评价异常4 , 
				@健康指导 , 
				@危险因素控制 , 
				@危险因素控制体重 , 
				@危险因素控制疫苗 , 
				@危险因素控制其他 , 
				@FIELD5 , 
				@症状其他 , 
				@G_XYYC , 
				@G_XYZC , 
				@G_QTZHZH , 
				@缺项 , 
				@口唇其他 , 
				@齿列其他 , 
				@咽部其他 , 
				@YDGNQT , 
				@餐后2H血糖 , 
				@老年人状况评估 , 
				@老年人自理评估 , 
				@粉尘 , 
				@物理因素 , 
				@职业病其他 , 
				@粉尘防护有无 , 
				@物理防护有无 , 
				@其他防护有无 , 
				@粉尘防护措施 , 
				@物理防护措施 , 
				@其他防护措施 , 
				@TNBFXJF , 
				@左侧原因 , 
				@右侧原因 , 
				@尿微量白蛋白 , 
				@完整度 , 
				@齿列缺齿 , 
				@齿列龋齿 , 
				@齿列义齿  
			  ); 
			  --更新 行状态为1，表示已上传
			  UPDATE [tb_健康体检] SET RowState = '1' WHERE 个人档案编号 = @个人档案编号 and substring(创建时间,1,10) =SUBSTRING( @创建时间,1,10)
		 IF @@error = 0 BEGIN 
			 COMMIT TRANSACTION 
			 SELECT 0 , '执行成功' 
			 RETURN 
		 END 
		 ELSE BEGIN 
			 ROLLBACK TRANSACTION 
			 SELECT 1 , '执行失败' 
			 RETURN 
		 END 
	END
	ELSE
	BEGIN

		BEGIN TRANSACTION 
 
			  UPDATE  [192.168.10.121].[AtomEHR.YSDB01].[DBO].[tb_健康体检] SET 
				症状 = CASE WHEN ISNULL( @症状,'')= '1' THEN  症状 ELSE @症状 END , --  
				体温 = CASE WHEN ISNULL( @体温,0.00)= 0.00 THEN  体温 ELSE @体温 END , --   
				呼吸 = CASE WHEN ISNULL( @呼吸,0)= 0 THEN  呼吸 ELSE @呼吸 END , --   
				脉搏 = CASE WHEN ISNULL( @脉搏,'')= '' THEN  脉搏 ELSE @脉搏 END , --   
				血压右侧1 = CASE WHEN ISNULL( @血压右侧1,'')= '' THEN  血压右侧1 ELSE @血压右侧1 END , --  
				血压右侧2 = CASE WHEN ISNULL( @血压右侧2,'')= '' THEN  血压右侧2 ELSE @血压右侧2 END , --  
				血压左侧1 = CASE WHEN ISNULL( @血压左侧1,'')= '' THEN  血压左侧1 ELSE @血压左侧1 END , --  
				血压左侧2 = CASE WHEN ISNULL( @血压左侧2,'')= '' THEN  血压左侧2 ELSE @血压左侧2 END , --  
				身高 = CASE WHEN ISNULL( @身高,0.00)= 0.00 THEN  身高 ELSE @身高 END , --  
				腰围 = CASE WHEN ISNULL( @腰围,0.00)= 0.00 THEN  腰围 ELSE @腰围 END , --  
				体重 = CASE WHEN ISNULL( @体重,0.00)= 0.00 THEN  体重 ELSE @体重 END , --  
				体重指数 = CASE WHEN ISNULL( @体重指数,'')= '' THEN  体重指数 ELSE @体重指数 END , --  
				老年人认知 = CASE WHEN ISNULL( @老年人认知,'')= '' THEN  老年人认知 ELSE @老年人认知 END , --  
				老年人情感 = CASE WHEN ISNULL( @老年人情感,'')= '' THEN  老年人情感 ELSE @老年人情感 END , --  
				老年人认知分 = CASE WHEN ISNULL( @老年人认知分,'')= '' THEN  老年人认知分 ELSE @老年人认知分 END , --  
				老年人情感分 = CASE WHEN ISNULL( @老年人情感分,'')= '' THEN  老年人情感分 ELSE @老年人情感分 END , --  
				左眼视力 =CASE WHEN ISNULL( @左眼视力,0.00)= 0.00 THEN  左眼视力 ELSE @左眼视力 END , --  
				右眼视力 =CASE WHEN ISNULL( @右眼视力,0.00)= 0.00 THEN  右眼视力 ELSE @右眼视力 END , --  
				左眼矫正 =CASE WHEN ISNULL( @左眼矫正,'')= '' THEN  左眼矫正 ELSE @左眼矫正 END , --  
				右眼矫正 =CASE WHEN ISNULL( @右眼矫正,'')= '' THEN  右眼矫正 ELSE @右眼矫正 END , --  
				听力 = CASE WHEN ISNULL( @听力,'')= '1' THEN  听力 ELSE @听力 END , --   
				运动功能 = CASE WHEN ISNULL( @运动功能,'')= '1' THEN  运动功能 ELSE @运动功能 END , --  
				皮肤 = CASE WHEN ISNULL( @皮肤,'')= '1' THEN  皮肤 ELSE @皮肤 END , --  
				淋巴结 = CASE WHEN ISNULL( @淋巴结,'')= '1' THEN  淋巴结 ELSE @淋巴结 END , --    
				淋巴结其他 = CASE WHEN ISNULL( @淋巴结其他,'')= '' THEN  淋巴结其他 ELSE @淋巴结其他 END , --  
				桶状胸 = CASE WHEN ISNULL( @桶状胸,'')= '2' THEN  桶状胸 ELSE @桶状胸 END , --  
				呼吸音 = CASE WHEN ISNULL( @呼吸音,'')= '1' THEN  呼吸音 ELSE @呼吸音 END , --  
				呼吸音异常 = CASE WHEN ISNULL( @呼吸音异常,'')= '' THEN  呼吸音异常 ELSE @呼吸音异常 END , --  
				罗音 = CASE WHEN ISNULL( @罗音,'')= '1' THEN  罗音 ELSE @罗音 END , --  
				罗音异常 = CASE WHEN ISNULL( @罗音异常,'')= '' THEN  罗音异常 ELSE @罗音异常 END , --  
				心率 = CASE WHEN ISNULL( @心率,0)= 0 THEN  心率 ELSE @心率 END , --  
				心律 = CASE WHEN ISNULL( @心律,'')= '1' THEN  心律 ELSE @心律 END , --  
				杂音 = CASE WHEN ISNULL( @杂音,'')= '1' THEN  杂音 ELSE @杂音 END , --  
				杂音有 = CASE WHEN ISNULL( @杂音有,'')= '' THEN  杂音有 ELSE @杂音有 END , --  
				压痛 = CASE WHEN ISNULL( @压痛,'')= '1' THEN  压痛 ELSE @压痛 END , --  
				压痛有 = CASE WHEN ISNULL( @压痛有,'')= '' THEN  压痛有 ELSE @压痛有 END , --  
				包块 = CASE WHEN ISNULL( @包块,'')= '1' THEN  包块 ELSE @包块 END , --  
				包块有 = CASE WHEN ISNULL( @包块有,'')=''  THEN  包块有 ELSE @包块有 END , --  
				肝大 = CASE WHEN ISNULL( @肝大,'')='1' THEN  肝大 ELSE @肝大 END , --  
				肝大有 = CASE WHEN ISNULL( @肝大有,'')= '' THEN  肝大有 ELSE @肝大有 END , --    
				脾大 =CASE WHEN ISNULL( @脾大,'')= '1' THEN  脾大 ELSE @脾大 END , --  
				脾大有 = CASE WHEN ISNULL( @脾大有,'')= '' THEN  脾大有 ELSE @脾大有 END , --  
				浊音 = CASE WHEN ISNULL( @浊音,'')= '1' THEN  浊音 ELSE @浊音 END , --  
				浊音有 = CASE WHEN ISNULL( @浊音有,'')= '' THEN  浊音有 ELSE @浊音有 END , --  
				下肢水肿 = CASE WHEN ISNULL( @下肢水肿,'')= '1' THEN  下肢水肿 ELSE @下肢水肿 END , --  
				肛门指诊 = CASE WHEN ISNULL( @肛门指诊,'')= '' THEN  肛门指诊 ELSE @肛门指诊 END , --  
				肛门指诊异常 = CASE WHEN ISNULL( @肛门指诊异常,'')= '' THEN  肛门指诊异常 ELSE @肛门指诊异常 END , --  
				G_QLX = CASE WHEN ISNULL( @G_QLX,'')= '' THEN  G_QLX ELSE @G_QLX END , --  
				查体其他 = CASE WHEN ISNULL( @查体其他,'')= '' THEN  查体其他 ELSE @查体其他 END , --    
				白细胞 = CASE WHEN ISNULL( @白细胞,'')= '' THEN  白细胞 ELSE @白细胞 END , --   
				血红蛋白 = CASE WHEN ISNULL( @血红蛋白,'')= '' THEN  血红蛋白 ELSE @血红蛋白 END , --  
				血小板 = CASE WHEN ISNULL( @血小板,'')= '' THEN  血小板 ELSE @血小板 END , --   
				血常规其他 = CASE WHEN ISNULL( @血常规其他,'')= '' THEN  血常规其他 ELSE @血常规其他 END , --  
				尿蛋白 = CASE WHEN ISNULL( @尿蛋白,'')= '' THEN  尿蛋白 ELSE @尿蛋白 END , --  
				尿糖 = CASE WHEN ISNULL( @尿糖,'')= '' THEN  尿糖 ELSE @尿糖 END , --  
				尿酮体 = CASE WHEN ISNULL( @尿酮体,'')= '' THEN  尿酮体 ELSE @尿酮体 END , --  
				尿潜血 = CASE WHEN ISNULL( @尿潜血,'')= '' THEN  尿潜血 ELSE @尿潜血 END , --  
				尿常规其他 = CASE WHEN ISNULL( @尿常规其他,'')= '' THEN  尿常规其他 ELSE @尿常规其他 END , --  
				大便潜血 =CASE WHEN ISNULL( @大便潜血,'')= '' THEN  大便潜血 ELSE @大便潜血 END , --  
				血清谷丙转氨酶 = CASE WHEN ISNULL( @血清谷丙转氨酶,'')='' THEN  血清谷丙转氨酶 ELSE @血清谷丙转氨酶 END , --   
				血清谷草转氨酶 = CASE WHEN ISNULL( @血清谷草转氨酶,'')='' THEN  血清谷草转氨酶 ELSE @血清谷草转氨酶 END , --   
				白蛋白 = CASE WHEN ISNULL( @白蛋白,'')= '' THEN  白蛋白 ELSE @白蛋白 END , --  
				总胆红素 = CASE WHEN ISNULL( @总胆红素,'')= '' THEN  总胆红素 ELSE @总胆红素 END , --  
				结合胆红素 = CASE WHEN ISNULL( @结合胆红素,'')= '' THEN  结合胆红素 ELSE @结合胆红素 END , --  
				血清肌酐 = CASE WHEN ISNULL( @血清肌酐,'')= '' THEN  血清肌酐 ELSE @血清肌酐 END , --  
				血尿素氮 = CASE WHEN ISNULL( @血尿素氮,'')= '' THEN  血尿素氮 ELSE @血尿素氮 END , --  
				总胆固醇 = CASE WHEN ISNULL( @总胆固醇,'')= '' THEN  总胆固醇 ELSE @总胆固醇 END , --  
				甘油三酯 = CASE WHEN ISNULL( @甘油三酯,'')= '' THEN  甘油三酯 ELSE @甘油三酯 END , --  
				血清低密度脂蛋白胆固醇 = CASE WHEN ISNULL( @血清低密度脂蛋白胆固醇,'')= '' THEN  血清低密度脂蛋白胆固醇 ELSE @血清低密度脂蛋白胆固醇 END , --  
				血清高密度脂蛋白胆固醇 = CASE WHEN ISNULL( @血清高密度脂蛋白胆固醇,'')= '' THEN  血清高密度脂蛋白胆固醇 ELSE @血清高密度脂蛋白胆固醇 END , --  
				空腹血糖 = CASE WHEN ISNULL( @空腹血糖,'')= '' THEN  空腹血糖 ELSE @空腹血糖 END , --  
				乙型肝炎表面抗原 = CASE WHEN ISNULL( @乙型肝炎表面抗原,'')= '' THEN  乙型肝炎表面抗原 ELSE @乙型肝炎表面抗原 END , --  
				眼底 = CASE WHEN ISNULL( @眼底,'')= '' THEN  眼底 ELSE @眼底 END , --  
				眼底异常 = CASE WHEN ISNULL( @眼底异常,'')= '' THEN  眼底异常 ELSE @眼底异常 END , --   
				心电图 = CASE WHEN ISNULL( @心电图,'')= '' THEN  心电图 ELSE @心电图 END , --   
				心电图异常 = CASE WHEN ISNULL( @心电图异常,'')= '' THEN  心电图异常 ELSE @心电图异常 END , --  
				胸部X线片 =CASE WHEN ISNULL( @胸部X线片,'')= '' THEN  胸部X线片 ELSE @胸部X线片 END , --  
				胸部X线片异常 = CASE WHEN ISNULL( @胸部X线片异常,'')= '' THEN  胸部X线片异常 ELSE @胸部X线片异常 END , --  
				B超 = CASE WHEN ISNULL( @B超,'')= '' THEN  B超 ELSE @B超 END , --  
				B超其他 = CASE WHEN ISNULL( @B超其他,'')= '' THEN  B超其他 ELSE @B超其他 END , --  
				辅助检查其他 = CASE WHEN ISNULL( @辅助检查其他,'')= '' THEN  辅助检查其他 ELSE @辅助检查其他 END , --  
				G_JLJJY = CASE WHEN ISNULL( @G_JLJJY,'')= '' THEN  G_JLJJY ELSE @G_JLJJY END , --  
				创建机构 = CASE WHEN ISNULL( @创建机构,'')= '' THEN  创建机构 ELSE @创建机构 END , --  
				创建时间 = CASE WHEN ISNULL( @创建时间,'')= '' THEN  创建时间 ELSE @创建时间 END , --  
				创建人 = CASE WHEN ISNULL( @创建人,'')= '' THEN  创建人 ELSE @创建人 END , --   
				修改时间 = CASE WHEN ISNULL( @修改时间,'')= '' THEN  修改时间 ELSE @修改时间 END , --   
				修改人 = CASE WHEN ISNULL( @修改人,'')= '' THEN  修改人 ELSE @修改人 END , --   
				体检日期 = CASE WHEN ISNULL( @体检日期,'')= '' THEN  体检日期 ELSE @体检日期 END , --  
				所属机构 = CASE WHEN ISNULL( @所属机构,'')= '' THEN  所属机构 ELSE @所属机构 END , --  
				FIELD1 = CASE WHEN ISNULL( @FIELD1,'')= '' THEN  FIELD1 ELSE @FIELD1 END , --  
				FIELD2 = CASE WHEN ISNULL( @FIELD2,'')= '' THEN  FIELD2 ELSE @FIELD2 END , --  
				FIELD3 = CASE WHEN ISNULL( @FIELD3,'')= '' THEN  FIELD3 ELSE @FIELD3 END , --  
				FIELD4 = CASE WHEN ISNULL( @FIELD4,'')= '' THEN  FIELD4 ELSE @FIELD4 END , --  
				WBC_SUP = CASE WHEN ISNULL( @WBC_SUP,'')= '' THEN  WBC_SUP ELSE @WBC_SUP END , --   
				PLT_SUP = CASE WHEN ISNULL( @PLT_SUP,'')= '' THEN  PLT_SUP ELSE @PLT_SUP END , --   
				G_TUNWEI = CASE WHEN ISNULL( @G_TUNWEI,'')= '' THEN  G_TUNWEI ELSE @G_TUNWEI END , --  
				G_YTWBZ = CASE WHEN ISNULL( @G_YTWBZ,'')= '' THEN  G_YTWBZ ELSE @G_YTWBZ END , --  
				锻炼频率 = CASE WHEN ISNULL( @锻炼频率,'')= '4' THEN  锻炼频率 ELSE @锻炼频率 END , --  
				每次锻炼时间 = CASE WHEN ISNULL( @每次锻炼时间,'')='' THEN  每次锻炼时间 ELSE @每次锻炼时间 END , --  
				坚持锻炼时间 = CASE WHEN ISNULL( @坚持锻炼时间,'')='' THEN  坚持锻炼时间 ELSE @坚持锻炼时间 END , --  
				锻炼方式 = CASE WHEN ISNULL( @锻炼方式,'')= '' THEN  锻炼方式 ELSE @锻炼方式 END , --  
				饮食习惯 = CASE WHEN ISNULL( @饮食习惯,'')= '1' THEN  饮食习惯 ELSE @饮食习惯 END , --  
				吸烟状况 = CASE WHEN ISNULL( @吸烟状况,'')= '1' THEN  吸烟状况 ELSE @吸烟状况 END , --  
				日吸烟量 = CASE WHEN ISNULL( @日吸烟量,'')= '' THEN  日吸烟量 ELSE @日吸烟量 END , --  
				开始吸烟年龄 = CASE WHEN ISNULL( @开始吸烟年龄,'')= '' THEN  开始吸烟年龄 ELSE @开始吸烟年龄 END , --   
				戒烟年龄 = CASE WHEN ISNULL( @戒烟年龄,'')='' THEN  戒烟年龄 ELSE @戒烟年龄 END , --  
				饮酒频率 = CASE WHEN ISNULL( @饮酒频率,'')='1' THEN  饮酒频率 ELSE @饮酒频率 END , --  
				日饮酒量 = CASE WHEN ISNULL( @日饮酒量,'')='' THEN  日饮酒量 ELSE @日饮酒量 END , --  
				是否戒酒 = CASE WHEN ISNULL( @是否戒酒,'')='1' THEN  是否戒酒 ELSE @是否戒酒 END , --  
				戒酒年龄 = CASE WHEN ISNULL( @戒酒年龄,'')='' THEN  戒酒年龄 ELSE @戒酒年龄 END , --  
				开始饮酒年龄 = CASE WHEN ISNULL( @开始饮酒年龄,'')= '' THEN  开始饮酒年龄 ELSE @开始饮酒年龄 END , --  
				近一年内是否曾醉酒 =CASE WHEN ISNULL( @近一年内是否曾醉酒,'')= '' THEN  近一年内是否曾醉酒 ELSE @近一年内是否曾醉酒 END , --  
				饮酒种类 = CASE WHEN ISNULL( @饮酒种类,'')= '' THEN  饮酒种类 ELSE @饮酒种类 END , --  
				饮酒种类其它 = CASE WHEN ISNULL( @饮酒种类其它,'')= '' THEN  饮酒种类其它 ELSE @饮酒种类其它 END , --  
				有无职业病 = CASE WHEN ISNULL( @有无职业病,'')= '1' THEN  有无职业病 ELSE @有无职业病 END , --  
				具体职业 =CASE WHEN ISNULL( @具体职业,'')= '' THEN  具体职业 ELSE @具体职业 END , --  
				从业时间 =CASE WHEN ISNULL( @从业时间,'')= '' THEN  从业时间 ELSE @从业时间 END , --  
				化学物质 =CASE WHEN ISNULL( @化学物质,'')= '' THEN  化学物质 ELSE @化学物质 END , --  
				化学物质防护 = CASE WHEN ISNULL( @化学物质防护,'')= '' THEN  化学物质防护 ELSE @化学物质防护 END , --  
				化学物质具体防护 = CASE WHEN ISNULL( @化学物质具体防护,'')= '' THEN  化学物质具体防护 ELSE @化学物质具体防护 END , --  
				毒物 = CASE WHEN ISNULL( @毒物,'')= '' THEN  毒物 ELSE @毒物 END , --   
				G_DWFHCS = CASE WHEN ISNULL( @G_DWFHCS,'')= '' THEN  G_DWFHCS ELSE @G_DWFHCS END , --   
				G_DWFHCSQT = CASE WHEN ISNULL( @G_DWFHCSQT,'')= '' THEN  G_DWFHCSQT ELSE @G_DWFHCSQT END , --   
				放射物质 =CASE WHEN ISNULL( @放射物质,'')= '' THEN  放射物质 ELSE @放射物质 END , --    
				放射物质防护措施有无 = CASE WHEN ISNULL( @放射物质防护措施有无,'')= '' THEN  放射物质防护措施有无 ELSE @放射物质防护措施有无 END , --   
				放射物质防护措施其他 = CASE WHEN ISNULL( @放射物质防护措施其他,'')= '' THEN  放射物质防护措施其他 ELSE @放射物质防护措施其他 END , --   
				口唇 =CASE WHEN ISNULL( @口唇,'')= '1' THEN  口唇 ELSE @口唇 END , --   
				齿列 =CASE WHEN ISNULL( @齿列,'')= '1' THEN  齿列 ELSE @齿列 END , --   
				咽部 =CASE WHEN ISNULL( @咽部,'')= '1' THEN  咽部 ELSE @咽部 END , --   
				皮肤其他 = CASE WHEN ISNULL( @皮肤其他,'')= '' THEN  皮肤其他 ELSE @皮肤其他 END , --   
				巩膜 = CASE WHEN ISNULL( @巩膜,'')= '1' THEN  巩膜 ELSE @巩膜 END , --   
				巩膜其他 = CASE WHEN ISNULL( @巩膜其他,'')= '' THEN  巩膜其他 ELSE @巩膜其他 END , --   
				足背动脉搏动 = CASE WHEN ISNULL( @足背动脉搏动,'')= '2' THEN  足背动脉搏动 ELSE @足背动脉搏动 END , --   
				乳腺 = CASE WHEN ISNULL( @乳腺,'')= '' THEN  乳腺 ELSE @乳腺 END , --   
				乳腺其他 = CASE WHEN ISNULL( @乳腺其他,'')= '' THEN  乳腺其他 ELSE @乳腺其他 END , --   
				外阴 =  CASE WHEN ISNULL( @外阴,'')= '' THEN  外阴 ELSE @外阴 END , --   
				外阴异常 = CASE WHEN ISNULL( @外阴异常,'')= '' THEN  外阴异常 ELSE @外阴异常 END , --   
				阴道 =CASE WHEN ISNULL( @阴道,'')= '' THEN  阴道 ELSE @阴道 END , --   
				阴道异常 = CASE WHEN ISNULL( @阴道异常,'')= '' THEN  阴道异常 ELSE @阴道异常 END , --   
				宫颈 = CASE WHEN ISNULL( @宫颈,'')= '' THEN  宫颈 ELSE @宫颈 END , --    
				宫颈异常 = CASE WHEN ISNULL( @宫颈异常,'')= '' THEN  宫颈异常 ELSE @宫颈异常 END , --   
				宫体 = CASE WHEN ISNULL( @宫体,'')= '' THEN  宫体 ELSE @宫体 END , --   
				宫体异常 = CASE WHEN ISNULL( @宫体异常,'')= '' THEN  宫体异常 ELSE @宫体异常 END , --   
				附件 = CASE WHEN ISNULL( @附件,'')= '' THEN  附件 ELSE @附件 END , --   
				附件异常 = CASE WHEN ISNULL( @附件异常,'')= '' THEN  附件异常 ELSE @附件异常 END , --   
				血钾浓度 = CASE WHEN ISNULL( @血钾浓度,'')= '' THEN  血钾浓度 ELSE @血钾浓度 END , --   
				血钠浓度 = CASE WHEN ISNULL( @血钠浓度,'')= '' THEN  血钠浓度 ELSE @血钠浓度 END , --   
				糖化血红蛋白 = CASE WHEN ISNULL( @糖化血红蛋白,'')= '' THEN  糖化血红蛋白 ELSE @糖化血红蛋白 END , --    
				宫颈涂片 = CASE WHEN ISNULL( @宫颈涂片,'')= '' THEN  宫颈涂片 ELSE @宫颈涂片 END , --   
				宫颈涂片异常 = CASE WHEN ISNULL( @宫颈涂片异常,'')= '' THEN  宫颈涂片异常 ELSE @宫颈涂片异常 END , --   
				平和质 =CASE WHEN ISNULL( @平和质,'')= '' THEN  平和质 ELSE @平和质 END , --   
				气虚质 =CASE WHEN ISNULL( @气虚质,'')= '' THEN  气虚质 ELSE @气虚质 END , --   
				阳虚质 =CASE WHEN ISNULL( @阳虚质,'')= '' THEN  阳虚质 ELSE @阳虚质 END , --   
				阴虚质 =CASE WHEN ISNULL( @阴虚质,'')= '' THEN  阴虚质 ELSE @阴虚质 END , --   
				痰湿质 =CASE WHEN ISNULL( @痰湿质,'')= '' THEN  痰湿质 ELSE @痰湿质 END , --   
				湿热质 =CASE WHEN ISNULL( @湿热质,'')= '' THEN  湿热质 ELSE @湿热质 END , --   
				血瘀质 =CASE WHEN ISNULL( @血瘀质,'')= '' THEN  血瘀质 ELSE @血瘀质 END , --   
				气郁质 =CASE WHEN ISNULL( @气郁质,'')= '' THEN  气郁质 ELSE @气郁质 END , --   
				特禀质 =CASE WHEN ISNULL( @特禀质,'')= '' THEN  特禀质 ELSE @特禀质 END , --   
				脑血管疾病 = CASE WHEN ISNULL( @脑血管疾病,'')= '1' THEN  脑血管疾病 ELSE @脑血管疾病 END , --   
				脑血管疾病其他 = CASE WHEN ISNULL( @脑血管疾病其他,'')= '' THEN  脑血管疾病其他 ELSE @脑血管疾病其他 END , --   
				肾脏疾病 = CASE WHEN ISNULL( @肾脏疾病,'')= '1' THEN  肾脏疾病 ELSE @肾脏疾病 END , --   
				肾脏疾病其他 = CASE WHEN ISNULL( @肾脏疾病其他,'')= '' THEN  肾脏疾病其他 ELSE @肾脏疾病其他 END , --   
				心脏疾病 = CASE WHEN ISNULL( @心脏疾病,'')= '1' THEN  心脏疾病 ELSE @心脏疾病 END , --   
				心脏疾病其他 = CASE WHEN ISNULL( @心脏疾病其他,'')= '' THEN  心脏疾病其他 ELSE @心脏疾病其他 END , --   
				血管疾病 = CASE WHEN ISNULL( @血管疾病,'')= '1' THEN  血管疾病 ELSE @血管疾病 END , --   
				血管疾病其他 = CASE WHEN ISNULL( @血管疾病其他,'')= '' THEN  血管疾病其他 ELSE @血管疾病其他 END , --   
				眼部疾病 = CASE WHEN ISNULL( @眼部疾病,'')= '1' THEN  眼部疾病 ELSE @眼部疾病 END , --   
				眼部疾病其他 = CASE WHEN ISNULL( @眼部疾病其他,'')='' THEN  眼部疾病其他 ELSE @眼部疾病其他 END , --   
				神经系统疾病 = CASE WHEN ISNULL( @神经系统疾病,'')='1' THEN  神经系统疾病 ELSE @神经系统疾病 END , --   
				神经系统疾病其他 = CASE WHEN ISNULL( @神经系统疾病其他,'')= '' THEN  神经系统疾病其他 ELSE @神经系统疾病其他 END , --     
				其他系统疾病 = CASE WHEN ISNULL( @其他系统疾病,'')= '1' THEN  其他系统疾病 ELSE @其他系统疾病 END , --   
				其他系统疾病其他 = CASE WHEN ISNULL( @其他系统疾病其他,'')= '' THEN  其他系统疾病其他 ELSE @其他系统疾病其他 END , --   
				健康评价 = CASE WHEN ISNULL( @健康评价,'')= '1' THEN  健康评价 ELSE @健康评价 END , --   
				健康评价异常1 = CASE WHEN ISNULL(@健康评价异常1,'')= '' THEN  健康评价异常1 ELSE @健康评价异常1 END , --   
				健康评价异常2 = CASE WHEN ISNULL(@健康评价异常2,'')= '' THEN  健康评价异常2 ELSE @健康评价异常2 END , --   
				健康评价异常3 = CASE WHEN ISNULL(@健康评价异常3,'')= '' THEN  健康评价异常3 ELSE @健康评价异常3 END , --   
				健康评价异常4 = CASE WHEN ISNULL(@健康评价异常4,'')= '' THEN  健康评价异常4 ELSE @健康评价异常4 END , --   
				健康指导 = CASE WHEN ISNULL( @健康指导,'')= '' THEN  健康指导 ELSE @健康指导 END , --   
				危险因素控制 = CASE WHEN ISNULL( @危险因素控制,'')= '' THEN  危险因素控制 ELSE @危险因素控制 END , --   
				危险因素控制体重 = CASE WHEN ISNULL( @危险因素控制体重,'')= '' THEN  危险因素控制体重 ELSE @危险因素控制体重 END , --   
				危险因素控制疫苗 = CASE WHEN ISNULL( @危险因素控制疫苗,'')= '' THEN  危险因素控制疫苗 ELSE @危险因素控制疫苗 END , --   
				危险因素控制其他 = CASE WHEN ISNULL( @危险因素控制其他,'')= '' THEN  危险因素控制其他 ELSE @危险因素控制其他 END , --   
				FIELD5 = CASE WHEN ISNULL( @FIELD5,'')= '' THEN  FIELD5 ELSE @FIELD5 END , --   
				症状其他 = CASE WHEN ISNULL( @症状其他,'')= '' THEN  症状其他 ELSE @症状其他 END , --   
				G_XYYC = CASE WHEN ISNULL( @G_XYYC,'')= '' THEN  G_XYYC ELSE @G_XYYC END , --   
				G_XYZC = CASE WHEN ISNULL( @G_XYZC,'')= '' THEN  G_XYZC ELSE @G_XYZC END , --   
				G_QTZHZH = CASE WHEN ISNULL( @G_QTZHZH,'')= '' THEN  G_QTZHZH ELSE @G_QTZHZH END , --   
				缺项 = CASE WHEN ISNULL( @缺项,'')= '' THEN  缺项 ELSE @缺项 END , --   
				口唇其他 =CASE WHEN ISNULL( @口唇其他,'')= '' THEN  口唇其他 ELSE @口唇其他 END , --   
				齿列其他 =CASE WHEN ISNULL( @齿列其他,'')= '' THEN  齿列其他 ELSE @齿列其他 END , --   
				咽部其他 =CASE WHEN ISNULL( @咽部其他,'')= '' THEN  咽部其他 ELSE @咽部其他 END , --   
				YDGNQT = CASE WHEN ISNULL( @YDGNQT,'')= '' THEN  YDGNQT ELSE @YDGNQT END , --   
				餐后2H血糖 = CASE WHEN ISNULL( @餐后2H血糖,'')= '' THEN  餐后2H血糖 ELSE @餐后2H血糖 END , --   
				老年人状况评估 = CASE WHEN ISNULL( @老年人状况评估,'')= '' THEN  老年人状况评估 ELSE @老年人状况评估 END , --   
				老年人自理评估 = CASE WHEN ISNULL( @老年人自理评估,'')= '' THEN  老年人自理评估 ELSE @老年人自理评估 END , --   
				粉尘 =CASE WHEN ISNULL( @粉尘,'')= '' THEN  粉尘 ELSE @粉尘 END , --   
				物理因素 =CASE WHEN ISNULL( @物理因素,'')= '' THEN  物理因素 ELSE @物理因素 END , --   
				职业病其他 = CASE WHEN ISNULL( @职业病其他,'')= '' THEN  职业病其他 ELSE @职业病其他 END , --   
				粉尘防护有无 = CASE WHEN ISNULL( @粉尘防护有无,'')= '' THEN  粉尘防护有无 ELSE @粉尘防护有无 END , --     
				物理防护有无 = CASE WHEN ISNULL( @物理防护有无,'')= '' THEN  物理防护有无 ELSE @物理防护有无 END , --     
				其他防护有无 = CASE WHEN ISNULL( @其他防护有无,'')= '' THEN  其他防护有无 ELSE @其他防护有无 END , --     
				粉尘防护措施 = CASE WHEN ISNULL( @粉尘防护措施,'')= '' THEN  粉尘防护措施 ELSE @粉尘防护措施 END , --     
				物理防护措施 = CASE WHEN ISNULL( @物理防护措施,'')= '' THEN  物理防护措施 ELSE @物理防护措施 END , --     
				其他防护措施 = CASE WHEN ISNULL( @其他防护措施,'')= '' THEN  其他防护措施 ELSE @其他防护措施 END , --     
				TNBFXJF =CASE WHEN ISNULL( @TNBFXJF,'')= '' THEN  TNBFXJF ELSE @TNBFXJF END , --   
				左侧原因 = CASE WHEN ISNULL( @左侧原因,'')= '' THEN  左侧原因 ELSE @左侧原因 END , --   
				右侧原因 = CASE WHEN ISNULL( @右侧原因,'')= '' THEN  右侧原因 ELSE @右侧原因 END , --   
				尿微量白蛋白 = CASE WHEN ISNULL( @尿微量白蛋白,'')= '' THEN  尿微量白蛋白 ELSE @尿微量白蛋白 END , --   
				完整度 = CASE WHEN ISNULL( @完整度,'')= '' THEN  完整度 ELSE @完整度 END , --   
				齿列缺齿 = CASE WHEN ISNULL( @齿列缺齿,'')= '' THEN  齿列缺齿 ELSE @齿列缺齿 END , --   
				齿列龋齿 = CASE WHEN ISNULL( @齿列龋齿,'')= '' THEN  齿列龋齿 ELSE @齿列龋齿 END , --   
				齿列义齿 = CASE WHEN ISNULL( @齿列义齿,'')= '' THEN  齿列义齿 ELSE @齿列义齿 END 
			 WHERE 个人档案编号 = @个人档案编号 ; 
			 --更新 行状态为1，表示已上传
				UPDATE [tb_健康体检] SET RowState = '1' WHERE 个人档案编号 = @个人档案编号 and substring(创建时间,1,10) =SUBSTRING( @创建时间,1,10)
			 IF @@error = 0 BEGIN 
				 COMMIT TRANSACTION 
				 SELECT 0 , '执行成功' 
				 RETURN 
			 END 
			 ELSE BEGIN 
				 ROLLBACK TRANSACTION 
				 SELECT 1 , '执行失败' 
				 RETURN 
			 END 
	END

--2. 住院史、非免疫及用药情况
	IF NOT EXISTS (SELECT 1 FROM  [192.168.10.121].[AtomEHR.YSDB01].[DBO].[tb_健康体检_住院史] 
		WHERE 个人档案编号 = @个人档案编号 and substring(创建日期,1,10) =SUBSTRING( @创建时间,1,10))--不存在，插入
	BEGIN
		INSERT INTO [192.168.10.121].[AtomEHR.YSDB01].[DBO].[tb_健康体检_住院史] 
		(个人档案编号,类型,入院日期,出院日期,原因, 医疗机构名称,病案号,创建日期)
		select 个人档案编号,类型,入院日期,出院日期,原因, 医疗机构名称,病案号,创建日期 from [tb_健康体检_住院史]
		WHERE 个人档案编号 = @个人档案编号 and substring(创建日期,1,10) =SUBSTRING( @创建时间,1,10)

		UPDATE [tb_健康体检_住院史] SET RowState = '1' 
		WHERE 个人档案编号 = @个人档案编号 and substring(创建日期,1,10) =SUBSTRING( @创建时间,1,10)
	END

	IF NOT EXISTS (SELECT 1 FROM  [192.168.10.121].[AtomEHR.YSDB01].[DBO].[tb_健康体检_用药情况表] 
		WHERE 个人档案编号 = @个人档案编号 and substring(创建时间,1,10) =SUBSTRING( @创建时间,1,10))--不存在，插入
	BEGIN
		INSERT INTO [192.168.10.121].[AtomEHR.YSDB01].[DBO].[tb_健康体检_用药情况表] 
		(个人档案编号,药物名称,用法,用量,用药时间, 服药依从性,创建时间)
		select 个人档案编号,药物名称,用法,用量,用药时间, 服药依从性,创建时间 from [tb_健康体检_用药情况表]
		WHERE 个人档案编号 = @个人档案编号 and substring(创建时间,1,10) =SUBSTRING( @创建时间,1,10)

		UPDATE [tb_健康体检_用药情况表] SET RowState = '1' 
		WHERE 个人档案编号 = @个人档案编号 and substring(创建时间,1,10) =SUBSTRING( @创建时间,1,10)
	END
	
	IF NOT EXISTS (SELECT 1 FROM  [192.168.10.121].[AtomEHR.YSDB01].[DBO].[tb_健康体检_非免疫规划预防接种史] 
		WHERE 个人档案编号 = @个人档案编号 and substring(创建日期,1,10) =SUBSTRING( @创建时间,1,10))--不存在，插入
	BEGIN
		INSERT INTO [192.168.10.121].[AtomEHR.YSDB01].[DBO].[tb_健康体检_非免疫规划预防接种史] 
		(个人档案编号,接种名称,接种日期,接种机构,创建日期)
		select 个人档案编号,接种名称,接种日期,接种机构,创建日期 from [tb_健康体检_非免疫规划预防接种史]
		WHERE 个人档案编号 = @个人档案编号 and substring(创建日期,1,10) =SUBSTRING( @创建时间,1,10)

		UPDATE [tb_健康体检_非免疫规划预防接种史] SET RowState = '1' 
		WHERE 个人档案编号 = @个人档案编号 and substring(创建日期,1,10) =SUBSTRING( @创建时间,1,10)
	END

END
GO
/****** Object:  StoredProcedure [dbo].[USP_IMPORTtb_老年人生活自理能力评价121]    Script Date: 04/25/2016 11:58:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------------------------------------ 
--功能：添加或更新【tb_老年人生活自理能力评价】表中的数据 
--说明： 
--创建日期：【2016-04-14 14:20:36】 
------------------------------------ 
CREATE PROCEDURE [dbo].[USP_IMPORTtb_老年人生活自理能力评价121]
( 
  @个人档案编号 VARCHAR(20) = NULL, -- 
  @G_ZZBS VARCHAR(50) = NULL, -- 
  @G_ZZXZZ VARCHAR(50)= NULL , -- 
  @G_ZZCX VARCHAR(50) = NULL, -- 
  @G_ZZZZ VARCHAR(50) = NULL, -- 
  @G_ZTH VARCHAR(50) = NULL, -- 
  @G_ZTYY VARCHAR(50)= NULL , -- 
  @G_ZTZD VARCHAR(50)= NULL , -- 
  @G_ZTZZ VARCHAR(50)= NULL , -- 
  @G_SFTZ DECIMAL(5) = NULL, -- 
  @G_RXY INT = NULL, -- 
  @G_RYJ DECIMAL(5) = NULL, -- 
  @G_ZYD INT = NULL, -- 
  @G_YDSJ DECIMAL(5) = NULL, -- 
  @G_YS VARCHAR(20) = NULL, -- 
  @G_XLTZ VARCHAR(100) = NULL, -- 
  @G_ZYXW VARCHAR(20)= NULL , -- 
  @G_YMJZ VARCHAR(100)= NULL , -- 
  @G_GXBYF VARCHAR(100)= NULL , -- 
  @G_GZSSYF VARCHAR(100)= NULL , -- 
  @下次随访目标 VARCHAR(100) = NULL, -- 
  @下次随访日期 VARCHAR(80)= NULL , -- 
  @随访医生 VARCHAR(20) = NULL, -- 
  @创建机构 VARCHAR(20)= NULL , -- 
  @创建人 VARCHAR(20)= NULL, -- 
  @更新人 VARCHAR(20)= NULL, -- 
  @所属机构 VARCHAR(20)= NULL , -- 
  @创建时间 VARCHAR(20)= NULL , -- 
  @更新时间 VARCHAR(20)= NULL , -- 
  @随访日期 VARCHAR(20)= NULL , -- 
  @缺项 VARCHAR(4) = NULL, -- 
  @进餐评分 VARCHAR(20) = NULL, -- 
  @梳洗评分 VARCHAR(20) = NULL, -- 
  @如厕评分 VARCHAR(20) = NULL, -- 
  @活动评分 VARCHAR(20) = NULL, -- 
  @总评分 VARCHAR(20) = NULL, -- 
  @穿衣评分 VARCHAR(20) = NULL, -- 
  @随访次数 VARCHAR(20) = NULL, -- 
  @完整度 VARCHAR(20)  = NULL--  
) 
AS 
SET NOCOUNT ON 
	IF NOT EXISTS(SELECT 1 FROM [192.168.10.121].[AtomEHR.YSDB01].[DBO].[tb_老年人随访] 
		WHERE 个人档案编号 = @个人档案编号 and substring(创建时间,1,10) =SUBSTRING( @创建时间,1,10))
	begin
	
   BEGIN TRANSACTION 
 
  INSERT INTO [192.168.10.121].[AtomEHR.YSDB01].[DBO].[tb_老年人随访]  
  ( 
    个人档案编号 , --  
    G_ZZBS , --  
    G_ZZXZZ , --  
    G_ZZCX , --  
    G_ZZZZ , --  
    G_ZTH , --  
    G_ZTYY , --  
    G_ZTZD , --  
    G_ZTZZ , --  
    G_SFTZ , --  
    G_RXY , --  
    G_RYJ , --  
    G_ZYD , --  
    G_YDSJ , --  
    G_YS , --  
    G_XLTZ , --  
    G_ZYXW , --  
    G_YMJZ , --  
    G_GXBYF , --  
    G_GZSSYF , --  
    下次随访目标 , --  
    下次随访日期 , --  
    随访医生 , --  
    创建机构 , --  
    创建人 , --  
    更新人 , --  
    所属机构 , --  
    创建时间 , --  
    更新时间 , --  
    随访日期 , --  
    缺项 , --  
    进餐评分 , --  
    梳洗评分 , --  
    如厕评分 , --  
    活动评分 , --  
    总评分 , --  
    穿衣评分 , --  
    随访次数 , --  
    完整度   --  
  ) 
  VALUES 
  ( 
    @个人档案编号 , 
    @G_ZZBS , 
    @G_ZZXZZ , 
    @G_ZZCX , 
    @G_ZZZZ , 
    @G_ZTH , 
    @G_ZTYY , 
    @G_ZTZD , 
    @G_ZTZZ , 
    @G_SFTZ , 
    @G_RXY , 
    @G_RYJ , 
    @G_ZYD , 
    @G_YDSJ , 
    @G_YS , 
    @G_XLTZ , 
    @G_ZYXW , 
    @G_YMJZ , 
    @G_GXBYF , 
    @G_GZSSYF , 
    @下次随访目标 , 
    @下次随访日期 , 
    @随访医生 , 
    @创建机构 , 
    @创建人 , 
    @更新人 , 
    @所属机构 , 
    @创建时间 , 
    @更新时间 , 
    @随访日期 , 
    @缺项 , 
    @进餐评分 , 
    @梳洗评分 , 
    @如厕评分 , 
    @活动评分 , 
    @总评分 , 
    @穿衣评分 , 
    @随访次数 , 
    @完整度  
  ); 
 
 update tb_老年人生活自理能力评价
			set RowState = '1' 
			where 个人档案编号 = @个人档案编号 and substring(创建时间,1,10) =SUBSTRING( @创建时间,1,10)

     IF @@error = 0 BEGIN 
         COMMIT TRANSACTION 
         SELECT 0 , '执行成功' 
         RETURN 
     END 
     ELSE BEGIN 
         ROLLBACK TRANSACTION 
         SELECT 1 , '执行失败' 
         RETURN 
     END 
end
else --update
begin

	 BEGIN TRANSACTION 
 
  UPDATE [192.168.10.121].[AtomEHR.YSDB01].[DBO].[tb_老年人随访]  SET 
    G_ZZBS = @G_ZZBS , --  
    G_ZZXZZ = @G_ZZXZZ , --  
    G_ZZCX = @G_ZZCX , --  
    G_ZZZZ = @G_ZZZZ , --  
    G_ZTH = @G_ZTH , --  
    G_ZTYY = @G_ZTYY , --  
    G_ZTZD = @G_ZTZD , --  
    G_ZTZZ = @G_ZTZZ , --  
    G_SFTZ = @G_SFTZ , --  
    G_RXY = @G_RXY , --  
    G_RYJ = @G_RYJ , --  
    G_ZYD = @G_ZYD , --  
    G_YDSJ = @G_YDSJ , --  
    G_YS = @G_YS , --  
    G_XLTZ = @G_XLTZ , --  
    G_ZYXW = @G_ZYXW , --  
    G_YMJZ = @G_YMJZ , --  
    G_GXBYF = @G_GXBYF , --  
    G_GZSSYF = @G_GZSSYF , --  
    下次随访目标 = @下次随访目标 , --  
    下次随访日期 = @下次随访日期 , --  
    随访医生 = @随访医生 , --  
    创建机构 = @创建机构 , --  
    创建人 = @创建人 , --  
    更新人 = @更新人 , --  
    所属机构 = @所属机构 , --  
    创建时间 = @创建时间 , --  
    更新时间 = @更新时间 , --  
    随访日期 = @随访日期 , --  
    缺项 = @缺项 , --  
    进餐评分 = @进餐评分 , --  
    梳洗评分 = @梳洗评分 , --  
    如厕评分 = @如厕评分 , --  
    活动评分 = @活动评分 , --  
    总评分 = @总评分 , --  
    穿衣评分 = @穿衣评分 , --  
    随访次数 = @随访次数 , --  
    完整度 = @完整度   --  
 WHERE 个人档案编号 = @个人档案编号 and substring(创建时间,1,10) =SUBSTRING( @创建时间,1,10)
 update tb_老年人生活自理能力评价
			set RowState = '1' 
			where 个人档案编号 = @个人档案编号 and substring(创建时间,1,10) =SUBSTRING( @创建时间,1,10)
     IF @@error = 0 BEGIN 
         COMMIT TRANSACTION 
         SELECT 0 , '执行成功' 
         RETURN 
     END 
     ELSE BEGIN 
         ROLLBACK TRANSACTION 
         SELECT 1 , '执行失败' 
         RETURN 
     END 
end
 RETURN
GO
/****** Object:  StoredProcedure [dbo].[USP_IMPORTDATAFROM121]    Script Date: 04/25/2016 11:58:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_IMPORTDATAFROM121 '371323B10019'

CREATE PROC [dbo].[USP_IMPORTDATAFROM121]
	@PRGID	VARCHAR(20)  --所属机构 ， 姚店子 371323B10019
AS
BEGIN
	--DECLARE @PRGID VARCHAR(20) = '371323B10019'
	INSERT INTO TB_导出用户 (身份证号,姓名,性别,居住地址,出生日期,个人档案编号,所属机构)  
		SELECT 身份证号,姓名,性别,居住地址,出生日期, 个人档案编号,所属机构
		FROM [192.168.10.121].[AtomEHR.YSDB].[DBO].[TB_健康档案] 
			WHERE 所属机构 = @PRGID or  substring([所属机构],1,7)+'1'+substring([所属机构],9,7) like  @PRGID + '%'

	--SELECT * FROM TB_导出用户

	--DELETE TB_导出用户
END
GO
/****** Object:  StoredProcedure [dbo].[SP_SAVE住院史]    Script Date: 04/25/2016 11:58:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/

CREATE PROC [dbo].[SP_SAVE住院史]
		@_ID BIGINT = NULL
      ,@_身份证号		VARCHAR(20)
      ,@_个人档案编号	VARCHAR(20)
      ,@_类型			VARCHAR(1)
      ,@_入院日期		VARCHAR(20)
      ,@_出院日期		VARCHAR(20)
      ,@_原因			VARCHAR(200)
      ,@_医疗机构名称	VARCHAR(100)
      ,@_病案号			VARCHAR(40)
      ,@_创建日期		VARCHAR(40)
AS
BEGIN
	IF  ISNULL(@_ID,'')=''
	BEGIN
		INSERT INTO tb_健康体检_住院史 VALUES
		(@_身份证号,@_个人档案编号,@_类型,@_入院日期,@_出院日期,@_原因,@_医疗机构名称,@_病案号,@_创建日期);
	END
	ELSE 
	BEGIN
		UPDATE tb_健康体检_住院史 SET 
		入院日期 = CASE WHEN ISNULL(@_入院日期,'') <> '' THEN @_入院日期 ELSE 入院日期 END,
		出院日期 = CASE WHEN ISNULL(@_出院日期,'') <> '' THEN @_出院日期 ELSE 出院日期 END,
		原因 = CASE WHEN ISNULL(@_原因,'') <> '' THEN @_原因 ELSE 原因 END,
		医疗机构名称 = CASE WHEN ISNULL(@_医疗机构名称,'') <> '' THEN @_医疗机构名称 ELSE 医疗机构名称 END,
		病案号 = CASE WHEN ISNULL(@_病案号,'') <> '' THEN @_病案号 ELSE 病案号 END,
		创建日期 = CASE WHEN ISNULL(@_创建日期,'') <> '' THEN @_创建日期 ELSE 创建日期 END
		WHERE ID = @_ID

	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_SAVE用药情况表]    Script Date: 04/25/2016 11:58:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/

CREATE PROC [dbo].[SP_SAVE用药情况表]
		--@_ID BIGINT  = NULL
      @_身份证号		VARCHAR(20)
      ,@_个人档案编号	VARCHAR(20)
      ,@_药物名称			VARCHAR(100)
      ,@_用法		VARCHAR(100)
      ,@_用量		VARCHAR(20)
      ,@_用药时间			VARCHAR(20)
      ,@_服药依从性	VARCHAR(100)
      ,@_创建时间		VARCHAR(40)
AS
BEGIN
	--IF ISNULL(@_ID,'')=''
	--BEGIN
		INSERT INTO tb_健康体检_用药情况表 VALUES
		(@_身份证号,@_个人档案编号,@_药物名称,@_用法,@_用量,@_用药时间,@_服药依从性,@_创建时间);
	--END
	--ELSE 
	--BEGIN
	--	UPDATE tb_健康体检_用药情况表 SET 
	--	药物名称 = CASE WHEN ISNULL(@_药物名称,'') <> '' THEN @_药物名称 ELSE 药物名称 END,
	--	用法 = CASE WHEN ISNULL(@_用法,'') <> '' THEN @_用法 ELSE 用法 END,
	--	用量 = CASE WHEN ISNULL(@_用量,'') <> '' THEN @_用量 ELSE 用量 END,
	--	用药时间 = CASE WHEN ISNULL(@_用药时间,'') <> '' THEN @_用药时间 ELSE 用药时间 END,
	--	服药依从性 = CASE WHEN ISNULL(@_服药依从性,'') <> '' THEN @_服药依从性 ELSE 服药依从性 END,
	--	创建时间 = CASE WHEN ISNULL(@_创建时间,'') <> '' THEN @_创建时间 ELSE 创建时间 END
	--	WHERE ID = @_ID

	--END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_SAVE非免疫规划预防接种史]    Script Date: 04/25/2016 11:58:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/

CREATE PROC [dbo].[SP_SAVE非免疫规划预防接种史]
      @_身份证号		VARCHAR(20)
      ,@_个人档案编号	VARCHAR(20)
      ,@_接种名称			VARCHAR(100)
      ,@_接种日期		VARCHAR(20)
      ,@_接种机构		VARCHAR(40)
      ,@_创建日期			VARCHAR(40)
AS
BEGIN
	--IF ISNULL(@_ID,'')=''
	--BEGIN
		INSERT INTO tb_健康体检_非免疫规划预防接种史 VALUES
		(@_身份证号,@_个人档案编号,@_接种名称,@_接种日期,@_接种机构,@_创建日期);
	--END
	--ELSE 
	--BEGIN
	--	UPDATE tb_健康体检_非免疫规划预防接种史 SET 
	--	接种名称 = CASE WHEN ISNULL(@_接种名称,'') <> '' THEN @_接种名称 ELSE 接种名称 END,
	--	接种日期 = CASE WHEN ISNULL(@_接种日期,'') <> '' THEN @_接种日期 ELSE 接种日期 END,
	--	接种机构 = CASE WHEN ISNULL(@_接种机构,'') <> '' THEN @_接种机构 ELSE 接种机构 END,
	--	创建日期 = CASE WHEN ISNULL(@_创建日期,'') <> '' THEN @_创建日期 ELSE 创建日期 END
	--	WHERE ID = @_ID

	--END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_GetDataDicts]    Script Date: 04/25/2016 11:58:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetDataDicts]
AS
BEGIN
/*-------------------------------------------------------------------------------------
  程序说明:下载数据字典 BY:JONNY 2010-06-14
  返回结果: 多个数据表.
  -------------------------------------------------------------------------------------

SELECT * FROM dbo.tb_Customer
SELECT * FROM dbo.tb_CommonDataDict

dbo.sp_GetCustomerByAttributeCodes ',CUS,','Y'

sp_GetDataDicts

*/

 --   SELECT * FROM dbo.vw_LogOPType --日志类型   0
	--SELECT * FROM dbo.sys_BusinessTables    --1
	SELECT [Account] ,[用户编码],[UserName],[Tel],[Email],[LastLoginTime],[LastLogoutTime],
	[IsLocked],[CreateTime],[FlagAdmin],[FlagOnline],[LoginCounter],[DataSets],[所属机构] 
	FROM dbo.tb_MyUser--2
 --   SELECT * FROM tb_CustomerAttribute --3
 --   SELECT * FROM dbo.tb_CommonDataDict WHERE DataType=1 --银行资料 4
 --   SELECT * FROM dbo.tb_CommDataDictType --5
 --   SELECT * FROM dbo.tb_CommonDataDict WHERE DataType=6 --部门资料 6
	--SELECT * FROM dbo.tb_CommonDataDict WHERE DataType=20 --机构级别 7
	SELECT * FROM dbo.tb_机构信息  --8 WHERE 机构级别 <>'1'
	--SELECT * FROM dbo.tb_地区档案 WHERE 地区编码 <>'0' --9
	--与户主关系 
	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'yhzgx' AND P_FLAG = '1'  --10
	--档案状态
	--SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'rkx_dazt' AND P_FLAG = '1' --11
	--性别
	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'xb_xingbie' AND P_FLAG = '1' order by P_CODE
	--证件类别
	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'zjlb' AND P_FLAG = '1' --13
	--居住状况
	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'jzzk' AND P_FLAG = '1'
	--民族
	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'mz_minzu' AND P_FLAG = '1' --15
	--血型
	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'xuexing' AND P_FLAG = '1'
	--RH
	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'rhxx' AND P_FLAG = '1' --17
	--职业
	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'zy_zhiye' AND P_FLAG = '1'
	--文化程度
	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'whcd' AND P_FLAG = '1' --19
	--劳动强度
	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'ldqd' AND P_FLAG = '1'
	--婚姻状况
	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'hyzk' AND P_FLAG = '1' --21
	--档案类别
	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'dalb' AND P_FLAG = '1' --22
	--有1/无2
	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'yw_youwu' AND P_FLAG = '1' --23
	--家族史成员
	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'jzscy' AND P_FLAG = '1' --24
	--住房类型
	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'zflx' AND P_FLAG = '1'  --25
	--厕所类型
	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'cslx' AND P_FLAG = '1'  --26
	--是否
	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'sf_shifou' AND P_FLAG = '1'  --27
	--是否戒酒
	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'sfjj' AND P_FLAG = '1'  --28
	--服药依从性
	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'fyycx-mb' AND P_FLAG = '1'  order by P_CODE --29
	
	--考核项
	--SELECT * FROM [dbo].tb_考核项 where 是否考核项=1  --30
	
	--常用字典
	SELECT * FROM [dbo].tb_常用字典  where P_FLAG=1  --31

	--与户主关系  只查询 户主
	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'yhzgx' AND P_FLAG = '1' AND P_CODE = '1'  --32
	--与户主关系  查询除 户主 以外的用户
 	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'yhzgx' AND P_FLAG = '1' AND P_CODE <> '1'  --33
	--档案状态
 	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'rkx_dazt' AND P_FLAG = '1'  --34
	--有无异常
 	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'ywyc' AND P_FLAG = '1'  --35
	--阴道分泌物
 	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'ydfmw' AND P_FLAG = '1'  --36
	--阴道清洁度
 	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'ydqjdzd' AND P_FLAG = '1'  --37
	--阴性阳性
 	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'yxyx' AND P_FLAG = '1'  --38
	--恢复状态  已恢复，未恢复
 	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'hfzt' AND P_FLAG = '1'  --39
	--处理类型，结案 转诊
 	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'cllx' AND P_FLAG = '1'  --40
	--产后42天指导
 	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = '42zhidao' AND P_FLAG = '1'  --41
	--知情同意
 	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'zhiqingTY' AND P_FLAG = '1'  --42
	--蓝牙设备分类
 	SELECT * FROM [dbo].[tb_常用字典] WHERE P_FUN_CODE = 'bluetooth' AND P_FLAG = '1'  --42
END
GO
