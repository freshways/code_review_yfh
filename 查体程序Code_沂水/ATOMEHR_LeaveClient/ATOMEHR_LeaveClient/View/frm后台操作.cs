﻿using ATOMEHR_LeaveClient.ncg;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ATOMEHR_LeaveClient.SqlSugar_DALs;
using ATOMEHR_LeaveClient.SqlSugar_Models;

namespace ATOMEHR_LeaveClient
{
    public partial class frm后台操作 : Form
    {
        public frm后台操作()
        {
            InitializeComponent();
        }

        #region 下载/同步到本地数据库
        private void btn导出数据_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Program._bRGID))
            {
                Import个人档案(Program._bRGID);
                Import用户信息(Program._bRGID);
                Import_医生信息(Program._bRGID);
                Import_B超报告模版(Program._bRGID);
            }
            else
            {
                MessageBox.Show("请维护查体机构！");
            }
        }

        private void btn导出参照数据_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Program._bRGID))
            {
                Import_参照数据_体检表(Program._bRGID);
                Import_参照数据_体检表_用药(Program._bRGID);
                Import_参照数据_自理能力评估(Program._bRGID);
            }
            else
            {
                MessageBox.Show("请维护查体机构！");
            }
        }

        private void Import_B超报告模版(string RGID)
        {
            dal_B超报告模版 dalB超模版 = new dal_B超报告模版();
            List<tb_B超报告模版> ls_GetB超模版 = dalB超模版.Get_B超报告模版(RGID);
            dal_B超报告模版_Lite dalB超模版Lite = new dal_B超报告模版_Lite();
            //先全部删除
            dalB超模版Lite.Delete("");

            int insertNum = dalB超模版Lite.SetB超报告模版(ls_GetB超模版);
            MessageBox.Show("导入B超模版数据成功!" + "\n" + "总共导入数据" + insertNum + "条.");
        }

        private void Import个人档案(string rgid)
        {
            try
            {
                //int result = 0;
                List<tb_导出用户Info> list = new List<tb_导出用户Info>();
                tb_导出用户Info tb = null;
                //string rgid = Program._bRGID == "" ? "371323B10019" : Program._bRGID; //如果该参数是空默认姚店子
                SqlDataReader reader = DBManager.ExecuteProcByReader(DBManager.Conn121, "USP_IMPORTDATAFROM121", new SqlParameter[] { new SqlParameter("@PRGID", rgid) });
                using (reader)
                {
                    while (reader.Read())
                    {
                        tb = new tb_导出用户Info();
                        tb.身份证号 = reader["身份证号"].ToString();
                        tb.姓名 = reader["姓名"].ToString();
                        tb.性别 = reader["性别"].ToString();
                        tb.出生日期 = reader["出生日期"].ToString();
                        tb.居住地址 = reader["居住地址"].ToString();
                        tb.个人档案编号 = reader["个人档案编号"].ToString();
                        tb.所属机构 = reader["所属机构"].ToString();
                        list.Add(tb);
                    }
                }
                //先全部删除
                tb_导出用户DAL.Delete("");

                tb_导出用户DAL.InsertList(list);
                MessageBox.Show("导入个人档案成功，总共导入数据" + list.Count + "条！");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Import机构信息()
        {

        }

        private void Import用户信息(string rgid)
        {
            try
            {
                List<tb_MyUser> list = new List<tb_MyUser>();
                tb_MyUser tb = null;

                string SQL = "select * from tb_MyUser where 所属机构=@PRGID";
                SqlDataReader reader = DBManager.ExecuteQuery(SQL, new SqlParameter[] { new SqlParameter("@PRGID", rgid) });
                using (reader)
                {
                    while (reader.Read())
                    {
                        tb = new tb_MyUser();
                        tb.isid = Convert.ToInt32(reader["isid"].ToString());
                        tb.Account = reader["Account"].ToString();
                        tb.NovellAccount = reader["NovellAccount"].ToString();
                        tb.DomainName = reader["DomainName"].ToString();
                        tb.用户编码 = reader["用户编码"].ToString();
                        tb.UserName = reader["UserName"].ToString();
                        tb.Address = reader["Address"].ToString();
                        tb.Tel = reader["Tel"].ToString();

                        tb.Email = reader["Account"].ToString();
                        tb.Password = reader["NovellAccount"].ToString();
                        //tb.LastLoginTime = reader["LastLoginTime"];
                        tb.IsLocked = Convert.ToInt16(reader["IsLocked"]);
                        tb.DataSets = reader["DataSets"].ToString();
                        tb.所属机构 = reader["所属机构"].ToString();
                        list.Add(tb);
                    }
                }
                //先全部删除
                tb_MyUserDAL.Delete("");

                tb_MyUserDAL.InsertList(list);
                MessageBox.Show("导入用户信息成功，总共导入数据" + list.Count + "条！");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        //Begin WXF 2018-10-26 | 09:40
        //导入去年数据作为参照值(去年数据并不能取自sqlite本身现有数据,因为内年的的查体数据有上传完后再公卫上修改增删的,所以为了数据完整度需要从公卫数据库重新取值) 
        private void Import_参照数据_体检表(string rgid)
        {
            dal_健康体检 dal健康体检 = new dal_健康体检();
            dal_健康体检_Reference dal健康体检Ref = new dal_健康体检_Reference();

            if (dal健康体检Ref.GetTableIsNull())
            {
            }
            else
            {
                dal健康体检Ref.DeleteAll();
            }

            List<tb_健康体检_Lite> ls_健康体检 = new List<tb_健康体检_Lite>();
            List<tb_健康体检_Reference> ls_健康体检Ref = new List<tb_健康体检_Reference>();

            ls_健康体检 = dal健康体检.Get参照数据(rgid);

            foreach (tb_健康体检_Lite item in ls_健康体检)
            {
                tb_健康体检_Reference tb健康体检Ref = new tb_健康体检_Reference();

                tb健康体检Ref.ID = item.ID;
                tb健康体检Ref.个人档案编号 = item.个人档案编号;
                tb健康体检Ref.体检日期 = item.体检日期;
                tb健康体检Ref.创建时间 = item.创建时间;
                tb健康体检Ref.修改时间 = item.修改时间;
                tb健康体检Ref.创建机构 = item.创建机构;
                tb健康体检Ref.所属机构 = item.所属机构;
                tb健康体检Ref.创建人 = item.创建人;
                tb健康体检Ref.修改人 = item.修改人;
                tb健康体检Ref.锻炼频率 = item.锻炼频率;
                tb健康体检Ref.每次锻炼时间 = item.每次锻炼时间;
                tb健康体检Ref.坚持锻炼时间 = item.坚持锻炼时间;
                tb健康体检Ref.锻炼方式 = item.锻炼方式;
                tb健康体检Ref.饮食习惯 = item.饮食习惯;
                tb健康体检Ref.吸烟状况 = item.吸烟状况;
                tb健康体检Ref.日吸烟量 = item.日吸烟量;
                tb健康体检Ref.开始吸烟年龄 = item.开始吸烟年龄;
                tb健康体检Ref.戒烟年龄 = item.戒烟年龄;
                tb健康体检Ref.饮酒频率 = item.饮酒频率;
                tb健康体检Ref.日饮酒量 = item.日饮酒量;
                tb健康体检Ref.是否戒酒 = item.是否戒酒;
                tb健康体检Ref.戒酒年龄 = item.戒酒年龄;
                tb健康体检Ref.开始饮酒年龄 = item.开始饮酒年龄;
                tb健康体检Ref.近一年内是否曾醉酒 = item.近一年内是否曾醉酒;
                tb健康体检Ref.饮酒种类 = item.饮酒种类;
                tb健康体检Ref.饮酒种类其它 = item.饮酒种类其它;
                tb健康体检Ref.有无职业病 = item.有无职业病;
                tb健康体检Ref.具体职业 = item.具体职业;
                tb健康体检Ref.从业时间 = item.从业时间;
                tb健康体检Ref.粉尘 = item.粉尘;
                tb健康体检Ref.粉尘防护有无 = item.粉尘防护有无;
                tb健康体检Ref.粉尘防护措施 = item.粉尘防护措施;
                tb健康体检Ref.放射物质 = item.放射物质;
                tb健康体检Ref.放射物质防护措施有无 = item.放射物质防护措施有无;
                tb健康体检Ref.放射物质防护措施其他 = item.放射物质防护措施其他;
                tb健康体检Ref.物理因素 = item.物理因素;
                tb健康体检Ref.物理防护有无 = item.物理防护有无;
                tb健康体检Ref.物理防护措施 = item.物理防护措施;
                tb健康体检Ref.化学物质 = item.化学物质;
                tb健康体检Ref.化学物质防护 = item.化学物质防护;
                tb健康体检Ref.化学物质具体防护 = item.化学物质具体防护;
                tb健康体检Ref.职业病其他 = item.职业病其他;
                tb健康体检Ref.其他防护有无 = item.其他防护有无;
                tb健康体检Ref.其他防护措施 = item.其他防护措施;
                tb健康体检Ref.齿列 = item.齿列;
                tb健康体检Ref.齿列其他 = item.齿列其他;
                tb健康体检Ref.齿列缺齿 = item.齿列缺齿;
                tb健康体检Ref.齿列龋齿 = item.齿列龋齿;
                tb健康体检Ref.齿列义齿 = item.齿列义齿;
                tb健康体检Ref.脑血管疾病 = item.脑血管疾病;
                tb健康体检Ref.脑血管疾病其他 = item.脑血管疾病其他;
                tb健康体检Ref.肾脏疾病 = item.肾脏疾病;
                tb健康体检Ref.肾脏疾病其他 = item.肾脏疾病其他;
                tb健康体检Ref.心脏疾病 = item.心脏疾病;
                tb健康体检Ref.心脏疾病其他 = item.心脏疾病其他;
                tb健康体检Ref.眼部疾病 = item.眼部疾病;
                tb健康体检Ref.眼部疾病其他 = item.眼部疾病其他;
                tb健康体检Ref.神经系统疾病 = item.神经系统疾病;
                tb健康体检Ref.神经系统疾病其他 = item.神经系统疾病其他;
                tb健康体检Ref.其他系统疾病 = item.其他系统疾病;
                tb健康体检Ref.其他系统疾病其他 = item.其他系统疾病其他;

                ls_健康体检Ref.Add(tb健康体检Ref);
            }

            int insertNum = dal健康体检Ref.Set参照数据(ls_健康体检Ref);
            MessageBox.Show("导入体检表参照数据成功!" + "\n" + "总共导入数据" + insertNum + "条.");
        }
        //End

        //Begin WXF 2019-02-20 | 18:12
        //获取最新的体检表,高血压随访,糖尿病随访,冠心病随访,脑卒中随访,精神病随访的用药情况 
        private void Import_参照数据_体检表_用药(string rgid)
        {
            dal_健康体检_用药情况_Reference dal用药情况Ref = new dal_健康体检_用药情况_Reference();

            if (dal用药情况Ref.GetTableIsNull())
            {
            }
            else
            {
                dal用药情况Ref.DeleteAll();
            }

            List<tb_健康体检_用药情况_Reference> ls_用药情况Ref = new List<tb_健康体检_用药情况_Reference>();
            ls_用药情况Ref.AddRange(dal用药情况Ref.Get体检表用药(rgid));
            ls_用药情况Ref.AddRange(dal用药情况Ref.Get高血压随访用药(rgid));
            ls_用药情况Ref.AddRange(dal用药情况Ref.Get糖尿病随访用药(rgid));
            ls_用药情况Ref.AddRange(dal用药情况Ref.Get冠心病随访用药(rgid));
            ls_用药情况Ref.AddRange(dal用药情况Ref.Get脑卒中随访用药(rgid));
            ls_用药情况Ref.AddRange(dal用药情况Ref.Get重症精神疾病随访用药(rgid));

            int insertNum = dal用药情况Ref.Set参照数据(ls_用药情况Ref);
            MessageBox.Show("导入用药情况参照数据成功!" + "\n" + "总共导入数据" + insertNum + "条.");
        }
        //End

        //Begin WXF 2019-02-20 | 10:47
        //获取去年老年人自理能力评估数据参照 
        private void Import_参照数据_自理能力评估(string rgid)
        {
            dal_老年人生活自理能力评估_Reference dal自理评估Ref = new dal_老年人生活自理能力评估_Reference();

            if (dal自理评估Ref.GetTableIsNull())
            {
            }
            else
            {
                dal自理评估Ref.DeleteAll();
            }

            List<tb_老年人生活自理能力评估_Reference> ls_自理评估Ref = new List<tb_老年人生活自理能力评估_Reference>();
            ls_自理评估Ref = dal自理评估Ref.Get自理评估FromSS(rgid);

            int insertNum = dal自理评估Ref.Set参照数据(ls_自理评估Ref);
            MessageBox.Show("导入老年人自理能力评估参照数据成功!" + "\n" + "总共导入数据" + insertNum + "条.");

        }
        //End

        //Begin WXF 2018-11-20 | 16:23
        //导入医生信息
        private void Import_医生信息(string rgid)
        {
            dal_医生信息 dal医生信息 = new dal_医生信息();

            if (dal医生信息.GetTableIsNull())
            {
            }
            else
            {
                dal医生信息.DeleteAll();
            }

            int insertNum = dal医生信息.Set医生信息ToLite(dal医生信息.Get医生信息FromSS(rgid));
            MessageBox.Show("导入医生信息成功!" + "\n" + "总共导入数据" + insertNum + "条.");
        }
        //End



        #endregion

        #region 上传到服务器
        private void btn上传体检信息_Click(object sender, EventArgs e)
        {
            try
            {
                int yesImport = 0;
                int noDocNO = 0;//存在身份证号，无档案的数量
                List<tb_健康体检Info> hasImportData = new List<tb_健康体检Info>();
                List<tb_健康体检Info> list健康体检 = tb_健康体检DAL.Gettb_健康体检InfoList("  RowState is  null");
                //先在服务器上重新查询一次，看判断一下是否已经建档
                for (int i = 0; i < list健康体检.Count; i++)
                {
                    if (string.IsNullOrEmpty(list健康体检[i].个人档案编号))
                    {
                        List<string> result = tb_健康体检DAL.isExistsIn121(list健康体检[i].身份证号);
                        if (result.Count == 3) //wxf 2018年11月22日 2改为3 原:if (result.Count == 2) 为2不就永远进不来了么?
                        {
                            list健康体检[i].个人档案编号 = result[0];
                            list健康体检[i].所属机构 = result[1];
                            list健康体检[i].创建机构 = result[2];
                            //wxf 查询完毕,创建机构没有为空或null的情况
                            //if (String.IsNullOrEmpty(result[2]))
                            //{
                            //    list健康体检[i].创建机构 = result[1];
                            //}
                            //else
                            //{
                            //    list健康体检[i].创建机构 = result[2];
                            //}
                            tb_健康体检DAL.UpdateSys_DocNoPrgid(list健康体检[i]);
                        }
                    }
                }

                dal_健康体检 dal健康体检 = new dal_健康体检();

                for (int i = 0; i < list健康体检.Count; i++)
                {
                    if (!string.IsNullOrEmpty(list健康体检[i].个人档案编号))
                    {
                        if (string.IsNullOrEmpty(list健康体检[i].RowState))//未上传
                        {

                            //Begin WXF 2018-11-23 | 15:35
                            //上传体检数据判断是否需要重新组装医师签字 
                            string str_医师签字 = dal健康体检.Get医师签字(list健康体检[i].个人档案编号, list健康体检[i].所属机构, list健康体检[i].体检日期);
                            if (!string.IsNullOrEmpty(str_医师签字))
                            {
                                list健康体检[i].医师签字 = tb_健康体检DAL.merge_strJson医师签字(str_医师签字, list健康体检[i].医师签字);
                            }
                            //End

                            tb_健康体检DAL.Addtb_健康体检To121(list健康体检[i]);
                            hasImportData.Add(list健康体检[i]);
                            yesImport++;
                        }
                    }
                    else //不存在档案编号的，先在服务器上重新查询一次，看判断一下是否已经建档
                    {
                        noDocNO++;
                    }
                }

                List<tb_健康体检_住院史Info> list住院史 = tb_健康体检_住院史DAL.GetInfoList("");
                List<tb_健康体检_用药情况表Info> list用药情况表 = tb_健康体检_用药情况表DAL.GetInfoList("");
                List<tb_健康体检_非免疫规划预防接种史Info> list非免疫规划 = tb_非免疫规划DAL.GetInfoList("");
                ////先在服务器上重新查询一次，看判断一下是否已经建档
                //for (int i = 0; i < list住院史.Count; i++)
                //{
                //    if (string.IsNullOrEmpty(list住院史[i].个人档案编号))
                //    {
                //        list住院史[i].个人档案编号 = tb_健康体检DAL.isExistsIn121(list住院史[i].身份证号, list住院史[i].所属机构);
                //    }
                //}
                ////先在服务器上重新查询一次，看判断一下是否已经建档
                //for (int i = 0; i < list用药情况表.Count; i++)
                //{
                //    if (string.IsNullOrEmpty(list用药情况表[i].个人档案编号))
                //    {
                //        list用药情况表[i].个人档案编号 = tb_健康体检DAL.isExistsIn121(list用药情况表[i].身份证号, list用药情况表[i].所属机构);
                //    }
                //}
                ////先在服务器上重新查询一次，看判断一下是否已经建档
                //for (int i = 0; i < list住院史.Count; i++)
                //{
                //    if (string.IsNullOrEmpty(list住院史[i].个人档案编号))
                //    {
                //        list住院史[i].个人档案编号 = tb_健康体检DAL.isExistsIn121(list健康体检[i].身份证号, list健康体检[i].所属机构);
                //    }
                //}
                for (int i = 0; i < list住院史.Count; i++)
                {
                    tb_健康体检_住院史DAL.AddTo121(list住院史[i]);//先导入到121的相应表中
                }
                for (int i = 0; i < list用药情况表.Count; i++)
                {
                    tb_健康体检_用药情况表DAL.AddTo121(list用药情况表[i]);//先导入到121的相应表中
                }
                for (int i = 0; i < list非免疫规划.Count; i++)
                {
                    tb_非免疫规划DAL.AddTo121(list非免疫规划[i]);//先导入到121的相应表中
                }

                for (int i = 0; i < hasImportData.Count; i++)
                {
                    tb_健康体检DAL.UpdateSys_RowState(hasImportData[i]);
                }


                //Begin WXF 2019-04-22 | 18:12
                //上传B超报告
                dal_健康体检_B超 dalB超 = new dal_健康体检_B超();
                dal_健康体检_B超_Lite dalB超Lite = new dal_健康体检_B超_Lite();
                List<tb_健康体检_B超_Lite> ls_未上传 = dalB超Lite.Get未上传体检B超();
                List<tb_健康体检_B超_Lite> ls_已上传 = new List<tb_健康体检_B超_Lite>();

                int InsertCount = 0;
                int UpdateCount = 0;
                if (ls_未上传 != null && ls_未上传.Count > 0)
                {
                    foreach (tb_健康体检_B超_Lite item in ls_未上传)
                    {
                        tb_健康体检_B超 B超Bean = new tb_健康体检_B超();
                        B超Bean.个人档案编号 = item.个人档案编号;
                        B超Bean.超声所见 = item.超声所见;
                        B超Bean.超声提示 = item.超声提示;
                        B超Bean.备注 = item.备注;
                        B超Bean.所属机构 = item.所属机构;
                        B超Bean.检查日期 = item.检查日期;
                        B超Bean.创建日期 = Convert.ToDateTime(item.创建日期);
                        B超Bean.创建人 = item.创建人;

                        if (dalB超.isExistB超报告(B超Bean))
                        {
                            UpdateCount += dalB超.UpdateB超报告(B超Bean);
                        }
                        else
                        {
                            InsertCount += dalB超.InsertB超报告(B超Bean);
                        }

                        item.RowState = "1";
                        ls_已上传.Add(item);
                    }
                }

                if (ls_已上传 != null && ls_已上传.Count > 0)
                    dalB超Lite.Set体检B超上传状态(ls_已上传);
                //End

                MessageBox.Show("上传成功： " + yesImport + "条，未建档的数据总共有 " + noDocNO + "条！" + "\n" + "上传B超报告 " + InsertCount + "条,更新B超报告 " + UpdateCount + "条");
            }
            catch (Exception EX)
            {
                MessageBox.Show(EX.Message);
            }
        }

        private void btn上传体质辨识_Click(object sender, EventArgs e)
        {
            try
            {
                int yesImport = 0;
                int noDocNO = 0;//存在身份证号，无档案的数量
                List<tb_老年人中医药特征管理Info> list健康体检 = tb_老年人中医药特征管理DAL.Gettb_老年人中医药特征管理InfoList("");

                //先在服务器上重新查询一次，看判断一下是否已经建档
                for (int i = 0; i < list健康体检.Count; i++)
                {
                    if (string.IsNullOrEmpty(list健康体检[i].个人档案编号))
                    {
                        List<string> result = tb_健康体检DAL.isExistsIn121(list健康体检[i].身份证号);
                        if (result.Count == 3) //wxf 2018年11月22日 2改为3 原:if (result.Count == 2) 为2不就永远进不来了么?
                        {
                            list健康体检[i].个人档案编号 = result[0];
                            list健康体检[i].所属机构 = result[1];
                            list健康体检[i].创建机构 = result[2];
                            //wxf 查询完毕,创建机构没有为空或null的情况
                            //if (String.IsNullOrEmpty(result[2]))
                            //{
                            //    list健康体检[i].创建机构 = result[1];
                            //}
                            //else
                            //{
                            //    list健康体检[i].创建机构 = result[2];
                            //}
                            tb_老年人中医药特征管理DAL.UpdateSys_DocNoPrgid(list健康体检[i]);
                        }
                    }
                }

                for (int i = 0; i < list健康体检.Count; i++)
                {

                    if (!string.IsNullOrEmpty(list健康体检[i].个人档案编号) && string.IsNullOrEmpty(list健康体检[i].RowState))
                    {
                        tb_老年人中医药特征管理DAL.importtb_老年人中医药特征管理("USP_import中医药体质辨识", list健康体检[i]);
                        tb_老年人中医药特征管理DAL.UpdateRowState(list健康体检[i]);
                        yesImport++;
                    }
                    else if (string.IsNullOrEmpty(list健康体检[i].个人档案编号))
                    {
                        noDocNO++;
                    }
                }
                MessageBox.Show("上传成功： " + yesImport + "条，未建档的数据总共有 " + noDocNO + "条！");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn上传随访表_Click(object sender, EventArgs e)
        {
            try
            {
                int yesImport = 0;
                int noDocNO = 0;//存在身份证号，无档案的数量
                List<tb_老年人生活自理能力评价Info> list健康体检 = tb_老年人生活自理能力评价DAL.GetInfoList("");
                //先在服务器上重新查询一次，看判断一下是否已经建档
                for (int i = 0; i < list健康体检.Count; i++)
                {
                    if (string.IsNullOrEmpty(list健康体检[i].个人档案编号) && string.IsNullOrEmpty(list健康体检[i].RowState))
                    {
                        List<string> result = tb_健康体检DAL.isExistsIn121(list健康体检[i].身份证号);
                        if (result.Count == 3) //wxf 2018年11月22日 2改为3 原:if (result.Count == 2) 为2不就永远进不来了么?
                        {
                            list健康体检[i].个人档案编号 = result[0];
                            list健康体检[i].所属机构 = result[1];
                            list健康体检[i].创建机构 = result[2];
                            //wxf 查询完毕,创建机构没有为空或null的情况
                            //if (String.IsNullOrEmpty(result[2]))
                            //{
                            //    list健康体检[i].创建机构 = result[1];
                            //}
                            //else
                            //{
                            //    list健康体检[i].创建机构 = result[2];
                            //}
                            tb_老年人生活自理能力评价DAL.UpdateSys_DocNoPrgid(list健康体检[i]);
                        }
                    }
                }
                for (int i = 0; i < list健康体检.Count; i++)
                {

                    if (!string.IsNullOrEmpty(list健康体检[i].个人档案编号) && string.IsNullOrEmpty(list健康体检[i].RowState))
                    {
                        tb_老年人生活自理能力评价DAL.importtb_老年人中医药特征管理("USP_IMPORT老年人生活自理能力评价", list健康体检[i]);
                        tb_老年人生活自理能力评价DAL.UpdateRowState(list健康体检[i]);
                        yesImport++;
                    }
                    else if (string.IsNullOrEmpty(list健康体检[i].个人档案编号))
                    {
                        noDocNO++;
                    }
                }
                MessageBox.Show("上传成功： " + yesImport + "条，未建档的数据总共有 " + noDocNO + "条！");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        private void buttonEx蓝牙配置_Click(object sender, EventArgs e)
        {
            frm蓝牙配置 frm = new frm蓝牙配置();
            frm.ShowDialog();
        }

        #region 查看本地未上传数据/已体检数据
        private void btn未上传体检数据_Click(object sender, EventArgs e)
        {
            List<tb_健康体检Info> list = tb_健康体检DAL.Gettb_健康体检InfoList("  RowState ISNULL OR RowState = ''");
            if (list.Count > 0)
            {
                frm查询结果 frm = new frm查询结果("1", list, "未上传体检数据");
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("不存在未上传数据！");
            }
        }

        private void btn查看未上传体质辨识数据_Click(object sender, EventArgs e)
        {
            List<tb_老年人中医药特征管理Info> list = tb_老年人中医药特征管理DAL.Gettb_老年人中医药特征管理InfoList("  RowState ISNULL OR RowState = ''");
            if (list.Count > 0)
            {
                frm查询结果 frm = new frm查询结果("3", list, "未上传体质辨识数据");
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("不存在未上传数据！");
            }
        }

        private void btn查看未上传自理能力评估数据_Click(object sender, EventArgs e)
        {
            List<tb_老年人生活自理能力评价Info> list = tb_老年人生活自理能力评价DAL.GetInfoList("  RowState ISNULL OR RowState = ''");
            if (list.Count > 0)
            {
                frm查询结果 frm = new frm查询结果("2", list, "未上传自理能力评估数据");
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("不存在未上传数据！");
            }
        }

        private void btn查看已体检数据_Click(object sender, EventArgs e)
        {
            List<tb_健康体检Info> list = tb_健康体检DAL.Gettb_健康体检InfoList("  创建时间 >='" + DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd") + "' ");
            if (list.Count > 0)
            {
                frm查询结果 frm = new frm查询结果("1", list, "已体检数据");
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("不存在数据！");
            }
        }

        private void btn已体检中医辨识_Click(object sender, EventArgs e)
        {
            List<tb_老年人中医药特征管理Info> list = tb_老年人中医药特征管理DAL.Gettb_老年人中医药特征管理InfoList("  创建时间 >='" + DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd") + "' ");
            if (list.Count > 0)
            {
                frm查询结果 frm = new frm查询结果("3", list, "已体检中医辨识数据");
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("不存在数据！");
            }
        }

        private void btn已体检自理能力_Click(object sender, EventArgs e)
        {
            List<tb_老年人生活自理能力评价Info> list = tb_老年人生活自理能力评价DAL.GetInfoList("  创建时间 >='" + DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd") + "' ");
            if (list.Count > 0)
            {
                frm查询结果 frm = new frm查询结果("2", list, "已体检自理能力评估数据");
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("不存在数据！");
            }
        }
        #endregion

        private void frm后台操作_Load(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            Double startingPoint = (this.Width / 2) - (g.MeasureString(this.Text.Trim(), this.Font).Width / 2);
            Double ws = g.MeasureString("*", this.Font).Width;
            String tmp = " ";
            Double tw = 0;
            while ((tw + ws) < startingPoint)
            {
                tmp += "*";
                tw += ws;
            }
            this.Text = tmp.Replace("*", " ") + this.Text.Trim();
        }

        //--------王森-----------------
        private void btn获取尿常规_Click(object sender, EventArgs e)
        {
            Frm尿常规 frm尿常规 = new Frm尿常规();
            frm尿常规.ShowDialog();
        }
        //---------王森-----------------
    }
}
