﻿namespace ATOMEHR_LeaveClient
{
    partial class frmEnterIDNO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEnterIDNO));
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt身份证号 = new System.Windows.Forms.TextBox();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt档案编号 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnUpdate = new DevExpress.XtraEditors.SimpleButton();
            this.btn管理员登录 = new ATOMEHR_LeaveClient.ButtonEx();
            this.btn查询 = new ATOMEHR_LeaveClient.ButtonEx();
            this.btn打印 = new ATOMEHR_LeaveClient.ButtonEx();
            this.buttonEx2 = new ATOMEHR_LeaveClient.ButtonEx();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 21.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(113, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(481, 38);
            this.label2.TabIndex = 9;
            this.label2.Text = "基本公共卫生查体随访数据采集系统";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::ATOMEHR_LeaveClient.Properties.Resources.logo;
            this.pictureBox1.Location = new System.Drawing.Point(2, 55);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(111, 93);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(77, 239);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 24);
            this.label1.TabIndex = 10;
            this.label1.Text = "姓  名：";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(53, 315);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 24);
            this.label3.TabIndex = 11;
            this.label3.Text = "身份证号：";
            // 
            // txt身份证号
            // 
            this.txt身份证号.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt身份证号.Font = new System.Drawing.Font("宋体", 20F);
            this.txt身份证号.Location = new System.Drawing.Point(193, 309);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Size = new System.Drawing.Size(264, 38);
            this.txt身份证号.TabIndex = 13;
            // 
            // txt姓名
            // 
            this.txt姓名.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt姓名.Location = new System.Drawing.Point(193, 235);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20F);
            this.txt姓名.Properties.Appearance.Options.UseFont = true;
            this.txt姓名.Properties.NullText = "输入姓名";
            this.txt姓名.Size = new System.Drawing.Size(176, 40);
            this.txt姓名.TabIndex = 17;
            this.txt姓名.Click += new System.EventHandler(this.txt姓名_Click);
            this.txt姓名.Leave += new System.EventHandler(this.lookUpEdit1_Leave);
            // 
            // txt档案编号
            // 
            this.txt档案编号.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt档案编号.Font = new System.Drawing.Font("宋体", 20F);
            this.txt档案编号.Location = new System.Drawing.Point(193, 382);
            this.txt档案编号.Name = "txt档案编号";
            this.txt档案编号.Size = new System.Drawing.Size(264, 38);
            this.txt档案编号.TabIndex = 20;
            this.txt档案编号.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt档案编号_KeyDown);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(53, 388);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 24);
            this.label4.TabIndex = 19;
            this.label4.Text = "档案编号：";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnUpdate.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnUpdate.Appearance.Options.UseBackColor = true;
            this.btnUpdate.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.Location = new System.Drawing.Point(478, 466);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(35, 37);
            this.btnUpdate.TabIndex = 22;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btn管理员登录
            // 
            this.btn管理员登录.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn管理员登录.BackColor = System.Drawing.Color.Transparent;
            this.btn管理员登录.BaseColor = System.Drawing.Color.Transparent;
            this.btn管理员登录.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn管理员登录.Location = new System.Drawing.Point(349, 458);
            this.btn管理员登录.Margin = new System.Windows.Forms.Padding(4);
            this.btn管理员登录.Name = "btn管理员登录";
            this.btn管理员登录.Size = new System.Drawing.Size(123, 48);
            this.btn管理员登录.TabIndex = 21;
            this.btn管理员登录.Text = "系统设置";
            this.btn管理员登录.UseVisualStyleBackColor = false;
            this.btn管理员登录.Click += new System.EventHandler(this.btn管理员登录_Click);
            // 
            // btn查询
            // 
            this.btn查询.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn查询.BackColor = System.Drawing.Color.Transparent;
            this.btn查询.BaseColor = System.Drawing.Color.Yellow;
            this.btn查询.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn查询.Location = new System.Drawing.Point(371, 237);
            this.btn查询.Margin = new System.Windows.Forms.Padding(5);
            this.btn查询.Name = "btn查询";
            this.btn查询.Size = new System.Drawing.Size(85, 36);
            this.btn查询.TabIndex = 18;
            this.btn查询.Text = "查 询";
            this.btn查询.UseVisualStyleBackColor = false;
            this.btn查询.Click += new System.EventHandler(this.btn查询_Click);
            // 
            // btn打印
            // 
            this.btn打印.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn打印.BackColor = System.Drawing.Color.Transparent;
            this.btn打印.BaseColor = System.Drawing.Color.Yellow;
            this.btn打印.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn打印.Location = new System.Drawing.Point(193, 530);
            this.btn打印.Margin = new System.Windows.Forms.Padding(5);
            this.btn打印.Name = "btn打印";
            this.btn打印.Size = new System.Drawing.Size(263, 48);
            this.btn打印.TabIndex = 12;
            this.btn打印.Text = "查体条码打印";
            this.btn打印.UseVisualStyleBackColor = false;
            this.btn打印.Click += new System.EventHandler(this.btn打印_Click);
            // 
            // buttonEx2
            // 
            this.buttonEx2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonEx2.BackColor = System.Drawing.Color.Transparent;
            this.buttonEx2.BaseColor = System.Drawing.Color.Yellow;
            this.buttonEx2.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.buttonEx2.Location = new System.Drawing.Point(193, 458);
            this.buttonEx2.Margin = new System.Windows.Forms.Padding(5);
            this.buttonEx2.Name = "buttonEx2";
            this.buttonEx2.Size = new System.Drawing.Size(123, 48);
            this.buttonEx2.TabIndex = 12;
            this.buttonEx2.Text = "登  录";
            this.buttonEx2.UseVisualStyleBackColor = false;
            this.buttonEx2.Click += new System.EventHandler(this.buttonEx2_Click);
            // 
            // frmEnterIDNO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ATOMEHR_LeaveClient.Properties.Resources.main1;
            this.ClientSize = new System.Drawing.Size(595, 698);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btn管理员登录);
            this.Controls.Add(this.txt档案编号);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btn查询);
            this.Controls.Add(this.txt身份证号);
            this.Controls.Add(this.btn打印);
            this.Controls.Add(this.buttonEx2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txt姓名);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEnterIDNO";
            this.ShowIcon = false;
            this.Text = "输入居民身份证号登录";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmEnterIDNO_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private ButtonEx buttonEx2;
        private System.Windows.Forms.TextBox txt身份证号;
        private ButtonEx btn查询;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private System.Windows.Forms.TextBox txt档案编号;
        private System.Windows.Forms.Label label4;
        private ButtonEx btn管理员登录;
        private DevExpress.XtraEditors.SimpleButton btnUpdate;
        private ButtonEx btn打印;
    }
}