﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO.Ports;
using Microsoft.Win32;
using DecodeInterface;
using System.IO;
//using cn.net.sunshine.physicalExam.modules.patient;

namespace ATOMEHR_LeaveClient.ncg
{
    public partial class Frm尿常规 : DevExpress.XtraEditors.XtraForm
    {
        // com串口
        private SerialPort _comm = new SerialPort();
        //默认分配1页内存，并始终限制不允许超过
        private List<byte> _buffer = new List<byte>(4096);
        private byte[] _binary_data_1 = new byte[1024];

        private List<尿常规> _尿常规List = new List<尿常规>();

        public Frm尿常规()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 页面加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Frm尿常规_Load(object sender, EventArgs e)
        {
            if (!refreshComList())
            {
                return;
            }

            // 取配置中的尿常规com口值
            string 尿常规Com = ControlsHelper.IniReadValue("BlueTooth", "ncgCom", Path.Combine(Program.toAppPath, Program.iniName));
            //string 尿常规Com = ControlsHelper.GetConfigValue("ncgCom", ProductName + ".exe");
            // 展示当前设置的com口
            ControlsHelper.setComboBoxSelectedItem(this.cb串口号, 尿常规Com);
        }


        private List<string> getComList()
        {
            List<string> list = new List<string>();

            RegistryKey keyCom = Registry.LocalMachine.OpenSubKey("Hardware\\DeviceMap\\SerialComm");
            if (keyCom != null)
            {
                string[] sSubKeys = keyCom.GetValueNames();
                foreach (string item in sSubKeys)
                {
                    string sValue = (string)keyCom.GetValue(item);
                    list.Add(sValue);
                }
            }

            return list;
        }

        /// <summary>
        /// 刷新com端口
        /// </summary>
        /// <returns>是否有com端口</returns>
        private bool refreshComList()
        {
            bool flag = true;
            // 获取com串口列表
            List<string> comList = getComList();

            // 反填至combox展示
            ATOMEHR_LeaveClient.ControlsHelper.ComboxData cbData = new ControlsHelper.ComboxData();
            this.cb串口号.Properties.Items.Clear();
            foreach (var item in comList)
            {
                cbData.Value = item;
                cbData.Text = item;
                this.cb串口号.Properties.Items.Add(cbData);
            }

            // 无com串口
            if (comList.Count < 1)
            {
                XtraMessageBox.Show("未检测到com口!");
                flag = false;
                return flag;
            }

            return flag;
        }

        private void initCom()
        {
            this.barStaticItem1.Caption = "com串口初始化中,请稍等!";

            if (_comm.IsOpen)
            {
                _comm.Close();
            }

            //关闭时点击，则设置好端口，波特率后打开
            _comm.PortName = this.cb串口号.Text.Trim();
            _comm.BaudRate = 9600;
            try
            {
                _comm.Open();
                this.Invoke((EventHandler)(delegate { }));

                //添加事件注册
                _comm.DataReceived += comm_DataReceived;
                this.barStaticItem1.Caption = "接收准备成功,现在可以接收数据了!";
            }
            catch (Exception ex)
            {
                //捕获到异常信息，创建一个新的comm对象，之前的不能用了。
                _comm = new SerialPort();
                //现实异常信息给客户。
                MessageBox.Show(ex.Message);
            }


        }

        private void comm_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            尿常规 尿常规bean = new 尿常规();
            try
            {
                //Listening = true;//设置标记，说明我已经开始处理数据，一会儿要使用系统UI的。
                int n = _comm.BytesToRead;//先记录下来，避免某种原因，人为的原因，操作几次之间时间长，缓存不一致
                byte[] buf = new byte[n];//声明一个临时数组存储当前来的串口数据
                //received_count += n;//增加接收计数
                _comm.Read(buf, 0, n);//读取缓冲数据

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //<协议解析>
                bool data_1_catched = false;//缓存记录数据是否捕获到
                //1.缓存数据
                _buffer.AddRange(buf);
                //2.完整性判断
                while (_buffer.Count > 4)//至少要包含头（2字节）+长度（1字节）+校验（1字节）
                {
                    //请不要担心使用>=，因为>=已经和>,<,=一样，是独立操作符，并不是解析成>和=2个符号
                    //2.1 查找数据头
                    if (_buffer[0] == 0x02)
                    {
                        if (_buffer[_buffer.Count - 1] != 0x03) break;//如果最后一位不是结束

                        //至此，已经被找到了一条完整数据。我们将数据直接分析，或是缓存起来一起分析
                        //我们这里采用的办法是缓存一次，好处就是如果你某种原因，数据堆积在缓存buffer中
                        //已经很多了，那你需要循环的找到最后一组，只分析最新数据，过往数据你已经处理不及时
                        //了，就不要浪费更多时间了，这也是考虑到系统负载能够降低。
                        _binary_data_1 = new byte[_buffer.Count];
                        _buffer.CopyTo(0, _binary_data_1, 0, _buffer.Count);//复制一条完整数据到具体的数据缓存
                        data_1_catched = true;
                        _buffer.RemoveRange(0, _buffer.Count);//正确分析一条数据，从缓存中移除数据
                        continue;//继续下一次循环
                    }
                    else
                    {
                        //这里是很重要的，如果数据开始不是头，则删除数据
                        _buffer.RemoveAt(0);
                    }
                }
                //分析数据
                if (data_1_catched)
                {
                    //对缓存数据进行解码，完成解码后更新界面
                    this.Invoke((EventHandler)(delegate
                    {
                        U120 IDecode = new U120();
                        string ss = System.Text.Encoding.Default.GetString(_binary_data_1, 0, _binary_data_1.Length);
                        IDecode.DataDecode(ss);
                        if (IDecode.ItemResultsRetrun != null)
                        {
                            //DecodeInterface.ItemResult

                            foreach (DecodeInterface.ItemResult re in IDecode.ItemResultsRetrun)
                            {
                                if (re.ItemID == "Date")
                                {
                                    尿常规bean.检查时间 = Convert.ToDateTime(re.ItemValue);
                                }
                                else if (re.ItemID == "ID")
                                {
                                    尿常规bean.条码 = re.ItemValue.Replace(" ", "");
                                }
                                else if (re.ItemID.IndexOf("PRO") > -1)
                                {
                                    尿常规bean.尿蛋白 = re.ItemValue;
                                }
                                else if (re.ItemID == "mAlb")
                                {
                                    尿常规bean.尿微量白蛋白 = re.ItemValue;
                                }
                                else if (re.ItemID == "LEU")
                                {
                                    尿常规bean.尿白细胞 = re.ItemValue;
                                }
                                else if (re.ItemID.IndexOf("GLU") > -1)
                                {
                                    尿常规bean.尿糖 = re.ItemValue;
                                }
                                else if (re.ItemID.IndexOf("KET") > -1 || re.ItemID == "KET")
                                {
                                    尿常规bean.尿酮体 = re.ItemValue;
                                }
                                else if (re.ItemID.IndexOf("BLO") > -1 || re.ItemID == "BLO")
                                {
                                    尿常规bean.尿潜血 = re.ItemValue;
                                }
                            }
                            //截取凤海版的字符串
                            string tempStr = ss.Substring(ss.IndexOf("NAME") + 5, 18).Trim();
                            if (tempStr.Length == 17) 
                            {
                                tempStr = ss.Substring(ss.IndexOf("NAME") + 6, 18).Trim(); 
                            }
                            if (!string.IsNullOrEmpty(tempStr) && !tempStr.StartsWith("ID"))
                            {
                                尿常规bean.条码 = tempStr;
                            }
                        }

                    }));
                }

            }
            finally
            {
                //Listening = false;//我用完了，ui可以关闭串口了。
            }

            if (!string.IsNullOrEmpty(尿常规bean.条码) && !isExist(尿常规bean) && !尿常规bean.条码.Contains("("))
            {
                _尿常规List.Add(尿常规bean);
            }
        }

        private bool isExist(尿常规 bean)
        {
            bool flag = false;
            foreach (var item in _尿常规List)
            {
                if (string.Equals(bean.条码, item.条码) && bean.检查时间 == item.检查时间)
                {
                    flag = true;
                    break;
                }
            }

            return flag;
        }

        /// <summary>
        /// 串口改变
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cb串口号_SelectedIndexChanged(object sender, EventArgs e)
        {
            string value = ((ATOMEHR_LeaveClient.ControlsHelper.ComboxData)this.cb串口号.SelectedItem).Value;
            //ControlsHelper.SetValue("ncgCom", value, ProductName + ".exe");

            ControlsHelper.IniWriteValue("BlueTooth", "ncgCom", value, System.IO.Path.Combine(Program.toAppPath, Program.iniName));

            initCom();
        }

        /// <summary>
        /// 刷新com端口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRefreshComPort_Click(object sender, EventArgs e)
        {
            refreshComList();
        }

        private void btnRefreshData_Click(object sender, EventArgs e)
        {
            this.gc.DataSource = _尿常规List;
            this.gv.BestFitColumns();
        }

        private void Frm尿常规_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._comm.Close();
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int 成功 = 0;
                int 失败 = 0;
                int 未找到档案 = 0;
                foreach (var bean in _尿常规List)
                {
                    //为本条用户赋值
                    //bean.条码;//四十里为档案号 按照档案号查询,并赋值--有一个问题,必须要在收集尿检数据之后才能上传
                    if (setProgramCurrentUser(bean.条码))
                    {
                        //获取到本地档案
                        tb_健康体检Info tb_New = new tb_健康体检Info();
                        tb_New.尿蛋白 = bean.尿蛋白;
                        tb_New.尿潜血 = bean.尿潜血;
                        tb_New.尿糖 = bean.尿糖;
                        tb_New.尿酮体 = bean.尿酮体;
                        tb_New.尿微量白蛋白 = bean.尿微量白蛋白;//bean.尿蛋白;//尿微量白蛋白;//this.txtmAlb微量蛋白.Text.Trim();

                        if (tb_健康体检DAL.UpdateSys_尿常规(tb_New))
                        {
                            成功++;
                        }
                        else
                        {
                            失败++;
                        }
                    }
                    else 
                    {
                        未找到档案++;
                    }
                }
                MessageBox.Show("保存数据完成！成功 " + 成功 + " 条，失败 " + 失败 + " 条，跳过未找到档案 " + 未找到档案 + " 条！");
            }
            catch (Exception ex)
            {
                MessageBox.Show("保存数据失败！ " + ex.Message);
            }
        }

        /// <summary>
        /// 设置默认用户
        /// </summary>
        /// <param name="条码"></param>
        public bool setProgramCurrentUser(string 条码)
        {
            List<tb_导出用户Info> list;
            string s个人档案编号 = 条码;
            if (s个人档案编号.Length < 17)//沂水打印条码是截取了之后，所以判断一下给拼接下
                s个人档案编号 = "371323" + s个人档案编号;

            string sql = "  (个人档案编号 = '" + s个人档案编号 + "' or 个人档案编号 = '" + s个人档案编号.Substring(1) + "' ) ";
            list = tb_导出用户DAL.GetInfoList(sql);
            if (list.Count > 0)//凤海 写的是 ==1 .....有所改变
            {
                Program.currentUser.Name = DESEncrypt.DES解密(list[0].姓名);
                Program.currentUser.ID = list[0].身份证号;
                Program.currentUser.Gender = ControlsHelper.ReturnDis字典显示("xb_xingbie", list[0].性别);
                Program.currentUser.Birth = list[0].出生日期;
                Program.currentUser.Addr = list[0].居住地址;
                Program.currentUser.DocNo = list[0].个人档案编号;
                Program.currentUser.Prgid = list[0].所属机构;
                return true;
            }
            else
            {
                return false;
                //MessageBox.Show("");
                //Program.currentUser.Name = "";
                //Program.currentUser.ID = "";
                //Program.currentUser.Gender = "";
                //Program.currentUser.Birth = "";
                //Program.currentUser.Addr = "";
                //Program.currentUser.DocNo = s个人档案编号;
                //Program.currentUser.Prgid = "";
            }
        }
    }

    public class U120 : DecodeBase
    {

        public List<ItemResult> ItemResultsRetrun = new List<ItemResult>();//记录


        /// <summary>
        /// 入口：指定一段标本的开始位和结束位
        /// </summary>
        /// <param name="strComData"></param>
        public override void DataDecode(string strComData)
        {
            //在调用解码之前，数据位已经校验好开始和结束位
            base.DataDecodeComm((char)0x02, (char)0x03, 0, strComData);
        }
        /// <summary> 数据解码过程
        /// </summary>
        public override void InstrDataDecode(string strSampleData, out int Status, out  string strType, out  Exception ex, out string strSampleNo,
            out  string strDate, out List<ItemResult> ItemResults, out List<ItemImgResult> ItemImgResults, out float Heat, out float XYDepth,
            out float Haemoglobin, out string strOtherSampleData)
        {
            strType = "U120";
            strSampleNo = "";
            strDate = "";
            Status = 0;
            ItemResults = new List<ItemResult>();                       //解码项目及结果
            ItemImgResults = new List<ItemImgResult>();                 //图形结果
            Heat = 0;
            XYDepth = 0;
            Haemoglobin = 0;
            strOtherSampleData = "";

            try
            {
                strDate = System.DateTime.Now.ToString();                     //默认标本日期为当前时间(在后面的代码中再换成真实的)
                //strDate = strDate.Substring(0, strDate.Length - 2) + "00";  //去掉秒
                //strSampleData = strSampleData.Replace("\r\n", "");          //将原始数据中换行符去掉

                string[] strFGF = { "\n", "\r" };
                string[] strWhiteSpace = { " " };
                string[] strDataLines = strSampleData.Split(strFGF, StringSplitOptions.RemoveEmptyEntries);

                // -取固定位置-时间和ID 
                ItemResult newItemResultdate = new ItemResult();
                newItemResultdate.ItemID = "Date";
                newItemResultdate.ItemValue = strDataLines[1];
                ItemResults.Add(newItemResultdate);

                ItemResult newItemResultID = new ItemResult();
                newItemResultID.ItemID = "ID";
                newItemResultID.ItemValue = strDataLines[3].Replace("ID:", "");
                ItemResults.Add(newItemResultID);


                //剩下的从第4行开始读取
                for (int index = 4; index < strDataLines.Length; index++)
                {
                    if (strDataLines[index].Length < 2)
                    { }
                    else
                    {
                        if (strDataLines[index].Contains("BIOWAY B-11"))
                        {
                            int lastloc = strDataLines[index].LastIndexOf('-');
                            strSampleNo = strDataLines[index].Substring(lastloc + 1, strDataLines[index].Length - 1 - lastloc);
                            strSampleNo = Convert.ToInt64(strSampleNo).ToString();
                        }
                        else
                        {
                            string[] strItemDetail = strDataLines[index].Split(strWhiteSpace, StringSplitOptions.RemoveEmptyEntries);
                            if (strItemDetail.Length >= 2)
                            {
                                //获取个项目的值
                                ItemResult newItemResult = new ItemResult();
                                newItemResult.ItemID = strItemDetail[0];
                                newItemResult.ItemValue = strItemDetail[1];
                                ItemResults.Add(newItemResult);
                                if ((newItemResult.ItemID == "PRO" || newItemResult.ItemID.IndexOf("PRO") > -1) && strItemDetail.Length >= 4)
                                {
                                    //在这里要取一下“尿蛋白”的微量值  英文缩写：mAlb 
                                    ItemResult newItemResultPRO = new ItemResult();
                                    newItemResultPRO.ItemID = "mAlb";
                                    newItemResultPRO.ItemValue = strItemDetail[2];
                                    ItemResults.Add(newItemResultPRO);
                                }
                            }
                            else if (strItemDetail.Length == 1)
                            {
                                //获取个项目的值
                                ItemResult newItemResult = new ItemResult();
                                newItemResult.ItemID = strItemDetail[0];
                                newItemResult.ItemValue = "-";
                                ItemResults.Add(newItemResult);
                            }
                        }
                    }
                }

                //下面这两个赋值没用了
                strDate = strDataLines[1].ToString();                   //解析日期
                strSampleNo = strDataLines[3].Replace("ID:", "").ToString(); ;  //解析标本号

                ItemResultsRetrun = ItemResults;
                Status = 1;
                ex = null;
            }
            catch (Exception e)
            {
                Status = -1;
                ex = e;
            }
        }

    }
}