﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATOMEHR_LeaveClient
{
    public class 尿常规
    {
        public DateTime 检查时间 { get; set; }
        public string 条码 { get; set; }
        public string 尿蛋白 { get; set; }
        public string 尿微量白蛋白 { get; set; }        
        public string 尿白细胞 { get; set; }
        public string 尿糖 { get; set; }
        public string 尿酮体 { get; set; }
        public string 尿潜血 { get; set; }
        public string 保存状态 { get; set; }
    }
}
