﻿namespace ATOMEHR_LeaveClient
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            this.documentManager1 = new DevExpress.XtraBars.Docking2010.DocumentManager(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barDockingMenuItem1 = new DevExpress.XtraBars.BarDockingMenuItem();
            this.windowsUIView1 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.WindowsUIView(this.components);
            this.tileContainer1 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.TileContainer(this.components);
            this.documentTile体温 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            this.document体温 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.documentTile血氧 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            this.document血氧 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.documentTile尿常规 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            this.document尿常规 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.documentTile血糖 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            this.document血糖 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.documentTile随访表 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            this.document随访表 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.documentTile体检1 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            this.document体检1 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.documentTile体检2 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            this.document体检2 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.documentTile体检3 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            this.document体检3 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.documentTile视力 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            this.document视力 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.documentTile身高体重 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            this.document身高体重 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.documentTile心电 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            this.document心电 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.documentTile体质辨识 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            this.document体质辨识 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.documentTile血压 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            this.document血压 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.documentTileB超 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            this.documentB超 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtDangAnNO = new DevExpress.XtraEditors.TextEdit();
            this.btn读卡 = new DevExpress.XtraEditors.SimpleButton();
            this.txt连续扫码 = new DevExpress.XtraEditors.TextEdit();
            this.picHeader = new DevExpress.XtraEditors.PictureEdit();
            this.txt出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.txt居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt性别 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.documentManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowsUIView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tileContainer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile体温)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.document体温)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile血氧)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.document血氧)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile尿常规)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.document尿常规)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile血糖)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.document血糖)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile随访表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.document随访表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile体检1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.document体检1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile体检2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.document体检2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile体检3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.document体检3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile视力)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.document视力)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile身高体重)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.document身高体重)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile心电)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.document心电)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile体质辨识)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.document体质辨识)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile血压)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.document血压)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTileB超)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentB超)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDangAnNO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt连续扫码.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHeader.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            this.SuspendLayout();
            // 
            // documentManager1
            // 
            this.documentManager1.ContainerControl = this;
            this.documentManager1.MenuManager = this.barManager1;
            this.documentManager1.View = this.windowsUIView1;
            this.documentManager1.ViewCollection.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseView[] {
            this.windowsUIView1});
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barDockingMenuItem1});
            this.barManager1.MaxItemId = 1;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(608, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 732);
            this.barDockControlBottom.Size = new System.Drawing.Size(608, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 732);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(608, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 732);
            // 
            // barDockingMenuItem1
            // 
            this.barDockingMenuItem1.Caption = "Window";
            this.barDockingMenuItem1.Id = 0;
            this.barDockingMenuItem1.Name = "barDockingMenuItem1";
            // 
            // windowsUIView1
            // 
            this.windowsUIView1.ContentContainers.AddRange(new DevExpress.XtraBars.Docking2010.Views.WindowsUI.IContentContainer[] {
            this.tileContainer1});
            this.windowsUIView1.Documents.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseDocument[] {
            this.document体温,
            this.document血氧,
            this.document血糖,
            this.document尿常规,
            this.document视力,
            this.document随访表,
            this.document身高体重,
            this.document心电,
            this.document体检1,
            this.document体质辨识,
            this.document体检2,
            this.document体检3,
            this.document血压,
            this.documentB超});
            this.windowsUIView1.Tiles.AddRange(new DevExpress.XtraBars.Docking2010.Views.WindowsUI.BaseTile[] {
            this.documentTile体温,
            this.documentTile血氧,
            this.documentTile血糖,
            this.documentTile尿常规,
            this.documentTile视力,
            this.documentTile随访表,
            this.documentTile身高体重,
            this.documentTile心电,
            this.documentTile体检1,
            this.documentTile体质辨识,
            this.documentTile体检2,
            this.documentTile体检3,
            this.documentTile血压,
            this.documentTileB超});
            this.windowsUIView1.QueryControl += new DevExpress.XtraBars.Docking2010.Views.QueryControlEventHandler(this.windowsUIView1_QueryControl);
            this.windowsUIView1.ControlReleasing += new DevExpress.XtraBars.Docking2010.Views.ControlReleasingEventHandler(this.windowsUIView1_ControlReleasing);
            // 
            // tileContainer1
            // 
            this.tileContainer1.Items.AddRange(new DevExpress.XtraBars.Docking2010.Views.WindowsUI.BaseTile[] {
            this.documentTile体温,
            this.documentTile血氧,
            this.documentTile尿常规,
            this.documentTile血糖,
            this.documentTile随访表,
            this.documentTile体检1,
            this.documentTile体检2,
            this.documentTile体检3,
            this.documentTile视力,
            this.documentTile身高体重,
            this.documentTile心电,
            this.documentTile体质辨识,
            this.documentTile血压,
            this.documentTileB超});
            this.tileContainer1.Name = "tileContainer1";
            this.tileContainer1.Properties.ColumnCount = 0;
            this.tileContainer1.Properties.HeaderOffset = -40;
            this.tileContainer1.Properties.IndentBetweenGroups = 0;
            this.tileContainer1.Properties.IndentBetweenItems = 3;
            this.tileContainer1.Properties.ItemSize = 140;
            this.tileContainer1.Properties.RowCount = 3;
            // 
            // documentTile体温
            // 
            this.documentTile体温.Appearances.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(200)))), ((int)(((byte)(83)))));
            this.documentTile体温.Appearances.Normal.Options.UseBackColor = true;
            this.documentTile体温.Document = this.document体温;
            tileItemElement1.Appearance.Normal.BackColor = System.Drawing.Color.Transparent;
            tileItemElement1.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 12F);
            tileItemElement1.Appearance.Normal.Options.UseBackColor = true;
            tileItemElement1.Appearance.Normal.Options.UseFont = true;
            tileItemElement1.Image = global::ATOMEHR_LeaveClient.Properties.Resources.体温;
            tileItemElement1.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopCenter;
            tileItemElement1.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomInside;
            tileItemElement1.ImageSize = new System.Drawing.Size(120, 120);
            tileItemElement1.Text = "体温";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.documentTile体温.Elements.Add(tileItemElement1);
            this.documentTile体温.Name = "documentTile体温";
            this.documentTile体温.Properties.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            // 
            // document体温
            // 
            this.document体温.Caption = "体温";
            this.document体温.ControlName = "体温";
            // 
            // documentTile血氧
            // 
            this.documentTile血氧.Appearances.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(127)))), ((int)(((byte)(35)))));
            this.documentTile血氧.Appearances.Normal.Options.UseBackColor = true;
            this.documentTile血氧.Document = this.document血氧;
            tileItemElement2.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 12F);
            tileItemElement2.Appearance.Normal.Options.UseFont = true;
            tileItemElement2.Image = global::ATOMEHR_LeaveClient.Properties.Resources.心电;
            tileItemElement2.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement2.Text = "脉搏";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.documentTile血氧.Elements.Add(tileItemElement2);
            this.tileContainer1.SetID(this.documentTile血氧, 1);
            this.documentTile血氧.Name = "documentTile血氧";
            this.documentTile血氧.Properties.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            // 
            // document血氧
            // 
            this.document血氧.Caption = "脉搏";
            this.document血氧.ControlName = "脉搏";
            // 
            // documentTile尿常规
            // 
            this.documentTile尿常规.Appearances.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(184)))), ((int)(((byte)(212)))));
            this.documentTile尿常规.Appearances.Normal.Options.UseBackColor = true;
            this.documentTile尿常规.Document = this.document尿常规;
            tileItemElement3.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 12F);
            tileItemElement3.Appearance.Normal.Options.UseFont = true;
            tileItemElement3.Image = global::ATOMEHR_LeaveClient.Properties.Resources.尿常规;
            tileItemElement3.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement3.Text = "尿常规";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.documentTile尿常规.Elements.Add(tileItemElement3);
            this.tileContainer1.SetID(this.documentTile尿常规, 3);
            this.documentTile尿常规.Name = "document4Tile";
            this.documentTile尿常规.Properties.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            // 
            // document尿常规
            // 
            this.document尿常规.Caption = "尿常规";
            this.document尿常规.ControlName = "尿常规";
            this.document尿常规.Properties.AllowActivate = DevExpress.Utils.DefaultBoolean.True;
            this.document尿常规.Properties.AllowClose = DevExpress.Utils.DefaultBoolean.True;
            this.document尿常规.Properties.AllowDock = DevExpress.Utils.DefaultBoolean.True;
            this.document尿常规.Properties.AllowFloat = DevExpress.Utils.DefaultBoolean.True;
            // 
            // documentTile血糖
            // 
            this.documentTile血糖.Appearances.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(0)))));
            this.documentTile血糖.Appearances.Normal.Options.UseBackColor = true;
            this.documentTile血糖.Document = this.document血糖;
            tileItemElement4.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 12F);
            tileItemElement4.Appearance.Normal.Options.UseFont = true;
            tileItemElement4.Image = global::ATOMEHR_LeaveClient.Properties.Resources.血糖;
            tileItemElement4.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement4.Text = "血糖";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.documentTile血糖.Elements.Add(tileItemElement4);
            this.documentTile血糖.Group = "";
            this.tileContainer1.SetID(this.documentTile血糖, 2);
            this.documentTile血糖.Name = "documentTile血糖";
            this.documentTile血糖.Properties.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            // 
            // document血糖
            // 
            this.document血糖.Caption = "血糖";
            this.document血糖.ControlName = "血糖";
            // 
            // documentTile随访表
            // 
            this.documentTile随访表.Appearances.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(109)))), ((int)(((byte)(0)))));
            this.documentTile随访表.Appearances.Normal.Options.UseBackColor = true;
            this.documentTile随访表.Document = this.document随访表;
            tileItemElement5.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 12F);
            tileItemElement5.Appearance.Normal.Options.UseFont = true;
            tileItemElement5.Image = global::ATOMEHR_LeaveClient.Properties.Resources.随访;
            tileItemElement5.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement5.Text = "自理能力评估";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.documentTile随访表.Elements.Add(tileItemElement5);
            this.documentTile随访表.Group = "";
            this.tileContainer1.SetID(this.documentTile随访表, 5);
            this.documentTile随访表.Name = "document6Tile";
            this.documentTile随访表.Properties.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.documentTile随访表.Properties.RowCount = 3;
            // 
            // document随访表
            // 
            this.document随访表.Caption = "自理能力评估";
            this.document随访表.ControlName = "自理能力评估";
            // 
            // documentTile体检1
            // 
            this.documentTile体检1.Appearances.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(88)))), ((int)(((byte)(183)))));
            this.documentTile体检1.Appearances.Normal.Options.UseBackColor = true;
            this.documentTile体检1.Document = this.document体检1;
            tileItemElement6.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 11F);
            tileItemElement6.Appearance.Normal.Options.UseFont = true;
            tileItemElement6.Image = global::ATOMEHR_LeaveClient.Properties.Resources.体检;
            tileItemElement6.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement6.Text = "体检1";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.documentTile体检1.Elements.Add(tileItemElement6);
            this.documentTile体检1.Group = "";
            this.tileContainer1.SetID(this.documentTile体检1, 8);
            this.documentTile体检1.Name = "document9Tile";
            this.documentTile体检1.Properties.ItemSize = DevExpress.XtraEditors.TileItemSize.Small;
            // 
            // document体检1
            // 
            this.document体检1.ActionImage = ((System.Drawing.Image)(resources.GetObject("document体检1.ActionImage")));
            this.document体检1.Caption = "体检";
            this.document体检1.ControlName = "体检";
            // 
            // documentTile体检2
            // 
            this.documentTile体检2.Appearances.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(24)))), ((int)(((byte)(68)))));
            this.documentTile体检2.Appearances.Normal.Options.UseBackColor = true;
            this.documentTile体检2.Document = this.document体检2;
            tileItemElement7.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 11F);
            tileItemElement7.Appearance.Normal.Options.UseFont = true;
            tileItemElement7.Image = global::ATOMEHR_LeaveClient.Properties.Resources.体检;
            tileItemElement7.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement7.Text = "体检2";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.documentTile体检2.Elements.Add(tileItemElement7);
            this.documentTile体检2.Group = "";
            this.tileContainer1.SetID(this.documentTile体检2, 10);
            this.documentTile体检2.Name = "documentTile体检2";
            this.documentTile体检2.Properties.ItemSize = DevExpress.XtraEditors.TileItemSize.Small;
            // 
            // document体检2
            // 
            this.document体检2.Caption = "体检";
            this.document体检2.ControlName = "体检";
            // 
            // documentTile体检3
            // 
            this.documentTile体检3.Appearances.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.documentTile体检3.Appearances.Normal.Options.UseBackColor = true;
            this.documentTile体检3.Document = this.document体检3;
            tileItemElement8.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 11F);
            tileItemElement8.Appearance.Normal.Options.UseFont = true;
            tileItemElement8.Image = global::ATOMEHR_LeaveClient.Properties.Resources.体检;
            tileItemElement8.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement8.Text = "体检3";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.documentTile体检3.Elements.Add(tileItemElement8);
            this.documentTile体检3.Group = "";
            this.tileContainer1.SetID(this.documentTile体检3, 11);
            this.documentTile体检3.Name = "documentTile体检3";
            this.documentTile体检3.Properties.ItemSize = DevExpress.XtraEditors.TileItemSize.Small;
            this.documentTile体检3.Properties.RowCount = 2;
            // 
            // document体检3
            // 
            this.document体检3.Caption = "体检";
            this.document体检3.ControlName = "体检";
            // 
            // documentTile视力
            // 
            this.documentTile视力.Appearances.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.documentTile视力.Appearances.Normal.Options.UseBackColor = true;
            this.documentTile视力.Document = this.document视力;
            tileItemElement9.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            tileItemElement9.Appearance.Normal.Options.UseFont = true;
            tileItemElement9.Image = global::ATOMEHR_LeaveClient.Properties.Resources.视力;
            tileItemElement9.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement9.Text = "视力";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.documentTile视力.Elements.Add(tileItemElement9);
            this.documentTile视力.Group = "";
            this.tileContainer1.SetID(this.documentTile视力, 4);
            this.documentTile视力.Name = "document5Tile";
            this.documentTile视力.Properties.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            // 
            // document视力
            // 
            this.document视力.Caption = "视力";
            this.document视力.ControlName = "视力";
            // 
            // documentTile身高体重
            // 
            this.documentTile身高体重.Appearances.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(188)))), ((int)(((byte)(154)))));
            this.documentTile身高体重.Appearances.Normal.Options.UseBackColor = true;
            this.documentTile身高体重.Document = this.document身高体重;
            tileItemElement10.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 12F);
            tileItemElement10.Appearance.Normal.Options.UseFont = true;
            tileItemElement10.Image = global::ATOMEHR_LeaveClient.Properties.Resources.身高体重;
            tileItemElement10.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement10.Text = "身高体重";
            tileItemElement10.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.documentTile身高体重.Elements.Add(tileItemElement10);
            this.documentTile身高体重.Group = "";
            this.tileContainer1.SetID(this.documentTile身高体重, 6);
            this.documentTile身高体重.Name = "document7Tile";
            this.documentTile身高体重.Properties.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            // 
            // document身高体重
            // 
            this.document身高体重.Caption = "身高体重";
            this.document身高体重.ControlName = "身高体重";
            // 
            // documentTile心电
            // 
            this.documentTile心电.Appearances.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(155)))), ((int)(((byte)(19)))));
            this.documentTile心电.Appearances.Normal.Options.UseBackColor = true;
            this.documentTile心电.Document = this.document心电;
            tileItemElement11.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 12F);
            tileItemElement11.Appearance.Normal.Options.UseFont = true;
            tileItemElement11.Image = ((System.Drawing.Image)(resources.GetObject("tileItemElement11.Image")));
            tileItemElement11.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement11.Text = "心电";
            tileItemElement11.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.documentTile心电.Elements.Add(tileItemElement11);
            this.documentTile心电.Group = "";
            this.tileContainer1.SetID(this.documentTile心电, 7);
            this.documentTile心电.Name = "document8Tile";
            this.documentTile心电.Properties.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            // 
            // document心电
            // 
            this.document心电.Caption = "心电";
            this.document心电.ControlName = "心电";
            // 
            // documentTile体质辨识
            // 
            this.documentTile体质辨识.Appearances.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(76)))), ((int)(((byte)(61)))));
            this.documentTile体质辨识.Appearances.Normal.Options.UseBackColor = true;
            this.documentTile体质辨识.Document = this.document体质辨识;
            tileItemElement12.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 12F);
            tileItemElement12.Appearance.Normal.Options.UseFont = true;
            tileItemElement12.Image = global::ATOMEHR_LeaveClient.Properties.Resources.中医体质辨识;
            tileItemElement12.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement12.Text = "体质辨识";
            tileItemElement12.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.documentTile体质辨识.Elements.Add(tileItemElement12);
            this.tileContainer1.SetID(this.documentTile体质辨识, 9);
            this.documentTile体质辨识.Name = "document10Tile";
            this.documentTile体质辨识.Properties.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            // 
            // document体质辨识
            // 
            this.document体质辨识.Caption = "体质辨识";
            this.document体质辨识.ControlName = "体质辨识";
            // 
            // documentTile血压
            // 
            this.documentTile血压.Appearances.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(88)))), ((int)(((byte)(181)))));
            this.documentTile血压.Appearances.Normal.Options.UseBackColor = true;
            this.documentTile血压.Document = this.document血压;
            tileItemElement13.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 12F);
            tileItemElement13.Appearance.Normal.Options.UseFont = true;
            tileItemElement13.Image = global::ATOMEHR_LeaveClient.Properties.Resources.血压;
            tileItemElement13.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement13.Text = "血压";
            tileItemElement13.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.documentTile血压.Elements.Add(tileItemElement13);
            this.documentTile血压.Group = "";
            this.tileContainer1.SetID(this.documentTile血压, 12);
            this.documentTile血压.Name = "documentTile血压";
            this.documentTile血压.Properties.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            // 
            // document血压
            // 
            this.document血压.Caption = "血压";
            this.document血压.ControlName = "血压";
            // 
            // documentTileB超
            // 
            this.documentTileB超.Document = this.documentB超;
            tileItemElement14.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 12F);
            tileItemElement14.Appearance.Normal.Options.UseFont = true;
            tileItemElement14.Text = "B超";
            tileItemElement14.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.documentTileB超.Elements.Add(tileItemElement14);
            this.tileContainer1.SetID(this.documentTileB超, 13);
            this.documentTileB超.Name = "documentTileB超";
            this.documentTileB超.Properties.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            // 
            // documentB超
            // 
            this.documentB超.Caption = "B超";
            this.documentB超.ControlName = "B超";
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Top;
            this.dockPanel1.FloatVertical = true;
            this.dockPanel1.ID = new System.Guid("50fd6547-4f16-4909-b188-a3bea3ea33ae");
            this.dockPanel1.Location = new System.Drawing.Point(0, 0);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.AllowFloating = false;
            this.dockPanel1.Options.ShowAutoHideButton = false;
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.OriginalSize = new System.Drawing.Size(200, 182);
            this.dockPanel1.Size = new System.Drawing.Size(608, 182);
            this.dockPanel1.Text = "基本身份信息";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.panelControl1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(600, 155);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.layoutControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(600, 155);
            this.panelControl1.TabIndex = 1;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtDangAnNO);
            this.layoutControl1.Controls.Add(this.btn读卡);
            this.layoutControl1.Controls.Add(this.txt连续扫码);
            this.layoutControl1.Controls.Add(this.picHeader);
            this.layoutControl1.Controls.Add(this.txt出生日期);
            this.layoutControl1.Controls.Add(this.txt居住地址);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt性别);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(723, 1, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(596, 151);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtDangAnNO
            // 
            this.txtDangAnNO.Enabled = false;
            this.txtDangAnNO.Location = new System.Drawing.Point(404, 35);
            this.txtDangAnNO.MenuManager = this.barManager1;
            this.txtDangAnNO.Name = "txtDangAnNO";
            this.txtDangAnNO.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtDangAnNO.Properties.Appearance.Options.UseFont = true;
            this.txtDangAnNO.Size = new System.Drawing.Size(187, 22);
            this.txtDangAnNO.StyleController = this.layoutControl1;
            this.txtDangAnNO.TabIndex = 11;
            // 
            // btn读卡
            // 
            this.btn读卡.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.btn读卡.Appearance.Options.UseFont = true;
            this.btn读卡.Location = new System.Drawing.Point(496, 118);
            this.btn读卡.Name = "btn读卡";
            this.btn读卡.Size = new System.Drawing.Size(95, 28);
            this.btn读卡.StyleController = this.layoutControl1;
            this.btn读卡.TabIndex = 10;
            this.btn读卡.Text = "读 卡";
            this.btn读卡.Click += new System.EventHandler(this.btn读卡_Click);
            // 
            // txt连续扫码
            // 
            this.txt连续扫码.Location = new System.Drawing.Point(218, 118);
            this.txt连续扫码.MenuManager = this.barManager1;
            this.txt连续扫码.Name = "txt连续扫码";
            this.txt连续扫码.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txt连续扫码.Properties.Appearance.Options.UseFont = true;
            this.txt连续扫码.Size = new System.Drawing.Size(274, 22);
            this.txt连续扫码.StyleController = this.layoutControl1;
            this.txt连续扫码.TabIndex = 6;
            this.txt连续扫码.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt连续扫码_KeyDown);
            // 
            // picHeader
            // 
            this.picHeader.Location = new System.Drawing.Point(5, 5);
            this.picHeader.MenuManager = this.barManager1;
            this.picHeader.Name = "picHeader";
            this.picHeader.Size = new System.Drawing.Size(114, 122);
            this.picHeader.StyleController = this.layoutControl1;
            this.picHeader.TabIndex = 0;
            // 
            // txt出生日期
            // 
            this.txt出生日期.EditValue = "11111";
            this.txt出生日期.Enabled = false;
            this.txt出生日期.Location = new System.Drawing.Point(218, 35);
            this.txt出生日期.MenuManager = this.barManager1;
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txt出生日期.Properties.Appearance.Options.UseFont = true;
            this.txt出生日期.Properties.AutoHeight = false;
            this.txt出生日期.Size = new System.Drawing.Size(137, 22);
            this.txt出生日期.StyleController = this.layoutControl1;
            this.txt出生日期.TabIndex = 9;
            // 
            // txt居住地址
            // 
            this.txt居住地址.EditValue = "11111";
            this.txt居住地址.Enabled = false;
            this.txt居住地址.Location = new System.Drawing.Point(218, 91);
            this.txt居住地址.MenuManager = this.barManager1;
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txt居住地址.Properties.Appearance.Options.UseFont = true;
            this.txt居住地址.Properties.AutoHeight = false;
            this.txt居住地址.Size = new System.Drawing.Size(373, 23);
            this.txt居住地址.StyleController = this.layoutControl1;
            this.txt居住地址.TabIndex = 8;
            // 
            // txt身份证号
            // 
            this.txt身份证号.EditValue = "11111";
            this.txt身份证号.Enabled = false;
            this.txt身份证号.Location = new System.Drawing.Point(218, 61);
            this.txt身份证号.MenuManager = this.barManager1;
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txt身份证号.Properties.Appearance.Options.UseFont = true;
            this.txt身份证号.Properties.AutoHeight = false;
            this.txt身份证号.Size = new System.Drawing.Size(373, 26);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 7;
            // 
            // txt性别
            // 
            this.txt性别.EditValue = "11111";
            this.txt性别.Enabled = false;
            this.txt性别.Location = new System.Drawing.Point(448, 5);
            this.txt性别.MenuManager = this.barManager1;
            this.txt性别.Name = "txt性别";
            this.txt性别.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txt性别.Properties.Appearance.Options.UseFont = true;
            this.txt性别.Properties.AutoHeight = false;
            this.txt性别.Size = new System.Drawing.Size(143, 26);
            this.txt性别.StyleController = this.layoutControl1;
            this.txt性别.TabIndex = 5;
            // 
            // txt姓名
            // 
            this.txt姓名.EditValue = "11111";
            this.txt姓名.Enabled = false;
            this.txt姓名.Location = new System.Drawing.Point(218, 5);
            this.txt姓名.MenuManager = this.barManager1;
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txt姓名.Properties.Appearance.Options.UseFont = true;
            this.txt姓名.Properties.AutoHeight = false;
            this.txt姓名.Size = new System.Drawing.Size(181, 26);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem4,
            this.layoutControlItem3,
            this.layoutControlItem7,
            this.emptySpaceItem1,
            this.layoutControlItem8,
            this.layoutControlItem9});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(596, 151);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.txt姓名;
            this.layoutControlItem1.CustomizationFormText = "姓名";
            this.layoutControlItem1.Location = new System.Drawing.Point(118, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(280, 30);
            this.layoutControlItem1.Text = "姓名";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.Control = this.txt性别;
            this.layoutControlItem2.CustomizationFormText = "性别";
            this.layoutControlItem2.Location = new System.Drawing.Point(398, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(192, 30);
            this.layoutControlItem2.Text = "性别";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(42, 17);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.Control = this.txt居住地址;
            this.layoutControlItem5.CustomizationFormText = "居住地址";
            this.layoutControlItem5.Location = new System.Drawing.Point(118, 86);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(472, 27);
            this.layoutControlItem5.Text = "居住地址";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.Control = this.txt出生日期;
            this.layoutControlItem6.CustomizationFormText = "出生日期";
            this.layoutControlItem6.Location = new System.Drawing.Point(118, 30);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(236, 26);
            this.layoutControlItem6.Text = "出生日期";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.Control = this.txt身份证号;
            this.layoutControlItem4.CustomizationFormText = "身份证号";
            this.layoutControlItem4.Location = new System.Drawing.Point(118, 56);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(472, 30);
            this.layoutControlItem4.Text = "身份证号";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.picHeader;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(178, 220);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(102, 126);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(118, 126);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlItem7.Control = this.txt连续扫码;
            this.layoutControlItem7.CustomizationFormText = "连续扫码";
            this.layoutControlItem7.Location = new System.Drawing.Point(118, 113);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(373, 32);
            this.layoutControlItem7.Text = "连续扫码";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 126);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(118, 19);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btn读卡;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(491, 113);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 32);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(43, 32);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(99, 32);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            this.layoutControlItem8.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.Control = this.txtDangAnNO;
            this.layoutControlItem9.CustomizationFormText = "条码号";
            this.layoutControlItem9.Location = new System.Drawing.Point(354, 30);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(236, 26);
            this.layoutControlItem9.Text = "条码号";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(42, 17);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(608, 732);
            this.Controls.Add(this.dockPanel1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.DoubleBuffered = true;
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "公共卫生查体单机版";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.documentManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowsUIView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tileContainer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile体温)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.document体温)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile血氧)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.document血氧)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile尿常规)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.document尿常规)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile血糖)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.document血糖)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile随访表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.document随访表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile体检1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.document体检1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile体检2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.document体检2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile体检3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.document体检3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile视力)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.document视力)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile身高体重)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.document身高体重)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile心电)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.document心电)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile体质辨识)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.document体质辨识)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile血压)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.document血压)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTileB超)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentB超)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDangAnNO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt连续扫码.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHeader.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Docking2010.DocumentManager documentManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraEditors.PictureEdit picHeader;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txt出生日期;
        private DevExpress.XtraEditors.TextEdit txt居住地址;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt性别;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraBars.BarDockingMenuItem barDockingMenuItem1;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.WindowsUIView windowsUIView1;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.TileContainer tileContainer1;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile documentTile体温;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document document体温;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile documentTile血氧;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document document血氧;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile documentTile血糖;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document document血糖;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile documentTile尿常规;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document document尿常规;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile documentTile视力;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document document视力;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile documentTile随访表;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document document随访表;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile documentTile身高体重;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document document身高体重;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile documentTile心电;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document document心电;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile documentTile体检1;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document document体检1;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile documentTile体质辨识;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document document体质辨识;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile documentTile体检2;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document document体检2;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile documentTile体检3;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document document体检3;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile documentTile血压;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document document血压;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.TextEdit txt连续扫码;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.SimpleButton btn读卡;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile documentTileB超;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document documentB超;
        private DevExpress.XtraEditors.TextEdit txtDangAnNO;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;

    }
}