﻿using DevExpress.XtraEditors;
namespace ATOMEHR_LeaveClient
{
    partial class frm后台操作
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm后台操作));
            this.btn查看未上传自理能力评估数据 = new DevExpress.XtraEditors.SimpleButton();
            this.btn查看未上传体质辨识数据 = new DevExpress.XtraEditors.SimpleButton();
            this.btn未上传体检数据 = new DevExpress.XtraEditors.SimpleButton();
            this.buttonEx蓝牙配置 = new DevExpress.XtraEditors.SimpleButton();
            this.btn上传随访表 = new DevExpress.XtraEditors.SimpleButton();
            this.buttonEx3 = new DevExpress.XtraEditors.SimpleButton();
            this.btn上传体检信息 = new DevExpress.XtraEditors.SimpleButton();
            this.btn导出数据 = new DevExpress.XtraEditors.SimpleButton();
            this.btn查看已体检数据 = new DevExpress.XtraEditors.SimpleButton();
            this.btn已体检中医辨识 = new DevExpress.XtraEditors.SimpleButton();
            this.btn已体检自理能力 = new DevExpress.XtraEditors.SimpleButton();
            this.btn获取尿常规 = new DevExpress.XtraEditors.SimpleButton();
            this.btn导出参照数据 = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // btn查看未上传自理能力评估数据
            // 
            this.btn查看未上传自理能力评估数据.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn查看未上传自理能力评估数据.Appearance.Options.UseFont = true;
            this.btn查看未上传自理能力评估数据.Appearance.Options.UseTextOptions = true;
            this.btn查看未上传自理能力评估数据.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btn查看未上传自理能力评估数据.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn查看未上传自理能力评估数据.Image = ((System.Drawing.Image)(resources.GetObject("btn查看未上传自理能力评估数据.Image")));
            this.btn查看未上传自理能力评估数据.Location = new System.Drawing.Point(0, 402);
            this.btn查看未上传自理能力评估数据.Name = "btn查看未上传自理能力评估数据";
            this.btn查看未上传自理能力评估数据.Size = new System.Drawing.Size(384, 52);
            this.btn查看未上传自理能力评估数据.TabIndex = 7;
            this.btn查看未上传自理能力评估数据.Text = "查看未上传老年人自理评估数据";
            this.btn查看未上传自理能力评估数据.Click += new System.EventHandler(this.btn查看未上传自理能力评估数据_Click);
            // 
            // btn查看未上传体质辨识数据
            // 
            this.btn查看未上传体质辨识数据.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn查看未上传体质辨识数据.Appearance.Options.UseFont = true;
            this.btn查看未上传体质辨识数据.Appearance.Options.UseTextOptions = true;
            this.btn查看未上传体质辨识数据.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btn查看未上传体质辨识数据.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn查看未上传体质辨识数据.Image = ((System.Drawing.Image)(resources.GetObject("btn查看未上传体质辨识数据.Image")));
            this.btn查看未上传体质辨识数据.Location = new System.Drawing.Point(0, 350);
            this.btn查看未上传体质辨识数据.Name = "btn查看未上传体质辨识数据";
            this.btn查看未上传体质辨识数据.Size = new System.Drawing.Size(384, 52);
            this.btn查看未上传体质辨识数据.TabIndex = 6;
            this.btn查看未上传体质辨识数据.Text = "查看未上传中医体质辨识数据";
            this.btn查看未上传体质辨识数据.Click += new System.EventHandler(this.btn查看未上传体质辨识数据_Click);
            // 
            // btn未上传体检数据
            // 
            this.btn未上传体检数据.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn未上传体检数据.Appearance.Options.UseFont = true;
            this.btn未上传体检数据.Appearance.Options.UseTextOptions = true;
            this.btn未上传体检数据.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btn未上传体检数据.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn未上传体检数据.Image = ((System.Drawing.Image)(resources.GetObject("btn未上传体检数据.Image")));
            this.btn未上传体检数据.Location = new System.Drawing.Point(0, 298);
            this.btn未上传体检数据.Name = "btn未上传体检数据";
            this.btn未上传体检数据.Size = new System.Drawing.Size(384, 52);
            this.btn未上传体检数据.TabIndex = 5;
            this.btn未上传体检数据.Text = "查看未上传体检数据";
            this.btn未上传体检数据.Click += new System.EventHandler(this.btn未上传体检数据_Click);
            // 
            // buttonEx蓝牙配置
            // 
            this.buttonEx蓝牙配置.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.buttonEx蓝牙配置.Appearance.Options.UseFont = true;
            this.buttonEx蓝牙配置.Appearance.Options.UseTextOptions = true;
            this.buttonEx蓝牙配置.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.buttonEx蓝牙配置.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonEx蓝牙配置.Image = ((System.Drawing.Image)(resources.GetObject("buttonEx蓝牙配置.Image")));
            this.buttonEx蓝牙配置.Location = new System.Drawing.Point(0, 0);
            this.buttonEx蓝牙配置.Name = "buttonEx蓝牙配置";
            this.buttonEx蓝牙配置.Size = new System.Drawing.Size(384, 52);
            this.buttonEx蓝牙配置.TabIndex = 1;
            this.buttonEx蓝牙配置.Text = "蓝牙/系统配置";
            this.buttonEx蓝牙配置.Click += new System.EventHandler(this.buttonEx蓝牙配置_Click);
            // 
            // btn上传随访表
            // 
            this.btn上传随访表.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn上传随访表.Appearance.Options.UseFont = true;
            this.btn上传随访表.Appearance.Options.UseTextOptions = true;
            this.btn上传随访表.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btn上传随访表.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn上传随访表.Image = ((System.Drawing.Image)(resources.GetObject("btn上传随访表.Image")));
            this.btn上传随访表.Location = new System.Drawing.Point(0, 247);
            this.btn上传随访表.Name = "btn上传随访表";
            this.btn上传随访表.Size = new System.Drawing.Size(384, 51);
            this.btn上传随访表.TabIndex = 3;
            this.btn上传随访表.Text = "上传老年人自理评估信息(3)";
            this.btn上传随访表.Click += new System.EventHandler(this.btn上传随访表_Click);
            // 
            // buttonEx3
            // 
            this.buttonEx3.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.buttonEx3.Appearance.Options.UseFont = true;
            this.buttonEx3.Appearance.Options.UseTextOptions = true;
            this.buttonEx3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.buttonEx3.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonEx3.Image = ((System.Drawing.Image)(resources.GetObject("buttonEx3.Image")));
            this.buttonEx3.Location = new System.Drawing.Point(0, 196);
            this.buttonEx3.Name = "buttonEx3";
            this.buttonEx3.Size = new System.Drawing.Size(384, 51);
            this.buttonEx3.TabIndex = 4;
            this.buttonEx3.Text = "上传中医体质辨识信息(2)";
            this.buttonEx3.Click += new System.EventHandler(this.btn上传体质辨识_Click);
            // 
            // btn上传体检信息
            // 
            this.btn上传体检信息.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn上传体检信息.Appearance.Options.UseFont = true;
            this.btn上传体检信息.Appearance.Options.UseTextOptions = true;
            this.btn上传体检信息.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btn上传体检信息.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn上传体检信息.Image = ((System.Drawing.Image)(resources.GetObject("btn上传体检信息.Image")));
            this.btn上传体检信息.Location = new System.Drawing.Point(0, 148);
            this.btn上传体检信息.Name = "btn上传体检信息";
            this.btn上传体检信息.Size = new System.Drawing.Size(384, 48);
            this.btn上传体检信息.TabIndex = 2;
            this.btn上传体检信息.Text = "上传体检信息(1)";
            this.btn上传体检信息.Click += new System.EventHandler(this.btn上传体检信息_Click);
            // 
            // btn导出数据
            // 
            this.btn导出数据.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn导出数据.Appearance.Options.UseFont = true;
            this.btn导出数据.Appearance.Options.UseTextOptions = true;
            this.btn导出数据.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btn导出数据.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn导出数据.Image = ((System.Drawing.Image)(resources.GetObject("btn导出数据.Image")));
            this.btn导出数据.Location = new System.Drawing.Point(0, 52);
            this.btn导出数据.Name = "btn导出数据";
            this.btn导出数据.Size = new System.Drawing.Size(384, 48);
            this.btn导出数据.TabIndex = 0;
            this.btn导出数据.Text = "导入基础信息到本地数据库";
            this.btn导出数据.ToolTip = "档案、用户、机构等基础信息";
            this.btn导出数据.Click += new System.EventHandler(this.btn导出数据_Click);
            // 
            // btn查看已体检数据
            // 
            this.btn查看已体检数据.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn查看已体检数据.Appearance.Options.UseFont = true;
            this.btn查看已体检数据.Appearance.Options.UseTextOptions = true;
            this.btn查看已体检数据.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btn查看已体检数据.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn查看已体检数据.Image = ((System.Drawing.Image)(resources.GetObject("btn查看已体检数据.Image")));
            this.btn查看已体检数据.Location = new System.Drawing.Point(0, 454);
            this.btn查看已体检数据.Name = "btn查看已体检数据";
            this.btn查看已体检数据.Size = new System.Drawing.Size(384, 52);
            this.btn查看已体检数据.TabIndex = 8;
            this.btn查看已体检数据.Text = "查看已体检数据";
            this.btn查看已体检数据.Click += new System.EventHandler(this.btn查看已体检数据_Click);
            // 
            // btn已体检中医辨识
            // 
            this.btn已体检中医辨识.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn已体检中医辨识.Appearance.Options.UseFont = true;
            this.btn已体检中医辨识.Appearance.Options.UseTextOptions = true;
            this.btn已体检中医辨识.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btn已体检中医辨识.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn已体检中医辨识.Image = ((System.Drawing.Image)(resources.GetObject("btn已体检中医辨识.Image")));
            this.btn已体检中医辨识.Location = new System.Drawing.Point(0, 506);
            this.btn已体检中医辨识.Name = "btn已体检中医辨识";
            this.btn已体检中医辨识.Size = new System.Drawing.Size(384, 52);
            this.btn已体检中医辨识.TabIndex = 9;
            this.btn已体检中医辨识.Text = "查看已体检中医辨识数据";
            this.btn已体检中医辨识.Click += new System.EventHandler(this.btn已体检中医辨识_Click);
            // 
            // btn已体检自理能力
            // 
            this.btn已体检自理能力.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn已体检自理能力.Appearance.Options.UseFont = true;
            this.btn已体检自理能力.Appearance.Options.UseTextOptions = true;
            this.btn已体检自理能力.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btn已体检自理能力.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn已体检自理能力.Image = ((System.Drawing.Image)(resources.GetObject("btn已体检自理能力.Image")));
            this.btn已体检自理能力.Location = new System.Drawing.Point(0, 558);
            this.btn已体检自理能力.Name = "btn已体检自理能力";
            this.btn已体检自理能力.Size = new System.Drawing.Size(384, 52);
            this.btn已体检自理能力.TabIndex = 10;
            this.btn已体检自理能力.Text = "查看已体检自理能力数据";
            this.btn已体检自理能力.Click += new System.EventHandler(this.btn已体检自理能力_Click);
            // 
            // btn获取尿常规
            // 
            this.btn获取尿常规.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn获取尿常规.Appearance.Options.UseFont = true;
            this.btn获取尿常规.Appearance.Options.UseTextOptions = true;
            this.btn获取尿常规.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btn获取尿常规.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn获取尿常规.Image = ((System.Drawing.Image)(resources.GetObject("btn获取尿常规.Image")));
            this.btn获取尿常规.Location = new System.Drawing.Point(0, 610);
            this.btn获取尿常规.Name = "btn获取尿常规";
            this.btn获取尿常规.Size = new System.Drawing.Size(384, 52);
            this.btn获取尿常规.TabIndex = 11;
            this.btn获取尿常规.Text = "获取尿常规(四十里堡)";
            this.btn获取尿常规.Click += new System.EventHandler(this.btn获取尿常规_Click);
            // 
            // btn导出参照数据
            // 
            this.btn导出参照数据.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn导出参照数据.Appearance.Options.UseFont = true;
            this.btn导出参照数据.Appearance.Options.UseTextOptions = true;
            this.btn导出参照数据.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btn导出参照数据.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn导出参照数据.Image = ((System.Drawing.Image)(resources.GetObject("btn导出参照数据.Image")));
            this.btn导出参照数据.Location = new System.Drawing.Point(0, 100);
            this.btn导出参照数据.Name = "btn导出参照数据";
            this.btn导出参照数据.Size = new System.Drawing.Size(384, 48);
            this.btn导出参照数据.TabIndex = 1;
            this.btn导出参照数据.Text = "导入参照数据到本地数据库";
            this.btn导出参照数据.ToolTip = "档案、用户、机构等基础信息";
            this.btn导出参照数据.Click += new System.EventHandler(this.btn导出参照数据_Click);
            // 
            // frm后台操作
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 674);
            this.Controls.Add(this.btn获取尿常规);
            this.Controls.Add(this.btn已体检自理能力);
            this.Controls.Add(this.btn已体检中医辨识);
            this.Controls.Add(this.btn查看已体检数据);
            this.Controls.Add(this.btn查看未上传自理能力评估数据);
            this.Controls.Add(this.btn查看未上传体质辨识数据);
            this.Controls.Add(this.btn未上传体检数据);
            this.Controls.Add(this.btn上传随访表);
            this.Controls.Add(this.buttonEx3);
            this.Controls.Add(this.btn上传体检信息);
            this.Controls.Add(this.btn导出参照数据);
            this.Controls.Add(this.btn导出数据);
            this.Controls.Add(this.buttonEx蓝牙配置);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm后台操作";
            this.Text = "后台操作";
            this.Load += new System.EventHandler(this.frm后台操作_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private SimpleButton btn导出数据;
        private SimpleButton btn上传体检信息;
        private SimpleButton buttonEx3;
        private SimpleButton btn上传随访表;
        private SimpleButton buttonEx蓝牙配置;
        private SimpleButton btn未上传体检数据;
        private SimpleButton btn查看未上传体质辨识数据;
        private SimpleButton btn查看未上传自理能力评估数据;
        private SimpleButton btn查看已体检数据;
        private SimpleButton btn已体检中医辨识;
        private SimpleButton btn已体检自理能力;
        private SimpleButton btn获取尿常规;
        private SimpleButton btn导出参照数据;
    }
}