﻿using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableXtraReport;

namespace ATOMEHR_LeaveClient
{
    public partial class frm查询姓名 : Form
    {
        string name;
        private List<tb_导出用户Info> list;

        public frm查询姓名()
        {
            InitializeComponent();
        }
        public frm查询姓名(string _name)
        {
            InitializeComponent();
            name = _name;
        }

        public frm查询姓名(string p, List<tb_导出用户Info> _list)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            this.name = p;
            this.list = _list;
            for (int i = 0; i < list.Count; i++)
            {
                list[i].姓名 = DESEncrypt.DES解密(list[i].姓名);
                list[i].性别 = ControlsHelper.ReturnDis字典显示("xb_xingbie", list[i].性别);
            }
            this.textEdit1.Text = name;
            this.gridControl1.DataSource = list;
            this.gridView1.BestFitColumns();
        }

        private void btn查询_Click(object sender, EventArgs e)
        {
            List<tb_导出用户Info> list;
            if (string.IsNullOrEmpty(this.textEdit1.Text.Trim()))
            {
                MessageBox.Show("请输入要查找的姓名", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            string sql = "  (姓名 like '" + DESEncrypt.DES加密(this.textEdit1.Text.Trim()) + "%' or 姓名 like'" + this.textEdit1.Text.Trim() + "%') ";
            if (this.checkEdit1.Checked)
                sql += " and substr(出生日期,1,4)<='" + DateTime.Now.AddYears(-65).ToString("yyyy-MM-dd") + "' ";
            list = tb_导出用户DAL.GetInfoList(sql);
            if (list.Count == 0)
            {
                MessageBox.Show("未检索到相关的用户信息！");
                return;
            }
            for (int i = 0; i < list.Count; i++)
            {
                list[i].姓名 = DESEncrypt.DES解密(list[i].姓名);
                list[i].性别 = ControlsHelper.ReturnDis字典显示("xb_xingbie", list[i].性别);
            }
            this.gridControl1.DataSource = list;
        }

        private void gridControl1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //GridHitInfo hInfo = gridView1.CalcHitInfo(new Point(e.X, e.Y));
            if (gridView1.GetFocusedRow() != null)//hInfo.InRow)
            {
                tb_导出用户Info tb = this.gridView1.GetFocusedRow() as tb_导出用户Info;
                if (tb == null) return;
                Program.currentUser.Addr = tb.居住地址;
                Program.currentUser.Birth = tb.出生日期;
                Program.currentUser.Gender = tb.性别;
                Program.currentUser.ID = tb.身份证号;
                Program.currentUser.Name = tb.姓名;
                Program.currentUser.DocNo = tb.个人档案编号;
                Program.currentUser.Prgid = tb.所属机构;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
        }

        private void btn确定_Click(object sender, EventArgs e)
        {
            gridControl1_MouseDoubleClick(sender, null);
        }

        private void tbn打印_Click(object sender, EventArgs e)
        {
            tb_导出用户Info tb = this.gridView1.GetFocusedRow() as tb_导出用户Info;

            if (tb == null) return;

            string bartype = "7"; //老年人查体类型
            string s档案号 = tb.个人档案编号;
            string s出生日期 = tb.出生日期;
            string s年龄 = (System.DateTime.Now.Year - Convert.ToDateTime(s出生日期).Year).ToString();
            string s姓名 = tb.姓名;
            string s性别 = tb.性别;
            string s检查 = "老年人查体";
            if (s档案号.Length < 18)
                s档案号 = bartype + s档案号;
            TableXReport xtr = new TableXReport();

            Font ft = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            xtr.SetReportTitle("打印时间 " + System.DateTime.Now.ToString(), true, ft, TextAlignment.TopLeft, Size.Empty);
            xtr.SetReportTitle(s姓名 + "/" + s性别 + "/" + s年龄 + "岁 查体", true, ft, TextAlignment.TopLeft, Size.Empty);
            xtr.SetReportTitle(s检查, true, ft, TextAlignment.TopLeft, Size.Empty);
            //指定条码格式
            DevExpress.XtraPrinting.BarCode.Code128Generator barcode = new DevExpress.XtraPrinting.BarCode.Code128Generator();
            barcode.CharacterSet = DevExpress.XtraPrinting.BarCode.Code128Charset.CharsetC;

            xtr.SetReportBarCode(s档案号.Substring(0), new Point(5, 135), new Size(400, 125), barcode, true);
            xtr.TextAlignment = TextAlignment.BottomCenter;
            //SetReportMain只能最后设置一次    
            xtr.SetReportMain(0, 0, 1, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(20, 20, 10, 10), System.Drawing.Printing.PaperKind.Custom, new Size(500, 300), 0, 0, "");

            //预览条形码页面(该页面有打印功能)
            ReportPrintTool tool = new ReportPrintTool(xtr);
            tool.Print();
            tool.ShowPreviewDialog();            
        }
    }
}
