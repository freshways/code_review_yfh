﻿namespace ATOMEHR_LeaveClient
{
    partial class frmLogin
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLogin));
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl提示 = new System.Windows.Forms.Label();
            this.btnUpdate = new DevExpress.XtraEditors.SimpleButton();
            this.btn管理员登录 = new ATOMEHR_LeaveClient.ButtonEx();
            this.buttonEx3 = new ATOMEHR_LeaveClient.ButtonEx();
            this.buttonEx2 = new ATOMEHR_LeaveClient.ButtonEx();
            this.buttonEx1 = new ATOMEHR_LeaveClient.ButtonEx();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(165, 257);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(221, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "请刷居民身份证登录系统";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::ATOMEHR_LeaveClient.Properties.Resources.logo;
            this.pictureBox1.Location = new System.Drawing.Point(8, 56);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(111, 93);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 21.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(119, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(481, 38);
            this.label2.TabIndex = 7;
            this.label2.Text = "基本公共卫生查体随访数据采集系统";
            // 
            // lbl提示
            // 
            this.lbl提示.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl提示.AutoSize = true;
            this.lbl提示.BackColor = System.Drawing.Color.Transparent;
            this.lbl提示.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl提示.Location = new System.Drawing.Point(300, 382);
            this.lbl提示.Name = "lbl提示";
            this.lbl提示.Size = new System.Drawing.Size(0, 25);
            this.lbl提示.TabIndex = 8;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnUpdate.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnUpdate.Appearance.Options.UseBackColor = true;
            this.btnUpdate.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.Location = new System.Drawing.Point(282, 596);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(36, 36);
            this.btnUpdate.TabIndex = 9;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btn管理员登录
            // 
            this.btn管理员登录.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn管理员登录.BackColor = System.Drawing.Color.Transparent;
            this.btn管理员登录.BaseColor = System.Drawing.Color.Transparent;
            this.btn管理员登录.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn管理员登录.Location = new System.Drawing.Point(239, 521);
            this.btn管理员登录.Margin = new System.Windows.Forms.Padding(5);
            this.btn管理员登录.Name = "btn管理员登录";
            this.btn管理员登录.Size = new System.Drawing.Size(123, 48);
            this.btn管理员登录.TabIndex = 5;
            this.btn管理员登录.Text = "管理员登录";
            this.btn管理员登录.UseVisualStyleBackColor = false;
            this.btn管理员登录.Click += new System.EventHandler(this.btn管理员登录_Click);
            // 
            // buttonEx3
            // 
            this.buttonEx3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonEx3.BackColor = System.Drawing.Color.Transparent;
            this.buttonEx3.BaseColor = System.Drawing.Color.Yellow;
            this.buttonEx3.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.buttonEx3.Location = new System.Drawing.Point(150, 433);
            this.buttonEx3.Margin = new System.Windows.Forms.Padding(5);
            this.buttonEx3.Name = "buttonEx3";
            this.buttonEx3.Size = new System.Drawing.Size(301, 48);
            this.buttonEx3.TabIndex = 3;
            this.buttonEx3.Text = "输入身份证号登录";
            this.buttonEx3.UseVisualStyleBackColor = false;
            this.buttonEx3.Click += new System.EventHandler(this.buttonEx3_Click);
            // 
            // buttonEx2
            // 
            this.buttonEx2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonEx2.BackColor = System.Drawing.Color.Transparent;
            this.buttonEx2.BaseColor = System.Drawing.Color.Yellow;
            this.buttonEx2.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.buttonEx2.Location = new System.Drawing.Point(328, 307);
            this.buttonEx2.Margin = new System.Windows.Forms.Padding(5);
            this.buttonEx2.Name = "buttonEx2";
            this.buttonEx2.Size = new System.Drawing.Size(123, 48);
            this.buttonEx2.TabIndex = 2;
            this.buttonEx2.Text = "读取";
            this.buttonEx2.UseVisualStyleBackColor = false;
            this.buttonEx2.Click += new System.EventHandler(this.buttonEx2_Click);
            // 
            // buttonEx1
            // 
            this.buttonEx1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonEx1.BackColor = System.Drawing.Color.Transparent;
            this.buttonEx1.BaseColor = System.Drawing.Color.Yellow;
            this.buttonEx1.Font = new System.Drawing.Font("微软雅黑", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.buttonEx1.Location = new System.Drawing.Point(150, 307);
            this.buttonEx1.Margin = new System.Windows.Forms.Padding(5);
            this.buttonEx1.Name = "buttonEx1";
            this.buttonEx1.Size = new System.Drawing.Size(123, 48);
            this.buttonEx1.TabIndex = 1;
            this.buttonEx1.Text = "连接设备";
            this.buttonEx1.UseVisualStyleBackColor = false;
            this.buttonEx1.Click += new System.EventHandler(this.buttonEx1_Click);
            // 
            // frmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ATOMEHR_LeaveClient.Properties.Resources.main1;
            this.ClientSize = new System.Drawing.Size(600, 698);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.lbl提示);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btn管理员登录);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonEx3);
            this.Controls.Add(this.buttonEx2);
            this.Controls.Add(this.buttonEx1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("宋体", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLogin";
            this.ShowIcon = false;
            this.Text = "基本公共卫生查体随访终端系统";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmLogin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ButtonEx buttonEx1;
        private ButtonEx buttonEx2;
        private ButtonEx buttonEx3;
        private System.Windows.Forms.Label label1;
        private ButtonEx btn管理员登录;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl提示;
        private DevExpress.XtraEditors.SimpleButton btnUpdate;


    }
}

