﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;
using System.IO.Ports;
using ATOMEHR_LeaveClient.View;
using serialPort.PortClass;

namespace ATOMEHR_LeaveClient
{
    public partial class frmMain : DevExpress.XtraEditors.XtraForm
    {
        private SerialPort PublicSP = new SerialPort();
        public frmMain()
        {
            InitializeComponent();
            LoadUserInfo();

            //Begin WXF 2019-04-18 | 14:57
            //取消最大尺寸的限制 
            //this.Width = System.Windows.Forms.SystemInformation.WorkingArea.Width;
            //this.Height = (System.Windows.Forms.SystemInformation.WorkingArea.Height - 80);
            //this.MaximumSize = new Size(Width, Height);
            //End
											 
            //this.Location = new Point(
            //    Screen.PrimaryScreen.WorkingArea.Width - this.Size.Width, //屏幕工作区域减去窗体宽度
            //    Screen.PrimaryScreen.WorkingArea.Height - this.Size.Height); //屏幕工作区域减去窗体高度

            this.WindowState = FormWindowState.Maximized;
        }

        private void LoadUserInfo()
        {
            this.txt姓名.Text = Program.currentUser.Name;
            this.txt性别.Text = Program.currentUser.Gender;
            this.txt出生日期.Text = Program.currentUser.Birth;//string.Format(Program.currentUser.Birth, "yyyy-MM-dd"); 
            this.txt身份证号.Text = Program.currentUser.ID;
            this.txt居住地址.Text = Program.currentUser.Addr;
            this.txtDangAnNO.Text = Program.currentUser.DocNo;//add by lid
            PicBoxLoadPic(picHeader, Program.currentUser.PhotoPath, 0);
        }

        public bool PicBoxLoadPic(PictureEdit pictureBox, string picFileName, int picshow)
        {
            bool ret = false;
            try
            {
                if (!File.Exists(picFileName))
                {
                    pictureBox.Image = null;
                    return ret;
                }
                /*
                if (picshow == 1)
                {
                    pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
                }
                else 
                {
                    pictureBox.SizeMode = PictureBoxSizeMode.AutoSize;
                }*/
                pictureBox.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
                //这种方式不好，载入文件后一直占有，所以改用流的方式
                //Bitmap myImage = new Bitmap(picFileName);
                //pictureBox.Size = myImage.Size;//new Size(300,400);
                //pictureBox.Image = (Image)myImage;
                //pictureBox.Image = Bitmap.FromFile(picFileName); //这种方式不好，载入文件后一直占有
                using (FileStream fs = new FileStream(picFileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    BinaryReader br = new BinaryReader(fs);
                    MemoryStream ms = new MemoryStream(br.ReadBytes((int)fs.Length));
                    pictureBox.Image = Image.FromStream(ms);
                }
                ret = true;
            }
            catch (Exception ex)
            {
            }
            return ret;
        }

        Port_Temperature PortTe = null;
        Port_HeightAndWeight PortHW = null;
        Port_BloodPressure PortBP = null;
		
        private void windowsUIView1_QueryControl(object sender, DevExpress.XtraBars.Docking2010.Views.QueryControlEventArgs e)
        {
            if (e.Document == document体温)
            {
                if (Program.BlueType.Contains("体温"))
                {
                    switch (Program.BlueType)
                    {
                        case "体温":
                            uc体温 control = new uc体温(PublicSP);
                            control.TxtParentBarCode = this.txt连续扫码;
                            e.Control = control;
                            break;
                        case "体温(有线)":
                            if (PortTe == null)
                            {
                                PortTe = new Port_Temperature();
                                PortTe.PortName = Program.ThermometerGunCom;
                                uc体温COM control2 = new uc体温COM(PortTe);
                                control2.TxtParentBarCode = this.txt连续扫码;
                                e.Control = control2;
                            }
                            break;
                        default:
                            MessageBox.Show("不支持此模块！请使用【" + Program.BlueType + "】模块！");
                            e.Control = new Panel();
                            break;
                    }                    
                }
            }
            else if (e.Document == document血氧)
            {
                if (Program.BlueType != "脉搏")
                {
                    MessageBox.Show("不支持此模块！请使用【" + Program.BlueType + "】模块！");
                    e.Control = new Panel();
                    return;
                }
                e.Control = new uc血氧检测();
                e.Control.Dock = DockStyle.Fill;
            }
            else if (e.Document == document心电)
            {
                if (Program.BlueType != "心电")
                {
                    MessageBox.Show("不支持此模块！请使用【" + Program.BlueType + "】模块！");
                    e.Control = new Panel();
                    return;
                }
                e.Control = new uc心电PC80B();
                e.Control.Dock = DockStyle.Fill;
            }
            else if (e.Document == documentB超)
            {
                //e.Control = new ucB超();
                //e.Control.Dock = DockStyle.Fill;

                //update by lid---B超连续扫描时获得焦点--start
                ucB超 control = new ucB超();
                control.TxtParentBarCode = this.txt连续扫码;
                e.Control = control;
                e.Control.Dock = DockStyle.Fill;
                // update by lid ---end
            }
            else if (e.Document == document血压)
            {
                switch (Program.BlueType)
                {
                    case "血压":
                        uc血压 control = new uc血压(PublicSP);
                        control.TxtParentBarCode = this.txt连续扫码;

                        e.Control = control;
                        break;
                    case "血压RBP9804":
                        e.Control = new uc血压RBP9804();
                        break;
                    case "血压RBP9808(有线)":
                        if (PortBP == null)
                        {
                            PortBP = new Port_BloodPressure();
                            PortBP.PortName = Program.ThermometerGunCom;
                        }

                        uc血压RBP9808 control3 = new uc血压RBP9808(PortBP);
                         control3.TxtParentBarCode = this.txt连续扫码;
                         e.Control = control3;
                         break;
                    default:
                        MessageBox.Show("不支持此模块！请使用【" + Program.BlueType + "】模块！");
                        break;
                }

                e.Control.Dock = DockStyle.Fill;

                //if (Program.BlueType == "血压" || Program.BlueType == "血压RBP9804")
                //{//这个地方写反了，重新纠正
                //    if (Program.BlueType == "血压")
                //    {
                //        uc血压 control = new uc血压(PublicSP);
                //        control.TxtParentBarCode = this.txt连续扫码;

                //        e.Control = control;
                //    }
                //    else if (Program.BlueType == "血压RBP9804")
                //        e.Control = new uc血压RBP9804();
                //    e.Control.Dock = DockStyle.Fill;
                //}
                //else
                //{
                //    MessageBox.Show("不支持此模块！请使用【" + Program.BlueType + "】模块！");
                //    e.Control = new Panel();
                //    return;
                //}
            }
            else if (e.Document == document尿常规)
            {
                if (Program.BlueType != "尿常规")
                {
                    MessageBox.Show("不支持此模块！请使用【" + Program.BlueType + "】模块！");
                    e.Control = new Panel();
                    return;
                }
                e.Control = new uc尿常规(PublicSP);
                e.Control.Dock = DockStyle.Fill;
            }
            else if (e.Document == document视力)
            {
                e.Control = new UC视力();
                e.Control.Dock = DockStyle.Fill;
            }
            else if (e.Document == document身高体重)
            {
                if (Program.BlueType != "身高体重")
                {
                    MessageBox.Show("不支持此模块！请使用【" + Program.BlueType + "】模块！");
                    e.Control = new Panel();
                    return;
                }

                uc身高体重 control = new uc身高体重(PublicSP);
                control.TxtParentBarCode = this.txt连续扫码;
                e.Control = control;
                e.Control.Dock = DockStyle.Fill;

            }
            else if (e.Document == document体质辨识)
            {
                string id = Program.currentUser.ID;
                if (!string.IsNullOrEmpty(id))
                {
                    if (id.Length < 6) { MessageBox.Show("此人身份证号格式错误，不能进行中医辨识体质！"); e.Control = new Panel(); return; }
                    string birthYear = id.Substring(6, 4);
                    int _year = DateTime.Now.Year;
                    if (_year - Convert.ToInt32(birthYear) < 65)
                    {
                        if (MessageBox.Show("此人年龄未达65岁，是否进行中医辨识体质？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.No)
                        {
                            e.Control = new Panel();
                            return;
                        }
                    }
                }
                e.Control = new uc中医体质辨识();
            }
            else if (e.Document == document体检1)
            {
                e.Control = new uc健康体检表1();
            }
            else if (e.Document == document体检2)
            {
                e.Control = new uc健康体检表2();
            }
            else if (e.Document == document体检3)
            {
                e.Control = new uc健康体检表3();
            }
            else if (e.Document == document随访表)
            {
                string id = Program.currentUser.ID;
                if (!string.IsNullOrEmpty(id))
                {
                    // modify by 朱文杰 2017-05-26 10:24 for 15位身份证年龄计算错误 begin
                    int _year = DateTime.Now.Year;
                    string birthYear = string.Empty;
                    if (id.Length == 18)
                    {
                        birthYear = id.Substring(6, 4);
                    }
                    else
                    {
                        birthYear = "19" + id.Substring(6, 2);
                    }
                    // modify by 朱文杰 2017-05-26 10:24 for 15位身份证年龄计算错误 end
                    if (_year - Convert.ToInt32(birthYear) < 65)
                    {
                        MessageBox.Show("此人年龄未达65岁，不能进行自理评估！");
                        e.Control = new Panel();
                        return;
                    }
                }
                e.Control = new uc老年人随访();
            }
            else if (e.Document == document血糖)
            {
                if (Program.BlueType != "血糖")
                {
                    MessageBox.Show("不支持此模块！请使用【" + Program.BlueType + "】模块！");
                    e.Control = new Panel();
                    return;
                }
                e.Control = new uc血糖(PublicSP);
            }
            else
                return;
        }

        private void windowsUIView1_ControlReleasing(object sender, DevExpress.XtraBars.Docking2010.Views.ControlReleasingEventArgs e)
        {
            if (e.Document == document身高体重 || e.Document == document体温 || e.Document == document血压)
            {
                // 默认即为保持连接

            }

            else
            {
                //菜单返回事件,都需要释放后重新加载
                e.Cancel = false; //取消相关事件
                e.KeepControl = false; //不保持控件
            }
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            //如果是连接中，先关闭
            if (PublicSP != null)
            {
                if (PublicSP.IsOpen)
                {
                    PublicSP.Close();
                }
            }
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Dispose();
            //Application.Restart();
        }

        private void txt连续扫码_KeyDown(object sender, KeyEventArgs e)
        {
            if (string.IsNullOrEmpty(this.txt连续扫码.Text)) return;
            //add by lid---start
            string newDabh = "";//新的档案编号
            string bh = txt连续扫码.Text.Trim();
            // add by 朱文杰 2017-05-26 10:23 for 联系扫码直接回车报错 begin
            if (bh.Length < 3)
            {
                return;
            }
            // add by 朱文杰 2017-05-26 10:23 for 联系扫码直接回车报错 end
            /*
            if ("1".Equals(bh.Substring(0, 1)))
            {
                string jigouNo=ControlsHelper.IniReadValue("BlueTooth", "ParentRGID", Path.Combine(Program.toAppPath, Program.iniName));
                //蒙阴档案号17位
                if (jigouNo.Equals("371328"))
                {
                    newDabh = "37" + bh.Substring(1, bh.Length - 1);
                }
                //沂水档案号18位
                else if (jigouNo.Equals("371323"))
                {
                    newDabh = "371" + bh.Substring(1, bh.Length - 1);
                }

                //newDabh = "37" + bh.Substring(1, bh.Length - 1);
            }*/
            if ("1".Equals(bh.Substring(0, 1)))
            {
                //newDabh = "37" + bh.Substring(1, bh.Length - 1);
                string jigouNo = ControlsHelper.IniReadValue("BlueTooth", "ParentRGID", Path.Combine(Program.toAppPath, Program.iniName));
                //蒙阴档案号17位
                if (jigouNo.Equals("371328"))
                {
                    newDabh = "37" + bh.Substring(1, bh.Length - 1);
                }
                //沂水档案号18位
                else if (jigouNo.Equals("371323"))
                {
                    newDabh = "371" + bh.Substring(1, bh.Length - 1);
                }

            }
            else if ("2".Equals(bh.Substring(0, 1)))
            {
                newDabh = "371" + bh.Substring(1, bh.Length - 1);


                //newDabh = "37" + bh.Substring(1, bh.Length - 1);
            }
            else
            {
                newDabh = bh;
            }
            //add by lid---end
            List<tb_导出用户Info> list;
            if (e.KeyCode == Keys.Enter)
            {
                //string s个人档案编号 = txt连续扫码.Text.Trim();
                string s个人档案编号 = newDabh;// add by lid
                //if (txt连续扫码.Text.Trim().Length < 17)//沂水打印条码是截取了之后，所以判断一下给拼接下
                //    s个人档案编号 = "371323" + s个人档案编号;

                string sql = "  (个人档案编号 = '" + s个人档案编号 + "' or 个人档案编号 = '" + s个人档案编号.Substring(1) + "' ) ";
                list = tb_导出用户DAL.GetInfoList(sql);
                if (list.Count == 1)
                {
                    //this.txt姓名.Text = DESEncrypt.DES解密(list[0].姓名);
                    //this.txt身份证号.Text = list[0].身份证号;
                    //this.txt连续扫码.Text = list[0].个人档案编号;
                    Program.currentUser.Name = DESEncrypt.DES解密(list[0].姓名);
                    Program.currentUser.ID = list[0].身份证号;
                    Program.currentUser.Gender = ControlsHelper.ReturnDis字典显示("xb_xingbie", list[0].性别);
                    Program.currentUser.Birth = list[0].出生日期;
                    Program.currentUser.Addr = list[0].居住地址;
                    Program.currentUser.DocNo = list[0].个人档案编号;
                    Program.currentUser.Prgid = list[0].所属机构;

                    LoadUserInfo();
                    this.txt连续扫码.Text = "";
                }
                else
                {// add by lid---start
                    Program.currentUser.DocNo = newDabh;
                    //LoadUserInfo();
                    this.txt姓名.Text = "";
                    this.txt性别.Text = "";
                    this.txt出生日期.Text = "";//string.Format(Program.currentUser.Birth, "yyyy-MM-dd"); 
                    this.txt身份证号.Text = "";
                    this.txt居住地址.Text = "";
                    this.txt连续扫码.Text = "";
                    this.txtDangAnNO.Text = Program.currentUser.DocNo;
                }// add by lid---end
                // update by lid---start
                //else if (list.Count == 0)
                //{
                //    MessageBox.Show("未检索到相关的用户信息！");
                //    this.txt连续扫码.Text = "";
                //    return;
                //}
                //update by lid----end
            }
        }

        private void btn读卡_Click(object sender, EventArgs e)
        {

        }

        private void frmMain_Load(object sender, EventArgs e)
        {

        }
    }
}