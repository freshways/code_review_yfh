﻿using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ATOMEHR_LeaveClient
{
    public partial class frm查询结果 : Form
    {
        public frm查询结果()
        {
            InitializeComponent();
        }

        public frm查询结果(string type, object _list,string thisName)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            if (!string.IsNullOrEmpty(thisName)) this.Text = thisName;
            switch (type)
            {
                case "1":
                    List<tb_健康体检Info> list1 = _list as List<tb_健康体检Info>;
                    for (int i = 0; i < list1.Count; i++)
                    {
                        list1[i].RowID = i + 1;
                        list1[i].姓名 = DESEncrypt.DES解密(list1[i].姓名);
                        list1[i].性别 = ControlsHelper.ReturnDis字典显示("xb_xingbie", list1[i].性别);
                    }
                    this.gridControl1.DataSource = list1;
                    this.gridView1.BestFitColumns();
                    break;
                case "2":
                    List<tb_老年人生活自理能力评价Info> list2 = _list as List<tb_老年人生活自理能力评价Info>;
                    for (int i = 0; i < list2.Count; i++)
                    {
                        list2[i].RowID = i + 1;
                        list2[i].姓名 = DESEncrypt.DES解密(list2[i].姓名);
                        list2[i].性别 = ControlsHelper.ReturnDis字典显示("xb_xingbie", list2[i].性别);
                    }
                    this.gridControl1.DataSource = list2;
                    this.gridView1.BestFitColumns();
                    break;
                case "3":
                    List<tb_老年人中医药特征管理Info> list3 = _list as List<tb_老年人中医药特征管理Info>;
                    for (int i = 0; i < list3.Count; i++)
                    {
                        list3[i].RowID = i + 1;
                        list3[i].姓名 = DESEncrypt.DES解密(list3[i].姓名);
                        list3[i].性别 = ControlsHelper.ReturnDis字典显示("xb_xingbie", list3[i].性别);
                    }
                    this.gridControl1.DataSource = list3;
                    this.gridView1.BestFitColumns();
                    break;
                default:
                    break;
            }

        }

        private void btnVisible_Click(object sender, EventArgs e)
        {
            tb_健康体检DAL.UpdateSys_RowState_Hiden();
            this.gridControl1.DataSource = null;
        }
    }
}
