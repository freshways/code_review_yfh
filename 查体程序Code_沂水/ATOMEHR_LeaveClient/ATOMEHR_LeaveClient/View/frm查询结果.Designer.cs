﻿namespace ATOMEHR_LeaveClient
{
    partial class frm查询结果
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col序号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col身份证号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col居住地址 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col出生日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col个人档案编号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col性别 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col查体时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Controls.Add(this.panelControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(637, 440);
            this.panelControl2.TabIndex = 1;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.gridControl1);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 12);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(633, 426);
            this.panelControl3.TabIndex = 2;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(629, 422);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 40;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col序号,
            this.col姓名,
            this.col身份证号,
            this.col居住地址,
            this.col出生日期,
            this.col个人档案编号,
            this.col性别,
            this.col查体时间});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // col序号
            // 
            this.col序号.Caption = "序号";
            this.col序号.FieldName = "RowID";
            this.col序号.Name = "col序号";
            this.col序号.Visible = true;
            this.col序号.VisibleIndex = 0;
            this.col序号.Width = 63;
            // 
            // col姓名
            // 
            this.col姓名.AppearanceHeader.Options.UseTextOptions = true;
            this.col姓名.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col姓名.Caption = "姓名";
            this.col姓名.FieldName = "姓名";
            this.col姓名.Name = "col姓名";
            this.col姓名.OptionsColumn.AllowEdit = false;
            this.col姓名.Visible = true;
            this.col姓名.VisibleIndex = 1;
            this.col姓名.Width = 142;
            // 
            // col身份证号
            // 
            this.col身份证号.AppearanceHeader.Options.UseTextOptions = true;
            this.col身份证号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col身份证号.Caption = "身份证号";
            this.col身份证号.FieldName = "身份证号";
            this.col身份证号.Name = "col身份证号";
            this.col身份证号.OptionsColumn.AllowEdit = false;
            this.col身份证号.Visible = true;
            this.col身份证号.VisibleIndex = 2;
            this.col身份证号.Width = 209;
            // 
            // col居住地址
            // 
            this.col居住地址.AppearanceHeader.Options.UseTextOptions = true;
            this.col居住地址.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col居住地址.Caption = "居住地址";
            this.col居住地址.FieldName = "居住地址";
            this.col居住地址.Name = "col居住地址";
            this.col居住地址.OptionsColumn.AllowEdit = false;
            // 
            // col出生日期
            // 
            this.col出生日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col出生日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col出生日期.Caption = "出生日期";
            this.col出生日期.FieldName = "出生日期";
            this.col出生日期.Name = "col出生日期";
            this.col出生日期.OptionsColumn.AllowEdit = false;
            // 
            // col个人档案编号
            // 
            this.col个人档案编号.AppearanceHeader.Options.UseTextOptions = true;
            this.col个人档案编号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col个人档案编号.Caption = "个人档案编号";
            this.col个人档案编号.FieldName = "个人档案编号";
            this.col个人档案编号.Name = "col个人档案编号";
            this.col个人档案编号.OptionsColumn.AllowEdit = false;
            this.col个人档案编号.Visible = true;
            this.col个人档案编号.VisibleIndex = 3;
            this.col个人档案编号.Width = 209;
            // 
            // col性别
            // 
            this.col性别.AppearanceHeader.Options.UseTextOptions = true;
            this.col性别.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col性别.Caption = "性别";
            this.col性别.FieldName = "性别";
            this.col性别.Name = "col性别";
            this.col性别.OptionsColumn.AllowEdit = false;
            this.col性别.Visible = true;
            this.col性别.VisibleIndex = 4;
            this.col性别.Width = 209;
            // 
            // col查体时间
            // 
            this.col查体时间.AppearanceHeader.Options.UseTextOptions = true;
            this.col查体时间.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col查体时间.Caption = "查体时间";
            this.col查体时间.FieldName = "创建时间";
            this.col查体时间.Name = "col查体时间";
            this.col查体时间.Visible = true;
            this.col查体时间.VisibleIndex = 5;
            this.col查体时间.Width = 212;
            // 
            // panelControl1
            // 
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(2, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(633, 10);
            this.panelControl1.TabIndex = 1;
            // 
            // frm查询结果
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(637, 440);
            this.Controls.Add(this.panelControl2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm查询结果";
            this.ShowIcon = false;
            this.Text = "查询";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn col姓名;
        private DevExpress.XtraGrid.Columns.GridColumn col身份证号;
        private DevExpress.XtraGrid.Columns.GridColumn col居住地址;
        private DevExpress.XtraGrid.Columns.GridColumn col出生日期;
        private DevExpress.XtraGrid.Columns.GridColumn col性别;
        private DevExpress.XtraGrid.Columns.GridColumn col个人档案编号;
        private DevExpress.XtraGrid.Columns.GridColumn col序号;
        private DevExpress.XtraGrid.Columns.GridColumn col查体时间;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl1;
    }
}