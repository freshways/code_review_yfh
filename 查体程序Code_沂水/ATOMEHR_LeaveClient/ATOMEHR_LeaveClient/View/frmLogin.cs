﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.NetworkInformation;

namespace ATOMEHR_LeaveClient
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 输入身份证号登录
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonEx3_Click(object sender, EventArgs e)
        {
            frmEnterIDNO idno = new frmEnterIDNO();
            if (DialogResult.OK == idno.ShowDialog())
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
        /// <summary>
        /// 连接设备
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonEx1_Click(object sender, EventArgs e)
        {
            byte[] bltname = new byte[128];
            bltname = System.Text.Encoding.Default.GetBytes(Program.IDCardName);
            bool bRlt = bluetooth.Connect100WT(bltname, 1);
            if (bRlt == false)
            {
                this.lbl提示.Text = "设备连接失败，请重试！";
                this.lbl提示.BackColor = System.Drawing.Color.Red;
            }
            else
            {
                this.lbl提示.Text = "设备连接成功，请读取身份证！";
                this.lbl提示.BackColor = System.Drawing.Color.SeaGreen;
            }
        }
        /// <summary>
        /// 读取身份证信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonEx2_Click(object sender, EventArgs e)
        {
            int ReadCS = 0;
            int ReadOK = 0;
            int cLeng = 0;
            int pLeng = 0;
            int fLeng = 0;
            byte[] contentBuff = new byte[256];
            byte[] picBuf = new byte[1024];
            byte[] fBuf = new byte[1024];
            byte[] fileRoot = new byte[256];
            bluetooth.FindCard();
            bluetooth.GetCard();
            int bResult;
            ReadCS = ReadCS + 1;
            bResult = bluetooth.ReadContentS(ref cLeng, contentBuff, ref pLeng, picBuf, ref  fLeng, fBuf, fileRoot);
            if (bResult != 0)
            {
                MessageBox.Show("读取身份证失败，请重试！");
                //return;
            }
            else
            {
                CardInfo objCardInfo = null;
                //发布后的运行路劲，发布的是Release版本的
                string sPath; //
                sPath = System.Environment.CurrentDirectory + "\\";
                //未发布的程序运行路劲
#if DEBUG
                sPath = System.Windows.Forms.Application.StartupPath + "\\";
#endif
                fileRoot = System.Text.Encoding.Default.GetBytes(sPath);


                objCardInfo = new CardInfo();
                System.IO.StreamReader objStreamReader = new System.IO.StreamReader(sPath + "wx.txt", System.Text.Encoding.Default);
                //读取已转化文本

                objCardInfo.Name = objStreamReader.ReadLine();
                objCardInfo.Sex = objStreamReader.ReadLine();
                objCardInfo.Nation = objStreamReader.ReadLine();
                objCardInfo.Birthday = objStreamReader.ReadLine();
                objCardInfo.Address = objStreamReader.ReadLine();
                objCardInfo.CardNo = objStreamReader.ReadLine();
                objCardInfo.Department = objStreamReader.ReadLine();
                objCardInfo.StartDate = objStreamReader.ReadLine();
                objCardInfo.EndDate = objStreamReader.ReadLine();
                objCardInfo.PhotoPath = sPath + "zp.bmp";
                objStreamReader.Close();

                Program.currentUser.Name = objCardInfo.Name;
                Program.currentUser.Gender = objCardInfo.Sex;
                //_user.Nation = objStreamReader.ReadLine();
                Program.currentUser.Birth = objCardInfo.Birthday;
                Program.currentUser.Addr = objCardInfo.Address;
                Program.currentUser.ID = objCardInfo.CardNo;
                //根据身份证号查询出当前用户的档案信息
                List<tb_导出用户Info> list = tb_导出用户DAL.GetInfoList(" upper(身份证号) =upper( '" + Program.currentUser.ID + "')");
                if (list.Count == 1)
                {
                    Program.currentUser.DocNo = list[0].个人档案编号;
                    Program.currentUser.Prgid = list[0].所属机构;
                }
                else if (list.Count == 0)
                {
                    Program.currentUser.DocNo = ""; //1代表这个人不存在档案
                    MessageBox.Show("此人不存在个人基本档案信息，请核实！");
                }
                else if (list.Count > 1)
                {
                    frm查询姓名 frm = new frm查询姓名(Program.currentUser.Name, list);
                    if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {

                    }
                }
                //_user.Department = objStreamReader.ReadLine();
                //_user.StartDate = objStreamReader.ReadLine();
                //_user.EndDate = objStreamReader.ReadLine();
                Program.currentUser.PhotoPath = sPath + "zp.bmp";

                string sPhotoPath = objCardInfo.PhotoPath;
                if (null != objCardInfo)
                {
                    //this.DialogResult = System.Windows.Forms.DialogResult.OK;
                    this.Hide();
                    frmMain frm = new frmMain();
                    if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        this.Show();
                    }
                }

                ReadOK = ReadOK + 1;

            }

        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            this.Text += "-" + Application.ProductVersion;
            if (!File.Exists(Path.Combine(Program.toAppPath, Program.iniName)))
            {
                MessageBox.Show("此电脑还未与相关的蓝牙设备进行配对，请先用管理员登录，进行蓝牙配对");
            }
            else
            {
                //Program.BlueName = ControlsHelper.IniReadValue("BlueTooth", "Name", Path.Combine(Program.toAppPath, Program.iniName));
                //Program.BlueAddr = ControlsHelper.IniReadValue("BlueTooth", "Addr", Path.Combine(Program.toAppPath, Program.iniName));
                //Program.BlueType = ControlsHelper.IniReadValue("BlueTooth", "Type", Path.Combine(Program.toAppPath, Program.iniName));
                //Program.IDCardName = ControlsHelper.IniReadValue("BlueTooth", "IDCardName", Path.Combine(Program.toAppPath, Program.iniName));
                //Program.IDCardAddr = ControlsHelper.IniReadValue("BlueTooth", "IDCardAddr", Path.Combine(Program.toAppPath, Program.iniName));
                ////2016年6月29日14:13:27 yufh 添加
                //Program._bRGID = ControlsHelper.IniReadValue("BlueTooth", "RGID", Path.Combine(Program.toAppPath, Program.iniSys));
                //Program._bCreateuser = ControlsHelper.IniReadValue("BlueTooth", "Createuser", Path.Combine(Program.toAppPath, Program.iniSys));
                Program.BindingIni();
                this.buttonEx1_Click(null, null);
            }
        }

        private void btn管理员登录_Click(object sender, EventArgs e)
        {
            frm后台操作 frm = new frm后台操作();
            frm.ShowDialog();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!NetPing()) return;
            if (System.IO.File.Exists("uplistdo.exe") == false)
            {
                MessageBox.Show("升级工具丢失，请联系软件维护人员处理故障!");
                return;
            }
            System.Diagnostics.Process.Start("uplistdo.exe");
        }

        private bool  NetPing()
        {
            bool state = false;
            #region 取网络;网络连通，继续运行；不通退出。
            if (SystemInformation.Network)
            {
                Ping ping = new Ping();
                try
                {
                    PingReply pr = ping.Send(Program._bSerVer);

                    if (pr.Status == IPStatus.Success)
                    {
                        state= true;
                    }
                    else
                    {
                        state= false;
                    }
                }
                catch (Exception ex)
                {
                    state= false;
                }
            }

            #endregion
            return state;
        }

    }
}
