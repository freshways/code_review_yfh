﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net.NetworkInformation;

namespace ATOMEHR_LeaveClient
{
    public partial class frmEnterIDNO : Form
    {
        public frmEnterIDNO()
        {
            InitializeComponent();
        }

        private void frmEnterIDNO_Load(object sender, EventArgs e)
        {
            this.Text += "-" + Application.ProductVersion;
            if (!File.Exists(Path.Combine(Program.toAppPath, Program.iniName)))
            {
                MessageBox.Show("此电脑还未与相关的蓝牙设备进行配对，请先用管理员登录，进行蓝牙配对");
            }
            else
            {
                //Program.BlueName = ControlsHelper.IniReadValue("BlueTooth", "Name", Path.Combine(Program.toAppPath, Program.iniName));
                //Program.BlueAddr = ControlsHelper.IniReadValue("BlueTooth", "Addr", Path.Combine(Program.toAppPath, Program.iniName));
                //Program.BlueType = ControlsHelper.IniReadValue("BlueTooth", "Type", Path.Combine(Program.toAppPath, Program.iniName));
                //Program.IDCardName = ControlsHelper.IniReadValue("BlueTooth", "IDCardName", Path.Combine(Program.toAppPath, Program.iniName));
                //Program.IDCardAddr = ControlsHelper.IniReadValue("BlueTooth", "IDCardAddr", Path.Combine(Program.toAppPath, Program.iniName));
                ////2016年6月29日14:13:27 yufh 添加
                //Program._bRGID = ControlsHelper.IniReadValue("BlueTooth", "RGID", Path.Combine(Program.toAppPath, Program.iniName));
                //Program._bCreateuser = ControlsHelper.IniReadValue("BlueTooth", "UserID", Path.Combine(Program.toAppPath, Program.iniName));
                //Program._bSerVer = ControlsHelper.IniReadValue("BlueTooth", "SerVer", Path.Combine(Program.toAppPath, Program.iniName));
                //Program._bDoctor = ControlsHelper.IniReadValue("BlueTooth", "Doctor", Path.Combine(Program.toAppPath, Program.iniName));
                Program.BindingIni();
            }
            this.txt档案编号.Focus();
        }

        private void lookUpEdit1_Leave(object sender, EventArgs e)
        {

        }

        private void btn查询_Click(object sender, EventArgs e)
        {
            List<tb_导出用户Info> list;
            if (string.IsNullOrEmpty(this.txt姓名.Text.Trim()))
            {
                MessageBox.Show("请输入要查找的姓名", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            string sql = "  (姓名 like '" + DESEncrypt.DES加密(this.txt姓名.Text.Trim()) + "%' or 姓名 like'" + this.txt姓名.Text.Trim() + "%')";
            list = tb_导出用户DAL.GetInfoList(sql);
            if (list.Count == 1)
            {
                this.txt身份证号.Text = list[0].身份证号;
                this.txt档案编号.Text = list[0].个人档案编号;
                Program.currentUser.Name = DESEncrypt.DES解密(list[0].姓名);
                Program.currentUser.ID = list[0].身份证号;
                Program.currentUser.Gender = ControlsHelper.ReturnDis字典显示("xb_xingbie", list[0].性别);
                Program.currentUser.Birth = list[0].出生日期;
                Program.currentUser.Addr = list[0].居住地址;
                Program.currentUser.DocNo = list[0].个人档案编号;
                Program.currentUser.Prgid = list[0].所属机构;
            }
            else if (list.Count == 0)
            {
                MessageBox.Show("未检索到相关的用户信息！");
                return;
            }
            else
            {
                frm查询姓名 frm = new frm查询姓名(this.txt姓名.Text.Trim(), list);
                frm.WindowState = FormWindowState.Maximized;
                if (DialogResult.OK == frm.ShowDialog())
                {
                    this.txt身份证号.Text = Program.currentUser.ID;
                    this.txt姓名.Text = Program.currentUser.Name;
                    this.txt档案编号.Text = Program.currentUser.DocNo;
                }
            }
        }
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonEx2_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txt姓名.Text.Trim()) || this.txt姓名.EditValue == null)
            {
                MessageBox.Show("姓名不能为空！");
                return;
            }
            if (string.IsNullOrEmpty(this.txt身份证号.Text.Trim()) && string.IsNullOrEmpty(this.txt档案编号.Text.Trim()))
            {
                MessageBox.Show("{身份证号/档案编号}至少一个不能为空！");
                return;
            }
            if (string.IsNullOrEmpty(Program.currentUser.Name))
            {
                Program.currentUser.Name = this.txt姓名.Text.Trim();
            }
            if (string.IsNullOrEmpty(Program.currentUser.ID))
            {
                Program.currentUser.ID = this.txt身份证号.Text.Trim();
            }
            if (string.IsNullOrEmpty(Program.currentUser.DocNo) && string.IsNullOrEmpty(txt档案编号.Text.Trim()))
            {
                Program.currentUser.DocNo = this.txt档案编号.Text.Trim();
            }

            //this.Close();
            //this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Hide();
            frmMain frm = new frmMain();
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.Show();
                this.txt姓名.Text = "";
                this.txt身份证号.Text = "";
                this.txt档案编号.Text = "";
            }
        }

        /// <summary>
        /// 登录---add by lid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dlSender()
        {
            if (string.IsNullOrEmpty(Program.currentUser.DocNo) && string.IsNullOrEmpty(txt档案编号.Text.Trim()))
            {
                Program.currentUser.DocNo = this.txt档案编号.Text.Trim();
            }

            //this.Close();
            //this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Hide();
            frmMain frm = new frmMain();
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.Show();
                this.txt姓名.Text = "";
                this.txt身份证号.Text = "";
                this.txt档案编号.Text = "";
            }
        }

        private void txt姓名_Click(object sender, EventArgs e)
        {
            //int i = TabTip.ShowInputPanel();
            //MessageBox.Show(i.ToString());
        }

        private void txt档案编号_KeyDown(object sender, KeyEventArgs e)
        {
            if (string.IsNullOrEmpty(this.txt档案编号.Text)) return;
            //add by lid---start
            string newDabh = "";//新的档案编号
            string bh = txt档案编号.Text.Trim();

            if ("1".Equals(bh.Substring(0, 1)))
            {
                string jigouNo = ControlsHelper.IniReadValue("BlueTooth", "ParentRGID", Path.Combine(Program.toAppPath, Program.iniName));
                //蒙阴档案号17位
                if (jigouNo.Equals("371328"))
                {
                    newDabh = "37" + bh.Substring(1, bh.Length - 1);
                }
                //沂水档案号18位
                else if (jigouNo.Equals("371323"))
                {
                    newDabh = "371" + bh.Substring(1, bh.Length - 1);
                }
                //newDabh = "37" + bh.Substring(1, bh.Length - 1);
            }

            //Begin WXF 2019-04-08 | 22:29
            //这给if如果不加 else的话,如果查不到条码(比如没档案的0开头的条码)(newDabh会为空),那么到了下面s个人档案编号.Substring(1)这地方就会报错
            else
            {
                newDabh = bh;
            }
            //End
											 

            //add by lid---end
            List<tb_导出用户Info> list;
            if (e.KeyCode == Keys.Enter)
            {
                //string s个人档案编号 = txt档案编号.Text.Trim();
                string s个人档案编号 = newDabh;// add by lid

                //if (txt档案编号.Text.Trim().Length < 17)//沂水打印条码是截取了之后，所以判断一下给拼接下
                //    s个人档案编号 = "371323" + s个人档案编号;

                string sql = "  (个人档案编号 = '" + s个人档案编号 + "' or 个人档案编号 = '" + s个人档案编号.Substring(1) + "' ) ";
                list = tb_导出用户DAL.GetInfoList(sql);
                if (list.Count == 1)
                {
                    this.txt姓名.Text = DESEncrypt.DES解密(list[0].姓名);
                    this.txt身份证号.Text = list[0].身份证号;
                    this.txt档案编号.Text = list[0].个人档案编号;
                    Program.currentUser.Name = DESEncrypt.DES解密(list[0].姓名);
                    Program.currentUser.ID = list[0].身份证号;
                    Program.currentUser.Gender = ControlsHelper.ReturnDis字典显示("xb_xingbie", list[0].性别);
                    Program.currentUser.Birth = list[0].出生日期;
                    Program.currentUser.Addr = list[0].居住地址;
                    Program.currentUser.DocNo = list[0].个人档案编号;
                    Program.currentUser.Prgid = list[0].所属机构;
                    buttonEx2.PerformClick();
                }
                else
                {// add by lid---start
                    Program.currentUser.DocNo = newDabh;
                    dlSender();
                }// add by lid---end

                // update by  lid-----start---没建档存档案号
                //else if (list.Count == 0)
                //{
                //    MessageBox.Show("未检索到相关的用户信息！");
                //    this.txt档案编号.Text = "";
                //    return;
                //}
                //update by lid----------end

                //SendKeys.Send("Tab");
            }
        }

        private void btn管理员登录_Click(object sender, EventArgs e)
        {
            frm后台操作 frm = new frm后台操作();
            frm.ShowDialog();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!NetPing()) return;
            if (System.IO.File.Exists("uplistdo.exe") == false)
            {
                MessageBox.Show("升级工具丢失，请联系软件维护人员处理故障!");
                return;
            }
            System.Diagnostics.Process.Start("uplistdo.exe");
        }

        private bool NetPing()
        {
            bool state = false;
            #region 取网络;网络连通，继续运行；不通退出。
            if (SystemInformation.Network)
            {
                Ping ping = new Ping();
                try
                {
                    PingReply pr = ping.Send(Program._bSerVer);

                    if (pr.Status == IPStatus.Success)
                    {
                        state = true;
                    }
                    else
                    {
                        state = false;
                    }
                }
                catch (Exception ex)
                {
                    state = false;
                }
            }

            #endregion
            return state;
        }

        private void btn打印_Click(object sender, EventArgs e)
        {
            frm查询姓名 frm = new frm查询姓名("");
            frm.WindowState = FormWindowState.Maximized;
            frm.ShowDialog();
        }

    }
}