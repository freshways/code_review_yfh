﻿namespace ATOMEHR_LeaveClient
{
    partial class frm蓝牙配置
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm蓝牙配置));
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.tab蓝牙 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col蓝牙名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMacID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn配对 = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.tab系统 = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtPwd = new DevExpress.XtraEditors.TextEdit();
            this.txtUid = new DevExpress.XtraEditors.TextEdit();
            this.txtDatabase = new DevExpress.XtraEditors.TextEdit();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.btnImportRegion = new DevExpress.XtraEditors.SimpleButton();
            this.txtRGID = new DevExpress.XtraEditors.TextEdit();
            this.txtDefaultLogin = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btn读取配置 = new DevExpress.XtraEditors.SimpleButton();
            this.txt服务器地址 = new DevExpress.XtraEditors.TextEdit();
            this.btn系统配置保存 = new DevExpress.XtraEditors.SimpleButton();
            this.txt随访医生 = new DevExpress.XtraEditors.TextEdit();
            this.txt用户 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt机构 = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tab主机 = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.btnSaveConfigure = new DevExpress.XtraEditors.SimpleButton();
            this.txt主机数据库密码 = new DevExpress.XtraEditors.TextEdit();
            this.btnReadConfigure = new DevExpress.XtraEditors.SimpleButton();
            this.txt主机数据库用户名 = new DevExpress.XtraEditors.TextEdit();
            this.txt主机数据库名称 = new DevExpress.XtraEditors.TextEdit();
            this.txt主机地址 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.btn用药情况配置 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.tab蓝牙.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).BeginInit();
            this.tab系统.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPwd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUid.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatabase.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRGID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDefaultLogin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服务器地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt用户.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            this.tab主机.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt主机数据库密码.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt主机数据库用户名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt主机数据库名称.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt主机地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.tab蓝牙;
            this.xtraTabControl1.Size = new System.Drawing.Size(373, 591);
            this.xtraTabControl1.TabIndex = 5;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tab蓝牙,
            this.tab系统,
            this.tab主机,
            this.xtraTabPage1});
            // 
            // tab蓝牙
            // 
            this.tab蓝牙.Controls.Add(this.gridControl1);
            this.tab蓝牙.Controls.Add(this.btn配对);
            this.tab蓝牙.Controls.Add(this.comboBoxEdit1);
            this.tab蓝牙.Font = new System.Drawing.Font("宋体", 9F);
            this.tab蓝牙.Image = ((System.Drawing.Image)(resources.GetObject("tab蓝牙.Image")));
            this.tab蓝牙.Name = "tab蓝牙";
            this.tab蓝牙.Size = new System.Drawing.Size(367, 544);
            this.tab蓝牙.Text = "蓝牙配置";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 36);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(367, 443);
            this.gridControl1.TabIndex = 5;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col蓝牙名称,
            this.colMacID});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.IndicatorWidth = 20;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowHeight = 25;
            // 
            // col蓝牙名称
            // 
            this.col蓝牙名称.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.col蓝牙名称.AppearanceHeader.Options.UseFont = true;
            this.col蓝牙名称.AppearanceHeader.Options.UseTextOptions = true;
            this.col蓝牙名称.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col蓝牙名称.Caption = "蓝牙名称";
            this.col蓝牙名称.FieldName = "Name";
            this.col蓝牙名称.Name = "col蓝牙名称";
            this.col蓝牙名称.OptionsColumn.AllowEdit = false;
            this.col蓝牙名称.Visible = true;
            this.col蓝牙名称.VisibleIndex = 1;
            // 
            // colMacID
            // 
            this.colMacID.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.colMacID.AppearanceHeader.Options.UseFont = true;
            this.colMacID.AppearanceHeader.Options.UseTextOptions = true;
            this.colMacID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMacID.Caption = "MAC地址";
            this.colMacID.FieldName = "Addr";
            this.colMacID.Name = "colMacID";
            this.colMacID.OptionsColumn.AllowEdit = false;
            this.colMacID.Visible = true;
            this.colMacID.VisibleIndex = 2;
            // 
            // btn配对
            // 
            this.btn配对.Appearance.Font = new System.Drawing.Font("Tahoma", 20F, System.Drawing.FontStyle.Bold);
            this.btn配对.Appearance.Options.UseFont = true;
            this.btn配对.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn配对.Image = ((System.Drawing.Image)(resources.GetObject("btn配对.Image")));
            this.btn配对.Location = new System.Drawing.Point(0, 479);
            this.btn配对.Name = "btn配对";
            this.btn配对.Size = new System.Drawing.Size(367, 65);
            this.btn配对.TabIndex = 6;
            this.btn配对.Text = "配  对";
            this.btn配对.Click += new System.EventHandler(this.btn配对_Click);
            // 
            // comboBoxEdit1
            // 
            this.comboBoxEdit1.Dock = System.Windows.Forms.DockStyle.Top;
            this.comboBoxEdit1.EditValue = "请选择";
            this.comboBoxEdit1.Location = new System.Drawing.Point(0, 0);
            this.comboBoxEdit1.Name = "comboBoxEdit1";
            this.comboBoxEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.comboBoxEdit1.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEdit1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 19F);
            this.comboBoxEdit1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.comboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit1.Properties.Items.AddRange(new object[] {
            "请选择",
            "身份证读卡器",
            "体温",
            "体温(有线)",
            "血压",
            "血压(有线)",
            "血压RBP9804",
            "血压RBP9808(有线)",
            "身高体重",
            "身高体重(有线)",
            "脉搏",
            "心电",
            "尿常规",
            "血糖"});
            this.comboBoxEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit1.Size = new System.Drawing.Size(367, 36);
            this.comboBoxEdit1.TabIndex = 2;
            // 
            // tab系统
            // 
            this.tab系统.Controls.Add(this.layoutControl1);
            this.tab系统.Font = new System.Drawing.Font("宋体", 9F);
            this.tab系统.Image = ((System.Drawing.Image)(resources.GetObject("tab系统.Image")));
            this.tab系统.Name = "tab系统";
            this.tab系统.Size = new System.Drawing.Size(367, 544);
            this.tab系统.Text = "系统配置";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtPwd);
            this.layoutControl1.Controls.Add(this.txtUid);
            this.layoutControl1.Controls.Add(this.txtDatabase);
            this.layoutControl1.Controls.Add(this.richTextBox1);
            this.layoutControl1.Controls.Add(this.btnImportRegion);
            this.layoutControl1.Controls.Add(this.txtRGID);
            this.layoutControl1.Controls.Add(this.txtDefaultLogin);
            this.layoutControl1.Controls.Add(this.btn读取配置);
            this.layoutControl1.Controls.Add(this.txt服务器地址);
            this.layoutControl1.Controls.Add(this.btn系统配置保存);
            this.layoutControl1.Controls.Add(this.txt随访医生);
            this.layoutControl1.Controls.Add(this.txt用户);
            this.layoutControl1.Controls.Add(this.txt机构);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(890, 121, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(367, 544);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtPwd
            // 
            this.txtPwd.Location = new System.Drawing.Point(117, 332);
            this.txtPwd.Name = "txtPwd";
            this.txtPwd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txtPwd.Properties.Appearance.Options.UseFont = true;
            this.txtPwd.Properties.UseSystemPasswordChar = true;
            this.txtPwd.Size = new System.Drawing.Size(238, 36);
            this.txtPwd.StyleController = this.layoutControl1;
            this.txtPwd.TabIndex = 20;
            // 
            // txtUid
            // 
            this.txtUid.Location = new System.Drawing.Point(117, 292);
            this.txtUid.Name = "txtUid";
            this.txtUid.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txtUid.Properties.Appearance.Options.UseFont = true;
            this.txtUid.Properties.UseSystemPasswordChar = true;
            this.txtUid.Size = new System.Drawing.Size(238, 36);
            this.txtUid.StyleController = this.layoutControl1;
            this.txtUid.TabIndex = 19;
            // 
            // txtDatabase
            // 
            this.txtDatabase.Location = new System.Drawing.Point(117, 252);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txtDatabase.Properties.Appearance.Options.UseFont = true;
            this.txtDatabase.Size = new System.Drawing.Size(238, 36);
            this.txtDatabase.StyleController = this.layoutControl1;
            this.txtDatabase.TabIndex = 18;
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Enabled = false;
            this.richTextBox1.ForeColor = System.Drawing.Color.Green;
            this.richTextBox1.Location = new System.Drawing.Point(43, 471);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(312, 48);
            this.richTextBox1.TabIndex = 16;
            this.richTextBox1.Text = "1.配置单位代码2.下载机构3.保存4.配置查体机构\n5.配置创建人";
            // 
            // btnImportRegion
            // 
            this.btnImportRegion.Image = ((System.Drawing.Image)(resources.GetObject("btnImportRegion.Image")));
            this.btnImportRegion.Location = new System.Drawing.Point(12, 429);
            this.btnImportRegion.Name = "btnImportRegion";
            this.btnImportRegion.Size = new System.Drawing.Size(343, 38);
            this.btnImportRegion.StyleController = this.layoutControl1;
            this.btnImportRegion.TabIndex = 13;
            this.btnImportRegion.Text = "下载机构信息";
            this.btnImportRegion.Click += new System.EventHandler(this.btnImportRegion_Click);
            // 
            // txtRGID
            // 
            this.txtRGID.Location = new System.Drawing.Point(117, 12);
            this.txtRGID.Name = "txtRGID";
            this.txtRGID.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txtRGID.Properties.Appearance.Options.UseFont = true;
            this.txtRGID.Properties.NullText = "请填写县区代码";
            this.txtRGID.Size = new System.Drawing.Size(238, 36);
            this.txtRGID.StyleController = this.layoutControl1;
            this.txtRGID.TabIndex = 12;
            this.txtRGID.ToolTip = "根据单位代码下载机构信息";
            // 
            // txtDefaultLogin
            // 
            this.txtDefaultLogin.EditValue = "";
            this.txtDefaultLogin.Location = new System.Drawing.Point(117, 212);
            this.txtDefaultLogin.Name = "txtDefaultLogin";
            this.txtDefaultLogin.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txtDefaultLogin.Properties.Appearance.Options.UseFont = true;
            this.txtDefaultLogin.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtDefaultLogin.Properties.AppearanceDropDown.Options.UseFont = true;
            this.txtDefaultLogin.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtDefaultLogin.Properties.Items.AddRange(new object[] {
            "刷卡",
            "扫码"});
            this.txtDefaultLogin.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txtDefaultLogin.Size = new System.Drawing.Size(238, 36);
            this.txtDefaultLogin.StyleController = this.layoutControl1;
            this.txtDefaultLogin.TabIndex = 11;
            // 
            // btn读取配置
            // 
            this.btn读取配置.Appearance.Font = new System.Drawing.Font("Tahoma", 19F, System.Drawing.FontStyle.Bold);
            this.btn读取配置.Appearance.Options.UseFont = true;
            this.btn读取配置.Image = ((System.Drawing.Image)(resources.GetObject("btn读取配置.Image")));
            this.btn读取配置.Location = new System.Drawing.Point(12, 385);
            this.btn读取配置.Margin = new System.Windows.Forms.Padding(2);
            this.btn读取配置.Name = "btn读取配置";
            this.btn读取配置.Size = new System.Drawing.Size(163, 38);
            this.btn读取配置.StyleController = this.layoutControl1;
            this.btn读取配置.TabIndex = 9;
            this.btn读取配置.Text = "读取配置";
            this.btn读取配置.Click += new System.EventHandler(this.btn读取配置_Click);
            // 
            // txt服务器地址
            // 
            this.txt服务器地址.EditValue = "127.0.0.1";
            this.txt服务器地址.Location = new System.Drawing.Point(117, 52);
            this.txt服务器地址.Margin = new System.Windows.Forms.Padding(2);
            this.txt服务器地址.Name = "txt服务器地址";
            this.txt服务器地址.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt服务器地址.Properties.Appearance.Options.UseFont = true;
            this.txt服务器地址.Size = new System.Drawing.Size(238, 36);
            this.txt服务器地址.StyleController = this.layoutControl1;
            this.txt服务器地址.TabIndex = 8;
            this.txt服务器地址.ToolTip = "用于检测升级、上传等";
            // 
            // btn系统配置保存
            // 
            this.btn系统配置保存.Appearance.Font = new System.Drawing.Font("Tahoma", 20F, System.Drawing.FontStyle.Bold);
            this.btn系统配置保存.Appearance.Options.UseFont = true;
            this.btn系统配置保存.Image = ((System.Drawing.Image)(resources.GetObject("btn系统配置保存.Image")));
            this.btn系统配置保存.Location = new System.Drawing.Point(179, 385);
            this.btn系统配置保存.Margin = new System.Windows.Forms.Padding(2);
            this.btn系统配置保存.Name = "btn系统配置保存";
            this.btn系统配置保存.Size = new System.Drawing.Size(176, 40);
            this.btn系统配置保存.StyleController = this.layoutControl1;
            this.btn系统配置保存.TabIndex = 7;
            this.btn系统配置保存.Text = "保 存";
            this.btn系统配置保存.Click += new System.EventHandler(this.btn系统配置保存_Click);
            // 
            // txt随访医生
            // 
            this.txt随访医生.Location = new System.Drawing.Point(117, 172);
            this.txt随访医生.Margin = new System.Windows.Forms.Padding(2);
            this.txt随访医生.Name = "txt随访医生";
            this.txt随访医生.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt随访医生.Properties.Appearance.Options.UseFont = true;
            this.txt随访医生.Properties.NullText = "请填写随访医生";
            this.txt随访医生.Size = new System.Drawing.Size(238, 36);
            this.txt随访医生.StyleController = this.layoutControl1;
            this.txt随访医生.TabIndex = 6;
            // 
            // txt用户
            // 
            this.txt用户.Location = new System.Drawing.Point(117, 132);
            this.txt用户.Margin = new System.Windows.Forms.Padding(2);
            this.txt用户.Name = "txt用户";
            this.txt用户.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt用户.Properties.Appearance.Options.UseFont = true;
            this.txt用户.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt用户.Properties.AppearanceDropDown.Options.UseFont = true;
            this.txt用户.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt用户.Properties.NullText = "请选择操作用户...";
            this.txt用户.Size = new System.Drawing.Size(238, 36);
            this.txt用户.StyleController = this.layoutControl1;
            this.txt用户.TabIndex = 5;
            this.txt用户.ToolTip = "上传数据以此用户作为创建人";
            // 
            // txt机构
            // 
            this.txt机构.Location = new System.Drawing.Point(117, 92);
            this.txt机构.Margin = new System.Windows.Forms.Padding(2);
            this.txt机构.Name = "txt机构";
            this.txt机构.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt机构.Properties.Appearance.Options.UseFont = true;
            this.txt机构.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt机构.Properties.AppearanceDropDown.Options.UseFont = true;
            this.txt机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt机构.Properties.NullText = "请选择查体机构...";
            this.txt机构.Size = new System.Drawing.Size(238, 36);
            this.txt机构.StyleController = this.layoutControl1;
            this.txt机构.TabIndex = 4;
            this.txt机构.ToolTip = "查体机构";
            this.txt机构.EditValueChanged += new System.EventHandler(this.txt机构_EditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.emptySpaceItem1,
            this.layoutControlItem8,
            this.layoutControlItem7,
            this.emptySpaceItem2,
            this.layoutControlItem9,
            this.layoutControlItem12,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem24});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(367, 544);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.txt机构;
            this.layoutControlItem1.CustomizationFormText = "机构";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 80);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(347, 40);
            this.layoutControlItem1.Text = "机构";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.txt随访医生;
            this.layoutControlItem3.CustomizationFormText = "随访医生";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 160);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(347, 40);
            this.layoutControlItem3.Text = "随访医生";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.txt用户;
            this.layoutControlItem2.CustomizationFormText = "用户";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(347, 40);
            this.layoutControlItem2.Text = "创建人";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btn系统配置保存;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(167, 373);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(180, 44);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt服务器地址;
            this.layoutControlItem5.CustomizationFormText = "服务器地址";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 40);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(347, 40);
            this.layoutControlItem5.Text = "服务器地址";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btn读取配置;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 373);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(167, 44);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 511);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(347, 13);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.txtDefaultLogin;
            this.layoutControlItem8.CustomizationFormText = "默认登陆";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 200);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(347, 40);
            this.layoutControlItem8.Text = "默认登陆";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.txtRGID;
            this.layoutControlItem7.CustomizationFormText = "单位编码";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(347, 40);
            this.layoutControlItem7.Text = "单位编码*";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 360);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(347, 13);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.btnImportRegion;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 417);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(347, 42);
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem12.Control = this.richTextBox1;
            this.layoutControlItem12.CustomizationFormText = "说明:";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 459);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(347, 52);
            this.layoutControlItem12.Text = "说明:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(28, 14);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.layoutControlItem22.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.txtDatabase;
            this.layoutControlItem22.CustomizationFormText = "数据库名称";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 240);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(347, 40);
            this.layoutControlItem22.Text = "数据库名称";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.layoutControlItem23.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.Control = this.txtUid;
            this.layoutControlItem23.CustomizationFormText = "数据库用户名";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 280);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(347, 40);
            this.layoutControlItem23.Text = "数据库用户名";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.layoutControlItem24.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem24.Control = this.txtPwd;
            this.layoutControlItem24.CustomizationFormText = "数据库密码";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 320);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(347, 40);
            this.layoutControlItem24.Text = "数据库密码";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // tab主机
            // 
            this.tab主机.Controls.Add(this.layoutControl2);
            this.tab主机.Image = ((System.Drawing.Image)(resources.GetObject("tab主机.Image")));
            this.tab主机.Name = "tab主机";
            this.tab主机.Size = new System.Drawing.Size(367, 544);
            this.tab主机.Text = "主机配置";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.btnSaveConfigure);
            this.layoutControl2.Controls.Add(this.txt主机数据库密码);
            this.layoutControl2.Controls.Add(this.btnReadConfigure);
            this.layoutControl2.Controls.Add(this.txt主机数据库用户名);
            this.layoutControl2.Controls.Add(this.txt主机数据库名称);
            this.layoutControl2.Controls.Add(this.txt主机地址);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(367, 544);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // btnSaveConfigure
            // 
            this.btnSaveConfigure.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveConfigure.Image")));
            this.btnSaveConfigure.Location = new System.Drawing.Point(195, 186);
            this.btnSaveConfigure.Name = "btnSaveConfigure";
            this.btnSaveConfigure.Size = new System.Drawing.Size(160, 38);
            this.btnSaveConfigure.StyleController = this.layoutControl2;
            this.btnSaveConfigure.TabIndex = 10;
            this.btnSaveConfigure.Text = "保存";
            this.btnSaveConfigure.Click += new System.EventHandler(this.btnSaveConfigure_Click);
            // 
            // txt主机数据库密码
            // 
            this.txt主机数据库密码.Location = new System.Drawing.Point(143, 132);
            this.txt主机数据库密码.Name = "txt主机数据库密码";
            this.txt主机数据库密码.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt主机数据库密码.Properties.Appearance.Options.UseFont = true;
            this.txt主机数据库密码.Properties.UseSystemPasswordChar = true;
            this.txt主机数据库密码.Size = new System.Drawing.Size(212, 36);
            this.txt主机数据库密码.StyleController = this.layoutControl2;
            this.txt主机数据库密码.TabIndex = 8;
            // 
            // btnReadConfigure
            // 
            this.btnReadConfigure.Image = ((System.Drawing.Image)(resources.GetObject("btnReadConfigure.Image")));
            this.btnReadConfigure.Location = new System.Drawing.Point(12, 186);
            this.btnReadConfigure.Name = "btnReadConfigure";
            this.btnReadConfigure.Size = new System.Drawing.Size(166, 38);
            this.btnReadConfigure.StyleController = this.layoutControl2;
            this.btnReadConfigure.TabIndex = 10;
            this.btnReadConfigure.Text = "读取配置";
            this.btnReadConfigure.Click += new System.EventHandler(this.btnReadConfigure_Click);
            // 
            // txt主机数据库用户名
            // 
            this.txt主机数据库用户名.Location = new System.Drawing.Point(143, 92);
            this.txt主机数据库用户名.Name = "txt主机数据库用户名";
            this.txt主机数据库用户名.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt主机数据库用户名.Properties.Appearance.Options.UseFont = true;
            this.txt主机数据库用户名.Properties.UseSystemPasswordChar = true;
            this.txt主机数据库用户名.Size = new System.Drawing.Size(212, 36);
            this.txt主机数据库用户名.StyleController = this.layoutControl2;
            this.txt主机数据库用户名.TabIndex = 7;
            // 
            // txt主机数据库名称
            // 
            this.txt主机数据库名称.Location = new System.Drawing.Point(143, 52);
            this.txt主机数据库名称.Name = "txt主机数据库名称";
            this.txt主机数据库名称.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt主机数据库名称.Properties.Appearance.Options.UseFont = true;
            this.txt主机数据库名称.Size = new System.Drawing.Size(212, 36);
            this.txt主机数据库名称.StyleController = this.layoutControl2;
            this.txt主机数据库名称.TabIndex = 6;
            // 
            // txt主机地址
            // 
            this.txt主机地址.Location = new System.Drawing.Point(143, 12);
            this.txt主机地址.Name = "txt主机地址";
            this.txt主机地址.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt主机地址.Properties.Appearance.Options.UseFont = true;
            this.txt主机地址.Size = new System.Drawing.Size(212, 36);
            this.txt主机地址.StyleController = this.layoutControl2;
            this.txt主机地址.TabIndex = 6;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem19,
            this.emptySpaceItem5,
            this.layoutControlItem20,
            this.emptySpaceItem3,
            this.emptySpaceItem6});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(367, 544);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.Control = this.txt主机地址;
            this.layoutControlItem10.CustomizationFormText = "主机地址";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(347, 40);
            this.layoutControlItem10.Text = "主机地址";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(128, 19);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.Control = this.txt主机数据库名称;
            this.layoutControlItem11.CustomizationFormText = "主机数据库名称";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 40);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(347, 40);
            this.layoutControlItem11.Text = "主机数据库名称";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(128, 19);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.Control = this.txt主机数据库用户名;
            this.layoutControlItem13.CustomizationFormText = "主机数据库用户名";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 80);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(347, 40);
            this.layoutControlItem13.Text = "主机数据库用户名";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(128, 19);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.layoutControlItem14.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem14.Control = this.txt主机数据库密码;
            this.layoutControlItem14.CustomizationFormText = "主机数据库密码";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(347, 40);
            this.layoutControlItem14.Text = "主机数据库密码";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(128, 19);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.btnReadConfigure;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 174);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(170, 42);
            this.layoutControlItem19.Text = "layoutControlItem19";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextToControlDistance = 0;
            this.layoutControlItem19.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(170, 174);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(13, 42);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.btnSaveConfigure;
            this.layoutControlItem20.CustomizationFormText = "layoutControlItem20";
            this.layoutControlItem20.Location = new System.Drawing.Point(183, 174);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(164, 42);
            this.layoutControlItem20.Text = "layoutControlItem20";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextToControlDistance = 0;
            this.layoutControlItem20.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 216);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(347, 308);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 160);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(347, 14);
            this.emptySpaceItem6.Text = "emptySpaceItem6";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.layoutControl3);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(367, 544);
            this.xtraTabPage1.Text = "其它配置";
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.btn用药情况配置);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 0);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(367, 544);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // btn用药情况配置
            // 
            this.btn用药情况配置.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn用药情况配置.Appearance.Options.UseFont = true;
            this.btn用药情况配置.Image = ((System.Drawing.Image)(resources.GetObject("btn用药情况配置.Image")));
            this.btn用药情况配置.Location = new System.Drawing.Point(12, 12);
            this.btn用药情况配置.Margin = new System.Windows.Forms.Padding(2);
            this.btn用药情况配置.Name = "btn用药情况配置";
            this.btn用药情况配置.Size = new System.Drawing.Size(343, 38);
            this.btn用药情况配置.StyleController = this.layoutControl3;
            this.btn用药情况配置.TabIndex = 6;
            this.btn用药情况配置.Text = "用药情况配置";
            this.btn用药情况配置.Click += new System.EventHandler(this.btn用药情况配置_Click);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem4,
            this.layoutControlItem21});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(367, 544);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 42);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(347, 482);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.btn用药情况配置;
            this.layoutControlItem21.CustomizationFormText = "layoutControlItem21";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(347, 42);
            this.layoutControlItem21.Text = "layoutControlItem21";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextToControlDistance = 0;
            this.layoutControlItem21.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.layoutControlItem15.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem15.CustomizationFormText = "主机地址";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(345, 40);
            this.layoutControlItem15.Text = "主机地址";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(128, 19);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem16.CustomizationFormText = "主机数据库名称";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 40);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(345, 40);
            this.layoutControlItem16.Text = "主机数据库名称";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(128, 19);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.layoutControlItem17.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem17.CustomizationFormText = "主机数据库用户名";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 80);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(345, 40);
            this.layoutControlItem17.Text = "主机数据库用户名";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(128, 19);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.layoutControlItem18.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem18.CustomizationFormText = "主机数据库密码";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(345, 40);
            this.layoutControlItem18.Text = "主机数据库密码";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(128, 19);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // frm蓝牙配置
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(373, 591);
            this.Controls.Add(this.xtraTabControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm蓝牙配置";
            this.Text = "蓝牙配置";
            this.Load += new System.EventHandler(this.frm蓝牙配置_Load);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.tab蓝牙.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).EndInit();
            this.tab系统.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtPwd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUid.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatabase.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRGID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDefaultLogin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服务器地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt用户.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            this.tab主机.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt主机数据库密码.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt主机数据库用户名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt主机数据库名称.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt主机地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage tab蓝牙;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn col蓝牙名称;
        private DevExpress.XtraGrid.Columns.GridColumn colMacID;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit1;
        private DevExpress.XtraTab.XtraTabPage tab系统;
        private DevExpress.XtraEditors.SimpleButton btn配对;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btn系统配置保存;
        private DevExpress.XtraEditors.TextEdit txt随访医生;
        private DevExpress.XtraEditors.LookUpEdit txt用户;
        private DevExpress.XtraEditors.LookUpEdit txt机构;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit txt服务器地址;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.SimpleButton btn读取配置;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.ComboBoxEdit txtDefaultLogin;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.TextEdit txtRGID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.SimpleButton btnImportRegion;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraTab.XtraTabPage tab主机;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.TextEdit txt主机数据库用户名;
        private DevExpress.XtraEditors.TextEdit txt主机数据库名称;
        private DevExpress.XtraEditors.TextEdit txt主机地址;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraEditors.TextEdit txt主机数据库密码;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraEditors.SimpleButton btnReadConfigure;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraEditors.SimpleButton btnSaveConfigure;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.SimpleButton btn用药情况配置;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraEditors.TextEdit txtDatabase;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraEditors.TextEdit txtUid;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraEditors.TextEdit txtPwd;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
    }
}