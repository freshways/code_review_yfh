﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ATOMEHR_LeaveClient.MODEL;
using ATOMEHR_LeaveClient.DAL;

namespace ATOMEHR_LeaveClient.View
{
    public partial class Frm用药情况添加或修改 : DevExpress.XtraEditors.XtraForm
    {
        private string _pageStatus = "添加";
        public string PageStatus
        {
            get { return _pageStatus; }
            set { _pageStatus = value; }
        }

        private tb_用药情况配置表 _tb_用药情况配置表;
        public tb_用药情况配置表 Tb_用药情况配置表
        {
            get { return _tb_用药情况配置表; }
            set { _tb_用药情况配置表 = value; }
        }

        public Frm用药情况添加或修改()
        {
            InitializeComponent();
        }

        private void Frm用药情况添加或修改_Load(object sender, EventArgs e)
        {
            重置信息();

        }

        private void 重置信息()
        {
            if ("添加".Equals(_pageStatus))
            {
                this.txt名称.Text = "";
                this.txt拼音码.Text = "";
            }
            else
            {
                this.txt名称.Text = _tb_用药情况配置表.名称;
                this.txt拼音码.Text = _tb_用药情况配置表.拼音码;
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if ("添加".Equals(_pageStatus))
            {
                _tb_用药情况配置表 = new tb_用药情况配置表();
                // TODO 获取主键
                //_tb_用药情况配置表.Id = "";
                _tb_用药情况配置表.名称 = this.txt名称.Text.Trim();
                _tb_用药情况配置表.拼音码 = this.txt拼音码.Text.Trim();

                int count = 用药情况配置DAL.添加用药情况配置(_tb_用药情况配置表);
                if (count > 0)
                {
                    XtraMessageBox.Show("添加成功!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                {
                    XtraMessageBox.Show("添加失败!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            // 修改
            else
            {
                _tb_用药情况配置表.名称 = this.txt名称.Text.Trim();
                _tb_用药情况配置表.拼音码 = this.txt拼音码.Text.Trim();

                int count = 用药情况配置DAL.更新用药情况配置(_tb_用药情况配置表);
                if (count > 0)
                {
                    XtraMessageBox.Show("更新成功!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                {
                    XtraMessageBox.Show("更新失败!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            重置信息();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}