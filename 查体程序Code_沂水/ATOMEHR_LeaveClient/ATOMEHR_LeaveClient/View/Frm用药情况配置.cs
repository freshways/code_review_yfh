﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ATOMEHR_LeaveClient.MODEL;
using ATOMEHR_LeaveClient.DAL;

namespace ATOMEHR_LeaveClient.View
{
    public partial class Frm用药情况配置 : DevExpress.XtraEditors.XtraForm
    {
        List<tb_用药情况配置表> _list;
        public Frm用药情况配置()
        {
            InitializeComponent();
        }

        private void Frm用药情况配置_Load(object sender, EventArgs e)
        {
            initData();
        }

        private void initData()
        {
            _list = 用药情况配置DAL.获取用药情况配置();
            this.gc药品.DataSource = _list;
            this.gv药品.BestFitColumns();
        }

        private void btn添加_Click(object sender, EventArgs e)
        {
            Frm用药情况添加或修改 frm用药情况修改 = new Frm用药情况添加或修改();
            if (DialogResult.OK == frm用药情况修改.ShowDialog())
            {
                initData();
            }
        }

        private void btn修改_Click(object sender, EventArgs e)
        {
            int[] idxs = this.gv药品.GetSelectedRows();
            if (idxs.Count() < 1)
            {
                XtraMessageBox.Show("请先选择需要修改的药品!", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                return;
            }
            if (idxs.Count() > 1)
            {
                XtraMessageBox.Show("一次只能修改一条药品信息!", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                return;
            }

            int idxInDt = this.gv药品.GetDataSourceRowIndex(idxs[0]);
            tb_用药情况配置表 bean = _list[idxInDt];

            Frm用药情况添加或修改 frm用药情况修改 = new Frm用药情况添加或修改();
            frm用药情况修改.PageStatus = "修改";
            frm用药情况修改.Tb_用药情况配置表 = bean;
            if (DialogResult.OK == frm用药情况修改.ShowDialog())
            {
                initData();
            }
            //用药情况配置DAL.更新用药情况配置(bean);
        }

        private void btn删除_Click(object sender, EventArgs e)
        {
            int[] idxs = this.gv药品.GetSelectedRows();
            if (idxs.Count() < 1)
            {
                XtraMessageBox.Show("请先选择需要删除的药品!", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                return;
            }

            if (DialogResult.OK != XtraMessageBox.Show("您确定要删除选中的[" + idxs.Count() + "]条药品吗?", "警告", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning))
            {
                return;
            }

            List<tb_用药情况配置表> deleteList = new List<tb_用药情况配置表>();

            int idxInDt;
            for (int i = 0; i < idxs.Length; i++)
            {
                idxInDt = this.gv药品.GetDataSourceRowIndex(idxs[0]);
                deleteList.Add(_list[idxInDt]);
            }

            int count = 用药情况配置DAL.删除用药情况配置(deleteList);

            if (count > 0)
            {
                XtraMessageBox.Show("删除[" + count + "]条数据成功!", "提示");
                initData();
            }
            else
            {
                XtraMessageBox.Show("删除[" + count + "]条数据失败!", "提示");
            }
        }

        private void gv药品_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            e.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;

            //CustomDrawRowIndicator用于处理行头的自动编号
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                int num = e.RowHandle + 1;
                e.Info.DisplayText = num.ToString();
            }
        }



    }
}