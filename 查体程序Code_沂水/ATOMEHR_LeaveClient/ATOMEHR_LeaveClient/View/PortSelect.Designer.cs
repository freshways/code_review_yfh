﻿namespace ATOMEHR_LeaveClient.View
{
    partial class PortSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox_PortNames = new System.Windows.Forms.ComboBox();
            this.label_View = new System.Windows.Forms.Label();
            this.button_Open = new System.Windows.Forms.Button();
            this.button_Refresh = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comboBox_PortNames
            // 
            this.comboBox_PortNames.FormattingEnabled = true;
            this.comboBox_PortNames.Location = new System.Drawing.Point(14, 24);
            this.comboBox_PortNames.Name = "comboBox_PortNames";
            this.comboBox_PortNames.Size = new System.Drawing.Size(177, 20);
            this.comboBox_PortNames.TabIndex = 0;
            // 
            // label_View
            // 
            this.label_View.AutoSize = true;
            this.label_View.Location = new System.Drawing.Point(12, 9);
            this.label_View.Name = "label_View";
            this.label_View.Size = new System.Drawing.Size(83, 12);
            this.label_View.TabIndex = 1;
            this.label_View.Text = "选择xxxcom口:";
            // 
            // button_Open
            // 
            this.button_Open.Location = new System.Drawing.Point(12, 59);
            this.button_Open.Name = "button_Open";
            this.button_Open.Size = new System.Drawing.Size(75, 23);
            this.button_Open.TabIndex = 2;
            this.button_Open.Text = "打开";
            this.button_Open.UseVisualStyleBackColor = true;
            this.button_Open.Click += new System.EventHandler(this.button_Open_Click);
            // 
            // button_Refresh
            // 
            this.button_Refresh.Location = new System.Drawing.Point(116, 59);
            this.button_Refresh.Name = "button_Refresh";
            this.button_Refresh.Size = new System.Drawing.Size(75, 23);
            this.button_Refresh.TabIndex = 3;
            this.button_Refresh.Text = "刷新";
            this.button_Refresh.UseVisualStyleBackColor = true;
            this.button_Refresh.Click += new System.EventHandler(this.button_Refresh_Click);
            // 
            // PortSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(204, 96);
            this.Controls.Add(this.button_Refresh);
            this.Controls.Add(this.button_Open);
            this.Controls.Add(this.label_View);
            this.Controls.Add(this.comboBox_PortNames);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PortSelect";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "选择设备串口";
            this.Load += new System.EventHandler(this.PortSelect_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox_PortNames;
        private System.Windows.Forms.Label label_View;
        private System.Windows.Forms.Button button_Open;
        private System.Windows.Forms.Button button_Refresh;
    }
}