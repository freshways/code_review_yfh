﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;
using ATOMEHR_LeaveClient.View;

namespace ATOMEHR_LeaveClient
{
    public partial class frm蓝牙配置 : Form
    {

        public IntPtr hBluetooth;
        血氧信息.ONDISCOVERYCOMPLETEDDelegate onDiscovery;

        public frm蓝牙配置()
        {
            InitializeComponent();
            onDiscovery = new 血氧信息.ONDISCOVERYCOMPLETEDDelegate(funDiscoveryFinish);
        }

        public void funDiscoveryFinish(int count, IntPtr devices)
        {
            try
            {
                if (count > 0)
                {
                    List<tagBLUETOOTH_DEVICE> listDevices = new List<tagBLUETOOTH_DEVICE>();
                    tagBLUETOOTH_DEVICE[] list = new tagBLUETOOTH_DEVICE[count];
                    int size = Marshal.SizeOf(typeof(tagBLUETOOTH_DEVICE));
                    //IntPtr allocIntPtr = Marshal.AllocHGlobal(size * count);
                    for (int n = 0; n < count; n++)
                    {
                        IntPtr ptr = (IntPtr)((UInt32)devices + n * size);
                        tagBLUETOOTH_DEVICE tag = (tagBLUETOOTH_DEVICE)Marshal.PtrToStructure(ptr, typeof(tagBLUETOOTH_DEVICE));
                        list[n] = tag;
                        listDevices.Add(tag);
                    }

                    this.gridControl1.DataSource = listDevices;

                }
                else//为搜索到蓝牙设备
                {
                    MessageBox.Show("未搜索到蓝牙设备，请重新连接！");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void frm蓝牙配置_Load(object sender, EventArgs e)
        {
            hBluetooth = 血氧信息.BluetoothOperationOpen();
            if (hBluetooth != null)
            {
                血氧信息.BluetoothOperationRegisterDiscoveryCallBack(hBluetooth, onDiscovery);
                血氧信息.BluetoothOperationDiscovery(hBluetooth, 5);
            }

            //2017-02-28 11:03:56 yufh 添加
            Get机构("");

            // 2017-05-25 11:13 add by 朱文杰 begin
            btn读取配置_Click(null, null);
            btnReadConfigure_Click(null, null);
            // 2017-05-25 11:13 add by 朱文杰 end
        }

        private void btn配对_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.comboBoxEdit1.Text.Trim() == "请选择")
                {
                    MessageBox.Show("请先选择蓝牙查体类型");
                    return;
                }

                if (this.gridView1.SelectedRowsCount == 1)
                {
                    tagBLUETOOTH_DEVICE tag = (tagBLUETOOTH_DEVICE)this.gridView1.GetRow(this.gridView1.GetSelectedRows()[0]);
                    if (string.IsNullOrEmpty(tag.Name)) return;
                    if (tag.Name.StartsWith("SS-"))//配置身份证读卡器
                    {
                        ControlsHelper.IniWriteValue("BlueTooth", "IDCardName", tag.Name, System.IO.Path.Combine(Program.toAppPath, Program.iniName));
                        ControlsHelper.IniWriteValue("BlueTooth", "IDCardAddr", tag.Addr, System.IO.Path.Combine(Program.toAppPath, Program.iniName));
                        MessageBox.Show("身份证读卡器设备配对成功，请配置另外的查体设备！");
                        this.comboBoxEdit1.SelectedIndex = 0;
                        return;
                    }
                    else //配置其他的蓝牙设备 
                    {
                        ControlsHelper.IniWriteValue("BlueTooth", "Name", tag.Name, System.IO.Path.Combine(Program.toAppPath, Program.iniName));
                        ControlsHelper.IniWriteValue("BlueTooth", "Addr", tag.Addr, System.IO.Path.Combine(Program.toAppPath, Program.iniName));
                        ControlsHelper.IniWriteValue("BlueTooth", "Type", this.comboBoxEdit1.Text, System.IO.Path.Combine(Program.toAppPath, Program.iniName));
                        MessageBox.Show("设备配对成功！");
                    }
                }
                else if (this.comboBoxEdit1.Text.Contains("有线"))
                {
                    PortSelect frm = new PortSelect(this.comboBoxEdit1.Text,"配置",null,null);
                    frm.ShowDialog();
                    ControlsHelper.IniWriteValue("BlueTooth", "Type", this.comboBoxEdit1.Text, Path.Combine(Program.toAppPath, Program.iniName));
                    ControlsHelper.IniWriteValue("Connection", "ThermometerGunCom", frm._por, Path.Combine(Program.toAppPath, Program.iniName));
                    MessageBox.Show("设备配对成功！");
                }
                else
                {
                    MessageBox.Show("只能选择一个蓝牙设备！");
                    return;
                }
                
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #region 系统配置

        private void SysIniLoad()
        {

        }

        private static DBClass dbConstring;
        private void Get机构(string RGID)
        {
            try
            {
                if (string.IsNullOrEmpty(RGID)) return;

                dbConstring = new DBClass(DbFilePath.FilePathName);
                string SQL = "select 机构编号,机构名称 from  tb_机构信息 where (机构编号=@RGID or 上级机构=@RGID) ";
                System.Data.SQLite.SQLiteParameter sp = new System.Data.SQLite.SQLiteParameter("@RGID", RGID);
                DataTable dt = SQLiteHelper.ExecuteDataSet(dbConstring.m_ConnString, SQL, CommandType.Text, sp).Tables[0];
                if (dt != null && dt.Rows.Count > 0)
                    txt机构.Properties.DataSource = dt;
                txt机构.Properties.DisplayMember = "机构名称";
                txt机构.Properties.ValueMember = "机构编号";
            }
            catch (Exception)
            {

            }
        }

        private void txt机构_EditValueChanged(object sender, EventArgs e)
        {
            Get用户(txt机构.EditValue.ToString());
        }

        private void Get用户(string rgid)
        {
            if (string.IsNullOrEmpty(rgid)) return;
            try
            {
                dbConstring = new DBClass(DbFilePath.FilePathName);
                string SQL = "select 用户编码,UserName from  tb_MyUser where IsLocked='0' and 所属机构='" + rgid + "' ";
                DataTable dt = SQLiteHelper.ExecuteDataSet(dbConstring.m_ConnString, SQL, CommandType.Text).Tables[0];
                if (dt != null && dt.Rows.Count > 0)
                    txt用户.Properties.DataSource = dt;
                txt用户.Properties.DisplayMember = "UserName";
                txt用户.Properties.ValueMember = "用户编码";
            }
            catch (Exception)
            {

            }
        }

        private void btn系统配置保存_Click(object sender, EventArgs e)
        {
            ControlsHelper.IniWriteValue("BlueTooth", "ParentRGID", this.txtRGID.Text.Trim(), Path.Combine(Program.toAppPath, Program.iniName));
            ControlsHelper.IniWriteValue("BlueTooth", "SerVer", this.txt服务器地址.Text.Trim(), Path.Combine(Program.toAppPath, Program.iniName));
            ControlsHelper.IniWriteValue("BlueTooth", "Doctor", this.txt随访医生.Text.Trim(), Path.Combine(Program.toAppPath, Program.iniName));
            if (txt机构.Text != "" && txt机构.EditValue != null)
            {
                ControlsHelper.IniWriteValue("BlueTooth", "RGID", this.txt机构.EditValue.ToString(), Path.Combine(Program.toAppPath, Program.iniName));
                ControlsHelper.IniWriteValue("BlueTooth", "RGName", this.txt机构.Text.ToString(), Path.Combine(Program.toAppPath, Program.iniName));
            }
            if (txt用户.Text != "" && txt用户.EditValue != null)
                ControlsHelper.IniWriteValue("BlueTooth", "UserID", this.txt用户.EditValue.ToString(), Path.Combine(Program.toAppPath, Program.iniName));
            ControlsHelper.IniWriteValue("BlueTooth", "DefaultLogin", this.txtDefaultLogin.EditValue.ToString(), Path.Combine(Program.toAppPath, Program.iniName));
            //---add by lid--start
            ControlsHelper.IniWriteValue("BlueTooth", "database", this.txtDatabase.EditValue.ToString(), Path.Combine(Program.toAppPath, Program.iniName));
            ControlsHelper.IniWriteValue("BlueTooth", "uid", this.txtUid.EditValue.ToString(), Path.Combine(Program.toAppPath, Program.iniName));
            ControlsHelper.IniWriteValue("BlueTooth", "pwd", this.txtPwd.EditValue.ToString(), Path.Combine(Program.toAppPath, Program.iniName));
            //---add by lid---end
            MessageBox.Show("配置保存成功！");
            Get机构(txtRGID.Text.Trim());
            Program.BindingIni();
        }

        private void btn读取配置_Click(object sender, EventArgs e)
        {
            this.txtRGID.Text = ControlsHelper.IniReadValue("BlueTooth", "ParentRGID", Path.Combine(Program.toAppPath, Program.iniName));
            this.txt服务器地址.Text = ControlsHelper.IniReadValue("BlueTooth", "SerVer", Path.Combine(Program.toAppPath, Program.iniName));
            this.txt随访医生.Text = ControlsHelper.IniReadValue("BlueTooth", "Doctor", Path.Combine(Program.toAppPath, Program.iniName));
            this.txt机构.EditValue = ControlsHelper.IniReadValue("BlueTooth", "RGID", Path.Combine(Program.toAppPath, Program.iniName));
            this.txt用户.EditValue = ControlsHelper.IniReadValue("BlueTooth", "UserID", Path.Combine(Program.toAppPath, Program.iniName));
            //
            this.txtDefaultLogin.EditValue = ControlsHelper.IniReadValue("BlueTooth", "DefaultLogin", Path.Combine(Program.toAppPath, Program.iniName));
            Get机构(txtRGID.Text.Trim());

            //add by lid---start
            this.txtDatabase.Text = ControlsHelper.IniReadValue("BlueTooth", "database", Path.Combine(Program.toAppPath, Program.iniName));
            this.txtUid.Text = ControlsHelper.IniReadValue("BlueTooth", "uid", Path.Combine(Program.toAppPath, Program.iniName));
            this.txtPwd.Text = ControlsHelper.IniReadValue("BlueTooth", "pwd", Path.Combine(Program.toAppPath, Program.iniName));

            //add by lid---end
        }

        #endregion

        private void btnImportRegion_Click(object sender, EventArgs e)
        {
            try
            {
                List<tb_机构信息> list = new List<tb_机构信息>();
                tb_机构信息 tb = null;

                string SQL = "select * from tb_机构信息 ";
                SqlDataReader reader = DBManager.ExecuteQuery(SQL);
                using (reader)
                {
                    while (reader.Read())
                    {
                        tb = new tb_机构信息();
                        tb.ID = Convert.ToInt64(reader["ID"].ToString());
                        tb.机构编号 = reader["机构编号"].ToString();
                        tb.机构名称 = reader["机构名称"].ToString();
                        tb.P_RGID_MARKER = reader["P_RGID_MARKER"].ToString();
                        tb.负责人 = reader["负责人"].ToString();
                        tb.联系人 = reader["联系人"].ToString();
                        tb.联系电话 = reader["联系电话"].ToString();
                        tb.上级机构 = reader["上级机构"].ToString();
                        tb.机构级别 = reader["机构级别"].ToString();
                        tb.创建人 = reader["创建人"].ToString();
                        //tb.创建时间 = Convert.ToDateTime(reader["创建时间"]);
                        tb.状态 = reader["状态"].ToString();
                        tb.P_QYDW = reader["P_QYDW"].ToString();
                        list.Add(tb);
                    }
                }
                //先全部删除
                tb_机构信息DAL.Delete("");

                tb_机构信息DAL.InsertList(list);
                MessageBox.Show("导入数据成功，总共导入数据" + list.Count + "条！");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// 保存主机的相关配置信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveConfigure_Click(object sender, EventArgs e)
        {
            ControlsHelper.IniWriteValue("BlueTooth", "ZJSerVer", this.txt主机地址.Text.Trim(), Path.Combine(Program.toAppPath, Program.iniName));
            ControlsHelper.IniWriteValue("BlueTooth", "ZJDatabase", this.txt主机数据库名称.EditValue.ToString(), Path.Combine(Program.toAppPath, Program.iniName));
            ControlsHelper.IniWriteValue("BlueTooth", "ZJUid", this.txt主机数据库用户名.EditValue.ToString(), Path.Combine(Program.toAppPath, Program.iniName));
            ControlsHelper.IniWriteValue("BlueTooth", "ZJPwd", this.txt主机数据库密码.EditValue.ToString(), Path.Combine(Program.toAppPath, Program.iniName));
            MessageBox.Show("配置保存成功！");
            Program.BindingIni();
        }

        /// <summary>
        /// 读取主机的相关配置信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReadConfigure_Click(object sender, EventArgs e)
        {
            this.txt主机地址.Text = ControlsHelper.IniReadValue("BlueTooth", "ZJSerVer", Path.Combine(Program.toAppPath, Program.iniName));
            this.txt主机数据库名称.Text = ControlsHelper.IniReadValue("BlueTooth", "ZJDatabase", Path.Combine(Program.toAppPath, Program.iniName));
            this.txt主机数据库用户名.Text = ControlsHelper.IniReadValue("BlueTooth", "ZJUid", Path.Combine(Program.toAppPath, Program.iniName));
            this.txt主机数据库密码.Text = ControlsHelper.IniReadValue("BlueTooth", "ZJPwd", Path.Combine(Program.toAppPath, Program.iniName));
        }

        private void btn用药情况配置_Click(object sender, EventArgs e)
        {
            Frm用药情况配置 frm用药情况配置 = new Frm用药情况配置();
            frm用药情况配置.ShowDialog();
        }

    }
}
