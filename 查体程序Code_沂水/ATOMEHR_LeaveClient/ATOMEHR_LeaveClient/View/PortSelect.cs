﻿using DevExpress.XtraEditors;
using serialPort.PortClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ATOMEHR_LeaveClient.View
{
    public partial class PortSelect : Form
    {
        PortClass pc;
        string ProjectName;
        string FormName;
        TextEdit te;
        DevExpress.XtraBars.Docking2010.Views.QueryControlEventArgs e;

        public PortSelect(string _ProjectName, string _FormName, TextEdit _te, DevExpress.XtraBars.Docking2010.Views.QueryControlEventArgs e)
        {
            InitializeComponent();
            this.ProjectName = _ProjectName;
            this.FormName = _FormName;
            this.te = _te;
            this.e = e;
            pc = new PortClass();
            BindPortNames();
        }

        public string _por = string.Empty;
        public PortSelect()
        {
            InitializeComponent();
        }

        private void button_Open_Click(object sender, EventArgs e)
        {
            string strPortNames = comboBox_PortNames.Text;
            if (string.IsNullOrEmpty(strPortNames))
            {
                MessageBox.Show("请先选择" + ProjectName + "串口!");
                return;
            }
            else
            {
            //    uc体温 control = new uc体温();
            //    control.TxtParentBarCode = te;
            //    e.Control = control;
            }
            _por = strPortNames;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void button_Refresh_Click(object sender, EventArgs e)
        {
            try
            {
                button_Refresh.Enabled = false;
                BindPortNames();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                button_Refresh.Enabled = true;
            }
        }

        private void BindPortNames()
        {
            string[] PortNames = pc.GetPortNames();
            if (comboBox_PortNames.Items.Count > 0) comboBox_PortNames.Items.Clear();
            comboBox_PortNames.Text = string.Empty;
            comboBox_PortNames.Items.AddRange(PortNames);
        }

        private void PortSelect_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ProjectName))
            {
                label_View.Text = "选择" + ProjectName + "串口:";
            }
        }
    }
}
