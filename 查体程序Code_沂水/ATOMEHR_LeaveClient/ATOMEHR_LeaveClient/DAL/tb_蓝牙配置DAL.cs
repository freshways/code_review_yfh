﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Data.SQLite;

namespace ATOMEHR_LeaveClient
{
    public class tb_蓝牙配置DAL
    {
        /// <summary>
        /// 获取所有信息
        /// </summary>
        /// <param name="where">条件</param>
        /// <returns>结果集</returns>
        public static SQLiteDataReader GetInfo(string where)
        {
            string sqlStr = "SELECT * FROM  where ";
            if (String.IsNullOrEmpty(where))
            {
                sqlStr += "1=1";
            }
            else
            {
                sqlStr += where;
            }
            SQLiteDataReader reader = null;
            try
            {
                reader = DBManager.ExecuteQueryForSQLite(sqlStr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reader;
        }

        /// <summary>
        /// 获取所有信息集合
        /// </summary>
        /// <returns>List集合</returns>
        public static List<tb_蓝牙配置Info> GetInfoList(string where)
        {
            List<tb_蓝牙配置Info> infoList = new List<tb_蓝牙配置Info>();

            SQLiteDataReader reader = null;
            try
            {
                reader = GetInfo(where);
            }
            catch (Exception)
            {
                throw;
            }

            while (reader.Read())
            {
                tb_蓝牙配置Info info = new tb_蓝牙配置Info();
                info.ID = reader["ID"].ToString();
                info.蓝牙名称 = reader["蓝牙名称"].ToString();
                info.类别 = reader["类别"].ToString();
                infoList.Add(info);
            }
            reader.Close();
            return infoList;
        }

        /// <summary>
        /// 根据 主键 获取一个实体对象
        /// <param name="ID">主键</param>
        /// </summary>
        public static tb_蓝牙配置Info GetInfoById(string ID)
        {
            string strWhere = "ID =" + ID;
            List<tb_蓝牙配置Info> list = GetInfoList(strWhere);
            if (list.Count > 0)
                return list[0];
            return null;
        }

        /// 添加数据
        /// </summary>
        /// <param name="info">数据表实体对象</param>
        public static bool Add(tb_蓝牙配置Info info)
        {
            string _蓝牙名称 = info.蓝牙名称;
            string _类别 = info.类别;

            string sql = "INSERT INTO  VALUES (@_蓝牙名称,@_类别)";
            int rst = DBManager.ExecuteUpdate(DBManager.Conn, sql, new object[] { new SqlParameter("@_蓝牙名称", _蓝牙名称), new SqlParameter("@_类别", _类别) });
            if (rst > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除一个对象
        /// </summary>
        /// <param name="ID">主键</param>
        /// <returns></returns>
        public static bool DeleteInfo(tb_蓝牙配置Info info)
        {
            bool rst = false;
            string sqlStr = "DELETE FROM  WHERE ID=" + info.ID;
            int rows = DBManager.ExecuteUpdate(DBManager.Conn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }

        /// 更新数据
        /// </summary>
        /// <param name="info">数据表实体</param>
        public static bool UpdateInfo(tb_蓝牙配置Info info)
        {
            string _蓝牙名称 = info.蓝牙名称;
            string _类别 = info.类别;
            string sql = "UPDATE  SET 蓝牙名称=@_蓝牙名称, 类别=@_类别  WHERE ID=" + info.ID;
            int rst = DBManager.ExecuteUpdate(DBManager.Conn, sql, new object[] { new SqlParameter("@_蓝牙名称", _蓝牙名称), new SqlParameter("@_类别", _类别) });
            if (rst > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
