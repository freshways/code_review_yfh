﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Linq;
using System.Text;

namespace ATOMEHR_LeaveClient
{
    public class tb_健康体检_用药情况表DAL
    {
        /// <summary>
        /// 获取所有信息
        /// </summary>
        /// <param name="where">条件</param>
        /// <returns>结果集</returns>
        public static SQLiteDataReader GetInfo(string where)
        {
            string sqlStr = "SELECT * FROM tb_健康体检_用药情况表 where ";
            if (String.IsNullOrEmpty(where))
            {
                sqlStr += "1=1";
            }
            else
            {
                sqlStr += where;
            }
            SQLiteDataReader reader = null;
            try
            {
                reader = DBManager.ExecuteQueryForSQLite(sqlStr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reader;
        }

        /// <summary>
        /// 获取所有信息集合
        /// </summary>
        /// <returns>List集合</returns>
        public static List<tb_健康体检_用药情况表Info> GetInfoList(string where)
        {
            List<tb_健康体检_用药情况表Info> infoList = new List<tb_健康体检_用药情况表Info>();

            SQLiteDataReader reader = null;
            try
            {
                reader = GetInfo(where);
            }
            catch (Exception)
            {
                throw;
            }

            while (reader.Read())
            {
                tb_健康体检_用药情况表Info info = new tb_健康体检_用药情况表Info();
                info.ID = reader["ID"].ToString();
                info.身份证号 = reader["身份证号"].ToString();
                info.个人档案编号 = reader["个人档案编号"].ToString();
                info.药物名称 = reader["药物名称"].ToString();
                info.用法 = reader["用法"].ToString();
                info.用量 = reader["用量"].ToString();
                info.用药时间 = reader["用药时间"].ToString();
                info.服药依从性 = reader["服药依从性"].ToString();
                info.创建时间 = reader["创建时间"].ToString();
                info.BlueName = reader["BlueName"].ToString();
                info.BlueAddr = reader["BlueAddr"].ToString();
                info.BlueType = reader["BlueType"].ToString();
                infoList.Add(info);
            }
            reader.Close();
            return infoList;
        }

        /// <summary>
        /// 根据 主键 获取一个实体对象
        /// <param name="ID">主键</param>
        /// </summary>
        public static tb_健康体检_用药情况表Info GetInfoById(string ID)
        {
            string strWhere = "ID =" + ID;
            List<tb_健康体检_用药情况表Info> list = GetInfoList(strWhere);
            if (list.Count > 0)
                return list[0];
            return null;
        }

        /// 添加数据
        /// </summary>
        /// <param name="info">数据表实体对象</param>
        public static bool Add(tb_健康体检_用药情况表Info info)
        {
            string _身份证号 = Program.currentUser.ID;
            string _个人档案编号 = Program.currentUser.DocNo;
            string _药物名称 = info.药物名称;
            string _用法 = info.用法;
            string _用量 = info.用量;
            string _用药时间 = info.用药时间;
            string _服药依从性 = info.服药依从性;
            string _创建时间 = Program._currentTime;
            string _BlueName = Program.BlueName;
            string _BlueAddr = Program.BlueAddr;
            string _BlueType = Program.BlueType;

            string sql = "INSERT INTO tb_健康体检_用药情况表 VALUES (NULL,@_身份证号,@_个人档案编号,@_药物名称,@_用法,@_用量,@_用药时间,@_服药依从性,@_创建时间,'',@_BlueAddr,@_BlueType,@_BlueName)";
            //int rst = DBManager.ExecuteProc("SP_SAVE用药情况表", new object[] { new SqlParameter("@_ID", info.ID), new SqlParameter("@_身份证号", _身份证号), new SqlParameter("@_个人档案编号", _个人档案编号), new SqlParameter("@_药物名称", _药物名称), new SqlParameter("@_用法", _用法), new SqlParameter("@_用量", _用量), new SqlParameter("@_用药时间", _用药时间), new SqlParameter("@_服药依从性", _服药依从性), new SqlParameter("@_创建时间", _创建时间) });
            int rst = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sql, new object[] { new SQLiteParameter("@_身份证号", _身份证号), new SQLiteParameter("@_个人档案编号", _个人档案编号), new SQLiteParameter("@_药物名称", _药物名称), new SQLiteParameter("@_用法", _用法), new SQLiteParameter("@_用量", _用量), new SQLiteParameter("@_用药时间", _用药时间), new SQLiteParameter("@_服药依从性", _服药依从性), new SQLiteParameter("@_创建时间", _创建时间), new SQLiteParameter("@_BlueAddr", _BlueAddr), new SQLiteParameter("@_BlueType", _BlueType), new SQLiteParameter("@_BlueName", _BlueName) });
            if (rst > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除一个对象
        /// </summary>
        /// <param name="ID">主键</param>
        /// <returns></returns>
        public static bool DeleteInfo(tb_健康体检_用药情况表Info info)
        {
            bool rst = false;
            string sqlStr = "DELETE FROM tb_健康体检_用药情况表 WHERE ID=" + info.ID;
            int rows = DBManager.ExecuteUpdate(DBManager.Conn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }
        /// <summary>
        /// 通过身份证号进行删除
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool DeleteInfo(string key)
        {
            bool rst = false;
            string sqlStr = "DELETE FROM tb_健康体检_用药情况表 WHERE 身份证号='" + key + "'";
            int rows = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }
        /// 更新数据
        /// </summary>
        /// <param name="info">数据表实体</param>
        public static bool UpdateInfo(tb_健康体检_用药情况表Info info)
        {
            string _身份证号 = Program.currentUser.ID;
            string _个人档案编号 = Program.currentUser.DocNo;
            string _药物名称 = info.药物名称;
            string _用法 = info.用法;
            string _用量 = info.用量;
            string _用药时间 = info.用药时间;
            string _服药依从性 = info.服药依从性;
            string _创建时间 = Program._currentTime;
            string sql = "UPDATE tb_健康体检_用药情况表 SET 身份证号=@_身份证号, 个人档案编号=@_个人档案编号, 药物名称=@_药物名称, 用法=@_用法, 用量=@_用量, 用药时间=@_用药时间, 服药依从性=@_服药依从性, 创建时间=@_创建时间  WHERE ID=" + info.ID;
            int rst = DBManager.ExecuteUpdate(DBManager.Conn, sql, new object[] { new SqlParameter("@_身份证号", _身份证号), new SqlParameter("@_个人档案编号", _个人档案编号), new SqlParameter("@_药物名称", _药物名称), new SqlParameter("@_用法", _用法), new SqlParameter("@_用量", _用量), new SqlParameter("@_用药时间", _用药时间), new SqlParameter("@_服药依从性", _服药依从性), new SqlParameter("@_创建时间", _创建时间) });
            if (rst > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool AddTo121(tb_健康体检_用药情况表Info info)
        {
            string _身份证号 = info.身份证号;
            string _个人档案编号 = info.个人档案编号;
            string _药物名称 = info.药物名称;
            string _用法 = info.用法;
            string _用量 = info.用量;
            string _用药时间 = info.用药时间;
            string _服药依从性 = info.服药依从性;
            string _创建时间 = info.创建时间;
            string _BlueName = info.BlueName;
            string _BlueAddr = info.BlueAddr;
            string _BlueType = info.BlueType;
            //string sql = "INSERT INTO tb_健康体检_用药情况表 VALUES (@_身份证号,@_个人档案编号,@_药物名称,@_用法,@_用量,@_用药时间,@_服药依从性,@_创建时间,'')";
            int rst = DBManager.ExecuteProc(DBManager.Conn121, "USP_IMPORT用药情况表", new object[] { new SqlParameter("@_身份证号", _身份证号), new SqlParameter("@_个人档案编号", _个人档案编号), new SqlParameter("@_药物名称", _药物名称), new SqlParameter("@_用法", _用法), new SqlParameter("@_用量", _用量), new SqlParameter("@_用药时间", _用药时间), new SqlParameter("@_服药依从性", _服药依从性), new SqlParameter("@_创建时间", _创建时间), new SqlParameter("@_BlueName", _BlueName), new SqlParameter("@_BlueAddr", _BlueAddr), new SqlParameter("@_BlueType", _BlueType) });

            if (rst > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
