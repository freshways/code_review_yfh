﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.SQLite;

namespace ATOMEHR_LeaveClient
{
    class tb_老年人生活自理能力评价DAL
    {
        /// <summary>
        /// 获取所有信息
        /// </summary>
        /// <param name="where">条件</param>
        /// <returns>结果集</returns>
        public static SQLiteDataReader GetInfo(string where)
        {
            string sqlStr = "SELECT * FROM tb_老年人生活自理能力评价 where ";
            if (String.IsNullOrEmpty(where))
            {
                sqlStr += "1=1";
            }
            else
            {
                sqlStr += where;
            }
            SQLiteDataReader reader = null;
            try
            {
                reader = DBManager.ExecuteQueryForSQLite(sqlStr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reader;
        }

        /// <summary>
        /// 获取所有信息集合
        /// </summary>
        /// <returns>List集合</returns>
        public static List<tb_老年人生活自理能力评价Info> GetInfoList(string where)
        {
            List<tb_老年人生活自理能力评价Info> infoList = new List<tb_老年人生活自理能力评价Info>();

            SQLiteDataReader reader = null;
            try
            {
                reader = GetInfo(where);
            }
            catch (Exception)
            {
                throw;
            }

            while (reader.Read())
            {
                tb_老年人生活自理能力评价Info info = new tb_老年人生活自理能力评价Info();
                info.ID = reader["ID"].ToString();
                info.身份证号 = reader["身份证号"].ToString();
                info.姓名 = reader["姓名"].ToString();
                info.性别 = reader["性别"].ToString();
                info.出生日期 = reader["出生日期"].ToString();
                info.居住地址 = reader["居住地址"].ToString();
                info.个人档案编号 = reader["个人档案编号"].ToString();
                info.G_ZZBS = reader["G_ZZBS"].ToString();
                info.G_ZZXZZ = reader["G_ZZXZZ"].ToString();
                info.G_ZZCX = reader["G_ZZCX"].ToString();
                info.G_ZZZZ = reader["G_ZZZZ"].ToString();
                info.G_ZTH = reader["G_ZTH"].ToString();
                info.G_ZTYY = reader["G_ZTYY"].ToString();
                info.G_ZTZD = reader["G_ZTZD"].ToString();
                info.G_ZTZZ = reader["G_ZTZZ"].ToString();
                info.G_SFTZ = decimal.Parse(reader["G_SFTZ"].ToString());
                info.G_RXY = int.Parse(reader["G_RXY"].ToString());
                info.G_RYJ = decimal.Parse(reader["G_RYJ"].ToString());
                info.G_ZYD = int.Parse(reader["G_ZYD"].ToString());
                info.G_YDSJ = decimal.Parse(reader["G_YDSJ"].ToString());
                info.G_YS = reader["G_YS"].ToString();
                info.G_XLTZ = reader["G_XLTZ"].ToString();
                info.G_ZYXW = reader["G_ZYXW"].ToString();
                info.G_YMJZ = reader["G_YMJZ"].ToString();
                info.G_GXBYF = reader["G_GXBYF"].ToString();
                info.G_GZSSYF = reader["G_GZSSYF"].ToString();
                info.下次随访目标 = reader["下次随访目标"].ToString();
                info.下次随访日期 = reader["下次随访日期"].ToString();
                info.随访医生 = reader["随访医生"].ToString();
                info.创建机构 = reader["创建机构"].ToString();
                info.创建人 = reader["创建人"].ToString();
                info.更新人 = reader["更新人"].ToString();
                info.所属机构 = reader["所属机构"].ToString();
                info.创建时间 = reader["创建时间"].ToString();
                info.更新时间 = reader["更新时间"].ToString();
                info.随访日期 = reader["随访日期"].ToString();
                info.缺项 = reader["缺项"].ToString();
                info.进餐评分 = reader["进餐评分"].ToString();
                info.梳洗评分 = reader["梳洗评分"].ToString();
                info.如厕评分 = reader["如厕评分"].ToString();
                info.活动评分 = reader["活动评分"].ToString();
                info.总评分 = reader["总评分"].ToString();
                info.穿衣评分 = reader["穿衣评分"].ToString();
                info.随访次数 = reader["随访次数"].ToString();
                info.完整度 = reader["完整度"].ToString();
                info.BlueName = reader["BlueName"].ToString();
                info.BlueAddr = reader["BlueAddr"].ToString();
                info.BlueType = reader["BlueType"].ToString();
                info.RowState = reader["RowState"].ToString();
                infoList.Add(info);
            }
            reader.Close();
            return infoList;
        }

        /// <summary>
        /// 根据 主键 获取一个实体对象
        /// <param name="ID">身份证号</param>
        /// </summary>
        public static tb_老年人生活自理能力评价Info GetInfoById(string ID)
        {
            string strWhere = " 身份证号 ='" + ID + "'";
            List<tb_老年人生活自理能力评价Info> list = GetInfoList(strWhere);
            if (list.Count > 0)
                return list[0];
            return null;
        }

        /// 添加数据
        /// </summary>
        /// <param name="info">数据表实体对象</param>
        public static bool Save(tb_老年人生活自理能力评价Info info)
        {
            string _身份证号 = Program.currentUser.ID;
            string _姓名 = Program.currentUser.Name;
            string _性别 = Program.currentUser.Gender;
            string _出生日期 = Program.currentUser.Birth;
            string _居住地址 = Program.currentUser.Addr;
            string _个人档案编号 = Program.currentUser.DocNo;
            string _G_ZZBS = info.G_ZZBS;
            string _G_ZZXZZ = info.G_ZZXZZ;
            string _G_ZZCX = info.G_ZZCX;
            string _G_ZZZZ = info.G_ZZZZ;
            string _G_ZTH = info.G_ZTH;
            string _G_ZTYY = info.G_ZTYY;
            string _G_ZTZD = info.G_ZTZD;
            string _G_ZTZZ = info.G_ZTZZ;
            decimal _G_SFTZ = info.G_SFTZ;
            int _G_RXY = info.G_RXY;
            decimal _G_RYJ = info.G_RYJ;
            int _G_ZYD = info.G_ZYD;
            decimal _G_YDSJ = info.G_YDSJ;
            string _G_YS = info.G_YS;
            string _G_XLTZ = info.G_XLTZ;
            string _G_ZYXW = info.G_ZYXW;
            string _G_YMJZ = info.G_YMJZ;
            string _G_GXBYF = info.G_GXBYF;
            string _G_GZSSYF = info.G_GZSSYF;
            string _下次随访目标 = info.下次随访目标;
            string _下次随访日期 = info.下次随访日期;
            string _随访医生 = info.随访医生;
            string _创建机构 = Program.currentUser.Prgid;
            string _创建人 = Program.currentUser.创建人; //"371323B100190001";
            string _更新人 = Program.currentUser.创建人; //"371323B100190001";
            string _所属机构 = Program.currentUser.Prgid;
            string _创建时间 = Program._currentTime;
            string _更新时间 = Program._currentTime;
            string _随访日期 = info.随访日期;
            string _缺项 = info.缺项;
            string _进餐评分 = info.进餐评分;
            string _梳洗评分 = info.梳洗评分;
            string _如厕评分 = info.如厕评分;
            string _活动评分 = info.活动评分;
            string _总评分 = info.总评分;
            string _穿衣评分 = info.穿衣评分;
            string _随访次数 = info.随访次数;
            string _完整度 = info.完整度;
            string _BlueName = Program.BlueName;
            string _BlueAddr = Program.BlueAddr;
            string _BlueType = Program.BlueType;
            string sql = "INSERT INTO tb_老年人生活自理能力评价 VALUES (null,@身份证号,@姓名,@性别,@出生日期,@居住地址,@个人档案编号,@G_ZZBS,@G_ZZXZZ,@G_ZZCX,@G_ZZZZ,@G_ZTH,@G_ZTYY,@G_ZTZD,@G_ZTZZ,@G_SFTZ,@G_RXY,@G_RYJ,@G_ZYD,@G_YDSJ,@G_YS,@G_XLTZ,@G_ZYXW,@G_YMJZ,@G_GXBYF,@G_GZSSYF,@下次随访目标,@下次随访日期,@随访医生,@创建机构,@创建人,@更新人,@所属机构,@创建时间,@更新时间,@随访日期,@缺项,@进餐评分,@梳洗评分,@如厕评分,@活动评分,@总评分,@穿衣评分,@随访次数,@完整度,'',@_BlueAddr,@_BlueType,@_BlueName)";
            int rst = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sql, new object[] { new SQLiteParameter("@身份证号", _身份证号), new SQLiteParameter("@姓名", _姓名), new SQLiteParameter("@性别", _性别), new SQLiteParameter("@出生日期", _出生日期), new SQLiteParameter("@居住地址", _居住地址), new SQLiteParameter("@个人档案编号", _个人档案编号), new SQLiteParameter("@G_ZZBS", _G_ZZBS), new SQLiteParameter("@G_ZZXZZ", _G_ZZXZZ), new SQLiteParameter("@G_ZZCX", _G_ZZCX), new SQLiteParameter("@G_ZZZZ", _G_ZZZZ), new SQLiteParameter("@G_ZTH", _G_ZTH), new SQLiteParameter("@G_ZTYY", _G_ZTYY), new SQLiteParameter("@G_ZTZD", _G_ZTZD), new SQLiteParameter("@G_ZTZZ", _G_ZTZZ), new SQLiteParameter("@G_SFTZ", _G_SFTZ), new SQLiteParameter("@G_RXY", _G_RXY), new SQLiteParameter("@G_RYJ", _G_RYJ), new SQLiteParameter("@G_ZYD", _G_ZYD), new SQLiteParameter("@G_YDSJ", _G_YDSJ), new SQLiteParameter("@G_YS", _G_YS), new SQLiteParameter("@G_XLTZ", _G_XLTZ), new SQLiteParameter("@G_ZYXW", _G_ZYXW), new SQLiteParameter("@G_YMJZ", _G_YMJZ), new SQLiteParameter("@G_GXBYF", _G_GXBYF), new SQLiteParameter("@G_GZSSYF", _G_GZSSYF), new SQLiteParameter("@下次随访目标", _下次随访目标), new SQLiteParameter("@下次随访日期", _下次随访日期), new SQLiteParameter("@随访医生", _随访医生), new SQLiteParameter("@创建机构", _创建机构), new SQLiteParameter("@创建人", _创建人), new SQLiteParameter("@更新人", _更新人), new SQLiteParameter("@所属机构", _所属机构), new SQLiteParameter("@创建时间", _创建时间), new SQLiteParameter("@更新时间", _更新时间), new SQLiteParameter("@随访日期", _随访日期), new SQLiteParameter("@缺项", _缺项), new SQLiteParameter("@进餐评分", _进餐评分), new SQLiteParameter("@梳洗评分", _梳洗评分), new SQLiteParameter("@如厕评分", _如厕评分), new SQLiteParameter("@活动评分", _活动评分), new SQLiteParameter("@总评分", _总评分), new SQLiteParameter("@穿衣评分", _穿衣评分), new SQLiteParameter("@随访次数", _随访次数), new SQLiteParameter("@完整度", _完整度), new SQLiteParameter("@_BlueAddr", _BlueAddr), new SQLiteParameter("@_BlueType", _BlueType), new SQLiteParameter("@_BlueName", _BlueName) });
            // int rst = DBManager.ExecuteUpdate(sql, new object[] { new SQLiteParameter("@_身份证号", _身份证号), new SQLiteParameter("@_姓名", _姓名), new SQLiteParameter("@_性别", _性别), new SQLiteParameter("@_出生日期", _出生日期), new SQLiteParameter("@_居住地址", _居住地址), new SQLiteParameter("@_个人档案编号", _个人档案编号), new SQLiteParameter("@_G_ZZBS", _G_ZZBS), new SQLiteParameter("@_G_ZZXZZ", _G_ZZXZZ), new SQLiteParameter("@_G_ZZCX", _G_ZZCX), new SQLiteParameter("@_G_ZZZZ", _G_ZZZZ), new SQLiteParameter("@_G_ZTH", _G_ZTH), new SQLiteParameter("@_G_ZTYY", _G_ZTYY), new SQLiteParameter("@_G_ZTZD", _G_ZTZD), new SQLiteParameter("@_G_ZTZZ", _G_ZTZZ), new SQLiteParameter("@_G_SFTZ", _G_SFTZ), new SQLiteParameter("@_G_RXY", _G_RXY), new SQLiteParameter("@_G_RYJ", _G_RYJ), new SQLiteParameter("@_G_ZYD", _G_ZYD), new SQLiteParameter("@_G_YDSJ", _G_YDSJ), new SQLiteParameter("@_G_YS", _G_YS), new SQLiteParameter("@_G_XLTZ", _G_XLTZ), new SQLiteParameter("@_G_ZYXW", _G_ZYXW), new SQLiteParameter("@_G_YMJZ", _G_YMJZ), new SQLiteParameter("@_G_GXBYF", _G_GXBYF), new SQLiteParameter("@_G_GZSSYF", _G_GZSSYF), new SQLiteParameter("@_下次随访目标", _下次随访目标), new SQLiteParameter("@_下次随访日期", _下次随访日期), new SQLiteParameter("@_随访医生", _随访医生), new SQLiteParameter("@_创建机构", _创建机构), new SQLiteParameter("@_创建人", _创建人), new SQLiteParameter("@_更新人", _更新人), new SQLiteParameter("@_所属机构", _所属机构), new SQLiteParameter("@_创建时间", _创建时间), new SQLiteParameter("@_更新时间", _更新时间), new SQLiteParameter("@_随访日期", _随访日期), new SQLiteParameter("@_缺项", _缺项), new SQLiteParameter("@_进餐评分", _进餐评分), new SQLiteParameter("@_梳洗评分", _梳洗评分), new SQLiteParameter("@_如厕评分", _如厕评分), new SQLiteParameter("@_活动评分", _活动评分), new SQLiteParameter("@_总评分", _总评分), new SQLiteParameter("@_穿衣评分", _穿衣评分), new SQLiteParameter("@_随访次数", _随访次数), new SQLiteParameter("@_完整度", _完整度) });
            if (rst != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除一个对象
        /// </summary>
        /// <param name="ID">主键</param>
        /// <returns></returns>
        public static bool DeleteInfo(tb_老年人生活自理能力评价Info info)
        {
            bool rst = false;
            string sqlStr = "DELETE FROM tb_老年人生活自理能力评价 WHERE ID=" + info.ID;
            int rows = DBManager.ExecuteUpdate(DBManager.Conn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }

        /// 更新数据
        /// </summary>
        /// <param name="info">数据表实体</param>
        public static bool UpdateInfo(tb_老年人生活自理能力评价Info info)
        {
            string _身份证号 = info.身份证号;
            string _姓名 = info.姓名;
            string _性别 = info.性别;
            string _出生日期 = info.出生日期;
            string _居住地址 = info.居住地址;
            string _个人档案编号 = info.个人档案编号;
            string _G_ZZBS = info.G_ZZBS;
            string _G_ZZXZZ = info.G_ZZXZZ;
            string _G_ZZCX = info.G_ZZCX;
            string _G_ZZZZ = info.G_ZZZZ;
            string _G_ZTH = info.G_ZTH;
            string _G_ZTYY = info.G_ZTYY;
            string _G_ZTZD = info.G_ZTZD;
            string _G_ZTZZ = info.G_ZTZZ;
            decimal _G_SFTZ = info.G_SFTZ;
            int _G_RXY = info.G_RXY;
            decimal _G_RYJ = info.G_RYJ;
            int _G_ZYD = info.G_ZYD;
            decimal _G_YDSJ = info.G_YDSJ;
            string _G_YS = info.G_YS;
            string _G_XLTZ = info.G_XLTZ;
            string _G_ZYXW = info.G_ZYXW;
            string _G_YMJZ = info.G_YMJZ;
            string _G_GXBYF = info.G_GXBYF;
            string _G_GZSSYF = info.G_GZSSYF;
            string _下次随访目标 = info.下次随访目标;
            string _下次随访日期 = info.下次随访日期;
            string _随访医生 = info.随访医生;
            string _创建机构 = info.创建机构;
            string _创建人 = info.创建人;
            string _更新人 = info.更新人;
            string _所属机构 = info.所属机构;
            string _创建时间 = info.创建时间;
            string _更新时间 = info.更新时间;
            string _随访日期 = info.随访日期;
            string _缺项 = info.缺项;
            string _进餐评分 = info.进餐评分;
            string _梳洗评分 = info.梳洗评分;
            string _如厕评分 = info.如厕评分;
            string _活动评分 = info.活动评分;
            string _总评分 = info.总评分;
            string _穿衣评分 = info.穿衣评分;
            string _随访次数 = info.随访次数;
            string _完整度 = info.完整度;
            string sql = "UPDATE tb_老年人生活自理能力评价 SET 身份证号=@_身份证号, 姓名=@_姓名, 性别=@_性别, 出生日期=@_出生日期, 居住地址=@_居住地址, 个人档案编号=@_个人档案编号, G_ZZBS=@_G_ZZBS, G_ZZXZZ=@_G_ZZXZZ, G_ZZCX=@_G_ZZCX, G_ZZZZ=@_G_ZZZZ, G_ZTH=@_G_ZTH, G_ZTYY=@_G_ZTYY, G_ZTZD=@_G_ZTZD, G_ZTZZ=@_G_ZTZZ, G_SFTZ=@_G_SFTZ, G_RXY=@_G_RXY, G_RYJ=@_G_RYJ, G_ZYD=@_G_ZYD, G_YDSJ=@_G_YDSJ, G_YS=@_G_YS, G_XLTZ=@_G_XLTZ, G_ZYXW=@_G_ZYXW, G_YMJZ=@_G_YMJZ, G_GXBYF=@_G_GXBYF, G_GZSSYF=@_G_GZSSYF, 下次随访目标=@_下次随访目标, 下次随访日期=@_下次随访日期, 随访医生=@_随访医生, 创建机构=@_创建机构, 创建人=@_创建人, 更新人=@_更新人, 所属机构=@_所属机构, 创建时间=@_创建时间, 更新时间=@_更新时间, 随访日期=@_随访日期, 缺项=@_缺项, 进餐评分=@_进餐评分, 梳洗评分=@_梳洗评分, 如厕评分=@_如厕评分, 活动评分=@_活动评分, 总评分=@_总评分, 穿衣评分=@_穿衣评分, 随访次数=@_随访次数, 完整度=@_完整度  WHERE 身份证号='" + info.身份证号 + "' and substr(创建时间,0,10) = substr('" + Program._currentTime + "',0,10)";
            int rst = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sql, new object[] { new SQLiteParameter("@_身份证号", _身份证号), new SQLiteParameter("@_姓名", _姓名), new SQLiteParameter("@_性别", _性别), new SQLiteParameter("@_出生日期", _出生日期), new SQLiteParameter("@_居住地址", _居住地址), new SQLiteParameter("@_个人档案编号", _个人档案编号), new SQLiteParameter("@_G_ZZBS", _G_ZZBS), new SQLiteParameter("@_G_ZZXZZ", _G_ZZXZZ), new SQLiteParameter("@_G_ZZCX", _G_ZZCX), new SQLiteParameter("@_G_ZZZZ", _G_ZZZZ), new SQLiteParameter("@_G_ZTH", _G_ZTH), new SQLiteParameter("@_G_ZTYY", _G_ZTYY), new SQLiteParameter("@_G_ZTZD", _G_ZTZD), new SQLiteParameter("@_G_ZTZZ", _G_ZTZZ), new SQLiteParameter("@_G_SFTZ", _G_SFTZ), new SQLiteParameter("@_G_RXY", _G_RXY), new SQLiteParameter("@_G_RYJ", _G_RYJ), new SQLiteParameter("@_G_ZYD", _G_ZYD), new SQLiteParameter("@_G_YDSJ", _G_YDSJ), new SQLiteParameter("@_G_YS", _G_YS), new SQLiteParameter("@_G_XLTZ", _G_XLTZ), new SQLiteParameter("@_G_ZYXW", _G_ZYXW), new SQLiteParameter("@_G_YMJZ", _G_YMJZ), new SQLiteParameter("@_G_GXBYF", _G_GXBYF), new SQLiteParameter("@_G_GZSSYF", _G_GZSSYF), new SQLiteParameter("@_下次随访目标", _下次随访目标), new SQLiteParameter("@_下次随访日期", _下次随访日期), new SQLiteParameter("@_随访医生", _随访医生), new SQLiteParameter("@_创建机构", _创建机构), new SQLiteParameter("@_创建人", _创建人), new SQLiteParameter("@_更新人", _更新人), new SQLiteParameter("@_所属机构", _所属机构), new SQLiteParameter("@_创建时间", _创建时间), new SQLiteParameter("@_更新时间", _更新时间), new SQLiteParameter("@_随访日期", _随访日期), new SQLiteParameter("@_缺项", _缺项), new SQLiteParameter("@_进餐评分", _进餐评分), new SQLiteParameter("@_梳洗评分", _梳洗评分), new SQLiteParameter("@_如厕评分", _如厕评分), new SQLiteParameter("@_活动评分", _活动评分), new SQLiteParameter("@_总评分", _总评分), new SQLiteParameter("@_穿衣评分", _穿衣评分), new SQLiteParameter("@_随访次数", _随访次数), new SQLiteParameter("@_完整度", _完整度) });
            if (rst > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool importtb_老年人中医药特征管理(string spName, tb_老年人生活自理能力评价Info info)
        {
            string _身份证号 = info.身份证号;
            string _姓名 = info.姓名;
            string _性别 = info.性别;
            string _出生日期 = info.出生日期;
            string _居住地址 = info.居住地址;
            string _个人档案编号 = info.个人档案编号;
            string _G_ZZBS = info.G_ZZBS;
            string _G_ZZXZZ = info.G_ZZXZZ;
            string _G_ZZCX = info.G_ZZCX;
            string _G_ZZZZ = info.G_ZZZZ;
            string _G_ZTH = info.G_ZTH;
            string _G_ZTYY = info.G_ZTYY;
            string _G_ZTZD = info.G_ZTZD;
            string _G_ZTZZ = info.G_ZTZZ;
            decimal _G_SFTZ = info.G_SFTZ;
            int _G_RXY = info.G_RXY;
            decimal _G_RYJ = info.G_RYJ;
            int _G_ZYD = info.G_ZYD;
            decimal _G_YDSJ = info.G_YDSJ;
            string _G_YS = info.G_YS;
            string _G_XLTZ = info.G_XLTZ;
            string _G_ZYXW = info.G_ZYXW;
            string _G_YMJZ = info.G_YMJZ;
            string _G_GXBYF = info.G_GXBYF;
            string _G_GZSSYF = info.G_GZSSYF;
            string _下次随访目标 = info.下次随访目标;
            string _下次随访日期 = info.下次随访日期;
            string _随访医生 = info.随访医生;
            string _创建机构 = info.创建机构;
            string _创建人 = info.创建人;
            string _更新人 = info.更新人;
            string _所属机构 = info.所属机构;
            string _创建时间 = info.创建时间;
            string _更新时间 = info.更新时间;
            string _随访日期 = info.随访日期;
            string _缺项 = info.缺项;
            string _进餐评分 = info.进餐评分;
            string _梳洗评分 = info.梳洗评分;
            string _如厕评分 = info.如厕评分;
            string _活动评分 = info.活动评分;
            string _总评分 = info.总评分;
            string _穿衣评分 = info.穿衣评分;
            string _随访次数 = info.随访次数;
            string _完整度 = info.完整度;
            string _BlueName = info.BlueName;
            string _BlueAddr = info.BlueAddr;
            string _BlueType = info.BlueType;

            //string sql = "INSERT INTO  VALUES (@_身份证号,@_姓名,@_性别,@_出生日期,@_居住地址,@_个人档案编号,@_G_ZZBS,@_G_ZZXZZ,@_G_ZZCX,@_G_ZZZZ,@_G_ZTH,@_G_ZTYY,@_G_ZTZD,@_G_ZTZZ,@_G_SFTZ,@_G_RXY,@_G_RYJ,@_G_ZYD,@_G_YDSJ,@_G_YS,@_G_XLTZ,@_G_ZYXW,@_G_YMJZ,@_G_GXBYF,@_G_GZSSYF,@_下次随访目标,@_下次随访日期,@_随访医生,@_创建机构,@_创建人,@_更新人,@_所属机构,@_创建时间,@_更新时间,@_随访日期,@_缺项,@_进餐评分,@_梳洗评分,@_如厕评分,@_活动评分,@_总评分,@_穿衣评分,@_随访次数,@_完整度)";
            int rst = DBManager.ExecuteProc(DBManager.Conn121, spName, new object[] { new SqlParameter("@身份证号", _身份证号), new SqlParameter("@姓名", _姓名), new SqlParameter("@性别", _性别), new SqlParameter("@出生日期", _出生日期), new SqlParameter("@居住地址", _居住地址), new SqlParameter("@个人档案编号", _个人档案编号), new SqlParameter("@G_ZZBS", _G_ZZBS), new SqlParameter("@G_ZZXZZ", _G_ZZXZZ), new SqlParameter("@G_ZZCX", _G_ZZCX), new SqlParameter("@G_ZZZZ", _G_ZZZZ), new SqlParameter("@G_ZTH", _G_ZTH), new SqlParameter("@G_ZTYY", _G_ZTYY), new SqlParameter("@G_ZTZD", _G_ZTZD), new SqlParameter("@G_ZTZZ", _G_ZTZZ), new SqlParameter("@G_SFTZ", _G_SFTZ), new SqlParameter("@G_RXY", _G_RXY), new SqlParameter("@G_RYJ", _G_RYJ), new SqlParameter("@G_ZYD", _G_ZYD), new SqlParameter("@G_YDSJ", _G_YDSJ), new SqlParameter("@G_YS", _G_YS), new SqlParameter("@G_XLTZ", _G_XLTZ), new SqlParameter("@G_ZYXW", _G_ZYXW), new SqlParameter("@G_YMJZ", _G_YMJZ), new SqlParameter("@G_GXBYF", _G_GXBYF), new SqlParameter("@G_GZSSYF", _G_GZSSYF), new SqlParameter("@下次随访目标", _下次随访目标), new SqlParameter("@下次随访日期", _下次随访日期), new SqlParameter("@随访医生", _随访医生), new SqlParameter("@创建机构", _创建机构), new SqlParameter("@创建人", _创建人), new SqlParameter("@更新人", _更新人), new SqlParameter("@所属机构", _所属机构), new SqlParameter("@创建时间", _创建时间), new SqlParameter("@更新时间", _更新时间), new SqlParameter("@随访日期", _随访日期), new SqlParameter("@缺项", _缺项), new SqlParameter("@进餐评分", _进餐评分), new SqlParameter("@梳洗评分", _梳洗评分), new SqlParameter("@如厕评分", _如厕评分), new SqlParameter("@活动评分", _活动评分), new SqlParameter("@总评分", _总评分), new SqlParameter("@穿衣评分", _穿衣评分), new SqlParameter("@随访次数", _随访次数), new SqlParameter("@完整度", _完整度), new SqlParameter("@BlueName", _BlueName), new SqlParameter("@BlueAddr", _BlueAddr), new SqlParameter("@BlueType", _BlueType) });
            // int rst = DBManager.ExecuteUpdate(sql, new object[] { new SqlParameter("@_身份证号", _身份证号), new SqlParameter("@_姓名", _姓名), new SqlParameter("@_性别", _性别), new SqlParameter("@_出生日期", _出生日期), new SqlParameter("@_居住地址", _居住地址), new SqlParameter("@_个人档案编号", _个人档案编号), new SqlParameter("@_G_ZZBS", _G_ZZBS), new SqlParameter("@_G_ZZXZZ", _G_ZZXZZ), new SqlParameter("@_G_ZZCX", _G_ZZCX), new SqlParameter("@_G_ZZZZ", _G_ZZZZ), new SqlParameter("@_G_ZTH", _G_ZTH), new SqlParameter("@_G_ZTYY", _G_ZTYY), new SqlParameter("@_G_ZTZD", _G_ZTZD), new SqlParameter("@_G_ZTZZ", _G_ZTZZ), new SqlParameter("@_G_SFTZ", _G_SFTZ), new SqlParameter("@_G_RXY", _G_RXY), new SqlParameter("@_G_RYJ", _G_RYJ), new SqlParameter("@_G_ZYD", _G_ZYD), new SqlParameter("@_G_YDSJ", _G_YDSJ), new SqlParameter("@_G_YS", _G_YS), new SqlParameter("@_G_XLTZ", _G_XLTZ), new SqlParameter("@_G_ZYXW", _G_ZYXW), new SqlParameter("@_G_YMJZ", _G_YMJZ), new SqlParameter("@_G_GXBYF", _G_GXBYF), new SqlParameter("@_G_GZSSYF", _G_GZSSYF), new SqlParameter("@_下次随访目标", _下次随访目标), new SqlParameter("@_下次随访日期", _下次随访日期), new SqlParameter("@_随访医生", _随访医生), new SqlParameter("@_创建机构", _创建机构), new SqlParameter("@_创建人", _创建人), new SqlParameter("@_更新人", _更新人), new SqlParameter("@_所属机构", _所属机构), new SqlParameter("@_创建时间", _创建时间), new SqlParameter("@_更新时间", _更新时间), new SqlParameter("@_随访日期", _随访日期), new SqlParameter("@_缺项", _缺项), new SqlParameter("@_进餐评分", _进餐评分), new SqlParameter("@_梳洗评分", _梳洗评分), new SqlParameter("@_如厕评分", _如厕评分), new SqlParameter("@_活动评分", _活动评分), new SqlParameter("@_总评分", _总评分), new SqlParameter("@_穿衣评分", _穿衣评分), new SqlParameter("@_随访次数", _随访次数), new SqlParameter("@_完整度", _完整度) });
            if (rst != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal static bool UpdateRowState(tb_老年人生活自理能力评价Info info)
        {
            bool rst = false;
            string sqlStr = "update tb_老年人生活自理能力评价 set rowstate = '1' WHERE ID=" + info.ID;
            int rows = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }

        internal static bool UpdateSys_DocNoPrgid(tb_老年人生活自理能力评价Info info)
        {
            bool rst = false;
            string sqlStr = "update tb_老年人生活自理能力评价 set 个人档案编号=@DocNO,所属机构=@Prgid,创建机构=@Prgid WHERE ID=" + info.ID;
            int rows = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sqlStr, new object[] { new SQLiteParameter("@DocNO", info.个人档案编号), new SQLiteParameter("@Prgid", info.所属机构) });
            //int  = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }
    }
}
