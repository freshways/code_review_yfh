﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Linq;
using System.Text;

namespace ATOMEHR_LeaveClient
{
    public class tb_非免疫规划DAL
    {
        /// <summary>
        /// 获取所有信息
        /// </summary>
        /// <param name="where">条件</param>
        /// <returns>结果集</returns>
        public static SQLiteDataReader GetInfo(string where)
        {
            string sqlStr = "SELECT * FROM tb_健康体检_非免疫规划预防接种史 where ";
            if (String.IsNullOrEmpty(where))
            {
                sqlStr += "1=1";
            }
            else
            {
                sqlStr += where;
            }
            SQLiteDataReader reader = null;
            try
            {
                reader = DBManager.ExecuteQueryForSQLite(sqlStr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reader;
        }

        /// <summary>
        /// 获取所有信息集合
        /// </summary>
        /// <returns>List集合</returns>
        public static List<tb_健康体检_非免疫规划预防接种史Info> GetInfoList(string where)
        {
            List<tb_健康体检_非免疫规划预防接种史Info> infoList = new List<tb_健康体检_非免疫规划预防接种史Info>();

            SQLiteDataReader reader = null;
            try
            {
                reader = GetInfo(where);
            }
            catch (Exception)
            {
                throw;
            }

            while (reader.Read())
            {
                tb_健康体检_非免疫规划预防接种史Info info = new tb_健康体检_非免疫规划预防接种史Info();
                info.ID = reader["ID"].ToString();
                info.身份证号 = reader["身份证号"].ToString();
                info.个人档案编号 = reader["个人档案编号"].ToString();
                info.接种名称 = reader["接种名称"].ToString();
                info.接种日期 = reader["接种日期"].ToString();
                info.接种机构 = reader["接种机构"].ToString();
                info.创建日期 = reader["创建日期"].ToString();
                info.BlueName = reader["BlueName"].ToString();
                info.BlueAddr = reader["BlueAddr"].ToString();
                info.BlueType = reader["BlueType"].ToString();
                infoList.Add(info);
            }
            reader.Close();
            return infoList;
        }

        /// <summary>
        /// 根据 主键 获取一个实体对象
        /// <param name="ID">主键</param>
        /// </summary>
        public static tb_健康体检_非免疫规划预防接种史Info GetInfoById(string ID)
        {
            string strWhere = "ID =" + ID;
            List<tb_健康体检_非免疫规划预防接种史Info> list = GetInfoList(strWhere);
            if (list.Count > 0)
                return list[0];
            return null;
        }

        /// 添加数据
        /// </summary>
        /// <param name="info">数据表实体对象</param>
        public static bool Add(tb_健康体检_非免疫规划预防接种史Info info)
        {
            string _身份证号 = Program.currentUser.ID;
            string _个人档案编号 = Program.currentUser.DocNo;
            string _接种名称 = info.接种名称;
            string _接种日期 = info.接种日期;
            string _接种机构 = info.接种机构;
            string _创建日期 = Program._currentTime;
            string _BlueName = Program.BlueName;
            string _BlueAddr = Program.BlueAddr;
            string _BlueType = Program.BlueType;

            string sql = "INSERT INTO tb_健康体检_非免疫规划预防接种史 VALUES (NULL,@_身份证号,@_个人档案编号,@_接种名称,@_接种日期,@_接种机构,@_创建日期,'',@BlueAddr,@BlueType,@BlueName)";
            //int rst = DBManager.ExecuteProc("SP_SAVE非免疫规划预防接种史", new object[] { new SqlParameter("@_ID", info.ID), new SqlParameter("@_身份证号", _身份证号), new SqlParameter("@_个人档案编号", _个人档案编号), new SqlParameter("@_接种名称", _接种名称), new SqlParameter("@_接种日期", _接种日期), new SqlParameter("@_接种机构", _接种机构), new SqlParameter("@_创建日期", _创建日期) });
            int rst = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sql, new object[] { new SQLiteParameter("@_身份证号", _身份证号), new SQLiteParameter("@_个人档案编号", _个人档案编号), new SQLiteParameter("@_接种名称", _接种名称), new SQLiteParameter("@_接种日期", _接种日期), new SQLiteParameter("@_接种机构", _接种机构), new SQLiteParameter("@_创建日期", _创建日期), new SQLiteParameter("@BlueAddr", _BlueAddr), new SQLiteParameter("@BlueType", _BlueType), new SQLiteParameter("@BlueName", _BlueName) });
            if (rst > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除一个对象
        /// </summary>
        /// <param name="ID">主键</param>
        /// <returns></returns>
        public static bool DeleteInfo(tb_健康体检_非免疫规划预防接种史Info info)
        {
            bool rst = false;
            string sqlStr = "DELETE FROM tb_健康体检_非免疫规划预防接种史 WHERE ID=" + info.ID;
            int rows = DBManager.ExecuteUpdate(DBManager.Conn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }
        /// <summary>
        /// 通过身份证号进行删除
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool DeleteInfo(string key)
        {
            bool rst = false;
            string sqlStr = "DELETE FROM tb_健康体检_非免疫规划预防接种史 WHERE 身份证号='" + key + "'";
            int rows = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }
        /// 更新数据
        /// </summary>
        /// <param name="info">数据表实体</param>
        public static bool UpdateInfo(tb_健康体检_非免疫规划预防接种史Info info)
        {
            string _身份证号 = info.身份证号;
            string _个人档案编号 = info.个人档案编号;
            string _接种名称 = info.接种名称;
            string _接种日期 = info.接种日期;
            string _接种机构 = info.接种机构;
            string _创建日期 = info.创建日期;
            string sql = "UPDATE tb_健康体检_非免疫规划预防接种史 SET 身份证号=@_身份证号, 个人档案编号=@_个人档案编号, 接种名称=@_接种名称, 接种日期=@_接种日期, 接种机构=@_接种机构, 创建日期=@_创建日期, BlueAddr=@_BlueAddr, BlueName=@_BlueName, BlueType=@_BlueType  WHERE ID=" + info.ID;
            int rst = DBManager.ExecuteUpdate(DBManager.Conn, sql, new object[] { new SqlParameter("@_身份证号", _身份证号), new SqlParameter("@_个人档案编号", _个人档案编号), new SqlParameter("@_接种名称", _接种名称), new SqlParameter("@_接种日期", _接种日期), new SqlParameter("@_接种机构", _接种机构), new SqlParameter("@_创建日期", _创建日期), new SqlParameter("@_BlueAddr", Program.BlueAddr), new SqlParameter("@_BlueName", Program.BlueName), new SqlParameter("@_BlueType", Program.BlueType) });
            if (rst > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool AddTo121(tb_健康体检_非免疫规划预防接种史Info info)
        {
            string _身份证号 = info.身份证号;
            string _个人档案编号 = info.个人档案编号;
            string _接种名称 = info.接种名称;
            string _接种日期 = info.接种日期;
            string _接种机构 = info.接种机构;
            string _创建日期 = info.创建日期;
            string _BlueName = info.BlueName;
            string _BlueAddr = info.BlueAddr;
            string _BlueType = info.BlueType;

            int rst = DBManager.ExecuteProc(DBManager.Conn121, "USP_IMPORT非免疫规划预防接种史", new object[] { new SqlParameter("@_身份证号", _身份证号), new SqlParameter("@_个人档案编号", _个人档案编号), new SqlParameter("@_接种名称", _接种名称), new SqlParameter("@_接种日期", _接种日期), new SqlParameter("@_接种机构", _接种机构), new SqlParameter("@_创建日期", _创建日期), new SqlParameter("@_BlueName", _BlueName), new SqlParameter("@_BlueAddr", _BlueAddr), new SqlParameter("@_BlueType", _BlueType) });
            if (rst > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
