using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.SQLite;
using ATOMEHR_LeaveClient.SqlSugar_DALs;
using ATOMEHR_LeaveClient.SqlSugar_Models;
using ATOMEHR_LeaveClient.Util;
using Newtonsoft.Json;

namespace ATOMEHR_LeaveClient
{
    /// <summary>
    /// tb_健康体检DAL类
    /// BY：狂人代码生成器 V1.0
    /// 作者：金属狂人
    /// 日期：2016年04月12日 03:22:54
    /// </summary>
    public class tb_健康体检DAL
    {
        public enum Module_体检
        {
            症状,
            身高体重,
            血压,
            生活方式,
            脏器功能,
            查体,
            辅助检查,
            心电图,
            胸部X线片,
            B超,
            现存主要健康问题,
            主要用药情况,
            非免疫规划预防接种史,
            健康评价,
            健康指导,
            结果反馈,
        }

        /// <summary>
        /// 获取所有信息
        /// </summary>
        /// <param name="where">条件</param>
        /// <returns>结果集</returns>
        public static SQLiteDataReader Gettb_健康体检Info(string where)
        {
            string sqlStr = "SELECT * FROM tb_健康体检 where ";
            if (String.IsNullOrEmpty(where))
            {
                sqlStr += "1=1";
            }
            else
            {
                sqlStr += where;
            }
            SQLiteDataReader reader = null;
            try
            {
                reader = DBManager.ExecuteQueryForSQLite(sqlStr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reader;
        }

        /// <summary>
        /// 获取所有信息集合
        /// </summary>
        /// <returns>List集合</returns>
        public static List<tb_健康体检Info> Gettb_健康体检InfoList(string where)
        {
            List<tb_健康体检Info> infoList = new List<tb_健康体检Info>();

            SQLiteDataReader reader = null;
            try
            {
                reader = Gettb_健康体检Info(where);
            }
            catch (Exception)
            {
                throw;
            }

            while (reader.Read())
            {
                tb_健康体检Info info = new tb_健康体检Info();
                info.ID = reader["ID"].ToString();
                info.身份证号 = reader["身份证号"].ToString();
                info.姓名 = reader["姓名"].ToString();
                info.性别 = reader["性别"].ToString();
                info.出生日期 = reader["出生日期"].ToString();
                info.症状 = reader["症状"].ToString();
                info.体温 = string.IsNullOrEmpty(reader["体温"].ToString()) ? 0 : Convert.ToDouble(reader["体温"].ToString());
                info.呼吸 = string.IsNullOrEmpty(reader["呼吸"].ToString()) ? 0 : Convert.ToInt32(reader["呼吸"].ToString());
                info.脉搏 = string.IsNullOrEmpty(reader["脉搏"].ToString()) ? 0 : Convert.ToInt32(reader["脉搏"].ToString());
                info.血压右侧1 = reader["血压右侧1"].ToString();
                info.血压右侧2 = reader["血压右侧2"].ToString();
                info.血压左侧1 = reader["血压左侧1"].ToString();
                info.血压左侧2 = reader["血压左侧2"].ToString();
                info.身高 = string.IsNullOrEmpty(reader["身高"].ToString()) ? 0 : Convert.ToDouble(reader["身高"].ToString());
                info.腰围 = string.IsNullOrEmpty(reader["腰围"].ToString()) ? 0 : Convert.ToDouble(reader["腰围"].ToString());
                info.体重 = string.IsNullOrEmpty(reader["体重"].ToString()) ? 0 : Convert.ToDouble(reader["体重"].ToString());
                info.体重指数 = reader["体重指数"].ToString();
                info.老年人认知 = reader["老年人认知"].ToString();
                info.老年人情感 = reader["老年人情感"].ToString();
                info.老年人认知分 = reader["老年人认知分"].ToString();
                info.老年人情感分 = reader["老年人情感分"].ToString();
                info.左眼视力 = string.IsNullOrEmpty(reader["左眼视力"].ToString()) ? 0 : Convert.ToDouble(reader["左眼视力"].ToString());
                info.右眼视力 = string.IsNullOrEmpty(reader["右眼视力"].ToString()) ? 0 : Convert.ToDouble(reader["右眼视力"].ToString());
                info.左眼矫正 = reader["左眼矫正"].ToString();
                info.右眼矫正 = reader["右眼矫正"].ToString();
                info.听力 = reader["听力"].ToString();
                info.运动功能 = reader["运动功能"].ToString();
                info.皮肤 = reader["皮肤"].ToString();
                info.淋巴结 = reader["淋巴结"].ToString();
                info.淋巴结其他 = reader["淋巴结其他"].ToString();
                info.桶状胸 = reader["桶状胸"].ToString();
                info.呼吸音 = reader["呼吸音"].ToString();
                info.呼吸音异常 = reader["呼吸音异常"].ToString();
                info.罗音 = reader["罗音"].ToString();
                info.罗音异常 = reader["罗音异常"].ToString();
                info.心率 = string.IsNullOrEmpty(reader["心率"].ToString()) ? 0 : Convert.ToInt32(reader["心率"].ToString());
                info.心律 = reader["心律"].ToString();
                info.杂音 = reader["杂音"].ToString();
                info.杂音有 = reader["杂音有"].ToString();
                info.压痛 = reader["压痛"].ToString();
                info.压痛有 = reader["压痛有"].ToString();
                info.包块 = reader["包块"].ToString();
                info.包块有 = reader["包块有"].ToString();
                info.肝大 = reader["肝大"].ToString();
                info.肝大有 = reader["肝大有"].ToString();
                info.脾大 = reader["脾大"].ToString();
                info.脾大有 = reader["脾大有"].ToString();
                info.浊音 = reader["浊音"].ToString();
                info.浊音有 = reader["浊音有"].ToString();
                info.下肢水肿 = reader["下肢水肿"].ToString();
                info.肛门指诊 = reader["肛门指诊"].ToString();
                info.肛门指诊异常 = reader["肛门指诊异常"].ToString();
                info.G_QLX = reader["G_QLX"].ToString();
                info.查体其他 = reader["查体其他"].ToString();
                info.白细胞 = reader["白细胞"].ToString();
                info.血红蛋白 = reader["血红蛋白"].ToString();
                info.血小板 = reader["血小板"].ToString();
                info.血常规其他 = reader["血常规其他"].ToString();
                info.尿蛋白 = reader["尿蛋白"].ToString();
                info.尿糖 = reader["尿糖"].ToString();
                info.尿酮体 = reader["尿酮体"].ToString();
                info.尿潜血 = reader["尿潜血"].ToString();
                info.尿常规其他 = reader["尿常规其他"].ToString();
                info.大便潜血 = reader["大便潜血"].ToString();
                info.血清谷丙转氨酶 = reader["血清谷丙转氨酶"].ToString();
                info.血清谷草转氨酶 = reader["血清谷草转氨酶"].ToString();
                info.白蛋白 = reader["白蛋白"].ToString();
                info.总胆红素 = reader["总胆红素"].ToString();
                info.结合胆红素 = reader["结合胆红素"].ToString();
                info.血清肌酐 = reader["血清肌酐"].ToString();
                info.血尿素氮 = reader["血尿素氮"].ToString();
                info.总胆固醇 = reader["总胆固醇"].ToString();
                info.甘油三酯 = reader["甘油三酯"].ToString();
                info.血清低密度脂蛋白胆固醇 = reader["血清低密度脂蛋白胆固醇"].ToString();
                info.血清高密度脂蛋白胆固醇 = reader["血清高密度脂蛋白胆固醇"].ToString();
                info.空腹血糖 = reader["空腹血糖"].ToString();
                info.乙型肝炎表面抗原 = reader["乙型肝炎表面抗原"].ToString();
                info.眼底 = reader["眼底"].ToString();
                info.眼底异常 = reader["眼底异常"].ToString();
                info.心电图 = reader["心电图"].ToString();
                info.心电图异常 = reader["心电图异常"].ToString();
                info.胸部X线片 = reader["胸部X线片"].ToString();
                info.胸部X线片异常 = reader["胸部X线片异常"].ToString();
                info.B超 = reader["B超"].ToString();
                info.B超其他 = reader["B超其他"].ToString();
                info.辅助检查其他 = reader["辅助检查其他"].ToString();
                info.G_JLJJY = reader["G_JLJJY"].ToString();
                info.个人档案编号 = reader["个人档案编号"].ToString();
                info.创建机构 = reader["创建机构"].ToString();
                info.创建时间 = reader["创建时间"].ToString();
                info.创建人 = reader["创建人"].ToString();
                info.修改时间 = reader["修改时间"].ToString();
                info.修改人 = reader["修改人"].ToString();
                info.体检日期 = reader["体检日期"].ToString();
                info.所属机构 = reader["所属机构"].ToString();
                info.FIELD1 = reader["FIELD1"].ToString();
                info.FIELD2 = reader["FIELD2"].ToString();
                info.FIELD3 = reader["FIELD3"].ToString();
                info.FIELD4 = reader["FIELD4"].ToString();
                info.WBC_SUP = string.IsNullOrEmpty(reader["WBC_SUP"].ToString()) ? 0 : Convert.ToInt32(reader["WBC_SUP"].ToString());
                info.PLT_SUP = string.IsNullOrEmpty(reader["PLT_SUP"].ToString()) ? 0 : Convert.ToInt32(reader["PLT_SUP"].ToString());
                info.G_TUNWEI = reader["G_TUNWEI"].ToString();
                info.G_YTWBZ = reader["G_YTWBZ"].ToString();
                info.锻炼频率 = reader["锻炼频率"].ToString();
                info.每次锻炼时间 = reader["每次锻炼时间"].ToString();
                info.坚持锻炼时间 = reader["坚持锻炼时间"].ToString();
                info.锻炼方式 = reader["锻炼方式"].ToString();
                info.饮食习惯 = reader["饮食习惯"].ToString();
                info.吸烟状况 = reader["吸烟状况"].ToString();
                info.日吸烟量 = reader["日吸烟量"].ToString();
                info.开始吸烟年龄 = reader["开始吸烟年龄"].ToString();
                info.戒烟年龄 = reader["戒烟年龄"].ToString();
                info.饮酒频率 = reader["饮酒频率"].ToString();
                info.日饮酒量 = reader["日饮酒量"].ToString();
                info.是否戒酒 = reader["是否戒酒"].ToString();
                info.戒酒年龄 = reader["戒酒年龄"].ToString();
                info.开始饮酒年龄 = reader["开始饮酒年龄"].ToString();
                info.近一年内是否曾醉酒 = reader["近一年内是否曾醉酒"].ToString();
                info.饮酒种类 = reader["饮酒种类"].ToString();
                info.饮酒种类其它 = reader["饮酒种类其它"].ToString();
                info.有无职业病 = reader["有无职业病"].ToString();
                info.具体职业 = reader["具体职业"].ToString();
                info.从业时间 = reader["从业时间"].ToString();
                info.化学物质 = reader["化学物质"].ToString();
                info.化学物质防护 = reader["化学物质防护"].ToString();
                info.化学物质具体防护 = reader["化学物质具体防护"].ToString();
                info.毒物 = reader["毒物"].ToString();
                info.G_DWFHCS = reader["G_DWFHCS"].ToString();
                info.G_DWFHCSQT = reader["G_DWFHCSQT"].ToString();
                info.放射物质 = reader["放射物质"].ToString();
                info.放射物质防护措施有无 = reader["放射物质防护措施有无"].ToString();
                info.放射物质防护措施其他 = reader["放射物质防护措施其他"].ToString();
                info.口唇 = reader["口唇"].ToString();
                info.齿列 = reader["齿列"].ToString();
                info.咽部 = reader["咽部"].ToString();
                info.皮肤其他 = reader["皮肤其他"].ToString();
                info.巩膜 = reader["巩膜"].ToString();
                info.巩膜其他 = reader["巩膜其他"].ToString();
                info.足背动脉搏动 = reader["足背动脉搏动"].ToString();
                info.乳腺 = reader["乳腺"].ToString();
                info.乳腺其他 = reader["乳腺其他"].ToString();
                info.外阴 = reader["外阴"].ToString();
                info.外阴异常 = reader["外阴异常"].ToString();
                info.阴道 = reader["阴道"].ToString();
                info.阴道异常 = reader["阴道异常"].ToString();
                info.宫颈 = reader["宫颈"].ToString();
                info.宫颈异常 = reader["宫颈异常"].ToString();
                info.宫体 = reader["宫体"].ToString();
                info.宫体异常 = reader["宫体异常"].ToString();
                info.附件 = reader["附件"].ToString();
                info.附件异常 = reader["附件异常"].ToString();
                info.血钾浓度 = reader["血钾浓度"].ToString();
                info.血钠浓度 = reader["血钠浓度"].ToString();
                info.糖化血红蛋白 = reader["糖化血红蛋白"].ToString();
                info.宫颈涂片 = reader["宫颈涂片"].ToString();
                info.宫颈涂片异常 = reader["宫颈涂片异常"].ToString();
                info.平和质 = reader["平和质"].ToString();
                info.气虚质 = reader["气虚质"].ToString();
                info.阳虚质 = reader["阳虚质"].ToString();
                info.阴虚质 = reader["阴虚质"].ToString();
                info.痰湿质 = reader["痰湿质"].ToString();
                info.湿热质 = reader["湿热质"].ToString();
                info.血瘀质 = reader["血瘀质"].ToString();
                info.气郁质 = reader["气郁质"].ToString();
                info.特禀质 = reader["特禀质"].ToString();
                info.脑血管疾病 = reader["脑血管疾病"].ToString();
                info.脑血管疾病其他 = reader["脑血管疾病其他"].ToString();
                info.肾脏疾病 = reader["肾脏疾病"].ToString();
                info.肾脏疾病其他 = reader["肾脏疾病其他"].ToString();
                info.心脏疾病 = reader["心脏疾病"].ToString();
                info.心脏疾病其他 = reader["心脏疾病其他"].ToString();
                info.血管疾病 = reader["血管疾病"].ToString();
                info.血管疾病其他 = reader["血管疾病其他"].ToString();
                info.眼部疾病 = reader["眼部疾病"].ToString();
                info.眼部疾病其他 = reader["眼部疾病其他"].ToString();
                info.神经系统疾病 = reader["神经系统疾病"].ToString();
                info.神经系统疾病其他 = reader["神经系统疾病其他"].ToString();
                info.其他系统疾病 = reader["其他系统疾病"].ToString();
                info.其他系统疾病其他 = reader["其他系统疾病其他"].ToString();
                info.健康评价 = reader["健康评价"].ToString();
                info.健康评价异常1 = reader["健康评价异常1"].ToString();
                info.健康评价异常2 = reader["健康评价异常2"].ToString();
                info.健康评价异常3 = reader["健康评价异常3"].ToString();
                info.健康评价异常4 = reader["健康评价异常4"].ToString();
                info.健康指导 = reader["健康指导"].ToString();
                info.危险因素控制 = reader["危险因素控制"].ToString();
                info.危险因素控制体重 = reader["危险因素控制体重"].ToString();
                info.危险因素控制疫苗 = reader["危险因素控制疫苗"].ToString();
                info.危险因素控制其他 = reader["危险因素控制其他"].ToString();
                info.FIELD5 = reader["FIELD5"].ToString();
                info.症状其他 = reader["症状其他"].ToString();
                info.G_XYYC = reader["G_XYYC"].ToString();
                info.G_XYZC = reader["G_XYZC"].ToString();
                info.G_QTZHZH = reader["G_QTZHZH"].ToString();
                info.缺项 = reader["缺项"].ToString();
                info.口唇其他 = reader["口唇其他"].ToString();
                info.齿列其他 = reader["齿列其他"].ToString();
                info.咽部其他 = reader["咽部其他"].ToString();
                info.YDGNQT = reader["YDGNQT"].ToString();
                info.餐后2H血糖 = reader["餐后2H血糖"].ToString();
                info.老年人状况评估 = reader["老年人状况评估"].ToString();
                info.老年人自理评估 = reader["老年人自理评估"].ToString();
                info.粉尘 = reader["粉尘"].ToString();
                info.物理因素 = reader["物理因素"].ToString();
                info.职业病其他 = reader["职业病其他"].ToString();
                info.粉尘防护有无 = reader["粉尘防护有无"].ToString();
                info.物理防护有无 = reader["物理防护有无"].ToString();
                info.其他防护有无 = reader["其他防护有无"].ToString();
                info.粉尘防护措施 = reader["粉尘防护措施"].ToString();
                info.物理防护措施 = reader["物理防护措施"].ToString();
                info.其他防护措施 = reader["其他防护措施"].ToString();
                info.TNBFXJF = reader["TNBFXJF"].ToString();
                info.左侧原因 = reader["左侧原因"].ToString();
                info.右侧原因 = reader["右侧原因"].ToString();
                info.尿微量白蛋白 = reader["尿微量白蛋白"].ToString();
                info.完整度 = reader["完整度"].ToString();
                info.齿列缺齿 = reader["齿列缺齿"].ToString();
                info.齿列龋齿 = reader["齿列龋齿"].ToString();
                info.齿列义齿 = reader["齿列义齿"].ToString();
                info.BlueName = reader["BlueName"].ToString();
                info.BlueAddr = reader["BlueAddr"].ToString();
                info.BlueType = reader["BlueType"].ToString();
                info.RowState = reader["RowState"].ToString();
                //info.医师签字 = reader["医师签字"].ToString();
                infoList.Add(info);
            }
            reader.Close();
            return infoList;
        }

        /// <summary>
        /// 根据 主键 获取一个实体对象
        /// <param name="ID">主键</param>
        /// </summary>
        public static tb_健康体检Info Gettb_健康体检InfoById(string ID)
        {
            string strWhere = " 身份证号 ='" + ID + "'";
            List<tb_健康体检Info> list = Gettb_健康体检InfoList(strWhere);
            if (list.Count > 0)
                return list[0];
            return null;
        }

        private static DBClass dbConstring;
        /// <summary>
        /// 添加、修改
        /// </summary>
        /// <param name="Info"></param>
        /// <returns></returns>
        public static bool UpdateSys_尿常规(tb_健康体检Info Info)
        {
            bool bBool = false;
            DataSet datSet;
            DataTable datTable;
            isExists_健康体检(out datSet, out datTable);
            DataRow datRow = null;

            bool bAdd = false;
            if (datTable.Rows.Count > 0)
            {
                datRow = datTable.Rows[0];
            }
            else
            {
                bAdd = true;
            }

            if (bAdd)
            {
                //增加一行
                datRow = datTable.NewRow();
                //datRow["ID"] = Info.ID;
                GetNewDataRow_健康体检(datRow);
            }
            datRow["尿蛋白"] = Info.尿蛋白;
            datRow["尿潜血"] = Info.尿潜血;
            datRow["尿糖"] = Info.尿糖;
            datRow["尿酮体"] = Info.尿酮体;
            datRow["尿微量白蛋白"] = Info.尿微量白蛋白;

            if (bAdd)
            {
                datTable.Rows.Add(datRow);
            }

            try
            {
                DBManager.UpdateDataSetForSqlite(datSet, "tb_健康体检");
                bBool = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return bBool;
        }
        public static bool UpdateSys_心电(tb_健康体检Info Info)
        {
            bool bBool = false;
            DataSet datSet;
            DataTable datTable;
            isExists_健康体检(out datSet, out datTable);
            DataRow datRow = null;

            bool bAdd = false;
            if (datTable.Rows.Count > 0)
            {
                datRow = datTable.Rows[0];
            }
            else
            {
                bAdd = true;
            }

            if (bAdd)
            {
                //增加一行
                datRow = datTable.NewRow();
                //datRow["ID"] = Info.ID;
                GetNewDataRow_健康体检(datRow);
            }
            datRow["心电图"] = Info.心电图;
            datRow["心电图异常"] = Info.心电图异常;

            if (bAdd)
            {
                datTable.Rows.Add(datRow);
            }

            try
            {
                DBManager.UpdateDataSetForSqlite(datSet, "tb_健康体检");
                bBool = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return bBool;
        }

        public static bool UpdateSys_B超(tb_健康体检Info Info)
        {
            bool bBool = false;
            DataSet datSet;
            DataTable datTable;
            isExists_健康体检(out datSet, out datTable);
            DataRow datRow = null;

            bool bAdd = false;
            if (datTable.Rows.Count > 0)
            {
                datRow = datTable.Rows[0];
            }
            else
            {
                bAdd = true;
            }

            if (bAdd)
            {
                //增加一行
                datRow = datTable.NewRow();
                //datRow["ID"] = Info.ID;
                GetNewDataRow_健康体检(datRow);
            }
            datRow["B超"] = Info.B超;
            datRow["B超其他"] = Info.B超其他;
            datRow["医师签字"] = Info.医师签字;

            if (bAdd)
            {
                datTable.Rows.Add(datRow);
            }

            try
            {
                DBManager.UpdateDataSetForSqlite(datSet, "tb_健康体检");
                bBool = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return bBool;
        }

        //Begin WXF 2019-04-22 | 15:30
        //保存tb_健康体检_B超 
        public static bool UpdateSys_B超报告(tb_健康体检_B超 tb_B超)
        {
            bool bBool = false;
            DataSet datSet;
            DataTable datTable;
            isExists_B超(out datSet, out datTable);
            DataRow datRow = null;

            bool bAdd = false;
            if (datTable.Rows.Count > 0)
            {
                datRow = datTable.Rows[0];
            }
            else
            {
                bAdd = true;
            }

            if (bAdd)
            {
                datRow = datTable.NewRow();
                GetNewDataRow_B超(datRow);
            }

            datRow["超声所见"] = tb_B超.超声所见;
            datRow["超声提示"] = tb_B超.超声提示;

            if (bAdd)
            {
                datTable.Rows.Add(datRow);
            }

            try
            {
                DBManager.UpdateDataSetForSqlite(datSet, "tb_健康体检_B超");
                bBool = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return bBool;
        }
        //End

        public static bool UpdateSys_身高体重(tb_健康体检Info Info)
        {
            bool bBool = false;
            DataSet datSet;
            DataTable datTable;
            isExists_健康体检(out datSet, out datTable);
            DataRow datRow = null;

            bool bAdd = false;
            if (datTable.Rows.Count > 0)
            {
                datRow = datTable.Rows[0];
            }
            else
            {
                bAdd = true;
            }

            if (bAdd)
            {
                //增加一行
                datRow = datTable.NewRow();
                //datRow["ID"] = Info.ID;
                GetNewDataRow_健康体检(datRow);
            }
            datRow["身高"] = Info.身高;
            datRow["体重"] = Info.体重;
            datRow["腰围"] = Info.腰围;
            datRow["体重指数"] = Info.体重指数;
            datRow["危险因素控制"] = Info.危险因素控制;
            datRow["危险因素控制体重"] = Info.危险因素控制体重;
            datRow["医师签字"] = Info.医师签字;

            if (bAdd)
            {
                datTable.Rows.Add(datRow);
            }

            try
            {
                DBManager.UpdateDataSetForSqlite(datSet, "tb_健康体检");
                bBool = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return bBool;
        }
        public static bool UpdateSys_视力(tb_健康体检Info Info)
        {
            bool bBool = false;
            DataSet datSet;
            DataTable datTable;
            isExists_健康体检(out datSet, out datTable);
            DataRow datRow = null;

            bool bAdd = false;
            if (datTable.Rows.Count > 0)
            {
                datRow = datTable.Rows[0];
            }
            else
            {
                bAdd = true;
            }

            if (bAdd)
            {
                //增加一行
                datRow = datTable.NewRow();
                //datRow["ID"] = Info.ID;
                GetNewDataRow_健康体检(datRow);
            }
            datRow["左眼视力"] = Info.左眼视力;
            datRow["右眼视力"] = Info.右眼视力;
            datRow["左眼矫正"] = Info.左眼矫正;
            datRow["右眼矫正"] = Info.右眼矫正;


            if (bAdd)
            {
                datTable.Rows.Add(datRow);
            }

            try
            {
                DBManager.UpdateDataSetForSqlite(datSet, "tb_健康体检");
                bBool = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return bBool;
        }
        public static bool UpdateSys_血压(tb_健康体检Info Info)
        {
            bool bBool = false;
            DataSet datSet;
            DataTable datTable;
            isExists_健康体检(out datSet, out datTable);
            DataRow datRow = null;

            bool bAdd = false;
            if (datTable.Rows.Count > 0)
            {
                datRow = datTable.Rows[0];
            }
            else
            {
                bAdd = true;
            }

            if (bAdd)
            {
                //增加一行
                datRow = datTable.NewRow();
                //datRow["ID"] = Info.ID;
                GetNewDataRow_健康体检(datRow);
            }
            datRow["血压右侧1"] = Info.血压右侧1;
            datRow["血压右侧2"] = Info.血压右侧2;
            datRow["血压左侧1"] = Info.血压左侧1;
            datRow["血压左侧2"] = Info.血压左侧2;
            //wxf 2018年10月16日 16:00:03 泉庄要求心率脉搏从心电结果中取
            //datRow["心率"] = Info.心率;
            //datRow["脉搏"] = Info.脉搏;
            datRow["医师签字"] = Info.医师签字;

            if (bAdd)
            {
                datTable.Rows.Add(datRow);
            }

            try
            {
                DBManager.UpdateDataSetForSqlite(datSet, "tb_健康体检");
                bBool = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return bBool;
        }
        public static bool UpdateSys_血糖(tb_健康体检Info Info)
        {
            bool bBool = false;
            DataSet datSet;
            DataTable datTable;
            isExists_健康体检(out datSet, out datTable);
            DataRow datRow = null;

            bool bAdd = false;
            if (datTable.Rows.Count > 0)
            {
                datRow = datTable.Rows[0];
            }
            else
            {
                bAdd = true;
            }

            if (bAdd)
            {
                //增加一行
                datRow = datTable.NewRow();
                //datRow["ID"] = Info.ID;
                GetNewDataRow_健康体检(datRow);
            }
            datRow["空腹血糖"] = Info.空腹血糖;

            if (bAdd)
            {
                datTable.Rows.Add(datRow);
            }

            try
            {
                DBManager.UpdateDataSetForSqlite(datSet, "tb_健康体检");
                bBool = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return bBool;
        }
        public static bool UpdateSys_脉搏(tb_健康体检Info Info)
        {
            bool bBool = false;
            DataSet datSet;
            DataTable datTable;
            isExists_健康体检(out datSet, out datTable);
            DataRow datRow = null;

            bool bAdd = false;
            if (datTable.Rows.Count > 0)
            {
                datRow = datTable.Rows[0];
            }
            else
            {
                bAdd = true;
            }

            if (bAdd)
            {
                //增加一行
                datRow = datTable.NewRow();
                //datRow["ID"] = Info.ID;
                GetNewDataRow_健康体检(datRow);
            }
            datRow["脉搏"] = Info.脉搏;
            datRow["心率"] = Info.心率;

            if (bAdd)
            {
                datTable.Rows.Add(datRow);
            }

            try
            {
                DBManager.UpdateDataSetForSqlite(datSet, "tb_健康体检");
                bBool = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return bBool;
        }
        public static bool UpdateSys_体温(tb_健康体检Info Info)
        {
            bool bBool = false;
            DataSet datSet;
            DataTable datTable;
            isExists_健康体检(out datSet, out datTable);
            DataRow datRow = null;

            bool bAdd = false;
            if (datTable.Rows.Count > 0)
            {
                datRow = datTable.Rows[0];
            }
            else
            {
                bAdd = true;
            }

            if (bAdd)
            {
                //增加一行
                datRow = datTable.NewRow();
                //datRow["ID"] = Info.ID;
                GetNewDataRow_健康体检(datRow);
            }
            datRow["体温"] = Info.体温;
            datRow["呼吸"] = Info.呼吸;

            if (bAdd)
            {
                datTable.Rows.Add(datRow);
            }

            try
            {
                DBManager.UpdateDataSetForSqlite(datSet, "tb_健康体检");
                bBool = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return bBool;
        }
        public static bool UpdateSys_体检1(tb_健康体检Info Info)
        {
            bool bBool = false;
            DataSet datSet;
            DataTable datTable;
            isExists_健康体检(out datSet, out datTable);
            DataRow datRow = null;

            bool bAdd = false;
            if (datTable.Rows.Count > 0)
            {
                datRow = datTable.Rows[0];
            }
            else
            {
                bAdd = true;
            }

            if (bAdd)
            {
                //增加一行
                datRow = datTable.NewRow();
                //datRow["ID"] = Info.ID;
                GetNewDataRow_健康体检(datRow);
            }
            datRow["症状"] = Info.症状;
            datRow["症状其他"] = Info.症状其他;
            datRow["老年人状况评估"] = Info.老年人状况评估;
            datRow["老年人自理评估"] = Info.老年人自理评估;
            datRow["老年人情感"] = Info.老年人情感;
            datRow["老年人情感分"] = Info.老年人情感分;
            datRow["老年人认知"] = Info.老年人认知;
            datRow["老年人认知分"] = Info.老年人认知分;
            datRow["锻炼频率"] = Info.锻炼频率;
            datRow["每次锻炼时间"] = Info.每次锻炼时间;
            datRow["坚持锻炼时间"] = Info.坚持锻炼时间;
            datRow["锻炼方式"] = Info.锻炼方式;
            datRow["饮食习惯"] = Info.饮食习惯;
            datRow["吸烟状况"] = Info.吸烟状况;
            datRow["日吸烟量"] = Info.日吸烟量;
            datRow["开始吸烟年龄"] = Info.开始吸烟年龄;
            datRow["戒烟年龄"] = Info.戒烟年龄;
            datRow["饮酒频率"] = Info.饮酒频率;
            datRow["日饮酒量"] = Info.日饮酒量;
            datRow["是否戒酒"] = Info.是否戒酒;
            datRow["戒酒年龄"] = Info.戒酒年龄;
            datRow["开始饮酒年龄"] = Info.开始饮酒年龄;
            datRow["近一年内是否曾醉酒"] = Info.近一年内是否曾醉酒;
            datRow["饮酒种类"] = Info.饮酒种类;
            datRow["有无职业病"] = Info.有无职业病;
            datRow["具体职业"] = Info.具体职业;
            datRow["从业时间"] = Info.从业时间;
            datRow["粉尘"] = Info.粉尘;
            datRow["粉尘防护有无"] = Info.粉尘防护有无;
            datRow["粉尘防护措施"] = Info.粉尘防护措施;
            datRow["放射物质"] = Info.放射物质;
            datRow["放射物质防护措施有无"] = Info.放射物质防护措施有无;
            datRow["放射物质防护措施其他"] = Info.放射物质防护措施其他;
            datRow["物理因素"] = Info.物理因素;
            datRow["物理防护有无"] = Info.物理防护有无;
            datRow["物理防护措施"] = Info.物理防护措施;
            datRow["化学物质"] = Info.化学物质;
            datRow["化学物质防护"] = Info.化学物质防护;
            datRow["化学物质具体防护"] = Info.化学物质具体防护;
            datRow["职业病其他"] = Info.职业病其他;
            datRow["其他防护有无"] = Info.其他防护有无;
            datRow["其他防护措施"] = Info.其他防护措施;
            datRow["医师签字"] = Info.医师签字;

            if (bAdd)
            {
                datTable.Rows.Add(datRow);
            }

            try
            {
                DBManager.UpdateDataSetForSqlite(datSet, "tb_健康体检");
                bBool = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return bBool;
        }
        public static bool UpdateSys_体检2(tb_健康体检Info Info)
        {
            bool bBool = false;
            DataSet datSet;
            DataTable datTable;
            isExists_健康体检(out datSet, out datTable);
            DataRow datRow = null;

            bool bAdd = false;
            if (datTable.Rows.Count > 0)
            {
                datRow = datTable.Rows[0];
            }
            else
            {
                bAdd = true;
            }

            if (bAdd)
            {
                //增加一行
                datRow = datTable.NewRow();
                //datRow["ID"] = Info.ID;
                GetNewDataRow_健康体检(datRow);
            }
            datRow["脑血管疾病"] = Info.脑血管疾病;
            datRow["脑血管疾病其他"] = Info.脑血管疾病其他;
            datRow["肾脏疾病"] = Info.肾脏疾病;
            datRow["肾脏疾病其他"] = Info.肾脏疾病其他;
            datRow["心脏疾病"] = Info.心脏疾病;
            datRow["心脏疾病其他"] = Info.心脏疾病其他;
            datRow["血管疾病"] = Info.血管疾病;
            datRow["血管疾病其他"] = Info.血管疾病其他;
            datRow["眼部疾病"] = Info.眼部疾病;
            datRow["眼部疾病其他"] = Info.眼部疾病其他;
            datRow["神经系统疾病"] = Info.神经系统疾病;
            datRow["神经系统疾病其他"] = Info.神经系统疾病其他;
            datRow["其他系统疾病"] = Info.其他系统疾病;
            datRow["其他系统疾病其他"] = Info.其他系统疾病其他;
            datRow["医师签字"] = Info.医师签字;

            if (bAdd)
            {
                datTable.Rows.Add(datRow);
            }

            try
            {
                DBManager.UpdateDataSetForSqlite(datSet, "tb_健康体检");
                bBool = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return bBool;
        }
        public static bool UpdateSys_体检3(tb_健康体检Info Info)
        {
            bool bBool = false;
            DataSet datSet;
            DataTable datTable;
            isExists_健康体检(out datSet, out datTable);
            DataRow datRow = null;

            bool bAdd = false;
            if (datTable.Rows.Count > 0)
            {
                datRow = datTable.Rows[0];
            }
            else
            {
                bAdd = true;
            }

            if (bAdd)
            {
                //增加一行
                datRow = datTable.NewRow();
                //datRow["ID"] = Info.ID;
                GetNewDataRow_健康体检(datRow);
            }
            datRow["口唇"] = Info.口唇;
            datRow["口唇其他"] = Info.口唇其他;
            datRow["齿列"] = Info.齿列;
            datRow["齿列龋齿"] = Info.齿列龋齿;
            datRow["齿列缺齿"] = Info.齿列缺齿;
            datRow["齿列义齿"] = Info.齿列义齿;
            datRow["齿列其他"] = Info.齿列其他;
            datRow["咽部"] = Info.咽部;
            datRow["咽部其他"] = Info.咽部其他;
            datRow["左眼视力"] = Info.左眼视力;
            datRow["右眼视力"] = Info.右眼视力;
            datRow["左眼矫正"] = Info.左眼矫正;
            datRow["右眼矫正"] = Info.右眼矫正;
            datRow["听力"] = Info.听力;
            datRow["运动功能"] = Info.运动功能;
            datRow["眼底"] = Info.眼底;
            datRow["眼底异常"] = Info.眼底异常;
            datRow["皮肤"] = Info.皮肤;
            datRow["皮肤其他"] = Info.皮肤其他;
            datRow["巩膜"] = Info.巩膜;
            datRow["巩膜其他"] = Info.巩膜其他;
            datRow["淋巴结"] = Info.淋巴结;
            datRow["淋巴结其他"] = Info.淋巴结其他;
            datRow["桶状胸"] = Info.桶状胸;
            datRow["呼吸音"] = Info.呼吸音;
            datRow["呼吸音异常"] = Info.呼吸音异常;
            datRow["罗音"] = Info.罗音;
            datRow["罗音异常"] = Info.罗音异常;
            datRow["心律"] = Info.心律;
            datRow["杂音"] = Info.杂音;
            datRow["杂音有"] = Info.杂音有;
            datRow["压痛"] = Info.压痛;
            datRow["压痛有"] = Info.压痛有;
            datRow["包块"] = Info.包块;
            datRow["包块有"] = Info.包块有;
            datRow["肝大"] = Info.肝大;
            datRow["肝大有"] = Info.肝大有;
            datRow["脾大"] = Info.脾大;
            datRow["脾大有"] = Info.脾大有;
            datRow["浊音"] = Info.浊音;
            datRow["浊音有"] = Info.浊音有;
            datRow["下肢水肿"] = Info.下肢水肿;
            datRow["足背动脉搏动"] = Info.足背动脉搏动;
            datRow["医师签字"] = Info.医师签字;

            if (bAdd)
            {
                datTable.Rows.Add(datRow);
            }

            try
            {
                DBManager.UpdateDataSetForSqlite(datSet, "tb_健康体检");
                bBool = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return bBool;
        }
        private static void isExists_健康体检(out DataSet datSet, out DataTable datTable)
        {
            dbConstring = new DBClass(DbFilePath.FilePathName);

            string strSql = "", strTableName = "tb_健康体检";
            //strSql = "SELECT * FROM  " + strTableName + " WHERE  (个人档案编号='" + Program.currentUser.DocNo  +
            //    "' or 身份证号='" + Program.currentUser.ID + "') AND 体检日期='" + DateTime.Now.ToString("yyyy-MM-dd") + "'  ";
            strSql = "SELECT * FROM  " + strTableName + " WHERE   体检日期='" + DateTime.Now.ToString("yyyy-MM-dd") + "' ";
            if (!string.IsNullOrEmpty(Program.currentUser.DocNo))
                strSql += " AND 个人档案编号='" + Program.currentUser.DocNo + "' ";
            if (!string.IsNullOrEmpty(Program.currentUser.ID))
                strSql += " AND 身份证号='" + Program.currentUser.ID + "' ";
            datSet = SQLiteHelper.ExecuteDataSet(dbConstring.m_ConnString, strSql, CommandType.Text);
            datTable = datSet.Tables[0];
            datTable.TableName = strTableName;
        }

        private static void isExists_B超(out DataSet datSet, out DataTable datTable)
        {
            dbConstring = new DBClass(DbFilePath.FilePathName);

            string strSql = "", strTableName = "tb_健康体检_B超";

            strSql = "SELECT * FROM  " + strTableName + " WHERE 检查日期='" + DateTime.Now.ToString("yyyy-MM-dd") + "' ";
            if (!string.IsNullOrEmpty(Program.currentUser.DocNo))
                strSql += " AND 个人档案编号='" + Program.currentUser.DocNo + "' ";

            datSet = SQLiteHelper.ExecuteDataSet(dbConstring.m_ConnString, strSql, CommandType.Text);
            datTable = datSet.Tables[0];
            datTable.TableName = strTableName;
        }


        private static void GetNewDataRow_健康体检(DataRow dataRow)
        {
            dataRow["身份证号"] = Program.currentUser.ID;
            dataRow["个人档案编号"] = Program.currentUser.DocNo;
            dataRow["姓名"] = Program.currentUser.Name;
            dataRow["性别"] = Program.currentUser.Gender;
            dataRow["出生日期"] = Program.currentUser.Birth;
            dataRow["体检日期"] = DateTime.Now.ToString("yyyy-MM-dd");
            // add by 朱文杰 2017-05-25 11:35 for 责任医生不展示 begin
            dataRow["FIELD2"] = Program._bDoctor;
            // add by 朱文杰 2017-05-25 11:35 end
            dataRow["创建时间"] = Program._currentTime;
            dataRow["修改时间"] = Program._currentTime;
            dataRow["所属机构"] = Program.currentUser.Prgid;
            dataRow["创建机构"] = Program.currentUser.Prgid;
            dataRow["创建人"] = Program.currentUser.创建人;
            dataRow["修改人"] = Program.currentUser.创建人;
            dataRow["症状"] = "1";
            dataRow["听力"] = "1";
            dataRow["运动功能"] = "1";
            dataRow["皮肤"] = "1";
            dataRow["淋巴结"] = "1";
            dataRow["桶状胸"] = "2";
            dataRow["呼吸音"] = "1";
            dataRow["罗音"] = "1";
            dataRow["心律"] = "1";
            dataRow["杂音"] = "1";
            dataRow["压痛"] = "1";
            dataRow["包块"] = "1";
            dataRow["肝大"] = "1";
            dataRow["脾大"] = "1";
            dataRow["浊音"] = "1";
            dataRow["下肢水肿"] = "1";
            dataRow["锻炼频率"] = "4";
            dataRow["饮食习惯"] = "1";
            dataRow["吸烟状况"] = "1";
            dataRow["饮酒频率"] = "1";
            dataRow["是否戒酒"] = "1";
            dataRow["有无职业病"] = "1";
            dataRow["口唇"] = "1";
            dataRow["齿列"] = "1";
            dataRow["咽部"] = "1";
            dataRow["巩膜"] = "1";
            dataRow["足背动脉搏动"] = "2";
            dataRow["脑血管疾病"] = "1";
            dataRow["肾脏疾病"] = "1";
            dataRow["心脏疾病"] = "1";
            dataRow["血管疾病"] = "1";
            dataRow["眼部疾病"] = "1";
            dataRow["神经系统疾病"] = "1";
            dataRow["其他系统疾病"] = "1";
            dataRow["健康评价"] = "1";
            dataRow["BlueName"] = Program.BlueName;
            dataRow["BlueAddr"] = Program.BlueAddr;
            dataRow["BlueType"] = Program.BlueType;
        }

        private static void GetNewDataRow_B超(DataRow dataRow)
        {
            dataRow["个人档案编号"] = Program.currentUser.DocNo;
            dataRow["备注"] = string.Empty;
            dataRow["所属机构"] = Program.currentUser.Prgid;
            dataRow["检查日期"] = DateTime.Now.ToString("yyyy-MM-dd");
            dataRow["创建日期"] = Program._currentTime;
            dataRow["创建人"] = Program.currentUser.创建人;
        }

        /// 添加数据
        /// </summary>
        /// <param name="info">数据表实体对象</param>
        public static bool Addtb_健康体检(tb_健康体检Info info)
        {
            string _身份证号 = Program.currentUser.ID;
            string _姓名 = Program.currentUser.Name;
            string _性别 = Program.currentUser.Gender;
            string _出生日期 = Program.currentUser.Birth;
            string _症状 = info.症状;
            double _体温 = info.体温;
            int _呼吸 = info.呼吸;
            int _脉搏 = info.脉搏;
            string _血压右侧1 = info.血压右侧1;
            string _血压右侧2 = info.血压右侧2;
            string _血压左侧1 = info.血压左侧1;
            string _血压左侧2 = info.血压左侧2;
            double _身高 = info.身高;
            double _腰围 = info.腰围;
            double _体重 = info.体重;
            string _体重指数 = info.体重指数;
            string _老年人认知 = info.老年人认知;
            string _老年人情感 = info.老年人情感;
            string _老年人认知分 = info.老年人认知分;
            string _老年人情感分 = info.老年人情感分;
            double _左眼视力 = info.左眼视力;
            double _右眼视力 = info.右眼视力;
            string _左眼矫正 = info.左眼矫正;
            string _右眼矫正 = info.右眼矫正;
            string _听力 = info.听力;
            string _运动功能 = info.运动功能;
            string _皮肤 = info.皮肤;
            string _淋巴结 = info.淋巴结;
            string _淋巴结其他 = info.淋巴结其他;
            string _桶状胸 = info.桶状胸;
            string _呼吸音 = info.呼吸音;
            string _呼吸音异常 = info.呼吸音异常;
            string _罗音 = info.罗音;
            string _罗音异常 = info.罗音异常;
            int _心率 = info.心率;
            string _心律 = info.心律;
            string _杂音 = info.杂音;
            string _杂音有 = info.杂音有;
            string _压痛 = info.压痛;
            string _压痛有 = info.压痛有;
            string _包块 = info.包块;
            string _包块有 = info.包块有;
            string _肝大 = info.肝大;
            string _肝大有 = info.肝大有;
            string _脾大 = info.脾大;
            string _脾大有 = info.脾大有;
            string _浊音 = info.浊音;
            string _浊音有 = info.浊音有;
            string _下肢水肿 = info.下肢水肿;
            string _肛门指诊 = info.肛门指诊;
            string _肛门指诊异常 = info.肛门指诊异常;
            string _G_QLX = info.G_QLX;
            string _查体其他 = info.查体其他;
            string _白细胞 = info.白细胞;
            string _血红蛋白 = info.血红蛋白;
            string _血小板 = info.血小板;
            string _血常规其他 = info.血常规其他;
            string _尿蛋白 = info.尿蛋白;
            string _尿糖 = info.尿糖;
            string _尿酮体 = info.尿酮体;
            string _尿潜血 = info.尿潜血;
            string _尿常规其他 = info.尿常规其他;
            string _大便潜血 = info.大便潜血;
            string _血清谷丙转氨酶 = info.血清谷丙转氨酶;
            string _血清谷草转氨酶 = info.血清谷草转氨酶;
            string _白蛋白 = info.白蛋白;
            string _总胆红素 = info.总胆红素;
            string _结合胆红素 = info.结合胆红素;
            string _血清肌酐 = info.血清肌酐;
            string _血尿素氮 = info.血尿素氮;
            string _总胆固醇 = info.总胆固醇;
            string _甘油三酯 = info.甘油三酯;
            string _血清低密度脂蛋白胆固醇 = info.血清低密度脂蛋白胆固醇;
            string _血清高密度脂蛋白胆固醇 = info.血清高密度脂蛋白胆固醇;
            string _空腹血糖 = info.空腹血糖;
            string _乙型肝炎表面抗原 = info.乙型肝炎表面抗原;
            string _眼底 = info.眼底;
            string _眼底异常 = info.眼底异常;
            string _心电图 = info.心电图;
            string _心电图异常 = info.心电图异常;
            string _胸部X线片 = info.胸部X线片;
            string _胸部X线片异常 = info.胸部X线片异常;
            string _B超 = info.B超;
            string _B超其他 = info.B超其他;
            string _辅助检查其他 = info.辅助检查其他;
            string _G_JLJJY = info.G_JLJJY;
            string _个人档案编号 = Program.currentUser.DocNo;
            string _创建机构 = Program.currentUser.Prgid;
            string _创建时间 = Program._currentTime;
            string _创建人 = Program.currentUser.创建人; //"371323B100190001";
            string _修改时间 = Program._currentTime;
            string _修改人 = Program.currentUser.创建人; //"371323B100190001";
            string _体检日期 = Program._currentTime.Substring(0, 10);
            string _所属机构 = Program.currentUser.Prgid;
            string _FIELD1 = info.FIELD1;
            string _FIELD2 = info.FIELD2;
            string _FIELD3 = info.FIELD3;
            string _FIELD4 = info.FIELD4;
            int _WBC_SUP = info.WBC_SUP;
            int _PLT_SUP = info.PLT_SUP;
            string _G_TUNWEI = info.G_TUNWEI;
            string _G_YTWBZ = info.G_YTWBZ;
            string _锻炼频率 = info.锻炼频率;
            string _每次锻炼时间 = info.每次锻炼时间;
            string _坚持锻炼时间 = info.坚持锻炼时间;
            string _锻炼方式 = info.锻炼方式;
            string _饮食习惯 = info.饮食习惯;
            string _吸烟状况 = info.吸烟状况;
            string _日吸烟量 = info.日吸烟量;
            string _开始吸烟年龄 = info.开始吸烟年龄;
            string _戒烟年龄 = info.戒烟年龄;
            string _饮酒频率 = info.饮酒频率;
            string _日饮酒量 = info.日饮酒量;
            string _是否戒酒 = info.是否戒酒;
            string _戒酒年龄 = info.戒酒年龄;
            string _开始饮酒年龄 = info.开始饮酒年龄;
            string _近一年内是否曾醉酒 = info.近一年内是否曾醉酒;
            string _饮酒种类 = info.饮酒种类;
            string _饮酒种类其它 = info.饮酒种类其它;
            string _有无职业病 = info.有无职业病;
            string _具体职业 = info.具体职业;
            string _从业时间 = info.从业时间;
            string _化学物质 = info.化学物质;
            string _化学物质防护 = info.化学物质防护;
            string _化学物质具体防护 = info.化学物质具体防护;
            string _毒物 = info.毒物;
            string _G_DWFHCS = info.G_DWFHCS;
            string _G_DWFHCSQT = info.G_DWFHCSQT;
            string _放射物质 = info.放射物质;
            string _放射物质防护措施有无 = info.放射物质防护措施有无;
            string _放射物质防护措施其他 = info.放射物质防护措施其他;
            string _口唇 = info.口唇;
            string _齿列 = info.齿列;
            string _咽部 = info.咽部;
            string _皮肤其他 = info.皮肤其他;
            string _巩膜 = info.巩膜;
            string _巩膜其他 = info.巩膜其他;
            string _足背动脉搏动 = info.足背动脉搏动;
            string _乳腺 = info.乳腺;
            string _乳腺其他 = info.乳腺其他;
            string _外阴 = info.外阴;
            string _外阴异常 = info.外阴异常;
            string _阴道 = info.阴道;
            string _阴道异常 = info.阴道异常;
            string _宫颈 = info.宫颈;
            string _宫颈异常 = info.宫颈异常;
            string _宫体 = info.宫体;
            string _宫体异常 = info.宫体异常;
            string _附件 = info.附件;
            string _附件异常 = info.附件异常;
            string _血钾浓度 = info.血钾浓度;
            string _血钠浓度 = info.血钠浓度;
            string _糖化血红蛋白 = info.糖化血红蛋白;
            string _宫颈涂片 = info.宫颈涂片;
            string _宫颈涂片异常 = info.宫颈涂片异常;
            string _平和质 = info.平和质;
            string _气虚质 = info.气虚质;
            string _阳虚质 = info.阳虚质;
            string _阴虚质 = info.阴虚质;
            string _痰湿质 = info.痰湿质;
            string _湿热质 = info.湿热质;
            string _血瘀质 = info.血瘀质;
            string _气郁质 = info.气郁质;
            string _特禀质 = info.特禀质;
            string _脑血管疾病 = info.脑血管疾病;
            string _脑血管疾病其他 = info.脑血管疾病其他;
            string _肾脏疾病 = info.肾脏疾病;
            string _肾脏疾病其他 = info.肾脏疾病其他;
            string _心脏疾病 = info.心脏疾病;
            string _心脏疾病其他 = info.心脏疾病其他;
            string _血管疾病 = info.血管疾病;
            string _血管疾病其他 = info.血管疾病其他;
            string _眼部疾病 = info.眼部疾病;
            string _眼部疾病其他 = info.眼部疾病其他;
            string _神经系统疾病 = info.神经系统疾病;
            string _神经系统疾病其他 = info.神经系统疾病其他;
            string _其他系统疾病 = info.其他系统疾病;
            string _其他系统疾病其他 = info.其他系统疾病其他;
            string _健康评价 = info.健康评价;
            string _健康评价异常1 = info.健康评价异常1;
            string _健康评价异常2 = info.健康评价异常2;
            string _健康评价异常3 = info.健康评价异常3;
            string _健康评价异常4 = info.健康评价异常4;
            string _健康指导 = info.健康指导;
            string _危险因素控制 = info.危险因素控制;
            string _危险因素控制体重 = info.危险因素控制体重;
            string _危险因素控制疫苗 = info.危险因素控制疫苗;
            string _危险因素控制其他 = info.危险因素控制其他;
            string _FIELD5 = info.FIELD5;
            string _症状其他 = info.症状其他;
            string _G_XYYC = info.G_XYYC;
            string _G_XYZC = info.G_XYZC;
            string _G_QTZHZH = info.G_QTZHZH;
            string _缺项 = info.缺项;
            string _口唇其他 = info.口唇其他;
            string _齿列其他 = info.齿列其他;
            string _咽部其他 = info.咽部其他;
            string _YDGNQT = info.YDGNQT;
            string _餐后2H血糖 = info.餐后2H血糖;
            string _老年人状况评估 = info.老年人状况评估;
            string _老年人自理评估 = info.老年人自理评估;
            string _粉尘 = info.粉尘;
            string _物理因素 = info.物理因素;
            string _职业病其他 = info.职业病其他;
            string _粉尘防护有无 = info.粉尘防护有无;
            string _物理防护有无 = info.物理防护有无;
            string _其他防护有无 = info.其他防护有无;
            string _粉尘防护措施 = info.粉尘防护措施;
            string _物理防护措施 = info.物理防护措施;
            string _其他防护措施 = info.其他防护措施;
            string _TNBFXJF = info.TNBFXJF;
            string _左侧原因 = info.左侧原因;
            string _右侧原因 = info.右侧原因;
            string _尿微量白蛋白 = info.尿微量白蛋白;
            string _完整度 = info.完整度;
            string _齿列缺齿 = info.齿列缺齿;
            string _齿列龋齿 = info.齿列龋齿;
            string _齿列义齿 = info.齿列义齿;
            string _BlueName = Program.BlueName;
            string _BlueAddr = Program.BlueAddr;
            string _BlueType = Program.BlueType;

            string sql = "INSERT INTO tb_健康体检 VALUES (@_身份证号,@_姓名,@_性别,@_出生日期,@_症状,@_体温,@_呼吸,@_脉搏,@_血压右侧1,@_血压右侧2,@_血压左侧1,@_血压左侧2,@_身高,@_腰围,@_体重,@_体重指数,@_老年人认知,@_老年人情感,@_老年人认知分,@_老年人情感分,@_左眼视力,@_右眼视力,@_左眼矫正,@_右眼矫正,@_听力,@_运动功能,@_皮肤,@_淋巴结,@_淋巴结其他,@_桶状胸,@_呼吸音,@_呼吸音异常,@_罗音,@_罗音异常,@_心率,@_心律,@_杂音,@_杂音有,@_压痛,@_压痛有,@_包块,@_包块有,@_肝大,@_肝大有,@_脾大,@_脾大有,@_浊音,@_浊音有,@_下肢水肿,@_肛门指诊,@_肛门指诊异常,@_G_QLX,@_查体其他,@_白细胞,@_血红蛋白,@_血小板,@_血常规其他,@_尿蛋白,@_尿糖,@_尿酮体,@_尿潜血,@_尿常规其他,@_大便潜血,@_血清谷丙转氨酶,@_血清谷草转氨酶,@_白蛋白,@_总胆红素,@_结合胆红素,@_血清肌酐,@_血尿素氮,@_总胆固醇,@_甘油三酯,@_血清低密度脂蛋白胆固醇,@_血清高密度脂蛋白胆固醇,@_空腹血糖,@_乙型肝炎表面抗原,@_眼底,@_眼底异常,@_心电图,@_心电图异常,@_胸部X线片,@_胸部X线片异常,@_B超,@_B超其他,@_辅助检查其他,@_G_JLJJY,@_个人档案编号,@_创建机构,@_创建时间,@_创建人,@_修改时间,@_修改人,@_体检日期,@_所属机构,@_FIELD1,@_FIELD2,@_FIELD3,@_FIELD4,@_WBC_SUP,@_PLT_SUP,@_G_TUNWEI,@_G_YTWBZ,@_锻炼频率,@_每次锻炼时间,@_坚持锻炼时间,@_锻炼方式,@_饮食习惯,@_吸烟状况,@_日吸烟量,@_开始吸烟年龄,@_戒烟年龄,@_饮酒频率,@_日饮酒量,@_是否戒酒,@_戒酒年龄,@_开始饮酒年龄,@_近一年内是否曾醉酒,@_饮酒种类,@_饮酒种类其它,@_有无职业病,@_具体职业,@_从业时间,@_化学物质,@_化学物质防护,@_化学物质具体防护,@_毒物,@_G_DWFHCS,@_G_DWFHCSQT,@_放射物质,@_放射物质防护措施有无,@_放射物质防护措施其他,@_口唇,@_齿列,@_咽部,@_皮肤其他,@_巩膜,@_巩膜其他,@_足背动脉搏动,@_乳腺,@_乳腺其他,@_外阴,@_外阴异常,@_阴道,@_阴道异常,@_宫颈,@_宫颈异常,@_宫体,@_宫体异常,@_附件,@_附件异常,@_血钾浓度,@_血钠浓度,@_糖化血红蛋白,@_宫颈涂片,@_宫颈涂片异常,@_平和质,@_气虚质,@_阳虚质,@_阴虚质,@_痰湿质,@_湿热质,@_血瘀质,@_气郁质,@_特禀质,@_脑血管疾病,@_脑血管疾病其他,@_肾脏疾病,@_肾脏疾病其他,@_心脏疾病,@_心脏疾病其他,@_血管疾病,@_血管疾病其他,@_眼部疾病,@_眼部疾病其他,@_神经系统疾病,@_神经系统疾病其他,@_其他系统疾病,@_其他系统疾病其他,@_健康评价,@_健康评价异常1,@_健康评价异常2,@_健康评价异常3,@_健康评价异常4,@_健康指导,@_危险因素控制,@_危险因素控制体重,@_危险因素控制疫苗,@_危险因素控制其他,@_FIELD5,@_症状其他,@_G_XYYC,@_G_XYZC,@_G_QTZHZH,@_缺项,@_口唇其他,@_齿列其他,@_咽部其他,@_YDGNQT,@_餐后2H血糖,@_老年人状况评估,@_老年人自理评估,@_粉尘,@_物理因素,@_职业病其他,@_粉尘防护有无,@_物理防护有无,@_其他防护有无,@_粉尘防护措施,@_物理防护措施,@_其他防护措施,@_TNBFXJF,@_左侧原因,@_右侧原因,@_尿微量白蛋白,@_完整度,@_齿列缺齿,@_齿列龋齿,@_齿列义齿)";

            //int rst = DBManager.ExecuteUpdate(sql, new object[] { new SqlParameter("@_身份证号", _身份证号), new SqlParameter("@_姓名", _姓名), new SqlParameter("@_性别", _性别), new SqlParameter("@_出生日期", _出生日期), new SqlParameter("@_症状", _症状), new SqlParameter("@_体温", _体温), new SqlParameter("@_呼吸", _呼吸), new SqlParameter("@_脉搏", _脉搏), new SqlParameter("@_血压右侧1", _血压右侧1), new SqlParameter("@_血压右侧2", _血压右侧2), new SqlParameter("@_血压左侧1", _血压左侧1), new SqlParameter("@_血压左侧2", _血压左侧2), new SqlParameter("@_身高", _身高), new SqlParameter("@_腰围", _腰围), new SqlParameter("@_体重", _体重), new SqlParameter("@_体重指数", _体重指数), new SqlParameter("@_老年人认知", _老年人认知), new SqlParameter("@_老年人情感", _老年人情感), new SqlParameter("@_老年人认知分", _老年人认知分), new SqlParameter("@_老年人情感分", _老年人情感分), new SqlParameter("@_左眼视力", _左眼视力), new SqlParameter("@_右眼视力", _右眼视力), new SqlParameter("@_左眼矫正", _左眼矫正), new SqlParameter("@_右眼矫正", _右眼矫正), new SqlParameter("@_听力", _听力), new SqlParameter("@_运动功能", _运动功能), new SqlParameter("@_皮肤", _皮肤), new SqlParameter("@_淋巴结", _淋巴结), new SqlParameter("@_淋巴结其他", _淋巴结其他), new SqlParameter("@_桶状胸", _桶状胸), new SqlParameter("@_呼吸音", _呼吸音), new SqlParameter("@_呼吸音异常", _呼吸音异常), new SqlParameter("@_罗音", _罗音), new SqlParameter("@_罗音异常", _罗音异常), new SqlParameter("@_心率", _心率), new SqlParameter("@_心律", _心律), new SqlParameter("@_杂音", _杂音), new SqlParameter("@_杂音有", _杂音有), new SqlParameter("@_压痛", _压痛), new SqlParameter("@_压痛有", _压痛有), new SqlParameter("@_包块", _包块), new SqlParameter("@_包块有", _包块有), new SqlParameter("@_肝大", _肝大), new SqlParameter("@_肝大有", _肝大有), new SqlParameter("@_脾大", _脾大), new SqlParameter("@_脾大有", _脾大有), new SqlParameter("@_浊音", _浊音), new SqlParameter("@_浊音有", _浊音有), new SqlParameter("@_下肢水肿", _下肢水肿), new SqlParameter("@_肛门指诊", _肛门指诊), new SqlParameter("@_肛门指诊异常", _肛门指诊异常), new SqlParameter("@_G_QLX", _G_QLX), new SqlParameter("@_查体其他", _查体其他), new SqlParameter("@_白细胞", _白细胞), new SqlParameter("@_血红蛋白", _血红蛋白), new SqlParameter("@_血小板", _血小板), new SqlParameter("@_血常规其他", _血常规其他), new SqlParameter("@_尿蛋白", _尿蛋白), new SqlParameter("@_尿糖", _尿糖), new SqlParameter("@_尿酮体", _尿酮体), new SqlParameter("@_尿潜血", _尿潜血), new SqlParameter("@_尿常规其他", _尿常规其他), new SqlParameter("@_大便潜血", _大便潜血), new SqlParameter("@_血清谷丙转氨酶", _血清谷丙转氨酶), new SqlParameter("@_血清谷草转氨酶", _血清谷草转氨酶), new SqlParameter("@_白蛋白", _白蛋白), new SqlParameter("@_总胆红素", _总胆红素), new SqlParameter("@_结合胆红素", _结合胆红素), new SqlParameter("@_血清肌酐", _血清肌酐), new SqlParameter("@_血尿素氮", _血尿素氮), new SqlParameter("@_总胆固醇", _总胆固醇), new SqlParameter("@_甘油三酯", _甘油三酯), new SqlParameter("@_血清低密度脂蛋白胆固醇", _血清低密度脂蛋白胆固醇), new SqlParameter("@_血清高密度脂蛋白胆固醇", _血清高密度脂蛋白胆固醇), new SqlParameter("@_空腹血糖", _空腹血糖), new SqlParameter("@_乙型肝炎表面抗原", _乙型肝炎表面抗原), new SqlParameter("@_眼底", _眼底), new SqlParameter("@_眼底异常", _眼底异常), new SqlParameter("@_心电图", _心电图), new SqlParameter("@_心电图异常", _心电图异常), new SqlParameter("@_胸部X线片", _胸部X线片), new SqlParameter("@_胸部X线片异常", _胸部X线片异常), new SqlParameter("@_B超", _B超), new SqlParameter("@_B超其他", _B超其他), new SqlParameter("@_辅助检查其他", _辅助检查其他), new SqlParameter("@_G_JLJJY", _G_JLJJY == null ? "" : _G_JLJJY), new SqlParameter("@_个人档案编号", _个人档案编号), new SqlParameter("@_创建机构", _创建机构), new SqlParameter("@_创建时间", _创建时间), new SqlParameter("@_创建人", _创建人), new SqlParameter("@_修改时间", _修改时间), new SqlParameter("@_修改人", _修改人), new SqlParameter("@_体检日期", _体检日期), new SqlParameter("@_所属机构", _所属机构), new SqlParameter("@_FIELD1", _FIELD1), new SqlParameter("@_FIELD2", _FIELD2), new SqlParameter("@_FIELD3", _FIELD3), new SqlParameter("@_FIELD4", _FIELD4), new SqlParameter("@_WBC_SUP", _WBC_SUP), new SqlParameter("@_PLT_SUP", _PLT_SUP), new SqlParameter("@_G_TUNWEI", _G_TUNWEI), new SqlParameter("@_G_YTWBZ", _G_YTWBZ), new SqlParameter("@_锻炼频率", _锻炼频率), new SqlParameter("@_每次锻炼时间", _每次锻炼时间), new SqlParameter("@_坚持锻炼时间", _坚持锻炼时间), new SqlParameter("@_锻炼方式", _锻炼方式), new SqlParameter("@_饮食习惯", _饮食习惯), new SqlParameter("@_吸烟状况", _吸烟状况), new SqlParameter("@_日吸烟量", _日吸烟量), new SqlParameter("@_开始吸烟年龄", _开始吸烟年龄), new SqlParameter("@_戒烟年龄", _戒烟年龄), new SqlParameter("@_饮酒频率", _饮酒频率), new SqlParameter("@_日饮酒量", _日饮酒量), new SqlParameter("@_是否戒酒", _是否戒酒), new SqlParameter("@_戒酒年龄", _戒酒年龄), new SqlParameter("@_开始饮酒年龄", _开始饮酒年龄), new SqlParameter("@_近一年内是否曾醉酒", _近一年内是否曾醉酒), new SqlParameter("@_饮酒种类", _饮酒种类), new SqlParameter("@_饮酒种类其它", _饮酒种类其它), new SqlParameter("@_有无职业病", _有无职业病), new SqlParameter("@_具体职业", _具体职业), new SqlParameter("@_从业时间", _从业时间), new SqlParameter("@_化学物质", _化学物质), new SqlParameter("@_化学物质防护", _化学物质防护), new SqlParameter("@_化学物质具体防护", _化学物质具体防护), new SqlParameter("@_毒物", _毒物), new SqlParameter("@_G_DWFHCS", _G_DWFHCS), new SqlParameter("@_G_DWFHCSQT", _G_DWFHCSQT), new SqlParameter("@_放射物质", _放射物质), new SqlParameter("@_放射物质防护措施有无", _放射物质防护措施有无), new SqlParameter("@_放射物质防护措施其他", _放射物质防护措施其他), new SqlParameter("@_口唇", _口唇), new SqlParameter("@_齿列", _齿列), new SqlParameter("@_咽部", _咽部), new SqlParameter("@_皮肤其他", _皮肤其他), new SqlParameter("@_巩膜", _巩膜), new SqlParameter("@_巩膜其他", _巩膜其他), new SqlParameter("@_足背动脉搏动", _足背动脉搏动), new SqlParameter("@_乳腺", _乳腺), new SqlParameter("@_乳腺其他", _乳腺其他), new SqlParameter("@_外阴", _外阴), new SqlParameter("@_外阴异常", _外阴异常), new SqlParameter("@_阴道", _阴道), new SqlParameter("@_阴道异常", _阴道异常), new SqlParameter("@_宫颈", _宫颈), new SqlParameter("@_宫颈异常", _宫颈异常), new SqlParameter("@_宫体", _宫体), new SqlParameter("@_宫体异常", _宫体异常), new SqlParameter("@_附件", _附件), new SqlParameter("@_附件异常", _附件异常), new SqlParameter("@_血钾浓度", _血钾浓度), new SqlParameter("@_血钠浓度", _血钠浓度), new SqlParameter("@_糖化血红蛋白", _糖化血红蛋白), new SqlParameter("@_宫颈涂片", _宫颈涂片), new SqlParameter("@_宫颈涂片异常", _宫颈涂片异常), new SqlParameter("@_平和质", _平和质), new SqlParameter("@_气虚质", _气虚质), new SqlParameter("@_阳虚质", _阳虚质), new SqlParameter("@_阴虚质", _阴虚质), new SqlParameter("@_痰湿质", _痰湿质), new SqlParameter("@_湿热质", _湿热质), new SqlParameter("@_血瘀质", _血瘀质), new SqlParameter("@_气郁质", _气郁质), new SqlParameter("@_特禀质", _特禀质), new SqlParameter("@_脑血管疾病", _脑血管疾病), new SqlParameter("@_脑血管疾病其他", _脑血管疾病其他), new SqlParameter("@_肾脏疾病", _肾脏疾病), new SqlParameter("@_肾脏疾病其他", _肾脏疾病其他), new SqlParameter("@_心脏疾病", _心脏疾病), new SqlParameter("@_心脏疾病其他", _心脏疾病其他), new SqlParameter("@_血管疾病", _血管疾病), new SqlParameter("@_血管疾病其他", _血管疾病其他), new SqlParameter("@_眼部疾病", _眼部疾病), new SqlParameter("@_眼部疾病其他", _眼部疾病其他), new SqlParameter("@_神经系统疾病", _神经系统疾病), new SqlParameter("@_神经系统疾病其他", _神经系统疾病其他), new SqlParameter("@_其他系统疾病", _其他系统疾病), new SqlParameter("@_其他系统疾病其他", _其他系统疾病其他), new SqlParameter("@_健康评价", _健康评价), new SqlParameter("@_健康评价异常1", _健康评价异常1), new SqlParameter("@_健康评价异常2", _健康评价异常2), new SqlParameter("@_健康评价异常3", _健康评价异常3), new SqlParameter("@_健康评价异常4", _健康评价异常4), new SqlParameter("@_健康指导", _健康指导), new SqlParameter("@_危险因素控制", _危险因素控制), new SqlParameter("@_危险因素控制体重", _危险因素控制体重), new SqlParameter("@_危险因素控制疫苗", _危险因素控制疫苗), new SqlParameter("@_危险因素控制其他", _危险因素控制其他), new SqlParameter("@_FIELD5", _FIELD5), new SqlParameter("@_症状其他", _症状其他), new SqlParameter("@_G_XYYC", _G_XYYC), new SqlParameter("@_G_XYZC", _G_XYZC), new SqlParameter("@_G_QTZHZH", _G_QTZHZH), new SqlParameter("@_缺项", _缺项), new SqlParameter("@_口唇其他", _口唇其他), new SqlParameter("@_齿列其他", _齿列其他), new SqlParameter("@_咽部其他", _咽部其他), new SqlParameter("@_YDGNQT", _YDGNQT), new SqlParameter("@_餐后2H血糖", _餐后2H血糖), new SqlParameter("@_老年人状况评估", _老年人状况评估), new SqlParameter("@_老年人自理评估", _老年人自理评估), new SqlParameter("@_粉尘", _粉尘), new SqlParameter("@_物理因素", _物理因素), new SqlParameter("@_职业病其他", _职业病其他), new SqlParameter("@_粉尘防护有无", _粉尘防护有无), new SqlParameter("@_物理防护有无", _物理防护有无), new SqlParameter("@_其他防护有无", _其他防护有无), new SqlParameter("@_粉尘防护措施", _粉尘防护措施), new SqlParameter("@_物理防护措施", _物理防护措施), new SqlParameter("@_其他防护措施", _其他防护措施), new SqlParameter("@_TNBFXJF", _TNBFXJF), new SqlParameter("@_左侧原因", _左侧原因), new SqlParameter("@_右侧原因", _右侧原因), new SqlParameter("@_尿微量白蛋白", _尿微量白蛋白), new SqlParameter("@_完整度", _完整度), new SqlParameter("@_齿列缺齿", _齿列缺齿), new SqlParameter("@_齿列龋齿", _齿列龋齿), new SqlParameter("@_齿列义齿", _齿列义齿) });

            int rst = DBManager.ExecuteProc(DBManager.Conn, "USP_健康体检", new object[] { new SqlParameter("@身份证号", _身份证号), new SqlParameter("@姓名", _姓名), new SqlParameter("@性别", _性别), new SqlParameter("@出生日期", _出生日期), new SqlParameter("@症状", _症状), new SqlParameter("@体温", _体温), new SqlParameter("@呼吸", _呼吸), new SqlParameter("@脉搏", _脉搏), new SqlParameter("@血压右侧1", _血压右侧1), new SqlParameter("@血压右侧2", _血压右侧2), new SqlParameter("@血压左侧1", _血压左侧1), new SqlParameter("@血压左侧2", _血压左侧2), new SqlParameter("@身高", _身高), new SqlParameter("@腰围", _腰围), new SqlParameter("@体重", _体重), new SqlParameter("@体重指数", _体重指数), new SqlParameter("@老年人认知", _老年人认知), new SqlParameter("@老年人情感", _老年人情感), new SqlParameter("@老年人认知分", _老年人认知分), new SqlParameter("@老年人情感分", _老年人情感分), new SqlParameter("@左眼视力", _左眼视力), new SqlParameter("@右眼视力", _右眼视力), new SqlParameter("@左眼矫正", _左眼矫正), new SqlParameter("@右眼矫正", _右眼矫正), new SqlParameter("@听力", _听力), new SqlParameter("@运动功能", _运动功能), new SqlParameter("@皮肤", _皮肤), new SqlParameter("@淋巴结", _淋巴结), new SqlParameter("@淋巴结其他", _淋巴结其他), new SqlParameter("@桶状胸", _桶状胸), new SqlParameter("@呼吸音", _呼吸音), new SqlParameter("@呼吸音异常", _呼吸音异常), new SqlParameter("@罗音", _罗音), new SqlParameter("@罗音异常", _罗音异常), new SqlParameter("@心率", _心率), new SqlParameter("@心律", _心律), new SqlParameter("@杂音", _杂音), new SqlParameter("@杂音有", _杂音有), new SqlParameter("@压痛", _压痛), new SqlParameter("@压痛有", _压痛有), new SqlParameter("@包块", _包块), new SqlParameter("@包块有", _包块有), new SqlParameter("@肝大", _肝大), new SqlParameter("@肝大有", _肝大有), new SqlParameter("@脾大", _脾大), new SqlParameter("@脾大有", _脾大有), new SqlParameter("@浊音", _浊音), new SqlParameter("@浊音有", _浊音有), new SqlParameter("@下肢水肿", _下肢水肿), new SqlParameter("@肛门指诊", _肛门指诊), new SqlParameter("@肛门指诊异常", _肛门指诊异常), new SqlParameter("@G_QLX", _G_QLX), new SqlParameter("@查体其他", _查体其他), new SqlParameter("@白细胞", _白细胞), new SqlParameter("@血红蛋白", _血红蛋白), new SqlParameter("@血小板", _血小板), new SqlParameter("@血常规其他", _血常规其他), new SqlParameter("@尿蛋白", _尿蛋白), new SqlParameter("@尿糖", _尿糖), new SqlParameter("@尿酮体", _尿酮体), new SqlParameter("@尿潜血", _尿潜血), new SqlParameter("@尿常规其他", _尿常规其他), new SqlParameter("@大便潜血", _大便潜血), new SqlParameter("@血清谷丙转氨酶", _血清谷丙转氨酶), new SqlParameter("@血清谷草转氨酶", _血清谷草转氨酶), new SqlParameter("@白蛋白", _白蛋白), new SqlParameter("@总胆红素", _总胆红素), new SqlParameter("@结合胆红素", _结合胆红素), new SqlParameter("@血清肌酐", _血清肌酐), new SqlParameter("@血尿素氮", _血尿素氮), new SqlParameter("@总胆固醇", _总胆固醇), new SqlParameter("@甘油三酯", _甘油三酯), new SqlParameter("@血清低密度脂蛋白胆固醇", _血清低密度脂蛋白胆固醇), new SqlParameter("@血清高密度脂蛋白胆固醇", _血清高密度脂蛋白胆固醇), new SqlParameter("@空腹血糖", _空腹血糖), new SqlParameter("@乙型肝炎表面抗原", _乙型肝炎表面抗原), new SqlParameter("@眼底", _眼底), new SqlParameter("@眼底异常", _眼底异常), new SqlParameter("@心电图", _心电图), new SqlParameter("@心电图异常", _心电图异常), new SqlParameter("@胸部X线片", _胸部X线片), new SqlParameter("@胸部X线片异常", _胸部X线片异常), new SqlParameter("@B超", _B超), new SqlParameter("@B超其他", _B超其他), new SqlParameter("@辅助检查其他", _辅助检查其他), new SqlParameter("@G_JLJJY", _G_JLJJY == null ? "" : _G_JLJJY), new SqlParameter("@个人档案编号", _个人档案编号), new SqlParameter("@创建机构", _创建机构), new SqlParameter("@创建时间", _创建时间), new SqlParameter("@创建人", _创建人), new SqlParameter("@修改时间", _修改时间), new SqlParameter("@修改人", _修改人), new SqlParameter("@体检日期", _体检日期), new SqlParameter("@所属机构", _所属机构), new SqlParameter("@FIELD1", _FIELD1), new SqlParameter("@FIELD2", _FIELD2), new SqlParameter("@FIELD3", _FIELD3), new SqlParameter("@FIELD4", _FIELD4), new SqlParameter("@WBC_SUP", _WBC_SUP), new SqlParameter("@PLT_SUP", _PLT_SUP), new SqlParameter("@G_TUNWEI", _G_TUNWEI), new SqlParameter("@G_YTWBZ", _G_YTWBZ), new SqlParameter("@锻炼频率", _锻炼频率), new SqlParameter("@每次锻炼时间", _每次锻炼时间), new SqlParameter("@坚持锻炼时间", _坚持锻炼时间), new SqlParameter("@锻炼方式", _锻炼方式), new SqlParameter("@饮食习惯", _饮食习惯), new SqlParameter("@吸烟状况", _吸烟状况), new SqlParameter("@日吸烟量", _日吸烟量), new SqlParameter("@开始吸烟年龄", _开始吸烟年龄), new SqlParameter("@戒烟年龄", _戒烟年龄), new SqlParameter("@饮酒频率", _饮酒频率), new SqlParameter("@日饮酒量", _日饮酒量), new SqlParameter("@是否戒酒", _是否戒酒), new SqlParameter("@戒酒年龄", _戒酒年龄), new SqlParameter("@开始饮酒年龄", _开始饮酒年龄), new SqlParameter("@近一年内是否曾醉酒", _近一年内是否曾醉酒), new SqlParameter("@饮酒种类", _饮酒种类), new SqlParameter("@饮酒种类其它", _饮酒种类其它), new SqlParameter("@有无职业病", _有无职业病), new SqlParameter("@具体职业", _具体职业), new SqlParameter("@从业时间", _从业时间), new SqlParameter("@化学物质", _化学物质), new SqlParameter("@化学物质防护", _化学物质防护), new SqlParameter("@化学物质具体防护", _化学物质具体防护), new SqlParameter("@毒物", _毒物), new SqlParameter("@G_DWFHCS", _G_DWFHCS), new SqlParameter("@G_DWFHCSQT", _G_DWFHCSQT), new SqlParameter("@放射物质", _放射物质), new SqlParameter("@放射物质防护措施有无", _放射物质防护措施有无), new SqlParameter("@放射物质防护措施其他", _放射物质防护措施其他), new SqlParameter("@口唇", _口唇), new SqlParameter("@齿列", _齿列), new SqlParameter("@咽部", _咽部), new SqlParameter("@皮肤其他", _皮肤其他), new SqlParameter("@巩膜", _巩膜), new SqlParameter("@巩膜其他", _巩膜其他), new SqlParameter("@足背动脉搏动", _足背动脉搏动), new SqlParameter("@乳腺", _乳腺), new SqlParameter("@乳腺其他", _乳腺其他), new SqlParameter("@外阴", _外阴), new SqlParameter("@外阴异常", _外阴异常), new SqlParameter("@阴道", _阴道), new SqlParameter("@阴道异常", _阴道异常), new SqlParameter("@宫颈", _宫颈), new SqlParameter("@宫颈异常", _宫颈异常), new SqlParameter("@宫体", _宫体), new SqlParameter("@宫体异常", _宫体异常), new SqlParameter("@附件", _附件), new SqlParameter("@附件异常", _附件异常), new SqlParameter("@血钾浓度", _血钾浓度), new SqlParameter("@血钠浓度", _血钠浓度), new SqlParameter("@糖化血红蛋白", _糖化血红蛋白), new SqlParameter("@宫颈涂片", _宫颈涂片), new SqlParameter("@宫颈涂片异常", _宫颈涂片异常), new SqlParameter("@平和质", _平和质), new SqlParameter("@气虚质", _气虚质), new SqlParameter("@阳虚质", _阳虚质), new SqlParameter("@阴虚质", _阴虚质), new SqlParameter("@痰湿质", _痰湿质), new SqlParameter("@湿热质", _湿热质), new SqlParameter("@血瘀质", _血瘀质), new SqlParameter("@气郁质", _气郁质), new SqlParameter("@特禀质", _特禀质), new SqlParameter("@脑血管疾病", _脑血管疾病), new SqlParameter("@脑血管疾病其他", _脑血管疾病其他), new SqlParameter("@肾脏疾病", _肾脏疾病), new SqlParameter("@肾脏疾病其他", _肾脏疾病其他), new SqlParameter("@心脏疾病", _心脏疾病), new SqlParameter("@心脏疾病其他", _心脏疾病其他), new SqlParameter("@血管疾病", _血管疾病), new SqlParameter("@血管疾病其他", _血管疾病其他), new SqlParameter("@眼部疾病", _眼部疾病), new SqlParameter("@眼部疾病其他", _眼部疾病其他), new SqlParameter("@神经系统疾病", _神经系统疾病), new SqlParameter("@神经系统疾病其他", _神经系统疾病其他), new SqlParameter("@其他系统疾病", _其他系统疾病), new SqlParameter("@其他系统疾病其他", _其他系统疾病其他), new SqlParameter("@健康评价", _健康评价), new SqlParameter("@健康评价异常1", _健康评价异常1), new SqlParameter("@健康评价异常2", _健康评价异常2), new SqlParameter("@健康评价异常3", _健康评价异常3), new SqlParameter("@健康评价异常4", _健康评价异常4), new SqlParameter("@健康指导", _健康指导), new SqlParameter("@危险因素控制", _危险因素控制), new SqlParameter("@危险因素控制体重", _危险因素控制体重), new SqlParameter("@危险因素控制疫苗", _危险因素控制疫苗), new SqlParameter("@危险因素控制其他", _危险因素控制其他), new SqlParameter("@FIELD5", _FIELD5), new SqlParameter("@症状其他", _症状其他), new SqlParameter("@G_XYYC", _G_XYYC), new SqlParameter("@G_XYZC", _G_XYZC), new SqlParameter("@G_QTZHZH", _G_QTZHZH), new SqlParameter("@缺项", _缺项), new SqlParameter("@口唇其他", _口唇其他), new SqlParameter("@齿列其他", _齿列其他), new SqlParameter("@咽部其他", _咽部其他), new SqlParameter("@YDGNQT", _YDGNQT), new SqlParameter("@餐后2H血糖", _餐后2H血糖), new SqlParameter("@老年人状况评估", _老年人状况评估), new SqlParameter("@老年人自理评估", _老年人自理评估), new SqlParameter("@粉尘", _粉尘), new SqlParameter("@物理因素", _物理因素), new SqlParameter("@职业病其他", _职业病其他), new SqlParameter("@粉尘防护有无", _粉尘防护有无), new SqlParameter("@物理防护有无", _物理防护有无), new SqlParameter("@其他防护有无", _其他防护有无), new SqlParameter("@粉尘防护措施", _粉尘防护措施), new SqlParameter("@物理防护措施", _物理防护措施), new SqlParameter("@其他防护措施", _其他防护措施), new SqlParameter("@TNBFXJF", _TNBFXJF), new SqlParameter("@左侧原因", _左侧原因), new SqlParameter("@右侧原因", _右侧原因), new SqlParameter("@尿微量白蛋白", _尿微量白蛋白), new SqlParameter("@完整度", _完整度), new SqlParameter("@齿列缺齿", _齿列缺齿), new SqlParameter("@齿列龋齿", _齿列龋齿), new SqlParameter("@齿列义齿", _齿列义齿), new SqlParameter("@_BlueAddr", _BlueAddr), new SqlParameter("@_BlueType", _BlueType), new SqlParameter("@_BlueName", _BlueName) });
            if (rst > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        /// <summary>
        /// 单机版的数据导入到１２１的数据表上
        /// </summary>
        /// <param name="info"></param>
        ///// <returns></returns>
        //public static bool Addtb_健康体检To121(tb_健康体检Info info)
        //{
        //    string _身份证号 = info.身份证号;
        //    string _姓名 = info.姓名;
        //    string _性别 = info.性别;
        //    string _出生日期 = info.出生日期;
        //    string _症状 = info.症状;
        //    double _体温 = info.体温;
        //    int _呼吸 = info.呼吸;
        //    int _脉搏 = info.脉搏;
        //    string _血压右侧1 = info.血压右侧1;
        //    string _血压右侧2 = info.血压右侧2;
        //    string _血压左侧1 = info.血压左侧1;
        //    string _血压左侧2 = info.血压左侧2;
        //    double _身高 = info.身高;
        //    double _腰围 = info.腰围;
        //    double _体重 = info.体重;
        //    string _体重指数 = info.体重指数;
        //    string _老年人认知 = info.老年人认知;
        //    string _老年人情感 = info.老年人情感;
        //    string _老年人认知分 = info.老年人认知分;
        //    string _老年人情感分 = info.老年人情感分;
        //    double _左眼视力 = info.左眼视力;
        //    double _右眼视力 = info.右眼视力;
        //    string _左眼矫正 = info.左眼矫正;
        //    string _右眼矫正 = info.右眼矫正;
        //    string _听力 = info.听力;
        //    string _运动功能 = info.运动功能;
        //    string _皮肤 = info.皮肤;
        //    string _淋巴结 = info.淋巴结;
        //    string _淋巴结其他 = info.淋巴结其他;
        //    string _桶状胸 = info.桶状胸;
        //    string _呼吸音 = info.呼吸音;
        //    string _呼吸音异常 = info.呼吸音异常;
        //    string _罗音 = info.罗音;
        //    string _罗音异常 = info.罗音异常;
        //    int _心率 = info.心率;
        //    string _心律 = info.心律;
        //    string _杂音 = info.杂音;
        //    string _杂音有 = info.杂音有;
        //    string _压痛 = info.压痛;
        //    string _压痛有 = info.压痛有;
        //    string _包块 = info.包块;
        //    string _包块有 = info.包块有;
        //    string _肝大 = info.肝大;
        //    string _肝大有 = info.肝大有;
        //    string _脾大 = info.脾大;
        //    string _脾大有 = info.脾大有;
        //    string _浊音 = info.浊音;
        //    string _浊音有 = info.浊音有;
        //    string _下肢水肿 = info.下肢水肿;
        //    string _肛门指诊 = info.肛门指诊;
        //    string _肛门指诊异常 = info.肛门指诊异常;
        //    string _G_QLX = info.G_QLX;
        //    string _查体其他 = info.查体其他;
        //    string _白细胞 = info.白细胞;
        //    string _血红蛋白 = info.血红蛋白;
        //    string _血小板 = info.血小板;
        //    string _血常规其他 = info.血常规其他;
        //    string _尿蛋白 = info.尿蛋白;
        //    string _尿糖 = info.尿糖;
        //    string _尿酮体 = info.尿酮体;
        //    string _尿潜血 = info.尿潜血;
        //    string _尿常规其他 = info.尿常规其他;
        //    string _大便潜血 = info.大便潜血;
        //    string _血清谷丙转氨酶 = info.血清谷丙转氨酶;
        //    string _血清谷草转氨酶 = info.血清谷草转氨酶;
        //    string _白蛋白 = info.白蛋白;
        //    string _总胆红素 = info.总胆红素;
        //    string _结合胆红素 = info.结合胆红素;
        //    string _血清肌酐 = info.血清肌酐;
        //    string _血尿素氮 = info.血尿素氮;
        //    string _总胆固醇 = info.总胆固醇;
        //    string _甘油三酯 = info.甘油三酯;
        //    string _血清低密度脂蛋白胆固醇 = info.血清低密度脂蛋白胆固醇;
        //    string _血清高密度脂蛋白胆固醇 = info.血清高密度脂蛋白胆固醇;
        //    string _空腹血糖 = info.空腹血糖;
        //    string _乙型肝炎表面抗原 = info.乙型肝炎表面抗原;
        //    string _眼底 = info.眼底;
        //    string _眼底异常 = info.眼底异常;
        //    string _心电图 = info.心电图;
        //    string _心电图异常 = info.心电图异常;
        //    string _胸部X线片 = info.胸部X线片;
        //    string _胸部X线片异常 = info.胸部X线片异常;
        //    string _B超 = info.B超;
        //    string _B超其他 = info.B超其他;
        //    string _辅助检查其他 = info.辅助检查其他;
        //    string _G_JLJJY = info.G_JLJJY;
        //    string _个人档案编号 = info.个人档案编号;
        //    string _创建机构 = info.创建机构;
        //    string _创建时间 = info.创建时间;
        //    string _创建人 = info.创建人;
        //    string _修改时间 = info.修改时间;
        //    string _修改人 = info.修改人;
        //    string _体检日期 = info.体检日期;
        //    string _所属机构 = info.所属机构;
        //    string _FIELD1 = info.FIELD1;
        //    string _FIELD2 = info.FIELD2;
        //    string _FIELD3 = info.FIELD3;
        //    string _FIELD4 = info.FIELD4;
        //    int _WBC_SUP = info.WBC_SUP;
        //    int _PLT_SUP = info.PLT_SUP;
        //    string _G_TUNWEI = info.G_TUNWEI;
        //    string _G_YTWBZ = info.G_YTWBZ;
        //    string _锻炼频率 = info.锻炼频率;
        //    string _每次锻炼时间 = info.每次锻炼时间;
        //    string _坚持锻炼时间 = info.坚持锻炼时间;
        //    string _锻炼方式 = info.锻炼方式;
        //    string _饮食习惯 = info.饮食习惯;
        //    string _吸烟状况 = info.吸烟状况;
        //    string _日吸烟量 = info.日吸烟量;
        //    string _开始吸烟年龄 = info.开始吸烟年龄;
        //    string _戒烟年龄 = info.戒烟年龄;
        //    string _饮酒频率 = info.饮酒频率;
        //    string _日饮酒量 = info.日饮酒量;
        //    string _是否戒酒 = info.是否戒酒;
        //    string _戒酒年龄 = info.戒酒年龄;
        //    string _开始饮酒年龄 = info.开始饮酒年龄;
        //    string _近一年内是否曾醉酒 = info.近一年内是否曾醉酒;
        //    string _饮酒种类 = info.饮酒种类;
        //    string _饮酒种类其它 = info.饮酒种类其它;
        //    string _有无职业病 = info.有无职业病;
        //    string _具体职业 = info.具体职业;
        //    string _从业时间 = info.从业时间;
        //    string _化学物质 = info.化学物质;
        //    string _化学物质防护 = info.化学物质防护;
        //    string _化学物质具体防护 = info.化学物质具体防护;
        //    string _毒物 = info.毒物;
        //    string _G_DWFHCS = info.G_DWFHCS;
        //    string _G_DWFHCSQT = info.G_DWFHCSQT;
        //    string _放射物质 = info.放射物质;
        //    string _放射物质防护措施有无 = info.放射物质防护措施有无;
        //    string _放射物质防护措施其他 = info.放射物质防护措施其他;
        //    string _口唇 = info.口唇;
        //    string _齿列 = info.齿列;
        //    string _咽部 = info.咽部;
        //    string _皮肤其他 = info.皮肤其他;
        //    string _巩膜 = info.巩膜;
        //    string _巩膜其他 = info.巩膜其他;
        //    string _足背动脉搏动 = info.足背动脉搏动;
        //    string _乳腺 = info.乳腺;
        //    string _乳腺其他 = info.乳腺其他;
        //    string _外阴 = info.外阴;
        //    string _外阴异常 = info.外阴异常;
        //    string _阴道 = info.阴道;
        //    string _阴道异常 = info.阴道异常;
        //    string _宫颈 = info.宫颈;
        //    string _宫颈异常 = info.宫颈异常;
        //    string _宫体 = info.宫体;
        //    string _宫体异常 = info.宫体异常;
        //    string _附件 = info.附件;
        //    string _附件异常 = info.附件异常;
        //    string _血钾浓度 = info.血钾浓度;
        //    string _血钠浓度 = info.血钠浓度;
        //    string _糖化血红蛋白 = info.糖化血红蛋白;
        //    string _宫颈涂片 = info.宫颈涂片;
        //    string _宫颈涂片异常 = info.宫颈涂片异常;
        //    string _平和质 = info.平和质;
        //    string _气虚质 = info.气虚质;
        //    string _阳虚质 = info.阳虚质;
        //    string _阴虚质 = info.阴虚质;
        //    string _痰湿质 = info.痰湿质;
        //    string _湿热质 = info.湿热质;
        //    string _血瘀质 = info.血瘀质;
        //    string _气郁质 = info.气郁质;
        //    string _特禀质 = info.特禀质;
        //    string _脑血管疾病 = info.脑血管疾病;
        //    string _脑血管疾病其他 = info.脑血管疾病其他;
        //    string _肾脏疾病 = info.肾脏疾病;
        //    string _肾脏疾病其他 = info.肾脏疾病其他;
        //    string _心脏疾病 = info.心脏疾病;
        //    string _心脏疾病其他 = info.心脏疾病其他;
        //    string _血管疾病 = info.血管疾病;
        //    string _血管疾病其他 = info.血管疾病其他;
        //    string _眼部疾病 = info.眼部疾病;
        //    string _眼部疾病其他 = info.眼部疾病其他;
        //    string _神经系统疾病 = info.神经系统疾病;
        //    string _神经系统疾病其他 = info.神经系统疾病其他;
        //    string _其他系统疾病 = info.其他系统疾病;
        //    string _其他系统疾病其他 = info.其他系统疾病其他;
        //    string _健康评价 = info.健康评价;
        //    string _健康评价异常1 = info.健康评价异常1;
        //    string _健康评价异常2 = info.健康评价异常2;
        //    string _健康评价异常3 = info.健康评价异常3;
        //    string _健康评价异常4 = info.健康评价异常4;
        //    string _健康指导 = info.健康指导;
        //    string _危险因素控制 = info.危险因素控制;
        //    string _危险因素控制体重 = info.危险因素控制体重;
        //    string _危险因素控制疫苗 = info.危险因素控制疫苗;
        //    string _危险因素控制其他 = info.危险因素控制其他;
        //    string _FIELD5 = info.FIELD5;
        //    string _症状其他 = info.症状其他;
        //    string _G_XYYC = info.G_XYYC;
        //    string _G_XYZC = info.G_XYZC;
        //    string _G_QTZHZH = info.G_QTZHZH;
        //    string _缺项 = info.缺项;
        //    string _口唇其他 = info.口唇其他;
        //    string _齿列其他 = info.齿列其他;
        //    string _咽部其他 = info.咽部其他;
        //    string _YDGNQT = info.YDGNQT;
        //    string _餐后2H血糖 = info.餐后2H血糖;
        //    string _老年人状况评估 = info.老年人状况评估;
        //    string _老年人自理评估 = info.老年人自理评估;
        //    string _粉尘 = info.粉尘;
        //    string _物理因素 = info.物理因素;
        //    string _职业病其他 = info.职业病其他;
        //    string _粉尘防护有无 = info.粉尘防护有无;
        //    string _物理防护有无 = info.物理防护有无;
        //    string _其他防护有无 = info.其他防护有无;
        //    string _粉尘防护措施 = info.粉尘防护措施;
        //    string _物理防护措施 = info.物理防护措施;
        //    string _其他防护措施 = info.其他防护措施;
        //    string _TNBFXJF = info.TNBFXJF;
        //    string _左侧原因 = info.左侧原因;
        //    string _右侧原因 = info.右侧原因;
        //    string _尿微量白蛋白 = info.尿微量白蛋白;
        //    string _完整度 = info.完整度;
        //    string _齿列缺齿 = info.齿列缺齿;
        //    string _齿列龋齿 = info.齿列龋齿;
        //    string _齿列义齿 = info.齿列义齿;

        //    string sql = "INSERT INTO tb_健康体检 VALUES (@_身份证号,@_姓名,@_性别,@_出生日期,@_症状,@_体温,@_呼吸,@_脉搏,@_血压右侧1,@_血压右侧2,@_血压左侧1,@_血压左侧2,@_身高,@_腰围,@_体重,@_体重指数,@_老年人认知,@_老年人情感,@_老年人认知分,@_老年人情感分,@_左眼视力,@_右眼视力,@_左眼矫正,@_右眼矫正,@_听力,@_运动功能,@_皮肤,@_淋巴结,@_淋巴结其他,@_桶状胸,@_呼吸音,@_呼吸音异常,@_罗音,@_罗音异常,@_心率,@_心律,@_杂音,@_杂音有,@_压痛,@_压痛有,@_包块,@_包块有,@_肝大,@_肝大有,@_脾大,@_脾大有,@_浊音,@_浊音有,@_下肢水肿,@_肛门指诊,@_肛门指诊异常,@_G_QLX,@_查体其他,@_白细胞,@_血红蛋白,@_血小板,@_血常规其他,@_尿蛋白,@_尿糖,@_尿酮体,@_尿潜血,@_尿常规其他,@_大便潜血,@_血清谷丙转氨酶,@_血清谷草转氨酶,@_白蛋白,@_总胆红素,@_结合胆红素,@_血清肌酐,@_血尿素氮,@_总胆固醇,@_甘油三酯,@_血清低密度脂蛋白胆固醇,@_血清高密度脂蛋白胆固醇,@_空腹血糖,@_乙型肝炎表面抗原,@_眼底,@_眼底异常,@_心电图,@_心电图异常,@_胸部X线片,@_胸部X线片异常,@_B超,@_B超其他,@_辅助检查其他,@_G_JLJJY,@_个人档案编号,@_创建机构,@_创建时间,@_创建人,@_修改时间,@_修改人,@_体检日期,@_所属机构,@_FIELD1,@_FIELD2,@_FIELD3,@_FIELD4,@_WBC_SUP,@_PLT_SUP,@_G_TUNWEI,@_G_YTWBZ,@_锻炼频率,@_每次锻炼时间,@_坚持锻炼时间,@_锻炼方式,@_饮食习惯,@_吸烟状况,@_日吸烟量,@_开始吸烟年龄,@_戒烟年龄,@_饮酒频率,@_日饮酒量,@_是否戒酒,@_戒酒年龄,@_开始饮酒年龄,@_近一年内是否曾醉酒,@_饮酒种类,@_饮酒种类其它,@_有无职业病,@_具体职业,@_从业时间,@_化学物质,@_化学物质防护,@_化学物质具体防护,@_毒物,@_G_DWFHCS,@_G_DWFHCSQT,@_放射物质,@_放射物质防护措施有无,@_放射物质防护措施其他,@_口唇,@_齿列,@_咽部,@_皮肤其他,@_巩膜,@_巩膜其他,@_足背动脉搏动,@_乳腺,@_乳腺其他,@_外阴,@_外阴异常,@_阴道,@_阴道异常,@_宫颈,@_宫颈异常,@_宫体,@_宫体异常,@_附件,@_附件异常,@_血钾浓度,@_血钠浓度,@_糖化血红蛋白,@_宫颈涂片,@_宫颈涂片异常,@_平和质,@_气虚质,@_阳虚质,@_阴虚质,@_痰湿质,@_湿热质,@_血瘀质,@_气郁质,@_特禀质,@_脑血管疾病,@_脑血管疾病其他,@_肾脏疾病,@_肾脏疾病其他,@_心脏疾病,@_心脏疾病其他,@_血管疾病,@_血管疾病其他,@_眼部疾病,@_眼部疾病其他,@_神经系统疾病,@_神经系统疾病其他,@_其他系统疾病,@_其他系统疾病其他,@_健康评价,@_健康评价异常1,@_健康评价异常2,@_健康评价异常3,@_健康评价异常4,@_健康指导,@_危险因素控制,@_危险因素控制体重,@_危险因素控制疫苗,@_危险因素控制其他,@_FIELD5,@_症状其他,@_G_XYYC,@_G_XYZC,@_G_QTZHZH,@_缺项,@_口唇其他,@_齿列其他,@_咽部其他,@_YDGNQT,@_餐后2H血糖,@_老年人状况评估,@_老年人自理评估,@_粉尘,@_物理因素,@_职业病其他,@_粉尘防护有无,@_物理防护有无,@_其他防护有无,@_粉尘防护措施,@_物理防护措施,@_其他防护措施,@_TNBFXJF,@_左侧原因,@_右侧原因,@_尿微量白蛋白,@_完整度,@_齿列缺齿,@_齿列龋齿,@_齿列义齿)";

        //    int rst = DBManager.ExecuteUpdate(DBManager.Conn121, sql, new object[] { new SqlParameter("@_身份证号", _身份证号), new SqlParameter("@_姓名", _姓名), new SqlParameter("@_性别", _性别), new SqlParameter("@_出生日期", _出生日期), new SqlParameter("@_症状", _症状), new SqlParameter("@_体温", _体温), new SqlParameter("@_呼吸", _呼吸), new SqlParameter("@_脉搏", _脉搏), new SqlParameter("@_血压右侧1", _血压右侧1), new SqlParameter("@_血压右侧2", _血压右侧2), new SqlParameter("@_血压左侧1", _血压左侧1), new SqlParameter("@_血压左侧2", _血压左侧2), new SqlParameter("@_身高", _身高), new SqlParameter("@_腰围", _腰围), new SqlParameter("@_体重", _体重), new SqlParameter("@_体重指数", _体重指数), new SqlParameter("@_老年人认知", _老年人认知), new SqlParameter("@_老年人情感", _老年人情感), new SqlParameter("@_老年人认知分", _老年人认知分), new SqlParameter("@_老年人情感分", _老年人情感分), new SqlParameter("@_左眼视力", _左眼视力), new SqlParameter("@_右眼视力", _右眼视力), new SqlParameter("@_左眼矫正", _左眼矫正), new SqlParameter("@_右眼矫正", _右眼矫正), new SqlParameter("@_听力", _听力), new SqlParameter("@_运动功能", _运动功能), new SqlParameter("@_皮肤", _皮肤), new SqlParameter("@_淋巴结", _淋巴结), new SqlParameter("@_淋巴结其他", _淋巴结其他), new SqlParameter("@_桶状胸", _桶状胸), new SqlParameter("@_呼吸音", _呼吸音), new SqlParameter("@_呼吸音异常", _呼吸音异常), new SqlParameter("@_罗音", _罗音), new SqlParameter("@_罗音异常", _罗音异常), new SqlParameter("@_心率", _心率), new SqlParameter("@_心律", _心律), new SqlParameter("@_杂音", _杂音), new SqlParameter("@_杂音有", _杂音有), new SqlParameter("@_压痛", _压痛), new SqlParameter("@_压痛有", _压痛有), new SqlParameter("@_包块", _包块), new SqlParameter("@_包块有", _包块有), new SqlParameter("@_肝大", _肝大), new SqlParameter("@_肝大有", _肝大有), new SqlParameter("@_脾大", _脾大), new SqlParameter("@_脾大有", _脾大有), new SqlParameter("@_浊音", _浊音), new SqlParameter("@_浊音有", _浊音有), new SqlParameter("@_下肢水肿", _下肢水肿), new SqlParameter("@_肛门指诊", _肛门指诊), new SqlParameter("@_肛门指诊异常", _肛门指诊异常), new SqlParameter("@_G_QLX", _G_QLX), new SqlParameter("@_查体其他", _查体其他), new SqlParameter("@_白细胞", _白细胞), new SqlParameter("@_血红蛋白", _血红蛋白), new SqlParameter("@_血小板", _血小板), new SqlParameter("@_血常规其他", _血常规其他), new SqlParameter("@_尿蛋白", _尿蛋白), new SqlParameter("@_尿糖", _尿糖), new SqlParameter("@_尿酮体", _尿酮体), new SqlParameter("@_尿潜血", _尿潜血), new SqlParameter("@_尿常规其他", _尿常规其他), new SqlParameter("@_大便潜血", _大便潜血), new SqlParameter("@_血清谷丙转氨酶", _血清谷丙转氨酶), new SqlParameter("@_血清谷草转氨酶", _血清谷草转氨酶), new SqlParameter("@_白蛋白", _白蛋白), new SqlParameter("@_总胆红素", _总胆红素), new SqlParameter("@_结合胆红素", _结合胆红素), new SqlParameter("@_血清肌酐", _血清肌酐), new SqlParameter("@_血尿素氮", _血尿素氮), new SqlParameter("@_总胆固醇", _总胆固醇), new SqlParameter("@_甘油三酯", _甘油三酯), new SqlParameter("@_血清低密度脂蛋白胆固醇", _血清低密度脂蛋白胆固醇), new SqlParameter("@_血清高密度脂蛋白胆固醇", _血清高密度脂蛋白胆固醇), new SqlParameter("@_空腹血糖", _空腹血糖), new SqlParameter("@_乙型肝炎表面抗原", _乙型肝炎表面抗原), new SqlParameter("@_眼底", _眼底), new SqlParameter("@_眼底异常", _眼底异常), new SqlParameter("@_心电图", _心电图), new SqlParameter("@_心电图异常", _心电图异常), new SqlParameter("@_胸部X线片", _胸部X线片), new SqlParameter("@_胸部X线片异常", _胸部X线片异常), new SqlParameter("@_B超", _B超), new SqlParameter("@_B超其他", _B超其他), new SqlParameter("@_辅助检查其他", _辅助检查其他), new SqlParameter("@_G_JLJJY", _G_JLJJY == null ? "" : _G_JLJJY), new SqlParameter("@_个人档案编号", _个人档案编号), new SqlParameter("@_创建机构", _创建机构), new SqlParameter("@_创建时间", _创建时间), new SqlParameter("@_创建人", _创建人), new SqlParameter("@_修改时间", _修改时间), new SqlParameter("@_修改人", _修改人), new SqlParameter("@_体检日期", _体检日期), new SqlParameter("@_所属机构", _所属机构), new SqlParameter("@_FIELD1", _FIELD1), new SqlParameter("@_FIELD2", _FIELD2), new SqlParameter("@_FIELD3", _FIELD3), new SqlParameter("@_FIELD4", _FIELD4), new SqlParameter("@_WBC_SUP", _WBC_SUP), new SqlParameter("@_PLT_SUP", _PLT_SUP), new SqlParameter("@_G_TUNWEI", _G_TUNWEI), new SqlParameter("@_G_YTWBZ", _G_YTWBZ), new SqlParameter("@_锻炼频率", _锻炼频率), new SqlParameter("@_每次锻炼时间", _每次锻炼时间), new SqlParameter("@_坚持锻炼时间", _坚持锻炼时间), new SqlParameter("@_锻炼方式", _锻炼方式), new SqlParameter("@_饮食习惯", _饮食习惯), new SqlParameter("@_吸烟状况", _吸烟状况), new SqlParameter("@_日吸烟量", _日吸烟量), new SqlParameter("@_开始吸烟年龄", _开始吸烟年龄), new SqlParameter("@_戒烟年龄", _戒烟年龄), new SqlParameter("@_饮酒频率", _饮酒频率), new SqlParameter("@_日饮酒量", _日饮酒量), new SqlParameter("@_是否戒酒", _是否戒酒), new SqlParameter("@_戒酒年龄", _戒酒年龄), new SqlParameter("@_开始饮酒年龄", _开始饮酒年龄), new SqlParameter("@_近一年内是否曾醉酒", _近一年内是否曾醉酒), new SqlParameter("@_饮酒种类", _饮酒种类), new SqlParameter("@_饮酒种类其它", _饮酒种类其它), new SqlParameter("@_有无职业病", _有无职业病), new SqlParameter("@_具体职业", _具体职业), new SqlParameter("@_从业时间", _从业时间), new SqlParameter("@_化学物质", _化学物质), new SqlParameter("@_化学物质防护", _化学物质防护), new SqlParameter("@_化学物质具体防护", _化学物质具体防护), new SqlParameter("@_毒物", _毒物), new SqlParameter("@_G_DWFHCS", _G_DWFHCS), new SqlParameter("@_G_DWFHCSQT", _G_DWFHCSQT), new SqlParameter("@_放射物质", _放射物质), new SqlParameter("@_放射物质防护措施有无", _放射物质防护措施有无), new SqlParameter("@_放射物质防护措施其他", _放射物质防护措施其他), new SqlParameter("@_口唇", _口唇), new SqlParameter("@_齿列", _齿列), new SqlParameter("@_咽部", _咽部), new SqlParameter("@_皮肤其他", _皮肤其他), new SqlParameter("@_巩膜", _巩膜), new SqlParameter("@_巩膜其他", _巩膜其他), new SqlParameter("@_足背动脉搏动", _足背动脉搏动), new SqlParameter("@_乳腺", _乳腺), new SqlParameter("@_乳腺其他", _乳腺其他), new SqlParameter("@_外阴", _外阴), new SqlParameter("@_外阴异常", _外阴异常), new SqlParameter("@_阴道", _阴道), new SqlParameter("@_阴道异常", _阴道异常), new SqlParameter("@_宫颈", _宫颈), new SqlParameter("@_宫颈异常", _宫颈异常), new SqlParameter("@_宫体", _宫体), new SqlParameter("@_宫体异常", _宫体异常), new SqlParameter("@_附件", _附件), new SqlParameter("@_附件异常", _附件异常), new SqlParameter("@_血钾浓度", _血钾浓度), new SqlParameter("@_血钠浓度", _血钠浓度), new SqlParameter("@_糖化血红蛋白", _糖化血红蛋白), new SqlParameter("@_宫颈涂片", _宫颈涂片), new SqlParameter("@_宫颈涂片异常", _宫颈涂片异常), new SqlParameter("@_平和质", _平和质), new SqlParameter("@_气虚质", _气虚质), new SqlParameter("@_阳虚质", _阳虚质), new SqlParameter("@_阴虚质", _阴虚质), new SqlParameter("@_痰湿质", _痰湿质), new SqlParameter("@_湿热质", _湿热质), new SqlParameter("@_血瘀质", _血瘀质), new SqlParameter("@_气郁质", _气郁质), new SqlParameter("@_特禀质", _特禀质), new SqlParameter("@_脑血管疾病", _脑血管疾病), new SqlParameter("@_脑血管疾病其他", _脑血管疾病其他), new SqlParameter("@_肾脏疾病", _肾脏疾病), new SqlParameter("@_肾脏疾病其他", _肾脏疾病其他), new SqlParameter("@_心脏疾病", _心脏疾病), new SqlParameter("@_心脏疾病其他", _心脏疾病其他), new SqlParameter("@_血管疾病", _血管疾病), new SqlParameter("@_血管疾病其他", _血管疾病其他), new SqlParameter("@_眼部疾病", _眼部疾病), new SqlParameter("@_眼部疾病其他", _眼部疾病其他), new SqlParameter("@_神经系统疾病", _神经系统疾病), new SqlParameter("@_神经系统疾病其他", _神经系统疾病其他), new SqlParameter("@_其他系统疾病", _其他系统疾病), new SqlParameter("@_其他系统疾病其他", _其他系统疾病其他), new SqlParameter("@_健康评价", _健康评价), new SqlParameter("@_健康评价异常1", _健康评价异常1), new SqlParameter("@_健康评价异常2", _健康评价异常2), new SqlParameter("@_健康评价异常3", _健康评价异常3), new SqlParameter("@_健康评价异常4", _健康评价异常4), new SqlParameter("@_健康指导", _健康指导), new SqlParameter("@_危险因素控制", _危险因素控制), new SqlParameter("@_危险因素控制体重", _危险因素控制体重), new SqlParameter("@_危险因素控制疫苗", _危险因素控制疫苗), new SqlParameter("@_危险因素控制其他", _危险因素控制其他), new SqlParameter("@_FIELD5", _FIELD5), new SqlParameter("@_症状其他", _症状其他), new SqlParameter("@_G_XYYC", _G_XYYC), new SqlParameter("@_G_XYZC", _G_XYZC), new SqlParameter("@_G_QTZHZH", _G_QTZHZH), new SqlParameter("@_缺项", _缺项), new SqlParameter("@_口唇其他", _口唇其他), new SqlParameter("@_齿列其他", _齿列其他), new SqlParameter("@_咽部其他", _咽部其他), new SqlParameter("@_YDGNQT", _YDGNQT), new SqlParameter("@_餐后2H血糖", _餐后2H血糖), new SqlParameter("@_老年人状况评估", _老年人状况评估), new SqlParameter("@_老年人自理评估", _老年人自理评估), new SqlParameter("@_粉尘", _粉尘), new SqlParameter("@_物理因素", _物理因素), new SqlParameter("@_职业病其他", _职业病其他), new SqlParameter("@_粉尘防护有无", _粉尘防护有无), new SqlParameter("@_物理防护有无", _物理防护有无), new SqlParameter("@_其他防护有无", _其他防护有无), new SqlParameter("@_粉尘防护措施", _粉尘防护措施), new SqlParameter("@_物理防护措施", _物理防护措施), new SqlParameter("@_其他防护措施", _其他防护措施), new SqlParameter("@_TNBFXJF", _TNBFXJF), new SqlParameter("@_左侧原因", _左侧原因), new SqlParameter("@_右侧原因", _右侧原因), new SqlParameter("@_尿微量白蛋白", _尿微量白蛋白), new SqlParameter("@_完整度", _完整度), new SqlParameter("@_齿列缺齿", _齿列缺齿), new SqlParameter("@_齿列龋齿", _齿列龋齿), new SqlParameter("@_齿列义齿", _齿列义齿) });

        //    if (rst > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }

        //}
        public static bool Addtb_健康体检To121(tb_健康体检Info info)
        {
            string _身份证号 = info.身份证号;
            string _姓名 = info.姓名;
            string _性别 = info.性别;
            string _出生日期 = info.出生日期;
            string _症状 = info.症状;
            double _体温 = info.体温;
            int _呼吸 = info.呼吸;
            int _脉搏 = info.脉搏;
            string _血压右侧1 = info.血压右侧1;
            string _血压右侧2 = info.血压右侧2;
            string _血压左侧1 = info.血压左侧1;
            string _血压左侧2 = info.血压左侧2;
            double _身高 = info.身高;
            double _腰围 = info.腰围;
            double _体重 = info.体重;
            string _体重指数 = info.体重指数;
            string _老年人认知 = info.老年人认知;
            string _老年人情感 = info.老年人情感;
            string _老年人认知分 = info.老年人认知分;
            string _老年人情感分 = info.老年人情感分;
            double _左眼视力 = info.左眼视力;
            double _右眼视力 = info.右眼视力;
            string _左眼矫正 = info.左眼矫正;
            string _右眼矫正 = info.右眼矫正;
            string _听力 = info.听力;
            string _运动功能 = info.运动功能;
            string _皮肤 = info.皮肤;
            string _淋巴结 = info.淋巴结;
            string _淋巴结其他 = info.淋巴结其他;
            string _桶状胸 = info.桶状胸;
            string _呼吸音 = info.呼吸音;
            string _呼吸音异常 = info.呼吸音异常;
            string _罗音 = info.罗音;
            string _罗音异常 = info.罗音异常;
            int _心率 = info.心率;
            string _心律 = info.心律;
            string _杂音 = info.杂音;
            string _杂音有 = info.杂音有;
            string _压痛 = info.压痛;
            string _压痛有 = info.压痛有;
            string _包块 = info.包块;
            string _包块有 = info.包块有;
            string _肝大 = info.肝大;
            string _肝大有 = info.肝大有;
            string _脾大 = info.脾大;
            string _脾大有 = info.脾大有;
            string _浊音 = info.浊音;
            string _浊音有 = info.浊音有;
            string _下肢水肿 = info.下肢水肿;
            string _肛门指诊 = info.肛门指诊;
            string _肛门指诊异常 = info.肛门指诊异常;
            string _G_QLX = info.G_QLX;
            string _查体其他 = info.查体其他;
            string _白细胞 = info.白细胞;
            string _血红蛋白 = info.血红蛋白;
            string _血小板 = info.血小板;
            string _血常规其他 = info.血常规其他;
            string _尿蛋白 = info.尿蛋白;
            string _尿糖 = info.尿糖;
            string _尿酮体 = info.尿酮体;
            string _尿潜血 = info.尿潜血;
            string _尿常规其他 = info.尿常规其他;
            string _大便潜血 = info.大便潜血;
            string _血清谷丙转氨酶 = info.血清谷丙转氨酶;
            string _血清谷草转氨酶 = info.血清谷草转氨酶;
            string _白蛋白 = info.白蛋白;
            string _总胆红素 = info.总胆红素;
            string _结合胆红素 = info.结合胆红素;
            string _血清肌酐 = info.血清肌酐;
            string _血尿素氮 = info.血尿素氮;
            string _总胆固醇 = info.总胆固醇;
            string _甘油三酯 = info.甘油三酯;
            string _血清低密度脂蛋白胆固醇 = info.血清低密度脂蛋白胆固醇;
            string _血清高密度脂蛋白胆固醇 = info.血清高密度脂蛋白胆固醇;
            string _空腹血糖 = info.空腹血糖;
            string _乙型肝炎表面抗原 = info.乙型肝炎表面抗原;
            string _眼底 = info.眼底;
            string _眼底异常 = info.眼底异常;
            string _心电图 = info.心电图;
            string _心电图异常 = info.心电图异常;
            string _胸部X线片 = info.胸部X线片;
            string _胸部X线片异常 = info.胸部X线片异常;
            string _B超 = info.B超;
            string _B超其他 = info.B超其他;
            string _辅助检查其他 = info.辅助检查其他;
            string _G_JLJJY = info.G_JLJJY;
            string _个人档案编号 = info.个人档案编号;
            string _创建机构 = info.创建机构;
            string _创建时间 = info.创建时间;
            string _创建人 = info.创建人;
            string _修改时间 = info.修改时间;
            string _修改人 = info.修改人;
            string _体检日期 = info.体检日期;
            string _所属机构 = info.所属机构;
            string _FIELD1 = info.FIELD1;
            string _FIELD2 = info.FIELD2;
            string _FIELD3 = info.FIELD3;
            string _FIELD4 = info.FIELD4;
            int _WBC_SUP = info.WBC_SUP;
            int _PLT_SUP = info.PLT_SUP;
            string _G_TUNWEI = info.G_TUNWEI;
            string _G_YTWBZ = info.G_YTWBZ;
            string _锻炼频率 = info.锻炼频率;
            string _每次锻炼时间 = info.每次锻炼时间;
            string _坚持锻炼时间 = info.坚持锻炼时间;
            string _锻炼方式 = info.锻炼方式;
            string _饮食习惯 = info.饮食习惯;
            string _吸烟状况 = info.吸烟状况;
            string _日吸烟量 = info.日吸烟量;
            string _开始吸烟年龄 = info.开始吸烟年龄;
            string _戒烟年龄 = info.戒烟年龄;
            string _饮酒频率 = info.饮酒频率;
            string _日饮酒量 = info.日饮酒量;
            string _是否戒酒 = info.是否戒酒;
            string _戒酒年龄 = info.戒酒年龄;
            string _开始饮酒年龄 = info.开始饮酒年龄;
            string _近一年内是否曾醉酒 = info.近一年内是否曾醉酒;
            string _饮酒种类 = info.饮酒种类;
            string _饮酒种类其它 = info.饮酒种类其它;
            string _有无职业病 = info.有无职业病;
            string _具体职业 = info.具体职业;
            string _从业时间 = info.从业时间;
            string _化学物质 = info.化学物质;
            string _化学物质防护 = info.化学物质防护;
            string _化学物质具体防护 = info.化学物质具体防护;
            string _毒物 = info.毒物;
            string _G_DWFHCS = info.G_DWFHCS;
            string _G_DWFHCSQT = info.G_DWFHCSQT;
            string _放射物质 = info.放射物质;
            string _放射物质防护措施有无 = info.放射物质防护措施有无;
            string _放射物质防护措施其他 = info.放射物质防护措施其他;
            string _口唇 = info.口唇;
            string _齿列 = info.齿列;
            string _咽部 = info.咽部;
            string _皮肤其他 = info.皮肤其他;
            string _巩膜 = info.巩膜;
            string _巩膜其他 = info.巩膜其他;
            string _足背动脉搏动 = info.足背动脉搏动;
            string _乳腺 = info.乳腺;
            string _乳腺其他 = info.乳腺其他;
            string _外阴 = info.外阴;
            string _外阴异常 = info.外阴异常;
            string _阴道 = info.阴道;
            string _阴道异常 = info.阴道异常;
            string _宫颈 = info.宫颈;
            string _宫颈异常 = info.宫颈异常;
            string _宫体 = info.宫体;
            string _宫体异常 = info.宫体异常;
            string _附件 = info.附件;
            string _附件异常 = info.附件异常;
            string _血钾浓度 = info.血钾浓度;
            string _血钠浓度 = info.血钠浓度;
            string _糖化血红蛋白 = info.糖化血红蛋白;
            string _宫颈涂片 = info.宫颈涂片;
            string _宫颈涂片异常 = info.宫颈涂片异常;
            string _平和质 = info.平和质;
            string _气虚质 = info.气虚质;
            string _阳虚质 = info.阳虚质;
            string _阴虚质 = info.阴虚质;
            string _痰湿质 = info.痰湿质;
            string _湿热质 = info.湿热质;
            string _血瘀质 = info.血瘀质;
            string _气郁质 = info.气郁质;
            string _特禀质 = info.特禀质;
            string _脑血管疾病 = info.脑血管疾病;
            string _脑血管疾病其他 = info.脑血管疾病其他;
            string _肾脏疾病 = info.肾脏疾病;
            string _肾脏疾病其他 = info.肾脏疾病其他;
            string _心脏疾病 = info.心脏疾病;
            string _心脏疾病其他 = info.心脏疾病其他;
            string _血管疾病 = info.血管疾病;
            string _血管疾病其他 = info.血管疾病其他;
            string _眼部疾病 = info.眼部疾病;
            string _眼部疾病其他 = info.眼部疾病其他;
            string _神经系统疾病 = info.神经系统疾病;
            string _神经系统疾病其他 = info.神经系统疾病其他;
            string _其他系统疾病 = info.其他系统疾病;
            string _其他系统疾病其他 = info.其他系统疾病其他;
            string _健康评价 = info.健康评价;
            string _健康评价异常1 = info.健康评价异常1;
            string _健康评价异常2 = info.健康评价异常2;
            string _健康评价异常3 = info.健康评价异常3;
            string _健康评价异常4 = info.健康评价异常4;
            string _健康指导 = info.健康指导;
            string _危险因素控制 = info.危险因素控制;
            string _危险因素控制体重 = info.危险因素控制体重;
            string _危险因素控制疫苗 = info.危险因素控制疫苗;
            string _危险因素控制其他 = info.危险因素控制其他;
            string _FIELD5 = info.FIELD5;
            string _症状其他 = info.症状其他;
            string _G_XYYC = info.G_XYYC;
            string _G_XYZC = info.G_XYZC;
            string _G_QTZHZH = info.G_QTZHZH;
            string _缺项 = info.缺项;
            string _口唇其他 = info.口唇其他;
            string _齿列其他 = info.齿列其他;
            string _咽部其他 = info.咽部其他;
            string _YDGNQT = info.YDGNQT;
            string _餐后2H血糖 = info.餐后2H血糖;
            string _老年人状况评估 = info.老年人状况评估;
            string _老年人自理评估 = info.老年人自理评估;
            string _粉尘 = info.粉尘;
            string _物理因素 = info.物理因素;
            string _职业病其他 = info.职业病其他;
            string _粉尘防护有无 = info.粉尘防护有无;
            string _物理防护有无 = info.物理防护有无;
            string _其他防护有无 = info.其他防护有无;
            string _粉尘防护措施 = info.粉尘防护措施;
            string _物理防护措施 = info.物理防护措施;
            string _其他防护措施 = info.其他防护措施;
            string _TNBFXJF = info.TNBFXJF;
            string _左侧原因 = info.左侧原因;
            string _右侧原因 = info.右侧原因;
            string _尿微量白蛋白 = info.尿微量白蛋白;
            string _完整度 = info.完整度;
            string _齿列缺齿 = info.齿列缺齿;
            string _齿列龋齿 = info.齿列龋齿;
            string _齿列义齿 = info.齿列义齿;
            string _BlueName = info.BlueName;
            string _BlueAddr = info.BlueAddr;
            string _BlueType = info.BlueType;
            string _医师签字 = info.医师签字;

            int rst = DBManager.ExecuteProc(DBManager.Conn121, "USP_IMPORT健康体检TO121", new object[] { new SqlParameter("@身份证号", _身份证号), new SqlParameter("@姓名", _姓名), new SqlParameter("@性别", _性别), new SqlParameter("@出生日期", _出生日期), new SqlParameter("@症状", _症状), new SqlParameter("@体温", _体温), new SqlParameter("@呼吸", _呼吸), new SqlParameter("@脉搏", _脉搏), new SqlParameter("@血压右侧1", _血压右侧1), new SqlParameter("@血压右侧2", _血压右侧2), new SqlParameter("@血压左侧1", _血压左侧1), new SqlParameter("@血压左侧2", _血压左侧2), new SqlParameter("@身高", _身高), new SqlParameter("@腰围", _腰围), new SqlParameter("@体重", _体重), new SqlParameter("@体重指数", _体重指数), new SqlParameter("@老年人认知", _老年人认知), new SqlParameter("@老年人情感", _老年人情感), new SqlParameter("@老年人认知分", _老年人认知分), new SqlParameter("@老年人情感分", _老年人情感分), new SqlParameter("@左眼视力", _左眼视力), new SqlParameter("@右眼视力", _右眼视力), new SqlParameter("@左眼矫正", _左眼矫正), new SqlParameter("@右眼矫正", _右眼矫正), new SqlParameter("@听力", _听力), new SqlParameter("@运动功能", _运动功能), new SqlParameter("@皮肤", _皮肤), new SqlParameter("@淋巴结", _淋巴结), new SqlParameter("@淋巴结其他", _淋巴结其他), new SqlParameter("@桶状胸", _桶状胸), new SqlParameter("@呼吸音", _呼吸音), new SqlParameter("@呼吸音异常", _呼吸音异常), new SqlParameter("@罗音", _罗音), new SqlParameter("@罗音异常", _罗音异常), new SqlParameter("@心率", _心率), new SqlParameter("@心律", _心律), new SqlParameter("@杂音", _杂音), new SqlParameter("@杂音有", _杂音有), new SqlParameter("@压痛", _压痛), new SqlParameter("@压痛有", _压痛有), new SqlParameter("@包块", _包块), new SqlParameter("@包块有", _包块有), new SqlParameter("@肝大", _肝大), new SqlParameter("@肝大有", _肝大有), new SqlParameter("@脾大", _脾大), new SqlParameter("@脾大有", _脾大有), new SqlParameter("@浊音", _浊音), new SqlParameter("@浊音有", _浊音有), new SqlParameter("@下肢水肿", _下肢水肿), new SqlParameter("@肛门指诊", _肛门指诊), new SqlParameter("@肛门指诊异常", _肛门指诊异常), new SqlParameter("@G_QLX", _G_QLX), new SqlParameter("@查体其他", _查体其他), new SqlParameter("@白细胞", _白细胞), new SqlParameter("@血红蛋白", _血红蛋白), new SqlParameter("@血小板", _血小板), new SqlParameter("@血常规其他", _血常规其他), new SqlParameter("@尿蛋白", _尿蛋白), new SqlParameter("@尿糖", _尿糖), new SqlParameter("@尿酮体", _尿酮体), new SqlParameter("@尿潜血", _尿潜血), new SqlParameter("@尿常规其他", _尿常规其他), new SqlParameter("@大便潜血", _大便潜血), new SqlParameter("@血清谷丙转氨酶", _血清谷丙转氨酶), new SqlParameter("@血清谷草转氨酶", _血清谷草转氨酶), new SqlParameter("@白蛋白", _白蛋白), new SqlParameter("@总胆红素", _总胆红素), new SqlParameter("@结合胆红素", _结合胆红素), new SqlParameter("@血清肌酐", _血清肌酐), new SqlParameter("@血尿素氮", _血尿素氮), new SqlParameter("@总胆固醇", _总胆固醇), new SqlParameter("@甘油三酯", _甘油三酯), new SqlParameter("@血清低密度脂蛋白胆固醇", _血清低密度脂蛋白胆固醇), new SqlParameter("@血清高密度脂蛋白胆固醇", _血清高密度脂蛋白胆固醇), new SqlParameter("@空腹血糖", _空腹血糖), new SqlParameter("@乙型肝炎表面抗原", _乙型肝炎表面抗原), new SqlParameter("@眼底", _眼底), new SqlParameter("@眼底异常", _眼底异常), new SqlParameter("@心电图", _心电图), new SqlParameter("@心电图异常", _心电图异常), new SqlParameter("@胸部X线片", _胸部X线片), new SqlParameter("@胸部X线片异常", _胸部X线片异常), new SqlParameter("@B超", _B超), new SqlParameter("@B超其他", _B超其他), new SqlParameter("@辅助检查其他", _辅助检查其他), new SqlParameter("@G_JLJJY", _G_JLJJY == null ? "" : _G_JLJJY), new SqlParameter("@个人档案编号", _个人档案编号), new SqlParameter("@创建机构", _创建机构), new SqlParameter("@创建时间", _创建时间), new SqlParameter("@创建人", _创建人), new SqlParameter("@修改时间", _修改时间), new SqlParameter("@修改人", _修改人), new SqlParameter("@体检日期", _体检日期), new SqlParameter("@所属机构", _所属机构), new SqlParameter("@FIELD1", _FIELD1), new SqlParameter("@FIELD2", _FIELD2), new SqlParameter("@FIELD3", _FIELD3), new SqlParameter("@FIELD4", _FIELD4), new SqlParameter("@WBC_SUP", _WBC_SUP), new SqlParameter("@PLT_SUP", _PLT_SUP), new SqlParameter("@G_TUNWEI", _G_TUNWEI), new SqlParameter("@G_YTWBZ", _G_YTWBZ), new SqlParameter("@锻炼频率", _锻炼频率), new SqlParameter("@每次锻炼时间", _每次锻炼时间), new SqlParameter("@坚持锻炼时间", _坚持锻炼时间), new SqlParameter("@锻炼方式", _锻炼方式), new SqlParameter("@饮食习惯", _饮食习惯), new SqlParameter("@吸烟状况", _吸烟状况), new SqlParameter("@日吸烟量", _日吸烟量), new SqlParameter("@开始吸烟年龄", _开始吸烟年龄), new SqlParameter("@戒烟年龄", _戒烟年龄), new SqlParameter("@饮酒频率", _饮酒频率), new SqlParameter("@日饮酒量", _日饮酒量), new SqlParameter("@是否戒酒", _是否戒酒), new SqlParameter("@戒酒年龄", _戒酒年龄), new SqlParameter("@开始饮酒年龄", _开始饮酒年龄), new SqlParameter("@近一年内是否曾醉酒", _近一年内是否曾醉酒), new SqlParameter("@饮酒种类", _饮酒种类), new SqlParameter("@饮酒种类其它", _饮酒种类其它), new SqlParameter("@有无职业病", _有无职业病), new SqlParameter("@具体职业", _具体职业), new SqlParameter("@从业时间", _从业时间), new SqlParameter("@化学物质", _化学物质), new SqlParameter("@化学物质防护", _化学物质防护), new SqlParameter("@化学物质具体防护", _化学物质具体防护), new SqlParameter("@毒物", _毒物), new SqlParameter("@G_DWFHCS", _G_DWFHCS), new SqlParameter("@G_DWFHCSQT", _G_DWFHCSQT), new SqlParameter("@放射物质", _放射物质), new SqlParameter("@放射物质防护措施有无", _放射物质防护措施有无), new SqlParameter("@放射物质防护措施其他", _放射物质防护措施其他), new SqlParameter("@口唇", _口唇), new SqlParameter("@齿列", _齿列), new SqlParameter("@咽部", _咽部), new SqlParameter("@皮肤其他", _皮肤其他), new SqlParameter("@巩膜", _巩膜), new SqlParameter("@巩膜其他", _巩膜其他), new SqlParameter("@足背动脉搏动", _足背动脉搏动), new SqlParameter("@乳腺", _乳腺), new SqlParameter("@乳腺其他", _乳腺其他), new SqlParameter("@外阴", _外阴), new SqlParameter("@外阴异常", _外阴异常), new SqlParameter("@阴道", _阴道), new SqlParameter("@阴道异常", _阴道异常), new SqlParameter("@宫颈", _宫颈), new SqlParameter("@宫颈异常", _宫颈异常), new SqlParameter("@宫体", _宫体), new SqlParameter("@宫体异常", _宫体异常), new SqlParameter("@附件", _附件), new SqlParameter("@附件异常", _附件异常), new SqlParameter("@血钾浓度", _血钾浓度), new SqlParameter("@血钠浓度", _血钠浓度), new SqlParameter("@糖化血红蛋白", _糖化血红蛋白), new SqlParameter("@宫颈涂片", _宫颈涂片), new SqlParameter("@宫颈涂片异常", _宫颈涂片异常), new SqlParameter("@平和质", _平和质), new SqlParameter("@气虚质", _气虚质), new SqlParameter("@阳虚质", _阳虚质), new SqlParameter("@阴虚质", _阴虚质), new SqlParameter("@痰湿质", _痰湿质), new SqlParameter("@湿热质", _湿热质), new SqlParameter("@血瘀质", _血瘀质), new SqlParameter("@气郁质", _气郁质), new SqlParameter("@特禀质", _特禀质), new SqlParameter("@脑血管疾病", _脑血管疾病), new SqlParameter("@脑血管疾病其他", _脑血管疾病其他), new SqlParameter("@肾脏疾病", _肾脏疾病), new SqlParameter("@肾脏疾病其他", _肾脏疾病其他), new SqlParameter("@心脏疾病", _心脏疾病), new SqlParameter("@心脏疾病其他", _心脏疾病其他), new SqlParameter("@血管疾病", _血管疾病), new SqlParameter("@血管疾病其他", _血管疾病其他), new SqlParameter("@眼部疾病", _眼部疾病), new SqlParameter("@眼部疾病其他", _眼部疾病其他), new SqlParameter("@神经系统疾病", _神经系统疾病), new SqlParameter("@神经系统疾病其他", _神经系统疾病其他), new SqlParameter("@其他系统疾病", _其他系统疾病), new SqlParameter("@其他系统疾病其他", _其他系统疾病其他), new SqlParameter("@健康评价", _健康评价), new SqlParameter("@健康评价异常1", _健康评价异常1), new SqlParameter("@健康评价异常2", _健康评价异常2), new SqlParameter("@健康评价异常3", _健康评价异常3), new SqlParameter("@健康评价异常4", _健康评价异常4), new SqlParameter("@健康指导", _健康指导), new SqlParameter("@危险因素控制", _危险因素控制), new SqlParameter("@危险因素控制体重", _危险因素控制体重), new SqlParameter("@危险因素控制疫苗", _危险因素控制疫苗), new SqlParameter("@危险因素控制其他", _危险因素控制其他), new SqlParameter("@FIELD5", _FIELD5), new SqlParameter("@症状其他", _症状其他), new SqlParameter("@G_XYYC", _G_XYYC), new SqlParameter("@G_XYZC", _G_XYZC), new SqlParameter("@G_QTZHZH", _G_QTZHZH), new SqlParameter("@缺项", _缺项), new SqlParameter("@口唇其他", _口唇其他), new SqlParameter("@齿列其他", _齿列其他), new SqlParameter("@咽部其他", _咽部其他), new SqlParameter("@YDGNQT", _YDGNQT), new SqlParameter("@餐后2H血糖", _餐后2H血糖), new SqlParameter("@老年人状况评估", _老年人状况评估), new SqlParameter("@老年人自理评估", _老年人自理评估), new SqlParameter("@粉尘", _粉尘), new SqlParameter("@物理因素", _物理因素), new SqlParameter("@职业病其他", _职业病其他), new SqlParameter("@粉尘防护有无", _粉尘防护有无), new SqlParameter("@物理防护有无", _物理防护有无), new SqlParameter("@其他防护有无", _其他防护有无), new SqlParameter("@粉尘防护措施", _粉尘防护措施), new SqlParameter("@物理防护措施", _物理防护措施), new SqlParameter("@其他防护措施", _其他防护措施), new SqlParameter("@TNBFXJF", _TNBFXJF), new SqlParameter("@左侧原因", _左侧原因), new SqlParameter("@右侧原因", _右侧原因), new SqlParameter("@尿微量白蛋白", _尿微量白蛋白), new SqlParameter("@完整度", _完整度), new SqlParameter("@齿列缺齿", _齿列缺齿), new SqlParameter("@齿列龋齿", _齿列龋齿), new SqlParameter("@齿列义齿", _齿列义齿), new SqlParameter("@_BlueName", _BlueName), new SqlParameter("@_BlueAddr", _BlueAddr), new SqlParameter("@_BlueType", _BlueType), new SqlParameter("@_医师签字", _医师签字) });
            if (rst > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        /// <summary>
        /// 删除一个对象
        /// </summary>
        /// <param name="ID">主键</param>
        /// <returns></returns>
        public static bool Deletetb_健康体检Info(tb_健康体检Info info)
        {
            bool rst = false;
            string sqlStr = "DELETE FROM tb_健康体检 WHERE ID=" + info.ID;
            int rows = DBManager.ExecuteUpdate(DBManager.Conn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }

        /// 更新数据
        /// </summary>
        /// <param name="info">数据表实体</param>
        public static bool Updatetb_健康体检Info(tb_健康体检Info info)
        {
            string _身份证号 = info.身份证号;
            string _姓名 = info.姓名;
            string _性别 = info.性别;
            string _出生日期 = info.出生日期;
            string _症状 = info.症状;
            double _体温 = info.体温;
            int _呼吸 = info.呼吸;
            int _脉搏 = info.脉搏;
            string _血压右侧1 = info.血压右侧1;
            string _血压右侧2 = info.血压右侧2;
            string _血压左侧1 = info.血压左侧1;
            string _血压左侧2 = info.血压左侧2;
            double _身高 = info.身高;
            double _腰围 = info.腰围;
            double _体重 = info.体重;
            string _体重指数 = info.体重指数;
            string _老年人认知 = info.老年人认知;
            string _老年人情感 = info.老年人情感;
            string _老年人认知分 = info.老年人认知分;
            string _老年人情感分 = info.老年人情感分;
            double _左眼视力 = info.左眼视力;
            double _右眼视力 = info.右眼视力;
            string _左眼矫正 = info.左眼矫正;
            string _右眼矫正 = info.右眼矫正;
            string _听力 = info.听力;
            string _运动功能 = info.运动功能;
            string _皮肤 = info.皮肤;
            string _淋巴结 = info.淋巴结;
            string _淋巴结其他 = info.淋巴结其他;
            string _桶状胸 = info.桶状胸;
            string _呼吸音 = info.呼吸音;
            string _呼吸音异常 = info.呼吸音异常;
            string _罗音 = info.罗音;
            string _罗音异常 = info.罗音异常;
            int _心率 = info.心率;
            string _心律 = info.心律;
            string _杂音 = info.杂音;
            string _杂音有 = info.杂音有;
            string _压痛 = info.压痛;
            string _压痛有 = info.压痛有;
            string _包块 = info.包块;
            string _包块有 = info.包块有;
            string _肝大 = info.肝大;
            string _肝大有 = info.肝大有;
            string _脾大 = info.脾大;
            string _脾大有 = info.脾大有;
            string _浊音 = info.浊音;
            string _浊音有 = info.浊音有;
            string _下肢水肿 = info.下肢水肿;
            string _肛门指诊 = info.肛门指诊;
            string _肛门指诊异常 = info.肛门指诊异常;
            string _G_QLX = info.G_QLX;
            string _查体其他 = info.查体其他;
            string _白细胞 = info.白细胞;
            string _血红蛋白 = info.血红蛋白;
            string _血小板 = info.血小板;
            string _血常规其他 = info.血常规其他;
            string _尿蛋白 = info.尿蛋白;
            string _尿糖 = info.尿糖;
            string _尿酮体 = info.尿酮体;
            string _尿潜血 = info.尿潜血;
            string _尿常规其他 = info.尿常规其他;
            string _大便潜血 = info.大便潜血;
            string _血清谷丙转氨酶 = info.血清谷丙转氨酶;
            string _血清谷草转氨酶 = info.血清谷草转氨酶;
            string _白蛋白 = info.白蛋白;
            string _总胆红素 = info.总胆红素;
            string _结合胆红素 = info.结合胆红素;
            string _血清肌酐 = info.血清肌酐;
            string _血尿素氮 = info.血尿素氮;
            string _总胆固醇 = info.总胆固醇;
            string _甘油三酯 = info.甘油三酯;
            string _血清低密度脂蛋白胆固醇 = info.血清低密度脂蛋白胆固醇;
            string _血清高密度脂蛋白胆固醇 = info.血清高密度脂蛋白胆固醇;
            string _空腹血糖 = info.空腹血糖;
            string _乙型肝炎表面抗原 = info.乙型肝炎表面抗原;
            string _眼底 = info.眼底;
            string _眼底异常 = info.眼底异常;
            string _心电图 = info.心电图;
            string _心电图异常 = info.心电图异常;
            string _胸部X线片 = info.胸部X线片;
            string _胸部X线片异常 = info.胸部X线片异常;
            string _B超 = info.B超;
            string _B超其他 = info.B超其他;
            string _辅助检查其他 = info.辅助检查其他;
            string _G_JLJJY = info.G_JLJJY;
            string _个人档案编号 = info.个人档案编号;
            string _创建机构 = Program.currentUser.Prgid;
            string _创建时间 = Program._currentTime;
            //TODO: 这个需要配置到文件中，后续再改
            string _创建人 = Program.currentUser.创建人; //"371323B100190001";
            string _修改时间 = Program._currentTime;
            string _修改人 = Program.currentUser.创建人; //"371323B100190001";
            string _体检日期 = Program._currentTime.Substring(0, 10);
            string _所属机构 = Program.currentUser.Prgid;
            string _FIELD1 = info.FIELD1;
            string _FIELD2 = info.FIELD2;
            string _FIELD3 = info.FIELD3;
            string _FIELD4 = info.FIELD4;
            int _WBC_SUP = info.WBC_SUP;
            int _PLT_SUP = info.PLT_SUP;
            string _G_TUNWEI = info.G_TUNWEI;
            string _G_YTWBZ = info.G_YTWBZ;
            string _锻炼频率 = info.锻炼频率;
            string _每次锻炼时间 = info.每次锻炼时间;
            string _坚持锻炼时间 = info.坚持锻炼时间;
            string _锻炼方式 = info.锻炼方式;
            string _饮食习惯 = info.饮食习惯;
            string _吸烟状况 = info.吸烟状况;
            string _日吸烟量 = info.日吸烟量;
            string _开始吸烟年龄 = info.开始吸烟年龄;
            string _戒烟年龄 = info.戒烟年龄;
            string _饮酒频率 = info.饮酒频率;
            string _日饮酒量 = info.日饮酒量;
            string _是否戒酒 = info.是否戒酒;
            string _戒酒年龄 = info.戒酒年龄;
            string _开始饮酒年龄 = info.开始饮酒年龄;
            string _近一年内是否曾醉酒 = info.近一年内是否曾醉酒;
            string _饮酒种类 = info.饮酒种类;
            string _饮酒种类其它 = info.饮酒种类其它;
            string _有无职业病 = info.有无职业病;
            string _具体职业 = info.具体职业;
            string _从业时间 = info.从业时间;
            string _化学物质 = info.化学物质;
            string _化学物质防护 = info.化学物质防护;
            string _化学物质具体防护 = info.化学物质具体防护;
            string _毒物 = info.毒物;
            string _G_DWFHCS = info.G_DWFHCS;
            string _G_DWFHCSQT = info.G_DWFHCSQT;
            string _放射物质 = info.放射物质;
            string _放射物质防护措施有无 = info.放射物质防护措施有无;
            string _放射物质防护措施其他 = info.放射物质防护措施其他;
            string _口唇 = info.口唇;
            string _齿列 = info.齿列;
            string _咽部 = info.咽部;
            string _皮肤其他 = info.皮肤其他;
            string _巩膜 = info.巩膜;
            string _巩膜其他 = info.巩膜其他;
            string _足背动脉搏动 = info.足背动脉搏动;
            string _乳腺 = info.乳腺;
            string _乳腺其他 = info.乳腺其他;
            string _外阴 = info.外阴;
            string _外阴异常 = info.外阴异常;
            string _阴道 = info.阴道;
            string _阴道异常 = info.阴道异常;
            string _宫颈 = info.宫颈;
            string _宫颈异常 = info.宫颈异常;
            string _宫体 = info.宫体;
            string _宫体异常 = info.宫体异常;
            string _附件 = info.附件;
            string _附件异常 = info.附件异常;
            string _血钾浓度 = info.血钾浓度;
            string _血钠浓度 = info.血钠浓度;
            string _糖化血红蛋白 = info.糖化血红蛋白;
            string _宫颈涂片 = info.宫颈涂片;
            string _宫颈涂片异常 = info.宫颈涂片异常;
            string _平和质 = info.平和质;
            string _气虚质 = info.气虚质;
            string _阳虚质 = info.阳虚质;
            string _阴虚质 = info.阴虚质;
            string _痰湿质 = info.痰湿质;
            string _湿热质 = info.湿热质;
            string _血瘀质 = info.血瘀质;
            string _气郁质 = info.气郁质;
            string _特禀质 = info.特禀质;
            string _脑血管疾病 = info.脑血管疾病;
            string _脑血管疾病其他 = info.脑血管疾病其他;
            string _肾脏疾病 = info.肾脏疾病;
            string _肾脏疾病其他 = info.肾脏疾病其他;
            string _心脏疾病 = info.心脏疾病;
            string _心脏疾病其他 = info.心脏疾病其他;
            string _血管疾病 = info.血管疾病;
            string _血管疾病其他 = info.血管疾病其他;
            string _眼部疾病 = info.眼部疾病;
            string _眼部疾病其他 = info.眼部疾病其他;
            string _神经系统疾病 = info.神经系统疾病;
            string _神经系统疾病其他 = info.神经系统疾病其他;
            string _其他系统疾病 = info.其他系统疾病;
            string _其他系统疾病其他 = info.其他系统疾病其他;
            string _健康评价 = info.健康评价;
            string _健康评价异常1 = info.健康评价异常1;
            string _健康评价异常2 = info.健康评价异常2;
            string _健康评价异常3 = info.健康评价异常3;
            string _健康评价异常4 = info.健康评价异常4;
            string _健康指导 = info.健康指导;
            string _危险因素控制 = info.危险因素控制;
            string _危险因素控制体重 = info.危险因素控制体重;
            string _危险因素控制疫苗 = info.危险因素控制疫苗;
            string _危险因素控制其他 = info.危险因素控制其他;
            string _FIELD5 = info.FIELD5;
            string _症状其他 = info.症状其他;
            string _G_XYYC = info.G_XYYC;
            string _G_XYZC = info.G_XYZC;
            string _G_QTZHZH = info.G_QTZHZH;
            string _缺项 = info.缺项;
            string _口唇其他 = info.口唇其他;
            string _齿列其他 = info.齿列其他;
            string _咽部其他 = info.咽部其他;
            string _YDGNQT = info.YDGNQT;
            string _餐后2H血糖 = info.餐后2H血糖;
            string _老年人状况评估 = info.老年人状况评估;
            string _老年人自理评估 = info.老年人自理评估;
            string _粉尘 = info.粉尘;
            string _物理因素 = info.物理因素;
            string _职业病其他 = info.职业病其他;
            string _粉尘防护有无 = info.粉尘防护有无;
            string _物理防护有无 = info.物理防护有无;
            string _其他防护有无 = info.其他防护有无;
            string _粉尘防护措施 = info.粉尘防护措施;
            string _物理防护措施 = info.物理防护措施;
            string _其他防护措施 = info.其他防护措施;
            string _TNBFXJF = info.TNBFXJF;
            string _左侧原因 = info.左侧原因;
            string _右侧原因 = info.右侧原因;
            string _尿微量白蛋白 = info.尿微量白蛋白;
            string _完整度 = info.完整度;
            string _齿列缺齿 = info.齿列缺齿;
            string _齿列龋齿 = info.齿列龋齿;
            string _齿列义齿 = info.齿列义齿;
            string sql = "UPDATE tb_健康体检 SET 身份证号=@_身份证号, 姓名=@_姓名, 性别=@_性别, 出生日期=@_出生日期, 症状=@_症状, 体温=@_体温, 呼吸=@_呼吸, 脉搏=@_脉搏, 血压右侧1=@_血压右侧1, 血压右侧2=@_血压右侧2, 血压左侧1=@_血压左侧1, 血压左侧2=@_血压左侧2, 身高=@_身高, 腰围=@_腰围, 体重=@_体重, 体重指数=@_体重指数, 老年人认知=@_老年人认知, 老年人情感=@_老年人情感, 老年人认知分=@_老年人认知分, 老年人情感分=@_老年人情感分, 左眼视力=@_左眼视力, 右眼视力=@_右眼视力, 左眼矫正=@_左眼矫正, 右眼矫正=@_右眼矫正, 听力=@_听力, 运动功能=@_运动功能, 皮肤=@_皮肤, 淋巴结=@_淋巴结, 淋巴结其他=@_淋巴结其他, 桶状胸=@_桶状胸, 呼吸音=@_呼吸音, 呼吸音异常=@_呼吸音异常, 罗音=@_罗音, 罗音异常=@_罗音异常, 心率=@_心率, 心律=@_心律, 杂音=@_杂音, 杂音有=@_杂音有, 压痛=@_压痛, 压痛有=@_压痛有, 包块=@_包块, 包块有=@_包块有, 肝大=@_肝大, 肝大有=@_肝大有, 脾大=@_脾大, 脾大有=@_脾大有, 浊音=@_浊音, 浊音有=@_浊音有, 下肢水肿=@_下肢水肿, 肛门指诊=@_肛门指诊, 肛门指诊异常=@_肛门指诊异常, GQLX=@_G_QLX, 查体其他=@_查体其他, 白细胞=@_白细胞, 血红蛋白=@_血红蛋白, 血小板=@_血小板, 血常规其他=@_血常规其他, 尿蛋白=@_尿蛋白, 尿糖=@_尿糖, 尿酮体=@_尿酮体, 尿潜血=@_尿潜血, 尿常规其他=@_尿常规其他, 大便潜血=@_大便潜血, 血清谷丙转氨酶=@_血清谷丙转氨酶, 血清谷草转氨酶=@_血清谷草转氨酶, 白蛋白=@_白蛋白, 总胆红素=@_总胆红素, 结合胆红素=@_结合胆红素, 血清肌酐=@_血清肌酐, 血尿素氮=@_血尿素氮, 总胆固醇=@_总胆固醇, 甘油三酯=@_甘油三酯, 血清低密度脂蛋白胆固醇=@_血清低密度脂蛋白胆固醇, 血清高密度脂蛋白胆固醇=@_血清高密度脂蛋白胆固醇, 空腹血糖=@_空腹血糖, 乙型肝炎表面抗原=@_乙型肝炎表面抗原, 眼底=@_眼底, 眼底异常=@_眼底异常, 心电图=@_心电图, 心电图异常=@_心电图异常, 胸部X线片=@_胸部X线片, 胸部X线片异常=@_胸部X线片异常, B超=@_B超, B超其他=@_B超其他, 辅助检查其他=@_辅助检查其他, GJLJJY=@_G_JLJJY, 个人档案编号=@_个人档案编号, 创建机构=@_创建机构, 创建时间=@_创建时间, 创建人=@_创建人, 修改时间=@_修改时间, 修改人=@_修改人, 体检日期=@_体检日期, 所属机构=@_所属机构, FIELD1=@_FIELD1, FIELD2=@_FIELD2, FIELD3=@_FIELD3, FIELD4=@_FIELD4, WBCSUP=@_WBC_SUP, PLTSUP=@_PLT_SUP, GTUNWEI=@_G_TUNWEI, GYTWBZ=@_G_YTWBZ, 锻炼频率=@_锻炼频率, 每次锻炼时间=@_每次锻炼时间, 坚持锻炼时间=@_坚持锻炼时间, 锻炼方式=@_锻炼方式, 饮食习惯=@_饮食习惯, 吸烟状况=@_吸烟状况, 日吸烟量=@_日吸烟量, 开始吸烟年龄=@_开始吸烟年龄, 戒烟年龄=@_戒烟年龄, 饮酒频率=@_饮酒频率, 日饮酒量=@_日饮酒量, 是否戒酒=@_是否戒酒, 戒酒年龄=@_戒酒年龄, 开始饮酒年龄=@_开始饮酒年龄, 近一年内是否曾醉酒=@_近一年内是否曾醉酒, 饮酒种类=@_饮酒种类, 饮酒种类其它=@_饮酒种类其它, 有无职业病=@_有无职业病, 具体职业=@_具体职业, 从业时间=@_从业时间, 化学物质=@_化学物质, 化学物质防护=@_化学物质防护, 化学物质具体防护=@_化学物质具体防护, 毒物=@_毒物, GDWFHCS=@_G_DWFHCS, GDWFHCSQT=@_G_DWFHCSQT, 放射物质=@_放射物质, 放射物质防护措施有无=@_放射物质防护措施有无, 放射物质防护措施其他=@_放射物质防护措施其他, 口唇=@_口唇, 齿列=@_齿列, 咽部=@_咽部, 皮肤其他=@_皮肤其他, 巩膜=@_巩膜, 巩膜其他=@_巩膜其他, 足背动脉搏动=@_足背动脉搏动, 乳腺=@_乳腺, 乳腺其他=@_乳腺其他, 外阴=@_外阴, 外阴异常=@_外阴异常, 阴道=@_阴道, 阴道异常=@_阴道异常, 宫颈=@_宫颈, 宫颈异常=@_宫颈异常, 宫体=@_宫体, 宫体异常=@_宫体异常, 附件=@_附件, 附件异常=@_附件异常, 血钾浓度=@_血钾浓度, 血钠浓度=@_血钠浓度, 糖化血红蛋白=@_糖化血红蛋白, 宫颈涂片=@_宫颈涂片, 宫颈涂片异常=@_宫颈涂片异常, 平和质=@_平和质, 气虚质=@_气虚质, 阳虚质=@_阳虚质, 阴虚质=@_阴虚质, 痰湿质=@_痰湿质, 湿热质=@_湿热质, 血瘀质=@_血瘀质, 气郁质=@_气郁质, 特禀质=@_特禀质, 脑血管疾病=@_脑血管疾病, 脑血管疾病其他=@_脑血管疾病其他, 肾脏疾病=@_肾脏疾病, 肾脏疾病其他=@_肾脏疾病其他, 心脏疾病=@_心脏疾病, 心脏疾病其他=@_心脏疾病其他, 血管疾病=@_血管疾病, 血管疾病其他=@_血管疾病其他, 眼部疾病=@_眼部疾病, 眼部疾病其他=@_眼部疾病其他, 神经系统疾病=@_神经系统疾病, 神经系统疾病其他=@_神经系统疾病其他, 其他系统疾病=@_其他系统疾病, 其他系统疾病其他=@_其他系统疾病其他, 健康评价=@_健康评价, 健康评价异常1=@_健康评价异常1, 健康评价异常2=@_健康评价异常2, 健康评价异常3=@_健康评价异常3, 健康评价异常4=@_健康评价异常4, 健康指导=@_健康指导, 危险因素控制=@_危险因素控制, 危险因素控制体重=@_危险因素控制体重, 危险因素控制疫苗=@_危险因素控制疫苗, 危险因素控制其他=@_危险因素控制其他, FIELD5=@_FIELD5, 症状其他=@_症状其他, GXYYC=@_G_XYYC, GXYZC=@_G_XYZC, GQTZHZH=@_G_QTZHZH, 缺项=@_缺项, 口唇其他=@_口唇其他, 齿列其他=@_齿列其他, 咽部其他=@_咽部其他, YDGNQT=@_YDGNQT, 餐后2H血糖=@_餐后2H血糖, 老年人状况评估=@_老年人状况评估, 老年人自理评估=@_老年人自理评估, 粉尘=@_粉尘, 物理因素=@_物理因素, 职业病其他=@_职业病其他, 粉尘防护有无=@_粉尘防护有无, 物理防护有无=@_物理防护有无, 其他防护有无=@_其他防护有无, 粉尘防护措施=@_粉尘防护措施, 物理防护措施=@_物理防护措施, 其他防护措施=@_其他防护措施, TNBFXJF=@_TNBFXJF, 左侧原因=@_左侧原因, 右侧原因=@_右侧原因, 尿微量白蛋白=@_尿微量白蛋白, 完整度=@_完整度, 齿列缺齿=@_齿列缺齿, 齿列龋齿=@_齿列龋齿, 齿列义齿=@_齿列义齿  WHERE ID=" + info.ID;
            int rst = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sql, new object[] { new SqlParameter("@_身份证号", _身份证号), new SqlParameter("@_姓名", _姓名), new SqlParameter("@_性别", _性别), new SqlParameter("@_出生日期", _出生日期), new SqlParameter("@_症状", _症状), new SqlParameter("@_体温", _体温), new SqlParameter("@_呼吸", _呼吸), new SqlParameter("@_脉搏", _脉搏), new SqlParameter("@_血压右侧1", _血压右侧1), new SqlParameter("@_血压右侧2", _血压右侧2), new SqlParameter("@_血压左侧1", _血压左侧1), new SqlParameter("@_血压左侧2", _血压左侧2), new SqlParameter("@_身高", _身高), new SqlParameter("@_腰围", _腰围), new SqlParameter("@_体重", _体重), new SqlParameter("@_体重指数", _体重指数), new SqlParameter("@_老年人认知", _老年人认知), new SqlParameter("@_老年人情感", _老年人情感), new SqlParameter("@_老年人认知分", _老年人认知分), new SqlParameter("@_老年人情感分", _老年人情感分), new SqlParameter("@_左眼视力", _左眼视力), new SqlParameter("@_右眼视力", _右眼视力), new SqlParameter("@_左眼矫正", _左眼矫正), new SqlParameter("@_右眼矫正", _右眼矫正), new SqlParameter("@_听力", _听力), new SqlParameter("@_运动功能", _运动功能), new SqlParameter("@_皮肤", _皮肤), new SqlParameter("@_淋巴结", _淋巴结), new SqlParameter("@_淋巴结其他", _淋巴结其他), new SqlParameter("@_桶状胸", _桶状胸), new SqlParameter("@_呼吸音", _呼吸音), new SqlParameter("@_呼吸音异常", _呼吸音异常), new SqlParameter("@_罗音", _罗音), new SqlParameter("@_罗音异常", _罗音异常), new SqlParameter("@_心率", _心率), new SqlParameter("@_心律", _心律), new SqlParameter("@_杂音", _杂音), new SqlParameter("@_杂音有", _杂音有), new SqlParameter("@_压痛", _压痛), new SqlParameter("@_压痛有", _压痛有), new SqlParameter("@_包块", _包块), new SqlParameter("@_包块有", _包块有), new SqlParameter("@_肝大", _肝大), new SqlParameter("@_肝大有", _肝大有), new SqlParameter("@_脾大", _脾大), new SqlParameter("@_脾大有", _脾大有), new SqlParameter("@_浊音", _浊音), new SqlParameter("@_浊音有", _浊音有), new SqlParameter("@_下肢水肿", _下肢水肿), new SqlParameter("@_肛门指诊", _肛门指诊), new SqlParameter("@_肛门指诊异常", _肛门指诊异常), new SqlParameter("@_G_QLX", _G_QLX), new SqlParameter("@_查体其他", _查体其他), new SqlParameter("@_白细胞", _白细胞), new SqlParameter("@_血红蛋白", _血红蛋白), new SqlParameter("@_血小板", _血小板), new SqlParameter("@_血常规其他", _血常规其他), new SqlParameter("@_尿蛋白", _尿蛋白), new SqlParameter("@_尿糖", _尿糖), new SqlParameter("@_尿酮体", _尿酮体), new SqlParameter("@_尿潜血", _尿潜血), new SqlParameter("@_尿常规其他", _尿常规其他), new SqlParameter("@_大便潜血", _大便潜血), new SqlParameter("@_血清谷丙转氨酶", _血清谷丙转氨酶), new SqlParameter("@_血清谷草转氨酶", _血清谷草转氨酶), new SqlParameter("@_白蛋白", _白蛋白), new SqlParameter("@_总胆红素", _总胆红素), new SqlParameter("@_结合胆红素", _结合胆红素), new SqlParameter("@_血清肌酐", _血清肌酐), new SqlParameter("@_血尿素氮", _血尿素氮), new SqlParameter("@_总胆固醇", _总胆固醇), new SqlParameter("@_甘油三酯", _甘油三酯), new SqlParameter("@_血清低密度脂蛋白胆固醇", _血清低密度脂蛋白胆固醇), new SqlParameter("@_血清高密度脂蛋白胆固醇", _血清高密度脂蛋白胆固醇), new SqlParameter("@_空腹血糖", _空腹血糖), new SqlParameter("@_乙型肝炎表面抗原", _乙型肝炎表面抗原), new SqlParameter("@_眼底", _眼底), new SqlParameter("@_眼底异常", _眼底异常), new SqlParameter("@_心电图", _心电图), new SqlParameter("@_心电图异常", _心电图异常), new SqlParameter("@_胸部X线片", _胸部X线片), new SqlParameter("@_胸部X线片异常", _胸部X线片异常), new SqlParameter("@_B超", _B超), new SqlParameter("@_B超其他", _B超其他), new SqlParameter("@_辅助检查其他", _辅助检查其他), new SqlParameter("@_G_JLJJY", _G_JLJJY), new SqlParameter("@_个人档案编号", _个人档案编号), new SqlParameter("@_创建机构", _创建机构), new SqlParameter("@_创建时间", _创建时间), new SqlParameter("@_创建人", _创建人), new SqlParameter("@_修改时间", _修改时间), new SqlParameter("@_修改人", _修改人), new SqlParameter("@_体检日期", _体检日期), new SqlParameter("@_所属机构", _所属机构), new SqlParameter("@_FIELD1", _FIELD1), new SqlParameter("@_FIELD2", _FIELD2), new SqlParameter("@_FIELD3", _FIELD3), new SqlParameter("@_FIELD4", _FIELD4), new SqlParameter("@_WBC_SUP", _WBC_SUP), new SqlParameter("@_PLT_SUP", _PLT_SUP), new SqlParameter("@_G_TUNWEI", _G_TUNWEI), new SqlParameter("@_G_YTWBZ", _G_YTWBZ), new SqlParameter("@_锻炼频率", _锻炼频率), new SqlParameter("@_每次锻炼时间", _每次锻炼时间), new SqlParameter("@_坚持锻炼时间", _坚持锻炼时间), new SqlParameter("@_锻炼方式", _锻炼方式), new SqlParameter("@_饮食习惯", _饮食习惯), new SqlParameter("@_吸烟状况", _吸烟状况), new SqlParameter("@_日吸烟量", _日吸烟量), new SqlParameter("@_开始吸烟年龄", _开始吸烟年龄), new SqlParameter("@_戒烟年龄", _戒烟年龄), new SqlParameter("@_饮酒频率", _饮酒频率), new SqlParameter("@_日饮酒量", _日饮酒量), new SqlParameter("@_是否戒酒", _是否戒酒), new SqlParameter("@_戒酒年龄", _戒酒年龄), new SqlParameter("@_开始饮酒年龄", _开始饮酒年龄), new SqlParameter("@_近一年内是否曾醉酒", _近一年内是否曾醉酒), new SqlParameter("@_饮酒种类", _饮酒种类), new SqlParameter("@_饮酒种类其它", _饮酒种类其它), new SqlParameter("@_有无职业病", _有无职业病), new SqlParameter("@_具体职业", _具体职业), new SqlParameter("@_从业时间", _从业时间), new SqlParameter("@_化学物质", _化学物质), new SqlParameter("@_化学物质防护", _化学物质防护), new SqlParameter("@_化学物质具体防护", _化学物质具体防护), new SqlParameter("@_毒物", _毒物), new SqlParameter("@_G_DWFHCS", _G_DWFHCS), new SqlParameter("@_G_DWFHCSQT", _G_DWFHCSQT), new SqlParameter("@_放射物质", _放射物质), new SqlParameter("@_放射物质防护措施有无", _放射物质防护措施有无), new SqlParameter("@_放射物质防护措施其他", _放射物质防护措施其他), new SqlParameter("@_口唇", _口唇), new SqlParameter("@_齿列", _齿列), new SqlParameter("@_咽部", _咽部), new SqlParameter("@_皮肤其他", _皮肤其他), new SqlParameter("@_巩膜", _巩膜), new SqlParameter("@_巩膜其他", _巩膜其他), new SqlParameter("@_足背动脉搏动", _足背动脉搏动), new SqlParameter("@_乳腺", _乳腺), new SqlParameter("@_乳腺其他", _乳腺其他), new SqlParameter("@_外阴", _外阴), new SqlParameter("@_外阴异常", _外阴异常), new SqlParameter("@_阴道", _阴道), new SqlParameter("@_阴道异常", _阴道异常), new SqlParameter("@_宫颈", _宫颈), new SqlParameter("@_宫颈异常", _宫颈异常), new SqlParameter("@_宫体", _宫体), new SqlParameter("@_宫体异常", _宫体异常), new SqlParameter("@_附件", _附件), new SqlParameter("@_附件异常", _附件异常), new SqlParameter("@_血钾浓度", _血钾浓度), new SqlParameter("@_血钠浓度", _血钠浓度), new SqlParameter("@_糖化血红蛋白", _糖化血红蛋白), new SqlParameter("@_宫颈涂片", _宫颈涂片), new SqlParameter("@_宫颈涂片异常", _宫颈涂片异常), new SqlParameter("@_平和质", _平和质), new SqlParameter("@_气虚质", _气虚质), new SqlParameter("@_阳虚质", _阳虚质), new SqlParameter("@_阴虚质", _阴虚质), new SqlParameter("@_痰湿质", _痰湿质), new SqlParameter("@_湿热质", _湿热质), new SqlParameter("@_血瘀质", _血瘀质), new SqlParameter("@_气郁质", _气郁质), new SqlParameter("@_特禀质", _特禀质), new SqlParameter("@_脑血管疾病", _脑血管疾病), new SqlParameter("@_脑血管疾病其他", _脑血管疾病其他), new SqlParameter("@_肾脏疾病", _肾脏疾病), new SqlParameter("@_肾脏疾病其他", _肾脏疾病其他), new SqlParameter("@_心脏疾病", _心脏疾病), new SqlParameter("@_心脏疾病其他", _心脏疾病其他), new SqlParameter("@_血管疾病", _血管疾病), new SqlParameter("@_血管疾病其他", _血管疾病其他), new SqlParameter("@_眼部疾病", _眼部疾病), new SqlParameter("@_眼部疾病其他", _眼部疾病其他), new SqlParameter("@_神经系统疾病", _神经系统疾病), new SqlParameter("@_神经系统疾病其他", _神经系统疾病其他), new SqlParameter("@_其他系统疾病", _其他系统疾病), new SqlParameter("@_其他系统疾病其他", _其他系统疾病其他), new SqlParameter("@_健康评价", _健康评价), new SqlParameter("@_健康评价异常1", _健康评价异常1), new SqlParameter("@_健康评价异常2", _健康评价异常2), new SqlParameter("@_健康评价异常3", _健康评价异常3), new SqlParameter("@_健康评价异常4", _健康评价异常4), new SqlParameter("@_健康指导", _健康指导), new SqlParameter("@_危险因素控制", _危险因素控制), new SqlParameter("@_危险因素控制体重", _危险因素控制体重), new SqlParameter("@_危险因素控制疫苗", _危险因素控制疫苗), new SqlParameter("@_危险因素控制其他", _危险因素控制其他), new SqlParameter("@_FIELD5", _FIELD5), new SqlParameter("@_症状其他", _症状其他), new SqlParameter("@_G_XYYC", _G_XYYC), new SqlParameter("@_G_XYZC", _G_XYZC), new SqlParameter("@_G_QTZHZH", _G_QTZHZH), new SqlParameter("@_缺项", _缺项), new SqlParameter("@_口唇其他", _口唇其他), new SqlParameter("@_齿列其他", _齿列其他), new SqlParameter("@_咽部其他", _咽部其他), new SqlParameter("@_YDGNQT", _YDGNQT), new SqlParameter("@_餐后2H血糖", _餐后2H血糖), new SqlParameter("@_老年人状况评估", _老年人状况评估), new SqlParameter("@_老年人自理评估", _老年人自理评估), new SqlParameter("@_粉尘", _粉尘), new SqlParameter("@_物理因素", _物理因素), new SqlParameter("@_职业病其他", _职业病其他), new SqlParameter("@_粉尘防护有无", _粉尘防护有无), new SqlParameter("@_物理防护有无", _物理防护有无), new SqlParameter("@_其他防护有无", _其他防护有无), new SqlParameter("@_粉尘防护措施", _粉尘防护措施), new SqlParameter("@_物理防护措施", _物理防护措施), new SqlParameter("@_其他防护措施", _其他防护措施), new SqlParameter("@_TNBFXJF", _TNBFXJF), new SqlParameter("@_左侧原因", _左侧原因), new SqlParameter("@_右侧原因", _右侧原因), new SqlParameter("@_尿微量白蛋白", _尿微量白蛋白), new SqlParameter("@_完整度", _完整度), new SqlParameter("@_齿列缺齿", _齿列缺齿), new SqlParameter("@_齿列龋齿", _齿列龋齿), new SqlParameter("@_齿列义齿", _齿列义齿) });
            if (rst > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal static bool UpdateSys_RowState(tb_健康体检Info info)
        {

            bool rst = false;
            string sqlStr = "update tb_健康体检 set rowstate = '1' WHERE ID=" + info.ID;
            int rows = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }

        internal static bool UpdateSys_RowState_Hiden()
        {

            bool rst = false;
            string sqlStr = "update tb_健康体检 set rowstate = '-1' WHERE RowState ISNULL OR RowState = '' ";
            int rows = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }

        internal static bool UpdateSys_DocNoPrgid(tb_健康体检Info info)
        {

            bool rst = false;
            string sqlStr = "update tb_健康体检 set 个人档案编号=@DocNO,所属机构=@Prgid,创建机构=@Prgid WHERE ID=" + info.ID;
            int rows = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sqlStr, new object[] { new SQLiteParameter("@DocNO", info.个人档案编号), new SQLiteParameter("@Prgid", info.所属机构) });
            //int  = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }
        public static List<string> isExistsIn121(string cardNo)
        {
            string rgid = Program._bRGID == "" ? "371323B10019" : Program._bRGID; //如果该参数是空默认姚店子
            string sql = "select 个人档案编号,所属机构,创建机构 from tb_健康档案 where 身份证号 = '" + cardNo + "' and (所属机构 = '" + rgid + "' or substring([所属机构],1,7)+'1'+substring([所属机构],9,7) like '" + rgid + "%') and 档案状态 = '1'";
            List<string> list = new List<string>();
            SqlDataReader reader = DBManager.ExecuteQuery(sql);
            using (reader)
            {
                while (reader.Read())
                {
                    string _docNo = reader["个人档案编号"].ToString();
                    string prgid = reader["所属机构"].ToString();
                    string cjjg = reader["创建机构"].ToString();
                    list.Add(_docNo);
                    list.Add(prgid);
                    list.Add(cjjg);
                }
            }

            return list;
        }
        //-----------------王森-----------------------------------
        /// <summary>
        /// 四十里尿检机器数据传输---王森
        /// </summary>
        /// <param name="datSet"></param>
        /// <param name="datTable"></param>
        /// <param name="条码"></param>
        private static void isExistsNCG(out DataSet datSet, out DataTable datTable, string 条码)
        {
            dbConstring = new DBClass(DbFilePath.FilePathName);

            string strSql = "", strTableName = "tb_健康体检";
            //strSql = "SELECT * FROM  " + strTableName + " WHERE  (个人档案编号='" + Program.currentUser.DocNo  +
            //    "' or 身份证号='" + Program.currentUser.ID + "') AND 体检日期='" + DateTime.Now.ToString("yyyy-MM-dd") + "'  ";
            strSql = "SELECT * FROM  " + strTableName + " WHERE   体检日期='" + DateTime.Now.ToString("yyyy-MM-dd") + "' ";
            if (!string.IsNullOrEmpty(条码))
                strSql += " AND 个人档案编号='" + 条码 + "' ";
            //if (!string.IsNullOrEmpty(Program.currentUser.ID))
            //    strSql += " AND 身份证号='" + Program.currentUser.ID + "' ";
            datSet = SQLiteHelper.ExecuteDataSet(dbConstring.m_ConnString, strSql, CommandType.Text);
            datTable = datSet.Tables[0];
            datTable.TableName = strTableName;
        }
        //--------------------------------------------


        //Begin WXF 2018-11-21 | 17:15
        //组装医师签字json串 
        /// <summary>
        /// 一次组装一个医师签字
        /// </summary>
        /// <param name="recordNum"></param>
        /// <param name="doctorName"></param>
        /// <param name="mod"></param>
        /// <returns></returns>
        public static string JsonToStr_医师签字(string recordNum, string doctorName, Module_体检 mod)
        {
            dal_健康体检_Lite Lite健康体检 = new dal_健康体检_Lite();
            tb_健康体检_Lite tb健康体检Lite = Lite健康体检.Get_健康体检Today(recordNum);


            if (string.IsNullOrEmpty(tb健康体检Lite.医师签字))
            {
                Bean_医师签字模块 bean = new Bean_医师签字模块();
                return Bean_医师签字(mod, ref bean, doctorName);
            }
            else
            {
                Bean_医师签字模块 bean = JsonConvert.DeserializeObject<Bean_医师签字模块>(tb健康体检Lite.医师签字);
                return Bean_医师签字(mod, ref bean, doctorName);
            }
        }

        /// <summary>
        /// 一次组装多个医师签字
        /// </summary>
        /// <param name="recordNum"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        public static string JsonToStr_医师签字(string recordNum, Dictionary<Module_体检, string> dic)
        {
            dal_健康体检_Lite Lite健康体检 = new dal_健康体检_Lite();
            tb_健康体检_Lite tb健康体检Lite = Lite健康体检.Get_健康体检Today(recordNum);
            string result = string.Empty;

            if (string.IsNullOrEmpty(tb健康体检Lite.医师签字))
            {
                Bean_医师签字模块 bean = new Bean_医师签字模块();
                foreach (var item in dic)
                {
                    result = Bean_医师签字(item.Key, ref bean, item.Value);
                }
            }
            else
            {
                Bean_医师签字模块 bean = JsonConvert.DeserializeObject<Bean_医师签字模块>(tb健康体检Lite.医师签字);
                foreach (var item in dic)
                {
                    result = Bean_医师签字(item.Key, ref bean, item.Value);
                }
            }

            return result;
        }

        private static string Bean_医师签字(Module_体检 mod, ref Bean_医师签字模块 bean, string doctorName)
        {
            switch (mod)
            {
                case Module_体检.症状:
                    bean.症状 = doctorName;
                    break;
                case Module_体检.身高体重:
                    bean.身高体重 = doctorName;
                    break;
                case Module_体检.血压:
                    bean.血压 = doctorName;
                    break;
                case Module_体检.生活方式:
                    bean.生活方式 = doctorName;
                    break;
                case Module_体检.脏器功能:
                    bean.脏器功能 = doctorName;
                    break;
                case Module_体检.查体:
                    bean.查体 = doctorName;
                    break;
                case Module_体检.辅助检查:
                    bean.辅助检查 = doctorName;
                    break;
                case Module_体检.心电图:
                    bean.心电图 = doctorName;
                    break;
                case Module_体检.胸部X线片:
                    bean.胸部X线片 = doctorName;
                    break;
                case Module_体检.B超:
                    bean.B超 = doctorName;
                    break;
                case Module_体检.现存主要健康问题:
                    bean.现存主要健康问题 = doctorName;
                    break;
                case Module_体检.主要用药情况:
                    bean.主要用药情况 = doctorName;
                    break;
                case Module_体检.非免疫规划预防接种史:
                    bean.非免疫规划预防接种史 = doctorName;
                    break;
                case Module_体检.健康评价:
                    bean.健康评价 = doctorName;
                    break;
                case Module_体检.健康指导:
                    bean.健康指导 = doctorName;
                    break;
                case Module_体检.结果反馈:
                    bean.结果反馈 = doctorName;
                    break;
                default:
                    break;
            }

            return bean == null ? "" : JsonConvert.SerializeObject(bean);
        }

        /// <summary>
        /// 传入服务器上的医师签字json串与本地的医师签字json串
        /// 合并两个json串的内容
        /// </summary>
        /// <param name="strServer"></param>
        /// <param name="strHost"></param>
        /// <returns></returns>
        public static string merge_strJson医师签字(string strServer, string strHost)
        {
            Bean_医师签字模块 beanServer = new Bean_医师签字模块();
            if (!string.IsNullOrEmpty(strServer))
            {
                JsonConvert.DeserializeObject<Bean_医师签字模块>(strServer);
            }
            Bean_医师签字模块 beanHost = new Bean_医师签字模块();
            if (!string.IsNullOrEmpty(strHost))
            {
                JsonConvert.DeserializeObject<Bean_医师签字模块>(strHost);
            }

            beanServer.症状 = string.IsNullOrEmpty(beanHost.症状) ? beanServer.症状 : beanHost.症状;
            beanServer.身高体重 = string.IsNullOrEmpty(beanHost.身高体重) ? beanServer.身高体重 : beanHost.身高体重;
            beanServer.生活方式 = string.IsNullOrEmpty(beanHost.生活方式) ? beanServer.生活方式 : beanHost.生活方式;
            beanServer.脏器功能 = string.IsNullOrEmpty(beanHost.脏器功能) ? beanServer.脏器功能 : beanHost.脏器功能;
            beanServer.查体 = string.IsNullOrEmpty(beanHost.查体) ? beanServer.查体 : beanHost.查体;
            beanServer.辅助检查 = string.IsNullOrEmpty(beanHost.辅助检查) ? beanServer.辅助检查 : beanHost.辅助检查;
            beanServer.心电图 = string.IsNullOrEmpty(beanHost.心电图) ? beanServer.心电图 : beanHost.心电图;
            beanServer.胸部X线片 = string.IsNullOrEmpty(beanHost.胸部X线片) ? beanServer.胸部X线片 : beanHost.胸部X线片;
            beanServer.B超 = string.IsNullOrEmpty(beanHost.B超) ? beanServer.B超 : beanHost.B超;
            beanServer.现存主要健康问题 = string.IsNullOrEmpty(beanHost.现存主要健康问题) ? beanServer.现存主要健康问题 : beanHost.现存主要健康问题;
            beanServer.主要用药情况 = string.IsNullOrEmpty(beanHost.主要用药情况) ? beanServer.主要用药情况 : beanHost.主要用药情况;
            beanServer.非免疫规划预防接种史 = string.IsNullOrEmpty(beanHost.非免疫规划预防接种史) ? beanServer.非免疫规划预防接种史 : beanHost.非免疫规划预防接种史;
            beanServer.健康评价 = string.IsNullOrEmpty(beanHost.健康评价) ? beanServer.健康评价 : beanHost.健康评价;
            beanServer.健康指导 = string.IsNullOrEmpty(beanHost.健康指导) ? beanServer.健康指导 : beanHost.健康指导;
            beanServer.结果反馈 = string.IsNullOrEmpty(beanHost.结果反馈) ? beanServer.结果反馈 : beanHost.结果反馈;

            return beanServer == null ? "" : JsonConvert.SerializeObject(beanServer);
        }

        //End

    }
}
