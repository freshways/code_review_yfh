﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Data.SQLite;

namespace ATOMEHR_LeaveClient
{
    public class tb_健康体检_住院史DAL2
    {
        /// <summary>
        /// 获取所有信息
        /// </summary>
        /// <param name="where">条件</param>
        /// <returns>结果集</returns>
        public static SQLiteDataReader GetInfo(string where)
        {
            string sqlStr = "SELECT * FROM tb_健康体检_住院史 where ";
            if (String.IsNullOrEmpty(where))
            {
                sqlStr += "1=1";
            }
            else
            {
                sqlStr += where;
            }
            SQLiteDataReader reader = null;
            try
            {
                reader = DBManager.ExecuteQueryForSQLite(sqlStr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reader;
        }

        /// <summary>
        /// 获取所有信息集合
        /// </summary>
        /// <returns>List集合</returns>
        public static List<tb_健康体检_住院史Info> GetInfoList(string where)
        {
            List<tb_健康体检_住院史Info> infoList = new List<tb_健康体检_住院史Info>();

            SQLiteDataReader reader = null;
            try
            {
                reader = GetInfo(where);
            }
            catch (Exception)
            {
                throw;
            }

            while (reader.Read())
            {
                tb_健康体检_住院史Info info = new tb_健康体检_住院史Info();
                info.ID = reader["ID"].ToString();
                info.身份证号 = reader["身份证号"].ToString();
                info.个人档案编号 = reader["个人档案编号"].ToString();
                info.类型 = reader["类型"].ToString();
                info.入院日期 = reader["入院日期"].ToString();
                info.出院日期 = reader["出院日期"].ToString();
                info.原因 = reader["原因"].ToString();
                info.医疗机构名称 = reader["医疗机构名称"].ToString();
                info.病案号 = reader["病案号"].ToString();
                info.创建日期 = reader["创建日期"].ToString();
                info.BlueName = reader["BlueName"].ToString();
                info.BlueAddr = reader["BlueAddr"].ToString();
                info.BlueType = reader["BlueType"].ToString();
                infoList.Add(info);
            }
            reader.Close();
            return infoList;
        }

        /// <summary>
        /// 根据 主键 获取一个实体对象
        /// <param name="ID">主键</param>
        /// </summary>
        public static tb_健康体检_住院史Info GetInfoById(string ID)
        {
            string strWhere = "ID =" + ID;
            List<tb_健康体检_住院史Info> list = GetInfoList(strWhere);
            if (list.Count > 0)
                return list[0];
            return null;
        }

        /// 添加数据
        /// </summary>
        /// <param name="info">数据表实体对象</param>
        public static bool Add(tb_健康体检_住院史Info info)
        {
            string _身份证号 = Program.currentUser.ID;
            string _个人档案编号 = Program.currentUser.DocNo;
            string _类型 = info.类型;
            string _入院日期 = string.IsNullOrEmpty(info.入院日期) ? "" : info.入院日期.Substring(0, 10);
            string _出院日期 = string.IsNullOrEmpty(info.出院日期) ? "" : info.出院日期.Substring(0, 10);
            string _原因 = info.原因 == null ? "" : info.原因;
            string _医疗机构名称 = info.医疗机构名称 == null ? "" : info.医疗机构名称;
            string _病案号 = info.病案号;
            string _创建日期 = Program._currentTime;
            string _BlueName = Program.BlueName;
            string _BlueAddr = Program.BlueAddr;
            string _BlueType = Program.BlueType;

            string sql = "INSERT INTO tb_健康体检_住院史 VALUES (@_身份证号,@_个人档案编号,@_类型,@_入院日期,@_出院日期,@_原因,@_医疗机构名称,@_病案号,@_创建日期,'',@_BlueAddr,@_BlueType,@_BlueName)";

            //int rst = DBManager.ExecuteProc("SP_SAVE住院史", new object[] {  new SqlParameter("@_身份证号", _身份证号), new SqlParameter("@_个人档案编号", _个人档案编号), new SqlParameter("@_类型", _类型), new SqlParameter("@_入院日期", _入院日期), new SqlParameter("@_出院日期", _出院日期), new SqlParameter("@_原因", _原因), new SqlParameter("@_医疗机构名称", _医疗机构名称), new SqlParameter("@_病案号", _病案号), new SqlParameter("@_创建日期", _创建日期) });

            int rst = DBManager.ExecuteUpdate(DBManager.Conn, sql, new object[] { new SqlParameter("@_身份证号", _身份证号), new SqlParameter("@_个人档案编号", _个人档案编号), new SqlParameter("@_类型", _类型), new SqlParameter("@_入院日期", _入院日期), new SqlParameter("@_出院日期", _出院日期), new SqlParameter("@_原因", _原因), new SqlParameter("@_医疗机构名称", _医疗机构名称), new SqlParameter("@_病案号", _病案号), new SqlParameter("@_创建日期", _创建日期), new SqlParameter("@_BlueAddr", _BlueAddr), new SqlParameter("@_BlueType", _BlueType), new SqlParameter("@_BlueName", _BlueName) });
            if (rst > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool AddTo121(tb_健康体检_住院史Info info)
        {
            string _身份证号 = info.身份证号;
            string _个人档案编号 = info.个人档案编号;
            string _类型 = info.类型;
            string _入院日期 = info.入院日期;
            string _出院日期 = info.出院日期;
            string _原因 = info.原因;
            string _医疗机构名称 = info.医疗机构名称;
            string _病案号 = info.病案号;
            string _创建日期 = info.创建日期;
            string _BlueName = info.BlueName;
            string _BlueAddr = info.BlueAddr;
            string _BlueType = info.BlueType;
            int rst = DBManager.ExecuteProc(DBManager.Conn121, "USP_IMPORT住院史", new object[] { new SqlParameter("@_身份证号", _身份证号), new SqlParameter("@_个人档案编号", _个人档案编号), new SqlParameter("@_类型", _类型), new SqlParameter("@_入院日期", _入院日期), new SqlParameter("@_出院日期", _出院日期), new SqlParameter("@_原因", _原因), new SqlParameter("@_医疗机构名称", _医疗机构名称), new SqlParameter("@_病案号", _病案号), new SqlParameter("@_创建日期", _创建日期), new SqlParameter("@_BlueName", _BlueName), new SqlParameter("@_BlueAddr", _BlueAddr), new SqlParameter("@_BlueType", _BlueType) });

            if (rst > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除一个对象
        /// </summary>
        /// <param name="ID">主键</param>
        /// <returns></returns>
        public static bool DeleteInfo(tb_健康体检_住院史Info info)
        {
            bool rst = false;
            string sqlStr = "DELETE FROM tb_健康体检_住院史 WHERE ID=" + info.ID;
            int rows = DBManager.ExecuteUpdate(DBManager.Conn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }
        /// <summary>
        /// 通过身份证号进行删除 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool Delete住院Info(string key)
        {
            bool rst = false;
            string sqlStr = "DELETE FROM tb_健康体检_住院史 WHERE 类型 = '1' and 身份证号=" + key;
            int rows = DBManager.ExecuteUpdate(DBManager.Conn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }
        public static bool Delete建床Info(string key)
        {
            bool rst = false;
            string sqlStr = "DELETE FROM tb_健康体检_住院史 WHERE 类型 = '2' and 身份证号=" + key;
            int rows = DBManager.ExecuteUpdate(DBManager.Conn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }
        /// 更新数据
        /// </summary>
        /// <param name="info">数据表实体</param>
        public static bool UpdateInfo(tb_健康体检_住院史Info info)
        {
            string _身份证号 = info.身份证号;
            string _个人档案编号 = info.个人档案编号;
            string _类型 = info.类型;
            string _入院日期 = info.入院日期;
            string _出院日期 = info.出院日期;
            string _原因 = info.原因;
            string _医疗机构名称 = info.医疗机构名称;
            string _病案号 = info.病案号;
            string _创建日期 = info.创建日期;
            string sql = "UPDATE tb_健康体检_住院史 SET 身份证号=@_身份证号, 个人档案编号=@_个人档案编号, 类型=@_类型, 入院日期=@_入院日期, 出院日期=@_出院日期, 原因=@_原因, 医疗机构名称=@_医疗机构名称, 病案号=@_病案号, 创建日期=@_创建日期  WHERE ID=" + info.ID;
            int rst = DBManager.ExecuteUpdate(DBManager.Conn, sql, new object[] { new SqlParameter("@_身份证号", _身份证号), new SqlParameter("@_个人档案编号", _个人档案编号), new SqlParameter("@_类型", _类型), new SqlParameter("@_入院日期", _入院日期), new SqlParameter("@_出院日期", _出院日期), new SqlParameter("@_原因", _原因), new SqlParameter("@_医疗机构名称", _医疗机构名称), new SqlParameter("@_病案号", _病案号), new SqlParameter("@_创建日期", _创建日期) });
            if (rst > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
