﻿using ATOMEHR_LeaveClient.MODEL;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;

namespace ATOMEHR_LeaveClient.DAL
{
    class 用药情况配置DAL
    {
        public static List<tb_用药情况配置表> 获取用药情况配置()
        {
            List<tb_用药情况配置表> list = new List<tb_用药情况配置表>();

            SQLiteDataReader reader = null;
            try
            {
                reader = DBManager.ExecuteQueryForSQLite("select * from tb_用药情况配置");
                while (reader.Read())
                {
                    tb_用药情况配置表 bean = new tb_用药情况配置表();
                    bean.Id = reader["id"].ToString();
                    bean.名称 = reader["名称"].ToString();
                    bean.拼音码 = reader["拼音码"].ToString();
                    list.Add(bean);
                }
                reader.Close();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (null != reader)
                {
                    reader.Close();
                }
            }

            return list;
        }

        public static int 添加用药情况配置(tb_用药情况配置表 bean)
        {
            int count = 0;
            List<SQLiteParameter> paramList = new List<SQLiteParameter>();
            SQLiteParameter param;

            string sql = "insert into tb_用药情况配置 values(@id,@名称, @拼音码)";
            param = new SQLiteParameter("id", bean.Id);
            paramList.Add(param);
            param = new SQLiteParameter("名称", bean.名称);
            paramList.Add(param);
            param = new SQLiteParameter("拼音码", bean.拼音码);
            paramList.Add(param);

            count = DBManager.ExecuteSQLitUpdate(sql, paramList);

            return count;
        }

        public static int 更新用药情况配置(tb_用药情况配置表 bean)
        {
            int count = 0;
            List<SQLiteParameter> paramList = new List<SQLiteParameter>();
            SQLiteParameter param;

            string sql = "update tb_用药情况配置 set 名称 = @名称, 拼音码 = @拼音码 where id = @id";
            param = new SQLiteParameter("名称", bean.名称);
            paramList.Add(param);
            param = new SQLiteParameter("拼音码", bean.拼音码);
            paramList.Add(param);
            param = new SQLiteParameter("id", bean.Id);
            paramList.Add(param);

            count = DBManager.ExecuteSQLitUpdate(sql, paramList);

            return count;
        }

        public static int 删除用药情况配置(List<tb_用药情况配置表> deleteList)
        {
            int count = 0;
            string sql = string.Empty;
            List<SQLiteParameter> paramList;
            SQLiteParameter param;
            foreach (var item in deleteList)
            {
                paramList = new List<SQLiteParameter>();
                sql = "delete from tb_用药情况配置 where id = @id";
                param = new SQLiteParameter("id", item.Id);
                paramList.Add(param);

                count = count + DBManager.ExecuteSQLitUpdate(sql, paramList);
            }

            return count;
        }
    }
}