﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SQLite;
using System.Data.Common;

namespace ATOMEHR_LeaveClient
{
    public class tb_导出用户DAL
    {
        private static DBClass dbConstring;
        /// <summary>
        /// 获取所有信息
        /// </summary>
        /// <param name="where">条件</param>
        /// <returns>结果集</returns>
        public static SQLiteDataReader GetInfo(string where)
        {
            string sqlStr = "SELECT * FROM tb_导出用户 where ";
            if (String.IsNullOrEmpty(where))
            {
                sqlStr += "1=1";
            }
            else
            {
                sqlStr += where;
            }
            sqlStr += " limit 100 "; //2016年6月29日14:19:39 yufh添加 默认显示100行，我觉得应该足够了，太多的话一是卡二是不好看
            SQLiteDataReader datRead = null;
            try
            {
                datRead = DBManager.ExecuteQueryForSQLite(sqlStr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return datRead;
        }

        /// <summary>
        /// 获取所有信息集合
        /// </summary>
        /// <returns>List集合</returns>
        public static List<tb_导出用户Info> GetInfoList(string where)
        {
            List<tb_导出用户Info> infoList = new List<tb_导出用户Info>();

            SQLiteDataReader reader = null;
            try
            {
                reader = GetInfo(where);
            }
            catch (Exception)
            {
                //throw;
                return null;
            }

            while (reader.Read())
            {
                tb_导出用户Info info = new tb_导出用户Info();
                info.ID = reader["ID"].ToString();
                info.身份证号 = reader["身份证号"].ToString();
                info.姓名 = reader["姓名"].ToString();
                info.性别 = reader["性别"].ToString();
                info.居住地址 = reader["居住地址"].ToString();
                info.出生日期 = reader["出生日期"].ToString();
                info.个人档案编号 = reader["个人档案编号"].ToString();
                info.所属机构 = reader["所属机构"].ToString();
                infoList.Add(info);
            }
            reader.Close();
            return infoList;
        }

        /// <summary>
        /// 根据 主键 获取一个实体对象
        /// <param name="ID">主键</param>
        /// </summary>
        public static tb_导出用户Info GetInfoById(string ID)
        {
            string strWhere = " 身份证号 =" + ID;
            List<tb_导出用户Info> list = GetInfoList(strWhere);
            if (list.Count > 0)
                return list[0];
            return null;
        }

        /// 添加数据
        /// </summary>
        /// <param name="info">数据表实体对象</param>
        public static bool Add(tb_导出用户Info info)
        {
            string _身份证号 = info.身份证号;
            string _姓名 = info.姓名;
            string _性别 = info.性别;
            string _居住地址 = info.居住地址;
            string _出生日期 = info.出生日期;
            string 个人档案编号 = info.个人档案编号;
            string 所属机构 = info.所属机构;

            string sql = "INSERT INTO tb_导出用户 VALUES (NULL,@_身份证号,@_姓名,@_性别,@_居住地址,@_出生日期,@个人档案编号,@所属机构)";
            int rst = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sql, new object[] { new SQLiteParameter("@_身份证号", _身份证号), new SQLiteParameter("@_姓名", _姓名), new SQLiteParameter("@_性别", _性别), new SQLiteParameter("@_居住地址", _居住地址), new SQLiteParameter("@_出生日期", _出生日期), new SQLiteParameter("@个人档案编号", 个人档案编号), new SQLiteParameter("@所属机构", 所属机构) });
            if (rst > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除一个对象
        /// </summary>
        /// <param name="ID">主键</param>
        /// <returns></returns>
        public static bool DeleteInfo(tb_导出用户Info info)
        {
            bool rst = false;
            string sqlStr = "DELETE FROM tb_导出用户 WHERE ID=" + info.ID;
            int rows = DBManager.ExecuteUpdate(DBManager.Conn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }
        public static bool Delete(string sqlWhere)
        {
            bool rst = false;
            string sqlStr = "DELETE FROM tb_导出用户 WHERE 1=1 ";
            if (!string.IsNullOrEmpty(sqlWhere))
            {
                sqlStr += sqlWhere;
            }
            int rows = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }

        /// 更新数据
        /// </summary>
        /// <param name="info">数据表实体</param>
        public static bool UpdateInfo(tb_导出用户Info info)
        {
            string _身份证号 = info.身份证号;
            string _姓名 = info.姓名;
            string _性别 = info.性别;
            string _居住地址 = info.居住地址;
            string _出生日期 = info.出生日期;
            string sql = "UPDATE tb_导出用户 SET 身份证号=@_身份证号, 姓名=@_姓名, 性别=@_性别, 居住地址=@_居住地址, 出生日期=@_出生日期  WHERE ID=" + info.ID;
            int rst = DBManager.ExecuteUpdate(DBManager.Conn, sql, new object[] { new SqlParameter("@_身份证号", _身份证号), new SqlParameter("@_姓名", _姓名), new SqlParameter("@_性别", _性别), new SqlParameter("@_居住地址", _居住地址), new SqlParameter("@_出生日期", _出生日期) });
            if (rst > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void InsertList(List<tb_导出用户Info> list)
        {
            SQLiteConnection conn = DBManager.SQLiteConn;
            conn.Open();
            DbTransaction trans = conn.BeginTransaction();

            try
            {
                for (int i = 0; i < list.Count; i++)
                {
                    string _身份证号 = list[i].身份证号;
                    string _姓名 = list[i].姓名;
                    string _性别 = list[i].性别;
                    string _居住地址 = list[i].居住地址;
                    string _出生日期 = list[i].出生日期;
                    string 个人档案编号 = list[i].个人档案编号;
                    string 所属机构 = list[i].所属机构;

                    string sql = "INSERT INTO tb_导出用户 VALUES (NULL,@_身份证号,@_姓名,@_性别,@_居住地址,@_出生日期,@个人档案编号,@所属机构)";
                    SQLiteCommand cmd = new SQLiteCommand(sql, conn);
                    cmd.Parameters.AddRange(new object[] { new SQLiteParameter("@_身份证号", _身份证号), new SQLiteParameter("@_姓名", _姓名), new SQLiteParameter("@_性别", _性别), new SQLiteParameter("@_居住地址", _居住地址), new SQLiteParameter("@_出生日期", _出生日期), new SQLiteParameter("@个人档案编号", 个人档案编号), new SQLiteParameter("@所属机构", 所属机构) });

                    cmd.ExecuteNonQuery();
                }
                trans.Commit(); // <-------------------  
            }
            catch (Exception ex)
            {
                trans.Rollback(); // <-------------------  
                conn.Close();
                conn.Dispose();
                throw; // <-------------------  
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }
    }
}
