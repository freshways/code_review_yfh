﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SQLite;
using System.Data.Common;

namespace ATOMEHR_LeaveClient
{
    public class tb_机构信息DAL
    {
        //private DBHelper dbHelper = new DBHelper();
        /// <summary>
        /// 构造函数
        /// </summary>
        public tb_机构信息DAL()
        {
        }
        /// <summary>
        /// 添加记录
        /// </summary>
        /// <param name="model">MODEL.tb_机构信息实体类</param>
        /// <returns>新增记录的ID</returns>
        public long Add(tb_机构信息 model)
        {
            string sql = @"INSERT INTO tb_机构信息
				(ID,机构编号,机构名称,P_RGID_MARKER,负责人,联系人,联系电话,上级机构,机构级别,创建人,创建时间,状态,P_QYDW,辖区人口) 
				VALUES(@ID,@机构编号,@机构名称,@P_RGID_MARKER,@负责人,@联系人,@联系电话,@上级机构,@机构级别,@创建人,@创建时间,@状态,@P_QYDW,@辖区人口);
				SELECT SCOPE_IDENTITY();";
            SQLiteParameter[] parameters = new SQLiteParameter[]{
                new SQLiteParameter("@ID",  model.ID ),
				new SQLiteParameter("@机构编号",  model.机构编号 ),
                new SQLiteParameter("@机构名称",  model.机构名称 ),
                new SQLiteParameter("@P_RGID_MARKER", model.P_RGID_MARKER ),
                new SQLiteParameter("@负责人",  model.负责人 ),
                new SQLiteParameter("@联系人", model.联系人 ),
                new SQLiteParameter("@联系电话",  model.联系电话 ),
                new SQLiteParameter("@上级机构",  model.上级机构 ),
                new SQLiteParameter("@机构级别", model.机构级别 ),
                new SQLiteParameter("@创建人",  model.创建人 ),
                new SQLiteParameter("@创建时间",  model.创建时间 ),
                new SQLiteParameter("@状态",  model.状态 ),
                new SQLiteParameter("@P_QYDW",  model.P_QYDW )
			};

            return DBManager.ExecuteUpdate(DBManager.SQLiteConn, sql, parameters);
        }
        
        /// <summary>
        /// 删除记录
        /// </summary>
        public static bool Delete(string sqlWhere)
        {
            bool rst = false;
            string sqlStr = "DELETE FROM tb_机构信息 WHERE 1=1 ";
            if (!string.IsNullOrEmpty(sqlWhere))
            {
                sqlStr += sqlWhere;
            }
            int rows = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }
        /// <summary>
        /// 将DataRedar转换为List
        /// </summary>
        private List<tb_机构信息> DataReaderToList(SQLiteDataReader dataReader)
        {
            List<tb_机构信息> List = new List<tb_机构信息>();
            tb_机构信息 model = null;
            while (dataReader.Read())
            {
                model = new tb_机构信息();
                model.ID = dataReader.GetInt64(0);
                model.机构编号 = dataReader.GetString(1);
                if (!dataReader.IsDBNull(2))
                    model.机构名称 = dataReader.GetString(2);
                if (!dataReader.IsDBNull(3))
                    model.P_RGID_MARKER = dataReader.GetString(3);
                if (!dataReader.IsDBNull(4))
                    model.负责人 = dataReader.GetString(4);
                if (!dataReader.IsDBNull(5))
                    model.联系人 = dataReader.GetString(5);
                if (!dataReader.IsDBNull(6))
                    model.联系电话 = dataReader.GetString(6);
                if (!dataReader.IsDBNull(7))
                    model.上级机构 = dataReader.GetString(7);
                if (!dataReader.IsDBNull(8))
                    model.机构级别 = dataReader.GetString(8);
                if (!dataReader.IsDBNull(9))
                    model.创建人 = dataReader.GetString(9);
                if (!dataReader.IsDBNull(10))
                    model.创建时间 = dataReader.GetDateTime(10);
                if (!dataReader.IsDBNull(11))
                    model.状态 = dataReader.GetString(11);
                if (!dataReader.IsDBNull(12))
                    model.P_QYDW = dataReader.GetString(12);
                List.Add(model);
            }
            return List;
        }
        /// <summary>
        /// 查询所有记录
        /// </summary>
        public List<tb_机构信息> GetAll()
        {
            string sql = "SELECT * FROM tb_机构信息";
            SQLiteDataReader dataReader = DBManager.ExecuteQueryForSQLite(sql);
            List<tb_机构信息> List = DataReaderToList(dataReader);
            dataReader.Close();
            return List;
        }
        
        /// <summary>
        /// 根据主键查询一条记录
        /// </summary>
        public tb_机构信息 Get(string 机构编号)
        {
            string sql = "SELECT * FROM tb_机构信息 WHERE 机构编号=@机构编号";
            SQLiteParameter[] parameters = new SQLiteParameter[]{
				new SQLiteParameter("@机构编号",  机构编号 )
			};
            SQLiteDataReader dataReader = DBManager.ExecuteQueryForSQLite(sql, parameters);
            List<tb_机构信息> List = DataReaderToList(dataReader);
            dataReader.Close();
            return List.Count > 0 ? List[0] : null;
        }

        /// <summary>
        /// 添加记录
        /// </summary>
        /// <param name="model">MODEL.tb_MyUser实体类</param>
        /// <returns>新增记录的ID</returns>
        public static void InsertList(List<tb_机构信息> list)
        {
            SQLiteConnection conn = DBManager.SQLiteConn;
            conn.Open();
            DbTransaction trans = conn.BeginTransaction();

            try
            {
                foreach (tb_机构信息 model in list)
                {
                    string sql = @"INSERT INTO tb_机构信息
				(ID,机构编号,机构名称,P_RGID_MARKER,负责人,联系人,联系电话,上级机构,机构级别,创建人,创建时间,状态,P_QYDW) 
				VALUES(@ID,@机构编号,@机构名称,@P_RGID_MARKER,@负责人,@联系人,@联系电话,@上级机构,@机构级别,@创建人,@创建时间,@状态,@P_QYDW);";
                    SQLiteCommand cmd = new SQLiteCommand(sql, conn);
                    cmd.Parameters.AddRange(new SQLiteParameter[]{
                        new SQLiteParameter("@ID",  model.ID ),
				        new SQLiteParameter("@机构编号",  model.机构编号 ),
                        new SQLiteParameter("@机构名称",  model.机构名称 ),
                        new SQLiteParameter("@P_RGID_MARKER", model.P_RGID_MARKER ),
                        new SQLiteParameter("@负责人",  model.负责人 ),
                        new SQLiteParameter("@联系人", model.联系人 ),
                        new SQLiteParameter("@联系电话",  model.联系电话 ),
                        new SQLiteParameter("@上级机构",  model.上级机构 ),
                        new SQLiteParameter("@机构级别", model.机构级别 ),
                        new SQLiteParameter("@创建人",  model.创建人 ),
                        new SQLiteParameter("@创建时间",  model.创建时间 ),
                        new SQLiteParameter("@状态",  model.状态 ),
                        new SQLiteParameter("@P_QYDW",  model.P_QYDW )
			        });
                    cmd.ExecuteNonQuery();
                }
                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                conn.Close();
                conn.Dispose();
                throw ex;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

        }

    }

}
