using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.SQLite;

namespace ATOMEHR_LeaveClient
{
    /// <summary>
    /// tb_老年人中医药特征管理DAL类
    /// BY：狂人代码生成器 V1.0
    /// 作者：金属狂人
    /// 日期：2016年04月12日 03:22:54
    /// </summary>
    public class tb_老年人中医药特征管理DAL
    {

        /// <summary>
        /// 获取所有信息
        /// </summary>
        /// <param name="where">条件</param>
        /// <returns>结果集</returns>
        public static SQLiteDataReader Gettb_老年人中医药特征管理Info(string where)
        {
            string sqlStr = "SELECT * FROM tb_老年人中医药特征管理 where ";
            if (String.IsNullOrEmpty(where))
            {
                sqlStr += "1=1";
            }
            else
            {
                sqlStr += where;
            }
            SQLiteDataReader reader = null;
            try
            {
                reader = DBManager.ExecuteQueryForSQLite(sqlStr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reader;
        }

        /// <summary>
        /// 获取所有信息集合
        /// </summary>
        /// <returns>List集合</returns>
        public static List<tb_老年人中医药特征管理Info> Gettb_老年人中医药特征管理InfoList(string where)
        {
            List<tb_老年人中医药特征管理Info> infoList = new List<tb_老年人中医药特征管理Info>();

            SQLiteDataReader reader = null;
            try
            {
                reader = Gettb_老年人中医药特征管理Info(where);
            }
            catch (Exception)
            {
                throw;
            }

            while (reader.Read())
            {
                tb_老年人中医药特征管理Info info = new tb_老年人中医药特征管理Info();
                info.ID = reader["ID"].ToString();
                info.身份证号 = reader["身份证号"].ToString();
                info.个人档案编号 = reader["个人档案编号"].ToString();
                info.姓名 = reader["姓名"].ToString();
                info.性别 = reader["性别"].ToString();
                info.出生日期 = reader["出生日期"].ToString();
                info.居住地址 = reader["居住地址"].ToString();
                info.创建机构 = reader["创建机构"].ToString();
                info.创建人 = reader["创建人"].ToString();
                info.修改人 = reader["修改人"].ToString();
                info.所属机构 = reader["所属机构"].ToString();
                info.创建时间 = reader["创建时间"].ToString();
                info.修改时间 = reader["修改时间"].ToString();
                info.特征1 = int.Parse(reader["特征1"].ToString());
                info.特征2 = int.Parse(reader["特征2"].ToString());
                info.特征3 = int.Parse(reader["特征3"].ToString());
                info.特征4 = int.Parse(reader["特征4"].ToString());
                info.特征5 = int.Parse(reader["特征5"].ToString());
                info.特征6 = int.Parse(reader["特征6"].ToString());
                info.特征7 = int.Parse(reader["特征7"].ToString());
                info.特征8 = int.Parse(reader["特征8"].ToString());
                info.特征9 = int.Parse(reader["特征9"].ToString());
                info.特征10 = int.Parse(reader["特征10"].ToString());
                info.特征11 = int.Parse(reader["特征11"].ToString());
                info.特征12 = int.Parse(reader["特征12"].ToString());
                info.特征13 = int.Parse(reader["特征13"].ToString());
                info.特征14 = int.Parse(reader["特征14"].ToString());
                info.特征15 = int.Parse(reader["特征15"].ToString());
                info.特征16 = int.Parse(reader["特征16"].ToString());
                info.特征17 = int.Parse(reader["特征17"].ToString());
                info.特征18 = int.Parse(reader["特征18"].ToString());
                info.特征19 = int.Parse(reader["特征19"].ToString());
                info.特征20 = int.Parse(reader["特征20"].ToString());
                info.特征21 = int.Parse(reader["特征21"].ToString());
                info.特征22 = int.Parse(reader["特征22"].ToString());
                info.特征23 = int.Parse(reader["特征23"].ToString());
                info.特征24 = int.Parse(reader["特征24"].ToString());
                info.特征25 = int.Parse(reader["特征25"].ToString());
                info.特征26 = int.Parse(reader["特征26"].ToString());
                info.特征27 = int.Parse(reader["特征27"].ToString());
                info.特征28 = int.Parse(reader["特征28"].ToString());
                info.特征29 = int.Parse(reader["特征29"].ToString());
                info.特征30 = int.Parse(reader["特征30"].ToString());
                info.特征31 = int.Parse(reader["特征31"].ToString());
                info.特征32 = int.Parse(reader["特征32"].ToString());
                info.特征33 = int.Parse(reader["特征33"].ToString());
                info.气虚质得分 = int.Parse(reader["气虚质得分"].ToString());
                info.气虚质辨识 = reader["气虚质辨识"].ToString();
                info.气虚质指导 = reader["气虚质指导"].ToString();
                info.气虚质其他 = reader["气虚质其他"].ToString();
                info.阳虚质得分 = int.Parse(reader["阳虚质得分"].ToString());
                info.阳虚质辨识 = reader["阳虚质辨识"].ToString();
                info.阳虚质指导 = reader["阳虚质指导"].ToString();
                info.阳虚质其他 = reader["阳虚质其他"].ToString();
                info.阴虚质得分 = int.Parse(reader["阴虚质得分"].ToString());
                info.阴虚质辨识 = reader["阴虚质辨识"].ToString();
                info.阴虚质指导 = reader["阴虚质指导"].ToString();
                info.阴虚质其他 = reader["阴虚质其他"].ToString();
                info.痰湿质得分 = int.Parse(reader["痰湿质得分"].ToString());
                info.痰湿质辨识 = reader["痰湿质辨识"].ToString();
                info.痰湿质指导 = reader["痰湿质指导"].ToString();
                info.痰湿质其他 = reader["痰湿质其他"].ToString();
                info.湿热质得分 = int.Parse(reader["湿热质得分"].ToString());
                info.湿热质辨识 = reader["湿热质辨识"].ToString();
                info.湿热质指导 = reader["湿热质指导"].ToString();
                info.湿热质其他 = reader["湿热质其他"].ToString();
                info.血瘀质得分 = int.Parse(reader["血瘀质得分"].ToString());
                info.血瘀质辨识 = reader["血瘀质辨识"].ToString();
                info.血瘀质指导 = reader["血瘀质指导"].ToString();
                info.血瘀质其他 = reader["血瘀质其他"].ToString();
                info.气郁质得分 = int.Parse(reader["气郁质得分"].ToString());
                info.气郁质辨识 = reader["气郁质辨识"].ToString();
                info.气郁质指导 = reader["气郁质指导"].ToString();
                info.气郁质其他 = reader["气郁质其他"].ToString();
                info.特禀质得分 = int.Parse(reader["特禀质得分"].ToString());
                info.特禀质辨识 = reader["特禀质辨识"].ToString();
                info.特禀质指导 = reader["特禀质指导"].ToString();
                info.特禀质其他 = reader["特禀质其他"].ToString();
                info.平和质得分 = int.Parse(reader["平和质得分"].ToString());
                info.平和质辨识 = reader["平和质辨识"].ToString();
                info.平和质指导 = reader["平和质指导"].ToString();
                info.平和质其他 = reader["平和质其他"].ToString();
                info.发生时间 = reader["发生时间"].ToString();
                info.医生签名 = reader["医生签名"].ToString();
                info.BlueName = reader["BlueName"].ToString();
                info.BlueAddr = reader["BlueAddr"].ToString();
                info.BlueType = reader["BlueType"].ToString();
                info.RowState = reader["RowState"].ToString();
                infoList.Add(info);
            }
            reader.Close();
            return infoList;
        }

        /// <summary>
        /// 根据 主键 获取一个实体对象
        /// <param name="ID">身份证号</param>
        /// </summary>
        public static tb_老年人中医药特征管理Info Gettb_老年人中医药特征管理InfoById(string ID)
        {
            string strWhere = " 身份证号 =" + ID;
            List<tb_老年人中医药特征管理Info> list = Gettb_老年人中医药特征管理InfoList(strWhere);
            if (list.Count > 0)
                return list[0];
            return null;
        }

        /// 添加数据
        /// </summary>
        /// <param name="info">数据表实体对象</param>
        public static bool Addtb_老年人中医药特征管理(tb_老年人中医药特征管理Info info)
        {
            string _身份证号 = Program.currentUser.ID;
            string _姓名 = Program.currentUser.Name;
            string _性别 = Program.currentUser.Gender;
            string _出生日期 = Program.currentUser.Birth;
            string _居住地址 = Program.currentUser.Addr;
            string _个人档案编号 = Program.currentUser.DocNo;
            string _创建机构 = Program.currentUser.Prgid;
            string _创建人 = Program.currentUser.创建人; //"371323B100190001";
            string _修改人 = Program.currentUser.创建人; //"371323B100190001";
            string _所属机构 = Program.currentUser.Prgid;
            string _创建时间 = Program._currentTime;
            string _修改时间 = Program._currentTime;
            int _特征1 = info.特征1;
            int _特征2 = info.特征2;
            int _特征3 = info.特征3;
            int _特征4 = info.特征4;
            int _特征5 = info.特征5;
            int _特征6 = info.特征6;
            int _特征7 = info.特征7;
            int _特征8 = info.特征8;
            int _特征9 = info.特征9;
            int _特征10 = info.特征10;
            int _特征11 = info.特征11;
            int _特征12 = info.特征12;
            int _特征13 = info.特征13;
            int _特征14 = info.特征14;
            int _特征15 = info.特征15;
            int _特征16 = info.特征16;
            int _特征17 = info.特征17;
            int _特征18 = info.特征18;
            int _特征19 = info.特征19;
            int _特征20 = info.特征20;
            int _特征21 = info.特征21;
            int _特征22 = info.特征22;
            int _特征23 = info.特征23;
            int _特征24 = info.特征24;
            int _特征25 = info.特征25;
            int _特征26 = info.特征26;
            int _特征27 = info.特征27;
            int _特征28 = info.特征28;
            int _特征29 = info.特征29;
            int _特征30 = info.特征30;
            int _特征31 = info.特征31;
            int _特征32 = info.特征32;
            int _特征33 = info.特征33;
            int _气虚质得分 = info.气虚质得分;
            string _气虚质辨识 = info.气虚质辨识;
            string _气虚质指导 = info.气虚质指导;
            string _气虚质其他 = info.气虚质其他;
            int _阳虚质得分 = info.阳虚质得分;
            string _阳虚质辨识 = info.阳虚质辨识;
            string _阳虚质指导 = info.阳虚质指导;
            string _阳虚质其他 = info.阳虚质其他;
            int _阴虚质得分 = info.阴虚质得分;
            string _阴虚质辨识 = info.阴虚质辨识;
            string _阴虚质指导 = info.阴虚质指导;
            string _阴虚质其他 = info.阴虚质其他;
            int _痰湿质得分 = info.痰湿质得分;
            string _痰湿质辨识 = info.痰湿质辨识;
            string _痰湿质指导 = info.痰湿质指导;
            string _痰湿质其他 = info.痰湿质其他;
            int _湿热质得分 = info.湿热质得分;
            string _湿热质辨识 = info.湿热质辨识;
            string _湿热质指导 = info.湿热质指导;
            string _湿热质其他 = info.湿热质其他;
            int _血瘀质得分 = info.血瘀质得分;
            string _血瘀质辨识 = info.血瘀质辨识;
            string _血瘀质指导 = info.血瘀质指导;
            string _血瘀质其他 = info.血瘀质其他;
            int _气郁质得分 = info.气郁质得分;
            string _气郁质辨识 = info.气郁质辨识;
            string _气郁质指导 = info.气郁质指导;
            string _气郁质其他 = info.气郁质其他;
            int _特禀质得分 = info.特禀质得分;
            string _特禀质辨识 = info.特禀质辨识;
            string _特禀质指导 = info.特禀质指导;
            string _特禀质其他 = info.特禀质其他;
            int _平和质得分 = info.平和质得分;
            string _平和质辨识 = info.平和质辨识;
            string _平和质指导 = info.平和质指导;
            string _平和质其他 = info.平和质其他;
            string _发生时间 = info.发生时间;
            string _医生签名 = info.医生签名;
            string _BlueName = Program.BlueName;
            string _BlueAddr = Program.BlueAddr;
            string _BlueType = Program.BlueType;
            string sql = "INSERT INTO tb_老年人中医药特征管理 VALUES (NULL,@_个人档案编号,@_身份证号,@_姓名,@_性别,@_出生日期,@_居住地址,@_创建机构,@_创建人,@_修改人,@_所属机构,@_创建时间,@_修改时间,@_特征1,@_特征2,@_特征3,@_特征4,@_特征5,@_特征6,@_特征7,@_特征8,@_特征9,@_特征10,@_特征11,@_特征12,@_特征13,@_特征14,@_特征15,@_特征16,@_特征17,@_特征18,@_特征19,@_特征20,@_特征21,@_特征22,@_特征23,@_特征24,@_特征25,@_特征26,@_特征27,@_特征28,@_特征29,@_特征30,@_特征31,@_特征32,@_特征33,@_气虚质得分,@_气虚质辨识,@_气虚质指导,@_气虚质其他,@_阳虚质得分,@_阳虚质辨识,@_阳虚质指导,@_阳虚质其他,@_阴虚质得分,@_阴虚质辨识,@_阴虚质指导,@_阴虚质其他,@_痰湿质得分,@_痰湿质辨识,@_痰湿质指导,@_痰湿质其他,@_湿热质得分,@_湿热质辨识,@_湿热质指导,@_湿热质其他,@_血瘀质得分,@_血瘀质辨识,@_血瘀质指导,@_血瘀质其他,@_气郁质得分,@_气郁质辨识,@_气郁质指导,@_气郁质其他,@_特禀质得分,@_特禀质辨识,@_特禀质指导,@_特禀质其他,@_平和质得分,@_平和质辨识,@_平和质指导,@_平和质其他,@_发生时间,@_医生签名,'',@BlueAddr,@BlueType,@BlueName)";

            int rst = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sql, new object[] { new SQLiteParameter("@_个人档案编号", _个人档案编号), new SQLiteParameter("@_身份证号", _身份证号), new SQLiteParameter("@_姓名", _姓名), new SQLiteParameter("@_性别", _性别), new SQLiteParameter("@_出生日期", _出生日期), new SQLiteParameter("@_居住地址", _居住地址), new SQLiteParameter("@_创建机构", _创建机构), new SQLiteParameter("@_创建人", _创建人), new SQLiteParameter("@_修改人", _修改人), new SQLiteParameter("@_所属机构", _所属机构), new SQLiteParameter("@_创建时间", _创建时间), new SQLiteParameter("@_修改时间", _修改时间), new SQLiteParameter("@_特征1", _特征1), new SQLiteParameter("@_特征2", _特征2), new SQLiteParameter("@_特征3", _特征3), new SQLiteParameter("@_特征4", _特征4), new SQLiteParameter("@_特征5", _特征5), new SQLiteParameter("@_特征6", _特征6), new SQLiteParameter("@_特征7", _特征7), new SQLiteParameter("@_特征8", _特征8), new SQLiteParameter("@_特征9", _特征9), new SQLiteParameter("@_特征10", _特征10), new SQLiteParameter("@_特征11", _特征11), new SQLiteParameter("@_特征12", _特征12), new SQLiteParameter("@_特征13", _特征13), new SQLiteParameter("@_特征14", _特征14), new SQLiteParameter("@_特征15", _特征15), new SQLiteParameter("@_特征16", _特征16), new SQLiteParameter("@_特征17", _特征17), new SQLiteParameter("@_特征18", _特征18), new SQLiteParameter("@_特征19", _特征19), new SQLiteParameter("@_特征20", _特征20), new SQLiteParameter("@_特征21", _特征21), new SQLiteParameter("@_特征22", _特征22), new SQLiteParameter("@_特征23", _特征23), new SQLiteParameter("@_特征24", _特征24), new SQLiteParameter("@_特征25", _特征25), new SQLiteParameter("@_特征26", _特征26), new SQLiteParameter("@_特征27", _特征27), new SQLiteParameter("@_特征28", _特征28), new SQLiteParameter("@_特征29", _特征29), new SQLiteParameter("@_特征30", _特征30), new SQLiteParameter("@_特征31", _特征31), new SQLiteParameter("@_特征32", _特征32), new SQLiteParameter("@_特征33", _特征33), new SQLiteParameter("@_气虚质得分", _气虚质得分), new SQLiteParameter("@_气虚质辨识", _气虚质辨识), new SQLiteParameter("@_气虚质指导", _气虚质指导), new SQLiteParameter("@_气虚质其他", _气虚质其他), new SQLiteParameter("@_阳虚质得分", _阳虚质得分), new SQLiteParameter("@_阳虚质辨识", _阳虚质辨识), new SQLiteParameter("@_阳虚质指导", _阳虚质指导), new SQLiteParameter("@_阳虚质其他", _阳虚质其他), new SQLiteParameter("@_阴虚质得分", _阴虚质得分), new SQLiteParameter("@_阴虚质辨识", _阴虚质辨识), new SQLiteParameter("@_阴虚质指导", _阴虚质指导), new SQLiteParameter("@_阴虚质其他", _阴虚质其他), new SQLiteParameter("@_痰湿质得分", _痰湿质得分), new SQLiteParameter("@_痰湿质辨识", _痰湿质辨识), new SQLiteParameter("@_痰湿质指导", _痰湿质指导), new SQLiteParameter("@_痰湿质其他", _痰湿质其他), new SQLiteParameter("@_湿热质得分", _湿热质得分), new SQLiteParameter("@_湿热质辨识", _湿热质辨识), new SQLiteParameter("@_湿热质指导", _湿热质指导), new SQLiteParameter("@_湿热质其他", _湿热质其他), new SQLiteParameter("@_血瘀质得分", _血瘀质得分), new SQLiteParameter("@_血瘀质辨识", _血瘀质辨识), new SQLiteParameter("@_血瘀质指导", _血瘀质指导), new SQLiteParameter("@_血瘀质其他", _血瘀质其他), new SQLiteParameter("@_气郁质得分", _气郁质得分), new SQLiteParameter("@_气郁质辨识", _气郁质辨识), new SQLiteParameter("@_气郁质指导", _气郁质指导), new SQLiteParameter("@_气郁质其他", _气郁质其他), new SQLiteParameter("@_特禀质得分", _特禀质得分), new SQLiteParameter("@_特禀质辨识", _特禀质辨识), new SQLiteParameter("@_特禀质指导", _特禀质指导), new SQLiteParameter("@_特禀质其他", _特禀质其他), new SQLiteParameter("@_平和质得分", _平和质得分), new SQLiteParameter("@_平和质辨识", _平和质辨识), new SQLiteParameter("@_平和质指导", _平和质指导), new SQLiteParameter("@_平和质其他", _平和质其他), new SQLiteParameter("@_发生时间", _发生时间), new SQLiteParameter("@_医生签名", _医生签名), new SQLiteParameter("@BlueAddr", Program.BlueAddr), new SQLiteParameter("@BlueType", Program.BlueType), new SQLiteParameter("@BlueName", Program.BlueName) });
            if (rst != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool importtb_老年人中医药特征管理(string spName, tb_老年人中医药特征管理Info info)
        {
            string _身份证号 = info.身份证号;
            string _姓名 = info.姓名;
            string _性别 = info.性别;
            string _出生日期 = info.出生日期;
            string _居住地址 = info.居住地址;
            string _个人档案编号 = info.个人档案编号;
            string _创建机构 = info.创建机构;
            string _创建人 = info.创建人;
            string _修改人 = info.修改人;
            string _所属机构 = info.所属机构;
            string _创建时间 = info.创建时间;
            string _修改时间 = info.修改时间;
            int _特征1 = info.特征1;
            int _特征2 = info.特征2;
            int _特征3 = info.特征3;
            int _特征4 = info.特征4;
            int _特征5 = info.特征5;
            int _特征6 = info.特征6;
            int _特征7 = info.特征7;
            int _特征8 = info.特征8;
            int _特征9 = info.特征9;
            int _特征10 = info.特征10;
            int _特征11 = info.特征11;
            int _特征12 = info.特征12;
            int _特征13 = info.特征13;
            int _特征14 = info.特征14;
            int _特征15 = info.特征15;
            int _特征16 = info.特征16;
            int _特征17 = info.特征17;
            int _特征18 = info.特征18;
            int _特征19 = info.特征19;
            int _特征20 = info.特征20;
            int _特征21 = info.特征21;
            int _特征22 = info.特征22;
            int _特征23 = info.特征23;
            int _特征24 = info.特征24;
            int _特征25 = info.特征25;
            int _特征26 = info.特征26;
            int _特征27 = info.特征27;
            int _特征28 = info.特征28;
            int _特征29 = info.特征29;
            int _特征30 = info.特征30;
            int _特征31 = info.特征31;
            int _特征32 = info.特征32;
            int _特征33 = info.特征33;
            int _气虚质得分 = info.气虚质得分;
            string _气虚质辨识 = info.气虚质辨识;
            string _气虚质指导 = info.气虚质指导;
            string _气虚质其他 = info.气虚质其他;
            int _阳虚质得分 = info.阳虚质得分;
            string _阳虚质辨识 = info.阳虚质辨识;
            string _阳虚质指导 = info.阳虚质指导;
            string _阳虚质其他 = info.阳虚质其他;
            int _阴虚质得分 = info.阴虚质得分;
            string _阴虚质辨识 = info.阴虚质辨识;
            string _阴虚质指导 = info.阴虚质指导;
            string _阴虚质其他 = info.阴虚质其他;
            int _痰湿质得分 = info.痰湿质得分;
            string _痰湿质辨识 = info.痰湿质辨识;
            string _痰湿质指导 = info.痰湿质指导;
            string _痰湿质其他 = info.痰湿质其他;
            int _湿热质得分 = info.湿热质得分;
            string _湿热质辨识 = info.湿热质辨识;
            string _湿热质指导 = info.湿热质指导;
            string _湿热质其他 = info.湿热质其他;
            int _血瘀质得分 = info.血瘀质得分;
            string _血瘀质辨识 = info.血瘀质辨识;
            string _血瘀质指导 = info.血瘀质指导;
            string _血瘀质其他 = info.血瘀质其他;
            int _气郁质得分 = info.气郁质得分;
            string _气郁质辨识 = info.气郁质辨识;
            string _气郁质指导 = info.气郁质指导;
            string _气郁质其他 = info.气郁质其他;
            int _特禀质得分 = info.特禀质得分;
            string _特禀质辨识 = info.特禀质辨识;
            string _特禀质指导 = info.特禀质指导;
            string _特禀质其他 = info.特禀质其他;
            int _平和质得分 = info.平和质得分;
            string _平和质辨识 = info.平和质辨识;
            string _平和质指导 = info.平和质指导;
            string _平和质其他 = info.平和质其他;
            string _发生时间 = info.发生时间;
            string _医生签名 = info.医生签名;
            string _BlueName = info.BlueName;
            string _BlueAddr = info.BlueAddr;
            string _BlueType = info.BlueType;

            string sql = "INSERT INTO tb_老年人中医药特征管理 VALUES (@_身份证号,@_姓名,@_性别,@_出生日期,@_居住地址,@_创建机构,@_创建人,@_修改人,@_所属机构,@_创建时间,@_修改时间,@_特征1,@_特征2,@_特征3,@_特征4,@_特征5,@_特征6,@_特征7,@_特征8,@_特征9,@_特征10,@_特征11,@_特征12,@_特征13,@_特征14,@_特征15,@_特征16,@_特征17,@_特征18,@_特征19,@_特征20,@_特征21,@_特征22,@_特征23,@_特征24,@_特征25,@_特征26,@_特征27,@_特征28,@_特征29,@_特征30,@_特征31,@_特征32,@_特征33,@_气虚质得分,@_气虚质辨识,@_气虚质指导,@_气虚质其他,@_阳虚质得分,@_阳虚质辨识,@_阳虚质指导,@_阳虚质其他,@_阴虚质得分,@_阴虚质辨识,@_阴虚质指导,@_阴虚质其他,@_痰湿质得分,@_痰湿质辨识,@_痰湿质指导,@_痰湿质其他,@_湿热质得分,@_湿热质辨识,@_湿热质指导,@_湿热质其他,@_血瘀质得分,@_血瘀质辨识,@_血瘀质指导,@_血瘀质其他,@_气郁质得分,@_气郁质辨识,@_气郁质指导,@_气郁质其他,@_特禀质得分,@_特禀质辨识,@_特禀质指导,@_特禀质其他,@_平和质得分,@_平和质辨识,@_平和质指导,@_平和质其他,@_发生时间,@_医生签名)";

            int rst = DBManager.ExecuteProc(DBManager.Conn121, "USP_import中医药体质辨识", new object[] { new SqlParameter("@身份证号", _身份证号), new SqlParameter("@姓名", _姓名), new SqlParameter("@性别", _性别), new SqlParameter("@出生日期", _出生日期), new SqlParameter("@居住地址", _居住地址), new SqlParameter("@个人档案编号", _个人档案编号), new SqlParameter("@创建机构", _创建机构), new SqlParameter("@创建人", _创建人), new SqlParameter("@修改人", _修改人), new SqlParameter("@所属机构", _所属机构), new SqlParameter("@创建时间", _创建时间), new SqlParameter("@修改时间", _修改时间), new SqlParameter("@特征1", _特征1), new SqlParameter("@特征2", _特征2), new SqlParameter("@特征3", _特征3), new SqlParameter("@特征4", _特征4), new SqlParameter("@特征5", _特征5), new SqlParameter("@特征6", _特征6), new SqlParameter("@特征7", _特征7), new SqlParameter("@特征8", _特征8), new SqlParameter("@特征9", _特征9), new SqlParameter("@特征10", _特征10), new SqlParameter("@特征11", _特征11), new SqlParameter("@特征12", _特征12), new SqlParameter("@特征13", _特征13), new SqlParameter("@特征14", _特征14), new SqlParameter("@特征15", _特征15), new SqlParameter("@特征16", _特征16), new SqlParameter("@特征17", _特征17), new SqlParameter("@特征18", _特征18), new SqlParameter("@特征19", _特征19), new SqlParameter("@特征20", _特征20), new SqlParameter("@特征21", _特征21), new SqlParameter("@特征22", _特征22), new SqlParameter("@特征23", _特征23), new SqlParameter("@特征24", _特征24), new SqlParameter("@特征25", _特征25), new SqlParameter("@特征26", _特征26), new SqlParameter("@特征27", _特征27), new SqlParameter("@特征28", _特征28), new SqlParameter("@特征29", _特征29), new SqlParameter("@特征30", _特征30), new SqlParameter("@特征31", _特征31), new SqlParameter("@特征32", _特征32), new SqlParameter("@特征33", _特征33), new SqlParameter("@气虚质得分", _气虚质得分), new SqlParameter("@气虚质辨识", _气虚质辨识), new SqlParameter("@气虚质指导", _气虚质指导), new SqlParameter("@气虚质其他", _气虚质其他), new SqlParameter("@阳虚质得分", _阳虚质得分), new SqlParameter("@阳虚质辨识", _阳虚质辨识), new SqlParameter("@阳虚质指导", _阳虚质指导), new SqlParameter("@阳虚质其他", _阳虚质其他), new SqlParameter("@阴虚质得分", _阴虚质得分), new SqlParameter("@阴虚质辨识", _阴虚质辨识), new SqlParameter("@阴虚质指导", _阴虚质指导), new SqlParameter("@阴虚质其他", _阴虚质其他), new SqlParameter("@痰湿质得分", _痰湿质得分), new SqlParameter("@痰湿质辨识", _痰湿质辨识), new SqlParameter("@痰湿质指导", _痰湿质指导), new SqlParameter("@痰湿质其他", _痰湿质其他), new SqlParameter("@湿热质得分", _湿热质得分), new SqlParameter("@湿热质辨识", _湿热质辨识), new SqlParameter("@湿热质指导", _湿热质指导), new SqlParameter("@湿热质其他", _湿热质其他), new SqlParameter("@血瘀质得分", _血瘀质得分), new SqlParameter("@血瘀质辨识", _血瘀质辨识), new SqlParameter("@血瘀质指导", _血瘀质指导), new SqlParameter("@血瘀质其他", _血瘀质其他), new SqlParameter("@气郁质得分", _气郁质得分), new SqlParameter("@气郁质辨识", _气郁质辨识), new SqlParameter("@气郁质指导", _气郁质指导), new SqlParameter("@气郁质其他", _气郁质其他), new SqlParameter("@特禀质得分", _特禀质得分), new SqlParameter("@特禀质辨识", _特禀质辨识), new SqlParameter("@特禀质指导", _特禀质指导), new SqlParameter("@特禀质其他", _特禀质其他), new SqlParameter("@平和质得分", _平和质得分), new SqlParameter("@平和质辨识", _平和质辨识), new SqlParameter("@平和质指导", _平和质指导), new SqlParameter("@平和质其他", _平和质其他), new SqlParameter("@发生时间", _发生时间), new SqlParameter("@医生签名", _医生签名), new SqlParameter("@BlueName", _BlueName), new SqlParameter("@BlueAddr", _BlueAddr), new SqlParameter("@BlueType", _BlueType) });

            //int rst = DBManager.ExecuteUpdate(sql, new object[] { new SqlParameter("@_身份证号",_身份证号),new SqlParameter("@_姓名",_姓名),new SqlParameter("@_性别",_性别),new SqlParameter("@_出生日期",_出生日期),new SqlParameter("@_居住地址",_居住地址),new SqlParameter("@_创建机构",_创建机构),new SqlParameter("@_创建人",_创建人),new SqlParameter("@_修改人",_修改人),new SqlParameter("@_所属机构",_所属机构),new SqlParameter("@_创建时间",_创建时间),new SqlParameter("@_修改时间",_修改时间),new SqlParameter("@_特征1",_特征1),new SqlParameter("@_特征2",_特征2),new SqlParameter("@_特征3",_特征3),new SqlParameter("@_特征4",_特征4),new SqlParameter("@_特征5",_特征5),new SqlParameter("@_特征6",_特征6),new SqlParameter("@_特征7",_特征7),new SqlParameter("@_特征8",_特征8),new SqlParameter("@_特征9",_特征9),new SqlParameter("@_特征10",_特征10),new SqlParameter("@_特征11",_特征11),new SqlParameter("@_特征12",_特征12),new SqlParameter("@_特征13",_特征13),new SqlParameter("@_特征14",_特征14),new SqlParameter("@_特征15",_特征15),new SqlParameter("@_特征16",_特征16),new SqlParameter("@_特征17",_特征17),new SqlParameter("@_特征18",_特征18),new SqlParameter("@_特征19",_特征19),new SqlParameter("@_特征20",_特征20),new SqlParameter("@_特征21",_特征21),new SqlParameter("@_特征22",_特征22),new SqlParameter("@_特征23",_特征23),new SqlParameter("@_特征24",_特征24),new SqlParameter("@_特征25",_特征25),new SqlParameter("@_特征26",_特征26),new SqlParameter("@_特征27",_特征27),new SqlParameter("@_特征28",_特征28),new SqlParameter("@_特征29",_特征29),new SqlParameter("@_特征30",_特征30),new SqlParameter("@_特征31",_特征31),new SqlParameter("@_特征32",_特征32),new SqlParameter("@_特征33",_特征33),new SqlParameter("@_气虚质得分",_气虚质得分),new SqlParameter("@_气虚质辨识",_气虚质辨识),new SqlParameter("@_气虚质指导",_气虚质指导),new SqlParameter("@_气虚质其他",_气虚质其他),new SqlParameter("@_阳虚质得分",_阳虚质得分),new SqlParameter("@_阳虚质辨识",_阳虚质辨识),new SqlParameter("@_阳虚质指导",_阳虚质指导),new SqlParameter("@_阳虚质其他",_阳虚质其他),new SqlParameter("@_阴虚质得分",_阴虚质得分),new SqlParameter("@_阴虚质辨识",_阴虚质辨识),new SqlParameter("@_阴虚质指导",_阴虚质指导),new SqlParameter("@_阴虚质其他",_阴虚质其他),new SqlParameter("@_痰湿质得分",_痰湿质得分),new SqlParameter("@_痰湿质辨识",_痰湿质辨识),new SqlParameter("@_痰湿质指导",_痰湿质指导),new SqlParameter("@_痰湿质其他",_痰湿质其他),new SqlParameter("@_湿热质得分",_湿热质得分),new SqlParameter("@_湿热质辨识",_湿热质辨识),new SqlParameter("@_湿热质指导",_湿热质指导),new SqlParameter("@_湿热质其他",_湿热质其他),new SqlParameter("@_血瘀质得分",_血瘀质得分),new SqlParameter("@_血瘀质辨识",_血瘀质辨识),new SqlParameter("@_血瘀质指导",_血瘀质指导),new SqlParameter("@_血瘀质其他",_血瘀质其他),new SqlParameter("@_气郁质得分",_气郁质得分),new SqlParameter("@_气郁质辨识",_气郁质辨识),new SqlParameter("@_气郁质指导",_气郁质指导),new SqlParameter("@_气郁质其他",_气郁质其他),new SqlParameter("@_特禀质得分",_特禀质得分),new SqlParameter("@_特禀质辨识",_特禀质辨识),new SqlParameter("@_特禀质指导",_特禀质指导),new SqlParameter("@_特禀质其他",_特禀质其他),new SqlParameter("@_平和质得分",_平和质得分),new SqlParameter("@_平和质辨识",_平和质辨识),new SqlParameter("@_平和质指导",_平和质指导),new SqlParameter("@_平和质其他",_平和质其他),new SqlParameter("@_发生时间",_发生时间),new SqlParameter("@_医生签名",_医生签名) });
            if (rst != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除一个对象
        /// </summary>
        /// <param name="ID">主键</param>
        /// <returns></returns>
        public static bool Deletetb_老年人中医药特征管理Info(tb_老年人中医药特征管理Info info)
        {
            bool rst = false;
            string sqlStr = "DELETE FROM tb_老年人中医药特征管理 WHERE ID=" + info.ID;
            int rows = DBManager.ExecuteUpdate(DBManager.Conn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }

        /// 更新数据
        /// </summary>
        /// <param name="info">数据表实体</param>
        public static bool Updatetb_老年人中医药特征管理Info(tb_老年人中医药特征管理Info info)
        {
            string _身份证号 = info.身份证号;
            string _姓名 = info.姓名;
            string _性别 = info.性别;
            string _出生日期 = info.出生日期;
            string _居住地址 = info.居住地址;
            string _创建机构 = info.创建机构;
            string _创建人 = info.创建人;
            string _修改人 = info.修改人;
            string _所属机构 = info.所属机构;
            string _创建时间 = info.创建时间;
            string _修改时间 = info.修改时间;
            int _特征1 = info.特征1;
            int _特征2 = info.特征2;
            int _特征3 = info.特征3;
            int _特征4 = info.特征4;
            int _特征5 = info.特征5;
            int _特征6 = info.特征6;
            int _特征7 = info.特征7;
            int _特征8 = info.特征8;
            int _特征9 = info.特征9;
            int _特征10 = info.特征10;
            int _特征11 = info.特征11;
            int _特征12 = info.特征12;
            int _特征13 = info.特征13;
            int _特征14 = info.特征14;
            int _特征15 = info.特征15;
            int _特征16 = info.特征16;
            int _特征17 = info.特征17;
            int _特征18 = info.特征18;
            int _特征19 = info.特征19;
            int _特征20 = info.特征20;
            int _特征21 = info.特征21;
            int _特征22 = info.特征22;
            int _特征23 = info.特征23;
            int _特征24 = info.特征24;
            int _特征25 = info.特征25;
            int _特征26 = info.特征26;
            int _特征27 = info.特征27;
            int _特征28 = info.特征28;
            int _特征29 = info.特征29;
            int _特征30 = info.特征30;
            int _特征31 = info.特征31;
            int _特征32 = info.特征32;
            int _特征33 = info.特征33;
            int _气虚质得分 = info.气虚质得分;
            string _气虚质辨识 = info.气虚质辨识;
            string _气虚质指导 = info.气虚质指导;
            string _气虚质其他 = info.气虚质其他;
            int _阳虚质得分 = info.阳虚质得分;
            string _阳虚质辨识 = info.阳虚质辨识;
            string _阳虚质指导 = info.阳虚质指导;
            string _阳虚质其他 = info.阳虚质其他;
            int _阴虚质得分 = info.阴虚质得分;
            string _阴虚质辨识 = info.阴虚质辨识;
            string _阴虚质指导 = info.阴虚质指导;
            string _阴虚质其他 = info.阴虚质其他;
            int _痰湿质得分 = info.痰湿质得分;
            string _痰湿质辨识 = info.痰湿质辨识;
            string _痰湿质指导 = info.痰湿质指导;
            string _痰湿质其他 = info.痰湿质其他;
            int _湿热质得分 = info.湿热质得分;
            string _湿热质辨识 = info.湿热质辨识;
            string _湿热质指导 = info.湿热质指导;
            string _湿热质其他 = info.湿热质其他;
            int _血瘀质得分 = info.血瘀质得分;
            string _血瘀质辨识 = info.血瘀质辨识;
            string _血瘀质指导 = info.血瘀质指导;
            string _血瘀质其他 = info.血瘀质其他;
            int _气郁质得分 = info.气郁质得分;
            string _气郁质辨识 = info.气郁质辨识;
            string _气郁质指导 = info.气郁质指导;
            string _气郁质其他 = info.气郁质其他;
            int _特禀质得分 = info.特禀质得分;
            string _特禀质辨识 = info.特禀质辨识;
            string _特禀质指导 = info.特禀质指导;
            string _特禀质其他 = info.特禀质其他;
            int _平和质得分 = info.平和质得分;
            string _平和质辨识 = info.平和质辨识;
            string _平和质指导 = info.平和质指导;
            string _平和质其他 = info.平和质其他;
            string _发生时间 = info.发生时间;
            string _医生签名 = info.医生签名;
            string sql = "UPDATE tb_老年人中医药特征管理 SET 身份证号=@_身份证号, 姓名=@_姓名, 性别=@_性别, 出生日期=@_出生日期, 居住地址=@_居住地址, 创建机构=@_创建机构, 创建人=@_创建人, 修改人=@_修改人, 所属机构=@_所属机构, 创建时间=@_创建时间, 修改时间=@_修改时间, 特征1=@_特征1, 特征2=@_特征2, 特征3=@_特征3, 特征4=@_特征4, 特征5=@_特征5, 特征6=@_特征6, 特征7=@_特征7, 特征8=@_特征8, 特征9=@_特征9, 特征10=@_特征10, 特征11=@_特征11, 特征12=@_特征12, 特征13=@_特征13, 特征14=@_特征14, 特征15=@_特征15, 特征16=@_特征16, 特征17=@_特征17, 特征18=@_特征18, 特征19=@_特征19, 特征20=@_特征20, 特征21=@_特征21, 特征22=@_特征22, 特征23=@_特征23, 特征24=@_特征24, 特征25=@_特征25, 特征26=@_特征26, 特征27=@_特征27, 特征28=@_特征28, 特征29=@_特征29, 特征30=@_特征30, 特征31=@_特征31, 特征32=@_特征32, 特征33=@_特征33, 气虚质得分=@_气虚质得分, 气虚质辨识=@_气虚质辨识, 气虚质指导=@_气虚质指导, 气虚质其他=@_气虚质其他, 阳虚质得分=@_阳虚质得分, 阳虚质辨识=@_阳虚质辨识, 阳虚质指导=@_阳虚质指导, 阳虚质其他=@_阳虚质其他, 阴虚质得分=@_阴虚质得分, 阴虚质辨识=@_阴虚质辨识, 阴虚质指导=@_阴虚质指导, 阴虚质其他=@_阴虚质其他, 痰湿质得分=@_痰湿质得分, 痰湿质辨识=@_痰湿质辨识, 痰湿质指导=@_痰湿质指导, 痰湿质其他=@_痰湿质其他, 湿热质得分=@_湿热质得分, 湿热质辨识=@_湿热质辨识, 湿热质指导=@_湿热质指导, 湿热质其他=@_湿热质其他, 血瘀质得分=@_血瘀质得分, 血瘀质辨识=@_血瘀质辨识, 血瘀质指导=@_血瘀质指导, 血瘀质其他=@_血瘀质其他, 气郁质得分=@_气郁质得分, 气郁质辨识=@_气郁质辨识, 气郁质指导=@_气郁质指导, 气郁质其他=@_气郁质其他, 特禀质得分=@_特禀质得分, 特禀质辨识=@_特禀质辨识, 特禀质指导=@_特禀质指导, 特禀质其他=@_特禀质其他, 平和质得分=@_平和质得分, 平和质辨识=@_平和质辨识, 平和质指导=@_平和质指导, 平和质其他=@_平和质其他, 发生时间=@_发生时间, 医生签名=@_医生签名  WHERE 身份证号='" + info.身份证号 + "' and substr(创建时间,0,10) = substr('" + Program._currentTime + "',0,10)";
            int rst = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sql, new object[] { new SQLiteParameter("@_身份证号", _身份证号), new SQLiteParameter("@_姓名", _姓名), new SQLiteParameter("@_性别", _性别), new SQLiteParameter("@_出生日期", _出生日期), new SQLiteParameter("@_居住地址", _居住地址), new SQLiteParameter("@_创建机构", _创建机构), new SQLiteParameter("@_创建人", _创建人), new SQLiteParameter("@_修改人", _修改人), new SQLiteParameter("@_所属机构", _所属机构), new SQLiteParameter("@_创建时间", _创建时间), new SQLiteParameter("@_修改时间", _修改时间), new SQLiteParameter("@_特征1", _特征1), new SQLiteParameter("@_特征2", _特征2), new SQLiteParameter("@_特征3", _特征3), new SQLiteParameter("@_特征4", _特征4), new SQLiteParameter("@_特征5", _特征5), new SQLiteParameter("@_特征6", _特征6), new SQLiteParameter("@_特征7", _特征7), new SQLiteParameter("@_特征8", _特征8), new SQLiteParameter("@_特征9", _特征9), new SQLiteParameter("@_特征10", _特征10), new SQLiteParameter("@_特征11", _特征11), new SQLiteParameter("@_特征12", _特征12), new SQLiteParameter("@_特征13", _特征13), new SQLiteParameter("@_特征14", _特征14), new SQLiteParameter("@_特征15", _特征15), new SQLiteParameter("@_特征16", _特征16), new SQLiteParameter("@_特征17", _特征17), new SQLiteParameter("@_特征18", _特征18), new SQLiteParameter("@_特征19", _特征19), new SQLiteParameter("@_特征20", _特征20), new SQLiteParameter("@_特征21", _特征21), new SQLiteParameter("@_特征22", _特征22), new SQLiteParameter("@_特征23", _特征23), new SQLiteParameter("@_特征24", _特征24), new SQLiteParameter("@_特征25", _特征25), new SQLiteParameter("@_特征26", _特征26), new SQLiteParameter("@_特征27", _特征27), new SQLiteParameter("@_特征28", _特征28), new SQLiteParameter("@_特征29", _特征29), new SQLiteParameter("@_特征30", _特征30), new SQLiteParameter("@_特征31", _特征31), new SQLiteParameter("@_特征32", _特征32), new SQLiteParameter("@_特征33", _特征33), new SQLiteParameter("@_气虚质得分", _气虚质得分), new SQLiteParameter("@_气虚质辨识", _气虚质辨识), new SQLiteParameter("@_气虚质指导", _气虚质指导), new SQLiteParameter("@_气虚质其他", _气虚质其他), new SQLiteParameter("@_阳虚质得分", _阳虚质得分), new SQLiteParameter("@_阳虚质辨识", _阳虚质辨识), new SQLiteParameter("@_阳虚质指导", _阳虚质指导), new SQLiteParameter("@_阳虚质其他", _阳虚质其他), new SQLiteParameter("@_阴虚质得分", _阴虚质得分), new SQLiteParameter("@_阴虚质辨识", _阴虚质辨识), new SQLiteParameter("@_阴虚质指导", _阴虚质指导), new SQLiteParameter("@_阴虚质其他", _阴虚质其他), new SQLiteParameter("@_痰湿质得分", _痰湿质得分), new SQLiteParameter("@_痰湿质辨识", _痰湿质辨识), new SQLiteParameter("@_痰湿质指导", _痰湿质指导), new SQLiteParameter("@_痰湿质其他", _痰湿质其他), new SQLiteParameter("@_湿热质得分", _湿热质得分), new SQLiteParameter("@_湿热质辨识", _湿热质辨识), new SQLiteParameter("@_湿热质指导", _湿热质指导), new SQLiteParameter("@_湿热质其他", _湿热质其他), new SQLiteParameter("@_血瘀质得分", _血瘀质得分), new SQLiteParameter("@_血瘀质辨识", _血瘀质辨识), new SQLiteParameter("@_血瘀质指导", _血瘀质指导), new SQLiteParameter("@_血瘀质其他", _血瘀质其他), new SQLiteParameter("@_气郁质得分", _气郁质得分), new SQLiteParameter("@_气郁质辨识", _气郁质辨识), new SQLiteParameter("@_气郁质指导", _气郁质指导), new SQLiteParameter("@_气郁质其他", _气郁质其他), new SQLiteParameter("@_特禀质得分", _特禀质得分), new SQLiteParameter("@_特禀质辨识", _特禀质辨识), new SQLiteParameter("@_特禀质指导", _特禀质指导), new SQLiteParameter("@_特禀质其他", _特禀质其他), new SQLiteParameter("@_平和质得分", _平和质得分), new SQLiteParameter("@_平和质辨识", _平和质辨识), new SQLiteParameter("@_平和质指导", _平和质指导), new SQLiteParameter("@_平和质其他", _平和质其他), new SQLiteParameter("@_发生时间", _发生时间), new SQLiteParameter("@_医生签名", _医生签名) });
            if (rst > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal static bool UpdateRowState(tb_老年人中医药特征管理Info info)
        {
            bool rst = false;
            string sqlStr = "UPDATE  tb_老年人中医药特征管理 SET ROWSTATE = '1' WHERE ID=" + info.ID;
            int rows = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }

        internal static bool UpdateSys_DocNoPrgid(tb_老年人中医药特征管理Info info)
        {
            bool rst = false;
            string sqlStr = "update tb_老年人中医药特征管理 set 个人档案编号=@DocNO,所属机构=@Prgid,创建机构=@Prgid WHERE ID=" + info.ID;
            int rows = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sqlStr, new object[] { new SQLiteParameter("@DocNO", info.个人档案编号), new SQLiteParameter("@Prgid", info.所属机构) });
            //int  = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }
    }
}
