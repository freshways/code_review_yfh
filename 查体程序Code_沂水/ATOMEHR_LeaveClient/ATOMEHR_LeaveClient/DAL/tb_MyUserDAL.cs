﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SQLite;
using System.Data.Common;

namespace ATOMEHR_LeaveClient
{
    public class tb_MyUserDAL
    {
        //private DBHelper dbHelper = new DBHelper();
        /// <summary>
        /// 构造函数
        /// </summary>
        public tb_MyUserDAL()
        {
        }
        /// <summary>
        /// 添加记录
        /// </summary>
        /// <param name="model">MODEL.tb_MyUser实体类</param>
        /// <returns>新增记录的ID</returns>
        public int Add(ATOMEHR_LeaveClient.tb_MyUser model)
        {
            string sql = @"INSERT INTO tb_MyUser
				(isid,Account,NovellAccount,DomainName,用户编码,UserName,Address,Tel,Email,Password,LastLoginTime,LastLogoutTime,IsLocked,CreateTime,FlagAdmin,FlagOnline,LoginCounter,DataSets,所属机构,身份证) 
				VALUES(@isid,@Account,@NovellAccount,@DomainName,@用户编码,@UserName,@Address,@Tel,@Email,@Password,@LastLoginTime,@LastLogoutTime,@IsLocked,@CreateTime,@FlagAdmin,@FlagOnline,@LoginCounter,@DataSets,@所属机构,@身份证);";
            SQLiteParameter[] parameters = new SQLiteParameter[]{
                new SQLiteParameter("@isid", model.isid),
				new SQLiteParameter("@Account", model.Account),
				new SQLiteParameter("@NovellAccount", model.NovellAccount), 
                new SQLiteParameter("@DomainName", model.DomainName ),
                new SQLiteParameter("@用户编码", model.用户编码 ),
				new SQLiteParameter("@UserName", model.UserName ),
                new SQLiteParameter("@Address",   model.Address ),
				new SQLiteParameter("@Tel", model.Tel ),
				new SQLiteParameter("@Email",  model.Email ),
				new SQLiteParameter("@Password",   model.Password ),
				new SQLiteParameter("@LastLoginTime",   model.LastLoginTime ),
				new SQLiteParameter("@LastLogoutTime",  model.LastLogoutTime ),
				new SQLiteParameter("@IsLocked",  model.IsLocked ),
				new SQLiteParameter("@CreateTime",   model.CreateTime ),
				new SQLiteParameter("@FlagAdmin",  model.FlagAdmin ),
				new SQLiteParameter("@FlagOnline",  model.FlagOnline ),
				new SQLiteParameter("@LoginCounter",  model.LoginCounter ),
				new SQLiteParameter("@DataSets",  model.DataSets ),
				new SQLiteParameter("@所属机构",  model.所属机构 )
			};

            return DBManager.ExecuteUpdate(DBManager.SQLiteConn, sql, parameters);
        }

        /// <summary>
        /// 将DataRedar转换为List
        /// </summary>
        private List<ATOMEHR_LeaveClient.tb_MyUser> DataReaderToList(SQLiteDataReader dataReader)
        {
            List<ATOMEHR_LeaveClient.tb_MyUser> List = new List<ATOMEHR_LeaveClient.tb_MyUser>();
            ATOMEHR_LeaveClient.tb_MyUser model = null;
            while (dataReader.Read())
            {
                model = new ATOMEHR_LeaveClient.tb_MyUser();
                model.isid = dataReader.GetInt32(0);
                model.Account = dataReader.GetString(1);
                if (!dataReader.IsDBNull(2))
                    model.NovellAccount = dataReader.GetString(2);
                if (!dataReader.IsDBNull(3))
                    model.DomainName = dataReader.GetString(3);
                if (!dataReader.IsDBNull(4))
                    model.用户编码 = dataReader.GetString(4);
                model.UserName = dataReader.GetString(5);
                if (!dataReader.IsDBNull(6))
                    model.Address = dataReader.GetString(6);
                if (!dataReader.IsDBNull(7))
                    model.Tel = dataReader.GetString(7);
                if (!dataReader.IsDBNull(8))
                    model.Email = dataReader.GetString(8);
                if (!dataReader.IsDBNull(9))
                    model.Password = dataReader.GetString(9);
                if (!dataReader.IsDBNull(10))
                    model.LastLoginTime = dataReader.GetDateTime(10);
                if (!dataReader.IsDBNull(11))
                    model.LastLogoutTime = dataReader.GetDateTime(11);
                if (!dataReader.IsDBNull(12))
                    model.IsLocked = dataReader.GetInt16(12);
                if (!dataReader.IsDBNull(13))
                    model.CreateTime = dataReader.GetDateTime(13);
                if (!dataReader.IsDBNull(14))
                    model.FlagAdmin = dataReader.GetString(14);
                if (!dataReader.IsDBNull(15))
                    model.FlagOnline = dataReader.GetString(15);
                if (!dataReader.IsDBNull(16))
                    model.LoginCounter = dataReader.GetInt32(16);
                if (!dataReader.IsDBNull(17))
                    model.DataSets = dataReader.GetString(17);
                if (!dataReader.IsDBNull(18))
                    model.所属机构 = dataReader.GetString(18);
                List.Add(model);
            }
            return List;
        }
        /// <summary>
        /// 查询所有记录
        /// </summary>
        public List<ATOMEHR_LeaveClient.tb_MyUser> GetAll()
        {
            string sql = "SELECT * FROM tb_MyUser";
            SQLiteDataReader dataReader = DBManager.ExecuteQueryForSQLite(sql);
            List<ATOMEHR_LeaveClient.tb_MyUser> List = DataReaderToList(dataReader);
            dataReader.Close();
            return List;
        }

        /// <summary>
        /// 删除记录
        /// </summary>
        public static bool Delete(string sqlWhere)
        {
            bool rst = false;
            string sqlStr = "DELETE FROM tb_MyUser WHERE 1=1 ";
            if (!string.IsNullOrEmpty(sqlWhere))
            {
                sqlStr += sqlWhere;
            }
            int rows = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }

        /// <summary>
        /// 添加记录
        /// </summary>
        /// <param name="model">MODEL.tb_MyUser实体类</param>
        /// <returns>新增记录的ID</returns>
        public static void InsertList(List<tb_MyUser> list)
        {
            SQLiteConnection conn = DBManager.SQLiteConn;
            conn.Open();
            DbTransaction trans = conn.BeginTransaction();

            try
            {
                foreach (tb_MyUser model in list)
                {
                    string sql = @"INSERT INTO tb_MyUser
				(isid,Account,NovellAccount,DomainName,用户编码,UserName,Address,Tel,Email,Password,LastLoginTime,LastLogoutTime,IsLocked,CreateTime,FlagAdmin,FlagOnline,LoginCounter,DataSets,所属机构) 
				VALUES(@isid,@Account,@NovellAccount,@DomainName,@用户编码,@UserName,@Address,@Tel,@Email,@Password,@LastLoginTime,@LastLogoutTime,@IsLocked,@CreateTime,@FlagAdmin,@FlagOnline,@LoginCounter,@DataSets,@所属机构);";
                    SQLiteCommand cmd = new SQLiteCommand(sql, conn);
                    cmd.Parameters.AddRange(new SQLiteParameter[]{
                        new SQLiteParameter("@isid", model.isid),
				new SQLiteParameter("@Account", model.Account),
				new SQLiteParameter("@NovellAccount", model.NovellAccount), 
                new SQLiteParameter("@DomainName", model.DomainName ),
                new SQLiteParameter("@用户编码", model.用户编码 ),
				new SQLiteParameter("@UserName", model.UserName ),
                new SQLiteParameter("@Address",   model.Address ),
				new SQLiteParameter("@Tel", model.Tel ),
				new SQLiteParameter("@Email",  model.Email ),
				new SQLiteParameter("@Password",   model.Password ),
				new SQLiteParameter("@LastLoginTime",   model.LastLoginTime ),
				new SQLiteParameter("@LastLogoutTime",  model.LastLogoutTime ),
				new SQLiteParameter("@IsLocked",  model.IsLocked ),
				new SQLiteParameter("@CreateTime",   model.CreateTime ),
				new SQLiteParameter("@FlagAdmin",  model.FlagAdmin ),
				new SQLiteParameter("@FlagOnline",  model.FlagOnline ),
				new SQLiteParameter("@LoginCounter",  model.LoginCounter ),
				new SQLiteParameter("@DataSets",  model.DataSets ),
				new SQLiteParameter("@所属机构",  model.所属机构 )
			});
                    cmd.ExecuteNonQuery();
                }
                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback(); 
                conn.Close();
                conn.Dispose();
                throw ex;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
            
        }
        
    }
}
