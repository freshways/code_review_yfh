﻿using InTheHand.Net;
using InTheHand.Net.Sockets;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace ATOMEHR_LeaveClient
{
    public partial class frm蓝牙通讯 : Form
    {
        BluetoothClient btClient;
        BluetoothDeviceInfo[] devices;
        BluetoothEndPoint ePoint;
        BluetoothAddress address;
        SerialPort port;

        //InTheHand.Net.Ports.BluetoothSerialPort blueport;
        System.Threading.Thread t1;
        private string str;
        string portName = string.Empty;

        public frm蓝牙通讯()
        {
            InitializeComponent();
            PortList.Items.Clear();
            string[] Ports = SerialPort.GetPortNames();

            for (int i = 0; i < Ports.Length; i++)
            {
                string s = Ports[i].ToUpper();
                Regex reg = new Regex("[^COM\\d]", RegexOptions.IgnoreCase | RegexOptions.Multiline);
                s = reg.Replace(s, "");

                PortList.Items.Add(s);
            }
            if (Ports.Length > 1) PortList.SelectedIndex = 1;
        }
        SerialPort BluetoothConnection = new SerialPort();
        private void button连接_Click(object sender, EventArgs e)
        {
            if (!BluetoothConnection.IsOpen)
            {
                //Start
                //Status = "正在连接蓝牙设备";
                //BluetoothConnection = new SerialPort();
                //ConnectButton.Enabled = false;
                //BluetoothConnection.PortName = PortList.SelectedItem.ToString();
                //BluetoothConnection.Open();
                //BluetoothConnection.ReadTimeout = 10000;
                //BluetoothConnection.DataReceived += new SerialDataReceivedEventHandler(BlueToothDataReceived);
                //Status = "蓝牙连接成功";
            }
        }

        private void PortList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
