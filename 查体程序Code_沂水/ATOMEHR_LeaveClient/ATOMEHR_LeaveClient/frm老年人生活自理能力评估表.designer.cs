﻿namespace ATOMEHR_LeaveClient
{
    partial class frm老年人生活自理能力评估表
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm老年人生活自理能力评估表));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.result = new DevExpress.XtraEditors.LabelControl();
            this.result5 = new DevExpress.XtraEditors.LabelControl();
            this.result4 = new DevExpress.XtraEditors.LabelControl();
            this.result3 = new DevExpress.XtraEditors.LabelControl();
            this.result2 = new DevExpress.XtraEditors.LabelControl();
            this.result1 = new DevExpress.XtraEditors.LabelControl();
            this.txt最近修改人 = new DevExpress.XtraEditors.TextEdit();
            this.txt最近更新时间 = new DevExpress.XtraEditors.TextEdit();
            this.txt创建机构 = new DevExpress.XtraEditors.TextEdit();
            this.txt创建人 = new DevExpress.XtraEditors.TextEdit();
            this.txt当前所属机构 = new DevExpress.XtraEditors.TextEdit();
            this.txt创建时间 = new DevExpress.XtraEditors.TextEdit();
            this.txt随访医生签名 = new DevExpress.XtraEditors.TextEdit();
            this.txt录入医生 = new DevExpress.XtraEditors.TextEdit();
            this.txt下次随访目标 = new DevExpress.XtraEditors.MemoEdit();
            this.chk54 = new DevExpress.XtraEditors.CheckEdit();
            this.chk53 = new DevExpress.XtraEditors.CheckEdit();
            this.chk52 = new DevExpress.XtraEditors.CheckEdit();
            this.chk51 = new DevExpress.XtraEditors.CheckEdit();
            this.chk44 = new DevExpress.XtraEditors.CheckEdit();
            this.chk42 = new DevExpress.XtraEditors.CheckEdit();
            this.chk43 = new DevExpress.XtraEditors.CheckEdit();
            this.chk41 = new DevExpress.XtraEditors.CheckEdit();
            this.chk34 = new DevExpress.XtraEditors.CheckEdit();
            this.chk33 = new DevExpress.XtraEditors.CheckEdit();
            this.chk31 = new DevExpress.XtraEditors.CheckEdit();
            this.chk24 = new DevExpress.XtraEditors.CheckEdit();
            this.chk23 = new DevExpress.XtraEditors.CheckEdit();
            this.chk22 = new DevExpress.XtraEditors.CheckEdit();
            this.chk21 = new DevExpress.XtraEditors.CheckEdit();
            this.chk14 = new DevExpress.XtraEditors.CheckEdit();
            this.chk13 = new DevExpress.XtraEditors.CheckEdit();
            this.chk11 = new DevExpress.XtraEditors.CheckEdit();
            this.txt居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.txt出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt性别 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.dte随访日期 = new DevExpress.XtraEditors.DateEdit();
            this.dte下次随访日期 = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleLabelItem2 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleLabelItem3 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleLabelItem4 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleLabelItem7 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleLabelItem6 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleLabelItem8 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleLabelItem9 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleLabelItem5 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleLabelItem10 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem18 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem24 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem23 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem21 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem25 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem20 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem19 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem22 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem27 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem28 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem31 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem35 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem32 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem30 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem33 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem34 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem36 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem38 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl随访日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl下次随访日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl录入医生 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl随访医生签名 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近更新时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt当前所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访医生签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访目标.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk54.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk53.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk52.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk51.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk44.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk43.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk41.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk34.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk33.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl随访日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl下次随访日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl录入医生)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl随访医生签名)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1075, 40);
            this.panelControl1.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn保存);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1071, 36);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn保存
            // 
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(4, 4);
            this.btn保存.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(100, 29);
            this.btn保存.TabIndex = 0;
            this.btn保存.Text = "保存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.result);
            this.layoutControl1.Controls.Add(this.result5);
            this.layoutControl1.Controls.Add(this.result4);
            this.layoutControl1.Controls.Add(this.result3);
            this.layoutControl1.Controls.Add(this.result2);
            this.layoutControl1.Controls.Add(this.result1);
            this.layoutControl1.Controls.Add(this.txt最近修改人);
            this.layoutControl1.Controls.Add(this.txt最近更新时间);
            this.layoutControl1.Controls.Add(this.txt创建机构);
            this.layoutControl1.Controls.Add(this.txt创建人);
            this.layoutControl1.Controls.Add(this.txt当前所属机构);
            this.layoutControl1.Controls.Add(this.txt创建时间);
            this.layoutControl1.Controls.Add(this.txt随访医生签名);
            this.layoutControl1.Controls.Add(this.txt录入医生);
            this.layoutControl1.Controls.Add(this.txt下次随访目标);
            this.layoutControl1.Controls.Add(this.chk54);
            this.layoutControl1.Controls.Add(this.chk53);
            this.layoutControl1.Controls.Add(this.chk52);
            this.layoutControl1.Controls.Add(this.chk51);
            this.layoutControl1.Controls.Add(this.chk44);
            this.layoutControl1.Controls.Add(this.chk42);
            this.layoutControl1.Controls.Add(this.chk43);
            this.layoutControl1.Controls.Add(this.chk41);
            this.layoutControl1.Controls.Add(this.chk34);
            this.layoutControl1.Controls.Add(this.chk33);
            this.layoutControl1.Controls.Add(this.chk31);
            this.layoutControl1.Controls.Add(this.chk24);
            this.layoutControl1.Controls.Add(this.chk23);
            this.layoutControl1.Controls.Add(this.chk22);
            this.layoutControl1.Controls.Add(this.chk21);
            this.layoutControl1.Controls.Add(this.chk14);
            this.layoutControl1.Controls.Add(this.chk13);
            this.layoutControl1.Controls.Add(this.chk11);
            this.layoutControl1.Controls.Add(this.txt居住地址);
            this.layoutControl1.Controls.Add(this.txt出生日期);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt性别);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.dte随访日期);
            this.layoutControl1.Controls.Add(this.dte下次随访日期);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 40);
            this.layoutControl1.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(566, 87, 250, 201);
            this.layoutControl1.OptionsFocus.AllowFocusGroups = false;
            this.layoutControl1.OptionsFocus.AllowFocusReadonlyEditors = false;
            this.layoutControl1.OptionsFocus.AllowFocusTabbedGroups = false;
            this.layoutControl1.OptionsFocus.EnableAutoTabOrder = false;
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.OptionsView.EnableIndentsInGroupsWithoutBorders = true;
            this.layoutControl1.OptionsView.EnableTransparentBackColor = false;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1075, 550);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // result
            // 
            this.result.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.result.Appearance.ForeColor = System.Drawing.Color.Red;
            this.result.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.result.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.result.Location = new System.Drawing.Point(655, 533);
            this.result.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(111, 21);
            this.result.StyleController = this.layoutControl1;
            this.result.TabIndex = 46;
            // 
            // result5
            // 
            this.result5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.result5.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.result5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.result5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.result5.Location = new System.Drawing.Point(655, 448);
            this.result5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.result5.Name = "result5";
            this.result5.Size = new System.Drawing.Size(111, 81);
            this.result5.StyleController = this.layoutControl1;
            this.result5.TabIndex = 45;
            this.result5.TextChanged += new System.EventHandler(this.result_TextChanged);
            // 
            // result4
            // 
            this.result4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.result4.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.result4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.result4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.result4.Location = new System.Drawing.Point(655, 373);
            this.result4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.result4.Name = "result4";
            this.result4.Size = new System.Drawing.Size(111, 71);
            this.result4.StyleController = this.layoutControl1;
            this.result4.TabIndex = 44;
            this.result4.TextChanged += new System.EventHandler(this.result_TextChanged);
            // 
            // result3
            // 
            this.result3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.result3.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.result3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.result3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.result3.Location = new System.Drawing.Point(655, 288);
            this.result3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.result3.Name = "result3";
            this.result3.Size = new System.Drawing.Size(111, 81);
            this.result3.StyleController = this.layoutControl1;
            this.result3.TabIndex = 43;
            this.result3.TextChanged += new System.EventHandler(this.result_TextChanged);
            // 
            // result2
            // 
            this.result2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.result2.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.result2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.result2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.result2.Location = new System.Drawing.Point(655, 203);
            this.result2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.result2.Name = "result2";
            this.result2.Size = new System.Drawing.Size(111, 81);
            this.result2.StyleController = this.layoutControl1;
            this.result2.TabIndex = 42;
            this.result2.TextChanged += new System.EventHandler(this.result_TextChanged);
            // 
            // result1
            // 
            this.result1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.result1.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.result1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.result1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.result1.Location = new System.Drawing.Point(655, 113);
            this.result1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.result1.Name = "result1";
            this.result1.Size = new System.Drawing.Size(111, 86);
            this.result1.StyleController = this.layoutControl1;
            this.result1.TabIndex = 41;
            this.result1.TextChanged += new System.EventHandler(this.result_TextChanged);
            // 
            // txt最近修改人
            // 
            this.txt最近修改人.Location = new System.Drawing.Point(755, 724);
            this.txt最近修改人.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt最近修改人.Name = "txt最近修改人";
            this.txt最近修改人.Properties.ReadOnly = true;
            this.txt最近修改人.Size = new System.Drawing.Size(294, 24);
            this.txt最近修改人.StyleController = this.layoutControl1;
            this.txt最近修改人.TabIndex = 37;
            // 
            // txt最近更新时间
            // 
            this.txt最近更新时间.Location = new System.Drawing.Point(413, 696);
            this.txt最近更新时间.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt最近更新时间.Name = "txt最近更新时间";
            this.txt最近更新时间.Properties.ReadOnly = true;
            this.txt最近更新时间.Size = new System.Drawing.Size(253, 24);
            this.txt最近更新时间.StyleController = this.layoutControl1;
            this.txt最近更新时间.TabIndex = 33;
            // 
            // txt创建机构
            // 
            this.txt创建机构.Location = new System.Drawing.Point(90, 724);
            this.txt创建机构.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt创建机构.Name = "txt创建机构";
            this.txt创建机构.Properties.ReadOnly = true;
            this.txt创建机构.Size = new System.Drawing.Size(234, 24);
            this.txt创建机构.StyleController = this.layoutControl1;
            this.txt创建机构.TabIndex = 35;
            // 
            // txt创建人
            // 
            this.txt创建人.Location = new System.Drawing.Point(413, 724);
            this.txt创建人.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt创建人.Name = "txt创建人";
            this.txt创建人.Properties.ReadOnly = true;
            this.txt创建人.Size = new System.Drawing.Size(253, 24);
            this.txt创建人.StyleController = this.layoutControl1;
            this.txt创建人.TabIndex = 36;
            // 
            // txt当前所属机构
            // 
            this.txt当前所属机构.Location = new System.Drawing.Point(755, 696);
            this.txt当前所属机构.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt当前所属机构.Name = "txt当前所属机构";
            this.txt当前所属机构.Properties.ReadOnly = true;
            this.txt当前所属机构.Size = new System.Drawing.Size(294, 24);
            this.txt当前所属机构.StyleController = this.layoutControl1;
            this.txt当前所属机构.TabIndex = 34;
            // 
            // txt创建时间
            // 
            this.txt创建时间.Location = new System.Drawing.Point(90, 696);
            this.txt创建时间.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt创建时间.Name = "txt创建时间";
            this.txt创建时间.Properties.ReadOnly = true;
            this.txt创建时间.Size = new System.Drawing.Size(234, 24);
            this.txt创建时间.StyleController = this.layoutControl1;
            this.txt创建时间.TabIndex = 32;
            // 
            // txt随访医生签名
            // 
            this.txt随访医生签名.Location = new System.Drawing.Point(360, 642);
            this.txt随访医生签名.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt随访医生签名.Name = "txt随访医生签名";
            this.txt随访医生签名.Size = new System.Drawing.Size(141, 24);
            this.txt随访医生签名.StyleController = this.layoutControl1;
            this.txt随访医生签名.TabIndex = 29;
            // 
            // txt录入医生
            // 
            this.txt录入医生.Location = new System.Drawing.Point(360, 666);
            this.txt录入医生.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt录入医生.Name = "txt录入医生";
            this.txt录入医生.Size = new System.Drawing.Size(141, 24);
            this.txt录入医生.StyleController = this.layoutControl1;
            this.txt录入医生.TabIndex = 31;
            // 
            // txt下次随访目标
            // 
            this.txt下次随访目标.Location = new System.Drawing.Point(110, 588);
            this.txt下次随访目标.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt下次随访目标.Name = "txt下次随访目标";
            this.txt下次随访目标.Size = new System.Drawing.Size(391, 50);
            this.txt下次随访目标.StyleController = this.layoutControl1;
            this.txt下次随访目标.TabIndex = 27;
            this.txt下次随访目标.UseOptimizedRendering = true;
            // 
            // chk54
            // 
            this.chk54.Location = new System.Drawing.Point(515, 448);
            this.chk54.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chk54.Name = "chk54";
            this.chk54.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.chk54.Properties.Appearance.Options.UseBackColor = true;
            this.chk54.Properties.Appearance.Options.UseTextOptions = true;
            this.chk54.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk54.Properties.AutoHeight = false;
            this.chk54.Properties.Caption = "卧床不起， 活动完全需要帮助";
            this.chk54.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk54.Properties.RadioGroupIndex = 5;
            this.chk54.Size = new System.Drawing.Size(136, 56);
            this.chk54.StyleController = this.layoutControl1;
            this.chk54.TabIndex = 26;
            this.chk54.TabStop = false;
            this.chk54.Tag = "10";
            this.chk54.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk53
            // 
            this.chk53.Location = new System.Drawing.Point(375, 448);
            this.chk53.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chk53.Name = "chk53";
            this.chk53.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.chk53.Properties.Appearance.Options.UseBackColor = true;
            this.chk53.Properties.Appearance.Options.UseTextOptions = true;
            this.chk53.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk53.Properties.AutoHeight = false;
            this.chk53.Properties.Caption = "借助较大的外力才能完成站立、行走，不能上下楼梯 ";
            this.chk53.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk53.Properties.RadioGroupIndex = 5;
            this.chk53.Size = new System.Drawing.Size(136, 56);
            this.chk53.StyleController = this.layoutControl1;
            this.chk53.TabIndex = 25;
            this.chk53.TabStop = false;
            this.chk53.Tag = "5";
            this.chk53.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk52
            // 
            this.chk52.Location = new System.Drawing.Point(235, 448);
            this.chk52.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chk52.Name = "chk52";
            this.chk52.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.chk52.Properties.Appearance.Options.UseBackColor = true;
            this.chk52.Properties.Appearance.Options.UseTextOptions = true;
            this.chk52.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk52.Properties.AutoHeight = false;
            this.chk52.Properties.Caption = "借助较小的外力或辅助装置能完成站立、行走、上下楼梯等 ";
            this.chk52.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk52.Properties.RadioGroupIndex = 5;
            this.chk52.Size = new System.Drawing.Size(136, 56);
            this.chk52.StyleController = this.layoutControl1;
            this.chk52.TabIndex = 24;
            this.chk52.TabStop = false;
            this.chk52.Tag = "1";
            this.chk52.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk51
            // 
            this.chk51.Location = new System.Drawing.Point(105, 448);
            this.chk51.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chk51.Name = "chk51";
            this.chk51.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.chk51.Properties.Appearance.Options.UseBackColor = true;
            this.chk51.Properties.AutoHeight = false;
            this.chk51.Properties.Caption = "独立完成所有活动";
            this.chk51.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk51.Properties.RadioGroupIndex = 5;
            this.chk51.Size = new System.Drawing.Size(126, 56);
            this.chk51.StyleController = this.layoutControl1;
            this.chk51.TabIndex = 23;
            this.chk51.TabStop = false;
            this.chk51.Tag = "0";
            this.chk51.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk44
            // 
            this.chk44.Location = new System.Drawing.Point(515, 373);
            this.chk44.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chk44.Name = "chk44";
            this.chk44.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.chk44.Properties.Appearance.Options.UseBackColor = true;
            this.chk44.Properties.Appearance.Options.UseTextOptions = true;
            this.chk44.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk44.Properties.AutoHeight = false;
            this.chk44.Properties.Caption = "完全失禁， 完全需要帮助";
            this.chk44.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk44.Properties.RadioGroupIndex = 4;
            this.chk44.Size = new System.Drawing.Size(136, 46);
            this.chk44.StyleController = this.layoutControl1;
            this.chk44.TabIndex = 22;
            this.chk44.TabStop = false;
            this.chk44.Tag = "10";
            this.chk44.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk42
            // 
            this.chk42.Location = new System.Drawing.Point(235, 373);
            this.chk42.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chk42.Name = "chk42";
            this.chk42.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.chk42.Properties.Appearance.Options.UseBackColor = true;
            this.chk42.Properties.Appearance.Options.UseTextOptions = true;
            this.chk42.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk42.Properties.AutoHeight = false;
            this.chk42.Properties.Caption = "偶尔失禁，但基本上能如厕或使用便具 ";
            this.chk42.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk42.Properties.RadioGroupIndex = 4;
            this.chk42.Size = new System.Drawing.Size(136, 46);
            this.chk42.StyleController = this.layoutControl1;
            this.chk42.TabIndex = 20;
            this.chk42.TabStop = false;
            this.chk42.Tag = "1";
            this.chk42.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk43
            // 
            this.chk43.Location = new System.Drawing.Point(375, 373);
            this.chk43.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chk43.Name = "chk43";
            this.chk43.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.chk43.Properties.Appearance.Options.UseBackColor = true;
            this.chk43.Properties.Appearance.Options.UseTextOptions = true;
            this.chk43.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk43.Properties.AutoHeight = false;
            this.chk43.Properties.Caption = "经常失禁，在很多提示 和协助下尚能如厕或使用便具";
            this.chk43.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk43.Properties.RadioGroupIndex = 4;
            this.chk43.Size = new System.Drawing.Size(136, 46);
            this.chk43.StyleController = this.layoutControl1;
            this.chk43.TabIndex = 21;
            this.chk43.TabStop = false;
            this.chk43.Tag = "5";
            this.chk43.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk41
            // 
            this.chk41.Location = new System.Drawing.Point(105, 373);
            this.chk41.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chk41.Name = "chk41";
            this.chk41.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.chk41.Properties.Appearance.Options.UseBackColor = true;
            this.chk41.Properties.Appearance.Options.UseTextOptions = true;
            this.chk41.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk41.Properties.AutoHeight = false;
            this.chk41.Properties.Caption = "不需协助，可自控";
            this.chk41.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk41.Properties.RadioGroupIndex = 4;
            this.chk41.Size = new System.Drawing.Size(126, 46);
            this.chk41.StyleController = this.layoutControl1;
            this.chk41.TabIndex = 19;
            this.chk41.TabStop = false;
            this.chk41.Tag = "0";
            this.chk41.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk34
            // 
            this.chk34.Location = new System.Drawing.Point(515, 288);
            this.chk34.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chk34.Name = "chk34";
            this.chk34.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.chk34.Properties.Appearance.Options.UseBackColor = true;
            this.chk34.Properties.AutoHeight = false;
            this.chk34.Properties.Caption = "完全需要帮助";
            this.chk34.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk34.Properties.RadioGroupIndex = 3;
            this.chk34.Size = new System.Drawing.Size(136, 56);
            this.chk34.StyleController = this.layoutControl1;
            this.chk34.TabIndex = 18;
            this.chk34.TabStop = false;
            this.chk34.Tag = "5";
            this.chk34.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk33
            // 
            this.chk33.Location = new System.Drawing.Point(375, 288);
            this.chk33.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chk33.Name = "chk33";
            this.chk33.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.chk33.Properties.Appearance.Options.UseBackColor = true;
            this.chk33.Properties.Appearance.Options.UseTextOptions = true;
            this.chk33.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk33.Properties.AutoHeight = false;
            this.chk33.Properties.Caption = "\t需要协助， 在适当的时间内完成部分穿衣 ";
            this.chk33.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk33.Properties.RadioGroupIndex = 3;
            this.chk33.Size = new System.Drawing.Size(136, 56);
            this.chk33.StyleController = this.layoutControl1;
            this.chk33.TabIndex = 17;
            this.chk33.TabStop = false;
            this.chk33.Tag = "3";
            this.chk33.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk31
            // 
            this.chk31.Location = new System.Drawing.Point(105, 288);
            this.chk31.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chk31.Name = "chk31";
            this.chk31.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.chk31.Properties.Appearance.Options.UseBackColor = true;
            this.chk31.Properties.AutoHeight = false;
            this.chk31.Properties.Caption = "独立完成";
            this.chk31.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk31.Properties.RadioGroupIndex = 3;
            this.chk31.Size = new System.Drawing.Size(126, 56);
            this.chk31.StyleController = this.layoutControl1;
            this.chk31.TabIndex = 16;
            this.chk31.TabStop = false;
            this.chk31.Tag = "0";
            this.chk31.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk24
            // 
            this.chk24.Location = new System.Drawing.Point(515, 203);
            this.chk24.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chk24.Name = "chk24";
            this.chk24.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.chk24.Properties.Appearance.Options.UseBackColor = true;
            this.chk24.Properties.Appearance.Options.UseTextOptions = true;
            this.chk24.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.chk24.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk24.Properties.AutoHeight = false;
            this.chk24.Properties.Caption = "完全需要帮助";
            this.chk24.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk24.Properties.RadioGroupIndex = 2;
            this.chk24.Size = new System.Drawing.Size(136, 56);
            this.chk24.StyleController = this.layoutControl1;
            this.chk24.TabIndex = 15;
            this.chk24.TabStop = false;
            this.chk24.Tag = "7";
            this.chk24.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk23
            // 
            this.chk23.Location = new System.Drawing.Point(375, 203);
            this.chk23.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chk23.Name = "chk23";
            this.chk23.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.chk23.Properties.Appearance.Options.UseBackColor = true;
            this.chk23.Properties.Appearance.Options.UseTextOptions = true;
            this.chk23.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.chk23.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk23.Properties.AutoHeight = false;
            this.chk23.Properties.Caption = "在协助下和适当的时间内，能完成部分梳洗活动";
            this.chk23.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk23.Properties.RadioGroupIndex = 2;
            this.chk23.Size = new System.Drawing.Size(136, 56);
            this.chk23.StyleController = this.layoutControl1;
            this.chk23.TabIndex = 14;
            this.chk23.TabStop = false;
            this.chk23.Tag = "3";
            this.chk23.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk22
            // 
            this.chk22.Location = new System.Drawing.Point(235, 203);
            this.chk22.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chk22.Name = "chk22";
            this.chk22.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.chk22.Properties.Appearance.Options.UseBackColor = true;
            this.chk22.Properties.Appearance.Options.UseTextOptions = true;
            this.chk22.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.chk22.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk22.Properties.AutoHeight = false;
            this.chk22.Properties.Caption = "\t能独立地洗头、梳头、洗脸、刷牙、剃须等；洗澡需要协助";
            this.chk22.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk22.Properties.RadioGroupIndex = 2;
            this.chk22.Size = new System.Drawing.Size(136, 56);
            this.chk22.StyleController = this.layoutControl1;
            this.chk22.TabIndex = 13;
            this.chk22.TabStop = false;
            this.chk22.Tag = "1";
            this.chk22.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk21
            // 
            this.chk21.Location = new System.Drawing.Point(105, 203);
            this.chk21.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chk21.Name = "chk21";
            this.chk21.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.chk21.Properties.Appearance.Options.UseBackColor = true;
            this.chk21.Properties.Appearance.Options.UseTextOptions = true;
            this.chk21.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.chk21.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk21.Properties.AutoHeight = false;
            this.chk21.Properties.Caption = "独立完成";
            this.chk21.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk21.Properties.RadioGroupIndex = 2;
            this.chk21.Size = new System.Drawing.Size(126, 56);
            this.chk21.StyleController = this.layoutControl1;
            this.chk21.TabIndex = 12;
            this.chk21.TabStop = false;
            this.chk21.Tag = "0";
            this.chk21.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk14
            // 
            this.chk14.Location = new System.Drawing.Point(515, 113);
            this.chk14.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chk14.Name = "chk14";
            this.chk14.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.chk14.Properties.Appearance.Options.UseBackColor = true;
            this.chk14.Properties.AutoHeight = false;
            this.chk14.Properties.Caption = "完全需要帮助";
            this.chk14.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk14.Properties.RadioGroupIndex = 1;
            this.chk14.Size = new System.Drawing.Size(136, 61);
            this.chk14.StyleController = this.layoutControl1;
            this.chk14.TabIndex = 11;
            this.chk14.TabStop = false;
            this.chk14.Tag = "5";
            this.chk14.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk13
            // 
            this.chk13.Location = new System.Drawing.Point(375, 113);
            this.chk13.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chk13.Name = "chk13";
            this.chk13.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.chk13.Properties.Appearance.Options.UseBackColor = true;
            this.chk13.Properties.Appearance.Options.UseTextOptions = true;
            this.chk13.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.chk13.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk13.Properties.AutoHeight = false;
            this.chk13.Properties.Caption = "需要协助，如切碎、搅拌食物等";
            this.chk13.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk13.Properties.RadioGroupIndex = 1;
            this.chk13.Size = new System.Drawing.Size(136, 61);
            this.chk13.StyleController = this.layoutControl1;
            this.chk13.TabIndex = 10;
            this.chk13.TabStop = false;
            this.chk13.Tag = "3";
            this.chk13.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk11
            // 
            this.chk11.Location = new System.Drawing.Point(105, 113);
            this.chk11.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chk11.Name = "chk11";
            this.chk11.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.chk11.Properties.Appearance.Options.UseBackColor = true;
            this.chk11.Properties.Appearance.Options.UseTextOptions = true;
            this.chk11.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk11.Properties.AutoHeight = false;
            this.chk11.Properties.Caption = "独立完成";
            this.chk11.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk11.Properties.RadioGroupIndex = 1;
            this.chk11.Size = new System.Drawing.Size(126, 61);
            this.chk11.StyleController = this.layoutControl1;
            this.chk11.TabIndex = 9;
            this.chk11.TabStop = false;
            this.chk11.Tag = "0";
            this.chk11.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // txt居住地址
            // 
            this.txt居住地址.Location = new System.Drawing.Point(439, 29);
            this.txt居住地址.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Properties.ReadOnly = true;
            this.txt居住地址.Size = new System.Drawing.Size(610, 24);
            this.txt居住地址.StyleController = this.layoutControl1;
            this.txt居住地址.TabIndex = 8;
            // 
            // txt出生日期
            // 
            this.txt出生日期.Location = new System.Drawing.Point(110, 29);
            this.txt出生日期.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Properties.ReadOnly = true;
            this.txt出生日期.Size = new System.Drawing.Size(220, 24);
            this.txt出生日期.StyleController = this.layoutControl1;
            this.txt出生日期.TabIndex = 6;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(716, 5);
            this.txt身份证号.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Properties.ReadOnly = true;
            this.txt身份证号.Size = new System.Drawing.Size(333, 24);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 5;
            // 
            // txt性别
            // 
            this.txt性别.Location = new System.Drawing.Point(439, 5);
            this.txt性别.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt性别.Name = "txt性别";
            this.txt性别.Properties.ReadOnly = true;
            this.txt性别.Size = new System.Drawing.Size(168, 24);
            this.txt性别.StyleController = this.layoutControl1;
            this.txt性别.TabIndex = 6;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(110, 5);
            this.txt姓名.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Properties.ReadOnly = true;
            this.txt姓名.Size = new System.Drawing.Size(220, 24);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 4;
            // 
            // dte随访日期
            // 
            this.dte随访日期.EditValue = null;
            this.dte随访日期.Location = new System.Drawing.Point(110, 642);
            this.dte随访日期.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dte随访日期.Name = "dte随访日期";
            this.dte随访日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte随访日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte随访日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte随访日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte随访日期.Size = new System.Drawing.Size(141, 24);
            this.dte随访日期.StyleController = this.layoutControl1;
            this.dte随访日期.TabIndex = 28;
            // 
            // dte下次随访日期
            // 
            this.dte下次随访日期.EditValue = null;
            this.dte下次随访日期.Location = new System.Drawing.Point(110, 666);
            this.dte下次随访日期.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dte下次随访日期.Name = "dte下次随访日期";
            this.dte下次随访日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte下次随访日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte下次随访日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte下次随访日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte下次随访日期.Size = new System.Drawing.Size(141, 24);
            this.dte下次随访日期.StyleController = this.layoutControl1;
            this.dte下次随访日期.TabIndex = 30;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.layoutControlGroup5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1054, 753);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleLabelItem1,
            this.simpleLabelItem2,
            this.simpleLabelItem3,
            this.simpleLabelItem4,
            this.simpleLabelItem7,
            this.simpleLabelItem6,
            this.simpleLabelItem8,
            this.simpleLabelItem9,
            this.simpleLabelItem5,
            this.layoutControlItem8,
            this.emptySpaceItem1,
            this.layoutControlItem9,
            this.layoutControlItem11,
            this.simpleLabelItem10,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.emptySpaceItem3,
            this.emptySpaceItem6,
            this.emptySpaceItem8,
            this.layoutControlItem10,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.emptySpaceItem14,
            this.emptySpaceItem9,
            this.emptySpaceItem10,
            this.emptySpaceItem11,
            this.emptySpaceItem12,
            this.emptySpaceItem15,
            this.layoutControlItem15,
            this.emptySpaceItem18,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.emptySpaceItem16,
            this.emptySpaceItem24,
            this.emptySpaceItem23,
            this.emptySpaceItem21,
            this.emptySpaceItem13,
            this.emptySpaceItem25,
            this.layoutControlItem18,
            this.layoutControlItem20,
            this.layoutControlItem19,
            this.layoutControlItem21,
            this.emptySpaceItem20,
            this.emptySpaceItem19,
            this.emptySpaceItem22,
            this.emptySpaceItem27,
            this.emptySpaceItem28,
            this.emptySpaceItem31,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem24,
            this.layoutControlItem25,
            this.emptySpaceItem35,
            this.emptySpaceItem32,
            this.emptySpaceItem30,
            this.emptySpaceItem33,
            this.emptySpaceItem34,
            this.emptySpaceItem36,
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.layoutControlItem35,
            this.layoutControlItem36,
            this.layoutControlItem37,
            this.layoutControlItem38});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 54);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(1054, 505);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.simpleLabelItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseBackColor = true;
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem1.CustomizationFormText = "老年人生活自理能力评估 ";
            this.simpleLabelItem1.Location = new System.Drawing.Point(0, 0);
            this.simpleLabelItem1.MaxSize = new System.Drawing.Size(400, 18);
            this.simpleLabelItem1.MinSize = new System.Drawing.Size(299, 18);
            this.simpleLabelItem1.Name = "simpleLabelItem1";
            this.simpleLabelItem1.Size = new System.Drawing.Size(1048, 18);
            this.simpleLabelItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem1.Text = "老年人生活自理能力评估 ";
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(180, 18);
            // 
            // simpleLabelItem2
            // 
            this.simpleLabelItem2.AllowHotTrack = false;
            this.simpleLabelItem2.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.simpleLabelItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.simpleLabelItem2.AppearanceItemCaption.Options.UseBackColor = true;
            this.simpleLabelItem2.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem2.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleLabelItem2.CustomizationFormText = "评估事项、内容与评分 ";
            this.simpleLabelItem2.Location = new System.Drawing.Point(0, 18);
            this.simpleLabelItem2.MaxSize = new System.Drawing.Size(100, 18);
            this.simpleLabelItem2.MinSize = new System.Drawing.Size(100, 18);
            this.simpleLabelItem2.Name = "simpleLabelItem2";
            this.simpleLabelItem2.Size = new System.Drawing.Size(100, 36);
            this.simpleLabelItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem2.Text = "评估事项、内容与评分 ";
            this.simpleLabelItem2.TextSize = new System.Drawing.Size(180, 18);
            // 
            // simpleLabelItem3
            // 
            this.simpleLabelItem3.AllowHotTrack = false;
            this.simpleLabelItem3.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.simpleLabelItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.simpleLabelItem3.AppearanceItemCaption.Options.UseBackColor = true;
            this.simpleLabelItem3.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem3.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleLabelItem3.CustomizationFormText = "程度等级 ";
            this.simpleLabelItem3.Location = new System.Drawing.Point(100, 18);
            this.simpleLabelItem3.MaxSize = new System.Drawing.Size(300, 18);
            this.simpleLabelItem3.MinSize = new System.Drawing.Size(299, 18);
            this.simpleLabelItem3.Name = "simpleLabelItem3";
            this.simpleLabelItem3.Size = new System.Drawing.Size(948, 18);
            this.simpleLabelItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem3.Text = "程度等级 ";
            this.simpleLabelItem3.TextSize = new System.Drawing.Size(180, 18);
            // 
            // simpleLabelItem4
            // 
            this.simpleLabelItem4.AllowHotTrack = false;
            this.simpleLabelItem4.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.simpleLabelItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.simpleLabelItem4.AppearanceItemCaption.Options.UseBackColor = true;
            this.simpleLabelItem4.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem4.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleLabelItem4.CustomizationFormText = "可自理 ";
            this.simpleLabelItem4.Location = new System.Drawing.Point(100, 36);
            this.simpleLabelItem4.MaxSize = new System.Drawing.Size(130, 18);
            this.simpleLabelItem4.MinSize = new System.Drawing.Size(130, 18);
            this.simpleLabelItem4.Name = "simpleLabelItem4";
            this.simpleLabelItem4.Size = new System.Drawing.Size(130, 18);
            this.simpleLabelItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem4.Text = "可自理 ";
            this.simpleLabelItem4.TextSize = new System.Drawing.Size(180, 18);
            // 
            // simpleLabelItem7
            // 
            this.simpleLabelItem7.AllowHotTrack = false;
            this.simpleLabelItem7.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.simpleLabelItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.simpleLabelItem7.AppearanceItemCaption.Options.UseBackColor = true;
            this.simpleLabelItem7.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem7.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleLabelItem7.CustomizationFormText = "轻度依赖 ";
            this.simpleLabelItem7.Location = new System.Drawing.Point(230, 36);
            this.simpleLabelItem7.MaxSize = new System.Drawing.Size(140, 18);
            this.simpleLabelItem7.MinSize = new System.Drawing.Size(140, 18);
            this.simpleLabelItem7.Name = "simpleLabelItem7";
            this.simpleLabelItem7.Size = new System.Drawing.Size(140, 18);
            this.simpleLabelItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem7.Text = "轻度依赖 ";
            this.simpleLabelItem7.TextSize = new System.Drawing.Size(180, 18);
            // 
            // simpleLabelItem6
            // 
            this.simpleLabelItem6.AllowHotTrack = false;
            this.simpleLabelItem6.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.simpleLabelItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.simpleLabelItem6.AppearanceItemCaption.Options.UseBackColor = true;
            this.simpleLabelItem6.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem6.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleLabelItem6.CustomizationFormText = "中度依赖";
            this.simpleLabelItem6.Location = new System.Drawing.Point(370, 36);
            this.simpleLabelItem6.MaxSize = new System.Drawing.Size(140, 18);
            this.simpleLabelItem6.MinSize = new System.Drawing.Size(140, 18);
            this.simpleLabelItem6.Name = "simpleLabelItem6";
            this.simpleLabelItem6.Size = new System.Drawing.Size(140, 18);
            this.simpleLabelItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem6.Text = "中度依赖";
            this.simpleLabelItem6.TextSize = new System.Drawing.Size(180, 18);
            // 
            // simpleLabelItem8
            // 
            this.simpleLabelItem8.AllowHotTrack = false;
            this.simpleLabelItem8.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.simpleLabelItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.simpleLabelItem8.AppearanceItemCaption.Options.UseBackColor = true;
            this.simpleLabelItem8.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem8.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleLabelItem8.CustomizationFormText = "不能自理 ";
            this.simpleLabelItem8.Location = new System.Drawing.Point(510, 36);
            this.simpleLabelItem8.MaxSize = new System.Drawing.Size(140, 18);
            this.simpleLabelItem8.MinSize = new System.Drawing.Size(140, 18);
            this.simpleLabelItem8.Name = "simpleLabelItem8";
            this.simpleLabelItem8.Size = new System.Drawing.Size(140, 18);
            this.simpleLabelItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem8.Text = "不能自理 ";
            this.simpleLabelItem8.TextSize = new System.Drawing.Size(180, 18);
            // 
            // simpleLabelItem9
            // 
            this.simpleLabelItem9.AllowHotTrack = false;
            this.simpleLabelItem9.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.simpleLabelItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.simpleLabelItem9.AppearanceItemCaption.Options.UseBackColor = true;
            this.simpleLabelItem9.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem9.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleLabelItem9.CustomizationFormText = "判断评分";
            this.simpleLabelItem9.Location = new System.Drawing.Point(650, 36);
            this.simpleLabelItem9.MaxSize = new System.Drawing.Size(90, 18);
            this.simpleLabelItem9.MinSize = new System.Drawing.Size(90, 18);
            this.simpleLabelItem9.Name = "simpleLabelItem9";
            this.simpleLabelItem9.Size = new System.Drawing.Size(398, 18);
            this.simpleLabelItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem9.Text = "判断评分";
            this.simpleLabelItem9.TextSize = new System.Drawing.Size(180, 18);
            // 
            // simpleLabelItem5
            // 
            this.simpleLabelItem5.AllowHotTrack = false;
            this.simpleLabelItem5.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.simpleLabelItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.simpleLabelItem5.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.simpleLabelItem5.AppearanceItemCaption.Options.UseBackColor = true;
            this.simpleLabelItem5.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem5.AppearanceItemCaption.Options.UseForeColor = true;
            this.simpleLabelItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem5.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleLabelItem5.CustomizationFormText = "（1）进餐：使用餐具将饭菜送入口、咀嚼、吞咽等活动";
            this.simpleLabelItem5.Location = new System.Drawing.Point(0, 54);
            this.simpleLabelItem5.MaxSize = new System.Drawing.Size(100, 65);
            this.simpleLabelItem5.MinSize = new System.Drawing.Size(100, 65);
            this.simpleLabelItem5.Name = "simpleLabelItem5";
            this.simpleLabelItem5.Size = new System.Drawing.Size(100, 65);
            this.simpleLabelItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem5.Text = "（1）进餐：使用餐具将饭菜送入口、咀嚼、吞咽等活动";
            this.simpleLabelItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.simpleLabelItem5.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem8.Control = this.chk11;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(100, 54);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(130, 65);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(130, 65);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(130, 65);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "           -----";
            this.emptySpaceItem1.Location = new System.Drawing.Point(230, 54);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(140, 65);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(140, 65);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(140, 65);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "           -----";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem1.TextVisible = true;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem9.Control = this.chk13;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(370, 54);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(140, 65);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(140, 65);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(140, 65);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem11.Control = this.chk14;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(510, 54);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(140, 65);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(140, 65);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(140, 65);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // simpleLabelItem10
            // 
            this.simpleLabelItem10.AllowHotTrack = false;
            this.simpleLabelItem10.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.simpleLabelItem10.AppearanceItemCaption.Options.UseBackColor = true;
            this.simpleLabelItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem10.CustomizationFormText = "评分";
            this.simpleLabelItem10.Location = new System.Drawing.Point(0, 119);
            this.simpleLabelItem10.MaxSize = new System.Drawing.Size(100, 25);
            this.simpleLabelItem10.MinSize = new System.Drawing.Size(100, 25);
            this.simpleLabelItem10.Name = "simpleLabelItem10";
            this.simpleLabelItem10.Size = new System.Drawing.Size(100, 25);
            this.simpleLabelItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem10.Text = "评分";
            this.simpleLabelItem10.TextSize = new System.Drawing.Size(180, 18);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem4.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem4.CustomizationFormText = "0";
            this.emptySpaceItem4.Location = new System.Drawing.Point(100, 119);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 25);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 25);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(130, 25);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.Text = "0";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem4.TextVisible = true;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem5.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem5.CustomizationFormText = "0";
            this.emptySpaceItem5.Location = new System.Drawing.Point(230, 119);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 25);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 25);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(140, 25);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.Text = "0";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem5.TextVisible = true;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem3.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem3.CustomizationFormText = "3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(370, 119);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 25);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 25);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(140, 25);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem3.TextVisible = true;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem6.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem6.CustomizationFormText = "5";
            this.emptySpaceItem6.Location = new System.Drawing.Point(510, 119);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 25);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 25);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(140, 25);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.Text = "5";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem6.TextVisible = true;
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.emptySpaceItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem8.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.emptySpaceItem8.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem8.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem8.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem8.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem8.CustomizationFormText = "（2）梳洗：梳头、洗脸、刷牙、剃须洗澡等活动";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 144);
            this.emptySpaceItem8.MaxSize = new System.Drawing.Size(100, 60);
            this.emptySpaceItem8.MinSize = new System.Drawing.Size(100, 60);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(100, 60);
            this.emptySpaceItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem8.Text = "（2）梳洗：梳头、洗脸、刷牙、剃须洗澡等活动";
            this.emptySpaceItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            this.emptySpaceItem8.TextVisible = true;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem10.Control = this.chk21;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(100, 144);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(130, 60);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(130, 60);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(130, 60);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem12.Control = this.chk22;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(230, 144);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(0, 60);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(140, 60);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(140, 60);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem13.Control = this.chk23;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(370, 144);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(0, 60);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(140, 60);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(140, 60);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "layoutControlItem13";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem14.Control = this.chk24;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(510, 144);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(0, 60);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(140, 60);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(140, 60);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "layoutControlItem14";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextToControlDistance = 0;
            this.layoutControlItem14.TextVisible = false;
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem14.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem14.CustomizationFormText = "评分";
            this.emptySpaceItem14.Location = new System.Drawing.Point(0, 204);
            this.emptySpaceItem14.MaxSize = new System.Drawing.Size(0, 25);
            this.emptySpaceItem14.MinSize = new System.Drawing.Size(10, 25);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(100, 25);
            this.emptySpaceItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem14.Text = "评分";
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem14.TextVisible = true;
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem9.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem9.CustomizationFormText = "0";
            this.emptySpaceItem9.Location = new System.Drawing.Point(100, 204);
            this.emptySpaceItem9.MaxSize = new System.Drawing.Size(130, 25);
            this.emptySpaceItem9.MinSize = new System.Drawing.Size(130, 25);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(130, 25);
            this.emptySpaceItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem9.Text = "0";
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem9.TextVisible = true;
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem10.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem10.CustomizationFormText = "1";
            this.emptySpaceItem10.Location = new System.Drawing.Point(230, 204);
            this.emptySpaceItem10.MaxSize = new System.Drawing.Size(140, 25);
            this.emptySpaceItem10.MinSize = new System.Drawing.Size(140, 25);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(140, 25);
            this.emptySpaceItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem10.Text = "1";
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem10.TextVisible = true;
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem11.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem11.CustomizationFormText = "3";
            this.emptySpaceItem11.Location = new System.Drawing.Point(370, 204);
            this.emptySpaceItem11.MaxSize = new System.Drawing.Size(140, 25);
            this.emptySpaceItem11.MinSize = new System.Drawing.Size(140, 25);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(140, 25);
            this.emptySpaceItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem11.Text = "3";
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem11.TextVisible = true;
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem12.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem12.CustomizationFormText = "7";
            this.emptySpaceItem12.Location = new System.Drawing.Point(510, 204);
            this.emptySpaceItem12.MaxSize = new System.Drawing.Size(140, 25);
            this.emptySpaceItem12.MinSize = new System.Drawing.Size(140, 25);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(140, 25);
            this.emptySpaceItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem12.Text = "7";
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem12.TextVisible = true;
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.emptySpaceItem15.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem15.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.emptySpaceItem15.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem15.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem15.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem15.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem15.CustomizationFormText = "（3）穿衣：穿衣裤、袜子、鞋子等活动";
            this.emptySpaceItem15.Location = new System.Drawing.Point(0, 229);
            this.emptySpaceItem15.MaxSize = new System.Drawing.Size(100, 60);
            this.emptySpaceItem15.MinSize = new System.Drawing.Size(100, 60);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(100, 60);
            this.emptySpaceItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem15.Text = "（3）穿衣：穿衣裤、袜子、鞋子等活动";
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem15.TextVisible = true;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.chk31;
            this.layoutControlItem15.CustomizationFormText = "layoutControlItem15";
            this.layoutControlItem15.Location = new System.Drawing.Point(100, 229);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(130, 60);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(130, 60);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(130, 60);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "layoutControlItem15";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextToControlDistance = 0;
            this.layoutControlItem15.TextVisible = false;
            // 
            // emptySpaceItem18
            // 
            this.emptySpaceItem18.AllowHotTrack = false;
            this.emptySpaceItem18.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.emptySpaceItem18.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem18.CustomizationFormText = "emptySpaceItem18";
            this.emptySpaceItem18.Location = new System.Drawing.Point(230, 229);
            this.emptySpaceItem18.MaxSize = new System.Drawing.Size(140, 60);
            this.emptySpaceItem18.MinSize = new System.Drawing.Size(140, 60);
            this.emptySpaceItem18.Name = "emptySpaceItem18";
            this.emptySpaceItem18.Size = new System.Drawing.Size(140, 60);
            this.emptySpaceItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem18.Text = "emptySpaceItem18";
            this.emptySpaceItem18.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem16.Control = this.chk33;
            this.layoutControlItem16.CustomizationFormText = "layoutControlItem16";
            this.layoutControlItem16.Location = new System.Drawing.Point(370, 229);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(140, 60);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(140, 60);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(140, 60);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "layoutControlItem16";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextToControlDistance = 0;
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem17.Control = this.chk34;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(510, 229);
            this.layoutControlItem17.MaxSize = new System.Drawing.Size(140, 60);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(140, 60);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(140, 60);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem16.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem16.CustomizationFormText = "评分";
            this.emptySpaceItem16.Location = new System.Drawing.Point(0, 289);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.Size = new System.Drawing.Size(100, 25);
            this.emptySpaceItem16.Text = "评分";
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem16.TextVisible = true;
            // 
            // emptySpaceItem24
            // 
            this.emptySpaceItem24.AllowHotTrack = false;
            this.emptySpaceItem24.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem24.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem24.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem24.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem24.CustomizationFormText = "0";
            this.emptySpaceItem24.Location = new System.Drawing.Point(100, 289);
            this.emptySpaceItem24.Name = "emptySpaceItem24";
            this.emptySpaceItem24.Size = new System.Drawing.Size(130, 25);
            this.emptySpaceItem24.Text = "0";
            this.emptySpaceItem24.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem24.TextVisible = true;
            // 
            // emptySpaceItem23
            // 
            this.emptySpaceItem23.AllowHotTrack = false;
            this.emptySpaceItem23.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem23.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem23.CustomizationFormText = "0";
            this.emptySpaceItem23.Location = new System.Drawing.Point(230, 289);
            this.emptySpaceItem23.Name = "emptySpaceItem23";
            this.emptySpaceItem23.Size = new System.Drawing.Size(140, 25);
            this.emptySpaceItem23.Text = "0";
            this.emptySpaceItem23.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem23.TextVisible = true;
            // 
            // emptySpaceItem21
            // 
            this.emptySpaceItem21.AllowHotTrack = false;
            this.emptySpaceItem21.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem21.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem21.CustomizationFormText = "3";
            this.emptySpaceItem21.Location = new System.Drawing.Point(370, 289);
            this.emptySpaceItem21.MinSize = new System.Drawing.Size(10, 25);
            this.emptySpaceItem21.Name = "emptySpaceItem21";
            this.emptySpaceItem21.Size = new System.Drawing.Size(140, 25);
            this.emptySpaceItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem21.Text = "3";
            this.emptySpaceItem21.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem21.TextVisible = true;
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem13.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem13.CustomizationFormText = "5";
            this.emptySpaceItem13.Location = new System.Drawing.Point(510, 289);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(140, 25);
            this.emptySpaceItem13.Text = "5";
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem13.TextVisible = true;
            // 
            // emptySpaceItem25
            // 
            this.emptySpaceItem25.AllowHotTrack = false;
            this.emptySpaceItem25.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.emptySpaceItem25.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem25.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.emptySpaceItem25.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem25.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem25.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem25.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem25.CustomizationFormText = "（4）如厕：小便、大便等活动及自控";
            this.emptySpaceItem25.Location = new System.Drawing.Point(0, 314);
            this.emptySpaceItem25.MaxSize = new System.Drawing.Size(100, 0);
            this.emptySpaceItem25.MinSize = new System.Drawing.Size(100, 50);
            this.emptySpaceItem25.Name = "emptySpaceItem25";
            this.emptySpaceItem25.Size = new System.Drawing.Size(100, 50);
            this.emptySpaceItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem25.Text = "（4）如厕：小便、大便等活动及自控";
            this.emptySpaceItem25.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem25.TextVisible = true;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem18.Control = this.chk41;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(100, 314);
            this.layoutControlItem18.MaxSize = new System.Drawing.Size(130, 60);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(130, 50);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(130, 50);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "layoutControlItem18";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextToControlDistance = 0;
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem20.Control = this.chk42;
            this.layoutControlItem20.CustomizationFormText = "layoutControlItem20";
            this.layoutControlItem20.Location = new System.Drawing.Point(230, 314);
            this.layoutControlItem20.MaxSize = new System.Drawing.Size(140, 60);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(140, 50);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(140, 50);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "layoutControlItem20";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextToControlDistance = 0;
            this.layoutControlItem20.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.chk43;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(370, 314);
            this.layoutControlItem19.MaxSize = new System.Drawing.Size(0, 60);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(140, 50);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(140, 50);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "layoutControlItem19";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextToControlDistance = 0;
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem21.Control = this.chk44;
            this.layoutControlItem21.CustomizationFormText = "layoutControlItem21";
            this.layoutControlItem21.Location = new System.Drawing.Point(510, 314);
            this.layoutControlItem21.MaxSize = new System.Drawing.Size(0, 65);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(140, 50);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(140, 50);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "layoutControlItem21";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextToControlDistance = 0;
            this.layoutControlItem21.TextVisible = false;
            // 
            // emptySpaceItem20
            // 
            this.emptySpaceItem20.AllowHotTrack = false;
            this.emptySpaceItem20.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem20.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem20.CustomizationFormText = "评分";
            this.emptySpaceItem20.Location = new System.Drawing.Point(0, 364);
            this.emptySpaceItem20.MinSize = new System.Drawing.Size(10, 25);
            this.emptySpaceItem20.Name = "emptySpaceItem20";
            this.emptySpaceItem20.Size = new System.Drawing.Size(100, 25);
            this.emptySpaceItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem20.Text = "评分";
            this.emptySpaceItem20.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem20.TextVisible = true;
            // 
            // emptySpaceItem19
            // 
            this.emptySpaceItem19.AllowHotTrack = false;
            this.emptySpaceItem19.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem19.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem19.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem19.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem19.CustomizationFormText = "0";
            this.emptySpaceItem19.Location = new System.Drawing.Point(100, 364);
            this.emptySpaceItem19.MaxSize = new System.Drawing.Size(0, 25);
            this.emptySpaceItem19.MinSize = new System.Drawing.Size(10, 25);
            this.emptySpaceItem19.Name = "emptySpaceItem19";
            this.emptySpaceItem19.Size = new System.Drawing.Size(130, 25);
            this.emptySpaceItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem19.Text = "0";
            this.emptySpaceItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem19.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem19.TextVisible = true;
            // 
            // emptySpaceItem22
            // 
            this.emptySpaceItem22.AllowHotTrack = false;
            this.emptySpaceItem22.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem22.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem22.CustomizationFormText = "1";
            this.emptySpaceItem22.Location = new System.Drawing.Point(230, 364);
            this.emptySpaceItem22.MaxSize = new System.Drawing.Size(0, 25);
            this.emptySpaceItem22.MinSize = new System.Drawing.Size(10, 25);
            this.emptySpaceItem22.Name = "emptySpaceItem22";
            this.emptySpaceItem22.Size = new System.Drawing.Size(140, 25);
            this.emptySpaceItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem22.Text = "1";
            this.emptySpaceItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem22.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem22.TextVisible = true;
            // 
            // emptySpaceItem27
            // 
            this.emptySpaceItem27.AllowHotTrack = false;
            this.emptySpaceItem27.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem27.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem27.CustomizationFormText = "5";
            this.emptySpaceItem27.Location = new System.Drawing.Point(370, 364);
            this.emptySpaceItem27.MaxSize = new System.Drawing.Size(140, 25);
            this.emptySpaceItem27.MinSize = new System.Drawing.Size(140, 25);
            this.emptySpaceItem27.Name = "emptySpaceItem27";
            this.emptySpaceItem27.Size = new System.Drawing.Size(140, 25);
            this.emptySpaceItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem27.Text = "5";
            this.emptySpaceItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem27.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem27.TextVisible = true;
            // 
            // emptySpaceItem28
            // 
            this.emptySpaceItem28.AllowHotTrack = false;
            this.emptySpaceItem28.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem28.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem28.CustomizationFormText = "10";
            this.emptySpaceItem28.Location = new System.Drawing.Point(510, 364);
            this.emptySpaceItem28.MaxSize = new System.Drawing.Size(140, 25);
            this.emptySpaceItem28.MinSize = new System.Drawing.Size(140, 25);
            this.emptySpaceItem28.Name = "emptySpaceItem28";
            this.emptySpaceItem28.Size = new System.Drawing.Size(140, 25);
            this.emptySpaceItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem28.Text = "10";
            this.emptySpaceItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem28.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem28.TextVisible = true;
            // 
            // emptySpaceItem31
            // 
            this.emptySpaceItem31.AllowHotTrack = false;
            this.emptySpaceItem31.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.emptySpaceItem31.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem31.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.emptySpaceItem31.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem31.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem31.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem31.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem31.CustomizationFormText = "（5）活动：站立、室内行走、上下楼梯、户外活动";
            this.emptySpaceItem31.Location = new System.Drawing.Point(0, 389);
            this.emptySpaceItem31.MaxSize = new System.Drawing.Size(100, 0);
            this.emptySpaceItem31.MinSize = new System.Drawing.Size(100, 60);
            this.emptySpaceItem31.Name = "emptySpaceItem31";
            this.emptySpaceItem31.Size = new System.Drawing.Size(100, 60);
            this.emptySpaceItem31.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem31.Text = "（5）活动：站立、室内行走、上下楼梯、户外活动";
            this.emptySpaceItem31.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem31.TextVisible = true;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem22.Control = this.chk51;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(100, 389);
            this.layoutControlItem22.MaxSize = new System.Drawing.Size(130, 60);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(130, 60);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(130, 60);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "layoutControlItem22";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextToControlDistance = 0;
            this.layoutControlItem22.TextVisible = false;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.chk52;
            this.layoutControlItem23.CustomizationFormText = "layoutControlItem23";
            this.layoutControlItem23.Location = new System.Drawing.Point(230, 389);
            this.layoutControlItem23.MaxSize = new System.Drawing.Size(0, 60);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(140, 60);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(140, 60);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "layoutControlItem23";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextToControlDistance = 0;
            this.layoutControlItem23.TextVisible = false;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.chk53;
            this.layoutControlItem24.CustomizationFormText = "layoutControlItem24";
            this.layoutControlItem24.Location = new System.Drawing.Point(370, 389);
            this.layoutControlItem24.MaxSize = new System.Drawing.Size(0, 60);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(140, 60);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(140, 60);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.Text = "layoutControlItem24";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem24.TextToControlDistance = 0;
            this.layoutControlItem24.TextVisible = false;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem25.Control = this.chk54;
            this.layoutControlItem25.CustomizationFormText = "layoutControlItem25";
            this.layoutControlItem25.Location = new System.Drawing.Point(510, 389);
            this.layoutControlItem25.MaxSize = new System.Drawing.Size(140, 60);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(140, 60);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(140, 60);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Tag = "10";
            this.layoutControlItem25.Text = "layoutControlItem25";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextToControlDistance = 0;
            this.layoutControlItem25.TextVisible = false;
            // 
            // emptySpaceItem35
            // 
            this.emptySpaceItem35.AllowHotTrack = false;
            this.emptySpaceItem35.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem35.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem35.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem35.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem35.CustomizationFormText = "评分";
            this.emptySpaceItem35.Location = new System.Drawing.Point(0, 449);
            this.emptySpaceItem35.MinSize = new System.Drawing.Size(10, 25);
            this.emptySpaceItem35.Name = "emptySpaceItem35";
            this.emptySpaceItem35.Size = new System.Drawing.Size(100, 25);
            this.emptySpaceItem35.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem35.Text = "评分";
            this.emptySpaceItem35.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem35.TextVisible = true;
            // 
            // emptySpaceItem32
            // 
            this.emptySpaceItem32.AllowHotTrack = false;
            this.emptySpaceItem32.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem32.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem32.CustomizationFormText = "0";
            this.emptySpaceItem32.Location = new System.Drawing.Point(100, 449);
            this.emptySpaceItem32.MinSize = new System.Drawing.Size(10, 25);
            this.emptySpaceItem32.Name = "emptySpaceItem32";
            this.emptySpaceItem32.Size = new System.Drawing.Size(130, 25);
            this.emptySpaceItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem32.Text = "0";
            this.emptySpaceItem32.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem32.TextVisible = true;
            // 
            // emptySpaceItem30
            // 
            this.emptySpaceItem30.AllowHotTrack = false;
            this.emptySpaceItem30.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem30.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem30.CustomizationFormText = "1";
            this.emptySpaceItem30.Location = new System.Drawing.Point(230, 449);
            this.emptySpaceItem30.MaxSize = new System.Drawing.Size(140, 0);
            this.emptySpaceItem30.MinSize = new System.Drawing.Size(140, 25);
            this.emptySpaceItem30.Name = "emptySpaceItem30";
            this.emptySpaceItem30.Size = new System.Drawing.Size(140, 25);
            this.emptySpaceItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem30.Text = "1";
            this.emptySpaceItem30.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem30.TextVisible = true;
            // 
            // emptySpaceItem33
            // 
            this.emptySpaceItem33.AllowHotTrack = false;
            this.emptySpaceItem33.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem33.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem33.CustomizationFormText = "5";
            this.emptySpaceItem33.Location = new System.Drawing.Point(370, 449);
            this.emptySpaceItem33.MaxSize = new System.Drawing.Size(140, 0);
            this.emptySpaceItem33.MinSize = new System.Drawing.Size(140, 25);
            this.emptySpaceItem33.Name = "emptySpaceItem33";
            this.emptySpaceItem33.Size = new System.Drawing.Size(140, 25);
            this.emptySpaceItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem33.Text = "5";
            this.emptySpaceItem33.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem33.TextVisible = true;
            // 
            // emptySpaceItem34
            // 
            this.emptySpaceItem34.AllowHotTrack = false;
            this.emptySpaceItem34.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem34.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem34.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem34.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem34.CustomizationFormText = "10";
            this.emptySpaceItem34.Location = new System.Drawing.Point(510, 449);
            this.emptySpaceItem34.MaxSize = new System.Drawing.Size(140, 0);
            this.emptySpaceItem34.MinSize = new System.Drawing.Size(140, 25);
            this.emptySpaceItem34.Name = "emptySpaceItem34";
            this.emptySpaceItem34.Size = new System.Drawing.Size(140, 25);
            this.emptySpaceItem34.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem34.Text = "10";
            this.emptySpaceItem34.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem34.TextVisible = true;
            // 
            // emptySpaceItem36
            // 
            this.emptySpaceItem36.AllowHotTrack = false;
            this.emptySpaceItem36.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.emptySpaceItem36.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem36.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem36.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem36.CustomizationFormText = "总评分";
            this.emptySpaceItem36.Location = new System.Drawing.Point(0, 474);
            this.emptySpaceItem36.MinSize = new System.Drawing.Size(10, 25);
            this.emptySpaceItem36.Name = "emptySpaceItem36";
            this.emptySpaceItem36.Size = new System.Drawing.Size(650, 25);
            this.emptySpaceItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem36.Text = "总评分";
            this.emptySpaceItem36.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem36.TextVisible = true;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.result1;
            this.layoutControlItem33.CustomizationFormText = "layoutControlItem33";
            this.layoutControlItem33.Location = new System.Drawing.Point(650, 54);
            this.layoutControlItem33.MaxSize = new System.Drawing.Size(115, 90);
            this.layoutControlItem33.MinSize = new System.Drawing.Size(115, 90);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(398, 90);
            this.layoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem33.Text = "layoutControlItem33";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem33.TextToControlDistance = 0;
            this.layoutControlItem33.TextVisible = false;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.result2;
            this.layoutControlItem34.CustomizationFormText = "layoutControlItem34";
            this.layoutControlItem34.Location = new System.Drawing.Point(650, 144);
            this.layoutControlItem34.MaxSize = new System.Drawing.Size(115, 85);
            this.layoutControlItem34.MinSize = new System.Drawing.Size(115, 85);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(398, 85);
            this.layoutControlItem34.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem34.Text = "layoutControlItem34";
            this.layoutControlItem34.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem34.TextToControlDistance = 0;
            this.layoutControlItem34.TextVisible = false;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.result3;
            this.layoutControlItem35.CustomizationFormText = "layoutControlItem35";
            this.layoutControlItem35.Location = new System.Drawing.Point(650, 229);
            this.layoutControlItem35.MaxSize = new System.Drawing.Size(115, 85);
            this.layoutControlItem35.MinSize = new System.Drawing.Size(115, 85);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(398, 85);
            this.layoutControlItem35.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem35.Text = "layoutControlItem35";
            this.layoutControlItem35.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem35.TextToControlDistance = 0;
            this.layoutControlItem35.TextVisible = false;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.result4;
            this.layoutControlItem36.CustomizationFormText = "layoutControlItem36";
            this.layoutControlItem36.Location = new System.Drawing.Point(650, 314);
            this.layoutControlItem36.MaxSize = new System.Drawing.Size(115, 75);
            this.layoutControlItem36.MinSize = new System.Drawing.Size(115, 75);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(398, 75);
            this.layoutControlItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem36.Text = "layoutControlItem36";
            this.layoutControlItem36.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem36.TextToControlDistance = 0;
            this.layoutControlItem36.TextVisible = false;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.result5;
            this.layoutControlItem37.CustomizationFormText = "layoutControlItem37";
            this.layoutControlItem37.Location = new System.Drawing.Point(650, 389);
            this.layoutControlItem37.MaxSize = new System.Drawing.Size(115, 85);
            this.layoutControlItem37.MinSize = new System.Drawing.Size(115, 85);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(398, 85);
            this.layoutControlItem37.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem37.Text = "layoutControlItem37";
            this.layoutControlItem37.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem37.TextToControlDistance = 0;
            this.layoutControlItem37.TextVisible = false;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.result;
            this.layoutControlItem38.CustomizationFormText = "layoutControlItem38";
            this.layoutControlItem38.Location = new System.Drawing.Point(650, 474);
            this.layoutControlItem38.MaxSize = new System.Drawing.Size(115, 25);
            this.layoutControlItem38.MinSize = new System.Drawing.Size(115, 25);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(398, 25);
            this.layoutControlItem38.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem38.Text = "layoutControlItem38";
            this.layoutControlItem38.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem38.TextToControlDistance = 0;
            this.layoutControlItem38.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem38,
            this.layoutControlItem26,
            this.lbl随访日期,
            this.lbl下次随访日期,
            this.lbl录入医生,
            this.lbl随访医生签名});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 559);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(1054, 132);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // emptySpaceItem38
            // 
            this.emptySpaceItem38.AllowHotTrack = false;
            this.emptySpaceItem38.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem38.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem38.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem38.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem38.CustomizationFormText = "下次随访事项 ";
            this.emptySpaceItem38.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem38.Name = "emptySpaceItem38";
            this.emptySpaceItem38.Size = new System.Drawing.Size(1048, 24);
            this.emptySpaceItem38.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.SupportHorzAlignment;
            this.emptySpaceItem38.Text = "下次随访事项 ";
            this.emptySpaceItem38.TextSize = new System.Drawing.Size(180, 0);
            this.emptySpaceItem38.TextVisible = true;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem26.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.Control = this.txt下次随访目标;
            this.layoutControlItem26.CustomizationFormText = "下次随访目标";
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem26.MaxSize = new System.Drawing.Size(500, 0);
            this.layoutControlItem26.MinSize = new System.Drawing.Size(500, 54);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(1048, 54);
            this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem26.Text = "下次随访目标";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(100, 50);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // lbl随访日期
            // 
            this.lbl随访日期.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl随访日期.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl随访日期.AppearanceItemCaption.Options.UseFont = true;
            this.lbl随访日期.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl随访日期.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl随访日期.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl随访日期.Control = this.dte随访日期;
            this.lbl随访日期.CustomizationFormText = "随访日期*";
            this.lbl随访日期.Location = new System.Drawing.Point(0, 78);
            this.lbl随访日期.MaxSize = new System.Drawing.Size(250, 24);
            this.lbl随访日期.MinSize = new System.Drawing.Size(250, 24);
            this.lbl随访日期.Name = "lbl随访日期";
            this.lbl随访日期.Size = new System.Drawing.Size(250, 24);
            this.lbl随访日期.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl随访日期.Text = "随访日期*";
            this.lbl随访日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl随访日期.TextSize = new System.Drawing.Size(100, 20);
            this.lbl随访日期.TextToControlDistance = 5;
            // 
            // lbl下次随访日期
            // 
            this.lbl下次随访日期.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl下次随访日期.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl下次随访日期.AppearanceItemCaption.Options.UseFont = true;
            this.lbl下次随访日期.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl下次随访日期.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl下次随访日期.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl下次随访日期.Control = this.dte下次随访日期;
            this.lbl下次随访日期.CustomizationFormText = "下次随访日期*";
            this.lbl下次随访日期.Location = new System.Drawing.Point(0, 102);
            this.lbl下次随访日期.MaxSize = new System.Drawing.Size(250, 24);
            this.lbl下次随访日期.MinSize = new System.Drawing.Size(250, 24);
            this.lbl下次随访日期.Name = "lbl下次随访日期";
            this.lbl下次随访日期.Size = new System.Drawing.Size(250, 24);
            this.lbl下次随访日期.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl下次随访日期.Text = "下次随访日期*";
            this.lbl下次随访日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl下次随访日期.TextSize = new System.Drawing.Size(100, 14);
            this.lbl下次随访日期.TextToControlDistance = 5;
            // 
            // lbl录入医生
            // 
            this.lbl录入医生.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl录入医生.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl录入医生.Control = this.txt录入医生;
            this.lbl录入医生.CustomizationFormText = "录入医生";
            this.lbl录入医生.Location = new System.Drawing.Point(250, 102);
            this.lbl录入医生.MaxSize = new System.Drawing.Size(250, 24);
            this.lbl录入医生.MinSize = new System.Drawing.Size(250, 24);
            this.lbl录入医生.Name = "lbl录入医生";
            this.lbl录入医生.Size = new System.Drawing.Size(798, 24);
            this.lbl录入医生.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl录入医生.Text = "录入医生";
            this.lbl录入医生.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl录入医生.TextSize = new System.Drawing.Size(100, 20);
            this.lbl录入医生.TextToControlDistance = 5;
            // 
            // lbl随访医生签名
            // 
            this.lbl随访医生签名.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl随访医生签名.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl随访医生签名.AppearanceItemCaption.Options.UseFont = true;
            this.lbl随访医生签名.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl随访医生签名.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl随访医生签名.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl随访医生签名.Control = this.txt随访医生签名;
            this.lbl随访医生签名.CustomizationFormText = "随访医生签名";
            this.lbl随访医生签名.Location = new System.Drawing.Point(250, 78);
            this.lbl随访医生签名.MaxSize = new System.Drawing.Size(250, 24);
            this.lbl随访医生签名.MinSize = new System.Drawing.Size(250, 24);
            this.lbl随访医生签名.Name = "lbl随访医生签名";
            this.lbl随访医生签名.Size = new System.Drawing.Size(798, 24);
            this.lbl随访医生签名.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl随访医生签名.Text = "随访医生签名";
            this.lbl随访医生签名.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl随访医生签名.TextSize = new System.Drawing.Size(100, 20);
            this.lbl随访医生签名.TextToControlDistance = 5;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem28,
            this.layoutControlItem32,
            this.layoutControlItem29,
            this.layoutControlItem31,
            this.layoutControlItem27,
            this.layoutControlItem30});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 691);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Size = new System.Drawing.Size(1054, 62);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.txt当前所属机构;
            this.layoutControlItem28.CustomizationFormText = "当前所属机构:";
            this.layoutControlItem28.Location = new System.Drawing.Point(665, 0);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(383, 28);
            this.layoutControlItem28.Text = "当前所属机构:";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.txt最近修改人;
            this.layoutControlItem32.CustomizationFormText = "最近修改人: ";
            this.layoutControlItem32.Location = new System.Drawing.Point(665, 28);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(383, 28);
            this.layoutControlItem32.Text = "最近修改人: ";
            this.layoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem32.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.txt创建人;
            this.layoutControlItem29.CustomizationFormText = "创建人:";
            this.layoutControlItem29.Location = new System.Drawing.Point(323, 28);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(342, 28);
            this.layoutControlItem29.Text = "创建人:";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.txt最近更新时间;
            this.layoutControlItem31.CustomizationFormText = "最近更新时间: ";
            this.layoutControlItem31.Location = new System.Drawing.Point(323, 0);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(342, 28);
            this.layoutControlItem31.Text = "最近更新时间: ";
            this.layoutControlItem31.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem31.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.txt创建时间;
            this.layoutControlItem27.CustomizationFormText = "创建时间: ";
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(323, 28);
            this.layoutControlItem27.Text = "录入时间: ";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.txt创建机构;
            this.layoutControlItem30.CustomizationFormText = "创建机构: ";
            this.layoutControlItem30.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(323, 28);
            this.layoutControlItem30.Text = "创建机构: ";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem1,
            this.layoutControlItem4,
            this.layoutControlItem3,
            this.layoutControlItem7});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Size = new System.Drawing.Size(1054, 54);
            this.layoutControlGroup5.Text = "layoutControlGroup5";
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt出生日期;
            this.layoutControlItem5.CustomizationFormText = "出生日期";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(194, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(329, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "出生日期";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.txt姓名;
            this.layoutControlItem1.CustomizationFormText = "姓名";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(194, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(329, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "姓名";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.txt身份证号;
            this.layoutControlItem4.CustomizationFormText = "身份证号";
            this.layoutControlItem4.Location = new System.Drawing.Point(606, 0);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(194, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(442, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "身份证号";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.txt性别;
            this.layoutControlItem3.CustomizationFormText = "性别";
            this.layoutControlItem3.Location = new System.Drawing.Point(329, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(194, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(277, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "性别";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.txt居住地址;
            this.layoutControlItem7.CustomizationFormText = "居住地址";
            this.layoutControlItem7.Location = new System.Drawing.Point(329, 24);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(194, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(719, 24);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "居住地址";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // frm老年人生活自理能力评估表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1075, 590);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frm老年人生活自理能力评估表";
            this.Load += new System.EventHandler(this.UC老年人生活自理能力评估表_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt最近修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近更新时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt当前所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访医生签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访目标.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk54.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk53.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk52.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk51.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk44.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk43.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk41.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk34.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk33.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl随访日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl下次随访日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl录入医生)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl随访医生签名)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txt出生日期;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt性别;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem2;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem3;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem4;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.TextEdit txt居住地址;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem6;
        public DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem7;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem8;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem9;
        private DevExpress.XtraEditors.CheckEdit chk14;
        private DevExpress.XtraEditors.CheckEdit chk13;
        private DevExpress.XtraEditors.CheckEdit chk11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraEditors.CheckEdit chk22;
        private DevExpress.XtraEditors.CheckEdit chk21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.CheckEdit chk24;
        private DevExpress.XtraEditors.CheckEdit chk23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem18;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem20;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem21;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem23;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem24;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem25;
        private DevExpress.XtraEditors.CheckEdit chk41;
        private DevExpress.XtraEditors.CheckEdit chk34;
        private DevExpress.XtraEditors.CheckEdit chk33;
        private DevExpress.XtraEditors.CheckEdit chk31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraEditors.CheckEdit chk44;
        private DevExpress.XtraEditors.CheckEdit chk42;
        private DevExpress.XtraEditors.CheckEdit chk43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem19;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem22;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem27;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem28;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem33;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem34;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem35;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem30;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem31;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem32;
        private DevExpress.XtraEditors.CheckEdit chk54;
        private DevExpress.XtraEditors.CheckEdit chk53;
        private DevExpress.XtraEditors.CheckEdit chk52;
        private DevExpress.XtraEditors.CheckEdit chk51;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem36;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem38;
        private DevExpress.XtraEditors.MemoEdit txt下次随访目标;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraEditors.TextEdit txt随访医生签名;
        private DevExpress.XtraEditors.TextEdit txt录入医生;
        private DevExpress.XtraLayout.LayoutControlItem lbl随访日期;
        private DevExpress.XtraLayout.LayoutControlItem lbl录入医生;
        private DevExpress.XtraLayout.LayoutControlItem lbl下次随访日期;
        private DevExpress.XtraLayout.LayoutControlItem lbl随访医生签名;
        private DevExpress.XtraEditors.DateEdit dte随访日期;
        private DevExpress.XtraEditors.DateEdit dte下次随访日期;
        private DevExpress.XtraEditors.TextEdit txt最近修改人;
        private DevExpress.XtraEditors.TextEdit txt最近更新时间;
        private DevExpress.XtraEditors.TextEdit txt创建机构;
        private DevExpress.XtraEditors.TextEdit txt创建人;
        private DevExpress.XtraEditors.TextEdit txt当前所属机构;
        private DevExpress.XtraEditors.TextEdit txt创建时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraEditors.LabelControl result1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraEditors.LabelControl result4;
        private DevExpress.XtraEditors.LabelControl result3;
        private DevExpress.XtraEditors.LabelControl result2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraEditors.LabelControl result;
        private DevExpress.XtraEditors.LabelControl result5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;

    }
}
