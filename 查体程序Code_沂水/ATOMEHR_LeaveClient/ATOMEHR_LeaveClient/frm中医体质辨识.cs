﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ATOMEHR_LeaveClient
{
    public partial class frm中医体质辨识 : Form
    {

        #region Fields
        User currentUser;
        tb_老年人中医药特征管理Info tab_体质辨识 = new tb_老年人中医药特征管理Info();
        string _serverDateTime;

        int t1 = 0, t2 = 0, t3 = 0, t4 = 0, t5 = 0, t6 = 0, t7 = 0, t8 = 0, t9 = 0, t10 = 0, t11 = 0, t12 = 0, t13 = 0, t14 = 0, t15 = 0, t16 = 0, t17 = 0, t18 = 0, t19 = 0, t20 = 0, t21 = 0, t22 = 0, t23 = 0, t24 = 0, t25 = 0, t26 = 0, t27 = 0, t28 = 0, t29 = 0, t30 = 0, t31 = 0, t32 = 0, t33 = 0;
        string is气虚质, is阳虚质, is阴虚质, is痰湿质, is湿热质, is血瘀质, is气郁质, is特禀质, is平和质;
        int jf1 = 0, jf2 = 0, jf3 = 0, jf4 = 0, jf5 = 0, jf6 = 0, jf7 = 0, jf8 = 0, jf9 = 0;
        #endregion
        #region 问卷列表

        List<Question> list = new List<Question>()
        {
            new Question(){Index=0, QuestionName="(1)您精力充沛吗？（指精神头足，乐于做事）",Score = 0},
            new Question(){Index=1, QuestionName="(2)您容易疲乏吗？（指体力如何，是否稍微活动一下或做一点家务劳动就感到累）",Score = 0},
            new Question(){Index=2, QuestionName="(3)您容易气短，呼吸短促，接不上气吗？",Score = 0},
            new Question(){Index=3, QuestionName="(4)您说话声音低弱无力吗?（指说话没有力气）",Score = 0},
            new Question(){Index=4, QuestionName="(5)您感到闷闷不乐、情绪低沉吗?（指心情不愉快，情绪低落）",Score = 0},
            new Question(){Index=5, QuestionName="(6)您容易精神紧张、焦虑不安吗?（指遇事是否心情紧张）",Score = 0},
            new Question(){Index=6, QuestionName="(7)您因为生活状态改变而感到孤独、失落吗？",Score = 0},
            new Question(){Index=7, QuestionName="(8)您容易感到害怕或受到惊吓吗?",Score = 0},
            new Question(){Index=8, QuestionName="(9)您感到身体超重不轻松吗?(感觉身体沉重)\r\n[BMI指数=体重（kg）/身高2（m）]",Score = 0},
            new Question(){Index=9, QuestionName="(10)您眼睛干涩吗?",Score = 0},
            new Question(){Index=10, QuestionName="(11)您手脚发凉吗?（不包含因周围温度低或穿的少导致的手脚发冷）",Score = 0},
            new Question(){Index=11, QuestionName="(12)您胃脘部、背部或腰膝部怕冷吗？（指上腹部、背部、腰部或膝关节等，有一处或多处怕冷）",Score = 0},
            new Question(){Index=12, QuestionName="(13)您比一般人耐受不了寒冷吗？（指比别人容易害怕冬天或是夏天的冷空调、电扇等）",Score = 0},
            new Question(){Index=13, QuestionName="(14)您容易患感冒吗?（指每年感冒的次数）",Score = 0},
            new Question(){Index=14, QuestionName="(15)您没有感冒时也会鼻塞、流鼻涕吗?",Score = 0},
            new Question(){Index=15, QuestionName="(16)您有口粘口腻，或睡眠打鼾吗？",Score = 0},
            new Question(){Index=16, QuestionName="(17)您容易过敏(对药物、食物、气味、花粉或在季节交替、气候变化时)吗?",Score = 0},
            new Question(){Index=17, QuestionName="(18)您的皮肤容易起荨麻疹吗? (包括风团、风疹块、风疙瘩)",Score = 0},
            new Question(){Index=18, QuestionName="(19)您的皮肤在不知不觉中会出现青紫瘀斑、皮下出血吗?（指皮肤在没有外伤的情况下出现青一块紫一块的情况）",Score = 0},
            new Question(){Index=19, QuestionName="(20)您的皮肤一抓就红，并出现抓痕吗?（指被指甲或钝物划过后皮肤的反应）",Score = 0},
            new Question(){Index=20, QuestionName="(21)您皮肤或口唇干吗?",Score = 0},
            new Question(){Index=21, QuestionName="(22)您有肢体麻木或固定部位疼痛的感觉吗？",Score = 0},
            new Question(){Index=22, QuestionName="(23)您面部或鼻部有油腻感或者油亮发光吗?（指脸上或鼻子）",Score = 0},
            new Question(){Index=23, QuestionName="(24)您面色或目眶晦黯，或出现褐色斑块/斑点吗?",Score = 0},
            new Question(){Index=24, QuestionName="(25)您有皮肤湿疹、疮疖吗？",Score = 0},
            new Question(){Index=25, QuestionName="(26)您感到口干咽燥、总想喝水吗？",Score = 0},
            new Question(){Index=26, QuestionName="(27)您感到口苦或嘴里有异味吗?（指口苦或口臭）",Score = 0},
            new Question(){Index=27, QuestionName="(28)您腹部肥大吗?（指腹部脂肪肥厚）",Score = 0},
            new Question(){Index=28, QuestionName="(29)您吃(喝)凉的东西会感到不舒服或者怕吃(喝)凉的东西吗？（指不喜欢吃凉的食物，或吃了凉的食物后会不舒服）",Score = 0},
            new Question(){Index=29, QuestionName="(30)您有大便黏滞不爽、解不尽的感觉吗?(大便容易粘在马桶或便坑壁上)",Score = 0},
            new Question(){Index=30, QuestionName="(31)您容易大便干燥吗?",Score = 0},
            new Question(){Index=31, QuestionName="(32)您舌苔厚腻或有舌苔厚厚的感觉吗?（如果自我感觉不清楚可由调查员观察后填写）",Score = 0},
            new Question(){Index=32, QuestionName="(33)您舌下静脉瘀紫或增粗吗？（可由调查员辅助观察后填写）",Score = 0}
        };

        #endregion
        public frm中医体质辨识()
        {
            InitializeComponent();
        }
        public frm中医体质辨识(User _currentUser)
        {
            InitializeComponent();
            this.currentUser = _currentUser;
            this.currentUser.Tab_中医辨识体质 = tab_体质辨识;
        }
        private void frm中医体质辨识_Load(object sender, EventArgs e)
        {
            this.lblQuestion.Text = list[0].QuestionName;
            this.lblQuestion.Tag = list[0].Index;
            SetBtnEnable(list[0].Index);
            this.dockPanel1.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
        }
        private void 计算体质()
        {
            if (t1 != 0 && t2 != 0 && t3 != 0 && t4 != 0 && t5 != 0 && t6 != 0 && t7 != 0 && t8 != 0 && t9 != 0 && t10 != 0 && t11 != 0 && t12 != 0 && t13 != 0 && t14 != 0 && t15 != 0 && t16 != 0 && t17 != 0 && t18 != 0 && t19 != 0 && t20 != 0 && t21 != 0 && t22 != 0 && t23 != 0 && t24 != 0 && t25 != 0 && t26 != 0 && t27 != 0 && t28 != 0 && t29 != 0 && t30 != 0 && t31 != 0 && t32 != 0 && t33 != 0)
            {
                //算每种体质的积分
                jf1 = t2 + t3 + t4 + t14;
                jf2 = t11 + t12 + t13 + t29;
                jf3 = t10 + t21 + t26 + t31;
                jf4 = t9 + t16 + t28 + t32;
                jf5 = t23 + t25 + t27 + t30;
                jf6 = t19 + t22 + t24 + t33;
                jf7 = t5 + t6 + t7 + t8;
                jf8 = t15 + t17 + t18 + t20;
                jf9 = t1 + (6 - t2) + (6 - t4) + (6 - t5) + (6 - t13);
                //赋积分
                this.textBox1.Text = jf1.ToString();
                this.textBox2.Text = jf2.ToString();
                this.textBox3.Text = jf3.ToString();
                this.textBox4.Text = jf4.ToString();
                this.textBox5.Text = jf5.ToString();
                this.textBox6.Text = jf6.ToString();
                this.textBox7.Text = jf7.ToString();
                this.textBox8.Text = jf8.ToString();
                this.textBox9.Text = jf9.ToString();

                //根据积分赋体质辨识
                //根据积分赋体质辨识
                if (jf1 >= 9 && jf1 <= 10)
                {
                    is气虚质 = "2";
                    this.labelControl1.Text = "倾向是";
                    this.sp1.ForeColor = System.Drawing.Color.Blue;
                }
                else if (jf1 >= 11)
                {
                    is气虚质 = "1";
                    this.labelControl1.Text = "是";
                    this.sp1.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    is气虚质 = "";
                    this.labelControl1.Text = "";
                    this.sp1.ForeColor = System.Drawing.Color.Black;
                }
                if (jf2 >= 9 && jf2 <= 10)
                {
                    is阳虚质 = "2";
                    this.labelControl2.Text = "倾向是";
                    this.sp2.ForeColor = System.Drawing.Color.Blue;
                }
                else if (jf2 >= 11)
                {
                    is阳虚质 = "1";
                    this.labelControl2.Text = "是";
                    this.sp2.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    is阳虚质 = "";
                    this.labelControl2.Text = "";
                    this.sp2.ForeColor = System.Drawing.Color.Black;
                }
                if (jf3 >= 9 && jf3 <= 10)
                {
                    is阴虚质 = "2";
                    this.labelControl3.Text = "倾向是";
                    this.sp3.ForeColor = System.Drawing.Color.Blue;
                }
                else if (jf3 >= 11)
                {
                    is阴虚质 = "1";
                    this.labelControl3.Text = "是";
                    this.sp3.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    is阴虚质 = "";
                    this.labelControl3.Text = "";
                    this.sp3.ForeColor = System.Drawing.Color.Black;
                }
                if (jf4 >= 9 && jf4 <= 10)
                {
                    is痰湿质 = "2";
                    this.labelControl4.Text = "倾向是";
                    this.sp4.ForeColor = System.Drawing.Color.Blue;
                }
                else if (jf4 >= 11)
                {
                    is痰湿质 = "1";
                    this.labelControl4.Text = "是";
                    this.sp4.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    is痰湿质 = "";
                    this.labelControl4.Text = "";
                    this.sp4.ForeColor = System.Drawing.Color.Black;
                }
                if (jf5 >= 9 && jf5 <= 10)
                {
                    is湿热质 = "2";
                    this.labelControl5.Text = "倾向是";
                    this.sp5.ForeColor = System.Drawing.Color.Blue;
                }
                else if (jf5 >= 11)
                {
                    is湿热质 = "1";
                    this.labelControl5.Text = "是";
                    this.sp5.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    is湿热质 = "";
                    this.labelControl5.Text = "";
                    this.sp5.ForeColor = System.Drawing.Color.Black;
                }
                if (jf6 >= 9 && jf6 <= 10)
                {
                    is血瘀质 = "2";
                    this.labelControl6.Text = "倾向是";
                    this.sp6.ForeColor = System.Drawing.Color.Blue;
                }
                else if (jf6 >= 11)
                {
                    is血瘀质 = "1";
                    this.labelControl6.Text = "是";
                    this.sp6.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    is血瘀质 = "";
                    this.labelControl6.Text = "";
                    this.sp6.ForeColor = System.Drawing.Color.Black;
                }
                if (jf7 >= 9 && jf7 <= 10)
                {
                    is气郁质 = "2";
                    this.labelControl7.Text = "倾向是";
                    this.sp7.ForeColor = System.Drawing.Color.Blue;
                }
                else if (jf7 >= 11)
                {
                    is气郁质 = "1";
                    this.labelControl7.Text = "是";
                    this.sp7.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    is气郁质 = "";
                    this.labelControl7.Text = "";
                    this.sp7.ForeColor = System.Drawing.Color.Black;
                }
                if (jf8 >= 9 && jf8 <= 10)
                {
                    is特禀质 = "2";
                    this.labelControl8.Text = "倾向是";
                    this.sp8.ForeColor = System.Drawing.Color.Blue;
                }
                else if (jf8 >= 11)
                {
                    is特禀质 = "1";
                    this.labelControl8.Text = "是";
                    this.sp8.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    is特禀质 = "";
                    this.labelControl8.Text = "";
                    this.sp8.ForeColor = System.Drawing.Color.Black;
                }
                if (jf9 >= 17)
                {
                    if (jf1 < 8 && jf2 < 8 && jf3 < 8 && jf4 < 8 && jf5 < 8 && jf6 < 8 && jf7 < 8 && jf8 < 8)
                    {
                        is平和质 = "1";
                        this.labelControl9.Text = "是";
                        this.sp9.ForeColor = System.Drawing.Color.Red;
                    }
                    else if (jf1 < 10 && jf2 < 10 && jf3 < 10 && jf4 < 10 && jf5 < 10 && jf6 < 10 && jf7 < 10 && jf8 < 10)
                    {
                        is平和质 = "2";
                        this.labelControl9.Text = "基本是";
                        this.sp9.ForeColor = System.Drawing.Color.Blue;
                    }
                    else
                    {
                        is平和质 = "";
                        this.labelControl9.Text = "";
                        this.sp9.ForeColor = System.Drawing.Color.Black;
                    }
                }
                else
                {
                    is平和质 = "";
                    this.labelControl9.Text = "";
                    this.sp9.ForeColor = System.Drawing.Color.Black;
                }
            }
            else
            {
                Clear();
            }
        }
        private void Clear()
        {
            this.textBox1.Text = "";
            this.textBox2.Text = "";
            this.textBox3.Text = "";
            this.textBox4.Text = "";
            this.textBox5.Text = "";
            this.textBox6.Text = "";
            this.textBox7.Text = "";
            this.textBox8.Text = "";
            this.textBox9.Text = "";

            this.labelControl1.Text = "";
            this.labelControl2.Text = "";
            this.labelControl3.Text = "";
            this.labelControl4.Text = "";
            this.labelControl5.Text = "";
            this.labelControl6.Text = "";
            this.labelControl7.Text = "";
            this.labelControl8.Text = "";
            this.labelControl9.Text = "";

            this.sp1.ForeColor = System.Drawing.Color.Black;
            this.sp2.ForeColor = System.Drawing.Color.Black;
            this.sp3.ForeColor = System.Drawing.Color.Black;
            this.sp4.ForeColor = System.Drawing.Color.Black;
            this.sp5.ForeColor = System.Drawing.Color.Black;
            this.sp6.ForeColor = System.Drawing.Color.Black;
            this.sp7.ForeColor = System.Drawing.Color.Black;
            this.sp8.ForeColor = System.Drawing.Color.Black;
            this.sp9.ForeColor = System.Drawing.Color.Black;
        }
        private void dockPanel1_Click(object sender, EventArgs e)
        {

        }
        private void btn上一项_Click(object sender, EventArgs e)
        {
            int index = (int)this.lblQuestion.Tag;
            //SetBtnEnable(index - 1);
            Question lastQues = list.Find(x => x.Index == index - 1);
            SetBtnEnable(lastQues);
            SetRadioValue(lastQues.Score);
            if (index != 0)
                SetLblQuestion(index - 1);

        }
        private void SetLblQuestion(Question question)
        {
            if (question == null) return;
            this.lblQuestion.Text = question.QuestionName;
            this.lblQuestion.Tag = question.Index;
            this.btn上一项.Enabled = false;
        }
        private void SetLblQuestion(int index)
        {
            if (index == 33) return;
            Question question = list.Find(x => x.Index == index);
            this.lblQuestion.Text = question.QuestionName;
            this.lblQuestion.Tag = question.Index;
            //this.btn上一项.Enabled = false;
        }
        private void SetBtnEnable(int index)
        {
            if (index == 0)
            {
                this.btn上一项.Enabled = false;
                this.btn下一项.Enabled = false;
            }
            else if (index == 33)
            {
                this.btn上一项.Enabled = true;
                this.btn下一项.Enabled = false;
            }
            else
            {
                this.btn上一项.Enabled = true;
                //this.btn下一项.Enabled = true;
            }
        }
        private void btn下一项_Click(object sender, EventArgs e)
        {
            int index = (int)this.lblQuestion.Tag;
            Question nextQuestion = list.Find(x => x.Index == index + 1);
            SetBtnEnable(nextQuestion);
            SetRadioValue(nextQuestion.Score);
            if (index != 32)
                SetLblQuestion(index + 1);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="score"></param>
        private void SetRadioValue(int score)
        {
            this.radioGroup1.SelectedIndexChanged -= this.radioGroup1_SelectedIndexChanged;
            this.radioGroup1.EditValue = score;
            this.radioGroup1.SelectedIndexChanged += this.radioGroup1_SelectedIndexChanged;

            //switch (score)
            //{
            //    case 1:
            //        this.radioButton1.Checked = true;
            //        break;
            //    case 2:
            //        this.radioButton2.Checked = true;
            //        break;
            //    case 3:
            //        this.radioButton3.Checked = true;
            //        break;
            //    case 4:
            //        this.radioButton4.Checked = true;
            //        break;
            //    case 5:
            //        this.radioButton5.Checked = true;
            //        break;
            //    default:
            //        break;
            //}
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (fun必填项检查())
            {
                //if (_UpdateType == UpdateType.Add)//添加
                //{
                #region tb_老年人中医药特征管理
                //DataRow row = _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows.Add();
                //row[tb_老年人中医药特征管理.身份证号] = this.txt身份证号.Text.Trim();
                //row[tb_老年人中医药特征管理.姓名] = this.txt姓名.Text.Trim();
                //row[tb_老年人中医药特征管理.性别] = this.txt性别.Text.Trim();
                //row[tb_老年人中医药特征管理.居住地址] = this.txt居住地址.Text.Trim();
                //row[tb_老年人中医药特征管理.出生日期] = this.txt出生日期.Text.Trim();
                //tb_老年人中医药特征管理Info tab_体质辨识 = new tb_老年人中医药特征管理Info();
                //currentUser.Tab_中医辨识体质 = tab_体质辨识;

                tab_体质辨识.身份证号 = currentUser.ID;
                tab_体质辨识.姓名 = currentUser.Name;
                tab_体质辨识.性别 = currentUser.Gender;
                tab_体质辨识.出生日期 = currentUser.Birth;
                tab_体质辨识.居住地址 = currentUser.Addr;
                tab_体质辨识.气虚质辨识 = is气虚质;
                tab_体质辨识.气虚质得分 = jf1;
                //tab_体质辨识.气虚质其他 = this.txt1.Text.Trim();
                //tab_体质辨识.气虚质指导 = ControlsHelper.GetFlowLayoutResult(flow1).ToString();

                tab_体质辨识.阳虚质辨识 = is阳虚质;
                tab_体质辨识.阳虚质得分 = jf2;
                //tab_体质辨识.阳虚质其他 = this.txt2.Text.Trim();
                //tab_体质辨识.阳虚质指导 = ControlsHelper.GetFlowLayoutResult(flow2).ToString();

                tab_体质辨识.阴虚质辨识 = is阴虚质;
                tab_体质辨识.阴虚质得分 = jf3;
                //tab_体质辨识.阴虚质其他 = this.txt3.Text.Trim();
                //tab_体质辨识.阴虚质指导 = ControlsHelper.GetFlowLayoutResult(flow3).ToString();

                tab_体质辨识.痰湿质辨识 = is痰湿质;
                tab_体质辨识.痰湿质得分 = jf4;
                //tab_体质辨识.痰湿质其他 = this.txt4.Text.Trim();
                //tab_体质辨识.痰湿质指导 = ControlsHelper.GetFlowLayoutResult(flow4).ToString();

                tab_体质辨识.湿热质辨识 = is湿热质;
                tab_体质辨识.湿热质得分 = jf5;
                //tab_体质辨识.湿热质其他 = this.txt5.Text.Trim();
                //tab_体质辨识.湿热质指导 = ControlsHelper.GetFlowLayoutResult(flow5).ToString();

                tab_体质辨识.血瘀质辨识 = is血瘀质;
                tab_体质辨识.血瘀质得分 = jf6;
                //tab_体质辨识.血瘀质其他 = this.txt6.Text.Trim();
                //tab_体质辨识.血瘀质指导 = ControlsHelper.GetFlowLayoutResult(flow6).ToString();

                tab_体质辨识.气郁质辨识 = is气郁质;
                tab_体质辨识.气郁质得分 = jf7;
                //tab_体质辨识.气郁质其他 = this.txt7.Text.Trim();
                //tab_体质辨识.气郁质指导 = ControlsHelper.GetFlowLayoutResult(flow7).ToString();

                tab_体质辨识.特禀质辨识 = is特禀质;
                tab_体质辨识.特禀质得分 = jf8;
                //tab_体质辨识.特禀质其他 = this.txt8.Text.Trim();
                //tab_体质辨识.特禀质指导 = ControlsHelper.GetFlowLayoutResult(flow8).ToString();

                tab_体质辨识.平和质辨识 = is平和质;
                tab_体质辨识.平和质得分 = jf9;
                //tab_体质辨识.平和质其他 = this.txt9.Text.Trim();
                //tab_体质辨识.平和质指导 = ControlsHelper.GetFlowLayoutResult(flow9).ToString();

                tab_体质辨识.特征1 = t1;
                tab_体质辨识.特征2 = t2;
                tab_体质辨识.特征3 = t3;
                tab_体质辨识.特征4 = t4;
                tab_体质辨识.特征5 = t5;
                tab_体质辨识.特征6 = t6;
                tab_体质辨识.特征7 = t7;
                tab_体质辨识.特征8 = t8;
                tab_体质辨识.特征9 = t9;
                tab_体质辨识.特征10 = t10;
                tab_体质辨识.特征11 = t11;
                tab_体质辨识.特征12 = t12;
                tab_体质辨识.特征13 = t13;
                tab_体质辨识.特征14 = t14;
                tab_体质辨识.特征15 = t15;
                tab_体质辨识.特征16 = t16;
                tab_体质辨识.特征17 = t17;
                tab_体质辨识.特征18 = t18;
                tab_体质辨识.特征19 = t19;
                tab_体质辨识.特征20 = t20;
                tab_体质辨识.特征21 = t21;
                tab_体质辨识.特征22 = t22;
                tab_体质辨识.特征23 = t23;
                tab_体质辨识.特征24 = t24;
                tab_体质辨识.特征25 = t25;
                tab_体质辨识.特征26 = t26;
                tab_体质辨识.特征27 = t27;
                tab_体质辨识.特征28 = t28;
                tab_体质辨识.特征29 = t29;
                tab_体质辨识.特征30 = t30;
                tab_体质辨识.特征31 = t31;
                tab_体质辨识.特征32 = t32;
                tab_体质辨识.特征33 = t33;

                //TODO:保存修改人等
                //row[tb_老年人中医药特征管理.创建机构] = Loginer.CurrentUser.所属机构;
                //row[tb_老年人中医药特征管理.创建人] = Loginer.CurrentUser.用户编码;
                //row[tb_老年人中医药特征管理.修改人] = Loginer.CurrentUser.用户编码;
                //row[tb_老年人中医药特征管理.所属机构] = Loginer.CurrentUser.所属机构;
                tab_体质辨识.创建时间 = Program._currentTime;
                tab_体质辨识.修改时间 = Program._currentTime;
                tab_体质辨识.发生时间 = this.dte填表日期.Text.Trim();
                tab_体质辨识.医生签名 = this.txt医生签名.Text.Trim();

                #endregion
                //bool result = Save(currentUser);
                bool result = tb_老年人中医药特征管理DAL.Addtb_老年人中医药特征管理(tab_体质辨识);
                if (result)
                {
                    MessageBox.Show("保存数据成功！");
                    this.Close();
                }
            }
        }
        private bool fun必填项检查()
        {
            if (string.IsNullOrEmpty(this.dte填表日期.Text.Trim()))
            {
                MessageBox.Show("填表日期必填项，请确认！");
                this.txt医生签名.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(this.txt医生签名.Text.Trim()))
            {
                MessageBox.Show("医生签名是必填项，请确认！");
                this.txt医生签名.Focus();
                return false;
            }
            return true;
        }
        /// <summary>
        /// 指导按钮功能
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn指导_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            if (btn == null) return;
            SetBtn指导(btn);
            int index = Convert.ToInt32(btn.Tag);
            EnableFlowCtrl(true);



            int directIndex = Convert.ToInt32(this.flow指导.Tag);
            this.flow指导.Tag = index;
            //flow的tag不等于btn的tag时，证明是点击了另外体质的按钮，需要先进行保存
            if (directIndex != 0 && directIndex != index)
            {
                string zhidao = ControlsHelper.GetFlowLayoutResult(flow指导).ToString();
                switch (directIndex)
                {
                    case 1:
                        tab_体质辨识.气虚质其他 = this.txt指导.Text.Trim();
                        tab_体质辨识.气虚质指导 = zhidao;
                        break;
                    case 2:
                        tab_体质辨识.阳虚质其他 = this.txt指导.Text.Trim();
                        tab_体质辨识.阳虚质指导 = zhidao;
                        break;
                    case 3:
                        tab_体质辨识.阴虚质其他 = this.txt指导.Text.Trim();
                        tab_体质辨识.阴虚质指导 = zhidao;
                        break;
                    case 4:
                        tab_体质辨识.痰湿质其他 = this.txt指导.Text.Trim();
                        tab_体质辨识.痰湿质指导 = zhidao;
                        break;
                    case 5:
                        tab_体质辨识.湿热质其他 = this.txt指导.Text.Trim();
                        tab_体质辨识.湿热质指导 = zhidao;
                        break;
                    case 6:
                        tab_体质辨识.血瘀质其他 = this.txt指导.Text.Trim();
                        tab_体质辨识.血瘀质指导 = zhidao;
                        break;
                    case 7:
                        tab_体质辨识.气郁质其他 = this.txt指导.Text.Trim();
                        tab_体质辨识.气郁质指导 = zhidao;
                        break;
                    case 8:
                        tab_体质辨识.特禀质其他 = this.txt指导.Text.Trim();
                        tab_体质辨识.特禀质指导 = zhidao;
                        break;
                    case 9:
                        tab_体质辨识.平和质其他 = this.txt指导.Text.Trim();
                        tab_体质辨识.平和质指导 = zhidao;
                        break;
                    default:
                        break;
                }
            }
            SetFlowResult(index);

            //ClearFlowResult();//情况

        }

        private void SetBtn指导(SimpleButton btn)
        {
            int tag = Convert.ToInt32(btn.Tag);
            for (int i = 0; i < this.layoutControl1.Controls.Count; i++)
            {
                if (this.layoutControl1.Controls[i].GetType() == typeof(SimpleButton))
                {
                    SimpleButton simpleButton = (SimpleButton)this.layoutControl1.Controls[i];
                    if (simpleButton == null) continue;
                    if (simpleButton.Tag == null) continue;
                    int _tag = Convert.ToInt32(simpleButton.Tag);
                    if (_tag != tag)
                        simpleButton.ForeColor = System.Drawing.Color.Black;
                }
            }
            btn.Appearance.ForeColor = System.Drawing.Color.DarkRed;
        }
        private void EnableFlowCtrl(bool isTrue)
        {
            this.checkEdit1.Enabled = this.checkEdit2.Enabled = this.checkEdit3.Enabled = this.checkEdit4.Enabled = this.checkEdit5.Enabled = this.checkEdit6.Enabled = this.txt指导.Enabled = isTrue;
        }
        /// <summary>
        /// 根据index设置flow的值
        /// </summary>
        /// <param name="index"></param>
        private void SetFlowResult(int index)
        {
            switch (index)
            {
                case 1:
                    ControlsHelper.SetFlowLayoutResult(tab_体质辨识.气虚质指导, flow指导);
                    this.txt指导.Text = tab_体质辨识.气虚质其他;
                    break;
                case 2:
                    ControlsHelper.SetFlowLayoutResult(tab_体质辨识.阳虚质指导, flow指导);
                    this.txt指导.Text = tab_体质辨识.阳虚质其他;
                    break;
                case 3:
                    ControlsHelper.SetFlowLayoutResult(tab_体质辨识.阴虚质指导, flow指导);
                    this.txt指导.Text = tab_体质辨识.阴虚质其他;
                    break;
                case 4:
                    ControlsHelper.SetFlowLayoutResult(tab_体质辨识.痰湿质指导, flow指导);
                    this.txt指导.Text = tab_体质辨识.痰湿质其他;
                    break;
                case 5:
                    ControlsHelper.SetFlowLayoutResult(tab_体质辨识.湿热质指导, flow指导);
                    this.txt指导.Text = tab_体质辨识.湿热质其他;
                    break;
                case 6:
                    ControlsHelper.SetFlowLayoutResult(tab_体质辨识.血瘀质指导, flow指导);
                    this.txt指导.Text = tab_体质辨识.血瘀质其他;
                    break;
                case 7:
                    ControlsHelper.SetFlowLayoutResult(tab_体质辨识.气郁质指导, flow指导);
                    this.txt指导.Text = tab_体质辨识.气郁质其他;
                    break;
                case 8:
                    ControlsHelper.SetFlowLayoutResult(tab_体质辨识.特禀质指导, flow指导);
                    this.txt指导.Text = tab_体质辨识.特禀质其他;
                    break;
                case 9:
                    ControlsHelper.SetFlowLayoutResult(tab_体质辨识.平和质指导, flow指导);
                    this.txt指导.Text = tab_体质辨识.平和质其他;
                    break;
                default:
                    break;
            }

        }
        private void ClearFlowResult()
        {
            this.checkEdit1.Checked = this.checkEdit2.Checked = this.checkEdit3.Checked = this.checkEdit4.Checked = this.checkEdit5.Checked = this.checkEdit6.Checked = false;
            this.txt指导.Text = "";
        }
        private void radioGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //RadioButton radio = (RadioButton)sender;
            //if (radio == null) return;
            int questionIndex = (int)this.lblQuestion.Tag;
            int tag = (int)this.radioGroup1.EditValue;
            Question currentQues = list.Find(x => x.Index == questionIndex);
            currentQues.Score = tag;
            #region 变量赋值
            switch (questionIndex)
            {
                case 0:
                    this.t1 = tag;
                    break;
                case 1:
                    this.t2 = tag;
                    break;
                case 2:
                    this.t3 = tag;
                    break;
                case 3:
                    this.t4 = tag;
                    break;
                case 4:
                    this.t5 = tag;
                    break;
                case 5:
                    this.t6 = tag;
                    break;
                case 6:
                    this.t7 = tag; break;
                case 7:
                    this.t8 = tag; break;
                case 8:
                    this.t9 = tag; break;
                case 9:
                    this.t10 = tag; break;
                case 10:
                    this.t11 = tag; break;
                case 11:
                    this.t12 = tag; break;
                case 12:
                    this.t13 = tag; break;
                case 13:
                    this.t14 = tag; break;
                case 14:
                    this.t15 = tag; break;
                case 15:
                    this.t16 = tag; break;
                case 16:
                    this.t17 = tag; break;
                case 17:
                    this.t18 = tag; break;
                case 18:
                    this.t19 = tag; break;
                case 19:
                    this.t20 = tag; break;
                case 20:
                    this.t21 = tag; break;
                case 21:
                    this.t22 = tag; break;
                case 22:
                    this.t23 = tag; break;
                case 23:
                    this.t24 = tag; break;
                case 24:
                    this.t25 = tag; break;
                case 25:
                    this.t26 = tag; break;
                case 26:
                    this.t27 = tag; break;
                case 27:
                    this.t28 = tag; break;
                case 28:
                    this.t29 = tag; break;
                case 29:
                    this.t30 = tag; break;
                case 30:
                    this.t31 = tag; break;
                case 31:
                    this.t32 = tag; break;
                case 32:
                    this.t33 = tag; break;
                default:
                    break;
            }

            #endregion

            SetLblQuestion(questionIndex + 1);

            //最后一项时 进行体质计算
            if (questionIndex == 32)
            {
                计算体质();
                this.dockPanel1.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
            }
            else
            {
                Question nextQuestion = list.Find(x => x.Index == questionIndex + 1);
                if (nextQuestion.Score == 0)//表示下一题没有进行作答
                {
                    SetBtnEnable(nextQuestion);
                    this.radioGroup1.SelectedIndexChanged -= this.radioGroup1_SelectedIndexChanged;
                    this.radioGroup1.SelectedIndex = -1;
                    this.radioGroup1.SelectedIndexChanged += this.radioGroup1_SelectedIndexChanged;
                }
                else //下一题已做答
                {
                    this.radioGroup1.SelectedIndexChanged -= this.radioGroup1_SelectedIndexChanged;
                    this.radioGroup1.EditValue = nextQuestion.Score;
                    this.radioGroup1.SelectedIndexChanged += this.radioGroup1_SelectedIndexChanged;
                }

                //btn下一项_Click(null, null);
            }
        }

        private void SetBtnEnable(Question nextQuestion)
        {
            int index = nextQuestion.Index;
            int score = nextQuestion.Score;
            if (index == 0 && score == 0)
            {
                this.btn上一项.Enabled = false;
                this.btn下一项.Enabled = false;
            }
            else if (index == 0 && score != 0)
            {
                this.btn上一项.Enabled = false;
                this.btn下一项.Enabled = true;
            }
            else if (index != 0 && score == 0)
            {
                this.btn上一项.Enabled = true;
                this.btn下一项.Enabled = false;
            }
            else if (index == 32)
                this.btn下一项.Enabled = false;
            else
            {
                this.btn上一项.Enabled = true;
                this.btn下一项.Enabled = true;
            }
        }
    }


    //public class Question1
    //{
    //    int index;
    //    string question;
    //    int score;
    //    public int Index
    //    {
    //        get { return index; }
    //        set { index = value; }
    //    }

    //    public string QuestionName
    //    {
    //        get { return question; }
    //        set { question = value; }
    //    }

    //    public int Score
    //    {
    //        get { return score; }
    //        set { score = value; }
    //    }
    //}
}
