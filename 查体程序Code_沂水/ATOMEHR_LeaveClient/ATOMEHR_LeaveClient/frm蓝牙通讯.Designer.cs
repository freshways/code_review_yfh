﻿namespace ATOMEHR_LeaveClient
{
    partial class frm蓝牙通讯
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PortList = new System.Windows.Forms.ListBox();
            this.button连接 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // PortList
            // 
            this.PortList.FormattingEnabled = true;
            this.PortList.ItemHeight = 12;
            this.PortList.Location = new System.Drawing.Point(32, 34);
            this.PortList.Name = "PortList";
            this.PortList.Size = new System.Drawing.Size(178, 376);
            this.PortList.TabIndex = 0;
            this.PortList.SelectedIndexChanged += new System.EventHandler(this.PortList_SelectedIndexChanged);
            // 
            // button连接
            // 
            this.button连接.Location = new System.Drawing.Point(280, 34);
            this.button连接.Name = "button连接";
            this.button连接.Size = new System.Drawing.Size(150, 23);
            this.button连接.TabIndex = 1;
            this.button连接.Text = "连接蓝牙";
            this.button连接.UseVisualStyleBackColor = true;
            this.button连接.Click += new System.EventHandler(this.button连接_Click);
            // 
            // frm蓝牙通讯
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 455);
            this.Controls.Add(this.button连接);
            this.Controls.Add(this.PortList);
            this.Name = "frm蓝牙通讯";
            this.Text = "frm蓝牙通讯";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox PortList;
        private System.Windows.Forms.Button button连接;
    }
}