﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout;

namespace ATOMEHR_LeaveClient
{
    public partial class frm老年人生活自理能力评估表 : Form
    {

        #region Fields
        string _docNo;
        string _serverDateTime;
        string _id;//表的主键，通过id来查询一条数据
        User currentUser;
        #endregion

        public frm老年人生活自理能力评估表()
        {
            InitializeComponent();
        }

        public frm老年人生活自理能力评估表(User _currentUser)
        {
            InitializeComponent();
            currentUser = _currentUser;
            this.txt出生日期.Text = currentUser.Birth;
            this.txt居住地址.Text = currentUser.Addr;
            this.txt身份证号.Text = currentUser.ID;
            this.txt性别.Text = currentUser.Gender;
            this.txt姓名.Text = currentUser.Name;
            tb_老年人生活自理能力评价Info tb = tb_老年人生活自理能力评价DAL.GetInfoById(currentUser.ID);
            if (null != tb)
            {
                BindSuiFangData(tb);
            }
        }
        //public UC老年人生活自理能力评估表(Form frm, UpdateType _updateType)
        //{
        //    InitializeComponent();
        //    _frm = (frm个人健康)frm;
        //    _docNo = _frm._docNo;
        //    _serverDateTime = _Bll.ServiceDateTime;
        //    _UpdateType = _updateType;
        //    _id = _frm._param as string;
        //    if (_UpdateType == UpdateType.Add)//添加随访
        //    {
        //        _Bll.GetBusinessByKey(_docNo, true);
        //        //_Bll.NewBusiness();
        //        _ds老年人随访 = _Bll.CurrentBusiness;
        //    }
        //    else if (_UpdateType == UpdateType.Modify)
        //    {
        //        _ds老年人随访 = _Bll.GetOneDataByKey(_docNo, _id, true);
        //        this.dte随访日期.Properties.ReadOnly = true;
        //        this.dte下次随访日期.Properties.ReadOnly = true;
        //    }
        //    DoBindingSummaryEditor(_ds老年人随访);//绑定数据
        //}
        //private void DoBindingSummaryEditor(DataSet _ds老年人随访)
        //{
        //    if (_ds老年人随访 == null) return;
        //    if (_ds老年人随访.Tables.Count == 0) return;
        //    DataTable dt老年人信息 = _ds老年人随访.Tables[tb_老年人基本信息.__TableName];
        //    DataTable dt随访 = _ds老年人随访.Tables[tb_老年人随访.__TableName];
        //    if (dt老年人信息.Rows.Count == 1)
        //    {
        //        DataRow dr = dt老年人信息.Rows[0];
        //        this.txt姓名.Text = util.DESEncrypt.DES解密(dr[tb_老年人基本信息.姓名].ToString());
        //        this.txt性别.Text = _BLL.ReturnDis字典显示("xb_xingbie", dr[tb_老年人基本信息.性别].ToString());
        //        this.txt个人档案号.Text = dr[tb_老年人基本信息.个人档案编号].ToString();
        //        this.txt身份证号.Text = dr[tb_老年人基本信息.身份证号].ToString();
        //        this.txt出生日期.Text = dr[tb_老年人基本信息.出生日期].ToString();
        //        this.txt联系电话.Text = dr[tb_老年人基本信息.联系电话].ToString();
        //        this.txt居住地址.Text = _Bll.Return地区名称(dr[tb_老年人基本信息.省].ToString()) + _Bll.Return地区名称(dr[tb_老年人基本信息.市].ToString()) + _Bll.Return地区名称(dr[tb_老年人基本信息.区].ToString()) + _Bll.Return地区名称(dr[tb_老年人基本信息.街道].ToString()) + _Bll.Return地区名称(dr[tb_老年人基本信息.居委会].ToString()) + dr[tb_老年人基本信息.居住地址].ToString();

        //    }
        //    if (dt随访.Rows.Count == 1)
        //    {
        //        BindSuiFangData(dt随访.Rows[0]);
        //    }
        //    if (_UpdateType == UpdateType.Add)
        //    {
        //        this.txt创建人.Text = Loginer.CurrentUser.AccountName;
        //        this.txt创建时间.Text = _serverDateTime;
        //        this.txt最近更新时间.Text = _serverDateTime;
        //        this.txt当前所属机构.Text = Loginer.CurrentUser.所属机构名称;
        //        this.txt创建机构.Text = Loginer.CurrentUser.所属机构名称;
        //        this.txt最近修改人.Text = Loginer.CurrentUser.AccountName;
        //        this.txt录入医生.Text = Loginer.CurrentUser.AccountName;
        //    }
        //    else if (_UpdateType == UpdateType.Modify)
        //    {
        //        this.txt创建人.Text = _Bll.Return用户名称(dt随访.Rows[0][tb_老年人随访.创建人].ToString());
        //        this.txt创建时间.Text = dt随访.Rows[0][tb_老年人随访.创建时间].ToString();
        //        this.txt最近更新时间.Text = _serverDateTime;
        //        this.txt当前所属机构.Text = _Bll.Return机构名称(dt随访.Rows[0][tb_老年人随访.所属机构].ToString());
        //        this.txt创建机构.Text = _Bll.Return机构名称(dt随访.Rows[0][tb_老年人随访.创建机构].ToString());// Loginer.CurrentUser.所属机构名称;
        //        this.txt最近修改人.Text = Loginer.CurrentUser.AccountName; //Loginer.CurrentUser.AccountName;
        //        this.txt录入医生.Text = dt随访.Rows[0][tb_老年人随访.随访医生].ToString();
        //    }
        //}
        private void BindSuiFangData(tb_老年人生活自理能力评价Info dataRow)
        {
            if (dataRow == null) return;
            if (!string.IsNullOrEmpty(dataRow.进餐评分))
            {
                string 进餐评分 = dataRow.进餐评分;
                this.result1.Text = 进餐评分;
                switch (进餐评分)
                {
                    case "0":
                        this.chk11.Checked = true;
                        break;
                    case "3":
                        this.chk13.Checked = true;
                        break;
                    case "5":
                        this.chk14.Checked = true;
                        break;
                    default:
                        break;
                }
            }
            if (!string.IsNullOrEmpty(dataRow.梳洗评分))
            {
                string 梳洗评分 = dataRow.梳洗评分;
                this.result2.Text = 梳洗评分;
                switch (梳洗评分)
                {
                    case "0":
                        this.chk21.Checked = true;
                        break;
                    case "1":
                        this.chk22.Checked = true;
                        break;
                    case "3":
                        this.chk23.Checked = true;
                        break;
                    case "7":
                        this.chk24.Checked = true;
                        break;
                    default:
                        break;
                }
            }
            if (!string.IsNullOrEmpty(dataRow.穿衣评分))
            {
                string 穿衣评分 = dataRow.穿衣评分;
                this.result3.Text = 穿衣评分;
                switch (穿衣评分)
                {
                    case "0":
                        this.chk31.Checked = true;
                        break;
                    case "3":
                        this.chk33.Checked = true;
                        break;
                    case "7":
                        this.chk34.Checked = true;
                        break;
                    default:
                        break;
                }
            }
            if (!string.IsNullOrEmpty(dataRow.如厕评分))
            {
                string 如厕评分 = dataRow.如厕评分;
                this.result4.Text = 如厕评分;
                switch (如厕评分)
                {
                    case "0":
                        this.chk41.Checked = true;
                        break;
                    case "1":
                        this.chk42.Checked = true;
                        break;
                    case "5":
                        this.chk43.Checked = true;
                        break;
                    case "10":
                        this.chk44.Checked = true;
                        break;
                    default:
                        break;
                }
            }
            if (!string.IsNullOrEmpty(dataRow.活动评分))
            {
                string 活动评分 = dataRow.活动评分;
                this.result5.Text = 活动评分;
                switch (活动评分)
                {
                    case "0":
                        this.chk51.Checked = true;
                        break;
                    case "1":
                        this.chk52.Checked = true;
                        break;
                    case "5":
                        this.chk53.Checked = true;
                        break;
                    case "10":
                        this.chk54.Checked = true;
                        break;
                    default:
                        break;
                }
            }

            this.result.Text = string.IsNullOrEmpty(dataRow.总评分.ToString()) ? " " : dataRow.总评分.ToString();

            this.txt下次随访目标.Text = dataRow.下次随访目标.ToString();
            this.dte随访日期.Text = dataRow.随访日期.ToString();
            this.txt随访医生签名.Text = dataRow.随访医生.ToString();
            this.dte下次随访日期.Text = dataRow.下次随访日期.ToString();

        }
        private void UC老年人生活自理能力评估表_Load(object sender, EventArgs e)
        {

        }
        private void result_TextChanged(object sender, EventArgs e)
        {
            int result1 = string.IsNullOrEmpty(this.result1.Text.Trim()) ? 0 : Convert.ToInt32(this.result1.Text.Trim());
            int result2 = string.IsNullOrEmpty(this.result2.Text.Trim()) ? 0 : Convert.ToInt32(this.result2.Text.Trim());
            int result3 = string.IsNullOrEmpty(this.result3.Text.Trim()) ? 0 : Convert.ToInt32(this.result3.Text.Trim());
            int result4 = string.IsNullOrEmpty(this.result4.Text.Trim()) ? 0 : Convert.ToInt32(this.result4.Text.Trim());
            int result5 = string.IsNullOrEmpty(this.result5.Text.Trim()) ? 0 : Convert.ToInt32(this.result5.Text.Trim());

            this.result.Text = Convert.ToString(result1 + result2 + result3 + result4 + result5);
        }
        private void btn保存_Click(object sender, EventArgs e)
        {
            if (check验证())
            {
                //string happentime = this.dte随访日期.Text.Trim();
                ////判断此随访时间 是否已经添加  随访
                //if (!_Bll.isExists(_docNo, happentime))
                //{
                //获取随访次数
                //int sfcs = _Bll.getbndsfcs(_docNo, happentime.Substring(0, 4));
                //if (sfcs == 0) sfcs = 1;
                //else sfcs += 1;
                #region tb_老年人基本信息
                //TODO:上传时更新 老年人下次随访时间 字段
                //_ds老年人随访.Tables[tb_老年人基本信息.__TableName].Rows[0][tb_老年人基本信息.下次随访时间] = this.dte下次随访日期.Text;
                #endregion
                #region tb_老年人随访
                tb_老年人生活自理能力评价Info tb = new tb_老年人生活自理能力评价Info();
                currentUser.Tab_生活自理能力评价 = tb;
                int i = Get缺项();
                int j = Get完整度(i);
                //DataRow row = _ds老年人随访.Tables[tb_老年人随访.__TableName].Rows.Add();
                tb.进餐评分 = this.result1.Text.Trim();
                tb.梳洗评分 = this.result2.Text.Trim();
                tb.穿衣评分 = this.result3.Text.Trim();
                tb.如厕评分 = this.result4.Text.Trim();
                tb.活动评分 = this.result5.Text.Trim();
                tb.总评分 = this.result.Text.Trim();
                tb.下次随访目标 = this.txt下次随访目标.Text.Trim();
                tb.随访日期 = this.dte随访日期.Text;
                tb.随访医生 = this.txt随访医生签名.Text.Trim();
                tb.下次随访日期 = this.dte下次随访日期.Text.Trim();
                //tb.随访次数 = sfcs;
                tb.缺项 = i.ToString();
                tb.完整度 = j.ToString();
                tb.创建时间 = this.txt创建时间.Text;
                tb.更新时间 = this.txt最近更新时间.Text.Trim();
                //tb.所属机构 = Loginer.CurrentUser.所属机构;
                //tb.创建机构 = Loginer.CurrentUser.所属机构;
                //tb.创建人 = Loginer.CurrentUser.用户编码;
                //tb.更新人 = Loginer.CurrentUser.用户编码;
                #endregion
                #region tb_健康档案_个人健康特征
                //TODO:上传时更新 个人健康特征字段
                //_ds老年人随访.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.老年人随访] = i + "," + j;
                #endregion
                //}
                //else
                //{
                //    Msg.Warning("对不起，随访时间为“" + this.dte随访日期.Text.Trim() + "”的随访已经创建，请修改随访时间！");
                //    return;
                //}

                bool result = tb_老年人生活自理能力评价DAL.Save(tb);
                if (result)
                {
                    MessageBox.Show("保存数据成功！");
                    this.Close();
                }
            }

        }


        private int Get缺项()
        {
            int i = 0;
            if (this.result1.Text.Trim() == "") i++;
            if (this.result2.Text.Trim() == "") i++;
            if (this.result3.Text.Trim() == "") i++;
            if (this.result4.Text.Trim() == "") i++;
            if (this.result5.Text.Trim() == "") i++;
            if (this.txt下次随访目标.Text.Trim() == "") i++;
            if (this.dte随访日期.Text.Trim() == "") i++;
            if (this.dte下次随访日期.Text.Trim() == "") i++;
            if (this.txt随访医生签名.Text.Trim() == "") i++;
            return i;
        }
        public int Get完整度(int i)
        {
            int k = Convert.ToInt32((9 - i) * 100 / 9.0);
            return k;
        }
        private bool check验证()
        {
            if (string.IsNullOrEmpty(this.dte随访日期.Text.Trim()))
            {
                MessageBox.Show("随访日期是必填项！");
                //Msg.Warning("随访日期是必填项！");
                this.dte随访日期.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(this.dte下次随访日期.Text.Trim()))
            {
                MessageBox.Show("下次随访日期是必填项！");
                //Msg.Warning("下次随访日期是必填项！");
                this.dte下次随访日期.Focus();
                return false;
            }
            if (this.dte下次随访日期.DateTime <= this.dte随访日期.DateTime)
            {
                MessageBox.Show("下次随访日期填写不能小于本次随访日期！");
                //Msg.Warning("下次随访日期填写不能小于本次随访日期！");
                this.dte下次随访日期.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(result1.Text.Trim()) || string.IsNullOrEmpty(result2.Text.Trim()) || string.IsNullOrEmpty(result3.Text.Trim()) || string.IsNullOrEmpty(result4.Text.Trim()) || string.IsNullOrEmpty(result5.Text.Trim()))
            {
                MessageBox.Show("请将五个评估项全部选择！");
                //Msg.Warning("请将五个评估项全部选择！");
                return false;
            }
            return true;
        }
        private void btn重置_Click(object sender, EventArgs e)
        {

        }
        private void chk_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit chk = (CheckEdit)sender;
            string name = chk.Name;//chk11
            string result = "result";//result
            if (chk.Checked)
            {
                int value = Convert.ToInt32(chk.Tag);
                result += name.Substring(3, 1);
                Control ctrl = this.Controls.Find(result, true)[0];//根据name找到控件
                if (ctrl is LabelControl)
                {
                    ctrl.Text = value.ToString();
                }
            }
        }
    }
}
