﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace serialPort.PortClass
{
    public class PortClass
    {
        private SerialPort _Sp = new SerialPort();  // 初始化串口类

        private string _PortName;
        private int _BaudRate;
        private Parity _Parity;
        private int _DataBits;
        private StopBits _StopBits;

        public StopBits StopBits
        {
            get { return _StopBits; }
            set { _StopBits = value; }
        }
        public int DataBits
        {
            get { return _DataBits; }
            set { _DataBits = value; }
        }
        public Parity Parity
        {
            get { return _Parity; }
            set { _Parity = value; }
        }
        public int BaudRate
        {
            get { return _BaudRate; }
            set { _BaudRate = value; }
        }
        public string PortName
        {
            get { return _PortName; }
            set { _PortName = value; }
        }

        public enum DataType
        {
            bts, // byte
            chs, // char
            str, // string
        }

        private DataType dt;

        public DataType Dt
        {
            get { return dt; }
            set { dt = value; }
        }

        /// <summary>
        /// 打开串口
        /// </summary>
        /// <param name="PortName"></param>
        /// <param name="state"></param>
        /// <param name="message"></param>
        public void OpenPort(out bool state, out string message)
        {
            try
            {
                if (!string.IsNullOrEmpty(PortName))
                {
                    if (_Sp.IsOpen)
                    {
                        state = false;
                        message = "串口已是打开状态";
                    }
                    else
                    {
                        _Sp.PortName = PortName;            // 串口名称
                        _Sp.BaudRate = BaudRate;            // 波特率
                        _Sp.Parity = Parity;                // 校验位
                        _Sp.DataBits = DataBits;            // 数据位
                        _Sp.StopBits = StopBits;            // 停止位
                        //sp.DtrEnable = true;              // 启用控制终端就续信号
                        //sp.RtsEnable = true;              // 启用请求发送信号
                        _Sp.ReceivedBytesThreshold = 1;     // 必须一定要加上这句话，意思是接收缓冲区当中如果有一个字节的话就出发接收函数，如果不加上这句话，那就有时候触发接收有时候都发了好多次了也没有触发接收，有时候延时现象等等
                        _Sp.DiscardNull = true;             // 在添加到序列缓冲区前，是否丢弃接口上接收的空字节
                        _Sp.DataReceived += sp_DataReceived;// 绑定数据接收事件

                        _Sp.Open();

                        state = true;
                        message = "串口打开成功";
                    }
                }
                else
                {
                    state = false;
                    message = "请先输入串口名称";
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                Debug.WriteLine("Exception+++OpenPort:" + ex.Message);
#endif

                state = false;
                message = ex.Message;
            }
        }

        /// <summary>
        /// 关闭串口
        /// </summary>
        /// <param name="state"></param>
        /// <param name="message"></param>
        public void ClosePort(out bool state, out string message)
        {
            try
            {
                if (_Sp.IsOpen)
                {
                    _Sp.Close();    //先关闭
                    _Sp.Dispose();  //在释放(关闭程序就是释放)

                    state = true;
                    message = "串口关闭成功";
                }
                else
                {
                    state = false;
                    message = "串口是未打开状态";
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                Debug.WriteLine("Exception+++ClosePort:" + ex.Message);
#endif


                state = false;
                message = ex.Message;
            }
        }

        /// <summary>
        /// 判断串口是否打开
        /// </summary>
        /// <returns></returns>
        public bool isOpenPort()
        {
            if (_Sp.IsOpen)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string[] GetPortNames()
        {
            return SerialPort.GetPortNames();
        }

        /// <summary>
        /// 串口数据接收事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void sp_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort spDr = sender as SerialPort;

            if (Dt == DataType.bts)
            {
                ReadBts(spDr);
            }
            else if (Dt == DataType.chs)
            {
                ReadChs(spDr);
            }
            else if (Dt == DataType.str)
            {
                ReadStr(spDr);
            }
        }

        /// <summary>
        /// 接收byte数据
        /// </summary>
        /// <param name="spDr"></param>
        void ReadBts(SerialPort spDr)
        {
            int btsLenght = spDr.BytesToRead;
            if (btsLenght > 0)
            {
                byte[] bts = new byte[btsLenght];
                spDr.Read(bts, 0, btsLenght);

                if (bts.Length > 0)
                    ProtData(bts);
            }
        }

        /// <summary>
        /// 接收char数据
        /// </summary>
        /// <param name="spDr"></param>
        void ReadChs(SerialPort spDr)
        {
            int btsLenght = spDr.BytesToRead;
            if (btsLenght > 0)
            {
                char[] chs = new char[btsLenght];
                spDr.Read(chs, 0, btsLenght);

                if (chs.Length > 0)
                    ProtData(chs);
            }
        }

        /// <summary>
        /// 接收string数据
        /// </summary>
        /// <param name="spDr"></param>
        void ReadStr(SerialPort spDr)
        {
            int btsLenght = spDr.BytesToRead;
            if (btsLenght > 0)
            {
                string str = spDr.ReadExisting();

                if (!string.IsNullOrEmpty(str))
                    ProtData(str);
            }
        }

        protected virtual void ProtData(byte[] bts) { }
        protected virtual void ProtData(char[] chs) { }
        protected virtual void ProtData(string str) { }

        /// <summary>
        /// 向串口发送数据(byte[])
        /// </summary>
        /// <param name="bts"></param>
        protected void portWrite(byte[] bts)
        {
            _Sp.Write(bts, 0, bts.Length);
#if DEBUG
            Debug.WriteLine("发送数据完成");
#endif
        }

        /// <summary>
        /// 向串口发送数据(char[])
        /// </summary>
        /// <param name="bts"></param>
        protected void portWrite(char[] chs)
        {
            _Sp.Write(chs, 0, chs.Length);
#if DEBUG
            Debug.WriteLine("发送数据完成");
#endif
        }

        /// <summary>
        /// 向串口发送数据(string)
        /// </summary>
        /// <param name="bts"></param>
        protected void portWrite(string str)
        {
            _Sp.Write(str);
#if DEBUG
            Debug.WriteLine("发送数据完成");
#endif
        }

        /// <summary>
        /// 十六进制字符串转byte数组
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        protected byte[] HexStringToBytes(string str)
        {
            string[] strs = str.Split('-');
            byte[] bts = new byte[strs.Length];
            for (int i = 0; i < strs.Length; i++)
            {
                bts[i] = Convert.ToByte(strs[i], 16);
            }
            return bts;
        }

        /// <summary>
        /// byte数组转十六进制字符串
        /// </summary>
        /// <param name="bts"></param>
        /// <returns></returns>
        protected string BytesToHexString(byte[] bts)
        {
            string[] strs = new string[bts.Length];
            for (int i = 0; i < bts.Length; i++)
            {
                strs[i] = Convert.ToString(bts[i], 16);
            }
            return string.Join("-", strs);
        }

        /// <summary>
        /// byte数组转十进制字符串
        /// </summary>
        /// <param name="bts"></param>
        /// <returns></returns>
        protected string BytesToIntString(byte[] bts)
        {
            string[] strs = new string[bts.Length];
            for (int i = 0; i < bts.Length; i++)
            {
                strs[i] = Convert.ToInt32(bts[i]).ToString();
            }
            return string.Join("-", strs);
        }
    }
}
