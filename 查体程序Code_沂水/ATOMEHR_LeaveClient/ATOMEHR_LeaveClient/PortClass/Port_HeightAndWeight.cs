﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace serialPort.PortClass
{
    public class Port_HeightAndWeight : PortClass
    {
        public struct HWInfo
        {
            public string strState;
            public double dHeight;
            public double dWeight;
        }

        public delegate void deResponse(HWInfo hwi);
        public event deResponse evResponse;
        public Port_HeightAndWeight()
        {
            ProtParameter();
        }

        private void ProtParameter()
        {
            base.BaudRate = 9600;
            base.Parity = System.IO.Ports.Parity.None;
            base.DataBits = 8;
            base.StopBits = System.IO.Ports.StopBits.One;
            base.Dt = PortClass.DataType.chs;
        }

        List<char> buffer = new List<char>();
        protected override void ProtData(char[] chs)
        {
            if (chs.Length == 3 && chs[2] == '$')
            {
                Debug.WriteLine(string.Join("", chs));

                //Task.Run(() => responseAsync(chs));
                Task.Factory.StartNew(() => responseAsync(chs));
            }
            else
            {
                buffer.AddRange(chs);

                if ((buffer[buffer.Count - 1] == '\n') && buffer[0] == 'W')
                {
                    Debug.WriteLine(string.Join("", buffer.ToArray<char>()));

                    char[] chsResult = buffer.ToArray();
                    //Task.Run(() => responseAsync(chsResult));
                    Task.Factory.StartNew(() => responseAsync(chsResult));
                    buffer.Clear();
                }
            }
        }

        public void HW_isOnLine()
        {
            string strOnline = "10$";
            portWrite(strOnline);
        }

        public void HW_Start()
        {
            string strOnline = "11$";
            portWrite(strOnline);
        }

        /// <summary>
        /// 单次应答解析
        /// </summary>
        /// <param name="responseStr">应答字符串</param>
        /// <param name="resultBool">是否应答成功</param>
        /// <param name="resultMessage">返回信息</param>
        private void responseAsync(char[] chs)
        {
            HWInfo hwi = new HWInfo();
            if (chs[2] == '$')
            {
                if (chs[0] == '6')
                {
                    hwi.strState = "身高体重秤在线";
                    hwi.dHeight = default(double);
                    hwi.dWeight = default(double);
                }
                else if (chs[0] == '1')
                {
                    hwi.strState = "身高体重秤手动测量开始";
                    hwi.dHeight = default(double);
                    hwi.dWeight = default(double);
                }
                else
                {
                    hwi.strState = "无状态$";
                    hwi.dHeight = default(double);
                    hwi.dWeight = default(double);
                }
            }
            else if (chs[0] == 'W')
            {
                hwi.strState = "测量成功";

                double dWeight = Convert.ToDouble(string.Join("", chs.Skip(2).Take(5)));
                double dHeight = Convert.ToDouble(string.Join("", chs.Skip(10).Take(5)));
                hwi.dWeight = dWeight;
                hwi.dHeight = dHeight;

                Debug.WriteLine("测量成功->" + "  身高:" + dHeight + "  体重:" + dWeight);
            }
            else
            {
                hwi.strState = "无状态HW";
                hwi.dHeight = default(double);
                hwi.dWeight = default(double);
            }

            if (evResponse != null)
            {
                evResponse(hwi);
            }
        }
    }
}
