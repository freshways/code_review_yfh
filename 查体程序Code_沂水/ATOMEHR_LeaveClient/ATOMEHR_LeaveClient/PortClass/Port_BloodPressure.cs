﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace serialPort.PortClass
{
    public class Port_BloodPressure : PortClass
    {
        /// <summary>
        /// 血压计状态
        /// </summary>
        private enum State
        {
            ON,        // 连接血压计
            OFF,       // 关闭血压计
            Start,     // 启动测量
            Stop,      // 停止测量
            AutoStop,  // 测量完毕
        }

        private State _State;

        private Queue<byte[]> _QeBts = new Queue<byte[]>(); // 容纳测量中及结束时的测量数据

        string strResponse = Convert.ToByte("0x03", 16).ToString(); // 返回值中第四位等于0x03的为应答状态
        string strContinuousDP = Convert.ToByte("0x0f", 16).ToString(); // 返回值中第四位等于0x0f的为测量结果

        private List<byte> lsResponse = new List<byte>(); // 应答状态返回值

        public struct DPInfo
        {
            public int iSBP;           // 收缩压
            public int iDBP;           // 舒张压
            public int iPulse;         // 脉搏
            public string strDateTime; //时间
        }

        public Port_BloodPressure() 
        {
            ProtParameter();
        }

        /// <summary>
        /// 串口参数
        /// </summary>
        private void ProtParameter()
        {
            base.BaudRate = 115200;
            base.Parity = System.IO.Ports.Parity.None;
            base.DataBits = 8;
            base.StopBits = System.IO.Ports.StopBits.One;
        }

        protected override void ProtData(byte[] bts)
        {
            if (bts[3].ToString() == strResponse)
            {
                lsResponse = bts.ToList<byte>();
#if DEBUG
                Debug.WriteLine("应答数据");
#endif
            }
            else
            {
#if DEBUG
                if (bts[3].ToString() == strContinuousDP)
                    Debug.WriteLine("测量结果数据");
                else
                    Debug.WriteLine("测量连续数据");
#endif
                _QeBts.Enqueue(bts);

            }
        }

        /// <summary>
        /// 连接血压计
        /// </summary>
        public void BP_ON(out bool reBool, out string reMessage)
        {
            _State = State.ON;
#if DEBUG
            Debug.WriteLine("连接血压计Begin");
#endif

            string on = "0xCC-0x80-0x03-0x03-0x01-0x01-0x00-0x00";
            portWrite(HexStringToBytes(on));

            string onSuccess = "AA-80-03-03-01-01";
            string onFailed = "AA-80-03-03-01-01-01-01";
            Tuple<bool, string> tResult = responseResult(onSuccess, onFailed);
            reBool = tResult.Item1;
            reMessage = tResult.Item2;
#if DEBUG
            Debug.WriteLine("连接血压计End");
#endif
        }
        /// <summary>
        /// 关机
        /// </summary>
        public void BP_OFF(out bool reBool, out string reMessage)
        {
            _State = State.OFF;
#if DEBUG
            Debug.WriteLine("关机Begin");
#endif

            string off = "0xCC-0x80-0x03-0x03-0x01-0x04-0x00-0x05";
            portWrite(HexStringToBytes(off));

            string offSuccess = "AA-80-03-03-01-04-05";
            string offFailed = "AA-80-03-03-01-04-01-04";
            Tuple<bool, string> tResult = responseResult(offSuccess, offFailed);
            reBool = tResult.Item1;
            reMessage = tResult.Item2;
#if DEBUG
            Debug.WriteLine("关机End");
#endif
        }
        /// <summary>
        /// 启动测量
        /// </summary>
        public void BP_Start(out bool reBool, out string reMessage, Action<DPInfo> action)
        {
            _State = State.Start;
#if DEBUG
            Debug.WriteLine("启动测量Begin");
#endif

            string start = "0xCC-0x80-0x03-0x03-0x01-0x02-0x00-0x03";
            portWrite(HexStringToBytes(start));

            string startSuccess = "AA-80-03-03-01-02-03";
            string startFailed = "AA-80-03-03-01-02-01-02";
            Tuple<bool, string> tResult = responseResult(startSuccess, startFailed);
            reBool = tResult.Item1;
            reMessage = tResult.Item2;

            //Task.Run(() => continuousDPAsync(action));
            Task.Factory.StartNew(() => continuousDPAsync(action));
#if DEBUG
            Debug.WriteLine("启动测量End");
#endif
        }

        /// <summary>
        /// 停止测量
        /// </summary>
        public void BP_Stop(out bool reBool, out string reMessage)
        {
            _State = State.Stop;
#if DEBUG
            Debug.WriteLine("停止测量Begin");
#endif

            if (_QeBts.Count > 0)
            {
                _QeBts.Clear();
            }
            string stop = "0xCC-0x80-0x03-0x03-0x01-0x03-0x00-0x02";
            portWrite(HexStringToBytes(stop));

            string stopSuccess = "AA-80-03-03-01-03-02";
            string stopFailed = "AA-80-03-03-01-03-01-03";
            Tuple<bool, string> tResult = responseResult(stopSuccess, stopFailed);
            reBool = tResult.Item1;
            reMessage = tResult.Item2;
#if DEBUG
            Debug.WriteLine("停止测量End");
#endif
        }

        /// <summary>
        /// 单次应答解析
        /// </summary>
        /// <param name="responseStr">应答字符串</param>
        /// <param name="resultBool">是否应答成功</param>
        /// <param name="resultMessage">返回信息</param>
        private Tuple<bool, string> responseResult(string responseSuccess, string responseFailed)
        {
#if DEBUG
            Debug.WriteLine("应答解析");
#endif

            Stopwatch watch = new Stopwatch();
            watch.Start(); // 开始检测执行时间
            while (lsResponse == null || lsResponse.Count == 0) // 如果集合没有数据就一直监听直到有数据或者超时
            {
#if DEBUG
#else
                if (watch.IsRunning && watch.ElapsedMilliseconds > 2000) // 如果执行超过2000ms集合中还没有数据就视为超时
                {
                    watch.Reset(); // 停止执行时间测量并重置

                    return Tuple.Create<bool, string>(false, "应答超时");
                }
#endif
            }
#if DEBUG
            Debug.WriteLine("应答Time:" + watch.ElapsedMilliseconds);
#endif
            watch.Reset();

            byte[] btsSuccess = HexStringToBytes(responseSuccess); // 成功应答字符串解析为byte[]
            byte[] btsFailed = HexStringToBytes(responseFailed);   // 失败应答字符串解析为byte[]
            byte[] btsTemp = lsResponse.ToArray<byte>();          // 应答返回值
            lsResponse.Clear();

            if (Enumerable.SequenceEqual(btsSuccess, btsTemp))
            {
#if DEBUG
                Debug.WriteLine("指令应答成功:" + BytesToHexString(btsTemp));
#endif
                return Tuple.Create<bool, string>(true, "指令应答成功");
            }

            if (Enumerable.SequenceEqual(btsFailed, btsTemp))
            {
#if DEBUG
                Debug.WriteLine("指令应答失败:" + BytesToHexString(btsTemp));
#endif
                return Tuple.Create<bool, string>(false, "指令应答失败");
            }
#if DEBUG
            Debug.WriteLine("返回值无匹配");
#endif
            return Tuple.Create<bool, string>(false, "返回值无匹配");
        }

        /// <summary>
        /// 连续应答解析(时时血压)
        /// </summary>
        /// <returns></returns>
        private Tuple<bool, string> continuousDPAsync(Action<DPInfo> action)
        {
#if DEBUG

            Debug.WriteLine("测量解析");
#endif

            if (isTimeout(3000))
            {
                return Tuple.Create<bool, string>(false, "应答超时");
            }

            byte[] btsTemp = new byte[0];

            while (_State == State.Start)
            {
                if (_QeBts.Count > 0)
                {
                    byte[] bts = _QeBts.Dequeue();
                    btsTemp = bts;
                }
                else
                {
                    DPInfo DP = new DPInfo();

                    if (btsTemp.Length > 0 && btsTemp[3].ToString() == strContinuousDP)
                    {
                        _State = State.AutoStop;

                        if (!compareCheckCode(btsTemp))
                            return Tuple.Create<bool, string>(false, "结果校验值不匹配");

                        DP.iSBP = Convert.ToInt32(btsTemp[13]);
                        DP.iDBP = Convert.ToInt32(btsTemp[14]);
                        DP.iPulse = Convert.ToInt32(btsTemp[15]);
                        DP.strDateTime = Convert.ToDateTime((Convert.ToInt32(btsTemp[7]) + "-" + Convert.ToInt32(btsTemp[8]) + "-" + Convert.ToInt32(btsTemp[9]) + " " + Convert.ToInt32(btsTemp[10]) + ":" + Convert.ToInt32(btsTemp[11]) + ":" + Convert.ToInt32(btsTemp[12]))).ToString("yyyy-MM-dd HH:mm:ss");
#if DEBUG
                        Debug.WriteLine("测量结束:" + BytesToIntString(btsTemp));
                        Debug.WriteLine("收缩压:" + DP.iSBP + "  舒张压:" + DP.iDBP + "  脉搏:" + DP.iPulse + "  时间:" + DP.strDateTime);

                        action(DP);
#endif
                        return Tuple.Create<bool, string>(true, "测量结束");
                    }
                    else
                    {
                        if (!compareCheckCode(btsTemp))
                            return Tuple.Create<bool, string>(false, "时时校验值不匹配");

                        DP.iSBP = Convert.ToInt32(btsTemp[6]);
#if DEBUG
                        Debug.WriteLine("测量中..:" + BytesToIntString(btsTemp));
                        Debug.WriteLine("收缩压:" + DP.iSBP);
#endif
                        action(DP);
                    }

                    if (isTimeout(500))
                    {
#if DEBUG
                        Debug.WriteLine("测量超时");
#endif
                        return Tuple.Create<bool, string>(false, "返回超时");
                    }
                }
            }
#if DEBUG
            Debug.WriteLine("未进入测量状态");
#endif
            return Tuple.Create<bool, string>(false, "未进入测量状态");
        }

        /// <summary>
        /// 队列没有数据的时候进入等待或超时
        /// </summary>
        /// <returns></returns>
        private bool isTimeout(long lTimeout)
        {
            Stopwatch watch = new Stopwatch();
            watch.Start(); // 开始检测执行时间Timeout
            while (_QeBts.Count == 0) // 如果队列没有数据就一直监听直到有数据或者超时
            {
#if DEBUG
#else
                if (_State == State.Start && watch.IsRunning && watch.ElapsedMilliseconds > lTimeout) // 如果执行超过lTimeout ms队列中还没有数据就视为超时
                {
                    watch.Reset(); // 停止执行时间测量并重置
                    return true;
                }
#endif
                // 如果是手动停止测量需要停止计时,否则再次启动会超时(测量超时,实际正常)
                if (_State == State.Stop)
                {
#if DEBUG
                    Debug.WriteLine("isTimeout - if - Stop");
#endif

                    watch.Reset();
                    return false;
                }
            }
#if DEBUG
            Debug.WriteLine("测量Time:" + watch.ElapsedMilliseconds);
#endif
            watch.Reset();
            return false;
        }

        /// <summary>
        /// 验证返回值与校验位是否匹配
        /// </summary>
        /// <param name="bts"></param>
        /// <returns></returns>
        private bool compareCheckCode(byte[] bts)
        {
            byte xor = bts[2];
            for (int i = 3; i < bts.Length - 1; i++)
            {
                xor ^= bts[i];
            }
            if (xor != bts[bts.Length - 1])
            {
#if DEBUG
                Debug.WriteLine("异或校验值:" + xor + "  实际校验值" + bts[bts.Length - 1]);
#endif
                return false;
            }

            return true;
        }
    }
}
