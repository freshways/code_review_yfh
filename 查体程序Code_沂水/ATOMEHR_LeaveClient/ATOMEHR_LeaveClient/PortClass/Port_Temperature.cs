﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace serialPort.PortClass
{
    public class Port_Temperature : PortClass
    {
        public struct TeInfo
        {
            public double dTemperature;
            public string strState;
        }

        public delegate void deResponse(TeInfo ti);
        public event deResponse evResponse;

        public Port_Temperature()
        {
            ProtParameter();
        }

        /// <summary>
        /// 串口参数
        /// </summary>
        private void ProtParameter()
        {
            base.BaudRate = 4800;
            base.Parity = System.IO.Ports.Parity.None;
            base.DataBits = 8;
            base.StopBits = System.IO.Ports.StopBits.One;
        }

        List<byte> buffer = new List<byte>();
        protected override void ProtData(byte[] bts)
        {
            buffer.AddRange(bts);

            // 当缓存两个字节的时候开始判断,
            // 如果这两个字节是0 255那么这是开机第一次测量,
            // 如果这两个字节是255 255,那么这是休眠唤醒后的测量,第一个255是唤醒的标识(体温枪休眠后第一次按测量键),
            // 第二个255是正式测量(体温枪休眠后第二次按测量键(休眠唤醒后的第一次测量))后数据的的起始位
            if (buffer.Count == 2)
            {
                byte[] btsTemp = buffer.ToArray();

                // 开机后的第一次测量返回0 255
                if (btsTemp[0] == 0 && btsTemp[1] == 255)
                {
                    buffer.Clear();
                }

                // 休眠唤醒标识,返回一个255,如果两个都是255则是唤醒之后的第一次测量
                if (btsTemp[0] == btsTemp[1])
                {
                    // 移除唤醒返回标识(第一个255),第二个255和接下来的3位组成有效数据
                    buffer.RemoveAt(0);
                }
            }

            // 正常测量数据是4bit,起始位是255
            if (buffer.Count == 4 && buffer.First() == 255)
            {
                byte[] btsResult = buffer.ToArray();
                //Task.Run(() => responseAsync(btsResult));
                Task.Factory.StartNew(() => responseAsync(btsResult));

                buffer.Clear();
            }
        }

        private void responseAsync(byte[] btsResult)
        {
            TeInfo ti = new TeInfo();
            if ((btsResult[1] ^ btsResult[2]) == btsResult[3])
            {
                ti.dTemperature = (btsResult[1] * Math.Pow(16, 2) + btsResult[2]) / 10.0;
                ti.strState = "测量成功";

                Console.WriteLine("体温:" + ti.dTemperature);
            }
            else
            {
                ti.dTemperature = default(double);
                ti.strState = "结果校验值不匹配";
            }

            if (evResponse != null)
            {
                evResponse(ti);
            }
        }
    }
}
