﻿using AtomEHR.Library.UserControls;
using DevExpress.XtraEditors;
namespace ATOMEHR_LeaveClient
{
    partial class uc健康体检表1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc健康体检表1));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btn下一页 = new DevExpress.XtraEditors.SimpleButton();
            this.btn上一页 = new DevExpress.XtraEditors.SimpleButton();
            this.txt职业病其他防护 = new DevExpress.XtraEditors.TextEdit();
            this.txt职业病其他 = new DevExpress.XtraEditors.TextEdit();
            this.txt化学防护措施 = new DevExpress.XtraEditors.TextEdit();
            this.txt化学因素 = new DevExpress.XtraEditors.TextEdit();
            this.txt物理防护措施 = new DevExpress.XtraEditors.TextEdit();
            this.txt物理因素 = new DevExpress.XtraEditors.TextEdit();
            this.radio职业病其他防护措施有无 = new DevExpress.XtraEditors.RadioGroup();
            this.txt放射物质防护措施 = new DevExpress.XtraEditors.TextEdit();
            this.txt放射物质 = new DevExpress.XtraEditors.TextEdit();
            this.radio化学防护措施有无 = new DevExpress.XtraEditors.RadioGroup();
            this.radio物理防护措施有无 = new DevExpress.XtraEditors.RadioGroup();
            this.txt粉尘防护措施 = new DevExpress.XtraEditors.TextEdit();
            this.txt粉尘 = new DevExpress.XtraEditors.TextEdit();
            this.radio放射物质防护措施有无 = new DevExpress.XtraEditors.RadioGroup();
            this.radio粉尘防护措施有无 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio职业病有无 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.txt工种 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.txt从业时间 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.flow老年人认知能力 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio老年人认知能力 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.txt老年人认知能力总分 = new DevExpress.XtraEditors.TextEdit();
            this.radio老年人生活自理能力评估 = new DevExpress.XtraEditors.RadioGroup();
            this.flow老年人情感状态 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio老年人情感状态 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.txt老年人情感状态总分 = new DevExpress.XtraEditors.TextEdit();
            this.radio老年人健康状态评估 = new DevExpress.XtraEditors.RadioGroup();
            this.flow饮酒种类 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk饮酒种类_白酒 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮酒种类_啤酒 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮酒种类_红酒 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮酒种类_黄酒 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮酒种类_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt饮酒种类_其他 = new DevExpress.XtraEditors.TextEdit();
            this.radio近一年内是否曾醉酒 = new DevExpress.XtraEditors.RadioGroup();
            this.txt开始饮酒年龄 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt戒酒年龄 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.radio是否戒酒 = new DevExpress.XtraEditors.RadioGroup();
            this.txt日饮酒量 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.radio饮酒频率 = new DevExpress.XtraEditors.RadioGroup();
            this.radio吸烟情况 = new DevExpress.XtraEditors.RadioGroup();
            this.txt戒烟年龄 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt开始吸烟年龄 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt日吸烟量 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.flow饮食习惯 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk饮食习惯_荤素均衡 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮食习惯_荤食为主 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮食习惯_素食为主 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮食习惯_嗜盐 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮食习惯_嗜油 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮食习惯_嗜糖 = new DevExpress.XtraEditors.CheckEdit();
            this.txt生活方式_锻炼方式 = new DevExpress.XtraEditors.TextEdit();
            this.txt生活方式_坚持锻炼时间 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt生活方式_每次锻炼时间 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.radio生活方式_锻炼频率 = new DevExpress.XtraEditors.RadioGroup();
            this.flow症状 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk症状_无症状 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_头痛 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_头晕 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_心悸 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_胸闷 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_胸痛 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_慢性咳嗽 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_咳痰 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_呼吸困难 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_多饮 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_多尿 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_体重下降 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_乏力 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_关节肿痛 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_视力模糊 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_手脚麻木 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_尿急 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_尿痛 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_便秘 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_腹泻 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_恶心呕吐 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_眼花 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_耳鸣 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_乳房胀痛 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt症状_其他 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.TCG_Tags = new DevExpress.XtraLayout.TabbedControlGroup();
            this.LCG_职业病 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem129 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl粉尘 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl放射物质 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl物理因素 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem135 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl职业病其他 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem138 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem136 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem131 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl防护措施 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem130 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem134 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem133 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem139 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem137 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem132 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LCG_症状 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LCG_老年人评估 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.group老年人评估 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem128 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LCG_体育锻炼 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup19 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl锻炼频率 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LCG_饮食习惯 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lbl饮食习惯 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LCG_吸烟情况 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LCG_饮酒情况 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.lookUpEdit_医师签字 = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem_医师签字 = new DevExpress.XtraLayout.LayoutControlItem();
            this.flowLayoutPanel53 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.sBut_Get参照数据 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业病其他防护.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业病其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt化学防护措施.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt化学因素.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt物理防护措施.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt物理因素.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio职业病其他防护措施有无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt放射物质防护措施.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt放射物质.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio化学防护措施有无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio物理防护措施有无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt粉尘防护措施.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt粉尘.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio放射物质防护措施有无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio粉尘防护措施有无.Properties)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio职业病有无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt工种.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt从业时间.Properties)).BeginInit();
            this.flow老年人认知能力.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio老年人认知能力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt老年人认知能力总分.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio老年人生活自理能力评估.Properties)).BeginInit();
            this.flow老年人情感状态.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio老年人情感状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt老年人情感状态总分.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio老年人健康状态评估.Properties)).BeginInit();
            this.flow饮酒种类.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮酒种类_白酒.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮酒种类_啤酒.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮酒种类_红酒.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮酒种类_黄酒.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮酒种类_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt饮酒种类_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio近一年内是否曾醉酒.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否戒酒.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio饮酒频率.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio吸烟情况.Properties)).BeginInit();
            this.flow饮食习惯.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_荤素均衡.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_荤食为主.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_素食为主.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_嗜盐.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_嗜油.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_嗜糖.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt生活方式_锻炼方式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio生活方式_锻炼频率.Properties)).BeginInit();
            this.flow症状.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_无症状.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_头痛.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_头晕.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_心悸.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_胸闷.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_胸痛.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_慢性咳嗽.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_咳痰.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_呼吸困难.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_多饮.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_多尿.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_体重下降.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_乏力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_关节肿痛.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_视力模糊.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_手脚麻木.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_尿急.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_尿痛.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_便秘.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_腹泻.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_恶心呕吐.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_眼花.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_耳鸣.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_乳房胀痛.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt症状_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TCG_Tags)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_职业病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem129)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl粉尘)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl放射物质)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl物理因素)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem135)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl职业病其他)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem138)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem136)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem131)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl防护措施)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem130)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem134)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem133)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem139)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem137)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem132)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_症状)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_老年人评估)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group老年人评估)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem128)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_体育锻炼)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl锻炼频率)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_饮食习惯)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl饮食习惯)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_吸烟情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_饮酒情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_医师签字.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_医师签字)).BeginInit();
            this.flowLayoutPanel53.SuspendLayout();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btn下一页);
            this.layoutControl1.Controls.Add(this.btn上一页);
            this.layoutControl1.Controls.Add(this.txt职业病其他防护);
            this.layoutControl1.Controls.Add(this.txt职业病其他);
            this.layoutControl1.Controls.Add(this.txt化学防护措施);
            this.layoutControl1.Controls.Add(this.txt化学因素);
            this.layoutControl1.Controls.Add(this.txt物理防护措施);
            this.layoutControl1.Controls.Add(this.txt物理因素);
            this.layoutControl1.Controls.Add(this.radio职业病其他防护措施有无);
            this.layoutControl1.Controls.Add(this.txt放射物质防护措施);
            this.layoutControl1.Controls.Add(this.txt放射物质);
            this.layoutControl1.Controls.Add(this.radio化学防护措施有无);
            this.layoutControl1.Controls.Add(this.radio物理防护措施有无);
            this.layoutControl1.Controls.Add(this.txt粉尘防护措施);
            this.layoutControl1.Controls.Add(this.txt粉尘);
            this.layoutControl1.Controls.Add(this.radio放射物质防护措施有无);
            this.layoutControl1.Controls.Add(this.radio粉尘防护措施有无);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel1);
            this.layoutControl1.Controls.Add(this.flow老年人认知能力);
            this.layoutControl1.Controls.Add(this.radio老年人生活自理能力评估);
            this.layoutControl1.Controls.Add(this.flow老年人情感状态);
            this.layoutControl1.Controls.Add(this.radio老年人健康状态评估);
            this.layoutControl1.Controls.Add(this.flow饮酒种类);
            this.layoutControl1.Controls.Add(this.radio近一年内是否曾醉酒);
            this.layoutControl1.Controls.Add(this.txt开始饮酒年龄);
            this.layoutControl1.Controls.Add(this.txt戒酒年龄);
            this.layoutControl1.Controls.Add(this.radio是否戒酒);
            this.layoutControl1.Controls.Add(this.txt日饮酒量);
            this.layoutControl1.Controls.Add(this.radio饮酒频率);
            this.layoutControl1.Controls.Add(this.radio吸烟情况);
            this.layoutControl1.Controls.Add(this.txt戒烟年龄);
            this.layoutControl1.Controls.Add(this.txt开始吸烟年龄);
            this.layoutControl1.Controls.Add(this.txt日吸烟量);
            this.layoutControl1.Controls.Add(this.flow饮食习惯);
            this.layoutControl1.Controls.Add(this.txt生活方式_锻炼方式);
            this.layoutControl1.Controls.Add(this.txt生活方式_坚持锻炼时间);
            this.layoutControl1.Controls.Add(this.txt生活方式_每次锻炼时间);
            this.layoutControl1.Controls.Add(this.radio生活方式_锻炼频率);
            this.layoutControl1.Controls.Add(this.flow症状);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 44);
            this.layoutControl1.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(566, 240, 617, 651);
            this.layoutControl1.OptionsFocus.EnableAutoTabOrder = false;
            this.layoutControl1.OptionsView.AllowHotTrack = true;
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(748, 313);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btn下一页
            // 
            this.btn下一页.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.btn下一页.Appearance.Options.UseFont = true;
            this.btn下一页.Image = ((System.Drawing.Image)(resources.GetObject("btn下一页.Image")));
            this.btn下一页.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btn下一页.Location = new System.Drawing.Point(375, 276);
            this.btn下一页.Name = "btn下一页";
            this.btn下一页.Size = new System.Drawing.Size(368, 32);
            this.btn下一页.StyleController = this.layoutControl1;
            this.btn下一页.TabIndex = 60;
            this.btn下一页.Text = "下一页";
            this.btn下一页.Click += new System.EventHandler(this.btn下一页_Click);
            // 
            // btn上一页
            // 
            this.btn上一页.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.btn上一页.Appearance.Options.UseFont = true;
            this.btn上一页.Image = ((System.Drawing.Image)(resources.GetObject("btn上一页.Image")));
            this.btn上一页.Location = new System.Drawing.Point(5, 276);
            this.btn上一页.Name = "btn上一页";
            this.btn上一页.Size = new System.Drawing.Size(366, 32);
            this.btn上一页.StyleController = this.layoutControl1;
            this.btn上一页.TabIndex = 59;
            this.btn上一页.Text = "上一页";
            this.btn上一页.Click += new System.EventHandler(this.btn上一页_Click);
            // 
            // txt职业病其他防护
            // 
            this.txt职业病其他防护.Enabled = false;
            this.txt职业病其他防护.Location = new System.Drawing.Point(568, 233);
            this.txt职业病其他防护.Name = "txt职业病其他防护";
            this.txt职业病其他防护.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            this.txt职业病其他防护.Properties.Appearance.Options.UseFont = true;
            this.txt职业病其他防护.Properties.AutoHeight = false;
            this.txt职业病其他防护.Size = new System.Drawing.Size(172, 36);
            this.txt职业病其他防护.StyleController = this.layoutControl1;
            this.txt职业病其他防护.TabIndex = 58;
            // 
            // txt职业病其他
            // 
            this.txt职业病其他.Enabled = false;
            this.txt职业病其他.Location = new System.Drawing.Point(63, 233);
            this.txt职业病其他.Name = "txt职业病其他";
            this.txt职业病其他.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            this.txt职业病其他.Properties.Appearance.Options.UseFont = true;
            this.txt职业病其他.Properties.AutoHeight = false;
            this.txt职业病其他.Size = new System.Drawing.Size(253, 36);
            this.txt职业病其他.StyleController = this.layoutControl1;
            this.txt职业病其他.TabIndex = 56;
            // 
            // txt化学防护措施
            // 
            this.txt化学防护措施.Enabled = false;
            this.txt化学防护措施.Location = new System.Drawing.Point(568, 193);
            this.txt化学防护措施.Name = "txt化学防护措施";
            this.txt化学防护措施.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            this.txt化学防护措施.Properties.Appearance.Options.UseFont = true;
            this.txt化学防护措施.Properties.AutoHeight = false;
            this.txt化学防护措施.Size = new System.Drawing.Size(172, 36);
            this.txt化学防护措施.StyleController = this.layoutControl1;
            this.txt化学防护措施.TabIndex = 55;
            // 
            // txt化学因素
            // 
            this.txt化学因素.Enabled = false;
            this.txt化学因素.Location = new System.Drawing.Point(63, 193);
            this.txt化学因素.Name = "txt化学因素";
            this.txt化学因素.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            this.txt化学因素.Properties.Appearance.Options.UseFont = true;
            this.txt化学因素.Properties.AutoHeight = false;
            this.txt化学因素.Size = new System.Drawing.Size(253, 36);
            this.txt化学因素.StyleController = this.layoutControl1;
            this.txt化学因素.TabIndex = 53;
            // 
            // txt物理防护措施
            // 
            this.txt物理防护措施.Enabled = false;
            this.txt物理防护措施.Location = new System.Drawing.Point(568, 153);
            this.txt物理防护措施.Name = "txt物理防护措施";
            this.txt物理防护措施.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            this.txt物理防护措施.Properties.Appearance.Options.UseFont = true;
            this.txt物理防护措施.Properties.AutoHeight = false;
            this.txt物理防护措施.Size = new System.Drawing.Size(172, 36);
            this.txt物理防护措施.StyleController = this.layoutControl1;
            this.txt物理防护措施.TabIndex = 52;
            // 
            // txt物理因素
            // 
            this.txt物理因素.Enabled = false;
            this.txt物理因素.Location = new System.Drawing.Point(63, 153);
            this.txt物理因素.Name = "txt物理因素";
            this.txt物理因素.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            this.txt物理因素.Properties.Appearance.Options.UseFont = true;
            this.txt物理因素.Properties.AutoHeight = false;
            this.txt物理因素.Size = new System.Drawing.Size(253, 36);
            this.txt物理因素.StyleController = this.layoutControl1;
            this.txt物理因素.TabIndex = 51;
            // 
            // radio职业病其他防护措施有无
            // 
            this.radio职业病其他防护措施有无.EditValue = "1";
            this.radio职业病其他防护措施有无.Enabled = false;
            this.radio职业病其他防护措施有无.Location = new System.Drawing.Point(375, 233);
            this.radio职业病其他防护措施有无.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio职业病其他防护措施有无.Name = "radio职业病其他防护措施有无";
            this.radio职业病其他防护措施有无.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "有")});
            this.radio职业病其他防护措施有无.Size = new System.Drawing.Size(189, 36);
            this.radio职业病其他防护措施有无.StyleController = this.layoutControl1;
            this.radio职业病其他防护措施有无.TabIndex = 57;
            this.radio职业病其他防护措施有无.SelectedIndexChanged += new System.EventHandler(this.radio职业病其他防护措施有无_SelectedIndexChanged);
            // 
            // txt放射物质防护措施
            // 
            this.txt放射物质防护措施.Enabled = false;
            this.txt放射物质防护措施.Location = new System.Drawing.Point(568, 123);
            this.txt放射物质防护措施.Name = "txt放射物质防护措施";
            this.txt放射物质防护措施.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            this.txt放射物质防护措施.Properties.Appearance.Options.UseFont = true;
            this.txt放射物质防护措施.Properties.AutoHeight = false;
            this.txt放射物质防护措施.Size = new System.Drawing.Size(172, 26);
            this.txt放射物质防护措施.StyleController = this.layoutControl1;
            this.txt放射物质防护措施.TabIndex = 49;
            // 
            // txt放射物质
            // 
            this.txt放射物质.Enabled = false;
            this.txt放射物质.Location = new System.Drawing.Point(63, 123);
            this.txt放射物质.Name = "txt放射物质";
            this.txt放射物质.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            this.txt放射物质.Properties.Appearance.Options.UseFont = true;
            this.txt放射物质.Properties.AutoHeight = false;
            this.txt放射物质.Size = new System.Drawing.Size(253, 26);
            this.txt放射物质.StyleController = this.layoutControl1;
            this.txt放射物质.TabIndex = 47;
            // 
            // radio化学防护措施有无
            // 
            this.radio化学防护措施有无.EditValue = "1";
            this.radio化学防护措施有无.Enabled = false;
            this.radio化学防护措施有无.Location = new System.Drawing.Point(375, 193);
            this.radio化学防护措施有无.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio化学防护措施有无.Name = "radio化学防护措施有无";
            this.radio化学防护措施有无.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "有")});
            this.radio化学防护措施有无.Size = new System.Drawing.Size(189, 36);
            this.radio化学防护措施有无.StyleController = this.layoutControl1;
            this.radio化学防护措施有无.TabIndex = 54;
            this.radio化学防护措施有无.SelectedIndexChanged += new System.EventHandler(this.radio化学物质防护措施有无_SelectedIndexChanged);
            // 
            // radio物理防护措施有无
            // 
            this.radio物理防护措施有无.EditValue = "1";
            this.radio物理防护措施有无.Enabled = false;
            this.radio物理防护措施有无.Location = new System.Drawing.Point(375, 153);
            this.radio物理防护措施有无.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio物理防护措施有无.Name = "radio物理防护措施有无";
            this.radio物理防护措施有无.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "有")});
            this.radio物理防护措施有无.Size = new System.Drawing.Size(189, 36);
            this.radio物理防护措施有无.StyleController = this.layoutControl1;
            this.radio物理防护措施有无.TabIndex = 12;
            this.radio物理防护措施有无.SelectedIndexChanged += new System.EventHandler(this.radio物理因素防护措施有无_SelectedIndexChanged);
            // 
            // txt粉尘防护措施
            // 
            this.txt粉尘防护措施.Enabled = false;
            this.txt粉尘防护措施.Location = new System.Drawing.Point(568, 87);
            this.txt粉尘防护措施.Name = "txt粉尘防护措施";
            this.txt粉尘防护措施.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            this.txt粉尘防护措施.Properties.Appearance.Options.UseFont = true;
            this.txt粉尘防护措施.Properties.AutoHeight = false;
            this.txt粉尘防护措施.Size = new System.Drawing.Size(172, 32);
            this.txt粉尘防护措施.StyleController = this.layoutControl1;
            this.txt粉尘防护措施.TabIndex = 46;
            // 
            // txt粉尘
            // 
            this.txt粉尘.Enabled = false;
            this.txt粉尘.Location = new System.Drawing.Point(63, 87);
            this.txt粉尘.Name = "txt粉尘";
            this.txt粉尘.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            this.txt粉尘.Properties.Appearance.Options.UseFont = true;
            this.txt粉尘.Properties.AutoHeight = false;
            this.txt粉尘.Size = new System.Drawing.Size(253, 32);
            this.txt粉尘.StyleController = this.layoutControl1;
            this.txt粉尘.TabIndex = 44;
            // 
            // radio放射物质防护措施有无
            // 
            this.radio放射物质防护措施有无.EditValue = "1";
            this.radio放射物质防护措施有无.Enabled = false;
            this.radio放射物质防护措施有无.Location = new System.Drawing.Point(375, 123);
            this.radio放射物质防护措施有无.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio放射物质防护措施有无.Name = "radio放射物质防护措施有无";
            this.radio放射物质防护措施有无.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "有")});
            this.radio放射物质防护措施有无.Size = new System.Drawing.Size(189, 26);
            this.radio放射物质防护措施有无.StyleController = this.layoutControl1;
            this.radio放射物质防护措施有无.TabIndex = 48;
            this.radio放射物质防护措施有无.SelectedIndexChanged += new System.EventHandler(this.radio放射物质防护措施有无_SelectedIndexChanged);
            // 
            // radio粉尘防护措施有无
            // 
            this.radio粉尘防护措施有无.EditValue = "1";
            this.radio粉尘防护措施有无.Enabled = false;
            this.radio粉尘防护措施有无.Location = new System.Drawing.Point(375, 87);
            this.radio粉尘防护措施有无.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio粉尘防护措施有无.Name = "radio粉尘防护措施有无";
            this.radio粉尘防护措施有无.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "有")});
            this.radio粉尘防护措施有无.Size = new System.Drawing.Size(189, 32);
            this.radio粉尘防护措施有无.StyleController = this.layoutControl1;
            this.radio粉尘防护措施有无.TabIndex = 45;
            this.radio粉尘防护措施有无.SelectedIndexChanged += new System.EventHandler(this.radio粉尘防护措施有无_SelectedIndexChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel1.Controls.Add(this.radio职业病有无);
            this.flowLayoutPanel1.Controls.Add(this.labelControl38);
            this.flowLayoutPanel1.Controls.Add(this.txt工种);
            this.flowLayoutPanel1.Controls.Add(this.labelControl39);
            this.flowLayoutPanel1.Controls.Add(this.txt从业时间);
            this.flowLayoutPanel1.Controls.Add(this.labelControl40);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(8, 38);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(732, 45);
            this.flowLayoutPanel1.TabIndex = 43;
            // 
            // radio职业病有无
            // 
            this.radio职业病有无.EditValue = "1";
            this.radio职业病有无.Location = new System.Drawing.Point(3, 0);
            this.radio职业病有无.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio职业病有无.Name = "radio职业病有无";
            this.radio职业病有无.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.radio职业病有无.Properties.Appearance.Options.UseFont = true;
            this.radio职业病有无.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "有")});
            this.radio职业病有无.Size = new System.Drawing.Size(87, 33);
            this.radio职业病有无.TabIndex = 0;
            this.radio职业病有无.SelectedIndexChanged += new System.EventHandler(this.radion职业病有无_SelectedIndexChanged);
            // 
            // labelControl38
            // 
            this.labelControl38.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelControl38.Location = new System.Drawing.Point(96, 3);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(42, 17);
            this.labelControl38.TabIndex = 7;
            this.labelControl38.Text = "（工种";
            // 
            // txt工种
            // 
            this.txt工种.Enabled = false;
            this.txt工种.Location = new System.Drawing.Point(144, 0);
            this.txt工种.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt工种.Name = "txt工种";
            this.txt工种.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txt工种.Properties.Appearance.Options.UseFont = true;
            this.txt工种.Size = new System.Drawing.Size(100, 22);
            this.txt工种.TabIndex = 1;
            // 
            // labelControl39
            // 
            this.labelControl39.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelControl39.Location = new System.Drawing.Point(250, 3);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(56, 17);
            this.labelControl39.TabIndex = 9;
            this.labelControl39.Text = "从业时间";
            // 
            // txt从业时间
            // 
            this.txt从业时间.Enabled = false;
            this.txt从业时间.Location = new System.Drawing.Point(312, 0);
            this.txt从业时间.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt从业时间.Name = "txt从业时间";
            this.txt从业时间.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txt从业时间.Properties.Appearance.Options.UseFont = true;
            this.txt从业时间.Size = new System.Drawing.Size(100, 22);
            this.txt从业时间.TabIndex = 2;
            // 
            // labelControl40
            // 
            this.labelControl40.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelControl40.Location = new System.Drawing.Point(418, 3);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(28, 17);
            this.labelControl40.TabIndex = 11;
            this.labelControl40.Text = "年）";
            // 
            // flow老年人认知能力
            // 
            this.flow老年人认知能力.BackColor = System.Drawing.Color.White;
            this.flow老年人认知能力.Controls.Add(this.radio老年人认知能力);
            this.flow老年人认知能力.Controls.Add(this.labelControl36);
            this.flow老年人认知能力.Controls.Add(this.txt老年人认知能力总分);
            this.flow老年人认知能力.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.flow老年人认知能力.Location = new System.Drawing.Point(93, 154);
            this.flow老年人认知能力.Name = "flow老年人认知能力";
            this.flow老年人认知能力.Size = new System.Drawing.Size(647, 38);
            this.flow老年人认知能力.TabIndex = 27;
            // 
            // radio老年人认知能力
            // 
            this.radio老年人认知能力.Location = new System.Drawing.Point(3, 5);
            this.radio老年人认知能力.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.radio老年人认知能力.Name = "radio老年人认知能力";
            this.radio老年人认知能力.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.radio老年人认知能力.Properties.Appearance.Options.UseFont = true;
            this.radio老年人认知能力.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "阴性"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "阳性")});
            this.radio老年人认知能力.Size = new System.Drawing.Size(140, 34);
            this.radio老年人认知能力.TabIndex = 0;
            this.radio老年人认知能力.SelectedIndexChanged += new System.EventHandler(this.radio老年人认知能力_SelectedIndexChanged);
            // 
            // labelControl36
            // 
            this.labelControl36.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.labelControl36.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl36.Location = new System.Drawing.Point(149, 7);
            this.labelControl36.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(263, 27);
            this.labelControl36.TabIndex = 1;
            this.labelControl36.Text = "     简易智力状态检查,总分   ";
            // 
            // txt老年人认知能力总分
            // 
            this.txt老年人认知能力总分.Enabled = false;
            this.txt老年人认知能力总分.Location = new System.Drawing.Point(418, 5);
            this.txt老年人认知能力总分.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.txt老年人认知能力总分.Name = "txt老年人认知能力总分";
            this.txt老年人认知能力总分.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txt老年人认知能力总分.Properties.Appearance.Options.UseFont = true;
            this.txt老年人认知能力总分.Properties.AutoHeight = false;
            this.txt老年人认知能力总分.Size = new System.Drawing.Size(104, 29);
            this.txt老年人认知能力总分.TabIndex = 1;
            // 
            // radio老年人生活自理能力评估
            // 
            this.radio老年人生活自理能力评估.Location = new System.Drawing.Point(93, 79);
            this.radio老年人生活自理能力评估.Name = "radio老年人生活自理能力评估";
            this.radio老年人生活自理能力评估.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.radio老年人生活自理能力评估.Properties.Appearance.Options.UseFont = true;
            this.radio老年人生活自理能力评估.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "可自理(0～3分)"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "轻度依赖(4～8分)"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "中度依赖(9～18分)"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "不能自理(≥19分)")});
            this.radio老年人生活自理能力评估.Size = new System.Drawing.Size(647, 71);
            this.radio老年人生活自理能力评估.StyleController = this.layoutControl1;
            this.radio老年人生活自理能力评估.TabIndex = 26;
            // 
            // flow老年人情感状态
            // 
            this.flow老年人情感状态.BackColor = System.Drawing.Color.White;
            this.flow老年人情感状态.Controls.Add(this.radio老年人情感状态);
            this.flow老年人情感状态.Controls.Add(this.labelControl37);
            this.flow老年人情感状态.Controls.Add(this.txt老年人情感状态总分);
            this.flow老年人情感状态.Location = new System.Drawing.Point(93, 196);
            this.flow老年人情感状态.Name = "flow老年人情感状态";
            this.flow老年人情感状态.Size = new System.Drawing.Size(647, 38);
            this.flow老年人情感状态.TabIndex = 28;
            // 
            // radio老年人情感状态
            // 
            this.radio老年人情感状态.Location = new System.Drawing.Point(3, 5);
            this.radio老年人情感状态.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.radio老年人情感状态.Name = "radio老年人情感状态";
            this.radio老年人情感状态.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.radio老年人情感状态.Properties.Appearance.Options.UseFont = true;
            this.radio老年人情感状态.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "阴性"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "阳性")});
            this.radio老年人情感状态.Size = new System.Drawing.Size(140, 34);
            this.radio老年人情感状态.TabIndex = 0;
            this.radio老年人情感状态.SelectedIndexChanged += new System.EventHandler(this.radio老年人情感状态_SelectedIndexChanged);
            // 
            // labelControl37
            // 
            this.labelControl37.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.labelControl37.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl37.Location = new System.Drawing.Point(149, 7);
            this.labelControl37.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(263, 32);
            this.labelControl37.TabIndex = 1;
            this.labelControl37.Text = "     老年人抑郁评分检查,总分   ";
            // 
            // txt老年人情感状态总分
            // 
            this.txt老年人情感状态总分.Enabled = false;
            this.txt老年人情感状态总分.Location = new System.Drawing.Point(418, 5);
            this.txt老年人情感状态总分.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.txt老年人情感状态总分.Name = "txt老年人情感状态总分";
            this.txt老年人情感状态总分.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txt老年人情感状态总分.Properties.Appearance.Options.UseFont = true;
            this.txt老年人情感状态总分.Properties.AutoHeight = false;
            this.txt老年人情感状态总分.Size = new System.Drawing.Size(104, 34);
            this.txt老年人情感状态总分.TabIndex = 1;
            // 
            // radio老年人健康状态评估
            // 
            this.radio老年人健康状态评估.AutoSizeInLayoutControl = true;
            this.radio老年人健康状态评估.Location = new System.Drawing.Point(93, 38);
            this.radio老年人健康状态评估.Name = "radio老年人健康状态评估";
            this.radio老年人健康状态评估.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.radio老年人健康状态评估.Properties.Appearance.Options.UseFont = true;
            this.radio老年人健康状态评估.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "满意"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "基本满意"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "说不清楚"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "不太满意"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("5", "不满意")});
            this.radio老年人健康状态评估.Size = new System.Drawing.Size(647, 37);
            this.radio老年人健康状态评估.StyleController = this.layoutControl1;
            this.radio老年人健康状态评估.TabIndex = 25;
            // 
            // flow饮酒种类
            // 
            this.flow饮酒种类.BackColor = System.Drawing.Color.White;
            this.flow饮酒种类.Controls.Add(this.chk饮酒种类_白酒);
            this.flow饮酒种类.Controls.Add(this.chk饮酒种类_啤酒);
            this.flow饮酒种类.Controls.Add(this.chk饮酒种类_红酒);
            this.flow饮酒种类.Controls.Add(this.chk饮酒种类_黄酒);
            this.flow饮酒种类.Controls.Add(this.chk饮酒种类_其他);
            this.flow饮酒种类.Controls.Add(this.txt饮酒种类_其他);
            this.flow饮酒种类.Location = new System.Drawing.Point(113, 215);
            this.flow饮酒种类.Margin = new System.Windows.Forms.Padding(0);
            this.flow饮酒种类.Name = "flow饮酒种类";
            this.flow饮酒种类.Size = new System.Drawing.Size(627, 54);
            this.flow饮酒种类.TabIndex = 42;
            // 
            // chk饮酒种类_白酒
            // 
            this.chk饮酒种类_白酒.Enabled = false;
            this.chk饮酒种类_白酒.Location = new System.Drawing.Point(3, 3);
            this.chk饮酒种类_白酒.Name = "chk饮酒种类_白酒";
            this.chk饮酒种类_白酒.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk饮酒种类_白酒.Properties.Appearance.Options.UseFont = true;
            this.chk饮酒种类_白酒.Properties.Caption = "白酒";
            this.chk饮酒种类_白酒.Size = new System.Drawing.Size(77, 23);
            this.chk饮酒种类_白酒.TabIndex = 0;
            this.chk饮酒种类_白酒.Tag = "1";
            // 
            // chk饮酒种类_啤酒
            // 
            this.chk饮酒种类_啤酒.Enabled = false;
            this.chk饮酒种类_啤酒.Location = new System.Drawing.Point(86, 3);
            this.chk饮酒种类_啤酒.Name = "chk饮酒种类_啤酒";
            this.chk饮酒种类_啤酒.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk饮酒种类_啤酒.Properties.Appearance.Options.UseFont = true;
            this.chk饮酒种类_啤酒.Properties.Caption = "啤酒";
            this.chk饮酒种类_啤酒.Size = new System.Drawing.Size(83, 23);
            this.chk饮酒种类_啤酒.TabIndex = 1;
            this.chk饮酒种类_啤酒.Tag = "2";
            // 
            // chk饮酒种类_红酒
            // 
            this.chk饮酒种类_红酒.Enabled = false;
            this.chk饮酒种类_红酒.Location = new System.Drawing.Point(175, 3);
            this.chk饮酒种类_红酒.Name = "chk饮酒种类_红酒";
            this.chk饮酒种类_红酒.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk饮酒种类_红酒.Properties.Appearance.Options.UseFont = true;
            this.chk饮酒种类_红酒.Properties.Caption = "红酒";
            this.chk饮酒种类_红酒.Size = new System.Drawing.Size(93, 23);
            this.chk饮酒种类_红酒.TabIndex = 2;
            this.chk饮酒种类_红酒.Tag = "3";
            // 
            // chk饮酒种类_黄酒
            // 
            this.chk饮酒种类_黄酒.Enabled = false;
            this.chk饮酒种类_黄酒.Location = new System.Drawing.Point(274, 3);
            this.chk饮酒种类_黄酒.Name = "chk饮酒种类_黄酒";
            this.chk饮酒种类_黄酒.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk饮酒种类_黄酒.Properties.Appearance.Options.UseFont = true;
            this.chk饮酒种类_黄酒.Properties.Caption = "黄酒";
            this.chk饮酒种类_黄酒.Size = new System.Drawing.Size(85, 23);
            this.chk饮酒种类_黄酒.TabIndex = 3;
            this.chk饮酒种类_黄酒.Tag = "4";
            // 
            // chk饮酒种类_其他
            // 
            this.chk饮酒种类_其他.Enabled = false;
            this.chk饮酒种类_其他.Location = new System.Drawing.Point(365, 3);
            this.chk饮酒种类_其他.Name = "chk饮酒种类_其他";
            this.chk饮酒种类_其他.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk饮酒种类_其他.Properties.Appearance.Options.UseFont = true;
            this.chk饮酒种类_其他.Properties.Caption = "其他";
            this.chk饮酒种类_其他.Size = new System.Drawing.Size(58, 23);
            this.chk饮酒种类_其他.TabIndex = 4;
            this.chk饮酒种类_其他.Tag = "99";
            this.chk饮酒种类_其他.CheckedChanged += new System.EventHandler(this.chk饮酒种类_其他_CheckedChanged);
            // 
            // txt饮酒种类_其他
            // 
            this.txt饮酒种类_其他.Enabled = false;
            this.txt饮酒种类_其他.Location = new System.Drawing.Point(429, 3);
            this.txt饮酒种类_其他.Name = "txt饮酒种类_其他";
            this.txt饮酒种类_其他.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt饮酒种类_其他.Properties.Appearance.Options.UseFont = true;
            this.txt饮酒种类_其他.Size = new System.Drawing.Size(100, 26);
            this.txt饮酒种类_其他.TabIndex = 5;
            // 
            // radio近一年内是否曾醉酒
            // 
            this.radio近一年内是否曾醉酒.Enabled = false;
            this.radio近一年内是否曾醉酒.Location = new System.Drawing.Point(113, 179);
            this.radio近一年内是否曾醉酒.Name = "radio近一年内是否曾醉酒";
            this.radio近一年内是否曾醉酒.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio近一年内是否曾醉酒.Properties.Appearance.Options.UseFont = true;
            this.radio近一年内是否曾醉酒.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio近一年内是否曾醉酒.Size = new System.Drawing.Size(627, 32);
            this.radio近一年内是否曾醉酒.StyleController = this.layoutControl1;
            this.radio近一年内是否曾醉酒.TabIndex = 41;
            // 
            // txt开始饮酒年龄
            // 
            this.txt开始饮酒年龄.BackColor = System.Drawing.Color.White;
            this.txt开始饮酒年龄.Enabled = false;
            this.txt开始饮酒年龄.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt开始饮酒年龄.Lbl1Text = "岁";
            this.txt开始饮酒年龄.Location = new System.Drawing.Point(113, 151);
            this.txt开始饮酒年龄.Margin = new System.Windows.Forms.Padding(0);
            this.txt开始饮酒年龄.Name = "txt开始饮酒年龄";
            this.txt开始饮酒年龄.Size = new System.Drawing.Size(627, 24);
            this.txt开始饮酒年龄.TabIndex = 43;
            this.txt开始饮酒年龄.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt戒酒年龄
            // 
            this.txt戒酒年龄.BackColor = System.Drawing.Color.White;
            this.txt戒酒年龄.Enabled = false;
            this.txt戒酒年龄.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt戒酒年龄.Lbl1Text = "岁";
            this.txt戒酒年龄.Location = new System.Drawing.Point(113, 126);
            this.txt戒酒年龄.Margin = new System.Windows.Forms.Padding(0);
            this.txt戒酒年龄.Name = "txt戒酒年龄";
            this.txt戒酒年龄.Size = new System.Drawing.Size(627, 21);
            this.txt戒酒年龄.TabIndex = 42;
            this.txt戒酒年龄.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // radio是否戒酒
            // 
            this.radio是否戒酒.Enabled = false;
            this.radio是否戒酒.Location = new System.Drawing.Point(113, 97);
            this.radio是否戒酒.Name = "radio是否戒酒";
            this.radio是否戒酒.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio是否戒酒.Properties.Appearance.Options.UseFont = true;
            this.radio是否戒酒.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "未戒酒"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "已戒酒")});
            this.radio是否戒酒.Size = new System.Drawing.Size(627, 25);
            this.radio是否戒酒.StyleController = this.layoutControl1;
            this.radio是否戒酒.TabIndex = 40;
            // 
            // txt日饮酒量
            // 
            this.txt日饮酒量.BackColor = System.Drawing.Color.White;
            this.txt日饮酒量.Enabled = false;
            this.txt日饮酒量.Lbl1Size = new System.Drawing.Size(60, 18);
            this.txt日饮酒量.Lbl1Text = "     平均";
            this.txt日饮酒量.Lbl2Size = new System.Drawing.Size(56, 18);
            this.txt日饮酒量.Lbl2Text = "（两）";
            this.txt日饮酒量.Location = new System.Drawing.Point(113, 72);
            this.txt日饮酒量.Margin = new System.Windows.Forms.Padding(0);
            this.txt日饮酒量.Name = "txt日饮酒量";
            this.txt日饮酒量.Size = new System.Drawing.Size(627, 21);
            this.txt日饮酒量.TabIndex = 39;
            this.txt日饮酒量.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // radio饮酒频率
            // 
            this.radio饮酒频率.EditValue = "1";
            this.radio饮酒频率.Location = new System.Drawing.Point(113, 38);
            this.radio饮酒频率.Name = "radio饮酒频率";
            this.radio饮酒频率.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio饮酒频率.Properties.Appearance.Options.UseFont = true;
            this.radio饮酒频率.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "从不"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "偶尔"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "经常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "每天")});
            this.radio饮酒频率.Size = new System.Drawing.Size(627, 30);
            this.radio饮酒频率.StyleController = this.layoutControl1;
            this.radio饮酒频率.TabIndex = 38;
            this.radio饮酒频率.SelectedIndexChanged += new System.EventHandler(this.radio饮酒频率_SelectedIndexChanged);
            // 
            // radio吸烟情况
            // 
            this.radio吸烟情况.EditValue = "1";
            this.radio吸烟情况.Location = new System.Drawing.Point(113, 38);
            this.radio吸烟情况.Name = "radio吸烟情况";
            this.radio吸烟情况.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio吸烟情况.Properties.Appearance.Options.UseFont = true;
            this.radio吸烟情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "从不吸烟"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "已戒烟"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "吸烟")});
            this.radio吸烟情况.Size = new System.Drawing.Size(627, 68);
            this.radio吸烟情况.StyleController = this.layoutControl1;
            this.radio吸烟情况.TabIndex = 34;
            this.radio吸烟情况.SelectedIndexChanged += new System.EventHandler(this.radio吸烟情况_SelectedIndexChanged);
            // 
            // txt戒烟年龄
            // 
            this.txt戒烟年龄.BackColor = System.Drawing.Color.White;
            this.txt戒烟年龄.Enabled = false;
            this.txt戒烟年龄.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt戒烟年龄.Lbl1Text = "岁";
            this.txt戒烟年龄.Location = new System.Drawing.Point(113, 159);
            this.txt戒烟年龄.Margin = new System.Windows.Forms.Padding(0);
            this.txt戒烟年龄.Name = "txt戒烟年龄";
            this.txt戒烟年龄.Size = new System.Drawing.Size(627, 22);
            this.txt戒烟年龄.TabIndex = 37;
            this.txt戒烟年龄.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt开始吸烟年龄
            // 
            this.txt开始吸烟年龄.BackColor = System.Drawing.Color.White;
            this.txt开始吸烟年龄.Enabled = false;
            this.txt开始吸烟年龄.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt开始吸烟年龄.Lbl1Text = "岁";
            this.txt开始吸烟年龄.Location = new System.Drawing.Point(113, 134);
            this.txt开始吸烟年龄.Margin = new System.Windows.Forms.Padding(0);
            this.txt开始吸烟年龄.Name = "txt开始吸烟年龄";
            this.txt开始吸烟年龄.Size = new System.Drawing.Size(627, 21);
            this.txt开始吸烟年龄.TabIndex = 36;
            this.txt开始吸烟年龄.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt日吸烟量
            // 
            this.txt日吸烟量.BackColor = System.Drawing.Color.White;
            this.txt日吸烟量.Enabled = false;
            this.txt日吸烟量.Lbl1Size = new System.Drawing.Size(60, 18);
            this.txt日吸烟量.Lbl1Text = "      平均";
            this.txt日吸烟量.Lbl2Size = new System.Drawing.Size(30, 18);
            this.txt日吸烟量.Lbl2Text = "（支）";
            this.txt日吸烟量.Location = new System.Drawing.Point(113, 110);
            this.txt日吸烟量.Margin = new System.Windows.Forms.Padding(0);
            this.txt日吸烟量.Name = "txt日吸烟量";
            this.txt日吸烟量.Size = new System.Drawing.Size(627, 20);
            this.txt日吸烟量.TabIndex = 35;
            this.txt日吸烟量.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // flow饮食习惯
            // 
            this.flow饮食习惯.BackColor = System.Drawing.Color.White;
            this.flow饮食习惯.Controls.Add(this.chk饮食习惯_荤素均衡);
            this.flow饮食习惯.Controls.Add(this.chk饮食习惯_荤食为主);
            this.flow饮食习惯.Controls.Add(this.chk饮食习惯_素食为主);
            this.flow饮食习惯.Controls.Add(this.chk饮食习惯_嗜盐);
            this.flow饮食习惯.Controls.Add(this.chk饮食习惯_嗜油);
            this.flow饮食习惯.Controls.Add(this.chk饮食习惯_嗜糖);
            this.flow饮食习惯.Location = new System.Drawing.Point(93, 38);
            this.flow饮食习惯.Margin = new System.Windows.Forms.Padding(0);
            this.flow饮食习惯.Name = "flow饮食习惯";
            this.flow饮食习惯.Size = new System.Drawing.Size(647, 47);
            this.flow饮食习惯.TabIndex = 33;
            // 
            // chk饮食习惯_荤素均衡
            // 
            this.chk饮食习惯_荤素均衡.EditValue = true;
            this.chk饮食习惯_荤素均衡.Location = new System.Drawing.Point(3, 1);
            this.chk饮食习惯_荤素均衡.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.chk饮食习惯_荤素均衡.Name = "chk饮食习惯_荤素均衡";
            this.chk饮食习惯_荤素均衡.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk饮食习惯_荤素均衡.Properties.Appearance.Options.UseFont = true;
            this.chk饮食习惯_荤素均衡.Properties.Caption = "荤素均衡";
            this.chk饮食习惯_荤素均衡.Size = new System.Drawing.Size(126, 23);
            this.chk饮食习惯_荤素均衡.TabIndex = 0;
            this.chk饮食习惯_荤素均衡.Tag = "1";
            // 
            // chk饮食习惯_荤食为主
            // 
            this.chk饮食习惯_荤食为主.Location = new System.Drawing.Point(135, 1);
            this.chk饮食习惯_荤食为主.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.chk饮食习惯_荤食为主.Name = "chk饮食习惯_荤食为主";
            this.chk饮食习惯_荤食为主.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk饮食习惯_荤食为主.Properties.Appearance.Options.UseFont = true;
            this.chk饮食习惯_荤食为主.Properties.Caption = "荤食为主";
            this.chk饮食习惯_荤食为主.Size = new System.Drawing.Size(126, 23);
            this.chk饮食习惯_荤食为主.TabIndex = 1;
            this.chk饮食习惯_荤食为主.Tag = "2";
            // 
            // chk饮食习惯_素食为主
            // 
            this.chk饮食习惯_素食为主.Location = new System.Drawing.Point(267, 1);
            this.chk饮食习惯_素食为主.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.chk饮食习惯_素食为主.Name = "chk饮食习惯_素食为主";
            this.chk饮食习惯_素食为主.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk饮食习惯_素食为主.Properties.Appearance.Options.UseFont = true;
            this.chk饮食习惯_素食为主.Properties.Caption = "素食为主";
            this.chk饮食习惯_素食为主.Size = new System.Drawing.Size(126, 23);
            this.chk饮食习惯_素食为主.TabIndex = 2;
            this.chk饮食习惯_素食为主.Tag = "3";
            // 
            // chk饮食习惯_嗜盐
            // 
            this.chk饮食习惯_嗜盐.Location = new System.Drawing.Point(399, 1);
            this.chk饮食习惯_嗜盐.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.chk饮食习惯_嗜盐.Name = "chk饮食习惯_嗜盐";
            this.chk饮食习惯_嗜盐.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk饮食习惯_嗜盐.Properties.Appearance.Options.UseFont = true;
            this.chk饮食习惯_嗜盐.Properties.Caption = "嗜盐";
            this.chk饮食习惯_嗜盐.Size = new System.Drawing.Size(126, 23);
            this.chk饮食习惯_嗜盐.TabIndex = 3;
            this.chk饮食习惯_嗜盐.Tag = "4";
            // 
            // chk饮食习惯_嗜油
            // 
            this.chk饮食习惯_嗜油.Location = new System.Drawing.Point(3, 24);
            this.chk饮食习惯_嗜油.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.chk饮食习惯_嗜油.Name = "chk饮食习惯_嗜油";
            this.chk饮食习惯_嗜油.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk饮食习惯_嗜油.Properties.Appearance.Options.UseFont = true;
            this.chk饮食习惯_嗜油.Properties.Caption = "嗜油";
            this.chk饮食习惯_嗜油.Size = new System.Drawing.Size(126, 23);
            this.chk饮食习惯_嗜油.TabIndex = 4;
            this.chk饮食习惯_嗜油.Tag = "5";
            // 
            // chk饮食习惯_嗜糖
            // 
            this.chk饮食习惯_嗜糖.Location = new System.Drawing.Point(135, 24);
            this.chk饮食习惯_嗜糖.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.chk饮食习惯_嗜糖.Name = "chk饮食习惯_嗜糖";
            this.chk饮食习惯_嗜糖.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk饮食习惯_嗜糖.Properties.Appearance.Options.UseFont = true;
            this.chk饮食习惯_嗜糖.Properties.Caption = "嗜糖";
            this.chk饮食习惯_嗜糖.Size = new System.Drawing.Size(104, 23);
            this.chk饮食习惯_嗜糖.TabIndex = 5;
            this.chk饮食习惯_嗜糖.Tag = "6";
            // 
            // txt生活方式_锻炼方式
            // 
            this.txt生活方式_锻炼方式.EditValue = "";
            this.txt生活方式_锻炼方式.Location = new System.Drawing.Point(196, 150);
            this.txt生活方式_锻炼方式.Name = "txt生活方式_锻炼方式";
            this.txt生活方式_锻炼方式.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt生活方式_锻炼方式.Properties.Appearance.Options.UseFont = true;
            this.txt生活方式_锻炼方式.Size = new System.Drawing.Size(543, 36);
            this.txt生活方式_锻炼方式.StyleController = this.layoutControl1;
            this.txt生活方式_锻炼方式.TabIndex = 32;
            // 
            // txt生活方式_坚持锻炼时间
            // 
            this.txt生活方式_坚持锻炼时间.BackColor = System.Drawing.Color.White;
            this.txt生活方式_坚持锻炼时间.Font = new System.Drawing.Font("宋体", 9F);
            this.txt生活方式_坚持锻炼时间.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt生活方式_坚持锻炼时间.Lbl1Text = "年";
            this.txt生活方式_坚持锻炼时间.Location = new System.Drawing.Point(196, 125);
            this.txt生活方式_坚持锻炼时间.Margin = new System.Windows.Forms.Padding(0);
            this.txt生活方式_坚持锻炼时间.Name = "txt生活方式_坚持锻炼时间";
            this.txt生活方式_坚持锻炼时间.Size = new System.Drawing.Size(543, 21);
            this.txt生活方式_坚持锻炼时间.TabIndex = 31;
            this.txt生活方式_坚持锻炼时间.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt生活方式_每次锻炼时间
            // 
            this.txt生活方式_每次锻炼时间.BackColor = System.Drawing.Color.White;
            this.txt生活方式_每次锻炼时间.Font = new System.Drawing.Font("宋体", 15F);
            this.txt生活方式_每次锻炼时间.Lbl1Size = new System.Drawing.Size(30, 18);
            this.txt生活方式_每次锻炼时间.Lbl1Text = "分钟";
            this.txt生活方式_每次锻炼时间.Location = new System.Drawing.Point(196, 101);
            this.txt生活方式_每次锻炼时间.Margin = new System.Windows.Forms.Padding(0);
            this.txt生活方式_每次锻炼时间.Name = "txt生活方式_每次锻炼时间";
            this.txt生活方式_每次锻炼时间.Size = new System.Drawing.Size(543, 20);
            this.txt生活方式_每次锻炼时间.TabIndex = 30;
            this.txt生活方式_每次锻炼时间.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // radio生活方式_锻炼频率
            // 
            this.radio生活方式_锻炼频率.EditValue = "4";
            this.radio生活方式_锻炼频率.Location = new System.Drawing.Point(196, 39);
            this.radio生活方式_锻炼频率.Name = "radio生活方式_锻炼频率";
            this.radio生活方式_锻炼频率.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.radio生活方式_锻炼频率.Properties.Appearance.Options.UseFont = true;
            this.radio生活方式_锻炼频率.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "每天"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "每周一次以上"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "偶尔"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "不锻炼")});
            this.radio生活方式_锻炼频率.Size = new System.Drawing.Size(543, 58);
            this.radio生活方式_锻炼频率.StyleController = this.layoutControl1;
            this.radio生活方式_锻炼频率.TabIndex = 29;
            this.radio生活方式_锻炼频率.SelectedIndexChanged += new System.EventHandler(this.radio生活方式_锻炼频率_SelectedIndexChanged);
            // 
            // flow症状
            // 
            this.flow症状.BackColor = System.Drawing.Color.White;
            this.flow症状.Controls.Add(this.chk症状_无症状);
            this.flow症状.Controls.Add(this.chk症状_头痛);
            this.flow症状.Controls.Add(this.chk症状_头晕);
            this.flow症状.Controls.Add(this.chk症状_心悸);
            this.flow症状.Controls.Add(this.chk症状_胸闷);
            this.flow症状.Controls.Add(this.chk症状_胸痛);
            this.flow症状.Controls.Add(this.chk症状_慢性咳嗽);
            this.flow症状.Controls.Add(this.chk症状_咳痰);
            this.flow症状.Controls.Add(this.chk症状_呼吸困难);
            this.flow症状.Controls.Add(this.chk症状_多饮);
            this.flow症状.Controls.Add(this.chk症状_多尿);
            this.flow症状.Controls.Add(this.chk症状_体重下降);
            this.flow症状.Controls.Add(this.chk症状_乏力);
            this.flow症状.Controls.Add(this.chk症状_关节肿痛);
            this.flow症状.Controls.Add(this.chk症状_视力模糊);
            this.flow症状.Controls.Add(this.chk症状_手脚麻木);
            this.flow症状.Controls.Add(this.chk症状_尿急);
            this.flow症状.Controls.Add(this.chk症状_尿痛);
            this.flow症状.Controls.Add(this.chk症状_便秘);
            this.flow症状.Controls.Add(this.chk症状_腹泻);
            this.flow症状.Controls.Add(this.chk症状_恶心呕吐);
            this.flow症状.Controls.Add(this.chk症状_眼花);
            this.flow症状.Controls.Add(this.chk症状_耳鸣);
            this.flow症状.Controls.Add(this.chk症状_乳房胀痛);
            this.flow症状.Controls.Add(this.chk症状_其他);
            this.flow症状.Controls.Add(this.txt症状_其他);
            this.flow症状.Location = new System.Drawing.Point(8, 38);
            this.flow症状.Name = "flow症状";
            this.flow症状.Size = new System.Drawing.Size(732, 231);
            this.flow症状.TabIndex = 13;
            // 
            // chk症状_无症状
            // 
            this.chk症状_无症状.EditValue = true;
            this.chk症状_无症状.Location = new System.Drawing.Point(5, 5);
            this.chk症状_无症状.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_无症状.Name = "chk症状_无症状";
            this.chk症状_无症状.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_无症状.Properties.Appearance.Options.UseFont = true;
            this.chk症状_无症状.Properties.Caption = "无症状";
            this.chk症状_无症状.Size = new System.Drawing.Size(92, 33);
            this.chk症状_无症状.TabIndex = 0;
            this.chk症状_无症状.Tag = "1";
            this.chk症状_无症状.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk症状_头痛
            // 
            this.chk症状_头痛.Enabled = false;
            this.chk症状_头痛.Location = new System.Drawing.Point(107, 5);
            this.chk症状_头痛.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_头痛.Name = "chk症状_头痛";
            this.chk症状_头痛.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_头痛.Properties.Appearance.Options.UseFont = true;
            this.chk症状_头痛.Properties.Caption = "头痛";
            this.chk症状_头痛.Size = new System.Drawing.Size(71, 33);
            this.chk症状_头痛.TabIndex = 1;
            this.chk症状_头痛.Tag = "2";
            // 
            // chk症状_头晕
            // 
            this.chk症状_头晕.Enabled = false;
            this.chk症状_头晕.Location = new System.Drawing.Point(188, 5);
            this.chk症状_头晕.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_头晕.Name = "chk症状_头晕";
            this.chk症状_头晕.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_头晕.Properties.Appearance.Options.UseFont = true;
            this.chk症状_头晕.Properties.Caption = "头晕";
            this.chk症状_头晕.Size = new System.Drawing.Size(69, 33);
            this.chk症状_头晕.TabIndex = 2;
            this.chk症状_头晕.Tag = "3";
            // 
            // chk症状_心悸
            // 
            this.chk症状_心悸.Enabled = false;
            this.chk症状_心悸.Location = new System.Drawing.Point(267, 5);
            this.chk症状_心悸.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_心悸.Name = "chk症状_心悸";
            this.chk症状_心悸.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_心悸.Properties.Appearance.Options.UseFont = true;
            this.chk症状_心悸.Properties.Caption = "心悸";
            this.chk症状_心悸.Size = new System.Drawing.Size(72, 33);
            this.chk症状_心悸.TabIndex = 3;
            this.chk症状_心悸.Tag = "4";
            // 
            // chk症状_胸闷
            // 
            this.chk症状_胸闷.Enabled = false;
            this.chk症状_胸闷.Location = new System.Drawing.Point(349, 5);
            this.chk症状_胸闷.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_胸闷.Name = "chk症状_胸闷";
            this.chk症状_胸闷.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_胸闷.Properties.Appearance.Options.UseFont = true;
            this.chk症状_胸闷.Properties.Caption = "胸闷";
            this.chk症状_胸闷.Size = new System.Drawing.Size(81, 33);
            this.chk症状_胸闷.TabIndex = 4;
            this.chk症状_胸闷.Tag = "5";
            // 
            // chk症状_胸痛
            // 
            this.chk症状_胸痛.Enabled = false;
            this.chk症状_胸痛.Location = new System.Drawing.Point(440, 5);
            this.chk症状_胸痛.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_胸痛.Name = "chk症状_胸痛";
            this.chk症状_胸痛.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_胸痛.Properties.Appearance.Options.UseFont = true;
            this.chk症状_胸痛.Properties.Caption = "胸痛";
            this.chk症状_胸痛.Size = new System.Drawing.Size(71, 33);
            this.chk症状_胸痛.TabIndex = 5;
            this.chk症状_胸痛.Tag = "6";
            // 
            // chk症状_慢性咳嗽
            // 
            this.chk症状_慢性咳嗽.Enabled = false;
            this.chk症状_慢性咳嗽.Location = new System.Drawing.Point(521, 5);
            this.chk症状_慢性咳嗽.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_慢性咳嗽.Name = "chk症状_慢性咳嗽";
            this.chk症状_慢性咳嗽.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_慢性咳嗽.Properties.Appearance.Options.UseFont = true;
            this.chk症状_慢性咳嗽.Properties.Caption = "慢性咳嗽";
            this.chk症状_慢性咳嗽.Size = new System.Drawing.Size(134, 33);
            this.chk症状_慢性咳嗽.TabIndex = 6;
            this.chk症状_慢性咳嗽.Tag = "7";
            // 
            // chk症状_咳痰
            // 
            this.chk症状_咳痰.Enabled = false;
            this.chk症状_咳痰.Location = new System.Drawing.Point(5, 34);
            this.chk症状_咳痰.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_咳痰.Name = "chk症状_咳痰";
            this.chk症状_咳痰.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_咳痰.Properties.Appearance.Options.UseFont = true;
            this.chk症状_咳痰.Properties.Caption = "咳痰";
            this.chk症状_咳痰.Size = new System.Drawing.Size(76, 33);
            this.chk症状_咳痰.TabIndex = 7;
            this.chk症状_咳痰.Tag = "8";
            // 
            // chk症状_呼吸困难
            // 
            this.chk症状_呼吸困难.Enabled = false;
            this.chk症状_呼吸困难.Location = new System.Drawing.Point(91, 34);
            this.chk症状_呼吸困难.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_呼吸困难.Name = "chk症状_呼吸困难";
            this.chk症状_呼吸困难.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_呼吸困难.Properties.Appearance.Options.UseFont = true;
            this.chk症状_呼吸困难.Properties.Caption = "呼吸困难";
            this.chk症状_呼吸困难.Size = new System.Drawing.Size(139, 33);
            this.chk症状_呼吸困难.TabIndex = 8;
            this.chk症状_呼吸困难.Tag = "9";
            // 
            // chk症状_多饮
            // 
            this.chk症状_多饮.Enabled = false;
            this.chk症状_多饮.Location = new System.Drawing.Point(240, 34);
            this.chk症状_多饮.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_多饮.Name = "chk症状_多饮";
            this.chk症状_多饮.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_多饮.Properties.Appearance.Options.UseFont = true;
            this.chk症状_多饮.Properties.Caption = "多饮";
            this.chk症状_多饮.Size = new System.Drawing.Size(82, 33);
            this.chk症状_多饮.TabIndex = 9;
            this.chk症状_多饮.Tag = "10";
            // 
            // chk症状_多尿
            // 
            this.chk症状_多尿.Enabled = false;
            this.chk症状_多尿.Location = new System.Drawing.Point(332, 34);
            this.chk症状_多尿.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_多尿.Name = "chk症状_多尿";
            this.chk症状_多尿.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_多尿.Properties.Appearance.Options.UseFont = true;
            this.chk症状_多尿.Properties.Caption = "多尿";
            this.chk症状_多尿.Size = new System.Drawing.Size(75, 33);
            this.chk症状_多尿.TabIndex = 10;
            this.chk症状_多尿.Tag = "11";
            // 
            // chk症状_体重下降
            // 
            this.chk症状_体重下降.Enabled = false;
            this.chk症状_体重下降.Location = new System.Drawing.Point(417, 34);
            this.chk症状_体重下降.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_体重下降.Name = "chk症状_体重下降";
            this.chk症状_体重下降.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_体重下降.Properties.Appearance.Options.UseFont = true;
            this.chk症状_体重下降.Properties.Caption = "体重下降";
            this.chk症状_体重下降.Size = new System.Drawing.Size(142, 33);
            this.chk症状_体重下降.TabIndex = 11;
            this.chk症状_体重下降.Tag = "12";
            // 
            // chk症状_乏力
            // 
            this.chk症状_乏力.Enabled = false;
            this.chk症状_乏力.Location = new System.Drawing.Point(569, 34);
            this.chk症状_乏力.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_乏力.Name = "chk症状_乏力";
            this.chk症状_乏力.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_乏力.Properties.Appearance.Options.UseFont = true;
            this.chk症状_乏力.Properties.Caption = "乏力";
            this.chk症状_乏力.Size = new System.Drawing.Size(76, 33);
            this.chk症状_乏力.TabIndex = 12;
            this.chk症状_乏力.Tag = "13";
            // 
            // chk症状_关节肿痛
            // 
            this.chk症状_关节肿痛.Enabled = false;
            this.chk症状_关节肿痛.Location = new System.Drawing.Point(5, 63);
            this.chk症状_关节肿痛.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_关节肿痛.Name = "chk症状_关节肿痛";
            this.chk症状_关节肿痛.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_关节肿痛.Properties.Appearance.Options.UseFont = true;
            this.chk症状_关节肿痛.Properties.Caption = "关节肿痛";
            this.chk症状_关节肿痛.Size = new System.Drawing.Size(128, 33);
            this.chk症状_关节肿痛.TabIndex = 13;
            this.chk症状_关节肿痛.Tag = "14";
            // 
            // chk症状_视力模糊
            // 
            this.chk症状_视力模糊.Enabled = false;
            this.chk症状_视力模糊.Location = new System.Drawing.Point(143, 63);
            this.chk症状_视力模糊.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_视力模糊.Name = "chk症状_视力模糊";
            this.chk症状_视力模糊.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_视力模糊.Properties.Appearance.Options.UseFont = true;
            this.chk症状_视力模糊.Properties.Caption = "视力模糊";
            this.chk症状_视力模糊.Size = new System.Drawing.Size(138, 33);
            this.chk症状_视力模糊.TabIndex = 14;
            this.chk症状_视力模糊.Tag = "15";
            // 
            // chk症状_手脚麻木
            // 
            this.chk症状_手脚麻木.Enabled = false;
            this.chk症状_手脚麻木.Location = new System.Drawing.Point(291, 63);
            this.chk症状_手脚麻木.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_手脚麻木.Name = "chk症状_手脚麻木";
            this.chk症状_手脚麻木.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_手脚麻木.Properties.Appearance.Options.UseFont = true;
            this.chk症状_手脚麻木.Properties.Caption = "手脚麻木";
            this.chk症状_手脚麻木.Size = new System.Drawing.Size(139, 33);
            this.chk症状_手脚麻木.TabIndex = 15;
            this.chk症状_手脚麻木.Tag = "16";
            // 
            // chk症状_尿急
            // 
            this.chk症状_尿急.Enabled = false;
            this.chk症状_尿急.Location = new System.Drawing.Point(440, 63);
            this.chk症状_尿急.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_尿急.Name = "chk症状_尿急";
            this.chk症状_尿急.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_尿急.Properties.Appearance.Options.UseFont = true;
            this.chk症状_尿急.Properties.Caption = "尿急";
            this.chk症状_尿急.Size = new System.Drawing.Size(76, 33);
            this.chk症状_尿急.TabIndex = 16;
            this.chk症状_尿急.Tag = "17";
            // 
            // chk症状_尿痛
            // 
            this.chk症状_尿痛.Enabled = false;
            this.chk症状_尿痛.Location = new System.Drawing.Point(526, 63);
            this.chk症状_尿痛.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_尿痛.Name = "chk症状_尿痛";
            this.chk症状_尿痛.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_尿痛.Properties.Appearance.Options.UseFont = true;
            this.chk症状_尿痛.Properties.Caption = "尿痛";
            this.chk症状_尿痛.Size = new System.Drawing.Size(108, 33);
            this.chk症状_尿痛.TabIndex = 17;
            this.chk症状_尿痛.Tag = "18";
            // 
            // chk症状_便秘
            // 
            this.chk症状_便秘.Enabled = false;
            this.chk症状_便秘.Location = new System.Drawing.Point(644, 63);
            this.chk症状_便秘.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_便秘.Name = "chk症状_便秘";
            this.chk症状_便秘.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_便秘.Properties.Appearance.Options.UseFont = true;
            this.chk症状_便秘.Properties.Caption = "便秘";
            this.chk症状_便秘.Size = new System.Drawing.Size(82, 33);
            this.chk症状_便秘.TabIndex = 18;
            this.chk症状_便秘.Tag = "19";
            // 
            // chk症状_腹泻
            // 
            this.chk症状_腹泻.Enabled = false;
            this.chk症状_腹泻.Location = new System.Drawing.Point(5, 92);
            this.chk症状_腹泻.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_腹泻.Name = "chk症状_腹泻";
            this.chk症状_腹泻.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_腹泻.Properties.Appearance.Options.UseFont = true;
            this.chk症状_腹泻.Properties.Caption = "腹泻";
            this.chk症状_腹泻.Size = new System.Drawing.Size(87, 33);
            this.chk症状_腹泻.TabIndex = 19;
            this.chk症状_腹泻.Tag = "20";
            // 
            // chk症状_恶心呕吐
            // 
            this.chk症状_恶心呕吐.Enabled = false;
            this.chk症状_恶心呕吐.Location = new System.Drawing.Point(102, 92);
            this.chk症状_恶心呕吐.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_恶心呕吐.Name = "chk症状_恶心呕吐";
            this.chk症状_恶心呕吐.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_恶心呕吐.Properties.Appearance.Options.UseFont = true;
            this.chk症状_恶心呕吐.Properties.Caption = "恶心呕吐";
            this.chk症状_恶心呕吐.Size = new System.Drawing.Size(179, 33);
            this.chk症状_恶心呕吐.TabIndex = 20;
            this.chk症状_恶心呕吐.Tag = "21";
            // 
            // chk症状_眼花
            // 
            this.chk症状_眼花.Enabled = false;
            this.chk症状_眼花.Location = new System.Drawing.Point(291, 92);
            this.chk症状_眼花.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_眼花.Name = "chk症状_眼花";
            this.chk症状_眼花.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_眼花.Properties.Appearance.Options.UseFont = true;
            this.chk症状_眼花.Properties.Caption = "眼花";
            this.chk症状_眼花.Size = new System.Drawing.Size(76, 33);
            this.chk症状_眼花.TabIndex = 21;
            this.chk症状_眼花.Tag = "22";
            // 
            // chk症状_耳鸣
            // 
            this.chk症状_耳鸣.Enabled = false;
            this.chk症状_耳鸣.Location = new System.Drawing.Point(377, 92);
            this.chk症状_耳鸣.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_耳鸣.Name = "chk症状_耳鸣";
            this.chk症状_耳鸣.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_耳鸣.Properties.Appearance.Options.UseFont = true;
            this.chk症状_耳鸣.Properties.Caption = "耳鸣";
            this.chk症状_耳鸣.Size = new System.Drawing.Size(108, 33);
            this.chk症状_耳鸣.TabIndex = 22;
            this.chk症状_耳鸣.Tag = "23";
            // 
            // chk症状_乳房胀痛
            // 
            this.chk症状_乳房胀痛.Enabled = false;
            this.chk症状_乳房胀痛.Location = new System.Drawing.Point(495, 92);
            this.chk症状_乳房胀痛.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_乳房胀痛.Name = "chk症状_乳房胀痛";
            this.chk症状_乳房胀痛.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_乳房胀痛.Properties.Appearance.Options.UseFont = true;
            this.chk症状_乳房胀痛.Properties.Caption = "乳房胀痛";
            this.chk症状_乳房胀痛.Size = new System.Drawing.Size(166, 33);
            this.chk症状_乳房胀痛.TabIndex = 23;
            this.chk症状_乳房胀痛.Tag = "24";
            // 
            // chk症状_其他
            // 
            this.chk症状_其他.Enabled = false;
            this.chk症状_其他.Location = new System.Drawing.Point(5, 121);
            this.chk症状_其他.Margin = new System.Windows.Forms.Padding(5);
            this.chk症状_其他.Name = "chk症状_其他";
            this.chk症状_其他.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.chk症状_其他.Properties.Appearance.Options.UseFont = true;
            this.chk症状_其他.Properties.Caption = "其他";
            this.chk症状_其他.Size = new System.Drawing.Size(79, 33);
            this.chk症状_其他.TabIndex = 24;
            this.chk症状_其他.Tag = "99";
            this.chk症状_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt症状_其他
            // 
            this.txt症状_其他.Enabled = false;
            this.txt症状_其他.Location = new System.Drawing.Point(94, 121);
            this.txt症状_其他.Margin = new System.Windows.Forms.Padding(5);
            this.txt症状_其他.Name = "txt症状_其他";
            this.txt症状_其他.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txt症状_其他.Properties.Appearance.Options.UseFont = true;
            this.txt症状_其他.Size = new System.Drawing.Size(191, 36);
            this.txt症状_其他.TabIndex = 25;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(748, 313);
            this.layoutControlGroup1.Text = "健康体检表";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "个人信息";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(9, 9, 9, 9);
            this.layoutControlGroup2.Size = new System.Drawing.Size(742, 2);
            this.layoutControlGroup2.Text = "个人信息";
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.TCG_Tags,
            this.layoutControlItem1,
            this.layoutControlItem2});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 2);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(742, 305);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // TCG_Tags
            // 
            this.TCG_Tags.AppearanceTabPage.Header.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.TCG_Tags.AppearanceTabPage.Header.Options.UseFont = true;
            this.TCG_Tags.AppearanceTabPage.Header.Options.UseTextOptions = true;
            this.TCG_Tags.AppearanceTabPage.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TCG_Tags.AppearanceTabPage.HeaderActive.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TCG_Tags.AppearanceTabPage.HeaderActive.ForeColor = System.Drawing.Color.Blue;
            this.TCG_Tags.AppearanceTabPage.HeaderActive.Options.UseFont = true;
            this.TCG_Tags.AppearanceTabPage.HeaderActive.Options.UseForeColor = true;
            this.TCG_Tags.CustomizationFormText = "tabGroup";
            this.TCG_Tags.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TCG_Tags.Location = new System.Drawing.Point(0, 0);
            this.TCG_Tags.Name = "TCG_Tags";
            this.TCG_Tags.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.TCG_Tags.SelectedTabPage = this.LCG_老年人评估;
            this.TCG_Tags.SelectedTabPageIndex = 1;
            this.TCG_Tags.ShowTabHeader = DevExpress.Utils.DefaultBoolean.True;
            this.TCG_Tags.Size = new System.Drawing.Size(742, 269);
            this.TCG_Tags.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LCG_症状,
            this.LCG_老年人评估,
            this.LCG_体育锻炼,
            this.LCG_饮食习惯,
            this.LCG_吸烟情况,
            this.LCG_饮酒情况,
            this.LCG_职业病});
            this.TCG_Tags.Text = "TCG_Tags";
            this.TCG_Tags.SelectedPageChanged += new DevExpress.XtraLayout.LayoutTabPageChangedEventHandler(this.tabGroup_SelectedPageChanged);
            // 
            // LCG_职业病
            // 
            this.LCG_职业病.AppearanceTabPage.Header.Font = new System.Drawing.Font("Tahoma", 12F);
            this.LCG_职业病.AppearanceTabPage.Header.Options.UseFont = true;
            this.LCG_职业病.CustomizationFormText = "职业病";
            this.LCG_职业病.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem129,
            this.lbl粉尘,
            this.lbl放射物质,
            this.lbl物理因素,
            this.layoutControlItem135,
            this.lbl职业病其他,
            this.layoutControlItem138,
            this.layoutControlItem136,
            this.layoutControlItem131,
            this.lbl防护措施,
            this.layoutControlItem130,
            this.layoutControlItem134,
            this.layoutControlItem133,
            this.layoutControlItem139,
            this.layoutControlItem137,
            this.layoutControlItem132});
            this.LCG_职业病.Location = new System.Drawing.Point(0, 0);
            this.LCG_职业病.Name = "LCG_职业病";
            this.LCG_职业病.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.LCG_职业病.Size = new System.Drawing.Size(736, 235);
            this.LCG_职业病.Tag = "6";
            this.LCG_职业病.Text = "职业病";
            // 
            // layoutControlItem129
            // 
            this.layoutControlItem129.Control = this.flowLayoutPanel1;
            this.layoutControlItem129.CustomizationFormText = "layoutControlItem129";
            this.layoutControlItem129.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem129.Name = "layoutControlItem129";
            this.layoutControlItem129.Size = new System.Drawing.Size(736, 49);
            this.layoutControlItem129.Text = "layoutControlItem129";
            this.layoutControlItem129.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem129.TextToControlDistance = 0;
            this.layoutControlItem129.TextVisible = false;
            // 
            // lbl粉尘
            // 
            this.lbl粉尘.Control = this.txt粉尘;
            this.lbl粉尘.CustomizationFormText = "粉尘";
            this.lbl粉尘.Location = new System.Drawing.Point(0, 49);
            this.lbl粉尘.MaxSize = new System.Drawing.Size(0, 36);
            this.lbl粉尘.MinSize = new System.Drawing.Size(177, 36);
            this.lbl粉尘.Name = "lbl粉尘";
            this.lbl粉尘.Size = new System.Drawing.Size(312, 36);
            this.lbl粉尘.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl粉尘.Text = "粉尘";
            this.lbl粉尘.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl粉尘.TextSize = new System.Drawing.Size(50, 20);
            this.lbl粉尘.TextToControlDistance = 5;
            // 
            // lbl放射物质
            // 
            this.lbl放射物质.Control = this.txt放射物质;
            this.lbl放射物质.CustomizationFormText = "放射物质";
            this.lbl放射物质.Location = new System.Drawing.Point(0, 85);
            this.lbl放射物质.MaxSize = new System.Drawing.Size(0, 30);
            this.lbl放射物质.MinSize = new System.Drawing.Size(177, 30);
            this.lbl放射物质.Name = "lbl放射物质";
            this.lbl放射物质.Size = new System.Drawing.Size(312, 30);
            this.lbl放射物质.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl放射物质.Text = "放射物质";
            this.lbl放射物质.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl放射物质.TextSize = new System.Drawing.Size(50, 20);
            this.lbl放射物质.TextToControlDistance = 5;
            // 
            // lbl物理因素
            // 
            this.lbl物理因素.Control = this.txt物理因素;
            this.lbl物理因素.CustomizationFormText = "物理因素";
            this.lbl物理因素.Location = new System.Drawing.Point(0, 115);
            this.lbl物理因素.MaxSize = new System.Drawing.Size(0, 40);
            this.lbl物理因素.MinSize = new System.Drawing.Size(177, 40);
            this.lbl物理因素.Name = "lbl物理因素";
            this.lbl物理因素.Size = new System.Drawing.Size(312, 40);
            this.lbl物理因素.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl物理因素.Text = "物理因素";
            this.lbl物理因素.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl物理因素.TextSize = new System.Drawing.Size(50, 20);
            this.lbl物理因素.TextToControlDistance = 5;
            // 
            // layoutControlItem135
            // 
            this.layoutControlItem135.Control = this.txt化学因素;
            this.layoutControlItem135.CustomizationFormText = "化学因素";
            this.layoutControlItem135.Location = new System.Drawing.Point(0, 155);
            this.layoutControlItem135.MaxSize = new System.Drawing.Size(0, 40);
            this.layoutControlItem135.MinSize = new System.Drawing.Size(177, 40);
            this.layoutControlItem135.Name = "layoutControlItem135";
            this.layoutControlItem135.Size = new System.Drawing.Size(312, 40);
            this.layoutControlItem135.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem135.Text = "化学因素";
            this.layoutControlItem135.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem135.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem135.TextToControlDistance = 5;
            // 
            // lbl职业病其他
            // 
            this.lbl职业病其他.Control = this.txt职业病其他;
            this.lbl职业病其他.CustomizationFormText = "其他";
            this.lbl职业病其他.Location = new System.Drawing.Point(0, 195);
            this.lbl职业病其他.MaxSize = new System.Drawing.Size(0, 40);
            this.lbl职业病其他.MinSize = new System.Drawing.Size(177, 40);
            this.lbl职业病其他.Name = "lbl职业病其他";
            this.lbl职业病其他.Size = new System.Drawing.Size(312, 40);
            this.lbl职业病其他.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl职业病其他.Text = "其他";
            this.lbl职业病其他.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl职业病其他.TextSize = new System.Drawing.Size(50, 20);
            this.lbl职业病其他.TextToControlDistance = 5;
            // 
            // layoutControlItem138
            // 
            this.layoutControlItem138.Control = this.radio职业病其他防护措施有无;
            this.layoutControlItem138.CustomizationFormText = "防护措施";
            this.layoutControlItem138.Location = new System.Drawing.Point(312, 195);
            this.layoutControlItem138.MinSize = new System.Drawing.Size(177, 24);
            this.layoutControlItem138.Name = "layoutControlItem138";
            this.layoutControlItem138.Size = new System.Drawing.Size(248, 40);
            this.layoutControlItem138.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem138.Text = "防护措施";
            this.layoutControlItem138.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem138.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem138.TextToControlDistance = 5;
            // 
            // layoutControlItem136
            // 
            this.layoutControlItem136.Control = this.radio化学防护措施有无;
            this.layoutControlItem136.CustomizationFormText = "防护措施";
            this.layoutControlItem136.Location = new System.Drawing.Point(312, 155);
            this.layoutControlItem136.MinSize = new System.Drawing.Size(177, 24);
            this.layoutControlItem136.Name = "layoutControlItem136";
            this.layoutControlItem136.Size = new System.Drawing.Size(248, 40);
            this.layoutControlItem136.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem136.Text = "防护措施";
            this.layoutControlItem136.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem136.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem136.TextToControlDistance = 5;
            // 
            // layoutControlItem131
            // 
            this.layoutControlItem131.Control = this.radio物理防护措施有无;
            this.layoutControlItem131.CustomizationFormText = "防护措施";
            this.layoutControlItem131.Location = new System.Drawing.Point(312, 115);
            this.layoutControlItem131.MinSize = new System.Drawing.Size(177, 24);
            this.layoutControlItem131.Name = "layoutControlItem131";
            this.layoutControlItem131.Size = new System.Drawing.Size(248, 40);
            this.layoutControlItem131.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem131.Text = "防护措施";
            this.layoutControlItem131.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem131.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem131.TextToControlDistance = 5;
            // 
            // lbl防护措施
            // 
            this.lbl防护措施.Control = this.radio放射物质防护措施有无;
            this.lbl防护措施.CustomizationFormText = "防护措施";
            this.lbl防护措施.Location = new System.Drawing.Point(312, 85);
            this.lbl防护措施.MinSize = new System.Drawing.Size(177, 24);
            this.lbl防护措施.Name = "lbl防护措施";
            this.lbl防护措施.Size = new System.Drawing.Size(248, 30);
            this.lbl防护措施.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl防护措施.Text = "防护措施";
            this.lbl防护措施.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl防护措施.TextSize = new System.Drawing.Size(50, 20);
            this.lbl防护措施.TextToControlDistance = 5;
            // 
            // layoutControlItem130
            // 
            this.layoutControlItem130.Control = this.radio粉尘防护措施有无;
            this.layoutControlItem130.CustomizationFormText = "防护措施";
            this.layoutControlItem130.Location = new System.Drawing.Point(312, 49);
            this.layoutControlItem130.MinSize = new System.Drawing.Size(177, 24);
            this.layoutControlItem130.Name = "layoutControlItem130";
            this.layoutControlItem130.Size = new System.Drawing.Size(248, 36);
            this.layoutControlItem130.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem130.Text = "防护措施";
            this.layoutControlItem130.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem130.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem130.TextToControlDistance = 5;
            // 
            // layoutControlItem134
            // 
            this.layoutControlItem134.Control = this.txt放射物质防护措施;
            this.layoutControlItem134.CustomizationFormText = "layoutControlItem134";
            this.layoutControlItem134.Location = new System.Drawing.Point(560, 85);
            this.layoutControlItem134.Name = "layoutControlItem134";
            this.layoutControlItem134.Size = new System.Drawing.Size(176, 30);
            this.layoutControlItem134.Text = "layoutControlItem134";
            this.layoutControlItem134.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem134.TextToControlDistance = 0;
            this.layoutControlItem134.TextVisible = false;
            // 
            // layoutControlItem133
            // 
            this.layoutControlItem133.Control = this.txt物理防护措施;
            this.layoutControlItem133.CustomizationFormText = "layoutControlItem133";
            this.layoutControlItem133.Location = new System.Drawing.Point(560, 115);
            this.layoutControlItem133.Name = "layoutControlItem133";
            this.layoutControlItem133.Size = new System.Drawing.Size(176, 40);
            this.layoutControlItem133.Text = "layoutControlItem133";
            this.layoutControlItem133.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem133.TextToControlDistance = 0;
            this.layoutControlItem133.TextVisible = false;
            // 
            // layoutControlItem139
            // 
            this.layoutControlItem139.Control = this.txt职业病其他防护;
            this.layoutControlItem139.CustomizationFormText = "layoutControlItem139";
            this.layoutControlItem139.Location = new System.Drawing.Point(560, 195);
            this.layoutControlItem139.Name = "layoutControlItem139";
            this.layoutControlItem139.Size = new System.Drawing.Size(176, 40);
            this.layoutControlItem139.Text = "layoutControlItem139";
            this.layoutControlItem139.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem139.TextToControlDistance = 0;
            this.layoutControlItem139.TextVisible = false;
            // 
            // layoutControlItem137
            // 
            this.layoutControlItem137.Control = this.txt化学防护措施;
            this.layoutControlItem137.CustomizationFormText = "layoutControlItem137";
            this.layoutControlItem137.Location = new System.Drawing.Point(560, 155);
            this.layoutControlItem137.Name = "layoutControlItem137";
            this.layoutControlItem137.Size = new System.Drawing.Size(176, 40);
            this.layoutControlItem137.Text = "layoutControlItem137";
            this.layoutControlItem137.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem137.TextToControlDistance = 0;
            this.layoutControlItem137.TextVisible = false;
            // 
            // layoutControlItem132
            // 
            this.layoutControlItem132.Control = this.txt粉尘防护措施;
            this.layoutControlItem132.CustomizationFormText = "layoutControlItem132";
            this.layoutControlItem132.Location = new System.Drawing.Point(560, 49);
            this.layoutControlItem132.Name = "layoutControlItem132";
            this.layoutControlItem132.Size = new System.Drawing.Size(176, 36);
            this.layoutControlItem132.Text = "layoutControlItem132";
            this.layoutControlItem132.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem132.TextToControlDistance = 0;
            this.layoutControlItem132.TextVisible = false;
            // 
            // LCG_症状
            // 
            this.LCG_症状.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.LCG_症状.AppearanceGroup.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.LCG_症状.AppearanceGroup.Options.UseFont = true;
            this.LCG_症状.AppearanceGroup.Options.UseForeColor = true;
            this.LCG_症状.AppearanceTabPage.Header.Font = new System.Drawing.Font("Tahoma", 12F);
            this.LCG_症状.AppearanceTabPage.Header.Options.UseFont = true;
            this.LCG_症状.CustomizationFormText = "症状";
            this.LCG_症状.ExpandButtonVisible = true;
            this.LCG_症状.ExpandOnDoubleClick = true;
            this.LCG_症状.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10});
            this.LCG_症状.Location = new System.Drawing.Point(0, 0);
            this.LCG_症状.Name = "LCG_症状";
            this.LCG_症状.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.LCG_症状.Size = new System.Drawing.Size(736, 235);
            this.LCG_症状.Tag = "1";
            this.LCG_症状.Text = "症状";
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.flow症状;
            this.layoutControlItem10.CustomizationFormText = "症状";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(169, 200);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(736, 235);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "症状";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // LCG_老年人评估
            // 
            this.LCG_老年人评估.AppearanceTabPage.Header.Font = new System.Drawing.Font("Tahoma", 12F);
            this.LCG_老年人评估.AppearanceTabPage.Header.Options.UseFont = true;
            this.LCG_老年人评估.CustomizationFormText = "老年人评估";
            this.LCG_老年人评估.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.group老年人评估});
            this.LCG_老年人评估.Location = new System.Drawing.Point(0, 0);
            this.LCG_老年人评估.Name = "LCG_老年人评估";
            this.LCG_老年人评估.Size = new System.Drawing.Size(736, 235);
            this.LCG_老年人评估.Tag = "2";
            this.LCG_老年人评估.Text = "老年人评估";
            // 
            // group老年人评估
            // 
            this.group老年人评估.CustomizationFormText = "group老年人评估";
            this.group老年人评估.GroupBordersVisible = false;
            this.group老年人评估.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem14,
            this.layoutControlItem18,
            this.layoutControlItem20,
            this.layoutControlItem128,
            this.emptySpaceItem1});
            this.group老年人评估.Location = new System.Drawing.Point(0, 0);
            this.group老年人评估.Name = "group老年人评估";
            this.group老年人评估.Size = new System.Drawing.Size(736, 235);
            this.group老年人评估.Text = "group老年人评估";
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem14.Control = this.radio老年人健康状态评估;
            this.layoutControlItem14.CustomizationFormText = "老年人健康状态自我评估*";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(220, 40);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(736, 41);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "老年人健康状态自我评估*";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem18.Control = this.radio老年人生活自理能力评估;
            this.layoutControlItem18.CustomizationFormText = "老年人生活自理能力自我评估*";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 41);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(220, 50);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(736, 75);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "老年人生活自理能力自我评估*";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem20.Control = this.flow老年人认知能力;
            this.layoutControlItem20.CustomizationFormText = "老年人认知能力*";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 116);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(270, 40);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(736, 42);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "老年人认知能力*";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlItem128
            // 
            this.layoutControlItem128.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem128.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem128.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem128.Control = this.flow老年人情感状态;
            this.layoutControlItem128.CustomizationFormText = "老年人情感状态*";
            this.layoutControlItem128.Location = new System.Drawing.Point(0, 158);
            this.layoutControlItem128.MinSize = new System.Drawing.Size(270, 40);
            this.layoutControlItem128.Name = "layoutControlItem128";
            this.layoutControlItem128.Size = new System.Drawing.Size(736, 42);
            this.layoutControlItem128.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem128.Text = "老年人情感状态*";
            this.layoutControlItem128.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem128.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem128.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 200);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(736, 35);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LCG_体育锻炼
            // 
            this.LCG_体育锻炼.AppearanceTabPage.Header.Font = new System.Drawing.Font("Tahoma", 12F);
            this.LCG_体育锻炼.AppearanceTabPage.Header.Options.UseFont = true;
            this.LCG_体育锻炼.CustomizationFormText = "生活方式";
            this.LCG_体育锻炼.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup19,
            this.emptySpaceItem2,
            this.layoutControlGroup3});
            this.LCG_体育锻炼.Location = new System.Drawing.Point(0, 0);
            this.LCG_体育锻炼.Name = "LCG_体育锻炼";
            this.LCG_体育锻炼.Size = new System.Drawing.Size(736, 235);
            this.LCG_体育锻炼.Tag = "3";
            this.LCG_体育锻炼.Text = "体育锻炼";
            // 
            // layoutControlGroup19
            // 
            this.layoutControlGroup19.CustomizationFormText = "layoutControlGroup19";
            this.layoutControlGroup19.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl锻炼频率});
            this.layoutControlGroup19.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup19.Name = "layoutControlGroup19";
            this.layoutControlGroup19.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup19.Size = new System.Drawing.Size(82, 153);
            this.layoutControlGroup19.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup19.Text = "layoutControlGroup19";
            this.layoutControlGroup19.TextVisible = false;
            // 
            // lbl锻炼频率
            // 
            this.lbl锻炼频率.AllowHotTrack = false;
            this.lbl锻炼频率.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl锻炼频率.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl锻炼频率.AppearanceItemCaption.Options.UseFont = true;
            this.lbl锻炼频率.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl锻炼频率.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl锻炼频率.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl锻炼频率.CustomizationFormText = "体育锻炼";
            this.lbl锻炼频率.Location = new System.Drawing.Point(0, 0);
            this.lbl锻炼频率.MaxSize = new System.Drawing.Size(80, 0);
            this.lbl锻炼频率.MinSize = new System.Drawing.Size(80, 10);
            this.lbl锻炼频率.Name = "lbl锻炼频率";
            this.lbl锻炼频率.Size = new System.Drawing.Size(80, 151);
            this.lbl锻炼频率.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl锻炼频率.Text = "体育锻炼";
            this.lbl锻炼频率.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl锻炼频率.TextSize = new System.Drawing.Size(50, 20);
            this.lbl锻炼频率.TextVisible = true;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 153);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(736, 82);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem25,
            this.layoutControlItem24,
            this.layoutControlItem23,
            this.layoutControlItem22});
            this.layoutControlGroup3.Location = new System.Drawing.Point(82, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(654, 153);
            this.layoutControlGroup3.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.Control = this.txt生活方式_锻炼方式;
            this.layoutControlItem25.CustomizationFormText = "锻炼方式";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 111);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(652, 40);
            this.layoutControlItem25.Text = "锻炼方式";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem24.Control = this.txt生活方式_坚持锻炼时间;
            this.layoutControlItem24.CustomizationFormText = "坚持锻炼时间";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 86);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(652, 25);
            this.layoutControlItem24.Text = "坚持锻炼时间";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.Control = this.txt生活方式_每次锻炼时间;
            this.layoutControlItem23.CustomizationFormText = "每次锻炼时间";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 62);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(652, 24);
            this.layoutControlItem23.Text = "每次锻炼时间";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.radio生活方式_锻炼频率;
            this.layoutControlItem22.CustomizationFormText = "锻炼频率";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(652, 62);
            this.layoutControlItem22.Text = "锻炼频率";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // LCG_饮食习惯
            // 
            this.LCG_饮食习惯.AppearanceTabPage.Header.Font = new System.Drawing.Font("Tahoma", 12F);
            this.LCG_饮食习惯.AppearanceTabPage.Header.Options.UseFont = true;
            this.LCG_饮食习惯.CustomizationFormText = "饮食习惯";
            this.LCG_饮食习惯.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem4,
            this.lbl饮食习惯});
            this.LCG_饮食习惯.Location = new System.Drawing.Point(0, 0);
            this.LCG_饮食习惯.Name = "LCG_饮食习惯";
            this.LCG_饮食习惯.Size = new System.Drawing.Size(736, 235);
            this.LCG_饮食习惯.Text = "饮食习惯";
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 51);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(736, 184);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lbl饮食习惯
            // 
            this.lbl饮食习惯.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl饮食习惯.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl饮食习惯.AppearanceItemCaption.Options.UseFont = true;
            this.lbl饮食习惯.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl饮食习惯.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl饮食习惯.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl饮食习惯.Control = this.flow饮食习惯;
            this.lbl饮食习惯.CustomizationFormText = "饮食习惯";
            this.lbl饮食习惯.Location = new System.Drawing.Point(0, 0);
            this.lbl饮食习惯.Name = "lbl饮食习惯";
            this.lbl饮食习惯.Size = new System.Drawing.Size(736, 51);
            this.lbl饮食习惯.Text = "饮食习惯";
            this.lbl饮食习惯.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl饮食习惯.TextSize = new System.Drawing.Size(80, 24);
            this.lbl饮食习惯.TextToControlDistance = 5;
            // 
            // LCG_吸烟情况
            // 
            this.LCG_吸烟情况.AppearanceTabPage.Header.Font = new System.Drawing.Font("Tahoma", 12F);
            this.LCG_吸烟情况.AppearanceTabPage.Header.Options.UseFont = true;
            this.LCG_吸烟情况.CustomizationFormText = "吸烟情况";
            this.LCG_吸烟情况.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.emptySpaceItem3});
            this.LCG_吸烟情况.Location = new System.Drawing.Point(0, 0);
            this.LCG_吸烟情况.Name = "LCG_吸烟情况";
            this.LCG_吸烟情况.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.LCG_吸烟情况.Size = new System.Drawing.Size(736, 235);
            this.LCG_吸烟情况.Tag = "4";
            this.LCG_吸烟情况.Text = "吸烟情况";
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.Control = this.radio吸烟情况;
            this.layoutControlItem27.CustomizationFormText = "吸烟情况";
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(736, 72);
            this.layoutControlItem27.Text = "吸烟情况";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(100, 10);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.Control = this.txt日吸烟量;
            this.layoutControlItem28.CustomizationFormText = "日吸烟量";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(736, 24);
            this.layoutControlItem28.Text = "日吸烟量";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.Control = this.txt开始吸烟年龄;
            this.layoutControlItem29.CustomizationFormText = "开始吸烟年龄";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(736, 25);
            this.layoutControlItem29.Text = "开始吸烟年龄";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.Control = this.txt戒烟年龄;
            this.layoutControlItem30.CustomizationFormText = "戒烟年龄";
            this.layoutControlItem30.Location = new System.Drawing.Point(0, 121);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(736, 26);
            this.layoutControlItem30.Text = "戒烟年龄";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 147);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(736, 88);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LCG_饮酒情况
            // 
            this.LCG_饮酒情况.AppearanceTabPage.Header.Font = new System.Drawing.Font("Tahoma", 12F);
            this.LCG_饮酒情况.AppearanceTabPage.Header.Options.UseFont = true;
            this.LCG_饮酒情况.CustomizationFormText = "饮酒情况";
            this.LCG_饮酒情况.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem31,
            this.layoutControlItem32,
            this.layoutControlItem33,
            this.layoutControlItem35,
            this.layoutControlItem37,
            this.layoutControlItem34,
            this.layoutControlItem36});
            this.LCG_饮酒情况.Location = new System.Drawing.Point(0, 0);
            this.LCG_饮酒情况.Name = "LCG_饮酒情况";
            this.LCG_饮酒情况.Size = new System.Drawing.Size(736, 235);
            this.LCG_饮酒情况.Tag = "5";
            this.LCG_饮酒情况.Text = "饮酒情况";
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.Control = this.radio饮酒频率;
            this.layoutControlItem31.CustomizationFormText = "饮酒频率";
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(736, 34);
            this.layoutControlItem31.Text = "饮酒频率";
            this.layoutControlItem31.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem31.TextToControlDistance = 5;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.Control = this.txt日饮酒量;
            this.layoutControlItem32.CustomizationFormText = "日饮酒量";
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(736, 25);
            this.layoutControlItem32.Text = "日饮酒量";
            this.layoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem32.TextToControlDistance = 5;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem33.Control = this.radio是否戒酒;
            this.layoutControlItem33.CustomizationFormText = "是否戒酒";
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 59);
            this.layoutControlItem33.MinSize = new System.Drawing.Size(200, 29);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(736, 29);
            this.layoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem33.Text = "是否戒酒";
            this.layoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem33.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem33.TextToControlDistance = 5;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.AppearanceItemCaption.BorderColor = System.Drawing.Color.Black;
            this.layoutControlItem35.AppearanceItemCaption.Options.UseBorderColor = true;
            this.layoutControlItem35.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem35.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem35.Control = this.txt开始饮酒年龄;
            this.layoutControlItem35.CustomizationFormText = "开始饮酒年龄";
            this.layoutControlItem35.Location = new System.Drawing.Point(0, 113);
            this.layoutControlItem35.MinSize = new System.Drawing.Size(209, 1);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(736, 28);
            this.layoutControlItem35.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem35.Text = "开始饮酒年龄";
            this.layoutControlItem35.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem35.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem35.TextToControlDistance = 5;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem37.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem37.Control = this.flow饮酒种类;
            this.layoutControlItem37.CustomizationFormText = "饮酒种类";
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 177);
            this.layoutControlItem37.MinSize = new System.Drawing.Size(209, 35);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(736, 58);
            this.layoutControlItem37.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem37.Text = "饮酒种类";
            this.layoutControlItem37.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem37.TextSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem37.TextToControlDistance = 5;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem34.Control = this.txt戒酒年龄;
            this.layoutControlItem34.CustomizationFormText = "戒酒年龄";
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 88);
            this.layoutControlItem34.MinSize = new System.Drawing.Size(209, 1);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(736, 25);
            this.layoutControlItem34.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem34.Text = "戒酒年龄";
            this.layoutControlItem34.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem34.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem34.TextToControlDistance = 5;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem36.Control = this.radio近一年内是否曾醉酒;
            this.layoutControlItem36.CustomizationFormText = "近一年内是否曾醉酒";
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 141);
            this.layoutControlItem36.MinSize = new System.Drawing.Size(179, 35);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(736, 36);
            this.layoutControlItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem36.Text = "近一年内是否曾醉酒";
            this.layoutControlItem36.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem36.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem36.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btn上一页;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 269);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 36);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(51, 36);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(370, 36);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btn下一页;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(370, 269);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 36);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(51, 36);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(372, 36);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.layoutControl2);
            this.panelControl7.Controls.Add(this.flowLayoutPanel53);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(0, 0);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(748, 44);
            this.panelControl7.TabIndex = 1;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.lookUpEdit_医师签字);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Left;
            this.layoutControl2.Location = new System.Drawing.Point(2, 2);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(330, 130, 250, 350);
            this.layoutControl2.Root = this.layoutControlGroup11;
            this.layoutControl2.Size = new System.Drawing.Size(299, 40);
            this.layoutControl2.TabIndex = 6;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // lookUpEdit_医师签字
            // 
            this.lookUpEdit_医师签字.Location = new System.Drawing.Point(127, 2);
            this.lookUpEdit_医师签字.Name = "lookUpEdit_医师签字";
            this.lookUpEdit_医师签字.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit_医师签字.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit_医师签字.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit_医师签字.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit_医师签字.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_医师签字.Properties.NullText = "";
            this.lookUpEdit_医师签字.Size = new System.Drawing.Size(170, 36);
            this.lookUpEdit_医师签字.StyleController = this.layoutControl2;
            this.lookUpEdit_医师签字.TabIndex = 4;
            this.lookUpEdit_医师签字.EditValueChanged += new System.EventHandler(this.lookUpEdit_医师签字_EditValueChanged);
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.CustomizationFormText = "layoutControlGroup11";
            this.layoutControlGroup11.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup11.GroupBordersVisible = false;
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem_医师签字});
            this.layoutControlGroup11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup11.Size = new System.Drawing.Size(299, 40);
            this.layoutControlGroup11.Text = "layoutControlGroup11";
            this.layoutControlGroup11.TextVisible = false;
            // 
            // layoutControlItem_医师签字
            // 
            this.layoutControlItem_医师签字.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem_医师签字.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem_医师签字.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem_医师签字.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem_医师签字.Control = this.lookUpEdit_医师签字;
            this.layoutControlItem_医师签字.CustomizationFormText = "医师签字";
            this.layoutControlItem_医师签字.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem_医师签字.Name = "layoutControlItem_医师签字";
            this.layoutControlItem_医师签字.Size = new System.Drawing.Size(299, 40);
            this.layoutControlItem_医师签字.Text = "医师签字";
            this.layoutControlItem_医师签字.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem_医师签字.TextSize = new System.Drawing.Size(120, 19);
            this.layoutControlItem_医师签字.TextToControlDistance = 5;
            // 
            // flowLayoutPanel53
            // 
            this.flowLayoutPanel53.Controls.Add(this.btn保存);
            this.flowLayoutPanel53.Controls.Add(this.sBut_Get参照数据);
            this.flowLayoutPanel53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel53.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel53.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel53.Name = "flowLayoutPanel53";
            this.flowLayoutPanel53.Size = new System.Drawing.Size(744, 40);
            this.flowLayoutPanel53.TabIndex = 5;
            // 
            // btn保存
            // 
            this.btn保存.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.btn保存.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn保存.Appearance.Options.UseFont = true;
            this.btn保存.Appearance.Options.UseForeColor = true;
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(636, 3);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(105, 34);
            this.btn保存.TabIndex = 3;
            this.btn保存.Text = "保  存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // sBut_Get参照数据
            // 
            this.sBut_Get参照数据.Appearance.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.sBut_Get参照数据.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.sBut_Get参照数据.Appearance.Options.UseFont = true;
            this.sBut_Get参照数据.Appearance.Options.UseForeColor = true;
            this.sBut_Get参照数据.Image = ((System.Drawing.Image)(resources.GetObject("sBut_Get参照数据.Image")));
            this.sBut_Get参照数据.Location = new System.Drawing.Point(448, 2);
            this.sBut_Get参照数据.Margin = new System.Windows.Forms.Padding(2);
            this.sBut_Get参照数据.Name = "sBut_Get参照数据";
            this.sBut_Get参照数据.Size = new System.Drawing.Size(183, 35);
            this.sBut_Get参照数据.TabIndex = 6;
            this.sBut_Get参照数据.Text = "获取去年数据参照";
            this.sBut_Get参照数据.Click += new System.EventHandler(this.sBut_Get参照数据_Click);
            // 
            // uc健康体检表1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl7);
            this.Name = "uc健康体检表1";
            this.Size = new System.Drawing.Size(748, 357);
            this.Load += new System.EventHandler(this.UC健康体检表_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt职业病其他防护.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业病其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt化学防护措施.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt化学因素.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt物理防护措施.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt物理因素.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio职业病其他防护措施有无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt放射物质防护措施.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt放射物质.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio化学防护措施有无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio物理防护措施有无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt粉尘防护措施.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt粉尘.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio放射物质防护措施有无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio粉尘防护措施有无.Properties)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio职业病有无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt工种.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt从业时间.Properties)).EndInit();
            this.flow老年人认知能力.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio老年人认知能力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt老年人认知能力总分.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio老年人生活自理能力评估.Properties)).EndInit();
            this.flow老年人情感状态.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio老年人情感状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt老年人情感状态总分.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio老年人健康状态评估.Properties)).EndInit();
            this.flow饮酒种类.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk饮酒种类_白酒.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮酒种类_啤酒.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮酒种类_红酒.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮酒种类_黄酒.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮酒种类_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt饮酒种类_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio近一年内是否曾醉酒.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否戒酒.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio饮酒频率.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio吸烟情况.Properties)).EndInit();
            this.flow饮食习惯.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_荤素均衡.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_荤食为主.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_素食为主.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_嗜盐.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_嗜油.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_嗜糖.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt生活方式_锻炼方式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio生活方式_锻炼频率.Properties)).EndInit();
            this.flow症状.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_无症状.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_头痛.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_头晕.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_心悸.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_胸闷.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_胸痛.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_慢性咳嗽.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_咳痰.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_呼吸困难.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_多饮.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_多尿.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_体重下降.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_乏力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_关节肿痛.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_视力模糊.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_手脚麻木.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_尿急.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_尿痛.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_便秘.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_腹泻.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_恶心呕吐.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_眼花.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_耳鸣.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_乳房胀痛.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt症状_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TCG_Tags)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_职业病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem129)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl粉尘)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl放射物质)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl物理因素)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem135)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl职业病其他)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem138)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem136)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem131)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl防护措施)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem130)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem134)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem133)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem139)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem137)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem132)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_症状)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_老年人评估)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group老年人评估)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem128)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_体育锻炼)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl锻炼频率)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_饮食习惯)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl饮食习惯)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_吸烟情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_饮酒情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_医师签字.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_医师签字)).EndInit();
            this.flowLayoutPanel53.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private System.Windows.Forms.FlowLayoutPanel flow症状;
        private DevExpress.XtraEditors.CheckEdit chk症状_无症状;
        private DevExpress.XtraEditors.CheckEdit chk症状_头痛;
        private DevExpress.XtraEditors.CheckEdit chk症状_头晕;
        private DevExpress.XtraEditors.CheckEdit chk症状_心悸;
        private DevExpress.XtraEditors.CheckEdit chk症状_胸闷;
        private DevExpress.XtraEditors.CheckEdit chk症状_胸痛;
        private DevExpress.XtraEditors.CheckEdit chk症状_慢性咳嗽;
        private DevExpress.XtraEditors.CheckEdit chk症状_咳痰;
        private DevExpress.XtraEditors.CheckEdit chk症状_呼吸困难;
        private DevExpress.XtraEditors.CheckEdit chk症状_多饮;
        private DevExpress.XtraEditors.CheckEdit chk症状_多尿;
        private DevExpress.XtraEditors.CheckEdit chk症状_体重下降;
        private DevExpress.XtraEditors.CheckEdit chk症状_乏力;
        private DevExpress.XtraEditors.CheckEdit chk症状_关节肿痛;
        private DevExpress.XtraEditors.CheckEdit chk症状_视力模糊;
        private DevExpress.XtraEditors.CheckEdit chk症状_手脚麻木;
        private DevExpress.XtraEditors.CheckEdit chk症状_尿急;
        private DevExpress.XtraEditors.CheckEdit chk症状_尿痛;
        private DevExpress.XtraEditors.CheckEdit chk症状_便秘;
        private DevExpress.XtraEditors.CheckEdit chk症状_腹泻;
        private DevExpress.XtraEditors.CheckEdit chk症状_恶心呕吐;
        private DevExpress.XtraEditors.CheckEdit chk症状_眼花;
        private DevExpress.XtraEditors.CheckEdit chk症状_耳鸣;
        private DevExpress.XtraEditors.CheckEdit chk症状_乳房胀痛;
        private DevExpress.XtraEditors.CheckEdit chk症状_其他;
        private DevExpress.XtraEditors.TextEdit txt症状_其他;
        private DevExpress.XtraEditors.RadioGroup radio生活方式_锻炼频率;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.TextEdit txt生活方式_锻炼方式;
        private UCTxtLbl txt生活方式_坚持锻炼时间;
        private UCTxtLbl txt生活方式_每次锻炼时间;
        private System.Windows.Forms.FlowLayoutPanel flow饮食习惯;
        private DevExpress.XtraEditors.CheckEdit chk饮食习惯_荤素均衡;
        private DevExpress.XtraEditors.CheckEdit chk饮食习惯_荤食为主;
        private DevExpress.XtraEditors.CheckEdit chk饮食习惯_素食为主;
        private DevExpress.XtraEditors.CheckEdit chk饮食习惯_嗜盐;
        private DevExpress.XtraEditors.CheckEdit chk饮食习惯_嗜油;
        private DevExpress.XtraEditors.CheckEdit chk饮食习惯_嗜糖;
        private UCLblTxtLbl txt日吸烟量;
        private UCTxtLbl txt开始吸烟年龄;
        private UCTxtLbl txt戒烟年龄;
        private DevExpress.XtraEditors.RadioGroup radio吸烟情况;
        private DevExpress.XtraEditors.RadioGroup radio饮酒频率;
        private DevExpress.XtraEditors.RadioGroup radio是否戒酒;
        private UCLblTxtLbl txt日饮酒量;
        private UCTxtLbl txt戒酒年龄;
        private DevExpress.XtraEditors.RadioGroup radio近一年内是否曾醉酒;
        private UCTxtLbl txt开始饮酒年龄;
        private System.Windows.Forms.FlowLayoutPanel flow饮酒种类;
        private DevExpress.XtraEditors.CheckEdit chk饮酒种类_白酒;
        private DevExpress.XtraEditors.CheckEdit chk饮酒种类_啤酒;
        private DevExpress.XtraEditors.CheckEdit chk饮酒种类_红酒;
        private DevExpress.XtraEditors.CheckEdit chk饮酒种类_黄酒;
        private DevExpress.XtraEditors.TextEdit txt饮酒种类_其他;
        private DevExpress.XtraEditors.CheckEdit chk饮酒种类_其他;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel53;
        private DevExpress.XtraEditors.RadioGroup radio老年人健康状态评估;
        private DevExpress.XtraEditors.RadioGroup radio老年人生活自理能力评估;
        private System.Windows.Forms.FlowLayoutPanel flow老年人认知能力;
        private DevExpress.XtraEditors.RadioGroup radio老年人认知能力;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.TextEdit txt老年人认知能力总分;
        private System.Windows.Forms.FlowLayoutPanel flow老年人情感状态;
        private DevExpress.XtraEditors.RadioGroup radio老年人情感状态;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.TextEdit txt老年人情感状态总分;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.TextEdit txt粉尘防护措施;
        private DevExpress.XtraEditors.TextEdit txt粉尘;
        private DevExpress.XtraEditors.RadioGroup radio粉尘防护措施有无;
        private DevExpress.XtraEditors.RadioGroup radio职业病有无;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.TextEdit txt工种;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.TextEdit txt从业时间;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.TextEdit txt职业病其他防护;
        private DevExpress.XtraEditors.TextEdit txt职业病其他;
        private DevExpress.XtraEditors.TextEdit txt化学防护措施;
        private DevExpress.XtraEditors.TextEdit txt化学因素;
        private DevExpress.XtraEditors.TextEdit txt物理防护措施;
        private DevExpress.XtraEditors.TextEdit txt物理因素;
        private DevExpress.XtraEditors.RadioGroup radio职业病其他防护措施有无;
        private DevExpress.XtraEditors.TextEdit txt放射物质防护措施;
        private DevExpress.XtraEditors.TextEdit txt放射物质;
        private DevExpress.XtraEditors.RadioGroup radio化学防护措施有无;
        private DevExpress.XtraEditors.RadioGroup radio物理防护措施有无;
        private DevExpress.XtraEditors.RadioGroup radio放射物质防护措施有无;
        private DevExpress.XtraLayout.TabbedControlGroup TCG_Tags;
        private DevExpress.XtraLayout.LayoutControlGroup LCG_职业病;
        private DevExpress.XtraLayout.LayoutControlGroup LCG_症状;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlGroup LCG_老年人评估;
        private DevExpress.XtraLayout.LayoutControlGroup group老年人评估;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem128;
        private DevExpress.XtraLayout.LayoutControlGroup LCG_体育锻炼;
        private DevExpress.XtraLayout.LayoutControlItem lbl饮食习惯;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup19;
        private DevExpress.XtraLayout.EmptySpaceItem lbl锻炼频率;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlGroup LCG_吸烟情况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlGroup LCG_饮酒情况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem129;
        private DevExpress.XtraLayout.LayoutControlItem lbl粉尘;
        private DevExpress.XtraLayout.LayoutControlItem lbl放射物质;
        private DevExpress.XtraLayout.LayoutControlItem lbl物理因素;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem135;
        private DevExpress.XtraLayout.LayoutControlItem lbl职业病其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem138;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem136;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem131;
        private DevExpress.XtraLayout.LayoutControlItem lbl防护措施;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem130;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem134;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem133;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem139;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem137;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem132;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private SimpleButton btn下一页;
        private SimpleButton btn上一页;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup LCG_饮食习惯;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private LookUpEdit lookUpEdit_医师签字;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_医师签字;
        private SimpleButton sBut_Get参照数据;
    }
}