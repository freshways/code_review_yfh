﻿namespace ATOMEHR_LeaveClient
{
    partial class ucB超
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucB超));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.listBox_超声提示模版 = new DevExpress.XtraEditors.ListBoxControl();
            this.memoEdit_超声提示 = new DevExpress.XtraEditors.MemoEdit();
            this.memoEdit_超声所见 = new DevExpress.XtraEditors.MemoEdit();
            this.cbe_检查部位 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lookUpEdit_医师签字 = new DevExpress.XtraEditors.LookUpEdit();
            this.btnReset = new DevExpress.XtraEditors.SimpleButton();
            this.txtResultText = new DevExpress.XtraEditors.MemoEdit();
            this.txtResultValue = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.txtHRValue = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listBox_超声提示模版)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_超声提示.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_超声所见.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbe_检查部位.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_医师签字.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtResultText.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtResultValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHRValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.listBox_超声提示模版);
            this.layoutControl1.Controls.Add(this.memoEdit_超声提示);
            this.layoutControl1.Controls.Add(this.memoEdit_超声所见);
            this.layoutControl1.Controls.Add(this.cbe_检查部位);
            this.layoutControl1.Controls.Add(this.lookUpEdit_医师签字);
            this.layoutControl1.Controls.Add(this.btnReset);
            this.layoutControl1.Controls.Add(this.txtResultText);
            this.layoutControl1.Controls.Add(this.txtResultValue);
            this.layoutControl1.Controls.Add(this.btn保存);
            this.layoutControl1.Controls.Add(this.txtHRValue);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(587, 49, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(610, 435);
            this.layoutControl1.TabIndex = 23;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // listBox_超声提示模版
            // 
            this.listBox_超声提示模版.Appearance.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox_超声提示模版.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.listBox_超声提示模版.Appearance.Options.UseFont = true;
            this.listBox_超声提示模版.Appearance.Options.UseForeColor = true;
            this.listBox_超声提示模版.Location = new System.Drawing.Point(308, 203);
            this.listBox_超声提示模版.Name = "listBox_超声提示模版";
            this.listBox_超声提示模版.Size = new System.Drawing.Size(295, 167);
            this.listBox_超声提示模版.StyleController = this.layoutControl1;
            this.listBox_超声提示模版.TabIndex = 30;
            this.listBox_超声提示模版.DoubleClick += new System.EventHandler(this.listBox_超声提示模版_DoubleClick);
            // 
            // memoEdit_超声提示
            // 
            this.memoEdit_超声提示.Location = new System.Drawing.Point(28, 290);
            this.memoEdit_超声提示.MaximumSize = new System.Drawing.Size(0, 80);
            this.memoEdit_超声提示.MinimumSize = new System.Drawing.Size(0, 80);
            this.memoEdit_超声提示.Name = "memoEdit_超声提示";
            this.memoEdit_超声提示.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoEdit_超声提示.Properties.Appearance.Options.UseFont = true;
            this.memoEdit_超声提示.Size = new System.Drawing.Size(276, 80);
            this.memoEdit_超声提示.StyleController = this.layoutControl1;
            this.memoEdit_超声提示.TabIndex = 29;
            this.memoEdit_超声提示.UseOptimizedRendering = true;
            // 
            // memoEdit_超声所见
            // 
            this.memoEdit_超声所见.Location = new System.Drawing.Point(28, 203);
            this.memoEdit_超声所见.MinimumSize = new System.Drawing.Size(0, 50);
            this.memoEdit_超声所见.Name = "memoEdit_超声所见";
            this.memoEdit_超声所见.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoEdit_超声所见.Properties.Appearance.Options.UseFont = true;
            this.memoEdit_超声所见.Size = new System.Drawing.Size(276, 83);
            this.memoEdit_超声所见.StyleController = this.layoutControl1;
            this.memoEdit_超声所见.TabIndex = 28;
            this.memoEdit_超声所见.UseOptimizedRendering = true;
            // 
            // cbe_检查部位
            // 
            this.cbe_检查部位.EditValue = "腹部";
            this.cbe_检查部位.Location = new System.Drawing.Point(306, 41);
            this.cbe_检查部位.Name = "cbe_检查部位";
            this.cbe_检查部位.Properties.Appearance.Font = new System.Drawing.Font("宋体", 25F, System.Drawing.FontStyle.Bold);
            this.cbe_检查部位.Properties.Appearance.ForeColor = System.Drawing.Color.Lime;
            this.cbe_检查部位.Properties.Appearance.Options.UseFont = true;
            this.cbe_检查部位.Properties.Appearance.Options.UseForeColor = true;
            this.cbe_检查部位.Properties.Appearance.Options.UseTextOptions = true;
            this.cbe_检查部位.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.cbe_检查部位.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 25F);
            this.cbe_检查部位.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbe_检查部位.Properties.AppearanceDropDown.Options.UseTextOptions = true;
            this.cbe_检查部位.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.cbe_检查部位.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbe_检查部位.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbe_检查部位.Size = new System.Drawing.Size(297, 40);
            this.cbe_检查部位.StyleController = this.layoutControl1;
            this.cbe_检查部位.TabIndex = 26;
            this.cbe_检查部位.EditValueChanged += new System.EventHandler(this.cbe_检查部位_EditValueChanged);
            // 
            // lookUpEdit_医师签字
            // 
            this.lookUpEdit_医师签字.Location = new System.Drawing.Point(112, 374);
            this.lookUpEdit_医师签字.Name = "lookUpEdit_医师签字";
            this.lookUpEdit_医师签字.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit_医师签字.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit_医师签字.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit_医师签字.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit_医师签字.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_医师签字.Properties.NullText = "";
            this.lookUpEdit_医师签字.Size = new System.Drawing.Size(160, 48);
            this.lookUpEdit_医师签字.StyleController = this.layoutControl1;
            this.lookUpEdit_医师签字.TabIndex = 25;
            // 
            // btnReset
            // 
            this.btnReset.Appearance.Font = new System.Drawing.Font("Tahoma", 29F);
            this.btnReset.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.btnReset.Appearance.Options.UseFont = true;
            this.btnReset.Appearance.Options.UseForeColor = true;
            this.btnReset.Image = ((System.Drawing.Image)(resources.GetObject("btnReset.Image")));
            this.btnReset.Location = new System.Drawing.Point(276, 374);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(128, 54);
            this.btnReset.StyleController = this.layoutControl1;
            this.btnReset.TabIndex = 24;
            this.btnReset.Text = "重置";
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // txtResultText
            // 
            this.txtResultText.Location = new System.Drawing.Point(7, 163);
            this.txtResultText.Margin = new System.Windows.Forms.Padding(2);
            this.txtResultText.Name = "txtResultText";
            this.txtResultText.Properties.Appearance.Font = new System.Drawing.Font("宋体", 25.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResultText.Properties.Appearance.Options.UseFont = true;
            this.txtResultText.Size = new System.Drawing.Size(596, 36);
            this.txtResultText.StyleController = this.layoutControl1;
            this.txtResultText.TabIndex = 24;
            this.txtResultText.UseOptimizedRendering = true;
            // 
            // txtResultValue
            // 
            this.txtResultValue.EditValue = "";
            this.txtResultValue.Enabled = false;
            this.txtResultValue.Location = new System.Drawing.Point(7, 119);
            this.txtResultValue.Name = "txtResultValue";
            this.txtResultValue.Properties.Appearance.Font = new System.Drawing.Font("宋体", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResultValue.Properties.Appearance.ForeColor = System.Drawing.Color.Lime;
            this.txtResultValue.Properties.Appearance.Options.UseFont = true;
            this.txtResultValue.Properties.Appearance.Options.UseForeColor = true;
            this.txtResultValue.Properties.Appearance.Options.UseTextOptions = true;
            this.txtResultValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtResultValue.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 25F);
            this.txtResultValue.Properties.AppearanceDropDown.Options.UseFont = true;
            this.txtResultValue.Properties.AppearanceDropDown.Options.UseTextOptions = true;
            this.txtResultValue.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtResultValue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtResultValue.Properties.DropDownItemHeight = 50;
            this.txtResultValue.Properties.DropDownRows = 8;
            this.txtResultValue.Properties.Items.AddRange(new object[] {
            "肝胆未见明显异常      脾切除",
            "      肝胆脾未见明显异常",
            "肝囊肿，建议复查",
            "    胆囊体积缩小，壁增厚毛糙，请结合临床",
            "肝囊肿      胆囊结石",
            " 胆囊形态饱满，请结合临床",
            "轻度脂肪肝声像图",
            "肝胆大致正常",
            "胆囊切除术后",
            "脾大",
            "    肝血管瘤。",
            "轻度脂肪肝声像图  餐后胆囊声像图",
            "胆结石，建议复查",
            "胆囊体积缩小囊壁增厚毛",
            "脂肪肝声像图。      胆囊术后切除",
            "    胆囊结石",
            "肝胆脾未见明显异常     右肾囊肿",
            "    脂肪肝声像图。      胆囊炎声像图",
            "肝内静脉扩张",
            "    轻度脂肪肝声像图。",
            "    胆囊多发结石  ",
            "肝囊结石多发",
            "  餐后胆囊声像图",
            "    胆囊壁毛糙，请结合临床",
            "    脂肪肝声像图。",
            "胆囊多发结石",
            "肝内胆管结石",
            "    胆囊泥沙结石",
            " 脂肪肝声像图",
            "肝血管瘤",
            "胆囊泥沙样结石",
            "肝血管瘤钙化，建议复查",
            "    胆囊形态饱满，请结合临床",
            "    脂肪肝声像图。      胆囊切除术后",
            "餐后胆囊声像图",
            "脂肪肝声像图。",
            "      脂肪肝声像图。",
            "  酒精肝声像图",
            "    脂肪肝声像图。  ",
            " 肝胆脾未见明显异常。",
            "    胆囊多发结石。",
            "    正常肝胆胰脾。",
            " 胆囊炎并胆囊结石",
            "胆囊体积缩小，囊壁增厚毛糙，请结合临床",
            "    胆囊炎声像图",
            "    胆囊切除术后",
            " 胆囊炎",
            "胆囊摘除术后",
            "肝囊肿声像图",
            "   轻度脂肪肝声像图。",
            "",
            "慢性胆囊炎      胆囊结石",
            "胆囊泥沙结石",
            "    肝内静脉及门静脉扩张，淤血肝",
            "    胆囊炎",
            "右肾囊肿",
            "  轻度脂肪肝声像图。",
            "肝脾未见明显异常      胆囊切除术后",
            "考虑“肝血管瘤”",
            "   胆囊结石",
            "    胆囊息肉。",
            "肝实质站位，建议复查",
            "肝囊结石",
            "肝胆脾未见明显异常      右肾囊肿",
            "淤血肝轻度",
            "多囊肝、",
            "    肝血管瘤",
            "    肝囊肿",
            "胆管结石",
            "肝内静脉扩张，建议复查",
            "    肝胆脾未见明显异常  ",
            "右肝囊肿",
            "肝囊肿多发",
            "胆囊结石",
            "胆囊炎声像图",
            "    慢性血吸虫肝病。",
            "慢性血吸虫肝病。",
            "    胆囊多发结石",
            "    胆囊内强回声光团，考虑泥沙状结石",
            "酒精肝",
            "   餐后胆囊",
            "肝硬化腹水",
            "胆囊肿",
            "胆囊切除术后      胆总管结石",
            "肝实质占位，建议复查",
            "肝囊肿（两个）",
            "脾脏切除术后",
            "脂肪肝",
            "    肝胆脾未见明显异常",
            "左肝囊肿",
            "胆囊、胆管结石",
            "肝囊肿",
            "多囊肝"});
            this.txtResultValue.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txtResultValue.Size = new System.Drawing.Size(596, 40);
            this.txtResultValue.StyleController = this.layoutControl1;
            this.txtResultValue.TabIndex = 24;
            this.txtResultValue.SelectedIndexChanged += new System.EventHandler(this.txtResultValue_SelectedIndexChanged);
            // 
            // btn保存
            // 
            this.btn保存.Appearance.Font = new System.Drawing.Font("Tahoma", 29F);
            this.btn保存.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.btn保存.Appearance.Options.UseFont = true;
            this.btn保存.Appearance.Options.UseForeColor = true;
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(408, 374);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(195, 54);
            this.btn保存.StyleController = this.layoutControl1;
            this.btn保存.TabIndex = 20;
            this.btn保存.Text = "保存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // txtHRValue
            // 
            this.txtHRValue.EditValue = "正常";
            this.txtHRValue.Location = new System.Drawing.Point(7, 41);
            this.txtHRValue.Name = "txtHRValue";
            this.txtHRValue.Properties.Appearance.Font = new System.Drawing.Font("宋体", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHRValue.Properties.Appearance.ForeColor = System.Drawing.Color.Lime;
            this.txtHRValue.Properties.Appearance.Options.UseFont = true;
            this.txtHRValue.Properties.Appearance.Options.UseForeColor = true;
            this.txtHRValue.Properties.Appearance.Options.UseTextOptions = true;
            this.txtHRValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtHRValue.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 25F);
            this.txtHRValue.Properties.AppearanceDropDown.Options.UseFont = true;
            this.txtHRValue.Properties.AppearanceDropDown.Options.UseTextOptions = true;
            this.txtHRValue.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtHRValue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtHRValue.Properties.Items.AddRange(new object[] {
            "正常",
            "异常"});
            this.txtHRValue.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txtHRValue.Size = new System.Drawing.Size(295, 40);
            this.txtHRValue.StyleController = this.layoutControl1;
            this.txtHRValue.TabIndex = 17;
            this.txtHRValue.SelectedIndexChanged += new System.EventHandler(this.txtHRValue_SelectedIndexChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem9,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem8,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem6});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(610, 435);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F);
            this.layoutControlItem5.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txtHRValue;
            this.layoutControlItem5.CustomizationFormText = "心率/HR:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(299, 78);
            this.layoutControlItem5.Text = "B超:";
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(113, 31);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.btn保存;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(401, 367);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(199, 58);
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F);
            this.layoutControlItem1.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem1.Control = this.txtResultValue;
            this.layoutControlItem1.CustomizationFormText = "异常描述:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 78);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(600, 78);
            this.layoutControlItem1.Text = "异常描述:";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(113, 31);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtResultText;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 156);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(600, 40);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnReset;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(269, 367);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(132, 58);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.Control = this.lookUpEdit_医师签字;
            this.layoutControlItem4.CustomizationFormText = "医师签字";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 367);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(269, 58);
            this.layoutControlItem4.Text = "医师签字";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(100, 29);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem8.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.layoutControlItem8.Control = this.memoEdit_超声所见;
            this.layoutControlItem8.CustomizationFormText = "B超所见";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 196);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(301, 87);
            this.layoutControlItem8.Text = "B\n超\n所\n见";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(16, 76);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.layoutControlItem10.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.memoEdit_超声提示;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 283);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(301, 84);
            this.layoutControlItem10.Text = "超\n声\n提\n示";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(16, 76);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.listBox_超声提示模版;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(301, 196);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(299, 171);
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F);
            this.layoutControlItem6.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.cbe_检查部位;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(299, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(301, 78);
            this.layoutControlItem6.Text = "检查部位:";
            this.layoutControlItem6.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(113, 31);
            // 
            // ucB超
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ucB超";
            this.Size = new System.Drawing.Size(610, 435);
            this.Load += new System.EventHandler(this.ucB超_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listBox_超声提示模版)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_超声提示.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_超声所见.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbe_检查部位.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_医师签字.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtResultText.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtResultValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHRValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.ComboBoxEdit txtHRValue;
        private DevExpress.XtraEditors.ComboBoxEdit txtResultValue;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.MemoEdit txtResultText;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.SimpleButton btnReset;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_医师签字;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.ComboBoxEdit cbe_检查部位;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.ListBoxControl listBox_超声提示模版;
        private DevExpress.XtraEditors.MemoEdit memoEdit_超声提示;
        private DevExpress.XtraEditors.MemoEdit memoEdit_超声所见;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;

    }
}
