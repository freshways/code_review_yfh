﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ATOMEHR_LeaveClient
{
    public partial class UC视力 : UserControl
    {
        User currentUser;

        public UC视力()
        {
            InitializeComponent();
        }
        public UC视力(User _currentUser)
        {
            InitializeComponent();
            currentUser = _currentUser;
        }

        private void btn保存_Click(object sender, EventArgs e)
        {
            //if (funCheck())
            //{
            try
            {
                double zuoyan = 0d;
                if (!Double.TryParse(this.txt左眼视力.Text.Trim(), out zuoyan))
                {
                    MessageBox.Show("请填写正确的左眼视力格式！");
                    this.txt左眼视力.Focus();
                    return;
                }
                double youyan = 0d;
                if (!Double.TryParse(this.txt右眼视力.Text.Trim(), out youyan))
                {
                    MessageBox.Show("请填写正确的右眼视力格式！");
                    this.txt右眼视力.Focus();
                    return;
                }
                double zuoyan1 = 0d;
                if (!string.IsNullOrEmpty(this.txt左眼矫正视力.Text.Trim()) && !Double.TryParse(this.txt左眼矫正视力.Text.Trim(), out zuoyan1))
                {
                    MessageBox.Show("请填写正确的左眼矫正视力格式！");
                    this.txt左眼矫正视力.Focus();
                    return;
                }
                double youyan1 = 0d;
                if (!string.IsNullOrEmpty(this.txt右眼矫正视力.Text.Trim()) && !Double.TryParse(this.txt右眼矫正视力.Text.Trim(), out youyan1))
                {
                    MessageBox.Show("请填写正确的右眼矫正视力格式！");
                    this.txt右眼矫正视力.Focus();
                    return;
                }
                //tb_健康体检Info tb = tb_健康体检DAL.Gettb_健康体检InfoById(Program.currentUser.ID);
                //string sql = string.Empty;
                bool result = false;
                tb_健康体检Info tb_New = new tb_健康体检Info();
                tb_New.身份证号 = Program.currentUser.ID;
                tb_New.出生日期 = Program.currentUser.Birth;
                tb_New.姓名 = Program.currentUser.Name;
                tb_New.性别 = Program.currentUser.Gender;
                tb_New.个人档案编号 = Program.currentUser.DocNo;
                //tb_New.个人档案编号 = Program.currentUser.CreateTime;
                tb_New.左眼视力 = zuoyan;
                tb_New.右眼视力 = youyan;
                tb_New.左眼矫正 = this.txt左眼矫正视力.Text.Trim();
                tb_New.右眼矫正 = this.txt右眼矫正视力.Text.Trim();
                //tb_New.体检日期 = DateTime.Now.ToString("yyyy-MM-dd");
                if (tb_健康体检DAL.UpdateSys_视力(tb_New))
                {
                    MessageBox.Show("保存数据成功！");
                }
            }
            catch (Exception ex)
            {

            }
            //}
        }

        //private bool funCheck()
        //{
        //    if (Int32. this.txt左眼视力.Text.Trim())
        //    {

        //    }
        //    return true;
        //}

        private void UC视力_Load(object sender, EventArgs e)
        {
            tb_健康体检Info tb = tb_健康体检DAL.Gettb_健康体检InfoById(Program.currentUser.ID);
            if (tb != null)
            {
                if (tb.右眼视力 >0)
                    this.txt右眼视力.Text = tb.右眼视力.ToString();
                if (tb.左眼视力 > 0) 
                    this.txt左眼视力.Text = tb.左眼视力.ToString();

                if (string.IsNullOrEmpty(tb.右眼矫正))
                    this.txt右眼矫正视力.Text = tb.右眼矫正;
                if (string.IsNullOrEmpty(tb.左眼矫正)) 
                    this.txt左眼矫正视力.Text = tb.左眼矫正;
            }
        }
    }
}
