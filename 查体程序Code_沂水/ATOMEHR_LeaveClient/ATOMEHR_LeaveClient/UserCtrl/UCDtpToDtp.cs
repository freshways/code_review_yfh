﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace AtomEHR.Library.UserControls
{
    public partial class UCDtpToDtp : UserControl
    {
        public UCDtpToDtp()
        {
            InitializeComponent();
        }

        #region MyRegion
        [Category("Custom")]
        public DateEdit Dte1
        {
            get { return dte1; }
            set
            {
                value = dte1;
            }
        }
        [Category("Custom Size")]
        public Size Dte1Size
        {
            get { return dte1.Size; }
            set { value = this.dte1.Size; }
        }
        [Category("Custom")]
        public DateEdit Dte2
        {
            get { return dte2; }
            set
            {
                value = dte2;
            }
        }
        [Category("Custom Size")]
        public Size Dte2Size
        {
            get { return dte2.Size; }
            set { value = this.dte2.Size; }
        }
        #endregion
    }
}
