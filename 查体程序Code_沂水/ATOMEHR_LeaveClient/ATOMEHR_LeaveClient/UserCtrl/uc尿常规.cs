﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.Text.RegularExpressions;
using DecodeInterface;

namespace ATOMEHR_LeaveClient
{
    public partial class uc尿常规 : Base_UserControl
    {
        public uc尿常规()
        {
            InitializeComponent();
        }

        public uc尿常规(SerialPort spt)
        {
            InitializeComponent();
            comm = spt;
        }

        IntPtr hBluetooth;
        int nCOMID = 0; //端口

        private SerialPort comm = new SerialPort();
        private long received_count = 0;//接收计数
        private bool Listening = false;//是否没有执行完invoke相关操作
        private bool Closing = false;//是否正在关闭串口，执行Application.DoEvents，并阻止再次invoke
        private List<byte> buffer = new List<byte>(4096);//默认分配1页内存，并始终限制不允许超过
        private byte[] binary_data_1 = new byte[1024];

        private void uc尿常规_Load(object sender, EventArgs e)
        {
            //获取蓝牙操作句柄
            hBluetooth = EcgHelper.BluetoothOperationOpen();
            try
            {
                EcgHelper.BluetoothOperationConnect(hBluetooth, Program.BlueAddr, ref nCOMID);
                //EcgHelper.BluetoothOperationConnect(hBluetooth, "(00:1F:B7:08:0D:19)", ref nCOMID);
                //labelCom.Text += nCOMID.ToString();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("未与设备进行配对！请先初始化" + ex.Message);
                return;
            }
            //添加事件注册
            comm.DataReceived += comm_DataReceived;

            //关闭时点击，则设置好端口，波特率后打开
            comm.PortName = string.Format("COM{0}", nCOMID);
            comm.BaudRate = 9600;
            try
            {
                comm.Open();
                this.Invoke((EventHandler)(delegate { }));
            }
            catch (Exception ex)
            {
                //捕获到异常信息，创建一个新的comm对象，之前的不能用了。
                comm = new SerialPort();
                //现实异常信息给客户。
                MessageBox.Show(ex.Message);
            }
        }

        void comm_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (Closing) return;//如果正在关闭，忽略操作，直接返回，尽快的完成串口监听线程的一次循环
            try
            {
                Listening = true;//设置标记，说明我已经开始处理数据，一会儿要使用系统UI的。
                int n = comm.BytesToRead;//先记录下来，避免某种原因，人为的原因，操作几次之间时间长，缓存不一致
                byte[] buf = new byte[n];//声明一个临时数组存储当前来的串口数据
                received_count += n;//增加接收计数
                comm.Read(buf, 0, n);//读取缓冲数据

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //<协议解析>
                bool data_1_catched = false;//缓存记录数据是否捕获到
                //1.缓存数据
                buffer.AddRange(buf);
                //2.完整性判断
                while (buffer.Count > 4)//至少要包含头（2字节）+长度（1字节）+校验（1字节）
                {
                    //请不要担心使用>=，因为>=已经和>,<,=一样，是独立操作符，并不是解析成>和=2个符号
                    //2.1 查找数据头
                    if (buffer[0] == 0x02)
                    {
                        if (buffer[buffer.Count - 1] != 0x03) break;//如果最后一位不是结束

                        //至此，已经被找到了一条完整数据。我们将数据直接分析，或是缓存起来一起分析
                        //我们这里采用的办法是缓存一次，好处就是如果你某种原因，数据堆积在缓存buffer中
                        //已经很多了，那你需要循环的找到最后一组，只分析最新数据，过往数据你已经处理不及时
                        //了，就不要浪费更多时间了，这也是考虑到系统负载能够降低。
                        binary_data_1 = new byte[buffer.Count];
                        buffer.CopyTo(0, binary_data_1, 0, buffer.Count);//复制一条完整数据到具体的数据缓存
                        data_1_catched = true;
                        buffer.RemoveRange(0, buffer.Count);//正确分析一条数据，从缓存中移除数据
                        continue;//继续下一次循环
                    }
                    else
                    {
                        //这里是很重要的，如果数据开始不是头，则删除数据
                        buffer.RemoveAt(0);
                    }
                }
                //分析数据
                if (data_1_catched)
                {
                    //对缓存数据进行解码，完成解码后更新界面
                    this.Invoke((EventHandler)(delegate
                    {
                        labelState.Text = "开始截取数据...";
                        U120 IDecode = new U120();
                        string ss = System.Text.Encoding.Default.GetString(binary_data_1, 0, binary_data_1.Length);
                        IDecode.DataDecode(ss);
                        if (IDecode.ItemResultsRetrun != null)
                        {
                            //DecodeInterface.ItemResult
                            foreach (DecodeInterface.ItemResult re in IDecode.ItemResultsRetrun)
                            {
                                if (re.ItemID == "Date")
                                {
                                    this.txt检查时间.Text = re.ItemValue;
                                }
                                else if (re.ItemID == "ID")
                                {
                                    this.txtID.Text = re.ItemValue.Replace(" ", "");
                                }
                                else if (re.ItemID.IndexOf("PRO") > -1)
                                {
                                    this.txtPro尿蛋白.Text = re.ItemValue;
                                }
                                else if (re.ItemID == "mAlb")
                                {
                                    this.txtmAlb微量蛋白.Text = re.ItemValue;
                                }
                                else if (re.ItemID.IndexOf("GLU") > -1)
                                {
                                    this.txtGLU尿糖.Text = re.ItemValue;
                                }
                                else if (re.ItemID.IndexOf("KET") > -1 || re.ItemID == "KET")
                                {
                                    this.txtKET尿酮体.Text = re.ItemValue;
                                }
                                else if (re.ItemID.IndexOf("BLO") > -1 || re.ItemID == "BLO")
                                {
                                    this.txtBLO尿潜血.Text = re.ItemValue;
                                }
                            }
                        }
                        labelState.Text = "数据处理完成...";
                    }));
                }

            }
            finally
            {
                Listening = false;//我用完了，ui可以关闭串口了。
            }
        }

        private void btn保存_Click(object sender, EventArgs e)
        {
            try
            {
                tb_健康体检Info tb_New = new tb_健康体检Info();
                tb_New.尿蛋白 = this.txtPro尿蛋白.Text.Trim();
                tb_New.尿潜血 = this.txtBLO尿潜血.Text.Trim();
                tb_New.尿糖 = this.txtGLU尿糖.Text.Trim();
                tb_New.尿酮体 = this.txtKET尿酮体.Text.Trim();
                tb_New.尿微量白蛋白 = this.txtmAlb微量蛋白.Text.Trim();

                if (tb_健康体检DAL.UpdateSys_尿常规(tb_New))
                {
                    MessageBox.Show("保存数据成功！");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("保存数据失败！ " + ex.Message);
            }

        }


    }

    public class U120 : DecodeBase
    {

        public List<ItemResult> ItemResultsRetrun = new List<ItemResult>();//记录


        /// <summary>
        /// 入口：指定一段标本的开始位和结束位
        /// </summary>
        /// <param name="strComData"></param>
        public override void DataDecode(string strComData)
        {
            //在调用解码之前，数据位已经校验好开始和结束位
            base.DataDecodeComm((char)0x02, (char)0x03, 0, strComData);
        }
        /// <summary> 数据解码过程
        /// </summary>
        public override void InstrDataDecode(string strSampleData, out int Status, out  string strType, out  Exception ex, out string strSampleNo,
            out  string strDate, out List<ItemResult> ItemResults, out List<ItemImgResult> ItemImgResults, out float Heat, out float XYDepth,
            out float Haemoglobin, out string strOtherSampleData)
        {
            strType = "U120";
            strSampleNo = "";
            strDate = "";
            Status = 0;
            ItemResults = new List<ItemResult>();                       //解码项目及结果
            ItemImgResults = new List<ItemImgResult>();                 //图形结果
            Heat = 0;
            XYDepth = 0;
            Haemoglobin = 0;
            strOtherSampleData = "";

            try
            {
                strDate = System.DateTime.Now.ToString();                     //默认标本日期为当前时间(在后面的代码中再换成真实的)
                //strDate = strDate.Substring(0, strDate.Length - 2) + "00";  //去掉秒
                //strSampleData = strSampleData.Replace("\r\n", "");          //将原始数据中换行符去掉

                string[] strFGF = { "\n", "\r" };
                string[] strWhiteSpace = { " " };
                string[] strDataLines = strSampleData.Split(strFGF, StringSplitOptions.RemoveEmptyEntries);

                // -取固定位置-时间和ID 
                ItemResult newItemResultdate = new ItemResult();
                newItemResultdate.ItemID = "Date";
                newItemResultdate.ItemValue = strDataLines[1];
                ItemResults.Add(newItemResultdate);

                ItemResult newItemResultID = new ItemResult();
                newItemResultID.ItemID = "ID";
                newItemResultID.ItemValue = strDataLines[3].Replace("ID:", "");
                ItemResults.Add(newItemResultID);


                //剩下的从第4行开始读取
                for (int index = 4; index < strDataLines.Length; index++)
                {
                    if (strDataLines[index].Length < 2)
                    { }
                    else
                    {
                        if (strDataLines[index].Contains("BIOWAY B-11"))
                        {
                            int lastloc = strDataLines[index].LastIndexOf('-');
                            strSampleNo = strDataLines[index].Substring(lastloc + 1, strDataLines[index].Length - 1 - lastloc);
                            strSampleNo = Convert.ToInt64(strSampleNo).ToString();
                        }
                        else
                        {
                            string[] strItemDetail = strDataLines[index].Split(strWhiteSpace, StringSplitOptions.RemoveEmptyEntries);
                            if (strItemDetail.Length >= 2)
                            {
                                //获取个项目的值
                                ItemResult newItemResult = new ItemResult();
                                newItemResult.ItemID = strItemDetail[0];
                                newItemResult.ItemValue = strItemDetail[1];
                                ItemResults.Add(newItemResult);
                                if ((newItemResult.ItemID == "PRO" || newItemResult.ItemID.IndexOf("PRO") > -1) && strItemDetail.Length >= 4)
                                {
                                    //在这里要取一下“尿蛋白”的微量值  英文缩写：mAlb 
                                    ItemResult newItemResultPRO = new ItemResult();
                                    newItemResultPRO.ItemID = "mAlb";
                                    newItemResultPRO.ItemValue = strItemDetail[2];
                                    ItemResults.Add(newItemResultPRO);
                                }
                            }
                            else if (strItemDetail.Length == 1)
                            {
                                //获取个项目的值
                                ItemResult newItemResult = new ItemResult();
                                newItemResult.ItemID = strItemDetail[0];
                                newItemResult.ItemValue = "-";
                                ItemResults.Add(newItemResult);
                            }
                        }
                    }
                }

                //下面这两个赋值没用了
                strDate = strDataLines[1].ToString();                   //解析日期
                strSampleNo = strDataLines[3].Replace("ID:", "").ToString(); ;  //解析标本号

                ItemResultsRetrun = ItemResults;
                Status = 1;
                ex = null;
            }
            catch (Exception e)
            {
                Status = -1;
                ex = e;
            }
        }

    }
}
