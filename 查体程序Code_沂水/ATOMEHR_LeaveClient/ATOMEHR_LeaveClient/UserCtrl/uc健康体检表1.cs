﻿using AtomEHR.Library;
using ATOMEHR_LeaveClient.SqlSugar_DALs;
using ATOMEHR_LeaveClient.SqlSugar_Models;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ATOMEHR_LeaveClient
{

    public partial class uc健康体检表1 : UserControl
    {
        /*
     1.    症状选项对应的  code值   zz_zhengzhuang
         *ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
                    10002498	zz_zhengzhuang	症状	1	WZZ、MZZ	无症状		1
                    10002621	zz_zhengzhuang	症状	2	TT	头痛		1
                    10002624	zz_zhengzhuang	症状	3	TY	头晕		1
                    10002625	zz_zhengzhuang	症状	4	XJ	心悸		1
                    10002626	zz_zhengzhuang	症状	5	XM	胸闷		1
                    10002627	zz_zhengzhuang	症状	6	XT	胸痛		1
                    10002628	zz_zhengzhuang	症状	7	MXKS、MXHS	慢性咳嗽		1
                    10002629	zz_zhengzhuang	症状	8	KT、HT	咳痰		1
                    10003084	zz_zhengzhuang	症状	9	HXKN	呼吸困难		1
                    10003085	zz_zhengzhuang	症状	10	DY	多饮		1
                    10003086	zz_zhengzhuang	症状	11	DS、DN	多尿		1
                    10003087	zz_zhengzhuang	症状	12	TZXX、TCXX、TZXJ、	体重下降		1
                    10003088	zz_zhengzhuang	症状	13	FL	乏力		1
                    10003089	zz_zhengzhuang	症状	14	GJZT	关节肿痛		1
                    10003090	zz_zhengzhuang	症状	15	SLMH	视力模糊		1
                    10003091	zz_zhengzhuang	症状	16	SJMM	手脚麻木		1
                    10003092	zz_zhengzhuang	症状	17	SJ、NJ	尿急		1
                    10003093	zz_zhengzhuang	症状	18	ST、NT	尿痛		1
                    10003094	zz_zhengzhuang	症状	19	PM、BM、PL、BL、PB、	便秘		1
                    10003105	zz_zhengzhuang	症状	20	FX	腹泻		1
                    10003106	zz_zhengzhuang	症状	21	WXOT、EXOT	恶心呕吐		1
                    10003107	zz_zhengzhuang	症状	22	YH	眼花		1
                    10003108	zz_zhengzhuang	症状	23	EM	耳鸣		1
                    10003109	zz_zhengzhuang	症状	24	RFZT	乳房胀痛		1
                    10003110	zz_zhengzhuang	症状	99	QT、JT	其他		1
         * 
         * 
       2.  锻炼频率  dlpl
         *ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10003299	dlpl	锻炼频率	1		每天		1
            10003300	dlpl	锻炼频率	2		每周一次以上		1
            10003301	dlpl	锻炼频率	3		偶尔		1
            10003302	dlpl	锻炼频率	4		不锻炼		1
         * 
         * 
         *3. 饮食习惯  ysxg
         ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10002888	ysxg	饮食习惯	5		嗜油		1
            10002889	ysxg	饮食习惯	6		嗜糖		1
            10002890	ysxg	饮食习惯	1		荤素均衡		1
            10002891	ysxg	饮食习惯	2		荤食为主		1
            10002892	ysxg	饮食习惯	3		素食为主		1
            10002893	ysxg	饮食习惯	4		嗜盐		1
         * 
         * 
         4.吸烟情况 
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10003482	glkxyqk	管理卡吸烟情况	1		不吸烟		1
            10003483	glkxyqk	管理卡吸烟情况	2		已戒烟		1
            10003484	glkxyqk	管理卡吸烟情况	3		吸烟		1   
         * 
         5.饮酒频率  yjpl
         *ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10002832	yjpl	饮酒频率	1		从不		1
            10002833	yjpl	饮酒频率	2		偶尔		1
            10002834	yjpl	饮酒频率	3		经常		1
            10002835	yjpl	饮酒频率	4		每天		1
         * 
         6.是否戒酒
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10003111	sfjj	是否戒酒	1	WJJ	未戒酒		1
            10003112	sfjj	是否戒酒	2	YJJ	已戒酒		1
         * 
         7.饮酒种类
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
        10002836	yjzl	饮酒种类	1		白酒		1
        10002837	yjzl	饮酒种类	2		啤酒		1
        10002838	yjzl	饮酒种类	3		红酒		1
        10002839	yjzl	饮酒种类	4		黄酒		1
        10002840	yjzl	饮酒种类	99		其他		1
         * 
         8.口唇
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10002072	kc_houchun	口唇	1		红润		1
            10002073	kc_houchun	口唇	2		苍白		1
            10002074	kc_houchun	口唇	3		发绀		1
            10002075	kc_houchun	口唇	4		皲裂		1
            10002076	kc_houchun	口唇	5		疱疹		1
            10002077	kc_houchun	口唇	6	QT,JT	其他		1
         * 
         9.咽部
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10002748	yb_yanbu	咽部	1		无充血		1
            10002749	yb_yanbu	咽部	2		充血		1
            10002750	yb_yanbu	咽部	3		淋巴滤泡增生		1
            10002751	yb_yanbu	咽部	4	QT,JT	其他		1
         * 
         10.听力
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10002583	tingli	听力	1	TJ、TX	听见		1
            10002584	tingli	听力	2	TBQHWFTJ（EBHKZK	听不清或无法听见（耳鼻喉科专科就诊）		1
         * 
         11.运动功能
            10002894	yundong	运动功能	1	KSLWC	可顺利完成		1
            10002895	yundong	运动功能	2	WFDLWCQZRHYGDZ(	无法独立完成其中任何一个动作(上级医院就诊)		1
         * 
         12.正常异常
            10002936	zcyc	正常异常	1		正常		1
            10002937	zcyc	正常异常	2		异常		1
         * 
        13.下肢水肿
            10002739	xzsz	下肢水肿	4		双侧不对称		1
            10002740	xzsz	下肢水肿	5		双侧对称		1
            10002736	xzsz	下肢水肿	1		无		1
            10002737	xzsz	下肢水肿	2		左侧		1
            10002738	xzsz	下肢水肿	3		右侧		1
         * 
         14.
            10002932	zbdmbd	足背动脉搏动	1		未触及		1
            10002933	zbdmbd	足背动脉搏动	2		触及双侧对称		1
            10002934	zbdmbd	足背动脉搏动	3		触及左侧弱或消失		1
            10002935	zbdmbd	足背动脉搏动	4		触及右侧弱或消失		1
         * 
         15.肛门指诊
            10002492	gmzz	肛门指诊	1	WJYC、WXYC	未见异常		1
            10002493	gmzz	肛门指诊	2	CT	触痛		1
            10003051	gmzz	肛门指诊	3	BK	包块		1
            10003052	gmzz	肛门指诊	4	QLXYC	前列腺异常		1
            10003053	gmzz	肛门指诊	99	QT、JT	其他		1
         * 
         16.乳腺
            10002447	rx_ruxian	乳腺	1		未见异常		1
            10002448	rx_ruxian	乳腺	2		乳房切除		1
            10002449	rx_ruxian	乳腺	3		异常泌乳		1
            10002450	rx_ruxian	乳腺	4		乳腺包块		1
            10002451	rx_ruxian	乳腺	99		其他		1
         * 
         17.阴性阳性
            10002905	yxyx	阴性阳性	1		阴性		1
            10002906	yxyx	阴性阳性	2		阳性		1
         * 
         18.中药体质辨识
            10003047	zytzbs	中药体质辨识	1		是		1
            10003048	zytzbs	中药体质辨识	2		倾向是		1
            10003049	zytzbsphz	中药体质辨识平和质	1		是		1
            10003050	zytzbsphz	中药体质辨识平和质	2		基本是		1
         * 
         19.脑血管疾病
            10002290	nxgjb	脑血管疾病	1		未发现		1
            10002291	nxgjb	脑血管疾病	2		缺血性卒中		1
            10002292	nxgjb	脑血管疾病	3		脑出血		1
            10002293	nxgjb	脑血管疾病	4		蛛网膜下腔出血		1
            10002294	nxgjb	脑血管疾病	5		短暂性脑缺血发作		1
            10002295	nxgjb	脑血管疾病	99		其他		1
         * 
         20.肾脏疾病
            10002564	szjb	肾脏疾病	1		未发现		1
            10002565	szjb	肾脏疾病	2		糖尿病肾病		1
            10002566	szjb	肾脏疾病	3		肾功能衰竭		1
            10002567	szjb	肾脏疾病	4		急性肾炎		1
            10002568	szjb	肾脏疾病	5		慢性肾炎		1
            10002569	szjb	肾脏疾病	99		其他		1
         * 
         21.心脏疾病
            10002725	xzjb	心脏疾病	1		未发现		1
            10002726	xzjb	心脏疾病	2		心肌梗死		1
            10002727	xzjb	心脏疾病	3		心绞痛		1
            10002728	xzjb	心脏疾病	4		冠状动脉血运重建		1
            10002729	xzjb	心脏疾病	5		充血性心力衰竭		1
            10002730	xzjb	心脏疾病	6		心前区疼痛		1
            10002731	xzjb	心脏疾病	99		其他		1
         * 
         22.血管疾病
            10002678	xgjb	血管疾病	1		未发现		1
            10002679	xgjb	血管疾病	2		夹层动脉瘤		1
            10002680	xgjb	血管疾病	3		动脉闭塞性疾病		1
            10002681	xgjb	血管疾病	99		其他		1
         * 
         23.眼部疾病
            10002752	ybjb	眼部疾病	1		未发现		1
            10002753	ybjb	眼部疾病	2		视网膜出血或渗出		1
            10002754	ybjb	眼部疾病	3		视乳头水肿		1
            10002755	ybjb	眼部疾病	4		白内障		1
            10002756	ybjb	眼部疾病	99		其他		1
         * 
         24.未发现有
            10002630	wfxy	未发现有	1		未发现		1
            10002631	wfxy	未发现有	2		有		1
         * 
         25.齿列
                10003219	cl_chilei	齿列	1		正常		1
                10003220	cl_chilei	齿列	2		缺齿		1
                10003221	cl_chilei	齿列	3		龋齿		1
                10003222	cl_chilei	齿列	4		义齿(假牙)		1
                10003223	cl_chilei	齿列	5	QT,JT	其他		1
         * 
         26.体检有无异常
                10002585	tjywyc	体检有无异常	1		体检无异常		1
                10002586	tjywyc	体检有无异常	2		有异常		1
         * 
         27.体检有无异常
                10002585	tjywyc	体检有无异常	1		体检无异常		1
                10002586	tjywyc	体检有无异常	2		有异常		1
         * 
         28.健康指导
                10001896	jkzd	健康指导	1		定期随访		1
                10001897	jkzd	健康指导	2		纳入慢性病管理		1
                10001898	jkzd	健康指导	3		建议复查		1
                10001899	jkzd	健康指导	4		建议转诊		1
         * 
         29.危险因素指导
                10002653	wxyszd	危险因素指导	1		戒烟		1
                10002654	wxyszd	危险因素指导	2		健康饮酒		1
                10002655	wxyszd	危险因素指导	3		饮食		1
                10002656	wxyszd	危险因素指导	4		锻炼		1
                10002657	wxyszd	危险因素指导	97		减体重		1
                10002658	wxyszd	危险因素指导	98		建议疫苗接种		1
                10002659	wxyszd	危险因素指导	99		其他		1
         * 
         30.老年人状况评估
                10002990	zk_pg	老年人状况评估	1	NULL	满意	NULL	1
                10002991	zk_pg	老年人状况评估	2	NULL	基本满意	NULL	1
                10002992	zk_pg	老年人状况评估	3	NULL	说不清楚	NULL	1
                10002993	zk_pg	老年人状况评估	4	NULL	不太满意	NULL	1
                10002994	zk_pg	老年人状况评估	5	NULL	不满意	NULL	1
         * 
         31.老年人自理评估
                10002995	zl_pg	老年人自理评估	1	NULL	可自理(0～3分)	NULL	1
                10002996	zl_pg	老年人自理评估	2	NULL	轻度依赖(4～8分)	NULL	1
                10002997	zl_pg	老年人自理评估	3	NULL	中度依赖(9～18分)	NULL	1
                10002998	zl_pg	老年人自理评估	4	NULL	不能自理(≥19分)	NULL	1
         * 
         32.阴性阳性
                10002905	yxyx	阴性阳性	1		阴性		1
                10002906	yxyx	阴性阳性	2		阳性		1
         * 
         33.职业暴露详细
                10003131	baoluqk	职业暴露详细	1	W,M	无		1
                10003132	baoluqk	职业暴露详细	2	BX	不详		1
                10003133	baoluqk	职业暴露详细	3	Y	有		1
         * 
         34. pifu_pf  皮肤
            10002382	pifu_pf	皮肤	1		正常		1
            10002383	pifu_pf	皮肤	2		潮红		1
            10002384	pifu_pf	皮肤	3		苍白		1
            10002385	pifu_pf	皮肤	4		发绀		1
            10002386	pifu_pf	皮肤	5		黄染		1
            10002387	pifu_pf	皮肤	6		色素沉着		1
            10002388	pifu_pf	皮肤	99		其他		1
         * 
         35.
            10003492	gm_gongmo	巩膜	1		正常		1
            10003493	gm_gongmo	巩膜	2		黄染		1
            10003494	gm_gongmo	巩膜	3		充血		1
            10003495	gm_gongmo	巩膜	99		其他		1
         * 
         36.lbj2
         * 10002110  	lbj2	淋巴结	1		未触及		1
            10002111	lbj2	淋巴结	2		锁骨上		1
            10002112	lbj2	淋巴结	3		腋窝		1
            10002113	lbj2	淋巴结	4		其他		1
         * 
         37 . luoyin
         * 10002145	    luoyin	罗音	1	W、M	无		1
            10002146	luoyin	罗音	2	GLY	干罗音		1
            10002619	luoyin	罗音	3	SLY	湿罗音		1
            10002620	luoyin	罗音	4	QT、JT	其他		1
         * 
         38.心脏心律
         * 10002741 	xzxl	心脏心律	1		齐		1
            10002742	xzxl	心脏心律	2		不齐		1
            10002743	xzxl	心脏心律	3		绝对不齐		1
         * 
         39.
         * 10002667 	wy_wuyou	无有	1		无		1
            10002668	wy_wuyou	无有	2		有		1
            10002896	yw_youwu	有无	1		有		1
            10002897	yw_youwu	有无	2		无		1
         * 
        40.
         * 10002898 	ywyc	有无异常	1		未见异常		1
            10002899	ywyc	有无异常	99		异常		1
         */

        #region Fields

        #endregion

        #region Constructor
        public uc健康体检表1()
        {
            InitializeComponent();
        }

        #endregion

        #region Handle Events
        private void UC健康体检表_Load(object sender, EventArgs e)
        {
            InitView();

            DataBinder.BindingRadioEdit(radio是否戒酒, DataDictCache.Cache.t是否戒酒, "P_CODE");
            tb_健康体检Info tb = tb_健康体检DAL.Gettb_健康体检InfoById(Program.currentUser.ID);
            if (tb != null)//已经查体
            {
                DobindingDatasource(tb);
            }

            Binding_医师签字();
        }

        //Begin WXF 2018-11-21 | 15:57
        //绑定医师签字 
        private void Binding_医师签字()
        {
            dal_医生信息 dal医生信息 = new dal_医生信息();
            DataTable dt = dal医生信息.Get医生信息ToLookUp();

            lookUpEdit_医师签字.Properties.ValueMember = "b编码";
            lookUpEdit_医师签字.Properties.DisplayMember = "x医生姓名";
            lookUpEdit_医师签字.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("x医生姓名", "医生姓名"));
            lookUpEdit_医师签字.Properties.DataSource = dt;
            lookUpEdit_医师签字.EditValue = "请选择";
        }
        //End
        void 一般情况_体重指数_Enter(object sender, EventArgs e)
        {
            Decimal 身高 = 0;
            Decimal 体重 = 0;
            Decimal BMI = 0;
        }
        void 一般情况_体重_Leave(object sender, EventArgs e)
        {
            一般情况_体重指数_Enter(null, null);
            //Decimal 身高 = 0;
            //Decimal 体重 = 0;
            //Decimal BMI = 0;
            //if (string.IsNullOrEmpty(this.txt一般情况_身高.Txt1.Text) || string.IsNullOrEmpty(this.txt一般情况_体重.Txt1.Text)) return;
            //if (Decimal.TryParse(this.txt一般情况_身高.Txt1.Text, out 身高) && Decimal.TryParse(this.txt一般情况_体重.Txt1.Text, out 体重))
            //{
            //    BMI = Math.Round(体重 / ((身高 / 100) * (身高 / 100)), 2);
            //    this.txt一般情况_体重指数.Txt1.Text = BMI.ToString();
            //}
        }
        private void chk_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit checkEdit = (CheckEdit)sender;
            if (checkEdit == null) return;
            SetControlEnable(checkEdit);
        }
        private void chk其他_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit checkEdit = (CheckEdit)sender;
            if (checkEdit == null) return;
            string name = checkEdit.Name;
            string target = "txt" + name.Substring(3, name.IndexOf('_') - 2) + "其他";
            Control[] list = this.Controls.Find(target, true);
            if (list.Length == 1)
            {
                if (list[0] is TextEdit)
                {
                    TextEdit txt = (TextEdit)list[0];
                    SetTextEditEnable(checkEdit, txt);
                }
            }

        }
        private void radio生活方式_锻炼频率_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio生活方式_锻炼频率.EditValue == null ? "" : this.radio生活方式_锻炼频率.EditValue.ToString();
            if (!string.IsNullOrEmpty(index) && index == "4")//不锻炼
            {
                this.txt生活方式_锻炼方式.Enabled = false;
                this.txt生活方式_坚持锻炼时间.Enabled = false;
                this.txt生活方式_每次锻炼时间.Enabled = false;
            }
            else
            {
                this.txt生活方式_锻炼方式.Enabled = true;
                this.txt生活方式_坚持锻炼时间.Enabled = true;
                this.txt生活方式_每次锻炼时间.Enabled = true;
            }
        }
        private void radio吸烟情况_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio吸烟情况.EditValue == null ? "" : this.radio吸烟情况.EditValue.ToString();
            if (!string.IsNullOrEmpty(index) && index == "1")//不吸烟
            {
                this.txt日吸烟量.Enabled = false;
                this.txt开始吸烟年龄.Enabled = false;
                this.txt戒烟年龄.Enabled = false;
            }
            else if (!string.IsNullOrEmpty(index) && index == "2") //已戒烟
            {
                this.txt日吸烟量.Enabled = true;
                this.txt开始吸烟年龄.Enabled = true;
                this.txt戒烟年龄.Enabled = true;
            }
            else if (!string.IsNullOrEmpty(index) && index == "3") //戒烟
            {
                this.txt日吸烟量.Enabled = true;
                this.txt开始吸烟年龄.Enabled = true;
                this.txt戒烟年龄.Enabled = false;
            }
        }
        private void radio饮酒频率_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio饮酒频率.EditValue == null ? "" : this.radio饮酒频率.EditValue.ToString();
            if (!string.IsNullOrEmpty(index) && index == "1")//从不
            {
                this.txt日饮酒量.Enabled = false;
                this.radio是否戒酒.Enabled = false;
                this.txt戒酒年龄.Enabled = false;
                this.txt开始饮酒年龄.Enabled = false;
                this.radio近一年内是否曾醉酒.Enabled = false;
                SetFlowControlEnable(flow饮酒种类, false);
            }
            else
            {
                this.txt日饮酒量.Enabled = true;
                this.radio是否戒酒.Enabled = true;
                this.txt戒酒年龄.Enabled = true;
                this.txt开始饮酒年龄.Enabled = true;
                this.radio近一年内是否曾醉酒.Enabled = true;
                SetFlowControlEnable(flow饮酒种类, true);
            }
            chk饮酒种类_其他_CheckedChanged(null, null);
        }
        private void chk饮酒种类_其他_CheckedChanged(object sender, EventArgs e)
        {
            this.txt饮酒种类_其他.Enabled = chk饮酒种类_其他.Checked;
            this.txt饮酒种类_其他.Text = chk饮酒种类_其他.Checked ? this.txt饮酒种类_其他.Text : "";
        }

        private void radio老年人认知能力_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio老年人认知能力.EditValue == null ? "" : this.radio老年人认知能力.EditValue.ToString();
            if (index == null) return;

            if (index == "1")//阴性
                this.txt老年人认知能力总分.Enabled = true;
            else
            {
                this.txt老年人认知能力总分.Enabled = false;
                this.txt老年人认知能力总分.Text = "";
            }
        }
        private void radio老年人情感状态_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio老年人情感状态.EditValue == null ? "" : this.radio老年人情感状态.EditValue.ToString();
            if (index == "1")//阴性
                this.txt老年人情感状态总分.Enabled = true;
            else
            {
                this.txt老年人情感状态总分.Enabled = false;
                this.txt老年人情感状态总分.Text = "";
            }
        }
        private void radion职业病有无_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio职业病有无.EditValue == null ? "" : this.radio职业病有无.EditValue.ToString();
            if (index == "3")//有职业病
            {
                Set职业病Editable(true);
            }
            else
                Set职业病Editable(false);
        }

        private void radio粉尘防护措施有无_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = radio粉尘防护措施有无.EditValue == null ? "" : radio粉尘防护措施有无.EditValue.ToString();
            if (index == "3")//有
            {
                this.txt粉尘防护措施.Enabled = true;
            }
            else
            {
                this.txt粉尘防护措施.Enabled = false;
            }
        }
        private void radio放射物质防护措施有无_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = radio放射物质防护措施有无.EditValue == null ? "" : radio放射物质防护措施有无.EditValue.ToString();
            if (index == "3")//有
            {
                this.txt放射物质防护措施.Enabled = true;
            }
            else
            {
                this.txt放射物质防护措施.Enabled = false;
            }
        }
        private void radio物理因素防护措施有无_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = radio物理防护措施有无.EditValue == null ? "" : radio物理防护措施有无.EditValue.ToString();
            if (index == "3")//有
            {
                this.txt物理防护措施.Enabled = true;
            }
            else
            {
                this.txt物理防护措施.Enabled = false;
            }
        }
        private void radio化学物质防护措施有无_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = radio化学防护措施有无.EditValue == null ? "" : radio化学防护措施有无.EditValue.ToString();
            if (index == "3")//有
            {
                this.txt化学防护措施.Enabled = true;
            }
            else
            {
                this.txt化学防护措施.Enabled = false;
            }
        }
        private void radio职业病其他防护措施有无_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = radio职业病其他防护措施有无.EditValue == null ? "" : radio职业病其他防护措施有无.EditValue.ToString();
            if (index == "3")//有
            {
                this.txt职业病其他防护.Enabled = true;
            }
            else
            {
                this.txt职业病其他防护.Enabled = false;
            }
        }


        private void btn保存_Click(object sender, EventArgs e)
        {
            try
            {
                if (f保存检查())
                {
                    tb_健康体检Info tb = new tb_健康体检Info();

                    tb.身份证号 = Program.currentUser.ID;
                    tb.姓名 = Program.currentUser.Name;
                    tb.性别 = Program.currentUser.Gender;
                    tb.出生日期 = Program.currentUser.Birth;
                    tb.个人档案编号 = Program.currentUser.DocNo;
                    tb.症状 = ControlsHelper.GetFlowLayoutResult(flow症状).ToString();
                    tb.症状其他 = this.txt症状_其他.Text.Trim();
                    tb.老年人状况评估 = this.radio老年人健康状态评估.Text.Trim();
                    tb.老年人自理评估 = this.radio老年人生活自理能力评估.Text.Trim();
                    tb.老年人情感 = this.radio老年人情感状态.Text.Trim();
                    tb.老年人情感分 = this.txt老年人情感状态总分.Text.Trim();
                    tb.老年人认知 = this.radio老年人认知能力.Text.Trim();
                    tb.老年人认知分 = this.txt老年人认知能力总分.Text.Trim();

                    #region 生活方式
                    tb.锻炼频率 = this.radio生活方式_锻炼频率.Text.Trim();
                    tb.每次锻炼时间 = this.txt生活方式_每次锻炼时间.Txt1.Text.Trim();
                    tb.坚持锻炼时间 = this.txt生活方式_坚持锻炼时间.Txt1.Text.Trim();
                    tb.锻炼方式 = this.txt生活方式_锻炼方式.Text.Trim();
                    tb.饮食习惯 = ControlsHelper.GetFlowLayoutResult(flow饮食习惯).ToString();
                    tb.吸烟状况 = this.radio吸烟情况.Text.Trim();
                    tb.日吸烟量 = this.txt日吸烟量.Txt1.Text.Trim();
                    tb.开始吸烟年龄 = this.txt开始吸烟年龄.Txt1.Text.Trim();
                    tb.戒烟年龄 = this.txt戒烟年龄.Txt1.Text.Trim();
                    tb.饮酒频率 = this.radio饮酒频率.Text.Trim();
                    tb.日饮酒量 = this.txt日饮酒量.Txt1.Text.Trim();
                    tb.是否戒酒 = this.radio是否戒酒.Text.Trim();
                    tb.戒酒年龄 = this.txt戒酒年龄.Txt1.Text.Trim();
                    tb.开始饮酒年龄 = this.txt开始饮酒年龄.Txt1.Text.Trim();
                    tb.近一年内是否曾醉酒 = this.radio近一年内是否曾醉酒.Text;
                    tb.饮酒种类 = ControlsHelper.GetFlowLayoutResult(flow饮酒种类).ToString();
                    tb.饮酒种类其它 = this.txt饮酒种类_其他.Text.Trim();
                    string 有无职业病 = this.radio职业病有无.EditValue == null ? "" : this.radio职业病有无.Text.Trim();
                    tb.有无职业病 = 有无职业病;
                    if (有无职业病 == "3")//有
                    {
                        tb.具体职业 = this.txt工种.Text.Trim();
                        tb.从业时间 = this.txt从业时间.Text.Trim();
                        tb.粉尘 = this.txt粉尘.Text.Trim();
                        tb.粉尘防护有无 = this.radio粉尘防护措施有无.Text.Trim();
                        tb.粉尘防护措施 = this.txt粉尘防护措施.Text.Trim();
                        tb.放射物质 = this.txt放射物质.Text.Trim();
                        tb.放射物质防护措施有无 = this.radio放射物质防护措施有无.Text.Trim();
                        tb.放射物质防护措施其他 = this.txt放射物质防护措施.Text.Trim();
                        tb.物理因素 = this.txt物理因素.Text.Trim();
                        tb.物理防护有无 = this.radio物理防护措施有无.Text.Trim();
                        tb.物理防护措施 = this.txt物理防护措施.Text.Trim();
                        tb.化学物质 = this.txt化学因素.Text.Trim();
                        tb.化学物质防护 = this.radio化学防护措施有无.Text.Trim();
                        tb.化学物质具体防护 = this.txt化学防护措施.Text.Trim();
                        tb.职业病其他 = this.txt职业病其他.Text.Trim();
                        tb.其他防护有无 = this.radio职业病其他防护措施有无.Text.Trim();
                        tb.其他防护措施 = this.txt职业病其他防护.Text.Trim();
                    }

                    #endregion
                    tb.G_QLX = "";

                    //Begin WXF 2018-11-21 | 21:12
                    //保存医师签字 

                    if (string.IsNullOrEmpty(doctor_症状) || doctor_症状.Equals("请选择"))
                    {
                        //MessageBox.Show("请选择症状的责任医师!");
                        //return;
                    }
                    if (string.IsNullOrEmpty(doctor_生活方式) || doctor_生活方式.Equals("请选择"))
                    {
                        //MessageBox.Show("请选择生活方式的责任医师!");
                        //return;
                    }

                    Dictionary<tb_健康体检DAL.Module_体检, string> dic = new Dictionary<tb_健康体检DAL.Module_体检, string>();
                    dic.Add(tb_健康体检DAL.Module_体检.症状, doctor_症状);
                    dic.Add(tb_健康体检DAL.Module_体检.生活方式, doctor_生活方式);
                    tb.医师签字 = tb_健康体检DAL.JsonToStr_医师签字(Program.currentUser.DocNo, dic);
                    //End


                    //tb_健康体检Info list = tb_健康体检DAL.Gettb_健康体检InfoById(Program.currentUser.ID);
                    //bool result = false;

                    //result = tb_健康体检DAL.Addtb_健康体检(tb);

                    if (tb_健康体检DAL.UpdateSys_体检1(tb))
                    {
                        MessageBox.Show("保存成功！");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region Private Method
        private void Set职业病Editable(bool p)
        {
            this.txt工种.Enabled = p;
            this.txt从业时间.Enabled = p;

            this.txt粉尘.Enabled = p;
            this.radio粉尘防护措施有无.Enabled = p;
            //this.radio粉尘.EditValue = p ? "1" : "0";
            this.txt粉尘防护措施.Enabled = p;

            this.txt放射物质.Enabled = p;
            this.radio放射物质防护措施有无.Enabled = p;
            //this.radio放射.EditValue = p ? "1" : "0";
            this.txt放射物质防护措施.Enabled = p;

            this.txt物理因素.Enabled = p;
            this.radio物理防护措施有无.Enabled = p;
            //this.radio物理.EditValue = p ? "1" : "0";
            this.txt物理防护措施.Enabled = p;

            this.txt化学因素.Enabled = p;
            this.radio化学防护措施有无.Enabled = p;
            //this.radio化学.EditValue = p ? "1" : "0";
            this.txt化学防护措施.Enabled = p;

            this.txt职业病其他.Enabled = p;
            this.radio职业病其他防护措施有无.Enabled = p;
            //this.radio其他.EditValue = p ? "1" : "0";
            this.txt职业病其他防护.Enabled = p;
        }
        private bool f保存检查()
        {
            //string 出生日期 = _ds健康档案ForShow.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.出生日期].ToString();
            //if (!string.IsNullOrEmpty(出生日期))
            //{
            //    DateTime dt = DateTime.Now;
            //    if (DateTime.TryParse(出生日期, out dt))
            //    {
            //        if (DateTime.Now.Year - dt.Year >= 35)
            //        {
            //            if (this.txt一般情况_血压左侧.Txt1.Text.Trim() == "" && this.txt一般情况_血压左侧.Txt2.Text.Trim() == "" && this.txt一般情况_血压右侧.Txt1.Text.Trim() == "" && this.txt一般情况_血压右侧.Txt2.Text.Trim() == "")
            //            {
            //                if (Msg.AskQuestion("此人年龄已到35岁，是否输入血压值？"))
            //                {
            //                    this.txt一般情况_血压左侧.Txt1.Focus();
            //                    return false;
            //                }
            //            }
            //        }
            //    }
            //}

            //if (string.IsNullOrEmpty(this.dte体检日期.Text.Trim()))
            //{
            //    MessageBox.Show("体检日期是必填项，请输入体检日期!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    //Msg.Warning("体检日期是必填项，请输入体检日期");
            //    this.dte体检日期.Focus();
            //    return false;
            //}
            return true;
        }
        /// <summary>
        /// 第一个checkbox是否选中来决定 后续的checkbook的enable状态
        /// </summary>
        /// <param name="firstChk"></param>
        public void SetControlEnable(CheckEdit firstChk)
        {
            bool flag = firstChk.Checked;
            FlowLayoutPanel container = firstChk.Parent as FlowLayoutPanel;
            if (container == null) return;
            Control.ControlCollection controlList = container.Controls;
            for (int i = 1; i < controlList.Count; i++)
            {
                if (controlList[i] is CheckEdit)
                {
                    CheckEdit item = (CheckEdit)controlList[i];
                    item.Enabled = !flag;
                    if (flag)//选中了第一个控件
                    {
                        item.Checked = false;
                    }
                }
                else
                {
                    controlList[i].Enabled = false;
                }
            }
        }
        public void SetTextEditEnable(Control control, TextEdit textEdit)
        {
            if (control is CheckEdit)//CheckEdit 类型的其他
            {
                CheckEdit check = (CheckEdit)control;
                textEdit.Enabled = check.Checked;
                textEdit.Text = check.Checked ? textEdit.Text : "";
            }
            else if (true)
            {

            }
        }
        /// <summary>
        /// 设置FlowLayoutPanel中控件的  enable属性
        /// </summary>
        /// <param name="flowLayout"></param>
        /// <param name="p"></param>
        private void SetFlowControlEnable(FlowLayoutPanel flowLayout, bool p)
        {
            if (flowLayout == null || flowLayout.Controls.Count == 0) return;
            for (int i = 0; i < flowLayout.Controls.Count; i++)
            {
                Control item = flowLayout.Controls[i];
                item.Enabled = p;
            }
        }
        private void SetRadioGroupTxtEnable(RadioGroup radio, TextEdit txt)
        {
            string index = radio.EditValue == null ? "" : radio.EditValue.ToString();
            txt.Enabled = !string.IsNullOrEmpty(index) && index == "2" ? true : false;
            txt.Text = !string.IsNullOrEmpty(index) && index == "2" ? txt.Text : "";
        }
        //private object GetFlowLayoutResult(FlowLayoutPanel flow)
        //{
        //    if (flow == null || flow.Controls.Count == 0) return "";
        //    string result = "";
        //    for (int i = 0; i < flow.Controls.Count; i++)
        //    {
        //        if (flow.Controls[i].GetType() == typeof(CheckEdit))
        //        {
        //            CheckEdit chk = (CheckEdit)flow.Controls[i];
        //            if (chk == null) continue;
        //            if (chk.Checked)
        //            {
        //                result += chk.Tag + ",";
        //            }
        //        }
        //    }
        //    result = string.IsNullOrEmpty(result) ? result : result.Remove(result.Length - 1);
        //    return result;
        //}
        private void InitView()
        {
            this.TCG_Tags.SelectedTabPageIndex = 0;
            SetControlEnable(this.chk症状_无症状);
            //锻炼频率
            this.radio生活方式_锻炼频率.SelectedIndex = 3;
            this.txt生活方式_锻炼方式.Enabled = false;
            this.txt生活方式_坚持锻炼时间.Enabled = false;
            this.txt生活方式_每次锻炼时间.Enabled = false;

            this.txt生活方式_每次锻炼时间.Txt1.Properties.Mask.EditMask = "n0";
            this.txt生活方式_每次锻炼时间.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt生活方式_每次锻炼时间.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt生活方式_每次锻炼时间.Txt1.Width = 60;
            this.txt生活方式_每次锻炼时间.Lbl1.Width = 50;

            this.txt生活方式_坚持锻炼时间.Txt1.Properties.Mask.EditMask = "n0";
            this.txt生活方式_坚持锻炼时间.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt生活方式_坚持锻炼时间.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt日吸烟量.Txt1.Properties.Mask.EditMask = "n0";
            this.txt日吸烟量.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt日吸烟量.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt开始吸烟年龄.Txt1.Properties.Mask.EditMask = "n0";
            this.txt开始吸烟年龄.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt开始吸烟年龄.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt戒烟年龄.Txt1.Properties.Mask.EditMask = "n0";
            this.txt戒烟年龄.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt戒烟年龄.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt开始饮酒年龄.Txt1.Properties.Mask.EditMask = "n0";
            this.txt开始饮酒年龄.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt开始饮酒年龄.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt日饮酒量.Txt1.Properties.Mask.EditMask = "n0";
            this.txt日饮酒量.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt日饮酒量.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt戒酒年龄.Txt1.Properties.Mask.EditMask = "n0";
            this.txt戒酒年龄.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt戒酒年龄.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            //this.txt左眼视力.Properties.Mask.EditMask = @"n1";
            //this.txt左眼视力.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt左眼视力.Properties.Mask.UseMaskAsDisplayFormat = true;

            //this.txt矫正左眼视力.Properties.Mask.EditMask = @"n1";
            //this.txt矫正左眼视力.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt矫正左眼视力.Properties.Mask.UseMaskAsDisplayFormat = true;

            //this.txt右眼视力.Properties.Mask.EditMask = @"n1";
            //this.txt右眼视力.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt右眼视力.Properties.Mask.UseMaskAsDisplayFormat = true;

            //this.txt矫正右眼视力.Properties.Mask.EditMask = @"n1";
            //this.txt矫正右眼视力.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt矫正右眼视力.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt从业时间.Properties.Mask.EditMask = "n0";
            this.txt从业时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt从业时间.Properties.Mask.UseMaskAsDisplayFormat = true;
        }
        private void DobindingDatasource(tb_健康体检Info tb)
        {
            if (tb != null)
            {
                ControlsHelper.SetFlowLayoutResult(tb.症状.ToString(), flow症状);
                this.txt症状_其他.EditValue = tb.症状其他;

                #region 一般情况
                this.radio老年人健康状态评估.EditValue = tb.老年人状况评估;
                this.radio老年人生活自理能力评估.EditValue = tb.老年人自理评估;
                this.radio老年人情感状态.EditValue = tb.老年人情感;
                this.txt老年人情感状态总分.EditValue = tb.老年人情感分;
                this.radio老年人认知能力.EditValue = tb.老年人认知;
                this.txt老年人认知能力总分.EditValue = tb.老年人认知分;
                #endregion

                #region 生活方式
                this.radio生活方式_锻炼频率.EditValue = tb.锻炼频率;
                this.txt生活方式_每次锻炼时间.Txt1.EditValue = tb.每次锻炼时间;
                this.txt生活方式_坚持锻炼时间.Txt1.EditValue = tb.坚持锻炼时间;
                this.txt生活方式_锻炼方式.EditValue = tb.锻炼方式;
                ControlsHelper.SetFlowLayoutResult(tb.饮食习惯.ToString(), flow饮食习惯);

                this.radio吸烟情况.EditValue = tb.吸烟状况;
                this.txt日吸烟量.Txt1.EditValue = tb.日吸烟量;
                this.txt开始吸烟年龄.Txt1.EditValue = tb.开始吸烟年龄;
                this.txt戒烟年龄.Txt1.EditValue = tb.戒烟年龄;

                this.radio饮酒频率.EditValue = tb.饮酒频率;
                this.txt日饮酒量.Txt1.EditValue = tb.日饮酒量;
                this.radio是否戒酒.EditValue = tb.是否戒酒;
                this.txt戒酒年龄.Txt1.EditValue = tb.戒酒年龄;
                this.txt开始饮酒年龄.Txt1.EditValue = tb.开始饮酒年龄;
                this.radio近一年内是否曾醉酒.EditValue = tb.近一年内是否曾醉酒;
                ControlsHelper.SetFlowLayoutResult(tb.饮酒种类.ToString(), flow饮酒种类);
                this.txt饮酒种类_其他.EditValue = tb.饮酒种类其它;

                this.radio职业病有无.EditValue = tb.有无职业病 == null ? "1" : tb.有无职业病;

                this.txt工种.EditValue = tb.具体职业;
                this.txt从业时间.EditValue = tb.从业时间;
                this.txt粉尘.EditValue = tb.粉尘;
                this.radio粉尘防护措施有无.EditValue = tb.粉尘防护有无;
                this.txt粉尘防护措施.EditValue = tb.粉尘防护措施;
                this.txt放射物质.EditValue = tb.放射物质;
                this.radio放射物质防护措施有无.EditValue = tb.放射物质防护措施有无;
                this.txt放射物质防护措施.EditValue = tb.放射物质防护措施其他;
                this.txt物理因素.EditValue = tb.物理因素;
                this.radio物理防护措施有无.EditValue = tb.物理防护有无;
                this.txt物理防护措施.EditValue = tb.物理防护措施;
                this.txt化学因素.EditValue = tb.化学物质;
                this.radio化学防护措施有无.EditValue = tb.化学物质防护;
                this.txt化学防护措施.EditValue = tb.化学物质具体防护;
                this.txt职业病其他.EditValue = tb.职业病其他;
                this.radio职业病其他防护措施有无.EditValue = tb.其他防护有无;
                this.txt职业病其他防护.EditValue = tb.其他防护措施;

                #endregion
            }
        }
        #endregion

        private void btn上一页_Click(object sender, EventArgs e)
        {
            int index = this.TCG_Tags.SelectedTabPageIndex;
            if (index != 0)
                this.TCG_Tags.SelectedTabPageIndex = index - 1;
        }

        private void btn下一页_Click(object sender, EventArgs e)
        {
            int index = this.TCG_Tags.SelectedTabPageIndex;
            if (index != TCG_Tags.TabPages.Count)
                this.TCG_Tags.SelectedTabPageIndex = index + 1;
            //showTab();
        }

        private void tabGroup_SelectedPageChanged(object sender, LayoutTabPageChangedEventArgs e)
        {
            switch (e.Page.Text)
            {
                case "症状":
                    layoutControlItem_医师签字.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lookUpEdit_医师签字.EditValue = string.IsNullOrEmpty(doctor_症状) ? "请选择" : doctor_症状;
                    break;
                case "体育锻炼":
                    layoutControlItem_医师签字.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lookUpEdit_医师签字.EditValue = string.IsNullOrEmpty(doctor_生活方式) ? "请选择" : doctor_生活方式;
                    break;
                default:
                    layoutControlItem_医师签字.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    break;
            }
        }

        string doctor_症状 = string.Empty;
        string doctor_生活方式 = string.Empty;

        private void lookUpEdit_医师签字_EditValueChanged(object sender, EventArgs e)
        {
            string PageName = TCG_Tags.SelectedTabPage.Text;

            switch (PageName)
            {
                case "症状":
                    doctor_症状 = lookUpEdit_医师签字.EditValue.ToString();
                    break;
                case "体育锻炼":
                    doctor_生活方式 = lookUpEdit_医师签字.EditValue.ToString();
                    break;
                default:
                    break;
            }
        }

        //Begin WXF 2019-02-19 | 15:30
        //获取参照数据并给控件赋值 
        private void sBut_Get参照数据_Click(object sender, EventArgs e)
        {
            sBut_Get参照数据.Enabled = false;

            dal_健康体检_Reference dal健康体检Ref = new dal_健康体检_Reference();
            tb_健康体检_Reference tb健康体检Ref = new tb_健康体检_Reference();

            tb健康体检Ref = dal健康体检Ref.GetOneRow(Program.currentUser.DocNo);

            if (tb健康体检Ref != null)
            {
                switch (TCG_Tags.SelectedTabPageName)
                {
                    case "LCG_体育锻炼":
                        Set_LCG_体育锻炼(tb健康体检Ref);
                        break;
                    case "LCG_饮食习惯":
                        Set_LCG_饮食习惯(tb健康体检Ref);
                        break;
                    case "LCG_吸烟情况":
                        Set_LCG_吸烟情况(tb健康体检Ref);
                        break;
                    case "LCG_饮酒情况":
                        Set_LCG_饮酒情况(tb健康体检Ref);
                        break;
                    case "LCG_职业病":
                        Set_LCG_职业病(tb健康体检Ref);
                        break;
                    default:
                        MessageBox.Show("此页未设置参照数据!");
                        break;
                }
            }
            else
            {
                MessageBox.Show("此人无去年数据做参照!");
            }

            sBut_Get参照数据.Enabled = true;
        }

        private void Set_LCG_职业病(tb_健康体检_Reference tb健康体检Ref)
        {
            if (!string.IsNullOrEmpty(tb健康体检Ref.有无职业病) && !"1".Equals(tb健康体检Ref.有无职业病))
            {
                radio职业病有无.EditValue = tb健康体检Ref.有无职业病;
                txt工种.Text = tb健康体检Ref.具体职业;
                txt从业时间.Text = tb健康体检Ref.从业时间;

                txt粉尘.Text = tb健康体检Ref.粉尘;
                radio粉尘防护措施有无.EditValue = string.IsNullOrEmpty(tb健康体检Ref.粉尘防护有无) ? "1" : tb健康体检Ref.粉尘防护有无;
                txt粉尘防护措施.Text = tb健康体检Ref.粉尘防护措施;

                txt放射物质.Text = tb健康体检Ref.放射物质;
                radio放射物质防护措施有无.EditValue = string.IsNullOrEmpty(tb健康体检Ref.放射物质防护措施有无) ? "1" : tb健康体检Ref.放射物质防护措施有无;
                txt放射物质防护措施.Text = tb健康体检Ref.放射物质防护措施其他;

                txt物理因素.Text = tb健康体检Ref.物理因素;
                radio物理防护措施有无.EditValue = string.IsNullOrEmpty(tb健康体检Ref.物理防护有无) ? "1" : tb健康体检Ref.物理防护有无;
                txt物理防护措施.Text = tb健康体检Ref.物理防护措施;

                txt化学因素.Text = tb健康体检Ref.化学物质;
                radio化学防护措施有无.EditValue = string.IsNullOrEmpty(tb健康体检Ref.化学物质防护) ? "1" : tb健康体检Ref.化学物质防护;
                txt化学防护措施.Text = tb健康体检Ref.化学物质具体防护;

                txt职业病其他.Text = tb健康体检Ref.职业病其他;
                radio职业病其他防护措施有无.EditValue = string.IsNullOrEmpty(tb健康体检Ref.其他防护有无) ? "1" : tb健康体检Ref.其他防护有无;
                txt职业病其他防护.Text = tb健康体检Ref.其他防护措施;
            }
        }

        private void Set_LCG_饮酒情况(tb_健康体检_Reference tb健康体检Ref)
        {
            radio饮酒频率.EditValue = string.IsNullOrEmpty(tb健康体检Ref.饮酒频率) ? "1" : tb健康体检Ref.饮酒频率;
            txt日饮酒量.Txt1.Text = tb健康体检Ref.日饮酒量;
            radio是否戒酒.EditValue = string.IsNullOrEmpty(tb健康体检Ref.是否戒酒) ? "" : tb健康体检Ref.是否戒酒;
            txt戒酒年龄.Txt1.Text = tb健康体检Ref.戒酒年龄;
            txt开始饮酒年龄.Txt1.Text = tb健康体检Ref.开始饮酒年龄;
            radio近一年内是否曾醉酒.EditValue = string.IsNullOrEmpty(tb健康体检Ref.近一年内是否曾醉酒) ? "" : tb健康体检Ref.近一年内是否曾醉酒;
            if (!string.IsNullOrEmpty(tb健康体检Ref.饮酒种类))
            {
                List<string> ls_饮酒种类 = tb健康体检Ref.饮酒种类.Split(',').ToList<string>();
                if (ls_饮酒种类.IndexOf("1") >= 0) chk饮酒种类_白酒.Checked = true;
                if (ls_饮酒种类.IndexOf("2") >= 0) chk饮酒种类_啤酒.Checked = true;
                if (ls_饮酒种类.IndexOf("3") >= 0) chk饮酒种类_红酒.Checked = true;
                if (ls_饮酒种类.IndexOf("4") >= 0) chk饮酒种类_黄酒.Checked = true;
                if (ls_饮酒种类.IndexOf("99") >= 0)
                {
                    chk饮酒种类_其他.Checked = true;
                    txt饮酒种类_其他.Text = tb健康体检Ref.饮酒种类其它;
                }
            }
        }

        private void Set_LCG_吸烟情况(tb_健康体检_Reference tb健康体检Ref)
        {
            radio吸烟情况.EditValue = string.IsNullOrEmpty(tb健康体检Ref.吸烟状况) ? "1" : tb健康体检Ref.吸烟状况;
            txt日吸烟量.Txt1.Text = tb健康体检Ref.日吸烟量;
            txt开始吸烟年龄.Txt1.Text = tb健康体检Ref.开始吸烟年龄;
            txt戒烟年龄.Txt1.Text = tb健康体检Ref.戒烟年龄;
        }

        private void Set_LCG_饮食习惯(tb_健康体检_Reference tb健康体检Ref)
        {
            if (!string.IsNullOrEmpty(tb健康体检Ref.饮食习惯))
            {
                List<string> ls_饮食 = tb健康体检Ref.饮食习惯.Split(',').ToList<string>();
                if (ls_饮食.IndexOf("1") >= 0) chk饮食习惯_荤素均衡.Checked = true;
                if (ls_饮食.IndexOf("2") >= 0) chk饮食习惯_荤食为主.Checked = true;
                if (ls_饮食.IndexOf("3") >= 0) chk饮食习惯_素食为主.Checked = true;
                if (ls_饮食.IndexOf("4") >= 0) chk饮食习惯_嗜盐.Checked = true;
                if (ls_饮食.IndexOf("5") >= 0) chk饮食习惯_嗜油.Checked = true;
                if (ls_饮食.IndexOf("6") >= 0) chk饮食习惯_嗜糖.Checked = true;
            }
        }

        private void Set_LCG_体育锻炼(tb_健康体检_Reference tb健康体检Ref)
        {
            radio生活方式_锻炼频率.EditValue = string.IsNullOrEmpty(tb健康体检Ref.锻炼频率) ? "4" : tb健康体检Ref.锻炼频率;
            txt生活方式_每次锻炼时间.Txt1.Text = tb健康体检Ref.每次锻炼时间;
            txt生活方式_坚持锻炼时间.Txt1.Text = tb健康体检Ref.坚持锻炼时间;
            txt生活方式_锻炼方式.Text = tb健康体检Ref.锻炼方式;
        }
        //End
    }
}
