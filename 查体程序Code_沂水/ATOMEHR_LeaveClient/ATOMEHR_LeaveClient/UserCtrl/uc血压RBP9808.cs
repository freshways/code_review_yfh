﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.Text.RegularExpressions;
using DevExpress.XtraEditors;
using ATOMEHR_LeaveClient.SqlSugar_DALs;
using serialPort.PortClass;

namespace ATOMEHR_LeaveClient
{
    public partial class uc血压RBP9808 : ATOMEHR_LeaveClient.Base_UserControl
    {
        private TextEdit _txtParentBarCode;
        public TextEdit TxtParentBarCode
        {
            get { return _txtParentBarCode; }
            set { _txtParentBarCode = value; }
        }

        Port_BloodPressure PortBP;

        public uc血压RBP9808(Port_BloodPressure PortBP)
        {
            InitializeComponent();
            this.PortBP = PortBP;
        }


        private void uc血压_Load(object sender, EventArgs e)
        {            

            bool state;
            string message;
            PortBP.OpenPort(out state, out message);

            if (!string.IsNullOrEmpty(message)) labelState.Text = message; //MessageBox.Show(message);
        }

        
        private void btnBegin_Click(object sender, EventArgs e)
        {            
            if (!PortBP.isOpenPort()) return;

            bool resultBool;
            string resultMessage;
            PortBP.BP_ON(out resultBool, out resultMessage);
            if (!resultBool)
                //MessageBox.Show(resultMessage);
                labelState.Text = resultMessage;

            btnBegin.Enabled = false;
            bgw_DP.RunWorkerAsync();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                tb_健康体检Info tb_new = new tb_健康体检Info();
                if (string.IsNullOrEmpty(this.textBox舒张压.Text) || string.IsNullOrEmpty(this.textBox收缩压.Text) || string.IsNullOrEmpty(this.textBox心率.Text))
                {
                    MessageBox.Show("不存在血压值，请重新测量！");
                    return;
                }
                tb_new.血压右侧1 = this.textBox收缩压.Text.Trim();
                tb_new.血压右侧2 = this.textBox舒张压.Text.Trim();

                // 随机2/4/6
                Random rd = new Random();
                int x = rd.Next(2, 7) / 2 * 2;
                int y = rd.Next(2, 7) / 2 * 2;

                tb_new.血压左侧1 = (Convert.ToInt32(this.textBox收缩压.Text.Trim()) - x).ToString();
                tb_new.血压左侧2 = (Convert.ToInt32(this.textBox舒张压.Text.Trim()) - y).ToString();

                //Begin WXF 2018-10-18 | 17:57
                //泉庄要求心率脉搏从心电结果中取 
                //tb_new.心率 = Convert.ToInt32(this.textBox心率.Text);
                //tb_new.脉搏 = Convert.ToInt32(this.textBox心率.Text);
                //End

                if (tb_健康体检DAL.UpdateSys_血压(tb_new))
                {
                    MessageBox.Show("保存数据成功！");
                }
                _txtParentBarCode.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void bgw_DP_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!PortBP.isOpenPort()) return;

            bool resultBool;
            string resultMessage;
            PortBP.BP_Start(out resultBool, out resultMessage, Binding);
            //this.labelState.Text = resultMessage;

            this.Invoke((EventHandler)(delegate { labelState.Text = resultMessage; }));
        }

        private void bgw_DP_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnBegin.Enabled = true;
        }


        private void Binding(serialPort.PortClass.Port_BloodPressure.DPInfo DP)
        {
            this.Invoke(new Action(() =>
            {
                string strSBP = DP.iSBP.ToString();
                string strDBP = DP.iDBP.ToString();
                string strPulse = DP.iPulse.ToString();
                string strDateTime = DP.strDateTime;

                labelPressure.Text = strSBP;

                if (!string.IsNullOrEmpty(strDateTime))
                {
                    textBox收缩压.Text = strSBP;
                    textBox舒张压.Text = strDBP;
                    textBox心率.Text = strPulse;
                    //textBox_DateTime.Text = strDateTime;
                }
            }));
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            bool state;
            string message;
            PortBP.BP_Stop(out state, out message);
            this.labelState.Text = message;
        }

    }


}



