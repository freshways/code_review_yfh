﻿namespace ATOMEHR_LeaveClient
{
    partial class uc心电PC80B
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc心电PC80B));
            this.lblGainValue = new System.Windows.Forms.Label();
            this.labelState = new System.Windows.Forms.Label();
            this.labelCom = new System.Windows.Forms.Label();
            this.lblMSG = new System.Windows.Forms.Label();
            this.RealTimeWave = new System.Windows.Forms.Panel();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.btnBegin = new DevExpress.XtraEditors.SimpleButton();
            this.txtHRValue = new DevExpress.XtraEditors.TextEdit();
            this.txtResultValue = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtHRValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtResultValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblGainValue
            // 
            this.lblGainValue.Font = new System.Drawing.Font("宋体", 12F);
            this.lblGainValue.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblGainValue.Location = new System.Drawing.Point(206, 205);
            this.lblGainValue.Name = "lblGainValue";
            this.lblGainValue.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.lblGainValue.Size = new System.Drawing.Size(70, 23);
            this.lblGainValue.TabIndex = 13;
            this.lblGainValue.Text = "X2";
            // 
            // labelState
            // 
            this.labelState.Font = new System.Drawing.Font("宋体", 12F);
            this.labelState.ForeColor = System.Drawing.SystemColors.Highlight;
            this.labelState.Location = new System.Drawing.Point(99, 205);
            this.labelState.Name = "labelState";
            this.labelState.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.labelState.Size = new System.Drawing.Size(103, 23);
            this.labelState.TabIndex = 14;
            this.labelState.Text = "状态:等待";
            // 
            // labelCom
            // 
            this.labelCom.Font = new System.Drawing.Font("宋体", 12F);
            this.labelCom.ForeColor = System.Drawing.SystemColors.Highlight;
            this.labelCom.Location = new System.Drawing.Point(7, 205);
            this.labelCom.Name = "labelCom";
            this.labelCom.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.labelCom.Size = new System.Drawing.Size(88, 23);
            this.labelCom.TabIndex = 15;
            this.labelCom.Text = "COM:";
            // 
            // lblMSG
            // 
            this.lblMSG.BackColor = System.Drawing.SystemColors.Control;
            this.lblMSG.Font = new System.Drawing.Font("宋体", 12F);
            this.lblMSG.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblMSG.Location = new System.Drawing.Point(280, 205);
            this.lblMSG.Name = "lblMSG";
            this.lblMSG.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.lblMSG.Size = new System.Drawing.Size(323, 23);
            this.lblMSG.TabIndex = 16;
            this.lblMSG.Text = "MSG:";
            // 
            // RealTimeWave
            // 
            this.RealTimeWave.Location = new System.Drawing.Point(7, 232);
            this.RealTimeWave.Name = "RealTimeWave";
            this.RealTimeWave.Size = new System.Drawing.Size(596, 196);
            this.RealTimeWave.TabIndex = 9;
            this.RealTimeWave.Paint += new System.Windows.Forms.PaintEventHandler(this.RealTimeWave_Paint);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.RealTimeWave);
            this.layoutControl1.Controls.Add(this.btn保存);
            this.layoutControl1.Controls.Add(this.btnBegin);
            this.layoutControl1.Controls.Add(this.txtHRValue);
            this.layoutControl1.Controls.Add(this.labelState);
            this.layoutControl1.Controls.Add(this.labelCom);
            this.layoutControl1.Controls.Add(this.lblGainValue);
            this.layoutControl1.Controls.Add(this.lblMSG);
            this.layoutControl1.Controls.Add(this.txtResultValue);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(587, 49, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(610, 435);
            this.layoutControl1.TabIndex = 23;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btn保存
            // 
            this.btn保存.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.btn保存.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.btn保存.Appearance.Options.UseFont = true;
            this.btn保存.Appearance.Options.UseForeColor = true;
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(306, 163);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(297, 38);
            this.btn保存.StyleController = this.layoutControl1;
            this.btn保存.TabIndex = 20;
            this.btn保存.Text = "保存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // btnBegin
            // 
            this.btnBegin.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.btnBegin.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.btnBegin.Appearance.Options.UseFont = true;
            this.btnBegin.Appearance.Options.UseForeColor = true;
            this.btnBegin.Image = ((System.Drawing.Image)(resources.GetObject("btnBegin.Image")));
            this.btnBegin.Location = new System.Drawing.Point(7, 163);
            this.btnBegin.Name = "btnBegin";
            this.btnBegin.Size = new System.Drawing.Size(295, 38);
            this.btnBegin.StyleController = this.layoutControl1;
            this.btnBegin.TabIndex = 19;
            this.btnBegin.Text = "重新开始";
            this.btnBegin.Click += new System.EventHandler(this.btnBegin_Click);
            // 
            // txtHRValue
            // 
            this.txtHRValue.EditValue = "";
            this.txtHRValue.Location = new System.Drawing.Point(7, 41);
            this.txtHRValue.Name = "txtHRValue";
            this.txtHRValue.Properties.Appearance.Font = new System.Drawing.Font("宋体", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHRValue.Properties.Appearance.ForeColor = System.Drawing.Color.Lime;
            this.txtHRValue.Properties.Appearance.Options.UseFont = true;
            this.txtHRValue.Properties.Appearance.Options.UseForeColor = true;
            this.txtHRValue.Size = new System.Drawing.Size(596, 40);
            this.txtHRValue.StyleController = this.layoutControl1;
            this.txtHRValue.TabIndex = 17;
            // 
            // txtResultValue
            // 
            this.txtResultValue.EditValue = "";
            this.txtResultValue.Location = new System.Drawing.Point(7, 119);
            this.txtResultValue.Name = "txtResultValue";
            this.txtResultValue.Properties.Appearance.Font = new System.Drawing.Font("宋体", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResultValue.Properties.Appearance.ForeColor = System.Drawing.Color.Lime;
            this.txtResultValue.Properties.Appearance.Options.UseFont = true;
            this.txtResultValue.Properties.Appearance.Options.UseForeColor = true;
            this.txtResultValue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtResultValue.Properties.Items.AddRange(new object[] {
            "节律无异常。",
            "疑似心跳稍快，请注意休息。",
            "疑似心跳过快，请注意休息。",
            "疑似阵发性心跳过快，请咨询医生。",
            "疑似心跳稍缓，请注意休息。",
            "疑似心跳过缓，请注意休息。",
            "疑似心跳间期缩短，请咨询医生。",
            "疑似心跳间期不规则，请咨询医生。",
            "疑似心跳稍快伴有心跳间期缩短，请咨询医生。",
            "疑似心跳稍缓伴有心跳间期缩短，请咨询医生。",
            "疑似心跳稍缓伴有心跳间期不规则，请咨询医生。",
            "波形有漂移。",
            "疑似心跳过快伴有波形漂移，请咨询医生。",
            "疑似心跳过缓伴有波形漂移，请咨询医生。",
            "疑似心跳间期缩短伴有波形漂移，请咨询医生。",
            "疑似心跳间期不规则伴有波形漂移，请咨询医生。"});
            this.txtResultValue.Size = new System.Drawing.Size(596, 40);
            this.txtResultValue.StyleController = this.layoutControl1;
            this.txtResultValue.TabIndex = 18;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(610, 435);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.labelCom;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 198);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(92, 27);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.labelState;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(92, 198);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(107, 27);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.lblMSG;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(273, 198);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(327, 27);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F);
            this.layoutControlItem5.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem5.Control = this.txtHRValue;
            this.layoutControlItem5.CustomizationFormText = "心率/HR:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(600, 78);
            this.layoutControlItem5.Text = "(结果发送完成后显示)心率/HR:";
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(359, 31);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F);
            this.layoutControlItem6.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem6.Control = this.txtResultValue;
            this.layoutControlItem6.CustomizationFormText = "检查结果:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 78);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(600, 78);
            this.layoutControlItem6.Text = "检查结果:";
            this.layoutControlItem6.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(359, 31);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.lblGainValue;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(199, 198);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(74, 27);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btnBegin;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 156);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(299, 42);
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.btn保存;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(299, 156);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(301, 42);
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.RealTimeWave;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 225);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(600, 200);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // uc心电PC80B
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "uc心电PC80B";
            this.Size = new System.Drawing.Size(610, 435);
            this.Load += new System.EventHandler(this.uc心电PC80B_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtHRValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtResultValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblGainValue;
        private System.Windows.Forms.Label labelState;
        private System.Windows.Forms.Label labelCom;
        private System.Windows.Forms.Label lblMSG;
        private System.Windows.Forms.Panel RealTimeWave;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraEditors.SimpleButton btnBegin;
        private DevExpress.XtraEditors.TextEdit txtHRValue;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.ComboBoxEdit txtResultValue;

    }
}
