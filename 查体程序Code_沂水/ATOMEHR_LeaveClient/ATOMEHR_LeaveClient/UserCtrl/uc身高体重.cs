﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.Text.RegularExpressions;
using DevExpress.XtraEditors;
using ATOMEHR_LeaveClient.SqlSugar_DALs;
using ATOMEHR_LeaveClient.Util;

namespace ATOMEHR_LeaveClient
{
    public partial class uc身高体重 : UserControl
    {
        private TextEdit _txtParentBarCode;
        public TextEdit TxtParentBarCode
        {
            get { return _txtParentBarCode; }
            set { _txtParentBarCode = value; }
        }

        IntPtr hBluetooth;
        int nCOMID = 0; //端口

        private SerialPort comm = new SerialPort();
        private long received_count = 0;//接收计数

        private bool Listening = false;//是否没有执行完invoke相关操作
        private bool Closing = false;//是否正在关闭串口，执行Application.DoEvents，并阻止再次invoke
        private List<byte> buffer = new List<byte>(4096);//默认分配1页内存，并始终限制不允许超过
        private byte[] binary_data_1 = new byte[17];

        public uc身高体重()
        {
            InitializeComponent();
        }

        public uc身高体重(SerialPort spt)
        {
            InitializeComponent();
            comm = spt;
        }

        private void txt体重_EditValueChanged(object sender, EventArgs e)
        {
            //增加下面一行--看这里!!!!!
            chk减体重.Checked = false;

            Decimal 身高 = 0;
            Decimal 体重 = 0;
            Decimal BMI = 0;
            //if (string.IsNullOrEmpty(this.txt身高.Text) || string.IsNullOrEmpty(this.txt体重.Text)) return;
            if (string.IsNullOrEmpty(this.txt身高.Text)) return;
            //定制腰围 ------------王森
            this.txt腰围.SelectedIndex = 2;//80
            //定制腰围 ------------王森
            //if (Decimal.TryParse(this.txt身高.Text, out 身高))
            //{
            //    this.txt腰围.Text = (身高 * 0.401m).ToString();
            //}
            if (string.IsNullOrEmpty(this.txt体重.Text)) return;

            if (Decimal.TryParse(this.txt身高.Text, out 身高) && Decimal.TryParse(this.txt体重.Text, out 体重))
            {
                if (身高 <= 0 || 体重 <= 0)
                {
                    //MessageBox.Show("请站稳扶好！身高/体重没有正确显示！");
                    return;//遇到除数是零的直接退出
                }
                BMI = Math.Round(体重 / ((身高 / 100) * (身高 / 100)), 2);
                this.txt体重指数.Text = BMI.ToString();
                if (BMI > 24)
                {
                    chk减体重.Checked = true;
                }
                else
                {
                    chk减体重.Checked = false;
                }
                //定制腰围 ------------王森   添加时 一定要注意顺序

                if (BMI < 23)
                {
                    this.txt腰围.SelectedIndex = 1;//70
                }
                if (BMI > 24)
                {
                    this.txt腰围.SelectedIndex = 3;//90
                }
                if (BMI > 25)
                {
                    this.txt腰围.SelectedIndex = 4;//100
                }
                if (BMI > 26)
                {
                    this.txt腰围.SelectedIndex = 5;//110
                }
                //定制腰围 ------------王森
            }
        }

        private void chk减体重_CheckedChanged(object sender, EventArgs e)
        {
            bool index = this.chk减体重.Checked;
            if (index)
            {
                this.txt减体重目标.Enabled = true;
                //计算符合条件的体重  BMI = 体重/身高/身高   身高单位  米
                Decimal 身高 = 0;
                Decimal 体重 = 0;
                Decimal BMI = 24.0m;
                if (string.IsNullOrEmpty(this.txt身高.Text) || string.IsNullOrEmpty(this.txt体重.Text)) return;
                if (Decimal.TryParse(this.txt身高.Text, out 身高) && Decimal.TryParse(this.txt体重.Text, out 体重))
                {
                    体重 = Math.Floor(BMI * 身高 / 100 * 身高 / 100);
                    this.txt减体重目标.Text = 体重.ToString() + "KG";
                }
            }
            else
            {
                this.txt减体重目标.Enabled = false;
                this.txt减体重目标.Text = "";
            }
        }

        private void btn保存_Click(object sender, EventArgs e)
        {
            try
            {
                tb_健康体检Info tb_new = new tb_健康体检Info();
                if (!string.IsNullOrEmpty(this.txt身高.Text))
                {
                    tb_new.身高 = Convert.ToDouble(this.txt身高.Text);
                }
                if (!string.IsNullOrEmpty(this.txt体重.Text))
                {
                    tb_new.体重 = Convert.ToDouble(this.txt体重.Text);
                }
                if (!string.IsNullOrEmpty(this.txt体重指数.Text))
                {
                    tb_new.体重指数 = this.txt体重指数.Text;
                }
                if (!string.IsNullOrEmpty(this.txt腰围.Text))
                {
                    //定制腰围 ------------王森
                    tb_new.腰围 = Convert.ToDouble(this.txt腰围.Text);
                    //Random rd = new Random();
                    //int x = rd.Next(1, 7);
                    //tb_new.腰围 = Convert.ToDouble(this.txt腰围.Text) + Convert.ToDouble(x);
                    //定制腰围 ------------王森
                }
                if (this.chk减体重.Checked)
                {
                    tb_new.危险因素控制 = "97";
                    tb_new.危险因素控制体重 = this.txt减体重目标.Text.Trim();
                }


                //Begin WXF 2018-11-21 | 16:52
                //保存医师签字 
                if (lookUpEdit_医师签字.Text.Equals("请选择"))
                {
                    //MessageBox.Show("请选择身高体重的责任医师!");
                    //return;
                }
                else
                {
                    tb_new.医师签字 = tb_健康体检DAL.JsonToStr_医师签字(Program.currentUser.DocNo,lookUpEdit_医师签字.EditValue.ToString(),tb_健康体检DAL.Module_体检.身高体重);
                }
                //End
											 

                if (tb_健康体检DAL.UpdateSys_身高体重(tb_new))
                {
                    MessageBox.Show("保存数据成功！");

                    //Closing = true;//设置取数完毕，关闭
                    //comm.Close();
                }
                _txtParentBarCode.Focus();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void uc身高体重_Load(object sender, EventArgs e)
        {
            //获取蓝牙操作句柄
            hBluetooth = EcgHelper.BluetoothOperationOpen();
            try
            {
                EcgHelper.BluetoothOperationConnect(hBluetooth, Program.BlueAddr, ref nCOMID);

                //labelCom.Text += nCOMID.ToString();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("未与设备进行配对！请先初始化" + ex.Message);
                return;
            }
            //添加事件注册
            comm.DataReceived += comm_DataReceived;

            //关闭时点击，则设置好端口，波特率后打开
            comm.PortName = string.Format("COM{0}", nCOMID);
            comm.BaudRate = 9600;//外接蓝牙时，用9600能获取到
            //comm.BaudRate = 4800;
            try
            {
                comm.Open();
            }
            catch (Exception ex)
            {
                //捕获到异常信息，创建一个新的comm对象，之前的不能用了。
                comm = new SerialPort();
                //现实异常信息给客户。
                MessageBox.Show(ex.Message);
            }

            Binding_医师签字();
        }

        //Begin WXF 2018-11-21 | 15:57
        //绑定医师签字 
        private void Binding_医师签字()
        {
            dal_医生信息 dal医生信息 = new dal_医生信息();
            DataTable dt = dal医生信息.Get医生信息ToLookUp();

            lookUpEdit_医师签字.Properties.ValueMember = "b编码";
            lookUpEdit_医师签字.Properties.DisplayMember = "x医生姓名";
            lookUpEdit_医师签字.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("x医生姓名","医生姓名"));
            lookUpEdit_医师签字.Properties.DataSource = dt;
            lookUpEdit_医师签字.EditValue = "请选择";
        }
        //End

        void comm_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (Closing) return;//如果正在关闭，忽略操作，直接返回，尽快的完成串口监听线程的一次循环
            try
            {
                Listening = true;//设置标记，说明我已经开始处理数据，一会儿要使用系统UI的。
                int n = comm.BytesToRead;//先记录下来，避免某种原因，人为的原因，操作几次之间时间长，缓存不一致
                byte[] buf = new byte[n];//声明一个临时数组存储当前来的串口数据
                received_count += n;//增加接收计数
                comm.Read(buf, 0, n);//读取缓冲数据

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //<协议解析>
                bool data_1_catched = false;//缓存记录数据是否捕获到
                //1.缓存数据
                buffer.AddRange(buf);
                binary_data_1 = new byte[buffer.Count];
                //2.完整性判断
                while (buffer.Count > 4)//至少要包含头（2字节）
                {
                    //请不要担心使用>=，因为>=已经和>,<,=一样，是独立操作符，并不是解析成>和=2个符号
                    //2.1 查找数据头
                    if (buffer[0] == 0x57 && buffer[1] == 0x3A)
                    {
                        if (buffer[buffer.Count - 1] == 0x0A)//遇到测试结果直接跳出，并进行数据位校验
                        {
                            if (buffer.Count < 17) break;
                            buffer.CopyTo(0, binary_data_1, 0, buffer.Count);//复制一条完整数据到具体的数据缓存
                            data_1_catched = true;
                            buffer.RemoveRange(0, buffer.Count);//正确分析一条数据，从缓存中移除数据。
                            continue;//继续下一次循环
                        }
                        else
                        {
                            break;//如果没遇到结束位，直接返回什么都不做
                        }
                    }
                    else if (buffer[0] == 0x17)
                    {//龙家圈新增设备解码
                        if (buffer.Count < 18) break;
                        buffer.CopyTo(0, binary_data_1, 0, buffer.Count);//复制一条完整数据到具体的数据缓存
                        data_1_catched = true;
                        buffer.RemoveRange(0, buffer.Count);//正确分析一条数据，从缓存中移除数据。
                        continue;//继续下一次循环
                    }
                    else
                    {
                        //这里是很重要的，如果数据开始不是头，则删除数据
                        buffer.RemoveAt(0);
                    }
                }
                //分析数据
                if (data_1_catched)
                {
                    string ss = Encoding.ASCII.GetString(binary_data_1);					
                    //string[] sss = ss.Replace("\r\n", "").Split(new char[] { ' ' });
                    string[] sss = ss.Split((char)0x17);
                    if (binary_data_1[0] == 0x17)
                    {//龙家圈新增设备解码
                        //更新界面
                        this.Invoke((EventHandler)(delegate
                        {
                            //txData.Text = data;
                            foreach (string s in sss)
                            {
                                if (s.Length == 8)
                                {
                                    Decimal i身高 = 0,i体重 = 0;
                                    if (Decimal.TryParse(s.Substring(0, 4), out i身高) && Decimal.TryParse(s.Substring(4, 4), out i体重))
                                    {
                                        txt身高.Text = (i身高 / 10).ToString("0.0");
                                        txt体重.Text = (i体重 / 10).ToString("0.0");
                                    }
                                }
                                //textBoxBMI.Text = (Convert.ToDouble(ss.Substring(10, 4)) / 10).ToString("0.0");
                            }

                        }));
                    }
                    else
                    {
                        //更新界面
                        this.Invoke((EventHandler)(delegate
                        {
                            foreach (string s in sss)
                            {
                                string[] s1 = s.Split(new char[] { ':' });
                                if (s1[0] == "H")
                                    txt身高.Text = Convert.ToDouble(s1[1]).ToString();
                                else if (s1[0] == "W")
                                    txt体重.Text = Convert.ToDouble(s1[1]).ToString();
                            }
                        }));
                    }
                }
            }
            catch 
            {

            }
            finally
            {
                Listening = false;//我用完了，ui可以关闭串口了。
            }
        }
    }
}
