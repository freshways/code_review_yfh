﻿namespace ATOMEHR_LeaveClient
{
    partial class uc尿常规
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc尿常规));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtID = new DevExpress.XtraEditors.TextEdit();
            this.txt检查时间 = new DevExpress.XtraEditors.TextEdit();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.txtmAlb微量蛋白 = new DevExpress.XtraEditors.TextEdit();
            this.txtBLO尿潜血 = new DevExpress.XtraEditors.TextEdit();
            this.txtKET尿酮体 = new DevExpress.XtraEditors.TextEdit();
            this.txtPro尿蛋白 = new DevExpress.XtraEditors.TextEdit();
            this.txtGLU尿糖 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.labelState = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt检查时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmAlb微量蛋白.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBLO尿潜血.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKET尿酮体.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPro尿蛋白.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGLU尿糖.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtID);
            this.layoutControl1.Controls.Add(this.txt检查时间);
            this.layoutControl1.Controls.Add(this.btn保存);
            this.layoutControl1.Controls.Add(this.txtmAlb微量蛋白);
            this.layoutControl1.Controls.Add(this.txtBLO尿潜血);
            this.layoutControl1.Controls.Add(this.txtKET尿酮体);
            this.layoutControl1.Controls.Add(this.txtPro尿蛋白);
            this.layoutControl1.Controls.Add(this.txtGLU尿糖);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 23);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(356, 281, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(558, 458);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtID
            // 
            this.txtID.EditValue = "";
            this.txtID.Location = new System.Drawing.Point(196, 116);
            this.txtID.Name = "txtID";
            this.txtID.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20F);
            this.txtID.Properties.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.txtID.Properties.Appearance.Options.UseFont = true;
            this.txtID.Properties.Appearance.Options.UseForeColor = true;
            this.txtID.Size = new System.Drawing.Size(350, 40);
            this.txtID.StyleController = this.layoutControl1;
            this.txtID.TabIndex = 12;
            // 
            // txt检查时间
            // 
            this.txt检查时间.EditValue = "";
            this.txt检查时间.Location = new System.Drawing.Point(196, 71);
            this.txt检查时间.Name = "txt检查时间";
            this.txt检查时间.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20F);
            this.txt检查时间.Properties.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.txt检查时间.Properties.Appearance.Options.UseFont = true;
            this.txt检查时间.Properties.Appearance.Options.UseForeColor = true;
            this.txt检查时间.Size = new System.Drawing.Size(350, 40);
            this.txt检查时间.StyleController = this.layoutControl1;
            this.txt检查时间.TabIndex = 11;
            // 
            // btn保存
            // 
            this.btn保存.Appearance.Font = new System.Drawing.Font("Tahoma", 30F);
            this.btn保存.Appearance.Options.UseFont = true;
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(12, 12);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(534, 55);
            this.btn保存.StyleController = this.layoutControl1;
            this.btn保存.TabIndex = 10;
            this.btn保存.Text = "保 存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // txtmAlb微量蛋白
            // 
            this.txtmAlb微量蛋白.EditValue = "";
            this.txtmAlb微量蛋白.Location = new System.Drawing.Point(196, 341);
            this.txtmAlb微量蛋白.Name = "txtmAlb微量蛋白";
            this.txtmAlb微量蛋白.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20F);
            this.txtmAlb微量蛋白.Properties.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.txtmAlb微量蛋白.Properties.Appearance.Options.UseFont = true;
            this.txtmAlb微量蛋白.Properties.Appearance.Options.UseForeColor = true;
            this.txtmAlb微量蛋白.Size = new System.Drawing.Size(350, 40);
            this.txtmAlb微量蛋白.StyleController = this.layoutControl1;
            this.txtmAlb微量蛋白.TabIndex = 9;
            // 
            // txtBLO尿潜血
            // 
            this.txtBLO尿潜血.EditValue = "";
            this.txtBLO尿潜血.Location = new System.Drawing.Point(196, 296);
            this.txtBLO尿潜血.Name = "txtBLO尿潜血";
            this.txtBLO尿潜血.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20F);
            this.txtBLO尿潜血.Properties.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.txtBLO尿潜血.Properties.Appearance.Options.UseFont = true;
            this.txtBLO尿潜血.Properties.Appearance.Options.UseForeColor = true;
            this.txtBLO尿潜血.Size = new System.Drawing.Size(350, 40);
            this.txtBLO尿潜血.StyleController = this.layoutControl1;
            this.txtBLO尿潜血.TabIndex = 8;
            // 
            // txtKET尿酮体
            // 
            this.txtKET尿酮体.EditValue = "";
            this.txtKET尿酮体.Location = new System.Drawing.Point(196, 251);
            this.txtKET尿酮体.Name = "txtKET尿酮体";
            this.txtKET尿酮体.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20F);
            this.txtKET尿酮体.Properties.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.txtKET尿酮体.Properties.Appearance.Options.UseFont = true;
            this.txtKET尿酮体.Properties.Appearance.Options.UseForeColor = true;
            this.txtKET尿酮体.Size = new System.Drawing.Size(350, 40);
            this.txtKET尿酮体.StyleController = this.layoutControl1;
            this.txtKET尿酮体.TabIndex = 7;
            // 
            // txtPro尿蛋白
            // 
            this.txtPro尿蛋白.EditValue = "";
            this.txtPro尿蛋白.Location = new System.Drawing.Point(196, 161);
            this.txtPro尿蛋白.Name = "txtPro尿蛋白";
            this.txtPro尿蛋白.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20F);
            this.txtPro尿蛋白.Properties.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.txtPro尿蛋白.Properties.Appearance.Options.UseFont = true;
            this.txtPro尿蛋白.Properties.Appearance.Options.UseForeColor = true;
            this.txtPro尿蛋白.Size = new System.Drawing.Size(350, 40);
            this.txtPro尿蛋白.StyleController = this.layoutControl1;
            this.txtPro尿蛋白.TabIndex = 6;
            // 
            // txtGLU尿糖
            // 
            this.txtGLU尿糖.EditValue = "";
            this.txtGLU尿糖.Location = new System.Drawing.Point(196, 206);
            this.txtGLU尿糖.Name = "txtGLU尿糖";
            this.txtGLU尿糖.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20F);
            this.txtGLU尿糖.Properties.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.txtGLU尿糖.Properties.Appearance.Options.UseFont = true;
            this.txtGLU尿糖.Properties.Appearance.Options.UseForeColor = true;
            this.txtGLU尿糖.Size = new System.Drawing.Size(350, 40);
            this.txtGLU尿糖.StyleController = this.layoutControl1;
            this.txtGLU尿糖.TabIndex = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem1,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.emptySpaceItem1,
            this.layoutControlItem6});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(558, 458);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem2.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem2.Control = this.txtGLU尿糖;
            this.layoutControlItem2.CustomizationFormText = "尿糖/GLU";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 194);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(538, 45);
            this.layoutControlItem2.Text = "尿糖/GLU";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(179, 41);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem3.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem3.Control = this.txtKET尿酮体;
            this.layoutControlItem3.CustomizationFormText = "尿酮体/KET";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 239);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(538, 45);
            this.layoutControlItem3.Text = "尿酮体/KET";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(179, 41);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem4.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem4.Control = this.txtBLO尿潜血;
            this.layoutControlItem4.CustomizationFormText = "尿潜血/BLO";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 284);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(538, 45);
            this.layoutControlItem4.Text = "尿潜血/BLO";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(179, 41);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem5.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem5.Control = this.txtmAlb微量蛋白;
            this.layoutControlItem5.CustomizationFormText = "尿微量蛋白";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 329);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(538, 45);
            this.layoutControlItem5.Text = "尿微量蛋白";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(179, 41);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btn保存;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(538, 59);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem1.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem1.Control = this.txtPro尿蛋白;
            this.layoutControlItem1.CustomizationFormText = "尿蛋白/PRO";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 149);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(538, 45);
            this.layoutControlItem1.Text = "尿蛋白/PRO";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(179, 41);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem7.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem7.Control = this.txt检查时间;
            this.layoutControlItem7.CustomizationFormText = "检查时间";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 59);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(538, 45);
            this.layoutControlItem7.Text = "检查时间";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(179, 41);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem8.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem8.Control = this.txtID;
            this.layoutControlItem8.CustomizationFormText = "样本号";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 104);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(538, 45);
            this.layoutControlItem8.Text = "样本号";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(179, 41);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 374);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(538, 64);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // labelState
            // 
            this.labelState.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelState.Font = new System.Drawing.Font("宋体", 12F);
            this.labelState.ForeColor = System.Drawing.Color.Red;
            this.labelState.Location = new System.Drawing.Point(0, 0);
            this.labelState.Name = "labelState";
            this.labelState.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.labelState.Size = new System.Drawing.Size(558, 23);
            this.labelState.TabIndex = 1;
            this.labelState.Text = "等待结果";
            // 
            // uc尿常规
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.labelState);
            this.Name = "uc尿常规";
            this.Size = new System.Drawing.Size(558, 481);
            this.Load += new System.EventHandler(this.uc尿常规_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt检查时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmAlb微量蛋白.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBLO尿潜血.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKET尿酮体.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPro尿蛋白.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGLU尿糖.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraEditors.TextEdit txtmAlb微量蛋白;
        private DevExpress.XtraEditors.TextEdit txtBLO尿潜血;
        private DevExpress.XtraEditors.TextEdit txtKET尿酮体;
        private DevExpress.XtraEditors.TextEdit txtPro尿蛋白;
        private DevExpress.XtraEditors.TextEdit txtGLU尿糖;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private System.Windows.Forms.Label labelState;
        private DevExpress.XtraEditors.TextEdit txtID;
        private DevExpress.XtraEditors.TextEdit txt检查时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    }
}
