﻿namespace ATOMEHR_LeaveClient
{
    partial class uc老年人随访
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc老年人随访));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.sBut_Get参照数据 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btn下一项 = new DevExpress.XtraEditors.SimpleButton();
            this.btn上一项 = new DevExpress.XtraEditors.SimpleButton();
            this.result5 = new DevExpress.XtraEditors.LabelControl();
            this.result4 = new DevExpress.XtraEditors.LabelControl();
            this.result3 = new DevExpress.XtraEditors.LabelControl();
            this.result2 = new DevExpress.XtraEditors.LabelControl();
            this.result1 = new DevExpress.XtraEditors.LabelControl();
            this.txt随访医生签名 = new DevExpress.XtraEditors.TextEdit();
            this.txt录入医生 = new DevExpress.XtraEditors.TextEdit();
            this.txt下次随访目标 = new DevExpress.XtraEditors.MemoEdit();
            this.chk活动54 = new DevExpress.XtraEditors.CheckEdit();
            this.chk活动53 = new DevExpress.XtraEditors.CheckEdit();
            this.chk活动52 = new DevExpress.XtraEditors.CheckEdit();
            this.chk活动51 = new DevExpress.XtraEditors.CheckEdit();
            this.chk如厕44 = new DevExpress.XtraEditors.CheckEdit();
            this.chk如厕42 = new DevExpress.XtraEditors.CheckEdit();
            this.chk如厕43 = new DevExpress.XtraEditors.CheckEdit();
            this.chk如厕41 = new DevExpress.XtraEditors.CheckEdit();
            this.chk穿衣33 = new DevExpress.XtraEditors.CheckEdit();
            this.chk穿衣32 = new DevExpress.XtraEditors.CheckEdit();
            this.chk穿衣31 = new DevExpress.XtraEditors.CheckEdit();
            this.chk梳洗24 = new DevExpress.XtraEditors.CheckEdit();
            this.chk梳洗23 = new DevExpress.XtraEditors.CheckEdit();
            this.chk梳洗22 = new DevExpress.XtraEditors.CheckEdit();
            this.chk梳洗21 = new DevExpress.XtraEditors.CheckEdit();
            this.chk进餐13 = new DevExpress.XtraEditors.CheckEdit();
            this.chk进餐12 = new DevExpress.XtraEditors.CheckEdit();
            this.chk进餐11 = new DevExpress.XtraEditors.CheckEdit();
            this.dte随访日期 = new DevExpress.XtraEditors.DateEdit();
            this.dte下次随访日期 = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem38 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl随访日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl下次随访日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl录入医生 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl随访医生签名 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem36 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.result = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleLabelItem5 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem25 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem31 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访医生签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访目标.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk活动54.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk活动53.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk活动52.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk活动51.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk如厕44.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk如厕42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk如厕43.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk如厕41.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk穿衣33.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk穿衣32.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk穿衣31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk梳洗24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk梳洗23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk梳洗22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk梳洗21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk进餐13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk进餐12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk进餐11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl随访日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl下次随访日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl录入医生)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl随访医生签名)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.result)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(600, 46);
            this.panelControl1.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn保存);
            this.flowLayoutPanel1.Controls.Add(this.sBut_Get参照数据);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(596, 42);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn保存
            // 
            this.btn保存.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.btn保存.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn保存.Appearance.Options.UseFont = true;
            this.btn保存.Appearance.Options.UseForeColor = true;
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(488, 3);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(105, 36);
            this.btn保存.TabIndex = 0;
            this.btn保存.Text = "保 存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // sBut_Get参照数据
            // 
            this.sBut_Get参照数据.Appearance.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.sBut_Get参照数据.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.sBut_Get参照数据.Appearance.Options.UseFont = true;
            this.sBut_Get参照数据.Appearance.Options.UseForeColor = true;
            this.sBut_Get参照数据.Image = ((System.Drawing.Image)(resources.GetObject("sBut_Get参照数据.Image")));
            this.sBut_Get参照数据.Location = new System.Drawing.Point(300, 2);
            this.sBut_Get参照数据.Margin = new System.Windows.Forms.Padding(2);
            this.sBut_Get参照数据.Name = "sBut_Get参照数据";
            this.sBut_Get参照数据.Size = new System.Drawing.Size(183, 38);
            this.sBut_Get参照数据.TabIndex = 6;
            this.sBut_Get参照数据.Text = "获取去年数据参照";
            this.sBut_Get参照数据.Click += new System.EventHandler(this.sBut_Get参照数据_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btn下一项);
            this.layoutControl1.Controls.Add(this.btn上一项);
            this.layoutControl1.Controls.Add(this.result5);
            this.layoutControl1.Controls.Add(this.result4);
            this.layoutControl1.Controls.Add(this.result3);
            this.layoutControl1.Controls.Add(this.result2);
            this.layoutControl1.Controls.Add(this.result1);
            this.layoutControl1.Controls.Add(this.txt随访医生签名);
            this.layoutControl1.Controls.Add(this.txt录入医生);
            this.layoutControl1.Controls.Add(this.txt下次随访目标);
            this.layoutControl1.Controls.Add(this.chk活动54);
            this.layoutControl1.Controls.Add(this.chk活动53);
            this.layoutControl1.Controls.Add(this.chk活动52);
            this.layoutControl1.Controls.Add(this.chk活动51);
            this.layoutControl1.Controls.Add(this.chk如厕44);
            this.layoutControl1.Controls.Add(this.chk如厕42);
            this.layoutControl1.Controls.Add(this.chk如厕43);
            this.layoutControl1.Controls.Add(this.chk如厕41);
            this.layoutControl1.Controls.Add(this.chk穿衣33);
            this.layoutControl1.Controls.Add(this.chk穿衣32);
            this.layoutControl1.Controls.Add(this.chk穿衣31);
            this.layoutControl1.Controls.Add(this.chk梳洗24);
            this.layoutControl1.Controls.Add(this.chk梳洗23);
            this.layoutControl1.Controls.Add(this.chk梳洗22);
            this.layoutControl1.Controls.Add(this.chk梳洗21);
            this.layoutControl1.Controls.Add(this.chk进餐13);
            this.layoutControl1.Controls.Add(this.chk进餐12);
            this.layoutControl1.Controls.Add(this.chk进餐11);
            this.layoutControl1.Controls.Add(this.dte随访日期);
            this.layoutControl1.Controls.Add(this.dte下次随访日期);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 46);
            this.layoutControl1.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(566, 87, 250, 283);
            this.layoutControl1.OptionsFocus.AllowFocusGroups = false;
            this.layoutControl1.OptionsFocus.AllowFocusReadonlyEditors = false;
            this.layoutControl1.OptionsFocus.AllowFocusTabbedGroups = false;
            this.layoutControl1.OptionsFocus.EnableAutoTabOrder = false;
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.OptionsView.EnableIndentsInGroupsWithoutBorders = true;
            this.layoutControl1.OptionsView.EnableTransparentBackColor = false;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(600, 354);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btn下一项
            // 
            this.btn下一项.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn下一项.Appearance.Options.UseFont = true;
            this.btn下一项.Image = ((System.Drawing.Image)(resources.GetObject("btn下一项.Image")));
            this.btn下一项.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btn下一项.Location = new System.Drawing.Point(302, 314);
            this.btn下一项.Name = "btn下一项";
            this.btn下一项.Size = new System.Drawing.Size(296, 38);
            this.btn下一项.StyleController = this.layoutControl1;
            this.btn下一项.TabIndex = 47;
            this.btn下一项.Text = "下一项";
            this.btn下一项.Click += new System.EventHandler(this.btn下一页_Click);
            // 
            // btn上一项
            // 
            this.btn上一项.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn上一项.Appearance.Options.UseFont = true;
            this.btn上一项.Image = ((System.Drawing.Image)(resources.GetObject("btn上一项.Image")));
            this.btn上一项.Location = new System.Drawing.Point(2, 314);
            this.btn上一项.Name = "btn上一项";
            this.btn上一项.Size = new System.Drawing.Size(296, 38);
            this.btn上一项.StyleController = this.layoutControl1;
            this.btn上一项.TabIndex = 46;
            this.btn上一项.Text = "上一项";
            this.btn上一项.Click += new System.EventHandler(this.btn上一页_Click);
            // 
            // result5
            // 
            this.result5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.result5.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.result5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.result5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.result5.Location = new System.Drawing.Point(490, 78);
            this.result5.Name = "result5";
            this.result5.Size = new System.Drawing.Size(105, 179);
            this.result5.StyleController = this.layoutControl1;
            this.result5.TabIndex = 45;
            this.result5.TextChanged += new System.EventHandler(this.result_TextChanged);
            // 
            // result4
            // 
            this.result4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.result4.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.result4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.result4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.result4.Location = new System.Drawing.Point(498, 88);
            this.result4.Name = "result4";
            this.result4.Size = new System.Drawing.Size(97, 176);
            this.result4.StyleController = this.layoutControl1;
            this.result4.TabIndex = 44;
            this.result4.TextChanged += new System.EventHandler(this.result_TextChanged);
            // 
            // result3
            // 
            this.result3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.result3.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.result3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.result3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.result3.Location = new System.Drawing.Point(477, 90);
            this.result3.Name = "result3";
            this.result3.Size = new System.Drawing.Size(118, 167);
            this.result3.StyleController = this.layoutControl1;
            this.result3.TabIndex = 43;
            this.result3.TextChanged += new System.EventHandler(this.result_TextChanged);
            // 
            // result2
            // 
            this.result2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.result2.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.result2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.result2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.result2.Location = new System.Drawing.Point(477, 89);
            this.result2.Name = "result2";
            this.result2.Size = new System.Drawing.Size(118, 166);
            this.result2.StyleController = this.layoutControl1;
            this.result2.TabIndex = 42;
            this.result2.TextChanged += new System.EventHandler(this.result_TextChanged);
            // 
            // result1
            // 
            this.result1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.result1.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.result1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.result1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.result1.Location = new System.Drawing.Point(477, 98);
            this.result1.Name = "result1";
            this.result1.Size = new System.Drawing.Size(118, 144);
            this.result1.StyleController = this.layoutControl1;
            this.result1.TabIndex = 41;
            this.result1.TextChanged += new System.EventHandler(this.result_TextChanged);
            // 
            // txt随访医生签名
            // 
            this.txt随访医生签名.Location = new System.Drawing.Point(363, 248);
            this.txt随访医生签名.Name = "txt随访医生签名";
            this.txt随访医生签名.Properties.AutoHeight = false;
            this.txt随访医生签名.Size = new System.Drawing.Size(229, 26);
            this.txt随访医生签名.StyleController = this.layoutControl1;
            this.txt随访医生签名.TabIndex = 29;
            // 
            // txt录入医生
            // 
            this.txt录入医生.Location = new System.Drawing.Point(363, 278);
            this.txt录入医生.Name = "txt录入医生";
            this.txt录入医生.Properties.AutoHeight = false;
            this.txt录入医生.Size = new System.Drawing.Size(229, 26);
            this.txt录入医生.StyleController = this.layoutControl1;
            this.txt录入医生.TabIndex = 31;
            // 
            // txt下次随访目标
            // 
            this.txt下次随访目标.EditValue = "加强锻炼，保持自理";
            this.txt下次随访目标.Location = new System.Drawing.Point(113, 148);
            this.txt下次随访目标.Name = "txt下次随访目标";
            this.txt下次随访目标.Size = new System.Drawing.Size(479, 96);
            this.txt下次随访目标.StyleController = this.layoutControl1;
            this.txt下次随访目标.TabIndex = 27;
            this.txt下次随访目标.UseOptimizedRendering = true;
            // 
            // chk活动54
            // 
            this.chk活动54.Location = new System.Drawing.Point(5, 210);
            this.chk活动54.Name = "chk活动54";
            this.chk活动54.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.chk活动54.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.chk活动54.Properties.Appearance.Options.UseBackColor = true;
            this.chk活动54.Properties.Appearance.Options.UseFont = true;
            this.chk活动54.Properties.Appearance.Options.UseTextOptions = true;
            this.chk活动54.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk活动54.Properties.AutoHeight = false;
            this.chk活动54.Properties.Caption = "卧床不起， 活动完全需要帮助";
            this.chk活动54.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk活动54.Properties.RadioGroupIndex = 5;
            this.chk活动54.Size = new System.Drawing.Size(481, 47);
            this.chk活动54.StyleController = this.layoutControl1;
            this.chk活动54.TabIndex = 26;
            this.chk活动54.TabStop = false;
            this.chk活动54.Tag = "10";
            this.chk活动54.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk活动53
            // 
            this.chk活动53.Location = new System.Drawing.Point(5, 167);
            this.chk活动53.Name = "chk活动53";
            this.chk活动53.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.chk活动53.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.chk活动53.Properties.Appearance.Options.UseBackColor = true;
            this.chk活动53.Properties.Appearance.Options.UseFont = true;
            this.chk活动53.Properties.Appearance.Options.UseTextOptions = true;
            this.chk活动53.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk活动53.Properties.AutoHeight = false;
            this.chk活动53.Properties.Caption = "借助较大的外力才能完成站立、行走，不能上下楼梯 ";
            this.chk活动53.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk活动53.Properties.RadioGroupIndex = 5;
            this.chk活动53.Size = new System.Drawing.Size(481, 39);
            this.chk活动53.StyleController = this.layoutControl1;
            this.chk活动53.TabIndex = 25;
            this.chk活动53.TabStop = false;
            this.chk活动53.Tag = "5";
            this.chk活动53.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk活动52
            // 
            this.chk活动52.Location = new System.Drawing.Point(5, 119);
            this.chk活动52.Name = "chk活动52";
            this.chk活动52.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.chk活动52.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.chk活动52.Properties.Appearance.Options.UseBackColor = true;
            this.chk活动52.Properties.Appearance.Options.UseFont = true;
            this.chk活动52.Properties.Appearance.Options.UseTextOptions = true;
            this.chk活动52.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk活动52.Properties.AutoHeight = false;
            this.chk活动52.Properties.Caption = "借助较小的外力或辅助装置能完成站立、行走、上下楼梯等 ";
            this.chk活动52.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk活动52.Properties.RadioGroupIndex = 5;
            this.chk活动52.Size = new System.Drawing.Size(481, 44);
            this.chk活动52.StyleController = this.layoutControl1;
            this.chk活动52.TabIndex = 24;
            this.chk活动52.TabStop = false;
            this.chk活动52.Tag = "1";
            this.chk活动52.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk活动51
            // 
            this.chk活动51.Location = new System.Drawing.Point(5, 78);
            this.chk活动51.Name = "chk活动51";
            this.chk活动51.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.chk活动51.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.chk活动51.Properties.Appearance.Options.UseBackColor = true;
            this.chk活动51.Properties.Appearance.Options.UseFont = true;
            this.chk活动51.Properties.AutoHeight = false;
            this.chk活动51.Properties.Caption = "独立完成所有活动";
            this.chk活动51.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk活动51.Properties.RadioGroupIndex = 5;
            this.chk活动51.Size = new System.Drawing.Size(481, 37);
            this.chk活动51.StyleController = this.layoutControl1;
            this.chk活动51.TabIndex = 23;
            this.chk活动51.TabStop = false;
            this.chk活动51.Tag = "0";
            this.chk活动51.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk如厕44
            // 
            this.chk如厕44.Location = new System.Drawing.Point(5, 225);
            this.chk如厕44.Name = "chk如厕44";
            this.chk如厕44.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.chk如厕44.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.chk如厕44.Properties.Appearance.Options.UseBackColor = true;
            this.chk如厕44.Properties.Appearance.Options.UseFont = true;
            this.chk如厕44.Properties.Appearance.Options.UseTextOptions = true;
            this.chk如厕44.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk如厕44.Properties.AutoHeight = false;
            this.chk如厕44.Properties.Caption = "完全失禁， 完全需要帮助";
            this.chk如厕44.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk如厕44.Properties.RadioGroupIndex = 4;
            this.chk如厕44.Size = new System.Drawing.Size(489, 39);
            this.chk如厕44.StyleController = this.layoutControl1;
            this.chk如厕44.TabIndex = 22;
            this.chk如厕44.TabStop = false;
            this.chk如厕44.Tag = "10";
            this.chk如厕44.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk如厕42
            // 
            this.chk如厕42.Location = new System.Drawing.Point(5, 134);
            this.chk如厕42.Name = "chk如厕42";
            this.chk如厕42.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.chk如厕42.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.chk如厕42.Properties.Appearance.Options.UseBackColor = true;
            this.chk如厕42.Properties.Appearance.Options.UseFont = true;
            this.chk如厕42.Properties.Appearance.Options.UseTextOptions = true;
            this.chk如厕42.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk如厕42.Properties.AutoHeight = false;
            this.chk如厕42.Properties.Caption = "偶尔失禁，但基本上能如厕或使用便具 ";
            this.chk如厕42.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk如厕42.Properties.RadioGroupIndex = 4;
            this.chk如厕42.Size = new System.Drawing.Size(489, 42);
            this.chk如厕42.StyleController = this.layoutControl1;
            this.chk如厕42.TabIndex = 20;
            this.chk如厕42.TabStop = false;
            this.chk如厕42.Tag = "1";
            this.chk如厕42.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk如厕43
            // 
            this.chk如厕43.Location = new System.Drawing.Point(5, 180);
            this.chk如厕43.Name = "chk如厕43";
            this.chk如厕43.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.chk如厕43.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.chk如厕43.Properties.Appearance.Options.UseBackColor = true;
            this.chk如厕43.Properties.Appearance.Options.UseFont = true;
            this.chk如厕43.Properties.Appearance.Options.UseTextOptions = true;
            this.chk如厕43.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk如厕43.Properties.AutoHeight = false;
            this.chk如厕43.Properties.Caption = "经常失禁，在很多提示和协助下尚能如厕或使用便具";
            this.chk如厕43.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk如厕43.Properties.RadioGroupIndex = 4;
            this.chk如厕43.Size = new System.Drawing.Size(489, 41);
            this.chk如厕43.StyleController = this.layoutControl1;
            this.chk如厕43.TabIndex = 21;
            this.chk如厕43.TabStop = false;
            this.chk如厕43.Tag = "5";
            this.chk如厕43.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk如厕41
            // 
            this.chk如厕41.Location = new System.Drawing.Point(5, 88);
            this.chk如厕41.Name = "chk如厕41";
            this.chk如厕41.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.chk如厕41.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.chk如厕41.Properties.Appearance.Options.UseBackColor = true;
            this.chk如厕41.Properties.Appearance.Options.UseFont = true;
            this.chk如厕41.Properties.Appearance.Options.UseTextOptions = true;
            this.chk如厕41.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk如厕41.Properties.AutoHeight = false;
            this.chk如厕41.Properties.Caption = "不需协助，可自控";
            this.chk如厕41.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk如厕41.Properties.RadioGroupIndex = 4;
            this.chk如厕41.Size = new System.Drawing.Size(489, 42);
            this.chk如厕41.StyleController = this.layoutControl1;
            this.chk如厕41.TabIndex = 19;
            this.chk如厕41.TabStop = false;
            this.chk如厕41.Tag = "0";
            this.chk如厕41.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk穿衣33
            // 
            this.chk穿衣33.Location = new System.Drawing.Point(5, 199);
            this.chk穿衣33.Name = "chk穿衣33";
            this.chk穿衣33.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.chk穿衣33.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.chk穿衣33.Properties.Appearance.Options.UseBackColor = true;
            this.chk穿衣33.Properties.Appearance.Options.UseFont = true;
            this.chk穿衣33.Properties.AutoHeight = false;
            this.chk穿衣33.Properties.Caption = "完全需要帮助";
            this.chk穿衣33.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk穿衣33.Properties.RadioGroupIndex = 3;
            this.chk穿衣33.Size = new System.Drawing.Size(468, 58);
            this.chk穿衣33.StyleController = this.layoutControl1;
            this.chk穿衣33.TabIndex = 18;
            this.chk穿衣33.TabStop = false;
            this.chk穿衣33.Tag = "5";
            this.chk穿衣33.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk穿衣32
            // 
            this.chk穿衣32.Location = new System.Drawing.Point(5, 147);
            this.chk穿衣32.Name = "chk穿衣32";
            this.chk穿衣32.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.chk穿衣32.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.chk穿衣32.Properties.Appearance.Options.UseBackColor = true;
            this.chk穿衣32.Properties.Appearance.Options.UseFont = true;
            this.chk穿衣32.Properties.Appearance.Options.UseTextOptions = true;
            this.chk穿衣32.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk穿衣32.Properties.AutoHeight = false;
            this.chk穿衣32.Properties.Caption = "\t需要协助， 在适当的时间内完成部分穿衣 ";
            this.chk穿衣32.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk穿衣32.Properties.RadioGroupIndex = 3;
            this.chk穿衣32.Size = new System.Drawing.Size(468, 48);
            this.chk穿衣32.StyleController = this.layoutControl1;
            this.chk穿衣32.TabIndex = 17;
            this.chk穿衣32.TabStop = false;
            this.chk穿衣32.Tag = "3";
            this.chk穿衣32.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk穿衣31
            // 
            this.chk穿衣31.Location = new System.Drawing.Point(5, 90);
            this.chk穿衣31.Name = "chk穿衣31";
            this.chk穿衣31.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.chk穿衣31.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.chk穿衣31.Properties.Appearance.Options.UseBackColor = true;
            this.chk穿衣31.Properties.Appearance.Options.UseFont = true;
            this.chk穿衣31.Properties.AutoHeight = false;
            this.chk穿衣31.Properties.Caption = "独立完成";
            this.chk穿衣31.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk穿衣31.Properties.RadioGroupIndex = 3;
            this.chk穿衣31.Size = new System.Drawing.Size(468, 53);
            this.chk穿衣31.StyleController = this.layoutControl1;
            this.chk穿衣31.TabIndex = 16;
            this.chk穿衣31.TabStop = false;
            this.chk穿衣31.Tag = "0";
            this.chk穿衣31.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk梳洗24
            // 
            this.chk梳洗24.Location = new System.Drawing.Point(5, 211);
            this.chk梳洗24.Name = "chk梳洗24";
            this.chk梳洗24.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.chk梳洗24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.chk梳洗24.Properties.Appearance.Options.UseBackColor = true;
            this.chk梳洗24.Properties.Appearance.Options.UseFont = true;
            this.chk梳洗24.Properties.Appearance.Options.UseTextOptions = true;
            this.chk梳洗24.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk梳洗24.Properties.AutoHeight = false;
            this.chk梳洗24.Properties.Caption = "完全需要帮助";
            this.chk梳洗24.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk梳洗24.Properties.RadioGroupIndex = 2;
            this.chk梳洗24.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chk梳洗24.Size = new System.Drawing.Size(468, 44);
            this.chk梳洗24.StyleController = this.layoutControl1;
            this.chk梳洗24.TabIndex = 15;
            this.chk梳洗24.TabStop = false;
            this.chk梳洗24.Tag = "7";
            this.chk梳洗24.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk梳洗23
            // 
            this.chk梳洗23.Location = new System.Drawing.Point(5, 166);
            this.chk梳洗23.Name = "chk梳洗23";
            this.chk梳洗23.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.chk梳洗23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.chk梳洗23.Properties.Appearance.Options.UseBackColor = true;
            this.chk梳洗23.Properties.Appearance.Options.UseFont = true;
            this.chk梳洗23.Properties.Appearance.Options.UseTextOptions = true;
            this.chk梳洗23.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk梳洗23.Properties.AutoHeight = false;
            this.chk梳洗23.Properties.Caption = "在协助下和适当的时间内，能完成部分梳洗活动";
            this.chk梳洗23.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk梳洗23.Properties.RadioGroupIndex = 2;
            this.chk梳洗23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chk梳洗23.Size = new System.Drawing.Size(468, 41);
            this.chk梳洗23.StyleController = this.layoutControl1;
            this.chk梳洗23.TabIndex = 14;
            this.chk梳洗23.TabStop = false;
            this.chk梳洗23.Tag = "3";
            this.chk梳洗23.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk梳洗22
            // 
            this.chk梳洗22.Location = new System.Drawing.Point(5, 128);
            this.chk梳洗22.Name = "chk梳洗22";
            this.chk梳洗22.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.chk梳洗22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.chk梳洗22.Properties.Appearance.Options.UseBackColor = true;
            this.chk梳洗22.Properties.Appearance.Options.UseFont = true;
            this.chk梳洗22.Properties.Appearance.Options.UseTextOptions = true;
            this.chk梳洗22.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk梳洗22.Properties.AutoHeight = false;
            this.chk梳洗22.Properties.Caption = "\t能独立地洗头、梳头、洗脸、刷牙、剃须等；洗澡需要协助";
            this.chk梳洗22.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk梳洗22.Properties.RadioGroupIndex = 2;
            this.chk梳洗22.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chk梳洗22.Size = new System.Drawing.Size(468, 34);
            this.chk梳洗22.StyleController = this.layoutControl1;
            this.chk梳洗22.TabIndex = 13;
            this.chk梳洗22.TabStop = false;
            this.chk梳洗22.Tag = "1";
            this.chk梳洗22.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk梳洗21
            // 
            this.chk梳洗21.Location = new System.Drawing.Point(5, 89);
            this.chk梳洗21.Name = "chk梳洗21";
            this.chk梳洗21.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.chk梳洗21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.chk梳洗21.Properties.Appearance.Options.UseBackColor = true;
            this.chk梳洗21.Properties.Appearance.Options.UseFont = true;
            this.chk梳洗21.Properties.Appearance.Options.UseTextOptions = true;
            this.chk梳洗21.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk梳洗21.Properties.AutoHeight = false;
            this.chk梳洗21.Properties.Caption = "独立完成";
            this.chk梳洗21.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk梳洗21.Properties.RadioGroupIndex = 2;
            this.chk梳洗21.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chk梳洗21.Size = new System.Drawing.Size(468, 35);
            this.chk梳洗21.StyleController = this.layoutControl1;
            this.chk梳洗21.TabIndex = 12;
            this.chk梳洗21.TabStop = false;
            this.chk梳洗21.Tag = "0";
            this.chk梳洗21.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk进餐13
            // 
            this.chk进餐13.Location = new System.Drawing.Point(5, 193);
            this.chk进餐13.Name = "chk进餐13";
            this.chk进餐13.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.chk进餐13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.chk进餐13.Properties.Appearance.Options.UseBackColor = true;
            this.chk进餐13.Properties.Appearance.Options.UseFont = true;
            this.chk进餐13.Properties.AutoHeight = false;
            this.chk进餐13.Properties.Caption = "完全需要帮助";
            this.chk进餐13.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk进餐13.Properties.RadioGroupIndex = 1;
            this.chk进餐13.Size = new System.Drawing.Size(468, 49);
            this.chk进餐13.StyleController = this.layoutControl1;
            this.chk进餐13.TabIndex = 11;
            this.chk进餐13.TabStop = false;
            this.chk进餐13.Tag = "5";
            this.chk进餐13.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk进餐12
            // 
            this.chk进餐12.Location = new System.Drawing.Point(5, 145);
            this.chk进餐12.Name = "chk进餐12";
            this.chk进餐12.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.chk进餐12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.chk进餐12.Properties.Appearance.Options.UseBackColor = true;
            this.chk进餐12.Properties.Appearance.Options.UseFont = true;
            this.chk进餐12.Properties.Appearance.Options.UseTextOptions = true;
            this.chk进餐12.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.chk进餐12.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk进餐12.Properties.AutoHeight = false;
            this.chk进餐12.Properties.Caption = "需要协助，如切碎、搅拌食物等";
            this.chk进餐12.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk进餐12.Properties.RadioGroupIndex = 1;
            this.chk进餐12.Size = new System.Drawing.Size(468, 44);
            this.chk进餐12.StyleController = this.layoutControl1;
            this.chk进餐12.TabIndex = 10;
            this.chk进餐12.TabStop = false;
            this.chk进餐12.Tag = "3";
            this.chk进餐12.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk进餐11
            // 
            this.chk进餐11.Location = new System.Drawing.Point(5, 98);
            this.chk进餐11.Name = "chk进餐11";
            this.chk进餐11.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.chk进餐11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.chk进餐11.Properties.Appearance.Options.UseBackColor = true;
            this.chk进餐11.Properties.Appearance.Options.UseFont = true;
            this.chk进餐11.Properties.Appearance.Options.UseTextOptions = true;
            this.chk进餐11.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk进餐11.Properties.AutoHeight = false;
            this.chk进餐11.Properties.Caption = "独立完成";
            this.chk进餐11.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk进餐11.Properties.RadioGroupIndex = 1;
            this.chk进餐11.Size = new System.Drawing.Size(468, 43);
            this.chk进餐11.StyleController = this.layoutControl1;
            this.chk进餐11.TabIndex = 9;
            this.chk进餐11.TabStop = false;
            this.chk进餐11.Tag = "0";
            this.chk进餐11.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // dte随访日期
            // 
            this.dte随访日期.EditValue = null;
            this.dte随访日期.Location = new System.Drawing.Point(113, 248);
            this.dte随访日期.Name = "dte随访日期";
            this.dte随访日期.Properties.AutoHeight = false;
            this.dte随访日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte随访日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte随访日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte随访日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte随访日期.Size = new System.Drawing.Size(141, 26);
            this.dte随访日期.StyleController = this.layoutControl1;
            this.dte随访日期.TabIndex = 28;
            // 
            // dte下次随访日期
            // 
            this.dte下次随访日期.EditValue = null;
            this.dte下次随访日期.Location = new System.Drawing.Point(113, 278);
            this.dte下次随访日期.Name = "dte下次随访日期";
            this.dte下次随访日期.Properties.AutoHeight = false;
            this.dte下次随访日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte下次随访日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte下次随访日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte下次随访日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte下次随访日期.Size = new System.Drawing.Size(141, 26);
            this.dte下次随访日期.StyleController = this.layoutControl1;
            this.dte下次随访日期.TabIndex = 30;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1,
            this.layoutControlItem1,
            this.layoutControlItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(600, 354);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.AppearanceTabPage.Header.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.tabbedControlGroup1.AppearanceTabPage.Header.Options.UseFont = true;
            this.tabbedControlGroup1.AppearanceTabPage.Header.Options.UseTextOptions = true;
            this.tabbedControlGroup1.AppearanceTabPage.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tabbedControlGroup1.AppearanceTabPage.HeaderActive.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.tabbedControlGroup1.AppearanceTabPage.HeaderActive.ForeColor = System.Drawing.Color.Blue;
            this.tabbedControlGroup1.AppearanceTabPage.HeaderActive.Options.UseFont = true;
            this.tabbedControlGroup1.AppearanceTabPage.HeaderActive.Options.UseForeColor = true;
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup2;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(600, 312);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup4,
            this.layoutControlGroup5,
            this.layoutControlGroup12,
            this.layoutControlGroup13,
            this.layoutControlGroup6});
            this.tabbedControlGroup1.Text = "tabbedControlGroup1";
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "结果";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(594, 278);
            this.layoutControlGroup6.Text = "结果";
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem38,
            this.layoutControlItem26,
            this.lbl随访日期,
            this.lbl下次随访日期,
            this.lbl录入医生,
            this.lbl随访医生签名,
            this.emptySpaceItem36,
            this.result});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(594, 278);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // emptySpaceItem38
            // 
            this.emptySpaceItem38.AllowHotTrack = false;
            this.emptySpaceItem38.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.Control;
            this.emptySpaceItem38.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem38.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem38.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem38.CustomizationFormText = "下次随访事项 ";
            this.emptySpaceItem38.Location = new System.Drawing.Point(0, 79);
            this.emptySpaceItem38.Name = "emptySpaceItem38";
            this.emptySpaceItem38.Size = new System.Drawing.Size(588, 33);
            this.emptySpaceItem38.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.SupportHorzAlignment;
            this.emptySpaceItem38.Text = "下次随访事项 ";
            this.emptySpaceItem38.TextSize = new System.Drawing.Size(0, 0);
            this.emptySpaceItem38.TextVisible = true;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem26.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.Control = this.txt下次随访目标;
            this.layoutControlItem26.CustomizationFormText = "下次随访目标";
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 112);
            this.layoutControlItem26.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(588, 100);
            this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem26.Text = "下次随访目标";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(100, 50);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // lbl随访日期
            // 
            this.lbl随访日期.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl随访日期.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl随访日期.AppearanceItemCaption.Options.UseFont = true;
            this.lbl随访日期.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl随访日期.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl随访日期.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl随访日期.Control = this.dte随访日期;
            this.lbl随访日期.CustomizationFormText = "随访日期*";
            this.lbl随访日期.Location = new System.Drawing.Point(0, 212);
            this.lbl随访日期.MaxSize = new System.Drawing.Size(250, 30);
            this.lbl随访日期.MinSize = new System.Drawing.Size(250, 30);
            this.lbl随访日期.Name = "lbl随访日期";
            this.lbl随访日期.Size = new System.Drawing.Size(250, 30);
            this.lbl随访日期.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl随访日期.Text = "随访日期*";
            this.lbl随访日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl随访日期.TextSize = new System.Drawing.Size(100, 20);
            this.lbl随访日期.TextToControlDistance = 5;
            // 
            // lbl下次随访日期
            // 
            this.lbl下次随访日期.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl下次随访日期.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl下次随访日期.AppearanceItemCaption.Options.UseFont = true;
            this.lbl下次随访日期.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl下次随访日期.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl下次随访日期.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl下次随访日期.Control = this.dte下次随访日期;
            this.lbl下次随访日期.CustomizationFormText = "下次随访日期*";
            this.lbl下次随访日期.Location = new System.Drawing.Point(0, 242);
            this.lbl下次随访日期.MaxSize = new System.Drawing.Size(250, 30);
            this.lbl下次随访日期.MinSize = new System.Drawing.Size(250, 30);
            this.lbl下次随访日期.Name = "lbl下次随访日期";
            this.lbl下次随访日期.Size = new System.Drawing.Size(250, 30);
            this.lbl下次随访日期.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl下次随访日期.Text = "下次随访日期*";
            this.lbl下次随访日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl下次随访日期.TextSize = new System.Drawing.Size(100, 14);
            this.lbl下次随访日期.TextToControlDistance = 5;
            // 
            // lbl录入医生
            // 
            this.lbl录入医生.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl录入医生.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl录入医生.Control = this.txt录入医生;
            this.lbl录入医生.CustomizationFormText = "录入医生";
            this.lbl录入医生.Location = new System.Drawing.Point(250, 242);
            this.lbl录入医生.MaxSize = new System.Drawing.Size(250, 30);
            this.lbl录入医生.MinSize = new System.Drawing.Size(250, 30);
            this.lbl录入医生.Name = "lbl录入医生";
            this.lbl录入医生.Size = new System.Drawing.Size(338, 30);
            this.lbl录入医生.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl录入医生.Text = "录入医生";
            this.lbl录入医生.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl录入医生.TextSize = new System.Drawing.Size(100, 20);
            this.lbl录入医生.TextToControlDistance = 5;
            // 
            // lbl随访医生签名
            // 
            this.lbl随访医生签名.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl随访医生签名.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl随访医生签名.AppearanceItemCaption.Options.UseFont = true;
            this.lbl随访医生签名.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl随访医生签名.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl随访医生签名.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl随访医生签名.Control = this.txt随访医生签名;
            this.lbl随访医生签名.CustomizationFormText = "随访医生签名";
            this.lbl随访医生签名.Location = new System.Drawing.Point(250, 212);
            this.lbl随访医生签名.MaxSize = new System.Drawing.Size(250, 30);
            this.lbl随访医生签名.MinSize = new System.Drawing.Size(250, 30);
            this.lbl随访医生签名.Name = "lbl随访医生签名";
            this.lbl随访医生签名.Size = new System.Drawing.Size(338, 30);
            this.lbl随访医生签名.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl随访医生签名.Text = "随访医生签名";
            this.lbl随访医生签名.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl随访医生签名.TextSize = new System.Drawing.Size(100, 20);
            this.lbl随访医生签名.TextToControlDistance = 5;
            // 
            // emptySpaceItem36
            // 
            this.emptySpaceItem36.AllowHotTrack = false;
            this.emptySpaceItem36.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.emptySpaceItem36.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 20F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem36.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem36.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem36.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem36.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem36.CustomizationFormText = "总评分";
            this.emptySpaceItem36.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem36.MinSize = new System.Drawing.Size(10, 1);
            this.emptySpaceItem36.Name = "emptySpaceItem36";
            this.emptySpaceItem36.Size = new System.Drawing.Size(294, 79);
            this.emptySpaceItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem36.Text = "总评分：";
            this.emptySpaceItem36.TextSize = new System.Drawing.Size(0, 0);
            this.emptySpaceItem36.TextVisible = true;
            // 
            // result
            // 
            this.result.AllowHotTrack = false;
            this.result.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 22F);
            this.result.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            this.result.AppearanceItemCaption.Options.UseFont = true;
            this.result.AppearanceItemCaption.Options.UseForeColor = true;
            this.result.AppearanceItemCaption.Options.UseTextOptions = true;
            this.result.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.result.CustomizationFormText = "result";
            this.result.Location = new System.Drawing.Point(294, 0);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(294, 79);
            this.result.Text = " ";
            this.result.TextSize = new System.Drawing.Size(0, 0);
            this.result.TextVisible = true;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleLabelItem5,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem11,
            this.layoutControlItem33,
            this.emptySpaceItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(594, 278);
            this.layoutControlGroup2.Text = "进餐";
            // 
            // simpleLabelItem5
            // 
            this.simpleLabelItem5.AllowHotTrack = false;
            this.simpleLabelItem5.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.simpleLabelItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold);
            this.simpleLabelItem5.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.simpleLabelItem5.AppearanceItemCaption.Options.UseBackColor = true;
            this.simpleLabelItem5.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem5.AppearanceItemCaption.Options.UseForeColor = true;
            this.simpleLabelItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem5.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleLabelItem5.CustomizationFormText = "（1）进餐：使用餐具将饭菜送入口、咀嚼、吞咽等活动";
            this.simpleLabelItem5.Location = new System.Drawing.Point(0, 0);
            this.simpleLabelItem5.MaxSize = new System.Drawing.Size(100, 65);
            this.simpleLabelItem5.MinSize = new System.Drawing.Size(100, 65);
            this.simpleLabelItem5.Name = "simpleLabelItem5";
            this.simpleLabelItem5.Size = new System.Drawing.Size(594, 65);
            this.simpleLabelItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem5.Text = "（1）进餐：使用餐具将饭菜送入口、咀嚼、吞咽等活动";
            this.simpleLabelItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.simpleLabelItem5.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem8.Control = this.chk进餐11;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 65);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(472, 47);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem9.Control = this.chk进餐12;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 112);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(472, 48);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem11.Control = this.chk进餐13;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 160);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(472, 53);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.result1;
            this.layoutControlItem33.CustomizationFormText = "layoutControlItem33";
            this.layoutControlItem33.Location = new System.Drawing.Point(472, 65);
            this.layoutControlItem33.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(122, 148);
            this.layoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem33.Text = "layoutControlItem33";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem33.TextToControlDistance = 0;
            this.layoutControlItem33.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 213);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(594, 65);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "梳洗";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem14,
            this.emptySpaceItem8,
            this.layoutControlItem10,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem34,
            this.emptySpaceItem1});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(594, 278);
            this.layoutControlGroup4.Text = "梳洗";
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem14.Control = this.chk梳洗24;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 178);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(472, 48);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "layoutControlItem14";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextToControlDistance = 0;
            this.layoutControlItem14.TextVisible = false;
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.emptySpaceItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem8.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.emptySpaceItem8.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem8.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem8.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem8.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem8.CustomizationFormText = "（2）梳洗：梳头、洗脸、刷牙、剃须洗澡等活动";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem8.MinSize = new System.Drawing.Size(1, 1);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(594, 56);
            this.emptySpaceItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem8.Text = "（2）梳洗：梳头、洗脸、刷牙、剃须洗澡等活动";
            this.emptySpaceItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            this.emptySpaceItem8.TextVisible = true;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem10.Control = this.chk梳洗21;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(472, 39);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem12.Control = this.chk梳洗22;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 95);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(472, 38);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem13.Control = this.chk梳洗23;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 133);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(472, 45);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "layoutControlItem13";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.result2;
            this.layoutControlItem34.CustomizationFormText = "layoutControlItem34";
            this.layoutControlItem34.Location = new System.Drawing.Point(472, 56);
            this.layoutControlItem34.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(122, 170);
            this.layoutControlItem34.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem34.Text = "layoutControlItem34";
            this.layoutControlItem34.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem34.TextToControlDistance = 0;
            this.layoutControlItem34.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 226);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(594, 52);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "穿衣";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem15,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem35,
            this.emptySpaceItem3});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(594, 278);
            this.layoutControlGroup5.Text = "穿衣";
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.emptySpaceItem15.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem15.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.emptySpaceItem15.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem15.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem15.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem15.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem15.CustomizationFormText = "（3）穿衣：穿衣裤、袜子、鞋子等活动";
            this.emptySpaceItem15.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem15.MinSize = new System.Drawing.Size(1, 1);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(594, 57);
            this.emptySpaceItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem15.Text = "（3）穿衣：穿衣裤、袜子、鞋子等活动";
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            this.emptySpaceItem15.TextVisible = true;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.chk穿衣31;
            this.layoutControlItem15.CustomizationFormText = "layoutControlItem15";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 57);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(472, 57);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "layoutControlItem15";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextToControlDistance = 0;
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem16.Control = this.chk穿衣32;
            this.layoutControlItem16.CustomizationFormText = "layoutControlItem16";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 114);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(472, 52);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "layoutControlItem16";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextToControlDistance = 0;
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem17.Control = this.chk穿衣33;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 166);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(472, 62);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.result3;
            this.layoutControlItem35.CustomizationFormText = "layoutControlItem35";
            this.layoutControlItem35.Location = new System.Drawing.Point(472, 57);
            this.layoutControlItem35.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(122, 171);
            this.layoutControlItem35.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem35.Text = "layoutControlItem35";
            this.layoutControlItem35.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem35.TextToControlDistance = 0;
            this.layoutControlItem35.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 228);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(594, 50);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.CustomizationFormText = "如厕";
            this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem25,
            this.layoutControlItem18,
            this.layoutControlItem20,
            this.layoutControlItem19,
            this.layoutControlItem36,
            this.layoutControlItem21,
            this.emptySpaceItem4});
            this.layoutControlGroup12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.Size = new System.Drawing.Size(594, 278);
            this.layoutControlGroup12.Text = "如厕";
            // 
            // emptySpaceItem25
            // 
            this.emptySpaceItem25.AllowHotTrack = false;
            this.emptySpaceItem25.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.emptySpaceItem25.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem25.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.emptySpaceItem25.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem25.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem25.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem25.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem25.CustomizationFormText = "（4）如厕：小便、大便等活动及自控";
            this.emptySpaceItem25.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem25.MinSize = new System.Drawing.Size(1, 1);
            this.emptySpaceItem25.Name = "emptySpaceItem25";
            this.emptySpaceItem25.Size = new System.Drawing.Size(594, 55);
            this.emptySpaceItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem25.Text = "（4）如厕：小便、大便等活动及自控";
            this.emptySpaceItem25.TextSize = new System.Drawing.Size(0, 0);
            this.emptySpaceItem25.TextVisible = true;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem18.Control = this.chk如厕41;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 55);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(493, 46);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "layoutControlItem18";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextToControlDistance = 0;
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem20.Control = this.chk如厕42;
            this.layoutControlItem20.CustomizationFormText = "layoutControlItem20";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 101);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(493, 46);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "layoutControlItem20";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextToControlDistance = 0;
            this.layoutControlItem20.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.chk如厕43;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 147);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(493, 45);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "layoutControlItem19";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextToControlDistance = 0;
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.result4;
            this.layoutControlItem36.CustomizationFormText = "layoutControlItem36";
            this.layoutControlItem36.Location = new System.Drawing.Point(493, 55);
            this.layoutControlItem36.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(101, 180);
            this.layoutControlItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem36.Text = "layoutControlItem36";
            this.layoutControlItem36.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem36.TextToControlDistance = 0;
            this.layoutControlItem36.TextVisible = false;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem21.Control = this.chk如厕44;
            this.layoutControlItem21.CustomizationFormText = "layoutControlItem21";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 192);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(493, 43);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "layoutControlItem21";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextToControlDistance = 0;
            this.layoutControlItem21.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 235);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(594, 43);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup13
            // 
            this.layoutControlGroup13.CustomizationFormText = "活动";
            this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem37,
            this.layoutControlItem25,
            this.emptySpaceItem31,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem24,
            this.emptySpaceItem5,
            this.emptySpaceItem6});
            this.layoutControlGroup13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup13.Name = "layoutControlGroup13";
            this.layoutControlGroup13.Size = new System.Drawing.Size(594, 278);
            this.layoutControlGroup13.Text = "活动";
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.result5;
            this.layoutControlItem37.CustomizationFormText = "layoutControlItem37";
            this.layoutControlItem37.Location = new System.Drawing.Point(485, 45);
            this.layoutControlItem37.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(109, 183);
            this.layoutControlItem37.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem37.Text = "layoutControlItem37";
            this.layoutControlItem37.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem37.TextToControlDistance = 0;
            this.layoutControlItem37.TextVisible = false;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem25.Control = this.chk活动54;
            this.layoutControlItem25.CustomizationFormText = "layoutControlItem25";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 177);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(485, 51);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Tag = "10";
            this.layoutControlItem25.Text = "layoutControlItem25";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextToControlDistance = 0;
            this.layoutControlItem25.TextVisible = false;
            // 
            // emptySpaceItem31
            // 
            this.emptySpaceItem31.AllowHotTrack = false;
            this.emptySpaceItem31.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.emptySpaceItem31.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem31.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.emptySpaceItem31.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem31.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem31.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem31.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem31.CustomizationFormText = "（5）活动：站立、室内行走、上下楼梯、户外活动";
            this.emptySpaceItem31.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem31.MinSize = new System.Drawing.Size(1, 1);
            this.emptySpaceItem31.Name = "emptySpaceItem31";
            this.emptySpaceItem31.Size = new System.Drawing.Size(594, 1);
            this.emptySpaceItem31.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem31.Text = "（5）活动：站立、室内行走、上下楼梯、户外活动";
            this.emptySpaceItem31.TextSize = new System.Drawing.Size(0, 0);
            this.emptySpaceItem31.TextVisible = true;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem22.Control = this.chk活动51;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 45);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(485, 41);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "layoutControlItem22";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextToControlDistance = 0;
            this.layoutControlItem22.TextVisible = false;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.chk活动52;
            this.layoutControlItem23.CustomizationFormText = "layoutControlItem23";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 86);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(485, 48);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "layoutControlItem23";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextToControlDistance = 0;
            this.layoutControlItem23.TextVisible = false;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.chk活动53;
            this.layoutControlItem24.CustomizationFormText = "layoutControlItem24";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 134);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(485, 43);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.Text = "layoutControlItem24";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem24.TextToControlDistance = 0;
            this.layoutControlItem24.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.emptySpaceItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem5.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.emptySpaceItem5.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem5.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem5.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem5.CustomizationFormText = "（5）活动：站立、室内行走、上下楼梯、户外活动";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 1);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(594, 44);
            this.emptySpaceItem5.Text = "（5）活动：站立、室内行走、上下楼梯、户外活动";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            this.emptySpaceItem5.TextVisible = true;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 228);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(594, 50);
            this.emptySpaceItem6.Text = "emptySpaceItem6";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btn上一项;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 312);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(300, 42);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btn下一项;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(300, 312);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(300, 42);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // uc老年人随访
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "uc老年人随访";
            this.Size = new System.Drawing.Size(600, 400);
            this.Load += new System.EventHandler(this.UC老年人生活自理能力评估表_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt随访医生签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访目标.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk活动54.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk活动53.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk活动52.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk活动51.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk如厕44.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk如厕42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk如厕43.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk如厕41.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk穿衣33.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk穿衣32.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk穿衣31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk梳洗24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk梳洗23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk梳洗22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk梳洗21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk进餐13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk进餐12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk进餐11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl随访日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl下次随访日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl录入医生)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl随访医生签名)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.result)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.CheckEdit chk进餐13;
        private DevExpress.XtraEditors.CheckEdit chk进餐12;
        private DevExpress.XtraEditors.CheckEdit chk进餐11;
        private DevExpress.XtraEditors.CheckEdit chk梳洗22;
        private DevExpress.XtraEditors.CheckEdit chk梳洗21;
        private DevExpress.XtraEditors.CheckEdit chk梳洗24;
        private DevExpress.XtraEditors.CheckEdit chk梳洗23;
        private DevExpress.XtraEditors.CheckEdit chk如厕41;
        private DevExpress.XtraEditors.CheckEdit chk穿衣33;
        private DevExpress.XtraEditors.CheckEdit chk穿衣32;
        private DevExpress.XtraEditors.CheckEdit chk穿衣31;
        private DevExpress.XtraEditors.CheckEdit chk如厕44;
        private DevExpress.XtraEditors.CheckEdit chk如厕42;
        private DevExpress.XtraEditors.CheckEdit chk如厕43;
        private DevExpress.XtraEditors.CheckEdit chk活动54;
        private DevExpress.XtraEditors.CheckEdit chk活动53;
        private DevExpress.XtraEditors.CheckEdit chk活动52;
        private DevExpress.XtraEditors.CheckEdit chk活动51;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem38;
        private DevExpress.XtraEditors.MemoEdit txt下次随访目标;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraEditors.TextEdit txt随访医生签名;
        private DevExpress.XtraEditors.TextEdit txt录入医生;
        private DevExpress.XtraLayout.LayoutControlItem lbl随访日期;
        private DevExpress.XtraLayout.LayoutControlItem lbl录入医生;
        private DevExpress.XtraLayout.LayoutControlItem lbl下次随访日期;
        private DevExpress.XtraLayout.LayoutControlItem lbl随访医生签名;
        private DevExpress.XtraEditors.DateEdit dte随访日期;
        private DevExpress.XtraEditors.DateEdit dte下次随访日期;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.LabelControl result1;
        private DevExpress.XtraEditors.LabelControl result4;
        private DevExpress.XtraEditors.LabelControl result3;
        private DevExpress.XtraEditors.LabelControl result2;
        private DevExpress.XtraEditors.LabelControl result5;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem result;
        private DevExpress.XtraEditors.SimpleButton btn下一项;
        private DevExpress.XtraEditors.SimpleButton btn上一项;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.SimpleButton sBut_Get参照数据;

    }
}
