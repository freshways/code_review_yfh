﻿using AtomEHR.Library;
using ATOMEHR_LeaveClient.SqlSugar_DALs;
using ATOMEHR_LeaveClient.SqlSugar_Models;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ATOMEHR_LeaveClient
{

    public partial class uc健康体检表3 : UserControl
    {
        /*
     1.    症状选项对应的  code值   zz_zhengzhuang
         *ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
                    10002498	zz_zhengzhuang	症状	1	WZZ、MZZ	无症状		1
                    10002621	zz_zhengzhuang	症状	2	TT	头痛		1
                    10002624	zz_zhengzhuang	症状	3	TY	头晕		1
                    10002625	zz_zhengzhuang	症状	4	XJ	心悸		1
                    10002626	zz_zhengzhuang	症状	5	XM	胸闷		1
                    10002627	zz_zhengzhuang	症状	6	XT	胸痛		1
                    10002628	zz_zhengzhuang	症状	7	MXKS、MXHS	慢性咳嗽		1
                    10002629	zz_zhengzhuang	症状	8	KT、HT	咳痰		1
                    10003084	zz_zhengzhuang	症状	9	HXKN	呼吸困难		1
                    10003085	zz_zhengzhuang	症状	10	DY	多饮		1
                    10003086	zz_zhengzhuang	症状	11	DS、DN	多尿		1
                    10003087	zz_zhengzhuang	症状	12	TZXX、TCXX、TZXJ、	体重下降		1
                    10003088	zz_zhengzhuang	症状	13	FL	乏力		1
                    10003089	zz_zhengzhuang	症状	14	GJZT	关节肿痛		1
                    10003090	zz_zhengzhuang	症状	15	SLMH	视力模糊		1
                    10003091	zz_zhengzhuang	症状	16	SJMM	手脚麻木		1
                    10003092	zz_zhengzhuang	症状	17	SJ、NJ	尿急		1
                    10003093	zz_zhengzhuang	症状	18	ST、NT	尿痛		1
                    10003094	zz_zhengzhuang	症状	19	PM、BM、PL、BL、PB、	便秘		1
                    10003105	zz_zhengzhuang	症状	20	FX	腹泻		1
                    10003106	zz_zhengzhuang	症状	21	WXOT、EXOT	恶心呕吐		1
                    10003107	zz_zhengzhuang	症状	22	YH	眼花		1
                    10003108	zz_zhengzhuang	症状	23	EM	耳鸣		1
                    10003109	zz_zhengzhuang	症状	24	RFZT	乳房胀痛		1
                    10003110	zz_zhengzhuang	症状	99	QT、JT	其他		1
         * 
         * 
       2.  锻炼频率  dlpl
         *ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10003299	dlpl	锻炼频率	1		每天		1
            10003300	dlpl	锻炼频率	2		每周一次以上		1
            10003301	dlpl	锻炼频率	3		偶尔		1
            10003302	dlpl	锻炼频率	4		不锻炼		1
         * 
         * 
         *3. 饮食习惯  ysxg
         ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10002888	ysxg	饮食习惯	5		嗜油		1
            10002889	ysxg	饮食习惯	6		嗜糖		1
            10002890	ysxg	饮食习惯	1		荤素均衡		1
            10002891	ysxg	饮食习惯	2		荤食为主		1
            10002892	ysxg	饮食习惯	3		素食为主		1
            10002893	ysxg	饮食习惯	4		嗜盐		1
         * 
         * 
         4.吸烟情况 
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10003482	glkxyqk	管理卡吸烟情况	1		不吸烟		1
            10003483	glkxyqk	管理卡吸烟情况	2		已戒烟		1
            10003484	glkxyqk	管理卡吸烟情况	3		吸烟		1   
         * 
         5.饮酒频率  yjpl
         *ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10002832	yjpl	饮酒频率	1		从不		1
            10002833	yjpl	饮酒频率	2		偶尔		1
            10002834	yjpl	饮酒频率	3		经常		1
            10002835	yjpl	饮酒频率	4		每天		1
         * 
         6.是否戒酒
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10003111	sfjj	是否戒酒	1	WJJ	未戒酒		1
            10003112	sfjj	是否戒酒	2	YJJ	已戒酒		1
         * 
         7.饮酒种类
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
        10002836	yjzl	饮酒种类	1		白酒		1
        10002837	yjzl	饮酒种类	2		啤酒		1
        10002838	yjzl	饮酒种类	3		红酒		1
        10002839	yjzl	饮酒种类	4		黄酒		1
        10002840	yjzl	饮酒种类	99		其他		1
         * 
         8.口唇
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10002072	kc_houchun	口唇	1		红润		1
            10002073	kc_houchun	口唇	2		苍白		1
            10002074	kc_houchun	口唇	3		发绀		1
            10002075	kc_houchun	口唇	4		皲裂		1
            10002076	kc_houchun	口唇	5		疱疹		1
            10002077	kc_houchun	口唇	6	QT,JT	其他		1
         * 
         9.咽部
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10002748	yb_yanbu	咽部	1		无充血		1
            10002749	yb_yanbu	咽部	2		充血		1
            10002750	yb_yanbu	咽部	3		淋巴滤泡增生		1
            10002751	yb_yanbu	咽部	4	QT,JT	其他		1
         * 
         10.听力
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10002583	tingli	听力	1	TJ、TX	听见		1
            10002584	tingli	听力	2	TBQHWFTJ（EBHKZK	听不清或无法听见（耳鼻喉科专科就诊）		1
         * 
         11.运动功能
            10002894	yundong	运动功能	1	KSLWC	可顺利完成		1
            10002895	yundong	运动功能	2	WFDLWCQZRHYGDZ(	无法独立完成其中任何一个动作(上级医院就诊)		1
         * 
         12.正常异常
            10002936	zcyc	正常异常	1		正常		1
            10002937	zcyc	正常异常	2		异常		1
         * 
        13.下肢水肿
            10002739	xzsz	下肢水肿	4		双侧不对称		1
            10002740	xzsz	下肢水肿	5		双侧对称		1
            10002736	xzsz	下肢水肿	1		无		1
            10002737	xzsz	下肢水肿	2		左侧		1
            10002738	xzsz	下肢水肿	3		右侧		1
         * 
         14.
            10002932	zbdmbd	足背动脉搏动	1		未触及		1
            10002933	zbdmbd	足背动脉搏动	2		触及双侧对称		1
            10002934	zbdmbd	足背动脉搏动	3		触及左侧弱或消失		1
            10002935	zbdmbd	足背动脉搏动	4		触及右侧弱或消失		1
         * 
         15.肛门指诊
            10002492	gmzz	肛门指诊	1	WJYC、WXYC	未见异常		1
            10002493	gmzz	肛门指诊	2	CT	触痛		1
            10003051	gmzz	肛门指诊	3	BK	包块		1
            10003052	gmzz	肛门指诊	4	QLXYC	前列腺异常		1
            10003053	gmzz	肛门指诊	99	QT、JT	其他		1
         * 
         16.乳腺
            10002447	rx_ruxian	乳腺	1		未见异常		1
            10002448	rx_ruxian	乳腺	2		乳房切除		1
            10002449	rx_ruxian	乳腺	3		异常泌乳		1
            10002450	rx_ruxian	乳腺	4		乳腺包块		1
            10002451	rx_ruxian	乳腺	99		其他		1
         * 
         17.阴性阳性
            10002905	yxyx	阴性阳性	1		阴性		1
            10002906	yxyx	阴性阳性	2		阳性		1
         * 
         18.中药体质辨识
            10003047	zytzbs	中药体质辨识	1		是		1
            10003048	zytzbs	中药体质辨识	2		倾向是		1
            10003049	zytzbsphz	中药体质辨识平和质	1		是		1
            10003050	zytzbsphz	中药体质辨识平和质	2		基本是		1
         * 
         19.脑血管疾病
            10002290	nxgjb	脑血管疾病	1		未发现		1
            10002291	nxgjb	脑血管疾病	2		缺血性卒中		1
            10002292	nxgjb	脑血管疾病	3		脑出血		1
            10002293	nxgjb	脑血管疾病	4		蛛网膜下腔出血		1
            10002294	nxgjb	脑血管疾病	5		短暂性脑缺血发作		1
            10002295	nxgjb	脑血管疾病	99		其他		1
         * 
         20.肾脏疾病
            10002564	szjb	肾脏疾病	1		未发现		1
            10002565	szjb	肾脏疾病	2		糖尿病肾病		1
            10002566	szjb	肾脏疾病	3		肾功能衰竭		1
            10002567	szjb	肾脏疾病	4		急性肾炎		1
            10002568	szjb	肾脏疾病	5		慢性肾炎		1
            10002569	szjb	肾脏疾病	99		其他		1
         * 
         21.心脏疾病
            10002725	xzjb	心脏疾病	1		未发现		1
            10002726	xzjb	心脏疾病	2		心肌梗死		1
            10002727	xzjb	心脏疾病	3		心绞痛		1
            10002728	xzjb	心脏疾病	4		冠状动脉血运重建		1
            10002729	xzjb	心脏疾病	5		充血性心力衰竭		1
            10002730	xzjb	心脏疾病	6		心前区疼痛		1
            10002731	xzjb	心脏疾病	99		其他		1
         * 
         22.血管疾病
            10002678	xgjb	血管疾病	1		未发现		1
            10002679	xgjb	血管疾病	2		夹层动脉瘤		1
            10002680	xgjb	血管疾病	3		动脉闭塞性疾病		1
            10002681	xgjb	血管疾病	99		其他		1
         * 
         23.眼部疾病
            10002752	ybjb	眼部疾病	1		未发现		1
            10002753	ybjb	眼部疾病	2		视网膜出血或渗出		1
            10002754	ybjb	眼部疾病	3		视乳头水肿		1
            10002755	ybjb	眼部疾病	4		白内障		1
            10002756	ybjb	眼部疾病	99		其他		1
         * 
         24.未发现有
            10002630	wfxy	未发现有	1		未发现		1
            10002631	wfxy	未发现有	2		有		1
         * 
         25.齿列
                10003219	cl_chilei	齿列	1		正常		1
                10003220	cl_chilei	齿列	2		缺齿		1
                10003221	cl_chilei	齿列	3		龋齿		1
                10003222	cl_chilei	齿列	4		义齿(假牙)		1
                10003223	cl_chilei	齿列	5	QT,JT	其他		1
         * 
         26.体检有无异常
                10002585	tjywyc	体检有无异常	1		体检无异常		1
                10002586	tjywyc	体检有无异常	2		有异常		1
         * 
         27.体检有无异常
                10002585	tjywyc	体检有无异常	1		体检无异常		1
                10002586	tjywyc	体检有无异常	2		有异常		1
         * 
         28.健康指导
                10001896	jkzd	健康指导	1		定期随访		1
                10001897	jkzd	健康指导	2		纳入慢性病管理		1
                10001898	jkzd	健康指导	3		建议复查		1
                10001899	jkzd	健康指导	4		建议转诊		1
         * 
         29.危险因素指导
                10002653	wxyszd	危险因素指导	1		戒烟		1
                10002654	wxyszd	危险因素指导	2		健康饮酒		1
                10002655	wxyszd	危险因素指导	3		饮食		1
                10002656	wxyszd	危险因素指导	4		锻炼		1
                10002657	wxyszd	危险因素指导	97		减体重		1
                10002658	wxyszd	危险因素指导	98		建议疫苗接种		1
                10002659	wxyszd	危险因素指导	99		其他		1
         * 
         30.老年人状况评估
                10002990	zk_pg	老年人状况评估	1	NULL	满意	NULL	1
                10002991	zk_pg	老年人状况评估	2	NULL	基本满意	NULL	1
                10002992	zk_pg	老年人状况评估	3	NULL	说不清楚	NULL	1
                10002993	zk_pg	老年人状况评估	4	NULL	不太满意	NULL	1
                10002994	zk_pg	老年人状况评估	5	NULL	不满意	NULL	1
         * 
         31.老年人自理评估
                10002995	zl_pg	老年人自理评估	1	NULL	可自理(0～3分)	NULL	1
                10002996	zl_pg	老年人自理评估	2	NULL	轻度依赖(4～8分)	NULL	1
                10002997	zl_pg	老年人自理评估	3	NULL	中度依赖(9～18分)	NULL	1
                10002998	zl_pg	老年人自理评估	4	NULL	不能自理(≥19分)	NULL	1
         * 
         32.阴性阳性
                10002905	yxyx	阴性阳性	1		阴性		1
                10002906	yxyx	阴性阳性	2		阳性		1
         * 
         33.职业暴露详细
                10003131	baoluqk	职业暴露详细	1	W,M	无		1
                10003132	baoluqk	职业暴露详细	2	BX	不详		1
                10003133	baoluqk	职业暴露详细	3	Y	有		1
         * 
         34. pifu_pf  皮肤
            10002382	pifu_pf	皮肤	1		正常		1
            10002383	pifu_pf	皮肤	2		潮红		1
            10002384	pifu_pf	皮肤	3		苍白		1
            10002385	pifu_pf	皮肤	4		发绀		1
            10002386	pifu_pf	皮肤	5		黄染		1
            10002387	pifu_pf	皮肤	6		色素沉着		1
            10002388	pifu_pf	皮肤	99		其他		1
         * 
         35.
            10003492	gm_gongmo	巩膜	1		正常		1
            10003493	gm_gongmo	巩膜	2		黄染		1
            10003494	gm_gongmo	巩膜	3		充血		1
            10003495	gm_gongmo	巩膜	99		其他		1
         * 
         36.lbj2
         * 10002110  	lbj2	淋巴结	1		未触及		1
            10002111	lbj2	淋巴结	2		锁骨上		1
            10002112	lbj2	淋巴结	3		腋窝		1
            10002113	lbj2	淋巴结	4		其他		1
         * 
         37 . luoyin
         * 10002145	    luoyin	罗音	1	W、M	无		1
            10002146	luoyin	罗音	2	GLY	干罗音		1
            10002619	luoyin	罗音	3	SLY	湿罗音		1
            10002620	luoyin	罗音	4	QT、JT	其他		1
         * 
         38.心脏心律
         * 10002741 	xzxl	心脏心律	1		齐		1
            10002742	xzxl	心脏心律	2		不齐		1
            10002743	xzxl	心脏心律	3		绝对不齐		1
         * 
         39.
         * 10002667 	wy_wuyou	无有	1		无		1
            10002668	wy_wuyou	无有	2		有		1
            10002896	yw_youwu	有无	1		有		1
            10002897	yw_youwu	有无	2		无		1
         * 
        40.
         * 10002898 	ywyc	有无异常	1		未见异常		1
            10002899	ywyc	有无异常	99		异常		1
         */

        #region Constructor
        public uc健康体检表3()
        {
            InitializeComponent();
            tb_健康体检Info tb = tb_健康体检DAL.Gettb_健康体检InfoById(Program.currentUser.ID);
            if (tb != null)//已经查体
            {
                DobindingDatasource(tb);
            }
        }
        public uc健康体检表3(User _currentUser)
        {
            InitializeComponent();
            tb_健康体检Info tb = tb_健康体检DAL.Gettb_健康体检InfoById(Program.currentUser.ID);
            if (tb != null)//已经查体
            {
                DobindingDatasource(tb);
            }
        }
        #endregion

        #region Handle Events
        private void UC健康体检表_Load(object sender, EventArgs e)
        {
            InitView();
            //TODO:
            //if (_ds健康体检.Tables[0])//修改的时候绑定已存在的数据
            //{
            //    DobindingDatasource(Program.currentUser.Tab_健康体检);
            //    _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.修改时间] = _serverDateTime;
            //    _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.修改人] = Loginer.Program.currentUser.用户编码;
            //}
            //else  //新增
            //{
            //    //this.dte创建时间.EditValue = _serverDateTime;
            //    //this.txt创建人.EditValue = Loginer.Program.currentUser.AccountName;
            //    //this.dte最近更新时间.EditValue = _serverDateTime;
            //    //this.txt创建机构.EditValue = Loginer.Program.currentUser.所属机构名称;
            //    //this.txt当前所属机构.EditValue = Loginer.Program.currentUser.所属机构名称;
            //    //this.txt最近修改人.EditValue = Loginer.Program.currentUser.AccountName;

            //    //_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.个人档案编号] = _docNo;
            //    //_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.创建时间] = _serverDateTime;
            //    //_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.创建人] = Loginer.Program.currentUser.用户编码;
            //    //_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.创建机构] = Loginer.Program.currentUser.所属机构;
            //    //_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.修改时间] = _serverDateTime;
            //    //_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.修改人] = Loginer.Program.currentUser.用户编码;
            //    //_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.所属机构] = Loginer.Program.currentUser.所属机构;
            //}
            //this.gc住院史.DataSource = _ds健康体检.Tables["住院史"];
            //this.gc建床史.DataSource = _ds健康体检.Tables["家庭病床史"];
            //this.gc用药情况.DataSource = _ds健康体检.Tables[tb_健康体检_用药情况表.__TableName];
            //this.gc接种史.DataSource = _ds健康体检.Tables[tb_健康体检_非免疫规划预防接种史.__TableName];
            //if (_ds健康体检.Tables["住院史"].Rows.Count > 0)
            //{
            //    this.lblgc住院史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            //    this.radio住院史.EditValue = "1";
            //}
            //else
            //{
            //    this.radio住院史.EditValue = "2";
            //}
            //if (_ds健康体检.Tables["家庭病床史"].Rows.Count > 0)
            //{
            //    this.lblgc家庭病床史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            //    this.radio家庭病床史.EditValue = "1";
            //}
            //else
            //{
            //    this.radio家庭病床史.EditValue = "2";
            //}
            //if (_ds健康体检.Tables[tb_健康体检_用药情况表.__TableName].Rows.Count > 0)
            //{
            //    this.lblgc用药情况.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            //    this.radio主要用药情况.EditValue = "1";
            //}
            //else
            //{
            //    this.radio主要用药情况.EditValue = "2";
            //}
            //if (_ds健康体检.Tables[tb_健康体检_非免疫规划预防接种史.__TableName].Rows.Count > 0)
            //{
            //    this.lblgc接种史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            //    this.radio预防接种史.EditValue = "1";
            //}
            //else
            //{
            //    this.radio预防接种史.EditValue = "2";
            //}

            //if (_ds健康体检.Tables[tb_MXB高血压管理卡.__TableName].Rows.Count > 0)//存在管理卡，为高血压患者
            //    this.txt危险因素其他.Text = "低盐饮食";
            //else
            //    this.txt危险因素其他.Text = "减盐防控高血压";

            Binding_医师签字();
        }

        //Begin WXF 2018-11-21 | 15:57
        //绑定医师签字 
        private void Binding_医师签字()
        {
            dal_医生信息 dal医生信息 = new dal_医生信息();
            DataTable dt = dal医生信息.Get医生信息ToLookUp();

            lookUpEdit_医师签字.Properties.ValueMember = "b编码";
            lookUpEdit_医师签字.Properties.DisplayMember = "x医生姓名";
            lookUpEdit_医师签字.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("x医生姓名", "医生姓名"));
            lookUpEdit_医师签字.Properties.DataSource = dt;
            lookUpEdit_医师签字.EditValue = "请选择";
        }
        //End
        private void chk_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit checkEdit = (CheckEdit)sender;
            if (checkEdit == null) return;
            SetControlEnable(checkEdit);
        }
        private void chk其他_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit checkEdit = (CheckEdit)sender;
            if (checkEdit == null) return;
            string name = checkEdit.Name;
            string target = "txt" + name.Substring(3, name.IndexOf('_') - 2) + "其他";
            Control[] list = this.Controls.Find(target, true);
            if (list.Length == 1)
            {
                if (list[0] is TextEdit)
                {
                    TextEdit txt = (TextEdit)list[0];
                    SetTextEditEnable(checkEdit, txt);
                }
            }

        }
        private void radio口唇_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio口唇.EditValue == null ? "" : this.radio口唇.EditValue.ToString();
            this.txt口唇其他.Enabled = !string.IsNullOrEmpty(index) && index == "6" ? true : false;
            if (!string.IsNullOrEmpty(index) && index != "6") this.txt口唇其他.Text = "";
        }
        private void radio咽部_其他_CheckedChanged(object sender, EventArgs e)
        {
            this.txt咽部_其他.Enabled = this.radio咽部_其他.Checked;
            if (this.radio咽部_其他.Checked == false) this.txt咽部_其他.Text = "";
        }
        private void radio眼底_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio眼底.EditValue == null ? "" : this.radio眼底.EditValue.ToString();
            this.txt眼底.Enabled = !string.IsNullOrEmpty(index) && index == "2" ? true : false;
            if (!string.IsNullOrEmpty(index) && index != "2")
            {
                this.txt眼底.Text = "";
            }
        }
        private void radio呼吸音_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio呼吸音, txt呼吸音_异常);
            //RadioGroup radio = (RadioGroup)sender;
            //if (radio == null) return;

            //string index = this.radio.EditValue.ToString();
            //this.txt呼吸音_异常.Enabled = !string.IsNullOrEmpty(index) && index == "2" ? true : false;
        }
        private void radio杂音_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio杂音, txt杂音);
        }
        private void radio压痛_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio压痛, txt压痛);
        }
        private void radio移动性浊音_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio移动性浊音, txt移动性浊音);
        }
        private void radio脾大_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio脾大, txt脾大);
        }
        private void radio肝大_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio肝大, txt肝大);
        }
        private void radio包块_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio包块, txt包块);
        }

        private void chk齿列_正常_CheckedChanged(object sender, EventArgs e)
        {
            bool index = this.chk齿列_正常.Checked;
            Set齿列ControlEditable(index);
        }
        private void chk齿列_缺齿_CheckedChanged(object sender, EventArgs e)
        {
            bool index = this.chk齿列_缺齿.Checked;
            this.txt缺齿1.Enabled = index;
            this.txt缺齿2.Enabled = index;
            this.txt缺齿3.Enabled = index;
            this.txt缺齿4.Enabled = index;
            this.txt缺齿1.Text = index ? this.txt缺齿1.Text : "";
            this.txt缺齿2.Text = index ? this.txt缺齿2.Text : "";
            this.txt缺齿3.Text = index ? this.txt缺齿3.Text : "";
            this.txt缺齿4.Text = index ? this.txt缺齿4.Text : "";
        }
        private void chk齿列_龋齿_CheckedChanged(object sender, EventArgs e)
        {
            bool index = this.chk齿列_龋齿.Checked;
            this.txt龋齿1.Enabled = index;
            this.txt龋齿2.Enabled = index;
            this.txt龋齿3.Enabled = index;
            this.txt龋齿4.Enabled = index;
            this.txt龋齿1.Text = index ? this.txt龋齿1.Text : "";
            this.txt龋齿2.Text = index ? this.txt龋齿2.Text : "";
            this.txt龋齿3.Text = index ? this.txt龋齿3.Text : "";
            this.txt龋齿4.Text = index ? this.txt龋齿4.Text : "";
        }
        private void chk齿列_义齿_CheckedChanged(object sender, EventArgs e)
        {
            bool index = this.chk齿列_义齿.Checked;
            this.txt义齿1.Enabled = index;
            this.txt义齿2.Enabled = index;
            this.txt义齿3.Enabled = index;
            this.txt义齿4.Enabled = index;
            this.txt义齿1.Text = index ? this.txt义齿1.Text : "";
            this.txt义齿2.Text = index ? this.txt义齿2.Text : "";
            this.txt义齿3.Text = index ? this.txt义齿3.Text : "";
            this.txt义齿4.Text = index ? this.txt义齿4.Text : "";
        }
        private void chk齿列_其他_CheckedChanged(object sender, EventArgs e)
        {
            bool index = this.chk齿列_其他.Checked;
            //this.txt其他.Text = index ? "" : this.txt其他.Text;
            this.txt齿列_其他.Enabled = index;
            this.txt齿列_其他.Text = index ? this.txt齿列_其他.Text : "";

        }

        private void btn保存_Click(object sender, EventArgs e)
        {
            try
            {
                if (f保存检查())
                {
                    tb_健康体检Info tb = new tb_健康体检Info();
                    tb.身份证号 = Program.currentUser.ID;
                    tb.姓名 = Program.currentUser.Name;
                    tb.性别 = Program.currentUser.Gender;
                    tb.出生日期 = Program.currentUser.Birth;
                    tb.个人档案编号 = Program.currentUser.DocNo;
                    //return;
                    tb.G_QLX = "";
                    #region 脏器功能
                    tb.口唇 = this.radio口唇.Text.Trim();
                    tb.口唇其他 = this.txt口唇其他.Text.Trim();

                    string 齿列 = string.Empty;
                    if (chk齿列_正常.Checked) { 齿列 = "1"; }
                    else
                    {
                        if (chk齿列_缺齿.Checked) 齿列 += "2,";
                        if (chk齿列_龋齿.Checked) 齿列 += "3,";
                        if (chk齿列_义齿.Checked) 齿列 += "4,";
                        if (chk齿列_其他.Checked) 齿列 += "5";
                    }


                    tb.齿列 = 齿列;
                    tb.齿列龋齿 = this.txt龋齿1.Text.Trim() + "@" + this.txt龋齿2.Text.Trim() + "@" + this.txt龋齿3.Text.Trim() + "@" + this.txt龋齿4.Text.Trim();
                    tb.齿列缺齿 = this.txt缺齿1.Text.Trim() + "@" + this.txt缺齿2.Text.Trim() + "@" + this.txt缺齿3.Text.Trim() + "@" + this.txt缺齿4.Text.Trim();
                    tb.齿列义齿 = this.txt义齿1.Text.Trim() + "@" + this.txt义齿2.Text.Trim() + "@" + this.txt义齿3.Text.Trim() + "@" + this.txt义齿4.Text.Trim();
                    tb.齿列其他 = this.txt齿列_其他.Text.Trim();
                    tb.咽部 = ControlsHelper.GetFlowLayoutResult(flow咽部).ToString();
                    tb.咽部其他 = this.txt咽部_其他.Text.Trim();
                    if (!string.IsNullOrEmpty(this.txt左眼视力.Text.Trim()))
                        tb.左眼视力 = Convert.ToDouble(this.txt左眼视力.Text.Trim());

                    if (!string.IsNullOrEmpty(this.txt右眼视力.Text.Trim()))
                        tb.右眼视力 = Convert.ToDouble(this.txt右眼视力.Text.Trim());

                    //if (!string.IsNullOrEmpty(this.txt矫正左眼视力.Text.Trim()))
                    tb.左眼矫正 = this.txt矫正左眼视力.Text.Trim();
                    //if (!string.IsNullOrEmpty(this.txt矫正右眼视力.Text.Trim()))
                    tb.右眼矫正 = this.txt矫正右眼视力.Text.Trim();

                    tb.听力 = ControlsHelper.GetFlowLayoutResult(flow听力).ToString();
                    tb.运动功能 = ControlsHelper.GetFlowLayoutResult(flow运动功能).ToString();
                    #endregion

                    #region 查体
                    tb.眼底 = this.radio眼底.Text.Trim();
                    tb.眼底异常 = this.txt眼底.Text.Trim();
                    tb.皮肤 = ControlsHelper.GetFlowLayoutResult(flow皮肤).ToString();
                    tb.皮肤其他 = this.txt皮肤_其他.Text.Trim();
                    tb.巩膜 = ControlsHelper.GetFlowLayoutResult(flow巩膜).ToString();
                    tb.巩膜其他 = this.txt巩膜_其他.Text.Trim();
                    tb.淋巴结 = ControlsHelper.GetFlowLayoutResult(flow淋巴结).ToString();
                    tb.淋巴结其他 = this.txt淋巴结_其他.Text.Trim();

                    tb.桶状胸 = this.radio桶状胸.Text.Trim();
                    tb.呼吸音 = this.radio呼吸音.Text.Trim();
                    tb.呼吸音异常 = this.txt呼吸音_异常.Text.Trim();
                    tb.罗音 = ControlsHelper.GetFlowLayoutResult(flow罗音).ToString();
                    tb.罗音异常 = this.txt罗音_其他.Text.Trim();
                    if (!string.IsNullOrEmpty(this.txt心率.Txt1.Text.Trim()))
                        tb.心率 = Convert.ToInt32(txt心率.Txt1.EditValue);
                    tb.心律 = this.radio心律.Text.Trim();
                    tb.杂音 = this.radio杂音.Text.Trim();
                    tb.杂音有 = this.txt杂音.Text.Trim();
                    tb.压痛 = this.radio压痛.Text.Trim();
                    tb.压痛有 = this.txt压痛.Text.Trim();
                    tb.包块 = this.radio包块.Text.Trim();
                    tb.包块有 = this.txt包块.Text.Trim();
                    tb.肝大 = this.radio肝大.Text.Trim();
                    tb.肝大有 = this.txt肝大.Text.Trim();
                    tb.脾大 = this.radio脾大.Text.Trim();
                    tb.脾大有 = this.txt脾大.Text.Trim();
                    tb.浊音 = this.radio移动性浊音.Text.Trim();
                    tb.浊音有 = this.txt移动性浊音.Text.Trim();

                    tb.下肢水肿 = this.radio下肢水肿.Text.Trim();
                    tb.足背动脉搏动 = this.radio足背动脉搏动.Text.Trim();
                    #endregion

                    //TODO:上传时 要更新此表
                    //_ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.健康体检] = i + "," + j;

                    //tb_健康体检Info list = tb_健康体检DAL.Gettb_健康体检InfoById(Program.currentUser.ID);
                    //bool result = false;
                    ////if (list != null)
                    ////{
                    ////    result = tb_健康体检DAL.Updatetb_健康体检Info(tb);
                    ////}
                    ////else
                    //result = tb_健康体检DAL.Addtb_健康体检(tb);

                    //Begin WXF 2018-11-21 | 21:12
                    //保存医师签字 
                    //if (string.IsNullOrEmpty(doctor_脏器功能) || doctor_脏器功能.Equals("请选择"))
                    //{
                    //    MessageBox.Show("请选择脏器功能的责任医师!");
                    //    return;
                    //}
                    //if (string.IsNullOrEmpty(doctor_查体) || doctor_查体.Equals("请选择"))
                    //{
                    //    MessageBox.Show("请选择查体的责任医师!");
                    //    return;
                    //}

                    Dictionary<tb_健康体检DAL.Module_体检, string> dic = new Dictionary<tb_健康体检DAL.Module_体检, string>();
                    dic.Add(tb_健康体检DAL.Module_体检.脏器功能, doctor_脏器功能);
                    dic.Add(tb_健康体检DAL.Module_体检.查体, doctor_查体);
                    tb.医师签字 = tb_健康体检DAL.JsonToStr_医师签字(Program.currentUser.DocNo, dic);
                    //End

                    if (tb_健康体检DAL.UpdateSys_体检3(tb))
                    {
                        MessageBox.Show("保存成功！");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        //TODO:慢性病 管理
        /*
        private void AddNew慢病基础信息表(DataRow row)
        {
            row[tb_MXB慢病基础信息表.姓名] = util.DESEncrypt.DES加密(this.text姓名.Text.Trim());
            row[tb_MXB慢病基础信息表.拼音简码] = util.DESEncrypt.GetChineseSpell(this.text姓名.Text.Trim());
            row[tb_MXB慢病基础信息表.身份证号] = this.text身份证号.Text.Trim();
            row[tb_MXB慢病基础信息表.出生日期] = string.IsNullOrEmpty(this.text出生日期.Text);
            row[tb_MXB慢病基础信息表.民族] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.民族];//.SelectedIndex;
            row[tb_MXB慢病基础信息表.文化程度] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.文化程度];// ControlsHelper.GetComboxKey(this.cbo文化程度);//.SelectedIndex;
            row[tb_MXB慢病基础信息表.常住类型] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.常住类型]; //ontrolsHelper.GetComboxKey(this.cbo常住类型);//.SelectedIndex;
            row[tb_MXB慢病基础信息表.工作单位] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.工作单位];// this.txt工作单位.Text.Trim();
            row[tb_MXB慢病基础信息表.婚姻状况] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.婚姻状况]; //ControlsHelper.GetComboxKey(this.cbo婚姻状况);//.SelectedIndex;
            row[tb_MXB慢病基础信息表.职业] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.职业]; //ControlsHelper.GetComboxKey(this.cbo职业);//.SelectedIndex;
            row[tb_MXB慢病基础信息表.联系人电话] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.联系人电话]; //this.txt联系人电话.Text.Trim();
            row[tb_MXB慢病基础信息表.省] = "37";
            row[tb_MXB慢病基础信息表.市] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.市]; //this.cbo居住地址_市.EditValue;
            row[tb_MXB慢病基础信息表.区] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.区]; //this.cbo居住地址_县.EditValue;
            row[tb_MXB慢病基础信息表.街道] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.街道]; //this.cbo居住地址_街道.EditValue;
            row[tb_MXB慢病基础信息表.居委会] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.居委会]; //this.cbo居住地址_村委会.EditValue;
            row[tb_MXB慢病基础信息表.所属片区] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.所属片区];// this.cbo所属片区.Text.Trim();
            row[tb_MXB慢病基础信息表.居住地址] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.居住地址];// this.txt居住地址_详细地址.Text.Trim();
            row[tb_MXB慢病基础信息表.医疗费用支付方式] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.医疗费支付类型];// Get医疗费用支付方式();
            //row[tb_MXB慢病基础信息表.医疗费用支付类型其他] = this.txt医疗费用支付方式_其他.Text.Trim();
            row[tb_MXB慢病基础信息表.医疗保险号] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.医疗保险号]; //this.txt医疗保险号.Text.Trim();
            row[tb_MXB慢病基础信息表.新农合号] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.新农合号];// this.txt新农合号.Text.Trim();
            row[tb_MXB慢病基础信息表.建档机构] = Loginer.Program.currentUser.所属机构;
            row[tb_MXB慢病基础信息表.建档时间] = this.dte创建时间.Text.Trim();
            row[tb_MXB慢病基础信息表.建档人] = Loginer.Program.currentUser.用户编码;
            row[tb_MXB慢病基础信息表.创建机构] = Loginer.Program.currentUser.所属机构;
            row[tb_MXB慢病基础信息表.创建时间] = this.dte创建时间.Text.Trim();
            row[tb_MXB慢病基础信息表.创建人] = Loginer.Program.currentUser.用户编码;
            row[tb_MXB慢病基础信息表.更新时间] = this.dte创建时间.Text.Trim();
            row[tb_MXB慢病基础信息表.更新人] = Loginer.Program.currentUser.用户编码;
            row[tb_MXB慢病基础信息表.HAPPENTIME] = this.dte创建时间.Text.Trim();
            row[tb_MXB慢病基础信息表.所属机构] = Loginer.Program.currentUser.所属机构;
            row[tb_MXB慢病基础信息表.高血压随访时间] = "1900-01-01";
            row[tb_MXB慢病基础信息表.糖尿病随访时间] = "1900-01-01";
            row[tb_MXB慢病基础信息表.脑卒中随访时间] = "1900-01-01";
            row[tb_MXB慢病基础信息表.冠心病随访时间] = "1900-01-01";
            row[tb_MXB慢病基础信息表.家庭档案编号] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.家庭档案编号];
            row[tb_MXB慢病基础信息表.个人档案编号] = _docNo;

        }
        private void AddTNBGLK(DataRow row糖尿病)
        {
            row糖尿病[tb_MXB糖尿病管理卡.管理卡编号] = "";
            row糖尿病[tb_MXB糖尿病管理卡.个人档案编号] = _docNo;
            row糖尿病[tb_MXB糖尿病管理卡.所属机构] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.所属机构];
            row糖尿病[tb_MXB糖尿病管理卡.创建机构] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建机构];
            row糖尿病[tb_MXB糖尿病管理卡.创建时间] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建时间];// this.dte录入时间.Text.Trim();
            row糖尿病[tb_MXB糖尿病管理卡.创建人] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建人];// Loginer.Program.currentUser.用户编码;
            row糖尿病[tb_MXB糖尿病管理卡.发生时间] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.调查时间];
            row糖尿病[tb_MXB糖尿病管理卡.修改人] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.修改人];// this.dte录入时间.Text.Trim();
            row糖尿病[tb_MXB糖尿病管理卡.修改时间] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.修改时间];// this.dte录入时间.Text.Trim();
            row糖尿病[tb_MXB糖尿病管理卡.缺项] = "26";
            row糖尿病[tb_MXB糖尿病管理卡.完整度] = "0";
            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.糖尿病管理卡] = "26,0";
            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.糖尿病随访表] = "未建";
            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否糖尿病] = "1";
        }
        private void AddNewGXYGLK(DataRow rowGXYGLK)
        {
            rowGXYGLK[tb_MXB高血压管理卡.管理卡编号] = "";
            rowGXYGLK[tb_MXB高血压管理卡.个人档案编号] = _docNo;
            rowGXYGLK[tb_MXB高血压管理卡.所属机构] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.所属机构];
            rowGXYGLK[tb_MXB高血压管理卡.创建机构] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建机构];
            rowGXYGLK[tb_MXB高血压管理卡.发生时间] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.调查时间];// string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
            rowGXYGLK[tb_MXB高血压管理卡.创建时间] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建时间];// this.dte录入时间.Text.Trim();
            rowGXYGLK[tb_MXB高血压管理卡.创建人] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建人];// Loginer.Program.currentUser.用户编码;
            rowGXYGLK[tb_MXB高血压管理卡.修改人] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.修改人];// this.dte录入时间.Text.Trim();
            rowGXYGLK[tb_MXB高血压管理卡.修改时间] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.修改时间];// this.dte录入时间.Text.Trim();
            //rowGXYGLK[tb_MXB高血压管理卡.终止管理] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.档案状态];
            rowGXYGLK[tb_MXB高血压管理卡.缺项] = "20";
            rowGXYGLK[tb_MXB高血压管理卡.完整度] = "0";

            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.高血压管理卡] = "20,0";
            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.高血压随访表] = "未建";
            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否高血压] = "1";
        }
         */
        #endregion

        #region Private Method
        private bool f保存检查()
        {
            //string 出生日期 = _ds健康档案ForShow.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.出生日期].ToString();
            //if (!string.IsNullOrEmpty(出生日期))
            //{
            //    DateTime dt = DateTime.Now;
            //    if (DateTime.TryParse(出生日期, out dt))
            //    {
            //        if (DateTime.Now.Year - dt.Year >= 35)
            //        {
            //            if (this.txt一般情况_血压左侧.Txt1.Text.Trim() == "" && this.txt一般情况_血压左侧.Txt2.Text.Trim() == "" && this.txt一般情况_血压右侧.Txt1.Text.Trim() == "" && this.txt一般情况_血压右侧.Txt2.Text.Trim() == "")
            //            {
            //                if (Msg.AskQuestion("此人年龄已到35岁，是否输入血压值？"))
            //                {
            //                    this.txt一般情况_血压左侧.Txt1.Focus();
            //                    return false;
            //                }
            //            }
            //        }
            //    }
            //}

            return true;
        }
        /// <summary>
        /// 第一个checkbox是否选中来决定 后续的checkbook的enable状态
        /// </summary>
        /// <param name="firstChk"></param>
        public void SetControlEnable(CheckEdit firstChk)
        {
            bool flag = firstChk.Checked;
            FlowLayoutPanel container = firstChk.Parent as FlowLayoutPanel;
            if (container == null) return;
            Control.ControlCollection controlList = container.Controls;
            for (int i = 1; i < controlList.Count; i++)
            {
                if (controlList[i] is CheckEdit)
                {
                    CheckEdit item = (CheckEdit)controlList[i];
                    item.Enabled = !flag;
                    if (flag)//选中了第一个控件
                    {
                        item.Checked = false;
                    }
                }
                else
                {
                    controlList[i].Enabled = false;
                }
            }
        }
        public void SetTextEditEnable(Control control, TextEdit textEdit)
        {
            if (control is CheckEdit)//CheckEdit 类型的其他
            {
                CheckEdit check = (CheckEdit)control;
                textEdit.Enabled = check.Checked;
                textEdit.Text = check.Checked ? textEdit.Text : "";
            }
            else if (true)
            {

            }
        }
        /// <summary>
        /// 设置FlowLayoutPanel中控件的  enable属性
        /// </summary>
        /// <param name="flowLayout"></param>
        /// <param name="p"></param>
        private void SetFlowControlEnable(FlowLayoutPanel flowLayout, bool p)
        {
            if (flowLayout == null || flowLayout.Controls.Count == 0) return;
            for (int i = 0; i < flowLayout.Controls.Count; i++)
            {
                Control item = flowLayout.Controls[i];
                item.Enabled = p;
            }
        }
        private void SetRadioGroupTxtEnable(RadioGroup radio, TextEdit txt)
        {
            string index = radio.EditValue == null ? "" : radio.EditValue.ToString();
            txt.Enabled = !string.IsNullOrEmpty(index) && index == "2" ? true : false;
            txt.Text = !string.IsNullOrEmpty(index) && index == "2" ? txt.Text : "";
        }
        //private object GetFlowLayoutResult(FlowLayoutPanel flow)
        //{
        //    if (flow == null || flow.Controls.Count == 0) return "";
        //    string result = "";
        //    for (int i = 0; i < flow.Controls.Count; i++)
        //    {
        //        if (flow.Controls[i].GetType() == typeof(CheckEdit))
        //        {
        //            CheckEdit chk = (CheckEdit)flow.Controls[i];
        //            if (chk == null) continue;
        //            if (chk.Checked)
        //            {
        //                result += chk.Tag + ",";
        //            }
        //        }
        //    }
        //    result = string.IsNullOrEmpty(result) ? result : result.Remove(result.Length - 1);
        //    return result;
        //}
        private int Get完整度(int i)
        {
            int k = Convert.ToInt32((48 - i) * 100 / 48.0);
            return k;
        }
        private void InitView()
        {
            SetControlEnable(this.chk皮肤_正常);
            SetControlEnable(this.chk巩膜_正常);
            SetControlEnable(this.chk淋巴结_未触及);
            SetControlEnable(this.chk罗音_无);
            this.TCG_Tags.SelectedTabPageIndex = 0;
            this.txt心率.Txt1.Properties.Mask.EditMask = @"\d{1,5}";
            this.txt心率.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
        }
        private void DobindingDatasource(tb_健康体检Info tb)
        {
            if (tb != null)
            {

                #region 脏器功能
                this.radio口唇.EditValue = tb.口唇;
                this.txt口唇其他.EditValue = tb.口唇其他;
                string chilie = tb.齿列.ToString();
                if (!string.IsNullOrEmpty(chilie))
                {
                    this.chk齿列_正常.Checked = false;
                    string[] _strings = chilie.Split(',');
                    foreach (string item in _strings)
                    {
                        //10003219	cl_chilei	齿列	1		正常		1
                        //10003220	cl_chilei	齿列	2		缺齿		1
                        //10003221	cl_chilei	齿列	3		龋齿		1
                        //10003222	cl_chilei	齿列	4		义齿(假牙)		1
                        //10003223	cl_chilei	齿列	5	QT,JT	其他		1
                        switch (item)
                        {
                            case "1":
                                this.chk齿列_正常.Checked = true;
                                break;
                            case "2":
                                this.chk齿列_缺齿.Checked = true;
                                string quechi = tb.齿列缺齿.ToString();
                                if (!string.IsNullOrEmpty(quechi))
                                {
                                    string[] _queshis = quechi.Split('@');
                                    this.txt缺齿1.EditValue = _queshis[0];
                                    this.txt缺齿2.EditValue = _queshis[1];
                                    this.txt缺齿3.EditValue = _queshis[2];
                                    this.txt缺齿4.EditValue = _queshis[3];
                                }
                                break;
                            case "3":
                                this.chk齿列_龋齿.Checked = true;
                                string quchi = tb.齿列龋齿.ToString();
                                if (!string.IsNullOrEmpty(quchi))
                                {
                                    string[] _qushis = quchi.Split('@');
                                    this.txt龋齿1.EditValue = _qushis[0];
                                    this.txt龋齿2.EditValue = _qushis[1];
                                    this.txt龋齿3.EditValue = _qushis[2];
                                    this.txt龋齿4.EditValue = _qushis[3];
                                }
                                break;
                            case "4":
                                this.chk齿列_义齿.Checked = true;
                                string yichi = tb.齿列义齿.ToString();
                                if (!string.IsNullOrEmpty(yichi))
                                {
                                    string[] _yishis = yichi.Split('@');
                                    this.txt义齿1.EditValue = _yishis[0];
                                    this.txt义齿2.EditValue = _yishis[1];
                                    this.txt义齿3.EditValue = _yishis[2];
                                    this.txt义齿4.EditValue = _yishis[3];
                                }
                                break;
                            case "5":
                                this.chk齿列_其他.Checked = true;
                                this.txt齿列_其他.Text = tb.齿列其他.ToString();
                                break;
                            default:
                                break;
                        }
                    }
                }
                ControlsHelper.SetFlowLayoutResult(tb.咽部.ToString(), flow咽部);
                this.txt咽部_其他.EditValue = tb.咽部其他;
                this.txt左眼视力.EditValue = tb.左眼视力;
                this.txt右眼视力.EditValue = tb.右眼视力;
                this.txt矫正左眼视力.EditValue = tb.左眼矫正;
                this.txt矫正右眼视力.EditValue = tb.右眼矫正;
                ControlsHelper.SetFlowLayoutResult(tb.听力.ToString(), flow听力);
                ControlsHelper.SetFlowLayoutResult(tb.运动功能.ToString(), flow运动功能);
                #endregion

                #region 查体
                this.radio眼底.EditValue = tb.眼底;
                this.txt眼底.EditValue = tb.眼底异常;
                ControlsHelper.SetFlowLayoutResult(tb.皮肤.ToString(), flow皮肤);
                this.txt皮肤_其他.EditValue = tb.皮肤其他;
                ControlsHelper.SetFlowLayoutResult(tb.巩膜.ToString(), flow巩膜);
                this.txt巩膜_其他.EditValue = tb.巩膜其他;
                ControlsHelper.SetFlowLayoutResult(tb.淋巴结.ToString(), flow淋巴结);
                this.txt淋巴结_其他.EditValue = tb.淋巴结其他;

                this.radio桶状胸.EditValue = tb.桶状胸;
                this.radio呼吸音.EditValue = tb.呼吸音;
                this.txt呼吸音_异常.EditValue = tb.呼吸音异常;
                ControlsHelper.SetFlowLayoutResult(tb.罗音.ToString(), flow罗音);
                this.txt罗音_其他.EditValue = tb.罗音异常;
                this.txt心率.Txt1.EditValue = tb.心率;
                this.radio心律.EditValue = tb.心律;
                this.radio杂音.EditValue = tb.杂音;
                this.txt杂音.EditValue = tb.杂音有;
                this.radio压痛.EditValue = tb.压痛;
                this.txt压痛.EditValue = tb.压痛有;
                this.radio包块.EditValue = tb.包块;
                this.txt包块.EditValue = tb.包块有;
                this.radio肝大.EditValue = tb.肝大;
                this.txt肝大.EditValue = tb.肝大有;
                this.radio脾大.EditValue = tb.脾大;
                this.txt脾大.EditValue = tb.脾大有;
                this.radio移动性浊音.EditValue = tb.浊音;
                this.txt移动性浊音.EditValue = tb.浊音有;

                this.radio下肢水肿.EditValue = tb.下肢水肿;
                this.radio足背动脉搏动.EditValue = tb.足背动脉搏动;
                #endregion

            }
        }
        /// <summary>
        /// 设置控件的可用性
        /// </summary>
        /// <param name="index"></param>
        private void Set齿列ControlEditable(bool index)
        {
            //if (index)//勾选正常
            //{

            //}

            this.chk齿列_缺齿.Checked = index ? !index : index;
            this.chk齿列_缺齿.Enabled = !index;
            this.txt缺齿1.Enabled = this.chk齿列_缺齿.Enabled && this.chk齿列_缺齿.Checked;
            this.txt缺齿2.Enabled = this.chk齿列_缺齿.Enabled && this.chk齿列_缺齿.Checked;
            this.txt缺齿3.Enabled = this.chk齿列_缺齿.Enabled && this.chk齿列_缺齿.Checked;
            this.txt缺齿4.Enabled = this.chk齿列_缺齿.Enabled && this.chk齿列_缺齿.Checked;
            this.txt缺齿1.Text = index ? "" : this.txt缺齿1.Text;
            this.txt缺齿2.Text = index ? "" : this.txt缺齿2.Text;
            this.txt缺齿3.Text = index ? "" : this.txt缺齿3.Text;
            this.txt缺齿4.Text = index ? "" : this.txt缺齿4.Text;

            this.chk齿列_龋齿.Checked = index ? !index : index;
            this.chk齿列_龋齿.Enabled = !index;
            this.txt龋齿1.Enabled = this.chk齿列_龋齿.Enabled && this.chk齿列_龋齿.Checked;
            this.txt龋齿2.Enabled = this.chk齿列_龋齿.Enabled && this.chk齿列_龋齿.Checked;
            this.txt龋齿3.Enabled = this.chk齿列_龋齿.Enabled && this.chk齿列_龋齿.Checked;
            this.txt龋齿4.Enabled = this.chk齿列_龋齿.Enabled && this.chk齿列_龋齿.Checked;
            this.txt龋齿1.Text = index ? "" : this.txt龋齿1.Text;
            this.txt龋齿2.Text = index ? "" : this.txt龋齿2.Text;
            this.txt龋齿3.Text = index ? "" : this.txt龋齿3.Text;
            this.txt龋齿4.Text = index ? "" : this.txt龋齿4.Text;
            this.chk齿列_义齿.Checked = index ? !index : index;
            this.chk齿列_义齿.Enabled = !index;
            this.txt义齿1.Enabled = this.chk齿列_义齿.Enabled && this.chk齿列_义齿.Checked;
            this.txt义齿2.Enabled = this.chk齿列_义齿.Enabled && this.chk齿列_义齿.Checked;
            this.txt义齿3.Enabled = this.chk齿列_义齿.Enabled && this.chk齿列_义齿.Checked;
            this.txt义齿4.Enabled = this.chk齿列_义齿.Enabled && this.chk齿列_义齿.Checked;
            this.txt义齿1.Text = index ? "" : this.txt义齿1.Text;
            this.txt义齿2.Text = index ? "" : this.txt义齿2.Text;
            this.txt义齿3.Text = index ? "" : this.txt义齿3.Text;
            this.txt义齿4.Text = index ? "" : this.txt义齿4.Text;
            this.chk齿列_其他.Checked = index ? !index : index;
            this.chk齿列_其他.Enabled = !index;
            this.txt齿列_其他.Enabled = this.chk齿列_其他.Enabled && this.chk齿列_其他.Checked;
            this.txt齿列_其他.Text = index ? "" : this.txt齿列_其他.Text;
        }
        #endregion

        private void btn下一页_Click(object sender, EventArgs e)
        {
            int index = this.TCG_Tags.SelectedTabPageIndex;
            if (index != TCG_Tags.TabPages.Count) this.TCG_Tags.SelectedTabPageIndex = index + 1;
        }

        private void btn上一页_Click(object sender, EventArgs e)
        {
            int index = this.TCG_Tags.SelectedTabPageIndex;
            if (index != 0) this.TCG_Tags.SelectedTabPageIndex = index - 1;
        }


        //Begin WXF 2018-11-16 | 16:10
        //获取参照数据并赋给控件
        private void sBut_Get参照数据_Click(object sender, EventArgs e)
        {
            sBut_Get参照数据.Enabled = false;

            dal_健康体检_Reference dal健康体检Ref = new dal_健康体检_Reference();
            tb_健康体检_Reference tb健康体检Ref = new tb_健康体检_Reference();

            tb健康体检Ref = dal健康体检Ref.GetOneRow(Program.currentUser.DocNo);

            if (tb健康体检Ref != null)
            {
                switch (TCG_Tags.SelectedTabPageName)
                {
                    case "LCG_齿列":
                        Set_LCG_齿列(tb健康体检Ref);
                        break;
                    default:
                        MessageBox.Show("此页未设置参照数据!");
                        break;
                }
            }
            else
            {
                MessageBox.Show("此人无去年数据做参照!");
            }

            sBut_Get参照数据.Enabled = true;
        }

        /// <summary>
        /// 给控件赋参照值
        /// </summary>
        /// <param name="Reference"></param>
        private void Set_LCG_齿列(tb_健康体检_Reference Reference)
        {
            if (!"1".Equals(Reference.齿列) && !string.IsNullOrEmpty(Reference.齿列))
            {
                chk齿列_正常.Checked = false;
                Set齿列ControlEditable(false);

                string[] strs_齿列 = Reference.齿列.Split(',');

                foreach (string item in strs_齿列)
                {
                    switch (item)
                    {
                        case "2"://缺齿
                            chk齿列_缺齿.Checked = true;
                            chk齿列_缺齿_CheckedChanged(null, null);
                            Set_齿列Value("缺齿", Reference.齿列缺齿);
                            break;
                        case "3"://龋齿
                            chk齿列_龋齿.Checked = true;
                            chk齿列_龋齿_CheckedChanged(null, null);
                            Set_齿列Value("龋齿", Reference.齿列龋齿);
                            break;
                        case "4"://义齿
                            chk齿列_义齿.Checked = true;
                            chk齿列_义齿_CheckedChanged(null, null);
                            Set_齿列Value("义齿", Reference.齿列义齿);
                            break;
                        case "5"://其他
                            chk齿列_其他.Checked = true;
                            chk齿列_其他_CheckedChanged(null, null);
                            txt齿列_其他.Text = Reference.齿列其他;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// 给各种齿列赋值
        /// </summary>
        /// <param name="str_Type"></param>
        /// <param name="str_Value"></param>
        private void Set_齿列Value(string str_Type, string str_Value)
        {
            if (!string.IsNullOrWhiteSpace(str_Value))
            {
                ControlCollection cc = chk齿列_正常.Parent.Controls;

                string[] strs_齿列 = str_Value.Split('@');

                for (int i = 0; i < 4; i++)
                {
                    TextEdit te = (TextEdit)cc.Find("txt" + str_Type + (i + 1), true)[0];
                    te.Text = strs_齿列[i];
                }

            }
        }
        
        //End

        string doctor_脏器功能 = string.Empty;
        string doctor_查体 = string.Empty;

        private void lookUpEdit_医师签字_EditValueChanged(object sender, EventArgs e)
        {
            string PageName = TCG_Tags.SelectedTabPage.Text;

            if (PageName.Equals("口唇、咽部") || PageName.Equals("齿列") || PageName.Equals("视、听"))
            {
                PageName = "脏器功能";
            }
            else if (PageName.Equals("查体") || PageName.Equals("肺") || PageName.Equals("心脏") || PageName.Equals("腹部") || PageName.Equals("下肢水肿") || PageName.Equals("足背动脉搏动"))
            {
                PageName = "查体";
            }
            else
            {
                PageName = string.Empty;
            }

            switch (PageName)
            {
                case "脏器功能":
                    doctor_脏器功能 = lookUpEdit_医师签字.EditValue.ToString();
                    break;
                case "查体":
                    doctor_查体 = lookUpEdit_医师签字.EditValue.ToString();
                    break;
                default:
                    break;
            }
        }

        private void TCG_Tags_SelectedPageChanged(object sender, DevExpress.XtraLayout.LayoutTabPageChangedEventArgs e)
        {
            string PageName = e.Page.Text;

            if (PageName.Equals("口唇、咽部") || PageName.Equals("齿列") || PageName.Equals("视、听"))
            {
                PageName = "脏器功能";
            }
            else if (PageName.Equals("查体") || PageName.Equals("肺") || PageName.Equals("心脏") || PageName.Equals("腹部") || PageName.Equals("下肢水肿") || PageName.Equals("足背动脉搏动"))
            {
                PageName = "查体";
            }
            else
            {
                PageName = string.Empty;
            }

            switch (PageName)
            {
                case "脏器功能":
                    //layoutControlItem_医师签字.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lookUpEdit_医师签字.EditValue = string.IsNullOrEmpty(doctor_脏器功能) ? "请选择" : doctor_脏器功能;
                    break;
                case "查体":
                    //layoutControlItem_医师签字.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lookUpEdit_医师签字.EditValue = string.IsNullOrEmpty(doctor_查体) ? "请选择" : doctor_查体;
                    break;
                default:
                    //layoutControlItem_医师签字.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    break;
            }
        }

    }
}
