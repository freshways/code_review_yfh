﻿using AtomEHR.Library.UserControls;
namespace ATOMEHR_LeaveClient
{
    partial class uc健康体检表2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc健康体检表2));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.sBut_Cut其他用药参照 = new DevExpress.XtraEditors.SimpleButton();
            this.sBut_Get其他用药参照 = new DevExpress.XtraEditors.SimpleButton();
            this.gc其他用药参照 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col药物名称Ref = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col用法Ref = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col用量Ref = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col创建时间Ref = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col用药类型Ref = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lookUpEdit_医师签字 = new DevExpress.XtraEditors.LookUpEdit();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.sBut_Get参照数据 = new DevExpress.XtraEditors.SimpleButton();
            this.btn下一项 = new DevExpress.XtraEditors.SimpleButton();
            this.btn上一项 = new DevExpress.XtraEditors.SimpleButton();
            this.gc接种史 = new DevExpress.XtraGrid.GridControl();
            this.gv接种史 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col接种名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col接种日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dte接种日期 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.col接种机构 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col个人档案编号4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col创建日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn预防接种史删除 = new DevExpress.XtraEditors.SimpleButton();
            this.btn预防接种史添加 = new DevExpress.XtraEditors.SimpleButton();
            this.gc用药情况 = new DevExpress.XtraGrid.GridControl();
            this.gv用药情况 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col药物名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cbo用药 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.col用法 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cbo用法 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.col用量 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cbo用量 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.col用药时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col服药依从性 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lkp服药依从性 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.col个人档案编号3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col创建时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dte用药时间 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.btn添加用药情况 = new DevExpress.XtraEditors.SimpleButton();
            this.btn删除用药情况 = new DevExpress.XtraEditors.SimpleButton();
            this.radio预防接种史 = new DevExpress.XtraEditors.RadioGroup();
            this.btn病床史删除 = new DevExpress.XtraEditors.SimpleButton();
            this.btn病床史添加 = new DevExpress.XtraEditors.SimpleButton();
            this.gc建床史 = new DevExpress.XtraGrid.GridControl();
            this.gv建床史 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col建床日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dte建床日期 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.col撤床日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dte撤床日期 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.col原因2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col医疗机构名称2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col病案号2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col个人档案编号2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col类型2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.radio家庭病床史 = new DevExpress.XtraEditors.RadioGroup();
            this.btn住院史删除 = new DevExpress.XtraEditors.SimpleButton();
            this.radio主要用药情况 = new DevExpress.XtraEditors.RadioGroup();
            this.btn住院史添加 = new DevExpress.XtraEditors.SimpleButton();
            this.gc住院史 = new DevExpress.XtraGrid.GridControl();
            this.gv住院史 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col入院日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dte入院日期 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.col出院日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dte出院日期 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.col原因 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col医疗机构名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col病案号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col个人档案编号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col类型 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.radio住院史 = new DevExpress.XtraEditors.RadioGroup();
            this.flow脑血管疾病 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk脑血管疾病_未发现 = new DevExpress.XtraEditors.CheckEdit();
            this.chk脑血管疾病_缺血性卒中 = new DevExpress.XtraEditors.CheckEdit();
            this.chk脑血管疾病_脑出血 = new DevExpress.XtraEditors.CheckEdit();
            this.chk脑血管疾病_蛛网膜下腔出血 = new DevExpress.XtraEditors.CheckEdit();
            this.chk脑血管疾病_短暂性脑缺血发作 = new DevExpress.XtraEditors.CheckEdit();
            this.chk脑血管疾病_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt脑血管疾病_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flow其他系统疾病 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk其他系统疾病_未发现 = new DevExpress.XtraEditors.CheckEdit();
            this.chk其他系统疾病_糖尿病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk其他系统疾病_慢性支气管炎 = new DevExpress.XtraEditors.CheckEdit();
            this.chk其他系统疾病_慢性阻塞性肺气肿 = new DevExpress.XtraEditors.CheckEdit();
            this.chk其他系统疾病_恶性肿瘤 = new DevExpress.XtraEditors.CheckEdit();
            this.chk其他系统疾病_老年性骨关节病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk其他系统疾病_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt其他系统疾病_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flow神经系统疾病 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk神经系统疾病_未发现 = new DevExpress.XtraEditors.CheckEdit();
            this.chk神经系统疾病_阿尔茨海默症 = new DevExpress.XtraEditors.CheckEdit();
            this.chk神经系统疾病_帕金森症 = new DevExpress.XtraEditors.CheckEdit();
            this.chk神经系统疾病_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt神经系统疾病_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flow眼部疾病 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk眼部疾病_未发现 = new DevExpress.XtraEditors.CheckEdit();
            this.chk眼部疾病_视网膜出血或渗出 = new DevExpress.XtraEditors.CheckEdit();
            this.chk眼部疾病_视乳头水肿 = new DevExpress.XtraEditors.CheckEdit();
            this.chk眼部疾病_白内障 = new DevExpress.XtraEditors.CheckEdit();
            this.chk眼部疾病_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt眼部疾病_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flow心脏疾病 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk心脏疾病_未发现 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心脏疾病_心肌梗死 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心脏疾病_心绞痛 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心脏疾病_冠状动脉血运重建 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心脏疾病_充血性心力衰竭 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心脏疾病_心前区疼痛 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心脏疾病_高血压 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心脏疾病_夹层动脉瘤 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心脏疾病_动脉闭塞性疾病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心脏疾病_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.chktxt心脏疾病_冠心病 = new DevExpress.XtraEditors.CheckEdit();
            this.txt心脏疾病_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flow肾脏疾病 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk肾脏疾病_未发现 = new DevExpress.XtraEditors.CheckEdit();
            this.chk肾脏疾病_糖尿病肾病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk肾脏疾病_肾功能衰竭 = new DevExpress.XtraEditors.CheckEdit();
            this.chk肾脏疾病_急性肾炎 = new DevExpress.XtraEditors.CheckEdit();
            this.chk肾脏疾病_慢性肾炎 = new DevExpress.XtraEditors.CheckEdit();
            this.chk肾脏疾病_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt肾脏疾病_其他 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.TCG_Tags = new DevExpress.XtraLayout.TabbedControlGroup();
            this.LCG_现存主要健康问题 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl脑血管疾病 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl肾脏疾病 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl心脏疾病 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl眼部疾病 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl其他系统疾病 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl神经系统疾病 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LCG_住院治疗情况 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lblgc住院史 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl住院史 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem157 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem159 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lblgc家庭病床史 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem156 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem160 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem161 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem27 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem28 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LCG_主要用药情况 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem116 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem117 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem29 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lblgc用药情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lay其他用药参照 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LCG_接种史 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem118 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem162 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem163 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem30 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lblgc接种史 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_医师签字 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc其他用药参照)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_医师签字.Properties)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc接种史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv接种史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte接种日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte接种日期.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc用药情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv用药情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo用药)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo用法)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo用量)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp服药依从性)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte用药时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte用药时间.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio预防接种史.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc建床史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv建床史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte建床日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte建床日期.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte撤床日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte撤床日期.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio家庭病床史.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio主要用药情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc住院史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv住院史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte入院日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte入院日期.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出院日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出院日期.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio住院史.Properties)).BeginInit();
            this.flow脑血管疾病.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_未发现.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_缺血性卒中.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_脑出血.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_蛛网膜下腔出血.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_短暂性脑缺血发作.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脑血管疾病_其他.Properties)).BeginInit();
            this.flow其他系统疾病.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_未发现.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_糖尿病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_慢性支气管炎.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_慢性阻塞性肺气肿.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_恶性肿瘤.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_老年性骨关节病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt其他系统疾病_其他.Properties)).BeginInit();
            this.flow神经系统疾病.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk神经系统疾病_未发现.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk神经系统疾病_阿尔茨海默症.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk神经系统疾病_帕金森症.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk神经系统疾病_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt神经系统疾病_其他.Properties)).BeginInit();
            this.flow眼部疾病.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk眼部疾病_未发现.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk眼部疾病_视网膜出血或渗出.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk眼部疾病_视乳头水肿.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk眼部疾病_白内障.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk眼部疾病_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt眼部疾病_其他.Properties)).BeginInit();
            this.flow心脏疾病.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk心脏疾病_未发现.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心脏疾病_心肌梗死.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心脏疾病_心绞痛.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心脏疾病_冠状动脉血运重建.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心脏疾病_充血性心力衰竭.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心脏疾病_心前区疼痛.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心脏疾病_高血压.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心脏疾病_夹层动脉瘤.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心脏疾病_动脉闭塞性疾病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心脏疾病_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chktxt心脏疾病_冠心病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心脏疾病_其他.Properties)).BeginInit();
            this.flow肾脏疾病.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_未发现.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_糖尿病肾病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_肾功能衰竭.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_急性肾炎.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_慢性肾炎.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt肾脏疾病_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TCG_Tags)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_现存主要健康问题)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl脑血管疾病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl肾脏疾病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl心脏疾病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl眼部疾病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl其他系统疾病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl神经系统疾病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_住院治疗情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblgc住院史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl住院史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem157)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem159)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblgc家庭病床史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem156)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem160)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem161)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_主要用药情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem116)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem117)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblgc用药情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lay其他用药参照)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_接种史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem118)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem162)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem163)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblgc接种史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_医师签字)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.sBut_Cut其他用药参照);
            this.layoutControl1.Controls.Add(this.sBut_Get其他用药参照);
            this.layoutControl1.Controls.Add(this.gc其他用药参照);
            this.layoutControl1.Controls.Add(this.lookUpEdit_医师签字);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel1);
            this.layoutControl1.Controls.Add(this.btn下一项);
            this.layoutControl1.Controls.Add(this.btn上一项);
            this.layoutControl1.Controls.Add(this.gc接种史);
            this.layoutControl1.Controls.Add(this.btn预防接种史删除);
            this.layoutControl1.Controls.Add(this.btn预防接种史添加);
            this.layoutControl1.Controls.Add(this.gc用药情况);
            this.layoutControl1.Controls.Add(this.btn添加用药情况);
            this.layoutControl1.Controls.Add(this.btn删除用药情况);
            this.layoutControl1.Controls.Add(this.radio预防接种史);
            this.layoutControl1.Controls.Add(this.btn病床史删除);
            this.layoutControl1.Controls.Add(this.btn病床史添加);
            this.layoutControl1.Controls.Add(this.gc建床史);
            this.layoutControl1.Controls.Add(this.radio家庭病床史);
            this.layoutControl1.Controls.Add(this.btn住院史删除);
            this.layoutControl1.Controls.Add(this.radio主要用药情况);
            this.layoutControl1.Controls.Add(this.btn住院史添加);
            this.layoutControl1.Controls.Add(this.gc住院史);
            this.layoutControl1.Controls.Add(this.radio住院史);
            this.layoutControl1.Controls.Add(this.flow脑血管疾病);
            this.layoutControl1.Controls.Add(this.flow其他系统疾病);
            this.layoutControl1.Controls.Add(this.flow神经系统疾病);
            this.layoutControl1.Controls.Add(this.flow眼部疾病);
            this.layoutControl1.Controls.Add(this.flow心脏疾病);
            this.layoutControl1.Controls.Add(this.flow肾脏疾病);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(566, 240, 250, 350);
            this.layoutControl1.OptionsFocus.EnableAutoTabOrder = false;
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(718, 423);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // sBut_Cut其他用药参照
            // 
            this.sBut_Cut其他用药参照.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.sBut_Cut其他用药参照.Appearance.Options.UseForeColor = true;
            this.sBut_Cut其他用药参照.Enabled = false;
            this.sBut_Cut其他用药参照.Image = ((System.Drawing.Image)(resources.GetObject("sBut_Cut其他用药参照.Image")));
            this.sBut_Cut其他用药参照.Location = new System.Drawing.Point(524, 75);
            this.sBut_Cut其他用药参照.Name = "sBut_Cut其他用药参照";
            this.sBut_Cut其他用药参照.Size = new System.Drawing.Size(131, 36);
            this.sBut_Cut其他用药参照.StyleController = this.layoutControl1;
            this.sBut_Cut其他用药参照.TabIndex = 162;
            this.sBut_Cut其他用药参照.Text = "收起其他用药参照";
            this.sBut_Cut其他用药参照.Click += new System.EventHandler(this.sBut_Cut其他用药参照_Click);
            // 
            // sBut_Get其他用药参照
            // 
            this.sBut_Get其他用药参照.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.sBut_Get其他用药参照.Appearance.Options.UseForeColor = true;
            this.sBut_Get其他用药参照.Image = ((System.Drawing.Image)(resources.GetObject("sBut_Get其他用药参照.Image")));
            this.sBut_Get其他用药参照.Location = new System.Drawing.Point(389, 75);
            this.sBut_Get其他用药参照.Name = "sBut_Get其他用药参照";
            this.sBut_Get其他用药参照.Size = new System.Drawing.Size(131, 36);
            this.sBut_Get其他用药参照.StyleController = this.layoutControl1;
            this.sBut_Get其他用药参照.TabIndex = 161;
            this.sBut_Get其他用药参照.Text = "获取其他用药参照";
            this.sBut_Get其他用药参照.Click += new System.EventHandler(this.sBut_Get其他用药参照_Click);
            // 
            // gc其他用药参照
            // 
            this.gc其他用药参照.Location = new System.Drawing.Point(63, 249);
            this.gc其他用药参照.MainView = this.gridView1;
            this.gc其他用药参照.Name = "gc其他用药参照";
            this.gc其他用药参照.Size = new System.Drawing.Size(647, 130);
            this.gc其他用药参照.TabIndex = 160;
            this.gc其他用药参照.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col药物名称Ref,
            this.col用法Ref,
            this.col用量Ref,
            this.col创建时间Ref,
            this.col用药类型Ref});
            this.gridView1.GridControl = this.gc其他用药参照;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // col药物名称Ref
            // 
            this.col药物名称Ref.Caption = "药物名称";
            this.col药物名称Ref.FieldName = "药物名称";
            this.col药物名称Ref.Name = "col药物名称Ref";
            this.col药物名称Ref.Visible = true;
            this.col药物名称Ref.VisibleIndex = 0;
            // 
            // col用法Ref
            // 
            this.col用法Ref.Caption = "用法";
            this.col用法Ref.FieldName = "用法";
            this.col用法Ref.Name = "col用法Ref";
            this.col用法Ref.Visible = true;
            this.col用法Ref.VisibleIndex = 1;
            // 
            // col用量Ref
            // 
            this.col用量Ref.Caption = "用量";
            this.col用量Ref.FieldName = "用量";
            this.col用量Ref.Name = "col用量Ref";
            this.col用量Ref.Visible = true;
            this.col用量Ref.VisibleIndex = 2;
            // 
            // col创建时间Ref
            // 
            this.col创建时间Ref.Caption = "创建时间";
            this.col创建时间Ref.FieldName = "创建时间";
            this.col创建时间Ref.Name = "col创建时间Ref";
            this.col创建时间Ref.Visible = true;
            this.col创建时间Ref.VisibleIndex = 3;
            // 
            // col用药类型Ref
            // 
            this.col用药类型Ref.Caption = "用药类型";
            this.col用药类型Ref.FieldName = "数据来源";
            this.col用药类型Ref.Name = "col用药类型Ref";
            this.col用药类型Ref.Visible = true;
            this.col用药类型Ref.VisibleIndex = 4;
            // 
            // lookUpEdit_医师签字
            // 
            this.lookUpEdit_医师签字.Location = new System.Drawing.Point(96, 5);
            this.lookUpEdit_医师签字.Name = "lookUpEdit_医师签字";
            this.lookUpEdit_医师签字.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit_医师签字.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit_医师签字.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit_医师签字.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit_医师签字.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_医师签字.Properties.NullText = "";
            this.lookUpEdit_医师签字.Size = new System.Drawing.Size(209, 40);
            this.lookUpEdit_医师签字.StyleController = this.layoutControl1;
            this.lookUpEdit_医师签字.TabIndex = 159;
            this.lookUpEdit_医师签字.EditValueChanged += new System.EventHandler(this.lookUpEdit_医师签字_EditValueChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn保存);
            this.flowLayoutPanel1.Controls.Add(this.sBut_Get参照数据);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(309, 5);
            this.flowLayoutPanel1.MaximumSize = new System.Drawing.Size(0, 36);
            this.flowLayoutPanel1.MinimumSize = new System.Drawing.Size(0, 36);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(404, 36);
            this.flowLayoutPanel1.TabIndex = 158;
            // 
            // btn保存
            // 
            this.btn保存.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn保存.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn保存.Appearance.Options.UseFont = true;
            this.btn保存.Appearance.Options.UseForeColor = true;
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(275, 0);
            this.btn保存.Margin = new System.Windows.Forms.Padding(0);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(129, 37);
            this.btn保存.TabIndex = 3;
            this.btn保存.Text = "保 存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // sBut_Get参照数据
            // 
            this.sBut_Get参照数据.Appearance.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.sBut_Get参照数据.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.sBut_Get参照数据.Appearance.Options.UseFont = true;
            this.sBut_Get参照数据.Appearance.Options.UseForeColor = true;
            this.sBut_Get参照数据.Image = ((System.Drawing.Image)(resources.GetObject("sBut_Get参照数据.Image")));
            this.sBut_Get参照数据.Location = new System.Drawing.Point(90, 2);
            this.sBut_Get参照数据.Margin = new System.Windows.Forms.Padding(2);
            this.sBut_Get参照数据.Name = "sBut_Get参照数据";
            this.sBut_Get参照数据.Size = new System.Drawing.Size(183, 34);
            this.sBut_Get参照数据.TabIndex = 5;
            this.sBut_Get参照数据.Text = "获取去年数据参照";
            this.sBut_Get参照数据.Click += new System.EventHandler(this.sBut_Get参照数据_Click);
            // 
            // btn下一项
            // 
            this.btn下一项.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn下一项.Appearance.Options.UseFont = true;
            this.btn下一项.Image = ((System.Drawing.Image)(resources.GetObject("btn下一项.Image")));
            this.btn下一项.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btn下一项.Location = new System.Drawing.Point(373, 386);
            this.btn下一项.Name = "btn下一项";
            this.btn下一项.Size = new System.Drawing.Size(340, 32);
            this.btn下一项.StyleController = this.layoutControl1;
            this.btn下一项.TabIndex = 157;
            this.btn下一项.Text = "下一项";
            this.btn下一项.Click += new System.EventHandler(this.btn下一页_Click);
            // 
            // btn上一项
            // 
            this.btn上一项.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn上一项.Appearance.Options.UseFont = true;
            this.btn上一项.Image = ((System.Drawing.Image)(resources.GetObject("btn上一项.Image")));
            this.btn上一项.Location = new System.Drawing.Point(5, 386);
            this.btn上一项.Name = "btn上一项";
            this.btn上一项.Size = new System.Drawing.Size(364, 32);
            this.btn上一项.StyleController = this.layoutControl1;
            this.btn上一项.TabIndex = 156;
            this.btn上一项.Text = "上一项";
            this.btn上一项.Click += new System.EventHandler(this.btn上一页_Click);
            // 
            // gc接种史
            // 
            this.gc接种史.Location = new System.Drawing.Point(8, 115);
            this.gc接种史.MainView = this.gv接种史;
            this.gc接种史.Name = "gc接种史";
            this.gc接种史.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.dte接种日期});
            this.gc接种史.Size = new System.Drawing.Size(696, 264);
            this.gc接种史.TabIndex = 138;
            this.gc接种史.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv接种史});
            // 
            // gv接种史
            // 
            this.gv接种史.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gv接种史.Appearance.HeaderPanel.Options.UseFont = true;
            this.gv接种史.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gv接种史.Appearance.Row.Options.UseFont = true;
            this.gv接种史.ColumnPanelRowHeight = 40;
            this.gv接种史.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col接种名称,
            this.col接种日期,
            this.col接种机构,
            this.col个人档案编号4,
            this.col创建日期});
            this.gv接种史.GridControl = this.gc接种史;
            this.gv接种史.Name = "gv接种史";
            this.gv接种史.OptionsView.ColumnAutoWidth = false;
            this.gv接种史.OptionsView.ShowGroupPanel = false;
            this.gv接种史.RowHeight = 35;
            // 
            // col接种名称
            // 
            this.col接种名称.AppearanceHeader.Options.UseTextOptions = true;
            this.col接种名称.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col接种名称.Caption = "接种名称";
            this.col接种名称.FieldName = "接种名称";
            this.col接种名称.Name = "col接种名称";
            this.col接种名称.Visible = true;
            this.col接种名称.VisibleIndex = 0;
            this.col接种名称.Width = 148;
            // 
            // col接种日期
            // 
            this.col接种日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col接种日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col接种日期.Caption = "接种日期";
            this.col接种日期.ColumnEdit = this.dte接种日期;
            this.col接种日期.FieldName = "接种日期";
            this.col接种日期.Name = "col接种日期";
            this.col接种日期.Visible = true;
            this.col接种日期.VisibleIndex = 1;
            this.col接种日期.Width = 164;
            // 
            // dte接种日期
            // 
            this.dte接种日期.AutoHeight = false;
            this.dte接种日期.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte接种日期.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte接种日期.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dte接种日期.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dte接种日期.EditFormat.FormatString = "yyyy-MM-dd";
            this.dte接种日期.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dte接种日期.Mask.EditMask = "yyyy-MM-dd";
            this.dte接种日期.Mask.UseMaskAsDisplayFormat = true;
            this.dte接种日期.Name = "dte接种日期";
            // 
            // col接种机构
            // 
            this.col接种机构.AppearanceHeader.Options.UseTextOptions = true;
            this.col接种机构.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col接种机构.Caption = "接种机构";
            this.col接种机构.FieldName = "接种机构";
            this.col接种机构.Name = "col接种机构";
            this.col接种机构.Visible = true;
            this.col接种机构.VisibleIndex = 2;
            this.col接种机构.Width = 176;
            // 
            // col个人档案编号4
            // 
            this.col个人档案编号4.Caption = "个人档案编号";
            this.col个人档案编号4.FieldName = "创建日期";
            this.col个人档案编号4.Name = "col个人档案编号4";
            // 
            // col创建日期
            // 
            this.col创建日期.Caption = "gridColumn2";
            this.col创建日期.FieldName = "创建日期";
            this.col创建日期.Name = "col创建日期";
            // 
            // btn预防接种史删除
            // 
            this.btn预防接种史删除.Enabled = false;
            this.btn预防接种史删除.Image = ((System.Drawing.Image)(resources.GetObject("btn预防接种史删除.Image")));
            this.btn预防接种史删除.Location = new System.Drawing.Point(256, 75);
            this.btn预防接种史删除.Name = "btn预防接种史删除";
            this.btn预防接种史删除.Size = new System.Drawing.Size(113, 36);
            this.btn预防接种史删除.StyleController = this.layoutControl1;
            this.btn预防接种史删除.TabIndex = 13;
            this.btn预防接种史删除.Text = "删除当前行";
            this.btn预防接种史删除.Click += new System.EventHandler(this.btn预防接种史删除_Click);
            // 
            // btn预防接种史添加
            // 
            this.btn预防接种史添加.Enabled = false;
            this.btn预防接种史添加.Image = ((System.Drawing.Image)(resources.GetObject("btn预防接种史添加.Image")));
            this.btn预防接种史添加.Location = new System.Drawing.Point(151, 75);
            this.btn预防接种史添加.Name = "btn预防接种史添加";
            this.btn预防接种史添加.Size = new System.Drawing.Size(101, 36);
            this.btn预防接种史添加.StyleController = this.layoutControl1;
            this.btn预防接种史添加.TabIndex = 12;
            this.btn预防接种史添加.Text = "添加";
            this.btn预防接种史添加.Click += new System.EventHandler(this.btn预防接种史添加_Click);
            // 
            // gc用药情况
            // 
            this.gc用药情况.Location = new System.Drawing.Point(8, 115);
            this.gc用药情况.MainView = this.gv用药情况;
            this.gc用药情况.Name = "gc用药情况";
            this.gc用药情况.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.dte用药时间,
            this.lkp服药依从性,
            this.cbo用药,
            this.cbo用法,
            this.cbo用量});
            this.gc用药情况.Size = new System.Drawing.Size(702, 130);
            this.gc用药情况.TabIndex = 137;
            this.gc用药情况.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv用药情况});
            // 
            // gv用药情况
            // 
            this.gv用药情况.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gv用药情况.Appearance.HeaderPanel.Options.UseFont = true;
            this.gv用药情况.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gv用药情况.Appearance.Row.Options.UseFont = true;
            this.gv用药情况.ColumnPanelRowHeight = 40;
            this.gv用药情况.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col药物名称,
            this.col用法,
            this.col用量,
            this.col用药时间,
            this.col服药依从性,
            this.col个人档案编号3,
            this.col创建时间});
            this.gv用药情况.GridControl = this.gc用药情况;
            this.gv用药情况.Name = "gv用药情况";
            this.gv用药情况.OptionsView.ColumnAutoWidth = false;
            this.gv用药情况.OptionsView.ShowGroupPanel = false;
            this.gv用药情况.RowHeight = 35;
            // 
            // col药物名称
            // 
            this.col药物名称.AppearanceHeader.Options.UseTextOptions = true;
            this.col药物名称.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col药物名称.Caption = "药物名称";
            this.col药物名称.ColumnEdit = this.cbo用药;
            this.col药物名称.FieldName = "药物名称";
            this.col药物名称.Name = "col药物名称";
            this.col药物名称.Visible = true;
            this.col药物名称.VisibleIndex = 0;
            this.col药物名称.Width = 114;
            // 
            // cbo用药
            // 
            this.cbo用药.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cbo用药.Appearance.Options.UseFont = true;
            this.cbo用药.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 15F);
            this.cbo用药.AppearanceDropDown.Options.UseFont = true;
            this.cbo用药.AutoHeight = false;
            this.cbo用药.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo用药.Items.AddRange(new object[] {
            "阿司匹林",
            "阿司匹林肠溶片",
            "阿替洛尔片",
            "阿托伐他汀钙胶囊",
            "氨笨喋啶缓释片",
            "氨氯地平片",
            "北京降压0号",
            "倍他洛克",
            "苯磺酸氨氯地平片",
            "笨磺酸左旋氨氯地平片",
            "丹参片",
            "单硝酸异山梨酯片",
            "地奥心血康胶囊",
            "厄贝沙坦胶囊",
            "厄贝沙坦片",
            "二甲双胍",
            "氟桂利嗪",
            "复方阿司匹林",
            "复方氨笨喋啶",
            "复方丹参片",
            "复方利血平安笨蝶啶片",
            "复方罗布麻",
            "复方血栓通胶囊",
            "格列吡嗪",
            "酒石酸美托洛尔片",
            "卡马西平片",
            "卡托普利片",
            "坎地沙坦酯胶囊",
            "坎地沙坦酯片",
            "拉贝洛尔",
            "拉西地平片",
            "硫酸氢氯吡格雷片",
            "马来酸依那普利片",
            "美托洛尔",
            "脑立清片",
            "尼莫地平片",
            "牛黄降压胶囊",
            "牛黄降压丸",
            "吲达帕胺片",
            "普罗布考片",
            "普萘洛尔片",
            "氢氯噻嗪",
            "曲克芦丁片",
            "三精司乐平",
            "三嗪芦丁",
            "替米沙坦",
            "通心络胶囊",
            "维脑路通",
            "消渴丸",
            "硝苯地平缓释胶囊",
            "硝苯地平缓释片",
            "硝苯地平片",
            "缬沙坦胶囊",
            "心可舒片",
            "辛伐他汀片",
            "血塞通胶囊",
            "血塞通片",
            "盐酸氟桂利嗪胶囊",
            "盐酸拉贝洛尔",
            "依那普利片",
            "藻酸双酯",
            "珍菊降压片",
            "阿卡波糖片",
            "阿司匹林",
            "参芪降糖",
            "肠溶二甲双胍",
            "迪沙",
            "迪沙片",
            "地骨降糖胶囊",
            "二甲双胍",
            "格咧吡嗪片",
            "格列美脲片",
            "格列齐特片",
            "格列齐特缓释片",
            "甲钴安",
            "降糖颗粒",
            "坎地沙坦",
            "诺和龙",
            "瑞格列奈",
            "消渴丸",
            "硝苯地平",
            "辛伐他汀",
            "盐酸二甲双胍片",
            "盐酸吡格列酮片",
            "胰岛素",
            "胰激肽原酶",
            "优降糖",
            "振源胶囊"});
            this.cbo用药.Name = "cbo用药";
            // 
            // col用法
            // 
            this.col用法.AppearanceHeader.Options.UseTextOptions = true;
            this.col用法.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col用法.Caption = "用法";
            this.col用法.ColumnEdit = this.cbo用法;
            this.col用法.FieldName = "用法";
            this.col用法.Name = "col用法";
            this.col用法.Visible = true;
            this.col用法.VisibleIndex = 1;
            this.col用法.Width = 119;
            // 
            // cbo用法
            // 
            this.cbo用法.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 15F);
            this.cbo用法.AppearanceDropDown.Options.UseFont = true;
            this.cbo用法.AutoHeight = false;
            this.cbo用法.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo用法.Items.AddRange(new object[] {
            "口服",
            "皮下注射",
            "肌肉注射",
            "静脉注射"});
            this.cbo用法.Name = "cbo用法";
            // 
            // col用量
            // 
            this.col用量.AppearanceHeader.Options.UseTextOptions = true;
            this.col用量.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col用量.Caption = "用量";
            this.col用量.ColumnEdit = this.cbo用量;
            this.col用量.FieldName = "用量";
            this.col用量.Name = "col用量";
            this.col用量.Visible = true;
            this.col用量.VisibleIndex = 2;
            this.col用量.Width = 100;
            // 
            // cbo用量
            // 
            this.cbo用量.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 15F);
            this.cbo用量.AppearanceDropDown.Options.UseFont = true;
            this.cbo用量.AutoHeight = false;
            this.cbo用量.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo用量.Items.AddRange(new object[] {
            "每日 次，每次 mg",
            "每日 次，每次 片",
            "每周 次，每次 mg",
            "每周 次，每次 片"});
            this.cbo用量.Name = "cbo用量";
            // 
            // col用药时间
            // 
            this.col用药时间.AppearanceHeader.Options.UseTextOptions = true;
            this.col用药时间.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col用药时间.Caption = "用药时间";
            this.col用药时间.FieldName = "用药时间";
            this.col用药时间.Name = "col用药时间";
            this.col用药时间.Visible = true;
            this.col用药时间.VisibleIndex = 3;
            this.col用药时间.Width = 90;
            // 
            // col服药依从性
            // 
            this.col服药依从性.AppearanceHeader.Options.UseTextOptions = true;
            this.col服药依从性.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col服药依从性.Caption = "服药依从性";
            this.col服药依从性.ColumnEdit = this.lkp服药依从性;
            this.col服药依从性.FieldName = "服药依从性";
            this.col服药依从性.Name = "col服药依从性";
            this.col服药依从性.Visible = true;
            this.col服药依从性.VisibleIndex = 4;
            this.col服药依从性.Width = 90;
            // 
            // lkp服药依从性
            // 
            this.lkp服药依从性.AutoHeight = false;
            this.lkp服药依从性.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkp服药依从性.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "服药依从性")});
            this.lkp服药依从性.Name = "lkp服药依从性";
            this.lkp服药依从性.NullText = "";
            // 
            // col个人档案编号3
            // 
            this.col个人档案编号3.Caption = "个人档案编号";
            this.col个人档案编号3.FieldName = "个人档案编号";
            this.col个人档案编号3.Name = "col个人档案编号3";
            // 
            // col创建时间
            // 
            this.col创建时间.Caption = "创建时间";
            this.col创建时间.FieldName = "创建时间";
            this.col创建时间.Name = "col创建时间";
            // 
            // dte用药时间
            // 
            this.dte用药时间.AutoHeight = false;
            this.dte用药时间.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte用药时间.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte用药时间.Name = "dte用药时间";
            // 
            // btn添加用药情况
            // 
            this.btn添加用药情况.Enabled = false;
            this.btn添加用药情况.Image = ((System.Drawing.Image)(resources.GetObject("btn添加用药情况.Image")));
            this.btn添加用药情况.Location = new System.Drawing.Point(169, 75);
            this.btn添加用药情况.Name = "btn添加用药情况";
            this.btn添加用药情况.Size = new System.Drawing.Size(106, 36);
            this.btn添加用药情况.StyleController = this.layoutControl1;
            this.btn添加用药情况.TabIndex = 12;
            this.btn添加用药情况.Text = "添加";
            this.btn添加用药情况.Click += new System.EventHandler(this.btn添加用药情况_Click);
            // 
            // btn删除用药情况
            // 
            this.btn删除用药情况.Enabled = false;
            this.btn删除用药情况.Image = ((System.Drawing.Image)(resources.GetObject("btn删除用药情况.Image")));
            this.btn删除用药情况.Location = new System.Drawing.Point(279, 75);
            this.btn删除用药情况.Name = "btn删除用药情况";
            this.btn删除用药情况.Size = new System.Drawing.Size(106, 36);
            this.btn删除用药情况.StyleController = this.layoutControl1;
            this.btn删除用药情况.TabIndex = 13;
            this.btn删除用药情况.Text = "删除当前行";
            this.btn删除用药情况.Click += new System.EventHandler(this.btn删除用药情况_Click);
            // 
            // radio预防接种史
            // 
            this.radio预防接种史.EditValue = "2";
            this.radio预防接种史.Location = new System.Drawing.Point(8, 75);
            this.radio预防接种史.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio预防接种史.Name = "radio预防接种史";
            this.radio预防接种史.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.radio预防接种史.Properties.Appearance.Options.UseFont = true;
            this.radio预防接种史.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "有"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "无")});
            this.radio预防接种史.Size = new System.Drawing.Size(139, 36);
            this.radio预防接种史.StyleController = this.layoutControl1;
            this.radio预防接种史.TabIndex = 155;
            this.radio预防接种史.SelectedIndexChanged += new System.EventHandler(this.radio预防接种史_SelectedIndexChanged);
            // 
            // btn病床史删除
            // 
            this.btn病床史删除.Enabled = false;
            this.btn病床史删除.Image = ((System.Drawing.Image)(resources.GetObject("btn病床史删除.Image")));
            this.btn病床史删除.Location = new System.Drawing.Point(368, 217);
            this.btn病床史删除.Name = "btn病床史删除";
            this.btn病床史删除.Size = new System.Drawing.Size(131, 36);
            this.btn病床史删除.StyleController = this.layoutControl1;
            this.btn病床史删除.TabIndex = 13;
            this.btn病床史删除.Text = "删除当前行";
            this.btn病床史删除.Click += new System.EventHandler(this.btn病床史删除_Click);
            // 
            // btn病床史添加
            // 
            this.btn病床史添加.Enabled = false;
            this.btn病床史添加.Image = ((System.Drawing.Image)(resources.GetObject("btn病床史添加.Image")));
            this.btn病床史添加.Location = new System.Drawing.Point(266, 217);
            this.btn病床史添加.Name = "btn病床史添加";
            this.btn病床史添加.Size = new System.Drawing.Size(98, 36);
            this.btn病床史添加.StyleController = this.layoutControl1;
            this.btn病床史添加.TabIndex = 12;
            this.btn病床史添加.Text = "添加";
            this.btn病床史添加.Click += new System.EventHandler(this.btn病床史添加_Click);
            // 
            // gc建床史
            // 
            this.gc建床史.Location = new System.Drawing.Point(8, 257);
            this.gc建床史.MainView = this.gv建床史;
            this.gc建床史.Name = "gc建床史";
            this.gc建床史.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.dte撤床日期,
            this.dte建床日期});
            this.gc建床史.Size = new System.Drawing.Size(702, 122);
            this.gc建床史.TabIndex = 136;
            this.gc建床史.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv建床史});
            // 
            // gv建床史
            // 
            this.gv建床史.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col建床日期,
            this.col撤床日期,
            this.col原因2,
            this.col医疗机构名称2,
            this.col病案号2,
            this.col个人档案编号2,
            this.col类型2});
            this.gv建床史.GridControl = this.gc建床史;
            this.gv建床史.Name = "gv建床史";
            this.gv建床史.OptionsView.ColumnAutoWidth = false;
            this.gv建床史.OptionsView.ShowGroupPanel = false;
            // 
            // col建床日期
            // 
            this.col建床日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col建床日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col建床日期.Caption = "建床日期";
            this.col建床日期.ColumnEdit = this.dte建床日期;
            this.col建床日期.FieldName = "入院日期";
            this.col建床日期.Name = "col建床日期";
            this.col建床日期.Visible = true;
            this.col建床日期.VisibleIndex = 0;
            this.col建床日期.Width = 104;
            // 
            // dte建床日期
            // 
            this.dte建床日期.AutoHeight = false;
            this.dte建床日期.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte建床日期.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte建床日期.Mask.EditMask = "yyyy-MM-dd";
            this.dte建床日期.Mask.UseMaskAsDisplayFormat = true;
            this.dte建床日期.Name = "dte建床日期";
            // 
            // col撤床日期
            // 
            this.col撤床日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col撤床日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col撤床日期.Caption = "撤床日期";
            this.col撤床日期.ColumnEdit = this.dte撤床日期;
            this.col撤床日期.FieldName = "出院日期";
            this.col撤床日期.Name = "col撤床日期";
            this.col撤床日期.Visible = true;
            this.col撤床日期.VisibleIndex = 1;
            this.col撤床日期.Width = 94;
            // 
            // dte撤床日期
            // 
            this.dte撤床日期.AutoHeight = false;
            this.dte撤床日期.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte撤床日期.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte撤床日期.Mask.EditMask = "yyyy-MM-dd";
            this.dte撤床日期.Mask.UseMaskAsDisplayFormat = true;
            this.dte撤床日期.Name = "dte撤床日期";
            // 
            // col原因2
            // 
            this.col原因2.AppearanceHeader.Options.UseTextOptions = true;
            this.col原因2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col原因2.Caption = "原因";
            this.col原因2.FieldName = "原因";
            this.col原因2.Name = "col原因2";
            this.col原因2.Visible = true;
            this.col原因2.VisibleIndex = 2;
            this.col原因2.Width = 133;
            // 
            // col医疗机构名称2
            // 
            this.col医疗机构名称2.AppearanceHeader.Options.UseTextOptions = true;
            this.col医疗机构名称2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col医疗机构名称2.Caption = "医疗机构名称";
            this.col医疗机构名称2.FieldName = "医疗机构名称";
            this.col医疗机构名称2.Name = "col医疗机构名称2";
            this.col医疗机构名称2.Visible = true;
            this.col医疗机构名称2.VisibleIndex = 3;
            this.col医疗机构名称2.Width = 110;
            // 
            // col病案号2
            // 
            this.col病案号2.AppearanceHeader.Options.UseTextOptions = true;
            this.col病案号2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col病案号2.Caption = "病案号";
            this.col病案号2.FieldName = "病案号";
            this.col病案号2.Name = "col病案号2";
            this.col病案号2.Visible = true;
            this.col病案号2.VisibleIndex = 4;
            this.col病案号2.Width = 100;
            // 
            // col个人档案编号2
            // 
            this.col个人档案编号2.Caption = "个人档案编号";
            this.col个人档案编号2.FieldName = "个人档案编号";
            this.col个人档案编号2.Name = "col个人档案编号2";
            // 
            // col类型2
            // 
            this.col类型2.Caption = "类型";
            this.col类型2.FieldName = "类型";
            this.col类型2.Name = "col类型2";
            // 
            // radio家庭病床史
            // 
            this.radio家庭病床史.EditValue = "2";
            this.radio家庭病床史.Location = new System.Drawing.Point(8, 217);
            this.radio家庭病床史.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio家庭病床史.Name = "radio家庭病床史";
            this.radio家庭病床史.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.radio家庭病床史.Properties.Appearance.Options.UseFont = true;
            this.radio家庭病床史.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "有"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "无")});
            this.radio家庭病床史.Size = new System.Drawing.Size(254, 36);
            this.radio家庭病床史.StyleController = this.layoutControl1;
            this.radio家庭病床史.TabIndex = 153;
            this.radio家庭病床史.SelectedIndexChanged += new System.EventHandler(this.radio家庭病床史_SelectedIndexChanged);
            // 
            // btn住院史删除
            // 
            this.btn住院史删除.Enabled = false;
            this.btn住院史删除.Image = ((System.Drawing.Image)(resources.GetObject("btn住院史删除.Image")));
            this.btn住院史删除.Location = new System.Drawing.Point(368, 75);
            this.btn住院史删除.Name = "btn住院史删除";
            this.btn住院史删除.Size = new System.Drawing.Size(125, 36);
            this.btn住院史删除.StyleController = this.layoutControl1;
            this.btn住院史删除.TabIndex = 13;
            this.btn住院史删除.Tag = "2";
            this.btn住院史删除.Text = "删除当前行";
            this.btn住院史删除.Click += new System.EventHandler(this.btn住院史删除_Click);
            // 
            // radio主要用药情况
            // 
            this.radio主要用药情况.EditValue = "2";
            this.radio主要用药情况.Location = new System.Drawing.Point(8, 75);
            this.radio主要用药情况.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio主要用药情况.Name = "radio主要用药情况";
            this.radio主要用药情况.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.radio主要用药情况.Properties.Appearance.Options.UseFont = true;
            this.radio主要用药情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "有"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "无")});
            this.radio主要用药情况.Size = new System.Drawing.Size(157, 36);
            this.radio主要用药情况.StyleController = this.layoutControl1;
            this.radio主要用药情况.TabIndex = 154;
            this.radio主要用药情况.SelectedIndexChanged += new System.EventHandler(this.radio主要用药情况_SelectedIndexChanged);
            // 
            // btn住院史添加
            // 
            this.btn住院史添加.Enabled = false;
            this.btn住院史添加.Image = ((System.Drawing.Image)(resources.GetObject("btn住院史添加.Image")));
            this.btn住院史添加.Location = new System.Drawing.Point(268, 75);
            this.btn住院史添加.Name = "btn住院史添加";
            this.btn住院史添加.Size = new System.Drawing.Size(96, 36);
            this.btn住院史添加.StyleController = this.layoutControl1;
            this.btn住院史添加.TabIndex = 12;
            this.btn住院史添加.Tag = "0";
            this.btn住院史添加.Text = "添加";
            this.btn住院史添加.Click += new System.EventHandler(this.btn住院史添加_Click);
            // 
            // gc住院史
            // 
            this.gc住院史.Location = new System.Drawing.Point(8, 115);
            this.gc住院史.MainView = this.gv住院史;
            this.gc住院史.Name = "gc住院史";
            this.gc住院史.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.dte入院日期,
            this.dte出院日期});
            this.gc住院史.Size = new System.Drawing.Size(702, 98);
            this.gc住院史.TabIndex = 14;
            this.gc住院史.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv住院史});
            // 
            // gv住院史
            // 
            this.gv住院史.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col入院日期,
            this.col出院日期,
            this.col原因,
            this.col医疗机构名称,
            this.col病案号,
            this.col个人档案编号,
            this.col类型});
            this.gv住院史.GridControl = this.gc住院史;
            this.gv住院史.Name = "gv住院史";
            this.gv住院史.OptionsView.ColumnAutoWidth = false;
            this.gv住院史.OptionsView.ShowGroupPanel = false;
            // 
            // col入院日期
            // 
            this.col入院日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col入院日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col入院日期.Caption = "入院日期";
            this.col入院日期.ColumnEdit = this.dte入院日期;
            this.col入院日期.FieldName = "入院日期";
            this.col入院日期.Name = "col入院日期";
            this.col入院日期.Visible = true;
            this.col入院日期.VisibleIndex = 0;
            this.col入院日期.Width = 104;
            // 
            // dte入院日期
            // 
            this.dte入院日期.AutoHeight = false;
            this.dte入院日期.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte入院日期.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte入院日期.Mask.EditMask = "yyyy-MM-dd";
            this.dte入院日期.Mask.UseMaskAsDisplayFormat = true;
            this.dte入院日期.Name = "dte入院日期";
            // 
            // col出院日期
            // 
            this.col出院日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col出院日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col出院日期.Caption = "出院日期";
            this.col出院日期.ColumnEdit = this.dte出院日期;
            this.col出院日期.FieldName = "出院日期";
            this.col出院日期.Name = "col出院日期";
            this.col出院日期.Visible = true;
            this.col出院日期.VisibleIndex = 1;
            this.col出院日期.Width = 96;
            // 
            // dte出院日期
            // 
            this.dte出院日期.AutoHeight = false;
            this.dte出院日期.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出院日期.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出院日期.Mask.EditMask = "yyyy-MM-dd";
            this.dte出院日期.Mask.UseMaskAsDisplayFormat = true;
            this.dte出院日期.Name = "dte出院日期";
            // 
            // col原因
            // 
            this.col原因.AppearanceHeader.Options.UseTextOptions = true;
            this.col原因.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col原因.Caption = "原因";
            this.col原因.FieldName = "原因";
            this.col原因.Name = "col原因";
            this.col原因.Visible = true;
            this.col原因.VisibleIndex = 2;
            this.col原因.Width = 130;
            // 
            // col医疗机构名称
            // 
            this.col医疗机构名称.AppearanceHeader.Options.UseTextOptions = true;
            this.col医疗机构名称.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col医疗机构名称.Caption = "医疗机构名称";
            this.col医疗机构名称.FieldName = "医疗机构名称";
            this.col医疗机构名称.Name = "col医疗机构名称";
            this.col医疗机构名称.Visible = true;
            this.col医疗机构名称.VisibleIndex = 3;
            this.col医疗机构名称.Width = 108;
            // 
            // col病案号
            // 
            this.col病案号.AppearanceHeader.Options.UseTextOptions = true;
            this.col病案号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col病案号.Caption = "病案号";
            this.col病案号.FieldName = "病案号";
            this.col病案号.Name = "col病案号";
            this.col病案号.Visible = true;
            this.col病案号.VisibleIndex = 4;
            this.col病案号.Width = 106;
            // 
            // col个人档案编号
            // 
            this.col个人档案编号.AppearanceHeader.Options.UseTextOptions = true;
            this.col个人档案编号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col个人档案编号.Caption = "个人档案编号";
            this.col个人档案编号.FieldName = "个人档案编号";
            this.col个人档案编号.Name = "col个人档案编号";
            // 
            // col类型
            // 
            this.col类型.AppearanceHeader.Options.UseTextOptions = true;
            this.col类型.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col类型.Caption = "类型";
            this.col类型.FieldName = "类型";
            this.col类型.Name = "col类型";
            // 
            // radio住院史
            // 
            this.radio住院史.EditValue = "2";
            this.radio住院史.Location = new System.Drawing.Point(8, 75);
            this.radio住院史.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio住院史.Name = "radio住院史";
            this.radio住院史.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.radio住院史.Properties.Appearance.Options.UseFont = true;
            this.radio住院史.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "有"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "无")});
            this.radio住院史.Size = new System.Drawing.Size(256, 36);
            this.radio住院史.StyleController = this.layoutControl1;
            this.radio住院史.TabIndex = 152;
            this.radio住院史.SelectedIndexChanged += new System.EventHandler(this.radio住院史_SelectedIndexChanged);
            // 
            // flow脑血管疾病
            // 
            this.flow脑血管疾病.BackColor = System.Drawing.Color.White;
            this.flow脑血管疾病.Controls.Add(this.chk脑血管疾病_未发现);
            this.flow脑血管疾病.Controls.Add(this.chk脑血管疾病_缺血性卒中);
            this.flow脑血管疾病.Controls.Add(this.chk脑血管疾病_脑出血);
            this.flow脑血管疾病.Controls.Add(this.chk脑血管疾病_蛛网膜下腔出血);
            this.flow脑血管疾病.Controls.Add(this.chk脑血管疾病_短暂性脑缺血发作);
            this.flow脑血管疾病.Controls.Add(this.chk脑血管疾病_其他);
            this.flow脑血管疾病.Controls.Add(this.txt脑血管疾病_其他);
            this.flow脑血管疾病.Location = new System.Drawing.Point(103, 75);
            this.flow脑血管疾病.Name = "flow脑血管疾病";
            this.flow脑血管疾病.Size = new System.Drawing.Size(607, 51);
            this.flow脑血管疾病.TabIndex = 145;
            // 
            // chk脑血管疾病_未发现
            // 
            this.chk脑血管疾病_未发现.EditValue = true;
            this.chk脑血管疾病_未发现.Location = new System.Drawing.Point(3, 3);
            this.chk脑血管疾病_未发现.Name = "chk脑血管疾病_未发现";
            this.chk脑血管疾病_未发现.Properties.Caption = "未发现";
            this.chk脑血管疾病_未发现.Size = new System.Drawing.Size(91, 19);
            this.chk脑血管疾病_未发现.TabIndex = 0;
            this.chk脑血管疾病_未发现.Tag = "1";
            this.chk脑血管疾病_未发现.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk脑血管疾病_缺血性卒中
            // 
            this.chk脑血管疾病_缺血性卒中.Location = new System.Drawing.Point(100, 3);
            this.chk脑血管疾病_缺血性卒中.Name = "chk脑血管疾病_缺血性卒中";
            this.chk脑血管疾病_缺血性卒中.Properties.Caption = "缺血性卒中";
            this.chk脑血管疾病_缺血性卒中.Size = new System.Drawing.Size(93, 19);
            this.chk脑血管疾病_缺血性卒中.TabIndex = 1;
            this.chk脑血管疾病_缺血性卒中.Tag = "2";
            // 
            // chk脑血管疾病_脑出血
            // 
            this.chk脑血管疾病_脑出血.Location = new System.Drawing.Point(199, 3);
            this.chk脑血管疾病_脑出血.Name = "chk脑血管疾病_脑出血";
            this.chk脑血管疾病_脑出血.Properties.Caption = "脑出血";
            this.chk脑血管疾病_脑出血.Size = new System.Drawing.Size(101, 19);
            this.chk脑血管疾病_脑出血.TabIndex = 2;
            this.chk脑血管疾病_脑出血.Tag = "3";
            // 
            // chk脑血管疾病_蛛网膜下腔出血
            // 
            this.chk脑血管疾病_蛛网膜下腔出血.Location = new System.Drawing.Point(306, 3);
            this.chk脑血管疾病_蛛网膜下腔出血.Name = "chk脑血管疾病_蛛网膜下腔出血";
            this.chk脑血管疾病_蛛网膜下腔出血.Properties.Caption = "蛛网膜下腔出血";
            this.chk脑血管疾病_蛛网膜下腔出血.Size = new System.Drawing.Size(131, 19);
            this.chk脑血管疾病_蛛网膜下腔出血.TabIndex = 3;
            this.chk脑血管疾病_蛛网膜下腔出血.Tag = "4";
            // 
            // chk脑血管疾病_短暂性脑缺血发作
            // 
            this.chk脑血管疾病_短暂性脑缺血发作.Location = new System.Drawing.Point(443, 3);
            this.chk脑血管疾病_短暂性脑缺血发作.Name = "chk脑血管疾病_短暂性脑缺血发作";
            this.chk脑血管疾病_短暂性脑缺血发作.Properties.Caption = "短暂性脑缺血发作";
            this.chk脑血管疾病_短暂性脑缺血发作.Size = new System.Drawing.Size(152, 19);
            this.chk脑血管疾病_短暂性脑缺血发作.TabIndex = 4;
            this.chk脑血管疾病_短暂性脑缺血发作.Tag = "5";
            // 
            // chk脑血管疾病_其他
            // 
            this.chk脑血管疾病_其他.Location = new System.Drawing.Point(3, 28);
            this.chk脑血管疾病_其他.Name = "chk脑血管疾病_其他";
            this.chk脑血管疾病_其他.Properties.Caption = "其他";
            this.chk脑血管疾病_其他.Size = new System.Drawing.Size(86, 19);
            this.chk脑血管疾病_其他.TabIndex = 5;
            this.chk脑血管疾病_其他.Tag = "99";
            this.chk脑血管疾病_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt脑血管疾病_其他
            // 
            this.txt脑血管疾病_其他.Location = new System.Drawing.Point(95, 28);
            this.txt脑血管疾病_其他.Name = "txt脑血管疾病_其他";
            this.txt脑血管疾病_其他.Size = new System.Drawing.Size(133, 20);
            this.txt脑血管疾病_其他.TabIndex = 6;
            // 
            // flow其他系统疾病
            // 
            this.flow其他系统疾病.BackColor = System.Drawing.Color.White;
            this.flow其他系统疾病.Controls.Add(this.chk其他系统疾病_未发现);
            this.flow其他系统疾病.Controls.Add(this.chk其他系统疾病_糖尿病);
            this.flow其他系统疾病.Controls.Add(this.chk其他系统疾病_慢性支气管炎);
            this.flow其他系统疾病.Controls.Add(this.chk其他系统疾病_慢性阻塞性肺气肿);
            this.flow其他系统疾病.Controls.Add(this.chk其他系统疾病_恶性肿瘤);
            this.flow其他系统疾病.Controls.Add(this.chk其他系统疾病_老年性骨关节病);
            this.flow其他系统疾病.Controls.Add(this.chk其他系统疾病_其他);
            this.flow其他系统疾病.Controls.Add(this.txt其他系统疾病_其他);
            this.flow其他系统疾病.Location = new System.Drawing.Point(103, 327);
            this.flow其他系统疾病.Name = "flow其他系统疾病";
            this.flow其他系统疾病.Size = new System.Drawing.Size(607, 52);
            this.flow其他系统疾病.TabIndex = 151;
            // 
            // chk其他系统疾病_未发现
            // 
            this.chk其他系统疾病_未发现.EditValue = true;
            this.chk其他系统疾病_未发现.Location = new System.Drawing.Point(3, 3);
            this.chk其他系统疾病_未发现.Name = "chk其他系统疾病_未发现";
            this.chk其他系统疾病_未发现.Properties.Caption = "未发现";
            this.chk其他系统疾病_未发现.Size = new System.Drawing.Size(91, 19);
            this.chk其他系统疾病_未发现.TabIndex = 10;
            this.chk其他系统疾病_未发现.Tag = "1";
            this.chk其他系统疾病_未发现.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk其他系统疾病_糖尿病
            // 
            this.chk其他系统疾病_糖尿病.Location = new System.Drawing.Point(100, 3);
            this.chk其他系统疾病_糖尿病.Name = "chk其他系统疾病_糖尿病";
            this.chk其他系统疾病_糖尿病.Properties.Caption = "糖尿病";
            this.chk其他系统疾病_糖尿病.Size = new System.Drawing.Size(93, 19);
            this.chk其他系统疾病_糖尿病.TabIndex = 12;
            this.chk其他系统疾病_糖尿病.Tag = "2";
            // 
            // chk其他系统疾病_慢性支气管炎
            // 
            this.chk其他系统疾病_慢性支气管炎.Location = new System.Drawing.Point(199, 3);
            this.chk其他系统疾病_慢性支气管炎.Name = "chk其他系统疾病_慢性支气管炎";
            this.chk其他系统疾病_慢性支气管炎.Properties.Caption = "慢性支气管炎";
            this.chk其他系统疾病_慢性支气管炎.Size = new System.Drawing.Size(101, 19);
            this.chk其他系统疾病_慢性支气管炎.TabIndex = 13;
            this.chk其他系统疾病_慢性支气管炎.Tag = "3";
            // 
            // chk其他系统疾病_慢性阻塞性肺气肿
            // 
            this.chk其他系统疾病_慢性阻塞性肺气肿.Location = new System.Drawing.Point(306, 3);
            this.chk其他系统疾病_慢性阻塞性肺气肿.Name = "chk其他系统疾病_慢性阻塞性肺气肿";
            this.chk其他系统疾病_慢性阻塞性肺气肿.Properties.Caption = "慢性阻塞性肺气肿";
            this.chk其他系统疾病_慢性阻塞性肺气肿.Size = new System.Drawing.Size(137, 19);
            this.chk其他系统疾病_慢性阻塞性肺气肿.TabIndex = 14;
            this.chk其他系统疾病_慢性阻塞性肺气肿.Tag = "4";
            // 
            // chk其他系统疾病_恶性肿瘤
            // 
            this.chk其他系统疾病_恶性肿瘤.Location = new System.Drawing.Point(449, 3);
            this.chk其他系统疾病_恶性肿瘤.Name = "chk其他系统疾病_恶性肿瘤";
            this.chk其他系统疾病_恶性肿瘤.Properties.Caption = "恶性肿瘤";
            this.chk其他系统疾病_恶性肿瘤.Size = new System.Drawing.Size(94, 19);
            this.chk其他系统疾病_恶性肿瘤.TabIndex = 15;
            this.chk其他系统疾病_恶性肿瘤.Tag = "5";
            // 
            // chk其他系统疾病_老年性骨关节病
            // 
            this.chk其他系统疾病_老年性骨关节病.Location = new System.Drawing.Point(3, 28);
            this.chk其他系统疾病_老年性骨关节病.Name = "chk其他系统疾病_老年性骨关节病";
            this.chk其他系统疾病_老年性骨关节病.Properties.Caption = "老年性骨关节病";
            this.chk其他系统疾病_老年性骨关节病.Size = new System.Drawing.Size(135, 19);
            this.chk其他系统疾病_老年性骨关节病.TabIndex = 16;
            this.chk其他系统疾病_老年性骨关节病.Tag = "6";
            // 
            // chk其他系统疾病_其他
            // 
            this.chk其他系统疾病_其他.Location = new System.Drawing.Point(144, 28);
            this.chk其他系统疾病_其他.Name = "chk其他系统疾病_其他";
            this.chk其他系统疾病_其他.Properties.Caption = "其他";
            this.chk其他系统疾病_其他.Size = new System.Drawing.Size(49, 19);
            this.chk其他系统疾病_其他.TabIndex = 11;
            this.chk其他系统疾病_其他.Tag = "99";
            this.chk其他系统疾病_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt其他系统疾病_其他
            // 
            this.txt其他系统疾病_其他.Enabled = false;
            this.txt其他系统疾病_其他.Location = new System.Drawing.Point(199, 28);
            this.txt其他系统疾病_其他.Name = "txt其他系统疾病_其他";
            this.txt其他系统疾病_其他.Properties.AutoHeight = false;
            this.txt其他系统疾病_其他.Size = new System.Drawing.Size(133, 20);
            this.txt其他系统疾病_其他.TabIndex = 1;
            // 
            // flow神经系统疾病
            // 
            this.flow神经系统疾病.BackColor = System.Drawing.Color.White;
            this.flow神经系统疾病.Controls.Add(this.chk神经系统疾病_未发现);
            this.flow神经系统疾病.Controls.Add(this.chk神经系统疾病_阿尔茨海默症);
            this.flow神经系统疾病.Controls.Add(this.chk神经系统疾病_帕金森症);
            this.flow神经系统疾病.Controls.Add(this.chk神经系统疾病_其他);
            this.flow神经系统疾病.Controls.Add(this.txt神经系统疾病_其他);
            this.flow神经系统疾病.Location = new System.Drawing.Point(103, 296);
            this.flow神经系统疾病.Name = "flow神经系统疾病";
            this.flow神经系统疾病.Size = new System.Drawing.Size(607, 27);
            this.flow神经系统疾病.TabIndex = 150;
            // 
            // chk神经系统疾病_未发现
            // 
            this.chk神经系统疾病_未发现.EditValue = true;
            this.chk神经系统疾病_未发现.Location = new System.Drawing.Point(3, 3);
            this.chk神经系统疾病_未发现.Name = "chk神经系统疾病_未发现";
            this.chk神经系统疾病_未发现.Properties.Caption = "未发现";
            this.chk神经系统疾病_未发现.Size = new System.Drawing.Size(81, 19);
            this.chk神经系统疾病_未发现.TabIndex = 2;
            this.chk神经系统疾病_未发现.Tag = "1";
            this.chk神经系统疾病_未发现.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk神经系统疾病_阿尔茨海默症
            // 
            this.chk神经系统疾病_阿尔茨海默症.Location = new System.Drawing.Point(90, 3);
            this.chk神经系统疾病_阿尔茨海默症.Name = "chk神经系统疾病_阿尔茨海默症";
            this.chk神经系统疾病_阿尔茨海默症.Properties.Caption = "阿尔茨海默症(老年性痴呆)";
            this.chk神经系统疾病_阿尔茨海默症.Size = new System.Drawing.Size(191, 19);
            this.chk神经系统疾病_阿尔茨海默症.TabIndex = 7;
            this.chk神经系统疾病_阿尔茨海默症.Tag = "2";
            // 
            // chk神经系统疾病_帕金森症
            // 
            this.chk神经系统疾病_帕金森症.Location = new System.Drawing.Point(287, 3);
            this.chk神经系统疾病_帕金森症.Name = "chk神经系统疾病_帕金森症";
            this.chk神经系统疾病_帕金森症.Properties.Caption = "帕金森病";
            this.chk神经系统疾病_帕金森症.Size = new System.Drawing.Size(85, 19);
            this.chk神经系统疾病_帕金森症.TabIndex = 8;
            this.chk神经系统疾病_帕金森症.Tag = "3";
            // 
            // chk神经系统疾病_其他
            // 
            this.chk神经系统疾病_其他.Location = new System.Drawing.Point(378, 3);
            this.chk神经系统疾病_其他.Name = "chk神经系统疾病_其他";
            this.chk神经系统疾病_其他.Properties.Caption = "其他";
            this.chk神经系统疾病_其他.Size = new System.Drawing.Size(60, 19);
            this.chk神经系统疾病_其他.TabIndex = 6;
            this.chk神经系统疾病_其他.Tag = "99";
            this.chk神经系统疾病_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt神经系统疾病_其他
            // 
            this.txt神经系统疾病_其他.Enabled = false;
            this.txt神经系统疾病_其他.Location = new System.Drawing.Point(444, 3);
            this.txt神经系统疾病_其他.Name = "txt神经系统疾病_其他";
            this.txt神经系统疾病_其他.Properties.AutoHeight = false;
            this.txt神经系统疾病_其他.Size = new System.Drawing.Size(133, 20);
            this.txt神经系统疾病_其他.TabIndex = 1;
            // 
            // flow眼部疾病
            // 
            this.flow眼部疾病.BackColor = System.Drawing.Color.White;
            this.flow眼部疾病.Controls.Add(this.chk眼部疾病_未发现);
            this.flow眼部疾病.Controls.Add(this.chk眼部疾病_视网膜出血或渗出);
            this.flow眼部疾病.Controls.Add(this.chk眼部疾病_视乳头水肿);
            this.flow眼部疾病.Controls.Add(this.chk眼部疾病_白内障);
            this.flow眼部疾病.Controls.Add(this.chk眼部疾病_其他);
            this.flow眼部疾病.Controls.Add(this.txt眼部疾病_其他);
            this.flow眼部疾病.Location = new System.Drawing.Point(103, 266);
            this.flow眼部疾病.Name = "flow眼部疾病";
            this.flow眼部疾病.Size = new System.Drawing.Size(607, 26);
            this.flow眼部疾病.TabIndex = 149;
            // 
            // chk眼部疾病_未发现
            // 
            this.chk眼部疾病_未发现.EditValue = true;
            this.chk眼部疾病_未发现.Location = new System.Drawing.Point(3, 3);
            this.chk眼部疾病_未发现.Name = "chk眼部疾病_未发现";
            this.chk眼部疾病_未发现.Properties.Caption = "未发现";
            this.chk眼部疾病_未发现.Size = new System.Drawing.Size(69, 19);
            this.chk眼部疾病_未发现.TabIndex = 0;
            this.chk眼部疾病_未发现.Tag = "1";
            this.chk眼部疾病_未发现.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk眼部疾病_视网膜出血或渗出
            // 
            this.chk眼部疾病_视网膜出血或渗出.Location = new System.Drawing.Point(78, 3);
            this.chk眼部疾病_视网膜出血或渗出.Name = "chk眼部疾病_视网膜出血或渗出";
            this.chk眼部疾病_视网膜出血或渗出.Properties.Caption = "视网膜出血或渗出";
            this.chk眼部疾病_视网膜出血或渗出.Size = new System.Drawing.Size(130, 19);
            this.chk眼部疾病_视网膜出血或渗出.TabIndex = 1;
            this.chk眼部疾病_视网膜出血或渗出.Tag = "2";
            // 
            // chk眼部疾病_视乳头水肿
            // 
            this.chk眼部疾病_视乳头水肿.Location = new System.Drawing.Point(214, 3);
            this.chk眼部疾病_视乳头水肿.Name = "chk眼部疾病_视乳头水肿";
            this.chk眼部疾病_视乳头水肿.Properties.Caption = "视乳头水肿";
            this.chk眼部疾病_视乳头水肿.Size = new System.Drawing.Size(101, 19);
            this.chk眼部疾病_视乳头水肿.TabIndex = 2;
            this.chk眼部疾病_视乳头水肿.Tag = "3";
            // 
            // chk眼部疾病_白内障
            // 
            this.chk眼部疾病_白内障.Location = new System.Drawing.Point(321, 3);
            this.chk眼部疾病_白内障.Name = "chk眼部疾病_白内障";
            this.chk眼部疾病_白内障.Properties.Caption = "白内障";
            this.chk眼部疾病_白内障.Size = new System.Drawing.Size(62, 19);
            this.chk眼部疾病_白内障.TabIndex = 3;
            this.chk眼部疾病_白内障.Tag = "4";
            // 
            // chk眼部疾病_其他
            // 
            this.chk眼部疾病_其他.Location = new System.Drawing.Point(389, 3);
            this.chk眼部疾病_其他.Name = "chk眼部疾病_其他";
            this.chk眼部疾病_其他.Properties.Caption = "其他";
            this.chk眼部疾病_其他.Size = new System.Drawing.Size(60, 19);
            this.chk眼部疾病_其他.TabIndex = 5;
            this.chk眼部疾病_其他.Tag = "99";
            this.chk眼部疾病_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt眼部疾病_其他
            // 
            this.txt眼部疾病_其他.Location = new System.Drawing.Point(455, 3);
            this.txt眼部疾病_其他.Name = "txt眼部疾病_其他";
            this.txt眼部疾病_其他.Size = new System.Drawing.Size(133, 20);
            this.txt眼部疾病_其他.TabIndex = 6;
            // 
            // flow心脏疾病
            // 
            this.flow心脏疾病.BackColor = System.Drawing.Color.White;
            this.flow心脏疾病.Controls.Add(this.chk心脏疾病_未发现);
            this.flow心脏疾病.Controls.Add(this.chk心脏疾病_心肌梗死);
            this.flow心脏疾病.Controls.Add(this.chk心脏疾病_心绞痛);
            this.flow心脏疾病.Controls.Add(this.chk心脏疾病_冠状动脉血运重建);
            this.flow心脏疾病.Controls.Add(this.chk心脏疾病_充血性心力衰竭);
            this.flow心脏疾病.Controls.Add(this.chk心脏疾病_心前区疼痛);
            this.flow心脏疾病.Controls.Add(this.chk心脏疾病_高血压);
            this.flow心脏疾病.Controls.Add(this.chk心脏疾病_夹层动脉瘤);
            this.flow心脏疾病.Controls.Add(this.chk心脏疾病_动脉闭塞性疾病);
            this.flow心脏疾病.Controls.Add(this.chk心脏疾病_其他);
            this.flow心脏疾病.Controls.Add(this.chktxt心脏疾病_冠心病);
            this.flow心脏疾病.Controls.Add(this.txt心脏疾病_其他);
            this.flow心脏疾病.Location = new System.Drawing.Point(103, 185);
            this.flow心脏疾病.Name = "flow心脏疾病";
            this.flow心脏疾病.Size = new System.Drawing.Size(607, 77);
            this.flow心脏疾病.TabIndex = 147;
            // 
            // chk心脏疾病_未发现
            // 
            this.chk心脏疾病_未发现.EditValue = true;
            this.chk心脏疾病_未发现.Location = new System.Drawing.Point(3, 3);
            this.chk心脏疾病_未发现.Name = "chk心脏疾病_未发现";
            this.chk心脏疾病_未发现.Properties.Caption = "未发现";
            this.chk心脏疾病_未发现.Size = new System.Drawing.Size(91, 19);
            this.chk心脏疾病_未发现.TabIndex = 0;
            this.chk心脏疾病_未发现.Tag = "1";
            this.chk心脏疾病_未发现.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk心脏疾病_心肌梗死
            // 
            this.chk心脏疾病_心肌梗死.Location = new System.Drawing.Point(100, 3);
            this.chk心脏疾病_心肌梗死.Name = "chk心脏疾病_心肌梗死";
            this.chk心脏疾病_心肌梗死.Properties.Caption = "心肌梗死";
            this.chk心脏疾病_心肌梗死.Size = new System.Drawing.Size(93, 19);
            this.chk心脏疾病_心肌梗死.TabIndex = 1;
            this.chk心脏疾病_心肌梗死.Tag = "2";
            // 
            // chk心脏疾病_心绞痛
            // 
            this.chk心脏疾病_心绞痛.Location = new System.Drawing.Point(199, 3);
            this.chk心脏疾病_心绞痛.Name = "chk心脏疾病_心绞痛";
            this.chk心脏疾病_心绞痛.Properties.Caption = "心绞痛";
            this.chk心脏疾病_心绞痛.Size = new System.Drawing.Size(101, 19);
            this.chk心脏疾病_心绞痛.TabIndex = 2;
            this.chk心脏疾病_心绞痛.Tag = "3";
            // 
            // chk心脏疾病_冠状动脉血运重建
            // 
            this.chk心脏疾病_冠状动脉血运重建.Location = new System.Drawing.Point(306, 3);
            this.chk心脏疾病_冠状动脉血运重建.Name = "chk心脏疾病_冠状动脉血运重建";
            this.chk心脏疾病_冠状动脉血运重建.Properties.Caption = "冠状动脉血运重建";
            this.chk心脏疾病_冠状动脉血运重建.Size = new System.Drawing.Size(137, 19);
            this.chk心脏疾病_冠状动脉血运重建.TabIndex = 3;
            this.chk心脏疾病_冠状动脉血运重建.Tag = "4";
            // 
            // chk心脏疾病_充血性心力衰竭
            // 
            this.chk心脏疾病_充血性心力衰竭.Location = new System.Drawing.Point(449, 3);
            this.chk心脏疾病_充血性心力衰竭.Name = "chk心脏疾病_充血性心力衰竭";
            this.chk心脏疾病_充血性心力衰竭.Properties.Caption = "充血性心力衰竭";
            this.chk心脏疾病_充血性心力衰竭.Size = new System.Drawing.Size(136, 19);
            this.chk心脏疾病_充血性心力衰竭.TabIndex = 4;
            this.chk心脏疾病_充血性心力衰竭.Tag = "5";
            // 
            // chk心脏疾病_心前区疼痛
            // 
            this.chk心脏疾病_心前区疼痛.Location = new System.Drawing.Point(3, 28);
            this.chk心脏疾病_心前区疼痛.Name = "chk心脏疾病_心前区疼痛";
            this.chk心脏疾病_心前区疼痛.Properties.Caption = "心前区疼痛";
            this.chk心脏疾病_心前区疼痛.Size = new System.Drawing.Size(91, 19);
            this.chk心脏疾病_心前区疼痛.TabIndex = 5;
            this.chk心脏疾病_心前区疼痛.Tag = "6";
            // 
            // chk心脏疾病_高血压
            // 
            this.chk心脏疾病_高血压.Location = new System.Drawing.Point(100, 28);
            this.chk心脏疾病_高血压.Name = "chk心脏疾病_高血压";
            this.chk心脏疾病_高血压.Properties.Caption = "高血压病";
            this.chk心脏疾病_高血压.Size = new System.Drawing.Size(93, 19);
            this.chk心脏疾病_高血压.TabIndex = 13;
            this.chk心脏疾病_高血压.Tag = "7";
            // 
            // chk心脏疾病_夹层动脉瘤
            // 
            this.chk心脏疾病_夹层动脉瘤.Location = new System.Drawing.Point(199, 28);
            this.chk心脏疾病_夹层动脉瘤.Name = "chk心脏疾病_夹层动脉瘤";
            this.chk心脏疾病_夹层动脉瘤.Properties.Caption = "夹层动脉瘤";
            this.chk心脏疾病_夹层动脉瘤.Size = new System.Drawing.Size(101, 19);
            this.chk心脏疾病_夹层动脉瘤.TabIndex = 14;
            this.chk心脏疾病_夹层动脉瘤.Tag = "8";
            // 
            // chk心脏疾病_动脉闭塞性疾病
            // 
            this.chk心脏疾病_动脉闭塞性疾病.Location = new System.Drawing.Point(306, 28);
            this.chk心脏疾病_动脉闭塞性疾病.Name = "chk心脏疾病_动脉闭塞性疾病";
            this.chk心脏疾病_动脉闭塞性疾病.Properties.Caption = "动脉闭塞性疾病";
            this.chk心脏疾病_动脉闭塞性疾病.Size = new System.Drawing.Size(137, 19);
            this.chk心脏疾病_动脉闭塞性疾病.TabIndex = 15;
            this.chk心脏疾病_动脉闭塞性疾病.Tag = "9";
            // 
            // chk心脏疾病_其他
            // 
            this.chk心脏疾病_其他.Location = new System.Drawing.Point(449, 28);
            this.chk心脏疾病_其他.Name = "chk心脏疾病_其他";
            this.chk心脏疾病_其他.Properties.Caption = "其他";
            this.chk心脏疾病_其他.Size = new System.Drawing.Size(68, 19);
            this.chk心脏疾病_其他.TabIndex = 17;
            this.chk心脏疾病_其他.Tag = "99";
            this.chk心脏疾病_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // chktxt心脏疾病_冠心病
            // 
            this.chktxt心脏疾病_冠心病.Location = new System.Drawing.Point(3, 53);
            this.chktxt心脏疾病_冠心病.Name = "chktxt心脏疾病_冠心病";
            this.chktxt心脏疾病_冠心病.Properties.Caption = "冠心病";
            this.chktxt心脏疾病_冠心病.Size = new System.Drawing.Size(123, 19);
            this.chktxt心脏疾病_冠心病.TabIndex = 18;
            this.chktxt心脏疾病_冠心病.Tag = "99_1";
            // 
            // txt心脏疾病_其他
            // 
            this.txt心脏疾病_其他.Location = new System.Drawing.Point(132, 53);
            this.txt心脏疾病_其他.Name = "txt心脏疾病_其他";
            this.txt心脏疾病_其他.Size = new System.Drawing.Size(133, 20);
            this.txt心脏疾病_其他.TabIndex = 16;
            // 
            // flow肾脏疾病
            // 
            this.flow肾脏疾病.BackColor = System.Drawing.Color.White;
            this.flow肾脏疾病.Controls.Add(this.chk肾脏疾病_未发现);
            this.flow肾脏疾病.Controls.Add(this.chk肾脏疾病_糖尿病肾病);
            this.flow肾脏疾病.Controls.Add(this.chk肾脏疾病_肾功能衰竭);
            this.flow肾脏疾病.Controls.Add(this.chk肾脏疾病_急性肾炎);
            this.flow肾脏疾病.Controls.Add(this.chk肾脏疾病_慢性肾炎);
            this.flow肾脏疾病.Controls.Add(this.chk肾脏疾病_其他);
            this.flow肾脏疾病.Controls.Add(this.txt肾脏疾病_其他);
            this.flow肾脏疾病.Location = new System.Drawing.Point(103, 130);
            this.flow肾脏疾病.Name = "flow肾脏疾病";
            this.flow肾脏疾病.Size = new System.Drawing.Size(607, 51);
            this.flow肾脏疾病.TabIndex = 146;
            // 
            // chk肾脏疾病_未发现
            // 
            this.chk肾脏疾病_未发现.EditValue = true;
            this.chk肾脏疾病_未发现.Location = new System.Drawing.Point(3, 3);
            this.chk肾脏疾病_未发现.Name = "chk肾脏疾病_未发现";
            this.chk肾脏疾病_未发现.Properties.Caption = "未发现";
            this.chk肾脏疾病_未发现.Size = new System.Drawing.Size(91, 19);
            this.chk肾脏疾病_未发现.TabIndex = 0;
            this.chk肾脏疾病_未发现.Tag = "1";
            this.chk肾脏疾病_未发现.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk肾脏疾病_糖尿病肾病
            // 
            this.chk肾脏疾病_糖尿病肾病.Location = new System.Drawing.Point(100, 3);
            this.chk肾脏疾病_糖尿病肾病.Name = "chk肾脏疾病_糖尿病肾病";
            this.chk肾脏疾病_糖尿病肾病.Properties.Caption = "糖尿病肾病";
            this.chk肾脏疾病_糖尿病肾病.Size = new System.Drawing.Size(93, 19);
            this.chk肾脏疾病_糖尿病肾病.TabIndex = 1;
            this.chk肾脏疾病_糖尿病肾病.Tag = "2";
            // 
            // chk肾脏疾病_肾功能衰竭
            // 
            this.chk肾脏疾病_肾功能衰竭.Location = new System.Drawing.Point(199, 3);
            this.chk肾脏疾病_肾功能衰竭.Name = "chk肾脏疾病_肾功能衰竭";
            this.chk肾脏疾病_肾功能衰竭.Properties.Caption = "肾功能衰竭";
            this.chk肾脏疾病_肾功能衰竭.Size = new System.Drawing.Size(101, 19);
            this.chk肾脏疾病_肾功能衰竭.TabIndex = 2;
            this.chk肾脏疾病_肾功能衰竭.Tag = "3";
            // 
            // chk肾脏疾病_急性肾炎
            // 
            this.chk肾脏疾病_急性肾炎.Location = new System.Drawing.Point(306, 3);
            this.chk肾脏疾病_急性肾炎.Name = "chk肾脏疾病_急性肾炎";
            this.chk肾脏疾病_急性肾炎.Properties.Caption = "急性肾炎";
            this.chk肾脏疾病_急性肾炎.Size = new System.Drawing.Size(132, 19);
            this.chk肾脏疾病_急性肾炎.TabIndex = 3;
            this.chk肾脏疾病_急性肾炎.Tag = "4";
            // 
            // chk肾脏疾病_慢性肾炎
            // 
            this.chk肾脏疾病_慢性肾炎.Location = new System.Drawing.Point(444, 3);
            this.chk肾脏疾病_慢性肾炎.Name = "chk肾脏疾病_慢性肾炎";
            this.chk肾脏疾病_慢性肾炎.Properties.Caption = "慢性肾炎";
            this.chk肾脏疾病_慢性肾炎.Size = new System.Drawing.Size(128, 19);
            this.chk肾脏疾病_慢性肾炎.TabIndex = 4;
            this.chk肾脏疾病_慢性肾炎.Tag = "5";
            // 
            // chk肾脏疾病_其他
            // 
            this.chk肾脏疾病_其他.Location = new System.Drawing.Point(3, 28);
            this.chk肾脏疾病_其他.Name = "chk肾脏疾病_其他";
            this.chk肾脏疾病_其他.Properties.Caption = "其他";
            this.chk肾脏疾病_其他.Size = new System.Drawing.Size(100, 19);
            this.chk肾脏疾病_其他.TabIndex = 5;
            this.chk肾脏疾病_其他.Tag = "99";
            this.chk肾脏疾病_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt肾脏疾病_其他
            // 
            this.txt肾脏疾病_其他.Location = new System.Drawing.Point(109, 28);
            this.txt肾脏疾病_其他.Name = "txt肾脏疾病_其他";
            this.txt肾脏疾病_其他.Size = new System.Drawing.Size(133, 20);
            this.txt肾脏疾病_其他.TabIndex = 6;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.TCG_Tags,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem_医师签字});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(718, 423);
            this.layoutControlGroup1.Text = "健康体检表";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // TCG_Tags
            // 
            this.TCG_Tags.AppearanceTabPage.Header.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TCG_Tags.AppearanceTabPage.Header.Options.UseFont = true;
            this.TCG_Tags.AppearanceTabPage.Header.Options.UseTextOptions = true;
            this.TCG_Tags.AppearanceTabPage.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TCG_Tags.AppearanceTabPage.HeaderActive.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TCG_Tags.AppearanceTabPage.HeaderActive.ForeColor = System.Drawing.Color.Blue;
            this.TCG_Tags.AppearanceTabPage.HeaderActive.Options.UseFont = true;
            this.TCG_Tags.AppearanceTabPage.HeaderActive.Options.UseForeColor = true;
            this.TCG_Tags.CustomizationFormText = "tabbedControlGroup1";
            this.TCG_Tags.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TCG_Tags.Location = new System.Drawing.Point(0, 44);
            this.TCG_Tags.Name = "TCG_Tags";
            this.TCG_Tags.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.TCG_Tags.SelectedTabPage = this.LCG_接种史;
            this.TCG_Tags.SelectedTabPageIndex = 3;
            this.TCG_Tags.Size = new System.Drawing.Size(712, 337);
            this.TCG_Tags.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LCG_现存主要健康问题,
            this.LCG_住院治疗情况,
            this.LCG_主要用药情况,
            this.LCG_接种史});
            this.TCG_Tags.Text = "TCG_Tags";
            this.TCG_Tags.SelectedPageChanged += new DevExpress.XtraLayout.LayoutTabPageChangedEventHandler(this.TCG_Tags_SelectedPageChanged);
            // 
            // LCG_现存主要健康问题
            // 
            this.LCG_现存主要健康问题.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.LCG_现存主要健康问题.AppearanceGroup.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.LCG_现存主要健康问题.AppearanceGroup.Options.UseFont = true;
            this.LCG_现存主要健康问题.AppearanceGroup.Options.UseForeColor = true;
            this.LCG_现存主要健康问题.AppearanceGroup.Options.UseTextOptions = true;
            this.LCG_现存主要健康问题.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LCG_现存主要健康问题.CustomizationFormText = "现存主要健康问题";
            this.LCG_现存主要健康问题.ExpandButtonVisible = true;
            this.LCG_现存主要健康问题.ExpandOnDoubleClick = true;
            this.LCG_现存主要健康问题.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl脑血管疾病,
            this.lbl肾脏疾病,
            this.lbl心脏疾病,
            this.lbl眼部疾病,
            this.lbl其他系统疾病,
            this.lbl神经系统疾病});
            this.LCG_现存主要健康问题.Location = new System.Drawing.Point(0, 0);
            this.LCG_现存主要健康问题.Name = "LCG_现存主要健康问题";
            this.LCG_现存主要健康问题.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.LCG_现存主要健康问题.Size = new System.Drawing.Size(706, 308);
            this.LCG_现存主要健康问题.Text = "现存主要健康问题";
            // 
            // lbl脑血管疾病
            // 
            this.lbl脑血管疾病.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl脑血管疾病.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl脑血管疾病.AppearanceItemCaption.Options.UseFont = true;
            this.lbl脑血管疾病.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl脑血管疾病.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl脑血管疾病.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl脑血管疾病.Control = this.flow脑血管疾病;
            this.lbl脑血管疾病.CustomizationFormText = "脑血管疾病";
            this.lbl脑血管疾病.Location = new System.Drawing.Point(0, 0);
            this.lbl脑血管疾病.MinSize = new System.Drawing.Size(199, 26);
            this.lbl脑血管疾病.Name = "lbl脑血管疾病";
            this.lbl脑血管疾病.Size = new System.Drawing.Size(706, 55);
            this.lbl脑血管疾病.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl脑血管疾病.Text = "脑血管疾病";
            this.lbl脑血管疾病.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl脑血管疾病.TextSize = new System.Drawing.Size(90, 20);
            this.lbl脑血管疾病.TextToControlDistance = 5;
            // 
            // lbl肾脏疾病
            // 
            this.lbl肾脏疾病.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl肾脏疾病.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl肾脏疾病.AppearanceItemCaption.Options.UseFont = true;
            this.lbl肾脏疾病.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl肾脏疾病.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl肾脏疾病.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl肾脏疾病.Control = this.flow肾脏疾病;
            this.lbl肾脏疾病.CustomizationFormText = "肾脏疾病";
            this.lbl肾脏疾病.Location = new System.Drawing.Point(0, 55);
            this.lbl肾脏疾病.MinSize = new System.Drawing.Size(199, 26);
            this.lbl肾脏疾病.Name = "lbl肾脏疾病";
            this.lbl肾脏疾病.Size = new System.Drawing.Size(706, 55);
            this.lbl肾脏疾病.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl肾脏疾病.Text = "肾脏疾病";
            this.lbl肾脏疾病.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl肾脏疾病.TextSize = new System.Drawing.Size(90, 20);
            this.lbl肾脏疾病.TextToControlDistance = 5;
            // 
            // lbl心脏疾病
            // 
            this.lbl心脏疾病.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl心脏疾病.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl心脏疾病.AppearanceItemCaption.Options.UseFont = true;
            this.lbl心脏疾病.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl心脏疾病.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl心脏疾病.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl心脏疾病.Control = this.flow心脏疾病;
            this.lbl心脏疾病.CustomizationFormText = "心脏疾病";
            this.lbl心脏疾病.Location = new System.Drawing.Point(0, 110);
            this.lbl心脏疾病.MinSize = new System.Drawing.Size(199, 45);
            this.lbl心脏疾病.Name = "lbl心脏疾病";
            this.lbl心脏疾病.Size = new System.Drawing.Size(706, 81);
            this.lbl心脏疾病.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl心脏疾病.Text = "心血管疾病";
            this.lbl心脏疾病.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl心脏疾病.TextSize = new System.Drawing.Size(90, 20);
            this.lbl心脏疾病.TextToControlDistance = 5;
            // 
            // lbl眼部疾病
            // 
            this.lbl眼部疾病.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl眼部疾病.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl眼部疾病.AppearanceItemCaption.Options.UseFont = true;
            this.lbl眼部疾病.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl眼部疾病.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl眼部疾病.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl眼部疾病.Control = this.flow眼部疾病;
            this.lbl眼部疾病.CustomizationFormText = "眼部疾病";
            this.lbl眼部疾病.Location = new System.Drawing.Point(0, 191);
            this.lbl眼部疾病.MinSize = new System.Drawing.Size(199, 26);
            this.lbl眼部疾病.Name = "lbl眼部疾病";
            this.lbl眼部疾病.Size = new System.Drawing.Size(706, 30);
            this.lbl眼部疾病.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl眼部疾病.Text = "眼部疾病";
            this.lbl眼部疾病.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl眼部疾病.TextSize = new System.Drawing.Size(90, 20);
            this.lbl眼部疾病.TextToControlDistance = 5;
            // 
            // lbl其他系统疾病
            // 
            this.lbl其他系统疾病.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl其他系统疾病.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl其他系统疾病.AppearanceItemCaption.Options.UseFont = true;
            this.lbl其他系统疾病.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl其他系统疾病.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl其他系统疾病.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl其他系统疾病.Control = this.flow其他系统疾病;
            this.lbl其他系统疾病.CustomizationFormText = "其他系统疾病";
            this.lbl其他系统疾病.Location = new System.Drawing.Point(0, 252);
            this.lbl其他系统疾病.MinSize = new System.Drawing.Size(199, 26);
            this.lbl其他系统疾病.Name = "lbl其他系统疾病";
            this.lbl其他系统疾病.Size = new System.Drawing.Size(706, 56);
            this.lbl其他系统疾病.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl其他系统疾病.Text = "其他系统疾病";
            this.lbl其他系统疾病.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl其他系统疾病.TextSize = new System.Drawing.Size(90, 20);
            this.lbl其他系统疾病.TextToControlDistance = 5;
            // 
            // lbl神经系统疾病
            // 
            this.lbl神经系统疾病.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl神经系统疾病.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl神经系统疾病.AppearanceItemCaption.Options.UseFont = true;
            this.lbl神经系统疾病.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl神经系统疾病.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl神经系统疾病.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl神经系统疾病.Control = this.flow神经系统疾病;
            this.lbl神经系统疾病.CustomizationFormText = "神经系统疾病";
            this.lbl神经系统疾病.Location = new System.Drawing.Point(0, 221);
            this.lbl神经系统疾病.MinSize = new System.Drawing.Size(199, 26);
            this.lbl神经系统疾病.Name = "lbl神经系统疾病";
            this.lbl神经系统疾病.Size = new System.Drawing.Size(706, 31);
            this.lbl神经系统疾病.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl神经系统疾病.Text = "神经系统疾病";
            this.lbl神经系统疾病.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl神经系统疾病.TextSize = new System.Drawing.Size(90, 20);
            this.lbl神经系统疾病.TextToControlDistance = 5;
            // 
            // LCG_住院治疗情况
            // 
            this.LCG_住院治疗情况.CustomizationFormText = "住院治疗情况 ";
            this.LCG_住院治疗情况.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lblgc住院史,
            this.lbl住院史,
            this.layoutControlItem157,
            this.layoutControlItem159,
            this.lblgc家庭病床史,
            this.layoutControlItem156,
            this.layoutControlItem160,
            this.layoutControlItem161,
            this.emptySpaceItem27,
            this.emptySpaceItem28});
            this.LCG_住院治疗情况.Location = new System.Drawing.Point(0, 0);
            this.LCG_住院治疗情况.Name = "LCG_住院治疗情况";
            this.LCG_住院治疗情况.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.LCG_住院治疗情况.Size = new System.Drawing.Size(706, 308);
            this.LCG_住院治疗情况.Text = "住院治疗情况 ";
            // 
            // lblgc住院史
            // 
            this.lblgc住院史.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lblgc住院史.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblgc住院史.Control = this.gc住院史;
            this.lblgc住院史.CustomizationFormText = " ";
            this.lblgc住院史.Location = new System.Drawing.Point(0, 40);
            this.lblgc住院史.MaxSize = new System.Drawing.Size(700, 0);
            this.lblgc住院史.MinSize = new System.Drawing.Size(600, 90);
            this.lblgc住院史.Name = "lblgc住院史";
            this.lblgc住院史.Size = new System.Drawing.Size(706, 102);
            this.lblgc住院史.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lblgc住院史.Text = " ";
            this.lblgc住院史.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lblgc住院史.TextSize = new System.Drawing.Size(0, 0);
            this.lblgc住院史.TextToControlDistance = 0;
            this.lblgc住院史.TextVisible = false;
            // 
            // lbl住院史
            // 
            this.lbl住院史.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl住院史.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl住院史.Control = this.radio住院史;
            this.lbl住院史.CustomizationFormText = "住院史";
            this.lbl住院史.Location = new System.Drawing.Point(0, 0);
            this.lbl住院史.MinSize = new System.Drawing.Size(54, 40);
            this.lbl住院史.Name = "lbl住院史";
            this.lbl住院史.Size = new System.Drawing.Size(260, 40);
            this.lbl住院史.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl住院史.Text = "住院史";
            this.lbl住院史.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl住院史.TextSize = new System.Drawing.Size(0, 0);
            this.lbl住院史.TextToControlDistance = 0;
            this.lbl住院史.TextVisible = false;
            // 
            // layoutControlItem157
            // 
            this.layoutControlItem157.Control = this.btn住院史添加;
            this.layoutControlItem157.CustomizationFormText = "layoutControlItem157";
            this.layoutControlItem157.Location = new System.Drawing.Point(260, 0);
            this.layoutControlItem157.MaxSize = new System.Drawing.Size(0, 40);
            this.layoutControlItem157.MinSize = new System.Drawing.Size(1, 40);
            this.layoutControlItem157.Name = "layoutControlItem157";
            this.layoutControlItem157.Size = new System.Drawing.Size(100, 40);
            this.layoutControlItem157.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem157.Text = "layoutControlItem157";
            this.layoutControlItem157.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem157.TextToControlDistance = 0;
            this.layoutControlItem157.TextVisible = false;
            // 
            // layoutControlItem159
            // 
            this.layoutControlItem159.Control = this.btn住院史删除;
            this.layoutControlItem159.CustomizationFormText = "layoutControlItem159";
            this.layoutControlItem159.Location = new System.Drawing.Point(360, 0);
            this.layoutControlItem159.MaxSize = new System.Drawing.Size(0, 40);
            this.layoutControlItem159.MinSize = new System.Drawing.Size(1, 40);
            this.layoutControlItem159.Name = "layoutControlItem159";
            this.layoutControlItem159.Size = new System.Drawing.Size(129, 40);
            this.layoutControlItem159.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem159.Text = "layoutControlItem159";
            this.layoutControlItem159.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem159.TextToControlDistance = 0;
            this.layoutControlItem159.TextVisible = false;
            // 
            // lblgc家庭病床史
            // 
            this.lblgc家庭病床史.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lblgc家庭病床史.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblgc家庭病床史.Control = this.gc建床史;
            this.lblgc家庭病床史.CustomizationFormText = " ";
            this.lblgc家庭病床史.Location = new System.Drawing.Point(0, 182);
            this.lblgc家庭病床史.MaxSize = new System.Drawing.Size(700, 0);
            this.lblgc家庭病床史.MinSize = new System.Drawing.Size(600, 90);
            this.lblgc家庭病床史.Name = "lblgc家庭病床史";
            this.lblgc家庭病床史.Size = new System.Drawing.Size(706, 126);
            this.lblgc家庭病床史.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lblgc家庭病床史.Text = " ";
            this.lblgc家庭病床史.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lblgc家庭病床史.TextSize = new System.Drawing.Size(0, 0);
            this.lblgc家庭病床史.TextToControlDistance = 0;
            this.lblgc家庭病床史.TextVisible = false;
            // 
            // layoutControlItem156
            // 
            this.layoutControlItem156.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem156.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem156.Control = this.radio家庭病床史;
            this.layoutControlItem156.CustomizationFormText = "家庭病床史";
            this.layoutControlItem156.Location = new System.Drawing.Point(0, 142);
            this.layoutControlItem156.MinSize = new System.Drawing.Size(54, 40);
            this.layoutControlItem156.Name = "layoutControlItem156";
            this.layoutControlItem156.Size = new System.Drawing.Size(258, 40);
            this.layoutControlItem156.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem156.Text = "家庭病床史";
            this.layoutControlItem156.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem156.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem156.TextToControlDistance = 0;
            this.layoutControlItem156.TextVisible = false;
            // 
            // layoutControlItem160
            // 
            this.layoutControlItem160.Control = this.btn病床史添加;
            this.layoutControlItem160.CustomizationFormText = "layoutControlItem160";
            this.layoutControlItem160.Location = new System.Drawing.Point(258, 142);
            this.layoutControlItem160.MaxSize = new System.Drawing.Size(0, 40);
            this.layoutControlItem160.MinSize = new System.Drawing.Size(1, 40);
            this.layoutControlItem160.Name = "layoutControlItem160";
            this.layoutControlItem160.Size = new System.Drawing.Size(102, 40);
            this.layoutControlItem160.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem160.Text = "layoutControlItem160";
            this.layoutControlItem160.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem160.TextToControlDistance = 0;
            this.layoutControlItem160.TextVisible = false;
            // 
            // layoutControlItem161
            // 
            this.layoutControlItem161.Control = this.btn病床史删除;
            this.layoutControlItem161.CustomizationFormText = "layoutControlItem161";
            this.layoutControlItem161.Location = new System.Drawing.Point(360, 142);
            this.layoutControlItem161.MaxSize = new System.Drawing.Size(0, 40);
            this.layoutControlItem161.MinSize = new System.Drawing.Size(1, 40);
            this.layoutControlItem161.Name = "layoutControlItem161";
            this.layoutControlItem161.Size = new System.Drawing.Size(135, 40);
            this.layoutControlItem161.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem161.Text = "layoutControlItem161";
            this.layoutControlItem161.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem161.TextToControlDistance = 0;
            this.layoutControlItem161.TextVisible = false;
            // 
            // emptySpaceItem27
            // 
            this.emptySpaceItem27.AllowHotTrack = false;
            this.emptySpaceItem27.CustomizationFormText = "emptySpaceItem27";
            this.emptySpaceItem27.Location = new System.Drawing.Point(489, 0);
            this.emptySpaceItem27.Name = "emptySpaceItem27";
            this.emptySpaceItem27.Size = new System.Drawing.Size(217, 40);
            this.emptySpaceItem27.Text = "emptySpaceItem27";
            this.emptySpaceItem27.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem28
            // 
            this.emptySpaceItem28.AllowHotTrack = false;
            this.emptySpaceItem28.CustomizationFormText = "emptySpaceItem28";
            this.emptySpaceItem28.Location = new System.Drawing.Point(495, 142);
            this.emptySpaceItem28.Name = "emptySpaceItem28";
            this.emptySpaceItem28.Size = new System.Drawing.Size(211, 40);
            this.emptySpaceItem28.Text = "emptySpaceItem28";
            this.emptySpaceItem28.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LCG_主要用药情况
            // 
            this.LCG_主要用药情况.CustomizationFormText = "主要用药情况";
            this.LCG_主要用药情况.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem40,
            this.layoutControlItem116,
            this.layoutControlItem117,
            this.emptySpaceItem29,
            this.lblgc用药情况,
            this.lay其他用药参照,
            this.layoutControlItem5,
            this.layoutControlItem3});
            this.LCG_主要用药情况.Location = new System.Drawing.Point(0, 0);
            this.LCG_主要用药情况.Name = "LCG_主要用药情况";
            this.LCG_主要用药情况.Size = new System.Drawing.Size(706, 308);
            this.LCG_主要用药情况.Text = "主要用药情况";
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem40.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem40.Control = this.radio主要用药情况;
            this.layoutControlItem40.CustomizationFormText = "主要用药情况";
            this.layoutControlItem40.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem40.MinSize = new System.Drawing.Size(54, 40);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(161, 40);
            this.layoutControlItem40.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem40.Text = "主要用药情况";
            this.layoutControlItem40.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem40.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem40.TextToControlDistance = 0;
            this.layoutControlItem40.TextVisible = false;
            // 
            // layoutControlItem116
            // 
            this.layoutControlItem116.Control = this.btn删除用药情况;
            this.layoutControlItem116.CustomizationFormText = "layoutControlItem116";
            this.layoutControlItem116.Location = new System.Drawing.Point(271, 0);
            this.layoutControlItem116.MaxSize = new System.Drawing.Size(0, 40);
            this.layoutControlItem116.MinSize = new System.Drawing.Size(1, 40);
            this.layoutControlItem116.Name = "layoutControlItem116";
            this.layoutControlItem116.Size = new System.Drawing.Size(110, 40);
            this.layoutControlItem116.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem116.Text = "layoutControlItem116";
            this.layoutControlItem116.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem116.TextToControlDistance = 0;
            this.layoutControlItem116.TextVisible = false;
            // 
            // layoutControlItem117
            // 
            this.layoutControlItem117.Control = this.btn添加用药情况;
            this.layoutControlItem117.CustomizationFormText = "layoutControlItem117";
            this.layoutControlItem117.Location = new System.Drawing.Point(161, 0);
            this.layoutControlItem117.MaxSize = new System.Drawing.Size(0, 40);
            this.layoutControlItem117.MinSize = new System.Drawing.Size(1, 40);
            this.layoutControlItem117.Name = "layoutControlItem117";
            this.layoutControlItem117.Size = new System.Drawing.Size(110, 40);
            this.layoutControlItem117.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem117.Text = "layoutControlItem117";
            this.layoutControlItem117.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem117.TextToControlDistance = 0;
            this.layoutControlItem117.TextVisible = false;
            // 
            // emptySpaceItem29
            // 
            this.emptySpaceItem29.AllowHotTrack = false;
            this.emptySpaceItem29.CustomizationFormText = "emptySpaceItem29";
            this.emptySpaceItem29.Location = new System.Drawing.Point(651, 0);
            this.emptySpaceItem29.Name = "emptySpaceItem29";
            this.emptySpaceItem29.Size = new System.Drawing.Size(55, 40);
            this.emptySpaceItem29.Text = "emptySpaceItem29";
            this.emptySpaceItem29.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lblgc用药情况
            // 
            this.lblgc用药情况.Control = this.gc用药情况;
            this.lblgc用药情况.CustomizationFormText = " ";
            this.lblgc用药情况.Location = new System.Drawing.Point(0, 40);
            this.lblgc用药情况.MaxSize = new System.Drawing.Size(700, 0);
            this.lblgc用药情况.MinSize = new System.Drawing.Size(600, 90);
            this.lblgc用药情况.Name = "lblgc用药情况";
            this.lblgc用药情况.Size = new System.Drawing.Size(706, 134);
            this.lblgc用药情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lblgc用药情况.Text = " ";
            this.lblgc用药情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lblgc用药情况.TextSize = new System.Drawing.Size(0, 0);
            this.lblgc用药情况.TextToControlDistance = 0;
            this.lblgc用药情况.TextVisible = false;
            // 
            // lay其他用药参照
            // 
            this.lay其他用药参照.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lay其他用药参照.AppearanceItemCaption.Options.UseFont = true;
            this.lay其他用药参照.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lay其他用药参照.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lay其他用药参照.Control = this.gc其他用药参照;
            this.lay其他用药参照.CustomizationFormText = "其他用药参照";
            this.lay其他用药参照.Location = new System.Drawing.Point(0, 174);
            this.lay其他用药参照.Name = "lay其他用药参照";
            this.lay其他用药参照.Size = new System.Drawing.Size(706, 134);
            this.lay其他用药参照.Text = "其\n他\n用\n药\n参\n照";
            this.lay其他用药参照.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lay其他用药参照.TextSize = new System.Drawing.Size(50, 14);
            this.lay其他用药参照.TextToControlDistance = 5;
            this.lay其他用药参照.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.sBut_Get其他用药参照;
            this.layoutControlItem5.CustomizationFormText = "获取其他用药参照";
            this.layoutControlItem5.Location = new System.Drawing.Point(381, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 40);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(93, 40);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(135, 40);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "获取其他用药参照";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.sBut_Cut其他用药参照;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(516, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 40);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(93, 40);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(135, 40);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // LCG_接种史
            // 
            this.LCG_接种史.CustomizationFormText = "非免疫规划预防接种史";
            this.LCG_接种史.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem118,
            this.layoutControlItem162,
            this.layoutControlItem163,
            this.emptySpaceItem30,
            this.lblgc接种史});
            this.LCG_接种史.Location = new System.Drawing.Point(0, 0);
            this.LCG_接种史.Name = "LCG_接种史";
            this.LCG_接种史.Size = new System.Drawing.Size(706, 308);
            this.LCG_接种史.Text = "非免疫规划预防接种史";
            // 
            // layoutControlItem118
            // 
            this.layoutControlItem118.Control = this.radio预防接种史;
            this.layoutControlItem118.CustomizationFormText = "layoutControlItem118";
            this.layoutControlItem118.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem118.MinSize = new System.Drawing.Size(54, 40);
            this.layoutControlItem118.Name = "layoutControlItem118";
            this.layoutControlItem118.Size = new System.Drawing.Size(143, 40);
            this.layoutControlItem118.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem118.Text = "layoutControlItem118";
            this.layoutControlItem118.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem118.TextToControlDistance = 0;
            this.layoutControlItem118.TextVisible = false;
            // 
            // layoutControlItem162
            // 
            this.layoutControlItem162.Control = this.btn预防接种史添加;
            this.layoutControlItem162.CustomizationFormText = "layoutControlItem162";
            this.layoutControlItem162.Location = new System.Drawing.Point(143, 0);
            this.layoutControlItem162.MaxSize = new System.Drawing.Size(0, 40);
            this.layoutControlItem162.MinSize = new System.Drawing.Size(1, 40);
            this.layoutControlItem162.Name = "layoutControlItem162";
            this.layoutControlItem162.Size = new System.Drawing.Size(105, 40);
            this.layoutControlItem162.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem162.Text = "layoutControlItem162";
            this.layoutControlItem162.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem162.TextToControlDistance = 0;
            this.layoutControlItem162.TextVisible = false;
            // 
            // layoutControlItem163
            // 
            this.layoutControlItem163.Control = this.btn预防接种史删除;
            this.layoutControlItem163.CustomizationFormText = "layoutControlItem163";
            this.layoutControlItem163.Location = new System.Drawing.Point(248, 0);
            this.layoutControlItem163.MaxSize = new System.Drawing.Size(0, 40);
            this.layoutControlItem163.MinSize = new System.Drawing.Size(1, 40);
            this.layoutControlItem163.Name = "layoutControlItem163";
            this.layoutControlItem163.Size = new System.Drawing.Size(117, 40);
            this.layoutControlItem163.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem163.Text = "layoutControlItem163";
            this.layoutControlItem163.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem163.TextToControlDistance = 0;
            this.layoutControlItem163.TextVisible = false;
            // 
            // emptySpaceItem30
            // 
            this.emptySpaceItem30.AllowHotTrack = false;
            this.emptySpaceItem30.CustomizationFormText = "emptySpaceItem30";
            this.emptySpaceItem30.Location = new System.Drawing.Point(365, 0);
            this.emptySpaceItem30.Name = "emptySpaceItem30";
            this.emptySpaceItem30.Size = new System.Drawing.Size(341, 40);
            this.emptySpaceItem30.Text = "emptySpaceItem30";
            this.emptySpaceItem30.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lblgc接种史
            // 
            this.lblgc接种史.Control = this.gc接种史;
            this.lblgc接种史.CustomizationFormText = " ";
            this.lblgc接种史.Location = new System.Drawing.Point(0, 40);
            this.lblgc接种史.MaxSize = new System.Drawing.Size(700, 0);
            this.lblgc接种史.MinSize = new System.Drawing.Size(199, 90);
            this.lblgc接种史.Name = "lblgc接种史";
            this.lblgc接种史.Size = new System.Drawing.Size(706, 268);
            this.lblgc接种史.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lblgc接种史.Text = " ";
            this.lblgc接种史.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lblgc接种史.TextSize = new System.Drawing.Size(0, 0);
            this.lblgc接种史.TextToControlDistance = 0;
            this.lblgc接种史.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btn上一项;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 381);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 36);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(113, 36);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(368, 36);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btn下一项;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(368, 381);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 36);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(113, 36);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(344, 36);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.flowLayoutPanel1;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(304, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(408, 44);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem_医师签字
            // 
            this.layoutControlItem_医师签字.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem_医师签字.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem_医师签字.Control = this.lookUpEdit_医师签字;
            this.layoutControlItem_医师签字.CustomizationFormText = "医师签字";
            this.layoutControlItem_医师签字.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem_医师签字.Name = "layoutControlItem_医师签字";
            this.layoutControlItem_医师签字.Size = new System.Drawing.Size(304, 44);
            this.layoutControlItem_医师签字.Text = "医师签字";
            this.layoutControlItem_医师签字.TextSize = new System.Drawing.Size(88, 25);
            // 
            // uc健康体检表2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.layoutControl1);
            this.Name = "uc健康体检表2";
            this.Size = new System.Drawing.Size(718, 423);
            this.Load += new System.EventHandler(this.UC健康体检表_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc其他用药参照)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_医师签字.Properties)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc接种史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv接种史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte接种日期.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte接种日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc用药情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv用药情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo用药)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo用法)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo用量)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp服药依从性)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte用药时间.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte用药时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio预防接种史.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc建床史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv建床史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte建床日期.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte建床日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte撤床日期.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte撤床日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio家庭病床史.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio主要用药情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc住院史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv住院史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte入院日期.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte入院日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出院日期.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出院日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio住院史.Properties)).EndInit();
            this.flow脑血管疾病.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_未发现.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_缺血性卒中.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_脑出血.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_蛛网膜下腔出血.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_短暂性脑缺血发作.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脑血管疾病_其他.Properties)).EndInit();
            this.flow其他系统疾病.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_未发现.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_糖尿病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_慢性支气管炎.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_慢性阻塞性肺气肿.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_恶性肿瘤.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_老年性骨关节病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt其他系统疾病_其他.Properties)).EndInit();
            this.flow神经系统疾病.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk神经系统疾病_未发现.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk神经系统疾病_阿尔茨海默症.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk神经系统疾病_帕金森症.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk神经系统疾病_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt神经系统疾病_其他.Properties)).EndInit();
            this.flow眼部疾病.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk眼部疾病_未发现.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk眼部疾病_视网膜出血或渗出.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk眼部疾病_视乳头水肿.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk眼部疾病_白内障.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk眼部疾病_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt眼部疾病_其他.Properties)).EndInit();
            this.flow心脏疾病.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk心脏疾病_未发现.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心脏疾病_心肌梗死.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心脏疾病_心绞痛.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心脏疾病_冠状动脉血运重建.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心脏疾病_充血性心力衰竭.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心脏疾病_心前区疼痛.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心脏疾病_高血压.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心脏疾病_夹层动脉瘤.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心脏疾病_动脉闭塞性疾病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心脏疾病_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chktxt心脏疾病_冠心病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心脏疾病_其他.Properties)).EndInit();
            this.flow肾脏疾病.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_未发现.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_糖尿病肾病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_肾功能衰竭.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_急性肾炎.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_慢性肾炎.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt肾脏疾病_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TCG_Tags)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_现存主要健康问题)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl脑血管疾病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl肾脏疾病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl心脏疾病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl眼部疾病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl其他系统疾病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl神经系统疾病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_住院治疗情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblgc住院史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl住院史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem157)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem159)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblgc家庭病床史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem156)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem160)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem161)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_主要用药情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem116)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem117)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblgc用药情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lay其他用药参照)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_接种史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem118)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem162)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem163)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblgc接种史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_医师签字)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private System.Windows.Forms.FlowLayoutPanel flow脑血管疾病;
        private DevExpress.XtraEditors.CheckEdit chk脑血管疾病_未发现;
        private DevExpress.XtraEditors.CheckEdit chk脑血管疾病_缺血性卒中;
        private DevExpress.XtraEditors.CheckEdit chk脑血管疾病_脑出血;
        private DevExpress.XtraEditors.CheckEdit chk脑血管疾病_蛛网膜下腔出血;
        private DevExpress.XtraEditors.CheckEdit chk脑血管疾病_短暂性脑缺血发作;
        private DevExpress.XtraEditors.CheckEdit chk脑血管疾病_其他;
        private DevExpress.XtraEditors.TextEdit txt脑血管疾病_其他;
        private System.Windows.Forms.FlowLayoutPanel flow眼部疾病;
        private DevExpress.XtraEditors.CheckEdit chk眼部疾病_未发现;
        private DevExpress.XtraEditors.CheckEdit chk眼部疾病_视网膜出血或渗出;
        private DevExpress.XtraEditors.CheckEdit chk眼部疾病_视乳头水肿;
        private DevExpress.XtraEditors.CheckEdit chk眼部疾病_白内障;
        private DevExpress.XtraEditors.CheckEdit chk眼部疾病_其他;
        private DevExpress.XtraEditors.TextEdit txt眼部疾病_其他;
        private System.Windows.Forms.FlowLayoutPanel flow心脏疾病;
        private DevExpress.XtraEditors.CheckEdit chk心脏疾病_未发现;
        private DevExpress.XtraEditors.CheckEdit chk心脏疾病_心肌梗死;
        private DevExpress.XtraEditors.CheckEdit chk心脏疾病_心绞痛;
        private DevExpress.XtraEditors.CheckEdit chk心脏疾病_冠状动脉血运重建;
        private DevExpress.XtraEditors.CheckEdit chk心脏疾病_充血性心力衰竭;
        private DevExpress.XtraEditors.CheckEdit chk心脏疾病_心前区疼痛;
        private System.Windows.Forms.FlowLayoutPanel flow肾脏疾病;
        private DevExpress.XtraEditors.CheckEdit chk肾脏疾病_未发现;
        private DevExpress.XtraEditors.CheckEdit chk肾脏疾病_糖尿病肾病;
        private DevExpress.XtraEditors.CheckEdit chk肾脏疾病_肾功能衰竭;
        private DevExpress.XtraEditors.CheckEdit chk肾脏疾病_急性肾炎;
        private DevExpress.XtraEditors.CheckEdit chk肾脏疾病_慢性肾炎;
        private DevExpress.XtraEditors.CheckEdit chk肾脏疾病_其他;
        private DevExpress.XtraEditors.TextEdit txt肾脏疾病_其他;
        private System.Windows.Forms.FlowLayoutPanel flow其他系统疾病;
        private DevExpress.XtraEditors.TextEdit txt其他系统疾病_其他;
        private System.Windows.Forms.FlowLayoutPanel flow神经系统疾病;
        private DevExpress.XtraEditors.TextEdit txt神经系统疾病_其他;
        private DevExpress.XtraEditors.RadioGroup radio住院史;
        private DevExpress.XtraEditors.SimpleButton btn住院史删除;
        private DevExpress.XtraEditors.SimpleButton btn住院史添加;
        private DevExpress.XtraEditors.SimpleButton btn病床史删除;
        private DevExpress.XtraEditors.SimpleButton btn病床史添加;
        private DevExpress.XtraEditors.RadioGroup radio家庭病床史;
        private DevExpress.XtraEditors.SimpleButton btn预防接种史删除;
        private DevExpress.XtraEditors.SimpleButton btn预防接种史添加;
        private DevExpress.XtraEditors.RadioGroup radio预防接种史;
        private DevExpress.XtraEditors.SimpleButton btn删除用药情况;
        private DevExpress.XtraEditors.SimpleButton btn添加用药情况;
        private DevExpress.XtraEditors.RadioGroup radio主要用药情况;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraGrid.GridControl gc住院史;
        private DevExpress.XtraGrid.Views.Grid.GridView gv住院史;
        private DevExpress.XtraGrid.Columns.GridColumn col入院日期;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit dte入院日期;
        private DevExpress.XtraGrid.Columns.GridColumn col出院日期;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit dte出院日期;
        private DevExpress.XtraGrid.Columns.GridColumn col原因;
        private DevExpress.XtraGrid.Columns.GridColumn col医疗机构名称;
        private DevExpress.XtraGrid.Columns.GridColumn col病案号;
        private DevExpress.XtraGrid.Columns.GridColumn col个人档案编号;
        private DevExpress.XtraGrid.Columns.GridColumn col类型;
        private DevExpress.XtraGrid.GridControl gc建床史;
        private DevExpress.XtraGrid.Views.Grid.GridView gv建床史;
        private DevExpress.XtraGrid.Columns.GridColumn col建床日期;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit dte建床日期;
        private DevExpress.XtraGrid.Columns.GridColumn col撤床日期;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit dte撤床日期;
        private DevExpress.XtraGrid.Columns.GridColumn col原因2;
        private DevExpress.XtraGrid.Columns.GridColumn col医疗机构名称2;
        private DevExpress.XtraGrid.Columns.GridColumn col病案号2;
        private DevExpress.XtraGrid.Columns.GridColumn col个人档案编号2;
        private DevExpress.XtraGrid.Columns.GridColumn col类型2;
        private DevExpress.XtraGrid.GridControl gc用药情况;
        private DevExpress.XtraGrid.Views.Grid.GridView gv用药情况;
        private DevExpress.XtraGrid.Columns.GridColumn col药物名称;
        private DevExpress.XtraGrid.Columns.GridColumn col用法;
        private DevExpress.XtraGrid.Columns.GridColumn col用量;
        private DevExpress.XtraGrid.Columns.GridColumn col用药时间;
        private DevExpress.XtraGrid.Columns.GridColumn col服药依从性;
        private DevExpress.XtraGrid.Columns.GridColumn col个人档案编号3;
        private DevExpress.XtraGrid.Columns.GridColumn col创建时间;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit dte用药时间;
        private DevExpress.XtraGrid.GridControl gc接种史;
        private DevExpress.XtraGrid.Views.Grid.GridView gv接种史;
        private DevExpress.XtraGrid.Columns.GridColumn col接种名称;
        private DevExpress.XtraGrid.Columns.GridColumn col接种日期;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit dte接种日期;
        private DevExpress.XtraGrid.Columns.GridColumn col接种机构;
        private DevExpress.XtraGrid.Columns.GridColumn col个人档案编号4;
        private DevExpress.XtraGrid.Columns.GridColumn col创建日期;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lkp服药依从性;
        private DevExpress.XtraLayout.TabbedControlGroup TCG_Tags;
        private DevExpress.XtraLayout.LayoutControlGroup LCG_住院治疗情况;
        private DevExpress.XtraLayout.LayoutControlGroup LCG_现存主要健康问题;
        private DevExpress.XtraLayout.LayoutControlItem lbl脑血管疾病;
        private DevExpress.XtraLayout.LayoutControlItem lbl肾脏疾病;
        private DevExpress.XtraLayout.LayoutControlItem lbl心脏疾病;
        private DevExpress.XtraLayout.LayoutControlItem lbl眼部疾病;
        private DevExpress.XtraLayout.LayoutControlItem lbl神经系统疾病;
        private DevExpress.XtraLayout.LayoutControlItem lbl其他系统疾病;
        private DevExpress.XtraLayout.LayoutControlGroup LCG_主要用药情况;
        private DevExpress.XtraLayout.LayoutControlGroup LCG_接种史;
        private DevExpress.XtraLayout.LayoutControlItem lblgc住院史;
        private DevExpress.XtraLayout.LayoutControlItem lbl住院史;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem157;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem159;
        private DevExpress.XtraLayout.LayoutControlItem lblgc家庭病床史;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem156;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem160;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem161;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem27;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem116;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem117;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem29;
        private DevExpress.XtraLayout.LayoutControlItem lblgc用药情况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem118;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem162;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem163;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem30;
        private DevExpress.XtraLayout.LayoutControlItem lblgc接种史;
        private DevExpress.XtraEditors.SimpleButton btn下一项;
        private DevExpress.XtraEditors.SimpleButton btn上一项;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox cbo用药;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox cbo用法;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox cbo用量;
        private DevExpress.XtraEditors.CheckEdit chk心脏疾病_高血压;
        private DevExpress.XtraEditors.CheckEdit chk心脏疾病_夹层动脉瘤;
        private DevExpress.XtraEditors.CheckEdit chk心脏疾病_动脉闭塞性疾病;
        private DevExpress.XtraEditors.CheckEdit chk心脏疾病_其他;
        private DevExpress.XtraEditors.TextEdit txt心脏疾病_其他;
        private DevExpress.XtraEditors.CheckEdit chk其他系统疾病_未发现;
        private DevExpress.XtraEditors.CheckEdit chk神经系统疾病_未发现;
        private DevExpress.XtraEditors.CheckEdit chk其他系统疾病_其他;
        private DevExpress.XtraEditors.CheckEdit chk神经系统疾病_其他;
        private DevExpress.XtraEditors.CheckEdit chk其他系统疾病_糖尿病;
        private DevExpress.XtraEditors.CheckEdit chk其他系统疾病_慢性支气管炎;
        private DevExpress.XtraEditors.CheckEdit chk其他系统疾病_慢性阻塞性肺气肿;
        private DevExpress.XtraEditors.CheckEdit chk其他系统疾病_恶性肿瘤;
        private DevExpress.XtraEditors.CheckEdit chk其他系统疾病_老年性骨关节病;
        private DevExpress.XtraEditors.CheckEdit chk神经系统疾病_阿尔茨海默症;
        private DevExpress.XtraEditors.CheckEdit chk神经系统疾病_帕金森症;
        private DevExpress.XtraEditors.CheckEdit chktxt心脏疾病_冠心病;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.SimpleButton sBut_Get参照数据;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_医师签字;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_医师签字;
        private DevExpress.XtraGrid.GridControl gc其他用药参照;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem lay其他用药参照;
        private DevExpress.XtraGrid.Columns.GridColumn col药物名称Ref;
        private DevExpress.XtraGrid.Columns.GridColumn col用法Ref;
        private DevExpress.XtraGrid.Columns.GridColumn col用量Ref;
        private DevExpress.XtraGrid.Columns.GridColumn col创建时间Ref;
        private DevExpress.XtraGrid.Columns.GridColumn col用药类型Ref;
        private DevExpress.XtraEditors.SimpleButton sBut_Get其他用药参照;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.SimpleButton sBut_Cut其他用药参照;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
    }
}