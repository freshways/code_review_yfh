﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO.Ports;
using DevExpress.XtraEditors;
using serialPort.PortClass;

namespace ATOMEHR_LeaveClient
{
    public partial class uc体温COM : UserControl
    {
        private string Connection = Program.ThermometerGun;

        private TextEdit _txtParentBarCode;
        public TextEdit TxtParentBarCode
        {
            get { return _txtParentBarCode; }
            set { _txtParentBarCode = value; }
        }
        User currentUser;

        Port_Temperature PortTe;

        public uc体温COM(Port_Temperature PortTe)
        {
            InitializeComponent();
            currentUser = Program.currentUser;
            this.PortTe = PortTe;
        }

        private void btn保存_Click(object sender, EventArgs e)
        {
            if (funCheck())
            {
                try
                {
                    tb_健康体检Info tb_New = new tb_健康体检Info();
                    tb_New.身份证号 = currentUser.ID;
                    tb_New.出生日期 = currentUser.Birth;
                    tb_New.姓名 = currentUser.Name;
                    tb_New.性别 = currentUser.Gender;

                    tb_New.体温 = Convert.ToDouble(this.txt体温.Text.Trim());
                    if (string.IsNullOrEmpty(this.txt呼吸.Text.Trim())) { MessageBox.Show("请输入呼吸频率！"); return; }
                    tb_New.呼吸 = Convert.ToInt32(this.txt呼吸.Text.Trim());

                    if (tb_健康体检DAL.UpdateSys_体温(tb_New))
                    {
                        MessageBox.Show("保存数据成功！");
                    }
                    _txtParentBarCode.Focus();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private bool funCheck()
        {
            if (string.IsNullOrEmpty(this.txt体温.Text.Trim()))
            {
                this.lbl提示.Text = "未检测到体温值，请检查体温值";
                return false;
            }
            return true;
        }

        private void uc体温_Load(object sender, EventArgs e)
        {
            bool state;
            string message;
            PortTe.OpenPort(out state, out message);
            if (!string.IsNullOrEmpty(message)) lbl提示.Text = message;

            PortTe.evResponse += Binding;
        }

        private void Binding(serialPort.PortClass.Port_Temperature.TeInfo ti)
        {
            this.Invoke(new Action(() =>
            {
                txt体温.Text = ti.dTemperature.ToString();
                lbl提示.Text = ti.strState;
            }));
        }

    }
}
