﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using ATOMEHR_LeaveClient.SqlSugar_Models;
using ATOMEHR_LeaveClient.SqlSugar_DALs;

namespace ATOMEHR_LeaveClient
{
    public partial class uc老年人随访 : UserControl
    {

        #region Fields
        string _id;//表的主键，通过id来查询一条数据
        #endregion

        public uc老年人随访()
        {
            InitializeComponent();
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            tb_老年人生活自理能力评价Info tb = tb_老年人生活自理能力评价DAL.GetInfoById(Program.currentUser.ID);
            if (null != tb)
            {
                BindSuiFangData(tb);
            }
        }

        private void BindSuiFangData(tb_老年人生活自理能力评价Info dataRow)
        {
            if (dataRow == null) return;
            if (!string.IsNullOrEmpty(dataRow.进餐评分))
            {
                string 进餐评分 = dataRow.进餐评分;
                this.result1.Text = 进餐评分;
                switch (进餐评分)
                {
                    case "0":
                        this.chk进餐11.Checked = true;
                        break;
                    case "3":
                        this.chk进餐12.Checked = true;
                        break;
                    case "5":
                        this.chk进餐13.Checked = true;
                        break;
                    default:
                        break;
                }
            }
            if (!string.IsNullOrEmpty(dataRow.梳洗评分))
            {
                string 梳洗评分 = dataRow.梳洗评分;
                this.result2.Text = 梳洗评分;
                switch (梳洗评分)
                {
                    case "0":
                        this.chk梳洗21.Checked = true;
                        break;
                    case "1":
                        this.chk梳洗22.Checked = true;
                        break;
                    case "3":
                        this.chk梳洗23.Checked = true;
                        break;
                    case "7":
                        this.chk梳洗24.Checked = true;
                        break;
                    default:
                        break;
                }
            }
            if (!string.IsNullOrEmpty(dataRow.穿衣评分))
            {
                string 穿衣评分 = dataRow.穿衣评分;
                this.result3.Text = 穿衣评分;
                switch (穿衣评分)
                {
                    case "0":
                        this.chk穿衣31.Checked = true;
                        break;
                    case "3":
                        this.chk穿衣32.Checked = true;
                        break;
                    case "7":
                        this.chk穿衣33.Checked = true;
                        break;
                    default:
                        break;
                }
            }
            if (!string.IsNullOrEmpty(dataRow.如厕评分))
            {
                string 如厕评分 = dataRow.如厕评分;
                this.result4.Text = 如厕评分;
                switch (如厕评分)
                {
                    case "0":
                        this.chk如厕41.Checked = true;
                        break;
                    case "1":
                        this.chk如厕42.Checked = true;
                        break;
                    case "5":
                        this.chk如厕43.Checked = true;
                        break;
                    case "10":
                        this.chk如厕44.Checked = true;
                        break;
                    default:
                        break;
                }
            }
            if (!string.IsNullOrEmpty(dataRow.活动评分))
            {
                string 活动评分 = dataRow.活动评分;
                this.result5.Text = 活动评分;
                switch (活动评分)
                {
                    case "0":
                        this.chk活动51.Checked = true;
                        break;
                    case "1":
                        this.chk活动52.Checked = true;
                        break;
                    case "5":
                        this.chk活动53.Checked = true;
                        break;
                    case "10":
                        this.chk活动54.Checked = true;
                        break;
                    default:
                        break;
                }
            }

            this.result.Text = string.IsNullOrEmpty(dataRow.总评分.ToString()) ? " " : dataRow.总评分.ToString();

            this.txt下次随访目标.Text = dataRow.下次随访目标.ToString();
            // this.dte随访日期.Text = dataRow.随访日期.ToString();
            this.txt随访医生签名.Text = dataRow.随访医生.ToString();
            //this.dte下次随访日期.Text = dataRow.下次随访日期.ToString();

            this.dte随访日期.Text = DateTime.Now.ToString("yyyy-MM-dd");
            this.dte下次随访日期.Text = DateTime.Now.AddYears(1).ToString("yyyy-MM-dd");

        }
        private void UC老年人生活自理能力评估表_Load(object sender, EventArgs e)
        {
            this.dte随访日期.Text = DateTime.Now.ToString("yyyy-MM-dd");
            this.dte下次随访日期.Text = DateTime.Now.AddYears(1).ToString("yyyy-MM-dd");
            this.txt随访医生签名.Text = Program._bDoctor;
        }
        private void result_TextChanged(object sender, EventArgs e)
        {
            int result1 = string.IsNullOrEmpty(this.result1.Text.Trim()) ? 0 : Convert.ToInt32(this.result1.Text.Trim());
            int result2 = string.IsNullOrEmpty(this.result2.Text.Trim()) ? 0 : Convert.ToInt32(this.result2.Text.Trim());
            int result3 = string.IsNullOrEmpty(this.result3.Text.Trim()) ? 0 : Convert.ToInt32(this.result3.Text.Trim());
            int result4 = string.IsNullOrEmpty(this.result4.Text.Trim()) ? 0 : Convert.ToInt32(this.result4.Text.Trim());
            int result5 = string.IsNullOrEmpty(this.result5.Text.Trim()) ? 0 : Convert.ToInt32(this.result5.Text.Trim());

            this.result.Text = Convert.ToString(result1 + result2 + result3 + result4 + result5);
        }
        private void btn保存_Click(object sender, EventArgs e)
        {
            if (check验证())
            {
                //string happentime = this.dte随访日期.Text.Trim();
                ////判断此随访时间 是否已经添加  随访
                //if (!_Bll.isExists(_docNo, happentime))
                //{
                //获取随访次数
                //int sfcs = _Bll.getbndsfcs(_docNo, happentime.Substring(0, 4));
                //if (sfcs == 0) sfcs = 1;
                //else sfcs += 1;
                #region tb_老年人基本信息
                //TODO:上传时更新 老年人下次随访时间 字段
                //_ds老年人随访.Tables[tb_老年人基本信息.__TableName].Rows[0][tb_老年人基本信息.下次随访时间] = this.dte下次随访日期.Text;
                #endregion
                #region tb_老年人随访
                tb_老年人生活自理能力评价Info tb = new tb_老年人生活自理能力评价Info();
                int i = Get缺项();
                int j = Get完整度(i);
                //DataRow row = _ds老年人随访.Tables[tb_老年人随访.__TableName].Rows.Add();
                tb.身份证号 = Program.currentUser.ID;
                tb.姓名 = Program.currentUser.Name;
                tb.性别 = Program.currentUser.Gender;
                tb.出生日期 = Program.currentUser.Birth;
                tb.居住地址 = Program.currentUser.Addr;
                tb.个人档案编号 = Program.currentUser.DocNo;
                tb.进餐评分 = this.result1.Text.Trim();
                tb.梳洗评分 = this.result2.Text.Trim();
                tb.穿衣评分 = this.result3.Text.Trim();
                tb.如厕评分 = this.result4.Text.Trim();
                tb.活动评分 = this.result5.Text.Trim();
                tb.总评分 = this.result.Text.Trim();
                tb.下次随访目标 = this.txt下次随访目标.Text.Trim();
                tb.随访日期 = this.dte随访日期.Text;
                tb.随访医生 = this.txt随访医生签名.Text.Trim();
                tb.下次随访日期 = this.dte下次随访日期.Text.Trim();
                tb.创建时间 = Program._currentTime;
                //tb.随访次数 = sfcs;
                tb.缺项 = i.ToString();
                tb.完整度 = j.ToString();
                //tb.所属机构 = Loginer.CurrentUser.所属机构;
                //tb.创建机构 = Loginer.CurrentUser.所属机构;
                //tb.创建人 = Loginer.CurrentUser.用户编码;
                //tb.更新人 = Loginer.CurrentUser.用户编码;
                #endregion
                #region tb_健康档案_个人健康特征
                //TODO:上传时更新 个人健康特征字段
                //_ds老年人随访.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.老年人随访] = i + "," + j;
                #endregion
                //}
                //else
                //{
                //    Msg.Warning("对不起，随访时间为“" + this.dte随访日期.Text.Trim() + "”的随访已经创建，请修改随访时间！");
                //    return;
                //}
                string sqlWhere = " 身份证号 = '" + tb.身份证号 + "' and substr(创建时间,0,10) = substr('" + Program._currentTime + "',0,10)";
                List<tb_老年人生活自理能力评价Info> list = tb_老年人生活自理能力评价DAL.GetInfoList(sqlWhere);
                bool result = false;
                if (list.Count == 1)
                {
                    result = tb_老年人生活自理能力评价DAL.UpdateInfo(tb);
                }
                else
                {
                    result = tb_老年人生活自理能力评价DAL.Save(tb);
                }
                if (result)
                {
                    DBManager.Beep();
                    MessageBox.Show("保存数据成功！");
                }
            }

        }


        private int Get缺项()
        {
            int i = 0;
            if (this.result1.Text.Trim() == "") i++;
            if (this.result2.Text.Trim() == "") i++;
            if (this.result3.Text.Trim() == "") i++;
            if (this.result4.Text.Trim() == "") i++;
            if (this.result5.Text.Trim() == "") i++;
            if (this.txt下次随访目标.Text.Trim() == "") i++;
            if (this.dte随访日期.Text.Trim() == "") i++;
            if (this.dte下次随访日期.Text.Trim() == "") i++;
            if (this.txt随访医生签名.Text.Trim() == "") i++;
            return i;
        }
        public int Get完整度(int i)
        {
            int k = Convert.ToInt32((9 - i) * 100 / 9.0);
            return k;
        }
        private bool check验证()
        {
            if (string.IsNullOrEmpty(this.dte随访日期.Text.Trim()))
            {
                MessageBox.Show("随访日期是必填项！");
                //Msg.Warning("随访日期是必填项！");
                this.dte随访日期.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(this.dte下次随访日期.Text.Trim()))
            {
                MessageBox.Show("下次随访日期是必填项！");
                //Msg.Warning("下次随访日期是必填项！");
                this.dte下次随访日期.Focus();
                return false;
            }
            if (this.dte下次随访日期.DateTime <= this.dte随访日期.DateTime)
            {
                MessageBox.Show("下次随访日期填写不能小于本次随访日期！");
                //Msg.Warning("下次随访日期填写不能小于本次随访日期！");
                this.dte下次随访日期.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(result1.Text.Trim()) || string.IsNullOrEmpty(result2.Text.Trim()) || string.IsNullOrEmpty(result3.Text.Trim()) || string.IsNullOrEmpty(result4.Text.Trim()) || string.IsNullOrEmpty(result5.Text.Trim()))
            {
                MessageBox.Show("请将五个评估项全部选择！");
                //Msg.Warning("请将五个评估项全部选择！");
                return false;
            }
            return true;
        }
        private void chk_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit chk = (CheckEdit)sender;
            string name = chk.Name;//chk11
            string result = "result";//result
            if (chk.Checked)
            {
                int value = Convert.ToInt32(chk.Tag);
                result += name.Substring(5, 1);
                Control ctrl = this.Controls.Find(result, true)[0];//根据name找到控件
                if (ctrl is LabelControl)
                {
                    ctrl.Text = value.ToString();
                }
                int index = this.tabbedControlGroup1.SelectedTabPageIndex;
                if (index == this.tabbedControlGroup1.TabPages.Count) return;
                this.tabbedControlGroup1.SelectedTabPageIndex = index + 1;
            }
        }

        private void btn上一页_Click(object sender, EventArgs e)
        {
            int index = this.tabbedControlGroup1.SelectedTabPageIndex;
            if (index == 0) return;
            this.tabbedControlGroup1.SelectedTabPageIndex = index - 1;
        }

        private void btn下一页_Click(object sender, EventArgs e)
        {
            int index = this.tabbedControlGroup1.SelectedTabPageIndex;
            if (index == this.tabbedControlGroup1.TabPages.Count) return;
            this.tabbedControlGroup1.SelectedTabPageIndex = index + 1;
        }

        //Begin WXF 2019-02-19 | 17:18
        //设置老年人自理评估参照数据 
        private void sBut_Get参照数据_Click(object sender, EventArgs e)
        {
            sBut_Get参照数据.Enabled = false;

            dal_老年人生活自理能力评估_Reference dal自理评估Ref = new dal_老年人生活自理能力评估_Reference();
            tb_老年人生活自理能力评估_Reference tb自理评估Ref = new tb_老年人生活自理能力评估_Reference();

            tb自理评估Ref = dal自理评估Ref.GetOneRow(Program.currentUser.DocNo);

            if (tb自理评估Ref != null)
            {
                tb_老年人生活自理能力评价Info tb = new tb_老年人生活自理能力评价Info();
                tb.个人档案编号 = tb自理评估Ref.个人档案编号;
                tb.进餐评分 = tb自理评估Ref.进餐评分;
                tb.梳洗评分 = tb自理评估Ref.梳洗评分;
                tb.穿衣评分 = tb自理评估Ref.穿衣评分;
                tb.如厕评分 = tb自理评估Ref.如厕评分;
                tb.活动评分 = tb自理评估Ref.活动评分;
                tb.总评分 = tb自理评估Ref.总评分;
                tb.随访次数 = tb自理评估Ref.随访次数;
                tb.下次随访目标 = tb自理评估Ref.下次随访目标;
                tb.下次随访日期 = tb自理评估Ref.下次随访日期;
                tb.随访医生 = tb自理评估Ref.随访医生;
                tb.创建机构 = tb自理评估Ref.创建机构;
                tb.创建人 = tb自理评估Ref.创建人;
                tb.更新人 = tb自理评估Ref.更新人;
                tb.所属机构 = tb自理评估Ref.所属机构;
                tb.创建时间 = tb自理评估Ref.创建时间;
                tb.更新时间 = tb自理评估Ref.更新时间;
                tb.随访日期 = tb自理评估Ref.随访日期;

                BindSuiFangData(tb);
            }
            else
            {
                MessageBox.Show("此人无去年数据做参照!");
            }

            sBut_Get参照数据.Enabled = true;
        }
        //End

    }
}
