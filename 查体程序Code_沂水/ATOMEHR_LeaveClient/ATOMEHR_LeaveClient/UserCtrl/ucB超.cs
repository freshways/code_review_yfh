﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using DevExpress.XtraEditors;
using ATOMEHR_LeaveClient.SqlSugar_DALs;
using ATOMEHR_LeaveClient.SqlSugar_Models;

namespace ATOMEHR_LeaveClient
{
    public partial class ucB超 : Base_UserControl
    {
        private TextEdit _txtParentBarCode;
        public TextEdit TxtParentBarCode
        {
            get { return _txtParentBarCode; }
            set { _txtParentBarCode = value; }
        }
        #region 变量
        #endregion
        public ucB超()
        {
            InitializeComponent();
        }

        private void ucB超_Load(object sender, EventArgs e)
        {
            Binding_医师签字();
            Bind_B超检查部位();
            Bind_B超报告模版();
        }

        dal_B超报告模版_Lite dal = new dal_B超报告模版_Lite();
        Dictionary<string, string> dic_B超模板 = new Dictionary<string, string>();
        private void Bind_B超检查部位()
        {
            List<tb_B超报告模版> ls_检查部位 = dal.Get检查部位();

            List<string> lsStr = new List<string>();
            foreach (tb_B超报告模版 item in ls_检查部位)
            {
                lsStr.Add(item.检查部位);
            }

            if (lsStr.Count > 0) cbe_检查部位.Properties.Items.AddRange(lsStr);
        }

        private void Bind_B超报告模版()
        {
            List<tb_B超报告模版> ls_模板 = new List<tb_B超报告模版>();
            if (!string.IsNullOrEmpty(cbe_检查部位.Text))
            {
                ls_模板 = dal.GetB超报告模版(cbe_检查部位.Text);
            }

            if (dic_B超模板 != null && dic_B超模板.Count > 0) dic_B超模板.Clear();

            if (ls_模板 != null && ls_模板.Count > 0)
            {
                List<string> ls_超声提示 = new List<string>();
                foreach (tb_B超报告模版 item in ls_模板)
                {
                    if (!ls_超声提示.Contains(item.超声提示.Trim()))
                    {
                        ls_超声提示.Add(item.超声提示.Trim());
                        dic_B超模板.Add(item.超声提示.Trim(), item.超声所见.Trim());
                    }
                }

                if (listBox_超声提示模版.Items.Count > 0) listBox_超声提示模版.Items.Clear();
                if (ls_超声提示 != null && ls_超声提示.Count > 0) listBox_超声提示模版.Items.AddRange(ls_超声提示.ToArray());
            }
        }
        //Begin WXF 2018-11-21 | 15:57
        //绑定医师签字 
        private void Binding_医师签字()
        {
            dal_医生信息 dal医生信息 = new dal_医生信息();
            DataTable dt = dal医生信息.Get医生信息ToLookUp();

            lookUpEdit_医师签字.Properties.ValueMember = "b编码";
            lookUpEdit_医师签字.Properties.DisplayMember = "x医生姓名";
            lookUpEdit_医师签字.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("x医生姓名", "医生姓名"));
            lookUpEdit_医师签字.Properties.DataSource = dt;
            lookUpEdit_医师签字.EditValue = "请选择";
        }
        //End

        private void btn保存_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(this.txtHRValue.Text.Trim()))
                {
                    tb_健康体检Info tb_New = new tb_健康体检Info();
                    tb_健康体检_B超 tb_B超 = new tb_健康体检_B超();
                    string Bchao = "";
                    string yichang = string.Empty;
                    if (this.txtHRValue.Text.Trim() == "正常")
                    {
                        Bchao = "1";
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(this.txtResultText.Text.Trim()))
                        {
                            MessageBox.Show("请填写异常后进行保存！");
                            return;
                        }
                        Bchao = "2";
                        yichang = txtResultText.Text.Trim();
                    }
                    tb_New.B超 = Bchao;
                    tb_New.B超其他 = yichang;

                    //Begin WXF 2018-11-21 | 16:52
                    //保存医师签字 
                    if (lookUpEdit_医师签字.Text.Equals("请选择"))
                    {
                        MessageBox.Show("请选择B超的责任医师!");
                        return;
                    }
                    else
                    {
                        tb_New.医师签字 = tb_健康体检DAL.JsonToStr_医师签字(Program.currentUser.DocNo, lookUpEdit_医师签字.EditValue.ToString(), tb_健康体检DAL.Module_体检.B超);
                    }
                    //End


                    //Begin WXF 2019-04-22 | 15:25
                    //保存B超报告

                    if (string.IsNullOrEmpty(memoEdit_超声所见.Text ) || string.IsNullOrEmpty(memoEdit_超声提示.Text))
                    {
                        MessageBox.Show("请填写超声所见和超声提示!");
                        return;
                    }
                    else
                    {
                        tb_B超.超声所见 = memoEdit_超声所见.Text;
                        tb_B超.超声提示 = memoEdit_超声提示.Text;
                    }

                    //End


                    if (tb_健康体检DAL.UpdateSys_B超(tb_New) && tb_健康体检DAL.UpdateSys_B超报告(tb_B超))
                    {
                        MessageBox.Show("保存成功！");
                        // add by lid ---恢复B超默认值，并让连续扫描获得焦点--start
                        this.txtHRValue.Text = "正常";
                        this.txtResultValue.Text = "";
                        _txtParentBarCode.Focus();
                        // add by lid-----end
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtHRValue_SelectedIndexChanged(object sender, EventArgs e)
        {
            string result = this.txtHRValue.Text.Trim();
            if ("正常".Equals(result))
            {
                this.txtResultValue.Enabled = false;
                this.txtResultValue.SelectedIndex = -1;
                this.txtResultText.Text = string.Empty;
            }
            else
            {
                this.txtResultValue.Enabled = true;
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            this.txtHRValue.SelectedIndex = 0;
            this.txtResultValue.SelectedIndex = -1;
            this.txtResultText.Text = string.Empty;

            memoEdit_超声所见.Text = string.Empty;
            memoEdit_超声提示.Text = string.Empty;
        }

        private void txtResultValue_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selectedIdx = this.txtHRValue.SelectedIndex;
            if (selectedIdx > 0)
            {
                this.txtResultText.Text = this.txtResultValue.SelectedItem.ToString();
            }
            else
            {
                this.txtResultText.Text = string.Empty;
            }

        }

        private void listBox_超声提示模版_DoubleClick(object sender, EventArgs e)
        {
            string str超声提示 = listBox_超声提示模版.SelectedItem.ToString();

            if (string.IsNullOrEmpty(memoEdit_超声提示.Text))
            {
                memoEdit_超声提示.Text = str超声提示;
            }
            else
            {
                memoEdit_超声提示.Text += "\r\n" + str超声提示;
            }

            if (string.IsNullOrEmpty(memoEdit_超声所见.Text))
            {
                memoEdit_超声所见.Text = dic_B超模板[str超声提示];
            }
            else
            {
                memoEdit_超声所见.Text += "\r\n" + dic_B超模板[str超声提示];
            }
        }

        private void cbe_检查部位_EditValueChanged(object sender, EventArgs e)
        {
            Bind_B超报告模版();
        }
    }
}
