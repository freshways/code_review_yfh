﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.Text.RegularExpressions;
using DevExpress.XtraEditors;
using ATOMEHR_LeaveClient.SqlSugar_DALs;

namespace ATOMEHR_LeaveClient
{
    public partial class uc血压 : ATOMEHR_LeaveClient.Base_UserControl
    {
        private TextEdit _txtParentBarCode;
        public TextEdit TxtParentBarCode
        {
            get { return _txtParentBarCode; }
            set { _txtParentBarCode = value; }
        }
        public uc血压()
        {
            InitializeComponent();
        }

        public uc血压(SerialPort spt)
        {
            InitializeComponent();
            comm = spt;
        }

        IntPtr hBluetooth;
        int nCOMID = 0; //端口

        private SerialPort comm = new SerialPort();
        private long received_count = 0;//接收计数
        private long send_count = 0;//发送计数
        private bool Listening = false;//是否没有执行完invoke相关操作
        private bool Closing = false;//是否正在关闭串口，执行Application.DoEvents，并阻止再次invoke
        private List<byte> buffer = new List<byte>(4096);//默认分配1页内存，并始终限制不允许超过
        private byte[] binary_data_1 = new byte[9];

        private void uc血压_Load(object sender, EventArgs e)
        {
            //获取蓝牙操作句柄
            hBluetooth = EcgHelper.BluetoothOperationOpen();
            try
            {
                EcgHelper.BluetoothOperationConnect(hBluetooth, Program.BlueAddr, ref nCOMID);
                //EcgHelper.BluetoothOperationConnect(hBluetooth, "(88:1B:99:03:2A:E5)", ref nCOMID);
                //labelCom.Text += nCOMID.ToString();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("未与设备进行配对！请先初始化" + ex.Message);
                return;
            }
            //添加事件注册
            comm.DataReceived += comm_DataReceived;

            //关闭时点击，则设置好端口，波特率后打开
            comm.PortName = string.Format("COM{0}", nCOMID);
            comm.BaudRate = 9600;
            try
            {
                comm.Open();
            }
            catch (Exception ex)
            {
                //捕获到异常信息，创建一个新的comm对象，之前的不能用了。
                comm = new SerialPort();
                //现实异常信息给客户。
                MessageBox.Show(ex.Message);
            }

            Binding_医师签字();
        }

        //Begin WXF 2018-11-21 | 15:57
        //绑定医师签字 
        private void Binding_医师签字()
        {
            dal_医生信息 dal医生信息 = new dal_医生信息();
            DataTable dt = dal医生信息.Get医生信息ToLookUp();

            lookUpEdit_医师签字.Properties.ValueMember = "b编码";
            lookUpEdit_医师签字.Properties.DisplayMember = "x医生姓名";
            lookUpEdit_医师签字.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("x医生姓名", "医生姓名"));
            lookUpEdit_医师签字.Properties.DataSource = dt;
            lookUpEdit_医师签字.EditValue = "请选择";
        }
        //End

        void comm_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (Closing) return;//如果正在关闭，忽略操作，直接返回，尽快的完成串口监听线程的一次循环
            try
            {
                Listening = true;//设置标记，说明我已经开始处理数据，一会儿要使用系统UI的。
                int n = comm.BytesToRead;//先记录下来，避免某种原因，人为的原因，操作几次之间时间长，缓存不一致
                byte[] buf = new byte[n];//声明一个临时数组存储当前来的串口数据
                received_count += n;//增加接收计数
                comm.Read(buf, 0, n);//读取缓冲数据

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //<协议解析>
                bool data_1_catched = false;//缓存记录数据是否捕获到
                //1.缓存数据
                buffer.AddRange(buf);
                //2.完整性判断
                while (buffer.Count > 4)//至少要包含头（2字节）+长度（1字节）+校验（1字节）
                {
                    //请不要担心使用>=，因为>=已经和>,<,=一样，是独立操作符，并不是解析成>和=2个符号
                    //2.1 查找数据头
                    if (buffer[0] == 0xFD && buffer[1] == 0xFD)
                    {
                        //2.2 血压设备的第三位是分类码 
                        //关于指令
                        //发送指令：FA是开始指令、FE关机指令
                        //接收指令：FB是测过程中的压力信号、FC测试结果、FD异常
                        if (buffer[2] == 0x06)//开始测量
                        {
                            if (buffer.Count < 5) break;//数据包位数不够的时候什么都不做
                            //更新界面
                            this.Invoke((EventHandler)(delegate { labelState.Text = "开始测量"; }));
                            buffer.RemoveRange(0, 5);//从缓存中移除数据。固定5位
                            continue;
                        }
                        else if (buffer[2] == 0xFB)//遇到测试过程中发来的压力信号直接跳过进行下一条
                        {
                            if (buffer.Count < 7) break;
                            //我们的数据都是定好格式的，所以当我们找到分析出的数据1，就知道固定位置一定是这些数据，我们只要显示就可以了
                            string data = (Convert.ToInt32(buffer[3].ToString("X2"), 16) * 256 + Convert.ToInt32(buffer[4].ToString("X2"), 16)).ToString();
                            //更新界面
                            this.Invoke((EventHandler)(delegate { labelPressure.Text = data; }));

                            buffer.RemoveRange(0, 7);//从缓存中移除数据。固定7位
                            continue;//继续下一次循环
                        }
                        else if (buffer[2] == 0xFC)//遇到测试结果直接跳出，并进行数据位校验
                        {
                            if (buffer.Count < 8) break;
                            //至此，已经被找到了一条完整数据。我们将数据直接分析，或是缓存起来一起分析
                            //我们这里采用的办法是缓存一次，好处就是如果你某种原因，数据堆积在缓存buffer中
                            //已经很多了，那你需要循环的找到最后一组，只分析最新数据，过往数据你已经处理不及时
                            //了，就不要浪费更多时间了，这也是考虑到系统负载能够降低。
                            buffer.CopyTo(0, binary_data_1, 0, 8);//复制一条完整数据到具体的数据缓存
                            data_1_catched = true;
                            buffer.RemoveRange(0, 8);//正确分析一条数据，从缓存中移除数据。固定8位
                            continue;//继续下一次循环
                        }
                        else if (buffer[2] == 0xFD)//遇到异常直接跳出
                        {
                            if (buffer.Count < 6) break;//
                            //错误数据分析过程
                            //我们的数据都是定好格式的，所以当我们找到分析出的数据1，就知道固定位置一定是这些数据，我们只要显示就可以了
                            string data = buffer[3].ToString("X2");
                            //更新界面
                            this.Invoke((EventHandler)(delegate { labelState.Text = data; }));
                            //
                            buffer.RemoveRange(0, 6);//分析完错误数据后，从缓存中移除数据。固定6位
                            continue;
                        }
                        else
                        {
                            //不在协议内的数据，删之
                            buffer.RemoveRange(0, buffer.Count);
                            break;
                        }
                    }
                    else
                    {
                        //这里是很重要的，如果数据开始不是头，则删除数据
                        buffer.RemoveAt(0);
                    }
                }
                //分析数据
                if (data_1_catched)
                {
                    //我们的数据都是定好格式的，所以当我们找到分析出的数据1，就知道固定位置一定是这些数据，我们只要显示就可以了
                    //更新界面
                    this.Invoke((EventHandler)(delegate
                    {
                        int i收缩压 = Convert.ToInt32(binary_data_1[3].ToString("X2"), 16);
                        int i舒张压 = Convert.ToInt32(binary_data_1[4].ToString("X2"), 16);
                        textBox收缩压.Text = (i收缩压 % 2 == 0 ? i收缩压 : i收缩压 - 1).ToString();//判断奇偶数，遇到奇数-1
                        textBox舒张压.Text = (i舒张压 % 2 == 0 ? i舒张压 : i舒张压 - 1).ToString();
                        textBox心率.Text = (Convert.ToInt32(binary_data_1[5].ToString("X2"), 16)).ToString();
                    }));

                }
                //如果需要别的协议，只要扩展这个data_n_catched就可以了。往往我们协议多的情况下，还会包含数据编号，给来的数据进行
                //编号，协议优化后就是： 头+编号+长度+数据+校验
                //</协议解析>
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////

            }
            finally
            {
                Listening = false;//我用完了，ui可以关闭串口了。
            }
        }

        private void btnBegin_Click(object sender, EventArgs e)
        {
            //定义一个变量，记录发送了几个字节
            int n = 0;
            //16进制发送
            if (true)
            {
                string SendString = "FD-FD-FA-05-0D-0A".Replace("-", "");//txSend.Text.Replace("-", "");
                //我们不管规则了。如果写错了一些，我们允许的，只用正则得到有效的十六进制数
                MatchCollection mc = Regex.Matches(SendString, @"(?i)[\da-f]{2}");
                List<byte> buf = new List<byte>();//填充到这个临时列表中
                //依次添加到列表中
                foreach (Match m in mc)
                {
                    //buf.Add(byte.Parse(m.Value));
                    buf.Add(Convert.ToByte(m.Value, 16));
                }
                if (comm.IsOpen)
                {//转换列表为数组后发送
                    comm.Write(buf.ToArray(), 0, buf.Count);
                    //记录发送的字节数
                }
                n = buf.Count;
            }

        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                tb_健康体检Info tb_new = new tb_健康体检Info();
                if (string.IsNullOrEmpty(this.textBox舒张压.Text) || string.IsNullOrEmpty(this.textBox收缩压.Text) || string.IsNullOrEmpty(this.textBox心率.Text))
                {
                    MessageBox.Show("不存在血压值，请重新测量！");
                    return;
                }
                tb_new.血压右侧1 = this.textBox收缩压.Text.Trim();
                tb_new.血压右侧2 = this.textBox舒张压.Text.Trim();

                // 随机2/4/6
                Random rd = new Random();
                int x = rd.Next(2, 7) / 2 * 2;
                int y = rd.Next(2, 7) / 2 * 2;

                tb_new.血压左侧1 = (Convert.ToInt32(this.textBox收缩压.Text.Trim()) - x).ToString();
                tb_new.血压左侧2 = (Convert.ToInt32(this.textBox舒张压.Text.Trim()) - y).ToString();
                
                //Begin WXF 2018-10-18 | 17:57
                //泉庄要求心率脉搏从心电结果中取 
                //tb_new.心率 = Convert.ToInt32(this.textBox心率.Text);
                //tb_new.脉搏 = Convert.ToInt32(this.textBox心率.Text);
                //End

                //Begin WXF 2018-11-21 | 16:52
                //保存医师签字 
                if (lookUpEdit_医师签字.Text.Equals("请选择"))
                {
                    //MessageBox.Show("请选择血压的责任医师!");
                    //return;
                }
                else
                {
                    tb_new.医师签字 = tb_健康体检DAL.JsonToStr_医师签字(Program.currentUser.DocNo, lookUpEdit_医师签字.EditValue.ToString(), tb_健康体检DAL.Module_体检.血压);
                }
                //End
 
                if (tb_健康体检DAL.UpdateSys_血压(tb_new))
                {
                    MessageBox.Show("保存数据成功！");
                }
                _txtParentBarCode.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}


