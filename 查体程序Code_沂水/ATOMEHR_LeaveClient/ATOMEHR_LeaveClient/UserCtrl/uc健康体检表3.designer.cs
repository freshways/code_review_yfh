﻿using AtomEHR.Library.UserControls;
namespace ATOMEHR_LeaveClient
{
    partial class uc健康体检表3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc健康体检表3));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btn下一项 = new DevExpress.XtraEditors.SimpleButton();
            this.btn上一项 = new DevExpress.XtraEditors.SimpleButton();
            this.txt齿列_其他 = new DevExpress.XtraEditors.TextEdit();
            this.txt义齿4 = new DevExpress.XtraEditors.TextEdit();
            this.txt义齿3 = new DevExpress.XtraEditors.TextEdit();
            this.txt义齿2 = new DevExpress.XtraEditors.TextEdit();
            this.txt义齿1 = new DevExpress.XtraEditors.TextEdit();
            this.txt龋齿4 = new DevExpress.XtraEditors.TextEdit();
            this.txt龋齿3 = new DevExpress.XtraEditors.TextEdit();
            this.txt龋齿2 = new DevExpress.XtraEditors.TextEdit();
            this.txt龋齿1 = new DevExpress.XtraEditors.TextEdit();
            this.txt缺齿4 = new DevExpress.XtraEditors.TextEdit();
            this.txt缺齿2 = new DevExpress.XtraEditors.TextEdit();
            this.txt缺齿3 = new DevExpress.XtraEditors.TextEdit();
            this.txt缺齿1 = new DevExpress.XtraEditors.TextEdit();
            this.chk齿列_义齿 = new DevExpress.XtraEditors.CheckEdit();
            this.chk齿列_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.chk齿列_龋齿 = new DevExpress.XtraEditors.CheckEdit();
            this.chk齿列_缺齿 = new DevExpress.XtraEditors.CheckEdit();
            this.chk齿列_正常 = new DevExpress.XtraEditors.CheckEdit();
            this.radio足背动脉搏动 = new DevExpress.XtraEditors.RadioGroup();
            this.radio下肢水肿 = new DevExpress.XtraEditors.RadioGroup();
            this.radio心律 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel20 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio移动性浊音 = new DevExpress.XtraEditors.RadioGroup();
            this.txt移动性浊音 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel19 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio脾大 = new DevExpress.XtraEditors.RadioGroup();
            this.txt脾大 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel18 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio肝大 = new DevExpress.XtraEditors.RadioGroup();
            this.txt肝大 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel17 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio包块 = new DevExpress.XtraEditors.RadioGroup();
            this.txt包块 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel16 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio压痛 = new DevExpress.XtraEditors.RadioGroup();
            this.txt压痛 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel15 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio杂音 = new DevExpress.XtraEditors.RadioGroup();
            this.txt杂音 = new DevExpress.XtraEditors.TextEdit();
            this.txt心率 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.flowLayoutPanel13 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio呼吸音 = new DevExpress.XtraEditors.RadioGroup();
            this.txt呼吸音_异常 = new DevExpress.XtraEditors.TextEdit();
            this.radio桶状胸 = new DevExpress.XtraEditors.RadioGroup();
            this.flow巩膜 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk巩膜_正常 = new DevExpress.XtraEditors.CheckEdit();
            this.chk巩膜_黄染 = new DevExpress.XtraEditors.CheckEdit();
            this.chk巩膜_充血 = new DevExpress.XtraEditors.CheckEdit();
            this.chk巩膜_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt巩膜_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flow皮肤 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk皮肤_正常 = new DevExpress.XtraEditors.CheckEdit();
            this.chk皮肤_潮红 = new DevExpress.XtraEditors.CheckEdit();
            this.chk皮肤_苍白 = new DevExpress.XtraEditors.CheckEdit();
            this.chk皮肤_发绀 = new DevExpress.XtraEditors.CheckEdit();
            this.chk皮肤_黄染 = new DevExpress.XtraEditors.CheckEdit();
            this.chk皮肤_色素沉着 = new DevExpress.XtraEditors.CheckEdit();
            this.chk皮肤_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt皮肤_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flow罗音 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk罗音_无 = new DevExpress.XtraEditors.CheckEdit();
            this.chk罗音_干罗音 = new DevExpress.XtraEditors.CheckEdit();
            this.chk罗音_湿罗音 = new DevExpress.XtraEditors.CheckEdit();
            this.chk罗音_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt罗音_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio眼底 = new DevExpress.XtraEditors.RadioGroup();
            this.txt眼底 = new DevExpress.XtraEditors.TextEdit();
            this.flow听力 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio听力_听见 = new DevExpress.XtraEditors.CheckEdit();
            this.radio听力_听不清 = new DevExpress.XtraEditors.CheckEdit();
            this.flow淋巴结 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk淋巴结_未触及 = new DevExpress.XtraEditors.CheckEdit();
            this.chk淋巴结_锁骨上 = new DevExpress.XtraEditors.CheckEdit();
            this.chk淋巴结_腋窝 = new DevExpress.XtraEditors.CheckEdit();
            this.chk淋巴结_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt淋巴结_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flow运动功能 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio运动能力_可顺利完成 = new DevExpress.XtraEditors.CheckEdit();
            this.radio运动能力_无法完成 = new DevExpress.XtraEditors.CheckEdit();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txt左眼视力 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txt右眼视力 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txt矫正左眼视力 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txt矫正右眼视力 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.flow咽部 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio咽部_无充血 = new DevExpress.XtraEditors.CheckEdit();
            this.radio咽部_充血 = new DevExpress.XtraEditors.CheckEdit();
            this.radio咽部_淋巴滤泡增生 = new DevExpress.XtraEditors.CheckEdit();
            this.radio咽部_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt咽部_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flow口唇 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio口唇 = new DevExpress.XtraEditors.RadioGroup();
            this.txt口唇其他 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.TCG_Tags = new DevExpress.XtraLayout.TabbedControlGroup();
            this.LCG_心脏 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl心率 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem53 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem54 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LCG_口唇咽部 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl咽部 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LCG_齿列 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem24 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem140 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem143 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem145 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem144 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem146 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem147 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem141 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem148 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem150 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem151 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem149 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem152 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem154 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem155 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem153 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem142 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem158 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LCG_视听 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl视力 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl听力 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl运动能力 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LCG_查体 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl皮肤 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl巩膜 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl淋巴结 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LCG_肺 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl罗音 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LCG_腹部 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem55 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem56 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem57 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem58 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem59 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LCG_下肢水肿 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem60 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LCG_足背动脉搏动 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem61 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.lookUpEdit_医师签字 = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem_医师签字 = new DevExpress.XtraLayout.LayoutControlItem();
            this.flowLayoutPanel53 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.sBut_Get参照数据 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt齿列_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk齿列_义齿.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk齿列_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk齿列_龋齿.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk齿列_缺齿.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk齿列_正常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio足背动脉搏动.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio下肢水肿.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio心律.Properties)).BeginInit();
            this.flowLayoutPanel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio移动性浊音.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt移动性浊音.Properties)).BeginInit();
            this.flowLayoutPanel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio脾大.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脾大.Properties)).BeginInit();
            this.flowLayoutPanel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio肝大.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt肝大.Properties)).BeginInit();
            this.flowLayoutPanel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio包块.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt包块.Properties)).BeginInit();
            this.flowLayoutPanel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio压痛.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt压痛.Properties)).BeginInit();
            this.flowLayoutPanel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio杂音.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt杂音.Properties)).BeginInit();
            this.flowLayoutPanel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio呼吸音.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt呼吸音_异常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio桶状胸.Properties)).BeginInit();
            this.flow巩膜.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk巩膜_正常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk巩膜_黄染.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk巩膜_充血.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk巩膜_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt巩膜_其他.Properties)).BeginInit();
            this.flow皮肤.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_正常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_潮红.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_苍白.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_发绀.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_黄染.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_色素沉着.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt皮肤_其他.Properties)).BeginInit();
            this.flow罗音.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk罗音_无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk罗音_干罗音.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk罗音_湿罗音.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk罗音_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt罗音_其他.Properties)).BeginInit();
            this.flowLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio眼底.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt眼底.Properties)).BeginInit();
            this.flow听力.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio听力_听见.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio听力_听不清.Properties)).BeginInit();
            this.flow淋巴结.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk淋巴结_未触及.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk淋巴结_锁骨上.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk淋巴结_腋窝.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk淋巴结_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt淋巴结_其他.Properties)).BeginInit();
            this.flow运动功能.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio运动能力_可顺利完成.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio运动能力_无法完成.Properties)).BeginInit();
            this.flowLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt左眼视力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt右眼视力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt矫正左眼视力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt矫正右眼视力.Properties)).BeginInit();
            this.flow咽部.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio咽部_无充血.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio咽部_充血.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio咽部_淋巴滤泡增生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio咽部_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt咽部_其他.Properties)).BeginInit();
            this.flow口唇.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio口唇.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt口唇其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TCG_Tags)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_心脏)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl心率)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_口唇咽部)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl咽部)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_齿列)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem140)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem143)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem145)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem144)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem146)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem147)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem141)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem148)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem150)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem151)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem149)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem152)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem154)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem155)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem153)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem142)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem158)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_视听)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl视力)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl听力)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl运动能力)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_查体)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl皮肤)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl巩膜)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl淋巴结)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_肺)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl罗音)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_腹部)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_下肢水肿)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_足背动脉搏动)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_医师签字.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_医师签字)).BeginInit();
            this.flowLayoutPanel53.SuspendLayout();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btn下一项);
            this.layoutControl1.Controls.Add(this.btn上一项);
            this.layoutControl1.Controls.Add(this.txt齿列_其他);
            this.layoutControl1.Controls.Add(this.txt义齿4);
            this.layoutControl1.Controls.Add(this.txt义齿3);
            this.layoutControl1.Controls.Add(this.txt义齿2);
            this.layoutControl1.Controls.Add(this.txt义齿1);
            this.layoutControl1.Controls.Add(this.txt龋齿4);
            this.layoutControl1.Controls.Add(this.txt龋齿3);
            this.layoutControl1.Controls.Add(this.txt龋齿2);
            this.layoutControl1.Controls.Add(this.txt龋齿1);
            this.layoutControl1.Controls.Add(this.txt缺齿4);
            this.layoutControl1.Controls.Add(this.txt缺齿2);
            this.layoutControl1.Controls.Add(this.txt缺齿3);
            this.layoutControl1.Controls.Add(this.txt缺齿1);
            this.layoutControl1.Controls.Add(this.chk齿列_义齿);
            this.layoutControl1.Controls.Add(this.chk齿列_其他);
            this.layoutControl1.Controls.Add(this.chk齿列_龋齿);
            this.layoutControl1.Controls.Add(this.chk齿列_缺齿);
            this.layoutControl1.Controls.Add(this.chk齿列_正常);
            this.layoutControl1.Controls.Add(this.radio足背动脉搏动);
            this.layoutControl1.Controls.Add(this.radio下肢水肿);
            this.layoutControl1.Controls.Add(this.radio心律);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel20);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel19);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel18);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel17);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel16);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel15);
            this.layoutControl1.Controls.Add(this.txt心率);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel13);
            this.layoutControl1.Controls.Add(this.radio桶状胸);
            this.layoutControl1.Controls.Add(this.flow巩膜);
            this.layoutControl1.Controls.Add(this.flow皮肤);
            this.layoutControl1.Controls.Add(this.flow罗音);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel9);
            this.layoutControl1.Controls.Add(this.flow听力);
            this.layoutControl1.Controls.Add(this.flow淋巴结);
            this.layoutControl1.Controls.Add(this.flow运动功能);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel6);
            this.layoutControl1.Controls.Add(this.flow咽部);
            this.layoutControl1.Controls.Add(this.flow口唇);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 48);
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(566, 240, 491, 760);
            this.layoutControl1.OptionsFocus.EnableAutoTabOrder = false;
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(624, 423);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btn下一项
            // 
            this.btn下一项.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn下一项.Appearance.Options.UseFont = true;
            this.btn下一项.Image = ((System.Drawing.Image)(resources.GetObject("btn下一项.Image")));
            this.btn下一项.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btn下一项.Location = new System.Drawing.Point(314, 380);
            this.btn下一项.Name = "btn下一项";
            this.btn下一项.Size = new System.Drawing.Size(305, 38);
            this.btn下一项.StyleController = this.layoutControl1;
            this.btn下一项.TabIndex = 121;
            this.btn下一项.Text = "下一项";
            this.btn下一项.Click += new System.EventHandler(this.btn下一页_Click);
            // 
            // btn上一项
            // 
            this.btn上一项.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn上一项.Appearance.Options.UseFont = true;
            this.btn上一项.Image = ((System.Drawing.Image)(resources.GetObject("btn上一项.Image")));
            this.btn上一项.Location = new System.Drawing.Point(5, 380);
            this.btn上一项.Name = "btn上一项";
            this.btn上一项.Size = new System.Drawing.Size(305, 38);
            this.btn上一项.StyleController = this.layoutControl1;
            this.btn上一项.TabIndex = 120;
            this.btn上一项.Text = "上一项";
            this.btn上一项.Click += new System.EventHandler(this.btn上一页_Click);
            // 
            // txt齿列_其他
            // 
            this.txt齿列_其他.Enabled = false;
            this.txt齿列_其他.Location = new System.Drawing.Point(330, 149);
            this.txt齿列_其他.Name = "txt齿列_其他";
            this.txt齿列_其他.Properties.AutoHeight = false;
            this.txt齿列_其他.Size = new System.Drawing.Size(284, 28);
            this.txt齿列_其他.StyleController = this.layoutControl1;
            this.txt齿列_其他.TabIndex = 77;
            // 
            // txt义齿4
            // 
            this.txt义齿4.Enabled = false;
            this.txt义齿4.Location = new System.Drawing.Point(170, 176);
            this.txt义齿4.Name = "txt义齿4";
            this.txt义齿4.Properties.AutoHeight = false;
            this.txt义齿4.Size = new System.Drawing.Size(96, 31);
            this.txt义齿4.StyleController = this.layoutControl1;
            this.txt义齿4.TabIndex = 76;
            // 
            // txt义齿3
            // 
            this.txt义齿3.Enabled = false;
            this.txt义齿3.Location = new System.Drawing.Point(70, 176);
            this.txt义齿3.Name = "txt义齿3";
            this.txt义齿3.Properties.AutoHeight = false;
            this.txt义齿3.Size = new System.Drawing.Size(96, 31);
            this.txt义齿3.StyleController = this.layoutControl1;
            this.txt义齿3.TabIndex = 73;
            // 
            // txt义齿2
            // 
            this.txt义齿2.Enabled = false;
            this.txt义齿2.Location = new System.Drawing.Point(170, 141);
            this.txt义齿2.Name = "txt义齿2";
            this.txt义齿2.Properties.AutoHeight = false;
            this.txt义齿2.Size = new System.Drawing.Size(96, 31);
            this.txt义齿2.StyleController = this.layoutControl1;
            this.txt义齿2.TabIndex = 74;
            // 
            // txt义齿1
            // 
            this.txt义齿1.Enabled = false;
            this.txt义齿1.Location = new System.Drawing.Point(70, 141);
            this.txt义齿1.Name = "txt义齿1";
            this.txt义齿1.Properties.AutoHeight = false;
            this.txt义齿1.Size = new System.Drawing.Size(96, 31);
            this.txt义齿1.StyleController = this.layoutControl1;
            this.txt义齿1.TabIndex = 72;
            // 
            // txt龋齿4
            // 
            this.txt龋齿4.Enabled = false;
            this.txt龋齿4.Location = new System.Drawing.Point(430, 101);
            this.txt龋齿4.Name = "txt龋齿4";
            this.txt龋齿4.Properties.AutoHeight = false;
            this.txt龋齿4.Size = new System.Drawing.Size(96, 36);
            this.txt龋齿4.StyleController = this.layoutControl1;
            this.txt龋齿4.TabIndex = 70;
            // 
            // txt龋齿3
            // 
            this.txt龋齿3.Enabled = false;
            this.txt龋齿3.Location = new System.Drawing.Point(330, 101);
            this.txt龋齿3.Name = "txt龋齿3";
            this.txt龋齿3.Properties.AutoHeight = false;
            this.txt龋齿3.Size = new System.Drawing.Size(96, 36);
            this.txt龋齿3.StyleController = this.layoutControl1;
            this.txt龋齿3.TabIndex = 69;
            // 
            // txt龋齿2
            // 
            this.txt龋齿2.Enabled = false;
            this.txt龋齿2.Location = new System.Drawing.Point(430, 61);
            this.txt龋齿2.Name = "txt龋齿2";
            this.txt龋齿2.Properties.AutoHeight = false;
            this.txt龋齿2.Size = new System.Drawing.Size(96, 36);
            this.txt龋齿2.StyleController = this.layoutControl1;
            this.txt龋齿2.TabIndex = 68;
            // 
            // txt龋齿1
            // 
            this.txt龋齿1.Enabled = false;
            this.txt龋齿1.Location = new System.Drawing.Point(330, 61);
            this.txt龋齿1.Name = "txt龋齿1";
            this.txt龋齿1.Properties.AutoHeight = false;
            this.txt龋齿1.Size = new System.Drawing.Size(96, 36);
            this.txt龋齿1.StyleController = this.layoutControl1;
            this.txt龋齿1.TabIndex = 67;
            // 
            // txt缺齿4
            // 
            this.txt缺齿4.Enabled = false;
            this.txt缺齿4.Location = new System.Drawing.Point(170, 101);
            this.txt缺齿4.Name = "txt缺齿4";
            this.txt缺齿4.Properties.AutoHeight = false;
            this.txt缺齿4.Size = new System.Drawing.Size(96, 36);
            this.txt缺齿4.StyleController = this.layoutControl1;
            this.txt缺齿4.TabIndex = 65;
            // 
            // txt缺齿2
            // 
            this.txt缺齿2.Enabled = false;
            this.txt缺齿2.Location = new System.Drawing.Point(170, 61);
            this.txt缺齿2.Name = "txt缺齿2";
            this.txt缺齿2.Properties.AutoHeight = false;
            this.txt缺齿2.Size = new System.Drawing.Size(96, 36);
            this.txt缺齿2.StyleController = this.layoutControl1;
            this.txt缺齿2.TabIndex = 63;
            this.txt缺齿2.Tag = "";
            // 
            // txt缺齿3
            // 
            this.txt缺齿3.Enabled = false;
            this.txt缺齿3.Location = new System.Drawing.Point(70, 101);
            this.txt缺齿3.Name = "txt缺齿3";
            this.txt缺齿3.Properties.AutoHeight = false;
            this.txt缺齿3.Size = new System.Drawing.Size(96, 36);
            this.txt缺齿3.StyleController = this.layoutControl1;
            this.txt缺齿3.TabIndex = 64;
            // 
            // txt缺齿1
            // 
            this.txt缺齿1.Enabled = false;
            this.txt缺齿1.Location = new System.Drawing.Point(70, 61);
            this.txt缺齿1.Name = "txt缺齿1";
            this.txt缺齿1.Properties.AutoHeight = false;
            this.txt缺齿1.Size = new System.Drawing.Size(96, 36);
            this.txt缺齿1.StyleController = this.layoutControl1;
            this.txt缺齿1.TabIndex = 62;
            // 
            // chk齿列_义齿
            // 
            this.chk齿列_义齿.Enabled = false;
            this.chk齿列_义齿.Location = new System.Drawing.Point(10, 149);
            this.chk齿列_义齿.Name = "chk齿列_义齿";
            this.chk齿列_义齿.Properties.Appearance.Options.UseTextOptions = true;
            this.chk齿列_义齿.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk齿列_义齿.Properties.Caption = "义齿(假牙)";
            this.chk齿列_义齿.Size = new System.Drawing.Size(56, 32);
            this.chk齿列_义齿.StyleController = this.layoutControl1;
            this.chk齿列_义齿.TabIndex = 71;
            this.chk齿列_义齿.CheckedChanged += new System.EventHandler(this.chk齿列_义齿_CheckedChanged);
            // 
            // chk齿列_其他
            // 
            this.chk齿列_其他.Enabled = false;
            this.chk齿列_其他.Location = new System.Drawing.Point(270, 149);
            this.chk齿列_其他.Name = "chk齿列_其他";
            this.chk齿列_其他.Properties.Appearance.Options.UseTextOptions = true;
            this.chk齿列_其他.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk齿列_其他.Properties.Caption = "其他";
            this.chk齿列_其他.Size = new System.Drawing.Size(56, 19);
            this.chk齿列_其他.StyleController = this.layoutControl1;
            this.chk齿列_其他.TabIndex = 119;
            this.chk齿列_其他.CheckedChanged += new System.EventHandler(this.chk齿列_其他_CheckedChanged);
            // 
            // chk齿列_龋齿
            // 
            this.chk齿列_龋齿.Enabled = false;
            this.chk齿列_龋齿.Location = new System.Drawing.Point(270, 69);
            this.chk齿列_龋齿.Name = "chk齿列_龋齿";
            this.chk齿列_龋齿.Properties.Appearance.Options.UseTextOptions = true;
            this.chk齿列_龋齿.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk齿列_龋齿.Properties.Caption = "龋齿";
            this.chk齿列_龋齿.Size = new System.Drawing.Size(56, 19);
            this.chk齿列_龋齿.StyleController = this.layoutControl1;
            this.chk齿列_龋齿.TabIndex = 66;
            this.chk齿列_龋齿.CheckedChanged += new System.EventHandler(this.chk齿列_龋齿_CheckedChanged);
            // 
            // chk齿列_缺齿
            // 
            this.chk齿列_缺齿.Enabled = false;
            this.chk齿列_缺齿.Location = new System.Drawing.Point(10, 69);
            this.chk齿列_缺齿.Name = "chk齿列_缺齿";
            this.chk齿列_缺齿.Properties.Appearance.Options.UseTextOptions = true;
            this.chk齿列_缺齿.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk齿列_缺齿.Properties.Caption = "缺齿";
            this.chk齿列_缺齿.Size = new System.Drawing.Size(56, 19);
            this.chk齿列_缺齿.StyleController = this.layoutControl1;
            this.chk齿列_缺齿.TabIndex = 61;
            this.chk齿列_缺齿.CheckedChanged += new System.EventHandler(this.chk齿列_缺齿_CheckedChanged);
            // 
            // chk齿列_正常
            // 
            this.chk齿列_正常.EditValue = true;
            this.chk齿列_正常.Location = new System.Drawing.Point(10, 38);
            this.chk齿列_正常.Name = "chk齿列_正常";
            this.chk齿列_正常.Properties.Caption = "正常";
            this.chk齿列_正常.Size = new System.Drawing.Size(56, 19);
            this.chk齿列_正常.StyleController = this.layoutControl1;
            this.chk齿列_正常.TabIndex = 60;
            this.chk齿列_正常.CheckedChanged += new System.EventHandler(this.chk齿列_正常_CheckedChanged);
            // 
            // radio足背动脉搏动
            // 
            this.radio足背动脉搏动.EditValue = "2";
            this.radio足背动脉搏动.Location = new System.Drawing.Point(105, 38);
            this.radio足背动脉搏动.Name = "radio足背动脉搏动";
            this.radio足背动脉搏动.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio足背动脉搏动.Properties.Appearance.Options.UseFont = true;
            this.radio足背动脉搏动.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "未触及"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "触及双侧对称"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "触及左侧弱或消失"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "触及右侧弱或消失")});
            this.radio足背动脉搏动.Size = new System.Drawing.Size(509, 162);
            this.radio足背动脉搏动.StyleController = this.layoutControl1;
            this.radio足背动脉搏动.TabIndex = 98;
            // 
            // radio下肢水肿
            // 
            this.radio下肢水肿.EditValue = "1";
            this.radio下肢水肿.Location = new System.Drawing.Point(105, 38);
            this.radio下肢水肿.Name = "radio下肢水肿";
            this.radio下肢水肿.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio下肢水肿.Properties.Appearance.Options.UseFont = true;
            this.radio下肢水肿.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "左侧"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "右侧"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "双侧不对称"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("5", "双侧对称")});
            this.radio下肢水肿.Size = new System.Drawing.Size(509, 162);
            this.radio下肢水肿.StyleController = this.layoutControl1;
            this.radio下肢水肿.TabIndex = 97;
            // 
            // radio心律
            // 
            this.radio心律.EditValue = "1";
            this.radio心律.Location = new System.Drawing.Point(105, 81);
            this.radio心律.Name = "radio心律";
            this.radio心律.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio心律.Properties.Appearance.Options.UseFont = true;
            this.radio心律.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "齐"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "不齐"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "绝对不齐")});
            this.radio心律.Size = new System.Drawing.Size(509, 51);
            this.radio心律.StyleController = this.layoutControl1;
            this.radio心律.TabIndex = 90;
            // 
            // flowLayoutPanel20
            // 
            this.flowLayoutPanel20.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel20.Controls.Add(this.radio移动性浊音);
            this.flowLayoutPanel20.Controls.Add(this.txt移动性浊音);
            this.flowLayoutPanel20.Location = new System.Drawing.Point(105, 210);
            this.flowLayoutPanel20.Name = "flowLayoutPanel20";
            this.flowLayoutPanel20.Size = new System.Drawing.Size(509, 22);
            this.flowLayoutPanel20.TabIndex = 96;
            // 
            // radio移动性浊音
            // 
            this.radio移动性浊音.EditValue = "1";
            this.radio移动性浊音.Location = new System.Drawing.Point(3, 0);
            this.radio移动性浊音.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio移动性浊音.Name = "radio移动性浊音";
            this.radio移动性浊音.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio移动性浊音.Properties.Appearance.Options.UseFont = true;
            this.radio移动性浊音.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "有")});
            this.radio移动性浊音.Size = new System.Drawing.Size(126, 35);
            this.radio移动性浊音.TabIndex = 2;
            this.radio移动性浊音.SelectedIndexChanged += new System.EventHandler(this.radio移动性浊音_SelectedIndexChanged);
            // 
            // txt移动性浊音
            // 
            this.txt移动性浊音.Enabled = false;
            this.txt移动性浊音.Location = new System.Drawing.Point(129, 0);
            this.txt移动性浊音.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt移动性浊音.Name = "txt移动性浊音";
            this.txt移动性浊音.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt移动性浊音.Properties.Appearance.Options.UseFont = true;
            this.txt移动性浊音.Properties.AutoHeight = false;
            this.txt移动性浊音.Size = new System.Drawing.Size(184, 34);
            this.txt移动性浊音.TabIndex = 1;
            // 
            // flowLayoutPanel19
            // 
            this.flowLayoutPanel19.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel19.Controls.Add(this.radio脾大);
            this.flowLayoutPanel19.Controls.Add(this.txt脾大);
            this.flowLayoutPanel19.Location = new System.Drawing.Point(105, 184);
            this.flowLayoutPanel19.Name = "flowLayoutPanel19";
            this.flowLayoutPanel19.Size = new System.Drawing.Size(509, 22);
            this.flowLayoutPanel19.TabIndex = 95;
            // 
            // radio脾大
            // 
            this.radio脾大.EditValue = "1";
            this.radio脾大.Location = new System.Drawing.Point(3, 0);
            this.radio脾大.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio脾大.Name = "radio脾大";
            this.radio脾大.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio脾大.Properties.Appearance.Options.UseFont = true;
            this.radio脾大.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "有")});
            this.radio脾大.Size = new System.Drawing.Size(126, 33);
            this.radio脾大.TabIndex = 2;
            this.radio脾大.SelectedIndexChanged += new System.EventHandler(this.radio脾大_SelectedIndexChanged);
            // 
            // txt脾大
            // 
            this.txt脾大.Enabled = false;
            this.txt脾大.Location = new System.Drawing.Point(129, 0);
            this.txt脾大.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt脾大.Name = "txt脾大";
            this.txt脾大.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt脾大.Properties.Appearance.Options.UseFont = true;
            this.txt脾大.Properties.AutoHeight = false;
            this.txt脾大.Size = new System.Drawing.Size(184, 33);
            this.txt脾大.TabIndex = 1;
            // 
            // flowLayoutPanel18
            // 
            this.flowLayoutPanel18.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel18.Controls.Add(this.radio肝大);
            this.flowLayoutPanel18.Controls.Add(this.txt肝大);
            this.flowLayoutPanel18.Location = new System.Drawing.Point(105, 141);
            this.flowLayoutPanel18.Name = "flowLayoutPanel18";
            this.flowLayoutPanel18.Size = new System.Drawing.Size(509, 39);
            this.flowLayoutPanel18.TabIndex = 94;
            // 
            // radio肝大
            // 
            this.radio肝大.EditValue = "1";
            this.radio肝大.Location = new System.Drawing.Point(3, 0);
            this.radio肝大.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio肝大.Name = "radio肝大";
            this.radio肝大.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio肝大.Properties.Appearance.Options.UseFont = true;
            this.radio肝大.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "有")});
            this.radio肝大.Size = new System.Drawing.Size(126, 29);
            this.radio肝大.TabIndex = 2;
            this.radio肝大.SelectedIndexChanged += new System.EventHandler(this.radio肝大_SelectedIndexChanged);
            // 
            // txt肝大
            // 
            this.txt肝大.Enabled = false;
            this.txt肝大.Location = new System.Drawing.Point(129, 0);
            this.txt肝大.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt肝大.Name = "txt肝大";
            this.txt肝大.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt肝大.Properties.Appearance.Options.UseFont = true;
            this.txt肝大.Properties.AutoHeight = false;
            this.txt肝大.Size = new System.Drawing.Size(184, 29);
            this.txt肝大.TabIndex = 1;
            // 
            // flowLayoutPanel17
            // 
            this.flowLayoutPanel17.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel17.Controls.Add(this.radio包块);
            this.flowLayoutPanel17.Controls.Add(this.txt包块);
            this.flowLayoutPanel17.Location = new System.Drawing.Point(105, 89);
            this.flowLayoutPanel17.Name = "flowLayoutPanel17";
            this.flowLayoutPanel17.Size = new System.Drawing.Size(509, 48);
            this.flowLayoutPanel17.TabIndex = 93;
            // 
            // radio包块
            // 
            this.radio包块.EditValue = "1";
            this.radio包块.Location = new System.Drawing.Point(3, 0);
            this.radio包块.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio包块.Name = "radio包块";
            this.radio包块.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio包块.Properties.Appearance.Options.UseFont = true;
            this.radio包块.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "有")});
            this.radio包块.Size = new System.Drawing.Size(126, 26);
            this.radio包块.TabIndex = 0;
            this.radio包块.SelectedIndexChanged += new System.EventHandler(this.radio包块_SelectedIndexChanged);
            // 
            // txt包块
            // 
            this.txt包块.Enabled = false;
            this.txt包块.Location = new System.Drawing.Point(129, 0);
            this.txt包块.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt包块.Name = "txt包块";
            this.txt包块.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt包块.Properties.Appearance.Options.UseFont = true;
            this.txt包块.Properties.AutoHeight = false;
            this.txt包块.Size = new System.Drawing.Size(184, 26);
            this.txt包块.TabIndex = 1;
            // 
            // flowLayoutPanel16
            // 
            this.flowLayoutPanel16.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel16.Controls.Add(this.radio压痛);
            this.flowLayoutPanel16.Controls.Add(this.txt压痛);
            this.flowLayoutPanel16.Location = new System.Drawing.Point(105, 38);
            this.flowLayoutPanel16.Name = "flowLayoutPanel16";
            this.flowLayoutPanel16.Size = new System.Drawing.Size(509, 47);
            this.flowLayoutPanel16.TabIndex = 92;
            // 
            // radio压痛
            // 
            this.radio压痛.EditValue = "1";
            this.radio压痛.Location = new System.Drawing.Point(3, 0);
            this.radio压痛.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio压痛.Name = "radio压痛";
            this.radio压痛.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio压痛.Properties.Appearance.Options.UseFont = true;
            this.radio压痛.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "有")});
            this.radio压痛.Size = new System.Drawing.Size(126, 29);
            this.radio压痛.TabIndex = 0;
            this.radio压痛.SelectedIndexChanged += new System.EventHandler(this.radio压痛_SelectedIndexChanged);
            // 
            // txt压痛
            // 
            this.txt压痛.Enabled = false;
            this.txt压痛.Location = new System.Drawing.Point(129, 0);
            this.txt压痛.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt压痛.Name = "txt压痛";
            this.txt压痛.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt压痛.Properties.Appearance.Options.UseFont = true;
            this.txt压痛.Properties.AutoHeight = false;
            this.txt压痛.Size = new System.Drawing.Size(184, 31);
            this.txt压痛.TabIndex = 1;
            // 
            // flowLayoutPanel15
            // 
            this.flowLayoutPanel15.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel15.Controls.Add(this.radio杂音);
            this.flowLayoutPanel15.Controls.Add(this.txt杂音);
            this.flowLayoutPanel15.Location = new System.Drawing.Point(105, 136);
            this.flowLayoutPanel15.Name = "flowLayoutPanel15";
            this.flowLayoutPanel15.Size = new System.Drawing.Size(509, 59);
            this.flowLayoutPanel15.TabIndex = 91;
            // 
            // radio杂音
            // 
            this.radio杂音.EditValue = "1";
            this.radio杂音.Location = new System.Drawing.Point(3, 0);
            this.radio杂音.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio杂音.Name = "radio杂音";
            this.radio杂音.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio杂音.Properties.Appearance.Options.UseFont = true;
            this.radio杂音.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "有")});
            this.radio杂音.Size = new System.Drawing.Size(126, 34);
            this.radio杂音.TabIndex = 0;
            this.radio杂音.SelectedIndexChanged += new System.EventHandler(this.radio杂音_SelectedIndexChanged);
            // 
            // txt杂音
            // 
            this.txt杂音.Enabled = false;
            this.txt杂音.Location = new System.Drawing.Point(129, 0);
            this.txt杂音.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt杂音.Name = "txt杂音";
            this.txt杂音.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt杂音.Properties.Appearance.Options.UseFont = true;
            this.txt杂音.Properties.AutoHeight = false;
            this.txt杂音.Size = new System.Drawing.Size(184, 34);
            this.txt杂音.TabIndex = 1;
            // 
            // txt心率
            // 
            this.txt心率.BackColor = System.Drawing.Color.White;
            this.txt心率.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt心率.Lbl1Text = "次/分";
            this.txt心率.Location = new System.Drawing.Point(105, 38);
            this.txt心率.Margin = new System.Windows.Forms.Padding(0);
            this.txt心率.Name = "txt心率";
            this.txt心率.Size = new System.Drawing.Size(509, 39);
            this.txt心率.TabIndex = 89;
            this.txt心率.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // flowLayoutPanel13
            // 
            this.flowLayoutPanel13.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel13.Controls.Add(this.radio呼吸音);
            this.flowLayoutPanel13.Controls.Add(this.txt呼吸音_异常);
            this.flowLayoutPanel13.Location = new System.Drawing.Point(105, 93);
            this.flowLayoutPanel13.Name = "flowLayoutPanel13";
            this.flowLayoutPanel13.Size = new System.Drawing.Size(509, 60);
            this.flowLayoutPanel13.TabIndex = 87;
            // 
            // radio呼吸音
            // 
            this.radio呼吸音.EditValue = "1";
            this.radio呼吸音.Location = new System.Drawing.Point(3, 0);
            this.radio呼吸音.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio呼吸音.Name = "radio呼吸音";
            this.radio呼吸音.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio呼吸音.Properties.Appearance.Options.UseFont = true;
            this.radio呼吸音.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "正常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "异常")});
            this.radio呼吸音.Size = new System.Drawing.Size(126, 36);
            this.radio呼吸音.TabIndex = 0;
            this.radio呼吸音.SelectedIndexChanged += new System.EventHandler(this.radio呼吸音_SelectedIndexChanged);
            // 
            // txt呼吸音_异常
            // 
            this.txt呼吸音_异常.Enabled = false;
            this.txt呼吸音_异常.Location = new System.Drawing.Point(129, 0);
            this.txt呼吸音_异常.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt呼吸音_异常.Name = "txt呼吸音_异常";
            this.txt呼吸音_异常.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt呼吸音_异常.Properties.Appearance.Options.UseFont = true;
            this.txt呼吸音_异常.Properties.AutoHeight = false;
            this.txt呼吸音_异常.Size = new System.Drawing.Size(184, 36);
            this.txt呼吸音_异常.TabIndex = 1;
            // 
            // radio桶状胸
            // 
            this.radio桶状胸.EditValue = "2";
            this.radio桶状胸.Location = new System.Drawing.Point(105, 38);
            this.radio桶状胸.Name = "radio桶状胸";
            this.radio桶状胸.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio桶状胸.Properties.Appearance.Options.UseFont = true;
            this.radio桶状胸.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio桶状胸.Size = new System.Drawing.Size(509, 51);
            this.radio桶状胸.StyleController = this.layoutControl1;
            this.radio桶状胸.TabIndex = 86;
            // 
            // flow巩膜
            // 
            this.flow巩膜.BackColor = System.Drawing.Color.White;
            this.flow巩膜.Controls.Add(this.chk巩膜_正常);
            this.flow巩膜.Controls.Add(this.chk巩膜_黄染);
            this.flow巩膜.Controls.Add(this.chk巩膜_充血);
            this.flow巩膜.Controls.Add(this.chk巩膜_其他);
            this.flow巩膜.Controls.Add(this.txt巩膜_其他);
            this.flow巩膜.Location = new System.Drawing.Point(105, 172);
            this.flow巩膜.Name = "flow巩膜";
            this.flow巩膜.Size = new System.Drawing.Size(509, 33);
            this.flow巩膜.TabIndex = 84;
            // 
            // chk巩膜_正常
            // 
            this.chk巩膜_正常.EditValue = true;
            this.chk巩膜_正常.Location = new System.Drawing.Point(3, 3);
            this.chk巩膜_正常.Name = "chk巩膜_正常";
            this.chk巩膜_正常.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk巩膜_正常.Properties.Appearance.Options.UseFont = true;
            this.chk巩膜_正常.Properties.Caption = "正常";
            this.chk巩膜_正常.Size = new System.Drawing.Size(54, 23);
            this.chk巩膜_正常.TabIndex = 1;
            this.chk巩膜_正常.Tag = "1";
            this.chk巩膜_正常.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk巩膜_黄染
            // 
            this.chk巩膜_黄染.Location = new System.Drawing.Point(63, 3);
            this.chk巩膜_黄染.Name = "chk巩膜_黄染";
            this.chk巩膜_黄染.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk巩膜_黄染.Properties.Appearance.Options.UseFont = true;
            this.chk巩膜_黄染.Properties.Caption = "黄染";
            this.chk巩膜_黄染.Size = new System.Drawing.Size(66, 23);
            this.chk巩膜_黄染.TabIndex = 2;
            this.chk巩膜_黄染.Tag = "2";
            // 
            // chk巩膜_充血
            // 
            this.chk巩膜_充血.Location = new System.Drawing.Point(135, 3);
            this.chk巩膜_充血.Name = "chk巩膜_充血";
            this.chk巩膜_充血.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk巩膜_充血.Properties.Appearance.Options.UseFont = true;
            this.chk巩膜_充血.Properties.Caption = "充血";
            this.chk巩膜_充血.Size = new System.Drawing.Size(69, 23);
            this.chk巩膜_充血.TabIndex = 3;
            this.chk巩膜_充血.Tag = "3";
            // 
            // chk巩膜_其他
            // 
            this.chk巩膜_其他.Location = new System.Drawing.Point(210, 3);
            this.chk巩膜_其他.Name = "chk巩膜_其他";
            this.chk巩膜_其他.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk巩膜_其他.Properties.Appearance.Options.UseFont = true;
            this.chk巩膜_其他.Properties.Caption = "其他";
            this.chk巩膜_其他.Size = new System.Drawing.Size(54, 23);
            this.chk巩膜_其他.TabIndex = 4;
            this.chk巩膜_其他.Tag = "99";
            this.chk巩膜_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt巩膜_其他
            // 
            this.txt巩膜_其他.Enabled = false;
            this.txt巩膜_其他.Location = new System.Drawing.Point(270, 3);
            this.txt巩膜_其他.Name = "txt巩膜_其他";
            this.txt巩膜_其他.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt巩膜_其他.Properties.Appearance.Options.UseFont = true;
            this.txt巩膜_其他.Size = new System.Drawing.Size(100, 26);
            this.txt巩膜_其他.TabIndex = 5;
            // 
            // flow皮肤
            // 
            this.flow皮肤.BackColor = System.Drawing.Color.White;
            this.flow皮肤.Controls.Add(this.chk皮肤_正常);
            this.flow皮肤.Controls.Add(this.chk皮肤_潮红);
            this.flow皮肤.Controls.Add(this.chk皮肤_苍白);
            this.flow皮肤.Controls.Add(this.chk皮肤_发绀);
            this.flow皮肤.Controls.Add(this.chk皮肤_黄染);
            this.flow皮肤.Controls.Add(this.chk皮肤_色素沉着);
            this.flow皮肤.Controls.Add(this.chk皮肤_其他);
            this.flow皮肤.Controls.Add(this.txt皮肤_其他);
            this.flow皮肤.Location = new System.Drawing.Point(105, 88);
            this.flow皮肤.Margin = new System.Windows.Forms.Padding(0);
            this.flow皮肤.Name = "flow皮肤";
            this.flow皮肤.Size = new System.Drawing.Size(509, 80);
            this.flow皮肤.TabIndex = 83;
            // 
            // chk皮肤_正常
            // 
            this.chk皮肤_正常.EditValue = true;
            this.chk皮肤_正常.Location = new System.Drawing.Point(3, 3);
            this.chk皮肤_正常.Name = "chk皮肤_正常";
            this.chk皮肤_正常.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk皮肤_正常.Properties.Appearance.Options.UseFont = true;
            this.chk皮肤_正常.Properties.Caption = "正常";
            this.chk皮肤_正常.Size = new System.Drawing.Size(68, 23);
            this.chk皮肤_正常.TabIndex = 0;
            this.chk皮肤_正常.Tag = "1";
            this.chk皮肤_正常.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk皮肤_潮红
            // 
            this.chk皮肤_潮红.Location = new System.Drawing.Point(77, 3);
            this.chk皮肤_潮红.Name = "chk皮肤_潮红";
            this.chk皮肤_潮红.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk皮肤_潮红.Properties.Appearance.Options.UseFont = true;
            this.chk皮肤_潮红.Properties.Caption = "潮红";
            this.chk皮肤_潮红.Size = new System.Drawing.Size(66, 23);
            this.chk皮肤_潮红.TabIndex = 1;
            this.chk皮肤_潮红.Tag = "2";
            // 
            // chk皮肤_苍白
            // 
            this.chk皮肤_苍白.Location = new System.Drawing.Point(149, 3);
            this.chk皮肤_苍白.Name = "chk皮肤_苍白";
            this.chk皮肤_苍白.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk皮肤_苍白.Properties.Appearance.Options.UseFont = true;
            this.chk皮肤_苍白.Properties.Caption = "苍白";
            this.chk皮肤_苍白.Size = new System.Drawing.Size(69, 23);
            this.chk皮肤_苍白.TabIndex = 2;
            this.chk皮肤_苍白.Tag = "3";
            // 
            // chk皮肤_发绀
            // 
            this.chk皮肤_发绀.Location = new System.Drawing.Point(224, 3);
            this.chk皮肤_发绀.Name = "chk皮肤_发绀";
            this.chk皮肤_发绀.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk皮肤_发绀.Properties.Appearance.Options.UseFont = true;
            this.chk皮肤_发绀.Properties.Caption = "发绀";
            this.chk皮肤_发绀.Size = new System.Drawing.Size(85, 23);
            this.chk皮肤_发绀.TabIndex = 3;
            this.chk皮肤_发绀.Tag = "4";
            // 
            // chk皮肤_黄染
            // 
            this.chk皮肤_黄染.Location = new System.Drawing.Point(315, 3);
            this.chk皮肤_黄染.Name = "chk皮肤_黄染";
            this.chk皮肤_黄染.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk皮肤_黄染.Properties.Appearance.Options.UseFont = true;
            this.chk皮肤_黄染.Properties.Caption = "黄染";
            this.chk皮肤_黄染.Size = new System.Drawing.Size(69, 23);
            this.chk皮肤_黄染.TabIndex = 4;
            this.chk皮肤_黄染.Tag = "5";
            // 
            // chk皮肤_色素沉着
            // 
            this.chk皮肤_色素沉着.Location = new System.Drawing.Point(3, 28);
            this.chk皮肤_色素沉着.Name = "chk皮肤_色素沉着";
            this.chk皮肤_色素沉着.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk皮肤_色素沉着.Properties.Appearance.Options.UseFont = true;
            this.chk皮肤_色素沉着.Properties.Caption = "色素沉着";
            this.chk皮肤_色素沉着.Size = new System.Drawing.Size(126, 23);
            this.chk皮肤_色素沉着.TabIndex = 5;
            this.chk皮肤_色素沉着.Tag = "6";
            // 
            // chk皮肤_其他
            // 
            this.chk皮肤_其他.Location = new System.Drawing.Point(135, 28);
            this.chk皮肤_其他.Name = "chk皮肤_其他";
            this.chk皮肤_其他.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk皮肤_其他.Properties.Appearance.Options.UseFont = true;
            this.chk皮肤_其他.Properties.Caption = "其他";
            this.chk皮肤_其他.Size = new System.Drawing.Size(69, 23);
            this.chk皮肤_其他.TabIndex = 6;
            this.chk皮肤_其他.Tag = "99";
            this.chk皮肤_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt皮肤_其他
            // 
            this.txt皮肤_其他.Enabled = false;
            this.txt皮肤_其他.Location = new System.Drawing.Point(210, 28);
            this.txt皮肤_其他.Name = "txt皮肤_其他";
            this.txt皮肤_其他.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt皮肤_其他.Properties.Appearance.Options.UseFont = true;
            this.txt皮肤_其他.Size = new System.Drawing.Size(100, 26);
            this.txt皮肤_其他.TabIndex = 7;
            // 
            // flow罗音
            // 
            this.flow罗音.BackColor = System.Drawing.Color.White;
            this.flow罗音.Controls.Add(this.chk罗音_无);
            this.flow罗音.Controls.Add(this.chk罗音_干罗音);
            this.flow罗音.Controls.Add(this.chk罗音_湿罗音);
            this.flow罗音.Controls.Add(this.chk罗音_其他);
            this.flow罗音.Controls.Add(this.txt罗音_其他);
            this.flow罗音.Location = new System.Drawing.Point(105, 157);
            this.flow罗音.Name = "flow罗音";
            this.flow罗音.Size = new System.Drawing.Size(509, 47);
            this.flow罗音.TabIndex = 88;
            // 
            // chk罗音_无
            // 
            this.chk罗音_无.EditValue = true;
            this.chk罗音_无.Location = new System.Drawing.Point(3, 3);
            this.chk罗音_无.Name = "chk罗音_无";
            this.chk罗音_无.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk罗音_无.Properties.Appearance.Options.UseFont = true;
            this.chk罗音_无.Properties.Caption = "无";
            this.chk罗音_无.Size = new System.Drawing.Size(36, 23);
            this.chk罗音_无.TabIndex = 1;
            this.chk罗音_无.Tag = "1";
            this.chk罗音_无.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk罗音_干罗音
            // 
            this.chk罗音_干罗音.Location = new System.Drawing.Point(45, 3);
            this.chk罗音_干罗音.Name = "chk罗音_干罗音";
            this.chk罗音_干罗音.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk罗音_干罗音.Properties.Appearance.Options.UseFont = true;
            this.chk罗音_干罗音.Properties.Caption = "干罗音";
            this.chk罗音_干罗音.Size = new System.Drawing.Size(71, 23);
            this.chk罗音_干罗音.TabIndex = 2;
            this.chk罗音_干罗音.Tag = "2";
            // 
            // chk罗音_湿罗音
            // 
            this.chk罗音_湿罗音.Location = new System.Drawing.Point(122, 3);
            this.chk罗音_湿罗音.Name = "chk罗音_湿罗音";
            this.chk罗音_湿罗音.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk罗音_湿罗音.Properties.Appearance.Options.UseFont = true;
            this.chk罗音_湿罗音.Properties.Caption = "湿罗音";
            this.chk罗音_湿罗音.Size = new System.Drawing.Size(69, 23);
            this.chk罗音_湿罗音.TabIndex = 3;
            this.chk罗音_湿罗音.Tag = "3";
            // 
            // chk罗音_其他
            // 
            this.chk罗音_其他.Location = new System.Drawing.Point(197, 3);
            this.chk罗音_其他.Name = "chk罗音_其他";
            this.chk罗音_其他.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk罗音_其他.Properties.Appearance.Options.UseFont = true;
            this.chk罗音_其他.Properties.Caption = "其他";
            this.chk罗音_其他.Size = new System.Drawing.Size(54, 23);
            this.chk罗音_其他.TabIndex = 4;
            this.chk罗音_其他.Tag = "4";
            this.chk罗音_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt罗音_其他
            // 
            this.txt罗音_其他.Enabled = false;
            this.txt罗音_其他.Location = new System.Drawing.Point(257, 3);
            this.txt罗音_其他.Name = "txt罗音_其他";
            this.txt罗音_其他.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt罗音_其他.Properties.Appearance.Options.UseFont = true;
            this.txt罗音_其他.Size = new System.Drawing.Size(100, 26);
            this.txt罗音_其他.TabIndex = 5;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel9.Controls.Add(this.radio眼底);
            this.flowLayoutPanel9.Controls.Add(this.txt眼底);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(105, 38);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(509, 46);
            this.flowLayoutPanel9.TabIndex = 82;
            // 
            // radio眼底
            // 
            this.radio眼底.Location = new System.Drawing.Point(3, 0);
            this.radio眼底.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio眼底.Name = "radio眼底";
            this.radio眼底.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio眼底.Properties.Appearance.Options.UseFont = true;
            this.radio眼底.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "正常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "异常")});
            this.radio眼底.Size = new System.Drawing.Size(126, 26);
            this.radio眼底.TabIndex = 0;
            this.radio眼底.SelectedIndexChanged += new System.EventHandler(this.radio眼底_SelectedIndexChanged);
            // 
            // txt眼底
            // 
            this.txt眼底.Enabled = false;
            this.txt眼底.Location = new System.Drawing.Point(129, 0);
            this.txt眼底.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt眼底.Name = "txt眼底";
            this.txt眼底.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt眼底.Properties.Appearance.Options.UseFont = true;
            this.txt眼底.Size = new System.Drawing.Size(184, 26);
            this.txt眼底.TabIndex = 1;
            // 
            // flow听力
            // 
            this.flow听力.BackColor = System.Drawing.Color.White;
            this.flow听力.Controls.Add(this.radio听力_听见);
            this.flow听力.Controls.Add(this.radio听力_听不清);
            this.flow听力.Location = new System.Drawing.Point(85, 126);
            this.flow听力.Margin = new System.Windows.Forms.Padding(0);
            this.flow听力.Name = "flow听力";
            this.flow听力.Size = new System.Drawing.Size(529, 81);
            this.flow听力.TabIndex = 80;
            // 
            // radio听力_听见
            // 
            this.radio听力_听见.EditValue = true;
            this.radio听力_听见.Location = new System.Drawing.Point(3, 0);
            this.radio听力_听见.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio听力_听见.Name = "radio听力_听见";
            this.radio听力_听见.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio听力_听见.Properties.Appearance.Options.UseFont = true;
            this.radio听力_听见.Properties.Caption = "听见";
            this.radio听力_听见.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.radio听力_听见.Properties.RadioGroupIndex = 2;
            this.radio听力_听见.Size = new System.Drawing.Size(207, 23);
            this.radio听力_听见.TabIndex = 0;
            this.radio听力_听见.Tag = "1";
            // 
            // radio听力_听不清
            // 
            this.radio听力_听不清.Location = new System.Drawing.Point(3, 22);
            this.radio听力_听不清.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio听力_听不清.Name = "radio听力_听不清";
            this.radio听力_听不清.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio听力_听不清.Properties.Appearance.Options.UseFont = true;
            this.radio听力_听不清.Properties.Caption = "听不清或无法听见（耳鼻喉科专科就诊）";
            this.radio听力_听不清.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.radio听力_听不清.Properties.RadioGroupIndex = 2;
            this.radio听力_听不清.Size = new System.Drawing.Size(317, 23);
            this.radio听力_听不清.TabIndex = 1;
            this.radio听力_听不清.TabStop = false;
            this.radio听力_听不清.Tag = "2";
            // 
            // flow淋巴结
            // 
            this.flow淋巴结.BackColor = System.Drawing.Color.White;
            this.flow淋巴结.Controls.Add(this.chk淋巴结_未触及);
            this.flow淋巴结.Controls.Add(this.chk淋巴结_锁骨上);
            this.flow淋巴结.Controls.Add(this.chk淋巴结_腋窝);
            this.flow淋巴结.Controls.Add(this.chk淋巴结_其他);
            this.flow淋巴结.Controls.Add(this.txt淋巴结_其他);
            this.flow淋巴结.Location = new System.Drawing.Point(105, 209);
            this.flow淋巴结.Name = "flow淋巴结";
            this.flow淋巴结.Size = new System.Drawing.Size(509, 22);
            this.flow淋巴结.TabIndex = 85;
            // 
            // chk淋巴结_未触及
            // 
            this.chk淋巴结_未触及.EditValue = true;
            this.chk淋巴结_未触及.Location = new System.Drawing.Point(3, 3);
            this.chk淋巴结_未触及.Name = "chk淋巴结_未触及";
            this.chk淋巴结_未触及.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk淋巴结_未触及.Properties.Appearance.Options.UseFont = true;
            this.chk淋巴结_未触及.Properties.Caption = "未触及";
            this.chk淋巴结_未触及.Size = new System.Drawing.Size(68, 23);
            this.chk淋巴结_未触及.TabIndex = 1;
            this.chk淋巴结_未触及.Tag = "1";
            this.chk淋巴结_未触及.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk淋巴结_锁骨上
            // 
            this.chk淋巴结_锁骨上.Location = new System.Drawing.Point(77, 3);
            this.chk淋巴结_锁骨上.Name = "chk淋巴结_锁骨上";
            this.chk淋巴结_锁骨上.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk淋巴结_锁骨上.Properties.Appearance.Options.UseFont = true;
            this.chk淋巴结_锁骨上.Properties.Caption = "锁骨上";
            this.chk淋巴结_锁骨上.Size = new System.Drawing.Size(78, 23);
            this.chk淋巴结_锁骨上.TabIndex = 2;
            this.chk淋巴结_锁骨上.Tag = "2";
            // 
            // chk淋巴结_腋窝
            // 
            this.chk淋巴结_腋窝.Location = new System.Drawing.Point(161, 3);
            this.chk淋巴结_腋窝.Name = "chk淋巴结_腋窝";
            this.chk淋巴结_腋窝.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk淋巴结_腋窝.Properties.Appearance.Options.UseFont = true;
            this.chk淋巴结_腋窝.Properties.Caption = "腋窝";
            this.chk淋巴结_腋窝.Size = new System.Drawing.Size(77, 23);
            this.chk淋巴结_腋窝.TabIndex = 3;
            this.chk淋巴结_腋窝.Tag = "3";
            // 
            // chk淋巴结_其他
            // 
            this.chk淋巴结_其他.Location = new System.Drawing.Point(244, 3);
            this.chk淋巴结_其他.Name = "chk淋巴结_其他";
            this.chk淋巴结_其他.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chk淋巴结_其他.Properties.Appearance.Options.UseFont = true;
            this.chk淋巴结_其他.Properties.Caption = "其他";
            this.chk淋巴结_其他.Size = new System.Drawing.Size(54, 23);
            this.chk淋巴结_其他.TabIndex = 4;
            this.chk淋巴结_其他.Tag = "4";
            this.chk淋巴结_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt淋巴结_其他
            // 
            this.txt淋巴结_其他.Enabled = false;
            this.txt淋巴结_其他.Location = new System.Drawing.Point(304, 3);
            this.txt淋巴结_其他.Name = "txt淋巴结_其他";
            this.txt淋巴结_其他.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt淋巴结_其他.Properties.Appearance.Options.UseFont = true;
            this.txt淋巴结_其他.Size = new System.Drawing.Size(100, 26);
            this.txt淋巴结_其他.TabIndex = 5;
            // 
            // flow运动功能
            // 
            this.flow运动功能.BackColor = System.Drawing.Color.White;
            this.flow运动功能.Controls.Add(this.radio运动能力_可顺利完成);
            this.flow运动功能.Controls.Add(this.radio运动能力_无法完成);
            this.flow运动功能.Location = new System.Drawing.Point(85, 211);
            this.flow运动功能.Margin = new System.Windows.Forms.Padding(0);
            this.flow运动功能.Name = "flow运动功能";
            this.flow运动功能.Size = new System.Drawing.Size(529, 20);
            this.flow运动功能.TabIndex = 81;
            // 
            // radio运动能力_可顺利完成
            // 
            this.radio运动能力_可顺利完成.EditValue = true;
            this.radio运动能力_可顺利完成.Location = new System.Drawing.Point(3, 0);
            this.radio运动能力_可顺利完成.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio运动能力_可顺利完成.Name = "radio运动能力_可顺利完成";
            this.radio运动能力_可顺利完成.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio运动能力_可顺利完成.Properties.Appearance.Options.UseFont = true;
            this.radio运动能力_可顺利完成.Properties.Caption = "可顺利完成";
            this.radio运动能力_可顺利完成.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.radio运动能力_可顺利完成.Properties.RadioGroupIndex = 1;
            this.radio运动能力_可顺利完成.Size = new System.Drawing.Size(207, 23);
            this.radio运动能力_可顺利完成.TabIndex = 0;
            this.radio运动能力_可顺利完成.Tag = "1";
            // 
            // radio运动能力_无法完成
            // 
            this.radio运动能力_无法完成.Location = new System.Drawing.Point(3, 22);
            this.radio运动能力_无法完成.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio运动能力_无法完成.Name = "radio运动能力_无法完成";
            this.radio运动能力_无法完成.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio运动能力_无法完成.Properties.Appearance.Options.UseFont = true;
            this.radio运动能力_无法完成.Properties.Caption = "无法独立完成其中任何一个动作(上级医院就诊)";
            this.radio运动能力_无法完成.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.radio运动能力_无法完成.Properties.RadioGroupIndex = 1;
            this.radio运动能力_无法完成.Size = new System.Drawing.Size(385, 23);
            this.radio运动能力_无法完成.TabIndex = 1;
            this.radio运动能力_无法完成.TabStop = false;
            this.radio运动能力_无法完成.Tag = "2";
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel6.Controls.Add(this.labelControl1);
            this.flowLayoutPanel6.Controls.Add(this.txt左眼视力);
            this.flowLayoutPanel6.Controls.Add(this.labelControl2);
            this.flowLayoutPanel6.Controls.Add(this.txt右眼视力);
            this.flowLayoutPanel6.Controls.Add(this.labelControl3);
            this.flowLayoutPanel6.Controls.Add(this.txt矫正左眼视力);
            this.flowLayoutPanel6.Controls.Add(this.labelControl4);
            this.flowLayoutPanel6.Controls.Add(this.txt矫正右眼视力);
            this.flowLayoutPanel6.Controls.Add(this.labelControl5);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(85, 38);
            this.flowLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(529, 84);
            this.flowLayoutPanel6.TabIndex = 79;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(3, 1);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(47, 25);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "左眼";
            // 
            // txt左眼视力
            // 
            this.txt左眼视力.EnterMoveNextControl = true;
            this.txt左眼视力.Location = new System.Drawing.Point(56, 0);
            this.txt左眼视力.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt左眼视力.Name = "txt左眼视力";
            this.txt左眼视力.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt左眼视力.Properties.Appearance.Options.UseFont = true;
            this.txt左眼视力.Size = new System.Drawing.Size(60, 26);
            this.txt左眼视力.TabIndex = 0;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(122, 1);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(40, 25);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "右眼";
            // 
            // txt右眼视力
            // 
            this.txt右眼视力.EnterMoveNextControl = true;
            this.txt右眼视力.Location = new System.Drawing.Point(168, 0);
            this.txt右眼视力.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt右眼视力.Name = "txt右眼视力";
            this.txt右眼视力.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt右眼视力.Properties.Appearance.Options.UseFont = true;
            this.txt右眼视力.Size = new System.Drawing.Size(60, 26);
            this.txt右眼视力.TabIndex = 1;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Location = new System.Drawing.Point(234, 1);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(130, 25);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "(矫正视力：左眼";
            // 
            // txt矫正左眼视力
            // 
            this.txt矫正左眼视力.EnterMoveNextControl = true;
            this.txt矫正左眼视力.Location = new System.Drawing.Point(370, 0);
            this.txt矫正左眼视力.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt矫正左眼视力.Name = "txt矫正左眼视力";
            this.txt矫正左眼视力.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt矫正左眼视力.Properties.Appearance.Options.UseFont = true;
            this.txt矫正左眼视力.Size = new System.Drawing.Size(60, 26);
            this.txt矫正左眼视力.TabIndex = 2;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.Location = new System.Drawing.Point(436, 1);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(45, 25);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "右眼";
            // 
            // txt矫正右眼视力
            // 
            this.txt矫正右眼视力.EnterMoveNextControl = true;
            this.txt矫正右眼视力.Location = new System.Drawing.Point(3, 29);
            this.txt矫正右眼视力.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt矫正右眼视力.Name = "txt矫正右眼视力";
            this.txt矫正右眼视力.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt矫正右眼视力.Properties.Appearance.Options.UseFont = true;
            this.txt矫正右眼视力.Size = new System.Drawing.Size(60, 26);
            this.txt矫正右眼视力.TabIndex = 3;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.Location = new System.Drawing.Point(69, 30);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(42, 24);
            this.labelControl5.TabIndex = 8;
            this.labelControl5.Text = ")";
            // 
            // flow咽部
            // 
            this.flow咽部.BackColor = System.Drawing.Color.White;
            this.flow咽部.Controls.Add(this.radio咽部_无充血);
            this.flow咽部.Controls.Add(this.radio咽部_充血);
            this.flow咽部.Controls.Add(this.radio咽部_淋巴滤泡增生);
            this.flow咽部.Controls.Add(this.radio咽部_其他);
            this.flow咽部.Controls.Add(this.txt咽部_其他);
            this.flow咽部.Location = new System.Drawing.Point(65, 135);
            this.flow咽部.Name = "flow咽部";
            this.flow咽部.Size = new System.Drawing.Size(549, 89);
            this.flow咽部.TabIndex = 78;
            // 
            // radio咽部_无充血
            // 
            this.radio咽部_无充血.EditValue = true;
            this.radio咽部_无充血.Location = new System.Drawing.Point(3, 5);
            this.radio咽部_无充血.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.radio咽部_无充血.Name = "radio咽部_无充血";
            this.radio咽部_无充血.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio咽部_无充血.Properties.Appearance.Options.UseFont = true;
            this.radio咽部_无充血.Properties.Caption = "无充血";
            this.radio咽部_无充血.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.radio咽部_无充血.Properties.RadioGroupIndex = 0;
            this.radio咽部_无充血.Size = new System.Drawing.Size(106, 23);
            this.radio咽部_无充血.TabIndex = 0;
            this.radio咽部_无充血.Tag = "1";
            // 
            // radio咽部_充血
            // 
            this.radio咽部_充血.Location = new System.Drawing.Point(115, 5);
            this.radio咽部_充血.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.radio咽部_充血.Name = "radio咽部_充血";
            this.radio咽部_充血.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio咽部_充血.Properties.Appearance.Options.UseFont = true;
            this.radio咽部_充血.Properties.Caption = "充血";
            this.radio咽部_充血.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.radio咽部_充血.Properties.RadioGroupIndex = 0;
            this.radio咽部_充血.Size = new System.Drawing.Size(115, 23);
            this.radio咽部_充血.TabIndex = 1;
            this.radio咽部_充血.TabStop = false;
            this.radio咽部_充血.Tag = "2";
            // 
            // radio咽部_淋巴滤泡增生
            // 
            this.radio咽部_淋巴滤泡增生.Location = new System.Drawing.Point(236, 5);
            this.radio咽部_淋巴滤泡增生.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.radio咽部_淋巴滤泡增生.Name = "radio咽部_淋巴滤泡增生";
            this.radio咽部_淋巴滤泡增生.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio咽部_淋巴滤泡增生.Properties.Appearance.Options.UseFont = true;
            this.radio咽部_淋巴滤泡增生.Properties.Caption = "淋巴滤泡增生";
            this.radio咽部_淋巴滤泡增生.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.radio咽部_淋巴滤泡增生.Properties.RadioGroupIndex = 0;
            this.radio咽部_淋巴滤泡增生.Size = new System.Drawing.Size(191, 23);
            this.radio咽部_淋巴滤泡增生.TabIndex = 2;
            this.radio咽部_淋巴滤泡增生.TabStop = false;
            this.radio咽部_淋巴滤泡增生.Tag = "3";
            // 
            // radio咽部_其他
            // 
            this.radio咽部_其他.Location = new System.Drawing.Point(433, 5);
            this.radio咽部_其他.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.radio咽部_其他.Name = "radio咽部_其他";
            this.radio咽部_其他.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio咽部_其他.Properties.Appearance.Options.UseFont = true;
            this.radio咽部_其他.Properties.Caption = "其他";
            this.radio咽部_其他.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.radio咽部_其他.Properties.RadioGroupIndex = 0;
            this.radio咽部_其他.Size = new System.Drawing.Size(71, 23);
            this.radio咽部_其他.TabIndex = 3;
            this.radio咽部_其他.TabStop = false;
            this.radio咽部_其他.Tag = "4";
            this.radio咽部_其他.CheckedChanged += new System.EventHandler(this.radio咽部_其他_CheckedChanged);
            // 
            // txt咽部_其他
            // 
            this.txt咽部_其他.Enabled = false;
            this.txt咽部_其他.Location = new System.Drawing.Point(3, 27);
            this.txt咽部_其他.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.txt咽部_其他.Name = "txt咽部_其他";
            this.txt咽部_其他.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt咽部_其他.Properties.Appearance.Options.UseFont = true;
            this.txt咽部_其他.Properties.AutoHeight = false;
            this.txt咽部_其他.Size = new System.Drawing.Size(163, 34);
            this.txt咽部_其他.TabIndex = 4;
            // 
            // flow口唇
            // 
            this.flow口唇.BackColor = System.Drawing.Color.White;
            this.flow口唇.Controls.Add(this.radio口唇);
            this.flow口唇.Controls.Add(this.txt口唇其他);
            this.flow口唇.Location = new System.Drawing.Point(65, 38);
            this.flow口唇.Margin = new System.Windows.Forms.Padding(0);
            this.flow口唇.Name = "flow口唇";
            this.flow口唇.Size = new System.Drawing.Size(549, 93);
            this.flow口唇.TabIndex = 59;
            // 
            // radio口唇
            // 
            this.radio口唇.EditValue = "1";
            this.radio口唇.Location = new System.Drawing.Point(0, 0);
            this.radio口唇.Margin = new System.Windows.Forms.Padding(0);
            this.radio口唇.Name = "radio口唇";
            this.radio口唇.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radio口唇.Properties.Appearance.Options.UseFont = true;
            this.radio口唇.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "红润"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "苍白"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "发绀"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "皲裂"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("5", "疱疹"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("6", "其他")});
            this.radio口唇.Size = new System.Drawing.Size(428, 34);
            this.radio口唇.TabIndex = 0;
            this.radio口唇.SelectedIndexChanged += new System.EventHandler(this.radio口唇_SelectedIndexChanged);
            // 
            // txt口唇其他
            // 
            this.txt口唇其他.Enabled = false;
            this.txt口唇其他.Location = new System.Drawing.Point(3, 34);
            this.txt口唇其他.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.txt口唇其他.Name = "txt口唇其他";
            this.txt口唇其他.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt口唇其他.Properties.Appearance.Options.UseFont = true;
            this.txt口唇其他.Properties.AutoHeight = false;
            this.txt口唇其他.Size = new System.Drawing.Size(163, 34);
            this.txt口唇其他.TabIndex = 1;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.TCG_Tags,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.emptySpaceItem9});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(624, 423);
            this.layoutControlGroup1.Text = "健康体检表";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // TCG_Tags
            // 
            this.TCG_Tags.AppearanceItemCaption.Options.UseTextOptions = true;
            this.TCG_Tags.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TCG_Tags.AppearanceTabPage.Header.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TCG_Tags.AppearanceTabPage.Header.Options.UseFont = true;
            this.TCG_Tags.AppearanceTabPage.Header.Options.UseTextOptions = true;
            this.TCG_Tags.AppearanceTabPage.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TCG_Tags.AppearanceTabPage.HeaderActive.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.TCG_Tags.AppearanceTabPage.HeaderActive.ForeColor = System.Drawing.Color.Blue;
            this.TCG_Tags.AppearanceTabPage.HeaderActive.Options.UseFont = true;
            this.TCG_Tags.AppearanceTabPage.HeaderActive.Options.UseForeColor = true;
            this.TCG_Tags.AppearanceTabPage.HeaderActive.Options.UseTextOptions = true;
            this.TCG_Tags.AppearanceTabPage.HeaderActive.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TCG_Tags.CustomizationFormText = "tabbedControlGroup1";
            this.TCG_Tags.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TCG_Tags.Location = new System.Drawing.Point(0, 0);
            this.TCG_Tags.Name = "TCG_Tags";
            this.TCG_Tags.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.TCG_Tags.SelectedTabPage = this.LCG_齿列;
            this.TCG_Tags.SelectedTabPageIndex = 1;
            this.TCG_Tags.Size = new System.Drawing.Size(618, 335);
            this.TCG_Tags.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LCG_口唇咽部,
            this.LCG_齿列,
            this.LCG_视听,
            this.LCG_查体,
            this.LCG_肺,
            this.LCG_心脏,
            this.LCG_腹部,
            this.LCG_下肢水肿,
            this.LCG_足背动脉搏动});
            this.TCG_Tags.Text = "TCG_Tags";
            this.TCG_Tags.SelectedPageChanged += new DevExpress.XtraLayout.LayoutTabPageChangedEventHandler(this.TCG_Tags_SelectedPageChanged);
            // 
            // LCG_心脏
            // 
            this.LCG_心脏.CustomizationFormText = "心脏";
            this.LCG_心脏.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl心率,
            this.layoutControlItem53,
            this.layoutControlItem54,
            this.emptySpaceItem6});
            this.LCG_心脏.Location = new System.Drawing.Point(0, 0);
            this.LCG_心脏.Name = "LCG_心脏";
            this.LCG_心脏.Size = new System.Drawing.Size(608, 297);
            this.LCG_心脏.Text = "心脏";
            // 
            // lbl心率
            // 
            this.lbl心率.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl心率.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl心率.AppearanceItemCaption.Options.UseFont = true;
            this.lbl心率.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl心率.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl心率.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl心率.Control = this.txt心率;
            this.lbl心率.CustomizationFormText = "心率：";
            this.lbl心率.Location = new System.Drawing.Point(0, 0);
            this.lbl心率.MinSize = new System.Drawing.Size(199, 26);
            this.lbl心率.Name = "lbl心率";
            this.lbl心率.Size = new System.Drawing.Size(608, 43);
            this.lbl心率.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl心率.Text = "心率：";
            this.lbl心率.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl心率.TextSize = new System.Drawing.Size(90, 20);
            this.lbl心率.TextToControlDistance = 5;
            // 
            // layoutControlItem53
            // 
            this.layoutControlItem53.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem53.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem53.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem53.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem53.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem53.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem53.Control = this.radio心律;
            this.layoutControlItem53.CustomizationFormText = "心律：";
            this.layoutControlItem53.Location = new System.Drawing.Point(0, 43);
            this.layoutControlItem53.MinSize = new System.Drawing.Size(149, 26);
            this.layoutControlItem53.Name = "layoutControlItem53";
            this.layoutControlItem53.Size = new System.Drawing.Size(608, 55);
            this.layoutControlItem53.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem53.Text = "心律：";
            this.layoutControlItem53.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem53.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem53.TextToControlDistance = 5;
            // 
            // layoutControlItem54
            // 
            this.layoutControlItem54.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem54.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem54.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem54.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem54.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem54.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem54.Control = this.flowLayoutPanel15;
            this.layoutControlItem54.CustomizationFormText = "杂音：";
            this.layoutControlItem54.Location = new System.Drawing.Point(0, 98);
            this.layoutControlItem54.MinSize = new System.Drawing.Size(199, 26);
            this.layoutControlItem54.Name = "layoutControlItem54";
            this.layoutControlItem54.Size = new System.Drawing.Size(608, 63);
            this.layoutControlItem54.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem54.Text = "杂音：";
            this.layoutControlItem54.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem54.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem54.TextToControlDistance = 5;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 161);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(608, 136);
            this.emptySpaceItem6.Text = "emptySpaceItem6";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LCG_口唇咽部
            // 
            this.LCG_口唇咽部.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.LCG_口唇咽部.AppearanceGroup.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.LCG_口唇咽部.AppearanceGroup.Options.UseFont = true;
            this.LCG_口唇咽部.AppearanceGroup.Options.UseForeColor = true;
            this.LCG_口唇咽部.AppearanceGroup.Options.UseTextOptions = true;
            this.LCG_口唇咽部.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LCG_口唇咽部.CustomizationFormText = "脏器功能";
            this.LCG_口唇咽部.ExpandButtonVisible = true;
            this.LCG_口唇咽部.ExpandOnDoubleClick = true;
            this.LCG_口唇咽部.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem39,
            this.lbl咽部,
            this.emptySpaceItem1});
            this.LCG_口唇咽部.Location = new System.Drawing.Point(0, 0);
            this.LCG_口唇咽部.Name = "LCG_口唇咽部";
            this.LCG_口唇咽部.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.LCG_口唇咽部.Size = new System.Drawing.Size(608, 297);
            this.LCG_口唇咽部.Text = "口唇、咽部";
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem39.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem39.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem39.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem39.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem39.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem39.Control = this.flow口唇;
            this.layoutControlItem39.CustomizationFormText = "口唇";
            this.layoutControlItem39.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem39.MinSize = new System.Drawing.Size(215, 26);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(608, 97);
            this.layoutControlItem39.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem39.Text = "口唇";
            this.layoutControlItem39.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem39.TextSize = new System.Drawing.Size(50, 14);
            this.layoutControlItem39.TextToControlDistance = 5;
            // 
            // lbl咽部
            // 
            this.lbl咽部.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl咽部.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl咽部.AppearanceItemCaption.Options.UseFont = true;
            this.lbl咽部.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl咽部.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl咽部.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl咽部.Control = this.flow咽部;
            this.lbl咽部.CustomizationFormText = "咽部";
            this.lbl咽部.Location = new System.Drawing.Point(0, 97);
            this.lbl咽部.Name = "lbl咽部";
            this.lbl咽部.Size = new System.Drawing.Size(608, 93);
            this.lbl咽部.Text = "咽部";
            this.lbl咽部.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl咽部.TextSize = new System.Drawing.Size(50, 20);
            this.lbl咽部.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 190);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(608, 107);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LCG_齿列
            // 
            this.LCG_齿列.CustomizationFormText = "齿列";
            this.LCG_齿列.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem38,
            this.emptySpaceItem24,
            this.layoutControlItem140,
            this.layoutControlItem143,
            this.layoutControlItem145,
            this.layoutControlItem144,
            this.layoutControlItem146,
            this.layoutControlItem147,
            this.layoutControlItem141,
            this.layoutControlItem148,
            this.layoutControlItem150,
            this.layoutControlItem151,
            this.layoutControlItem149,
            this.layoutControlItem152,
            this.layoutControlItem154,
            this.layoutControlItem155,
            this.layoutControlItem153,
            this.layoutControlItem142,
            this.layoutControlItem158,
            this.emptySpaceItem2});
            this.LCG_齿列.Location = new System.Drawing.Point(0, 0);
            this.LCG_齿列.Name = "LCG_齿列";
            this.LCG_齿列.Size = new System.Drawing.Size(608, 297);
            this.LCG_齿列.Text = "齿列";
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.chk齿列_正常;
            this.layoutControlItem38.CustomizationFormText = "layoutControlItem38";
            this.layoutControlItem38.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem38.MaxSize = new System.Drawing.Size(60, 23);
            this.layoutControlItem38.MinSize = new System.Drawing.Size(60, 23);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(60, 23);
            this.layoutControlItem38.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem38.Text = "layoutControlItem38";
            this.layoutControlItem38.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem38.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem38.TextToControlDistance = 0;
            this.layoutControlItem38.TextVisible = false;
            // 
            // emptySpaceItem24
            // 
            this.emptySpaceItem24.AllowHotTrack = false;
            this.emptySpaceItem24.CustomizationFormText = "emptySpaceItem24";
            this.emptySpaceItem24.Location = new System.Drawing.Point(60, 0);
            this.emptySpaceItem24.Name = "emptySpaceItem24";
            this.emptySpaceItem24.Size = new System.Drawing.Size(548, 23);
            this.emptySpaceItem24.Text = "emptySpaceItem24";
            this.emptySpaceItem24.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem140
            // 
            this.layoutControlItem140.Control = this.chk齿列_缺齿;
            this.layoutControlItem140.CustomizationFormText = "layoutControlItem140";
            this.layoutControlItem140.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItem140.MaxSize = new System.Drawing.Size(60, 30);
            this.layoutControlItem140.MinSize = new System.Drawing.Size(60, 30);
            this.layoutControlItem140.Name = "layoutControlItem140";
            this.layoutControlItem140.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 10, 2);
            this.layoutControlItem140.Size = new System.Drawing.Size(60, 80);
            this.layoutControlItem140.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem140.Text = "layoutControlItem140";
            this.layoutControlItem140.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem140.TextToControlDistance = 0;
            this.layoutControlItem140.TextVisible = false;
            // 
            // layoutControlItem143
            // 
            this.layoutControlItem143.Control = this.chk齿列_义齿;
            this.layoutControlItem143.CustomizationFormText = "layoutControlItem143";
            this.layoutControlItem143.Location = new System.Drawing.Point(0, 103);
            this.layoutControlItem143.MaxSize = new System.Drawing.Size(60, 23);
            this.layoutControlItem143.MinSize = new System.Drawing.Size(60, 23);
            this.layoutControlItem143.Name = "layoutControlItem143";
            this.layoutControlItem143.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 10, 2);
            this.layoutControlItem143.Size = new System.Drawing.Size(60, 70);
            this.layoutControlItem143.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem143.Text = "layoutControlItem143";
            this.layoutControlItem143.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem143.TextToControlDistance = 0;
            this.layoutControlItem143.TextVisible = false;
            // 
            // layoutControlItem145
            // 
            this.layoutControlItem145.Control = this.txt缺齿3;
            this.layoutControlItem145.CustomizationFormText = "layoutControlItem145";
            this.layoutControlItem145.Location = new System.Drawing.Point(60, 63);
            this.layoutControlItem145.MaxSize = new System.Drawing.Size(100, 40);
            this.layoutControlItem145.MinSize = new System.Drawing.Size(100, 40);
            this.layoutControlItem145.Name = "layoutControlItem145";
            this.layoutControlItem145.Size = new System.Drawing.Size(100, 40);
            this.layoutControlItem145.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem145.Text = "layoutControlItem145";
            this.layoutControlItem145.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem145.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem145.TextToControlDistance = 0;
            this.layoutControlItem145.TextVisible = false;
            // 
            // layoutControlItem144
            // 
            this.layoutControlItem144.Control = this.txt缺齿1;
            this.layoutControlItem144.CustomizationFormText = "layoutControlItem144";
            this.layoutControlItem144.Location = new System.Drawing.Point(60, 23);
            this.layoutControlItem144.MaxSize = new System.Drawing.Size(100, 40);
            this.layoutControlItem144.MinSize = new System.Drawing.Size(100, 40);
            this.layoutControlItem144.Name = "layoutControlItem144";
            this.layoutControlItem144.Size = new System.Drawing.Size(100, 40);
            this.layoutControlItem144.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem144.Text = "layoutControlItem144";
            this.layoutControlItem144.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem144.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem144.TextToControlDistance = 0;
            this.layoutControlItem144.TextVisible = false;
            // 
            // layoutControlItem146
            // 
            this.layoutControlItem146.Control = this.txt缺齿2;
            this.layoutControlItem146.CustomizationFormText = "layoutControlItem146";
            this.layoutControlItem146.Location = new System.Drawing.Point(160, 23);
            this.layoutControlItem146.MaxSize = new System.Drawing.Size(100, 40);
            this.layoutControlItem146.MinSize = new System.Drawing.Size(100, 40);
            this.layoutControlItem146.Name = "layoutControlItem146";
            this.layoutControlItem146.Size = new System.Drawing.Size(100, 40);
            this.layoutControlItem146.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem146.Text = "layoutControlItem146";
            this.layoutControlItem146.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem146.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem146.TextToControlDistance = 0;
            this.layoutControlItem146.TextVisible = false;
            // 
            // layoutControlItem147
            // 
            this.layoutControlItem147.Control = this.txt缺齿4;
            this.layoutControlItem147.CustomizationFormText = "layoutControlItem147";
            this.layoutControlItem147.Location = new System.Drawing.Point(160, 63);
            this.layoutControlItem147.MaxSize = new System.Drawing.Size(100, 40);
            this.layoutControlItem147.MinSize = new System.Drawing.Size(100, 40);
            this.layoutControlItem147.Name = "layoutControlItem147";
            this.layoutControlItem147.Size = new System.Drawing.Size(100, 40);
            this.layoutControlItem147.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem147.Text = "layoutControlItem147";
            this.layoutControlItem147.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem147.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem147.TextToControlDistance = 0;
            this.layoutControlItem147.TextVisible = false;
            // 
            // layoutControlItem141
            // 
            this.layoutControlItem141.Control = this.chk齿列_龋齿;
            this.layoutControlItem141.CustomizationFormText = "layoutControlItem141";
            this.layoutControlItem141.Location = new System.Drawing.Point(260, 23);
            this.layoutControlItem141.MaxSize = new System.Drawing.Size(60, 90);
            this.layoutControlItem141.MinSize = new System.Drawing.Size(60, 70);
            this.layoutControlItem141.Name = "layoutControlItem141";
            this.layoutControlItem141.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 10, 2);
            this.layoutControlItem141.Size = new System.Drawing.Size(60, 80);
            this.layoutControlItem141.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem141.Text = "layoutControlItem141";
            this.layoutControlItem141.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem141.TextToControlDistance = 0;
            this.layoutControlItem141.TextVisible = false;
            // 
            // layoutControlItem148
            // 
            this.layoutControlItem148.Control = this.txt龋齿1;
            this.layoutControlItem148.CustomizationFormText = "layoutControlItem148";
            this.layoutControlItem148.Location = new System.Drawing.Point(320, 23);
            this.layoutControlItem148.MaxSize = new System.Drawing.Size(100, 40);
            this.layoutControlItem148.MinSize = new System.Drawing.Size(100, 40);
            this.layoutControlItem148.Name = "layoutControlItem148";
            this.layoutControlItem148.Size = new System.Drawing.Size(100, 40);
            this.layoutControlItem148.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem148.Text = "layoutControlItem148";
            this.layoutControlItem148.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem148.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem148.TextToControlDistance = 0;
            this.layoutControlItem148.TextVisible = false;
            // 
            // layoutControlItem150
            // 
            this.layoutControlItem150.Control = this.txt龋齿3;
            this.layoutControlItem150.CustomizationFormText = "layoutControlItem150";
            this.layoutControlItem150.Location = new System.Drawing.Point(320, 63);
            this.layoutControlItem150.MaxSize = new System.Drawing.Size(100, 40);
            this.layoutControlItem150.MinSize = new System.Drawing.Size(100, 40);
            this.layoutControlItem150.Name = "layoutControlItem150";
            this.layoutControlItem150.Size = new System.Drawing.Size(100, 40);
            this.layoutControlItem150.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem150.Text = "layoutControlItem150";
            this.layoutControlItem150.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem150.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem150.TextToControlDistance = 0;
            this.layoutControlItem150.TextVisible = false;
            // 
            // layoutControlItem151
            // 
            this.layoutControlItem151.Control = this.txt龋齿4;
            this.layoutControlItem151.CustomizationFormText = "layoutControlItem151";
            this.layoutControlItem151.Location = new System.Drawing.Point(420, 63);
            this.layoutControlItem151.MaxSize = new System.Drawing.Size(100, 40);
            this.layoutControlItem151.MinSize = new System.Drawing.Size(100, 40);
            this.layoutControlItem151.Name = "layoutControlItem151";
            this.layoutControlItem151.Size = new System.Drawing.Size(188, 40);
            this.layoutControlItem151.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem151.Text = "layoutControlItem151";
            this.layoutControlItem151.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem151.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem151.TextToControlDistance = 0;
            this.layoutControlItem151.TextVisible = false;
            // 
            // layoutControlItem149
            // 
            this.layoutControlItem149.Control = this.txt龋齿2;
            this.layoutControlItem149.CustomizationFormText = "layoutControlItem149";
            this.layoutControlItem149.Location = new System.Drawing.Point(420, 23);
            this.layoutControlItem149.MaxSize = new System.Drawing.Size(100, 40);
            this.layoutControlItem149.MinSize = new System.Drawing.Size(100, 40);
            this.layoutControlItem149.Name = "layoutControlItem149";
            this.layoutControlItem149.Size = new System.Drawing.Size(188, 40);
            this.layoutControlItem149.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem149.Text = "layoutControlItem149";
            this.layoutControlItem149.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem149.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem149.TextToControlDistance = 0;
            this.layoutControlItem149.TextVisible = false;
            // 
            // layoutControlItem152
            // 
            this.layoutControlItem152.Control = this.txt义齿1;
            this.layoutControlItem152.CustomizationFormText = "layoutControlItem152";
            this.layoutControlItem152.Location = new System.Drawing.Point(60, 103);
            this.layoutControlItem152.MaxSize = new System.Drawing.Size(100, 45);
            this.layoutControlItem152.MinSize = new System.Drawing.Size(100, 35);
            this.layoutControlItem152.Name = "layoutControlItem152";
            this.layoutControlItem152.Size = new System.Drawing.Size(100, 35);
            this.layoutControlItem152.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem152.Text = "layoutControlItem152";
            this.layoutControlItem152.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem152.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem152.TextToControlDistance = 0;
            this.layoutControlItem152.TextVisible = false;
            // 
            // layoutControlItem154
            // 
            this.layoutControlItem154.Control = this.txt义齿3;
            this.layoutControlItem154.CustomizationFormText = "layoutControlItem154";
            this.layoutControlItem154.Location = new System.Drawing.Point(60, 138);
            this.layoutControlItem154.MaxSize = new System.Drawing.Size(100, 35);
            this.layoutControlItem154.MinSize = new System.Drawing.Size(100, 35);
            this.layoutControlItem154.Name = "layoutControlItem154";
            this.layoutControlItem154.Size = new System.Drawing.Size(100, 35);
            this.layoutControlItem154.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem154.Text = "layoutControlItem154";
            this.layoutControlItem154.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem154.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem154.TextToControlDistance = 0;
            this.layoutControlItem154.TextVisible = false;
            // 
            // layoutControlItem155
            // 
            this.layoutControlItem155.Control = this.txt义齿4;
            this.layoutControlItem155.CustomizationFormText = "layoutControlItem155";
            this.layoutControlItem155.Location = new System.Drawing.Point(160, 138);
            this.layoutControlItem155.MaxSize = new System.Drawing.Size(100, 35);
            this.layoutControlItem155.MinSize = new System.Drawing.Size(100, 35);
            this.layoutControlItem155.Name = "layoutControlItem155";
            this.layoutControlItem155.Size = new System.Drawing.Size(100, 35);
            this.layoutControlItem155.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem155.Text = "layoutControlItem155";
            this.layoutControlItem155.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem155.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem155.TextToControlDistance = 0;
            this.layoutControlItem155.TextVisible = false;
            // 
            // layoutControlItem153
            // 
            this.layoutControlItem153.Control = this.txt义齿2;
            this.layoutControlItem153.CustomizationFormText = "layoutControlItem153";
            this.layoutControlItem153.Location = new System.Drawing.Point(160, 103);
            this.layoutControlItem153.MaxSize = new System.Drawing.Size(100, 45);
            this.layoutControlItem153.MinSize = new System.Drawing.Size(100, 35);
            this.layoutControlItem153.Name = "layoutControlItem153";
            this.layoutControlItem153.Size = new System.Drawing.Size(100, 35);
            this.layoutControlItem153.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem153.Text = "layoutControlItem153";
            this.layoutControlItem153.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem153.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem153.TextToControlDistance = 0;
            this.layoutControlItem153.TextVisible = false;
            // 
            // layoutControlItem142
            // 
            this.layoutControlItem142.Control = this.chk齿列_其他;
            this.layoutControlItem142.CustomizationFormText = "layoutControlItem142";
            this.layoutControlItem142.Location = new System.Drawing.Point(260, 103);
            this.layoutControlItem142.MaxSize = new System.Drawing.Size(60, 23);
            this.layoutControlItem142.MinSize = new System.Drawing.Size(60, 23);
            this.layoutControlItem142.Name = "layoutControlItem142";
            this.layoutControlItem142.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 10, 2);
            this.layoutControlItem142.Size = new System.Drawing.Size(60, 70);
            this.layoutControlItem142.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem142.Text = "layoutControlItem142";
            this.layoutControlItem142.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem142.TextToControlDistance = 0;
            this.layoutControlItem142.TextVisible = false;
            // 
            // layoutControlItem158
            // 
            this.layoutControlItem158.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem158.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem158.Control = this.txt齿列_其他;
            this.layoutControlItem158.CustomizationFormText = "layoutControlItem158";
            this.layoutControlItem158.Location = new System.Drawing.Point(320, 103);
            this.layoutControlItem158.MaxSize = new System.Drawing.Size(0, 40);
            this.layoutControlItem158.MinSize = new System.Drawing.Size(100, 40);
            this.layoutControlItem158.Name = "layoutControlItem158";
            this.layoutControlItem158.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 10, 2);
            this.layoutControlItem158.Size = new System.Drawing.Size(288, 70);
            this.layoutControlItem158.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem158.Text = "layoutControlItem158";
            this.layoutControlItem158.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem158.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem158.TextToControlDistance = 0;
            this.layoutControlItem158.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 173);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(608, 124);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LCG_视听
            // 
            this.LCG_视听.CustomizationFormText = "视、听";
            this.LCG_视听.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl视力,
            this.lbl听力,
            this.lbl运动能力,
            this.emptySpaceItem3});
            this.LCG_视听.Location = new System.Drawing.Point(0, 0);
            this.LCG_视听.Name = "LCG_视听";
            this.LCG_视听.Size = new System.Drawing.Size(608, 297);
            this.LCG_视听.Text = "视、听";
            // 
            // lbl视力
            // 
            this.lbl视力.AppearanceItemCaption.BorderColor = System.Drawing.Color.White;
            this.lbl视力.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl视力.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl视力.AppearanceItemCaption.Options.UseBorderColor = true;
            this.lbl视力.AppearanceItemCaption.Options.UseFont = true;
            this.lbl视力.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl视力.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl视力.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl视力.Control = this.flowLayoutPanel6;
            this.lbl视力.CustomizationFormText = "视力";
            this.lbl视力.Location = new System.Drawing.Point(0, 0);
            this.lbl视力.Name = "lbl视力";
            this.lbl视力.Size = new System.Drawing.Size(608, 88);
            this.lbl视力.Text = "视力";
            this.lbl视力.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl视力.TextSize = new System.Drawing.Size(70, 20);
            this.lbl视力.TextToControlDistance = 5;
            // 
            // lbl听力
            // 
            this.lbl听力.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl听力.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl听力.AppearanceItemCaption.Options.UseFont = true;
            this.lbl听力.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl听力.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl听力.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl听力.Control = this.flow听力;
            this.lbl听力.CustomizationFormText = "听力";
            this.lbl听力.Location = new System.Drawing.Point(0, 88);
            this.lbl听力.Name = "lbl听力";
            this.lbl听力.Size = new System.Drawing.Size(608, 85);
            this.lbl听力.Text = "听力";
            this.lbl听力.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl听力.TextSize = new System.Drawing.Size(70, 20);
            this.lbl听力.TextToControlDistance = 5;
            // 
            // lbl运动能力
            // 
            this.lbl运动能力.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl运动能力.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl运动能力.AppearanceItemCaption.Options.UseFont = true;
            this.lbl运动能力.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl运动能力.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl运动能力.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl运动能力.Control = this.flow运动功能;
            this.lbl运动能力.CustomizationFormText = "运动能力";
            this.lbl运动能力.Location = new System.Drawing.Point(0, 173);
            this.lbl运动能力.Name = "lbl运动能力";
            this.lbl运动能力.Size = new System.Drawing.Size(608, 24);
            this.lbl运动能力.Text = "运动能力";
            this.lbl运动能力.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl运动能力.TextSize = new System.Drawing.Size(70, 20);
            this.lbl运动能力.TextToControlDistance = 5;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 197);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(608, 100);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LCG_查体
            // 
            this.LCG_查体.CustomizationFormText = "查体";
            this.LCG_查体.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem45,
            this.lbl皮肤,
            this.lbl巩膜,
            this.lbl淋巴结,
            this.emptySpaceItem4});
            this.LCG_查体.Location = new System.Drawing.Point(0, 0);
            this.LCG_查体.Name = "LCG_查体";
            this.LCG_查体.Size = new System.Drawing.Size(608, 297);
            this.LCG_查体.Text = "查体";
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem45.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem45.Control = this.flowLayoutPanel9;
            this.layoutControlItem45.CustomizationFormText = "眼底";
            this.layoutControlItem45.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem45.MinSize = new System.Drawing.Size(199, 26);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(608, 50);
            this.layoutControlItem45.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem45.Text = "眼底";
            this.layoutControlItem45.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem45.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem45.TextToControlDistance = 5;
            // 
            // lbl皮肤
            // 
            this.lbl皮肤.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl皮肤.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl皮肤.AppearanceItemCaption.Options.UseFont = true;
            this.lbl皮肤.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl皮肤.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl皮肤.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl皮肤.Control = this.flow皮肤;
            this.lbl皮肤.CustomizationFormText = "皮肤";
            this.lbl皮肤.Location = new System.Drawing.Point(0, 50);
            this.lbl皮肤.MinSize = new System.Drawing.Size(199, 26);
            this.lbl皮肤.Name = "lbl皮肤";
            this.lbl皮肤.Size = new System.Drawing.Size(608, 84);
            this.lbl皮肤.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl皮肤.Text = "皮肤";
            this.lbl皮肤.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl皮肤.TextSize = new System.Drawing.Size(90, 20);
            this.lbl皮肤.TextToControlDistance = 5;
            // 
            // lbl巩膜
            // 
            this.lbl巩膜.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.lbl巩膜.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl巩膜.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl巩膜.AppearanceItemCaption.Options.UseFont = true;
            this.lbl巩膜.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl巩膜.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl巩膜.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl巩膜.Control = this.flow巩膜;
            this.lbl巩膜.CustomizationFormText = "巩膜";
            this.lbl巩膜.Location = new System.Drawing.Point(0, 134);
            this.lbl巩膜.MinSize = new System.Drawing.Size(199, 26);
            this.lbl巩膜.Name = "lbl巩膜";
            this.lbl巩膜.Size = new System.Drawing.Size(608, 37);
            this.lbl巩膜.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl巩膜.Text = "巩膜";
            this.lbl巩膜.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl巩膜.TextSize = new System.Drawing.Size(90, 20);
            this.lbl巩膜.TextToControlDistance = 5;
            // 
            // lbl淋巴结
            // 
            this.lbl淋巴结.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl淋巴结.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl淋巴结.AppearanceItemCaption.Options.UseFont = true;
            this.lbl淋巴结.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl淋巴结.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl淋巴结.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl淋巴结.Control = this.flow淋巴结;
            this.lbl淋巴结.CustomizationFormText = "淋巴结";
            this.lbl淋巴结.Location = new System.Drawing.Point(0, 171);
            this.lbl淋巴结.MinSize = new System.Drawing.Size(199, 26);
            this.lbl淋巴结.Name = "lbl淋巴结";
            this.lbl淋巴结.Size = new System.Drawing.Size(608, 26);
            this.lbl淋巴结.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl淋巴结.Text = "淋巴结";
            this.lbl淋巴结.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl淋巴结.TextSize = new System.Drawing.Size(90, 20);
            this.lbl淋巴结.TextToControlDistance = 5;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 197);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(608, 100);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LCG_肺
            // 
            this.LCG_肺.CustomizationFormText = "肺";
            this.LCG_肺.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem49,
            this.layoutControlItem50,
            this.lbl罗音,
            this.emptySpaceItem5});
            this.LCG_肺.Location = new System.Drawing.Point(0, 0);
            this.LCG_肺.Name = "LCG_肺";
            this.LCG_肺.Size = new System.Drawing.Size(608, 297);
            this.LCG_肺.Text = "肺";
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem49.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem49.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem49.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem49.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem49.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem49.Control = this.radio桶状胸;
            this.layoutControlItem49.CustomizationFormText = "桶状胸：";
            this.layoutControlItem49.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem49.MinSize = new System.Drawing.Size(149, 26);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Size = new System.Drawing.Size(608, 55);
            this.layoutControlItem49.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem49.Text = "桶状胸：";
            this.layoutControlItem49.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem49.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem49.TextToControlDistance = 5;
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem50.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem50.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem50.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem50.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem50.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem50.Control = this.flowLayoutPanel13;
            this.layoutControlItem50.CustomizationFormText = "呼吸音：";
            this.layoutControlItem50.Location = new System.Drawing.Point(0, 55);
            this.layoutControlItem50.MinSize = new System.Drawing.Size(199, 26);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Size = new System.Drawing.Size(608, 64);
            this.layoutControlItem50.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem50.Text = "呼吸音：";
            this.layoutControlItem50.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem50.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem50.TextToControlDistance = 5;
            // 
            // lbl罗音
            // 
            this.lbl罗音.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl罗音.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl罗音.AppearanceItemCaption.Options.UseFont = true;
            this.lbl罗音.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl罗音.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl罗音.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl罗音.Control = this.flow罗音;
            this.lbl罗音.CustomizationFormText = "罗音：";
            this.lbl罗音.Location = new System.Drawing.Point(0, 119);
            this.lbl罗音.MinSize = new System.Drawing.Size(199, 26);
            this.lbl罗音.Name = "lbl罗音";
            this.lbl罗音.Size = new System.Drawing.Size(608, 51);
            this.lbl罗音.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl罗音.Text = "罗音：";
            this.lbl罗音.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl罗音.TextSize = new System.Drawing.Size(90, 20);
            this.lbl罗音.TextToControlDistance = 5;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 170);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(608, 127);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LCG_腹部
            // 
            this.LCG_腹部.CustomizationFormText = "腹部";
            this.LCG_腹部.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem55,
            this.layoutControlItem56,
            this.layoutControlItem57,
            this.layoutControlItem58,
            this.layoutControlItem59,
            this.emptySpaceItem7});
            this.LCG_腹部.Location = new System.Drawing.Point(0, 0);
            this.LCG_腹部.Name = "LCG_腹部";
            this.LCG_腹部.Size = new System.Drawing.Size(608, 297);
            this.LCG_腹部.Text = "腹部";
            // 
            // layoutControlItem55
            // 
            this.layoutControlItem55.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem55.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem55.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem55.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem55.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem55.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem55.Control = this.flowLayoutPanel16;
            this.layoutControlItem55.CustomizationFormText = "压痛：";
            this.layoutControlItem55.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem55.MinSize = new System.Drawing.Size(199, 26);
            this.layoutControlItem55.Name = "layoutControlItem55";
            this.layoutControlItem55.Size = new System.Drawing.Size(608, 51);
            this.layoutControlItem55.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem55.Text = "压痛：";
            this.layoutControlItem55.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem55.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem55.TextToControlDistance = 5;
            // 
            // layoutControlItem56
            // 
            this.layoutControlItem56.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem56.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem56.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem56.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem56.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem56.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem56.Control = this.flowLayoutPanel17;
            this.layoutControlItem56.CustomizationFormText = "包块：";
            this.layoutControlItem56.Location = new System.Drawing.Point(0, 51);
            this.layoutControlItem56.MinSize = new System.Drawing.Size(199, 26);
            this.layoutControlItem56.Name = "layoutControlItem56";
            this.layoutControlItem56.Size = new System.Drawing.Size(608, 52);
            this.layoutControlItem56.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem56.Text = "包块：";
            this.layoutControlItem56.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem56.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem56.TextToControlDistance = 5;
            // 
            // layoutControlItem57
            // 
            this.layoutControlItem57.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem57.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem57.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem57.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem57.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem57.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem57.Control = this.flowLayoutPanel18;
            this.layoutControlItem57.CustomizationFormText = "肝大：";
            this.layoutControlItem57.Location = new System.Drawing.Point(0, 103);
            this.layoutControlItem57.MinSize = new System.Drawing.Size(199, 26);
            this.layoutControlItem57.Name = "layoutControlItem57";
            this.layoutControlItem57.Size = new System.Drawing.Size(608, 43);
            this.layoutControlItem57.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem57.Text = "肝大：";
            this.layoutControlItem57.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem57.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem57.TextToControlDistance = 5;
            // 
            // layoutControlItem58
            // 
            this.layoutControlItem58.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem58.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem58.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem58.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem58.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem58.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem58.Control = this.flowLayoutPanel19;
            this.layoutControlItem58.CustomizationFormText = "脾大：";
            this.layoutControlItem58.Location = new System.Drawing.Point(0, 146);
            this.layoutControlItem58.MinSize = new System.Drawing.Size(199, 26);
            this.layoutControlItem58.Name = "layoutControlItem58";
            this.layoutControlItem58.Size = new System.Drawing.Size(608, 26);
            this.layoutControlItem58.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem58.Text = "脾大：";
            this.layoutControlItem58.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem58.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem58.TextToControlDistance = 5;
            // 
            // layoutControlItem59
            // 
            this.layoutControlItem59.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem59.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem59.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem59.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem59.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem59.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem59.Control = this.flowLayoutPanel20;
            this.layoutControlItem59.CustomizationFormText = "移动性浊音：";
            this.layoutControlItem59.Location = new System.Drawing.Point(0, 172);
            this.layoutControlItem59.MinSize = new System.Drawing.Size(199, 26);
            this.layoutControlItem59.Name = "layoutControlItem59";
            this.layoutControlItem59.Size = new System.Drawing.Size(608, 26);
            this.layoutControlItem59.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem59.Text = "移动性浊音：";
            this.layoutControlItem59.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem59.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem59.TextToControlDistance = 5;
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 198);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(608, 99);
            this.emptySpaceItem7.Text = "emptySpaceItem7";
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LCG_下肢水肿
            // 
            this.LCG_下肢水肿.CustomizationFormText = "其他";
            this.LCG_下肢水肿.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem60,
            this.emptySpaceItem10});
            this.LCG_下肢水肿.Location = new System.Drawing.Point(0, 0);
            this.LCG_下肢水肿.Name = "LCG_下肢水肿";
            this.LCG_下肢水肿.Size = new System.Drawing.Size(608, 297);
            this.LCG_下肢水肿.Text = "下肢水肿";
            // 
            // layoutControlItem60
            // 
            this.layoutControlItem60.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem60.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem60.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem60.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem60.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem60.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem60.Control = this.radio下肢水肿;
            this.layoutControlItem60.CustomizationFormText = "下肢水肿";
            this.layoutControlItem60.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem60.MaxSize = new System.Drawing.Size(500, 0);
            this.layoutControlItem60.MinSize = new System.Drawing.Size(134, 26);
            this.layoutControlItem60.Name = "layoutControlItem60";
            this.layoutControlItem60.Size = new System.Drawing.Size(608, 166);
            this.layoutControlItem60.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem60.Text = "下肢水肿";
            this.layoutControlItem60.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem60.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem60.TextToControlDistance = 5;
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem10";
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 166);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(608, 131);
            this.emptySpaceItem10.Text = "emptySpaceItem10";
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LCG_足背动脉搏动
            // 
            this.LCG_足背动脉搏动.CustomizationFormText = "足背动脉搏动";
            this.LCG_足背动脉搏动.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem8,
            this.layoutControlItem61});
            this.LCG_足背动脉搏动.Location = new System.Drawing.Point(0, 0);
            this.LCG_足背动脉搏动.Name = "LCG_足背动脉搏动";
            this.LCG_足背动脉搏动.Size = new System.Drawing.Size(608, 297);
            this.LCG_足背动脉搏动.Text = "足背动脉搏动";
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 166);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(608, 131);
            this.emptySpaceItem8.Text = "emptySpaceItem8";
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem61
            // 
            this.layoutControlItem61.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem61.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem61.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem61.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem61.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem61.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem61.Control = this.radio足背动脉搏动;
            this.layoutControlItem61.CustomizationFormText = "足背动脉搏动";
            this.layoutControlItem61.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem61.MaxSize = new System.Drawing.Size(500, 0);
            this.layoutControlItem61.MinSize = new System.Drawing.Size(134, 26);
            this.layoutControlItem61.Name = "layoutControlItem61";
            this.layoutControlItem61.Size = new System.Drawing.Size(608, 166);
            this.layoutControlItem61.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem61.Text = "足背动脉搏动";
            this.layoutControlItem61.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem61.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem61.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btn上一项;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 375);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(309, 42);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btn下一项;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(309, 375);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(309, 42);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 335);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(618, 40);
            this.emptySpaceItem9.Text = "emptySpaceItem9";
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.layoutControl2);
            this.panelControl7.Controls.Add(this.flowLayoutPanel53);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(0, 0);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(624, 48);
            this.panelControl7.TabIndex = 1;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.lookUpEdit_医师签字);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Left;
            this.layoutControl2.Location = new System.Drawing.Point(2, 2);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(284, 44);
            this.layoutControl2.TabIndex = 6;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // lookUpEdit_医师签字
            // 
            this.lookUpEdit_医师签字.Location = new System.Drawing.Point(90, 3);
            this.lookUpEdit_医师签字.Name = "lookUpEdit_医师签字";
            this.lookUpEdit_医师签字.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit_医师签字.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit_医师签字.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit_医师签字.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit_医师签字.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_医师签字.Properties.NullText = "";
            this.lookUpEdit_医师签字.Size = new System.Drawing.Size(191, 36);
            this.lookUpEdit_医师签字.StyleController = this.layoutControl2;
            this.lookUpEdit_医师签字.TabIndex = 4;
            this.lookUpEdit_医师签字.EditValueChanged += new System.EventHandler(this.lookUpEdit_医师签字_EditValueChanged);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem_医师签字});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(284, 44);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem_医师签字
            // 
            this.layoutControlItem_医师签字.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem_医师签字.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem_医师签字.Control = this.lookUpEdit_医师签字;
            this.layoutControlItem_医师签字.CustomizationFormText = "医师签字";
            this.layoutControlItem_医师签字.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem_医师签字.Name = "layoutControlItem_医师签字";
            this.layoutControlItem_医师签字.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem_医师签字.Size = new System.Drawing.Size(284, 44);
            this.layoutControlItem_医师签字.Text = "医师签字";
            this.layoutControlItem_医师签字.TextSize = new System.Drawing.Size(84, 24);
            // 
            // flowLayoutPanel53
            // 
            this.flowLayoutPanel53.Controls.Add(this.btn保存);
            this.flowLayoutPanel53.Controls.Add(this.sBut_Get参照数据);
            this.flowLayoutPanel53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel53.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel53.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel53.Name = "flowLayoutPanel53";
            this.flowLayoutPanel53.Size = new System.Drawing.Size(620, 44);
            this.flowLayoutPanel53.TabIndex = 5;
            // 
            // btn保存
            // 
            this.btn保存.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn保存.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn保存.Appearance.Options.UseFont = true;
            this.btn保存.Appearance.Options.UseForeColor = true;
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(515, 0);
            this.btn保存.Margin = new System.Windows.Forms.Padding(0);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(105, 41);
            this.btn保存.TabIndex = 3;
            this.btn保存.Text = "保  存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // sBut_Get参照数据
            // 
            this.sBut_Get参照数据.Appearance.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.sBut_Get参照数据.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.sBut_Get参照数据.Appearance.Options.UseFont = true;
            this.sBut_Get参照数据.Appearance.Options.UseForeColor = true;
            this.sBut_Get参照数据.Image = ((System.Drawing.Image)(resources.GetObject("sBut_Get参照数据.Image")));
            this.sBut_Get参照数据.Location = new System.Drawing.Point(329, 3);
            this.sBut_Get参照数据.Name = "sBut_Get参照数据";
            this.sBut_Get参照数据.Size = new System.Drawing.Size(183, 38);
            this.sBut_Get参照数据.TabIndex = 4;
            this.sBut_Get参照数据.Text = "获取去年数据参照";
            this.sBut_Get参照数据.Click += new System.EventHandler(this.sBut_Get参照数据_Click);
            // 
            // uc健康体检表3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl7);
            this.Name = "uc健康体检表3";
            this.Size = new System.Drawing.Size(624, 471);
            this.Load += new System.EventHandler(this.UC健康体检表_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt齿列_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk齿列_义齿.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk齿列_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk齿列_龋齿.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk齿列_缺齿.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk齿列_正常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio足背动脉搏动.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio下肢水肿.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio心律.Properties)).EndInit();
            this.flowLayoutPanel20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio移动性浊音.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt移动性浊音.Properties)).EndInit();
            this.flowLayoutPanel19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio脾大.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脾大.Properties)).EndInit();
            this.flowLayoutPanel18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio肝大.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt肝大.Properties)).EndInit();
            this.flowLayoutPanel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio包块.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt包块.Properties)).EndInit();
            this.flowLayoutPanel16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio压痛.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt压痛.Properties)).EndInit();
            this.flowLayoutPanel15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio杂音.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt杂音.Properties)).EndInit();
            this.flowLayoutPanel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio呼吸音.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt呼吸音_异常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio桶状胸.Properties)).EndInit();
            this.flow巩膜.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk巩膜_正常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk巩膜_黄染.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk巩膜_充血.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk巩膜_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt巩膜_其他.Properties)).EndInit();
            this.flow皮肤.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_正常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_潮红.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_苍白.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_发绀.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_黄染.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_色素沉着.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt皮肤_其他.Properties)).EndInit();
            this.flow罗音.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk罗音_无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk罗音_干罗音.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk罗音_湿罗音.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk罗音_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt罗音_其他.Properties)).EndInit();
            this.flowLayoutPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio眼底.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt眼底.Properties)).EndInit();
            this.flow听力.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio听力_听见.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio听力_听不清.Properties)).EndInit();
            this.flow淋巴结.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk淋巴结_未触及.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk淋巴结_锁骨上.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk淋巴结_腋窝.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk淋巴结_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt淋巴结_其他.Properties)).EndInit();
            this.flow运动功能.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio运动能力_可顺利完成.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio运动能力_无法完成.Properties)).EndInit();
            this.flowLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt左眼视力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt右眼视力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt矫正左眼视力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt矫正右眼视力.Properties)).EndInit();
            this.flow咽部.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio咽部_无充血.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio咽部_充血.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio咽部_淋巴滤泡增生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio咽部_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt咽部_其他.Properties)).EndInit();
            this.flow口唇.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio口唇.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt口唇其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TCG_Tags)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_心脏)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl心率)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_口唇咽部)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl咽部)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_齿列)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem140)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem143)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem145)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem144)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem146)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem147)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem141)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem148)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem150)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem151)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem149)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem152)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem154)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem155)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem153)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem142)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem158)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_视听)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl视力)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl听力)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl运动能力)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_查体)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl皮肤)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl巩膜)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl淋巴结)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_肺)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl罗音)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_腹部)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_下肢水肿)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG_足背动脉搏动)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_医师签字.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_医师签字)).EndInit();
            this.flowLayoutPanel53.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private System.Windows.Forms.FlowLayoutPanel flow口唇;
        private DevExpress.XtraEditors.RadioGroup radio口唇;
        private DevExpress.XtraEditors.TextEdit txt口唇其他;
        private System.Windows.Forms.FlowLayoutPanel flow咽部;
        private DevExpress.XtraEditors.CheckEdit radio咽部_无充血;
        private DevExpress.XtraEditors.CheckEdit radio咽部_充血;
        private DevExpress.XtraEditors.CheckEdit radio咽部_淋巴滤泡增生;
        private DevExpress.XtraEditors.CheckEdit radio咽部_其他;
        private DevExpress.XtraEditors.TextEdit txt咽部_其他;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txt左眼视力;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txt右眼视力;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txt矫正左眼视力;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txt矫正右眼视力;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private System.Windows.Forms.FlowLayoutPanel flow运动功能;
        private DevExpress.XtraEditors.CheckEdit radio运动能力_可顺利完成;
        private DevExpress.XtraEditors.CheckEdit radio运动能力_无法完成;
        private System.Windows.Forms.FlowLayoutPanel flow听力;
        private DevExpress.XtraEditors.CheckEdit radio听力_听见;
        private DevExpress.XtraEditors.CheckEdit radio听力_听不清;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private DevExpress.XtraEditors.RadioGroup radio眼底;
        private DevExpress.XtraEditors.TextEdit txt眼底;
        private System.Windows.Forms.FlowLayoutPanel flow皮肤;
        private DevExpress.XtraEditors.CheckEdit chk皮肤_正常;
        private DevExpress.XtraEditors.CheckEdit chk皮肤_潮红;
        private DevExpress.XtraEditors.CheckEdit chk皮肤_苍白;
        private DevExpress.XtraEditors.CheckEdit chk皮肤_发绀;
        private DevExpress.XtraEditors.CheckEdit chk皮肤_黄染;
        private DevExpress.XtraEditors.CheckEdit chk皮肤_色素沉着;
        private DevExpress.XtraEditors.CheckEdit chk皮肤_其他;
        private DevExpress.XtraEditors.TextEdit txt皮肤_其他;
        private System.Windows.Forms.FlowLayoutPanel flow巩膜;
        private DevExpress.XtraEditors.CheckEdit chk巩膜_正常;
        private DevExpress.XtraEditors.CheckEdit chk巩膜_黄染;
        private DevExpress.XtraEditors.CheckEdit chk巩膜_充血;
        private DevExpress.XtraEditors.CheckEdit chk巩膜_其他;
        private DevExpress.XtraEditors.TextEdit txt巩膜_其他;
        private System.Windows.Forms.FlowLayoutPanel flow淋巴结;
        private DevExpress.XtraEditors.CheckEdit chk淋巴结_未触及;
        private DevExpress.XtraEditors.CheckEdit chk淋巴结_锁骨上;
        private DevExpress.XtraEditors.CheckEdit chk淋巴结_腋窝;
        private DevExpress.XtraEditors.CheckEdit chk淋巴结_其他;
        private DevExpress.XtraEditors.TextEdit txt淋巴结_其他;
        private DevExpress.XtraEditors.RadioGroup radio桶状胸;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel13;
        private DevExpress.XtraEditors.RadioGroup radio呼吸音;
        private DevExpress.XtraEditors.TextEdit txt呼吸音_异常;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private System.Windows.Forms.FlowLayoutPanel flow罗音;
        private DevExpress.XtraEditors.CheckEdit chk罗音_无;
        private DevExpress.XtraEditors.CheckEdit chk罗音_干罗音;
        private DevExpress.XtraEditors.CheckEdit chk罗音_湿罗音;
        private DevExpress.XtraEditors.CheckEdit chk罗音_其他;
        private DevExpress.XtraEditors.TextEdit txt罗音_其他;
        private DevExpress.XtraLayout.LayoutControlItem lbl罗音;
        private DevExpress.XtraEditors.RadioGroup radio心律;
        private UCTxtLbl txt心率;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel15;
        private DevExpress.XtraEditors.RadioGroup radio杂音;
        private DevExpress.XtraEditors.TextEdit txt杂音;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel16;
        private DevExpress.XtraEditors.RadioGroup radio压痛;
        private DevExpress.XtraEditors.TextEdit txt压痛;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel20;
        private DevExpress.XtraEditors.TextEdit txt移动性浊音;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel19;
        private DevExpress.XtraEditors.TextEdit txt脾大;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel18;
        private DevExpress.XtraEditors.TextEdit txt肝大;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel17;
        private DevExpress.XtraEditors.RadioGroup radio包块;
        private DevExpress.XtraEditors.TextEdit txt包块;
        private DevExpress.XtraEditors.RadioGroup radio下肢水肿;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem60;
        private DevExpress.XtraEditors.RadioGroup radio足背动脉搏动;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem61;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel53;
        private DevExpress.XtraEditors.RadioGroup radio移动性浊音;
        private DevExpress.XtraEditors.RadioGroup radio脾大;
        private DevExpress.XtraEditors.RadioGroup radio肝大;
        private DevExpress.XtraEditors.TextEdit txt龋齿4;
        private DevExpress.XtraEditors.TextEdit txt龋齿3;
        private DevExpress.XtraEditors.TextEdit txt龋齿2;
        private DevExpress.XtraEditors.TextEdit txt龋齿1;
        private DevExpress.XtraEditors.TextEdit txt缺齿4;
        private DevExpress.XtraEditors.TextEdit txt缺齿2;
        private DevExpress.XtraEditors.TextEdit txt缺齿3;
        private DevExpress.XtraEditors.TextEdit txt缺齿1;
        private DevExpress.XtraEditors.CheckEdit chk齿列_义齿;
        private DevExpress.XtraEditors.CheckEdit chk齿列_其他;
        private DevExpress.XtraEditors.CheckEdit chk齿列_龋齿;
        private DevExpress.XtraEditors.CheckEdit chk齿列_缺齿;
        private DevExpress.XtraEditors.CheckEdit chk齿列_正常;
        private DevExpress.XtraEditors.TextEdit txt义齿1;
        private DevExpress.XtraEditors.TextEdit txt齿列_其他;
        private DevExpress.XtraEditors.TextEdit txt义齿4;
        private DevExpress.XtraEditors.TextEdit txt义齿3;
        private DevExpress.XtraEditors.TextEdit txt义齿2;
        private DevExpress.XtraLayout.TabbedControlGroup TCG_Tags;
        private DevExpress.XtraLayout.LayoutControlGroup LCG_口唇咽部;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem lbl咽部;
        private DevExpress.XtraLayout.LayoutControlGroup LCG_视听;
        private DevExpress.XtraLayout.LayoutControlItem lbl视力;
        private DevExpress.XtraLayout.LayoutControlItem lbl听力;
        private DevExpress.XtraLayout.LayoutControlItem lbl运动能力;
        private DevExpress.XtraLayout.LayoutControlGroup LCG_查体;
        private DevExpress.XtraLayout.LayoutControlGroup LCG_肺;
        private DevExpress.XtraLayout.LayoutControlGroup LCG_心脏;
        private DevExpress.XtraLayout.LayoutControlGroup LCG_腹部;
        private DevExpress.XtraLayout.LayoutControlGroup LCG_下肢水肿;
        private DevExpress.XtraLayout.LayoutControlItem lbl心率;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem53;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem54;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraLayout.LayoutControlItem lbl皮肤;
        private DevExpress.XtraLayout.LayoutControlItem lbl巩膜;
        private DevExpress.XtraLayout.LayoutControlItem lbl淋巴结;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem55;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem56;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem57;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem58;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem59;
        private DevExpress.XtraLayout.LayoutControlGroup LCG_齿列;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem140;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem143;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem145;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem144;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem146;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem147;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem141;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem148;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem150;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem151;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem149;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem152;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem154;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem155;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem153;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem142;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem158;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraEditors.SimpleButton btn下一项;
        private DevExpress.XtraEditors.SimpleButton btn上一项;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.LayoutControlGroup LCG_足背动脉搏动;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraEditors.SimpleButton sBut_Get参照数据;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_医师签字;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_医师签字;
    }
}