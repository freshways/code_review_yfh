﻿using AtomEHR.Library;
using ATOMEHR_LeaveClient.SqlSugar_DALs;
using ATOMEHR_LeaveClient.SqlSugar_Models;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ATOMEHR_LeaveClient
{

    public partial class uc健康体检表2 : UserControl
    {
        /*
     1.    症状选项对应的  code值   zz_zhengzhuang
         *ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
                    10002498	zz_zhengzhuang	症状	1	WZZ、MZZ	无症状		1
                    10002621	zz_zhengzhuang	症状	2	TT	头痛		1
                    10002624	zz_zhengzhuang	症状	3	TY	头晕		1
                    10002625	zz_zhengzhuang	症状	4	XJ	心悸		1
                    10002626	zz_zhengzhuang	症状	5	XM	胸闷		1
                    10002627	zz_zhengzhuang	症状	6	XT	胸痛		1
                    10002628	zz_zhengzhuang	症状	7	MXKS、MXHS	慢性咳嗽		1
                    10002629	zz_zhengzhuang	症状	8	KT、HT	咳痰		1
                    10003084	zz_zhengzhuang	症状	9	HXKN	呼吸困难		1
                    10003085	zz_zhengzhuang	症状	10	DY	多饮		1
                    10003086	zz_zhengzhuang	症状	11	DS、DN	多尿		1
                    10003087	zz_zhengzhuang	症状	12	TZXX、TCXX、TZXJ、	体重下降		1
                    10003088	zz_zhengzhuang	症状	13	FL	乏力		1
                    10003089	zz_zhengzhuang	症状	14	GJZT	关节肿痛		1
                    10003090	zz_zhengzhuang	症状	15	SLMH	视力模糊		1
                    10003091	zz_zhengzhuang	症状	16	SJMM	手脚麻木		1
                    10003092	zz_zhengzhuang	症状	17	SJ、NJ	尿急		1
                    10003093	zz_zhengzhuang	症状	18	ST、NT	尿痛		1
                    10003094	zz_zhengzhuang	症状	19	PM、BM、PL、BL、PB、	便秘		1
                    10003105	zz_zhengzhuang	症状	20	FX	腹泻		1
                    10003106	zz_zhengzhuang	症状	21	WXOT、EXOT	恶心呕吐		1
                    10003107	zz_zhengzhuang	症状	22	YH	眼花		1
                    10003108	zz_zhengzhuang	症状	23	EM	耳鸣		1
                    10003109	zz_zhengzhuang	症状	24	RFZT	乳房胀痛		1
                    10003110	zz_zhengzhuang	症状	99	QT、JT	其他		1
         * 
         * 
       2.  锻炼频率  dlpl
         *ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10003299	dlpl	锻炼频率	1		每天		1
            10003300	dlpl	锻炼频率	2		每周一次以上		1
            10003301	dlpl	锻炼频率	3		偶尔		1
            10003302	dlpl	锻炼频率	4		不锻炼		1
         * 
         * 
         *3. 饮食习惯  ysxg
         ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10002888	ysxg	饮食习惯	5		嗜油		1
            10002889	ysxg	饮食习惯	6		嗜糖		1
            10002890	ysxg	饮食习惯	1		荤素均衡		1
            10002891	ysxg	饮食习惯	2		荤食为主		1
            10002892	ysxg	饮食习惯	3		素食为主		1
            10002893	ysxg	饮食习惯	4		嗜盐		1
         * 
         * 
         4.吸烟情况 
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10003482	glkxyqk	管理卡吸烟情况	1		不吸烟		1
            10003483	glkxyqk	管理卡吸烟情况	2		已戒烟		1
            10003484	glkxyqk	管理卡吸烟情况	3		吸烟		1   
         * 
         5.饮酒频率  yjpl
         *ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10002832	yjpl	饮酒频率	1		从不		1
            10002833	yjpl	饮酒频率	2		偶尔		1
            10002834	yjpl	饮酒频率	3		经常		1
            10002835	yjpl	饮酒频率	4		每天		1
         * 
         6.是否戒酒
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10003111	sfjj	是否戒酒	1	WJJ	未戒酒		1
            10003112	sfjj	是否戒酒	2	YJJ	已戒酒		1
         * 
         7.饮酒种类
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
        10002836	yjzl	饮酒种类	1		白酒		1
        10002837	yjzl	饮酒种类	2		啤酒		1
        10002838	yjzl	饮酒种类	3		红酒		1
        10002839	yjzl	饮酒种类	4		黄酒		1
        10002840	yjzl	饮酒种类	99		其他		1
         * 
         8.口唇
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10002072	kc_houchun	口唇	1		红润		1
            10002073	kc_houchun	口唇	2		苍白		1
            10002074	kc_houchun	口唇	3		发绀		1
            10002075	kc_houchun	口唇	4		皲裂		1
            10002076	kc_houchun	口唇	5		疱疹		1
            10002077	kc_houchun	口唇	6	QT,JT	其他		1
         * 
         9.咽部
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10002748	yb_yanbu	咽部	1		无充血		1
            10002749	yb_yanbu	咽部	2		充血		1
            10002750	yb_yanbu	咽部	3		淋巴滤泡增生		1
            10002751	yb_yanbu	咽部	4	QT,JT	其他		1
         * 
         10.听力
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10002583	tingli	听力	1	TJ、TX	听见		1
            10002584	tingli	听力	2	TBQHWFTJ（EBHKZK	听不清或无法听见（耳鼻喉科专科就诊）		1
         * 
         11.运动功能
            10002894	yundong	运动功能	1	KSLWC	可顺利完成		1
            10002895	yundong	运动功能	2	WFDLWCQZRHYGDZ(	无法独立完成其中任何一个动作(上级医院就诊)		1
         * 
         12.正常异常
            10002936	zcyc	正常异常	1		正常		1
            10002937	zcyc	正常异常	2		异常		1
         * 
        13.下肢水肿
            10002739	xzsz	下肢水肿	4		双侧不对称		1
            10002740	xzsz	下肢水肿	5		双侧对称		1
            10002736	xzsz	下肢水肿	1		无		1
            10002737	xzsz	下肢水肿	2		左侧		1
            10002738	xzsz	下肢水肿	3		右侧		1
         * 
         14.
            10002932	zbdmbd	足背动脉搏动	1		未触及		1
            10002933	zbdmbd	足背动脉搏动	2		触及双侧对称		1
            10002934	zbdmbd	足背动脉搏动	3		触及左侧弱或消失		1
            10002935	zbdmbd	足背动脉搏动	4		触及右侧弱或消失		1
         * 
         15.肛门指诊
            10002492	gmzz	肛门指诊	1	WJYC、WXYC	未见异常		1
            10002493	gmzz	肛门指诊	2	CT	触痛		1
            10003051	gmzz	肛门指诊	3	BK	包块		1
            10003052	gmzz	肛门指诊	4	QLXYC	前列腺异常		1
            10003053	gmzz	肛门指诊	99	QT、JT	其他		1
         * 
         16.乳腺
            10002447	rx_ruxian	乳腺	1		未见异常		1
            10002448	rx_ruxian	乳腺	2		乳房切除		1
            10002449	rx_ruxian	乳腺	3		异常泌乳		1
            10002450	rx_ruxian	乳腺	4		乳腺包块		1
            10002451	rx_ruxian	乳腺	99		其他		1
         * 
         17.阴性阳性
            10002905	yxyx	阴性阳性	1		阴性		1
            10002906	yxyx	阴性阳性	2		阳性		1
         * 
         18.中药体质辨识
            10003047	zytzbs	中药体质辨识	1		是		1
            10003048	zytzbs	中药体质辨识	2		倾向是		1
            10003049	zytzbsphz	中药体质辨识平和质	1		是		1
            10003050	zytzbsphz	中药体质辨识平和质	2		基本是		1
         * 
         19.脑血管疾病
            10002290	nxgjb	脑血管疾病	1		未发现		1
            10002291	nxgjb	脑血管疾病	2		缺血性卒中		1
            10002292	nxgjb	脑血管疾病	3		脑出血		1
            10002293	nxgjb	脑血管疾病	4		蛛网膜下腔出血		1
            10002294	nxgjb	脑血管疾病	5		短暂性脑缺血发作		1
            10002295	nxgjb	脑血管疾病	99		其他		1
         * 
         20.肾脏疾病
            10002564	szjb	肾脏疾病	1		未发现		1
            10002565	szjb	肾脏疾病	2		糖尿病肾病		1
            10002566	szjb	肾脏疾病	3		肾功能衰竭		1
            10002567	szjb	肾脏疾病	4		急性肾炎		1
            10002568	szjb	肾脏疾病	5		慢性肾炎		1
            10002569	szjb	肾脏疾病	99		其他		1
         * 
         21.心脏疾病
            10002725	xzjb	心脏疾病	1		未发现		1
            10002726	xzjb	心脏疾病	2		心肌梗死		1
            10002727	xzjb	心脏疾病	3		心绞痛		1
            10002728	xzjb	心脏疾病	4		冠状动脉血运重建		1
            10002729	xzjb	心脏疾病	5		充血性心力衰竭		1
            10002730	xzjb	心脏疾病	6		心前区疼痛		1
            10002731	xzjb	心脏疾病	99		其他		1
         * 
         22.血管疾病
            10002678	xgjb	血管疾病	1		未发现		1
            10002679	xgjb	血管疾病	2		夹层动脉瘤		1
            10002680	xgjb	血管疾病	3		动脉闭塞性疾病		1
            10002681	xgjb	血管疾病	99		其他		1
         * 
         23.眼部疾病
            10002752	ybjb	眼部疾病	1		未发现		1
            10002753	ybjb	眼部疾病	2		视网膜出血或渗出		1
            10002754	ybjb	眼部疾病	3		视乳头水肿		1
            10002755	ybjb	眼部疾病	4		白内障		1
            10002756	ybjb	眼部疾病	99		其他		1
         * 
         24.未发现有
            10002630	wfxy	未发现有	1		未发现		1
            10002631	wfxy	未发现有	2		有		1
         * 
         25.齿列
                10003219	cl_chilei	齿列	1		正常		1
                10003220	cl_chilei	齿列	2		缺齿		1
                10003221	cl_chilei	齿列	3		龋齿		1
                10003222	cl_chilei	齿列	4		义齿(假牙)		1
                10003223	cl_chilei	齿列	5	QT,JT	其他		1
         * 
         26.体检有无异常
                10002585	tjywyc	体检有无异常	1		体检无异常		1
                10002586	tjywyc	体检有无异常	2		有异常		1
         * 
         27.体检有无异常
                10002585	tjywyc	体检有无异常	1		体检无异常		1
                10002586	tjywyc	体检有无异常	2		有异常		1
         * 
         28.健康指导
                10001896	jkzd	健康指导	1		定期随访		1
                10001897	jkzd	健康指导	2		纳入慢性病管理		1
                10001898	jkzd	健康指导	3		建议复查		1
                10001899	jkzd	健康指导	4		建议转诊		1
         * 
         29.危险因素指导
                10002653	wxyszd	危险因素指导	1		戒烟		1
                10002654	wxyszd	危险因素指导	2		健康饮酒		1
                10002655	wxyszd	危险因素指导	3		饮食		1
                10002656	wxyszd	危险因素指导	4		锻炼		1
                10002657	wxyszd	危险因素指导	97		减体重		1
                10002658	wxyszd	危险因素指导	98		建议疫苗接种		1
                10002659	wxyszd	危险因素指导	99		其他		1
         * 
         30.老年人状况评估
                10002990	zk_pg	老年人状况评估	1	NULL	满意	NULL	1
                10002991	zk_pg	老年人状况评估	2	NULL	基本满意	NULL	1
                10002992	zk_pg	老年人状况评估	3	NULL	说不清楚	NULL	1
                10002993	zk_pg	老年人状况评估	4	NULL	不太满意	NULL	1
                10002994	zk_pg	老年人状况评估	5	NULL	不满意	NULL	1
         * 
         31.老年人自理评估
                10002995	zl_pg	老年人自理评估	1	NULL	可自理(0～3分)	NULL	1
                10002996	zl_pg	老年人自理评估	2	NULL	轻度依赖(4～8分)	NULL	1
                10002997	zl_pg	老年人自理评估	3	NULL	中度依赖(9～18分)	NULL	1
                10002998	zl_pg	老年人自理评估	4	NULL	不能自理(≥19分)	NULL	1
         * 
         32.阴性阳性
                10002905	yxyx	阴性阳性	1		阴性		1
                10002906	yxyx	阴性阳性	2		阳性		1
         * 
         33.职业暴露详细
                10003131	baoluqk	职业暴露详细	1	W,M	无		1
                10003132	baoluqk	职业暴露详细	2	BX	不详		1
                10003133	baoluqk	职业暴露详细	3	Y	有		1
         * 
         34. pifu_pf  皮肤
            10002382	pifu_pf	皮肤	1		正常		1
            10002383	pifu_pf	皮肤	2		潮红		1
            10002384	pifu_pf	皮肤	3		苍白		1
            10002385	pifu_pf	皮肤	4		发绀		1
            10002386	pifu_pf	皮肤	5		黄染		1
            10002387	pifu_pf	皮肤	6		色素沉着		1
            10002388	pifu_pf	皮肤	99		其他		1
         * 
         35.
            10003492	gm_gongmo	巩膜	1		正常		1
            10003493	gm_gongmo	巩膜	2		黄染		1
            10003494	gm_gongmo	巩膜	3		充血		1
            10003495	gm_gongmo	巩膜	99		其他		1
         * 
         36.lbj2
         * 10002110  	lbj2	淋巴结	1		未触及		1
            10002111	lbj2	淋巴结	2		锁骨上		1
            10002112	lbj2	淋巴结	3		腋窝		1
            10002113	lbj2	淋巴结	4		其他		1
         * 
         37 . luoyin
         * 10002145	    luoyin	罗音	1	W、M	无		1
            10002146	luoyin	罗音	2	GLY	干罗音		1
            10002619	luoyin	罗音	3	SLY	湿罗音		1
            10002620	luoyin	罗音	4	QT、JT	其他		1
         * 
         38.心脏心律
         * 10002741 	xzxl	心脏心律	1		齐		1
            10002742	xzxl	心脏心律	2		不齐		1
            10002743	xzxl	心脏心律	3		绝对不齐		1
         * 
         39.
         * 10002667 	wy_wuyou	无有	1		无		1
            10002668	wy_wuyou	无有	2		有		1
            10002896	yw_youwu	有无	1		有		1
            10002897	yw_youwu	有无	2		无		1
         * 
        40.
         * 10002898 	ywyc	有无异常	1		未见异常		1
            10002899	ywyc	有无异常	99		异常		1
         */

        #region Fields
        List<tb_健康体检_非免疫规划预防接种史Info> list免疫;
        List<tb_健康体检_用药情况表Info> list用药;
        List<tb_健康体检_住院史Info> list住院史;
        List<tb_健康体检_住院史Info> list建床史;
        DataSet dataSet免疫;
        DataSet dataSet用药;
        DataSet dataSet住院史;
        DataSet dataSet建床史;
        #endregion

        #region Constructor
        public uc健康体检表2()
        {
            InitializeComponent();
            tb_健康体检Info tb = tb_健康体检DAL.Gettb_健康体检InfoById(Program.currentUser.ID);
            //yfsccs.SqlHelper.FillDataset(yfsccs.SqlHelper.GetConnSting, CommandType.Text, "", ds免疫, "tb_健康体检_非免疫规划预防接种史");
            //string sql1 = "select * from tb_健康体检_非免疫规划预防接种史 where 1=1 and 身份证号 = @CardNo";
            //string sql2 = " select * from tb_健康体检_用药情况表 where 1=1 and 身份证号 = @CardNo";
            //string sql3 = " select * from tb_健康体检_住院史 where 1=1 and 类型 = '1' and 身份证号 = @CardNo";
            //string sql4 = " select * from tb_健康体检_住院史 where 1=1 and 类型 = '2' and 身份证号 = @CardNo";
            //SqlParameter meter = new SqlParameter("@CardNo", Program.currentUser.ID);
            //dataSet免疫 = yfsccs.SqlHelper.ExecuteDataset(sql1, new SqlParameter[] { meter });
            //dataSet用药 = yfsccs.SqlHelper.ExecuteDataset(sql2, new SqlParameter[] { meter });
            //dataSet住院史 = yfsccs.SqlHelper.ExecuteDataset(sql3, new SqlParameter[] { meter });
            //dataSet建床史 = yfsccs.SqlHelper.ExecuteDataset(sql4, new SqlParameter[] { meter });
            //dataSet = getDataSet();
            list免疫 = tb_非免疫规划DAL.GetInfoList("  身份证号 = '" + Program.currentUser.ID + "'");
            list用药 = tb_健康体检_用药情况表DAL.GetInfoList("  身份证号 = '" + Program.currentUser.ID + "'");
            list住院史 = tb_健康体检_住院史DAL.GetInfoList("  身份证号 = '" + Program.currentUser.ID + "' and 类型 = '1'");
            list建床史 = tb_健康体检_住院史DAL.GetInfoList("  身份证号 = '" + Program.currentUser.ID + "' and 类型 = '2'");
            //this.text出生日期.Text = currentUser.Birth;
            //this.text居住地址.Text = currentUser.Addr;
            //this.txt性别.Text = currentUser.Gender;
            //text身份证号.Text = currentUser.ID;
            //this.text姓名.Text = currentUser.Name;

            this.gc建床史.DataSource = list建床史;
            this.gc接种史.DataSource = list免疫;
            this.gc用药情况.DataSource = list用药;
            this.gc住院史.DataSource = list住院史;



            if (tb != null)//已经查体
            {
                DobindingDatasource(tb);

                if (list住院史.Count > 0)
                {
                    this.radio住院史.EditValue = "1";
                }
                else
                {
                    this.radio住院史.EditValue = "2";
                }
                if (list建床史.Count > 0)
                {
                    this.radio家庭病床史.EditValue = "1";
                }
                else
                {
                    this.radio家庭病床史.EditValue = "2";
                }
                if (list用药.Count > 0)
                {
                    this.radio主要用药情况.EditValue = "1";
                }
                else
                {
                    this.radio主要用药情况.EditValue = "2";
                }
                if (list免疫.Count > 0)
                {
                    this.radio预防接种史.EditValue = "1";
                }
                else
                {
                    this.radio预防接种史.EditValue = "2";
                }
            }
        }

        #endregion

        #region Handle Events
        private void UC健康体检表_Load(object sender, EventArgs e)
        {
            InitView();

            DataBinder.BindingLookupEditDataSource(lkp服药依从性, DataDictCache.Cache.t服药依从性, "P_DESC", "P_CODE");

            Binding_医师签字();
        }

        //Begin WXF 2018-11-21 | 15:57
        //绑定医师签字 
        private void Binding_医师签字()
        {
            dal_医生信息 dal医生信息 = new dal_医生信息();
            DataTable dt = dal医生信息.Get医生信息ToLookUp();

            lookUpEdit_医师签字.Properties.ValueMember = "b编码";
            lookUpEdit_医师签字.Properties.DisplayMember = "x医生姓名";
            lookUpEdit_医师签字.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("x医生姓名", "医生姓名"));
            lookUpEdit_医师签字.Properties.DataSource = dt;
            lookUpEdit_医师签字.EditValue = "请选择";
        }
        //End
        private void chk_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit checkEdit = (CheckEdit)sender;
            if (checkEdit == null) return;
            SetControlEnable(checkEdit);
        }
        private void chk其他_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit checkEdit = (CheckEdit)sender;
            if (checkEdit == null) return;
            string name = checkEdit.Name;
            string target = "txt" + name.Substring(3, name.IndexOf('_') - 2) + "其他";
            Control[] list = this.Controls.Find(target, true);
            if (list.Length == 1)
            {
                if (list[0] is TextEdit)
                {
                    TextEdit txt = (TextEdit)list[0];
                    SetTextEditEnable(checkEdit, txt);
                }
            }

            //Begin WXF 2018-11-15 | 15:20
            //控制每个疾病模块内的"其他"的快捷选项
            ControlCollection Control_All = checkEdit.Parent.Controls;

            if (Control_All != null && Control_All.Count > 0)
            {
                foreach (Control item in Control_All)
                {
                    if (item is CheckEdit && item.Name.ToString().Substring(0, 6).Equals("chktxt"))
                    {
                        CheckEdit ce = (CheckEdit)item;
                        ce.Enabled = checkEdit.Checked;
                        if (!checkEdit.Checked)
                        {
                            ce.Checked = false;
                        }
                    }
                }
            }
            //End
        }

        private void btn住院史添加_Click(object sender, EventArgs e)
        {

            //DataRow dr = dataSet住院史.Tables[0].Rows.Add();
            //dr["个人档案编号"] = Program.currentUser.DocNo;
            //dr["类型"] = "1";
            //dr["身份证号"] = Program.currentUser.ID;
            //this.gc住院史.RefreshDataSource();

            tb_健康体检_住院史Info newTb = new tb_健康体检_住院史Info();
            list住院史.Add(newTb);
            newTb.类型 = "1";
            newTb.身份证号 = Program.currentUser.ID;
            newTb.个人档案编号 = Program.currentUser.DocNo;
            this.gc住院史.RefreshDataSource();
        }
        private void btn住院史删除_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("是否要删除当前行数据?", "删除", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.Yes) return;
            this.gv住院史.DeleteRow(this.gv住院史.FocusedRowHandle);
            this.gc住院史.RefreshDataSource();
        }
        private void btn病床史添加_Click(object sender, EventArgs e)
        {
            //DataRow dr = dataSet建床史.Tables[0].Rows.Add();
            //dr["个人档案编号"] = Program.currentUser.DocNo;
            //dr["类型"] = "2";
            //dr["身份证号"] = Program.currentUser.ID;
            //this.gc建床史.RefreshDataSource();

            tb_健康体检_住院史Info newTb = new tb_健康体检_住院史Info();
            list建床史.Add(newTb);
            newTb.个人档案编号 = Program.currentUser.DocNo;
            newTb.类型 = "2";
            newTb.身份证号 = Program.currentUser.ID;
            this.gc建床史.RefreshDataSource();
        }
        private void btn病床史删除_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("是否要删除当前行数据?", "删除", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.Yes) return;
            this.gv建床史.DeleteRow(this.gv住院史.FocusedRowHandle);
            this.gc建床史.RefreshDataSource();
        }
        private void btn添加用药情况_Click(object sender, EventArgs e)
        {
            //DataRow dr = dataSet用药.Tables["0"].Rows.Add();
            //dr["个人档案编号"] = Program.currentUser.DocNo;
            //dr["身份证号"] = Program.currentUser.ID;
            //this.gc用药情况.RefreshDataSource();

            tb_健康体检_用药情况表Info newTb = new tb_健康体检_用药情况表Info();
            newTb.身份证号 = Program.currentUser.ID;
            newTb.个人档案编号 = Program.currentUser.DocNo;
            list用药.Add(newTb);
            this.gc用药情况.RefreshDataSource();

            //Begin WXF 2019-03-20 | 19:08
            //列宽自适应 
            this.gv用药情况.BestFitColumns();
            this.gv用药情况.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gv用药情况.OptionsView.ColumnAutoWidth = false;
            //End
        }
        private void btn删除用药情况_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("是否要删除当前行数据?", "删除", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.Yes) return;
            this.gv用药情况.DeleteRow(this.gv用药情况.FocusedRowHandle);
            this.gc用药情况.RefreshDataSource();
        }
        private void btn预防接种史添加_Click(object sender, EventArgs e)
        {
            //DataRow dr = dataSet免疫.Tables["0"].Rows.Add();
            //dr["个人档案编号"] = Program.currentUser.DocNo;
            //dr["身份证号"] = Program.currentUser.ID;
            //this.gc接种史.RefreshDataSource();

            tb_健康体检_非免疫规划预防接种史Info newTb = new tb_健康体检_非免疫规划预防接种史Info();
            newTb.身份证号 = Program.currentUser.ID;
            newTb.个人档案编号 = Program.currentUser.DocNo;
            list免疫.Add(newTb);
            this.gc接种史.RefreshDataSource();
        }
        private void btn预防接种史删除_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("是否要删除当前行数据?", "删除", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.Yes) return;
            this.gv接种史.DeleteRow(this.gv接种史.FocusedRowHandle);
            this.gc接种史.RefreshDataSource();
        }

        private void radio住院史_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio住院史.EditValue == null ? "" : this.radio住院史.EditValue.ToString();
            if (index == "1")//有
            {
                this.btn住院史添加.Enabled = this.btn住院史删除.Enabled = true;
                //this.lblgc住院史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
            else
            {
                this.btn住院史添加.Enabled = this.btn住院史删除.Enabled = false;
            }
        }
        private void radio家庭病床史_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio家庭病床史.EditValue == null ? "" : this.radio家庭病床史.EditValue.ToString();
            if (index == "1")//有
            {
                this.btn病床史添加.Enabled = this.btn病床史删除.Enabled = true;
            }
            else
            {
                this.btn病床史添加.Enabled = this.btn病床史删除.Enabled = false;
            }
        }
        private void radio预防接种史_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio预防接种史.EditValue == null ? "" : this.radio预防接种史.EditValue.ToString();
            if (index == "1")//有
            {
                this.btn预防接种史添加.Enabled = this.btn预防接种史删除.Enabled = true;
            }
            else
            {
                this.btn预防接种史添加.Enabled = this.btn预防接种史删除.Enabled = false;
            }
        }
        private void radio主要用药情况_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio主要用药情况.EditValue == null ? "" : this.radio主要用药情况.EditValue.ToString();
            if (index == "1")//有
            {
                this.btn添加用药情况.Enabled = this.btn删除用药情况.Enabled = true;
            }
            else
            {
                this.btn添加用药情况.Enabled = this.btn删除用药情况.Enabled = false;
            }
        }
        private void btn保存_Click(object sender, EventArgs e)
        {
            try
            {
                if (f保存检查())
                {
                    tb_健康体检Info tb = new tb_健康体检Info();

                    tb.身份证号 = Program.currentUser.ID;
                    tb.姓名 = Program.currentUser.Name;
                    tb.性别 = Program.currentUser.Gender;
                    tb.出生日期 = Program.currentUser.Birth;
                    tb.个人档案编号 = Program.currentUser.DocNo;
                    //return;

                    #region 现存主要健康问题
                    tb.脑血管疾病 = ControlsHelper.GetFlowLayoutResult(flow脑血管疾病).ToString();
                    tb.脑血管疾病其他 = this.txt脑血管疾病_其他.Text.Trim();
                    tb.肾脏疾病 = ControlsHelper.GetFlowLayoutResult(flow肾脏疾病).ToString();
                    tb.肾脏疾病其他 = this.txt肾脏疾病_其他.Text.Trim();
                    //Begin WXF 2019-03-29 | 17:45
                    //排除其他中的便捷选项 
                    tb.心脏疾病 = ControlsHelper.GetFlowLayoutResult(flow心脏疾病).ToString().Replace(",99_1", "");
                    //End
                    tb.心脏疾病其他 = this.txt心脏疾病_其他.Text.Trim();
                    tb.眼部疾病 = ControlsHelper.GetFlowLayoutResult(flow眼部疾病).ToString();
                    tb.眼部疾病其他 = this.txt眼部疾病_其他.Text.Trim();

                    //Begin WXF 2018-11-15 | 14:32
                    //@@UpdateValue 
                    tb.神经系统疾病 = ControlsHelper.GetFlowLayoutResult(flow神经系统疾病).ToString();
                    tb.神经系统疾病其他 = this.txt神经系统疾病_其他.Text.Trim();
                    tb.其他系统疾病 = ControlsHelper.GetFlowLayoutResult(flow其他系统疾病).ToString();
                    if (chktxt心脏疾病_冠心病.Checked)
                    {
                        tb.其他系统疾病其他 = chktxt心脏疾病_冠心病.Text + (string.IsNullOrEmpty(txt其他系统疾病_其他.Text.Trim()) ? txt其他系统疾病_其他.Text.Trim() : ("," + txt其他系统疾病_其他.Text.Trim()));
                    }
                    else
                    {
                        tb.其他系统疾病其他 = this.txt其他系统疾病_其他.Text.Trim();
                    }
                    //End

                    //Begin WXF 2018-11-21 | 21:12
                    //保存医师签字 
                    //if (string.IsNullOrEmpty(doctor_现存主要健康问题) || doctor_现存主要健康问题.Equals("请选择"))
                    //{
                    //    MessageBox.Show("请选择现存主要健康问题的责任医师!");
                    //    return;
                    //}
                    //if (string.IsNullOrEmpty(doctor_主要用药情况) || doctor_主要用药情况.Equals("请选择"))
                    //{
                    //    MessageBox.Show("请选择主要用药情况的责任医师!");
                    //    return;
                    //}
                    //if (string.IsNullOrEmpty(doctor_非免疫规划预防接种史) || doctor_非免疫规划预防接种史.Equals("请选择"))
                    //{
                    //    MessageBox.Show("请选择非免疫规划预防接种史的责任医师!");
                    //    return;
                    //}

                    Dictionary<tb_健康体检DAL.Module_体检, string> dic = new Dictionary<tb_健康体检DAL.Module_体检, string>();
                    dic.Add(tb_健康体检DAL.Module_体检.现存主要健康问题, doctor_现存主要健康问题);
                    dic.Add(tb_健康体检DAL.Module_体检.主要用药情况, doctor_主要用药情况);
                    dic.Add(tb_健康体检DAL.Module_体检.非免疫规划预防接种史, doctor_非免疫规划预防接种史);
                    tb.医师签字 = tb_健康体检DAL.JsonToStr_医师签字(Program.currentUser.DocNo, dic);
                    //End

                    #endregion


                    if (tb_健康体检DAL.UpdateSys_体检2(tb))
                    {
                        MessageBox.Show("保存成功！");
                        //保存 grid的信息

                        if (list建床史.Count > 0)
                        {
                            tb_健康体检_住院史DAL.Delete建床Info(Program.currentUser.ID);//先删除
                            for (int i = 0; i < list建床史.Count; i++)
                            {
                                tb_健康体检_住院史DAL.Add(list建床史[i]);
                            }
                        }
                        if (list免疫.Count > 0)
                        {
                            tb_非免疫规划DAL.DeleteInfo(Program.currentUser.ID);//先删除
                            for (int i = 0; i < list免疫.Count; i++)
                            {
                                tb_非免疫规划DAL.Add(list免疫[i]);
                            }
                        }
                        if (list用药.Count > 0)
                        {
                            tb_健康体检_用药情况表DAL.DeleteInfo(Program.currentUser.ID);//先删除
                            for (int i = 0; i < list用药.Count; i++)
                            {
                                tb_健康体检_用药情况表DAL.Add(list用药[i]);
                            }
                        }
                        if (list住院史.Count > 0)
                        {
                            tb_健康体检_住院史DAL.Delete住院Info(Program.currentUser.ID);//先删除
                            for (int i = 0; i < list住院史.Count; i++)
                            {
                                tb_健康体检_住院史DAL.Add(list住院史[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        //TODO:慢性病 管理
        /*
        private void AddNew慢病基础信息表(DataRow row)
        {
            row[tb_MXB慢病基础信息表.姓名] = util.DESEncrypt.DES加密(this.text姓名.Text.Trim());
            row[tb_MXB慢病基础信息表.拼音简码] = util.DESEncrypt.GetChineseSpell(this.text姓名.Text.Trim());
            row[tb_MXB慢病基础信息表.身份证号] = this.text身份证号.Text.Trim();
            row[tb_MXB慢病基础信息表.出生日期] = string.IsNullOrEmpty(this.text出生日期.Text);
            row[tb_MXB慢病基础信息表.民族] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.民族];//.SelectedIndex;
            row[tb_MXB慢病基础信息表.文化程度] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.文化程度];// ControlsHelper.GetComboxKey(this.cbo文化程度);//.SelectedIndex;
            row[tb_MXB慢病基础信息表.常住类型] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.常住类型]; //ontrolsHelper.GetComboxKey(this.cbo常住类型);//.SelectedIndex;
            row[tb_MXB慢病基础信息表.工作单位] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.工作单位];// this.txt工作单位.Text.Trim();
            row[tb_MXB慢病基础信息表.婚姻状况] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.婚姻状况]; //ControlsHelper.GetComboxKey(this.cbo婚姻状况);//.SelectedIndex;
            row[tb_MXB慢病基础信息表.职业] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.职业]; //ControlsHelper.GetComboxKey(this.cbo职业);//.SelectedIndex;
            row[tb_MXB慢病基础信息表.联系人电话] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.联系人电话]; //this.txt联系人电话.Text.Trim();
            row[tb_MXB慢病基础信息表.省] = "37";
            row[tb_MXB慢病基础信息表.市] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.市]; //this.cbo居住地址_市.EditValue;
            row[tb_MXB慢病基础信息表.区] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.区]; //this.cbo居住地址_县.EditValue;
            row[tb_MXB慢病基础信息表.街道] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.街道]; //this.cbo居住地址_街道.EditValue;
            row[tb_MXB慢病基础信息表.居委会] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.居委会]; //this.cbo居住地址_村委会.EditValue;
            row[tb_MXB慢病基础信息表.所属片区] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.所属片区];// this.cbo所属片区.Text.Trim();
            row[tb_MXB慢病基础信息表.居住地址] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.居住地址];// this.txt居住地址_详细地址.Text.Trim();
            row[tb_MXB慢病基础信息表.医疗费用支付方式] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.医疗费支付类型];// Get医疗费用支付方式();
            //row[tb_MXB慢病基础信息表.医疗费用支付类型其他] = this.txt医疗费用支付方式_其他.Text.Trim();
            row[tb_MXB慢病基础信息表.医疗保险号] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.医疗保险号]; //this.txt医疗保险号.Text.Trim();
            row[tb_MXB慢病基础信息表.新农合号] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.新农合号];// this.txt新农合号.Text.Trim();
            row[tb_MXB慢病基础信息表.建档机构] = Loginer.CurrentUser.所属机构;
            row[tb_MXB慢病基础信息表.建档时间] = this.dte创建时间.Text.Trim();
            row[tb_MXB慢病基础信息表.建档人] = Loginer.CurrentUser.用户编码;
            row[tb_MXB慢病基础信息表.创建机构] = Loginer.CurrentUser.所属机构;
            row[tb_MXB慢病基础信息表.创建时间] = this.dte创建时间.Text.Trim();
            row[tb_MXB慢病基础信息表.创建人] = Loginer.CurrentUser.用户编码;
            row[tb_MXB慢病基础信息表.更新时间] = this.dte创建时间.Text.Trim();
            row[tb_MXB慢病基础信息表.更新人] = Loginer.CurrentUser.用户编码;
            row[tb_MXB慢病基础信息表.HAPPENTIME] = this.dte创建时间.Text.Trim();
            row[tb_MXB慢病基础信息表.所属机构] = Loginer.CurrentUser.所属机构;
            row[tb_MXB慢病基础信息表.高血压随访时间] = "1900-01-01";
            row[tb_MXB慢病基础信息表.糖尿病随访时间] = "1900-01-01";
            row[tb_MXB慢病基础信息表.脑卒中随访时间] = "1900-01-01";
            row[tb_MXB慢病基础信息表.冠心病随访时间] = "1900-01-01";
            row[tb_MXB慢病基础信息表.家庭档案编号] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.家庭档案编号];
            row[tb_MXB慢病基础信息表.个人档案编号] = _docNo;

        }
        private void AddTNBGLK(DataRow row糖尿病)
        {
            row糖尿病[tb_MXB糖尿病管理卡.管理卡编号] = "";
            row糖尿病[tb_MXB糖尿病管理卡.个人档案编号] = _docNo;
            row糖尿病[tb_MXB糖尿病管理卡.所属机构] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.所属机构];
            row糖尿病[tb_MXB糖尿病管理卡.创建机构] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建机构];
            row糖尿病[tb_MXB糖尿病管理卡.创建时间] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建时间];// this.dte录入时间.Text.Trim();
            row糖尿病[tb_MXB糖尿病管理卡.创建人] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建人];// Loginer.CurrentUser.用户编码;
            row糖尿病[tb_MXB糖尿病管理卡.发生时间] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.调查时间];
            row糖尿病[tb_MXB糖尿病管理卡.修改人] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.修改人];// this.dte录入时间.Text.Trim();
            row糖尿病[tb_MXB糖尿病管理卡.修改时间] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.修改时间];// this.dte录入时间.Text.Trim();
            row糖尿病[tb_MXB糖尿病管理卡.缺项] = "26";
            row糖尿病[tb_MXB糖尿病管理卡.完整度] = "0";
            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.糖尿病管理卡] = "26,0";
            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.糖尿病随访表] = "未建";
            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否糖尿病] = "1";
        }
        private void AddNewGXYGLK(DataRow rowGXYGLK)
        {
            rowGXYGLK[tb_MXB高血压管理卡.管理卡编号] = "";
            rowGXYGLK[tb_MXB高血压管理卡.个人档案编号] = _docNo;
            rowGXYGLK[tb_MXB高血压管理卡.所属机构] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.所属机构];
            rowGXYGLK[tb_MXB高血压管理卡.创建机构] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建机构];
            rowGXYGLK[tb_MXB高血压管理卡.发生时间] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.调查时间];// string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
            rowGXYGLK[tb_MXB高血压管理卡.创建时间] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建时间];// this.dte录入时间.Text.Trim();
            rowGXYGLK[tb_MXB高血压管理卡.创建人] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建人];// Loginer.CurrentUser.用户编码;
            rowGXYGLK[tb_MXB高血压管理卡.修改人] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.修改人];// this.dte录入时间.Text.Trim();
            rowGXYGLK[tb_MXB高血压管理卡.修改时间] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.修改时间];// this.dte录入时间.Text.Trim();
            //rowGXYGLK[tb_MXB高血压管理卡.终止管理] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.档案状态];
            rowGXYGLK[tb_MXB高血压管理卡.缺项] = "20";
            rowGXYGLK[tb_MXB高血压管理卡.完整度] = "0";

            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.高血压管理卡] = "20,0";
            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.高血压随访表] = "未建";
            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否高血压] = "1";
        }
         */
        #endregion

        #region Private Method
        private bool f保存检查()
        {
            //string 出生日期 = _ds健康档案ForShow.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.出生日期].ToString();
            //if (!string.IsNullOrEmpty(出生日期))
            //{
            //    DateTime dt = DateTime.Now;
            //    if (DateTime.TryParse(出生日期, out dt))
            //    {
            //        if (DateTime.Now.Year - dt.Year >= 35)
            //        {
            //            if (this.txt一般情况_血压左侧.Txt1.Text.Trim() == "" && this.txt一般情况_血压左侧.Txt2.Text.Trim() == "" && this.txt一般情况_血压右侧.Txt1.Text.Trim() == "" && this.txt一般情况_血压右侧.Txt2.Text.Trim() == "")
            //            {
            //                if (Msg.AskQuestion("此人年龄已到35岁，是否输入血压值？"))
            //                {
            //                    this.txt一般情况_血压左侧.Txt1.Focus();
            //                    return false;
            //                }
            //            }
            //        }
            //    }
            //}

            return true;
        }
        /// <summary>
        /// 第一个checkbox是否选中来决定 后续的checkbook的enable状态
        /// </summary>
        /// <param name="firstChk"></param>
        public void SetControlEnable(CheckEdit firstChk)
        {
            bool flag = firstChk.Checked;
            FlowLayoutPanel container = firstChk.Parent as FlowLayoutPanel;
            if (container == null) return;
            Control.ControlCollection controlList = container.Controls;
            for (int i = 1; i < controlList.Count; i++)
            {

                //Begin WXF 2018-11-15 | 15:11
                //增加controlList[i].Name.ToString().Substring(0, 7).Equals("chk_txt")判断 
                //"其他"快捷选项由"chk*_其他"控制
                //End

                if (controlList[i] is CheckEdit && !controlList[i].Name.ToString().Substring(0, 6).Equals("chktxt"))
                {
                    CheckEdit item = (CheckEdit)controlList[i];
                    item.Enabled = !flag;
                    if (flag)//选中了第一个控件
                    {
                        item.Checked = false;
                    }
                }
                else
                {
                    controlList[i].Enabled = false;
                }
            }
        }
        public void SetTextEditEnable(Control control, TextEdit textEdit)
        {
            if (control is CheckEdit)//CheckEdit 类型的其他
            {
                CheckEdit check = (CheckEdit)control;
                textEdit.Enabled = check.Checked;
                textEdit.Text = check.Checked ? textEdit.Text : "";
            }
            else if (true)
            {

            }
        }
        /// <summary>
        /// 设置FlowLayoutPanel中控件的  enable属性
        /// </summary>
        /// <param name="flowLayout"></param>
        /// <param name="p"></param>
        private void SetFlowControlEnable(FlowLayoutPanel flowLayout, bool p)
        {
            if (flowLayout == null || flowLayout.Controls.Count == 0) return;
            for (int i = 0; i < flowLayout.Controls.Count; i++)
            {
                Control item = flowLayout.Controls[i];
                item.Enabled = p;
            }
        }
        private void SetRadioGroupTxtEnable(RadioGroup radio, TextEdit txt)
        {
            string index = radio.EditValue == null ? "" : radio.EditValue.ToString();
            txt.Enabled = !string.IsNullOrEmpty(index) && index == "2" ? true : false;
            txt.Text = !string.IsNullOrEmpty(index) && index == "2" ? txt.Text : "";
        }
        private int Get完整度(int i)
        {
            int k = Convert.ToInt32((48 - i) * 100 / 48.0);
            return k;
        }
        private void InitView()
        {
            SetControlEnable(this.chk脑血管疾病_未发现);
            SetControlEnable(this.chk肾脏疾病_未发现);
            SetControlEnable(this.chk心脏疾病_未发现);
            SetControlEnable(this.chk眼部疾病_未发现);

            //Begin WXF 2018-11-15 | 14:24
            SetControlEnable(this.chk神经系统疾病_未发现);
            SetControlEnable(this.chk其他系统疾病_未发现);
            //End

            this.TCG_Tags.SelectedTabPageIndex = 0;
        }
        private void DobindingDatasource(tb_健康体检Info tb)
        {
            if (tb != null)
            {

                #region 现存主要健康问题
                ControlsHelper.SetFlowLayoutResult(tb.脑血管疾病.ToString(), flow脑血管疾病);
                this.txt脑血管疾病_其他.EditValue = tb.脑血管疾病其他;
                ControlsHelper.SetFlowLayoutResult(tb.肾脏疾病.ToString(), flow肾脏疾病);
                this.txt肾脏疾病_其他.EditValue = tb.肾脏疾病其他;
                ControlsHelper.SetFlowLayoutResult(tb.心脏疾病.ToString(), flow心脏疾病);
                this.txt心脏疾病_其他.EditValue = tb.心脏疾病其他;
                ControlsHelper.SetFlowLayoutResult(tb.眼部疾病.ToString(), flow眼部疾病);
                this.txt眼部疾病_其他.EditValue = tb.眼部疾病其他;

                //Begin WXF 2018-11-15 | 14:29
                ControlsHelper.SetFlowLayoutResult(tb.神经系统疾病.ToString(), flow神经系统疾病);
                this.txt眼部疾病_其他.EditValue = tb.神经系统疾病其他;
                ControlsHelper.SetFlowLayoutResult(tb.其他系统疾病.ToString(), flow其他系统疾病);
                this.txt眼部疾病_其他.EditValue = tb.其他系统疾病其他;
                //End

                #endregion

            }
        }
        #endregion

        private void btn下一页_Click(object sender, EventArgs e)
        {
            int index = this.TCG_Tags.SelectedTabPageIndex;
            if (index != TCG_Tags.TabPages.Count) this.TCG_Tags.SelectedTabPageIndex = index + 1;
        }

        private void btn上一页_Click(object sender, EventArgs e)
        {
            int index = this.TCG_Tags.SelectedTabPageIndex;
            if (index != 0) this.TCG_Tags.SelectedTabPageIndex = index - 1;
        }


        //Begin WXF 2018-11-16 | 18:15
        //获取参照数据并赋给控件 
        private void sBut_Get参照数据_Click(object sender, EventArgs e)
        {
            sBut_Get参照数据.Enabled = false;

            switch (TCG_Tags.SelectedTabPageName)
            {
                case "LCG_现存主要健康问题":
                    dal_健康体检_Reference dal健康体检Ref = new dal_健康体检_Reference();
                    tb_健康体检_Reference tb健康体检Ref = new tb_健康体检_Reference();
                    tb健康体检Ref = dal健康体检Ref.GetOneRow(Program.currentUser.DocNo);
                    if (tb健康体检Ref != null)
                    {
                        Set_LCG_现存主要健康问题(tb健康体检Ref);
                    }
                    else
                    {
                        MessageBox.Show("此人无去年数据做参照!");
                    }
                    break;
                case "LCG_主要用药情况":
                    dal_健康体检_用药情况_Reference dal用药情况Ref = new dal_健康体检_用药情况_Reference();
                    List<tb_健康体检_用药情况_Reference> ls_体检用药情况Ref = new List<tb_健康体检_用药情况_Reference>();
                    ls_体检用药情况Ref = dal用药情况Ref.Get体检用药参照(Program.currentUser.DocNo);
                    if (ls_体检用药情况Ref != null && ls_体检用药情况Ref.Count > 0)
                    {
                        Set_LCG_主要用药情况(ls_体检用药情况Ref);
                    }
                    else
                    {
                        MessageBox.Show("此人无体检用药数据做参照!");
                    }
                    break;
                default:
                    MessageBox.Show("此页未设置参照数据!");
                    break;
            }

            sBut_Get参照数据.Enabled = true;
        }

        private void Set_LCG_现存主要健康问题(tb_健康体检_Reference Reference)
        {
            if (!"1".Equals(Reference.脑血管疾病.Substring(0, 1)) && !string.IsNullOrWhiteSpace(Reference.脑血管疾病))
            {
                Set_现存主要健康问题Value(chk脑血管疾病_未发现, Reference.脑血管疾病, Reference.脑血管疾病其他);
            }
            if (!"1".Equals(Reference.肾脏疾病.Substring(0, 1)) && !string.IsNullOrWhiteSpace(Reference.肾脏疾病))
            {
                Set_现存主要健康问题Value(chk肾脏疾病_未发现, Reference.肾脏疾病, Reference.肾脏疾病其他);
            }
            if (!"1".Equals(Reference.心脏疾病.Substring(0, 1)) && !string.IsNullOrWhiteSpace(Reference.心脏疾病))
            {
                Set_现存主要健康问题Value(chk心脏疾病_未发现, Reference.心脏疾病, Reference.心脏疾病其他);
            }
            if (!"1".Equals(Reference.眼部疾病.Substring(0, 1)) && !string.IsNullOrWhiteSpace(Reference.眼部疾病))
            {
                Set_现存主要健康问题Value(chk眼部疾病_未发现, Reference.眼部疾病, Reference.眼部疾病其他);
            }
            if (!"1".Equals(Reference.神经系统疾病.Substring(0, 1)) && !string.IsNullOrWhiteSpace(Reference.神经系统疾病))
            {
                Set_现存主要健康问题Value(chk神经系统疾病_未发现, Reference.神经系统疾病, Reference.神经系统疾病其他);
            }
            if (!"1".Equals(Reference.其他系统疾病.Substring(0, 1)) && !string.IsNullOrWhiteSpace(Reference.其他系统疾病))
            {
                Set_现存主要健康问题Value(chk其他系统疾病_未发现, Reference.其他系统疾病, Reference.其他系统疾病其他);
            }
        }

        private void Set_现存主要健康问题Value(CheckEdit ce, string str_Value, string str_Other)
        {
            ce.Checked = false;
            chk_CheckedChanged(ce, null);

            ControlCollection cc = ce.Parent.Controls;

            string[] strs_Value = str_Value.Split(',');

            foreach (string value in strs_Value)
            {
                foreach (Control col in cc)
                {
                    if (col is CheckEdit)
                    {
                        CheckEdit chk = (CheckEdit)col;
                        if (chk.Tag.ToString().Equals(value))
                        {
                            chk.Checked = true;
                        }
                    }
                }

                if ("99".Equals(value))
                {
                    string str_未发现 = ce.Name.ToString();
                    string str_其他 = str_未发现.Substring(0, str_未发现.IndexOf('_') + 1) + "其他";
                    string str_txt其他 = "txt" + str_未发现.Substring(3, str_未发现.IndexOf('_') - 2) + "其他";
                    CheckEdit chk_其他 = (CheckEdit)cc.Find(str_其他, false)[0];
                    TextEdit txt_其他 = (TextEdit)cc.Find(str_txt其他, false)[0];
                    chk其他_CheckedChanged(chk_其他, null);
                    txt_其他.Text = str_Other;
                }
            }
        }
        //End

        //Begin WXF 2019-02-20 | 18:48
        //获取参照数据并赋给控件(用药)
        private void Set_LCG_主要用药情况(List<tb_健康体检_用药情况_Reference> ls_体检用药情况Ref)
        {
            //gc用药情况.DataSource = ls_体检用药情况Ref;

            // 不能用上面的方式显示数据,因为这个已经绑定过数据源了
            foreach (tb_健康体检_用药情况_Reference item in ls_体检用药情况Ref)
            {
                tb_健康体检_用药情况表Info newTb = new tb_健康体检_用药情况表Info();
                newTb.身份证号 = Program.currentUser.ID;
                newTb.个人档案编号 = Program.currentUser.DocNo;

                newTb.药物名称 = item.药物名称;
                newTb.用法 = item.用法;
                newTb.用量 = item.用量;
                newTb.用药时间 = item.用药时间;
                newTb.服药依从性 = item.服药依从性;

                list用药.Add(newTb);
            }


            this.gc用药情况.RefreshDataSource();


            //Begin WXF 2019-03-20 | 19:08
            //列宽自适应 
            this.gv用药情况.BestFitColumns();
            this.gv用药情况.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gv用药情况.OptionsView.ColumnAutoWidth = false;
            //End
											 
        }
        //End


        string doctor_现存主要健康问题 = string.Empty;
        string doctor_主要用药情况 = string.Empty;
        string doctor_非免疫规划预防接种史 = string.Empty;

        private void lookUpEdit_医师签字_EditValueChanged(object sender, EventArgs e)
        {
            string PageName = TCG_Tags.SelectedTabPage.Text;

            switch (PageName)
            {
                case "现存主要健康问题":
                    doctor_现存主要健康问题 = lookUpEdit_医师签字.EditValue.ToString();
                    break;
                case "主要用药情况":
                    doctor_主要用药情况 = lookUpEdit_医师签字.EditValue.ToString();
                    break;
                case "非免疫规划预防接种史":
                    doctor_非免疫规划预防接种史 = lookUpEdit_医师签字.EditValue.ToString();
                    break;
                default:
                    break;
            }
        }

        private void TCG_Tags_SelectedPageChanged(object sender, DevExpress.XtraLayout.LayoutTabPageChangedEventArgs e)
        {
            switch (e.Page.Text)
            {
                case "现存主要健康问题":
                    layoutControlItem_医师签字.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lookUpEdit_医师签字.EditValue = string.IsNullOrEmpty(doctor_现存主要健康问题) ? "请选择" : doctor_现存主要健康问题;
                    break;
                case "主要用药情况":
                    layoutControlItem_医师签字.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lookUpEdit_医师签字.EditValue = string.IsNullOrEmpty(doctor_主要用药情况) ? "请选择" : doctor_主要用药情况;
                    break;
                case "非免疫规划预防接种史":
                    layoutControlItem_医师签字.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lookUpEdit_医师签字.EditValue = string.IsNullOrEmpty(doctor_非免疫规划预防接种史) ? "请选择" : doctor_非免疫规划预防接种史;
                    break;
                default:
                    layoutControlItem_医师签字.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    break;
            }
        }

        private void sBut_Get其他用药参照_Click(object sender, EventArgs e)
        {
            sBut_Get其他用药参照.Enabled = false;

            dal_健康体检_用药情况_Reference dal用药情况Ref = new dal_健康体检_用药情况_Reference();
            List<tb_健康体检_用药情况_Reference> ls_其他用药情况Ref = new List<tb_健康体检_用药情况_Reference>();
            ls_其他用药情况Ref = dal用药情况Ref.Get其他用药参照(Program.currentUser.DocNo);

            if (ls_其他用药情况Ref != null && ls_其他用药情况Ref.Count > 0)
            {
                this.lay其他用药参照.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                this.gc其他用药参照.DataSource = ls_其他用药情况Ref;
            }
            else
            {
                MessageBox.Show("此人无其他用药数据做参照!");
            }

            sBut_Cut其他用药参照.Enabled = true;
            sBut_Get其他用药参照.Enabled = true;
        }

        private void sBut_Cut其他用药参照_Click(object sender, EventArgs e)
        {
            this.lay其他用药参照.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            sBut_Cut其他用药参照.Enabled = false;
        }
    }
}
