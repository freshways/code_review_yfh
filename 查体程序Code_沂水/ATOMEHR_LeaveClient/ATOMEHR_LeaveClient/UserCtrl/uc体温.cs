﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO.Ports;
using DevExpress.XtraEditors;

namespace ATOMEHR_LeaveClient
{
    public partial class uc体温 : UserControl
    {
        private TextEdit _txtParentBarCode;
        public TextEdit TxtParentBarCode
        {
            get { return _txtParentBarCode; }
            set { _txtParentBarCode = value; }
        }
        User currentUser;
        IntPtr hBluetooth;
        int nCOMID = 0; //端口

        private SerialPort comm = new SerialPort();
        private long received_count = 0;//接收计数
        private bool Listening = false;//是否没有执行完invoke相关操作
        private bool Closing = false;//是否正在关闭串口，执行Application.DoEvents，并阻止再次invoke
        private List<byte> buffer = new List<byte>(4096);//默认分配1页内存，并始终限制不允许超过
        private byte[] binary_data_1 = new byte[4];

        Timer StatusTimer = new Timer();//定时器 用于定时连接设备
        int Start = 0; //计数器

        public uc体温()
        {
            InitializeComponent();
            currentUser = Program.currentUser;
        }

        public uc体温(SerialPort spt)
        {
            InitializeComponent();
            currentUser = Program.currentUser;
            comm = spt;
        }

        public uc体温(User _currentUser)
        {
            InitializeComponent();
            currentUser = _currentUser;
        }

        private void btn保存_Click(object sender, EventArgs e)
        {
            if (funCheck())
            {
                try
                {
                    tb_健康体检Info tb_New = new tb_健康体检Info();
                    tb_New.身份证号 = currentUser.ID;
                    tb_New.出生日期 = currentUser.Birth;
                    tb_New.姓名 = currentUser.Name;
                    tb_New.性别 = currentUser.Gender;

                    tb_New.体温 = Convert.ToDouble(this.txt体温.Text.Trim());
                    if (string.IsNullOrEmpty(this.txt呼吸.Text.Trim())) { MessageBox.Show("请输入呼吸频率！"); return; }
                    tb_New.呼吸 = Convert.ToInt32(this.txt呼吸.Text.Trim());

                    if (tb_健康体检DAL.UpdateSys_体温(tb_New))
                    {
                        MessageBox.Show("保存数据成功！");
                    }
                    _txtParentBarCode.Focus();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private bool funCheck()
        {
            if (string.IsNullOrEmpty(this.txt体温.Text.Trim()))
            {
                this.lbl提示.Text = "未检测到体温值，请检查体温值";
                return false;
            }
            return true;
        }

        private void uc体温_Load(object sender, EventArgs e)
        {
            //如果是连接中，先关闭
            if (comm != null)
            {
                if (comm.IsOpen)
                {
                    comm.Close();
                }
            }
            //获取蓝牙操作句柄
            hBluetooth = EcgHelper.BluetoothOperationOpen();
            try
            {
                if (Program.BlueAddr.Length < 6)
                {
                    nCOMID = Convert.ToInt32(Program.BlueAddr);
                }
                else
                {
                    bool trues = EcgHelper.BluetoothOperationConnect(hBluetooth, Program.BlueAddr, ref nCOMID);
                }
                if (nCOMID > 0)
                {
                    //添加事件注册
                    comm.DataReceived += comm_DataReceived;
                    //StatusTimer.Interval = 1000;
                    //StatusTimer.Tick += new EventHandler(T_Tick);

                    //关闭时点击，则设置好端口，波特率后打开
                    comm.PortName = string.Format("COM{0}", nCOMID);
                    comm.BaudRate = 4800;
                    comm.Open();
                    this.lbl提示.Text = "连接成功！";
                }
                //EcgHelper.BluetoothOperationConnect(hBluetooth, "(0F:03:16:03:01:3B)", ref nCOMID);
                //labelCom.Text += nCOMID.ToString();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("未与设备进行配对！请先初始化" + ex.Message);
                return;
            }
            ////添加事件注册
            //comm.DataReceived += comm_DataReceived;
            //StatusTimer.Interval = 1000;
            ////StatusTimer.Tick += new EventHandler(T_Tick);

            ////关闭时点击，则设置好端口，波特率后打开
            //comm.PortName = string.Format("COM{0}", nCOMID);
            //comm.BaudRate = 4800;
            //try
            //{
            //    comm.Open();
            //    this.lbl提示.Text = "连接成功！";
            //}
            //catch (Exception ex)
            //{
            //    //捕获到异常信息，创建一个新的comm对象，之前的不能用了。
            //    //comm = new SerialPort();
            //    //现实异常信息给客户。
            //    //MessageBox.Show(ex.Message);
            //    this.lbl提示.Text = "失败！" + ex.Message;

            //    this.BeginInvoke((EventHandler)(delegate
            //    {
            //        StatusTimer.Start();//如果连接失败，启动定时
            //    }));
            //}

            //tb_健康体检Info tb = tb_健康体检DAL.Gettb_健康体检InfoById(currentUser.ID);
            //if (tb != null)
            //{
            //    if (tb.体温 != null)
            //        this.txt体温.Text = tb.体温.ToString();
            //}
        }

        void comm_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (Closing) return;//如果正在关闭，忽略操作，直接返回，尽快的完成串口监听线程的一次循环
            try
            {
                Listening = true;//设置标记，说明我已经开始处理数据，一会儿要使用系统UI的。
                int n = comm.BytesToRead;//先记录下来，避免某种原因，人为的原因，操作几次之间时间长，缓存不一致
                byte[] buf = new byte[n];//声明一个临时数组存储当前来的串口数据
                received_count += n;//增加接收计数
                comm.Read(buf, 0, n);//读取缓冲数据

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //<协议解析>
                bool data_1_catched = false;//缓存记录数据是否捕获到
                //1.缓存数据
                buffer.AddRange(buf);
                //2.完整性判断
                while (buffer.Count > 1)//至少要包含头（2字节）+长度（1字节）+校验（1字节）
                {
                    if (buffer[0] == 0xFF)
                    {
                        int len = 2;//数据长度
                        if (buffer.Count < len + 1) break;//数据不够的时候什么都不做
                        //异或校验，逐个字节异或得到校验码
                        byte checksum = 0;
                        for (int i = 1; i < len + 1; i++)//len+3表示校验之前的位置
                        {
                            checksum ^= buffer[i];
                        }
                        if (checksum != buffer[3]) //如果数据校验失败，丢弃这一包数据
                        {
                            buffer.RemoveRange(0, 4);//从缓存中删除错误数据
                            continue;//继续下一次循环
                        }
                        //至此，已经被找到了一条完整数据。我们将数据直接分析，或是缓存起来一起分析
                        //我们这里采用的办法是缓存一次，好处就是如果你某种原因，数据堆积在缓存buffer中
                        //已经很多了，那你需要循环的找到最后一组，只分析最新数据，过往数据你已经处理不及时
                        //了，就不要浪费更多时间了，这也是考虑到系统负载能够降低。
                        buffer.CopyTo(0, binary_data_1, 0, 4);//复制一条完整数据到具体的数据缓存
                        data_1_catched = true;
                        buffer.RemoveRange(0, 4);//正确分析一条数据，从缓存中移除数据。
                    }
                    else
                    {
                        //这里是很重要的，如果数据开始不是头，则删除数据
                        buffer.RemoveAt(0);
                    }
                }
                //分析数据
                if (data_1_catched)
                {
                    //我们的数据都是定好格式的，所以当我们找到分析出的数据1，就知道固定位置一定是这些数据，我们只要显示就可以了
                    string data = binary_data_1[1].ToString("X2") + " " + binary_data_1[2].ToString("X2") + " ";
                    //更新界面
                    this.Invoke((EventHandler)(delegate
                    {
                        double d = Convert.ToInt32(data.Replace(" ", ""), 16) * 0.1;
                        txt体温.Text = d.ToString();
                    }));
                }

            }
            finally
            {
                Listening = false;//我用完了，ui可以关闭串口了。
            }
        }

        void T_Tick(object sender, EventArgs e)
        {
            Start++;
            if (Start > 5) { StatusTimer.Stop(); MessageBox.Show("连接失败，连接超过5秒！"); return; }
            try
            {
                comm.Open();
                this.lbl提示.Text = "连接成功！";
                StatusTimer.Stop();
                return;
            }
            catch (Exception ex)
            {
                //捕获到异常信息，创建一个新的comm对象，之前的不能用了。
                if (Start > 30) comm = new SerialPort();
                //现实异常信息给客户。
                this.lbl提示.Text = "第" + Start.ToString() + "次尝试连接！";
            }
        }

        private void txt呼吸_Click(object sender, EventArgs e)
        {
            TabTip.ShowInputPanel();
        }


    }
}
