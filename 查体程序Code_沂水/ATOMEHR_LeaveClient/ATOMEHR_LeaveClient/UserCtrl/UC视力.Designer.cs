﻿namespace ATOMEHR_LeaveClient
{
    partial class UC视力
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC视力));
            this.txt左眼视力 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.txt右眼矫正视力 = new DevExpress.XtraEditors.TextEdit();
            this.txt左眼矫正视力 = new DevExpress.XtraEditors.TextEdit();
            this.txt右眼视力 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.txt左眼视力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt右眼矫正视力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt左眼矫正视力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt右眼视力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.SuspendLayout();
            // 
            // txt左眼视力
            // 
            this.txt左眼视力.EditValue = "";
            this.txt左眼视力.Location = new System.Drawing.Point(12, 99);
            this.txt左眼视力.Name = "txt左眼视力";
            this.txt左眼视力.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(18)));
            this.txt左眼视力.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.txt左眼视力.Properties.Appearance.Options.UseFont = true;
            this.txt左眼视力.Properties.Appearance.Options.UseForeColor = true;
            this.txt左眼视力.Size = new System.Drawing.Size(417, 46);
            this.txt左眼视力.StyleController = this.layoutControl1;
            this.txt左眼视力.TabIndex = 2;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btn保存);
            this.layoutControl1.Controls.Add(this.txt右眼矫正视力);
            this.layoutControl1.Controls.Add(this.txt左眼矫正视力);
            this.layoutControl1.Controls.Add(this.txt左眼视力);
            this.layoutControl1.Controls.Add(this.txt右眼视力);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(489, 250, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(441, 450);
            this.layoutControl1.TabIndex = 9;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btn保存
            // 
            this.btn保存.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.btn保存.Appearance.BackColor2 = System.Drawing.Color.Teal;
            this.btn保存.Appearance.Font = new System.Drawing.Font("Tahoma", 22F, System.Drawing.FontStyle.Bold);
            this.btn保存.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.btn保存.Appearance.Options.UseBackColor = true;
            this.btn保存.Appearance.Options.UseFont = true;
            this.btn保存.Appearance.Options.UseForeColor = true;
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(12, 12);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(417, 56);
            this.btn保存.StyleController = this.layoutControl1;
            this.btn保存.TabIndex = 8;
            this.btn保存.Text = "保  存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // txt右眼矫正视力
            // 
            this.txt右眼矫正视力.EditValue = "";
            this.txt右眼矫正视力.Location = new System.Drawing.Point(12, 330);
            this.txt右眼矫正视力.Name = "txt右眼矫正视力";
            this.txt右眼矫正视力.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(18)));
            this.txt右眼矫正视力.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.txt右眼矫正视力.Properties.Appearance.Options.UseFont = true;
            this.txt右眼矫正视力.Properties.Appearance.Options.UseForeColor = true;
            this.txt右眼矫正视力.Size = new System.Drawing.Size(417, 46);
            this.txt右眼矫正视力.StyleController = this.layoutControl1;
            this.txt右眼矫正视力.TabIndex = 7;
            // 
            // txt左眼矫正视力
            // 
            this.txt左眼矫正视力.EditValue = "";
            this.txt左眼矫正视力.Location = new System.Drawing.Point(12, 253);
            this.txt左眼矫正视力.Name = "txt左眼矫正视力";
            this.txt左眼矫正视力.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(18)));
            this.txt左眼矫正视力.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.txt左眼矫正视力.Properties.Appearance.Options.UseFont = true;
            this.txt左眼矫正视力.Properties.Appearance.Options.UseForeColor = true;
            this.txt左眼矫正视力.Size = new System.Drawing.Size(417, 46);
            this.txt左眼矫正视力.StyleController = this.layoutControl1;
            this.txt左眼矫正视力.TabIndex = 6;
            // 
            // txt右眼视力
            // 
            this.txt右眼视力.EditValue = "";
            this.txt右眼视力.Location = new System.Drawing.Point(12, 176);
            this.txt右眼视力.Name = "txt右眼视力";
            this.txt右眼视力.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(18)));
            this.txt右眼视力.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.txt右眼视力.Properties.Appearance.Options.UseFont = true;
            this.txt右眼视力.Properties.Appearance.Options.UseForeColor = true;
            this.txt右眼视力.Size = new System.Drawing.Size(417, 46);
            this.txt右眼视力.StyleController = this.layoutControl1;
            this.txt右眼视力.TabIndex = 3;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.emptySpaceItem1,
            this.layoutControlItem5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(441, 450);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem1.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem1.Control = this.txt左眼视力;
            this.layoutControlItem1.CustomizationFormText = "左眼视力：";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(421, 77);
            this.layoutControlItem1.Text = "左眼视力：";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(147, 24);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem2.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem2.Control = this.txt右眼视力;
            this.layoutControlItem2.CustomizationFormText = "右眼视力：";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 137);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(421, 77);
            this.layoutControlItem2.Text = "右眼视力：";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(147, 24);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem3.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem3.Control = this.txt左眼矫正视力;
            this.layoutControlItem3.CustomizationFormText = "左眼矫正视力：";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 214);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(421, 77);
            this.layoutControlItem3.Text = "左眼矫正视力：";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(147, 24);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem4.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem4.Control = this.txt右眼矫正视力;
            this.layoutControlItem4.CustomizationFormText = "右眼矫正视力：";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 291);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(421, 77);
            this.layoutControlItem4.Text = "右眼矫正视力：";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(147, 24);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 368);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(421, 62);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.btn保存;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 60);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(55, 50);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(421, 60);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // UC视力
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "UC视力";
            this.Size = new System.Drawing.Size(441, 450);
            this.Load += new System.EventHandler(this.UC视力_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txt左眼视力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt右眼矫正视力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt左眼矫正视力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt右眼视力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txt左眼视力;
        private DevExpress.XtraEditors.TextEdit txt右眼视力;
        private DevExpress.XtraEditors.TextEdit txt右眼矫正视力;
        private DevExpress.XtraEditors.TextEdit txt左眼矫正视力;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    }
}
