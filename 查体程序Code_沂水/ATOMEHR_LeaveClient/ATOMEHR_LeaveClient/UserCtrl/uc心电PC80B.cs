﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace ATOMEHR_LeaveClient
{
    public partial class uc心电PC80B : Base_UserControl
    {
        #region 变量
        List<PointF> Points = new List<PointF>();
        Bitmap OrgBmp = null;
        //int Start = 0;
        Random R = new Random();
        PictureBox PB = new PictureBox();
        Timer StatusTimer = new Timer();//定时器

        IntPtr hBluetooth;
        IntPtr hSerialPort;//串口句柄
        int nCOMID = 0; //端口

        public static EcgHelper.OnGetRequest FuncON_GET_REQUEST = null;
        public static EcgHelper.OnGetRealtimePrepare FuncON_GET_REALTIME_PREPARE = null;
        public static EcgHelper.OnGetRealtimeMeasure FuncON_GET_REALTIME_MEASURE = null;
        public static EcgHelper.OnGetRealtimeResult FuncON_GET_REALTIME_RESULT = null;
        public static EcgHelper.OnGetFileTransmit fucTransmit;
        // 心电SCP文件解析        
        public EcgHelper.ECG_FILE_DATA efd = new EcgHelper.ECG_FILE_DATA();
        #endregion
        public uc心电PC80B()
        {
            InitializeComponent();
        }
        #region 初始化
        void Init()
        {
            //定义一个小格子的间距大小            
            int padding = 24;
            int PBWidth = RealTimeWave.Width;
            int PBHeight = RealTimeWave.Height;
            //计算画布的大小
            if ((RealTimeWave.Width % padding) > 0)
                PBWidth = RealTimeWave.Width - (RealTimeWave.Width % padding);
            if ((RealTimeWave.Height % padding) > 0)
                PBHeight = RealTimeWave.Height - (RealTimeWave.Height % padding);
            //OrgBmp = new Bitmap(this.ClientRectangle.Width, this.ClientRectangle.Height);
            OrgBmp = new Bitmap(PBWidth, PBHeight);
            //设置容器
            PB.Parent = RealTimeWave;
            //PB.Dock = DockStyle.Fill;//设置大小
            PB.Width = PBWidth;
            PB.Height = PBHeight;

            using (Graphics G = Graphics.FromImage(OrgBmp))
            {
                //默认填充
                G.Clear(Color.Black);
                G.DrawString("动态心电图", this.Font, Brushes.White, new PointF(0, 0));

                //定义画笔，里面的参数为画笔的颜色
                Pen pen = new Pen(Color.Silver, 1.5f); //网格颜色
                Pen Redpen = new Pen(Color.Red, 1.9f); //中心线颜色

                //循环绘制多条横线            
                for (int i = 0; i < PB.Height / padding; i++)
                {
                    //画线的方法，第一个参数为起始点X的坐标，第二个参数为起始

                    //点Y的坐标；第三个参数为终点X的坐标，第四个参数为终

                    //点Y的坐标；  
                    if (i == ((PB.Height / padding) / 2))
                        G.DrawLine(Redpen, 0, padding * i, PB.Width, padding * i);
                    else
                        G.DrawLine(pen, 0, padding * i, PB.Width, padding * i);
                }
                //循环绘制多条竖线           
                for (int i = 0; i < PB.Width / padding; i++)
                {
                    G.DrawLine(pen, padding * i, 0, padding * i, PB.Height);
                }
            }
            PB.Image = OrgBmp;

            StatusTimer.Interval = 10;
            StatusTimer.Tick += new EventHandler(T_Tick);
            //StatusTimer.Enabled = true;
        }
        #endregion

        private void uc心电PC80B_Load(object sender, EventArgs e)
        {
            //获取蓝牙操作句柄
            hBluetooth = EcgHelper.BluetoothOperationOpen();
            try
            {
                //EcgHelper.BluetoothOperationConnect(hBluetooth, "(8C:DE:52:F4:82:13)", ref nCOMID);
                EcgHelper.BluetoothOperationConnect(hBluetooth, Program.BlueAddr, ref nCOMID);

                labelCom.Text += nCOMID.ToString();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("未与设备进行配对！请先初始化" + ex.Message);
                return;
            }

            hSerialPort = EcgHelper.SerialPortOpen((int)DEVICE_TYPE_PC_80B.DEVICE_TYPE_PC_80B); //根据设备代码获取句柄
            if (hSerialPort != null)
            {//注册回调事件
                FuncON_GET_REQUEST = new EcgHelper.OnGetRequest(OnGetRequest);
                FuncON_GET_REALTIME_PREPARE = new EcgHelper.OnGetRealtimePrepare(OnGetRealtimePrepare);
                FuncON_GET_REALTIME_MEASURE = new EcgHelper.OnGetRealtimeMeasure(OnGetRealtimeMeasure);
                FuncON_GET_REALTIME_RESULT = new EcgHelper.OnGetRealtimeResult(OnGetRealtimeResult);
                fucTransmit = new EcgHelper.OnGetFileTransmit(OnGetFileTransmit);

                EcgHelper.ECGRegisterRequestCallBack(hSerialPort, FuncON_GET_REQUEST);
                EcgHelper.ECGRegisterRealTimePrepareCallBack(hSerialPort, FuncON_GET_REALTIME_PREPARE);
                EcgHelper.ECGRegisterRealTimeMeasureCallBack(hSerialPort, FuncON_GET_REALTIME_MEASURE);
                EcgHelper.ECGRegisterRealTimeResultCallBack(hSerialPort, FuncON_GET_REALTIME_RESULT);
                EcgHelper.ECGRegisterFileTransmitCallBack(hSerialPort, fucTransmit);
            }

            btnBegin.PerformClick();
        }

        #region 回调事件

        /// <summary>
        /// 获取到请求消息时调用。该消息将在接收文件数据或连续测量时获取到
        /// </summary>
        private void OnGetRequest(string sDeviceID, string sProductID, int nSmoothingMode, int nTransMode)
        {
            try
            {
                dX = 0;
                Points.Clear();
            }
            catch (Exception ex)
            {
                lblMSG.Text = "异常："+ex.Message;
            }
        }

        /// <summary>
        /// 获取到准备阶段实时数据时调用。调用频率为 6Hz。1mV 幅值对应 416 采样值。
        /// </summary>
        private void OnGetRealtimePrepare(int bLeadoff, int nGain, int nDataLength, [MarshalAs(UnmanagedType.LPArray, SizeConst = 25)]int[] arrData)
        {
            try
            {
                this.lblGainValue.Text = "X" + nGain.ToString();
                for (int i = 0; i < nDataLength; i++)
                {
                    Bitmap CacheBmp = new Bitmap(OrgBmp);
                    float dY = (float)ConvertCoordinates(nGain, arrData[i]);

                    Points.Add(new PointF(dX, dY));
                    dX += dStepLen;
                    if (dX > PB.Width)
                    {
                        Points.Clear();
                        dX = 0;
                    }
                    Pen ddPen = new Pen(col, 2);
                    if (Points.Count > 1)
                        using (Graphics G = Graphics.FromImage(CacheBmp))
                            G.DrawCurve(ddPen, Points.ToArray());
                    PB.Image = CacheBmp;
                }
            }
            catch (Exception ex)
            {
                lblMSG.Text = "异常:" + ex.Message;
            }
        }
        //int[] XDdata = new int[4500];
        List<int[]> XDdata = new List<int[]>();
        /// <summary>
        /// 获取到测量阶段实时时调用。调用频率为 6Hz。1mV 幅值对应 416 采样值
        /// </summary>
        private void OnGetRealtimeMeasure(int bLeadoff, int nGain, int nTransMode, int nHR, int nPower, int nIndex, int nDataLength,
            [MarshalAs(UnmanagedType.LPArray, SizeConst = 25)]int[] arrData)
        {
            try
            {
                XDdata.Add(arrData);
                this.lblGainValue.Text = "X" + nGain.ToString();
                this.txtHRValue.Text = nHR.ToString();
                for (int i = 0; i < nDataLength; i++)
                {
                    //XDdata[] = arrData[i];
                    Bitmap CacheBmp = new Bitmap(OrgBmp);
                    float dY = (float)ConvertCoordinates(nGain, arrData[i]);

                    Points.Add(new PointF(dX, dY));
                    dX += dStepLen;
                    if (dX > PB.Width)
                    {
                        Points.Clear();
                        dX = 0;
                    }
                    Pen ddPen = new Pen(col, 2);
                    if (Points.Count > 1)
                        using (Graphics G = Graphics.FromImage(CacheBmp))
                            G.DrawCurve(ddPen, Points.ToArray());
                    PB.Image = CacheBmp;
                }
            }
            catch (Exception ex)
            {
                lblMSG.Text = "是否接触不良？" + ex.Message;
            }
        }

        /// <summary>
        /// 测量结束时调用
        /// </summary>
        private void OnGetRealtimeResult(EcgHelper.SYSTEMTIME stTime, int nTransMode, int nResult)
        {
            dX = 0;
            XDdata.Count();
            try
            {
                if (nResult == EcgHelper.ECG_RESULT_00)
                    this.txtResultValue.Text = "正常";
                else
                    this.txtResultValue.Text = "异常" + EcgHelper.Mydic[nResult];
            }
            catch (Exception)
            {
                this.txtResultValue.Text = "异常";
            }
        }

        /// <summary>
        /// 获取文件数据时调用。 通过本函数， 应用将会收到来自快速测量的 30 秒记录数据
        /// </summary>
        public void OnGetFileTransmit(int nfinish, int ndatalength, [MarshalAs(UnmanagedType.LPArray, SizeConst = 10240)]byte[] arrdata)
        {
            try
            {
                if (nfinish == EcgHelper.FILE_TRANSMIT_START)
                    this.lblMSG.Text = "文件传输开始";
                else if (nfinish == EcgHelper.FILE_TRANSMIT_SUCCESS)
                {
                    lblMSG.Text = "数据接收完成,数据长度为：" + ndatalength;

                    if (EcgHelper.FileOperationAnalyseSCPFile(arrdata, arrdata.Length, ref efd))
                    {
                        //string string_temp = Encoding.ASCII.GetString(arrdata);
                        //System.IO.File.AppendAllText("D:\\aaa.scp", string_temp);

                        try
                        {//把返回的数据写入的本地
                            string filename = Program.currentUser.Name + "&" + Program.currentUser.ID + DateTime.Now.ToString("yyyyMMdd") + ".SCP";
                            string path = Application.StartupPath + "\\ECGdata\\";
                            if (!System.IO.Directory.Exists(path))//判断目录
                                System.IO.Directory.CreateDirectory(path);
                            if (!System.IO.File.Exists(path + filename))
                            {
                                System.IO.FileStream dataWriter = new System.IO.FileStream(path + filename, System.IO.FileMode.Append);
                                dataWriter.Seek(0, System.IO.SeekOrigin.End);
                                dataWriter.Write(arrdata, 0, arrdata.Length);
                                dataWriter.Flush();
                                dataWriter.Close();
                            }
                        }
                        catch  {}

                        lblGainValue.Text = (efd.nGain == 0) ? ("X1/2") : ("X" + efd.nGain);

                        this.txtHRValue.Text = efd.nAverageHR.ToString();
                        if (efd.nAnalysis == EcgHelper.ECG_RESULT_00)
                            this.txtResultValue.Text = "正常";
                        else
                            this.txtResultValue.Text = "异常:" + EcgHelper.Mydic[efd.nAnalysis];
                        if (EcgHelper.Mydic.ContainsKey(efd.nAnalysis))
                            this.lblMSG.Text = EcgHelper.Mydic[efd.nAnalysis];
                        //初始化
                        Points.Clear();
                        dX = 0;
                        nWaveIndex = 0;
                        //开始
                        StatusTimer.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMSG.Text = "异常：" + ex.Message;
            }
        }

        #endregion

        // 画波形计数器
        public int nWaveIndex = 0;
        // 波形X轴步长（影响波形宽度）
        public float dStepLen = 0.63f; //0.31  0.63  1.26  暂定这三个值
        public float dX = 0; // 波形横坐标
        //线条颜色
        Color col = Color.Lime;

        #region 画图
        void T_Tick(object sender, EventArgs e)
        {
            if (nWaveIndex < efd.DataLen && efd.DataLen > 0)
            {
                Bitmap CacheBmp = new Bitmap(OrgBmp);
                float dY = (float)ConvertCoordinates(efd.nGain, efd.ecgData[nWaveIndex]);
                nWaveIndex++;
                Points.Add(new PointF(dX, dY));
                dX += dStepLen;
                if (dX > PB.Width)
                {
                    Points.Clear();
                    dX = 0;
                }
                Pen ddPen = new Pen(col, 2);
                if (Points.Count > 1)
                    using (Graphics G = Graphics.FromImage(CacheBmp))
                        G.DrawCurve(ddPen, Points.ToArray());
                PB.Image = CacheBmp;
            }
            else
            {
                StatusTimer.Stop();
            }
        }

        //************************ Convert Coordinates **********************************
        // 常量定义
        public const int Base_Line_Data = 2048;  // 以2048为基线数据 表示 0mV电压值
        public const int Canvas_Count_of_mV = 4; // 表示在画布上画出多少mV的ECG信号，当ECG信号的范围为-2mV到+2mV，则此值为4
        public const int One_mV_to_ADC_Value = 416; // 1mV幅值对应ECG采样数据值416
        /// <summary>
        /// 将心电数据转换成心电波形上的Y轴坐标
        /// </summary>
        /// <param name="nGain">增益值</param>
        /// <param name="nData">心电数据</param>
        /// <returns>心电数据转换成Canvas上的Y轴坐标值</returns>
        private double ConvertCoordinates(int nGain, int nData)
        {
            // 增益值
            double dGain = (nGain == 0) ? (0.5) : (nGain);

            // Canvas的像素高度
            double dCanvHeight = PB.Height;

            // 将nData转换为以2048为基线的数值,转换后nData范围为(-2048~2048)
            nData = nData - Base_Line_Data;

            // nData转换成的 真实电压 mV值
            double dRealVolt = nData * dGain / One_mV_to_ADC_Value;

            // 电压mV值 与 屏幕像素高度 的 比例
            double dProportion = dCanvHeight / Canvas_Count_of_mV;

            // 在Canvas上转换后的实际像素高度（实际实际像素高度 = 转换比例 * 实际电压值）
            double dRealHeight = dProportion * dRealVolt;

            // 因为Canvas上（0，0）为左上角坐标，所以需要再对坐标进行一次转换
            return (dCanvHeight / 2) - dRealHeight;
        }

        private void RealTimeWave_Paint(object sender, PaintEventArgs e)
        {
            Init();
        }
        
        #endregion

        //开始
        private void btnBegin_Click(object sender, EventArgs e)
        {
            dX = 0;
            Points.Clear();
            XDdata.Clear();
            try
            {
                EcgHelper.SerialPortConnect(hSerialPort, nCOMID);

                this.labelState.Text = "状态:成功";
            }
            catch (Exception ex)
            {
                this.labelState.Text = "状态:失败！" + ex.Message;
            }
        }

        private void btn保存_Click(object sender, EventArgs e)
        {
            try
            {
                if (efd.ecgData == null)
                {
                    if (!(MessageBox.Show("未保存波形检查结果到本地！是否保存？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) ==
                         DialogResult.Yes))
                    { return; }

                }
                if (!string.IsNullOrEmpty(this.txtResultValue.Text.Trim()))
                {
                    tb_健康体检Info tb_New = new tb_健康体检Info();
                    string xindian = "";
                    string yichang = string.Empty;
                    if (this.txtResultValue.Text.Trim() == "正常")
                    {
                        xindian = "1";
                    }
                    else
                    {
                        xindian = "2";
                        yichang = txtResultValue.Text.Substring(3);
                    }
                    tb_New.心电图 = xindian;
                    tb_New.心电图异常 = yichang;

                    if (tb_健康体检DAL.UpdateSys_心电(tb_New))
                    {
                        MessageBox.Show("保存成功！");
                        //this.BeginInvoke(new Action(() => { 
                        //    tb_New.b心电结果 = 
                        //}));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
