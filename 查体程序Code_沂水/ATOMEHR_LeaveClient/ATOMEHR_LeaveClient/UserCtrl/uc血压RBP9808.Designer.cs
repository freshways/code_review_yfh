﻿namespace ATOMEHR_LeaveClient
{
    partial class uc血压RBP9808
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc血压RBP9808));
            this.labelState = new System.Windows.Forms.Label();
            this.labelPressure = new System.Windows.Forms.Label();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnStop = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnBegin = new DevExpress.XtraEditors.SimpleButton();
            this.textBox心率 = new DevExpress.XtraEditors.TextEdit();
            this.textBox舒张压 = new DevExpress.XtraEditors.TextEdit();
            this.textBox收缩压 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.bgw_DP = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textBox心率.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox舒张压.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox收缩压.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            this.SuspendLayout();
            // 
            // labelState
            // 
            this.labelState.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelState.Font = new System.Drawing.Font("宋体", 19F);
            this.labelState.ForeColor = System.Drawing.Color.Red;
            this.labelState.Location = new System.Drawing.Point(0, 0);
            this.labelState.Name = "labelState";
            this.labelState.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.labelState.Size = new System.Drawing.Size(566, 28);
            this.labelState.TabIndex = 10;
            this.labelState.Text = "准备测量";
            // 
            // labelPressure
            // 
            this.labelPressure.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPressure.Font = new System.Drawing.Font("宋体", 59F);
            this.labelPressure.Location = new System.Drawing.Point(0, 28);
            this.labelPressure.Name = "labelPressure";
            this.labelPressure.Size = new System.Drawing.Size(566, 80);
            this.labelPressure.TabIndex = 8;
            this.labelPressure.Text = "0";
            this.labelPressure.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnStop);
            this.layoutControl1.Controls.Add(this.simpleButton1);
            this.layoutControl1.Controls.Add(this.btnBegin);
            this.layoutControl1.Controls.Add(this.textBox心率);
            this.layoutControl1.Controls.Add(this.textBox舒张压);
            this.layoutControl1.Controls.Add(this.textBox收缩压);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 108);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(543, 298, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(566, 390);
            this.layoutControl1.TabIndex = 18;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnStop
            // 
            this.btnStop.Appearance.Font = new System.Drawing.Font("Tahoma", 29F);
            this.btnStop.Appearance.Options.UseFont = true;
            this.btnStop.Image = ((System.Drawing.Image)(resources.GetObject("btnStop.Image")));
            this.btnStop.Location = new System.Drawing.Point(220, 2);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(153, 54);
            this.btnStop.StyleController = this.layoutControl1;
            this.btnStop.TabIndex = 23;
            this.btnStop.Text = "停止";
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 29F);
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(377, 2);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(177, 54);
            this.simpleButton1.StyleController = this.layoutControl1;
            this.simpleButton1.TabIndex = 22;
            this.simpleButton1.Text = "保存";
            this.simpleButton1.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnBegin
            // 
            this.btnBegin.Appearance.Font = new System.Drawing.Font("Tahoma", 29F);
            this.btnBegin.Appearance.Options.UseFont = true;
            this.btnBegin.Image = ((System.Drawing.Image)(resources.GetObject("btnBegin.Image")));
            this.btnBegin.Location = new System.Drawing.Point(12, 2);
            this.btnBegin.Name = "btnBegin";
            this.btnBegin.Size = new System.Drawing.Size(204, 54);
            this.btnBegin.StyleController = this.layoutControl1;
            this.btnBegin.TabIndex = 21;
            this.btnBegin.Text = "开始";
            this.btnBegin.Click += new System.EventHandler(this.btnBegin_Click);
            // 
            // textBox心率
            // 
            this.textBox心率.Location = new System.Drawing.Point(12, 247);
            this.textBox心率.Name = "textBox心率";
            this.textBox心率.Properties.Appearance.Font = new System.Drawing.Font("宋体", 25F, System.Drawing.FontStyle.Bold);
            this.textBox心率.Properties.Appearance.Options.UseFont = true;
            this.textBox心率.Size = new System.Drawing.Size(542, 40);
            this.textBox心率.StyleController = this.layoutControl1;
            this.textBox心率.TabIndex = 20;
            // 
            // textBox舒张压
            // 
            this.textBox舒张压.Location = new System.Drawing.Point(12, 170);
            this.textBox舒张压.Name = "textBox舒张压";
            this.textBox舒张压.Properties.Appearance.Font = new System.Drawing.Font("宋体", 25F, System.Drawing.FontStyle.Bold);
            this.textBox舒张压.Properties.Appearance.Options.UseFont = true;
            this.textBox舒张压.Size = new System.Drawing.Size(542, 40);
            this.textBox舒张压.StyleController = this.layoutControl1;
            this.textBox舒张压.TabIndex = 19;
            // 
            // textBox收缩压
            // 
            this.textBox收缩压.Location = new System.Drawing.Point(12, 93);
            this.textBox收缩压.Name = "textBox收缩压";
            this.textBox收缩压.Properties.Appearance.Font = new System.Drawing.Font("宋体", 25F, System.Drawing.FontStyle.Bold);
            this.textBox收缩压.Properties.Appearance.Options.UseFont = true;
            this.textBox收缩压.Size = new System.Drawing.Size(542, 40);
            this.textBox收缩压.StyleController = this.layoutControl1;
            this.textBox收缩压.TabIndex = 18;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem2,
            this.layoutControlItem1,
            this.layoutControlItem5,
            this.layoutControlItem6});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 10, 0, 10);
            this.layoutControlGroup1.Size = new System.Drawing.Size(566, 390);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 289);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(546, 91);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnBegin;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(208, 58);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.simpleButton1;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(365, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(181, 58);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 22F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem2.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem2.Control = this.textBox心率;
            this.layoutControlItem2.CustomizationFormText = "PUL/心率";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 212);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(546, 77);
            this.layoutControlItem2.Text = "PUL/心率";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(126, 30);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 22F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem1.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem1.Control = this.textBox舒张压;
            this.layoutControlItem1.CustomizationFormText = "舒张压";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 135);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(546, 77);
            this.layoutControlItem1.Text = "舒张压";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(126, 30);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 22F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem5.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem5.Control = this.textBox收缩压;
            this.layoutControlItem5.CustomizationFormText = "收缩压";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 58);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(546, 77);
            this.layoutControlItem5.Text = "收缩压";
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(126, 30);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnStop;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(208, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(157, 58);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // bgw_DP
            // 
            this.bgw_DP.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgw_DP_DoWork);
            this.bgw_DP.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgw_DP_RunWorkerCompleted);
            // 
            // uc血压RBP9808
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.labelPressure);
            this.Controls.Add(this.labelState);
            this.Name = "uc血压RBP9808";
            this.Size = new System.Drawing.Size(566, 498);
            this.Load += new System.EventHandler(this.uc血压_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textBox心率.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox舒张压.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox收缩压.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelState;
        private System.Windows.Forms.Label labelPressure;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btnBegin;
        private DevExpress.XtraEditors.TextEdit textBox心率;
        private DevExpress.XtraEditors.TextEdit textBox舒张压;
        private DevExpress.XtraEditors.TextEdit textBox收缩压;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.SimpleButton btnStop;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private System.ComponentModel.BackgroundWorker bgw_DP;
    }
}
