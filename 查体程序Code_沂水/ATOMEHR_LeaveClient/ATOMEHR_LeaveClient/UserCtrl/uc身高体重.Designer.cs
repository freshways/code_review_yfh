﻿namespace ATOMEHR_LeaveClient
{
    partial class uc身高体重
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc身高体重));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.lookUpEdit_医师签字 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt腰围 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt减体重目标 = new DevExpress.XtraEditors.TextEdit();
            this.chk减体重 = new DevExpress.XtraEditors.CheckEdit();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.txt体重指数 = new DevExpress.XtraEditors.TextEdit();
            this.txt体重 = new DevExpress.XtraEditors.TextEdit();
            this.txt身高 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_医师签字.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt腰围.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt减体重目标.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk减体重.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体重指数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体重.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身高.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.lookUpEdit_医师签字);
            this.layoutControl1.Controls.Add(this.txt腰围);
            this.layoutControl1.Controls.Add(this.txt减体重目标);
            this.layoutControl1.Controls.Add(this.chk减体重);
            this.layoutControl1.Controls.Add(this.btn保存);
            this.layoutControl1.Controls.Add(this.txt体重指数);
            this.layoutControl1.Controls.Add(this.txt体重);
            this.layoutControl1.Controls.Add(this.txt身高);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(465, 186, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(417, 386);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // lookUpEdit_医师签字
            // 
            this.lookUpEdit_医师签字.Location = new System.Drawing.Point(5, 24);
            this.lookUpEdit_医师签字.Name = "lookUpEdit_医师签字";
            this.lookUpEdit_医师签字.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit_医师签字.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit_医师签字.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit_医师签字.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit_医师签字.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_医师签字.Properties.NullText = "";
            this.lookUpEdit_医师签字.Size = new System.Drawing.Size(120, 30);
            this.lookUpEdit_医师签字.StyleController = this.layoutControl1;
            this.lookUpEdit_医师签字.TabIndex = 12;
            // 
            // txt腰围
            // 
            this.txt腰围.EditValue = "80";
            this.txt腰围.Location = new System.Drawing.Point(5, 278);
            this.txt腰围.Name = "txt腰围";
            this.txt腰围.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold);
            this.txt腰围.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txt腰围.Properties.Appearance.Options.UseFont = true;
            this.txt腰围.Properties.Appearance.Options.UseForeColor = true;
            this.txt腰围.Properties.AutoHeight = false;
            this.txt腰围.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt腰围.Properties.Items.AddRange(new object[] {
            "60",
            "70",
            "80",
            "90",
            "100",
            "110",
            "120"});
            this.txt腰围.Size = new System.Drawing.Size(407, 40);
            this.txt腰围.StyleController = this.layoutControl1;
            this.txt腰围.TabIndex = 11;
            // 
            // txt减体重目标
            // 
            this.txt减体重目标.EditValue = "";
            this.txt减体重目标.Enabled = false;
            this.txt减体重目标.Location = new System.Drawing.Point(230, 322);
            this.txt减体重目标.Name = "txt减体重目标";
            this.txt减体重目标.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold);
            this.txt减体重目标.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.txt减体重目标.Properties.Appearance.Options.UseFont = true;
            this.txt减体重目标.Properties.Appearance.Options.UseForeColor = true;
            this.txt减体重目标.Properties.Appearance.Options.UseTextOptions = true;
            this.txt减体重目标.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txt减体重目标.Properties.AutoHeight = false;
            this.txt减体重目标.Size = new System.Drawing.Size(103, 36);
            this.txt减体重目标.StyleController = this.layoutControl1;
            this.txt减体重目标.TabIndex = 9;
            // 
            // chk减体重
            // 
            this.chk减体重.Enabled = false;
            this.chk减体重.Location = new System.Drawing.Point(80, 322);
            this.chk减体重.Name = "chk减体重";
            this.chk减体重.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 13F);
            this.chk减体重.Properties.Appearance.Options.UseFont = true;
            this.chk减体重.Properties.Appearance.Options.UseTextOptions = true;
            this.chk减体重.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.chk减体重.Properties.AutoHeight = false;
            this.chk减体重.Properties.Caption = "减体重";
            this.chk减体重.Size = new System.Drawing.Size(146, 36);
            this.chk减体重.StyleController = this.layoutControl1;
            this.chk减体重.TabIndex = 8;
            this.chk减体重.CheckedChanged += new System.EventHandler(this.chk减体重_CheckedChanged);
            // 
            // btn保存
            // 
            this.btn保存.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold);
            this.btn保存.Appearance.Options.UseFont = true;
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(205, 5);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(207, 46);
            this.btn保存.StyleController = this.layoutControl1;
            this.btn保存.TabIndex = 7;
            this.btn保存.Text = "保 存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // txt体重指数
            // 
            this.txt体重指数.EditValue = "";
            this.txt体重指数.Enabled = false;
            this.txt体重指数.Location = new System.Drawing.Point(5, 213);
            this.txt体重指数.Name = "txt体重指数";
            this.txt体重指数.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold);
            this.txt体重指数.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.txt体重指数.Properties.Appearance.Options.UseFont = true;
            this.txt体重指数.Properties.Appearance.Options.UseForeColor = true;
            this.txt体重指数.Properties.AutoHeight = false;
            this.txt体重指数.Size = new System.Drawing.Size(407, 36);
            this.txt体重指数.StyleController = this.layoutControl1;
            this.txt体重指数.TabIndex = 6;
            // 
            // txt体重
            // 
            this.txt体重.EditValue = "";
            this.txt体重.Location = new System.Drawing.Point(5, 148);
            this.txt体重.Name = "txt体重";
            this.txt体重.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold);
            this.txt体重.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.txt体重.Properties.Appearance.Options.UseFont = true;
            this.txt体重.Properties.Appearance.Options.UseForeColor = true;
            this.txt体重.Properties.AutoHeight = false;
            this.txt体重.Size = new System.Drawing.Size(407, 36);
            this.txt体重.StyleController = this.layoutControl1;
            this.txt体重.TabIndex = 5;
            this.txt体重.EditValueChanged += new System.EventHandler(this.txt体重_EditValueChanged);
            // 
            // txt身高
            // 
            this.txt身高.EditValue = "";
            this.txt身高.Location = new System.Drawing.Point(5, 83);
            this.txt身高.Name = "txt身高";
            this.txt身高.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold);
            this.txt身高.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.txt身高.Properties.Appearance.Options.UseFont = true;
            this.txt身高.Properties.Appearance.Options.UseForeColor = true;
            this.txt身高.Properties.AutoHeight = false;
            this.txt身高.Size = new System.Drawing.Size(407, 36);
            this.txt身高.StyleController = this.layoutControl1;
            this.txt身高.TabIndex = 4;
            this.txt身高.EditValueChanged += new System.EventHandler(this.txt体重_EditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem4,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.emptySpaceItem2,
            this.emptySpaceItem4,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(417, 386);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem1.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem1.Control = this.txt身高;
            this.layoutControlItem1.CustomizationFormText = "身高";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 53);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(411, 65);
            this.layoutControlItem1.Text = "身高";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(76, 22);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem2.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem2.Control = this.txt体重;
            this.layoutControlItem2.CustomizationFormText = "体重";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 118);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(411, 65);
            this.layoutControlItem2.Text = "体重";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(76, 22);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem3.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem3.Control = this.txt体重指数;
            this.layoutControlItem3.CustomizationFormText = "体重指数";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 183);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(411, 65);
            this.layoutControlItem3.Text = "体重指数";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(76, 22);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.chk减体重;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(75, 317);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(150, 40);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(150, 40);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(150, 40);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.Control = this.txt减体重目标;
            this.layoutControlItem6.CustomizationFormText = "KG";
            this.layoutControlItem6.Location = new System.Drawing.Point(225, 317);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(186, 40);
            this.layoutControlItem6.Text = " ";
            this.layoutControlItem6.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(76, 19);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btn保存;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(200, 0);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 50);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(39, 50);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(211, 53);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem7.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.Highlight;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem7.Control = this.txt腰围;
            this.layoutControlItem7.CustomizationFormText = "腰围";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 248);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(411, 69);
            this.layoutControlItem7.Text = "腰围";
            this.layoutControlItem7.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(76, 22);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.lookUpEdit_医师签字;
            this.layoutControlItem8.CustomizationFormText = "医师签字";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(124, 53);
            this.layoutControlItem8.Text = "医师签字";
            this.layoutControlItem8.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(76, 16);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(124, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(76, 53);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 357);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(411, 23);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 317);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(75, 40);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 310);
            this.emptySpaceItem3.Name = "emptySpaceItem2";
            this.emptySpaceItem3.Size = new System.Drawing.Size(61, 40);
            this.emptySpaceItem3.Text = "emptySpaceItem2";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // uc身高体重
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "uc身高体重";
            this.Size = new System.Drawing.Size(417, 386);
            this.Load += new System.EventHandler(this.uc身高体重_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_医师签字.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt腰围.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt减体重目标.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk减体重.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体重指数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体重.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身高.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraEditors.TextEdit txt体重指数;
        private DevExpress.XtraEditors.TextEdit txt体重;
        private DevExpress.XtraEditors.TextEdit txt身高;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit txt减体重目标;
        private DevExpress.XtraEditors.CheckEdit chk减体重;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.ComboBoxEdit txt腰围;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_医师签字;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
    }
}
