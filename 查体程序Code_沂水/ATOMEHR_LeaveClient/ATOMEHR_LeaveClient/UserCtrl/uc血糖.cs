﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.Text.RegularExpressions;

namespace ATOMEHR_LeaveClient
{
    public partial class uc血糖 : UserControl
    {
        public uc血糖()
        {
            InitializeComponent();
        }

        public uc血糖(SerialPort spt)
        {
            InitializeComponent();
            comm = spt;
        }

        IntPtr hBluetooth;
        int nCOMID = 0; //端口

        private SerialPort comm = new SerialPort();
        private long received_count = 0;//接收计数
        private long send_count = 0;//发送计数
        private bool Listening = false;//是否没有执行完invoke相关操作
        private bool Closing = false;//是否正在关闭串口，执行Application.DoEvents，并阻止再次invoke
        private List<byte> buffer = new List<byte>(4096);//默认分配1页内存，并始终限制不允许超过
        private byte[] binary_data_1 = new byte[14];

        private void uc血糖_Load(object sender, EventArgs e)
        {
            
        }

        void Init()
        {
            //获取蓝牙操作句柄
            hBluetooth = EcgHelper.BluetoothOperationOpen();
            try
            {
                EcgHelper.BluetoothOperationConnect(hBluetooth, Program.BlueAddr, ref nCOMID);
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("未与设备进行配对！请先初始化" + ex.Message);
                return;
            }
            //添加事件注册
            comm.DataReceived += comm_DataReceived;

            //关闭时点击，则设置好端口，波特率后打开
            comm.PortName = string.Format("COM{0}", nCOMID);
            comm.BaudRate = 9600;
            try
            {
                comm.Open();
                this.lbl提示.Text = "连接成功！";
                this.btn连接.Text = "关闭";
            }
            catch (Exception ex)
            {
                //捕获到异常信息，创建一个新的comm对象，之前的不能用了。
                comm = new SerialPort();
                //现实异常信息给客户。
                MessageBox.Show(ex.Message);
            }
        }

        void comm_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (Closing) return;//如果正在关闭，忽略操作，直接返回，尽快的完成串口监听线程的一次循环
            try
            {
                Listening = true;//设置标记，说明我已经开始处理数据，一会儿要使用系统UI的。
                int n = comm.BytesToRead;//先记录下来，避免某种原因，人为的原因，操作几次之间时间长，缓存不一致
                byte[] buf = new byte[n];//声明一个临时数组存储当前来的串口数据
                received_count += n;//增加接收计数
                comm.Read(buf, 0, n);//读取缓冲数据

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //<协议解析>
                bool data_1_catched = false;//缓存记录数据是否捕获到
                //1.缓存数据
                buffer.AddRange(buf);
                //2.完整性判断
                while (buffer.Count > 4)//至少要包含头（2字节）+长度（1字节）+机器代码（1字节）
                {
                    //请不要担心使用>=，因为>=已经和>,<,=一样，是独立操作符，并不是解析成>和=2个符号
                    //2.1 查找数据头
                    if (buffer[0] == 0x53 && buffer[1] == 0x4E)
                    {
                        //2.2探测缓存数据是否有一条数据的字节，如果不够，就不用费劲的做其他验证了
                        //前面已经限定了剩余长度>=4，那我们这里一定能访问到buffer[2]这个长度
                        int len = buffer[2];//数据长度
                        if (buffer.Count < len + 2) break;//数据不够的时候什么都不做
                        //2.3 校验数据，确认数据正确
                        //异或校验用^=和值校验直接+=，逐个字节异或得到校验码
                        byte checksum = 0;
                        for (int i = 2; i < len + 2; i++)//len+3表示校验之前的位置
                        {
                            checksum += buffer[i];//这里用的和值校验
                        }
                        if (checksum != buffer[len + 2]) //如果数据校验失败，丢弃这一包数据
                        {
                            buffer.RemoveRange(0, len + 2);//从缓存中删除错误数据
                            continue;//继续下一次循环
                        }
                        //至此，已经被找到了一条完整数据。我们将数据直接分析，或是缓存起来一起分析
                        //我们这里采用的办法是缓存一次，好处就是如果你某种原因，数据堆积在缓存buffer中
                        //已经很多了，那你需要循环的找到最后一组，只分析最新数据，过往数据你已经处理不及时
                        //了，就不要浪费更多时间了，这也是考虑到系统负载能够降低。
                        buffer.CopyTo(0, binary_data_1, 0, len + 2);//复制一条完整数据到具体的数据缓存
                        data_1_catched = true;
                        buffer.RemoveRange(0, len + 2);//正确分析一条数据，从缓存中移除数据。
                    }
                    else
                    {
                        //这里是很重要的，如果数据开始不是头，则删除数据
                        buffer.RemoveAt(0);
                    }
                }
                //分析数据
                if (data_1_catched)
                {
                    //我们的数据都是定好格式的，所以当我们找到分析出的数据1，就知道固定位置一定是这些数据，我们只要显示就可以了
                    if (binary_data_1[5] == 0x0B)//关机
                    {
                        this.Invoke((EventHandler)(delegate
                        {
                            this.btn连接.Text = "连接";
                            this.lbl提示.Text = "请插入试条确保设备开启";
                        }));
                        if (comm.IsOpen) comm.Close();
                        //如果设备发送过来的是关机命令，则重置
                        comm = new SerialPort();
                    }
                    else if (binary_data_1[5] == 0x03) //滴血符号闪烁
                    {
                        this.Invoke((EventHandler)(delegate
                        {
                            this.lbl提示.Text = "准备就绪请采集血液样本";
                        }));
                    }
                    else if (binary_data_1[5] != 0x04)//结果
                    {
                        string data = (binary_data_1[6] + binary_data_1[7]).ToString("X2");
                        //更新界面
                        this.Invoke((EventHandler)(delegate
                        {
                            double d = Convert.ToInt32(data.Replace(" ", ""), 16) * 0.1;
                            txt血糖.Text = d.ToString() + "mmol/L";
                        }));
                    }
                }
                //如果需要别的协议，只要扩展这个data_n_catched就可以了。往往我们协议多的情况下，还会包含数据编号，给来的数据进行
                //编号，协议优化后就是： 头+编号+长度+数据+校验
                //</协议解析>
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////

            }
            finally
            {
                Listening = false;//我用完了，ui可以关闭串口了。
            }
        }

        private void btn保存_Click(object sender, EventArgs e)
        {
            if (funCheck())
            {
                try
                {
                    tb_健康体检Info tb_New = new tb_健康体检Info();
                    //tb_New.身份证号 = currentUser.ID;
                    //tb_New.出生日期 = currentUser.Birth;
                    //tb_New.姓名 = currentUser.Name;
                    //tb_New.性别 = currentUser.Gender;

                    tb_New.空腹血糖 = (this.txt血糖.Text.Trim());

                    if (tb_健康体检DAL.UpdateSys_血糖(tb_New))
                    {
                        MessageBox.Show("保存数据成功！");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private bool funCheck()
        {
            if (string.IsNullOrEmpty(this.txt血糖.Text.Trim()))
            {
                this.lbl提示.Text = "未检测到血糖值，请检查血糖设备是否连接！";
                return false;
            }
            return true;
        }

        private void btn连接_Click(object sender, EventArgs e)
        {

            string powerDown = "53-4E-06-00-04-0B-02-00-17";// 关机指令
            if (this.btn连接.Text.Replace(" ","") == "连接")
            {
                Init();
            }
            else
            {
                //定义一个变量，记录发送了几个字节
                int n = 0;
                //16进制发送
                if (comm.IsOpen)
                {
                    string SendString = powerDown.Replace("-", "");//txSend.Text.Replace("-", "");
                    //我们不管规则了。如果写错了一些，我们允许的，只用正则得到有效的十六进制数
                    MatchCollection mc = Regex.Matches(SendString, @"(?i)[\da-f]{2}");
                    List<byte> buf = new List<byte>();//填充到这个临时列表中
                    //依次添加到列表中
                    foreach (Match m in mc)
                    {
                        //buf.Add(byte.Parse(m.Value));
                        buf.Add(Convert.ToByte(m.Value, 16));
                    }
                    //转换列表为数组后发送
                    comm.Write(buf.ToArray(), 0, buf.Count);
                    //记录发送的字节数
                    n = buf.Count;
                    //关闭
                    comm.Close();
                }
                
                this.btn连接.Text = "连接";
                this.lbl提示.Text = "请插入试条确保设备开启";
                Closing = true;
            }
        }


    }
}
