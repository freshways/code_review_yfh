﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace ATOMEHR_LeaveClient
{
    public partial class uc血氧检测 : Base_UserControl
    {
        public IntPtr handler;
        public int nError = 0;
        int nCOMID = 0; //端口
        IntPtr hBluetooth;//蓝牙句柄
        IntPtr hCOMPort;//串口句柄
        
        public uc血氧检测()
        {
            InitializeComponent();
        }


        /// <summary>
        /// 检查蓝牙连接情况
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frm血氧检测_Load(object sender, EventArgs e)
        {
            try
            {
                Init();
            }
            catch (Exception ex)
            {
                this.lblMsg.Text = ex.Message;
            }
        }

        void Init()
        {
            hBluetooth = 血氧信息.BluetoothOperationOpen();

            if (hBluetooth != null)
            {
                //if (血氧信息.BluetoothOperationConnect(hBluetooth, "(94:21:97:60:56:BF)", ref nCOMID))
                if (血氧信息.BluetoothOperationConnect(hBluetooth, Program.BlueAddr, ref nCOMID))
                {
                    hCOMPort = 血氧信息.SerialPortOpen((int)DEVICE_TYPE_PC_60NW.DEVICE_TYPE_PC_60NW_1);
                    if (hCOMPort != null)
                    {
                        if (血氧信息.SerialPortConnect(hCOMPort, nCOMID)) //血氧的设备如果不先打开串口，事件不被调用
                        {
                            血氧信息.FingerOximeterSetParamAction(hCOMPort, true); //不执行这个获取不到数据
                            血氧信息.FingerOximeterRegisterSpO2ParamCallBack(hCOMPort, fun_ON_GET_SPO2_PARAM_PW);
                        }
                        else
                        {
                            nError = EcgHelper.CreativeHealthGetLastError();
                            this.lblMsg.Text = "初始化失败：" + nError.ToString();
                        }
                    }
                }
            }
            else
            {
                nError = EcgHelper.CreativeHealthGetLastError();
            }
        }

        public void fun_ON_GET_SPO2_PARAM_PW(bool bProbeOff, int nSpO2, int nPR, int nPI, int nMode, int nPower)
        {
            try
            {
                this.textBox灌注指数.Text = (nPI/10.0).ToString()+"%";
                this.textBox脉率.Text = nPR.ToString();
                this.textBox血氧.Text = nSpO2.ToString();
            }
            catch (Exception ex)
            {
                this.lblMsg.Text = ex.Message;
            }

        }
        /// <summary>
        /// 重新读取
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn重新读取_Click(object sender, EventArgs e)
        {
            try
            {
                this.lblMsg.Text = "等待结果...";
                血氧信息.SerialPortDisconnect(hCOMPort);//重新获取时要先关闭串口

                if (血氧信息.SerialPortConnect(hCOMPort, nCOMID))
                {
                    血氧信息.FingerOximeterSetParamAction(hCOMPort, true);
                    血氧信息.FingerOximeterRegisterSpO2ParamCallBack(hCOMPort, fun_ON_GET_SPO2_PARAM_PW);
                }
                else
                {
                    this.lblMsg.Text = "请插入手指...";
                    MessageBox.Show("请插入手指");
                }
            }
            catch (Exception ex)
            {
                this.lblMsg.Text = ex.Message;
            }
        }

        private void btn保存_Click(object sender, EventArgs e)
        {
            try
            {
                血氧信息.SerialPortDisconnect(hCOMPort);//先关闭串口

                tb_健康体检Info tb_New = new tb_健康体检Info();

                if (!string.IsNullOrEmpty(this.textBox脉率.Text.Trim()))
                {
                    tb_New.脉搏 = Convert.ToInt32(this.textBox脉率.Text.Trim());
                    tb_New.心率 = Convert.ToInt32(this.textBox脉率.Text.Trim());
                    if (tb_健康体检DAL.UpdateSys_脉搏(tb_New))
                    {
                        MessageBox.Show("保存成功！");
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("请填写脉搏！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
    }
}
