﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ATOMEHR_LeaveClient.Util
{
    public enum DBConn
    {
        connGW,
        connHost,
        connSQLite,
    }
    class SqlSugarConn
    {
        //---公卫服务器
        private static string strServer = ControlsHelper.IniReadValue("BlueTooth", "SerVer", Path.Combine(Program.toAppPath, Program.iniName));
        private static string strUid = ControlsHelper.IniReadValue("BlueTooth", "uid", Path.Combine(Program.toAppPath, Program.iniName));
        private static string strPwd = ControlsHelper.IniReadValue("BlueTooth", "pwd", Path.Combine(Program.toAppPath, Program.iniName));
        private static string strDatabase = ControlsHelper.IniReadValue("BlueTooth", "database", Path.Combine(Program.toAppPath, Program.iniName));
        private static string connGW = @"Data Source=" + strServer + ";Initial Catalog=" + strDatabase + ";User Id=" + strUid + ";Password=" + strPwd + ";";

        //---主机
        private static string strZJServer = ControlsHelper.IniReadValue("BlueTooth", "ZJSerVer", Path.Combine(Program.toAppPath, Program.iniName));
        private static string strZJUid = ControlsHelper.IniReadValue("BlueTooth", "ZJUid", Path.Combine(Program.toAppPath, Program.iniName));
        private static string strZJPwd = ControlsHelper.IniReadValue("BlueTooth", "ZJPwd", Path.Combine(Program.toAppPath, Program.iniName));
        private static string strZJDatabase = ControlsHelper.IniReadValue("BlueTooth", "ZJDatabase", Path.Combine(Program.toAppPath, Program.iniName));
        private static string connHost = @"Data Source=" + strZJServer + ";Initial Catalog=" + strZJDatabase + ";User Id=" + strZJUid + ";Password=" + strZJPwd + ";";

        //---SQLiteDB路径//2019年5月8日 yufh 更新 ，为了不覆盖原来数据，新版本用新库
        private static string strSQLitePath = Environment.CurrentDirectory + "\\Data\\YSDB2019.db";
        private static string connSQLite = "Data Source=" + strSQLitePath + ";Version=3;";

        public SqlSugarClient db;

        public SqlSugarConn(DBConn dbConn)
        {
            switch (dbConn)
            {
                case DBConn.connGW:
                    MySqlSugarClient(connGW, DbType.SqlServer);
                    break;
                case DBConn.connHost:
                    MySqlSugarClient(connHost, DbType.SqlServer);
                    break;
                case DBConn.connSQLite:
                    MySqlSugarClient(connSQLite, DbType.Sqlite);
                    break;
                default:
                    break;
            }
        }

        private void MySqlSugarClient(string str_Conn, DbType DBType)
        {
            ConnectionConfig cc = new ConnectionConfig();
            cc.ConnectionString = str_Conn;
            cc.DbType = DBType;
            cc.IsAutoCloseConnection = true;
            cc.InitKeyType = InitKeyType.Attribute;

            db = new SqlSugarClient(cc);
        }
    }
}
