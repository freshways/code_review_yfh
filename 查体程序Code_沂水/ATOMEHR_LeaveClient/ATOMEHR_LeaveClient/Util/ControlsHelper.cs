﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.Grid;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Text.RegularExpressions;
using System.Collections;
using DevExpress.XtraReports.UI;
using System.IO;
using System.Runtime.InteropServices;
using System.Xml;

namespace ATOMEHR_LeaveClient
{
    public static class ControlsHelper
    {
        /// <summary>
        /// Bind Combobox Data
        /// </summary>
        /// <param name="key"></param>
        /// <param name="cbo"></param>
        public static void BindComboxData(DataTable table, ComboBoxEdit cbo)
        {
            cbo.Properties.Items.Clear();
            if (table.Rows.Count > 0)
            {
                ComboxData comboxData = null;
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    comboxData = new ComboxData();
                    comboxData.Text = table.Rows[i]["P_DESC"] as string;
                    comboxData.Value = table.Rows[i]["P_CODE"] as string;
                    cbo.Properties.Items.Add(comboxData);
                }
            }
            // 解决IConvertible问题
            //cbo.ParseEditValue += new ConvertEditValueEventHandler(comboBox_ParseEditValue);
        }
        /// <summary>
        /// combox绑定数据
        /// </summary>
        /// <param name="group_id"></param>
        /// <param name="cbo"></param>
        /// <param name="includeAll"></param>
        public static void BindComboxData(DataTable table, ComboBoxEdit cbo, bool includeAll)
        {
            cbo.Properties.Items.Clear();
            if (table.Rows.Count > 0)
            {
                ComboxData comboxData = null;
                if (includeAll)
                {
                    comboxData = new ComboxData() { Value = "", Text = "全部" };
                    cbo.Properties.Items.Add(comboxData);
                }
                else
                {
                    comboxData = new ComboxData() { Value = "", Text = "请选择" };
                    cbo.Properties.Items.Add(comboxData);
                }

                for (int i = 0; i < table.Rows.Count; i++)
                {
                    comboxData = new ComboxData();
                    comboxData.Text = table.Rows[i]["P_DESC"] as string;
                    comboxData.Value = table.Rows[i]["P_CODE"] as string;
                    cbo.Properties.Items.Add(comboxData);
                }
            }
            cbo.SelectedIndex = 0;
            // 解决IConvertible问题
            //cbo.ParseEditValue += new ConvertEditValueEventHandler(comboBox_ParseEditValue);
        }

        /// <summary>
        /// combox绑定数据
        /// </summary>
        /// <param name="group_id"></param>
        /// <param name="cbo"></param>
        /// <param name="includeAll"></param>
        public static void BindComboxData(DataTable table, ComboBoxEdit cbo, string valueMember, string displayMember)
        {
            cbo.Properties.Items.Clear();
            if (table.Rows.Count > 0)
            {
                ComboxData comboxData = null;

                comboxData = new ComboxData() { Value = "", Text = "请选择" };
                cbo.Properties.Items.Add(comboxData);

                for (int i = 0; i < table.Rows.Count; i++)
                {
                    comboxData = new ComboxData();
                    comboxData.Text = table.Rows[i][displayMember] as string;
                    comboxData.Value = table.Rows[i][valueMember] as string;
                    cbo.Properties.Items.Add(comboxData);
                }
            }
            cbo.SelectedIndex = 0;
            // 解决IConvertible问题
            //cbo.ParseEditValue += new ConvertEditValueEventHandler(comboBox_ParseEditValue);
        }

        public static void BindComboxDataNull(DataTable table, ComboBoxEdit cbo, string valueMember, string displayMember)
        {
            cbo.Properties.Items.Clear();
            if (table.Rows.Count > 0)
            {
                ComboxData comboxData = null;

                comboxData = new ComboxData() { Value = "", Text = "" };
                cbo.Properties.Items.Add(comboxData);

                for (int i = 0; i < table.Rows.Count; i++)
                {
                    comboxData = new ComboxData();
                    comboxData.Text = table.Rows[i][displayMember] as string;
                    comboxData.Value = table.Rows[i][valueMember] as string;
                    cbo.Properties.Items.Add(comboxData);
                }
            }
            cbo.SelectedIndex = 0;
            // 解决IConvertible问题
            //cbo.ParseEditValue += new ConvertEditValueEventHandler(comboBox_ParseEditValue);
        }

        /// <summary>
        /// Bind Combobox Data
        /// </summary>
        /// <param name="group_id"></param>
        /// <param name="cbo"></param>
        /// <param name="isInitValue"></param>
        public static void BindComboxData(DataTable table, RepositoryItemComboBox cbo)
        {
            cbo.Items.Clear();
            if (table.Rows.Count > 0)
            {
                ComboxData comboxData = null;
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    comboxData = new ComboxData();
                    comboxData.Text = table.Rows[i]["P_DESC"] as string;
                    comboxData.Value = table.Rows[i]["P_CODE"] as string;
                    cbo.Items.Add(comboxData);
                }
            }
            //3.下拉框选中值改变事件
            cbo.SelectedIndexChanged += new EventHandler(ComboBoxEdit_SelectedIndexChanged);
            // 解决IConvertible问题
            cbo.ParseEditValue += new ConvertEditValueEventHandler(comboBox_ParseEditValue);

        }

        private static void ComboBoxEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboxData item = new ComboxData();
            try
            {
                //1.获取下拉框选中值
                //item = (ComboxData)(sender as ComboBoxEdit).SelectedItem;
                //string text = item.Text.ToString();
                //string value = (string)item.Value;
                ////2.获取gridview选中的行
                //GridView myView = (gridControl1.MainView as GridView);
                //int dataIndex = myView.GetDataSourceRowIndex(myView.FocusedRowHandle);
                ////3.保存选中值到datatable
                //dt.Rows[dataIndex]["value"] = value;
                //dt.Rows[dataIndex]["text"] = text;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "提示");
            }
        }

        //public static string GetComboxKey(RepositoryItemComboBox cbo)
        //{
        //    string value = string.Empty;
        //    //if (cbo.Properties.Items.Count > 0) { }
        //    //foreach (var item in cbo.Properties.Items)
        //    //{
        //    //    ComboxData
        //    //       tt = item as ComboxData;

        //    //    if (tt != null)
        //    //    {
        //    //        if (tt.Value == id)
        //    //        {
        //    //            comboBoxEdit1.Text = tt.Text;
        //    //        }
        //    //    }

        //    //}}
        //    if (cbo != null)
        //    {
        //        ComboxData selectedItem = cbo.SelectedItem as ComboxData;
        //        if (selectedItem == null)
        //        {
        //            ComboxData _selectedItem = null;
        //            foreach (var item in cbo.Properties.Items)
        //            {
        //                _selectedItem = item as ComboxData;
        //                if (_selectedItem.Text == cbo.SelectedItem as string)
        //                {
        //                    value = _selectedItem.Value;
        //                }
        //            }
        //        }
        //        else { value = selectedItem.Value; }

        //    }
        //    return value;
        //}

        public static string getDefaultValue(string groupId)
        {
            return "";
        }

        /// <summary>
        /// 根据key值设置cbo的选中值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="cbo"></param>
        public static void SetComboxData(string key, ComboBoxEdit cbo)
        {
            if (string.IsNullOrEmpty(key)) return;
            if (cbo.Properties.Items.Count > 0)
            {
                foreach (var item in cbo.Properties.Items)
                {
                    ComboxData _item = item as ComboxData;
                    if (_item != null)
                    {
                        if (_item.Value == key)
                        {
                            cbo.Text = _item.Text;
                        }
                    }
                }
            }
            else
            {
                return;
            }
        }

        /// <summary>
        /// 根据key值设置cbo的选中值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="cbo"></param>
        public static void SetComboxNullData(string key, ComboBoxEdit cbo, string nulltext)
        {
            if (string.IsNullOrEmpty(key))
            {
                cbo.Text = nulltext;
                cbo.EditValue = null;
                return;
            }
        }
        /// <summary>
        /// 获取cbo的Key值
        /// </summary>
        /// <param name="cbo"></param>
        /// <returns></returns>
        public static string GetComboxKey(ComboBoxEdit cbo)
        {
            string value = string.Empty;

            if (cbo != null)
            {
                ComboxData selectedItem = cbo.SelectedItem as ComboxData;
                if (selectedItem == null)
                {
                    ComboxData _selectedItem = null;
                    foreach (var item in cbo.Properties.Items)
                    {
                        _selectedItem = item as ComboxData;
                        if (_selectedItem.Text == cbo.SelectedItem as string)
                        {
                            value = _selectedItem.Value;
                        }
                    }
                }
                else { value = selectedItem.Value; }

            }
            return value;
        }
        public static void comboBox_ParseEditValue(object sender, ConvertEditValueEventArgs e)
        {
            e.Value = e.Value.ToString();
            e.Handled = true;
        }
        public static void ExcelExport(GridView gridView, Form form)
        {
            gridView.CloseEditor();//关闭编辑

            if (gridView.RowCount <= 0)
            {
                XtraMessageBox.Show("没有可导出的信息，请确认！", "导出提醒", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            SaveFileDialog sfdExcelDown = new SaveFileDialog();
            sfdExcelDown.Filter = "Microsoft Excel|*.xls;*.xlsx";
            sfdExcelDown.InitialDirectory = GetExcelDefaultPath();
            sfdExcelDown.FileName = GetExcelDownloadDefaultFileName(form);
            DialogResult result = sfdExcelDown.ShowDialog();
            if (result != DialogResult.OK) return;

            try
            {
                string fileName = sfdExcelDown.FileName;
                gridView.OptionsPrint.AutoWidth = false;
                gridView.OptionsPrint.ShowPrintExportProgress = false;
                gridView.OptionsPrint.PrintHeader = gridView.OptionsView.ShowColumnHeaders;
                gridView.OptionsPrint.PrintFooter = gridView.OptionsView.ShowFooter;
                gridView.OptionsPrint.UsePrintStyles = true;

                gridView.ExportToXlsx(sfdExcelDown.FileName);
            }
            catch (System.IO.IOException ex)
            {
                //Msg.Warning(ex.Message);
                XtraMessageBox.Show("下载文件出错，请联系管理员！", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public static string GetExcelDefaultPath()
        {
            Microsoft.Win32.RegistryKey folders;

            folders = OpenRegistryPath(Registry.CurrentUser, @"\software\microsoft\windows\currentversion\explorer\shell folders");
            // Windows用户桌面路径
            string desktopPath = folders.GetValue("Desktop").ToString();
            //// Windows用户字体目录路径
            //string fontsPath = folders.GetValue("Fonts").ToString();
            //// Windows用户网络邻居路径
            //string nethoodPath = folders.GetValue("Nethood").ToString();
            //// Windows用户我的文档路径
            //string personalPath = folders.GetValue("Personal").ToString();
            //// Windows用户开始菜单程序路径
            //string programsPath = folders.GetValue("Programs").ToString();
            //// Windows用户存放用户最近访问文档快捷方式的目录路径
            //string recentPath = folders.GetValue("Recent").ToString();
            //// Windows用户发送到目录路径
            //string sendtoPath = folders.GetValue("Sendto").ToString();
            //// Windows用户开始菜单目录路径
            //string startmenuPath = folders.GetValue("Startmenu").ToString();
            //// Windows用户开始菜单启动项目录路径
            //string startupPath = folders.GetValue("Startup").ToString();
            //// Windows用户收藏夹目录路径
            //string favoritesPath = folders.GetValue("Favorites").ToString();
            //// Windows用户网页历史目录路径
            //string historyPath = folders.GetValue("History").ToString();
            //// Windows用户Cookies目录路径
            //string cookiesPath = folders.GetValue("Cookies").ToString();
            //// Windows用户Cache目录路径
            //string cachePath = folders.GetValue("Cache").ToString();
            //// Windows用户应用程式数据目录路径
            //string appdataPath = folders.GetValue("Appdata").ToString();
            //// Windows用户打印目录路径
            //string printhoodPath = folders.GetValue("Printhood").ToString();
            return desktopPath;
        }
        private static RegistryKey OpenRegistryPath(RegistryKey root, string s)
        {
            s = s.Remove(0, 1) + @"\";
            while (s.IndexOf(@"\") != -1)
            {
                root = root.OpenSubKey(s.Substring(0, s.IndexOf(@"\")));
                s = s.Remove(0, s.IndexOf(@"\") + 1);
            }
            return root;
        }
        public static string GetExcelDownloadDefaultFileName(Form form)
        {
            return "公共卫生系统_" + form.Text + "_Excel.xlsx";
        }
        /// <summary>
        /// 绑定combox方法
        /// </summary>
        /// edited by wang.maoguo 
        /// 2015-04-17
        /// <param name="dt">数据源 datatable</param>
        /// <param name="combox">控件combox</param>
        /// <param name="includeAll">是否包含全部</param>
        public static void BindDataforCombox(DataTable dt, ComboBoxEdit combox, bool includeAll)
        {
            combox.Properties.Items.Clear();
            if (dt == null || combox == null) return;
            combox.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            ComboxData data = null;
            if (dt.Rows.Count > 0)
            {
                if (includeAll)
                {
                    data = new ComboxData() { Value = "00", Text = "全部" };
                    combox.Properties.Items.Add(data);
                }

                foreach (DataRow item in dt.Rows)
                {
                    data = new ComboxData();
                    data.Text = item["name"].ToString();
                    data.Value = item["value"].ToString();
                    combox.Properties.Items.Add(data);
                }

            }
        }
        /// <summary>
        /// 价格大小写转换
        /// 王茂国  2015年4月27日
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static string ConvertDigitsToChinese(double x)
        {
            string s = x.ToString("#L#E#D#C#K#E#D#C#J#E#D#C#I#E#D#C#H#E#D#C#G#E#D#C#F#E#D#C#.0B0A");
            string d = Regex.Replace(s, @"((?<=-|^)[^1-9]*)|((?'z'0)[0A-E]*((?=[1-9])|(?'-z'(?=[F-L\.]|$))))|((?'b'[F-L])(?'z'0)[0A-L]*((?=[1-9])|(?'-z'(?=[\.]|$))))", "${b}${z}");
            string result = Regex.Replace(d, ".", m => "负元空零壹贰叁肆伍陆柒捌玖空空空空空空空分角拾佰仟萬億兆京垓秭穰"[m.Value[0] - '-'].ToString());
            return string.IsNullOrEmpty(result) ? "零元" : result;
        }


        public class ComboxData
        {
            public string Text { set; get; }
            public string Value { set; get; }
            public override string ToString()
            {
                return Text;
            }
        }
        public static int GetAge(string birth, string nowDate)
        {
            int returnAge = 0;
            char[] cArr = { '-', ' ', ':' };
            string[] strBirthdayArr = birth.Split(cArr);

            int birthYear = Convert.ToInt32(strBirthdayArr[0]);
            int birthMonth = Convert.ToInt32(strBirthdayArr[1]);
            int birthDay = Convert.ToInt32(strBirthdayArr[2]);

            int nowYear = Convert.ToInt32(nowDate.Substring(0, 4));
            int nowMonth = Convert.ToInt32(nowDate.Substring(5, 2));
            int nowDay = Convert.ToInt32(nowDate.Substring(8, 2));
            if (nowYear == birthYear)
            {
                returnAge = 0;
            }
            else
            {
                int ageDiff = nowYear - birthYear;

                if (ageDiff > 0)
                {
                    if (nowMonth == birthMonth)
                    {
                        int dayDiff = nowDay - birthDay;
                        if (dayDiff < 0)
                            returnAge = ageDiff - 1;
                        else
                            returnAge = ageDiff;
                    }
                    else
                    {
                        int monthDiff = nowMonth - birthMonth;
                        if (monthDiff < 0)
                            returnAge = ageDiff - 1;
                        else
                            returnAge = ageDiff;
                    }
                }
                else
                {
                    returnAge = -1;
                }
            }
            return returnAge;
        }
        public static bool Check身份证号(string str身份证号)
        {
            if ((Regex.IsMatch(str身份证号, @"^(^\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$", RegexOptions.IgnoreCase)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #region flow绑定处理

        /// <summary>
        /// 获取flow控件的值
        /// </summary>
        /// <param name="flow"></param>
        /// <returns></returns>
        public static object GetFlowLayoutResult(FlowLayoutPanel flow)
        {
            if (flow == null || flow.Controls.Count == 0) return "";
            string result = "";
            for (int i = 0; i < flow.Controls.Count; i++)
            {
                if (flow.Controls[i].GetType() == typeof(CheckEdit))
                {
                    CheckEdit chk = (CheckEdit)flow.Controls[i];
                    if (chk == null) continue;
                    if (chk.Checked)
                    {
                        result += chk.Tag + ",";
                    }
                }
            }
            result = string.IsNullOrEmpty(result) ? result : result.Remove(result.Length - 1);
            return result;
        }

        /// <summary>
        /// 绑定flow控件的值
        /// </summary>
        /// <param name="valueList"></param>
        /// <param name="flow"></param>
        public static void SetFlowLayoutResult(string valueList, FlowLayoutPanel flow)
        {
            if (string.IsNullOrEmpty(valueList)) valueList = "";
            if (flow == null) return;
            //if (string.IsNullOrEmpty(valueList)) return;
            //if (flow == null) return;
            for (int j = 0; j < flow.Controls.Count; j++)
            {
                if (flow.Controls[j].GetType() == typeof(CheckEdit))
                {
                    CheckEdit chk = (CheckEdit)flow.Controls[j];
                    chk.Checked = false;
                }
            }
            if (string.IsNullOrEmpty(valueList)) return;
            string[] list = valueList.Split(',');
            for (int i = 0; i < list.Length; i++)
            {
                string item = list[i];
                if (string.IsNullOrEmpty(item)) continue;

                for (int j = 0; j < flow.Controls.Count; j++)
                {
                    if (flow.Controls[j].GetType() == typeof(CheckEdit))
                    {
                        CheckEdit chk = (CheckEdit)flow.Controls[j];
                        string tag = chk.Tag == null ? "" : Convert.ToString(chk.Tag);
                        if (string.IsNullOrEmpty(tag)) continue;
                        if (tag == item)
                        {
                            chk.Checked = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 显示页面绑定flow控件的值
        /// </summary>
        /// <param name="type"></param>
        /// <param name="ValueCodes"></param>
        /// <returns></returns>
        public static string SetFlowLayoutValues(string type, string ValueCodes)
        {
            try
            {
                string[] scode = ValueCodes.Split(new char[] { ',' });
                string rtString = "";
                foreach (string s in scode)
                    rtString += ReturnDis字典显示(type, s) + ",";
                rtString = rtString.Substring(0, rtString.Length - 1);
                return rtString;
            }
            catch
            {
                return ValueCodes;
            }
        }
        #endregion

        public static string ReturnDis字典显示(string _类型, string _编码)
        {
            if (DataDictCache.Cache.t常用字典 != null)
            {
                DataRow[] dr = DataDictCache.Cache.t常用字典.Select("(P_FUN_CODE='" + _类型 + "' or P_FUN_DESC = '" + _类型 + "') and P_CODE='" + _编码 + "' ");

                if (dr.Length > 0)
                {
                    return dr[0]["P_DESC"].ToString();
                }
            }
            return _编码;
        }

        #region ini文件操作
        [DllImport("kernel32")]
        public static extern long WritePrivateProfileString(string section, string key, string val, string filePath);//写入ini
        [DllImport("kernel32")]
        public static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath); //读取ini
        /// <summary>
        /// 写入ini方法
        /// </summary>
        /// <param name="section">段</param>
        /// <param name="key">键</param>
        /// <param name="value">值</param>
        /// <param name="iniPath">路径</param>
        public static void IniWriteValue(string section, string key, string value, string iniPath)
        {
            WritePrivateProfileString(section, key, value, iniPath);
        }
        /// <summary>
        /// 读取ini方法
        /// </summary>
        /// <param name="section">段</param>
        /// <param name="key">键</param>
        /// <param name="iniPath">路径</param>
        /// <returns></returns>
        public static string IniReadValue(string section, string key, string iniPath)
        {
            StringBuilder sBulider = new StringBuilder(500);
            int i = GetPrivateProfileString(section, key, "", sBulider, 500, iniPath);
            return sBulider.ToString();
        }
        #endregion



        //------------王森代码----------------------
        /// <summary>
        /// 获取配置信息
        /// </summary>
        /// <param name="path"></param>
        /// <param name="appKey"></param>
        /// <returns></returns>
        public static string GetConfigValue(string appKey, string appName)
        {
            XmlDocument xDoc = new XmlDocument();
            XmlNode xNode;
            XmlElement xElem = null;
            try
            {
                xDoc.Load(Application.StartupPath + "//" + appName + ".config");
                xNode = xDoc.SelectSingleNode("//appSettings");
                xElem = (XmlElement)xNode.SelectSingleNode("//add[@key=\"" + appKey + "\"]");
            }
            catch (Exception ex)
            {
                throw ex;

            }
            if (xElem != null)
                return xElem.GetAttribute("value");
            else
                return "";
        }

        /// <summary>
        /// 设置ComboBox的默认值
        /// </summary>
        /// <param name="comboBox"></param>
        /// <param name="value"></param>
        public static void setComboBoxSelectedItem(ComboBoxEdit comboBox, string value)
        {
            if (null == value || "".Equals(value))
            {
                return;
            }

            for (int i = 0; i < comboBox.Properties.Items.Count; i++)
            {
                ComboxData comboxData = (ComboxData)comboBox.Properties.Items[i];
                if (value.Equals(comboxData.Value))
                {
                    comboBox.SelectedIndex = i;
                }
            }
        }

        /// <summary>
        /// 设置ComboBox的默认值
        /// </summary>
        /// <param name="comboBox"></param>
        /// <param name="value"></param>
        public static void setComboBoxSelectedItem(ComboBoxEdit comboBox, int val)
        {
            setComboBoxSelectedItem(comboBox, val.ToString());
        }

        /// <summary>
        /// 批量设置ComboBox的默认值
        /// </summary>
        /// <param name="list"></param>
        public static void setComboBoxSelectedItem(List<Dictionary<string, Object>> list)
        {
            ComboBoxEdit comboBox;
            string value;
            foreach (Dictionary<string, Object> item in list)
            {
                if (item.ContainsKey("comboBox") && item.ContainsKey("value"))
                {
                    if ((item["comboBox"] is ComboBoxEdit) && (item["value"] is string))
                    {
                        comboBox = (ComboBoxEdit)item["comboBox"];
                        value = (string)item["value"];
                        setComboBoxSelectedItem(comboBox, value);
                    }

                }

            }
        }
        /// <summary>
        /// 保存配置信息至.config文件
        /// </summary>
        /// <param name="AppKey"></param>
        /// <param name="AppValue"></param>
        public static void SetValue(string AppKey, string AppValue, string appName)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(Application.StartupPath + "//" + appName + ".config");

            XmlNode xNode;
            XmlElement xElem1;
            XmlElement xElem2;
            xNode = xDoc.SelectSingleNode("//appSettings");

            xElem1 = (System.Xml.XmlElement)xNode.SelectSingleNode("//add[@key='" + AppKey + "']");
            if (xElem1 != null) xElem1.SetAttribute("value", AppValue);
            else
            {
                xElem2 = xDoc.CreateElement("add");
                xElem2.SetAttribute("key", AppKey);
                xElem2.SetAttribute("value", AppValue);
                xNode.AppendChild(xElem2);
            }
            xDoc.Save(System.Windows.Forms.Application.ExecutablePath + ".config");
        }
        //----------------------------------
    }
}
