﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SQLite;

namespace ATOMEHR_LeaveClient
{
    /// <summary>
    /// 数据库函数处理类
    /// </summary>
    public class SqliteClass
    {
        public SqliteClass()
        {

        }
        private DBClass dbConstring;

        #region 通用查询数据集函数
        /// <summary>
        /// 查询返回DataSet任何表的信息，包含分页功能 （通用函数）
        /// </summary>
        /// <param name="qp">参数实体类(包括：SQL语句，翻页参数)</param>
        /// <param name="RecordCount">输出函数：查询的总记录数</param>
        /// <param name="bPagination">是否分页</param>
        /// <returns></returns>
        public System.Data.DataSet QueryTableInfo(QueryParam qp, out int RecordCount, bool bPagination)
        {
            dbConstring = new DBClass(DbFilePath.FilePathName);
            SQLiteConnection oraConn = new SQLiteConnection(dbConstring.m_ConnString);

            int TotalRecordForPageIndex = qp.PageIndex * qp.PageSize;
            int FirstRecordForPageIndex = (qp.PageIndex - 1) * qp.PageSize;

            //定义排序功能
            string strOrderBy;
            if (qp.OrderType == 1)
            {
                strOrderBy = " ORDER BY " + qp.Orderfld + " DESC";
            }
            else
            {
                strOrderBy = " ORDER BY " + qp.Orderfld + " ASC";
            }
            StringBuilder sb = new StringBuilder();
            if (bPagination)//True为分页，False不分页
            {
                sb.AppendFormat("SELECT * FROM (SELECT A.*, ROWNUM RN FROM (SELECT {0} FROM {1} {2} {3}) A WHERE ROWNUM <= {4})WHERE RN > {5} ",
                    qp.ReturnFields, qp.TableName.ToUpper(), qp.Where, strOrderBy, TotalRecordForPageIndex, FirstRecordForPageIndex);
            }
            else
            {
                //判断条件和排序字段是否为空
                if (qp.Where != null && qp.Orderfld != null)
                {
                    sb.AppendFormat("SELECT {0} FROM {1} {2} {3}", qp.ReturnFields, qp.TableName.ToUpper(), qp.Where, strOrderBy);
                }
                else
                {
                    if (qp.Where == null && qp.Orderfld != null)
                    {
                        sb.AppendFormat("SELECT {0} FROM {1} {2}", qp.ReturnFields, qp.TableName.ToUpper(), strOrderBy);
                    }
                    else if (qp.Where != null && qp.Orderfld == null)
                    {
                        sb.AppendFormat("SELECT {0} FROM {1} {2}", qp.ReturnFields, qp.TableName.ToUpper(), qp.Where);
                    }
                    else if (qp.Where == null && qp.Orderfld == null)
                    {
                        sb.AppendFormat("SELECT {0} FROM {1}", qp.ReturnFields, qp.TableName.ToUpper());
                    }
                }
            }

            System.Data.DataSet datSet = null;
            datSet = new DataSet();
            oraConn.Open();//打开数据库连接

            SQLiteCommand oraCommand = null;
            oraCommand = new SQLiteCommand("", oraConn);
            RecordCount = 0;
            try
            {
                datSet = dbConstring.GetDataSetBySql(sb.ToString(), qp.TableName);
                oraCommand.CommandText = String.Format("SELECT COUNT(*) FROM {0} {1}", qp.TableName, qp.Where);
                RecordCount = Convert.ToInt32(oraCommand.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
            finally
            {
                oraCommand.Dispose();
                oraConn.Dispose();
                oraConn.Close();
            }

            return datSet;

        }
        #endregion




        public List<int> GetAllTablesIDByField(string strTableName, string strField, int nFieldValue)
        {
            List<int> listId = new List<int>();
            dbConstring = new DBClass(DbFilePath.FilePathName);
            string strSql = "SELECT ID FROM " + strTableName + " WHERE " + strField + "=" + nFieldValue;
            SQLiteConnection conn = new SQLiteConnection(dbConstring.m_ConnString);
            SQLiteCommand comm = new SQLiteCommand(strSql, conn);
            SQLiteDataReader datRead = null;
            try
            {
                conn.Open();
                datRead = comm.ExecuteReader();
                while (datRead.Read())
                {
                    listId.Add(Convert.ToInt32(datRead["ID"]));
                }
            }
            catch { }
            finally
            {
                conn.Close();
                comm.Dispose();
            }
            return listId;
        }

        /// <summary>
        /// 添加、修改
        /// </summary>
        /// <param name="Info"></param>
        /// <returns></returns>
        public bool UpdateSys_尿常规(tb_健康体检Info Info)
        {
            bool bBool = false;
            dbConstring = new DBClass(DbFilePath.FilePathName);


            string strSql = "", strTableName = "tb_健康体检";
            strSql = "SELECT * FROM " + strTableName + " WHERE (个人档案编号=" + Program.currentUser.DocNo +
                " or 身份证号=" + Program.currentUser.ID + ") and 体检日期='" + DateTime.Now.ToString("yyyy-MM-dd") + "'  ";
            SQLiteConnection oraConn = null;
            SQLiteDataAdapter datAdapter = null;
            System.Data.DataSet datSet = new DataSet();

            oraConn = new SQLiteConnection(dbConstring.m_ConnString);
            oraConn.Open();//打开数据连接
            datAdapter = new SQLiteDataAdapter(strSql, oraConn);
            SQLiteCommandBuilder oraCmmdBuilder = new SQLiteCommandBuilder(datAdapter);
            datAdapter.Fill(datSet, strTableName);
            System.Data.DataTable datTable = null;
            datTable = datSet.Tables[0];
            System.Data.DataRow datRow = null;

            bool bAdd = false;
            if (datTable.Rows.Count > 0)
            {
                datRow = datTable.Rows[0];
            }
            else
            {
                bAdd = true;
            }

            if (bAdd)
            {
                //增加一行
                datRow = datTable.NewRow();
                //datRow["ID"] = Info.ID;
                datRow["身份证号"] = Program.currentUser.ID;
                datRow["姓名"] = Program.currentUser.Name;
                datRow["性别"] = Program.currentUser.Gender;
                datRow["出生日期"] = Program.currentUser.Birth;
            }
            datRow["尿蛋白"] = Info.尿蛋白;
            datRow["尿潜血"] = Info.尿潜血;
            datRow["尿糖"] = Info.尿糖;
            datRow["尿酮体"] = Info.尿酮体;
            datRow["尿微量白蛋白"] = Info.尿微量白蛋白;

            if (bAdd)
            {
                datTable.Rows.Add(datRow);
            }

            try
            {
                datAdapter.Update(datSet, strTableName);
                bBool = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                //关闭连接
                datSet = null;
                datTable = null;
                datRow = null;
                datAdapter.Dispose();
                oraCmmdBuilder.Dispose();
                oraConn.Dispose();
                oraConn.Close();
            }
            return bBool;
        }
    }
}
