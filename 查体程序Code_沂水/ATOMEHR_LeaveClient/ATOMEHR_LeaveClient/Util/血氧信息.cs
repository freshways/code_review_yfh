﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace ATOMEHR_LeaveClient
{
    public static class 血氧信息
    {

        #region 委托

        public delegate void ONDISCOVERYCOMPLETEDDelegate(int nCount, IntPtr devices);
        public delegate void ON_GET_DEVICE_VER_FODelegate(int nCount, IntPtr devices);
        /// <summary>
        /// 获取到血氧参数时调用。采样周期为 1s
        /// </summary>
        /// <param name="bProbeOff">bProbeOff 为 CREATIVE_TRUE 时，表示探头脱落</param>
        /// <param name="nSpO2">血氧饱和度值。单位为%。0 为无效值。如果 nSpO 为 98，则血氧饱和度为 98%</param>
        /// <param name="nPR">脉率值。单位是 bpm，0 为无效值</param>
        /// <param name="nPI">血流灌注指数。单位是%。0 为无效值。如果 nPI 为 145，则血流灌注指数为 14.5%。</param>
        /// <param name="nMode">测量模式。包括成人模式、新生儿模式和动物模式。参照 6.1.2 血氧测量模式</param>
        /// <param name="nPower">设备电池电量，精度为 0.1。如果 nPower 为 26，则电池电量为 2.6V</param>
        public delegate void ON_GET_SPO2_PARAM_PWDelegate(bool bProbeOff, int nSpO2, int nPR, int nPI, int nMode, int nPower);
        /// <summary>
        /// 获取到血氧波形时调用。采样频率为 50Hz。波形数值范围 0~127
        /// </summary>
        /// <param name="nLength">获取到血氧波形数据的组数</param>
        /// <param name="arrWave">血氧波形数据列表</param>
        public delegate void ON_GET_SPO2_WAVEDelegate(int nLength, IntPtr arrWave);

        #endregion

        #region 变量
        public static IntPtr handler;
        public static int nError = 0;
        public static User currentUser;
        #endregion

        #region 操作蓝牙
        /// <summary>
        /// 搜索远程蓝牙设备
        /// </summary>
        /// <param name="hBluetooth">用于蓝牙相关操作的句柄。</param>
        /// <param name="timeOut">搜索超时时间，如输入 15，则超时时间为 15s</param>
        /// <returns>如果返回CREATIVE_TRUE则调用成功，返回CREATIVE_FALSE则调用失败，通过CreativeDeviceGetLastError获得错误代码</returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern Boolean BluetoothOperationDiscovery(IntPtr hBluetooth, int timeOut);

        /// <summary>
        /// 获取用于蓝牙操作的句柄
        /// </summary>
        /// <returns></returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern IntPtr BluetoothOperationOpen();
        /// <summary>
        /// 注册 ON_DISCOVERY_COMPLETED 回调函数
        /// </summary>
        /// <param name="handler">hBluetooth: 用于蓝牙相关操作的句柄</param>
        /// <param name="func">回调函数地址。</param>
        /// <returns></returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern Boolean BluetoothOperationRegisterDiscoveryCallBack(IntPtr handler, ONDISCOVERYCOMPLETEDDelegate func);
        /// <summary>
        /// 获取错误代码
        /// </summary>
        /// <returns></returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern int CreativeHealthGetLastError();
        /// <summary>
        /// 与指定蓝牙设备进行连接。
        /// </summary>
        /// <param name="handler"></param>
        /// <param name="Addr"></param>
        /// <param name="nCOMID"></param>
        /// <returns></returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool BluetoothOperationConnect(IntPtr handler, string Addr, ref int nCOMID);
        /// <summary>
        /// 断开蓝牙连接。
        /// </summary>
        /// <param name="handler"></param>
        /// <returns></returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool BluetoothOperationDisConnect(IntPtr handler);
        /// <summary>
        /// 关闭蓝牙句柄
        /// </summary>
        /// <param name="handler"></param>
        /// <returns></returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool BluetoothOperationClose(IntPtr handler);
        #endregion

        #region 操作串口
        /// <summary>
        /// 获取用于串口操作的句柄
        /// </summary>
        /// <param name="nDeviceType"></param>
        /// <returns>不为NULL则调用成功，如果失败通过CreativeHealthGet LastError 获得错误代码</returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern IntPtr SerialPortOpen(int nDeviceType);
        /// <summary>
        /// 连接串口
        /// </summary>
        /// <param name="hCOMPort">用于串口相关操作的句柄</param>
        /// <param name="nCOMID">需要连接的串口号，由 BluetoothOperationConnect 获得</param>
        /// <returns>如果返回CREATIVE_TRUE则调用成功，返回CREATIVE_FALSE则调用失败，通过CreativeDeviceGetLastError获得错误代码</returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool SerialPortConnect(IntPtr hCOMPort, int nCOMID);
        /// <summary>
        /// 与串口断开连接
        /// </summary>
        /// <param name="hCOMPort">用于串口相关操作的句柄</param>
        /// <returns></returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool SerialPortDisconnect(IntPtr hCOMPort);
        /// <summary>
        /// 关闭串口句柄
        /// </summary>
        /// <param name="hCOMPort">用于串口相关操作的句柄</param>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern void SerialPortClose(IntPtr hCOMPort);
        #endregion

        #region 操作血氧
        /// <summary>
        /// 可通过该方法来设置设备开始或者停止发送血氧参数数据
        /// </summary>
        /// <param name="hCOMPort">用于串口相关操作的句柄</param>
        /// <param name="bFlag">CREATIVE_TRUE开始发送数据，CREATIVE_FALSE停止发送数据</param>
        /// <returns>如果返回CREATIVE_TRUE则调用成功，返回CREATIVE_FALSE则调用失败，通过CreativeDeviceGetLastError获得错误代码</returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool FingerOximeterSetParamAction(IntPtr hCOMPort, bool bFlag);
        /// <summary>
        /// 可通过该方法来设置设备开始或者停止发送血氧波形数据
        /// </summary>
        /// <param name="hCOMPort">用于串口相关操作的句柄</param>
        /// <param name="bFlag">CREATIVE_TRUE开始发送数据，CREATIVE_FALSE停止发送数据</param>
        /// <returns>如果返回CREATIVE_TRUE则调用成功，返回CREATIVE_FALSE则调用失败，通过CreativeDeviceGetLastError获得错误代码</returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool FingerOximeterSetWaveAction(IntPtr hCOMPort, bool bFlag);
        /// <summary>
        /// 查询设备版本信息
        /// </summary>
        /// <param name="hCOMPort">用于串口相关操作的句柄</param>
        /// <returns>如果返回CREATIVE_TRUE则调用成功，返回CREATIVE_FALSE则调用失败，通过CreativeDeviceGetLastError获得错误代码</returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool FingerOximeterQueryDeviceVer(IntPtr hCOMPort);
        /// <summary>
        /// 注册 ON_GET_DEVICE_VER_FO 回调函数
        /// </summary>
        /// <param name="hCOMPort">用于串口相关操作的句柄</param>
        /// <param name="func">回调函数地址</param>
        /// <returns>如果返回CREATIVE_TRUE则调用成功，返回CREATIVE_FALSE则调用失败，通过CreativeDeviceGetLastError获得错误代码</returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool FingerOximeterRegisterDeviceVerCallBack(IntPtr hCOMPort, ON_GET_DEVICE_VER_FODelegate func);
        /// <summary>
        /// 注册 ON_GET_SPO2_PARAM_PW 回调函数
        /// </summary>
        /// <param name="hCOMPort">用于串口相关操作的句柄</param>
        /// <param name="Func">回调函数地址。</param>
        /// <returns>如果返回CREATIVE_TRUE则调用成功，返回CREATIVE_FALSE则调用失败，通过CreativeDeviceGetLastError获得错误代码</returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool FingerOximeterRegisterSpO2ParamCallBack(IntPtr hCOMPort, ON_GET_SPO2_PARAM_PWDelegate Func);
        /// <summary>
        /// 注册 ON_GET_SPO2_WAVE 回调函数
        /// </summary>
        /// <param name="hCOMPort">用于串口相关操作的句柄</param>
        /// <param name="Func">回调函数地址</param>
        /// <returns>如果返回CREATIVE_TRUE则调用成功，返回CREATIVE_FALSE则调用失败，通过CreativeDeviceGetLastError获得错误代码</returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool FingerOximeterRegisterSpO2WaveCallBack(IntPtr hCOMPort, ON_GET_SPO2_WAVEDelegate Func);
        #endregion

    }

    /// <summary>
    /// 蓝牙设备 结构体
    /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    public struct tagBLUETOOTH_DEVICE
    {
        public string Name { get; set; }//蓝牙名称，如：”PC_300SNT”。
        public string Addr { get; set; }//蓝牙地址，如：”(00:1D:07:3E:F5:60)”
    }


    /// <summary>
    /// 设备类型代码 枚举
    /// </summary>
    public enum DEVICE_TYPE_PC_60NW
    {
        DEVICE_TYPE_PC_60NW = 7000,//PC-60NW 设备的代码为 7000
        DEVICE_TYPE_PC_60NW_1 = 7001//PC-60NW-1 设备的代码为 7001
    }
    /// <summary>
    /// 手指血氧 数据定义
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct tagWave
    {
        int nFlag;//当产生波形数据时，波形点的搏动标记。当有搏动的时候 nFlag 为 1，否则为 0
        int nData;//波形数据
    }
    /// <summary>
    /// 血氧测量模式
    /// </summary>
    public enum SPO2_MODE
    {
        SPO2_MODE_ADULT = 5015,//成人模式。
        SPO2_MODE_NEONATE = 5016,//说明：新生儿模式。
        SPO2_MODE_ANIMAL = 5017//说明：动物模式
    }
}
