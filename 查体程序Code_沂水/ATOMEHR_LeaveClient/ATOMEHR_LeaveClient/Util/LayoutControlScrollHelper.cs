﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ATOMEHR_LeaveClient
{
    public class LayoutControlScrollHelper
    {
        DevExpress.XtraLayout.LayoutControl scrollableControl;

        public LayoutControlScrollHelper(DevExpress.XtraLayout.LayoutControl scrollableControl)
        {
            this.scrollableControl = scrollableControl;
        }

        //Demo中调用了这个方法
        public void EnableScrollOnMouseWheel()
        {
            scrollableControl.VisibleChanged += OnVisibleChanged;
        }

        //EnableScrollOnMouseWheel()这个方法并不太实用，所有新建了下面的这个
        public void EnableScrollOnMouseWheelAllTime()
        {
            scrollableControl.Select();
            UnsubscribeFromMouseWheel(scrollableControl.Controls);
            SubscribeToMouseWheel(scrollableControl.Controls);
        }

        void OnVisibleChanged(object sender, EventArgs e)
        {
            scrollableControl.Select();
            UnsubscribeFromMouseWheel(scrollableControl.Controls);
            SubscribeToMouseWheel(scrollableControl.Controls);
        }

        private void SubscribeToMouseWheel(Control.ControlCollection controls)
        {
            foreach (Control ctrl in controls)
            {
                ctrl.MouseWheel += OnMouseWheel;
                SubscribeToMouseWheel(ctrl.Controls);
            }
        }

        private void UnsubscribeFromMouseWheel(Control.ControlCollection controls)
        {
            foreach (Control ctrl in controls)
            {
                ctrl.MouseWheel -= OnMouseWheel;
                UnsubscribeFromMouseWheel(ctrl.Controls);
            }
        }

        void OnMouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            Control control = sender as Control;
            if (control.Bounds.Contains(e.Location)) return;
            DevExpress.Utils.DXMouseEventArgs.GetMouseArgs(e).Handled = true;
            DevExpress.XtraLayout.Scrolling.ScrollInfo scrollInfo = ((DevExpress.XtraLayout.ILayoutControl)scrollableControl).Scroller;
            int scrollValue = scrollInfo.VScrollPos;
            int smallChange = scrollInfo.VScrollArgs.SmallChange;
            if (e.Delta < 0)
            {
                int delta = scrollValue + smallChange;
                scrollInfo.VScrollPos = Math.Min(delta, scrollInfo.VScrollArgs.Maximum);
            }
            else
                if (scrollValue < smallChange)
                    scrollInfo.VScrollPos = 0;
                else
                    scrollInfo.VScrollPos -= smallChange;
        }

        public void DisableScrollOnMouseWheel()
        {
            scrollableControl.VisibleChanged -= OnVisibleChanged;
            UnsubscribeFromMouseWheel(scrollableControl.Controls);
        }
    }
}
