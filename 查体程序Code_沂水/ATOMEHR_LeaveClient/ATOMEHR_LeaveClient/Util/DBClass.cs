﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;
using System.Data;
using System.Windows.Forms;

namespace ATOMEHR_LeaveClient
{
    /// <summary>
    /// 数据库操作类
    /// </summary>
    public class DBClass
    {
        /// <summary>
        /// 数据库文件路径
        /// </summary>
        public string m_ConnString;
        /// <summary>
        /// 数据库连接对象
        /// </summary>
        public SQLiteConnection m_Connection;

        /// <summary>
        /// 不带参数构造函数
        /// </summary>
        public DBClass()
        {

        }

        /// <summary>
        /// 带参数的构造函数
        /// </summary>
        /// <param name="strfileName">数据库文件名称</param>
        public DBClass(string strfileName)
        {
            StringBuilder strBuild = new StringBuilder();
            strBuild.AppendFormat("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}", strfileName);
            this.m_ConnString = strBuild.ToString();
            this.m_Connection = new SQLiteConnection(this.m_ConnString);
        }



        /// <summary>
        /// 打开数据库       
        /// </summary>
        public void Open()
        {
            m_Connection.Open();
        }

        /// <summary>
        /// 关闭数据库
        /// </summary>
        public void Close()
        {
            m_Connection.Close();
        }

        public void Dispose()
        {
            try
            {
                this.m_ConnString = null;
                this.m_Connection.Close();
            }
            catch { }
            this.m_Connection.Dispose();
        }
        /// <summary>
        /// 返回DataSet数据集合
        /// </summary>
        /// <param name="strCommandString">sql语句</param>
        /// <param name="strTableName">填充表名</param>
        /// <returns></returns>
        public DataSet GetDataSetBySql(string strCommandString, string strTableName)
        {
            this.m_Connection.Open();
            SQLiteCommand command = new SQLiteCommand(strCommandString, m_Connection);
            SQLiteDataAdapter datAdapter = new SQLiteDataAdapter();
            datAdapter.SelectCommand = new SQLiteCommand(strCommandString, m_Connection);
            DataSet dsDataSet = new DataSet();
            try
            {
                datAdapter.Fill(dsDataSet, strTableName);
            }
            catch
            {
            }
            finally
            {
                m_Connection.Close();
            }
            return dsDataSet;
        }

    }


    /// <summary>
    /// 文件路径
    /// </summary>
    public class DbFilePath
    {
        /// <summary>
        /// 数据库文件路径
        /// </summary>
        public static string FilePathName = Application.StartupPath + @"\Data\YSDB2019.db";
    }
}
