﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATOMEHR_LeaveClient.Util
{
    class Bean_医师签字模块
    {
        private string _症状;

        public string 症状
        {
            get { return _症状; }
            set { _症状 = value; }
        }
        private string _身高体重;

        public string 身高体重
        {
            get { return _身高体重; }
            set { _身高体重 = value; }
        }
        private string _血压;

        public string 血压
        {
            get { return _血压; }
            set { _血压 = value; }
        }
        private string _生活方式;

        public string 生活方式
        {
            get { return _生活方式; }
            set { _生活方式 = value; }
        }
        private string _脏器功能;

        public string 脏器功能
        {
            get { return _脏器功能; }
            set { _脏器功能 = value; }
        }
        private string _查体;

        public string 查体
        {
            get { return _查体; }
            set { _查体 = value; }
        }
        private string _辅助检查;

        public string 辅助检查
        {
            get { return _辅助检查; }
            set { _辅助检查 = value; }
        }
        private string _心电图;

        public string 心电图
        {
            get { return _心电图; }
            set { _心电图 = value; }
        }
        private string _胸部X线片;

        public string 胸部X线片
        {
            get { return _胸部X线片; }
            set { _胸部X线片 = value; }
        }
        private string _B超;

        public string B超
        {
            get { return _B超; }
            set { _B超 = value; }
        }
        private string _现存主要健康问题;

        public string 现存主要健康问题
        {
            get { return _现存主要健康问题; }
            set { _现存主要健康问题 = value; }
        }
        private string _主要用药情况;

        public string 主要用药情况
        {
            get { return _主要用药情况; }
            set { _主要用药情况 = value; }
        }
        private string _非免疫规划预防接种史;

        public string 非免疫规划预防接种史
        {
            get { return _非免疫规划预防接种史; }
            set { _非免疫规划预防接种史 = value; }
        }
        private string _健康评价;

        public string 健康评价
        {
            get { return _健康评价; }
            set { _健康评价 = value; }
        }
        private string _健康指导;

        public string 健康指导
        {
            get { return _健康指导; }
            set { _健康指导 = value; }
        }
        private string _结果反馈;

        public string 结果反馈
        {
            get { return _结果反馈; }
            set { _结果反馈 = value; }
        }
    }
}
