﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Drawing;
using System.Runtime.Serialization.Formatters.Binary;

namespace ATOMEHR_LeaveClient
{
    class bluetooth
    {
        //枚举设备
        [DllImport("SS-100WT-SDK.dll", EntryPoint = "FindBlthDev", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        public static extern int FindBlthDev(byte[] outBuff, int inBuffSize);

        //连接设备
        [DllImport("SS-100WT-SDK.dll", EntryPoint = "Connect100WT", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        public static extern bool Connect100WT(byte[] blthName,int port);

        //断开设备
        [DllImport("SS-100WT-SDK.dll", EntryPoint = "Disconnet100WT", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        public static extern bool Disconnet100WT();

        //复位SAM
        [DllImport("SS-100WT-SDK.dll", EntryPoint = "ResetSAM", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        public static extern int ResetSAM();

        //SAM状态检测
        [DllImport("SS-100WT-SDK.dll", EntryPoint = "CheckSAM", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        public static extern int CheckSAM();

        //读SAM管理信息
        [DllImport("SS-100WT-SDK.dll", EntryPoint = "ReadSAM", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        public static extern int ReadSAM(byte[] SAMNum);

        //寻找居民身份证
        [DllImport("SS-100WT-SDK.dll", EntryPoint = "FindCard", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        public static extern int FindCard();

        //选取居民身份证
        [DllImport("SS-100WT-SDK.dll", EntryPoint = "GetCard", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        public static extern int GetCard();

        //读文字和照片信息
        [DllImport("SS-100WT-SDK.dll", EntryPoint = "ReadContent", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        public static extern int ReadContent(ref int contentLength, byte[] contentBuff, ref int picLength, byte[] picBuff, byte[] fileRoot);

        //读文字照片和指纹
        [DllImport("SS-100WT-SDK.dll", EntryPoint = "ReadContentS", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        public static extern int ReadContentS(ref int contentLength, byte[] contentBuff, ref int picLength, byte[] picBuff, ref int fingerLength, byte[] fingerBuff, byte[] fileRoot);

        //读追加地址
        [DllImport("SS-100WT-SDK.dll", EntryPoint = "ReadAddress", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        public static extern int ReadAddress(ref int addressLength, byte[] addressBuff, byte[] fileRoot);

        //设置波特率
        [DllImport("SS-100WT-SDK.dll", EntryPoint = "SetBaud", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        public static extern int SetBaud(int baud);

        //设置SAM一帧通讯数据最大字节数 
        [DllImport("SS-100WT-SDK.dll", EntryPoint = "SetMaxData", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        public static extern int SetMaxData(int max);

        //一键读卡全部信息包括指纹
        [DllImport("SS-100WT-SDK.dll", EntryPoint = "ReadCard", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        public static extern int ReadCard();

    }
    /// <summary>
    /// 身份证的信息
    /// </summary>
    public class CardInfo
    {
        // 姓名
        public string Name;
        // 性别
        public string Sex;
        // 民族
        public string Nation;
        // 出生日期
        public string Birthday;
        // 地址
        public string Address;
        // 身份证号码
        public string CardNo;
        // 发卡机关
        public string Department;
        // 证件开始日期
        public string StartDate;
        // 证件结束日期
        public string EndDate;
        // 相片路径
        public string PhotoPath;
        // 追加地址
        public string AddressEx;
    }
}
