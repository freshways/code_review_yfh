using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.SQLite;
using System.Runtime.InteropServices;
using System.IO;

namespace ATOMEHR_LeaveClient
{
    /// <summary>
    /// DBManager类
    /// BY：狂人代码生成器 V1.0
    /// 作者：金属狂人
    /// 日期：2016年04月12日 03:22:55
    /// </summary>
    public static class DBManager
    {

        /// <summary>
        /// 连接字符串
        /// </summary>
        private static string connStr = @"Server=WANG;uid=sa;pwd=1234;database=YSDB_Leave";
        //private static string conn121Str = @"Server=192.168.10.121;uid=yggsuser;pwd=yggsuser;database=AtomEHR.YSDB";
        //---公卫服务器
        static string strServer = ControlsHelper.IniReadValue("BlueTooth", "SerVer", Path.Combine(Program.toAppPath, Program.iniName));
        static string strUid = ControlsHelper.IniReadValue("BlueTooth", "uid", Path.Combine(Program.toAppPath, Program.iniName));
        static string strPwd = ControlsHelper.IniReadValue("BlueTooth", "pwd", Path.Combine(Program.toAppPath, Program.iniName));
        static string strDatabase = ControlsHelper.IniReadValue("BlueTooth", "database", Path.Combine(Program.toAppPath, Program.iniName));
        private static string conn121Str = @"Server=" + strServer + ";uid=" + strUid + ";pwd=" + strPwd + ";database=" + strDatabase;

        //---主机
        static string strZJServer = ControlsHelper.IniReadValue("BlueTooth", "ZJSerVer", Path.Combine(Program.toAppPath, Program.iniName));
        static string strZJUid = ControlsHelper.IniReadValue("BlueTooth", "ZJUid", Path.Combine(Program.toAppPath, Program.iniName));
        static string strZJPwd = ControlsHelper.IniReadValue("BlueTooth", "ZJPwd", Path.Combine(Program.toAppPath, Program.iniName));
        static string strZJDatabase = ControlsHelper.IniReadValue("BlueTooth", "ZJDatabase", Path.Combine(Program.toAppPath, Program.iniName));
        private static string connZJStr = @"Server=" + strZJServer + ";uid=" + strZJUid + ";pwd=" + strZJPwd + ";database=" + strZJDatabase;


        //private static string conn121Str = @"Server=192.168.8.162\SQL2008;uid=sa;pwd=jhwsy120;database=AtomEHR.Normal";
        //private static string connStr = @"Server=(local);uid=sa;pwd=1234;database=YSDB_Leave";

        /// <summary>
        /// 获取一个新的连接
        /// </summary>
        /// <param name="connStr">连接字符串</param>
        /// <returns></returns>
        public static SqlConnection Conn
        {
            get
            {
                return new SqlConnection(connStr);
            }
        }
        public static SqlConnection Conn121
        {
            get
            {
                return new SqlConnection(conn121Str);
            }
        }
        public static SqlConnection ConnZJ
        {
            get
            {
                return new SqlConnection(connZJStr);
            }
        }
        public static SQLiteConnection SQLiteConn
        {
            get
            {
                DBClass dbClass;
                dbClass = new DBClass(DbFilePath.FilePathName);
                SQLiteConnection oraConn = new SQLiteConnection(dbClass.m_ConnString);
                return oraConn;
            }
        }
        /// <summary>
        /// 执行增、删、改 SQL语句
        /// </summary>
        /// <param name="sqlStr">SQL语句</param>
        /// <param name="param">参数列表</param>
        /// <returns>影响的行数</returns>
        public static int ExecuteUpdate(SqlConnection connType, string sqlStr, params object[] param)
        {
            SqlCommand cmd = new SqlCommand(sqlStr, connType);
            cmd.Parameters.AddRange(param);
            cmd.Connection.Open();
            int rows = 0;
            try
            {
                rows = cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return rows;
        }

        public static int ExecuteUpdate(SQLiteConnection connType, string sqlStr, params object[] param)
        {
            SQLiteCommand cmd = new SQLiteCommand(sqlStr, connType);
            cmd.Parameters.AddRange(param);
            cmd.Connection.Open();
            int rows = 0;
            try
            {
                rows = cmd.ExecuteNonQuery();
                //Beep();//保存成功后提示音
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection.Dispose();
            }
            return rows;
        }

        private static object[] ParamListToArray(List<SQLiteParameter> paramList)
        {
            if (null != paramList)
            {
                SQLiteParameter[] parames = new SQLiteParameter[paramList.Count];

                for (int i = 0; i < paramList.Count; i++)
                {
                    parames[i] = paramList[i];
                }

                return parames;
            }
            else
            {
                return new SQLiteParameter[0];
            }
        }

        public static int ExecuteSQLitUpdate(string sqlStr, params object[] param)
        {
            SQLiteCommand cmd = new SQLiteCommand(sqlStr, SQLiteConn);
            cmd.Parameters.AddRange(param);
            cmd.Connection.Open();
            int rows = 0;
            try
            {
                rows = cmd.ExecuteNonQuery();
                //Beep();//保存成功后提示音
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection.Dispose();
            }
            return rows;
        }

        public static int ExecuteSQLitUpdate(string sqlStr, List<SQLiteParameter> paramList)
        {
            return ExecuteSQLitUpdate(sqlStr, ParamListToArray(paramList));
        }

        /// <summary>
        /// 执行查询SQL语句
        /// </summary>
        /// <param name="sqlStr">SQL语句</param>
        /// <param name="param">参数列表</param>
        /// <returns></returns>
        public static SqlDataReader ExecuteQuery(string sqlStr, params object[] param)
        {
            SqlCommand cmd = new SqlCommand(sqlStr, Conn121);
            cmd.Parameters.AddRange(param);
            cmd.Connection.Open();
            try
            {
                return cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            }
            catch (Exception)
            {
                cmd.Connection.Close();
                throw;
            }
        }
        public static SQLiteDataReader ExecuteQueryForSQLite(string sqlStr, params object[] param)
        {
            SQLiteCommand cmd = new SQLiteCommand(sqlStr, SQLiteConn);
            cmd.Parameters.AddRange(param);
            cmd.Connection.Open();
            try
            {
                return cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            }
            catch (Exception)
            {
                cmd.Connection.Close();
                throw;
            }
        }
        public static SQLiteDataReader ExecuteQueryForSQLite(string sqlStr)
        {
            SQLiteCommand cmd = new SQLiteCommand(sqlStr, SQLiteConn);
            cmd.Connection.Open();
            try
            {
                return cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            }
            catch (Exception)
            {
                cmd.Connection.Close();
                throw;
            }
        }

        public static bool IsTableExist(string tableName)
        {
            bool isExist = false;
            string sqlStr = "SELECT COUNT(*) countNum FROM sqlite_master where type='table' and name=@tableName";
            SQLiteCommand cmd = new SQLiteCommand(sqlStr, SQLiteConn);
            SQLiteParameter param = new SQLiteParameter("tableName", tableName);
            cmd.Parameters.Add(param);
            cmd.Connection.Open();
            SQLiteDataReader reader;
            try
            {
                reader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
                if (reader.Read())
                {
                    isExist = Convert.ToInt32(reader["countNum"]) > 0;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }

            return isExist;
        }

        //在指定数据库中创建用药情况表
        public static void create用药情况表()
        {
            string sql = "CREATE TABLE tb_用药情况配置 (排序 NUMERIC, id INTEGER PRIMARY KEY, 名称 TEXT, 拼音码 TEXT)";
            SQLiteCommand command = new SQLiteCommand(sql, SQLiteConn);
            command.Connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                command.Connection.Close();
            }
        }

        /// <summary>
        /// 执行存储过程 - 无结果集
        /// </summary>
        /// <param name="paroName">存储过程名称</param>
        /// <param name="param">参数列表</param>
        /// <returns></returns>
        public static int ExecuteProc(SqlConnection connType, string procName, params object[] param)
        {
            SqlCommand cmd = new SqlCommand(procName, connType);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddRange(param);
            cmd.Connection.Open();
            try
            {
                return cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        /// <summary>
        /// 执行存储过程 - 有结果集
        /// </summary>
        /// <param name="paroName">存储过程名称</param>
        /// <param name="param">参数列表</param>
        /// <returns></returns>
        public static SqlDataReader ExecuteProcByReader(SqlConnection conn, string procName, params object[] param)
        {
            SqlCommand cmd = new SqlCommand(procName, conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddRange(param);
            cmd.Connection.Open();
            try
            {
                return cmd.ExecuteReader();
            }
            catch
            {
                cmd.Connection.Close();
                throw;
            }
        }

        public static int UpdateByDataSet(DataSet ds, string strTblName)
        {
            try
            {
                //SqlConnection  conn = new SqlConnection(Conn));
                SqlDataAdapter myAdapter = new SqlDataAdapter();
                SqlCommand myCommand = new SqlCommand(("select * from " + strTblName), Conn);
                myAdapter.SelectCommand = myCommand;
                SqlCommandBuilder myCommandBuilder = new SqlCommandBuilder(myAdapter);
                myAdapter.Update(ds, strTblName);
                return 0;
            }
            catch (Exception errBU)
            {
                throw errBU;
            }

        }


        private static DBClass dbConstring;
        /// <summary>
        /// 添加、修改
        /// </summary>
        /// <param name="Info"></param>
        /// <returns></returns>
        public static bool UpdateDataSetForSqlite(DataSet ds, string strTblName)
        {
            bool bBool = false;
            dbConstring = new DBClass(DbFilePath.FilePathName);
            try
            {
                SQLiteConnection oraConn = new SQLiteConnection(dbConstring.m_ConnString);
                SQLiteDataAdapter myAdapter = new SQLiteDataAdapter();
                SQLiteCommand myCommand = new SQLiteCommand(("select * from " + strTblName), oraConn);
                myAdapter.SelectCommand = myCommand;
                SQLiteCommandBuilder myCommandBuilder = new SQLiteCommandBuilder(myAdapter);
                myAdapter.Update(ds, strTblName);
                bBool = true;
                Beep();//保存成功后提示音
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return bBool;
        }

        /// <param name="iFrequency">声音频率（从37Hz到32767Hz）。在windows95中忽略</param>  
        /// <param name="iDuration">声音的持续时间，以毫秒为单位。</param>  
        [DllImport("Kernel32.dll")] //引入命名空间 using System.Runtime.InteropServices;  
        public static extern bool Beep(int frequency, int duration);

        /// <summary>
        /// 调用系统发音
        /// </summary>
        public static void Beep()
        {
            Beep(1500, 500);
        }

    }
}
