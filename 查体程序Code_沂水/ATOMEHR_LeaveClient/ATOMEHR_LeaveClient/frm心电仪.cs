﻿using InTheHand.Net;
using InTheHand.Net.Bluetooth;
using InTheHand.Windows.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ATOMEHR_LeaveClient
{
    public partial class frm心电仪 : Form
    {

        private ObexListener listener;
        private bool serviceStarted;

        BluetoothRadio radio = null;//蓝牙适配器  
        string sendFileName = null;//发送文件名  
        BluetoothAddress sendAddress = null;//发送目的地址  
        //ObexListener listener = null;//监听器  
        string recDir = null;//接受文件存放目录  
        Thread listenThread, sendThread;//发送/接收线程 

        public frm心电仪()
        {
            InitializeComponent();
            InTheHand.Net.Bluetooth.BluetoothRadio.PrimaryRadio.Mode = InTheHand.Net.Bluetooth.RadioMode.Discoverable;
            listener = new ObexListener(ObexTransport.Bluetooth);

            radio = BluetoothRadio.PrimaryRadio;//获取当前PC的蓝牙适配器  
            CheckForIllegalCrossThreadCalls = false;//不检查跨线程调用  
            if (radio == null)//检查该电脑蓝牙是否可用  
            {
                MessageBox.Show("这个电脑蓝牙不可用！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            recDir = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            labelRecDir.Text = recDir;
        }

        private void frm心电仪_Load(object sender, EventArgs e)
        {
            BluetoothRadio bluetoothRadio = BluetoothRadio.PrimaryRadio;
            if (bluetoothRadio == null)
            {
                Console.WriteLine("没有找到本机蓝牙设备!");
            }
            else
            {
                //Console.WriteLine("ClassOfDevice: " + bluetoothRadio.ClassOfDevice);
                //Console.WriteLine("HardwareStatus: " + bluetoothRadio.HardwareStatus);
                //Console.WriteLine("HciRevision: " + bluetoothRadio.HciRevision);
                //Console.WriteLine("HciVersion: " + bluetoothRadio.HciVersion);
                //Console.WriteLine("LmpSubversion: " + bluetoothRadio.LmpSubversion);
                //Console.WriteLine("LmpVersion: " + bluetoothRadio.LmpVersion);
                //Console.WriteLine("LocalAddress: " + bluetoothRadio.LocalAddress);
                //Console.WriteLine("Manufacturer: " + bluetoothRadio.Manufacturer);
                //Console.WriteLine("Mode: " + bluetoothRadio.Mode);
                //Console.WriteLine("Name: " + bluetoothRadio.Name);
                //Console.WriteLine("Remote:" + bluetoothRadio.Remote);
                //Console.WriteLine("SoftwareManufacturer: " + bluetoothRadio.SoftwareManufacturer);
                //Console.WriteLine("StackFactory: " + bluetoothRadio.StackFactory);
            }
            //Console.ReadKey();  
        }

        private void buttonSelectBluetooth_Click(object sender, EventArgs e)
        {
            SelectBluetoothDeviceDialog dialog = new SelectBluetoothDeviceDialog();
            dialog.ShowRemembered = true;//显示已经记住的蓝牙设备  
            dialog.ShowAuthenticated = true;//显示认证过的蓝牙设备  
            dialog.ShowUnknown = true;//显示位置蓝牙设备  
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                sendAddress = dialog.SelectedDevice.DeviceAddress;//获取选择的远程蓝牙地址  
                //labelAddress.Text = "地址:" + sendAddress.ToString() + "    设备名:" + dialog.SelectedDevice.DeviceName;
            }
        }
    }
}
