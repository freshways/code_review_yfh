﻿namespace ATOMEHR_LeaveClient
{
    partial class frm心电仪
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelRecDir = new System.Windows.Forms.TextBox();
            this.buttonSelectBluetooth = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelRecDir
            // 
            this.labelRecDir.Location = new System.Drawing.Point(250, 12);
            this.labelRecDir.Name = "labelRecDir";
            this.labelRecDir.Size = new System.Drawing.Size(418, 21);
            this.labelRecDir.TabIndex = 0;
            // 
            // buttonSelectBluetooth
            // 
            this.buttonSelectBluetooth.Location = new System.Drawing.Point(35, 86);
            this.buttonSelectBluetooth.Name = "buttonSelectBluetooth";
            this.buttonSelectBluetooth.Size = new System.Drawing.Size(101, 44);
            this.buttonSelectBluetooth.TabIndex = 1;
            this.buttonSelectBluetooth.Text = "选择蓝牙";
            this.buttonSelectBluetooth.UseVisualStyleBackColor = true;
            this.buttonSelectBluetooth.Click += new System.EventHandler(this.buttonSelectBluetooth_Click);
            // 
            // frm心电仪
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(909, 459);
            this.Controls.Add(this.buttonSelectBluetooth);
            this.Controls.Add(this.labelRecDir);
            this.Name = "frm心电仪";
            this.Text = "frm心电仪";
            this.Load += new System.EventHandler(this.frm心电仪_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox labelRecDir;
        private System.Windows.Forms.Button buttonSelectBluetooth;
    }
}