using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace ww.wwf.dao
{
	public sealed class SqlHelperParameterCache
	{
		private static Hashtable paramCache = Hashtable.Synchronized(new Hashtable());

		private SqlHelperParameterCache()
		{
		}

		private static System.Data.SqlClient.SqlParameter[] DiscoverSpParameterSet(System.Data.SqlClient.SqlConnection connection, string spName, bool includeReturnValueParameter)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			System.Data.SqlClient.SqlCommand sqlCommand = new System.Data.SqlClient.SqlCommand(spName, connection);
			sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
			connection.Open();
			System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(sqlCommand);
			connection.Close();
			if (!includeReturnValueParameter)
			{
				sqlCommand.Parameters.RemoveAt(0);
			}
			System.Data.SqlClient.SqlParameter[] array = new System.Data.SqlClient.SqlParameter[sqlCommand.Parameters.Count];
			sqlCommand.Parameters.CopyTo(array, 0);
			System.Data.SqlClient.SqlParameter[] array2 = array;
			for (int i = 0; i < array2.Length; i++)
			{
				System.Data.SqlClient.SqlParameter sqlParameter = array2[i];
				sqlParameter.Value = DBNull.Value;
			}
			return array;
		}

		private static System.Data.SqlClient.SqlParameter[] CloneParameters(System.Data.SqlClient.SqlParameter[] originalParameters)
		{
			System.Data.SqlClient.SqlParameter[] array = new System.Data.SqlClient.SqlParameter[originalParameters.Length];
			int i = 0;
			int num = originalParameters.Length;
			while (i < num)
			{
				array[i] = (System.Data.SqlClient.SqlParameter)((ICloneable)originalParameters[i]).Clone();
				i++;
			}
			return array;
		}

		public static void CacheParameterSet(string connectionString, string commandText, params System.Data.SqlClient.SqlParameter[] commandParameters)
		{
			if (connectionString == null || connectionString.Length == 0)
			{
				throw new ArgumentNullException("connectionString");
			}
			if (commandText == null || commandText.Length == 0)
			{
				throw new ArgumentNullException("commandText");
			}
			string key = connectionString + ":" + commandText;
			SqlHelperParameterCache.paramCache[key] = commandParameters;
		}

		public static System.Data.SqlClient.SqlParameter[] GetCachedParameterSet(string connectionString, string commandText)
		{
			if (connectionString == null || connectionString.Length == 0)
			{
				throw new ArgumentNullException("connectionString");
			}
			if (commandText == null || commandText.Length == 0)
			{
				throw new ArgumentNullException("commandText");
			}
			string key = connectionString + ":" + commandText;
			System.Data.SqlClient.SqlParameter[] array = SqlHelperParameterCache.paramCache[key] as System.Data.SqlClient.SqlParameter[];
			System.Data.SqlClient.SqlParameter[] result;
			if (array == null)
			{
				result = null;
			}
			else
			{
				result = SqlHelperParameterCache.CloneParameters(array);
			}
			return result;
		}

		public static System.Data.SqlClient.SqlParameter[] GetSpParameterSet(string connectionString, string spName)
		{
			return SqlHelperParameterCache.GetSpParameterSet(connectionString, spName, false);
		}

		public static System.Data.SqlClient.SqlParameter[] GetSpParameterSet(string connectionString, string spName, bool includeReturnValueParameter)
		{
			if (connectionString == null || connectionString.Length == 0)
			{
				throw new ArgumentNullException("connectionString");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			System.Data.SqlClient.SqlParameter[] spParameterSetInternal;
			using (System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(connectionString))
			{
				spParameterSetInternal = SqlHelperParameterCache.GetSpParameterSetInternal(sqlConnection, spName, includeReturnValueParameter);
			}
			return spParameterSetInternal;
		}

		internal static System.Data.SqlClient.SqlParameter[] GetSpParameterSet(System.Data.SqlClient.SqlConnection connection, string spName)
		{
			return SqlHelperParameterCache.GetSpParameterSet(connection, spName, false);
		}

		internal static System.Data.SqlClient.SqlParameter[] GetSpParameterSet(System.Data.SqlClient.SqlConnection connection, string spName, bool includeReturnValueParameter)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			System.Data.SqlClient.SqlParameter[] spParameterSetInternal;
			using (System.Data.SqlClient.SqlConnection sqlConnection = (System.Data.SqlClient.SqlConnection)((ICloneable)connection).Clone())
			{
				spParameterSetInternal = SqlHelperParameterCache.GetSpParameterSetInternal(sqlConnection, spName, includeReturnValueParameter);
			}
			return spParameterSetInternal;
		}

		private static System.Data.SqlClient.SqlParameter[] GetSpParameterSetInternal(System.Data.SqlClient.SqlConnection connection, string spName, bool includeReturnValueParameter)
		{
			if (connection == null)
			{
				throw new ArgumentNullException("connection");
			}
			if (spName == null || spName.Length == 0)
			{
				throw new ArgumentNullException("spName");
			}
			string key = connection.ConnectionString + ":" + spName + (includeReturnValueParameter ? ":include ReturnValue Parameter" : "");
			System.Data.SqlClient.SqlParameter[] array = SqlHelperParameterCache.paramCache[key] as System.Data.SqlClient.SqlParameter[];
			if (array == null)
			{
				System.Data.SqlClient.SqlParameter[] array2 = SqlHelperParameterCache.DiscoverSpParameterSet(connection, spName, includeReturnValueParameter);
				SqlHelperParameterCache.paramCache[key] = array2;
				array = array2;
			}
			return SqlHelperParameterCache.CloneParameters(array);
		}
	}
}
