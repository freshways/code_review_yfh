using System;
using System.Data;

namespace ww.wwf.dao
{
	public delegate void UpdateDataSetStoredProcUpdate3_EventHandler(string _strConn, System.Data.DataSet dsObject_1, string strTableName_1, string strStoredProcUpdateCommand_1, System.Data.DataSet dsObject_2, string strTableName_2, string strStoredProcUpdateCommand_2, System.Data.DataSet dsObject_3, string strTableName_3, string strStoredProcUpdateCommand_3);
}
