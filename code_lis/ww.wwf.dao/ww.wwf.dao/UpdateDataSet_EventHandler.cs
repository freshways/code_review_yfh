using System;
using System.Data;

namespace ww.wwf.dao
{
	public delegate void UpdateDataSet_EventHandler(string _strConn, System.Data.DataSet dsObject, string strTableName, string strKey, string strStoredProcInsertCommand, string strStoredProcDeleteCommand, string strStoredProcupdateCommand, int intUpdateBehavior);
}
