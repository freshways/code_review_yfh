using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Management;
using System.Text;

namespace ww.wwf.dao
{
	public class DAO
	{
		public int DbExecuteNonQueryByte(string _strConn, string strSql, string fid, string fname, string fdir_id, byte[] fvalue, string fuse)
		{
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.Common.DbCommand sqlStringCommand = database.GetSqlStringCommand(strSql);
			database.AddInParameter(sqlStringCommand, "@fid", System.Data.DbType.String, fid);
			database.AddInParameter(sqlStringCommand, "@fname", System.Data.DbType.String, fname);
			database.AddInParameter(sqlStringCommand, "@fdir_id", System.Data.DbType.String, fdir_id);
			database.AddInParameter(sqlStringCommand, "@fvalue", System.Data.DbType.Binary, fvalue);
			database.AddInParameter(sqlStringCommand, "@fuse", System.Data.DbType.String, fuse);
			return database.ExecuteNonQuery(sqlStringCommand);
		}

		public int DbApplyBcodeAdd(string _strConn, string strStoredProcImg, string strfapply_id, byte[] imgfbcode)
		{
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.Common.DbCommand storedProcCommand = database.GetStoredProcCommand(strStoredProcImg);
			database.AddInParameter(storedProcCommand, "@fapply_id", System.Data.DbType.String, strfapply_id);
			database.AddInParameter(storedProcCommand, "@fbcode", System.Data.DbType.Binary, imgfbcode);
			return database.ExecuteNonQuery(storedProcCommand);
		}

		public int DbInstrImgAdd(string _strConn, string strStoredProcImg, string strfimg_type, byte[] fimg, string strforder_by, string fremark, string strfio_id)
		{
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.Common.DbCommand storedProcCommand = database.GetStoredProcCommand(strStoredProcImg);
			database.AddInParameter(storedProcCommand, "@fimg_id", System.Data.DbType.String, this.DbGuid());
			database.AddInParameter(storedProcCommand, "@fimg_type", System.Data.DbType.String, strfimg_type);
			database.AddInParameter(storedProcCommand, "@fimg", System.Data.DbType.Binary, fimg);
			database.AddInParameter(storedProcCommand, "@forder_by", System.Data.DbType.String, strforder_by);
			database.AddInParameter(storedProcCommand, "@fremark", System.Data.DbType.String, fremark);
			database.AddInParameter(storedProcCommand, "@fio_id", System.Data.DbType.String, strfio_id);
			return database.ExecuteNonQuery(storedProcCommand);
		}

		public int DbReportImgAdd(string _strConn, string strStoredProcImg, string strfimg_type, byte[] fimg, string strforder_by, string fremark, string strfsample_id)
		{
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.Common.DbCommand storedProcCommand = database.GetStoredProcCommand(strStoredProcImg);
			database.AddInParameter(storedProcCommand, "@fimg_id", System.Data.DbType.String, this.DbGuid());
			database.AddInParameter(storedProcCommand, "@fimg_type", System.Data.DbType.String, strfimg_type);
			database.AddInParameter(storedProcCommand, "@fimg", System.Data.DbType.Binary, fimg);
			database.AddInParameter(storedProcCommand, "@forder_by", System.Data.DbType.String, strforder_by);
			database.AddInParameter(storedProcCommand, "@fremark", System.Data.DbType.String, fremark);
			database.AddInParameter(storedProcCommand, "@fsample_id", System.Data.DbType.String, strfsample_id);
			return database.ExecuteNonQuery(storedProcCommand);
		}

		public string DbTransaction(string _strConn, IList sqlList)
		{
			string result = "";
			int count = sqlList.Count;
			if (count > 0)
			{
				Database database = DatabaseFactory.CreateDatabase(_strConn);
				using (System.Data.Common.DbConnection dbConnection = database.CreateConnection())
				{
					dbConnection.Open();
					System.Data.Common.DbTransaction dbTransaction = dbConnection.BeginTransaction();
					try
					{
						for (int i = 0; i < count; i++)
						{
							string query = sqlList[i].ToString();
							System.Data.Common.DbCommand sqlStringCommand = database.GetSqlStringCommand(query);
							database.ExecuteNonQuery(sqlStringCommand, dbTransaction);
						}
						dbTransaction.Commit();
						result = "true";
					}
					catch (Exception ex)
					{
						dbTransaction.Rollback();
						result = "发生异常，事务回滚。\n" + ex.ToString();
					}
					dbConnection.Close();
				}
			}
			else
			{
				result = "未执行事务，因为sql数为0！";
			}
			return result;
		}

		public System.Data.DataSet DbLoadDataSetBySqlString(string _strConn, string strSql, string strTableName)
		{
			System.Data.DataSet dataSet = new System.Data.DataSet();
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.Common.DbCommand sqlStringCommand = database.GetSqlStringCommand(strSql);
			database.LoadDataSet(sqlStringCommand, dataSet, strTableName);
			return dataSet;
		}

		public System.Data.DataSet DbLoadDataSetByStoredProc(string _strConn, string strStoredProcName, string strTableName, string strInParameterName, string strInParameterValue)
		{
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.DataSet dataSet = new System.Data.DataSet();
			System.Data.Common.DbCommand storedProcCommand = database.GetStoredProcCommand(strStoredProcName);
			database.AddInParameter(storedProcCommand, strInParameterName, System.Data.DbType.String, strInParameterValue);
			database.LoadDataSet(storedProcCommand, dataSet, strTableName);
			return dataSet;
		}

		public System.Data.DataSet DbExecuteDataSetBySqlString(string _strConn, string strSql)
		{
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.Common.DbCommand sqlStringCommand = database.GetSqlStringCommand(strSql);
			return database.ExecuteDataSet(sqlStringCommand);
		}

		public System.Data.DataSet DbExecuteDataSetBySqlString(string _strConn, string strSql, System.Data.Common.DbParameter[] paramsList)
		{
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.Common.DbCommand sqlStringCommand = database.GetSqlStringCommand(strSql);
			for (int i = 0; i < paramsList.Length; i++)
			{
				System.Data.Common.DbParameter value = paramsList[i];
				sqlStringCommand.Parameters.Add(value);
			}
			return database.ExecuteDataSet(sqlStringCommand);
		}

		public System.Data.DataSet GetPatientInfoBy住院号(string _strConn, string str住院号)
		{
			string query = "SELECT * FROM 病人信息View where [fhz_zyh]=@zyh ";
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.Common.DbCommand sqlStringCommand = database.GetSqlStringCommand(query);
			System.Data.SqlClient.SqlParameter value = new System.Data.SqlClient.SqlParameter("@zyh", str住院号);
			sqlStringCommand.Parameters.Add(value);
			return database.ExecuteDataSet(sqlStringCommand);
		}

		public System.Data.DataSet GetPatientInfoBy档案号(string _strConn, string str档案号)
		{
			string query = "SELECT * FROM YSDB.YSDB.dbo.vw_lis_PATIENT where [fhz_id]=@dnh ";
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.Common.DbCommand sqlStringCommand = database.GetSqlStringCommand(query);
			System.Data.SqlClient.SqlParameter value = new System.Data.SqlClient.SqlParameter("@dnh", str档案号);
			sqlStringCommand.Parameters.Add(value);
			return database.ExecuteDataSet(sqlStringCommand);
		}

		public System.Data.DataSet GetPatientInfoBy档案号New(string _strConn, string str档案号)
		{
			string query = "SELECT * FROM YSDB.[AtomEHR.YSDB].dbo.vw_lis_PATIENT where [fhz_id]=@dnh ";
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.Common.DbCommand sqlStringCommand = database.GetSqlStringCommand(query);
			System.Data.SqlClient.SqlParameter value = new System.Data.SqlClient.SqlParameter("@dnh", str档案号);
			sqlStringCommand.Parameters.Add(value);
			return database.ExecuteDataSet(sqlStringCommand);
		}

		public System.Data.DataTable DbExecuteDataTableBySqlString(string _strConn, string strSql)
		{
			System.Data.DataTable result = new System.Data.DataTable();
			System.Data.DataSet dataSet = this.DbExecuteDataSetBySqlString(_strConn, strSql);
			if (dataSet != null)
			{
				result = dataSet.Tables[0];
			}
			return result;
		}

		public System.Data.DataSet DbExecuteDataSetByStoredProc(string _strConn, string strStoredProcName)
		{
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.Common.DbCommand storedProcCommand = database.GetStoredProcCommand(strStoredProcName);
			return database.ExecuteDataSet(storedProcCommand);
		}

		public System.Data.DataSet DbExecuteDataSetByStoredProc(string _strConn, string strStoredProcName, string[] sqlParams)
		{
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.Common.DbCommand storedProcCommand = database.GetStoredProcCommand(strStoredProcName, sqlParams);
			return database.ExecuteDataSet(storedProcCommand);
		}

		public System.Data.DataSet DbLoadDataSetPaging(string _strConn, string tblName, string fldName, int PageSize, int PageIndex, int IsReCount, int OrderType, string strWhere, string strLoadTableName)
		{
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.DataSet dataSet = new System.Data.DataSet();
			string storedProcedureName = "Sys_GetRecordByPage";
			System.Data.Common.DbCommand storedProcCommand = database.GetStoredProcCommand(storedProcedureName);
			database.AddInParameter(storedProcCommand, "tblName", System.Data.DbType.String, tblName);
			database.AddInParameter(storedProcCommand, "fldName", System.Data.DbType.String, fldName);
			database.AddInParameter(storedProcCommand, "PageSize", System.Data.DbType.Int32, PageSize);
			database.AddInParameter(storedProcCommand, "PageIndex", System.Data.DbType.Int32, PageIndex);
			database.AddInParameter(storedProcCommand, "IsReCount", System.Data.DbType.Int32, IsReCount);
			database.AddInParameter(storedProcCommand, "OrderType", System.Data.DbType.Int32, OrderType);
			database.AddInParameter(storedProcCommand, "strWhere", System.Data.DbType.AnsiString, strWhere);
			database.LoadDataSet(storedProcCommand, dataSet, strLoadTableName);
			return dataSet;
		}

		public System.Data.DataSet DbExecuteDataSetPaging(string _strConn, string tblName, string fldName, int PageSize, int PageIndex, int IsReCount, int OrderType, string strWhere)
		{
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			string storedProcedureName = "Sys_GetRecordByPage";
			System.Data.Common.DbCommand storedProcCommand = database.GetStoredProcCommand(storedProcedureName);
			database.AddInParameter(storedProcCommand, "tblName", System.Data.DbType.String, tblName);
			database.AddInParameter(storedProcCommand, "fldName", System.Data.DbType.String, fldName);
			database.AddInParameter(storedProcCommand, "PageSize", System.Data.DbType.Int32, PageSize);
			database.AddInParameter(storedProcCommand, "PageIndex", System.Data.DbType.Int32, PageIndex);
			database.AddInParameter(storedProcCommand, "IsReCount", System.Data.DbType.Int32, IsReCount);
			database.AddInParameter(storedProcCommand, "OrderType", System.Data.DbType.Int32, OrderType);
			database.AddInParameter(storedProcCommand, "strWhere", System.Data.DbType.AnsiString, strWhere);
			return database.ExecuteDataSet(storedProcCommand);
		}

		protected static System.Data.DbType DbTypeSet(Type type)
		{
			System.Data.DbType result = System.Data.DbType.String;
			if (type.Equals(typeof(int)) || type.IsEnum)
			{
				result = System.Data.DbType.Int32;
			}
			else if (type.Equals(typeof(short)))
			{
				result = System.Data.DbType.Int16;
			}
			else if (type.Equals(typeof(int)))
			{
				result = System.Data.DbType.Int32;
			}
			else if (type.Equals(typeof(long)))
			{
				result = System.Data.DbType.Int64;
			}
			else if (type.Equals(typeof(object)))
			{
				result = System.Data.DbType.Object;
			}
			else if (type.Equals(typeof(byte)))
			{
				result = System.Data.DbType.Byte;
			}
			else if (type.Equals(typeof(float)))
			{
				result = System.Data.DbType.Single;
			}
			else if (type.Equals(typeof(long)))
			{
				result = System.Data.DbType.Int32;
			}
			else if (type.Equals(typeof(double)) || type.Equals(typeof(double)))
			{
				result = System.Data.DbType.Decimal;
			}
			else if (type.Equals(typeof(DateTime)))
			{
				result = System.Data.DbType.DateTime;
			}
			else if (type.Equals(typeof(bool)))
			{
				result = System.Data.DbType.Boolean;
			}
			else if (type.Equals(typeof(bool)))
			{
				result = System.Data.DbType.Boolean;
			}
			else if (type.Equals(typeof(string)))
			{
				result = System.Data.DbType.String;
			}
			else if (type.Equals(typeof(decimal)))
			{
				result = System.Data.DbType.Decimal;
			}
			else if (type.Equals(typeof(byte[])))
			{
				result = System.Data.DbType.Binary;
			}
			else if (type.Equals(typeof(Guid)))
			{
				result = System.Data.DbType.Guid;
			}
			else if (type.Equals(typeof(decimal)))
			{
				result = System.Data.DbType.Decimal;
			}
			return result;
		}

		public int DbExecuteNonQueryBySqlString(string _strConn, string strSql)
		{
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.Common.DbCommand sqlStringCommand = database.GetSqlStringCommand(strSql);
			return database.ExecuteNonQuery(sqlStringCommand);
		}

		public int DbExecuteNonQueryBySqlString(string _strConn, string strSql, System.Data.Common.DbParameter[] sqlParams)
		{
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.Common.DbCommand sqlStringCommand = database.GetSqlStringCommand(strSql);
			for (int i = 0; i < sqlParams.Length; i++)
			{
				System.Data.Common.DbParameter value = sqlParams[i];
				sqlStringCommand.Parameters.Add(value);
			}
			return database.ExecuteNonQuery(sqlStringCommand);
		}

		public string DbComputeSum(System.Data.DataTable dt, string str列名)
		{
			string result = "";
			object obj = dt.Compute("Sum(" + str列名 + ")", "");
			if (obj != null)
			{
				result = obj.ToString();
			}
			return result;
		}

		public string DbDate()
		{
			return DateTime.Now.ToString("yyyy-MM-dd");
		}

		public string DbDateTime()
		{
			return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
		}

		public void WWFSaveLog(string strCon)
		{
			StreamWriter streamWriter = File.AppendText("Log/" + DateTime.Now.ToString("yyyy-MM-dd") + ".log");
			streamWriter.WriteLine(DateTime.Now.ToString() + " " + strCon);
			streamWriter.Flush();
			streamWriter.Close();
		}

		public string DbDTSelect(System.Data.DataTable dt, string strWhere, string strColumn)
		{
			string result = "";
			System.Data.DataRow[] array = dt.Select(strWhere);
			if (array != null)
			{
				System.Data.DataRow[] array2 = array;
				for (int i = 0; i < array2.Length; i++)
				{
					System.Data.DataRow dataRow = array2[i];
					result = dataRow[strColumn].ToString();
				}
			}
			return result;
		}

		public string DbIndexOfStrRep(string str字符串, string str包含字符, string str新值)
		{
			if (str字符串.IndexOf(str包含字符) > -1)
			{
				str字符串 = str字符串.Replace(str包含字符, str新值).Trim();
			}
			return str字符串;
		}

		public string DbGetNetCardMAC()
		{
			string result;
			try
			{
				string text = "";
				ManagementClass managementClass = new ManagementClass("Win32_NetworkAdapterConfiguration");
				ManagementObjectCollection instances = managementClass.GetInstances();
				using (ManagementObjectCollection.ManagementObjectEnumerator enumerator = instances.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						ManagementObject managementObject = (ManagementObject)enumerator.Current;
						if ((bool)managementObject["IPEnabled"])
						{
							text += managementObject["MACAddress"].ToString();
						}
					}
				}
				result = text;
			}
			catch
			{
				result = "";
			}
			return result;
		}

		public bool DbRecordExistsParam(string _strConn, string uspName, params object[] values)
		{
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			object obj = database.ExecuteScalar(uspName, values);
			int num = 0;
			bool result;
			try
			{
				num = int.Parse(obj.ToString());
			}
			catch
			{
				result = false;
				return result;
			}
			result = (num != 0);
			return result;
		}

		public bool DbRecordExists(string _strConn, string sql)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(sql);
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			object obj = database.ExecuteScalar(System.Data.CommandType.Text, stringBuilder.ToString());
			int num = 0;
			bool result;
			try
			{
				num = int.Parse(obj.ToString());
			}
			catch
			{
				result = false;
				return result;
			}
			result = (num != 0);
			return result;
		}

		public object DbExecuteScalarBySqlString(string _strConn, string strSql)
		{
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.Common.DbCommand sqlStringCommand = database.GetSqlStringCommand(strSql);
			return database.ExecuteScalar(sqlStringCommand);
		}

		public string DbExecuteScalarBySql(string _strConn, string strSql)
		{
			string result = "";
			object obj = this.DbExecuteScalarBySqlString(_strConn, strSql);
			if (obj != null)
			{
				result = obj.ToString();
			}
			return result;
		}

		public object DbExecuteScalarByStoredProc(string _strConn, string strStoredProc)
		{
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.Common.DbCommand storedProcCommand = database.GetStoredProcCommand(strStoredProc);
			return database.ExecuteScalar(storedProcCommand);
		}

		public object DbExecuteScalarByStoredProc(string _strConn, string strStoredProc, System.Data.Common.DbParameter[] procParams)
		{
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.Common.DbCommand storedProcCommand = database.GetStoredProcCommand(strStoredProc);
			for (int i = 0; i < procParams.Length; i++)
			{
				System.Data.Common.DbParameter value = procParams[i];
				storedProcCommand.Parameters.Add(value);
			}
			return database.ExecuteScalar(storedProcCommand);
		}

		public int DbUpdateDataSet(string _strConn, System.Data.DataSet dsObject, string strTableName, string strKey, string strStoredProcInsertCommand, string strStoredProcDeleteCommand, string strStoredProcupdateCommand, int intUpdateBehavior)
		{
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.DataSet dataSet = new System.Data.DataSet();
			System.Data.DataTable dataTable = new System.Data.DataTable();
			dataTable = dsObject.Tables[strTableName];
			int count = dataTable.Columns.Count;
			System.Data.Common.DbCommand storedProcCommand = database.GetStoredProcCommand(strStoredProcInsertCommand);
			for (int i = 0; i < count; i++)
			{
				Type dataType = dataTable.Columns[i].DataType;
				string columnName = dataTable.Columns[i].ColumnName;
				database.AddInParameter(storedProcCommand, columnName, DAO.DbTypeSet(dataType), columnName, System.Data.DataRowVersion.Current);
			}
			System.Data.Common.DbCommand storedProcCommand2 = database.GetStoredProcCommand(strStoredProcDeleteCommand);
			database.AddInParameter(storedProcCommand2, strKey, System.Data.DbType.String, strKey, System.Data.DataRowVersion.Current);
			System.Data.Common.DbCommand storedProcCommand3 = database.GetStoredProcCommand(strStoredProcupdateCommand);
			for (int j = 0; j < count; j++)
			{
				Type dataType = dataTable.Columns[j].DataType;
				string columnName = dataTable.Columns[j].ColumnName;
				database.AddInParameter(storedProcCommand3, columnName, DAO.DbTypeSet(dataType), columnName, System.Data.DataRowVersion.Current);
			}
			int result;
			switch (intUpdateBehavior)
			{
			case 0:
				result = database.UpdateDataSet(dsObject, strTableName, storedProcCommand, storedProcCommand3, storedProcCommand2, UpdateBehavior.Standard);
				break;
			case 1:
				result = database.UpdateDataSet(dsObject, strTableName, storedProcCommand, storedProcCommand3, storedProcCommand2, UpdateBehavior.Continue);
				break;
			case 2:
				result = database.UpdateDataSet(dsObject, strTableName, storedProcCommand, storedProcCommand3, storedProcCommand2, UpdateBehavior.Transactional);
				break;
			default:
				result = database.UpdateDataSet(dsObject, strTableName, storedProcCommand, storedProcCommand3, storedProcCommand2, UpdateBehavior.Standard);
				break;
			}
			return result;
		}

		public int DbUpdateDataSetStoredProcInsert2(string _strConn, System.Data.DataSet dsObject_1, string strTableName_1, string strStoredProcInsertCommand_1, System.Data.DataSet dsObject_2, string strTableName_2, string strStoredProcInsertCommand_2)
		{
			int num = 0;
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.DataSet dataSet = new System.Data.DataSet();
			System.Data.DataTable dataTable = new System.Data.DataTable();
			dataTable = dsObject_1.Tables[strTableName_1];
			int count = dataTable.Columns.Count;
			System.Data.Common.DbCommand storedProcCommand = database.GetStoredProcCommand(strStoredProcInsertCommand_1);
			for (int i = 0; i < count; i++)
			{
				Type dataType = dataTable.Columns[i].DataType;
				string columnName = dataTable.Columns[i].ColumnName;
				database.AddInParameter(storedProcCommand, columnName, DAO.DbTypeSet(dataType), columnName, System.Data.DataRowVersion.Current);
			}
			System.Data.DataSet dataSet2 = new System.Data.DataSet();
			System.Data.DataTable dataTable2 = new System.Data.DataTable();
			dataTable2 = dsObject_2.Tables[strTableName_2];
			int count2 = dataTable2.Columns.Count;
			System.Data.Common.DbCommand storedProcCommand2 = database.GetStoredProcCommand(strStoredProcInsertCommand_2);
			for (int i = 0; i < count2; i++)
			{
				Type dataType2 = dataTable2.Columns[i].DataType;
				string columnName2 = dataTable2.Columns[i].ColumnName;
				database.AddInParameter(storedProcCommand2, columnName2, DAO.DbTypeSet(dataType2), columnName2, System.Data.DataRowVersion.Current);
			}
			System.Data.Common.DbCommand deleteCommand = null;
			System.Data.Common.DbCommand updateCommand = null;
			int result;
			using (System.Data.Common.DbConnection dbConnection = database.CreateConnection())
			{
				dbConnection.Open();
				System.Data.Common.DbTransaction dbTransaction = dbConnection.BeginTransaction();
				try
				{
					int num2 = database.UpdateDataSet(dsObject_1, strTableName_1, storedProcCommand, updateCommand, deleteCommand, dbTransaction);
					int num3 = database.UpdateDataSet(dsObject_2, strTableName_2, storedProcCommand2, updateCommand, deleteCommand, dbTransaction);
					dbTransaction.Commit();
					num = num2 + num3;
				}
				catch (Exception)
				{
					dbTransaction.Rollback();
				}
				dbConnection.Close();
				result = num;
			}
			return result;
		}

		public int DbUpdateDataSetStoredProcInsert3(string _strConn, System.Data.DataSet dsObject_1, string strTableName_1, string strStoredProcInsertCommand_1, System.Data.DataSet dsObject_2, string strTableName_2, string strStoredProcInsertCommand_2, System.Data.DataSet dsObject_3, string strTableName_3, string strStoredProcInsertCommand_3)
		{
			int num = 0;
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.DataSet dataSet = new System.Data.DataSet();
			System.Data.DataTable dataTable = new System.Data.DataTable();
			dataTable = dsObject_1.Tables[strTableName_1];
			int count = dataTable.Columns.Count;
			System.Data.Common.DbCommand storedProcCommand = database.GetStoredProcCommand(strStoredProcInsertCommand_1);
			for (int i = 0; i < count; i++)
			{
				Type dataType = dataTable.Columns[i].DataType;
				string columnName = dataTable.Columns[i].ColumnName;
				database.AddInParameter(storedProcCommand, columnName, DAO.DbTypeSet(dataType), columnName, System.Data.DataRowVersion.Current);
			}
			System.Data.DataSet dataSet2 = new System.Data.DataSet();
			System.Data.DataTable dataTable2 = new System.Data.DataTable();
			dataTable2 = dsObject_2.Tables[strTableName_2];
			int count2 = dataTable2.Columns.Count;
			System.Data.Common.DbCommand storedProcCommand2 = database.GetStoredProcCommand(strStoredProcInsertCommand_2);
			for (int i = 0; i < count2; i++)
			{
				Type dataType2 = dataTable2.Columns[i].DataType;
				string columnName2 = dataTable2.Columns[i].ColumnName;
				database.AddInParameter(storedProcCommand2, columnName2, DAO.DbTypeSet(dataType2), columnName2, System.Data.DataRowVersion.Current);
			}
			System.Data.DataSet dataSet3 = new System.Data.DataSet();
			System.Data.DataTable dataTable3 = new System.Data.DataTable();
			dataTable3 = dsObject_3.Tables[strTableName_3];
			int count3 = dataTable3.Columns.Count;
			System.Data.Common.DbCommand storedProcCommand3 = database.GetStoredProcCommand(strStoredProcInsertCommand_3);
			for (int i = 0; i < count3; i++)
			{
				Type dataType3 = dataTable3.Columns[i].DataType;
				string columnName3 = dataTable3.Columns[i].ColumnName;
				database.AddInParameter(storedProcCommand3, columnName3, DAO.DbTypeSet(dataType3), columnName3, System.Data.DataRowVersion.Current);
			}
			System.Data.Common.DbCommand deleteCommand = null;
			System.Data.Common.DbCommand updateCommand = null;
			int result;
			using (System.Data.Common.DbConnection dbConnection = database.CreateConnection())
			{
				dbConnection.Open();
				System.Data.Common.DbTransaction dbTransaction = dbConnection.BeginTransaction();
				try
				{
					int num2 = database.UpdateDataSet(dsObject_1, strTableName_1, storedProcCommand, updateCommand, deleteCommand, dbTransaction);
					int num3 = database.UpdateDataSet(dsObject_2, strTableName_2, storedProcCommand2, updateCommand, deleteCommand, dbTransaction);
					int num4 = database.UpdateDataSet(dsObject_3, strTableName_3, storedProcCommand3, updateCommand, deleteCommand, dbTransaction);
					dbTransaction.Commit();
					num = num2 + num3 + num4;
				}
				catch (Exception)
				{
					dbTransaction.Rollback();
				}
				dbConnection.Close();
				result = num;
			}
			return result;
		}

		public int DbUpdateDataSetStoredProcUpdate2(string _strConn, System.Data.DataSet dsObject_1, string strTableName_1, string strStoredProcUpdateCommand_1, System.Data.DataSet dsObject_2, string strTableName_2, string strStoredProcUpdateCommand_2)
		{
			int num = 0;
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.DataSet dataSet = new System.Data.DataSet();
			System.Data.DataTable dataTable = new System.Data.DataTable();
			dataTable = dsObject_1.Tables[strTableName_1];
			int count = dataTable.Columns.Count;
			System.Data.Common.DbCommand storedProcCommand = database.GetStoredProcCommand(strStoredProcUpdateCommand_1);
			for (int i = 0; i < count; i++)
			{
				Type dataType = dataTable.Columns[i].DataType;
				string columnName = dataTable.Columns[i].ColumnName;
				database.AddInParameter(storedProcCommand, columnName, DAO.DbTypeSet(dataType), columnName, System.Data.DataRowVersion.Current);
			}
			System.Data.DataSet dataSet2 = new System.Data.DataSet();
			System.Data.DataTable dataTable2 = new System.Data.DataTable();
			dataTable2 = dsObject_2.Tables[strTableName_2];
			int count2 = dataTable2.Columns.Count;
			System.Data.Common.DbCommand storedProcCommand2 = database.GetStoredProcCommand(strStoredProcUpdateCommand_2);
			for (int i = 0; i < count2; i++)
			{
				Type dataType2 = dataTable2.Columns[i].DataType;
				string columnName2 = dataTable2.Columns[i].ColumnName;
				database.AddInParameter(storedProcCommand2, columnName2, DAO.DbTypeSet(dataType2), columnName2, System.Data.DataRowVersion.Current);
			}
			System.Data.Common.DbCommand deleteCommand = null;
			System.Data.Common.DbCommand insertCommand = null;
			int result;
			using (System.Data.Common.DbConnection dbConnection = database.CreateConnection())
			{
				dbConnection.Open();
				System.Data.Common.DbTransaction dbTransaction = dbConnection.BeginTransaction();
				try
				{
					int num2 = database.UpdateDataSet(dsObject_1, strTableName_1, insertCommand, storedProcCommand, deleteCommand, dbTransaction);
					int num3 = database.UpdateDataSet(dsObject_2, strTableName_2, insertCommand, storedProcCommand2, deleteCommand, dbTransaction);
					dbTransaction.Commit();
					num = num2 + num3;
				}
				catch (Exception)
				{
					dbTransaction.Rollback();
				}
				dbConnection.Close();
				result = num;
			}
			return result;
		}

		public int DbUpdateDataSetStoredProcUpdate3(string _strConn, System.Data.DataSet dsObject_1, string strTableName_1, string strStoredProcUpdateCommand_1, System.Data.DataSet dsObject_2, string strTableName_2, string strStoredProcUpdateCommand_2, System.Data.DataSet dsObject_3, string strTableName_3, string strStoredProcUpdateCommand_3)
		{
			int num = 0;
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.DataSet dataSet = new System.Data.DataSet();
			System.Data.DataTable dataTable = new System.Data.DataTable();
			dataTable = dsObject_1.Tables[strTableName_1];
			int count = dataTable.Columns.Count;
			System.Data.Common.DbCommand storedProcCommand = database.GetStoredProcCommand(strStoredProcUpdateCommand_1);
			for (int i = 0; i < count; i++)
			{
				Type dataType = dataTable.Columns[i].DataType;
				string columnName = dataTable.Columns[i].ColumnName;
				database.AddInParameter(storedProcCommand, columnName, DAO.DbTypeSet(dataType), columnName, System.Data.DataRowVersion.Current);
			}
			System.Data.DataSet dataSet2 = new System.Data.DataSet();
			System.Data.DataTable dataTable2 = new System.Data.DataTable();
			dataTable2 = dsObject_2.Tables[strTableName_2];
			int count2 = dataTable2.Columns.Count;
			System.Data.Common.DbCommand storedProcCommand2 = database.GetStoredProcCommand(strStoredProcUpdateCommand_2);
			for (int i = 0; i < count2; i++)
			{
				Type dataType2 = dataTable2.Columns[i].DataType;
				string columnName2 = dataTable2.Columns[i].ColumnName;
				database.AddInParameter(storedProcCommand2, columnName2, DAO.DbTypeSet(dataType2), columnName2, System.Data.DataRowVersion.Current);
			}
			System.Data.DataSet dataSet3 = new System.Data.DataSet();
			System.Data.DataTable dataTable3 = new System.Data.DataTable();
			dataTable3 = dsObject_3.Tables[strTableName_3];
			int count3 = dataTable3.Columns.Count;
			System.Data.Common.DbCommand storedProcCommand3 = database.GetStoredProcCommand(strStoredProcUpdateCommand_3);
			for (int i = 0; i < count3; i++)
			{
				Type dataType3 = dataTable3.Columns[i].DataType;
				string columnName3 = dataTable3.Columns[i].ColumnName;
				database.AddInParameter(storedProcCommand3, columnName3, DAO.DbTypeSet(dataType3), columnName3, System.Data.DataRowVersion.Current);
			}
			System.Data.Common.DbCommand deleteCommand = null;
			System.Data.Common.DbCommand insertCommand = null;
			int result;
			using (System.Data.Common.DbConnection dbConnection = database.CreateConnection())
			{
				dbConnection.Open();
				System.Data.Common.DbTransaction dbTransaction = dbConnection.BeginTransaction();
				try
				{
					int num2 = database.UpdateDataSet(dsObject_1, strTableName_1, insertCommand, storedProcCommand, deleteCommand, dbTransaction);
					int num3 = database.UpdateDataSet(dsObject_2, strTableName_2, insertCommand, storedProcCommand2, deleteCommand, dbTransaction);
					int num4 = database.UpdateDataSet(dsObject_3, strTableName_3, insertCommand, storedProcCommand3, deleteCommand, dbTransaction);
					dbTransaction.Commit();
					num = num2 + num3 + num4;
				}
				catch (Exception)
				{
					dbTransaction.Rollback();
				}
				dbConnection.Close();
				result = num;
			}
			return result;
		}

		public bool DbExecuteNonQueryTransaction2(string _strConn, string strSql1, string strSql2)
		{
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			bool flag = false;
			System.Data.Common.DbCommand sqlStringCommand = database.GetSqlStringCommand(strSql1);
			System.Data.Common.DbCommand sqlStringCommand2 = database.GetSqlStringCommand(strSql2);
			bool result;
			using (System.Data.Common.DbConnection dbConnection = database.CreateConnection())
			{
				dbConnection.Open();
				System.Data.Common.DbTransaction dbTransaction = dbConnection.BeginTransaction();
				try
				{
					database.ExecuteNonQuery(sqlStringCommand, dbTransaction);
					database.ExecuteNonQuery(sqlStringCommand2, dbTransaction);
					dbTransaction.Commit();
					flag = true;
				}
				catch (Exception)
				{
					dbTransaction.Rollback();
				}
				dbConnection.Close();
				result = flag;
			}
			return result;
		}

		public bool DbExecuteNonQueryTransaction3(string _strConn, string strSql1, string strSql2, string strSql3)
		{
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			bool flag = false;
			System.Data.Common.DbCommand sqlStringCommand = database.GetSqlStringCommand(strSql1);
			System.Data.Common.DbCommand sqlStringCommand2 = database.GetSqlStringCommand(strSql2);
			System.Data.Common.DbCommand sqlStringCommand3 = database.GetSqlStringCommand(strSql3);
			bool result;
			using (System.Data.Common.DbConnection dbConnection = database.CreateConnection())
			{
				dbConnection.Open();
				System.Data.Common.DbTransaction dbTransaction = dbConnection.BeginTransaction();
				try
				{
					database.ExecuteNonQuery(sqlStringCommand, dbTransaction);
					database.ExecuteNonQuery(sqlStringCommand2, dbTransaction);
					database.ExecuteNonQuery(sqlStringCommand3, dbTransaction);
					dbTransaction.Commit();
					flag = true;
				}
				catch (Exception)
				{
					dbTransaction.Rollback();
				}
				dbConnection.Close();
				result = flag;
			}
			return result;
		}

		public bool DbExecuteNonQueryTransaction4(string _strConn, string strSql1, string strSql2, string strSql3, string strSql4)
		{
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			bool flag = false;
			System.Data.Common.DbCommand sqlStringCommand = database.GetSqlStringCommand(strSql1);
			System.Data.Common.DbCommand sqlStringCommand2 = database.GetSqlStringCommand(strSql2);
			System.Data.Common.DbCommand sqlStringCommand3 = database.GetSqlStringCommand(strSql3);
			System.Data.Common.DbCommand sqlStringCommand4 = database.GetSqlStringCommand(strSql4);
			bool result;
			using (System.Data.Common.DbConnection dbConnection = database.CreateConnection())
			{
				dbConnection.Open();
				System.Data.Common.DbTransaction dbTransaction = dbConnection.BeginTransaction();
				try
				{
					database.ExecuteNonQuery(sqlStringCommand, dbTransaction);
					database.ExecuteNonQuery(sqlStringCommand2, dbTransaction);
					database.ExecuteNonQuery(sqlStringCommand3, dbTransaction);
					database.ExecuteNonQuery(sqlStringCommand4, dbTransaction);
					dbTransaction.Commit();
					flag = true;
				}
				catch (Exception)
				{
					dbTransaction.Rollback();
				}
				dbConnection.Close();
				result = flag;
			}
			return result;
		}

		public System.Data.IDataReader DbExecuteReaderBySqlString(string _strConn, string strSql)
		{
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.IDataReader result = null;
			System.Data.Common.DbCommand sqlStringCommand = database.GetSqlStringCommand(strSql);
			result = database.ExecuteReader(sqlStringCommand);
			using (System.Data.IDataReader dataReader = database.ExecuteReader(sqlStringCommand))
			{
				result = dataReader;
			}
			return result;
		}

		public System.Data.IDataReader DbExecuteReaderByStoredProc(string _strConn, string strStoredProc)
		{
			Database database = DatabaseFactory.CreateDatabase(_strConn);
			System.Data.IDataReader result = null;
			System.Data.Common.DbCommand storedProcCommand = database.GetStoredProcCommand(strStoredProc);
			result = database.ExecuteReader(storedProcCommand);
			using (System.Data.IDataReader dataReader = database.ExecuteReader(storedProcCommand))
			{
				result = dataReader;
			}
			return result;
		}

		public string DbGuid()
		{
			string text = Guid.NewGuid().ToString();
			return text.Replace("-", "");
		}
	}
}
