using System;
using System.Collections;
using System.Data;
using System.Text;
using System.Windows.Forms;
using ww.wwf.com;

namespace ww.wwf.dao
{
	public class DAOServer : MarshalByRefObject, IDAOServer
	{
		private string _HostName;

		private string _HostIpAddress;

		private string _AccessPasswordClient;

		private DAO dao = new DAO();

		private string strAccessPasswordServer = "F1-49-55-AA-44-F1-6D-12-0A-33-74-91-FF-0P-83-E7-tao";

		public static System.Data.DataTable ClientDT = new System.Data.DataTable();

		public static int ClientCount = 100;

		public static System.Data.DataTable ReportReturnDT = new System.Data.DataTable();

		public static IList ReportIlist = new ArrayList();

		public static event LoadDataSet_EventHandler LoadDataSet_SendedEvent;

		public static event LoadDataSetByStoredProc_EventHandler LoadDataSetByStoredProc_SendedEvent;

		public static event ExecuteDataSet_EventHandler ExecuteDataSet_SendedEvent;

		public static event ExecuteDataSetByStoredProc_EventHandler ExecuteDataSetByStoredProc_SendedEvent;

		public static event ExecuteDataSetPaging_EventHandler ExecuteDataSetPaging_SendedEvent;

		public static event ExecuteNonQuery_EventHandler ExecuteNonQuery_SendedEvent;

		public static event RecordExists_EventHandler RecordExists_SendedEvent;

		public static event ExecuteScalar_EventHandler ExecuteScalar_SendedEvent;

		public static event ExecuteScalarByStoredProc_EventHandler ExecuteScalarByStoredProc_SendedEvent;

		public static event UpdateDataSet_EventHandler UpdateDataSet_SendedEvent;

		public static event UpdateDataSetStoredProcInsert2_EventHandler UpdateDataSetStoredProcInsert2_SendedEvent;

		public static event UpdateDataSetStoredProcInsert3_EventHandler UpdateDataSetStoredProcInsert3_SendedEvent;

		public static event UpdateDataSetStoredProcUpdate2_EventHandler UpdateDataSetStoredProcUpdate2_SendedEvent;

		public static event UpdateDataSetStoredProcUpdate3_EventHandler UpdateDataSetStoredProcUpdate3_SendedEvent;

		public static event ExecuteNonQueryTransaction2_EventHandler ExecuteNonQueryTransaction2_SendedEvent;

		public static event ExecuteNonQueryTransaction3_EventHandler ExecuteNonQueryTransaction3_SendedEvent;

		public static event ExecuteNonQueryTransaction4_EventHandler ExecuteNonQueryTransaction4_SendedEvent;

		public static event ExecuteReader_EventHandler ExecuteReader_SendedEvent;

		public static event ExecuteReaderByStoredProc_EventHandler ExecuteReaderByStoredProc_SendedEvent;

		public static event LoadDataSetPaging_EventHandler LoadDataSetPaging_SendedEvent;

		public static event ExecuteNonQueryTransactionSqlList_EventHandler ExecuteNonQueryTransactionSqlList_SendedEvent;

		public static event DbReportImgAdd_EventHandler DbReportImgAdd_SendedEvent;

		public static event DbInstrImgAdd_EventHandler DbInstrImgAdd_SendedEvent;

		public static event DBReport_EventHandler DBReport_SendedEvent;

		public string strHostName
		{
			get
			{
				return this._HostName;
			}
			set
			{
				this._HostName = value;
			}
		}

		public string strHostIpAddress
		{
			get
			{
				return this._HostIpAddress;
			}
			set
			{
				this._HostIpAddress = value;
			}
		}

		public string strAccessPasswordClient
		{
			get
			{
				return this._AccessPasswordClient;
			}
			set
			{
				this._AccessPasswordClient = value;
			}
		}

		public static void DbGetClientDT()
		{
			DAO dAO = new DAO();
			HardInfo hardInfo = new HardInfo();
			string str = hardInfo.GetNetCardMAC() + hardInfo.GetCpuID();
			string text = Security.StrToEncrypt("SHA1", str);
			string str2 = text.Replace("-", "");
			if (!dAO.DbRecordExists("WW.DBConn", "SELECT count(1) FROM WWF_CLIENT where fid='" + str2 + "'"))
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append("insert into WWF_CLIENT(");
				stringBuilder.Append("fguid,fid,fpc_name,fip,fuse_flag,ftime,fname,fdept,fremark");
				stringBuilder.Append(")");
				stringBuilder.Append(" values (");
				stringBuilder.Append("'" + Public.BllGuid() + "',");
				stringBuilder.Append("'" + str2 + "',");
				stringBuilder.Append("'" + hardInfo.GetHostName() + "',");
				stringBuilder.Append("'" + hardInfo.GetHostIpAddress() + "',");
				stringBuilder.Append("1,");
				stringBuilder.Append("'" + DateTime.Now.ToString() + "',");
				stringBuilder.Append("' ',");
				stringBuilder.Append("' ',");
				stringBuilder.Append("'wwfserver'");
				stringBuilder.Append(")");
				dAO.DbExecuteNonQueryBySqlString("WW.DBConn", stringBuilder.ToString());
			}
			DAOServer.ClientDT = dAO.DbExecuteDataSetBySqlString("WW.DBConn", "SELECT TOP " + DAOServer.ClientCount + " * FROM WWF_CLIENT WHERE (fuse_flag = 1) ORDER BY ftime").Tables[0];
		}

		public bool DbClientUser(string strClientID)
		{
			bool result = false;
			try
			{
				System.Data.DataRow[] array = DAOServer.ClientDT.Select("fid='" + strClientID + "'");
				if (array.Length > 0)
				{
					result = true;
				}
				else
				{
					this.WWFSaveLog("未注册用户", "用户： '" + strClientID + "' 为未注册用户！不能访问本服务。");
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(strClientID, ex.ToString());
			}
			return result;
		}

		private bool DbAccessPassword()
		{
			bool result = false;
			if (this._AccessPasswordClient == this.strAccessPasswordServer)
			{
				result = true;
			}
			else
			{
				this.WWFSaveLog("*****非法用户*****", string.Concat(new string[]
				{
					"[主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"] 请把此消息通知软件开发者(tao:tel:11111111111,email:taoyinzhou@gmail.com,QQ:249116367)"
				}));
			}
			return result;
		}

		public System.Data.DataSet DbLoadDataSetBySqlString(string _strConn, string strSql, string strTableName)
		{
			System.Data.DataSet result = new System.Data.DataSet();
			try
			{
				if (DAOServer.LoadDataSet_SendedEvent != null)
				{
					if (this.DbAccessPassword())
					{
						DAOServer.LoadDataSet_SendedEvent(_strConn, strSql, strTableName);
						result = this.dao.DbLoadDataSetBySqlString(_strConn, strSql, strTableName);
					}
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(string.Concat(new string[]
				{
					"装载可读写数据集 根据sql！[主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"]"
				}), ex.ToString());
			}
			return result;
		}

		public System.Data.DataSet DbLoadDataSetByStoredProc(string _strConn, string strStoredProcName, string strTableName, string strInParameterName, string strInParameterValue)
		{
			System.Data.DataSet result = new System.Data.DataSet();
			try
			{
				if (DAOServer.LoadDataSetByStoredProc_SendedEvent != null)
				{
					if (this.DbAccessPassword())
					{
						DAOServer.LoadDataSetByStoredProc_SendedEvent(_strConn, strStoredProcName, strTableName, strInParameterName, strInParameterValue);
						result = this.dao.DbLoadDataSetByStoredProc(_strConn, strStoredProcName, strTableName, strInParameterName, strInParameterValue);
					}
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(string.Concat(new string[]
				{
					"装载可读写数据集 根据存过！[主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"]"
				}), ex.ToString());
			}
			return result;
		}

		public System.Data.DataSet DbExecuteDataSetBySqlString(string _strConn, string strSql)
		{
			System.Data.DataSet result = null;
			try
			{
				if (DAOServer.ExecuteDataSet_SendedEvent != null)
				{
					if (this.DbAccessPassword())
					{
						DAOServer.ExecuteDataSet_SendedEvent(_strConn, strSql);
						result = this.dao.DbExecuteDataSetBySqlString(_strConn, strSql);
					}
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(string.Concat(new string[]
				{
					"装载只读数据集 根据sql！[主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"]"
				}), ex.ToString());
			}
			return result;
		}

		public System.Data.DataSet DbExecuteDataSetByStoredProc(string _strConn, string strStoredProcName)
		{
			System.Data.DataSet result = null;
			try
			{
				if (DAOServer.ExecuteDataSetByStoredProc_SendedEvent != null)
				{
					if (this.DbAccessPassword())
					{
						DAOServer.ExecuteDataSetByStoredProc_SendedEvent(_strConn, strStoredProcName);
						result = this.dao.DbExecuteDataSetByStoredProc(_strConn, strStoredProcName);
					}
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(string.Concat(new string[]
				{
					"装载只读数据集 根据存过！[主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"]"
				}), ex.ToString());
			}
			return result;
		}

		public System.Data.DataSet DbExecuteDataSetPaging(string _strConn, string tblName, string fldName, int PageSize, int PageIndex, int IsReCount, int OrderType, string strWhere)
		{
			System.Data.DataSet result = null;
			try
			{
				if (DAOServer.ExecuteDataSetPaging_SendedEvent != null)
				{
					if (this.DbAccessPassword())
					{
						DAOServer.ExecuteDataSetPaging_SendedEvent(_strConn, tblName, fldName, PageSize, PageIndex, IsReCount, OrderType, strWhere);
						result = this.dao.DbExecuteDataSetPaging(_strConn, tblName, fldName, PageSize, PageIndex, IsReCount, OrderType, strWhere);
					}
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(string.Concat(new string[]
				{
					"装载只读数据集 分页！[主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"]"
				}), ex.ToString());
			}
			return result;
		}

		public int DbExecuteNonQueryBySqlString(string _strConn, string strSql)
		{
			int result = 0;
			try
			{
				if (DAOServer.ExecuteNonQuery_SendedEvent != null)
				{
					if (this.DbAccessPassword())
					{
						DAOServer.ExecuteNonQuery_SendedEvent(_strConn, strSql);
						result = this.dao.DbExecuteNonQueryBySqlString(_strConn, strSql);
					}
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(string.Concat(new string[]
				{
					"执行sql语句！[主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"]"
				}), ex.ToString());
			}
			return result;
		}

		public bool DbRecordExists(string _strConn, string sql)
		{
			bool result = false;
			try
			{
				if (DAOServer.RecordExists_SendedEvent != null)
				{
					if (this.DbAccessPassword())
					{
						DAOServer.RecordExists_SendedEvent(_strConn, sql);
						result = this.dao.DbRecordExists(_strConn, sql);
					}
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(string.Concat(new string[]
				{
					"判断记录存在否！[主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"]"
				}), ex.ToString());
			}
			return result;
		}

		public object DbExecuteScalarBySqlString(string _strConn, string strSql)
		{
			object result = null;
			try
			{
				if (DAOServer.ExecuteScalar_SendedEvent != null)
				{
					if (this.DbAccessPassword())
					{
						DAOServer.ExecuteScalar_SendedEvent(_strConn, strSql);
						result = this.dao.DbExecuteScalarBySqlString(_strConn, strSql);
					}
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(string.Concat(new string[]
				{
					"执行sql语句返回对象！[主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"]"
				}), ex.ToString());
			}
			return result;
		}

		public object DbExecuteScalarByStoredProc(string _strConn, string strStoredProc)
		{
			object result = null;
			try
			{
				if (DAOServer.ExecuteScalarByStoredProc_SendedEvent != null)
				{
					if (this.DbAccessPassword())
					{
						DAOServer.ExecuteScalarByStoredProc_SendedEvent(_strConn, strStoredProc);
						result = this.dao.DbExecuteScalarByStoredProc(_strConn, strStoredProc);
					}
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(string.Concat(new string[]
				{
					"执行存过返回对象！[主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"]"
				}), ex.ToString());
			}
			return result;
		}

		public int DbUpdateDataSet(string _strConn, System.Data.DataSet dsObject, string strTableName, string strKey, string strStoredProcInsertCommand, string strStoredProcDeleteCommand, string strStoredProcupdateCommand, int intUpdateBehavior)
		{
			int result = 0;
			try
			{
				if (DAOServer.UpdateDataSet_SendedEvent != null)
				{
					if (this.DbAccessPassword())
					{
						DAOServer.UpdateDataSet_SendedEvent(_strConn, dsObject, strTableName, strKey, strStoredProcInsertCommand, strStoredProcDeleteCommand, strStoredProcupdateCommand, intUpdateBehavior);
						result = this.dao.DbUpdateDataSet(_strConn, dsObject, strTableName, strKey, strStoredProcInsertCommand, strStoredProcDeleteCommand, strStoredProcupdateCommand, intUpdateBehavior);
					}
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(string.Concat(new string[]
				{
					"批量增、删、改数据集！[主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"]"
				}), ex.ToString());
			}
			return result;
		}

		public int DbUpdateDataSetStoredProcInsert2(string _strConn, System.Data.DataSet dsObject_1, string strTableName_1, string strStoredProcInsertCommand_1, System.Data.DataSet dsObject_2, string strTableName_2, string strStoredProcInsertCommand_2)
		{
			int result = 0;
			try
			{
				if (DAOServer.UpdateDataSetStoredProcInsert2_SendedEvent != null)
				{
					if (this.DbAccessPassword())
					{
						DAOServer.UpdateDataSetStoredProcInsert2_SendedEvent(_strConn, dsObject_1, strTableName_1, strStoredProcInsertCommand_1, dsObject_2, strTableName_2, strStoredProcInsertCommand_2);
						result = this.dao.DbUpdateDataSetStoredProcInsert2(_strConn, dsObject_1, strTableName_1, strStoredProcInsertCommand_1, dsObject_2, strTableName_2, strStoredProcInsertCommand_2);
					}
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(string.Concat(new string[]
				{
					"批量增加数据集 事务处理2个数据集！[主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"]"
				}), ex.ToString());
			}
			return result;
		}

		public int DbUpdateDataSetStoredProcInsert3(string _strConn, System.Data.DataSet dsObject_1, string strTableName_1, string strStoredProcInsertCommand_1, System.Data.DataSet dsObject_2, string strTableName_2, string strStoredProcInsertCommand_2, System.Data.DataSet dsObject_3, string strTableName_3, string strStoredProcInsertCommand_3)
		{
			int result = 0;
			try
			{
				if (DAOServer.UpdateDataSetStoredProcInsert3_SendedEvent != null)
				{
					if (this.DbAccessPassword())
					{
						DAOServer.UpdateDataSetStoredProcInsert3_SendedEvent(_strConn, dsObject_1, strTableName_1, strStoredProcInsertCommand_1, dsObject_2, strTableName_2, strStoredProcInsertCommand_2, dsObject_3, strTableName_3, strStoredProcInsertCommand_3);
						result = this.dao.DbUpdateDataSetStoredProcInsert3(_strConn, dsObject_1, strTableName_1, strStoredProcInsertCommand_1, dsObject_2, strTableName_2, strStoredProcInsertCommand_2, dsObject_3, strTableName_3, strStoredProcInsertCommand_3);
					}
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(string.Concat(new string[]
				{
					" 批量增加数据集 事务处理3个数据集！[主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"]"
				}), ex.ToString());
			}
			return result;
		}

		public int DbUpdateDataSetStoredProcUpdate2(string _strConn, System.Data.DataSet dsObject_1, string strTableName_1, string strStoredProcUpdateCommand_1, System.Data.DataSet dsObject_2, string strTableName_2, string strStoredProcUpdateCommand_2)
		{
			int result = 0;
			try
			{
				if (DAOServer.UpdateDataSetStoredProcUpdate2_SendedEvent != null)
				{
					if (this.DbAccessPassword())
					{
						DAOServer.UpdateDataSetStoredProcUpdate2_SendedEvent(_strConn, dsObject_1, strTableName_1, strStoredProcUpdateCommand_1, dsObject_2, strTableName_2, strStoredProcUpdateCommand_2);
						result = this.dao.DbUpdateDataSetStoredProcUpdate2(_strConn, dsObject_1, strTableName_1, strStoredProcUpdateCommand_1, dsObject_2, strTableName_2, strStoredProcUpdateCommand_2);
					}
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(string.Concat(new string[]
				{
					" 批量修改数据集 事务处理2个数据集！[主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"]"
				}), ex.ToString());
			}
			return result;
		}

		public int DbUpdateDataSetStoredProcUpdate3(string _strConn, System.Data.DataSet dsObject_1, string strTableName_1, string strStoredProcUpdateCommand_1, System.Data.DataSet dsObject_2, string strTableName_2, string strStoredProcUpdateCommand_2, System.Data.DataSet dsObject_3, string strTableName_3, string strStoredProcUpdateCommand_3)
		{
			int result = 0;
			try
			{
				if (DAOServer.UpdateDataSetStoredProcUpdate3_SendedEvent != null)
				{
					if (this.DbAccessPassword())
					{
						DAOServer.UpdateDataSetStoredProcUpdate3_SendedEvent(_strConn, dsObject_1, strTableName_1, strStoredProcUpdateCommand_1, dsObject_2, strTableName_2, strStoredProcUpdateCommand_2, dsObject_3, strTableName_3, strStoredProcUpdateCommand_3);
						result = this.dao.DbUpdateDataSetStoredProcUpdate3(_strConn, dsObject_1, strTableName_1, strStoredProcUpdateCommand_1, dsObject_2, strTableName_2, strStoredProcUpdateCommand_2, dsObject_3, strTableName_3, strStoredProcUpdateCommand_3);
					}
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(string.Concat(new string[]
				{
					"批量修改数据集 事务处理3个数据集！[主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"]"
				}), ex.ToString());
			}
			return result;
		}

		public bool DbExecuteNonQueryTransaction2(string _strConn, string strSql1, string strSql2)
		{
			bool result = false;
			try
			{
				if (DAOServer.ExecuteNonQueryTransaction2_SendedEvent != null)
				{
					if (this.DbAccessPassword())
					{
						DAOServer.ExecuteNonQueryTransaction2_SendedEvent(_strConn, strSql1, strSql2);
						result = this.dao.DbExecuteNonQueryTransaction2(_strConn, strSql1, strSql2);
					}
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(string.Concat(new string[]
				{
					"事务执行sql语句 2个sql语句！[主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"]"
				}), ex.ToString());
			}
			return result;
		}

		public bool DbExecuteNonQueryTransaction3(string _strConn, string strSql1, string strSql2, string strSql3)
		{
			bool result = false;
			try
			{
				if (DAOServer.ExecuteNonQueryTransaction3_SendedEvent != null)
				{
					if (this.DbAccessPassword())
					{
						DAOServer.ExecuteNonQueryTransaction3_SendedEvent(_strConn, strSql1, strSql2, strSql3);
						result = this.dao.DbExecuteNonQueryTransaction3(_strConn, strSql1, strSql2, strSql3);
					}
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(string.Concat(new string[]
				{
					"事务执行sql语句 3个sql语句！[主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"]"
				}), ex.ToString());
			}
			return result;
		}

		public bool DbExecuteNonQueryTransaction4(string _strConn, string strSql1, string strSql2, string strSql3, string strSql4)
		{
			bool result = false;
			try
			{
				if (DAOServer.ExecuteNonQueryTransaction4_SendedEvent != null)
				{
					if (this.DbAccessPassword())
					{
						DAOServer.ExecuteNonQueryTransaction4_SendedEvent(_strConn, strSql1, strSql2, strSql3, strSql4);
						result = this.dao.DbExecuteNonQueryTransaction4(_strConn, strSql1, strSql2, strSql3, strSql4);
					}
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(string.Concat(new string[]
				{
					"事务执行sql语句 4个sql语句！[主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"]"
				}), ex.ToString());
			}
			return result;
		}

		public string DbTransaction(string _strConn, IList sqlList)
		{
			string text = "";
			if (sqlList.Count > 0)
			{
				try
				{
					if (DAOServer.ExecuteNonQueryTransactionSqlList_SendedEvent != null)
					{
						if (this.DbAccessPassword())
						{
							DAOServer.ExecuteNonQueryTransactionSqlList_SendedEvent(_strConn, sqlList);
							text = this.dao.DbTransaction(_strConn, sqlList);
							if (!(text == "true"))
							{
								this.WWFSaveLog(string.Concat(new string[]
								{
									"ExecuteNonQueryTransactionSqlList执行事务处理SQL列表！[主机名：",
									this._HostName,
									" IP地址：",
									this._HostIpAddress,
									"]"
								}), text);
							}
						}
					}
				}
				catch (Exception ex)
				{
					this.WWFSaveLog(string.Concat(new string[]
					{
						"ExecuteNonQueryTransactionSqlList执行事务处理SQL列表！[主机名：",
						this._HostName,
						" IP地址：",
						this._HostIpAddress,
						"]"
					}), text + ex.ToString());
				}
			}
			else
			{
				text = "sql数为0";
			}
			return text;
		}

		public System.Data.IDataReader DbExecuteReaderBySqlString(string _strConn, string strSql)
		{
			System.Data.IDataReader result = null;
			try
			{
				if (DAOServer.ExecuteReader_SendedEvent != null)
				{
					if (this.DbAccessPassword())
					{
						DAOServer.ExecuteReader_SendedEvent(_strConn, strSql);
						result = this.dao.DbExecuteReaderBySqlString(_strConn, strSql);
					}
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(string.Concat(new string[]
				{
					"IDataReader执行Sql语句 只读！[主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"]"
				}), ex.ToString());
			}
			return result;
		}

		public System.Data.IDataReader DbExecuteReaderByStoredProc(string _strConn, string strStoredProc)
		{
			System.Data.IDataReader result = null;
			try
			{
				if (DAOServer.ExecuteReaderByStoredProc_SendedEvent != null)
				{
					if (this.DbAccessPassword())
					{
						DAOServer.ExecuteReaderByStoredProc_SendedEvent(_strConn, strStoredProc);
						result = this.dao.DbExecuteReaderByStoredProc(_strConn, strStoredProc);
					}
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(string.Concat(new string[]
				{
					"IDataReader执行存过 只读！[主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"]"
				}), ex.ToString());
			}
			return result;
		}

		public System.Data.DataSet DbLoadDataSetPaging(string _strConn, string tblName, string fldName, int PageSize, int PageIndex, int IsReCount, int OrderType, string strWhere, string strLoadTableName)
		{
			System.Data.DataSet result = new System.Data.DataSet();
			try
			{
				if (DAOServer.LoadDataSetPaging_SendedEvent != null)
				{
					if (this.DbAccessPassword())
					{
						DAOServer.LoadDataSetPaging_SendedEvent(_strConn, tblName, fldName, PageSize, PageIndex, IsReCount, OrderType, strWhere, strLoadTableName);
						result = this.dao.DbLoadDataSetPaging(_strConn, tblName, fldName, PageSize, PageIndex, IsReCount, OrderType, strWhere, strLoadTableName);
					}
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(string.Concat(new string[]
				{
					"IDataReader执行存过 只读！[主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"]"
				}), ex.ToString());
			}
			return result;
		}

		private void WWFSaveLog(string strErrorTitle, string strException)
		{
			try
			{
				Log log = new Log();
				log.WWFSaveLog(strErrorTitle, strException);
			}
			catch
			{
			}
		}

		public override object InitializeLifetimeService()
		{
			return null;
		}

		public int DbReportImgAdd(string _strConn, string strStoredProcImg, string strfimg_type, byte[] fimg, string strforder_by, string fremark, string strfsample_id)
		{
			int result = 0;
			try
			{
				if (DAOServer.ExecuteNonQuery_SendedEvent != null)
				{
					if (this.DbAccessPassword())
					{
						DAOServer.DbReportImgAdd_SendedEvent(_strConn, strStoredProcImg, strfimg_type, fimg, strforder_by, fremark, strfsample_id);
						result = this.dao.DbReportImgAdd(_strConn, strStoredProcImg, strfimg_type, fimg, strforder_by, fremark, strfsample_id);
					}
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(string.Concat(new string[]
				{
					"执行报告图形sql语句！[主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"]"
				}), ex.ToString());
			}
			return result;
		}

		public int DbInstrImgAdd(string _strConn, string strStoredProcImg, string strfimg_type, byte[] fimg, string strforder_by, string fremark, string strfio_id)
		{
			int result = 0;
			try
			{
				if (DAOServer.ExecuteNonQuery_SendedEvent != null)
				{
					if (this.DbAccessPassword())
					{
						DAOServer.DbInstrImgAdd_SendedEvent(_strConn, strStoredProcImg, strfimg_type, fimg, strforder_by, fremark, strfio_id);
						result = this.dao.DbInstrImgAdd(_strConn, strStoredProcImg, strfimg_type, fimg, strforder_by, fremark, strfio_id);
					}
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(string.Concat(new string[]
				{
					"执行仪器图形sql语句！[主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"]"
				}), ex.ToString());
			}
			return result;
		}

		public System.Data.DataTable DBReport(IList ilt)
		{
			DAOServer.ReportReturnDT = new System.Data.DataTable();
			DAOServer.ReportIlist = ilt;
			try
			{
				if (DAOServer.DBReport_SendedEvent != null)
				{
					if (this.DbAccessPassword())
					{
						DAOServer.DBReport_SendedEvent(ilt);
						this.GetFormByFormNameForm(DAOServer.ReportIlist[0].ToString()).Show();
						this.WWFSaveLog("报表记录数为：" + DAOServer.ReportReturnDT.Rows.Count.ToString(), "");
					}
				}
			}
			catch (Exception ex)
			{
				this.WWFSaveLog(string.Concat(new string[]
				{
					"执行DBReport [主机名：",
					this._HostName,
					" IP地址：",
					this._HostIpAddress,
					"]"
				}), ex.ToString());
			}
			return DAOServer.ReportReturnDT;
		}

		private Form GetFormByFormNameForm(string strFormName)
		{
			Form result = null;
			try
			{
				Type type = Type.GetType(strFormName);
				result = (Form)Activator.CreateInstance(type);
			}
			catch (Exception ex)
			{
				this.WWFSaveLog("报表设置有误：", ex.ToString());
			}
			return result;
		}
	}
}
