using System;

namespace ww.wwf.dao
{
	public delegate void LoadDataSetByStoredProc_EventHandler(string _strConn, string strStoredProcName, string strTableName, string strInParameterName, string strInParameterValue);
}
