using System;
using System.Data;

namespace ww.wwf.dao
{
	public delegate void UpdateDataSetStoredProcInsert2_EventHandler(string _strConn, System.Data.DataSet dsObject_1, string strTableName_1, string strStoredProcInsertCommand_1, System.Data.DataSet dsObject_2, string strTableName_2, string strStoredProcInsertCommand_2);
}
