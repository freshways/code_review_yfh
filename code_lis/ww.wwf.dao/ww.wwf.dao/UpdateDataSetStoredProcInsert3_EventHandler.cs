using System;
using System.Data;

namespace ww.wwf.dao
{
	public delegate void UpdateDataSetStoredProcInsert3_EventHandler(string _strConn, System.Data.DataSet dsObject_1, string strTableName_1, string strStoredProcInsertCommand_1, System.Data.DataSet dsObject_2, string strTableName_2, string strStoredProcInsertCommand_2, System.Data.DataSet dsObject_3, string strTableName_3, string strStoredProcInsertCommand_3);
}
