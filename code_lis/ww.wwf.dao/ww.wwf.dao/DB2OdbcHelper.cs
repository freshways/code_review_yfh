using System;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;

namespace ww.wwf.dao
{
	public class DB2OdbcHelper
	{
		private const string dsn = "DSN=OutBound";

		private const string conn = "Driver={IBM DB2 ODBC DRIVER};Database=YSDB;hostname=192.168.10.101;port=60000;protocol=TCPIP; uid=db2inst1; pwd=ysxwsj001";

		public static int ExecuteNonQuery(string strSQL)
		{
			return DB2OdbcHelper.ExecuteNonQuery("Driver={IBM DB2 ODBC DRIVER};Database=YSDB;hostname=192.168.10.101;port=60000;protocol=TCPIP; uid=db2inst1; pwd=ysxwsj001", strSQL);
		}

		public static int ExecuteNonQuery(string connectionStr, string strSQL)
		{
			System.Data.Odbc.OdbcConnection odbcConnection = new System.Data.Odbc.OdbcConnection(connectionStr);
			odbcConnection.Open();
			System.Data.Odbc.OdbcCommand odbcCommand = new System.Data.Odbc.OdbcCommand(strSQL, odbcConnection);
			int result;
			try
			{
				result = odbcCommand.ExecuteNonQuery();
				return result;
			}
			catch (Exception var_2_21)
			{
			}
			result = -1;
			return result;
		}

		public static System.Data.DataSet ExecuteDs(string strSQL)
		{
			return DB2OdbcHelper.ExecuteDs("Driver={IBM DB2 ODBC DRIVER};Database=YSDB;hostname=192.168.10.101;port=60000;protocol=TCPIP; uid=db2inst1; pwd=ysxwsj001", strSQL);
		}

		public static System.Data.DataSet ExecuteDs(string connectionStr, string strSQL)
		{
			System.Data.Odbc.OdbcConnection odbcConnection = new System.Data.Odbc.OdbcConnection(connectionStr);
			odbcConnection.Open();
			System.Data.Odbc.OdbcCommand selectCommand = new System.Data.Odbc.OdbcCommand(strSQL, odbcConnection);
			System.Data.DataSet result;
			try
			{
				System.Data.DataSet dataSet = new System.Data.DataSet();
				System.Data.Odbc.OdbcDataAdapter odbcDataAdapter = new System.Data.Odbc.OdbcDataAdapter(selectCommand);
				odbcDataAdapter.Fill(dataSet);
				result = dataSet;
				return result;
			}
			catch (System.Data.SqlClient.SqlException var_4_32)
			{
			}
			result = null;
			return result;
		}

		public static System.Data.Odbc.OdbcDataReader ExecuteReader(string strSQL)
		{
			return DB2OdbcHelper.ExecuteReader("Driver={IBM DB2 ODBC DRIVER};Database=YSDB;hostname=192.168.10.101;port=60000;protocol=TCPIP; uid=db2inst1; pwd=ysxwsj001", strSQL);
		}

		public static System.Data.Odbc.OdbcDataReader ExecuteReader(string connectionStr, string strSQL)
		{
			System.Data.Odbc.OdbcConnection odbcConnection = new System.Data.Odbc.OdbcConnection(connectionStr);
			odbcConnection.Open();
			System.Data.Odbc.OdbcCommand odbcCommand = new System.Data.Odbc.OdbcCommand(strSQL, odbcConnection);
			System.Data.Odbc.OdbcDataReader result;
			try
			{
				System.Data.Odbc.OdbcDataReader odbcDataReader = odbcCommand.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
				result = odbcDataReader;
				return result;
			}
			catch (System.Data.SqlClient.SqlException var_3_26)
			{
			}
			result = null;
			return result;
		}
	}
}
