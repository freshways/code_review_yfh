using System;
using System.IO;

namespace ww.wwf.dao
{
	public class Log
	{
		public void WWFSaveLog(string strErrorTitle, string strException)
		{
			if (strErrorTitle == "" || strErrorTitle == "")
			{
				strErrorTitle = "一般异常";
			}
			StreamWriter streamWriter = File.AppendText("wwfserver.log");
			streamWriter.WriteLine("时间：" + DateTime.Now.ToString());
			streamWriter.WriteLine("系统：wwfserver");
			streamWriter.WriteLine("标题：" + strErrorTitle);
			streamWriter.WriteLine("内容：" + strException);
			streamWriter.WriteLine("");
			streamWriter.Flush();
			streamWriter.Close();
		}
	}
}
