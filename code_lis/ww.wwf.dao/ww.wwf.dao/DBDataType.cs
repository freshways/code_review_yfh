using System;

namespace ww.wwf.dao
{
	public class DBDataType
	{
		public static string strVarchar_Access = "TEXT";

		public static string strFloat_Access = "FLOAT";

		public static string strVarchar_SqlServer = "VARCHAR";

		public static string strFloat_SqlServer = "FLOAT";

		public static string strVarchar_Oracle = "VARCHAR";

		public static string strFloat_Oracle = "FLOAT";
	}
}
