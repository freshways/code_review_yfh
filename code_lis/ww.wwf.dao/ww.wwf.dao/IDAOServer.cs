using System;
using System.Collections;
using System.Data;

namespace ww.wwf.dao
{
	public interface IDAOServer
	{
		bool DbClientUser(string strClientID);

		System.Data.DataSet DbLoadDataSetBySqlString(string _strConn, string strSql, string strTableName);

		System.Data.DataSet DbLoadDataSetByStoredProc(string _strConn, string strStoredProcName, string strTableName, string strInParameterName, string strInParameterValue);

		System.Data.DataSet DbExecuteDataSetBySqlString(string _strConn, string strSql);

		System.Data.DataSet DbExecuteDataSetByStoredProc(string _strConn, string strStoredProcName);

		System.Data.DataSet DbExecuteDataSetPaging(string _strConn, string tblName, string fldName, int PageSize, int PageIndex, int IsReCount, int OrderType, string strWhere);

		int DbExecuteNonQueryBySqlString(string _strConn, string strSql);

		bool DbRecordExists(string _strConn, string sql);

		object DbExecuteScalarBySqlString(string _strConn, string strSql);

		object DbExecuteScalarByStoredProc(string _strConn, string strStoredProc);

		int DbUpdateDataSet(string _strConn, System.Data.DataSet dsObject, string strTableName, string strKey, string strStoredProcInsertCommand, string strStoredProcDeleteCommand, string strStoredProcupdateCommand, int intUpdateBehavior);

		int DbUpdateDataSetStoredProcInsert2(string _strConn, System.Data.DataSet dsObject_1, string strTableName_1, string strStoredProcInsertCommand_1, System.Data.DataSet dsObject_2, string strTableName_2, string strStoredProcInsertCommand_2);

		int DbUpdateDataSetStoredProcInsert3(string _strConn, System.Data.DataSet dsObject_1, string strTableName_1, string strStoredProcInsertCommand_1, System.Data.DataSet dsObject_2, string strTableName_2, string strStoredProcInsertCommand_2, System.Data.DataSet dsObject_3, string strTableName_3, string strStoredProcInsertCommand_3);

		int DbUpdateDataSetStoredProcUpdate2(string _strConn, System.Data.DataSet dsObject_1, string strTableName_1, string strStoredProcUpdateCommand_1, System.Data.DataSet dsObject_2, string strTableName_2, string strStoredProcUpdateCommand_2);

		int DbUpdateDataSetStoredProcUpdate3(string _strConn, System.Data.DataSet dsObject_1, string strTableName_1, string strStoredProcUpdateCommand_1, System.Data.DataSet dsObject_2, string strTableName_2, string strStoredProcUpdateCommand_2, System.Data.DataSet dsObject_3, string strTableName_3, string strStoredProcUpdateCommand_3);

		bool DbExecuteNonQueryTransaction2(string _strConn, string strSql1, string strSql2);

		bool DbExecuteNonQueryTransaction3(string _strConn, string strSql1, string strSql2, string strSql3);

		bool DbExecuteNonQueryTransaction4(string _strConn, string strSql1, string strSql2, string strSql3, string strSql4);

		System.Data.IDataReader DbExecuteReaderBySqlString(string _strConn, string strSql);

		System.Data.IDataReader DbExecuteReaderByStoredProc(string _strConn, string strStoredProc);

		System.Data.DataSet DbLoadDataSetPaging(string _strConn, string tblName, string fldName, int PageSize, int PageIndex, int IsReCount, int OrderType, string strWhere, string strLoadTableName);

		string DbTransaction(string _strConn, IList sqlList);

		int DbReportImgAdd(string _strConn, string strStoredProcImg, string strfimg_type, byte[] fimg, string strforder_by, string fremark, string strfsample_id);

		int DbInstrImgAdd(string _strConn, string strStoredProcImg, string strfimg_type, byte[] fimg, string strforder_by, string fremark, string strfio_id);

		System.Data.DataTable DBReport(IList ilt);
	}
}
