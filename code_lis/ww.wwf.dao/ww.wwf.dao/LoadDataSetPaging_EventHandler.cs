using System;

namespace ww.wwf.dao
{
	public delegate void LoadDataSetPaging_EventHandler(string _strConn, string tblName, string fldName, int PageSize, int PageIndex, int IsReCount, int OrderType, string strWhere, string strLoadTableName);
}
