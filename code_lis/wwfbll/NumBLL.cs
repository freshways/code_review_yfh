using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ww.wwf.com;
using ww.wwf.dao;
namespace ww.wwf.wwfbll
{
    /// <summary>
    /// 
    /// </summary>
    public class NumBLL : DAOWWF
    {
        public NumBLL()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //	
        }


        /// <summary>
        /// 所有列表
        /// </summary>
        /// <returns></returns>
        public DataTable BllDT()
        {
            string sql = "SELECT * FROM wwf_num";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }
        /// <summary>
        /// 据ID取得长度sam_jy_fsample_code
        /// </summary>
        /// <param name="fnum_id"></param>
        /// <returns></returns>
        public int BllGetflengthByid(string fnum_id)
        {
            string sql = "SELECT flength FROM wwf_num WHERE (fnum_id = '" + fnum_id + "')";
            string strR = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).ToString();
            if (strR == "" || strR == null)
                return 0;
            else
                return Convert.ToInt32(strR);
        }

    }
}