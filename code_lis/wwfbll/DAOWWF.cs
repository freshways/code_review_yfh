using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using ww.wwf.dao;

namespace ww.wwf.wwfbll
{ 
    /// <summary>
    ///  数据库存取 业务逻辑 框架(系统)数据库
    /// </summary>
    public class DAOWWF
    {
        public DAOWWF()
        {
           
        }      

        public string DbGuid()
        {
            string strGuid = "";
            //string strGuid = System.Guid.NewGuid().ToString().ToUpper();
            strGuid = System.Guid.NewGuid().ToString();
            return strGuid.Replace("-", "");
        }
        /// <summary>
        /// 取得当前服务器日期时间 Access SqlServer、Oracle
        /// </summary>       
        /// <returns></returns>
        public string DbServerDateTim()
        {
            string datetime = System.DateTime.Now.ToString(); //取当前时间
            //数据库软件SqlServer/Access/Oracle
            if (LoginBLL.sysDB == "SqlServer" || LoginBLL.sysDB == "SqlServer")
            {
                
                datetime = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, "SELECT TOP 1 GETDATE() AS ServerDateTime FROM wwf_sys").ToString();
            }
            /*
            else if (LoginBLL.sysDB == "Oracle" || LoginBLL.sysDB == "oracle")//未测试
            {
             * //select t.*, t.rowid from OUTP_ORDERS t where t.visit_date>TO_DATE('2006-12-20','yyyy-mm-dd')
                datetime = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, "SELECT TOP 1 GETDATE() AS ServerDateTime FROM wwf_sys").ToString();
            }*/
            return datetime;
        }
        /// <summary>
        /// 取得格式后日期时间 2008-05-01 开始时间
        /// </summary>
        /// <param name="dtp"></param>
        /// <returns></returns>
        public string DbDateTime1(System.Windows.Forms.DateTimePicker dtp)
        {
            string strYear = dtp.Value.Year.ToString();
            string strMonth = dtp.Value.Month.ToString();
            string strDay = dtp.Value.Day.ToString();

            if (strMonth.Length == 1)
                strMonth = "0" + strMonth;

            if (strDay.Length == 1)
                strDay = "0" + strDay;

            return strYear + "-" + strMonth + "-" + strDay;
        }

        public string DbSysTime()
        {
            return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }
        /// <summary>
        /// 取得格式后日期时间 2008-05-01 结束时间
        /// </summary>
        /// <param name="dtp"></param>
        /// <returns></returns>
        public string DbDateTime2(System.Windows.Forms.DateTimePicker dtp)
        {
            string strYear = dtp.Value.Year.ToString();
            string strMonth = dtp.Value.Month.ToString();
            string strDay = dtp.Value.Day.ToString();

            if (strMonth.Length == 1)
                strMonth = "0" + strMonth;

            if (strDay.Length == 1)
                strDay = "0" + strDay;

            return strYear + "-" + strMonth + "-" + strDay+" 23:59:59";
        }
        /// <summary>
        /// 算号器
        /// </summary>
        /// <param name="fnum_id"></param>
        /// <returns></returns>
        public string DbNum(string fnum_id)
        {
            int intCurrentno;
            string ls_id, ls_tail = "";
            string za_sysyear, za_sysmonth, za_sysday, za_Prehead;
            int za_currentno, za_Length;//当前号
            string ls_sysyear = "", ls_sysmonth = "", ls_sysday = "";
            DateTime ldt_systime;
            String sql = "SELECT * FROM wwf_num WHERE fnum_id='" + fnum_id + "'";
            DataTable dt = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
            if (dt.Rows.Count != 0)
            {
                za_sysyear = dt.Rows[0]["fyear"].ToString();
                za_sysmonth = dt.Rows[0]["fmonth"].ToString();
                za_sysday = dt.Rows[0]["fday"].ToString();
                try
                {
                    za_currentno = Convert.ToInt32(dt.Rows[0]["fcurrent_num"].ToString());
                }
                catch
                {
                    za_currentno = 0;
                }
                za_Prehead = dt.Rows[0]["fprehead"].ToString();
                try
                {
                    za_Length = Convert.ToInt32(dt.Rows[0]["flength"].ToString());
                }
                catch
                {
                    za_Length = 4;
                }
                String strSet = "";
                ls_tail = za_Prehead.Trim();
                ldt_systime = Convert.ToDateTime(DbServerDateTim());
                ls_sysyear = ldt_systime.ToString("yyyy");
                ls_sysmonth = ldt_systime.ToString("MM");
                ls_sysday = ldt_systime.ToString("dd");
                if ((za_sysyear == null) || (za_sysyear.Length == 0))
                { }
                else
                {
                    if ((za_sysyear != ls_sysyear))
                    {
                        strSet = ",fyear='" + ls_sysyear + "'";
                    }
                    ls_tail = ls_tail + ls_sysyear;
                }
                if ((za_sysmonth == null) || (za_sysmonth.Length == 0))
                { }
                else
                {
                    if ((za_sysmonth != ls_sysmonth))
                    {
                        strSet = ",fmonth='" + ls_sysmonth + "'";

                    }
                    ls_tail = ls_tail + ls_sysmonth;
                }
                if ((za_sysday == null) || za_sysday.Length == 0)
                { }
                else
                {
                    if ((za_sysday != ls_sysday))
                    {
                        strSet = ",fday='" + ls_sysday + "'";
                    }
                    ls_tail = ls_tail + ls_sysday;
                }
                intCurrentno = za_currentno + 1;
                ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,"Update wwf_num Set fcurrent_num=" + intCurrentno + strSet + " where fnum_id='" + fnum_id + "'");
                ls_id = ls_tail + za_currentno.ToString().PadLeft(za_Length, '0');
            }
            else
            {
                ls_id = "-2";
            }
            return ls_id;
        }

    }
}