using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ww.wwf.com;
using ww.wwf.dao;
namespace ww.wwf.wwfbll
{
    /// <summary>
    /// 
    /// </summary>
    public class FuncParamBLL : DAOWWF
    {
        public static string stringssss= "";
        public FuncParamBLL()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //	
        }
       /// <summary>
        /// 功能参数列表 不管类别时参数为-1
       /// </summary>
       /// <param name="strffunc_id"></param>
       /// <param name="strtypeid"></param>
       /// <returns></returns>
        public DataTable BllDT(string strffunc_id, string strtypeid)
        {
            string strType = "";
            if (strtypeid == "-1")
            { }
            else
                strType = " and (ftype='" + strtypeid + "')";
            // string sql = "SELECT * FROM WWF_FUNC_PARAM";
            string sql = "SELECT * FROM WWF_FUNC_PARAM where (ffunc_id='" + strffunc_id + "') " + strType + " order by ftype,forderby";
            stringssss = sql;
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public int BllUpdate(DataRowView dr)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update WWF_FUNC_PARAM set ");
            strSql.Append("fpid='" + dr["fpid"].ToString() + "',");
           // strSql.Append("ffunc_id='" + dr["ffunc_id"].ToString() + "',");
            strSql.Append("ftype='" + dr["ftype"].ToString() + "',");
            //strSql.Append("fparam='" + dr["fparam"].ToString() + "',");
            strSql.Append("fremark='" + dr["fremark"].ToString() + "',");
            strSql.Append("fuse='" + dr["fuse"].ToString() + "',");
            strSql.Append("forderby='" + dr["forderby"].ToString() + "'");
            strSql.Append(" where fid='" + dr["fid"].ToString() + "'");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());
        }
    }
}