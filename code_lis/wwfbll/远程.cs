using System;
using System.Data;
using System.Data.Common;
using System.Collections;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using ww.wwf.dao;

namespace ww.wwf.wwfbll
{
    /*
     using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using ww.wwf.dao;
                wwf = (DAOServer)Activator.GetObject(typeof(DAOServer), System.Configuration.ConfigurationSettings.AppSettings["DAOServer"]);
                wwf.strConn = "WW.DBConn";
                wwf.strHostName = ww.wwf.com.Net.GetHostName();
                wwf.strHostIpAddress = ww.wwf.com.Net.GetHostIpAddress();
                this.dataGridView1.DataSource = this.wwf.ExecuteDataSet("SELECT * FROM wwf_sys").Tables[0];


               */



    /// <summary>
    ///  数据库存取 业务逻辑 框架(系统)数据库
    /// </summary>
    public class DAOWWF
    {
       // protected DAO dao = new DAO();//本地
        protected DAOServer dao = null;//远程 Remoting
        protected string strDBConn = "WW.DBConn";//远程 Remoting
        public DAOWWF()
        {
            dao = (DAOServer)Activator.GetObject(typeof(DAOServer), System.Configuration.ConfigurationSettings.AppSettings["DAOServer"]);            
            dao.strHostName = ww.wwf.com.Net.GetHostName();
            dao.strHostIpAddress = ww.wwf.com.Net.GetHostIpAddress();
            
        }
        public string DbGuid()
        {
            string strGuid = "";
            //string strGuid = System.Guid.NewGuid().ToString().ToUpper();
            strGuid = System.Guid.NewGuid().ToString();
            return strGuid.Replace("-", "");
        }
        /// <summary>
        /// 取得当前服务器日期时间 Sql Server、Oracle
        /// </summary>       
        /// <returns></returns>
        public string DbServerDateTim()
        {
            
            return this.dao.DbExecuteScalarBySqlString(this.strDBConn,"SELECT TOP 1 GETDATE() AS ServerDateTime FROM wwf_sys").ToString();
        }
        /// <summary>
        /// 算号器
        /// </summary>
        /// <param name="fnum_id"></param>
        /// <returns></returns>
        public string DbNum(string fnum_id)
        {
            int intCurrentno;
            string ls_id, ls_tail = "";
            string za_sysyear, za_sysmonth, za_sysday, za_Prehead;
            int za_currentno, za_Length;//当前号
            string ls_sysyear = "", ls_sysmonth = "", ls_sysday = "";
            DateTime ldt_systime;
            String sql = "SELECT * FROM wwf_num WHERE fnum_id='" + fnum_id + "'";
            DataTable dt = this.dao.DbExecuteDataSetBySqlString(this.strDBConn,sql).Tables[0];
            if (dt.Rows.Count != 0)
            {
                za_sysyear = dt.Rows[0]["fyear"].ToString();
                za_sysmonth = dt.Rows[0]["fmonth"].ToString();
                za_sysday = dt.Rows[0]["fday"].ToString();
                try
                {
                    za_currentno = Convert.ToInt32(dt.Rows[0]["fcurrent_num"].ToString());
                }
                catch
                {
                    za_currentno = 0;
                }
                za_Prehead = dt.Rows[0]["fprehead"].ToString();
                try
                {
                    za_Length = Convert.ToInt32(dt.Rows[0]["flength"].ToString());
                }
                catch
                {
                    za_Length = 4;
                }
                String strSet = "";
                ls_tail = za_Prehead.Trim();
                ldt_systime = Convert.ToDateTime(DbServerDateTim());
                ls_sysyear = ldt_systime.ToString("yyyy");
                ls_sysmonth = ldt_systime.ToString("MM");
                ls_sysday = ldt_systime.ToString("dd");
                if ((za_sysyear == null) || (za_sysyear.Length == 0))
                { }
                else
                {
                    if ((za_sysyear != ls_sysyear))
                    {
                        strSet = ",fyear='" + ls_sysyear + "'";
                    }
                    ls_tail = ls_tail + ls_sysyear;
                }
                if ((za_sysmonth == null) || (za_sysmonth.Length == 0))
                { }
                else
                {
                    if ((za_sysmonth != ls_sysmonth))
                    {
                        strSet = ",fmonth='" + ls_sysmonth + "'";

                    }
                    ls_tail = ls_tail + ls_sysmonth;
                }
                if ((za_sysday == null) || za_sysday.Length == 0)
                { }
                else
                {
                    if ((za_sysday != ls_sysday))
                    {
                        strSet = ",fday='" + ls_sysday + "'";
                    }
                    ls_tail = ls_tail + ls_sysday;
                }
                intCurrentno = za_currentno + 1;
                this.dao.DbExecuteNonQueryBySqlString(this.strDBConn,"Update wwf_num Set fcurrent_num=" + intCurrentno + strSet + " where fnum_id='" + fnum_id + "'");
                ls_id = ls_tail + za_currentno.ToString().PadLeft(za_Length, '0');
            }
            else
            {
                ls_id = "-2";
            }
            return ls_id;
        }

    }
}