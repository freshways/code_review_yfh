﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;

namespace ww.wwf.wwfbll
{
    public partial class WindowsPersonForm : Form
    {
        PersonBLL bllPerson = new PersonBLL();
        DataTable dtCurrAll = new DataTable();
        //DataTable dtCurr = new DataTable();
        DeptBll bllDept = new DeptBll();
        DataTable dtDept = new DataTable();
        int iinit = 0;
        DataTable dtThis = null;
        public WindowsPersonForm()
        {
            InitializeComponent();
            
            
        }
        private WindowsResult r;
        public WindowsPersonForm(WindowsResult r)
            : this()
        {
            this.r = r;

        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            try
            {
                string fperson_id = this.dataGridView1.CurrentRow.Cells["fperson_id"].Value.ToString();
                r.ChangeTextString(fperson_id);
                this.Close();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void buttonNO_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// 列出系统部门树
        /// </summary>
        public void GetTree(string pid)
        {
            try
            {
                this.wwTreeView1.ZADataTable = dtDept;
                this.wwTreeView1.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeView1.ZAParentFieldName = "fp_id";//上级字段名称
                this.wwTreeView1.ZADisplayFieldName = "fname";//显示字段名称
                this.wwTreeView1.ZAKeyFieldName = "fdept_id";//主键字段名称
                this.wwTreeView1.ZATreeViewShow();//显示树        
                try
                {
                    this.wwTreeView1.SelectedNode = wwTreeView1.Nodes[0].Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }
        private void ApplyUserForm_Load(object sender, EventArgs e)
        {
            try
            {
                dtDept = this.bllDept.BllDeptDTuse();
                this.com_listBindingSource.DataSource = dtDept;
                GetTree("-1");               
            
              
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void GetDT(string fdept_id)
        {
            try
            {
                //string sql = "SELECT * FROM wwf_person where fhelp_code='%" + textBox1.Text + "%' order by forder_by";
                //this.richTextBox1.Text = sql;
                dtThis = this.bllPerson.BllDTByfhelp_code(textBox1.Text, fdept_id);
                this.dataGridView1.DataSource = dtThis;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                GetDT("");
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void wwTreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                if (iinit == 0)
                {
                    GetDT("");
                }
                else
                {
                    GetDT(e.Node.Name.ToString());
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally {
                iinit = 1;
            }
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                string fperson_id = this.dataGridView1.CurrentRow.Cells["fperson_id"].Value.ToString();
                r.ChangeTextString(fperson_id);
                this.Close();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Space))//或(e.KeyCode==Keys.Enter) 
            {
                try
                {
                    string fperson_id = this.dataGridView1.CurrentRow.Cells["fperson_id"].Value.ToString();
                    r.ChangeTextString(fperson_id);
                    this.Close();
                }
                catch (Exception ex)
                {
                    WWMessage.MessageShowError(ex.ToString());
                }
            }
        }
    }
}