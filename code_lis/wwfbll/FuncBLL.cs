using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ww.wwf.com;
using ww.wwf.dao;
namespace ww.wwf.wwfbll
{
    /// <summary>
    /// 
    /// </summary>
    public class FuncBLL : DAOWWF
    {
        public FuncBLL()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //	
        }


        /// <summary>
        /// 所有功能列表
        /// </summary>
        /// <returns></returns>
        public DataTable BllFuncDT()
        {
            string sql = "SELECT * FROM wwf_func ORDER BY forder_by";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }
        /// <summary>
        /// 所有功能列表 上级取下级
        /// </summary>
        /// <returns></returns>
        public DataTable BllFuncDT(string fp_id)
        {
            string sql = "SELECT * FROM wwf_func where fp_id='" + fp_id + "' ORDER BY forder_by";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }
        /// <summary>
        /// 所有功能列表 上级取下级
        /// </summary>
        /// <returns></returns>
        public DataTable BllFuncDTByID(string ffunc_id)
        {
            string sql = "SELECT * FROM wwf_func where ffunc_id='" + ffunc_id + "'";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int BllUpdate(DataTable dt)
        {
            int returnValue = 0;

            StringBuilder strSql = new StringBuilder();
            strSql.Append("update wwf_func set ");
            strSql.Append("fp_id='" + dt.Rows[0]["fp_id"].ToString() + "',");
            strSql.Append("fname='" + dt.Rows[0]["fname"].ToString() + "',");
            strSql.Append("fname_e='" + dt.Rows[0]["fname_e"].ToString() + "',");
            strSql.Append("ficon='" + dt.Rows[0]["ficon"].ToString() + "',");
            strSql.Append("ffunc_winform='" + dt.Rows[0]["ffunc_winform"].ToString() + "',");
            strSql.Append("ffunc_web='" + dt.Rows[0]["ffunc_web"].ToString() + "',");
            strSql.Append("forder_by='" + dt.Rows[0]["forder_by"].ToString() + "',");
            strSql.Append("fuse_flag=" + dt.Rows[0]["fuse_flag"].ToString() + ",");
            strSql.Append("fhelp='" + dt.Rows[0]["fhelp"].ToString() + "',");
            if (dt.Rows[0]["fcj_flag"].ToString() == "" || dt.Rows[0]["fcj_flag"].ToString() == null)
                strSql.Append("fcj_flag=0,");
            else
                strSql.Append("fcj_flag='" + dt.Rows[0]["fcj_flag"].ToString() + "',");
            strSql.Append("fcj_name='" + dt.Rows[0]["fcj_name"].ToString() + "',");
            strSql.Append("fcj_zp='" + dt.Rows[0]["fcj_zp"].ToString() + "',");
            strSql.Append("fopen_mode='" + dt.Rows[0]["fopen_mode"].ToString() + "',");
            strSql.Append("fremark='" + dt.Rows[0]["fremark"].ToString() + "'");
            strSql.Append(" where ffunc_id='" + dt.Rows[0]["ffunc_id"].ToString() + "'");
            returnValue =ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());

            return returnValue;
        }
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int BllAdd(DataTable dt)
        {
            int returnValue = 0;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into wwf_func(");
            strSql.Append("ffunc_id,fp_id,fname,fname_e,ficon,ffunc_winform,ffunc_web,forder_by,fuse_flag,fhelp,fcj_flag,fcj_name,fcj_zp,fopen_mode,fremark");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + dt.Rows[0]["ffunc_id"].ToString() + "',");
            strSql.Append("'" + dt.Rows[0]["fp_id"].ToString() + "',");
            strSql.Append("'" + dt.Rows[0]["fname"].ToString() + "',");
            strSql.Append("'" + dt.Rows[0]["fname_e"].ToString() + "',");
            strSql.Append("'" + dt.Rows[0]["ficon"].ToString() + "',");
            strSql.Append("'" + dt.Rows[0]["ffunc_winform"].ToString() + "',");
            strSql.Append("'" + dt.Rows[0]["ffunc_web"].ToString() + "',");
            strSql.Append("'" + dt.Rows[0]["forder_by"].ToString() + "',");
            strSql.Append("" + dt.Rows[0]["fuse_flag"].ToString() + ",");
            strSql.Append("'" + dt.Rows[0]["fhelp"].ToString() + "',");
            strSql.Append("" + dt.Rows[0]["fcj_flag"].ToString() + ",");
            strSql.Append("'" + dt.Rows[0]["fcj_name"].ToString() + "',");
            strSql.Append("'" + dt.Rows[0]["fcj_zp"].ToString() + "',");
            strSql.Append("'" + dt.Rows[0]["fopen_mode"].ToString() + "',");
            strSql.Append("'" + dt.Rows[0]["fremark"].ToString() + "'");
            strSql.Append(")");
            returnValue =ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());

            return returnValue;
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool BllDelete(string id)
        {
            if (id == "-1" || id == "0")
            {
                return false;
            }
            else
            {
                string sql1 = "delete FROM wwf_func where ffunc_id='" + id + "'";
                string sql2 = "delete FROM wwf_org_func where ffunc_id='" + id + "'";
                return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryTransaction2(ww.wwf.wwfbll.WWFInit.strDBConn,sql1, sql2);
            }
        }
        /// <summary>
        /// 是否有下级
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool BllChildExists(string id)
        {
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbRecordExists(ww.wwf.wwfbll.WWFInit.strDBConn, "SELECT COUNT(1) FROM wwf_func WHERE (fp_id = '" + id + "')");
        }

    }
}