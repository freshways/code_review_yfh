using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ww.wwf.com;
using System.Collections;
using ww.wwf.dao;
namespace ww.wwf.wwfbll
{
    /// <summary>
    /// 
    /// </summary>
    public class UserdefinedBLL : DAOWWF
    {
        public UserdefinedBLL()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //	
        }
        #region  用户自定义类型
        /// <summary>
       /// 类型列表
       /// </summary>
       /// <param name="fuse_if"></param>
       /// <returns></returns>
        public DataTable BllTypeDT(int fuse_flag)
        {
            string sql = "";
            if (fuse_flag == 1 || fuse_flag == 0)
            {
                sql = "SELECT * FROM wwf_table where fuse_flag=" + fuse_flag + " ORDER BY forder_by";
            }
            else
            {
                sql = "SELECT * FROM wwf_table  ORDER BY forder_by";
            }
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }

        public DataTable BllTypeDTByfpid(string  fp_id)
        {
            string  sql = "SELECT * FROM wwf_table where fp_id='" + fp_id + "' ORDER BY forder_by";                        
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,sql).Tables[0];
            
        }

        public DataTable BllTypeDTByftable_id(string ftable_id)
        {
            string sql = "SELECT * FROM wwf_table where ftable_id='" + ftable_id + "'";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        /// <summary>
        /// 类型 增加一条数据
        /// </summary>
        public int BllTypeAdd(string fp_id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into wwf_table(");
            strSql.Append("ftable_id,fp_id,fuse_flag,fsum_flag,fexcel_top,fexcel_left,forder_by");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + this.DbGuid() + "',");
            strSql.Append("'" + fp_id + "',");         
            strSql.Append("1,");
            strSql.Append("1,");

            strSql.Append("1,");
            strSql.Append("1,");

            strSql.Append("'1'");
            strSql.Append(")");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());
            /*
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into wwf_table(");
            strSql.Append("ftable_id,fp_id,ftype,fcode,fname,fremark,fuse_flag,forder_by");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + dr["ftable_id"].ToString() + "',");
            strSql.Append("'" + dr["fp_id"].ToString() + "',");
            strSql.Append("'" + dr["ftype"].ToString() + "',");
            strSql.Append("'" + dr["fcode"].ToString() + "',");
            strSql.Append("'" + dr["fname"].ToString() + "',");
            strSql.Append("" + dr["fremark"].ToString() + ",");
            strSql.Append("'" + dr["fuse_flag"].ToString() + "',");
            strSql.Append("'" + dr["forder_by"].ToString() + "'");
            strSql.Append(")");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());
             * */
        }


        /// <summary>
        /// 类型 更新一条数据
        /// </summary>
        public int BllTypeUpdate(DataRow dr)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update wwf_table set ");
            strSql.Append("fp_id='" + dr["fp_id"].ToString() + "',");
            strSql.Append("ftype='" + dr["ftype"].ToString() + "',");
            strSql.Append("fcode='" + dr["fcode"].ToString() + "',");
            strSql.Append("fname='" + dr["fname"].ToString() + "',");
            strSql.Append("fremark='" + dr["fremark"].ToString() + "',");
            strSql.Append("fuse_flag=" + dr["fuse_flag"].ToString() + ",");
            strSql.Append("ftemplate='" + dr["ftemplate"].ToString() + "',");
            strSql.Append("fsum_flag=" + dr["fsum_flag"].ToString() + ",");
            strSql.Append("fexcel_left=" + dr["fexcel_left"].ToString() + ",");
            strSql.Append("fexcel_top=" + dr["fexcel_top"].ToString() + ",");   
            strSql.Append("forder_by='" + dr["forder_by"].ToString() + "'");
            strSql.Append(" where ftable_id='" + dr["ftable_id"].ToString() + "'");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());
        }
        #endregion

        #region  字段
        public DataTable BllColumnsByftable_id(string ftable_id)
        {
            string sql = "SELECT * FROM wwf_columns where ftable_id='" + ftable_id + "' ORDER BY forder_by";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }
        /// <summary>
        /// 字段 增加一条数据
        /// </summary>
        public int BllColumnsAdd(string ftable_id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into wwf_columns(");
            strSql.Append("fcolumns_id,ftable_id,fvalue_type,fxsws,fshow_flag,fgroup_flag,fsum_flag,fshow_width,fdefault,fdata_type,fread_flage,forder_by");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + this.DbGuid() + "',");
            strSql.Append("'" + ftable_id + "',");
            strSql.Append("'原始值',");//fold_value_flag
            strSql.Append("'2',");//fxsws
            strSql.Append("'1',");//fshow_flag
            strSql.Append("'0',");//fgroup_flag
            strSql.Append("'0',");//fsum_flag
            strSql.Append("'100',");//fshow_width
            strSql.Append("'',");//fdefault
            strSql.Append("'字符型',");//数据类型
            strSql.Append("0,");
            strSql.Append("'01'");//forder_by         
               
            strSql.Append(")");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());            
        }

        public int BllcolumnsDel(string ftable_id)
        {
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, "DELETE FROM WWF_COLUMNS WHERE (ftable_id = '" + ftable_id + "') and (fcode<>'CustomCol1' and fcode<>'CustomCol2')");
        }
         public int BllcolumnsZXKSDel(string ftable_id)
        {
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, "DELETE FROM WWF_COLUMNS WHERE (ftable_id = '" + ftable_id + "') and (fcode<>'CustomCol1' and fcode<>'Customcol3')");
        }
        
        /// <summary>
        /// 字段 增加一条数据
        /// </summary>
        public int BllColumnsAdd(string ftable_id, string fcode, string fname, string forder_by)
        {
          
            int intr = 0;
            if (ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbRecordExists(ww.wwf.wwfbll.WWFInit.strDBConn, "SELECT count(1) FROM wwf_columns where ftable_id='" + ftable_id + "' and fcode='" + fcode + "'"))
            { }
            else
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("insert into wwf_columns(");
                strSql.Append("fcolumns_id,ftable_id,fcode,fname,fvalue_type,fxsws,fshow_flag,fgroup_flag,fsum_flag,fshow_width,fdefault,fdata_type,forder_by");
                strSql.Append(")");
                strSql.Append(" values (");
                strSql.Append("'" + this.DbGuid() + "',");
                strSql.Append("'" + ftable_id + "',");
                strSql.Append("'" + fcode + "',");
                strSql.Append("'" + fname + "',");
                strSql.Append("'原始值',");//fold_value_flag
                strSql.Append("'2',");//fxsws
                strSql.Append("'1',");//fshow_flag
                strSql.Append("'0',");//fgroup_flag
                strSql.Append("'1',");//fsum_flag
                strSql.Append("'100',");//fshow_width
                strSql.Append("'',");//fdefault
                strSql.Append("'字符型',");//数据类型
                strSql.Append("'" + forder_by + "'");//forder_by         

                strSql.Append(")");
                if (ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString()) > 0)
                    intr = intr + 1;
            }
            return intr;
        }
        /// <summary>
        /// 字段 更新一条数据
        /// </summary>
        public int BllColumnsDel(string fcolumns_id)//dt.Rows[0][0][
        {
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, "delete FROM wwf_columns where fcolumns_id='" + fcolumns_id + "'");
        }
        /// <summary>
        /// 字段 更新一条数据
        /// </summary>
        public int BllColumnsUpdate(DataRow dr)//dt.Rows[0][0][
        {
            
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update wwf_columns set ");
            strSql.Append("ftable_id='" + dr["ftable_id"].ToString() + "',");
            strSql.Append("fcode='" + dr["fcode"].ToString() + "',");
            strSql.Append("fname='" + dr["fname"].ToString() + "',");
            strSql.Append("fold_columns='" + dr["fold_columns"].ToString() + "',");
            strSql.Append("fvalue_type='" + dr["fvalue_type"].ToString() + "',");
            strSql.Append("fdata_type='" + dr["fdata_type"].ToString() + "',");
            strSql.Append("fxsws=" + dr["fxsws"].ToString() + ",");
            strSql.Append("fshow_flag=" + dr["fshow_flag"].ToString() + ",");
            strSql.Append("fshow_width=" + dr["fshow_width"].ToString() + ",");
            strSql.Append("fgroup_flag=" + dr["fgroup_flag"].ToString() + ",");
            strSql.Append("fsum_flag=" + dr["fsum_flag"].ToString() + ",");
            strSql.Append("forder_by='" + dr["forder_by"].ToString() + "',");
            strSql.Append("fdefault='" + dr["fdefault"].ToString() + "',");          
            strSql.Append("fvalue_content='" + dr["fvalue_content"].ToString() + "',");
            strSql.Append("fread_flage=" + dr["fread_flage"].ToString() + ",");
            
            strSql.Append("fremark='" + dr["fremark"].ToString() + "'");
            strSql.Append(" where fcolumns_id='" + dr["fcolumns_id"].ToString() + "'");

            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,@strSql.ToString());
        }
        /*
        public string BllColumnsUpdate1111(DataRow dr)//dt.Rows[0][0][
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("update wwf_columns set ");
            strSql.Append("ftable_id='" + dr["ftable_id"].ToString() + "',");
            strSql.Append("fcode='" + dr["fcode"].ToString() + "',");
            strSql.Append("fname='" + dr["fname"].ToString() + "',");
            strSql.Append("fold_columns='" + dr["fold_columns"].ToString() + "',");
            strSql.Append("fold_value_flag=" + dr["fold_value_flag"].ToString() + ",");
            strSql.Append("fdata_type='" + dr["fdata_type"].ToString() + "',");
            strSql.Append("fxsws=" + dr["fxsws"].ToString() + ",");
            strSql.Append("fshow_flag=" + dr["fshow_flag"].ToString() + ",");
            strSql.Append("fshow_width=" + dr["fshow_width"].ToString() + ",");
            strSql.Append("fgroup_flag=" + dr["fgroup_flag"].ToString() + ",");
            strSql.Append("fsum_flag=" + dr["fsum_flag"].ToString() + ",");
            strSql.Append("forder_by='" + dr["forder_by"].ToString() + "',");
            strSql.Append("fdefault='" + dr["fdefault"].ToString() + "',");
            strSql.Append("fquery_sql='" + dr["fquery_sql"].ToString() + "',");
            strSql.Append("fformula='" + dr["fformula"].ToString() + "',");
            strSql.Append("fremark='" + dr["fremark"].ToString() + "'");
            strSql.Append(" where fcolumns_id='" + dr["fcolumns_id"].ToString() + "'");

            return strSql.ToString();
        }*/
        #endregion
       /// <summary>
       /// 临时表创建
       /// </summary>
       /// <param name="sql"></param>
       /// <returns></returns>
        public int BllTemTableCreate(string sql)
        {
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql);
        }
        /// <summary>
        /// 删除临时表
        /// </summary>
        /// <param name="strTableName"></param>
        /// <returns></returns>
        public int BllTemTableDel(string strTableName)
        {
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, "DROP TABLE " + strTableName);
        }
        /// <summary>
        /// 取得临时表
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public DataTable BllTemTableDT(string sql)
        {
           // string sql = "SELECT * FROM " + strTableName + " order by fid";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public string BllTemTableInsertBusiness(IList sqlList)
        {
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(ww.wwf.wwfbll.WWFInit.strDBConn, sqlList);
        }
        /// <summary>
        /// 修改临时表的合计值
        /// </summary>
        /// <param name="lissql"></param>
        /// <returns></returns>
        public string BllTemTableUpdateSum(IList lissql)
        {
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(ww.wwf.wwfbll.WWFInit.strDBConn, lissql);
        }
      
        /// <summary>
        /// 据表ID创建临时 内存表
        /// </summary>
        /// <param name="strTable_id"></param>
        public DataTable BllCreatYSDataTableBase(string strTable_id)
        {
            DataTable dtYSValue = new DataTable();
            DataTable dtTable = new DataTable();
            DataTable dtColumns = new DataTable();
            dtTable = BllTypeDTByftable_id(strTable_id);
            if (dtTable.Rows.Count > 0)
            {
                string ftype = dtTable.Rows[0]["ftype"].ToString();//表类型 只表才做运行                            
                if (ftype == "表")
                {
                    dtColumns = BllColumnsByftable_id(strTable_id);
                    if (dtColumns.Rows.Count > 0)
                    {
                        DataColumn columnFid = new DataColumn("fid", typeof(string));
                        dtYSValue.Columns.Add(columnFid);

                        string strColumnName = "";//列名
                        string strfvalue_type = "";//值类型
                        for (int i = 0; i < dtColumns.Rows.Count; i++)
                        {
                            strfvalue_type = dtColumns.Rows[i]["fvalue_type"].ToString();
                            strColumnName = dtColumns.Rows[i]["fcode"].ToString();
                            if (strfvalue_type == "公式值")
                            {
                            }
                            else
                            {
                                DataColumn column = new DataColumn(strColumnName, typeof(string));
                                dtYSValue.Columns.Add(column);
                            }
                        }
                    }
                }
            }
            return dtYSValue;
        }
        /// <summary>
        /// 取得科目列表
        /// </summary>
        /// <param name="ftable_id"></param>
        /// <returns></returns>
        public DataTable BllColumnsKMList(string ftable_id)
        {
            DataTable dtKM = new DataTable();
            DataColumn dataColumn1 = new DataColumn("科目", typeof(string));
            dtKM.Columns.Add(dataColumn1);

            string sql = "SELECT fvalue_content AS 科目 FROM wwf_columns t WHERE (ftable_id = '" + ftable_id + "') AND (fvalue_type = '科目值') ORDER BY forder_by";
            DataTable dt = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string strKM = dt.Rows[i]["科目"].ToString();
                DataRow[] currcolumXS = dtKM.Select("科目='" + strKM + "'");
                if (currcolumXS.Length > 0)
                {
                    /*
                    foreach (DataRow drOK in currcolumXS)
                    {
                        if (drOK["科目"].ToString() == strKM) { }
                        else
                        {
                            DataRow dr = dtKM.NewRow();
                            dr["科目"] = drOK["科目"].ToString();
                            dtKM.Rows.Add(dr);
                        }
                    }*/
                }
                else
                {
                    DataRow dr = dtKM.NewRow();
                    dr["科目"] = strKM;
                    dtKM.Rows.Add(dr);
                }
            }

            return dtKM;
        }
       
        /// <summary>
        /// 据表ID创建临时 内存表
        /// </summary>
        /// <param name="strTable_id"></param>
        public DataTable BllCreatYSDataTable(string strTable_id)
        {
            DataTable dtYSValue = new DataTable();
            DataTable dtTable = new DataTable();
            DataTable dtColumns = new DataTable();
            dtTable = BllTypeDTByftable_id(strTable_id);
            if (dtTable.Rows.Count > 0)
            {
                string ftype = dtTable.Rows[0]["ftype"].ToString();//表类型 只表才做运行                            
                if (ftype == "表")
                {
                    dtColumns = BllColumnsByftable_id(strTable_id);
                    if (dtColumns.Rows.Count > 0)
                    {
                        DataColumn columnFid = new DataColumn("fid", typeof(string));
                        dtYSValue.Columns.Add(columnFid);

                        string strColumnName = "";//列名
                        string strfvalue_type = "";//值类型
                        for (int i = 0; i < dtColumns.Rows.Count; i++)
                        {
                            strfvalue_type = dtColumns.Rows[i]["fvalue_type"].ToString();
                            strColumnName = dtColumns.Rows[i]["fcode"].ToString();
                           
                            //if (strfvalue_type == "公式值"||strfvalue_type == "合计值")
                            //if (strfvalue_type == "公式值")
                            //{
                            // }
                            // else
                            // {
                            DataColumn column = new DataColumn(strColumnName, typeof(string));
                            dtYSValue.Columns.Add(column);
                            //}
                        }
                    }
                }
            }
            return dtYSValue;
        }
    }
}

/*
 foreach(DataRowView drv in dv)
{
    System.Console.WriteLine(drv);
}

 */