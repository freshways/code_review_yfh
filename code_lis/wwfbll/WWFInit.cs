using System;
using System.Data;
using System.Data.Common;
using System.Collections;

using ww.wwf.com;
using ww.wwf.dao;
using System.IO;
namespace ww.wwf.wwfbll
{
    /// <summary>
    /// 系统初始化
    /// </summary>
    public class WWFInit
    {
        /// <summary>
        /// 远程 Remoting
        /// </summary>
       // public static DAOServer wwfRemotingDao = null;
       // public static DAO wwfRemotingDao = new DAO();
        public static DAO wwfRemotingDao = new DAO();
        /// <summary>
        /// 系统数据库
        /// </summary>
        public static string strDBConn = "WW.DBConn";
        /// <summary>
        /// His数据库
        /// </summary>
        public static string strCHisDBConn = "CHisDBConn";
        /// <summary>
        /// 业务数据库
        /// </summary>
       // public static string strDBConnBusiness = "WW.DBConn.Business";
        /// <summary>
        /// 客户端机器码
        /// </summary>
        public static string strClientID = "";

        public WWFInit()
        {
            DAO wwfRemotingDao = new DAO();
            //if (System.Configuration.ConfigurationSettings.AppSettings["WWServerUse"] == "true")
            //{               
            //   // DbCreatRemotingOBJ();
            //}
            
        }
        /// <summary>
        ///  实例化远程对象
        /// </summary>
        /// <returns>成功返回True,失败返回False</returns>
        private void DbCreatRemotingOBJ()
        {
            try
            {
                /*
                string strServerIP = System.Configuration.ConfigurationSettings.AppSettings["WWServerIP"];
                string strServerPort = System.Configuration.ConfigurationSettings.AppSettings["WWServerPort"];
                wwfRemotingDao = (DAOServer)Activator.GetObject(typeof(DAOServer),
      "http://" + strServerIP + ":" + strServerPort + "/DAOServer.soap");

                //获取本机IP
                wwfRemotingDao.strHostName = ww.wwf.com.Net.GetHostName();
                wwfRemotingDao.strHostIpAddress = ww.wwf.com.Net.GetHostIpAddress();
                wwfRemotingDao.strAccessPasswordClient = "F1-49-55-AA-44-F1-6D-12-0A-33-74-91-FF-0P-83-E7-tao";


                HardInfo hardinfo = new HardInfo();
                string str1 = hardinfo.GetNetCardMAC() + hardinfo.GetCpuID();
                string str2 = Security.StrToEncrypt("SHA1", str1);
                strClientID = str2.Replace("-", "");
                */
            }
            catch
            { }
        }
        /// <summary>
        /// 验证客户端是否已经注册
        /// </summary>
        /// <returns></returns>
        public bool DbClientUser()
        {
            bool bl = true;
           /*
           
            bl = wwfRemotingDao.DbClientUser(strClientID);          
            if (bl == false)
            {                
                ClientIDForm cf = new ClientIDForm();
                cf.Text = "此电脑未注册，请联系管理员！";
                cf.ShowDialog();               
            }       */    
            return bl;
        }

        public string DbGuid20()
        {
            string strGuid = "";
            //string strGuid = System.Guid.NewGuid().ToString().ToUpper();
            strGuid = System.Guid.NewGuid().ToString();
            strGuid = strGuid.Replace("-", "");


            int i = strGuid.Length;
            string new2 = strGuid.Substring(i - 20, 20);


            return new2;
        }
        public static void WWFSaveLog(string strErrorTitle, string strException)
        {
            if (strErrorTitle == "" || strErrorTitle == "")
            {
                strErrorTitle = "一般异常";
            }
            StreamWriter sw;
            //sw = File.AppendText(Server.MapPath(null) + "\\ZASuite~Log.log");//Web
            // <!--系统日志文件名--> <add key="SysLogFileName" value="Log.log" />
            //System.Configuration.ConfigurationSettings.AppSettings["SysName"] 
            sw = File.AppendText("wwf.log");//Winform

            sw.WriteLine("时间：" + System.DateTime.Now.ToString());
            sw.WriteLine("系统：" + "wwf");
            sw.WriteLine("标题：" + strErrorTitle);
            sw.WriteLine("内容：" + strException);
            sw.WriteLine("");
            sw.Flush();
            sw.Close();
        }

    }
}