﻿namespace ww.wwf.wwfbll
{
    partial class HelpForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        string SysHttpUrl = System.Configuration.ConfigurationSettings.AppSettings["SysHttpUrl"];

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HelpForm));
            this.simpleBrowserToolStrip = new System.Windows.Forms.ToolStrip();
            this.browserBackButton = new System.Windows.Forms.ToolStripButton();
            this.browserForwardButton = new System.Windows.Forms.ToolStripButton();
            this.browserAddressLabel = new System.Windows.Forms.ToolStripLabel();
            this.browserAddressTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.browserGoButton = new System.Windows.Forms.ToolStripButton();
            this.browserStopButton = new System.Windows.Forms.ToolStripButton();
            this.browserRefreshButton = new System.Windows.Forms.ToolStripButton();
            this.browserSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.zaSuiteTreeView1 = new ww.wwf.control.WWControlLib.WWTreeView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.打开ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelCurrUrl = new System.Windows.Forms.Label();
            this.simpleBrowserToolStrip.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // simpleBrowserToolStrip
            // 
            this.simpleBrowserToolStrip.AutoSize = false;
            this.simpleBrowserToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.browserBackButton,
            this.browserForwardButton,
            this.browserAddressLabel,
            this.browserAddressTextBox,
            this.browserGoButton,
            this.browserStopButton,
            this.browserRefreshButton,
            this.browserSeparator,
            this.toolStripButton1,
            this.toolStripButton2});
            this.simpleBrowserToolStrip.Location = new System.Drawing.Point(0, 0);
            this.simpleBrowserToolStrip.Name = "simpleBrowserToolStrip";
            this.simpleBrowserToolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.simpleBrowserToolStrip.Size = new System.Drawing.Size(845, 30);
            this.simpleBrowserToolStrip.TabIndex = 1;
            this.simpleBrowserToolStrip.Text = "toolStrip1";
            // 
            // browserBackButton
            // 
            this.browserBackButton.Image = ((System.Drawing.Image)(resources.GetObject("browserBackButton.Image")));
            this.browserBackButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.browserBackButton.Name = "browserBackButton";
            this.browserBackButton.Size = new System.Drawing.Size(49, 27);
            this.browserBackButton.Text = "后退";
            this.browserBackButton.Click += new System.EventHandler(this.browserBackButton_Click);
            // 
            // browserForwardButton
            // 
            this.browserForwardButton.Image = ((System.Drawing.Image)(resources.GetObject("browserForwardButton.Image")));
            this.browserForwardButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.browserForwardButton.Name = "browserForwardButton";
            this.browserForwardButton.Size = new System.Drawing.Size(49, 27);
            this.browserForwardButton.Text = "前进";
            this.browserForwardButton.Click += new System.EventHandler(this.browserForwardButton_Click);
            // 
            // browserAddressLabel
            // 
            this.browserAddressLabel.Name = "browserAddressLabel";
            this.browserAddressLabel.Size = new System.Drawing.Size(29, 27);
            this.browserAddressLabel.Text = "地址";
            // 
            // browserAddressTextBox
            // 
            this.browserAddressTextBox.AutoSize = false;
            this.browserAddressTextBox.Name = "browserAddressTextBox";
            this.browserAddressTextBox.Size = new System.Drawing.Size(350, 21);
            this.browserAddressTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.browserAddressTextBox_KeyDown);
            // 
            // browserGoButton
            // 
            this.browserGoButton.Image = ((System.Drawing.Image)(resources.GetObject("browserGoButton.Image")));
            this.browserGoButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.browserGoButton.Name = "browserGoButton";
            this.browserGoButton.Size = new System.Drawing.Size(61, 27);
            this.browserGoButton.Text = "确定  ";
            this.browserGoButton.Click += new System.EventHandler(this.browserGoButton_Click);
            // 
            // browserStopButton
            // 
            this.browserStopButton.Image = ((System.Drawing.Image)(resources.GetObject("browserStopButton.Image")));
            this.browserStopButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.browserStopButton.Name = "browserStopButton";
            this.browserStopButton.Size = new System.Drawing.Size(61, 27);
            this.browserStopButton.Text = "停止  ";
            this.browserStopButton.Click += new System.EventHandler(this.browserStopButton_Click);
            // 
            // browserRefreshButton
            // 
            this.browserRefreshButton.Image = ((System.Drawing.Image)(resources.GetObject("browserRefreshButton.Image")));
            this.browserRefreshButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.browserRefreshButton.Name = "browserRefreshButton";
            this.browserRefreshButton.Size = new System.Drawing.Size(61, 27);
            this.browserRefreshButton.Text = "刷新  ";
            this.browserRefreshButton.Click += new System.EventHandler(this.browserRefreshButton_Click);
            // 
            // browserSeparator
            // 
            this.browserSeparator.Name = "browserSeparator";
            this.browserSeparator.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(61, 27);
            this.toolStripButton1.Text = "关于  ";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(61, 27);
            this.toolStripButton2.Text = "退出  ";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(203, 60);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(642, 98);
            this.webBrowser1.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.tabControl1.Location = new System.Drawing.Point(0, 30);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(200, 128);
            this.tabControl1.TabIndex = 79;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.zaSuiteTreeView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 21);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(192, 103);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "业务功能";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // zaSuiteTreeView1
            // 
            this.zaSuiteTreeView1.BackColor = System.Drawing.SystemColors.Window;
            this.zaSuiteTreeView1.ContextMenuStrip = this.contextMenuStrip1;
            this.zaSuiteTreeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zaSuiteTreeView1.ImageIndex = 0;
            this.zaSuiteTreeView1.ImageList = this.imageList;
            this.zaSuiteTreeView1.Location = new System.Drawing.Point(3, 3);
            this.zaSuiteTreeView1.Name = "zaSuiteTreeView1";
            this.zaSuiteTreeView1.SelectedImageIndex = 1;
            this.zaSuiteTreeView1.Size = new System.Drawing.Size(186, 97);
            this.zaSuiteTreeView1.StateImageList = this.imageList;
            this.zaSuiteTreeView1.TabIndex = 79;
            this.zaSuiteTreeView1.ZADataTable = null;
            this.zaSuiteTreeView1.ZADisplayFieldName = "";
            this.zaSuiteTreeView1.ZAKeyFieldName = "";
            this.zaSuiteTreeView1.ZAParentFieldName = "";
            this.zaSuiteTreeView1.ZAToolTipTextName = "";
            this.zaSuiteTreeView1.ZATreeViewRootValue = "";
            this.zaSuiteTreeView1.DoubleClick += new System.EventHandler(this.zaSuiteTreeView1_DoubleClick);
            this.zaSuiteTreeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.zaSuiteTreeView1_AfterSelect);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.打开ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(95, 26);
            // 
            // 打开ToolStripMenuItem
            // 
            this.打开ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("打开ToolStripMenuItem.Image")));
            this.打开ToolStripMenuItem.Name = "打开ToolStripMenuItem";
            this.打开ToolStripMenuItem.Size = new System.Drawing.Size(94, 22);
            this.打开ToolStripMenuItem.Text = "打开";
            this.打开ToolStripMenuItem.Click += new System.EventHandler(this.打开ToolStripMenuItem_Click);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "1.gif");
            this.imageList.Images.SetKeyName(1, "2.gif");
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(200, 30);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 128);
            this.splitter1.TabIndex = 80;
            this.splitter1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelCurrUrl);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(203, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(642, 30);
            this.panel1.TabIndex = 82;
            // 
            // labelCurrUrl
            // 
            this.labelCurrUrl.AutoSize = true;
            this.labelCurrUrl.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelCurrUrl.ForeColor = System.Drawing.Color.Blue;
            this.labelCurrUrl.Location = new System.Drawing.Point(4, 9);
            this.labelCurrUrl.Name = "labelCurrUrl";
            this.labelCurrUrl.Size = new System.Drawing.Size(57, 12);
            this.labelCurrUrl.TabIndex = 82;
            this.labelCurrUrl.Text = "当前路径";
            // 
            // HelpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(845, 158);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.simpleBrowserToolStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "HelpForm";
            this.Text = "帮助";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.HelpForm_Load);
            this.simpleBrowserToolStrip.ResumeLayout(false);
            this.simpleBrowserToolStrip.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStrip simpleBrowserToolStrip;
        private System.Windows.Forms.ToolStripButton browserBackButton;
        private System.Windows.Forms.ToolStripButton browserForwardButton;
        private System.Windows.Forms.ToolStripButton browserStopButton;
        private System.Windows.Forms.ToolStripButton browserRefreshButton;
        private System.Windows.Forms.ToolStripSeparator browserSeparator;
        private System.Windows.Forms.ToolStripLabel browserAddressLabel;
        private System.Windows.Forms.ToolStripTextBox browserAddressTextBox;
        private System.Windows.Forms.ToolStripButton browserGoButton;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private ww.wwf.control.WWControlLib.WWTreeView zaSuiteTreeView1;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelCurrUrl;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 打开ToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
    }
}