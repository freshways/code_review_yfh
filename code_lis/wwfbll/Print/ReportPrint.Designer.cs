namespace ww.wwf.wwfbll.Print
{
    partial class ReportPrint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportPrint));
            this.dnReport = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolExport = new System.Windows.Forms.ToolStripSplitButton();
            this.toolExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.导出PDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tspbZoom = new System.Windows.Forms.ToolStripSplitButton();
            this.tool25 = new System.Windows.Forms.ToolStripMenuItem();
            this.tool50 = new System.Windows.Forms.ToolStripMenuItem();
            this.tool100 = new System.Windows.Forms.ToolStripMenuItem();
            this.tool200 = new System.Windows.Forms.ToolStripMenuItem();
            this.tool400 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolWhole = new System.Windows.Forms.ToolStripMenuItem();
            this.toolPageWidth = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.tspbNavigation = new System.Windows.Forms.ToolStripSplitButton();
            this.toolFirst = new System.Windows.Forms.ToolStripMenuItem();
            this.toolLast = new System.Windows.Forms.ToolStripMenuItem();
            this.toolPrevious = new System.Windows.Forms.ToolStripMenuItem();
            this.toolNext = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolJump = new System.Windows.Forms.ToolStripMenuItem();
            this.txtJump = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSplitButton1 = new System.Windows.Forms.ToolStripSplitButton();
            this.刷新ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ssToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.回退ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAbout = new System.Windows.Forms.ToolStripButton();
            this.rptViewer = new Microsoft.Reporting.WinForms.ReportViewer();
            this.PreviewDialog = new System.Windows.Forms.PrintPreviewDialog();
            this.saveFileDialogDC = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dnReport)).BeginInit();
            this.dnReport.SuspendLayout();
            this.SuspendLayout();
            // 
            // dnReport
            // 
            this.dnReport.AddNewItem = null;
            this.dnReport.AutoSize = false;
            this.dnReport.CountItem = null;
            this.dnReport.DeleteItem = null;
            this.dnReport.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton2,
            this.toolStripSeparator12,
            this.toolStripButton3,
            this.toolStripSeparator13,
            this.toolStripButton4,
            this.toolStripSeparator7,
            this.toolExport,
            this.toolStripSeparator5,
            this.tspbZoom,
            this.toolStripSeparator8,
            this.tspbNavigation,
            this.toolStripSeparator10,
            this.toolStripSplitButton1,
            this.toolStripSeparator4,
            this.toolStripButton1,
            this.toolStripSeparator1,
            this.toolStripButtonAbout});
            this.dnReport.Location = new System.Drawing.Point(0, 0);
            this.dnReport.MoveFirstItem = null;
            this.dnReport.MoveLastItem = null;
            this.dnReport.MoveNextItem = null;
            this.dnReport.MovePreviousItem = null;
            this.dnReport.Name = "dnReport";
            this.dnReport.PositionItem = null;
            this.dnReport.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.dnReport.Size = new System.Drawing.Size(382, 30);
            this.dnReport.TabIndex = 0;
            this.dnReport.Text = "bindingNavigator1";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(63, 27);
            this.toolStripButton2.Text = "页面设置";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(63, 27);
            this.toolStripButton3.Text = "打印预览";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(51, 27);
            this.toolStripButton4.Text = "打 印 ";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 30);
            // 
            // toolExport
            // 
            this.toolExport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolExcel,
            this.导出PDFToolStripMenuItem});
            this.toolExport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolExport.Name = "toolExport";
            this.toolExport.Size = new System.Drawing.Size(63, 27);
            this.toolExport.Text = "导 出 ";
            // 
            // toolExcel
            // 
            this.toolExcel.Name = "toolExcel";
            this.toolExcel.Size = new System.Drawing.Size(135, 22);
            this.toolExcel.Text = "导出Excel";
            this.toolExcel.Click += new System.EventHandler(this.toolExcel_Click);
            // 
            // 导出PDFToolStripMenuItem
            // 
            this.导出PDFToolStripMenuItem.Name = "导出PDFToolStripMenuItem";
            this.导出PDFToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.导出PDFToolStripMenuItem.Text = "导出PDF";
            this.导出PDFToolStripMenuItem.Click += new System.EventHandler(this.导出PDFToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 30);
            // 
            // tspbZoom
            // 
            this.tspbZoom.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tspbZoom.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tool25,
            this.tool50,
            this.tool100,
            this.tool200,
            this.tool400,
            this.toolWhole,
            this.toolPageWidth});
            this.tspbZoom.Image = ((System.Drawing.Image)(resources.GetObject("tspbZoom.Image")));
            this.tspbZoom.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tspbZoom.Name = "tspbZoom";
            this.tspbZoom.Size = new System.Drawing.Size(82, 27);
            this.tspbZoom.Text = "显示比例 ";
            // 
            // tool25
            // 
            this.tool25.Name = "tool25";
            this.tool25.Size = new System.Drawing.Size(102, 22);
            this.tool25.Text = "25%";
            this.tool25.Click += new System.EventHandler(this.tool25_Click);
            // 
            // tool50
            // 
            this.tool50.Name = "tool50";
            this.tool50.Size = new System.Drawing.Size(102, 22);
            this.tool50.Text = "50%";
            this.tool50.Click += new System.EventHandler(this.tool50_Click);
            // 
            // tool100
            // 
            this.tool100.Name = "tool100";
            this.tool100.Size = new System.Drawing.Size(102, 22);
            this.tool100.Text = "100%";
            this.tool100.Click += new System.EventHandler(this.tool100_Click);
            // 
            // tool200
            // 
            this.tool200.Name = "tool200";
            this.tool200.Size = new System.Drawing.Size(102, 22);
            this.tool200.Text = "200%";
            this.tool200.Click += new System.EventHandler(this.tool200_Click);
            // 
            // tool400
            // 
            this.tool400.Name = "tool400";
            this.tool400.Size = new System.Drawing.Size(102, 22);
            this.tool400.Text = "400%";
            this.tool400.Click += new System.EventHandler(this.tool400_Click);
            // 
            // toolWhole
            // 
            this.toolWhole.Name = "toolWhole";
            this.toolWhole.Size = new System.Drawing.Size(102, 22);
            this.toolWhole.Text = "整页";
            this.toolWhole.Click += new System.EventHandler(this.toolWhole_Click);
            // 
            // toolPageWidth
            // 
            this.toolPageWidth.Name = "toolPageWidth";
            this.toolPageWidth.Size = new System.Drawing.Size(102, 22);
            this.toolPageWidth.Text = "页宽";
            this.toolPageWidth.Click += new System.EventHandler(this.toolPageWidth_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 30);
            // 
            // tspbNavigation
            // 
            this.tspbNavigation.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tspbNavigation.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolFirst,
            this.toolLast,
            this.toolPrevious,
            this.toolNext,
            this.toolStripSeparator2,
            this.toolJump,
            this.txtJump});
            this.tspbNavigation.Image = ((System.Drawing.Image)(resources.GetObject("tspbNavigation.Image")));
            this.tspbNavigation.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tspbNavigation.Name = "tspbNavigation";
            this.tspbNavigation.Size = new System.Drawing.Size(63, 27);
            this.tspbNavigation.Text = "导 航 ";
            // 
            // toolFirst
            // 
            this.toolFirst.Name = "toolFirst";
            this.toolFirst.Size = new System.Drawing.Size(165, 22);
            this.toolFirst.Text = "第一页";
            this.toolFirst.Click += new System.EventHandler(this.toolFirst_Click);
            // 
            // toolLast
            // 
            this.toolLast.Name = "toolLast";
            this.toolLast.Size = new System.Drawing.Size(165, 22);
            this.toolLast.Text = "最后一页";
            this.toolLast.Click += new System.EventHandler(this.toolLast_Click);
            // 
            // toolPrevious
            // 
            this.toolPrevious.Name = "toolPrevious";
            this.toolPrevious.Size = new System.Drawing.Size(165, 22);
            this.toolPrevious.Text = "上一页";
            this.toolPrevious.Click += new System.EventHandler(this.toolPrevious_Click);
            // 
            // toolNext
            // 
            this.toolNext.Name = "toolNext";
            this.toolNext.Size = new System.Drawing.Size(165, 22);
            this.toolNext.Text = "下一页";
            this.toolNext.Click += new System.EventHandler(this.toolNext_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(162, 6);
            // 
            // toolJump
            // 
            this.toolJump.Name = "toolJump";
            this.toolJump.Size = new System.Drawing.Size(165, 22);
            this.toolJump.Text = "跳转到指定页：";
            this.toolJump.Click += new System.EventHandler(this.toolJump_Click);
            // 
            // txtJump
            // 
            this.txtJump.Name = "txtJump";
            this.txtJump.Size = new System.Drawing.Size(100, 22);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripSplitButton1
            // 
            this.toolStripSplitButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.刷新ToolStripMenuItem,
            this.ssToolStripMenuItem,
            this.回退ToolStripMenuItem,
            this.toolStripSeparator3});
            this.toolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton1.Name = "toolStripSplitButton1";
            this.toolStripSplitButton1.Size = new System.Drawing.Size(63, 27);
            this.toolStripSplitButton1.Text = "其 它 ";
            // 
            // 刷新ToolStripMenuItem
            // 
            this.刷新ToolStripMenuItem.Name = "刷新ToolStripMenuItem";
            this.刷新ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.刷新ToolStripMenuItem.Text = "刷新";
            this.刷新ToolStripMenuItem.Click += new System.EventHandler(this.刷新ToolStripMenuItem_Click);
            // 
            // ssToolStripMenuItem
            // 
            this.ssToolStripMenuItem.Name = "ssToolStripMenuItem";
            this.ssToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.ssToolStripMenuItem.Text = "停止";
            this.ssToolStripMenuItem.Click += new System.EventHandler(this.ssToolStripMenuItem_Click);
            // 
            // 回退ToolStripMenuItem
            // 
            this.回退ToolStripMenuItem.Name = "回退ToolStripMenuItem";
            this.回退ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.回退ToolStripMenuItem.Text = "回退";
            this.回退ToolStripMenuItem.Click += new System.EventHandler(this.回退ToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(97, 6);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(51, 27);
            this.toolStripButton1.Text = "关 闭 ";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonAbout
            // 
            this.toolStripButtonAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAbout.Name = "toolStripButtonAbout";
            this.toolStripButtonAbout.Size = new System.Drawing.Size(58, 27);
            this.toolStripButtonAbout.Text = "关 于  ";
            this.toolStripButtonAbout.Click += new System.EventHandler(this.toolStripButtonAbout_Click);
            // 
            // rptViewer
            // 
            this.rptViewer.AutoScroll = true;
            this.rptViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptViewer.DocumentMapCollapsed = true;
            this.rptViewer.LocalReport.ReportEmbeddedResource = "RDLCPrint.rptWuLiao.rdlc";
            this.rptViewer.Location = new System.Drawing.Point(0, 30);
            this.rptViewer.Name = "rptViewer";
            this.rptViewer.ShowToolBar = false;
            this.rptViewer.Size = new System.Drawing.Size(382, 127);
            this.rptViewer.TabIndex = 1;
            this.rptViewer.Drillthrough += new Microsoft.Reporting.WinForms.DrillthroughEventHandler(this.rptViewer_Drillthrough);
            // 
            // PreviewDialog
            // 
            this.PreviewDialog.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.PreviewDialog.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.PreviewDialog.ClientSize = new System.Drawing.Size(396, 296);
            this.PreviewDialog.Enabled = true;
            this.PreviewDialog.Icon = ((System.Drawing.Icon)(resources.GetObject("PreviewDialog.Icon")));
            this.PreviewDialog.Name = "printPreviewDialog1";
            this.PreviewDialog.Visible = false;
            // 
            // saveFileDialogDC
            // 
            this.saveFileDialogDC.DefaultExt = "xls";
            this.saveFileDialogDC.Filter = "|*.xls|*.pdf|";
            // 
            // ReportPrint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 157);
            this.Controls.Add(this.rptViewer);
            this.Controls.Add(this.dnReport);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ReportPrint";
            this.ShowIcon = false;
            this.Text = "打印";
            this.Load += new System.EventHandler(this.ReportViewer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dnReport)).EndInit();
            this.dnReport.ResumeLayout(false);
            this.dnReport.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingNavigator dnReport;
        private Microsoft.Reporting.WinForms.ReportViewer rptViewer;
        private System.Windows.Forms.PrintPreviewDialog PreviewDialog;
        private System.Windows.Forms.ToolStripSplitButton toolExport;
        private System.Windows.Forms.ToolStripMenuItem toolExcel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSplitButton tspbNavigation;
        private System.Windows.Forms.ToolStripMenuItem toolFirst;
        private System.Windows.Forms.ToolStripMenuItem toolLast;
        private System.Windows.Forms.ToolStripMenuItem toolPrevious;
        private System.Windows.Forms.ToolStripMenuItem toolNext;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem toolJump;
        private System.Windows.Forms.ToolStripTextBox txtJump;
        private System.Windows.Forms.ToolStripSplitButton tspbZoom;
        private System.Windows.Forms.ToolStripMenuItem tool25;
        private System.Windows.Forms.ToolStripMenuItem tool50;
        private System.Windows.Forms.ToolStripMenuItem tool100;
        private System.Windows.Forms.ToolStripMenuItem tool200;
        private System.Windows.Forms.ToolStripMenuItem tool400;
        private System.Windows.Forms.ToolStripMenuItem toolWhole;
        private System.Windows.Forms.ToolStripMenuItem toolPageWidth;
        private System.Windows.Forms.ToolStripMenuItem 导出PDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton1;
        private System.Windows.Forms.ToolStripMenuItem 刷新ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ssToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 回退ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonAbout;
        private System.Windows.Forms.SaveFileDialog saveFileDialogDC;

    }
}