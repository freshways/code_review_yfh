﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.com.Hardware;

namespace ww.wwf.wwfbll
{
    public partial class HardwareInfoForm : Form
    {
        //Local Connection

      static  Connection wmiConnection = new Connection();
        //Remote Connection
        //Connection wmiConnection = new Connection("neal.bailey",
        //                                          "3l!t3p@$$",
        //                                          "BAILEYSOFT",
        //                                          "192.168.2.100");

        //All the Objects
      /*
       // 硬件 
Win32_Processor, // CPU 处理器 
Win32_PhysicalMemory, // 物理内存条 
Win32_Keyboard, // 键盘 
Win32_PointingDevice, // 点输入设备，包括鼠标。 
Win32_FloppyDrive, // 软盘驱动器 
Win32_DiskDrive, // 硬盘驱动器 
Win32_CDROMDrive, // 光盘驱动器 
Win32_BaseBoard, // 主板 
Win32_BIOS, // BIOS 芯片 
Win32_ParallelPort, // 并口 
Win32_SerialPort, // 串口 
Win32_SerialPortConfiguration, // 串口配置 
Win32_SoundDevice, // 多媒体设置，一般指声卡。 
Win32_SystemSlot, // 主板插槽 (ISA & PCI & AGP) 
Win32_USBController, // USB 控制器 
Win32_NetworkAdapter, // 网络适配器 
Win32_NetworkAdapterConfiguration, // 网络适配器设置 
Win32_Printer, // 打印机 
Win32_PrinterConfiguration, // 打印机设置 
Win32_PrintJob, // 打印机任务 
Win32_TCPIPPrinterPort, // 打印机端口 
Win32_POTSModem, // MODEM 
Win32_POTSModemToSerialPort, // MODEM 端口 
Win32_DesktopMonitor, // 显示器 
Win32_DisplayConfiguration, // 显卡 
Win32_DisplayControllerConfiguration, // 显卡设置 
Win32_VideoController, // 显卡细节。 
Win32_VideoSettings, // 显卡支持的显示模式。 

// 操作系统 
Win32_TimeZone, // 时区 
Win32_SystemDriver, // 驱动程序 
Win32_DiskPartition, // 磁盘分区 
Win32_LogicalDisk, // 逻辑磁盘 
Win32_LogicalDiskToPartition, // 逻辑磁盘所在分区及始末位置。 
Win32_LogicalMemoryConfiguration, // 逻辑内存配置 
Win32_PageFile, // 系统页文件信息 
Win32_PageFileSetting, // 页文件设置 
Win32_BootConfiguration, // 系统启动配置 
Win32_ComputerSystem, // 计算机信息简要 
Win32_OperatingSystem, // 操作系统信息 
Win32_StartupCommand, // 系统自动启动程序 
Win32_Service, // 系统安装的服务 
Win32_Group, // 系统管理组 
Win32_GroupUser, // 系统组帐号 
Win32_UserAccount, // 用户帐号 
Win32_Process, // 系统进程 
Win32_Thread, // 系统线程 
Win32_Share, // 共享 
Win32_NetworkClient, // 已安装的网络客户端 
Win32_NetworkProtocol, // 已安装的网络协议 
} 
       */
        /// <summary>
      /// 主板 
        /// </summary>
        Win32_BaseBoard a = new Win32_BaseBoard(wmiConnection);
        /// <summary>
        /// 电池
        /// </summary>
        Win32_Battery b = new Win32_Battery(wmiConnection);
        /// <summary>
        /// BIOS 芯片 
        /// </summary>
        Win32_BIOS c = new Win32_BIOS(wmiConnection);
        /// <summary>
        /// 总线
        /// </summary>
        Win32_Bus d = new Win32_Bus(wmiConnection);
        /// <summary>
        /// 光盘驱动器 
        /// </summary>
        Win32_CDROMDrive ea = new Win32_CDROMDrive(wmiConnection);
        /// <summary>
        /// 硬盘驱动器 
        /// </summary>
        Win32_DiskDrive f = new Win32_DiskDrive(wmiConnection);
        /// <summary>
        /// 直接存储存取通道
        /// </summary>
        Win32_DMAChannel g = new Win32_DMAChannel(wmiConnection);
        /// <summary>
        /// 风扇
        /// </summary>
        Win32_Fan h = new Win32_Fan(wmiConnection);
        /// <summary>
        /// 软盘控制器
        /// </summary>
        Win32_FloppyController i = new Win32_FloppyController(wmiConnection);
        /// <summary>
        /// 软盘驱动器 
        /// </summary>
        Win32_FloppyDrive j = new Win32_FloppyDrive(wmiConnection);
        /// <summary>
        /// 集成器件
        /// </summary>
        Win32_IDEController k = new Win32_IDEController(wmiConnection);
        /// <summary>
        /// 中断资源
        /// </summary>
        Win32_IRQResource l = new Win32_IRQResource(wmiConnection);
        /// <summary>
        /// 键盘
        /// </summary>
        Win32_Keyboard m = new Win32_Keyboard(wmiConnection);
        /// <summary>
        /// 内存
        /// </summary>
        Win32_MemoryDevice n = new Win32_MemoryDevice(wmiConnection);
        /// <summary>
        /// 网络适配器 
        /// </summary>
        Win32_NetworkAdapter o = new Win32_NetworkAdapter(wmiConnection);
        /// <summary>
        /// 网络适配器设置 
        /// </summary>
        Win32_NetworkAdapterConfiguration p = new Win32_NetworkAdapterConfiguration(wmiConnection);
        /// <summary>
        /// 机载
        /// </summary>
        Win32_OnBoardDevice q = new Win32_OnBoardDevice(wmiConnection);
        /// <summary>
        /// 并口
        /// </summary>
        Win32_ParallelPort r = new Win32_ParallelPort(wmiConnection);
        /// <summary>
        /// 通信接口
        /// </summary>
        Win32_PCMCIController s = new Win32_PCMCIController(wmiConnection);
        /// <summary>
        /// 物理媒体
        /// </summary>
        Win32_PhysicalMedia t = new Win32_PhysicalMedia(wmiConnection);
        /// <summary>
        /// 物理内存条 
        /// </summary>
        Win32_PhysicalMemory u = new Win32_PhysicalMemory(wmiConnection);
        /// <summary>
        /// 端口地址
        /// </summary>
        Win32_PortConnector v = new Win32_PortConnector(wmiConnection);
        /// <summary>
        /// 端口资源
        /// </summary>
        Win32_PortResource w = new Win32_PortResource(wmiConnection);
        /// <summary>
        /// MODEM 
        /// </summary>
        Win32_POTSModem x = new Win32_POTSModem(wmiConnection);
        /// <summary>
        ///  CPU 处理器 
        /// </summary>
        Win32_Processor y = new Win32_Processor(wmiConnection);
        /// <summary>
        /// 小型计算机系统接口
        /// </summary>
        Win32_SCSIController z = new Win32_SCSIController(wmiConnection);
        /// <summary>
        /// 串口 
        /// </summary>
        Win32_SerialPort aa = new Win32_SerialPort(wmiConnection);
        /// <summary>
        ///  串口配置 
        /// </summary>
        Win32_SerialPortConfiguration bb = new Win32_SerialPortConfiguration(wmiConnection);
        /// <summary>
        ///  多媒体设置，一般指声卡。 
        /// </summary>
        Win32_SoundDevice cc = new Win32_SoundDevice(wmiConnection);
        /// <summary>
        /// 系统
        /// </summary>
        Win32_SystemEnclosure dd = new Win32_SystemEnclosure(wmiConnection);
        /// <summary>
        /// 磁带
        /// </summary>
        Win32_TapeDrive ee = new Win32_TapeDrive(wmiConnection);
        /// <summary>
        /// 温度
        /// </summary>
        Win32_TemperatureProbe ff = new Win32_TemperatureProbe(wmiConnection);
        /// <summary>
        /// UPS
        /// </summary>
        Win32_UninterruptiblePowerSupply gg = new Win32_UninterruptiblePowerSupply(wmiConnection);
        /// <summary>
        /// USB 控制器 
        /// </summary>
        Win32_USBController hh = new Win32_USBController(wmiConnection);
        /// <summary>
        /// 通用串行总结
        /// </summary>
        Win32_USBHub ii = new Win32_USBHub(wmiConnection);
        /// <summary>
        /// 显卡细节
        /// </summary>
        Win32_VideoController jj = new Win32_VideoController(wmiConnection);
        /// <summary>
        /// 电压
        /// </summary>
        Win32_VoltageProbe kk = new Win32_VoltageProbe(wmiConnection);

        public HardwareInfoForm()
        {
            InitializeComponent();
        }

        private void HardwareInfoForm_Load(object sender, EventArgs e)
        {
            try
            {               
                //Loop all the properties                
                richTextBoxInfo.AppendText("------| 主板 |------");
                foreach (string property in a.GetPropertyValues())
                {
                     richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);                   
                }
                richTextBoxInfo.AppendText("\n\n");
              

                richTextBoxInfo.AppendText("------| 电池 |------");
                foreach (string property in b.GetPropertyValues())
                {
                     richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");

                 richTextBoxInfo.AppendText("------| BIOS 芯片  |------");
                foreach (string property in c.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| 总线 |------");
                foreach (string property in d.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                 richTextBoxInfo.AppendText("------| 光盘驱动器  |------");
                foreach (string property in ea.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                 richTextBoxInfo.AppendText("------| 硬盘驱动器  |------");
                foreach (string property in f.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                 richTextBoxInfo.AppendText("------| 直接存储存取通道 |------");
                foreach (string property in g.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                 richTextBoxInfo.AppendText("------| 风扇 |------");
                foreach (string property in h.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                 richTextBoxInfo.AppendText("------| 软盘控制器 |------");
                foreach (string property in i.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                 richTextBoxInfo.AppendText("------| 软盘驱动 |------");
                foreach (string property in j.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| 集成器件 |------");
                foreach (string property in k.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| 中断 |------");
                foreach (string property in l.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                richTextBoxInfo.AppendText("\n\n");

                richTextBoxInfo.AppendText("------| 磁盘 |------");
                foreach (string property in m.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| 内存 |------");
                foreach (string property in n.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                 richTextBoxInfo.AppendText("------| 网络适配器  |------");
                foreach (string property in o.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                 richTextBoxInfo.AppendText("------| 网络适配器设置  |------");
                foreach (string property in p.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| 机载 |------");
                foreach (string property in q.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| 并口 |------");
                foreach (string property in r.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| 通信接口 |------");
                foreach (string property in s.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| 媒体 |------");
                foreach (string property in t.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| 内存 |------");
                foreach (string property in u.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| 端口地址 |------");
                foreach (string property in v.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| 端口 |------");
                foreach (string property in w.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| MODEM |------");
                foreach (string property in x.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| CPU |------");
                foreach (string property in y.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| 系统接口 |------");
                foreach (string property in z.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| 串口 |------");
                foreach (string property in aa.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| 串口配置 |------");
                foreach (string property in bb.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| 声卡 |------");
                foreach (string property in cc.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| 系统 |------");
                foreach (string property in dd.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| 磁带 |------");
                foreach (string property in ee.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| 温度 |------");
                foreach (string property in ff.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| UPS |------");
                foreach (string property in gg.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| USB |------");
                foreach (string property in hh.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| 通信串行 |------");
                foreach (string property in ii.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------| 显卡 |------");
                foreach (string property in jj.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
                 richTextBoxInfo.AppendText("\n\n");
                richTextBoxInfo.AppendText("------|  电压  |------");
                foreach (string property in kk.GetPropertyValues())
                {
                    richTextBoxInfo.AppendText("\n");
                    richTextBoxInfo.AppendText(property);
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
    }
}