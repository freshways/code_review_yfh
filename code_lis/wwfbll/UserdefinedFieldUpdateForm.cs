﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;

namespace ww.wwf.wwfbll
{
    public partial class UserdefinedFieldUpdateForm : Form
    {
        UserdefinedBLL bll = new UserdefinedBLL();
        protected string strTableID = "";//表ID
        DataTable dtCurr = null;
        //DataTable dtColumn = new DataTable();
        public UserdefinedFieldUpdateForm(string tableid)
        {
            dtCurr = new DataTable();
            strTableID = tableid;
            InitializeComponent();
            GetFieldDT();
        }
        private void UserdefinedFieldUpdateForm_Load(object sender, EventArgs e)
        {
            // GetFieldDT();
            dataGridView1.AutoGenerateColumns = false;
            wwfcolumnsBindingSource.DataSource = dtCurr;
            dataGridView1.DataSource = wwfcolumnsBindingSource;
            //dtColumn = bll.BllColumnsByftable_id(tableid);
        }
        public void DataGridViewSetStyleNew(DataGridView dgvShow)
        {
            try
            {
                DataRow[] colList_OK = dtCurr.Select("fshow_flag>=0", "forder_by");
                int intOrder = 0;
                int fread_flage = 0;
                if (colList_OK.Length > 0)
                {
                    foreach (DataRow drC in colList_OK)
                    {
                        if (drC["fread_flage"].ToString() != null || drC["fread_flage"].ToString() != "")
                        {
                            fread_flage = Convert.ToInt32(drC["fread_flage"].ToString());                            
                        }
                        if (drC["forder_by"].ToString() != null || drC["forder_by"].ToString() != "")
                        {
                            intOrder = Convert.ToInt32(drC["forder_by"].ToString());
                            if (intOrder >= colList_OK.Length)
                                intOrder = colList_OK.Length - 1;
                        }
                        else
                            intOrder = colList_OK.Length - 1;

                        DataGridViewTextBoxColumn newCol = new DataGridViewTextBoxColumn();
                        string strCName_OK = drC["fcode"].ToString();
                        string strfvalue_type = drC["fvalue_type"].ToString();
                        int intfshow_flag = Convert.ToInt32(drC["fshow_flag"].ToString());
                        if (strfvalue_type == "预定义")
                        {
                            if (dgvShow.Columns[strCName_OK] == null)
                                continue;
                            dgvShow.Columns[strCName_OK].Width = Convert.ToInt32(drC["fshow_width"].ToString());
                          
                            dgvShow.Columns[strCName_OK].DisplayIndex = intOrder;
                            dgvShow.Columns[strCName_OK].HeaderText = drC["fname"].ToString();
                            if (fread_flage == 0)
                                dgvShow.Columns[strCName_OK].ReadOnly = false;
                            else
                                dgvShow.Columns[strCName_OK].ReadOnly = true;
                        }                    
                        else
                        {
                            newCol.DataPropertyName = strCName_OK;
                            newCol.Name = strCName_OK;
                            if (drC["fshow_width"].ToString() != "" || drC["fshow_width"].ToString() != null)
                                newCol.Width = Convert.ToInt32(drC["fshow_width"].ToString());
                            if (drC["fname"].ToString() != null || drC["fname"].ToString() != "")
                                newCol.HeaderText = drC["fname"].ToString();
                            newCol.DisplayIndex = intOrder;

                            if (fread_flage == 0)
                                newCol.ReadOnly = false;
                            else
                                newCol.ReadOnly = true;

                            dgvShow.Columns.AddRange(new DataGridViewColumn[] { newCol });


                        }
                        if ((intfshow_flag == 0)||(strfvalue_type == "主键"))   
                            dgvShow.Columns[strCName_OK].Visible = false;
                        else
                            dgvShow.Columns[strCName_OK].Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
       /// <summary>
        /// DataGridView 列表风格设置
       /// </summary>
        /// <param name="dgvShow">DataGridView</param>
        public void DataGridViewSetStyle(DataGridView dgvShow)
        {

                     
              for (int i = 0; i < dgvShow.ColumnCount; i++)
              {
                  string strCurrColName = dgvShow.Columns[i].Name.ToString();

                  int fshow_width = 100;//宽度
                  string fshow_flag = "1";//显示标识    
                  int forder_by = 0;//排序
                  string fname = "";//列名
                  dgvShow.Columns[strCurrColName].Visible = false;
                  dgvShow.Columns[strCurrColName].Width = 100;

                  dgvShow.Columns[strCurrColName].DisplayIndex = dgvShow.ColumnCount-1;

                  DataRow[] colList = dtCurr.Select("fcode='" + strCurrColName + "' and fshow_flag=1");
                  if (colList.Length > 0)
                  {
                      foreach (DataRow drC in colList)
                      {

                          //fshow_width = drC["fshow_width"].ToString();
                          fshow_flag = drC["fshow_flag"].ToString();
                          fname = drC["fname"].ToString();


                          if (drC["fshow_width"].ToString() == "" || drC["fshow_width"].ToString() == null)
                          {
                              fshow_width = 100;
                          }
                          else
                          {
                              fshow_width = Convert.ToInt32(drC["fshow_width"].ToString());
                          }
                          if (fshow_flag == "" || fshow_flag == null)
                          {
                              fshow_flag = "1";
                          }
                          if (drC["forder_by"].ToString() == "" || drC["forder_by"].ToString() == null)
                          {
                              forder_by = 0;
                          }
                          else
                          {
                              forder_by = Convert.ToInt32(drC["forder_by"].ToString());
                          }
                          if (fshow_flag == "1")
                          {
                              dgvShow.Columns[strCurrColName].Visible = true;
                              dgvShow.Columns[strCurrColName].Width = fshow_width;
                              dgvShow.Columns[strCurrColName].HeaderText = fname;
                              if (dgvShow.Columns.Count > forder_by)
                                dgvShow.Columns[strCurrColName].DisplayIndex = forder_by;
                          }
                          //  WWMessage.MessageShowWarning(strCurrColName + " " + forder_by.ToString());
                         // intShowOrdery = intShowOrdery + 1;
                      }
                  }

              }
        }
       
        private void GetFieldDT()
        {
            try
            {
                dtCurr = this.bll.BllColumnsByftable_id(this.strTableID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
        }
        private void buttonNO_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.wwfcolumnsBindingSource.EndEdit();
                for (int i = 0; i < this.dtCurr.Rows.Count; i++)
                {
                    this.bll.BllColumnsUpdate(this.dtCurr.Rows[i]);
                }
                //GetFieldDT();
                this.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
        }
        

        private void buttonRef_Click(object sender, EventArgs e)
        {
            GetFieldDT();
        }
              
    }
}