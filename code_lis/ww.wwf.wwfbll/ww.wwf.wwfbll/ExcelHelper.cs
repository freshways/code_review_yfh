using Excel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ww.wwf.wwfbll
{
	public class ExcelHelper
	{
		protected string templetFile = null;

		protected string outputFile = null;

		protected object missing = Missing.Value;

		private string strTemExcelFileName = "";

		public ExcelHelper(string templetFilePath, string outputFilePath)
		{
			this.strTemExcelFileName = Environment.CurrentDirectory + "\\TemExcel.xls";
			if (outputFilePath == null)
			{
				throw new Exception("输出Excel文件路径不能为空！");
			}
			this.outputFile = outputFilePath;
			if (File.Exists(templetFilePath))
			{
				this.templetFile = templetFilePath;
			}
			else if (File.Exists(this.strTemExcelFileName))
			{
				this.templetFile = this.strTemExcelFileName;
			}
			else
			{
				this.CreateExcelFile();
				this.templetFile = this.strTemExcelFileName;
			}
		}

		private void CreateExcelFile()
		{
			Excel.Application application = new ApplicationClass();
			try
			{
				application.Visible = false;
				Workbook workbook = application.Workbooks.Add(true);
				Worksheet worksheet = workbook.Worksheets[1] as Worksheet;
				application.DisplayAlerts = false;
				application.AlertBeforeOverwriting = false;
				workbook.Save();
				application.Save(this.strTemExcelFileName);
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message.ToString());
			}
			finally
			{
				application.Quit();
				application = null;
			}
		}

		public void DataTableToExcel(System.Data.DataTable dt, int rows, int top, int left, string sheetPrefixName)
		{
			int count = dt.Rows.Count;
			int count2 = dt.Columns.Count;
			int sheetCount = this.GetSheetCount(count, rows);
			if (sheetPrefixName == null || sheetPrefixName.Trim() == "")
			{
				sheetPrefixName = "Sheet";
			}
			DateTime now = DateTime.Now;
			Excel.Application application = new ApplicationClass();
			application.Visible = true;
			DateTime now2 = DateTime.Now;
			Workbook workbook = application.Workbooks.Open(this.templetFile, this.missing, this.missing, this.missing, this.missing, this.missing, this.missing, this.missing, this.missing, this.missing, this.missing, this.missing, this.missing);
			Worksheet o = (Worksheet)workbook.Sheets.get_Item(1);
			for (int i = 1; i < sheetCount; i++)
			{
				((Worksheet)workbook.Worksheets.get_Item(i)).Copy(this.missing, workbook.Worksheets[i]);
			}
			for (int i = 1; i <= sheetCount; i++)
			{
				int num = (i - 1) * rows;
				int num2 = i * rows;
				if (i == sheetCount)
				{
					num2 = count;
				}
				Worksheet worksheet = (Worksheet)workbook.Worksheets.get_Item(i);
				worksheet.Name = sheetPrefixName + "-" + i.ToString();
				for (int j = 0; j < num2 - num; j++)
				{
					for (int k = 0; k < count2; k++)
					{
						worksheet.Cells[top + j, left + k] = dt.Rows[num + j][k].ToString();
					}
				}
			}
			try
			{
				workbook.SaveAs(this.outputFile, this.missing, this.missing, this.missing, this.missing, this.missing, XlSaveAsAccessMode.xlExclusive, this.missing, this.missing, this.missing, this.missing);
				workbook.Close(null, null, null);
				application.Workbooks.Close();
				application.Application.Quit();
				application.Quit();
				Marshal.ReleaseComObject(o);
				Marshal.ReleaseComObject(workbook);
				Marshal.ReleaseComObject(application);
				GC.Collect();
			}
			catch (Exception ex)
			{
				File.Delete(this.strTemExcelFileName);
				throw ex;
			}
			finally
			{
				Process[] processesByName = Process.GetProcessesByName("Excel");
				Process[] array = processesByName;
				for (int l = 0; l < array.Length; l++)
				{
					Process process = array[l];
					DateTime startTime = process.StartTime;
					if (startTime > now && startTime < now2)
					{
						process.Kill();
					}
				}
			}
		}

		private int GetSheetCount(int rowCount, int rows)
		{
			int num = rowCount % rows;
			int result;
			if (num == 0)
			{
				result = rowCount / rows;
			}
			else
			{
				result = Convert.ToInt32(rowCount / rows) + 1;
			}
			return result;
		}

		public void ArrayToExcel(string[,] arr, int rows, int top, int left, string sheetPrefixName)
		{
			int length = arr.GetLength(0);
			int length2 = arr.GetLength(1);
			int sheetCount = this.GetSheetCount(length, rows);
			if (sheetPrefixName == null || sheetPrefixName.Trim() == "")
			{
				sheetPrefixName = "Sheet";
			}
			DateTime now = DateTime.Now;
			Excel.Application application = new ApplicationClass();
			application.Visible = true;
			DateTime now2 = DateTime.Now;
			Workbook workbook = application.Workbooks.Open(this.templetFile, this.missing, this.missing, this.missing, this.missing, this.missing, this.missing, this.missing, this.missing, this.missing, this.missing, this.missing, this.missing);
			Worksheet o = (Worksheet)workbook.Sheets.get_Item(1);
			for (int i = 1; i < sheetCount; i++)
			{
				((Worksheet)workbook.Worksheets.get_Item(i)).Copy(this.missing, workbook.Worksheets[i]);
			}
			for (int i = 1; i <= sheetCount; i++)
			{
				int num = (i - 1) * rows;
				int num2 = i * rows;
				if (i == sheetCount)
				{
					num2 = length;
				}
				Worksheet worksheet = (Worksheet)workbook.Worksheets.get_Item(i);
				worksheet.Name = sheetPrefixName + "-" + i.ToString();
				for (int j = 0; j < num2 - num; j++)
				{
					for (int k = 0; k < length2; k++)
					{
						worksheet.Cells[top + j, left + k] = arr[num + j, k];
					}
				}
				Excel.TextBox textBox = (Excel.TextBox)worksheet.TextBoxes("txtAuthor");
				Excel.TextBox textBox2 = (Excel.TextBox)worksheet.TextBoxes("txtDate");
				Excel.TextBox textBox3 = (Excel.TextBox)worksheet.TextBoxes("txtVersion");
				textBox.Text = "lingyun_k";
				textBox2.Text = DateTime.Now.ToShortDateString();
				textBox3.Text = "1.0.0.0";
			}
			try
			{
				workbook.SaveAs(this.outputFile, this.missing, this.missing, this.missing, this.missing, this.missing, XlSaveAsAccessMode.xlExclusive, this.missing, this.missing, this.missing, this.missing);
				workbook.Close(null, null, null);
				application.Workbooks.Close();
				application.Application.Quit();
				application.Quit();
				Marshal.ReleaseComObject(o);
				Marshal.ReleaseComObject(workbook);
				Marshal.ReleaseComObject(application);
				GC.Collect();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				Process[] processesByName = Process.GetProcessesByName("Excel");
				Process[] array = processesByName;
				for (int l = 0; l < array.Length; l++)
				{
					Process process = array[l];
					DateTime startTime = process.StartTime;
					if (startTime > now && startTime < now2)
					{
						process.Kill();
					}
				}
			}
		}

		public string DbGuid()
		{
			string text = Guid.NewGuid().ToString();
			return text.Replace("-", "");
		}

		public void GridViewToExcel(DataGridView dgvExcel, string strftemplate, int intTop, int intLeft)
		{
			System.Data.DataTable dataTable = new System.Data.DataTable();
			System.Data.DataRow dataRow = dataTable.NewRow();
			for (int i = 0; i < dgvExcel.ColumnCount; i++)
			{
				if (dgvExcel.Columns[i].Visible)
				{
					string text = dgvExcel.Columns[i].HeaderText.ToString();
					System.Data.DataColumn column = new System.Data.DataColumn(text, typeof(string));
					dataTable.Columns.Add(column);
					dataRow[text] = text;
				}
			}
			dataTable.Rows.Add(dataRow);
			for (int j = 0; j < dgvExcel.Rows.Count; j++)
			{
				System.Data.DataRow dataRow2 = dataTable.NewRow();
				for (int k = 0; k < dgvExcel.ColumnCount; k++)
				{
					if (dgvExcel.Columns[k].Visible)
					{
						string columnName = dgvExcel.Columns[k].HeaderText.ToString();
						dataRow2[columnName] = dgvExcel.Rows[j].Cells[k].Value.ToString();
					}
				}
				dataTable.Rows.Add(dataRow2);
			}
			SaveFileDialog saveFileDialog = new SaveFileDialog();
			saveFileDialog.Filter = "XLS文件|*.xls|所有文件|*.*";
			saveFileDialog.FilterIndex = 1;
			saveFileDialog.RestoreDirectory = true;
			if (saveFileDialog.ShowDialog() == DialogResult.OK)
			{
				string fileName = saveFileDialog.FileName;
				ExcelHelper excelHelper = new ExcelHelper(strftemplate, fileName);
				excelHelper.DataTableToExcel(dataTable, dataTable.Rows.Count, intTop, intLeft, "页");
			}
		}

		public void GridViewToExcelShowNum(DataGridView dgvExcel, string strftemplate, int intTop, int intLeft, string strDate)
		{
			System.Data.DataTable dataTable = new System.Data.DataTable();
			System.Data.DataRow dataRow = dataTable.NewRow();
			for (int i = 0; i < dgvExcel.ColumnCount; i++)
			{
				if (dgvExcel.Columns[i].Visible)
				{
					string columnName = dgvExcel.Columns[i].HeaderText.ToString();
					System.Data.DataColumn column = new System.Data.DataColumn(columnName, typeof(string));
					dataTable.Columns.Add(column);
					dataRow[columnName] = i + 1;
				}
			}
			dataTable.Rows.Add(dataRow);
			for (int j = 0; j < dgvExcel.Rows.Count; j++)
			{
				System.Data.DataRow dataRow2 = dataTable.NewRow();
				for (int k = 0; k < dgvExcel.ColumnCount; k++)
				{
					if (dgvExcel.Columns[k].Visible)
					{
						string columnName2 = dgvExcel.Columns[k].HeaderText.ToString();
						dataRow2[columnName2] = dgvExcel.Rows[j].Cells[k].Value.ToString();
					}
				}
				dataTable.Rows.Add(dataRow2);
			}
			SaveFileDialog saveFileDialog = new SaveFileDialog();
			saveFileDialog.Filter = "XLS文件|*.xls|所有文件|*.*";
			saveFileDialog.FilterIndex = 1;
			saveFileDialog.RestoreDirectory = true;
			if (saveFileDialog.ShowDialog() == DialogResult.OK)
			{
				string fileName = saveFileDialog.FileName;
				ExcelHelper excelHelper = new ExcelHelper(strftemplate, fileName);
				excelHelper.DataTableToExcelAddTitle(dataTable, dataTable.Rows.Count, intTop, intLeft, "页", strDate);
			}
		}

		public void DataTableToExcelAddTitle(System.Data.DataTable dt, int rows, int top, int left, string sheetPrefixName, string strDate)
		{
			int count = dt.Rows.Count;
			int count2 = dt.Columns.Count;
			int sheetCount = this.GetSheetCount(count, rows);
			if (sheetPrefixName == null || sheetPrefixName.Trim() == "")
			{
				sheetPrefixName = "Sheet";
			}
			DateTime now = DateTime.Now;
			Excel.Application application = new ApplicationClass();
			application.Visible = true;
			DateTime now2 = DateTime.Now;
			Workbook workbook = application.Workbooks.Open(this.templetFile, this.missing, this.missing, this.missing, this.missing, this.missing, this.missing, this.missing, this.missing, this.missing, this.missing, this.missing, this.missing);
			Worksheet o = (Worksheet)workbook.Sheets.get_Item(1);
			for (int i = 1; i < sheetCount; i++)
			{
				((Worksheet)workbook.Worksheets.get_Item(i)).Copy(this.missing, workbook.Worksheets[i]);
			}
			for (int i = 1; i <= sheetCount; i++)
			{
				int num = (i - 1) * rows;
				int num2 = i * rows;
				if (i == sheetCount)
				{
					num2 = count;
				}
				Worksheet worksheet = (Worksheet)workbook.Worksheets.get_Item(i);
				worksheet.Name = sheetPrefixName + "-" + i.ToString();
				for (int j = 0; j < num2 - num; j++)
				{
					for (int k = 0; k < count2; k++)
					{
						worksheet.Cells[top + j, left + k] = dt.Rows[num + j][k].ToString();
					}
				}
				Excel.TextBox textBox = (Excel.TextBox)worksheet.TextBoxes("txtDate");
				textBox.Text = strDate;
			}
			try
			{
				workbook.SaveAs(this.outputFile, this.missing, this.missing, this.missing, this.missing, this.missing, XlSaveAsAccessMode.xlExclusive, this.missing, this.missing, this.missing, this.missing);
				workbook.Close(null, null, null);
				application.Workbooks.Close();
				application.Application.Quit();
				application.Quit();
				Marshal.ReleaseComObject(o);
				Marshal.ReleaseComObject(workbook);
				Marshal.ReleaseComObject(application);
				GC.Collect();
			}
			catch (Exception ex)
			{
				File.Delete(this.strTemExcelFileName);
				throw ex;
			}
			finally
			{
				Process[] processesByName = Process.GetProcessesByName("Excel");
				Process[] array = processesByName;
				for (int l = 0; l < array.Length; l++)
				{
					Process process = array[l];
					DateTime startTime = process.StartTime;
					if (startTime > now && startTime < now2)
					{
						process.Kill();
					}
				}
			}
		}

		public static void DataGridViewToExcel(DataGridView gridView, string sheetName = "Sheet1")
		{
			if (gridView != null && gridView.ColumnCount != 0)
			{
				SaveFileDialog saveFileDialog = new SaveFileDialog();
				saveFileDialog.Filter = "Excel表格|*.xlsx";
				DialogResult dialogResult = saveFileDialog.ShowDialog();
				if (dialogResult != DialogResult.Cancel)
				{
					string fileName = saveFileDialog.FileName;
					IWorkbook workbook = new XSSFWorkbook();
					ISheet sheet = workbook.CreateSheet(sheetName);
					IRow row = sheet.CreateRow(0);
					ICellStyle cellStyle = workbook.CreateCellStyle();
					cellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
					cellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
					cellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
					cellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
					cellStyle.FillForegroundColor = 42;
					cellStyle.FillPattern = FillPattern.SolidForeground;
					int i = 0;
					int num = 0;
					while (i < gridView.ColumnCount)
					{
						if (gridView.Columns[i].Visible)
						{
							ICell cell = row.CreateCell(num);
							cell.SetCellValue(gridView.Columns[i].HeaderText);
							cell.CellStyle = cellStyle;
							num++;
						}
						i++;
					}
					ICellStyle cellStyle2 = workbook.CreateCellStyle();
					cellStyle2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
					cellStyle2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
					cellStyle2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
					cellStyle2.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
					for (int j = 0; j < gridView.RowCount; j++)
					{
						IRow row2 = sheet.CreateRow(j + 1);
						int k = 0;
						int num2 = 0;
						while (k < gridView.ColumnCount)
						{
							if (gridView.Columns[k].Visible)
							{
								ICell cell = row2.CreateCell(num2);
								cell.SetCellValue(gridView.Rows[j].Cells[k].Value.ToString());
								cell.CellStyle = cellStyle2;
								num2++;
							}
							k++;
						}
					}
					FileStream fileStream = null;
					try
					{
						fileStream = File.Create(fileName);
						workbook.Write(fileStream);
					}
					catch (Exception ex)
					{
						throw ex;
					}
					finally
					{
						if (fileStream != null)
						{
							fileStream.Close();
						}
					}
				}
			}
		}

		public static void DataGridViewToExcel(DataGridView gridView, int top, int left, string sheetName = "Sheet1")
		{
			if (gridView != null && gridView.ColumnCount != 0)
			{
				SaveFileDialog saveFileDialog = new SaveFileDialog();
				saveFileDialog.Filter = "Excel表格|*.xlsx";
				DialogResult dialogResult = saveFileDialog.ShowDialog();
				if (dialogResult != DialogResult.Cancel)
				{
					string fileName = saveFileDialog.FileName;
					IWorkbook workbook = new XSSFWorkbook();
					ISheet sheet = workbook.CreateSheet(sheetName);
					IRow row = sheet.CreateRow(top);
					ICellStyle cellStyle = workbook.CreateCellStyle();
					cellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
					cellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
					cellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
					cellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
					cellStyle.FillForegroundColor = 42;
					cellStyle.FillPattern = FillPattern.SolidForeground;
					int i = 0;
					int num = 0;
					while (i < gridView.ColumnCount)
					{
						if (gridView.Columns[i].Visible)
						{
							ICell cell = row.CreateCell(left + num);
							cell.SetCellValue(gridView.Columns[i].HeaderText);
							cell.CellStyle = cellStyle;
							num++;
						}
						i++;
					}
					ICellStyle cellStyle2 = workbook.CreateCellStyle();
					cellStyle2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
					cellStyle2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
					cellStyle2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
					cellStyle2.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
					for (int j = 0; j < gridView.RowCount; j++)
					{
						IRow row2 = sheet.CreateRow(top + j + 1);
						int k = 0;
						int num2 = 0;
						while (k < gridView.ColumnCount)
						{
							if (gridView.Columns[k].Visible)
							{
								ICell cell = row2.CreateCell(left + num2);
								cell.SetCellValue(gridView.Rows[j].Cells[k].Value.ToString());
								cell.CellStyle = cellStyle2;
								num2++;
							}
							k++;
						}
					}
					FileStream fileStream = null;
					try
					{
						fileStream = File.Create(fileName);
						workbook.Write(fileStream);
					}
					catch (Exception ex)
					{
						throw ex;
					}
					finally
					{
						if (fileStream != null)
						{
							fileStream.Close();
						}
					}
				}
			}
		}

		public static void DataTableToExcel(System.Data.DataTable dtTable, string filePath, string sheetName = "Sheet1")
		{
			if (dtTable != null && dtTable.Columns.Count != 0)
			{
				if (string.IsNullOrWhiteSpace(filePath))
				{
					SaveFileDialog saveFileDialog = new SaveFileDialog();
					saveFileDialog.Filter = "Excel表格|*.xlsx";
					DialogResult dialogResult = saveFileDialog.ShowDialog();
					if (dialogResult == DialogResult.Cancel)
					{
						return;
					}
					filePath = saveFileDialog.FileName;
				}
				IWorkbook workbook = new XSSFWorkbook();
				ISheet sheet = workbook.CreateSheet(sheetName);
				IRow row = sheet.CreateRow(0);
				ICellStyle cellStyle = workbook.CreateCellStyle();
				cellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
				cellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
				cellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
				cellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
				cellStyle.FillForegroundColor = 42;
				cellStyle.FillPattern = FillPattern.SolidForeground;
				for (int i = 0; i < dtTable.Columns.Count; i++)
				{
					ICell cell = row.CreateCell(i);
					cell.SetCellValue(dtTable.Columns[i].ColumnName);
					cell.CellStyle = cellStyle;
				}
				ICellStyle cellStyle2 = workbook.CreateCellStyle();
				cellStyle2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
				cellStyle2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
				cellStyle2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
				cellStyle2.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
				for (int j = 0; j < dtTable.Rows.Count; j++)
				{
					IRow row2 = sheet.CreateRow(j + 1);
					for (int k = 0; k < dtTable.Columns.Count; k++)
					{
						ICell cell = row2.CreateCell(k);
						cell.SetCellValue(dtTable.Rows[j][k].ToString());
						cell.CellStyle = cellStyle2;
					}
				}
				FileStream fileStream = null;
				try
				{
					fileStream = File.Create(filePath);
					workbook.Write(fileStream);
				}
				catch (Exception ex)
				{
					throw ex;
				}
				finally
				{
					if (fileStream != null)
					{
						fileStream.Close();
					}
				}
			}
		}
	}
}
