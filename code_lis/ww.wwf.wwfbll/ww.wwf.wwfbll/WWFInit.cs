using System;
using System.IO;
using ww.wwf.dao;

namespace ww.wwf.wwfbll
{
	public class WWFInit
	{
		public static DAO wwfRemotingDao = new DAO();

		public static string strDBConn = "WW.DBConn";

		public static string strCHisDBConn = "CHisDBConn";

		public static string strClientID = "";

		public WWFInit()
		{
			DAO dAO = new DAO();
		}

		private void DbCreatRemotingOBJ()
		{
			try
			{
			}
			catch
			{
			}
		}

		public bool DbClientUser()
		{
			return true;
		}

		public string DbGuid20()
		{
			string text = Guid.NewGuid().ToString();
			text = text.Replace("-", "");
			int length = text.Length;
			return text.Substring(length - 20, 20);
		}

		public static void WWFSaveLog(string strErrorTitle, string strException)
		{
			if (strErrorTitle == "" || strErrorTitle == "")
			{
				strErrorTitle = "一般异常";
			}
			StreamWriter streamWriter = File.AppendText("wwf.log");
			streamWriter.WriteLine("时间：" + DateTime.Now.ToString());
			streamWriter.WriteLine("系统：wwf");
			streamWriter.WriteLine("标题：" + strErrorTitle);
			streamWriter.WriteLine("内容：" + strException);
			streamWriter.WriteLine("");
			streamWriter.Flush();
			streamWriter.Close();
		}
	}
}
