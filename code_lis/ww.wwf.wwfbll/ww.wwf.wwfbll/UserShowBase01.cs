﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;
using System.Collections;

namespace ww.wwf.wwfbll
{
    public partial class UserShowBase01 : Form
    {
        //DeptSumBLL bllDeptSum = new DeptSumBLL();
        ExcelHelper excel = new ExcelHelper("", "");      
        /// <summary>
        /// 表ID
        /// </summary>
        private string strTable_id = "08cf0ba4d75e47d6bb2c82e1773d668d";
        /// <summary>
        /// 原始值 数据表
        /// </summary>
        private DataTable dtYSValue = new DataTable();
        /// <summary>
        /// Excel导出模板
        /// </summary>
        private string strExcelTemplate = "";
        /// <summary>
        /// 据表ID取出的列值
        /// </summary>
        private DataTable dtColumns = null;
        /// <summary>
        /// 据表ID取出的表
        /// </summary>
        private DataTable dtTable = null;

        /// <summary>
        /// 自定义业务规则
        /// </summary>
        private UserdefinedBLL bll = new UserdefinedBLL();
        /// <summary>
        /// 当前要建临时表
        /// </summary>
        private string dbCreatTemTableName = "";
        /// <summary>
        /// 取出临时表的值
        /// </summary>
        public DataTable dtDBTemTable = new DataTable();
        public UserShowBase01()
        {
            InitializeComponent();
        }

        private void UserShowBase01_Load(object sender, EventArgs e)
        {
            dataGridView1.Visible = false;
            TJ();
        }
       
        public void TJ()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                dataGridView1.Visible = false;
                this.dtYSValue = this.bll.BllCreatYSDataTable(this.strTable_id);
                BllSetdtYSValue();
                //this.dataGridViewShow.DataSource = this.bllDeptSum.BllDt("2008-01-01", "2009-01-01");
                ShowValue();
                // dtDBTemTable
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        /// <summary>
        /// 设置原始值
        /// </summary>
        public void BllSetdtYSValue()
        { 
            /*
            //开始年、月　结束年、月-----------------
            string strYearS = dateTimePickerS.Value.Year.ToString();//开始年
            string strYearE = dateTimePickerE.Value.Year.ToString();//结束年
            string free1_1 = dateTimePickerS.Value.Month.ToString();//开始月
            string free1_2 = dateTimePickerE.Value.Month.ToString();//结束月
            string strDayS = dateTimePickerS.Value.Day.ToString();//开始天
            string strDayE = dateTimePickerE.Value.Day.ToString();//结束天

            if (free1_1.Length == 1)
                free1_1 = "0" + free1_1;
            if (free1_2.Length == 1)
                free1_2 = "0" + free1_2;
            if (strDayS.Length == 1)
                strDayS = "0" + strDayS;
            if (strDayE.Length == 1)
                strDayE = "0" + strDayE;

            string strS = strYearS + "-" + free1_1 + "-" + strDayS;
            string strE = strYearE + "-" + free1_2 + "-" + strDayE;

           


          //  DataTable dtOleZXKS = this.bllDeptSum.BllDtZXKS(strS, strE);
         //   DataTable dtOle = this.bllDeptSum.BllDt(strS, strE);

            dataGridView1.DataSource = dtOle;
            string strFid = "";

            string strzxksname = "";//执行科室名称
            for (int iv = 0; iv < dtOleZXKS.Rows.Count; iv++)
            {
                strzxksname = dtOleZXKS.Rows[iv]["执行科室"].ToString();
                if (strzxksname == "" || strzxksname == null)
                { }
                else
                {
                    //MessageBox.Show(strzxksname);
                    DataRow drTT = dtYSValue.NewRow();
                    strFid = iv.ToString();
                    for (int i = 0; i < this.dtYSValue.Columns.Count; i++)
                    {
                        string str = this.dtYSValue.Columns[i].ColumnName.ToString();
                        if (str == "fid")
                            drTT[str] = strFid.ToString();
                        else if (str == "fCustomCol1")
                            drTT[str] = strzxksname.ToString();
                        
                    }
                    this.dtYSValue.Rows.Add(drTT);
                }
            }

            for (int ole = 0; ole < dtOle.Rows.Count; ole++)
            {
                //string strKSCode = "";//执行科室代码
                string strKSCodeName = "";//执行科室名称
                string strXMCode = "";//核算项目代码
                string strFCredit = "";//付款

                strKSCodeName = dtOle.Rows[ole]["执行科室"].ToString();
                strXMCode = dtOle.Rows[ole]["ProjectID"].ToString();
                strXMCode = "f" + strXMCode;
                strFCredit = dtOle.Rows[ole]["付款"].ToString();

                DataRow[] currcolumXS = this.dtYSValue.Select("fCustomCol1='" + strKSCodeName + "'");
                if (currcolumXS != null)
                {
                    foreach (DataRow drXS in currcolumXS)
                    {
                       // MessageBox.Show("列：" + strXMCode + "   值：" + strFCredit);
                        try
                        {
                            drXS[strXMCode] = strFCredit;
                        }
                        catch { }
                        // strfsum_flag = drXS["fsum_flag"].ToString();
                        //strfxsws = drXS["fxsws"].ToString();
                    }
                }
            }*/
        }
       
        /// <summary>
        /// 显示统计列表
        /// </summary>
        protected void ShowValue()
        {
            try
            {
                this.dtTable = new DataTable();
                this.dtColumns = new DataTable();
                dtTable = this.bll.BllTypeDTByftable_id(strTable_id);

                if (dtTable.Rows.Count > 0)
                {
                    string ftype = dtTable.Rows[0]["ftype"].ToString();//表类型 只表才做运行
                    this.strExcelTemplate = dtTable.Rows[0]["ftemplate"].ToString();//导出模板
                    int fsum_flag = Convert.ToInt32(dtTable.Rows[0]["fsum_flag"].ToString());//有合计否
                    if (ftype == "表")
                    {
                        this.dtColumns = this.bll.BllColumnsByftable_id(strTable_id);
                        if (this.dtColumns.Rows.Count > 0)
                        {
                            #region 创建数据临时表 并取出公式值
                            this.dbCreatTemTableName = "tem_" + this.bll.DbGuid();
                            //string sqlCreate = "create table " + dbCreatTemTableName + "(pk_accsubj char(20),subjcode varchar(30),subjname varchar(40),dispname varchar(200),unit varchar(10))";
                            //string sqlCreate = "create table " + dbCreatTemTableName + " (fid,";//要创建的列
                            StringBuilder sqlCreate = new StringBuilder();//创建sql
                            StringBuilder sqlInsertCol = new StringBuilder();//插入的列

                            StringBuilder sqlGetGS = new StringBuilder();//取得公式
                            sqlInsertCol.Append("INSERT INTO " + dbCreatTemTableName + "  (fid");


                            sqlCreate.Append("create table " + dbCreatTemTableName + " (fid varchar(32)");
                            string strColumnName = "";//列名
                            string strfvalue_type = "";//值类型
                            string strfvalue_content = "";//公式值
                            string strfdata_type = "";//数据类型
                            //MessageBox.Show("dd");
                            for (int i = 0; i < dtColumns.Rows.Count; i++)
                            {
                                strfvalue_type = dtColumns.Rows[i]["fvalue_type"].ToString();
                                strfvalue_content = dtColumns.Rows[i]["fvalue_content"].ToString();
                                strColumnName = dtColumns.Rows[i]["fcode"].ToString();
                                strfdata_type = dtColumns.Rows[i]["fdata_type"].ToString();
                                if (strfvalue_type == "公式值")
                                {
                                    sqlGetGS.Append(",(" + strfvalue_content + ") as " + strColumnName);
                                }
                                else
                                {
                                    if (strfdata_type == "数字型")
                                    { sqlCreate.Append("," + strColumnName + " float(8)"); }
                                    else
                                    { sqlCreate.Append("," + strColumnName + " varchar(100)"); }
                                    sqlInsertCol.Append("," + strColumnName);
                                }
                            }

                            sqlCreate.Append(")");
                            string sql = "SELECT * " + sqlGetGS.ToString() + " FROM " + this.dbCreatTemTableName + " order by fid";
                            this.bll.BllTemTableCreate(sqlCreate.ToString());//临时表创建

                            #endregion

                            #region //给当前表设置值
                            //this.textBox1.Text = sqlInsertCol.ToString();
                            CurrDTSetValue(sqlInsertCol.ToString());
                            #endregion

                            dtDBTemTable = this.bll.BllTemTableDT(sql); //取出临时表的值                    
                            CurrDTSum(fsum_flag);// 求和             
                            bindingSource1.DataSource = dtDBTemTable;
                            this.dataGridViewShow.DataSource = bindingSource1;
                            CurrGridViewSet();//当前GridView设置

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                
                   this.bll.BllTemTableDel(this.dbCreatTemTableName);//临时表删除
             
            }
        }

        /// <summary>
        /// 给当前表设置值
        /// </summary>
        /// <param name="sqlInsertCol"></param>
        private void CurrDTSetValue(string sqlInsertCol)
        {
            IList sqlList = new ArrayList();
            for (int intYS = 0; intYS < dtYSValue.Rows.Count; intYS++)
            {
                StringBuilder sqlInsertColValue = new StringBuilder();//插入的列 值
                string strvalue = null;
                for (int intYSC = 0; intYSC < dtYSValue.Columns.Count; intYSC++)
                {
                    strvalue = dtYSValue.Rows[intYS][intYSC].ToString();
                    if (intYSC == 0)
                    {
                        sqlInsertColValue.Append("'" + strvalue + "'");
                    }
                    else
                    {
                        if (ww.wwf.com.Public.IsNumber(strvalue))
                        {
                            if (Convert.ToDouble(strvalue) == 0)
                                sqlInsertColValue.Append(",null");
                            else
                                sqlInsertColValue.Append("," + strvalue + "");
                        }
                        else
                        {
                            if (strvalue == null || strvalue == "")
                                sqlInsertColValue.Append(",null");
                            else
                                sqlInsertColValue.Append(",'" + strvalue + "'");
                        }
                    }

                }
                string sqlInsert = sqlInsertCol + ") VALUES (" + sqlInsertColValue.ToString() + ")";
                sqlList.Add(sqlInsert);

            }
            this.bll.BllTemTableInsertBusiness(sqlList);
        }

        /// <summary>
        /// 当前表Sum
        /// </summary>
        /// <param name="fsum_flag"></param>
        private void CurrDTSum(int fsum_flag)
        {
            if (fsum_flag == 1)
            {
                //计算合计                               
                string strCurrColumn = "";//当前列名
                string strfsum_flag = "0";//求和否
                string strfxsws = "0";//小数位数
                DataRow drRowSum = dtDBTemTable.NewRow();
                drRowSum[1] = "合  计";
                for (int c = 0; c < dtDBTemTable.Columns.Count; c++)
                {
                    strCurrColumn = dtDBTemTable.Columns[c].ColumnName.ToString();

                    //fcode
                    DataRow[] currcolumXS = dtColumns.Select("fcode='" + strCurrColumn + "'");
                    if (currcolumXS != null)
                    {
                        foreach (DataRow drXS in currcolumXS)
                        {
                            strfsum_flag = drXS["fsum_flag"].ToString();
                            strfxsws = drXS["fxsws"].ToString();
                        }
                    }
                    if (strfsum_flag == "1")
                    {
                        if (strfxsws == "" || strfxsws == null)
                            strfxsws = "2";
                        Decimal decSumValue = 0;
                        for (int isum = 0; isum < dtDBTemTable.Rows.Count; isum++)
                        {
                            if (dtDBTemTable.Rows[isum][strCurrColumn].ToString() == "" || dtDBTemTable.Rows[isum][strCurrColumn].ToString() == null)
                            {
                                decSumValue = decSumValue + 0;
                            }
                            else
                            {
                                // dtDBTemTable.Rows[isum][strCurrColumn] = ww.wwf.com.Public.BllGetXSWS(Convert.ToDecimal(dtDBTemTable.Rows[isum][strCurrColumn].ToString()), Convert.ToInt32(strfxsws));  
                                dtDBTemTable.Rows[isum][strCurrColumn] = dtDBTemTable.Rows[isum][strCurrColumn].ToString();
                                decSumValue = decSumValue + Convert.ToDecimal(dtDBTemTable.Rows[isum][strCurrColumn].ToString());
                            }
                        }
                        //MessageBox.Show(decSumValue.ToString());
                        // decSumValue = ww.wwf.com.Public.BllGetXSWS(decSumValue, Convert.ToInt32(strfxsws));
                        //decSumValue = decSumValue;
                        // if (decSumValue != 0)
                        drRowSum[strCurrColumn] = decSumValue;
                    }
                }
                dtDBTemTable.Rows.Add(drRowSum);
            }
        }

        /// <summary>
        /// 当前GridView设置
        /// </summary>
        private void CurrGridViewSet()
        {
            try
            {
                for (int i = 0; i < this.dataGridViewShow.ColumnCount; i++)
                {
                    string strCurrColName = this.dataGridViewShow.Columns[i].Name.ToString();
                    DataRow[] colList = dtColumns.Select("fcode='" + strCurrColName + "'");
                    int fshow_width = 100;//宽度
                    int fshow_flag = 1;//显示标识               
                    string fname = "";//列名
                    this.dataGridViewShow.Columns[strCurrColName].Visible = false;
                    int fxsws = 2;//小数位数
                    string fdata_type = "";//数据类型
                    if (colList.Length > 0)
                    {
                        foreach (DataRow drC in colList)
                        {
                            try
                            {
                                fxsws = Convert.ToInt32(drC["fxsws"].ToString());
                            }
                            catch { fxsws = 2; }
                            fdata_type = drC["fdata_type"].ToString();
                            //MessageBox.Show(fdata_type);

                            //System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
                            //dataGridViewCellStyle1.Format = "N2";
                            //dataGridViewCellStyle1.NullValue = null;

                            if (drC["fshow_width"].ToString() == "" || drC["fshow_width"].ToString() == null)
                            {
                            }
                            else
                            {
                                fshow_width = Convert.ToInt32(drC["fshow_width"].ToString());
                            }
                            if (drC["fshow_flag"].ToString() == "" || drC["fshow_flag"].ToString() == null)
                            {
                            }
                            else
                            {
                                fshow_flag = Convert.ToInt32(drC["fshow_flag"].ToString());
                            }
                            fname = drC["fname"].ToString();
                        }

                        if (fshow_flag == 1)
                        {
                            this.dataGridViewShow.Columns[strCurrColName].Visible = true;
                            this.dataGridViewShow.Columns[strCurrColName].Width = fshow_width;
                            this.dataGridViewShow.Columns[strCurrColName].HeaderText = fname;

                            if (fdata_type == "数字型")
                            {
                                this.dataGridViewShow.Columns[strCurrColName].DefaultCellStyle.Format = "N" + fxsws.ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void buttonSet_Click(object sender, EventArgs e)
        {
            try
            {
                UserdefinedFieldUpdateForm userf = new UserdefinedFieldUpdateForm(this.strTable_id);
                userf.ShowDialog();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

       

        private void toolStripButtonTJ_Click(object sender, EventArgs e)
        {
            TJ();
        }

        private void toolStripButtonExcel_Click(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                this.Validate();
                this.dataGridViewShow.EndEdit();
                if (this.dataGridViewShow.Rows.Count > 0)
                    excel.GridViewToExcel(this.dataGridViewShow, "", 1, 1);

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void toolStripButtonGS_Click(object sender, EventArgs e)
        {
            try
            {
                UserdefinedFieldUpdateForm userf = new UserdefinedFieldUpdateForm(this.strTable_id);
                userf.ShowDialog();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void toolStripButtonLQ_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;
        }

        private void toolStripButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
    }
}