using System;
using System.Data;
using System.Text;

namespace ww.wwf.wwfbll
{
	public class PositionBLL : DAOWWF
	{
		public System.Data.DataTable BllDT()
		{
			string strSql = "SELECT * FROM WWF_POSITION ORDER BY forder_by";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public int BllUpdate(System.Data.DataRowView dr)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update WWF_POSITION set ");
			stringBuilder.Append("fname='" + dr["fname"].ToString() + "',");
			stringBuilder.Append("forder_by='" + dr["forder_by"].ToString() + "',");
			stringBuilder.Append("fuse_flag=" + dr["fuse_flag"].ToString() + ",");
			stringBuilder.Append("fremark='" + dr["fremark"].ToString() + "'");
			stringBuilder.Append(" where fposition_id='" + dr["fposition_id"].ToString() + "'");
			StringBuilder stringBuilder2 = new StringBuilder();
			stringBuilder2.Append("update wwf_org set ");
			stringBuilder2.Append("fname='" + dr["fname"].ToString() + "'");
			stringBuilder2.Append(" where fposition_id='" + dr["fposition_id"].ToString() + "' and ftype = 'position'");
			int result;
			if (WWFInit.wwfRemotingDao.DbExecuteNonQueryTransaction2(WWFInit.strDBConn, stringBuilder.ToString(), stringBuilder2.ToString()))
			{
				result = 1;
			}
			else
			{
				result = 0;
			}
			return result;
		}

		public int BllAdd(System.Data.DataRowView dr)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into WWF_POSITION(");
			stringBuilder.Append("fposition_id,fname,forder_by,fuse_flag,fremark");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + dr["fposition_id"].ToString() + "',");
			stringBuilder.Append("'" + dr["fname"].ToString() + "',");
			stringBuilder.Append("'" + dr["forder_by"].ToString() + "',");
			stringBuilder.Append(dr["fuse_flag"].ToString() + ",");
			stringBuilder.Append("'" + dr["fremark"].ToString() + "'");
			stringBuilder.Append(")");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllDelete(string id)
		{
			string strSql = "delete FROM WWF_POSITION where fposition_id='" + id + "'";
			string strSql2 = "delete FROM wwf_org where fposition_id='" + id + "'";
			int result;
			if (WWFInit.wwfRemotingDao.DbExecuteNonQueryTransaction2(WWFInit.strDBConn, strSql, strSql2))
			{
				result = 1;
			}
			else
			{
				result = 0;
			}
			return result;
		}

		public int BllDelete7777(string id)
		{
			string strSql = "delete FROM WWF_POSITION where fposition_id='" + id + "'";
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, strSql);
		}
	}
}
