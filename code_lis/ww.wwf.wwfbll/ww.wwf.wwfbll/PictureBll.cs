using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace ww.wwf.wwfbll
{
	public class PictureBll
	{
		private OpenFileDialog openFileDialog1 = new OpenFileDialog();

		private SaveFileDialog saveFileDialog1 = new SaveFileDialog();

		public void ImgIn(PictureBox picBox)
		{
			this.openFileDialog1.Filter = "文件(*.jpg;*.gif;*.png;*.JPG;*.GIF;*.PNG)|*.jpg;*.gif;*.png;*.JPG;*.GIF;*.PNG|BMP文件(*.bmp)|*.bmp|所有文件(*.*)|*.*";
			this.saveFileDialog1.Title = "导入图像";
			this.saveFileDialog1.InitialDirectory = "c:\\";
			if (this.openFileDialog1.ShowDialog() == DialogResult.OK)
			{
				Stream stream;
				if ((stream = this.openFileDialog1.OpenFile()) != null)
				{
					try
					{
						byte[] buffer = new byte[stream.Length];
						stream.Position = 0L;
						stream.Read(buffer, 0, Convert.ToInt32(stream.Length));
						Bitmap image = new Bitmap(Image.FromStream(stream));
						picBox.Image = image;
					}
					catch (Exception ex)
					{
						WWMessage.MessageShowError(ex.Message.ToString());
					}
				}
			}
		}

		public void ImgOut(PictureBox picBox)
		{
			this.saveFileDialog1.Filter = "文件(*.jpg;*.gif;*.png;*.JPG;*.GIF;*.PNG)|*.jpg;*.gif;*.png;*.JPG;*.GIF;*.PNG|BMP文件(*.bmp)|*.bmp|所有文件(*.*)|*.*";
			this.saveFileDialog1.Title = "导出图像";
			this.saveFileDialog1.InitialDirectory = "c:\\";
			if (this.saveFileDialog1.ShowDialog() == DialogResult.OK)
			{
				if (this.saveFileDialog1.FileName != null)
				{
					try
					{
						Bitmap bitmap = new Bitmap(picBox.Image);
						string fileName = this.saveFileDialog1.FileName;
						picBox.Image.Save(fileName);
						WWMessage.MessageShowInformation("导出图片成功！");
					}
					catch (Exception ex)
					{
						WWMessage.MessageShowError(ex.Message.ToString());
					}
				}
			}
		}

		public void ImgNull(PictureBox picBox)
		{
			try
			{
				picBox.Image = null;
			}
			catch (Exception ex)
			{
				WWMessage.MessageShowError(ex.Message.ToString());
			}
		}
	}
}
