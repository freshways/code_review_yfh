using System;
using System.Collections;
using System.Data;
using System.Text;

namespace ww.wwf.wwfbll
{
	public class UserdefinedBLL : DAOWWF
	{
		public System.Data.DataTable BllTypeDT(int fuse_flag)
		{
			string strSql;
			if (fuse_flag == 1 || fuse_flag == 0)
			{
				strSql = "SELECT * FROM wwf_table where fuse_flag=" + fuse_flag + " ORDER BY forder_by";
			}
			else
			{
				strSql = "SELECT * FROM wwf_table  ORDER BY forder_by";
			}
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public System.Data.DataTable BllTypeDTByfpid(string fp_id)
		{
			string strSql = "SELECT * FROM wwf_table where fp_id='" + fp_id + "' ORDER BY forder_by";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public System.Data.DataTable BllTypeDTByftable_id(string ftable_id)
		{
			string strSql = "SELECT * FROM wwf_table where ftable_id='" + ftable_id + "'";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public int BllTypeAdd(string fp_id)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into wwf_table(");
			stringBuilder.Append("ftable_id,fp_id,fuse_flag,fsum_flag,fexcel_top,fexcel_left,forder_by");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + base.DbGuid() + "',");
			stringBuilder.Append("'" + fp_id + "',");
			stringBuilder.Append("1,");
			stringBuilder.Append("1,");
			stringBuilder.Append("1,");
			stringBuilder.Append("1,");
			stringBuilder.Append("'1'");
			stringBuilder.Append(")");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllTypeUpdate(System.Data.DataRow dr)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update wwf_table set ");
			stringBuilder.Append("fp_id='" + dr["fp_id"].ToString() + "',");
			stringBuilder.Append("ftype='" + dr["ftype"].ToString() + "',");
			stringBuilder.Append("fcode='" + dr["fcode"].ToString() + "',");
			stringBuilder.Append("fname='" + dr["fname"].ToString() + "',");
			stringBuilder.Append("fremark='" + dr["fremark"].ToString() + "',");
			stringBuilder.Append("fuse_flag=" + dr["fuse_flag"].ToString() + ",");
			stringBuilder.Append("ftemplate='" + dr["ftemplate"].ToString() + "',");
			stringBuilder.Append("fsum_flag=" + dr["fsum_flag"].ToString() + ",");
			stringBuilder.Append("fexcel_left=" + dr["fexcel_left"].ToString() + ",");
			stringBuilder.Append("fexcel_top=" + dr["fexcel_top"].ToString() + ",");
			stringBuilder.Append("forder_by='" + dr["forder_by"].ToString() + "'");
			stringBuilder.Append(" where ftable_id='" + dr["ftable_id"].ToString() + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public System.Data.DataTable BllColumnsByftable_id(string ftable_id)
		{
			string strSql = "SELECT * FROM wwf_columns where ftable_id='" + ftable_id + "' ORDER BY forder_by";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public int BllColumnsAdd(string ftable_id)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into wwf_columns(");
			stringBuilder.Append("fcolumns_id,ftable_id,fvalue_type,fxsws,fshow_flag,fgroup_flag,fsum_flag,fshow_width,fdefault,fdata_type,fread_flage,forder_by");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + base.DbGuid() + "',");
			stringBuilder.Append("'" + ftable_id + "',");
			stringBuilder.Append("'原始值',");
			stringBuilder.Append("'2',");
			stringBuilder.Append("'1',");
			stringBuilder.Append("'0',");
			stringBuilder.Append("'0',");
			stringBuilder.Append("'100',");
			stringBuilder.Append("'',");
			stringBuilder.Append("'字符型',");
			stringBuilder.Append("0,");
			stringBuilder.Append("'01'");
			stringBuilder.Append(")");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllcolumnsDel(string ftable_id)
		{
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, "DELETE FROM WWF_COLUMNS WHERE (ftable_id = '" + ftable_id + "') and (fcode<>'CustomCol1' and fcode<>'CustomCol2')");
		}

		public int BllcolumnsZXKSDel(string ftable_id)
		{
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, "DELETE FROM WWF_COLUMNS WHERE (ftable_id = '" + ftable_id + "') and (fcode<>'CustomCol1' and fcode<>'Customcol3')");
		}

		public int BllColumnsAdd(string ftable_id, string fcode, string fname, string forder_by)
		{
			int num = 0;
			if (!WWFInit.wwfRemotingDao.DbRecordExists(WWFInit.strDBConn, string.Concat(new string[]
			{
				"SELECT count(1) FROM wwf_columns where ftable_id='",
				ftable_id,
				"' and fcode='",
				fcode,
				"'"
			})))
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append("insert into wwf_columns(");
				stringBuilder.Append("fcolumns_id,ftable_id,fcode,fname,fvalue_type,fxsws,fshow_flag,fgroup_flag,fsum_flag,fshow_width,fdefault,fdata_type,forder_by");
				stringBuilder.Append(")");
				stringBuilder.Append(" values (");
				stringBuilder.Append("'" + base.DbGuid() + "',");
				stringBuilder.Append("'" + ftable_id + "',");
				stringBuilder.Append("'" + fcode + "',");
				stringBuilder.Append("'" + fname + "',");
				stringBuilder.Append("'原始值',");
				stringBuilder.Append("'2',");
				stringBuilder.Append("'1',");
				stringBuilder.Append("'0',");
				stringBuilder.Append("'1',");
				stringBuilder.Append("'100',");
				stringBuilder.Append("'',");
				stringBuilder.Append("'字符型',");
				stringBuilder.Append("'" + forder_by + "'");
				stringBuilder.Append(")");
				if (WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString()) > 0)
				{
					num++;
				}
			}
			return num;
		}

		public int BllColumnsDel(string fcolumns_id)
		{
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, "delete FROM wwf_columns where fcolumns_id='" + fcolumns_id + "'");
		}

		public int BllColumnsUpdate(System.Data.DataRow dr)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update wwf_columns set ");
			stringBuilder.Append("ftable_id='" + dr["ftable_id"].ToString() + "',");
			stringBuilder.Append("fcode='" + dr["fcode"].ToString() + "',");
			stringBuilder.Append("fname='" + dr["fname"].ToString() + "',");
			stringBuilder.Append("fold_columns='" + dr["fold_columns"].ToString() + "',");
			stringBuilder.Append("fvalue_type='" + dr["fvalue_type"].ToString() + "',");
			stringBuilder.Append("fdata_type='" + dr["fdata_type"].ToString() + "',");
			stringBuilder.Append("fxsws=" + dr["fxsws"].ToString() + ",");
			stringBuilder.Append("fshow_flag=" + dr["fshow_flag"].ToString() + ",");
			stringBuilder.Append("fshow_width=" + dr["fshow_width"].ToString() + ",");
			stringBuilder.Append("fgroup_flag=" + dr["fgroup_flag"].ToString() + ",");
			stringBuilder.Append("fsum_flag=" + dr["fsum_flag"].ToString() + ",");
			stringBuilder.Append("forder_by='" + dr["forder_by"].ToString() + "',");
			stringBuilder.Append("fdefault='" + dr["fdefault"].ToString() + "',");
			stringBuilder.Append("fvalue_content='" + dr["fvalue_content"].ToString() + "',");
			stringBuilder.Append("fread_flage=" + dr["fread_flage"].ToString() + ",");
			stringBuilder.Append("fremark='" + dr["fremark"].ToString() + "'");
			stringBuilder.Append(" where fcolumns_id='" + dr["fcolumns_id"].ToString() + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllTemTableCreate(string sql)
		{
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, sql);
		}

		public int BllTemTableDel(string strTableName)
		{
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, "DROP TABLE " + strTableName);
		}

		public System.Data.DataTable BllTemTableDT(string sql)
		{
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, sql).Tables[0];
		}

		public string BllTemTableInsertBusiness(IList sqlList)
		{
			return WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, sqlList);
		}

		public string BllTemTableUpdateSum(IList lissql)
		{
			return WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, lissql);
		}

		public System.Data.DataTable BllCreatYSDataTableBase(string strTable_id)
		{
			System.Data.DataTable dataTable = new System.Data.DataTable();
			System.Data.DataTable dataTable2 = new System.Data.DataTable();
			System.Data.DataTable dataTable3 = new System.Data.DataTable();
			dataTable2 = this.BllTypeDTByftable_id(strTable_id);
			if (dataTable2.Rows.Count > 0)
			{
				string a = dataTable2.Rows[0]["ftype"].ToString();
				if (a == "表")
				{
					dataTable3 = this.BllColumnsByftable_id(strTable_id);
					if (dataTable3.Rows.Count > 0)
					{
						System.Data.DataColumn column = new System.Data.DataColumn("fid", typeof(string));
						dataTable.Columns.Add(column);
						for (int i = 0; i < dataTable3.Rows.Count; i++)
						{
							string a2 = dataTable3.Rows[i]["fvalue_type"].ToString();
							string columnName = dataTable3.Rows[i]["fcode"].ToString();
							if (!(a2 == "公式值"))
							{
								System.Data.DataColumn column2 = new System.Data.DataColumn(columnName, typeof(string));
								dataTable.Columns.Add(column2);
							}
						}
					}
				}
			}
			return dataTable;
		}

		public System.Data.DataTable BllColumnsKMList(string ftable_id)
		{
			System.Data.DataTable dataTable = new System.Data.DataTable();
			System.Data.DataColumn column = new System.Data.DataColumn("科目", typeof(string));
			dataTable.Columns.Add(column);
			string strSql = "SELECT fvalue_content AS 科目 FROM wwf_columns t WHERE (ftable_id = '" + ftable_id + "') AND (fvalue_type = '科目值') ORDER BY forder_by";
			System.Data.DataTable dataTable2 = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
			for (int i = 0; i < dataTable2.Rows.Count; i++)
			{
				string text = dataTable2.Rows[i]["科目"].ToString();
				System.Data.DataRow[] array = dataTable.Select("科目='" + text + "'");
				if (array.Length <= 0)
				{
					System.Data.DataRow dataRow = dataTable.NewRow();
					dataRow["科目"] = text;
					dataTable.Rows.Add(dataRow);
				}
			}
			return dataTable;
		}

		public System.Data.DataTable BllCreatYSDataTable(string strTable_id)
		{
			System.Data.DataTable dataTable = new System.Data.DataTable();
			System.Data.DataTable dataTable2 = new System.Data.DataTable();
			System.Data.DataTable dataTable3 = new System.Data.DataTable();
			dataTable2 = this.BllTypeDTByftable_id(strTable_id);
			if (dataTable2.Rows.Count > 0)
			{
				string a = dataTable2.Rows[0]["ftype"].ToString();
				if (a == "表")
				{
					dataTable3 = this.BllColumnsByftable_id(strTable_id);
					if (dataTable3.Rows.Count > 0)
					{
						System.Data.DataColumn column = new System.Data.DataColumn("fid", typeof(string));
						dataTable.Columns.Add(column);
						for (int i = 0; i < dataTable3.Rows.Count; i++)
						{
							string text = dataTable3.Rows[i]["fvalue_type"].ToString();
							string columnName = dataTable3.Rows[i]["fcode"].ToString();
							System.Data.DataColumn column2 = new System.Data.DataColumn(columnName, typeof(string));
							dataTable.Columns.Add(column2);
						}
					}
				}
			}
			return dataTable;
		}
	}
}
