using System;
using System.Data;
using System.Text;

namespace ww.wwf.wwfbll
{
	public class FuncBLL : DAOWWF
	{
		public System.Data.DataTable BllFuncDT()
		{
			string strSql = "SELECT * FROM wwf_func ORDER BY forder_by";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public System.Data.DataTable BllFuncDT(string fp_id)
		{
			string strSql = "SELECT * FROM wwf_func where fp_id='" + fp_id + "' ORDER BY forder_by";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public System.Data.DataTable BllFuncDTByID(string ffunc_id)
		{
			string strSql = "SELECT * FROM wwf_func where ffunc_id='" + ffunc_id + "'";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public int BllUpdate(System.Data.DataTable dt)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update wwf_func set ");
			stringBuilder.Append("fp_id='" + dt.Rows[0]["fp_id"].ToString() + "',");
			stringBuilder.Append("fname='" + dt.Rows[0]["fname"].ToString() + "',");
			stringBuilder.Append("fname_e='" + dt.Rows[0]["fname_e"].ToString() + "',");
			stringBuilder.Append("ficon='" + dt.Rows[0]["ficon"].ToString() + "',");
			stringBuilder.Append("ffunc_winform='" + dt.Rows[0]["ffunc_winform"].ToString() + "',");
			stringBuilder.Append("ffunc_web='" + dt.Rows[0]["ffunc_web"].ToString() + "',");
			stringBuilder.Append("forder_by='" + dt.Rows[0]["forder_by"].ToString() + "',");
			stringBuilder.Append("fuse_flag=" + dt.Rows[0]["fuse_flag"].ToString() + ",");
			stringBuilder.Append("fhelp='" + dt.Rows[0]["fhelp"].ToString() + "',");
			if (dt.Rows[0]["fcj_flag"].ToString() == "" || dt.Rows[0]["fcj_flag"].ToString() == null)
			{
				stringBuilder.Append("fcj_flag=0,");
			}
			else
			{
				stringBuilder.Append("fcj_flag='" + dt.Rows[0]["fcj_flag"].ToString() + "',");
			}
			stringBuilder.Append("fcj_name='" + dt.Rows[0]["fcj_name"].ToString() + "',");
			stringBuilder.Append("fcj_zp='" + dt.Rows[0]["fcj_zp"].ToString() + "',");
			stringBuilder.Append("fopen_mode='" + dt.Rows[0]["fopen_mode"].ToString() + "',");
			stringBuilder.Append("fremark='" + dt.Rows[0]["fremark"].ToString() + "'");
			stringBuilder.Append(" where ffunc_id='" + dt.Rows[0]["ffunc_id"].ToString() + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllAdd(System.Data.DataTable dt)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into wwf_func(");
			stringBuilder.Append("ffunc_id,fp_id,fname,fname_e,ficon,ffunc_winform,ffunc_web,forder_by,fuse_flag,fhelp,fcj_flag,fcj_name,fcj_zp,fopen_mode,fremark");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + dt.Rows[0]["ffunc_id"].ToString() + "',");
			stringBuilder.Append("'" + dt.Rows[0]["fp_id"].ToString() + "',");
			stringBuilder.Append("'" + dt.Rows[0]["fname"].ToString() + "',");
			stringBuilder.Append("'" + dt.Rows[0]["fname_e"].ToString() + "',");
			stringBuilder.Append("'" + dt.Rows[0]["ficon"].ToString() + "',");
			stringBuilder.Append("'" + dt.Rows[0]["ffunc_winform"].ToString() + "',");
			stringBuilder.Append("'" + dt.Rows[0]["ffunc_web"].ToString() + "',");
			stringBuilder.Append("'" + dt.Rows[0]["forder_by"].ToString() + "',");
			stringBuilder.Append(dt.Rows[0]["fuse_flag"].ToString() + ",");
			stringBuilder.Append("'" + dt.Rows[0]["fhelp"].ToString() + "',");
			stringBuilder.Append(dt.Rows[0]["fcj_flag"].ToString() + ",");
			stringBuilder.Append("'" + dt.Rows[0]["fcj_name"].ToString() + "',");
			stringBuilder.Append("'" + dt.Rows[0]["fcj_zp"].ToString() + "',");
			stringBuilder.Append("'" + dt.Rows[0]["fopen_mode"].ToString() + "',");
			stringBuilder.Append("'" + dt.Rows[0]["fremark"].ToString() + "'");
			stringBuilder.Append(")");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public bool BllDelete(string id)
		{
			bool result;
			if (id == "-1" || id == "0")
			{
				result = false;
			}
			else
			{
				string strSql = "delete FROM wwf_func where ffunc_id='" + id + "'";
				string strSql2 = "delete FROM wwf_org_func where ffunc_id='" + id + "'";
				result = WWFInit.wwfRemotingDao.DbExecuteNonQueryTransaction2(WWFInit.strDBConn, strSql, strSql2);
			}
			return result;
		}

		public bool BllChildExists(string id)
		{
			return WWFInit.wwfRemotingDao.DbRecordExists(WWFInit.strDBConn, "SELECT COUNT(1) FROM wwf_func WHERE (fp_id = '" + id + "')");
		}
	}
}
