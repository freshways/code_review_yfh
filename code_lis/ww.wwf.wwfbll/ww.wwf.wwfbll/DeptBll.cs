using System;
using System.Data;
using System.Text;

namespace ww.wwf.wwfbll
{
	public class DeptBll : DAOWWF
	{
		public System.Data.DataTable BllDeptDTByCode(string strfcode, string strfname)
		{
			string strSql = string.Concat(new string[]
			{
				"SELECT * FROM wwf_dept where  (fdept_id='",
				strfcode,
				"' or fname ='",
				strfname,
				"')"
			});
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public System.Data.DataTable BllDeptDT()
		{
			string strSql = "SELECT ('['+fdept_id+']'+fname) as fname_show,* FROM wwf_dept ORDER BY forder_by";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public string BllDeptName(string id)
		{
			string strSql = "SELECT fname FROM wwf_dept where fdept_id='" + id + "'";
			return (string)WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(WWFInit.strDBConn, strSql);
		}

		public string Bllfp_id(string id)
		{
			string strSql = "SELECT fp_id FROM wwf_dept where fdept_id='" + id + "'";
			return (string)WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(WWFInit.strDBConn, strSql);
		}

		public string BllPersonName(string id)
		{
			string strSql = "SELECT fname FROM WWF_PERSON where fperson_id='" + id + "'";
			return WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(WWFInit.strDBConn, strSql).ToString();
		}

		public System.Data.DataTable BllDeptDTuse()
		{
			string strSql = "SELECT ('['+fdept_id+']'+fname) as fname_show, * FROM wwf_dept  where fuse_flag=1 ORDER BY forder_by";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public System.Data.DataTable BllDeptDTuse(string strname)
		{
			string strSql = "SELECT ('['+fdept_id+']'+fname) as fname_show, * FROM wwf_dept  where fuse_flag=1 and fremark like '%" + strname + "%' ORDER BY forder_by";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public System.Data.DataTable BllDeptDT(string fp_id)
		{
			string strSql = "SELECT * FROM wwf_dept where fp_id='" + fp_id + "' ORDER BY forder_by";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public System.Data.DataTable BllDeptDT(string fp_id, int fuse_flag)
		{
			string strSql = string.Concat(new object[]
			{
				"SELECT * FROM wwf_dept where fp_id='",
				fp_id,
				"' and fuse_flag=",
				fuse_flag,
				" ORDER BY forder_by"
			});
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public System.Data.DataTable BllDeptDTByID(string fdept_id)
		{
			string strSql = "SELECT * FROM wwf_dept where fdept_id='" + fdept_id + "'";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public int BllUpdate(System.Data.DataTable dt, string strid)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update wwf_dept set ");
			stringBuilder.Append("fdept_id='" + dt.Rows[0]["fdept_id"].ToString() + "',");
			stringBuilder.Append("fp_id='" + dt.Rows[0]["fp_id"].ToString() + "',");
			stringBuilder.Append("fname='" + dt.Rows[0]["fname"].ToString() + "',");
			stringBuilder.Append("fname_e='" + dt.Rows[0]["fname_e"].ToString() + "',");
			stringBuilder.Append("forder_by='" + dt.Rows[0]["forder_by"].ToString() + "',");
			stringBuilder.Append("fuse_flag=" + dt.Rows[0]["fuse_flag"].ToString() + ",");
			stringBuilder.Append("fremark='" + dt.Rows[0]["fremark"].ToString() + "'");
			stringBuilder.Append(" where fdept_id='" + strid + "'");
			StringBuilder stringBuilder2 = new StringBuilder();
			stringBuilder2.Append("update wwf_org set ");
			stringBuilder2.Append("fname='" + dt.Rows[0]["fname"].ToString() + "'");
			stringBuilder2.Append(" where forg_id='" + strid + "'");
			int result;
			if (WWFInit.wwfRemotingDao.DbExecuteNonQueryTransaction2(WWFInit.strDBConn, stringBuilder.ToString(), stringBuilder2.ToString()))
			{
				result = 1;
			}
			else
			{
				result = 0;
			}
			return result;
		}

		public int BllAdd(System.Data.DataTable dt)
		{
			int result = 0;
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into wwf_dept(");
			stringBuilder.Append("fdept_id,fp_id,fname,fname_e,forder_by,fuse_flag,fremark");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + dt.Rows[0]["fdept_id"].ToString() + "',");
			stringBuilder.Append("'" + dt.Rows[0]["fp_id"].ToString() + "',");
			stringBuilder.Append("'" + dt.Rows[0]["fname"].ToString() + "',");
			stringBuilder.Append("'" + dt.Rows[0]["fname_e"].ToString() + "',");
			stringBuilder.Append("'" + dt.Rows[0]["forder_by"].ToString() + "',");
			stringBuilder.Append(dt.Rows[0]["fuse_flag"].ToString() + ",");
			stringBuilder.Append("'" + dt.Rows[0]["fremark"].ToString() + "'");
			stringBuilder.Append(")");
			StringBuilder stringBuilder2 = new StringBuilder();
			stringBuilder2.Append("insert into wwf_org(");
			stringBuilder2.Append("forg_id,ftype,fp_id,fdept_id,fname,fname_e,forder_by,fuse_flag,fremark");
			stringBuilder2.Append(")");
			stringBuilder2.Append(" values (");
			stringBuilder2.Append("'" + dt.Rows[0]["fdept_id"].ToString() + "',");
			stringBuilder2.Append("'dept',");
			stringBuilder2.Append("'" + dt.Rows[0]["fp_id"].ToString() + "',");
			stringBuilder2.Append("'" + dt.Rows[0]["fdept_id"].ToString() + "',");
			stringBuilder2.Append("'" + dt.Rows[0]["fname"].ToString() + "',");
			stringBuilder2.Append("'" + dt.Rows[0]["fname_e"].ToString() + "',");
			stringBuilder2.Append("'" + dt.Rows[0]["forder_by"].ToString() + "',");
			stringBuilder2.Append(dt.Rows[0]["fuse_flag"].ToString() + ",");
			stringBuilder2.Append("'" + dt.Rows[0]["fremark"].ToString() + "'");
			stringBuilder2.Append(")");
			if (WWFInit.wwfRemotingDao.DbExecuteNonQueryTransaction2(WWFInit.strDBConn, stringBuilder.ToString(), stringBuilder2.ToString()))
			{
				result = 1;
			}
			return result;
		}

		public int BllDelete(string id)
		{
			string strSql = "delete FROM wwf_dept where fdept_id='" + id + "'";
			string strSql2 = "delete FROM wwf_org where forg_id='" + id + "'";
			int result;
			if (WWFInit.wwfRemotingDao.DbExecuteNonQueryTransaction2(WWFInit.strDBConn, strSql, strSql2))
			{
				result = 1;
			}
			else
			{
				result = 0;
			}
			return result;
		}

		public bool BllChildExists(string id)
		{
			return WWFInit.wwfRemotingDao.DbRecordExists(WWFInit.strDBConn, "SELECT COUNT(1) FROM wwf_dept WHERE (fp_id = '" + id + "')");
		}

		public string BllDeptNameByID(string id)
		{
			return WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(WWFInit.strDBConn, "SELECT fname FROM wwf_dept WHERE (fdept_id = '" + id + "')").ToString();
		}
	}
}
