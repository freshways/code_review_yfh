using System;
using System.Windows.Forms;

namespace ww.wwf.wwfbll
{
	public class WWMessage
	{
		private static string strMessTitle = "";

		public static void MessageShowInformation(string message)
		{
			MessageBox.Show(message, WWMessage.strMessTitle + " 提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
		}

		public static void MessageShowError(string message)
		{
			MessageBox.Show(message, WWMessage.strMessTitle + " 错误", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		}

		public static void MessageShowWarning(string message)
		{
			MessageBox.Show(message, WWMessage.strMessTitle + " 提示", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
		}

		public static bool MessageDialogResult(string message)
		{
			bool result = false;
			if (MessageBox.Show(message, WWMessage.strMessTitle + " 请选择", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
			{
				result = true;
			}
			return result;
		}
	}
}
