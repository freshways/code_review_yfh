using System;
using System.Collections;
using System.Globalization;

namespace ww.wwf.wwfbll
{
	public class FormulaGenerator
	{
		private static int m_iCharContor = 0;

		private static string m_strservice = "";

		public static string EvaluationResult(string astrexpr)
		{
			string result;
			try
			{
				if (astrexpr == "")
				{
					astrexpr = "0";
				}
				FormulaGenerator.EvalExpr(astrexpr.Replace(" ", ""));
				result = FormulaGenerator.m_strservice;
			}
			catch
			{
				result = "";
			}
			return result;
		}

		private static void EvalExpr(string astrexpr)
		{
			if (FormulaGenerator.ExistParenthesis(astrexpr))
			{
				int num = FormulaGenerator.PosMaxLevel(astrexpr);
				string text = astrexpr.Substring(num + 1);
				int num2 = FormulaGenerator.FindFirstChar(text, ')');
				string text2 = text.Substring(0, num2);
				FormulaGenerator.EvalAritm(text2.Trim());
				string str = astrexpr.Substring(0, num);
				string str2 = astrexpr.Substring(num + num2 + 2);
				FormulaGenerator.EvalExpr(str + FormulaGenerator.m_strservice + str2);
			}
			else
			{
				FormulaGenerator.EvalAritm(astrexpr.Trim());
			}
		}

		private static bool ExistParenthesis(string astrexpr)
		{
			FormulaGenerator.m_iCharContor = 0;
			FormulaGenerator.NbOfChar(astrexpr, '(');
			int iCharContor = FormulaGenerator.m_iCharContor;
			FormulaGenerator.m_iCharContor = 0;
			FormulaGenerator.NbOfChar(astrexpr, ')');
			int iCharContor2 = FormulaGenerator.m_iCharContor;
			if (iCharContor != iCharContor2)
			{
				throw new Exception("Sintax Error!");
			}
			return iCharContor != 0;
		}

		private static int PosMaxLevel(string astrexpr)
		{
			int num = -1;
			int num2 = 0;
			int num3 = FormulaGenerator.LevelOfParenthesis(astrexpr);
			char[] array = astrexpr.ToCharArray();
			int result;
			for (int i = 0; i < array.Length; i++)
			{
				char c = array[i];
				num++;
				if (c == '(')
				{
					num2++;
					if (num2 == num3)
					{
						result = num;
						return result;
					}
				}
				else if (c == ')')
				{
					num2--;
				}
			}
			result = 0;
			return result;
		}

		private static int LevelOfParenthesis(string astrexpr)
		{
			int num = 0;
			int num2 = -1;
			int num3 = 0;
			char[] array = astrexpr.ToCharArray();
			for (int i = 0; i < array.Length; i++)
			{
				char c = array[i];
				num2++;
				if (c == '(')
				{
					num++;
				}
				else if (c == ')')
				{
					num--;
				}
				if (num3 < num)
				{
					num3 = num;
				}
			}
			return num3;
		}

		private static int FindLastChar(string astrexpr, char acfinder)
		{
			return astrexpr.LastIndexOf(acfinder);
		}

		private static int FindFirstChar(string astrexpr, char acfinder)
		{
			return astrexpr.IndexOf(acfinder);
		}

		private static int FindSecondChar(string astrexpr, char acfinder)
		{
			int num = astrexpr.IndexOf(acfinder);
			int num2 = astrexpr.Substring(num + 1).IndexOf(acfinder);
			return num + num2 + 1;
		}

		private static void NbOfChar(string astrexpr, char acfinder)
		{
			int num = astrexpr.IndexOf(acfinder);
			if (num >= 0)
			{
				FormulaGenerator.m_iCharContor++;
				FormulaGenerator.NbOfChar(astrexpr.Substring(num + 1), acfinder);
			}
		}

		private static void EvalAritm(string astrexpr)
		{
			FormulaGenerator.ProcessPercent(astrexpr);
			string strservice = FormulaGenerator.m_strservice;
			FormulaGenerator.ProcessDivision(strservice);
			string strservice2 = FormulaGenerator.m_strservice;
			FormulaGenerator.ProcessMultiply(strservice2);
			string strservice3 = FormulaGenerator.m_strservice;
			FormulaGenerator.ProcessMinus(strservice3);
			string strservice4 = FormulaGenerator.m_strservice;
			FormulaGenerator.ProcessPlus(strservice4);
			string strservice5 = FormulaGenerator.m_strservice;
		}

		private static void ProcessPercent(string astrexpr)
		{
			FormulaGenerator.m_iCharContor = 0;
			FormulaGenerator.NbOfChar(astrexpr, '%');
			int iCharContor = FormulaGenerator.m_iCharContor;
			string text = astrexpr.Trim();
			FormulaGenerator.m_iCharContor = 0;
			FormulaGenerator.NbOfChar(text, '%');
			if (FormulaGenerator.m_iCharContor == 0)
			{
				FormulaGenerator.m_strservice = text;
			}
			if (iCharContor > 0)
			{
				string text2 = astrexpr.Trim();
				int num = FormulaGenerator.FindFirstChar(text2, '%');
				if (num < 1)
				{
					throw new Exception("Sintax error!");
				}
				string text3 = text2.Substring(0, num);
				int num2 = FormulaGenerator.FindLastChar(text3, '/');
				int num3 = FormulaGenerator.FindLastChar(text3, '*');
				int num4 = FormulaGenerator.FindLastChar(text3, '-');
				int num5 = FormulaGenerator.FindLastChar(text3, '+');
				int num6 = FormulaGenerator.NbMax(new ArrayList
				{
					num2,
					num3,
					num4,
					num5
				});
				if (num6 < 0)
				{
					num6 = 0;
				}
				string astrexpr2;
				string str;
				if (num6 >= 1)
				{
					astrexpr2 = text3.Substring(num6 + 1).Trim();
					str = text3.Substring(0, num6 + 1).Trim();
				}
				else
				{
					astrexpr2 = text3.Trim();
					str = "";
				}
				string str2 = "";
				if (num < text2.Length - 1)
				{
					str2 = text2.Substring(num + 1).Trim();
				}
				decimal value = FormulaGenerator.ConvertToValue(astrexpr2);
				text = str + Convert.ToString(value) + str2;
				FormulaGenerator.ProcessPercent(text);
			}
		}

		private static void ProcessDivision(string astrexpr)
		{
			FormulaGenerator.m_iCharContor = 0;
			FormulaGenerator.NbOfChar(astrexpr, '/');
			int iCharContor = FormulaGenerator.m_iCharContor;
			string text = astrexpr.Trim();
			FormulaGenerator.m_iCharContor = 0;
			FormulaGenerator.NbOfChar(text, '/');
			if (FormulaGenerator.m_iCharContor == 0)
			{
				FormulaGenerator.m_strservice = text;
			}
			if (iCharContor > 0)
			{
				string text2 = astrexpr.Trim();
				int num = FormulaGenerator.FindFirstChar(text2, '/');
				if (num < 1)
				{
					throw new Exception("Division error!");
				}
				string text3 = text2.Substring(0, num).Trim();
				if (num >= text2.Length - 1)
				{
					throw new Exception("Division error!");
				}
				string text4 = text2.Substring(num + 1).Trim();
				int num2 = FormulaGenerator.FindLastChar(text3, '/');
				int num3 = FormulaGenerator.FindLastChar(text3, '*');
				int num4 = FormulaGenerator.FindLastChar(text3, '-');
				int num5 = FormulaGenerator.FindLastChar(text3, '+');
				int num6 = FormulaGenerator.FindFirstChar(text4, '/');
				int num7 = FormulaGenerator.FindFirstChar(text4, '*');
				int num8 = FormulaGenerator.FindFirstChar(text4, '-');
				int num9 = FormulaGenerator.FindFirstChar(text4, '+');
				int num10 = FormulaGenerator.NbMax(new ArrayList
				{
					num2,
					num3,
					num4,
					num5
				});
				if (num10 <= 0)
				{
					num10 = -1;
				}
				int num11 = FormulaGenerator.NbMin(new ArrayList
				{
					num6,
					num7,
					num8,
					num9
				});
				if (num11 <= 0)
				{
					num11 = 0;
				}
				string text5 = text3.Substring(num10 + 1).Trim();
				string text6 = text3.Substring(0, num10 + 1).Trim();
				string text7 = text4.Substring(0, num11).Trim();
				string text8 = text4.Substring(num11).Trim();
				bool flag = false;
				if (text6.Length > 0)
				{
					if (text6.Substring(0, 1).Trim() == "-")
					{
						flag = true;
					}
				}
				bool flag2 = false;
				if (text7.Length > 0)
				{
					if (text7.Substring(0, 1).Trim() == "-")
					{
						flag2 = true;
					}
				}
				decimal num12;
				if (text5.Length > 0)
				{
					num12 = Convert.ToDecimal(FormulaGenerator.SetDecimalSeparator(text5));
				}
				else if (text6.Length > 0)
				{
					num12 = Convert.ToDecimal(FormulaGenerator.SetDecimalSeparator(text6));
				}
				else
				{
					num12 = 0m;
				}
				decimal num13;
				if (text7.Length > 0)
				{
					num13 = Convert.ToDecimal(FormulaGenerator.SetDecimalSeparator(text7));
				}
				else if (text8.Length > 0)
				{
					num13 = Convert.ToDecimal(FormulaGenerator.SetDecimalSeparator(text8));
				}
				else
				{
					num13 = 1m;
				}
				decimal value = 0m;
				if (num13 != 0m)
				{
					if (flag || flag2)
					{
						value = -1m * num12 / num13;
					}
					else
					{
						value = num12 / num13;
					}
				}
				text = text6 + Convert.ToString(value) + text8;
				if (text7 == "")
				{
					text = text6 + Convert.ToString(value);
				}
				FormulaGenerator.ProcessDivision(text);
			}
		}

		private static void ProcessMultiply(string astrexpr)
		{
			FormulaGenerator.m_iCharContor = 0;
			FormulaGenerator.NbOfChar(astrexpr, '*');
			int iCharContor = FormulaGenerator.m_iCharContor;
			string text = astrexpr.Trim();
			FormulaGenerator.m_iCharContor = 0;
			FormulaGenerator.NbOfChar(text, '*');
			if (FormulaGenerator.m_iCharContor == 0)
			{
				FormulaGenerator.m_strservice = text;
			}
			if (iCharContor > 0)
			{
				string text2 = astrexpr.Trim();
				int num = FormulaGenerator.FindFirstChar(text2, '*');
				if (num < 1)
				{
					throw new Exception("Error!");
				}
				string text3 = text2.Substring(0, num).Trim();
				if (num >= text2.Length - 1)
				{
					throw new Exception("Error!");
				}
				string text4 = text2.Substring(num + 1).Trim();
				int num2 = FormulaGenerator.FindLastChar(text3, '/');
				int num3 = FormulaGenerator.FindLastChar(text3, '*');
				int num4 = FormulaGenerator.FindLastChar(text3, '-');
				int num5 = FormulaGenerator.FindLastChar(text3, '+');
				int num6 = FormulaGenerator.FindFirstChar(text4, '/');
				int num7 = FormulaGenerator.FindFirstChar(text4, '*');
				int num8 = FormulaGenerator.FindFirstChar(text4, '-');
				int num9 = FormulaGenerator.FindFirstChar(text4, '+');
				int num10 = FormulaGenerator.NbMax(new ArrayList
				{
					num2,
					num3,
					num4,
					num5
				});
				if (num10 <= 0)
				{
					num10 = -1;
				}
				int num11 = FormulaGenerator.NbMin(new ArrayList
				{
					num6,
					num7,
					num8,
					num9
				});
				if (num11 <= 0)
				{
					num11 = 0;
				}
				string text5 = text3.Substring(num10 + 1).Trim();
				string text6 = text3.Substring(0, num10 + 1).Trim();
				string text7 = text4.Substring(0, num11).Trim();
				string text8 = text4.Substring(num11).Trim();
				bool flag = false;
				if (text6.Length > 0)
				{
					if (text6.Substring(0, 1).Trim() == "-")
					{
					}
				}
				bool flag2 = false;
				if (text7.Length > 0)
				{
					if (text7.Substring(0, 1).Trim() == "-")
					{
						flag2 = true;
					}
				}
				decimal num12;
				if (text5.Length > 0)
				{
					if (FormulaGenerator.IsDigit(text5))
					{
						num12 = Convert.ToDecimal(FormulaGenerator.SetDecimalSeparator(text5));
					}
					else
					{
						num12 = FormulaGenerator.ConvertToValue(text5);
					}
				}
				else if (text6.Length > 0)
				{
					if (FormulaGenerator.IsDigit(text6))
					{
						num12 = Convert.ToDecimal(FormulaGenerator.SetDecimalSeparator(text6));
					}
					else
					{
						num12 = FormulaGenerator.ConvertToValue(text6);
					}
				}
				else
				{
					num12 = 0m;
				}
				decimal num13;
				if (text7.Length > 0)
				{
					if (FormulaGenerator.IsDigit(text7))
					{
						num13 = Convert.ToDecimal(FormulaGenerator.SetDecimalSeparator(text7));
					}
					else
					{
						num13 = FormulaGenerator.ConvertToValue(text7);
					}
				}
				else if (text8.Length > 0)
				{
					if (FormulaGenerator.IsDigit(text8))
					{
						num13 = Convert.ToDecimal(FormulaGenerator.SetDecimalSeparator(text8));
					}
					else
					{
						num13 = FormulaGenerator.ConvertToValue(text8);
					}
				}
				else
				{
					num13 = 0m;
				}
				decimal value = 0m;
				if (num13 != 0m)
				{
					if (flag || flag2)
					{
						value = -1m * num12 * num13;
					}
					else
					{
						value = num12 * num13;
					}
				}
				text = text6 + Convert.ToString(value) + text8;
				if (text7 == "")
				{
					text = text6 + Convert.ToString(value);
				}
				FormulaGenerator.ProcessMultiply(text);
			}
		}

		private static void ProcessMinus(string astrexpr)
		{
			FormulaGenerator.m_iCharContor = 0;
			FormulaGenerator.NbOfChar(astrexpr, '-');
			int num = FormulaGenerator.FindFirstChar(astrexpr.Trim(), '-');
			string text = astrexpr.Trim();
			FormulaGenerator.m_iCharContor = 0;
			FormulaGenerator.NbOfChar(text, '-');
			if (num == 0)
			{
				FormulaGenerator.m_iCharContor--;
			}
			int iCharContor = FormulaGenerator.m_iCharContor;
			if (FormulaGenerator.m_iCharContor == 0)
			{
				FormulaGenerator.m_strservice = text;
			}
			if (iCharContor > 0)
			{
				string text2 = astrexpr.Trim();
				int num2 = FormulaGenerator.FindFirstChar(text2, '-');
				if (num2 == 0)
				{
					num2 = FormulaGenerator.FindSecondChar(text2, '-');
				}
				string text3;
				if (num2 >= 1)
				{
					text3 = text2.Substring(0, num2).Trim();
				}
				else
				{
					text3 = text2.Substring(0, num2).Trim();
				}
				if (num2 >= text2.Length - 1)
				{
					throw new Exception("Error!");
				}
				string text4 = text2.Substring(num2 + 1).Trim();
				int num3 = FormulaGenerator.FindLastChar(text3, '/');
				int num4 = FormulaGenerator.FindLastChar(text3, '*');
				int num5 = FormulaGenerator.FindLastChar(text3, '-');
				int num6 = FormulaGenerator.FindLastChar(text3, '+');
				int num7 = FormulaGenerator.FindFirstChar(text4, '/');
				int num8 = FormulaGenerator.FindFirstChar(text4, '*');
				int num9 = FormulaGenerator.FindFirstChar(text4, '-');
				int num10 = FormulaGenerator.FindFirstChar(text4, '+');
				int num11 = FormulaGenerator.NbMax(new ArrayList
				{
					num3,
					num4,
					num5,
					num6
				});
				if (num11 <= 0)
				{
					num11 = -1;
				}
				int num12 = FormulaGenerator.NbMin(new ArrayList
				{
					num7,
					num8,
					num9,
					num10
				});
				if (num12 <= 0)
				{
					num12 = 0;
				}
				string text5 = text3.Substring(num11 + 1).Trim();
				string text6 = text3.Substring(0, num11 + 1).Trim();
				string text7 = text4.Substring(0, num12).Trim();
				string text8 = text4.Substring(num12).Trim();
				decimal d = 0m;
				if (text5.Length > 0)
				{
					if (FormulaGenerator.IsDigit(text5))
					{
						d = Convert.ToDecimal(FormulaGenerator.SetDecimalSeparator(text5));
					}
					else
					{
						d = FormulaGenerator.ConvertToValue(text5);
					}
				}
				else if (text6.Length > 0)
				{
					if (FormulaGenerator.IsDigit(text6))
					{
						d = Convert.ToDecimal(FormulaGenerator.SetDecimalSeparator(text6));
					}
					else
					{
						d = FormulaGenerator.ConvertToValue(text6);
					}
				}
				decimal d2 = 0m;
				if (text7.Length > 0)
				{
					if (FormulaGenerator.IsDigit(text7))
					{
						d2 = Convert.ToDecimal(FormulaGenerator.SetDecimalSeparator(text7));
					}
					else
					{
						d2 = FormulaGenerator.ConvertToValue(text7);
					}
				}
				else if (text8.Length > 0)
				{
					if (FormulaGenerator.IsDigit(text8))
					{
						d2 = Convert.ToDecimal(FormulaGenerator.SetDecimalSeparator(text8));
					}
					else
					{
						d2 = FormulaGenerator.ConvertToValue(text8);
					}
				}
				decimal value = d - d2;
				text = text6 + Convert.ToString(value) + text8;
				if (text7 == "")
				{
					text = text6 + Convert.ToString(value);
				}
				FormulaGenerator.ProcessMinus(text);
			}
		}

		private static void ProcessPlus(string astrexpr)
		{
			FormulaGenerator.m_iCharContor = 0;
			FormulaGenerator.NbOfChar(astrexpr, '+');
			int iCharContor = FormulaGenerator.m_iCharContor;
			string text = astrexpr.Trim();
			FormulaGenerator.m_iCharContor = 0;
			FormulaGenerator.NbOfChar(text, '+');
			if (FormulaGenerator.m_iCharContor == 0)
			{
				FormulaGenerator.m_strservice = text;
			}
			if (iCharContor > 0)
			{
				string text2 = astrexpr.Trim();
				int num = FormulaGenerator.FindFirstChar(text2, '+');
				if (num < 1)
				{
					throw new Exception("Error!");
				}
				string text3 = text2.Substring(0, num).Trim();
				if (num >= text2.Length - 1)
				{
					throw new Exception("Error!");
				}
				string text4 = text2.Substring(num + 1).Trim();
				int num2 = FormulaGenerator.FindLastChar(text3, '/');
				int num3 = FormulaGenerator.FindLastChar(text3, '*');
				int num4 = FormulaGenerator.FindLastChar(text3, '-');
				int num5 = FormulaGenerator.FindLastChar(text3, '+');
				int num6 = FormulaGenerator.FindFirstChar(text4, '/');
				int num7 = FormulaGenerator.FindFirstChar(text4, '*');
				int num8 = FormulaGenerator.FindFirstChar(text4, '-');
				int num9 = FormulaGenerator.FindFirstChar(text4, '+');
				int num10 = FormulaGenerator.NbMax(new ArrayList
				{
					num2,
					num3,
					num4,
					num5
				});
				if (num10 <= 0)
				{
					num10 = -1;
				}
				int num11 = FormulaGenerator.NbMin(new ArrayList
				{
					num6,
					num7,
					num8,
					num9
				});
				if (num11 <= 0)
				{
					num11 = 0;
				}
				string text5 = text3.Substring(num10 + 1).Trim();
				string text6 = text3.Substring(0, num10 + 1).Trim();
				string text7 = text4.Substring(0, num11).Trim();
				string text8 = text4.Substring(num11).Trim();
				decimal d = 0m;
				if (text5.Length > 0)
				{
					if (FormulaGenerator.IsDigit(text5))
					{
						d = Convert.ToDecimal(FormulaGenerator.SetDecimalSeparator(text5));
					}
					else
					{
						d = FormulaGenerator.ConvertToValue(text5);
					}
				}
				else if (text6.Length > 0)
				{
					if (FormulaGenerator.IsDigit(text6))
					{
						d = Convert.ToDecimal(FormulaGenerator.SetDecimalSeparator(text6));
					}
					else
					{
						d = FormulaGenerator.ConvertToValue(text6);
					}
				}
				decimal d2 = 0m;
				if (text7.Length > 0)
				{
					if (FormulaGenerator.IsDigit(text7))
					{
						d2 = Convert.ToDecimal(FormulaGenerator.SetDecimalSeparator(text7));
					}
					else
					{
						d2 = FormulaGenerator.ConvertToValue(text7);
					}
				}
				else if (text8.Length > 0)
				{
					if (FormulaGenerator.IsDigit(text8))
					{
						d2 = Convert.ToDecimal(FormulaGenerator.SetDecimalSeparator(text8));
					}
					else
					{
						d2 = FormulaGenerator.ConvertToValue(text8);
					}
				}
				decimal value = d + d2;
				text = text6 + Convert.ToString(value) + text8;
				if (text7 == "")
				{
					text = text6 + Convert.ToString(value);
				}
				FormulaGenerator.ProcessPlus(text);
			}
			else if (!FormulaGenerator.IsDigit(astrexpr))
			{
				FormulaGenerator.ConvertToValue(astrexpr);
			}
		}

		private static decimal ConvertToValue(string astrexpr)
		{
			bool flag = true;
			for (int i = 0; i <= astrexpr.Length - 1; i++)
			{
				string text = astrexpr.Substring(i, 1);
				if (i != 0 || text != "-")
				{
					char[] array = text.ToCharArray();
					if (!char.IsDigit(array[0]) && text != "." && text != ",")
					{
						flag = false;
					}
				}
			}
			if (flag)
			{
				return Convert.ToDecimal(astrexpr) / 100m;
			}
			throw new Exception("Not numeric value!");
		}

		private static int NbMax(ArrayList aobjarl)
		{
			int num = 0;
			foreach (object current in aobjarl)
			{
				if ((int)current != -1)
				{
					if (num <= (int)current)
					{
						num = (int)current;
					}
				}
			}
			return num;
		}

		private static int NbMin(ArrayList aobjarl)
		{
			int num = FormulaGenerator.NbMax(aobjarl);
			foreach (object current in aobjarl)
			{
				if ((int)current != -1)
				{
					if (num >= (int)current)
					{
						num = (int)current;
					}
				}
			}
			return num;
		}

		private static bool IsDigit(string astrexpr)
		{
			char[] array = astrexpr.ToCharArray();
			bool result;
			for (int i = 0; i < array.Length; i++)
			{
				char c = array[i];
				if (c != '-' && c != ' ' && c != '.' && c != ',')
				{
					if (!char.IsDigit(c))
					{
						result = false;
						return result;
					}
				}
			}
			result = true;
			return result;
		}

		private static string SetDecimalSeparator(string astrexpr)
		{
			string numberDecimalSeparator = NumberFormatInfo.CurrentInfo.NumberDecimalSeparator;
			int num = astrexpr.IndexOf('.');
			int num2 = astrexpr.IndexOf(',');
			string result;
			if (num >= 0)
			{
				result = astrexpr.Replace(".", numberDecimalSeparator);
			}
			else if (num2 >= 0)
			{
				result = astrexpr.Replace(",", numberDecimalSeparator);
			}
			else
			{
				result = astrexpr;
			}
			return result;
		}
	}
}
