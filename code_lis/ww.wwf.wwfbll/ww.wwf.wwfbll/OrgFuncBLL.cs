using System;
using System.Data;
using System.Text;

namespace ww.wwf.wwfbll
{
	public class OrgFuncBLL : DAOWWF
	{
		public System.Data.DataTable BllDTAll()
		{
			string strSql = "SELECT *  FROM wwf_org ORDER BY forder_by";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public string Bllftype(string forg_id)
		{
			string strSql = "SELECT ftype FROM wwf_org WHERE (forg_id = '" + forg_id + "')";
			return WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(WWFInit.strDBConn, strSql).ToString();
		}

		public System.Data.DataTable BllFuncOK(string forg_id)
		{
			string strSql = "SELECT t1.ffunc_id, t1.fp_id, t1.fname FROM wwf_func t1,wwf_org_func t2 where t1.ffunc_id = t2.ffunc_id and t2.forg_id = '" + forg_id + "'";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public int BllOkFuncDelete(string forg_id, string ffunc_id)
		{
			string strSql = string.Concat(new string[]
			{
				"delete FROM wwf_org_func where forg_id='",
				forg_id,
				"' and ffunc_id='",
				ffunc_id,
				"'"
			});
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, strSql);
		}

		public void BllOkFuncInsert(string forg_id, string ffunc_id)
		{
			if (!WWFInit.wwfRemotingDao.DbRecordExists(WWFInit.strDBConn, string.Concat(new string[]
			{
				"SELECT COUNT(1) FROM wwf_org_func WHERE (forg_id = '",
				forg_id,
				"') AND (ffunc_id = '",
				ffunc_id,
				"')"
			})))
			{
				string strSql = string.Concat(new string[]
				{
					"INSERT INTO wwf_org_func(fid, forg_id, ffunc_id) VALUES ('",
					base.DbGuid(),
					"', '",
					forg_id,
					"', '",
					ffunc_id,
					"')"
				});
				WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, strSql);
			}
		}

		public System.Data.DataTable BllPositionOK(string forg_id)
		{
			string strSql = "SELECT *  FROM wwf_org where fp_id='" + forg_id + "' and ftype='position' ORDER BY forder_by";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public System.Data.DataTable BllPositionNO()
		{
			string strSql = "SELECT * FROM WWF_POSITION WHERE (fuse_flag = 1)";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public System.Data.DataTable BllPersonOK(string forg_id)
		{
			string strSql = "SELECT forg_id, (select fshow_name from wwf_person t1 where t1.fperson_id=t.fperson_id) AS PersonName FROM wwf_org t WHERE (fp_id = '" + forg_id + "') AND (ftype = 'person') ORDER BY fname";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public void BllFP(string fposition_id, string fp_id)
		{
			System.Data.DataTable dataTable = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, "SELECT * FROM WWF_POSITION WHERE (fposition_id = '" + fposition_id + "')").Tables[0];
			if (!WWFInit.wwfRemotingDao.DbRecordExists(WWFInit.strDBConn, string.Concat(new string[]
			{
				"SELECT COUNT(1) FROM wwf_org WHERE (ftype = 'position') AND (fposition_id = '",
				fposition_id,
				"') AND (fp_id = '",
				fp_id,
				"')"
			})))
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append("insert into wwf_org(");
				stringBuilder.Append("forg_id,ftype,fp_id,fname,fposition_id,fdept_id,fuse_flag,fremark");
				stringBuilder.Append(")");
				stringBuilder.Append(" values (");
				stringBuilder.Append("'" + base.DbGuid() + "',");
				stringBuilder.Append("'position',");
				stringBuilder.Append("'" + fp_id + "',");
				stringBuilder.Append("'" + dataTable.Rows[0]["fname"].ToString() + "',");
				stringBuilder.Append("'" + dataTable.Rows[0]["fposition_id"].ToString() + "',");
				stringBuilder.Append("'" + fp_id + "',");
				stringBuilder.Append("'1',");
				stringBuilder.Append("''");
				stringBuilder.Append(")");
				WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
			}
		}

		public void BllFPPerson(string fperson_id, string fp_id)
		{
			string str = WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(WWFInit.strDBConn, "SELECT fdept_id FROM wwf_org WHERE (forg_id = '" + fp_id + "')").ToString();
			string str2 = WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(WWFInit.strDBConn, "SELECT fposition_id FROM wwf_org WHERE (forg_id = '" + fp_id + "')").ToString();
			System.Data.DataTable dataTable = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, "SELECT * FROM wwf_person WHERE (fperson_id = '" + fperson_id + "')").Tables[0];
			if (!WWFInit.wwfRemotingDao.DbRecordExists(WWFInit.strDBConn, string.Concat(new string[]
			{
				"SELECT COUNT(1) FROM wwf_org WHERE (ftype = 'person') AND (fperson_id = '",
				fperson_id,
				"') AND (fp_id = '",
				fp_id,
				"')"
			})))
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append("insert into wwf_org(");
				stringBuilder.Append("forg_id,ftype,fp_id,fname,fperson_id,fposition_id,fdept_id,fuse_flag,fremark");
				stringBuilder.Append(")");
				stringBuilder.Append(" values (");
				stringBuilder.Append("'" + base.DbGuid() + "',");
				stringBuilder.Append("'person',");
				stringBuilder.Append("'" + fp_id + "',");
				stringBuilder.Append("'" + dataTable.Rows[0]["fname"].ToString() + "',");
				stringBuilder.Append("'" + dataTable.Rows[0]["fperson_id"].ToString() + "',");
				stringBuilder.Append("'" + str2 + "',");
				stringBuilder.Append("'" + str + "',");
				stringBuilder.Append("'1',");
				stringBuilder.Append("''");
				stringBuilder.Append(")");
				WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
			}
		}

		public void BllQX(string forg_id)
		{
			WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, "DELETE FROM wwf_org WHERE forg_id = '" + forg_id + "'");
		}

		public bool BllQXPerson(string forg_id)
		{
			string strSql = "delete FROM wwf_org WHERE (forg_id = '" + forg_id + "') ";
			string strSql2 = "delete FROM wwf_org_func WHERE (forg_id = '" + forg_id + "') ";
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryTransaction2(WWFInit.strDBConn, strSql, strSql2);
		}

		public bool BllQXPersonOK(string forg_id)
		{
			return WWFInit.wwfRemotingDao.DbRecordExists(WWFInit.strDBConn, "SELECT count(1) FROM wwf_org where fp_id = '" + forg_id + "'");
		}
	}
}
