using System;
using System.Data;
using ww.wwf.com;

namespace ww.wwf.wwfbll
{
	public class LoginBLL : DAOWWF
	{
		public static string strTest = "对不起，您当前使用的版本不能使用此功能！";

		public static string CoAdd = "";

		public static string CoContact = "";

		public static string Copyright = "";

		public static string CoTel = "";

		public static string CoBank = "";

		public static string CoNA = "";

		public static string CoContactDept = "";

		public static string CoEmail = "";

		public static string CoFax = "";

		public static string CoHttp = "";

		public static string CoMonitorTel = "";

		public static string CoZC = "";

		public static string CustomerAdd = "";

		public static string CustomerContact = "";

		public static string CustomerName = "";

		public static string CustomerTel = "";

		public static string sysDB = "";

		public static string sysName = "";

		public static string sysNum = "";

		public static string sysVar = "";

		public static string pidSys = "";

		public static string pidBusiness = "";

		public static string sysHelpAdd = "";

		public static string strPersonID = "";

		public static string strPersonName = "";

		public static string strPositionID = "";

		public static string strPositionName = "";

		public static string strDeptID = "";

		public static string strDeptName = "";

		public static string strOrgID = "";

		public static string SysStartPage = "";

		public void BllSetSysInfo()
		{
			System.Data.DataTable dataTable = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, "SELECT fsys_id, ftype, fvalue, fremark FROM wwf_sys").Tables[0];
			int i = 0;
			while (i < dataTable.Rows.Count)
			{
				string text = dataTable.Rows[i]["fsys_id"].ToString();
				string text2 = dataTable.Rows[i]["fvalue"].ToString();
				string text3 = text;
				switch (text3)
				{
				case "CoAdd":
					LoginBLL.CoAdd = text2;
					break;
				case "CoContact":
					LoginBLL.CoContact = text2;
					break;
				case "Copyright":
					LoginBLL.Copyright = text2;
					break;
				case "CoTel":
					LoginBLL.CoTel = text2;
					break;
				case "CustomerAdd":
					LoginBLL.CustomerAdd = text2;
					break;
				case "CustomerContact":
					LoginBLL.CustomerContact = text2;
					break;
				case "CustomerName":
					LoginBLL.CustomerName = text2;
					break;
				case "CustomerTel":
					LoginBLL.CustomerTel = text2;
					break;
				case "sysDB":
					LoginBLL.sysDB = text2;
					break;
				case "sysName":
					LoginBLL.sysName = text2;
					break;
				case "sysNum":
					LoginBLL.sysNum = text2;
					break;
				case "sysVar":
					LoginBLL.sysVar = text2;
					break;
				case "sysHelpAdd":
					LoginBLL.sysHelpAdd = text2;
					break;
				case "pidSys":
					LoginBLL.pidSys = text2;
					break;
				case "pidBusiness":
					LoginBLL.pidBusiness = text2;
					break;
				case "CoBank":
					LoginBLL.CoBank = text2;
					break;
				case "CoNA":
					LoginBLL.CoNA = text2;
					break;
				case "CoContactDept":
					LoginBLL.CoContactDept = text2;
					break;
				case "CoEmail":
					LoginBLL.CoEmail = text2;
					break;
				case "CoFax":
					LoginBLL.CoFax = text2;
					break;
				case "CoHttp":
					LoginBLL.CoHttp = text2;
					break;
				case "CoMonitorTel":
					LoginBLL.CoMonitorTel = text2;
					break;
				case "CoZC":
					LoginBLL.CoZC = text2;
					break;
				case "SysStartPage":
					LoginBLL.SysStartPage = text2;
					break;
				}
				i++;
				continue;
            }
		}

		public bool BllUserPassOk(string fperson_id, string fpass)
		{
			string text = Security.StrToEncrypt("MD5", fpass);
			string sql = string.Concat(new string[]
			{
				"SELECT count(1) FROM wwf_person where fperson_id='",
				fperson_id,
				"' AND (fpass='",
				text,
				"')"
			});
			return WWFInit.wwfRemotingDao.DbRecordExists(WWFInit.strDBConn, sql);
		}

		public bool BllUserPassOkParams(string fperson_id, string fpass)
		{
			string text = Security.StrToEncrypt("MD5", fpass);
			string uspName = "UserLogin";
			return WWFInit.wwfRemotingDao.DbRecordExistsParam(WWFInit.strDBConn, uspName, new object[]
			{
				fperson_id,
				text
			});
		}

		public System.Data.DataTable BllUserOrg(string fperson_id)
		{
			string strSql = "SELECT t.*,(SELECT fname  FROM wwf_dept WHERE (fdept_id = t.fdept_id)) AS fdept_name,(SELECT fname  FROM WWF_POSITION  WHERE (fposition_id = t.fposition_id)) AS fposition_name FROM wwf_org t where fperson_id='" + fperson_id + "' AND (ftype='person')";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public void BllSetLoginValue(System.Data.DataTable dtOrg, string forg_id)
		{
			System.Data.DataRow[] array = dtOrg.Select("forg_id='" + forg_id + "'");
			if (array.Length > 0)
			{
				System.Data.DataRow[] array2 = array;
				for (int i = 0; i < array2.Length; i++)
				{
					System.Data.DataRow dataRow = array2[i];
					LoginBLL.strPersonID = dataRow["fperson_id"].ToString();
					LoginBLL.strPersonName = dataRow["fname"].ToString();
					LoginBLL.strPositionID = dataRow["fposition_id"].ToString();
					LoginBLL.strPositionName = dataRow["fposition_name"].ToString();
					LoginBLL.strDeptID = dataRow["fdept_id"].ToString();
					LoginBLL.strDeptName = dataRow["fdept_name"].ToString();
					LoginBLL.strOrgID = dataRow["forg_id"].ToString();
				}
			}
		}

		public System.Data.DataTable BllOrgFuncDT(string forg_id)
		{
			string strSql = "SELECT t2.forg_id, t1.ffunc_id, t2.ftool_flag, t2.fgrzm_flag, t1.fp_id, t1.fname, t1.fname_e, t1.ficon, t1.ffunc_winform, t1.ffunc_web, t1.forder_by, t1.fuse_flag, t1.fhelp, t1.fcj_flag, t1.fcj_name, t1.fcj_zp  FROM wwf_func t1 ,  wwf_org_func t2 where t1.ffunc_id = t2.ffunc_id and  t1.fuse_flag=1 and t2.forg_id='" + forg_id + "' order by t1.forder_by";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public System.Data.DataTable BllOrgFuncDT(string forg_id, string fp_id)
		{
			string strSql = string.Concat(new string[]
			{
				"SELECT t2.forg_id, t1.ffunc_id, t2.ftool_flag, t2.fgrzm_flag, t1.fp_id, t1.fname, t1.fname_e, t1.ficon, t1.ffunc_winform, t1.ffunc_web, t1.forder_by, t1.fuse_flag, t1.fhelp, t1.fcj_flag, t1.fcj_name, t1.fcj_zp  FROM wwf_func t1 ,  wwf_org_func t2 where t1.ffunc_id = t2.ffunc_id and  t1.fuse_flag=1 and t2.forg_id='",
				forg_id,
				"' and t1.fp_id='",
				fp_id,
				"' order by t1.forder_by"
			});
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public System.Data.DataTable BllOrgFuncDTDesc(string forg_id, string fp_id)
		{
			string strSql = string.Concat(new string[]
			{
				"SELECT t2.forg_id, t1.ffunc_id, t2.ftool_flag, t2.fgrzm_flag, t1.fp_id, t1.fname, t1.fname_e, t1.ficon, t1.ffunc_winform, t1.ffunc_web, t1.forder_by, t1.fuse_flag, t1.fhelp, t1.fcj_flag, t1.fcj_name, t1.fcj_zp  FROM wwf_func t1 ,  wwf_org_func t2 where t1.ffunc_id = t2.ffunc_id and  t1.fuse_flag=1 and t2.forg_id='",
				forg_id,
				"' and t1.fp_id='",
				fp_id,
				"' order by t1.forder_by desc"
			});
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public System.Data.DataTable BllFuncListByID(string ffunc_id)
		{
			string strSql = "SELECT * FROM wwf_func WHERE (ffunc_id = '" + ffunc_id + "')";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public string BllFuncHelp(string strid)
		{
			object obj = WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(WWFInit.strDBConn, "SELECT FHELP FROM WWF_FUNC WHERE (FFUNC_ID = '" + strid + "')");
			string result;
			if (obj != null)
			{
				result = (string)obj;
			}
			else
			{
				result = "";
			}
			return result;
		}

        //==============================================
        /// <summary>
        /// 推送消息地址
        /// </summary>
        public static string sendurl { get; set; }

        /// <summary>
        /// 推送医院ID
        /// </summary>
        public static string sendappId { get; set; }

        /// <summary>
        /// 自动推送:默认不自动推送
        /// </summary>
        public static bool sendauto = false;

        /// <summary>
        /// 读卡url
        /// </summary>
        public static string readurl { get; set; }

        /// <summary>
        /// 读卡医院ID
        /// </summary>
        public static string readappId { get; set; }

        /// <summary>
        /// 读卡医院Key
        /// </summary>
        public static string readappkey { get; set; }

        /// <summary>
        /// 终端号
        /// </summary>
        public static string term_id { get; set; }
        
        //==============================================
        /// <summary>
        /// 设置全局参数
        /// 2019年11月12日 yfh 添加 暂时先添加这两个，以后用到再说
        /// </summary>
        public void BllSet系统参数()
        {
            try
            {
                DataTable dt = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, "SELECT * FROM 全局参数").Tables[0];
                string fname = "";//参数名称
                string fvalue = "";//参数值
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    fname = dt.Rows[i]["参数名称"].ToString();
                    fvalue = dt.Rows[i]["参数值"].ToString();
                    switch (fname)
                    {
                        case "sendurl":
                            sendurl = fvalue;
                            break;
                        case "sendappId":
                            sendappId = fvalue;
                            break;
                        case "sendauto":
                            sendauto = fvalue.Equals("true");
                            break;
                        case "readurl":
                            readurl = fvalue;
                            break;
                        case "readappId":
                            readappId = fvalue;
                            break;
                        case "readappkey":
                            readappkey = fvalue;
                            break;
                        case "term_id":
                            term_id = fvalue;
                            break;
                        default:
                            break;
                    }
                }
            }
            catch
            {
                //出现异常默认龙家圈医院地址
                sendurl = "http://192.168.10.171:1811/ehc-portal-push/sms/send";
                sendappId = "1DNUJKJV00000100007F0000F0308269";
                //读卡默认
                readurl = "http://192.168.10.171:1811/ehcService/gateway.do";
                readappId = "1DOB630TT0500100007F0000CC065635";
                readappkey = "1DOB630TT0510100007F0000D6D2E81F";
                term_id = "371300210001";
            }
        }
	}
}
