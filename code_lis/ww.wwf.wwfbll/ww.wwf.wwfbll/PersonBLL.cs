using System;
using System.Collections;
using System.Data;
using System.Text;

namespace ww.wwf.wwfbll
{
	public class PersonBLL : DAOWWF
	{
		public System.Data.DataTable BllDTByCode(string strfcode, string strfname)
		{
			string strSql = string.Concat(new string[]
			{
				"SELECT * FROM WWF_PERSON where  (fperson_id='",
				strfcode,
				"' or fname ='",
				strfname,
				"')"
			});
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public System.Data.DataTable BllDTAllPerson(string fuse_flag)
		{
			string strSql = "SELECT  t.fshow_name AS PersonName, t.* FROM wwf_person t WHERE (fuse_flag = " + fuse_flag + ") ORDER BY fdept_id";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public string BllPersonNameByID(string strfperson_id)
		{
			return (string)WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(WWFInit.strDBConn, "SELECT fname FROM WWF_PERSON WHERE (fperson_id = '" + strfperson_id + "')");
		}

		public System.Data.DataTable BllDTAllPersonByTypeAndUse(int fuse_flag, string ftype)
		{
			string strSql = string.Concat(new object[]
			{
				"SELECT fperson_id, fname, fshow_name FROM wwf_person WHERE ((ftype = '",
				ftype,
				"')or(ftype = '开发')) AND (fuse_flag = ",
				fuse_flag,
				")"
			});
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public System.Data.DataTable BllDTAllPersonByTypeAndUse(string ftype)
		{
			string strSql = "SELECT * FROM wwf_person WHERE (ftype = '" + ftype + "') AND (fuse_flag = 1)";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public System.Data.DataTable BllDTAllis0()
		{
			string strSql = "SELECT * FROM wwf_person WHERE fperson_id='-1'";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public System.Data.DataTable BllDT(string fdept_id)
		{
			string strSql;
			if (fdept_id == "" || fdept_id == null || fdept_id == "0")
			{
				strSql = "SELECT * FROM wwf_person ORDER BY forder_by";
			}
			else
			{
				strSql = "SELECT * FROM wwf_person where fdept_id='" + fdept_id + "' ORDER BY forder_by";
			}
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public System.Data.DataTable BllDTByID(string id)
		{
			string strSql = "SELECT * FROM wwf_person where fperson_id='" + id + "'";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public bool BllPersonExists(string fperson_id)
		{
			return WWFInit.wwfRemotingDao.DbRecordExists(WWFInit.strDBConn, "SELECT COUNT(1) FROM wwf_person WHERE (fperson_id = '" + fperson_id + "')");
		}

		public string BllPersonfhelp_codeUpdate(IList lisSql)
		{
			return WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, lisSql);
		}

		public System.Data.DataTable BllDTByfhelp_code(string fhelp_code, string fdept_id)
		{
			string strSql;
			if (fdept_id == "")
			{
				strSql = "SELECT * FROM wwf_person where fhelp_code like '" + fhelp_code + "%'   order by forder_by";
			}
			else
			{
				strSql = string.Concat(new string[]
				{
					"SELECT * FROM wwf_person where fhelp_code like '",
					fhelp_code,
					"%'  and fdept_id='",
					fdept_id,
					"' order by forder_by"
				});
			}
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public int BllAdd(System.Data.DataTable dt)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into wwf_person(");
			stringBuilder.Append("fperson_id,fdept_id,fname,fname_e,fpass,forder_by,ftype,fuse_flag,fshow_name,fhelp_code,fremark");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + dt.Rows[0]["fperson_id"].ToString() + "',");
			stringBuilder.Append("'" + dt.Rows[0]["fdept_id"].ToString() + "',");
			stringBuilder.Append("'" + dt.Rows[0]["fname"].ToString() + "',");
			stringBuilder.Append("'" + dt.Rows[0]["fname_e"].ToString() + "',");
			stringBuilder.Append("'" + dt.Rows[0]["fpass"].ToString() + "',");
			stringBuilder.Append("'" + dt.Rows[0]["forder_by"].ToString() + "',");
			stringBuilder.Append("'" + dt.Rows[0]["ftype"].ToString() + "',");
			stringBuilder.Append(dt.Rows[0]["fuse_flag"].ToString() + ",");
			stringBuilder.Append("'" + dt.Rows[0]["fshow_name"].ToString() + "',");
			stringBuilder.Append("'" + dt.Rows[0]["fhelp_code"].ToString() + "',");
			stringBuilder.Append("'" + dt.Rows[0]["fremark"].ToString() + "'");
			stringBuilder.Append(")");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllUpdate(System.Data.DataTable dt)
		{
			int result = 0;
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update wwf_person set ");
			stringBuilder.Append("fname='" + dt.Rows[0]["fname"].ToString() + "',");
			stringBuilder.Append("fname_e='" + dt.Rows[0]["fname_e"].ToString() + "',");
			stringBuilder.Append("forder_by='" + dt.Rows[0]["forder_by"].ToString() + "',");
			stringBuilder.Append("ftype='" + dt.Rows[0]["ftype"].ToString() + "',");
			stringBuilder.Append("fuse_flag='" + dt.Rows[0]["fuse_flag"].ToString() + "',");
			stringBuilder.Append("fshow_name='" + dt.Rows[0]["fshow_name"].ToString() + "',");
			stringBuilder.Append("fremark='" + dt.Rows[0]["fremark"].ToString() + "'");
			stringBuilder.Append(" where fperson_id='" + dt.Rows[0]["fperson_id"].ToString() + "'");
			StringBuilder stringBuilder2 = new StringBuilder();
			stringBuilder2.Append("update wwf_org set ");
			stringBuilder2.Append("fname='" + dt.Rows[0]["fname"].ToString() + "'");
			stringBuilder2.Append(" where fperson_id='" + dt.Rows[0]["fperson_id"].ToString() + "' and ftype='person'");
			if (WWFInit.wwfRemotingDao.DbExecuteNonQueryTransaction2(WWFInit.strDBConn, stringBuilder.ToString(), stringBuilder2.ToString()))
			{
				result = 1;
			}
			return result;
		}

		public int BllUpdatePass(System.Data.DataTable dt)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update wwf_person set ");
			stringBuilder.Append("fpass='" + dt.Rows[0]["fpass"].ToString() + "'");
			stringBuilder.Append(" where fperson_id='" + dt.Rows[0]["fperson_id"].ToString() + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public string BllDelete(string fperson_id)
		{
			IList list = new ArrayList();
			System.Data.DataTable dataTable = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, "SELECT forg_id FROM wwf_org WHERE (fperson_id = '" + fperson_id + "') AND (ftype = 'person')").Tables[0];
			for (int i = 0; i < dataTable.Rows.Count; i++)
			{
				list.Add("delete FROM wwf_org_func where forg_id='" + dataTable.Rows[i]["forg_id"].ToString() + "'");
			}
			list.Add("delete FROM wwf_person where fperson_id='" + fperson_id + "'");
			list.Add("delete FROM wwf_org where (fperson_id = '" + fperson_id + "') AND (ftype = 'person')");
			return WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, list);
		}
	}
}
