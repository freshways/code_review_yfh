using System;
using System.Data;

namespace ww.wwf.wwfbll
{
	public class NumBLL : DAOWWF
	{
		public System.Data.DataTable BllDT()
		{
			string strSql = "SELECT * FROM wwf_num";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public int BllGetflengthByid(string fnum_id)
		{
			string strSql = "SELECT flength FROM wwf_num WHERE (fnum_id = '" + fnum_id + "')";
			string text = WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(WWFInit.strDBConn, strSql).ToString();
			int result;
			if (text == "" || text == null)
			{
				result = 0;
			}
			else
			{
				result = Convert.ToInt32(text);
			}
			return result;
		}
	}
}
