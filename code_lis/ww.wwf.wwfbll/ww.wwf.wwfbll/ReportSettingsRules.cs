using System;
using System.Data;
using System.IO;

namespace ww.wwf.wwfbll
{
	public class ReportSettingsRules : DAOWWF
	{
		public System.Data.DataTable BllReportDT(int fuse_if)
		{
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, "SELECT * FROM SAM_REPORT t WHERE (fuse_if = " + fuse_if + ")").Tables[0];
		}

		public void BllReportSettings(string strFileName)
		{
			FileInfo fileInfo = new FileInfo(strFileName);
			if (!fileInfo.Exists)
			{
				StreamWriter streamWriter = new StreamWriter(strFileName, true);
				streamWriter.WriteLine("<?xml version='1.0' standalone='yes'?>");
				streamWriter.WriteLine("<ReportSettings>");
				System.Data.DataTable dataTable = new System.Data.DataTable();
				dataTable = this.BllReportDT(1);
				if (dataTable.Rows.Count > 0)
				{
					for (int i = 0; i < dataTable.Rows.Count; i++)
					{
						System.Data.DataRow dataRow = dataTable.Rows[i];
						streamWriter.WriteLine("<" + dataRow["fcode"].ToString() + ">");
						streamWriter.WriteLine("<ReportName>" + dataRow["fcode"].ToString() + "</ReportName>");
						streamWriter.WriteLine("<PrinterName>" + dataRow["fprinter_name"].ToString() + "</PrinterName>");
						streamWriter.WriteLine("<PaperName>" + dataRow["fpaper_name"].ToString() + "</PaperName>");
						streamWriter.WriteLine("<PageWidth>" + dataRow["fpage_width"].ToString() + "</PageWidth>");
						streamWriter.WriteLine("<PageHeight>" + dataRow["fpage_height"].ToString() + "</PageHeight>");
						streamWriter.WriteLine("<MarginTop>" + dataRow["fmargin_top"].ToString() + "</MarginTop>");
						streamWriter.WriteLine("<MarginBottom>" + dataRow["fmargin_bottom"].ToString() + "</MarginBottom>");
						streamWriter.WriteLine("<MarginLeft>" + dataRow["fmargin_left"].ToString() + "</MarginLeft>");
						streamWriter.WriteLine("<MarginRight>" + dataRow["fmargin_right"].ToString() + "</MarginRight>");
						streamWriter.WriteLine("<Orientation>" + dataRow["forientation"].ToString() + "</Orientation>");
						streamWriter.WriteLine("</" + dataRow["fcode"].ToString() + ">");
					}
				}
				else
				{
					streamWriter.WriteLine("<Template>");
					streamWriter.WriteLine("<ReportName>Template</ReportName>");
					streamWriter.WriteLine("<PrinterName>Microsoft Office Document Image Writer</PrinterName>");
					streamWriter.WriteLine("<PaperName>A4</PaperName>");
					streamWriter.WriteLine("<PageWidth>21.01</PageWidth>");
					streamWriter.WriteLine("<PageHeight>29.69</PageHeight>");
					streamWriter.WriteLine("<MarginTop>1</MarginTop>");
					streamWriter.WriteLine("<MarginBottom>1</MarginBottom>");
					streamWriter.WriteLine("<MarginLeft>1</MarginLeft>");
					streamWriter.WriteLine("<MarginRight>1</MarginRight>");
					streamWriter.WriteLine("<Orientation>H</Orientation>");
					streamWriter.WriteLine("</Template>");
				}
				streamWriter.WriteLine("</ReportSettings>");
				streamWriter.WriteLine(" ");
				streamWriter.Close();
			}
		}
	}
}
