using System;
using System.Data;
using System.Text;

namespace ww.wwf.wwfbll
{
	public class SysBLL : DAOWWF
	{
		public System.Data.DataTable BllDT()
		{
			string strSql = "SELECT * FROM wwf_sys ORDER BY fremark";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public int BllUpdate(System.Data.DataRowView dr)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update wwf_sys set ");
			stringBuilder.Append("fvalue='" + dr["fvalue"].ToString() + "'");
			stringBuilder.Append(" where fsys_id='" + dr["fsys_id"].ToString() + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}
	}
}
