using System;
using System.Data;
using System.Windows.Forms;

namespace ww.wwf.wwfbll
{
	public class DAOWWF
	{
		public string DbGuid()
		{
			string text = Guid.NewGuid().ToString();
			return text.Replace("-", "");
		}

		public string DbServerDateTim()
		{
			string result = DateTime.Now.ToString();
			if (LoginBLL.sysDB == "SqlServer" || LoginBLL.sysDB == "SqlServer")
			{
				result = WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(WWFInit.strDBConn, "SELECT TOP 1 GETDATE() AS ServerDateTime FROM wwf_sys").ToString();
			}
			return result;
		}

		public string DbDateTime1(DateTimePicker dtp)
		{
			string text = dtp.Value.Year.ToString();
			string text2 = dtp.Value.Month.ToString();
			string text3 = dtp.Value.Day.ToString();
			if (text2.Length == 1)
			{
				text2 = "0" + text2;
			}
			if (text3.Length == 1)
			{
				text3 = "0" + text3;
			}
			return string.Concat(new string[]
			{
				text,
				"-",
				text2,
				"-",
				text3
			});
		}

		public string DbSysTime()
		{
			return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
		}

		public string DbDateTime2(DateTimePicker dtp)
		{
			string text = dtp.Value.Year.ToString();
			string text2 = dtp.Value.Month.ToString();
			string text3 = dtp.Value.Day.ToString();
			if (text2.Length == 1)
			{
				text2 = "0" + text2;
			}
			if (text3.Length == 1)
			{
				text3 = "0" + text3;
			}
			return string.Concat(new string[]
			{
				text,
				"-",
				text2,
				"-",
				text3,
				" 23:59:59"
			});
		}

		public string DbNum(string fnum_id)
		{
			string strSql = "SELECT * FROM wwf_num WHERE fnum_id='" + fnum_id + "'";
			System.Data.DataTable dataTable = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
			string result;
			if (dataTable.Rows.Count != 0)
			{
				string text = dataTable.Rows[0]["fyear"].ToString();
				string text2 = dataTable.Rows[0]["fmonth"].ToString();
				string text3 = dataTable.Rows[0]["fday"].ToString();
				int num;
				try
				{
					num = Convert.ToInt32(dataTable.Rows[0]["fcurrent_num"].ToString());
				}
				catch
				{
					num = 0;
				}
				string text4 = dataTable.Rows[0]["fprehead"].ToString();
				int totalWidth;
				try
				{
					totalWidth = Convert.ToInt32(dataTable.Rows[0]["flength"].ToString());
				}
				catch
				{
					totalWidth = 4;
				}
				string text5 = "";
				string str = text4.Trim();
				DateTime dateTime = Convert.ToDateTime(this.DbServerDateTim());
				string text6 = dateTime.ToString("yyyy");
				string text7 = dateTime.ToString("MM");
				string text8 = dateTime.ToString("dd");
				if (text != null && text.Length != 0)
				{
					if (text != text6)
					{
						text5 = ",fyear='" + text6 + "'";
					}
					str += text6;
				}
				if (text2 != null && text2.Length != 0)
				{
					if (text2 != text7)
					{
						text5 = ",fmonth='" + text7 + "'";
					}
					str += text7;
				}
				if (text3 != null && text3.Length != 0)
				{
					if (text3 != text8)
					{
						text5 = ",fday='" + text8 + "'";
					}
					str += text8;
				}
				int num2 = num + 1;
				WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, string.Concat(new object[]
				{
					"Update wwf_num Set fcurrent_num=",
					num2,
					text5,
					" where fnum_id='",
					fnum_id,
					"'"
				}));
				result = str + num.ToString().PadLeft(totalWidth, '0');
			}
			else
			{
				result = "-2";
			}
			return result;
		}
	}
}
