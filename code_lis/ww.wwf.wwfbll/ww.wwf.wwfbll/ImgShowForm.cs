﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ww.wwf.wwfbll
{
    public partial class ImgShowForm : Form
    {
        Bitmap bit = null;
        public ImgShowForm(Bitmap bitmap)
        {
            InitializeComponent();          
            bit = bitmap;
        }

        private void ImgShowForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.pictureBoxImg.Image = bit;               
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
           
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonImgOut_Click(object sender, EventArgs e)
        {
            try
            {
                ImgBLL img = new ImgBLL();
                img.ImgOut(this.pictureBoxImg);
                
            }
            catch (Exception ex)
            {
              WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

    }
}