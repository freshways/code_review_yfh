using System;
using System.Collections;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Net;
using System.Diagnostics;
using System.Text.RegularExpressions;
namespace ww.wwf.com
{
	
	public class Public
	{

        public Public()
		{
			///
			/// TODO: 在此处添加构造函数逻辑
			///            
           
		}
        public static string BllGuid()
        {
            string strGuid = "";
            //string strGuid = System.Guid.NewGuid().ToString().ToUpper();
            strGuid = System.Guid.NewGuid().ToString();
            return strGuid.Replace("-", "");
        }
        /// <summary>
        /// 取得小数位数
        /// </summary>
        /// <returns></returns>
        public static Decimal BllGetXSWS(Decimal decValue, int intWX)
        {
            Decimal decr = 0;
            switch (intWX)
            {
                case 1:
                    decr = Decimal.Parse(decValue.ToString("0.0"));
                    break;
                case 2:
                    decr = Decimal.Parse(decValue.ToString("0.00"));
                    break;
                case 3:
                    decr = Decimal.Parse(decValue.ToString("0.000"));
                    break;
                case 4:
                    decr = Decimal.Parse(decValue.ToString("0.0000"));
                    break;
                case 5:
                    decr = Decimal.Parse(decValue.ToString("0.00000"));
                    break;
                case 6:
                    decr = Decimal.Parse(decValue.ToString("0.000000"));
                    break;
                case 7:
                    decr = Decimal.Parse(decValue.ToString("0.0000000"));
                    break;
                case 8:
                    decr = Decimal.Parse(decValue.ToString("0.0000000"));
                    break;
                default:
                    decr = Decimal.Parse(decValue.ToString("0.00"));
                    break;
            }
            return decr;
        }
        /// <summary>
        /// 是数字否
        /// </summary>
        /// <param name="strNumber"></param>
        /// <returns></returns>
        public static bool IsNumber(String strNumber)
        {

            Regex objNotNumberPattern = new Regex("[^0-9.-]");
            Regex objTwoDotPattern = new Regex("[0-9]*[.][0-9]*[.][0-9]*");
            Regex objTwoMinusPattern = new Regex("[0-9]*[-][0-9]*[-][0-9]*");
            String strValidRealPattern = "^([-]|[.]|[-.]|[0-9])[0-9]*[.]*[0-9]+$";
            String strValidIntegerPattern = "^([-]|[0-9])[0-9]*$";
            Regex objNumberPattern = new Regex("(" + strValidRealPattern + ")|(" + strValidIntegerPattern + ")");

            return (!objNotNumberPattern.IsMatch(strNumber) &&
            !objTwoDotPattern.IsMatch(strNumber) &&
            !objTwoMinusPattern.IsMatch(strNumber) &&
            objNumberPattern.IsMatch(strNumber)) && (!strNumber.Trim().Equals("-"));
        }
        // Stream 和 byte[] 之间的转换
        /* - - - - - - - - - - - - - - - - - - - - - - - -  
         * Stream 和 byte[] 之间的转换 
         * - - - - - - - - - - - - - - - - - - - - - - - */
        /// <summary> 
        /// 将 Stream 转成 byte[] 
        /// </summary> 
        public byte[] StreamToBytes(Stream stream)
        {
            byte[] bytes = new byte[stream.Length];
            stream.Read(bytes, 0, bytes.Length);
            // 设置当前流的位置为流的开始 
            stream.Seek(0, SeekOrigin.Begin);
            return bytes;
        }
        /// <summary> 
        /// 将 byte[] 转成 Stream 
        /// </summary> 
        public Stream BytesToStream(byte[] bytes)
        {
            Stream stream = new MemoryStream(bytes);
            return stream;
        }

        /* - - - - - - - - - - - - - - - - - - - - - - - -  
         * Stream 和 文件之间的转换 
         * - - - - - - - - - - - - - - - - - - - - - - - */
        /// <summary> 
        /// 将 Stream 写入文件 
        /// </summary> 
        public void StreamToFile(Stream stream, string fileName)
        {
            // 把 Stream 转换成 byte[] 
            byte[] bytes = new byte[stream.Length];
            stream.Read(bytes, 0, bytes.Length);
            // 设置当前流的位置为流的开始 
            stream.Seek(0, SeekOrigin.Begin);
            // 把 byte[] 写入文件 
            FileStream fs = new FileStream(fileName, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write(bytes);
            bw.Close();
            fs.Close();
        }
        /// <summary> 
        /// 从文件读取 Stream 
        /// </summary> 
        public Stream FileToStream(string fileName)
        {
            // 打开文件 
            FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            // 读取文件的 byte[] 
            byte[] bytes = new byte[fileStream.Length];
            fileStream.Read(bytes, 0, bytes.Length);
            fileStream.Close();
            // 把 byte[] 转换成 Stream 
            Stream stream = new MemoryStream(bytes);
            return stream;
        }
        
//处理用户的快捷键
        /*
        protected override bool ProcessCmdKey(ref   System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {


            if (keyData == (Keys.Control | Keys.S))
            {
                MessageBox.Show("陶1");
                return true;
            }
            if (keyData == (Keys.Control | Keys.Shift | Keys.C))
            {
                MessageBox.Show("陶2");
                return true;
            }
            if (keyData == (Keys.Control | Keys.P))
            {
                MessageBox.Show("陶3");
                return true;
            }
            if (keyData == (Keys.F4))
            {
                MessageBox.Show("陶4");
                butOK.PerformClick();
                return true;//此后还有一些信息需要处理，否则有异常
            }
            return base.ProcessCmdKey(ref   msg, keyData);
        }*/
       
	}
}
