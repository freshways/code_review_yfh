using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Data;

namespace ww.wwf.com
{
    public class DateTimeControl
    {
        //[DllImport("Kernel32.dll")]
        //public static extern void GetLocalTime(SystemTime st);
        //[DllImport("Kernel32.dll")]
        //public static extern void SetLocalTime(SystemTime st);
        //[StructLayout(LayoutKind.Sequential)]
        //public class SystemTime
        //{
        //    public ushort wYear;
        //    public ushort wMonth;
        //    public ushort wDayOfWeek;
        //    public ushort wDay;
        //    public ushort Whour;
        //    public ushort wMinute;
        //    public ushort wSecond;
        //    public ushort wMilliseconds;
        //}
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool SetSystemTime(ref SystemTime systemTime);
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool SetLocalTime(ref SystemTime systemTime);
        [StructLayout(LayoutKind.Sequential)]
        public struct SystemTime
        {
            [MarshalAs(UnmanagedType.U2)]
            public short wYear;
            [MarshalAs(UnmanagedType.U2)]
            public short wMonth;
            [MarshalAs(UnmanagedType.U2)]
            public short wDayOfWeek;
            [MarshalAs(UnmanagedType.U2)]
            public short wDay;
            [MarshalAs(UnmanagedType.U2)]
            public short wHour;
            [MarshalAs(UnmanagedType.U2)]
            public short wMinute;
            [MarshalAs(UnmanagedType.U2)]
            public short wSecond;
            [MarshalAs(UnmanagedType.U2)]
            public short wMilliseconds;
        }


        public static bool syncTime(DateTime _dt)
        {
            var syncDateTime = _dt;
            var localtime = new SystemTime()
            {
                wYear = (short)syncDateTime.Year,
                wMonth = (short)syncDateTime.Month,
                wDay = (short)syncDateTime.Day,
                wDayOfWeek = (short)syncDateTime.DayOfWeek,
                wHour = (short)syncDateTime.Hour,
                wMinute = (short)syncDateTime.Minute,
                wSecond = (short)syncDateTime.Second,
                wMilliseconds = (short)syncDateTime.Millisecond
            };

            if (!SetLocalTime(ref localtime))
            {
                // 若設定不成功，取得Win32 Error Code，例如取得1314則是權限不夠，要用系統管理員身份執行。 
                //if (System.Runtime.InteropServices.Marshal.GetLastWin32Error().ToString() == "1314")
                //{
                //    MessageBox.Show("同步服务器时间失败，权限不够", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //}
                //else
                //{
                //    MessageBox.Show("同步服务器时间失败", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //}
                return false;

            }
            else
            {
                return true;
            }
        }


        //public static void setDateTime(DateTime CurrentTime)
        //{
        //    try
        //    {
        //        SystemTime st = new SystemTime();
        //        st.wYear = (ushort)CurrentTime.Year;
        //        st.wMonth = (ushort)CurrentTime.Month;
        //        st.wDay = (ushort)CurrentTime.Day;
        //        st.Whour = (ushort)CurrentTime.Hour;
        //        st.wMinute = (ushort)CurrentTime.Minute;
        //        st.wSecond = (ushort)CurrentTime.Second;
        //        SetLocalTime(st);
        //    }
        //    catch
        //    {
        //        MessageBox.Show("系统时间设置失败!",
        //            "设置失败",
        //            MessageBoxButtons.OK,
        //            MessageBoxIcon.Information);
        //    }
        //    finally
        //    {
        //    }
        //}



    }
}

