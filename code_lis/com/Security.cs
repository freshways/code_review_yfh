using System;
using System.Collections.Generic;
using System.Text;
using System.Security;
using System.Security.Cryptography;
using System.Configuration;

namespace ww.wwf.com
{
    public class Security
    {
        public Security()
        {
            ///
            /// TODO: 在此处添加构造函数逻辑
            ///

        }
        /// <summary>
        /// 字符串加密
        /// </summary>
        /// <param name="format">加密格式 SHA1,MD5，当前系统采用MD5</param>
        /// <param name="str">明文字符</param>       
        /// <returns>加密码后字符</returns>
        public static string StrToEncrypt(string format, string str)
        {
            //string pwd = ww.wwf.com.Security.StrToEncrypt("MD5", this.SysLoginPassword.Text);
            //SHA1,MD5           
            string password = str;
            byte[] dataOfPwd = (new UnicodeEncoding()).GetBytes(password);
            byte[] hashValueOfPwd = ((HashAlgorithm)CryptoConfig.CreateFromName(format)).ComputeHash(dataOfPwd);
            password = BitConverter.ToString(hashValueOfPwd);
            
            return password;
        }

        /// <summary>
        /// 加密连接字符串
        /// </summary>
        public static void PwdConfiguration()
        {
            // 使用什么类型的加密

            string provider = "RsaProtectedConfigurationProvider";

            Configuration config = null;

            config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            // 加密连接字符串

            ConfigurationSection section = config.ConnectionStrings;

            if ((section.SectionInformation.IsProtected == false) &&

                (section.ElementInformation.IsLocked == false))
            {
                section.SectionInformation.ProtectSection(provider);

                section.SectionInformation.ForceSave = true;

                config.Save(ConfigurationSaveMode.Full);

            }
        }
    }
}
