using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ww.wwf.dao;
using ww.wwf.wwfbll;
using ww.wwf.com;
using System.Data.SqlClient;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;

namespace ww.lis.lisbll.sam
{
    /// <summary>
    /// 检验报告 规则
    /// </summary>
    public class ReportBLL : DAOWWF
    {
        int sam_jy_fsample_code = 4;//默认样本位数　来自 表wwf_num 行sam_jy_fsample_code
        string table_SAM_APPLY = "";
        string table_SAM_JY_RESULT = "";
        string table_SAM_SAMPLE = "";
        string table_SAM_JY_IMG = "";
        string view_LIS_REPORT = "";
        NumBLL bllNum = new NumBLL();
        public ReportBLL()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //           
            table_SAM_APPLY = "SAM_APPLY";
            table_SAM_JY_RESULT = "SAM_JY_RESULT";
            table_SAM_SAMPLE = "SAM_SAMPLE";
            table_SAM_JY_IMG = "SAM_JY_IMG";
            view_LIS_REPORT = "LIS_REPORT";
            // sam_jy_fsample_code = bllNum.BllGetflengthByid("sam_jy_fsample_code");
        }
        #region 报告
        /// <summary>
        /// 取得报告DT
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataTable BllReportDT(string strWhere)
        {
            string strsql = "SELECT * FROM " + view_LIS_REPORT + strWhere;
            //string sql = "SELECT * FROM " + this.table_SAM_APPLY + " t1 INNER JOIN  " + this.table_SAM_SAMPLE + " t2 ON t1.fapply_id = t2.fapply_id " + strWhere;
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strsql).Tables[0];
        }
        /// <summary>
        /// 结果
        /// </summary>
        /// <param name="strfapply_id"></param>
        /// <returns></returns>
        public DataTable BllReportResultDT(string strfapply_id)
        {
            string sql = "SELECT * FROM " + this.table_SAM_JY_RESULT + " where fapply_id='" + strfapply_id + "' order by forder_by";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }

        /// <summary>
        /// 结果新增
        /// </summary>
        /// <param name="drResult"></param>
        /// <returns></returns>
        public int BllReportResultAdd(DataRow drResult)
        {

            StringBuilder strSql_Result = new StringBuilder();
            strSql_Result.Append("insert into " + table_SAM_JY_RESULT + "(");
            strSql_Result.Append("fresult_id,fapply_id,fitem_id,fitem_code,fitem_name,fitem_unit,forder_by");
            strSql_Result.Append(")");
            strSql_Result.Append(" values (");
            strSql_Result.Append("'" + drResult["fresult_id"] + "',");
            strSql_Result.Append("'" + drResult["fapply_id"] + "',");
            strSql_Result.Append("'" + drResult["fitem_id"] + "',");
            strSql_Result.Append("'" + drResult["fitem_code"] + "',");
            strSql_Result.Append("'" + drResult["fitem_name"] + "',");
            strSql_Result.Append("'" + drResult["fitem_unit"] + "',");
            strSql_Result.Append("'" + drResult["forder_by"] + "'");
            strSql_Result.Append(")");

            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql_Result.ToString());
        }
        

        /// <summary>
        /// 增加图
        /// </summary>
        /// <param name="dtIMG"></param>
        /// <param name="strfapply_id"></param>
        /// <returns></returns>
        public string BllReportIMGAdd(DataTable dtIMG, string strfsample_id)
        {
            IList addList = new ArrayList();
            if (dtIMG != null)
            {
                if (dtIMG.Rows.Count > 0)
                {
                    string strDel = "delete FROM " + this.table_SAM_JY_IMG + " WHERE (fsample_id = '" + strfsample_id + "')";
                    ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strDel);
                    for (int iimg = 0; iimg < dtIMG.Rows.Count; iimg++)
                    {
                        DataRow drimg = dtIMG.Rows[iimg];
                        ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbReportImgAdd(ww.wwf.wwfbll.WWFInit.strDBConn, this.table_SAM_JY_IMG + "_ADD", (string)drimg["fimg_type"], (Byte[])drimg["fimg"], (string)drimg["forder_by"], (string)drimg["fimg_type"], strfsample_id);
                    }
                }
            }
            return "true";
        }
        public int BllReportResultDel(string fresult_id)
        {
            string sql = "delete from " + this.table_SAM_JY_RESULT + " where fresult_id='" + fresult_id + "'";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql);
        }

        /// <summary>
        /// 图
        /// </summary>
        /// <param name="strfapply_id"></param>
        /// <returns></returns>
        public DataTable BllReportImgDT(string strfsample_id)
        {
            string sql = "SELECT * FROM " + this.table_SAM_JY_IMG + " where fsample_id='" + strfsample_id + "'";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        /// <summary>
        /// 样本号存在否
        /// </summary>
        /// <param name="finstr_id"></param>
        /// <param name="fjy_date"></param>
        /// <param name="fsample_code"></param>
        /// <returns></returns>
        public bool BllReportExists(string finstr_id, string fjy_date, string fsample_code)
        {
            string sqlQuery = "SELECT count(1) FROM " + this.table_SAM_SAMPLE + " WHERE (fjy_instr = '" + finstr_id + "') AND (fjy_date = '" + fjy_date + "') AND (fsample_code = '" + fsample_code + "')";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbRecordExists(ww.wwf.wwfbll.WWFInit.strDBConn, sqlQuery);
        }
        /// <summary>
        /// 报告新增
        /// </summary>
        /// <param name="dataR"></param>
        /// <returns></returns>
        public string BllReportAdd(DataRow dataR)
        {
            IList addList = new ArrayList();
            if (BllReportExists(dataR["fjy_instr"].ToString(), dataR["fjy_date"].ToString(), dataR["fsample_code"].ToString()))
            {
                return "样本号为：" + dataR["fsample_code"].ToString() + "的记录已经存在！";
            }
            else
            {

                StringBuilder strSql = new StringBuilder();
                strSql.Append("insert into " + this.table_SAM_APPLY + "(");
                strSql.Append("fapply_id,fjytype_id,fsample_type_id,fjz_flag,fcharge_flag,fcharge_id,fstate,fjyf,fapply_user_id,fapply_dept_id,fapply_time,ftype_id,fhz_id,fhz_zyh,fsex,fname,fvolk,fhymen,fage,fjob,fgms,fjob_org,fadd,ftel,fage_unit,fage_year,fage_month,fage_day,fward_num,froom_num,fbed_num,fblood_abo,fblood_rh,fjymd,fdiagnose,fheat,fxyld,fcyy,fxhdb,fcreate_user_id,fcreate_time,fupdate_user_id,fupdate_time,fitem_group,fremark");
                strSql.Append(")");
                strSql.Append(" values (");
                strSql.Append("'" + dataR["fapply_id"] + "',");
                strSql.Append("'" + dataR["fjytype_id"] + "',");
                strSql.Append("'" + dataR["fsample_type_id"] + "',");
                strSql.Append("" + dataR["fjz_flag"] + ",");
                strSql.Append("" + dataR["fcharge_flag"] + ",");
                strSql.Append("'" + dataR["fcharge_id"] + "',");
                strSql.Append("'" + dataR["fstate"] + "',");
                strSql.Append("" + dataR["fjyf"] + ",");
                strSql.Append("'" + dataR["fapply_user_id"] + "',");
                strSql.Append("'" + dataR["fapply_dept_id"] + "',");
                strSql.Append("'" + dataR["fapply_time"] + "',");
                strSql.Append("'" + dataR["ftype_id"] + "',");
                strSql.Append("'" + dataR["fhz_id"] + "',");
                strSql.Append("'" + dataR["fhz_zyh"] + "',");
                strSql.Append("'" + dataR["fsex"] + "',");
                strSql.Append("'" + dataR["fname"] + "',");
                strSql.Append("'" + dataR["fvolk"] + "',");
                strSql.Append("'" + dataR["fhymen"] + "',");
                strSql.Append("" + dataR["fage"] + ",");
                strSql.Append("'" + dataR["fjob"] + "',");
                strSql.Append("'" + dataR["fgms"] + "',");
                strSql.Append("'" + dataR["fjob_org"] + "',");
                strSql.Append("'" + dataR["fadd"] + "',");
                strSql.Append("'" + dataR["ftel"] + "',");
                strSql.Append("'" + dataR["fage_unit"] + "',");
                strSql.Append("" + dataR["fage_year"] + ",");
                strSql.Append("" + dataR["fage_month"] + ",");
                strSql.Append("" + dataR["fage_day"] + ",");
                strSql.Append("'" + dataR["fward_num"] + "',");
                strSql.Append("'" + dataR["froom_num"] + "',");
                strSql.Append("'" + dataR["fbed_num"] + "',");
                strSql.Append("'" + dataR["fblood_abo"] + "',");
                strSql.Append("'" + dataR["fblood_rh"] + "',");
                strSql.Append("'" + dataR["fjymd"] + "',");
                strSql.Append("'" + dataR["fdiagnose"] + "',");
                strSql.Append("" + dataR["fheat"] + ",");
                strSql.Append("" + dataR["fxyld"] + ",");
                strSql.Append("'" + dataR["fcyy"] + "',");
                strSql.Append("" + dataR["fxhdb"] + ",");
                strSql.Append("'" + dataR["fcreate_user_id"] + "',");
                strSql.Append("'" + dataR["fcreate_time"] + "',");
                strSql.Append("'" + dataR["fupdate_user_id"] + "',");
                strSql.Append("'" + dataR["fupdate_time"] + "',");
                strSql.Append("'" + dataR["fitem_group"] + "',");
                strSql.Append("'" + dataR["fremark"] + "'");
                strSql.Append(")");
                addList.Add(strSql.ToString());

                StringBuilder strSqlSam = new StringBuilder();
                strSqlSam.Append("insert into " + this.table_SAM_SAMPLE + "(");
                strSqlSam.Append("fsample_id,fapply_id,fsample_barcode,fsample_code,fstate,fexamine_flag,fsampling_user_id,fsampling_time,fsampling_remark,fsend_user_id,fsend_user_time,fget_user_id,fget_user_time,fget_user_remark,fjy_instr,fjy_date,fjy_group,fjy_user_id,fjy_time,fexamine_user_id,fexamine_time,fhandling_user_id,fhandling_user_time,fhandling_remark,fprint_flag,fprint_time,fprint_count,frelease_time,fcreate_user_id,fcreate_time,fupdate_user_id,fupdate_time,fread_flag,fread_user_id,fread_user_remark,fremark");
                strSqlSam.Append(")");
                strSqlSam.Append(" values (");
                strSqlSam.Append("'" + dataR["fsample_id"] + "',");
                strSqlSam.Append("'" + dataR["fapply_id"] + "',");
                strSqlSam.Append("'" + dataR["fsample_barcode"] + "',");
                strSqlSam.Append("'" + dataR["fsample_code"] + "',");
                strSqlSam.Append("'" + dataR["fstate"] + "',");
                strSqlSam.Append("" + dataR["fexamine_flag"] + ",");
                strSqlSam.Append("'" + dataR["fsampling_user_id"] + "',");
                strSqlSam.Append("'" + dataR["fsampling_time"] + "',");
                strSqlSam.Append("'" + dataR["fsampling_remark"] + "',");
                strSqlSam.Append("'" + dataR["fsend_user_id"] + "',");
                strSqlSam.Append("'" + dataR["fsend_user_time"] + "',");
                strSqlSam.Append("'" + dataR["fget_user_id"] + "',");
                strSqlSam.Append("'" + dataR["fget_user_time"] + "',");
                strSqlSam.Append("'" + dataR["fget_user_remark"] + "',");
                strSqlSam.Append("'" + dataR["fjy_instr"] + "',");
                strSqlSam.Append("'" + dataR["fjy_date"] + "',");
                strSqlSam.Append("'" + dataR["fjy_group"] + "',");
                strSqlSam.Append("'" + dataR["fjy_user_id"] + "',");
                strSqlSam.Append("'" + dataR["fjy_time"] + "',");
                strSqlSam.Append("'" + dataR["fexamine_user_id"] + "',");
                strSqlSam.Append("'" + dataR["fexamine_time"] + "',");
                strSqlSam.Append("'" + dataR["fhandling_user_id"] + "',");
                strSqlSam.Append("'" + dataR["fhandling_user_time"] + "',");
                strSqlSam.Append("'" + dataR["fhandling_remark"] + "',");
                strSqlSam.Append("" + dataR["fprint_flag"] + ",");
                strSqlSam.Append("'" + dataR["fprint_time"] + "',");
                strSqlSam.Append("" + dataR["fprint_count"] + ",");
                strSqlSam.Append("'" + dataR["frelease_time"] + "',");
                strSqlSam.Append("'" + dataR["fcreate_user_id"] + "',");
                strSqlSam.Append("'" + dataR["fcreate_time"] + "',");
                strSqlSam.Append("'" + dataR["fupdate_user_id"] + "',");
                strSqlSam.Append("'" + dataR["fupdate_time"] + "',");
                strSqlSam.Append("" + dataR["fread_flag"] + ",");
                strSqlSam.Append("'" + dataR["fread_user_id"] + "',");
                strSqlSam.Append("'" + dataR["fread_user_remark"] + "',");
                strSqlSam.Append("'" + dataR["fremark"] + "'");
                strSqlSam.Append(")");
                addList.Add(strSqlSam.ToString());
            }

            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(ww.wwf.wwfbll.WWFInit.strDBConn, addList);
        }
        /*
          table_SAM_APPLY = "SAM_APPLY";
            table_SAM_JY_RESULT = "SAM_JY_RESULT";
            table_SAM_SAMPLE = "SAM_SAMPLE";
            table_SAM_JY_IMG = "SAM_JY_IMG";
            view_LIS_REPORT = "LIS_REPORT";
         */
        /// <summary>
        /// 报告保存
        /// </summary>
        /// <param name="dataReport">报告</param>
        /// <param name="dtResult">结果</param>
        /// <returns></returns>
        public string BllReportUpdate(DataRowView drReport, DataTable dtResult)
        {
            IList updateList = new ArrayList();
            #region 样本申请表 - SAM_APPLY
            StringBuilder strSqlReport = new StringBuilder();
            strSqlReport.Append("update " + this.table_SAM_APPLY + " set ");
            strSqlReport.Append("fjytype_id='" + drReport["fjytype_id"] + "',");
            strSqlReport.Append("fsample_type_id='" + drReport["fsample_type_id"] + "',");
            strSqlReport.Append("fjz_flag=" + drReport["fjz_flag"] + ",");
            strSqlReport.Append("fcharge_flag=" + drReport["fcharge_flag"] + ",");
            strSqlReport.Append("fcharge_id='" + drReport["fcharge_id"] + "',");
            strSqlReport.Append("fstate='" + drReport["fstate"] + "',");
            strSqlReport.Append("fjyf=" + drReport["fjyf"] + ",");
            strSqlReport.Append("fapply_user_id='" + drReport["fapply_user_id"] + "',");
            strSqlReport.Append("fapply_dept_id='" + drReport["fapply_dept_id"] + "',");
            strSqlReport.Append("fapply_time='" + drReport["fapply_time"] + "',");
            strSqlReport.Append("ftype_id='" + drReport["ftype_id"] + "',");
            strSqlReport.Append("fhz_id='" + drReport["fhz_id"] + "',");
            strSqlReport.Append("fhz_zyh='" + drReport["fhz_zyh"] + "',");
            strSqlReport.Append("fsex='" + drReport["fsex"] + "',");
            strSqlReport.Append("fname='" + drReport["fname"] + "',");
            strSqlReport.Append("fvolk='" + drReport["fvolk"] + "',");
            strSqlReport.Append("fhymen='" + drReport["fhymen"] + "',");
            strSqlReport.Append("fage=" + drReport["fage"] + ",");
            strSqlReport.Append("fjob='" + drReport["fjob"] + "',");
            strSqlReport.Append("fgms='" + drReport["fgms"] + "',");
            strSqlReport.Append("fjob_org='" + drReport["fjob_org"] + "',");
            strSqlReport.Append("fadd='" + drReport["fadd"] + "',");
            strSqlReport.Append("ftel='" + drReport["ftel"] + "',");
            strSqlReport.Append("fage_unit='" + drReport["fage_unit"] + "',");
            strSqlReport.Append("fage_year=" + drReport["fage_year"] + ",");
            strSqlReport.Append("fage_month=" + drReport["fage_month"] + ",");
            strSqlReport.Append("fage_day=" + drReport["fage_day"] + ",");
            strSqlReport.Append("fward_num='" + drReport["fward_num"] + "',");
            strSqlReport.Append("froom_num='" + drReport["froom_num"] + "',");
            strSqlReport.Append("fbed_num='" + drReport["fbed_num"] + "',");
            strSqlReport.Append("fblood_abo='" + drReport["fblood_abo"] + "',");
            strSqlReport.Append("fblood_rh='" + drReport["fblood_rh"] + "',");
            strSqlReport.Append("fjymd='" + drReport["fjymd"] + "',");
            strSqlReport.Append("fdiagnose='" + drReport["fdiagnose"] + "',");
            strSqlReport.Append("fheat=" + drReport["fheat"] + ",");
            strSqlReport.Append("fxyld=" + drReport["fxyld"] + ",");
            strSqlReport.Append("fcyy='" + drReport["fcyy"] + "',");
            strSqlReport.Append("fxhdb=" + drReport["fxhdb"] + ",");
            strSqlReport.Append("fcreate_user_id='" + drReport["fcreate_user_id"] + "',");
            strSqlReport.Append("fcreate_time='" + drReport["fcreate_time"] + "',");
            strSqlReport.Append("fupdate_user_id='" + drReport["fupdate_user_id"] + "',");
            strSqlReport.Append("fupdate_time='" + drReport["fupdate_time"] + "',");
            strSqlReport.Append("fitem_group='" + drReport["fitem_group"] + "',");
            strSqlReport.Append("fremark='" + drReport["fremark"] + "'");
            strSqlReport.Append(" where fapply_id='" + drReport["fapply_id"] + "' ");
            updateList.Add(strSqlReport.ToString());
            #endregion
            #region 样本表 - SAM_SAMPLE
            StringBuilder strSqlSample = new StringBuilder();
            strSqlSample.Append("update " + this.table_SAM_SAMPLE + " set ");
            strSqlSample.Append("fapply_id='" + drReport["fapply_id"] + "',");
            strSqlSample.Append("fsample_barcode='" + drReport["fsample_barcode"] + "',");
            strSqlSample.Append("fsample_code='" + drReport["fsample_code"] + "',");
            strSqlSample.Append("fstate='" + drReport["fstate"] + "',");
            strSqlSample.Append("fexamine_flag=" + drReport["fexamine_flag"] + ",");
            strSqlSample.Append("fsampling_user_id='" + drReport["fsampling_user_id"] + "',");
            strSqlSample.Append("fsampling_time='" + drReport["fsampling_time"] + "',");
            strSqlSample.Append("fsampling_remark='" + drReport["fsampling_remark"] + "',");
            strSqlSample.Append("fsend_user_id='" + drReport["fsend_user_id"] + "',");
            strSqlSample.Append("fsend_user_time='" + drReport["fsend_user_time"] + "',");
            strSqlSample.Append("fget_user_id='" + drReport["fget_user_id"] + "',");
            strSqlSample.Append("fget_user_time='" + drReport["fget_user_time"] + "',");
            strSqlSample.Append("fget_user_remark='" + drReport["fget_user_remark"] + "',");
            strSqlSample.Append("fjy_instr='" + drReport["fjy_instr"] + "',");
            strSqlSample.Append("fjy_date='" + drReport["fjy_date"] + "',");
            strSqlSample.Append("fjy_group='" + drReport["fjy_group"] + "',");
            strSqlSample.Append("fjy_user_id='" + drReport["fjy_user_id"] + "',");
            strSqlSample.Append("fjy_time='" + drReport["fjy_time"] + "',");
            strSqlSample.Append("fexamine_user_id='" + drReport["fexamine_user_id"] + "',");
            strSqlSample.Append("fexamine_time='" + drReport["fexamine_time"] + "',");
            strSqlSample.Append("fhandling_user_id='" + drReport["fhandling_user_id"] + "',");
            strSqlSample.Append("fhandling_user_time='" + drReport["fhandling_user_time"] + "',");
            strSqlSample.Append("fhandling_remark='" + drReport["fhandling_remark"] + "',");
            strSqlSample.Append("fprint_flag=" + drReport["fprint_flag"] + ",");
            strSqlSample.Append("fprint_time='" + drReport["fprint_time"] + "',");
            strSqlSample.Append("fprint_count=" + drReport["fprint_count"] + ",");
            strSqlSample.Append("frelease_time='" + drReport["frelease_time"] + "',");
            strSqlSample.Append("fcreate_user_id='" + drReport["fcreate_user_id"] + "',");
            strSqlSample.Append("fcreate_time='" + drReport["fcreate_time"] + "',");
            strSqlSample.Append("fupdate_user_id='" + drReport["fupdate_user_id"] + "',");
            strSqlSample.Append("fupdate_time='" + drReport["fupdate_time"] + "',");
            strSqlSample.Append("fread_flag=" + drReport["fread_flag"] + ",");
            strSqlSample.Append("fread_user_id='" + drReport["fread_user_id"] + "',");
            strSqlSample.Append("fread_user_remark='" + drReport["fread_user_remark"] + "',");
            strSqlSample.Append("fremark='" + drReport["fremark"] + "'");
            strSqlSample.Append(" where fsample_id='" + drReport["fsample_id"] + "' ");
            updateList.Add(strSqlSample.ToString());
            #endregion
            #region 结果表 - SAM_JY_RESULT
            for (int i = 0; i < dtResult.Rows.Count; i++)
            {
                DataRow drResult = dtResult.Rows[i];
                StringBuilder strSqlRelult = new StringBuilder();
                strSqlRelult.Append("update " + this.table_SAM_JY_RESULT + " set ");
                strSqlRelult.Append("fapply_id='" + drResult["fapply_id"] + "',");
                strSqlRelult.Append("fitem_id='" + drResult["fitem_id"] + "',");
                strSqlRelult.Append("fitem_code='" + drResult["fitem_code"] + "',");
                strSqlRelult.Append("fitem_name='" + drResult["fitem_name"] + "',");
                strSqlRelult.Append("fitem_unit='" + drResult["fitem_unit"] + "',");
                strSqlRelult.Append("fitem_ref='" + drResult["fitem_ref"] + "',");
                strSqlRelult.Append("fitem_badge='" + drResult["fitem_badge"] + "',");
                strSqlRelult.Append("forder_by='" + drResult["forder_by"] + "',");
                strSqlRelult.Append("fvalue='" + drResult["fvalue"] + "',");
                strSqlRelult.Append("fod='" + drResult["fod"] + "',");
                strSqlRelult.Append("fcutoff='" + drResult["fcutoff"] + "',");
                strSqlRelult.Append("fremark='" + drResult["fremark"] + "'");
                strSqlRelult.Append(" where fresult_id='" + drResult["fresult_id"] + "' ");
                updateList.Add(strSqlRelult.ToString());
            }
            #endregion
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(ww.wwf.wwfbll.WWFInit.strDBConn, updateList);
        }

        /// <summary>
        /// 删除报告
        /// </summary>
        /// <param name="strfapply_id"></param>
        /// <param name="strfsample_id"></param>
        /// <returns></returns>
        public string BllReportDel(string strfapply_id, string strfsample_id)
        {
            IList sqlList = new ArrayList();
            string sql1 = "delete FROM " + this.table_SAM_APPLY + "  where fapply_id='" + strfapply_id + "'";
            string sql2 = "delete FROM " + this.table_SAM_SAMPLE + " where fsample_id='" + strfsample_id + "'";
            string sql3 = "delete FROM " + this.table_SAM_JY_RESULT + " where fapply_id='" + strfapply_id + "'";
            string sql4 = "delete FROM " + this.table_SAM_JY_IMG + " where fsample_id='" + strfsample_id + "'";
            sqlList.Add(sql1);
            sqlList.Add(sql2);
            sqlList.Add(sql3);
            sqlList.Add(sql4);
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(ww.wwf.wwfbll.WWFInit.strDBConn, sqlList);
        }
        /// <summary>
        /// 取得样本号
        /// </summary>
        /// <param name="fsample_code"></param>
        /// <param name="upORdown"></param>
        /// <returns></returns>
        public string BllGetfsampleCodeUpOrDown(string fsample_code, string upORdown)
        {
            if (fsample_code == "" || fsample_code == null)
            { }
            else
            {
                this.sam_jy_fsample_code = fsample_code.Length;
            }
            string strBW = "";
            switch (this.sam_jy_fsample_code)
            {
                case 2:
                    strBW = "0";
                    break;
                case 3:
                    strBW = "00";
                    break;
                case 4:
                    strBW = "000";
                    break;
                case 5:
                    strBW = "0000";
                    break;
                case 6:
                    strBW = "00000";
                    break;
                case 7:
                    strBW = "000000";
                    break;
                case 8:
                    strBW = "0000000";
                    break;
                case 9:
                    strBW = "00000000";
                    break;
                case 10:
                    strBW = "000000000";
                    break;
                default:
                    strBW = "";
                    break;
            }

            string strRCode = "";//返回的样本号
            if (fsample_code == "" || fsample_code == null || fsample_code.Length == 0)
            {
                if (upORdown == "up")
                {
                    fsample_code = strBW + "2";
                }
                else
                {
                    fsample_code = strBW + "0";
                }
            }
            int intLength = fsample_code.Length;


            int intDownNum = 1;
            if (upORdown == "up")
            {
                if (ww.wwf.com.Public.IsNumber(fsample_code))
                {
                    if (Convert.ToInt32(fsample_code) > 1)
                        intDownNum = Convert.ToInt32(fsample_code) - 1;
                }

            }
            if (upORdown == "down")
            {
                if (ww.wwf.com.Public.IsNumber(fsample_code))
                {
                    intDownNum = Convert.ToInt32(fsample_code) + 1;
                }

            }
            int intLiengthDownNum = intDownNum.ToString().Length;
            int intCX = intLength - intLiengthDownNum;
            switch (intCX)
            {
                case 1:
                    strRCode = "0" + intDownNum.ToString();
                    break;
                case 2:
                    strRCode = "00" + intDownNum.ToString();
                    break;
                case 3:
                    strRCode = "000" + intDownNum.ToString();
                    break;
                case 4:
                    strRCode = "0000" + intDownNum.ToString();
                    break;
                case 5:
                    strRCode = "00000" + intDownNum.ToString();
                    break;
                case 6:
                    strRCode = "000000" + intDownNum.ToString();
                    break;
                case 7:
                    strRCode = "0000000" + intDownNum.ToString();
                    break;
                case 8:
                    strRCode = "00000000" + intDownNum.ToString();
                    break;
                case 9:
                    strRCode = "000000000" + intDownNum.ToString();
                    break;
                case 10:
                    strRCode = "0000000000" + intDownNum.ToString();
                    break;
                default:
                    strRCode = intDownNum.ToString();
                    break;
            }


            return strRCode;
        }
        /// <summary>
        /// 修改打印时间
        /// </summary>
        /// <param name="fjy_id"></param>
        /// <returns></returns>
        public int BllReportUpdatePrintTime(string fsample_id)
        {
            string strTime = this.DbServerDateTim();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update " + this.table_SAM_SAMPLE + " set ");
            strSql.Append("fprint_time='" + strTime + "',");
            strSql.Append("fprint_count=fprint_count+1,");
            strSql.Append("fprint_flag=1,");
            strSql.Append("fupdate_user_id='" + LoginBLL.strPersonID + "',");
            strSql.Append("fupdate_time='" + strTime + "'");
            strSql.Append(" where fsample_id='" + fsample_id + "'");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }
        #endregion

        #region  项目 参考值 标识
        /// <summary>
        /// 项目表
        /// </summary>
        /// <param name="fitem_id"></param>
        /// <returns></returns>
        public DataTable BllItemDT(string fitem_id)
        {
            string sql = "SELECT (SELECT fname FROM SAM_TYPE t1 WHERE (ftype = '常用单位') AND (fcode = t.funit_name)) as funitname,* FROM SAM_ITEM t WHERE (fitem_id = '" + fitem_id + "')";

            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        /// <summary>
        /// 参考明细表
        /// </summary>
        /// <param name="fitem_id"></param>
        /// <returns></returns>
        public DataTable BllItemRefDT(string fitem_id)
        {
            string sql = "SELECT * FROM SAM_ITEM_REF WHERE (fitem_id = '" + fitem_id + "')";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }

        /// <summary>
        /// 取得 参考值
        /// </summary>
        /// <param name="drItem"></param>
        /// <returns></returns>
        public string BllItemfref(string strItem_id, int intfage, string strfsex, string strfsample_type_id)
        {
            DataRow drItem = BllItemDT(strItem_id).Rows[0];
            string fref = "";
            string fitem_id = "";//项目ID
            string fitem_fname = "";//项目名称 
            string fref_if_age = "0";//年龄有关
            string fref_if_sex = "0";//性别有关
            string fref_if_sample = "0";//样本有关
            string fref_if_method = "0";//方法有关


            fitem_id = drItem["fitem_id"].ToString();
            fitem_fname = drItem["fname"].ToString();
            fref_if_age = drItem["fref_if_age"].ToString();
            fref_if_sex = drItem["fref_if_sex"].ToString();
            fref_if_sample = drItem["fref_if_sample"].ToString();
            fref_if_method = drItem["fref_if_method"].ToString();

            fref = drItem["fref"].ToString();//如果参考值没找到对照的明细那么取项目上设置默认那个
            if (fref_if_age == "0" & fref_if_sex == "0" & fref_if_sample == "0" & fref_if_method == "0")
            {
            }
            else
            {
                DataTable dtSAM_ITEM_REF = null;//项目参考值明细
                dtSAM_ITEM_REF = BllItemRefDT(fitem_id);//取得参考明细表   

                //string intfage = "0";//取得当前年龄
                //string strfsex = "0";//取得当前性别
                //string strfsample_type_id = "";//取得当前样本ID        
                string strQuerySql = "";//查询参考值明细SQL构造
                //年龄-------
                if (fref_if_age == "1" & fref_if_sex == "0" & fref_if_sample == "0")//年龄
                {
                    strQuerySql = "(" + intfage + ">=fage_low) AND (" + intfage + "<fage_high)";
                }
                if (fref_if_age == "1" & fref_if_sex == "1" & fref_if_sample == "0")//年龄+性别
                {
                    strQuerySql = "('" + strfsex + "'=fsex) AND (" + intfage + ">=fage_low) AND (" + intfage + "<fage_high)";
                }
                if (fref_if_age == "1" & fref_if_sex == "1" & fref_if_sample == "1")//年龄+性别+样本
                {
                    strQuerySql = "('" + strfsample_type_id + "'=fsample_type_id) AND ('" + strfsex + "'=fsex) AND (" + intfage + ">=fage_low) AND (" + intfage + "<fage_high)";
                }
                //性别-------
                if (fref_if_age == "0" & fref_if_sex == "1" & fref_if_sample == "0")//性别
                {
                    strQuerySql = "('" + strfsex + "'=fsex)";
                }
                if (fref_if_age == "0" & fref_if_sex == "1" & fref_if_sample == "1")//性别+样本
                {
                    strQuerySql = "('" + strfsample_type_id + "'=fsample_type_id) AND ('" + strfsex + "'=fsex)";
                }
                //样本-------
                if (fref_if_age == "0" & fref_if_sex == "0" & fref_if_sample == "1")//样本
                {
                    strQuerySql = "('" + strfsample_type_id + "'=fsample_type_id)";
                }
                if (fref_if_age == "0" & fref_if_sex == "1" & fref_if_sample == "1")//样本+性别
                {
                    strQuerySql = "('" + strfsample_type_id + "'=fsample_type_id) AND ('" + strfsex + "'=fsex)";
                }
                //------------------
                DataRow[] frfList = dtSAM_ITEM_REF.Select(strQuerySql);
                if (frfList.Length > 0)
                {
                    foreach (DataRow drref in frfList)
                    {
                        fref = drref["fref"].ToString();
                        //MessageBox.Show("参考值为:" + fref + "\n\n" + strQuerySql);
                    }
                }
                //else
                //{
                //fref = fref;//如果参考值没找到对照的明细那么取项目上设置默认那个
                //WWMessage.MessageShowWarning("项目："+fitem_fname+" 的参考值设置有误！\n\n" + strQuerySql);
                //}
                // ↑↓
            }
            return fref;
        }
        /// <summary>
        /// 结果标记 编号
        /// </summary>
        /// <param name="fvalue">值</param>
        /// <param name="fref">参考值</param>
        /// <returns>1: 2:↓ 3:↑</returns>
        public int BllItemValueBJ(string fvalue, string fref)
        {
            int intBJ = 1;
            string[] strRefList = System.Text.RegularExpressions.Regex.Split(fref, @"--");
            if (Public.IsNumber(fvalue))
            {
                Double douFvalue = Convert.ToDouble(fvalue);
                if (strRefList.Length == 2)
                {
                    if (Public.IsNumber(strRefList[0].ToString()) & Public.IsNumber(strRefList[1].ToString()))
                    {
                        if (douFvalue < Convert.ToDouble(strRefList[0].ToString()))//太小
                        {
                            intBJ = 2;
                        }
                        if (douFvalue > Convert.ToDouble(strRefList[1].ToString()))//太大
                        {
                            intBJ = 3;
                        }

                    }
                }
            }
            //   return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, "SELECT fname FROM SAM_TYPE WHERE (ftype = '结果标记') AND (fcode = '" + intBJ + "')").ToString();
            return intBJ;
        }
        /// <summary>
        /// 结果标记 名称
        /// </summary>
        /// <param name="intBJ"></param>
        /// <returns></returns>
        public string BllItemValueBJFname(int intBJ)
        {
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, "SELECT fname FROM SAM_TYPE WHERE (ftype = '结果标记') AND (fcode = '" + intBJ + "')").ToString();
        }
        #endregion
    }
}
