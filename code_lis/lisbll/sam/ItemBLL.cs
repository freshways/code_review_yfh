using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ww.wwf.dao;
using System.Collections;
using ww.wwf.wwfbll;
namespace ww.lis.lisbll.sam
{
    /// <summary>
    /// 
    /// </summary>
    public class ItemBLL : DAOWWF
    {
        public ItemBLL()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //	
        }
        #region  已经启用的仪器
        /// <summary>
        /// 取得已经启用的仪器 如果为"kfz" 开发组，那么显示全部已经启用的，否则过滤检验组
        /// </summary>
        /// <param name="fuse_if">启用否</param>
        /// <param name="fjygroup_id">检验组，如果为"kfz" 开发组，那么显示全部已经启用的，否则过滤检验组</param>
        /// <returns></returns>
        public DataTable BllInstrDT(int fuse_if, string fjygroup_id)
        {
            string sql = "";
            if (fjygroup_id == "kfz")
            {
                sql = "SELECT finstr_id, fname, '(' + finstr_id + ')' + fname AS ShowName,fjytype_id FROM sam_instr WHERE (fuse_if = " + fuse_if + ") ORDER BY forder_by";
            }
            else
            {
                sql = "SELECT finstr_id, fname, '(' + finstr_id + ')' + fname AS ShowName,fjytype_id FROM sam_instr WHERE (fuse_if = " + fuse_if + ") AND (fjygroup_id = '" + fjygroup_id + "') ORDER BY forder_by";
            }
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }

        #endregion

        #region 项目
        public string BllItemHelpCodeUpdate(IList lisSql)
        {
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(ww.wwf.wwfbll.WWFInit.strDBConn, lisSql);
        }

        /// <summary>
        /// 新增一个项目
        /// </summary>
        /// <param name="strInstrID"></param>
        /// <param name="strCurrItemGuid"></param>
        /// <returns></returns>
        public int BllItemAdd(string strInstrID, string strCurrItemGuid, string strfjytype_id, string strfsam_type_id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into sam_item(");
            strSql.Append("fitem_id,fcheck_type_id,fsam_type_id,fitem_code,finstr_id,fcheck_method_id,fitem_type_id,fprint_type_id,fuse_if,fxs,fround,fref_high,fref_low,fref_if_age,fref_if_sex,fref_if_sample,fref_if_method,fprint_num_group,fvalue_dd,fjx_if,fvalue_sx,fvalue_xx,fjg_value_sx,fjg_value_xx,fvalue_day_num,fvalue_day_no,fcount,fprice,fprice_dd,fjz_if,fjz_no_if,fjz_tat,fjz_no_tat,fzk_if,freagent_num");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + strCurrItemGuid + "',");
            strSql.Append("'" + strfjytype_id + "',");
            strSql.Append("'" + strfsam_type_id + "',");
            strSql.Append("'',");
            strSql.Append("'" + strInstrID + "',");
            strSql.Append("'1',");
            strSql.Append("'4',");
            strSql.Append("'1',");
            strSql.Append("1,");
            strSql.Append("1,");
            strSql.Append("0,");
            strSql.Append("0,");
            strSql.Append("0,");
            strSql.Append("0,");
            strSql.Append("0,");
            strSql.Append("0,");
            strSql.Append("0,");
            strSql.Append("0,");
            strSql.Append("1,");
            strSql.Append("0,");
            strSql.Append("0,");
            strSql.Append("0,");
            strSql.Append("0,");
            strSql.Append("0,");
            strSql.Append("0,");
            strSql.Append("0,");
            strSql.Append("0,");
            strSql.Append("0,");
            strSql.Append("0,");
            strSql.Append("0,");
            strSql.Append("1,");
            strSql.Append("0,");
            strSql.Append("0,");
            strSql.Append("0,");
            strSql.Append("0");
            strSql.Append(")");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }
        
        /// <summary>
        /// 修改一个项目
        /// </summary>
        /// <returns></returns>
        public int BllItemUpdate(DataRow rowCurrItem)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update sam_item set ");
            strSql.Append("fitem_code='" + rowCurrItem["fitem_code"] + "',");
            strSql.Append("finstr_id='" + rowCurrItem["finstr_id"] + "',");
            strSql.Append("fcheck_type_id='" + rowCurrItem["fcheck_type_id"] + "',");
            strSql.Append("fsam_type_id='" + rowCurrItem["fsam_type_id"] + "',");
            strSql.Append("fitem_type_id='" + rowCurrItem["fitem_type_id"] + "',");
            strSql.Append("fcheck_method_id='" + rowCurrItem["fcheck_method_id"] + "',");
            strSql.Append("fname='" + rowCurrItem["fname"] + "',");
            strSql.Append("fname_j='" + rowCurrItem["fname_j"] + "',");
            strSql.Append("fhelp_code='" + rowCurrItem["fhelp_code"] + "',");
            strSql.Append("fhis_item_code='" + rowCurrItem["fhis_item_code"] + "',");
            strSql.Append("fxh_code='" + rowCurrItem["fxh_code"] + "',");
            strSql.Append("funit_name='" + rowCurrItem["funit_name"] + "',");
            strSql.Append("finstr_unit='" + rowCurrItem["finstr_unit"] + "',");
            strSql.Append("fuse_if=" + rowCurrItem["fuse_if"] + ",");
            strSql.Append("fxs=" + rowCurrItem["fxs"] + ",");
            strSql.Append("fqc_code='" + rowCurrItem["fqc_code"] + "',");
            strSql.Append("fqc_check_type='" + rowCurrItem["fqc_check_type"] + "',");
            strSql.Append("fvalue_type_id='" + rowCurrItem["fvalue_type_id"] + "',");
            strSql.Append("fround=" + rowCurrItem["fround"] + ",");
            strSql.Append("ftime='" + rowCurrItem["ftime"] + "',");
            strSql.Append("fsample_type_id='" + rowCurrItem["fsample_type_id"] + "',");
            strSql.Append("fuser_id='" + rowCurrItem["fuser_id"] + "',");
            strSql.Append("fname_e='" + rowCurrItem["fname_e"] + "',");
            strSql.Append("fzhb_code='" + rowCurrItem["fzhb_code"] + "',");
            strSql.Append("freport_code='" + rowCurrItem["freport_code"] + "',");
            strSql.Append("fref_high=" + rowCurrItem["fref_high"] + ",");
            strSql.Append("fref_low=" + rowCurrItem["fref_low"] + ",");
            strSql.Append("fref='" + rowCurrItem["fref"] + "',");
            strSql.Append("fref_if_age=" + rowCurrItem["fref_if_age"] + ",");
            strSql.Append("fref_if_sex=" + rowCurrItem["fref_if_sex"] + ",");
            strSql.Append("fref_if_sample=" + rowCurrItem["fref_if_sample"] + ",");
            strSql.Append("fref_if_method=" + rowCurrItem["fref_if_method"] + ",");
            strSql.Append("fprint_type_id='" + rowCurrItem["fprint_type_id"] + "',");
            strSql.Append("fprint_num='" + rowCurrItem["fprint_num"] + "',");
            strSql.Append("fprint_num_group=" + rowCurrItem["fprint_num_group"] + ",");
            strSql.Append("fvalue_remark='" + rowCurrItem["fvalue_remark"] + "',");
            strSql.Append("fvalue_dd=" + rowCurrItem["fvalue_dd"] + ",");
            strSql.Append("fjx_if=" + rowCurrItem["fjx_if"] + ",");
            strSql.Append("fjx_formula='" + rowCurrItem["fjx_formula"] + "',");
            strSql.Append("fvalue_sx=" + rowCurrItem["fvalue_sx"] + ",");
            strSql.Append("fvalue_xx=" + rowCurrItem["fvalue_xx"] + ",");
            strSql.Append("fjg_value_sx=" + rowCurrItem["fjg_value_sx"] + ",");
            strSql.Append("fjg_value_xx=" + rowCurrItem["fjg_value_xx"] + ",");
            strSql.Append("fyy='" + rowCurrItem["fyy"] + "',");
            strSql.Append("fvalue_day_num=" + rowCurrItem["fvalue_day_num"] + ",");
            strSql.Append("fvalue_day_no=" + rowCurrItem["fvalue_day_no"] + ",");
            strSql.Append("fcount=" + rowCurrItem["fcount"] + ",");
            strSql.Append("fprice=" + rowCurrItem["fprice"] + ",");
            strSql.Append("fprice_dd=" + rowCurrItem["fprice_dd"] + ",");
            strSql.Append("fjz_if=" + rowCurrItem["fjz_if"] + ",");
            strSql.Append("fjz_no_if=" + rowCurrItem["fjz_no_if"] + ",");
            strSql.Append("fjz_tat=" + rowCurrItem["fjz_tat"] + ",");
            strSql.Append("fjz_no_tat=" + rowCurrItem["fjz_no_tat"] + ",");
            strSql.Append("fzk_if=" + rowCurrItem["fzk_if"] + ",");
            strSql.Append("fgroup_id='" + rowCurrItem["fgroup_id"] + "',");
            strSql.Append("fcheck_calendar='" + rowCurrItem["fcheck_calendar"] + "',");
            strSql.Append("funsure='" + rowCurrItem["funsure"] + "',");
            strSql.Append("fyx_ljv='" + rowCurrItem["fyx_ljv"] + "',");
            strSql.Append("fyx_pdf='" + rowCurrItem["fyx_pdf"] + "',");
            strSql.Append("fyx_formula='" + rowCurrItem["fyx_formula"] + "',");
            strSql.Append("fperiod='" + rowCurrItem["fperiod"] + "',");
            strSql.Append("freagent_id='" + rowCurrItem["freagent_id"] + "',");
            strSql.Append("freagent_num=" + rowCurrItem["freagent_num"] + ",");
            strSql.Append("freagent_unit_id='" + rowCurrItem["freagent_unit_id"] + "',");
            strSql.Append("fhz_prepare='" + rowCurrItem["fhz_prepare"] + "',");
            strSql.Append("fremark='" + rowCurrItem["fremark"] + "',");
            strSql.Append("forder_by='" + rowCurrItem["forder_by"] + "'");
            strSql.Append(" where fitem_id='" + rowCurrItem["fitem_id"] + "'");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }
        /// <summary>
        /// 修改一个项目 质控标识
        /// </summary>
        /// <returns></returns>
        public int BllItemUpdatefzk_if(string strfitem_id, string intfzk_if)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update sam_item set ");
            strSql.Append("fzk_if=" + intfzk_if + "");
            strSql.Append(" where fitem_id='" + strfitem_id + "'");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }

        /// <summary>
        /// 修改打印序号
        /// </summary>
        /// <param name="fprint_num"></param>
        /// <returns></returns>
        public int BllItemUpdatePrintNum(string fitem_id, string fprint_num)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update sam_item set ");
            strSql.Append("fprint_num='" + fprint_num + "'");
            strSql.Append(" where fitem_id='" + fitem_id + "'");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }

        /// <summary>
        /// 已经启用的项目
        /// </summary>
        /// <param name="fuse_if"></param>
        /// <param name="finstr_id"></param>
        /// <returns></returns>
        public DataTable BllItem(int fuse_if, string finstr_id)
        {
            string sql = "";
            if (fuse_if == 1 || fuse_if == 0)
            {
                sql = "SELECT * FROM sam_item where fuse_if=" + fuse_if + " and finstr_id='" + finstr_id + "' order by convert(int,fprint_num)";
            }
            else
            {
                sql = "SELECT * FROM sam_item where finstr_id='" + finstr_id + "' order by convert(int,fprint_num)";
            }
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }

        /// <summary>
        /// 已经启用的项目 质控项目
        /// </summary>
        /// <param name="fuse_if"></param>
        /// <param name="finstr_id"></param>
        /// <returns></returns>
        public DataTable BllItemZK(int fzk_if, string finstr_id)
        {
            string sql = "";
            sql = "SELECT * FROM sam_item where fzk_if=" + fzk_if + " and finstr_id='" + finstr_id + "' order by fprint_num";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
      /// <summary>
      /// 
      /// </summary>
      /// <param name="strWhere"></param>
      /// <returns></returns>
        public DataTable BllItem(string strWhere)
        {
            string sql = "";
            if (strWhere == "" || strWhere == null)
            {
                sql = "SELECT (SELECT fname FROM SAM_TYPE t1 WHERE (t1.ftype = '常用单位') AND (t1.fcode = t.funit_name)) as funit_name_zw,* FROM sam_item t ";
            }
            else
            {
                strWhere = " where " + strWhere;
                sql = "SELECT (SELECT fname FROM SAM_TYPE t1 WHERE (t1.ftype = '常用单位') AND (t1.fcode = t.funit_name)) as funit_name_zw,* FROM sam_item t " + strWhere ;
            }
            sql += " order by finstr_id,convert(int,fprint_num)";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        /// <summary>
        /// 取得项目
        /// </summary>
        /// <param name="fuse_if"></param>
        /// <param name="finstr_id"></param>
        /// <param name="fhelp_code"></param>
        /// <returns></returns>
        public DataTable BllItem(int fuse_if, string finstr_id, string fhelp_code)
        {
            string sql = "SELECT * FROM sam_item t where fuse_if=" + fuse_if + " and finstr_id='" + finstr_id + "' and fhelp_code like '" + fhelp_code + "%' order by convert(int,fprint_num)";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }


        //add by wjz 20151122 在日期和样本号不为空的情况下，只显示列表中没有的化验项目▽
        /// <summary>
        /// 取得项目
        /// </summary>
        /// <param name="fuse_if"></param>
        /// <param name="finstr_id"></param>
        /// <param name="fhelp_code"></param>
        /// <returns></returns>
        public DataTable BllItem(int fuse_if, string finstr_id, string fhelp_code, string strDate, string strSampleNo)
        {
            string sql = "SELECT * FROM sam_item t where fuse_if=" + fuse_if + " and finstr_id='" + finstr_id + "' and fhelp_code like '" + fhelp_code + "%' "
                +" and fitem_code not in (SELECT bb.FItem FROM [Lis_Ins_Result] aa left outer join [Lis_Ins_Result_Detail] bb on aa.FTaskID = bb.FTaskID "
                                            + " where FInstrID = '" + finstr_id + "' and FResultID='" + strSampleNo + "' and (FDateTime >= '" + strDate + "' and FDateTime <= '" + strDate + "'+' 23:59:59')) "
                +"order by convert(int,fprint_num)";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        //add by wjz 20151122 在日期和样本号不为空的情况下，只显示列表中没有的化验项目△

        /// <summary>
        /// 取得项目
        /// </summary>
        /// <param name="fuse_if"></param>
        /// <param name="fhelp_code"></param>
        /// <returns></returns>
        public DataTable BllItemAll(int fuse_if, string fhelp_code)
        {
            string sql = "SELECT * FROM sam_item t where fuse_if=" + fuse_if + " and fhelp_code like '" + fhelp_code + "%' order by convert(int,fprint_num)";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }

        /// <summary>
        /// 删除项目
        /// </summary>
        /// <param name="fitem_id"></param>
        public string BllItemDel(string fitem_id)
        {
            IList sqlList = new ArrayList();
            sqlList.Add("delete FROM sam_item where fitem_id='" + fitem_id + "'");
            sqlList.Add("delete FROM sam_item_ref where fitem_id='" + fitem_id + "'");
            sqlList.Add("delete FROM sam_item_io where fitem_id='" + fitem_id + "'");
            sqlList.Add("delete FROM sam_item_value where fitem_id='" + fitem_id + "'");

            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(ww.wwf.wwfbll.WWFInit.strDBConn, sqlList);
            // ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryTransaction2
        }
        #endregion

        #region  参考值明细
        /// <summary>
        /// 据项目ID取项目明细
        /// </summary>
        /// <param name="fitem_id"></param>
        /// <returns></returns>
        public DataTable BllItemRefDT(string fitem_id)
        {
            string sql = "SELECT * FROM sam_item_ref where fitem_id='" + fitem_id + "'";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        /// <summary>
        /// 项目参考明细新增
        /// </summary>
        /// <param name="fitem_id"></param>
        public void BllItemRefAdd(string fitem_id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into sam_item_ref(");
            strSql.Append("fref_id,fage_high,fage_low,fref_high,fref_low,fitem_id");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + this.DbGuid() + "',");
            strSql.Append("0,");
            strSql.Append("0,");
            strSql.Append("0,");
            strSql.Append("0,");
            strSql.Append("'" + fitem_id + "'");
            strSql.Append(")");
            ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }
        /// <summary>
        /// 删除参考值
        /// </summary>
        /// <param name="fitem_id"></param>
        public void BllItemRefDel(string fref_id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete FROM sam_item_ref ");
            strSql.Append(" where fref_id='" + fref_id + "'");
            ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }
        public void BllItemRefUpdate(DataRow drRow)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update sam_item_ref set ");
            strSql.Append("fitem_id='" + drRow["fitem_id"] + "',");
            strSql.Append("fsample_type_id='" + drRow["fsample_type_id"] + "',");
            strSql.Append("fcheck_method_id='" + drRow["fcheck_method_id"] + "',");
            strSql.Append("fref='" + drRow["fref_low"] + "--" + drRow["fref_high"] + "',");
            strSql.Append("forder_by='" + drRow["forder_by"] + "',");
            strSql.Append("fsex='" + drRow["fsex"] + "',");
            strSql.Append("fage_high=" + drRow["fage_high"] + ",");
            strSql.Append("fage_low=" + drRow["fage_low"] + ",");
            strSql.Append("fref_high=" + drRow["fref_high"] + ",");
            strSql.Append("fref_low=" + drRow["fref_low"] + "");
            strSql.Append(" where fref_id='" + drRow["fref_id"] + "'");
            ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }
        #endregion


        #region  项目仪器接口
        /// <summary>
        /// 接口明细
        /// </summary>
        /// <param name="fitem_id"></param>
        /// <returns></returns>
        public DataTable BllItemIODT(string fitem_id)
        {
            string sql = "SELECT * FROM sam_item_io where fitem_id='" + fitem_id + "'";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        /// <summary>
        /// 接口新增
        /// </summary>
        /// <param name="fitem_id"></param>
        public void BllItemIOAdd(string fitem_id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into sam_item_io(");
            strSql.Append("fio_id,fitem_id,fcode,fuse_if");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + this.DbGuid() + "',");
            strSql.Append("'" + fitem_id + "',");
            strSql.Append("'',");
            strSql.Append("1");
            strSql.Append(")");
            ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }
        /// <summary>
        /// 删除接口
        /// </summary>
        /// <param name="fitem_id"></param>
        public void BllItemIODel(string fio_id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete FROM sam_item_io ");
            strSql.Append(" where fio_id='" + fio_id + "'");
            ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }
        /// <summary>
        /// 修改接口
        /// </summary>
        /// <param name="fio_id"></param>
        /// <param name="fcode"></param>
        public void BllItemIOUpdate(string fio_id, string fcode, string str仪器id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update sam_item_io set ");
            strSql.Append("finstr_id='" + str仪器id + "',");
            strSql.Append("fcode='" + fcode + "'");
            strSql.Append(" where fio_id='" + fio_id + "'");
            ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }
        #endregion

        #region  项目常用取值
        /// <summary>
        /// 常用取值明细
        /// </summary>
        /// <param name="fitem_id"></param>
        /// <returns></returns>
        public DataTable BllItemValueDT(string fitem_id)
        {
            string sql = "SELECT * FROM sam_item_value where fitem_id='" + fitem_id + "' order by forder_by";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        /// <summary>
        /// 常用取值新增
        /// </summary>
        /// <param name="fitem_id"></param>
        public void BllItemValueAdd(string fitem_id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into sam_item_value(");
            strSql.Append("fvalue_id,fitem_id,fvalue,fvalue_flag,forder_by");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + this.DbGuid() + "',");
            strSql.Append("'" + fitem_id + "',");
            strSql.Append("'',");
            strSql.Append("1,");
            strSql.Append("1");
            strSql.Append(")");
            ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }
        /// <summary>
        /// 删除常用取值
        /// </summary>
        /// <param name="fitem_id"></param>
        public void BllItemValueDel(string fvalue_id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete FROM sam_item_value ");
            strSql.Append(" where fvalue_id='" + fvalue_id + "'");
            ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }
        /// <summary>
        /// 修改常用取值
        /// </summary>
        /// <param name="fvalue_id"></param>
        /// <param name="fvalue"></param>
        /// <param name="fvalue_flag"></param>
        /// <param name="forder_by"></param>
        public void BllItemValueUpdate(string fvalue_id, string fvalue, string fvalue_flag, string forder_by)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update sam_item_value set ");
            strSql.Append("fvalue='" + fvalue + "',");
            strSql.Append("fvalue_flag='" + fvalue_flag + "',");
            strSql.Append("forder_by='" + forder_by + "'");
            strSql.Append(" where fvalue_id='" + fvalue_id + "'");
            ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }
        #endregion
        #region  项目组合 及 项目 关系
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fgroup_id"></param>
        /// <param name="fitem_id"></param>
        /// <returns></returns>
        public bool BllItemGroupRefExists(string fgroup_id, string fitem_id)
        {
            string sql = "SELECT count(1) FROM SAM_ITEM_GROUP_REL where fitem_id='" + fitem_id + "' and fgroup_id='" + fgroup_id + "'";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbRecordExists(ww.wwf.wwfbll.WWFInit.strDBConn, sql);
        }
        /// <summary>
        /// 项目及项目组关系
        /// </summary>
        /// <param name="fgroup_id"></param>
        /// <returns></returns>
        public DataTable BllItemAndGroupRefDT(string fgroup_id)
        {
            string sql = "SELECT (SELECT fname FROM SAM_TYPE t5 WHERE (t5.ftype = '常用单位') AND (t5.fcode = t1.funit_name)) AS funit_name_zw,t2.fid, t2.fitem_id, t2.fgroup_id, t1.fitem_code, t1.fname, t1.fprint_num,t1.fuse_if FROM SAM_ITEM t1 ,SAM_ITEM_GROUP_REL t2 where t1.fitem_id = t2.fitem_id and t1.fuse_if=1 and t2.fgroup_id='" + fgroup_id + "'  order by convert(int,t1.fprint_num)";

            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fgroup_id"></param>
        /// <param name="fitem_id"></param>
        /// <returns></returns>
        public int BllItemGroupRefAdd(string fgroup_id, string fitem_id)
        {
            string sql =
                "INSERT INTO SAM_ITEM_GROUP_REL (fid, fgroup_id, fitem_id) VALUES ('" + this.DbGuid() + "', '" + fgroup_id + "', '" + fitem_id + "')";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fid"></param>
        /// <returns></returns>
        public int BllItemGroupRefDel(string fid)
        {
            string sql = "DELETE FROM SAM_ITEM_GROUP_REL WHERE (fid = '" + fid + "')";

            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql);
        }
        #endregion
        #region  项目组合
        /// <summary>
        /// 生成助 符
        /// </summary>
        /// <param name="lisSql"></param>
        /// <returns></returns>
        public string BllItemGroupHelpCodeUpdate(IList lisSql)
        {
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(ww.wwf.wwfbll.WWFInit.strDBConn, lisSql);
        }
       
        /// <summary>
        /// 项目组合 列表
        /// </summary>
        /// <param name="fitem_id"></param>
        /// <returns></returns>
        public DataTable BllItemGroupDT(string strWhere)
        {
            string sql = "";
            if (strWhere == "" || strWhere == null)
                sql = "SELECT * FROM SAM_ITEM_GROUP order by forder_by";
            else
            {
                strWhere = " where " + strWhere;
                sql = "SELECT * FROM SAM_ITEM_GROUP " + strWhere + " order by forder_by";
            }
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }/*
        public DataTable BllItemGroupDTSelect(string strWhere)
        {
            DataTable dt = new DataTable();

            string sql = "";
            if (strWhere == "" || strWhere == null)
                sql = "SELECT * FROM SAM_ITEM_GROUP order by forder_by";
            else
            {
                strWhere = " where " + strWhere;
                sql = "SELECT * FROM SAM_ITEM_GROUP " + strWhere + " order by forder_by";
            }

            dt = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
          

            DataColumn dataColumn1 = new DataColumn("sel", typeof(int));
            dt.Columns.Add(dataColumn1);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["sel"] = "0";
            
            }
                return dt;
        }*/
        /// <summary>
        /// 项目组合 新增
        /// </summary>
        /// <param name="dr"></param>
        public int BllItemGroupAdd(DataRow dr)
        {
            //add by wjz 20151106 目的：新加一个项目，填写完项目信息，点击“保存”无效；▽
            dr["fgroup_id"] = this.DbGuid();
            //add by wjz 20151106 目的：新加一个项目，填写完项目信息，点击“保存”无效；△

            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into SAM_ITEM_GROUP(");
            strSql.Append("fgroup_id,fcode,fname,fname_e,fjz_if,fjz_no_if,fcheck_type_id,fsam_type_id,fhelp_code,fuse_if,forder_by,fremark");
            strSql.Append(")");
            strSql.Append(" values (");

            //changed by wjz 20151106 目的：新加一个项目，填写完项目信息，点击“保存”无效；▽
            //strSql.Append("'" + this.DbGuid() + "',");
            strSql.Append("'" + dr["fgroup_id"] + "',");
            //changed by wjz 20151106 目的：新加一个项目，填写完项目信息，点击“保存”无效；△
            strSql.Append("'" + dr["fcode"] + "',");
            strSql.Append("'" + dr["fname"] + "',");
            strSql.Append("'" + dr["fname_e"] + "',");
            strSql.Append("" + dr["fjz_if"] + ",");
            strSql.Append("" + dr["fjz_no_if"] + ",");
            strSql.Append("'" + dr["fcheck_type_id"] + "',");
            strSql.Append("'" + dr["fsam_type_id"] + "',");
            strSql.Append("'" + dr["fhelp_code"] + "',");
            strSql.Append("" + dr["fuse_if"] + ",");
            strSql.Append("'" + dr["forder_by"] + "',");
            strSql.Append("'" + dr["fremark"] + "'");
            strSql.Append(")");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }
        /// <summary>
        /// 项目组合 删除
        /// </summary>
        /// <param name="fitem_id"></param>
        public bool BllItemGroupDel(string fgroup_id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete FROM SAM_ITEM_GROUP ");
            strSql.Append(" where fgroup_id='" + fgroup_id + "'");

            StringBuilder strSql2 = new StringBuilder();
            strSql2.Append("delete FROM SAM_ITEM_GROUP_REL ");
            strSql2.Append(" where fgroup_id='" + fgroup_id + "'");

            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryTransaction2(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString(), strSql2.ToString());
        }
        /// <summary>
        /// 项目组合 修改
        /// </summary>
        /// <param name="dr"></param>
        public int BllItemGroupUpdate(DataRow dr)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update SAM_ITEM_GROUP set ");
            strSql.Append("fcode='" + dr["fcode"] + "',");
            strSql.Append("fname='" + dr["fname"] + "',");
            strSql.Append("fname_e='" + dr["fname_e"] + "',");
            strSql.Append("fjz_if=" + dr["fjz_if"] + ",");
            strSql.Append("fjz_no_if=" + dr["fjz_no_if"] + ",");
            strSql.Append("fcheck_type_id='" + dr["fcheck_type_id"] + "',");
            strSql.Append("fsam_type_id='" + dr["fsam_type_id"] + "',");
            strSql.Append("fhelp_code='" + dr["fhelp_code"] + "',");
            strSql.Append("fuse_if=" + dr["fuse_if"] + ",");
            strSql.Append("forder_by='" + dr["forder_by"] + "',");
            strSql.Append("fremark='" + dr["fremark"] + "'");
            strSql.Append(" where fgroup_id='" + dr["fgroup_id"] + "'");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }
        #endregion

        #region  检验类别>样本类别>项目组合>项目整合查询

        public DataTable BllCheckAndSampleTypeDT()
        {
            DataTable dt = new DataTable();
            DataColumn dataColumn1 = new DataColumn("fid", typeof(string));
            dt.Columns.Add(dataColumn1);

            DataColumn dataColumn2 = new DataColumn("fpid", typeof(string));
            dt.Columns.Add(dataColumn2);

            DataColumn dataColumn3 = new DataColumn("fname", typeof(string));
            dt.Columns.Add(dataColumn3);

            DataColumn dataColumn4 = new DataColumn("tag", typeof(string));
            dt.Columns.Add(dataColumn4);

            DataTable dtCheckType = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, "SELECT fcheck_type_id, fname FROM SAM_CHECK_TYPE WHERE (fuse_if = 1) ORDER BY forder_by").Tables[0];           
            string strfcheck_type_id = "";
            for (int i = 0; i < dtCheckType.Rows.Count; i++)
            {
                DataRow drNewRow = dt.NewRow();
                strfcheck_type_id = dtCheckType.Rows[i]["fcheck_type_id"].ToString();
                drNewRow["fid"] = strfcheck_type_id;
                drNewRow["fpid"] = "-1";
                drNewRow["tag"] = "CheckType";
                drNewRow["fname"] = dtCheckType.Rows[i]["fname"];
                dt.Rows.Add(drNewRow);

                DataTable dtSampleType = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, "SELECT t2.fcheck_type_id, t2.fsample_type_id, t2.fid, t1.fname, t1.forder_by,t1.fhelp_code FROM SAM_CHECK_SAMPLE t2,SAM_SAMPLE_TYPE t1 where t2.fsample_type_id = t1.fsample_type_id and t2.fcheck_type_id='" + strfcheck_type_id + "' and t1.fuse_if=1 order by t1.forder_by").Tables[0];
                for (int ii = 0; ii < dtSampleType.Rows.Count; ii++)
                {
                    DataRow drNewRowSame = dt.NewRow();
                    drNewRowSame["fid"] = dtSampleType.Rows[ii]["fsample_type_id"].ToString();
                    drNewRowSame["fpid"] = strfcheck_type_id;
                    drNewRowSame["tag"] = "SampleType";
                    drNewRowSame["fname"] = dtSampleType.Rows[ii]["fname"].ToString();
                    dt.Rows.Add(drNewRowSame);
                }
            }
            return dt;
        }
        #endregion

        #region  收费大项>收费小项>检验项目>项目对照查询

        /// <summary>
        /// 收费大项
        /// </summary>
        /// <returns></returns>
        public DataTable BllGe收费大项()
        {
            DataTable dt = new DataTable();
            DataColumn dataColumn1 = new DataColumn("fid", typeof(string));
            dt.Columns.Add(dataColumn1);

            DataColumn dataColumn2 = new DataColumn("fpid", typeof(string));
            dt.Columns.Add(dataColumn2);

            DataColumn dataColumn3 = new DataColumn("fname", typeof(string));
            dt.Columns.Add(dataColumn3);

            DataColumn dataColumn4 = new DataColumn("tag", typeof(string));
            dt.Columns.Add(dataColumn4);

            DataTable dtCheckType = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strCHisDBConn, "select * from vw_lis_收费大项 ").Tables[0];
            string strfcheck_type_id = "";
            for (int i = 0; i < dtCheckType.Rows.Count; i++)
            {
                DataRow drNewRow = dt.NewRow();
                strfcheck_type_id = dtCheckType.Rows[i]["项目编码"].ToString();
                drNewRow["fid"] = strfcheck_type_id;
                drNewRow["fpid"] = "-1";
                drNewRow["tag"] = "SampleType";
                drNewRow["fname"] = dtCheckType.Rows[i]["项目名称"];
                dt.Rows.Add(drNewRow);

                //DataTable dtSampleType = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, "SELECT t2.fcheck_type_id, t2.fsample_type_id, t2.fid, t1.fname, t1.forder_by,t1.fhelp_code FROM SAM_CHECK_SAMPLE t2,SAM_SAMPLE_TYPE t1 where t2.fsample_type_id = t1.fsample_type_id and t2.fcheck_type_id='" + strfcheck_type_id + "' and t1.fuse_if=1 order by t1.forder_by").Tables[0];
                //for (int ii = 0; ii < dtSampleType.Rows.Count; ii++)
                //{
                //    DataRow drNewRowSame = dt.NewRow();
                //    drNewRowSame["fid"] = dtSampleType.Rows[ii]["fsample_type_id"].ToString();
                //    drNewRowSame["fpid"] = strfcheck_type_id;
                //    drNewRowSame["tag"] = "SampleType";
                //    drNewRowSame["fname"] = dtSampleType.Rows[ii]["fname"].ToString();
                //    dt.Rows.Add(drNewRowSame);
                //}
            }
            return dt;
        }

        /// <summary>
        /// 收费小项 列表
        /// </summary>
        /// <param name="fhelp_code"></param>
        /// <returns></returns>
        public DataTable BllGet收费小项(string fhelp_code)
        {
            string sWhere="where 1=1 ";
            if (fhelp_code != "")
                sWhere += "and 拼音代码 like '" + fhelp_code + "%' ";
            //收费编码 ,收费名称 ,单价 ,归并编码,拼音代码,fuse_if ,执行科室编码,b.检验ID,b.检验编码,b.检验名称,b.检验设备 
            string sql = "";
            //strWhere = " where a.收费编码 *= b.收费编码 " + strWhere;
            //sql = "select a.收费编码 ,a.收费名称 ,单价 ,归并编码,拼音代码 ,fuse_if,执行科室编码,b.检验ID,b.检验编码,b.检验名称,b.检验设备  " +
            //    "from his.chis2719.dbo.vw_lis_收费小项 a,SAM_收费项目对照 b " + strWhere + " order by a.收费编码";
            sql = "select 收费编码 ,收费名称 ,单价 ,归并编码,拼音代码 ,fuse_if,执行科室编码 from vw_lis_收费小项 " + sWhere;
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strCHisDBConn, sql).Tables[0];
        }

        /// <summary>
        /// 校验检验项目与收费项目
        /// </summary>
        /// <param name="fgroup_id"></param>
        /// <param name="fitem_id"></param>
        /// <returns></returns>
        public bool Bll检查对应项目(string s检验id)
        {
            string sql = "SELECT count(1) FROM SAM_收费项目对照 where 检验ID='" + s检验id + "' ";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbRecordExists(ww.wwf.wwfbll.WWFInit.strDBConn, sql);
        }

        /// <summary>
        /// 获取项目对应列表
        /// </summary>
        /// <param name="fgroup_id"></param>
        /// <returns></returns>
        public DataTable BllIGet对应关系(string s收费编码)
        {
            string sql = 
                            "select id,收费编码 ,收费名称,检验ID,检验编码,检验名称,检验设备 from SAM_收费项目对照 t  "+ 
                            "where 收费编码='" + s收费编码 + "'  order by ("+
                            "SELECT top 1 convert(int,z.fprint_num) FROM   SAM_ITEM z WHERE (z.fitem_id = t.检验ID)) ";

            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }

        /// <summary>
        /// 插入对应项目
        /// </summary>
        /// <param name="fgroup_id"></param>
        /// <param name="fitem_id"></param>
        /// <returns></returns>
        public int Bll插入对应项目(string s收费编码,string s收费名称,string 单价, string s检验项目ID)
        {
            string sql =
                "insert into SAM_收费项目对照(收费编码 ,收费名称,单价,检验ID,检验编码,检验名称,检验设备)  " +
                "select '" + s收费编码 + "' ,'" + s收费名称 + "' ," + 单价 + ", fitem_id 检验ID,fitem_code 检验编码,fname 检验名称,finstr_id 检验设备 " +
                "from SAM_ITEM where fitem_id='" + s检验项目ID + "' ";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql);
        }
        /// <summary>
        /// 更新对应项目
        /// </summary>
        /// <param name="fgroup_id"></param>
        /// <param name="fitem_id"></param>
        /// <returns></returns>
        public int Bll更新对应项目(string s收费编码,string s收费名称,string 单价, string s检验项目ID, string s检验编码, string s检验名称, string s检验设备)
        {
            string sql =
                "update SAM_收费项目对照 set 收费编码='" + s收费编码 + "',收费名称='" + s收费名称 + "',单价='" + 单价 + "',检验编码='" +
                s检验编码 + "',检验名称='" + s检验名称 + "',检验设备='" + s检验设备 + "' " +
                "where 检验ID ='" + s检验项目ID + "' ";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql);
        }

        /// <summary>
        /// 删除对应项目
        /// 2015-06-06 10:31:44 yufh调整，删除变更为更新，把对应项目置空
        /// </summary>
        /// <param name="fid"></param>
        /// <returns></returns>
        public int BllI删除对应项目(string fid)
        {
            //string sql = "delete from SAM_收费项目对照 where (id= '" + fid + "')";
            string sql = "update SAM_收费项目对照 set 收费编码=null,收费名称=null,单价=null where  检验ID='" + fid + "' ";

            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql);
        }

        /// <summary>
        /// 取得项目
        /// </summary>
        /// <param name="fuse_if">启用状态</param>
        /// <param name="finstr">设备id</param>
        /// <param name="fhelp_code">助记码</param>
        /// <returns></returns>
        public DataTable BllGet项目列表(int fuse_if, string finstr, string fhelp_code)
        {
            string sWhere = " and  fuse_if=" + fuse_if + " ";
            if (finstr != "")
                sWhere += " and finstr_id='" + finstr + "' ";
            if (fhelp_code != "")
                sWhere += " and fhelp_code like '" + fhelp_code + "%' ";
            string sql = @"SELECT 0 选择,fitem_id 检验id,fname 检验项目,fitem_code 检验编码,finstr_id 设备,  fhelp_code 助记符 ,b.收费名称,b.收费编码,b.单价
                                    FROM sam_item t left join SAM_收费项目对照 b on  t.fitem_id = b.检验ID  where 1=1  " + sWhere + " order by convert(int,fprint_num)";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }

        #endregion

        /// <summary>
        /// 获取项目常用值列表
        /// </summary>
        public DataTable GetItemValueList(string itemid)
        {
            string strSql = "SELECT [fvalue],[forder_by] FROM [dbo].[SAM_ITEM_VALUE] WHERE fitem_id=@itemid  ORDER BY CAST(forder_by AS INT) ";
            List<System.Data.SqlClient.SqlParameter> paramlist = new List<System.Data.SqlClient.SqlParameter>();
            paramlist.Add(new System.Data.SqlClient.SqlParameter("@itemid", itemid));
            DataSet ds = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql, paramlist.ToArray());
            if (ds == null || ds.Tables.Count == 0)
            {
                return null;
            }
            else
            {
                return ds.Tables[0];
            }
        }

    }
}
/*
 4.2.2.11	仪器类别 sam_instr_type
Name	Code	Data Type	Primary	Foreign Key	Mandatory
仪器类型_id	ftype_id	varchar(32)	TRUE	FALSE	TRUE
上级_id	fp_id	varchar(32)	FALSE	FALSE	FALSE
代号	fcode	varchar(32)	FALSE	FALSE	FALSE
名称	fname	varchar(220)	FALSE	FALSE	FALSE
英文名称	fname_e	varchar(200)	FALSE	FALSE	FALSE
启用否	fuse_if	int	FALSE	FALSE	FALSE
序号	forder_by	int	FALSE	FALSE	FALSE
助记符	fhelp_code	varchar(32)	FALSE	FALSE	FALSE
备注	fremark	varchar(200)	FALSE	FALSE	FALSE
4.2.2.12	仪器档案 sam_instr
Name	Code	Data Type	Primary	Foreign Key	Mandatory
仪器_id	finstr_id	varchar(32)	TRUE	FALSE	TRUE
仪器类型_id	ftype_id	varchar(32)	FALSE	TRUE	FALSE
检验组_id	fjygroup_id	varchar(32)	FALSE	FALSE	FALSE
检验类型_id	fjytype_id	varchar(32)	FALSE	FALSE	FALSE
启用否	fuse_if	int	FALSE	FALSE	FALSE
代号	fcode	varchar(32)	FALSE	FALSE	FALSE
名称	fname	varchar(220)	FALSE	FALSE	FALSE
英文名称	fname_e	varchar(200)	FALSE	FALSE	FALSE
序号	forder_by	int	FALSE	FALSE	FALSE
备注	fremark	varchar(200)	FALSE	FALSE	FALSE
机身号	fbody_no	varchar(32)	FALSE	FALSE	FALSE
新旧状态	fnew_old	varchar(32)	FALSE	FALSE	FALSE
购买价格	fprice	float	FALSE	FALSE	FALSE
图片	fimg	varchar(32)	FALSE	FALSE	FALSE
单位	funit	varchar(32)	FALSE	FALSE	FALSE
设备原值	fy_value	float(15)	FALSE	FALSE	FALSE
设备净值	fj_value	float(15)	FALSE	FALSE	FALSE
年折旧率	fzjl	float(15)	FALSE	FALSE	FALSE
已使用年限	fuse_year	int	FALSE	FALSE	FALSE
出厂日期	fout_date	varchar(32)	FALSE	FALSE	FALSE
到货日期	fdh_date	varchar(32)	FALSE	FALSE	FALSE
投用日期	fty_date	varchar(32)	FALSE	FALSE	FALSE
供应商_id	fgys_id	varchar(32)	FALSE	FALSE	FALSE
生产厂家_id	fcj_id	varchar(32)	FALSE	FALSE	FALSE
设备位置	fwz	varchar(32)	FALSE	FALSE	FALSE
国别	fgb	varchar(32)	FALSE	FALSE	FALSE
上帐日期	fsz_date	varchar(32)	FALSE	FALSE	FALSE
档案号	fdah	varchar(32)	FALSE	FALSE	FALSE
主要监护人	fjfr	varchar(32)	FALSE	FALSE	FALSE
创建人	fuser	varchar(32)	FALSE	FALSE	FALSE
创建时间	fdate	varchar(32)	FALSE	FALSE	FALSE
使用年限	flx	int	FALSE	FALSE	FALSE
折旧终止期	fzjzz_date	varchar(32)	FALSE	FALSE	FALSE
规格型号	fspec	varchar(32)	FALSE	FALSE	FALSE
助记符	fhelp_code	varchar(32)	FALSE	FALSE	FALSE
性能指标	fxlzb	varchar(32)	FALSE	FALSE	FALSE
 */