using System;
using System.Collections.Generic;
using System.Text;

namespace ww.lis.lisbll.sam
{
    /// <summary>
    /// 实体类SAM_JY 。(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    public class jymodel
    {
        public jymodel()
        { }
        #region Model
        private string _fjy_id;
        private string _fjy_date;
        private string _fjy_instr;
        private string _fjy_yb_tm;
        private string _fjy_yb_code;
        private string _fjy_yb_type;
        private string _fjy_sf_type;
        private decimal _fjy_f;
        private string _fjy_f_ok;
        private string _fjy_zt;
        private string _fjy_md;
        private string _fjy_lczd;
        private string _fhz_id;
        private string _fhz_type_id;
        private string _fhz_zyh;
        private string _fhz_name;
        private string _fhz_sex;
        private decimal _fhz_age;
        private string _fhz_age_unit;
        private string _fhz_age_date;
        private string _fhz_bq;
        private string _fhz_room;
        private string _fhz_bed;
        private string _fhz_volk;
        private string _fhz_hy;
        private string _fhz_job;
        private string _fhz_gms;
        private string _fhz_add;
        private string _fhz_tel;
        private string _fapply_id;
        private string _fapply_user_id;
        private string _fapply_dept_id;
        private string _fapply_time;
        private string _fsampling_user_id;
        private string _fsampling_time;
        private string _fjy_user_id;
        private string _fchenk_user_id;
        private string _fcheck_time;
        private string _freport_time;
        private string _fprint_time;
        private string _fprint_zt;
        private int _fprint_count;
        private string _fsys_user;
        private string _fsys_time;
        private string _fsys_dept;
        private string _fremark;
        private string _f1;
        private string _f2;
        private string _f3;
        private string _f4;
        private string _f5;
        private string _f6;
        private string _f7;
        private string _f8;
        private string _f9;
        private string _f10;
        private string _fhz_dept;
        private string _finstr_result_id;
        private string _sfzh;

        /// <summary>
        /// 身份证号
        /// </summary>
        public string S身份证号
        {
            set { _sfzh = value; }
            get { return _sfzh; }
        }

        /// <summary>
        /// 仪器检验结果id
        /// </summary>
        public string finstr_result_id
        {
            set { _finstr_result_id = value; }
            get { return _finstr_result_id; }
        }
        /// <summary>
        /// 检验_id
        /// </summary>
        public string fjy_检验id
        {
            set { _fjy_id = value; }
            get { return _fjy_id; }
        }
        /// <summary>
        /// 检验日期
        /// </summary>
        public string fjy_检验日期
        {
            set { _fjy_date = value; }
            get { return _fjy_date; }
        }
        /// <summary>
        /// 仪器id
        /// </summary>
        public string fjy_仪器ID
        {
            set { _fjy_instr = value; }
            get { return _fjy_instr; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fjy_yb_tm
        {
            set { _fjy_yb_tm = value; }
            get { return _fjy_yb_tm; }
        }
        /// <summary>
        /// 检验样本条码号
        /// </summary>
        public string fjy_yb_code
        {
            set { _fjy_yb_code = value; }
            get { return _fjy_yb_code; }
        }
        /// <summary>
        /// 检验样本类型id
        /// </summary>
        public string fjy_yb_type
        {
            set { _fjy_yb_type = value; }
            get { return _fjy_yb_type; }
        }
        /// <summary>
        /// 检验费别id
        /// </summary>
        public string fjy_收费类型ID
        {
            set { _fjy_sf_type = value; }
            get { return _fjy_sf_type; }
        }
        /// <summary>
        /// 检验费
        /// </summary>
        public decimal fjy_f
        {
            set { _fjy_f = value; }
            get { return _fjy_f; }
        }
        /// <summary>
        /// 检验收费否
        /// </summary>
        public string fjy_f_ok
        {
            set { _fjy_f_ok = value; }
            get { return _fjy_f_ok; }
        }
        /// <summary>
        /// 检验状态
        /// </summary>
        public string fjy_zt检验状态
        {
            set { _fjy_zt = value; }
            get { return _fjy_zt; }
        }
        /// <summary>
        /// 检验目的
        /// </summary>
        public string fjy_md
        {
            set { _fjy_md = value; }
            get { return _fjy_md; }
        }
        /// <summary>
        /// 检验临床诊断
        /// </summary>
        public string fjy_lczd
        {
            set { _fjy_lczd = value; }
            get { return _fjy_lczd; }
        }
        /// <summary>
        /// 患者id
        /// </summary>
        public string fhz_id
        {
            set { _fhz_id = value; }
            get { return _fhz_id; }
        }
        /// <summary>
        /// 患者类型_id
        /// </summary>
        public string fhz_患者类型id
        {
            set { _fhz_type_id = value; }
            get { return _fhz_type_id; }
        }
        /// <summary>
        /// 患者住院号
        /// </summary>
        public string fhz_住院号
        {
            set { _fhz_zyh = value; }
            get { return _fhz_zyh; }
        }
        /// <summary>
        /// 患者姓名
        /// </summary>
        public string fhz_姓名
        {
            set { _fhz_name = value; }
            get { return _fhz_name; }
        }
        /// <summary>
        /// 患者性别
        /// </summary>
        public string fhz_性别
        {
            set { _fhz_sex = value; }
            get { return _fhz_sex; }
        }
        /// <summary>
        /// 患者年龄
        /// </summary>
        public decimal fhz_年龄
        {
            set { _fhz_age = value; }
            get { return _fhz_age; }
        }
        /// <summary>
        /// 患者年龄单位
        /// </summary>
        public string fhz_年龄单位
        {
            set { _fhz_age_unit = value; }
            get { return _fhz_age_unit; }
        }
        /// <summary>
        /// 患者生日
        /// </summary>
        public string fhz_生日
        {
            set { _fhz_age_date = value; }
            get { return _fhz_age_date; }
        }
        /// <summary>
        /// 患者病区
        /// </summary>
        public string fhz_病区
        {
            set { _fhz_bq = value; }
            get { return _fhz_bq; }
        }
        /// <summary>
        /// 患者房间号
        /// </summary>
        public string fhz_room
        {
            set { _fhz_room = value; }
            get { return _fhz_room; }
        }
        /// <summary>
        /// 患者床号
        /// </summary>
        public string fhz_床号
        {
            set { _fhz_bed = value; }
            get { return _fhz_bed; }
        }
        /// <summary>
        /// 患者民族
        /// </summary>
        public string fhz_volk
        {
            set { _fhz_volk = value; }
            get { return _fhz_volk; }
        }
        /// <summary>
        /// 患者婚姻
        /// </summary>
        public string fhz_hy
        {
            set { _fhz_hy = value; }
            get { return _fhz_hy; }
        }
        /// <summary>
        /// 患者职业
        /// </summary>
        public string fhz_job
        {
            set { _fhz_job = value; }
            get { return _fhz_job; }
        }
        /// <summary>
        /// 患者过敏史
        /// </summary>
        public string fhz_gms
        {
            set { _fhz_gms = value; }
            get { return _fhz_gms; }
        }
        /// <summary>
        /// 患者住址
        /// </summary>
        public string fhz_add
        {
            set { _fhz_add = value; }
            get { return _fhz_add; }
        }
        /// <summary>
        /// 患者电话
        /// </summary>
        public string fhz_tel
        {
            set { _fhz_tel = value; }
            get { return _fhz_tel; }
        }
        /// <summary>
        /// 申请单id
        /// </summary>
        public string fapply_申请单ID
        {
            set { _fapply_id = value; }
            get { return _fapply_id; }
        }
        /// <summary>
        /// 申请人/送检医师
        /// </summary>
        public string fapply_user_id
        {
            set { _fapply_user_id = value; }
            get { return _fapply_user_id; }
        }
        /// <summary>
        /// 申请科室_id/病人科室
        /// </summary>
        public string fapply_dept_id
        {
            set { _fapply_dept_id = value; }
            get { return _fapply_dept_id; }
        }
        /// <summary>
        /// 申请时间/送检时间
        /// </summary>
        public string fapply_申请时间
        {
            set { _fapply_time = value; }
            get { return _fapply_time; }
        }
        /// <summary>
        /// 采样人_id
        /// </summary>
        public string fsampling_user_id
        {
            set { _fsampling_user_id = value; }
            get { return _fsampling_user_id; }
        }
        /// <summary>
        /// 采样时间
        /// </summary>
        public string fsampling_采样时间
        {
            set { _fsampling_time = value; }
            get { return _fsampling_time; }
        }
        /// <summary>
        /// 检验人_id
        /// </summary>
        public string fjy_user_id
        {
            set { _fjy_user_id = value; }
            get { return _fjy_user_id; }
        }
        /// <summary>
        /// 审核医师_id
        /// </summary>
        public string fchenk_审核医师ID
        {
            set { _fchenk_user_id = value; }
            get { return _fchenk_user_id; }
        }
        /// <summary>
        /// 审核时间
        /// </summary>
        public string fcheck_审核时间
        {
            set { _fcheck_time = value; }
            get { return _fcheck_time; }
        }
        /// <summary>
        /// 报告时间
        /// </summary>
        public string freport_报告时间
        {
            set { _freport_time = value; }
            get { return _freport_time; }
        }
        /// <summary>
        /// 打印时间
        /// </summary>
        public string fprint_time
        {
            set { _fprint_time = value; }
            get { return _fprint_time; }
        }
        /// <summary>
        /// 打印状态
        /// </summary>
        public string fprint_zt
        {
            set { _fprint_zt = value; }
            get { return _fprint_zt; }
        }
        /// <summary>
        /// 打印次数
        /// </summary>
        public int fprint_count
        {
            set { _fprint_count = value; }
            get { return _fprint_count; }
        }
        /// <summary>
        /// 系统用户
        /// </summary>
        public string fsys_用户ID
        {
            set { _fsys_user = value; }
            get { return _fsys_user; }
        }
        /// <summary>
        /// 系统时间
        /// </summary>
        public string fsys_创建时间
        {
            set { _fsys_time = value; }
            get { return _fsys_time; }
        }
        /// <summary>
        /// 系统部门
        /// </summary>
        public string fsys_部门ID
        {
            set { _fsys_dept = value; }
            get { return _fsys_dept; }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string fremark_备注
        {
            set { _fremark = value; }
            get { return _fremark; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string f1
        {
            set { _f1 = value; }
            get { return _f1; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string f2
        {
            set { _f2 = value; }
            get { return _f2; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string f3
        {
            set { _f3 = value; }
            get { return _f3; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string f4
        {
            set { _f4 = value; }
            get { return _f4; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string f5
        {
            set { _f5 = value; }
            get { return _f5; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string f6
        {
            set { _f6 = value; }
            get { return _f6; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string f7
        {
            set { _f7 = value; }
            get { return _f7; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string f8
        {
            set { _f8 = value; }
            get { return _f8; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string f9
        {
            set { _f9 = value; }
            get { return _f9; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string f10
        {
            set { _f10 = value; }
            get { return _f10; }
        }

        /// <summary>
        /// 患者科室
        /// </summary>
        public string fhz_dept患者科室
        {
            set { _fhz_dept = value; }
            get { return _fhz_dept; }
        }
        #endregion Model

    }
}

