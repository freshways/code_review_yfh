using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ww.wwf.dao;
using ww.wwf.wwfbll;
using System.Collections;
namespace ww.lis.lisbll.sam
{
    /// <summary>
    ///接口业务逻辑
    /// </summary>
    /// <remarks>
    /// 作者：tao
    /// 时间：2008.2
    /// 改动：[tao；2008.2]
    /// </remarks>
    public class InstrIOBll : DAOWWF
    {

        public InstrIOBll()
        {
            ///
            /// TODO: 在此处添加构造函数逻辑
            ///     

        }

     

        #region  仪器接口 数据
        /// <summary>
        /// 取得接口表
        /// </summary>
        /// <returns></returns>
        public DataTable BllIODT(string strWhere)
        {
            if (strWhere == "" || strWhere == null)
            {
                return null;
            }
            else
            {
                strWhere = " Where " + strWhere + " order by fsample_code";
                return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, "SELECT * FROM SAM_INSTR_IO" + strWhere).Tables[0];
            }

        }

        /// <summary>
        /// 仪器原始数据新增
        /// </summary>
        /// <param name="drRowIO">接口原始数据行</param>
        /// <param name="dtImg">接口图</param>
        /// <param name="dtResult">译后结果</param>
        /// <returns></returns>
        public string BllIOAdd(DataRow drRowIO, DataTable dtImg, DataTable dtResult)
        {
            IList sqlList = new ArrayList();
            StringBuilder strSqlIO = new StringBuilder();
            strSqlIO.Append("insert into SAM_INSTR_IO(");
            strSqlIO.Append("fio_id,finstr_id,fdate,fsample_code,fsam_type_id,fjz,fqc,fstate,fcontent,ftime,fremark");
            strSqlIO.Append(")");
            strSqlIO.Append(" values (");
            strSqlIO.Append("'" + drRowIO["fio_id"] + "',");
            strSqlIO.Append("'" + drRowIO["finstr_id"] + "',");
            strSqlIO.Append("'" + drRowIO["fdate"] + "',");
            strSqlIO.Append("'" + drRowIO["fsample_code"] + "',");
            strSqlIO.Append("'" + drRowIO["fsam_type_id"] + "',");
            strSqlIO.Append("" + drRowIO["fjz"] + ",");
            strSqlIO.Append("" + drRowIO["fqc"] + ",");
            strSqlIO.Append("'" + drRowIO["fstate"] + "',");
            strSqlIO.Append("'" + drRowIO["fcontent"] + "',");
            strSqlIO.Append("'" + drRowIO["ftime"] + "',");
            strSqlIO.Append("'" + drRowIO["fremark"] + "'");
            strSqlIO.Append(")");

            sqlList.Add(strSqlIO.ToString());
            for (int i = 0; i < dtImg.Rows.Count; i++)
            {
                DataRow drRowImg = dtImg.Rows[i];
                StringBuilder strSqlImg = new StringBuilder();
                strSqlImg.Append("insert into SAM_INSTR_IO_IMG(");
                strSqlImg.Append("fimg_id,fio_id,fimg_type,fimg,forder_by,fremark");
                strSqlImg.Append(")");
                strSqlImg.Append(" values (");
                strSqlImg.Append("'" + this.DbGuid() + "',");
                strSqlImg.Append("'" + drRowImg["fio_id"] + "',");
                strSqlImg.Append("'" + drRowImg["fimg_type"] + "',");
                strSqlImg.Append("" + drRowImg["fimg"] + ",");
                strSqlImg.Append("'" + drRowImg["forder_by"] + "',");
                strSqlImg.Append("'" + drRowImg["fremark"] + "'");
                strSqlImg.Append(")");
                sqlList.Add(strSqlImg.ToString());
            }

            for (int ii = 0; ii < dtResult.Rows.Count; ii++)
            {
                DataRow drResult = dtResult.Rows[ii];
                StringBuilder strSqlResult = new StringBuilder();
                strSqlResult.Append("insert into SAM_INSTR_IO_RESULT(");
                strSqlResult.Append("fresult_id,fio_id,fitem_code,fvalue,fod_value,fcutoff_value,fstate,ftime,fremark");
                strSqlResult.Append(")");
                strSqlResult.Append(" values (");
                strSqlResult.Append("'" + this.DbGuid() + "',");
                strSqlResult.Append("'" + drResult["fio_id"] + "',");
                strSqlResult.Append("'" + drResult["fitem_code"] + "',");
                strSqlResult.Append("'" + drResult["fvalue"] + "',");
                strSqlResult.Append("'" + drResult["fod_value"] + "',");
                strSqlResult.Append("'" + drResult["fcutoff_value"] + "',");
                strSqlResult.Append("'" + drResult["fstate"] + "',");
                strSqlResult.Append("'" + drResult["ftime"] + "',");
                strSqlResult.Append("'" + drResult["fremark"] + "'");
                strSqlResult.Append(")");
                sqlList.Add(strSqlResult.ToString());
            }
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(ww.wwf.wwfbll.WWFInit.strDBConn, sqlList);
        }
        #endregion

        #region  仪器接口 图
        /// <summary>
        /// 取得图表
        /// </summary>
        /// <returns></returns>
        public DataTable BllImgDT(string fio_id)
        {
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, "SELECT * FROM SAM_INSTR_IO_IMG"  + " WHERE (fio_id = '" + fio_id + "')").Tables[0];
        }
        #endregion

        #region  仪器结果
        /// <summary>
        /// 取得结果表
        /// </summary>
        /// <returns></returns>
        public DataTable BllResultDT(string fio_id)
        {
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, "SELECT (SELECT fitem_code FROM SAM_ITEM t1  WHERE (t1.fitem_id = t.fitem_code)) AS fitem_code_name, (SELECT fname FROM SAM_ITEM t1 WHERE (t1.fitem_id = t.fitem_code)) AS fitem_name, * FROM SAM_INSTR_IO_RESULT t WHERE (t.fio_id = '" + fio_id + "')").Tables[0];
        }
        #endregion
        
    }
}
