using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ww.wwf.dao;
using ww.wwf.wwfbll;
using ww.wwf.com;
using System.Collections;
using System.Data.SqlClient;
namespace ww.lis.lisbll.sam
{
    public class jybll : DAOWWF
    {
        public jybll()
        { }

        #region  主表

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public string  AddSql(jymodel model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into SAM_JY(");
            strSql.Append("fjy_id,fjy_date,fjy_instr,fjy_yb_tm,fjy_yb_code,fjy_yb_type,fjy_sf_type,fjy_f,fjy_f_ok,fjy_zt,fjy_md,fjy_lczd,fhz_id,fhz_type_id,fhz_zyh,fhz_name,fhz_sex,fhz_age,fhz_age_unit,fhz_age_date,fhz_bq,fhz_dept,fhz_room,fhz_bed,fhz_volk,fhz_hy,fhz_job,fhz_gms,fhz_add,fhz_tel,fapply_id,fapply_user_id,fapply_dept_id,fapply_time,fsampling_user_id,fsampling_time,fjy_user_id,fchenk_user_id,fcheck_time,freport_time,fprint_time,fprint_zt,fprint_count,fsys_user,fsys_time,fsys_dept,fremark,finstr_result_id,f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,sfzh");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + model.fjy_检验id + "',");
            strSql.Append("'" + model.fjy_检验日期 + "',");
            strSql.Append("'" + model.fjy_仪器ID + "',");
            strSql.Append("'" + model.fjy_yb_tm + "',");
            strSql.Append("'" + model.fjy_yb_code + "',");
            strSql.Append("'" + model.fjy_yb_type + "',");
            strSql.Append("'" + model.fjy_收费类型ID + "',");
            strSql.Append("" + model.fjy_f + ",");
            strSql.Append("'" + model.fjy_f_ok + "',");
            strSql.Append("'" + model.fjy_zt检验状态 + "',");
            strSql.Append("'" + model.fjy_md + "',");
            strSql.Append("'" + model.fjy_lczd + "',");
            strSql.Append("'" + model.fhz_id + "',");
            strSql.Append("'" + model.fhz_患者类型id + "',");
            strSql.Append("'" + model.fhz_住院号 + "',");
            strSql.Append("'" + model.fhz_姓名 + "',");
            strSql.Append("'" + model.fhz_性别 + "',");
            strSql.Append("" + model.fhz_年龄 + ",");
            strSql.Append("'" + model.fhz_年龄单位 + "',");
            strSql.Append("'" + model.fhz_生日 + "',");
            strSql.Append("'" + model.fhz_病区 + "',");
            strSql.Append("'" + model.fhz_dept患者科室 + "',");
            strSql.Append("'" + model.fhz_room + "',");
            strSql.Append("'" + model.fhz_床号 + "',");
            strSql.Append("'" + model.fhz_volk + "',");
            strSql.Append("'" + model.fhz_hy + "',");
            strSql.Append("'" + model.fhz_job + "',");
            strSql.Append("'" + model.fhz_gms + "',");
            strSql.Append("'" + model.fhz_add + "',");
            strSql.Append("'" + model.fhz_tel + "',");
            strSql.Append("'" + model.fapply_申请单ID + "',");
            strSql.Append("'" + model.fapply_user_id + "',");
            strSql.Append("'" + model.fapply_dept_id + "',");
            strSql.Append("'" + model.fapply_申请时间 + "',");
            strSql.Append("'" + model.fsampling_user_id + "',");
            strSql.Append("'" + model.fsampling_采样时间 + "',");
            strSql.Append("'" + model.fjy_user_id + "',");
            strSql.Append("'" + model.fchenk_审核医师ID + "',");
            strSql.Append("'" + model.fcheck_审核时间 + "',");
            strSql.Append("'" + model.freport_报告时间 + "',");
            strSql.Append("'" + model.fprint_time + "',");
            strSql.Append("'" + model.fprint_zt + "',");
            strSql.Append("" + model.fprint_count + ",");
            strSql.Append("'" + model.fsys_用户ID + "',");
            strSql.Append("'" + model.fsys_创建时间 + "',");
            strSql.Append("'" + model.fsys_部门ID + "',");
            strSql.Append("'" + model.fremark_备注 + "',");
            strSql.Append("'" + model.finstr_result_id + "',");
            strSql.Append("'" + model.f1 + "',");
            strSql.Append("'" + model.f2 + "',");
            strSql.Append("'" + model.f3 + "',");
            strSql.Append("'" + model.f4 + "',");
            strSql.Append("'" + model.f5 + "',");
            strSql.Append("'" + model.f6 + "',");
            strSql.Append("'" + model.f7 + "',");
            strSql.Append("'" + model.f8 + "',");
            strSql.Append("'" + model.f9 + "',");
            strSql.Append("'" + model.f10 + "',");
            strSql.Append("'" + model.S身份证号 + "'");
            strSql.Append(")");
            return strSql.ToString();
            //  ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());
            //  ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());
        }
        public string AddSqlPatient(jymodel model)
        {
            StringBuilder strSql = new StringBuilder();
            //判断存在否

            bool boolr = WWFInit.wwfRemotingDao.DbRecordExists(WWFInit.strDBConn, "SELECT  count(fhz_id) FROM  SAM_PATIENT WHERE (fhz_id = '" + model.fhz_id + "')");

            //  WWMessage.MessageShowWarning(boolr.ToString()+"SELECT  fhz_id FROM  SAM_PATIENT WHERE (fhz_id = '" + model.fhz_id + "')");
            if (boolr == true)
            {
                //2015-06-01 21:22:09 于凤海添加；判断患者是否存在，存在时先进行删除，后添加以更新病人信息;默认情况下直接插入
                strSql.Append(" delete from SAM_PATIENT where fhz_id='" + model.fhz_id + "'");
            }
            strSql.Append("insert into SAM_PATIENT(");
            //2015-05-18本句添加sfzh
            strSql.Append("fhz_id,fhz_zyh,fname,fsex,ftype_id,fdept_id,fsjys_id,fbed_num,fdiagnose,fbirthday,ffb_id,fuse_if,fremark,ftime,ftime_registration,sfzh,card_no");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + model.fhz_id + "',");
            strSql.Append("'" + model.fhz_住院号 + "',");
            strSql.Append("'" + model.fhz_姓名 + "',");
            strSql.Append("'" + model.fhz_性别 + "',");
            strSql.Append("'" + model.fhz_患者类型id + "',");
            strSql.Append("'" + model.fhz_dept患者科室 + "',");
            strSql.Append("'" + model.fapply_user_id + "',");
            strSql.Append("'" + model.fhz_床号 + "',");
            strSql.Append("'" + model.fjy_lczd + "',");
            strSql.Append("'" + model.fhz_生日 + "',");
            strSql.Append("'" + model.fjy_收费类型ID + "',");
            strSql.Append("1,");
            strSql.Append("'自动保存',");
            strSql.Append("'" + this.DbSysTime() + "',");
            //2015-05-18本句添加逗号
            strSql.Append("'" + this.DbSysTime() + "',");
            //2015-05-18本句是新加的
            strSql.Append("'" + model.S身份证号 + "',");
            //2019年12月5日 yfh 新加
            strSql.Append("'" + model.f1 + "'");
            strSql.Append(")");

            return strSql.ToString();
        }
       
        /*
         Name	Code	Data Type	Length	Precision	Primary	Foreign Key	Mandatory
患者_id	fhz_id	varchar(32)	32		TRUE	FALSE	TRUE
住院号	fhz_zyh	varchar(32)	32		FALSE	FALSE	FALSE
患者姓名	fname	varchar(32)	32		FALSE	FALSE	FALSE
性别	fsex	varchar(32)	32		FALSE	FALSE	FALSE
所在科室_id	fdept_id	varchar(32)	32		FALSE	FALSE	FALSE
床号	fbed_num	varchar(32)	32		FALSE	FALSE	FALSE
临床诊断	fdiagnose	varchar(200)	200		FALSE	FALSE	FALSE
送检医师	fsjys	varchar(32)	32		FALSE	FALSE	FALSE
费别	ffb	varchar(32)	32		FALSE	FALSE	FALSE
患者类型	ftype	varchar(32)	32		FALSE	FALSE	FALSE
民族	fvolk	varchar(32)	32		FALSE	FALSE	FALSE
婚姻	fhymen	varchar(32)	32		FALSE	FALSE	FALSE
生日	fbirthday	varchar(32)	32		FALSE	FALSE	FALSE
职业	fjob	varchar(32)	32		FALSE	FALSE	FALSE
过敏史	Pfgms	varchar(100)	100		FALSE	FALSE	FALSE
工作单位	fjob_org	varchar(128)	128		FALSE	FALSE	FALSE
住址	fadd	varchar(200)	200		FALSE	FALSE	FALSE
Tel	ftel	varchar(64)	64		FALSE	FALSE	FALSE
病区	fward_num	varchar(32)	32		FALSE	FALSE	FALSE
房间号	froom_num	varchar(32)	32		FALSE	FALSE	FALSE
ABO血型	fblood_abo	varchar(32)	32		FALSE	FALSE	FALSE
RH血型	fblood_rh	varchar(32)	32		FALSE	FALSE	FALSE
启用否	fuse_if	int			FALSE	FALSE	FALSE
助记符	fhelp_code	varchar(32)	32		FALSE	FALSE	FALSE
备注	fremark	varchar(200)	200		FALSE	FALSE	FALSE
时间	ftime	varchar(32)	32		FALSE	FALSE	FALSE
         */
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public string  UpdateSql(jymodel model)
        {
           StringBuilder strSql=new StringBuilder();
			strSql.Append("update SAM_JY set ");
			strSql.Append("fjy_date='"+model.fjy_检验日期+"',");
			strSql.Append("fjy_instr='"+model.fjy_仪器ID+"',");
			strSql.Append("fjy_yb_tm='"+model.fjy_yb_tm+"',");
			strSql.Append("fjy_yb_code='"+model.fjy_yb_code+"',");
			strSql.Append("fjy_yb_type='"+model.fjy_yb_type+"',");
			strSql.Append("fjy_sf_type='"+model.fjy_收费类型ID+"',");
			strSql.Append("fjy_f="+model.fjy_f+",");
			strSql.Append("fjy_f_ok='"+model.fjy_f_ok+"',");
			strSql.Append("fjy_zt='"+model.fjy_zt检验状态+"',");
			strSql.Append("fjy_md='"+model.fjy_md+"',");
			strSql.Append("fjy_lczd='"+model.fjy_lczd+"',");
			strSql.Append("fhz_id='"+model.fhz_id+"',");
			strSql.Append("fhz_type_id='"+model.fhz_患者类型id+"',");
			strSql.Append("fhz_zyh='"+model.fhz_住院号+"',");
			strSql.Append("fhz_name='"+model.fhz_姓名+"',");
			strSql.Append("fhz_sex='"+model.fhz_性别+"',");
			strSql.Append("fhz_age="+model.fhz_年龄+",");
			strSql.Append("fhz_age_unit='"+model.fhz_年龄单位+"',");
			strSql.Append("fhz_age_date='"+model.fhz_生日+"',");
			strSql.Append("fhz_bq='"+model.fhz_病区+"',");
			strSql.Append("fhz_dept='"+model.fhz_dept患者科室+"',");
			strSql.Append("fhz_room='"+model.fhz_room+"',");
			strSql.Append("fhz_bed='"+model.fhz_床号+"',");
			strSql.Append("fhz_volk='"+model.fhz_volk+"',");
			strSql.Append("fhz_hy='"+model.fhz_hy+"',");
			strSql.Append("fhz_job='"+model.fhz_job+"',");
			strSql.Append("fhz_gms='"+model.fhz_gms+"',");
			strSql.Append("fhz_add='"+model.fhz_add+"',");
			strSql.Append("fhz_tel='"+model.fhz_tel+"',");
			strSql.Append("fapply_id='"+model.fapply_申请单ID+"',");
			strSql.Append("fapply_user_id='"+model.fapply_user_id+"',");
			strSql.Append("fapply_dept_id='"+model.fapply_dept_id+"',");
			strSql.Append("fapply_time='"+model.fapply_申请时间+"',");
			strSql.Append("fsampling_user_id='"+model.fsampling_user_id+"',");
			strSql.Append("fsampling_time='"+model.fsampling_采样时间+"',");
			strSql.Append("fjy_user_id='"+model.fjy_user_id+"',");
			strSql.Append("fchenk_user_id='"+model.fchenk_审核医师ID+"',");
			strSql.Append("fcheck_time='"+model.fcheck_审核时间+"',");
			strSql.Append("freport_time='"+model.freport_报告时间+"',");
			strSql.Append("fprint_time='"+model.fprint_time+"',");
			strSql.Append("fprint_zt='"+model.fprint_zt+"',");
			strSql.Append("fprint_count="+model.fprint_count+",");
			strSql.Append("fsys_user='"+model.fsys_用户ID+"',");
			strSql.Append("fsys_time='"+model.fsys_创建时间+"',");
			strSql.Append("fsys_dept='"+model.fsys_部门ID+"',");
			strSql.Append("fremark='"+model.fremark_备注+"',");
            strSql.Append("finstr_result_id='" + model.finstr_result_id + "',");
			strSql.Append("f1='"+model.f1+"',");
			strSql.Append("f2='"+model.f2+"',");
			strSql.Append("f3='"+model.f3+"',");
			strSql.Append("f4='"+model.f4+"',");
			strSql.Append("f5='"+model.f5+"',");
			strSql.Append("f6='"+model.f6+"',");
			strSql.Append("f7='"+model.f7+"',");
			strSql.Append("f8='"+model.f8+"',");
			strSql.Append("f9='"+model.f9+"',");
			strSql.Append("f10='"+model.f10+"',");
            strSql.Append("sfzh='"+model.S身份证号+"'");
            strSql.Append(" where fjy_id='" + model.fjy_检验id + "' and  fjy_zt='未审核'");

            return strSql.ToString();
            // ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public void Delete(string fjy_id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete SAM_JY ");
            strSql.Append(" where fjy_id='" + fjy_id + "' and fjy_zt='未审核' ");           
            int intRZ= WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn,strSql.ToString());
            if (intRZ > 0)
            {
                string strDelS = "DELETE FROM SAM_JY_RESULT WHERE (fjy_id = '" + fjy_id + "')";
                WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, strDelS);
            }
        }
        public int UpdateZT(string fjy_id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update SAM_JY set fjy_zt='未审核',fchenk_user_id='',fcheck_time='',fsys_user='" + LoginBLL.strPersonID + "',fsys_time='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "'");
            strSql.Append(" where fjy_id='" + fjy_id + "'  ");
          return  WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, strSql.ToString());
           
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public jymodel GetModel(string fjy_id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select   ");
            strSql.Append(" fjy_id,fjy_date,fjy_instr,fjy_yb_tm,fjy_yb_code,fjy_yb_type,fjy_sf_type,fjy_f,fjy_f_ok,fjy_zt,fjy_md,fjy_lczd,fhz_id,fhz_type_id,fhz_zyh,fhz_name,fhz_sex,fhz_age,fhz_age_unit,fhz_age_date,fhz_bq,fhz_dept,fhz_room,fhz_bed,fhz_volk,fhz_hy,fhz_job,fhz_gms,fhz_add,fhz_tel,fapply_id,fapply_user_id,fapply_dept_id,fapply_time,fsampling_user_id,fsampling_time,fjy_user_id,fchenk_user_id,fcheck_time,freport_time,fprint_time,fprint_zt,fprint_count,fsys_user,fsys_time,fsys_dept,fremark,f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,sfzh ");
            strSql.Append(" from SAM_JY ");
            strSql.Append(" where fjy_id='" + fjy_id + "' ");
            jymodel model = new jymodel();
           
            DataSet ds = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {

                model.fjy_检验id = ds.Tables[0].Rows[0]["fjy_id"].ToString();
                model.fjy_检验日期 = ds.Tables[0].Rows[0]["fjy_date"].ToString();
                model.fjy_仪器ID = ds.Tables[0].Rows[0]["fjy_instr"].ToString();
                model.fjy_yb_tm = ds.Tables[0].Rows[0]["fjy_yb_tm"].ToString();
                model.fjy_yb_code = ds.Tables[0].Rows[0]["fjy_yb_code"].ToString();
                model.fjy_yb_type = ds.Tables[0].Rows[0]["fjy_yb_type"].ToString();
                model.fjy_收费类型ID = ds.Tables[0].Rows[0]["fjy_sf_type"].ToString();
                if (ds.Tables[0].Rows[0]["fjy_f"].ToString() != "")
                {
                    model.fjy_f = decimal.Parse(ds.Tables[0].Rows[0]["fjy_f"].ToString());
                }
                model.fjy_f_ok = ds.Tables[0].Rows[0]["fjy_f_ok"].ToString();
                model.fjy_zt检验状态 = ds.Tables[0].Rows[0]["fjy_zt"].ToString();
                model.fjy_md = ds.Tables[0].Rows[0]["fjy_md"].ToString();
                model.fjy_lczd = ds.Tables[0].Rows[0]["fjy_lczd"].ToString();
                model.fhz_id = ds.Tables[0].Rows[0]["fhz_id"].ToString();
                model.fhz_患者类型id = ds.Tables[0].Rows[0]["fhz_type_id"].ToString();
                model.fhz_住院号 = ds.Tables[0].Rows[0]["fhz_zyh"].ToString();
                model.fhz_姓名 = ds.Tables[0].Rows[0]["fhz_name"].ToString();
                model.fhz_性别 = ds.Tables[0].Rows[0]["fhz_sex"].ToString();
                if (ds.Tables[0].Rows[0]["fhz_age"].ToString() != "")
                {
                    model.fhz_年龄 = decimal.Parse(ds.Tables[0].Rows[0]["fhz_age"].ToString());
                }
                model.fhz_年龄单位 = ds.Tables[0].Rows[0]["fhz_age_unit"].ToString();
                model.fhz_生日 = ds.Tables[0].Rows[0]["fhz_age_date"].ToString();
                model.fhz_病区 = ds.Tables[0].Rows[0]["fhz_bq"].ToString();
                model.fhz_dept患者科室 = ds.Tables[0].Rows[0]["fhz_dept"].ToString();
                model.fhz_room = ds.Tables[0].Rows[0]["fhz_room"].ToString();
                model.fhz_床号 = ds.Tables[0].Rows[0]["fhz_bed"].ToString();
                model.fhz_volk = ds.Tables[0].Rows[0]["fhz_volk"].ToString();
                model.fhz_hy = ds.Tables[0].Rows[0]["fhz_hy"].ToString();
                model.fhz_job = ds.Tables[0].Rows[0]["fhz_job"].ToString();
                model.fhz_gms = ds.Tables[0].Rows[0]["fhz_gms"].ToString();
                model.fhz_add = ds.Tables[0].Rows[0]["fhz_add"].ToString();
                model.fhz_tel = ds.Tables[0].Rows[0]["fhz_tel"].ToString();
                model.fapply_申请单ID = ds.Tables[0].Rows[0]["fapply_id"].ToString();
                model.fapply_user_id = ds.Tables[0].Rows[0]["fapply_user_id"].ToString();
                model.fapply_dept_id = ds.Tables[0].Rows[0]["fapply_dept_id"].ToString();
                model.fapply_申请时间 = ds.Tables[0].Rows[0]["fapply_time"].ToString();
                model.fsampling_user_id = ds.Tables[0].Rows[0]["fsampling_user_id"].ToString();
                model.fsampling_采样时间 = ds.Tables[0].Rows[0]["fsampling_time"].ToString();
                model.fjy_user_id = ds.Tables[0].Rows[0]["fjy_user_id"].ToString();
                model.fchenk_审核医师ID = ds.Tables[0].Rows[0]["fchenk_user_id"].ToString();
                model.fcheck_审核时间 = ds.Tables[0].Rows[0]["fcheck_time"].ToString();
                model.freport_报告时间 = ds.Tables[0].Rows[0]["freport_time"].ToString();
                model.fprint_time = ds.Tables[0].Rows[0]["fprint_time"].ToString();
                model.fprint_zt = ds.Tables[0].Rows[0]["fprint_zt"].ToString();
                if (ds.Tables[0].Rows[0]["fprint_count"].ToString() != "")
                {
                    model.fprint_count = int.Parse(ds.Tables[0].Rows[0]["fprint_count"].ToString());
                }
                model.fsys_用户ID = ds.Tables[0].Rows[0]["fsys_user"].ToString();
                model.fsys_创建时间 = ds.Tables[0].Rows[0]["fsys_time"].ToString();
                model.fsys_部门ID = ds.Tables[0].Rows[0]["fsys_dept"].ToString();
                model.fremark_备注 = ds.Tables[0].Rows[0]["fremark"].ToString();
                model.f1 = ds.Tables[0].Rows[0]["f1"].ToString();
                model.f2 = ds.Tables[0].Rows[0]["f2"].ToString();
                model.f3 = ds.Tables[0].Rows[0]["f3"].ToString();
                model.f4 = ds.Tables[0].Rows[0]["f4"].ToString();
                model.f5 = ds.Tables[0].Rows[0]["f5"].ToString();
                model.f6 = ds.Tables[0].Rows[0]["f6"].ToString();
                model.f7 = ds.Tables[0].Rows[0]["f7"].ToString();
                model.f8 = ds.Tables[0].Rows[0]["f8"].ToString();
                model.f9 = ds.Tables[0].Rows[0]["f9"].ToString();
                model.f10 = ds.Tables[0].Rows[0]["f10"].ToString();
                model.S身份证号 = ds.Tables[0].Rows[0]["sfzh"].ToString();
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 根据日期、仪器型号、样本号获取检验记录（从SAM_JY表中取）
        /// </summary>
        /// <param name="strDate"></param>
        /// <param name="?"></param>
        /// <returns></returns>
        public jymodel GetModel(string strDate, string instrID, int int样本号)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select fjy_id,fjy_date,fjy_instr,fjy_yb_tm,fjy_yb_code,fjy_yb_type,fjy_sf_type,fjy_f,fjy_f_ok,fjy_zt,fjy_md,fjy_lczd,fhz_id,fhz_type_id,fhz_zyh,fhz_name,fhz_sex,fhz_age,fhz_age_unit,fhz_age_date,fhz_bq,fhz_dept,fhz_room,fhz_bed,fhz_volk,fhz_hy,fhz_job,fhz_gms,fhz_add,fhz_tel,fapply_id,fapply_user_id,fapply_dept_id,fapply_time,fsampling_user_id,fsampling_time,fjy_user_id,fchenk_user_id,fcheck_time,freport_time,fprint_time,fprint_zt,fprint_count,fsys_user,fsys_time,fsys_dept,fremark,f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,sfzh ");
            strSql.Append(" from SAM_JY ");
            strSql.Append(" where fjy_date=@testDate and fjy_instr=@instrID and fjy_yb_code=@testCode");

            SqlParameter[] paramsList = new SqlParameter[3];
            paramsList[0] = new SqlParameter("@testDate", strDate);
            paramsList[1] = new SqlParameter("@instrID", instrID);
            paramsList[2] = new SqlParameter("@testCode", int样本号.ToString());

            DataSet ds = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString(), paramsList);

            if ((ds!=null) && (ds.Tables.Count > 0) && (ds.Tables[0].Rows.Count > 0))
            {
                jymodel model = new jymodel();
                model.fjy_检验id = ds.Tables[0].Rows[0]["fjy_id"].ToString();
                model.fjy_检验日期 = ds.Tables[0].Rows[0]["fjy_date"].ToString();
                model.fjy_仪器ID = ds.Tables[0].Rows[0]["fjy_instr"].ToString();
                model.fjy_yb_tm = ds.Tables[0].Rows[0]["fjy_yb_tm"].ToString();
                model.fjy_yb_code = ds.Tables[0].Rows[0]["fjy_yb_code"].ToString();
                model.fjy_yb_type = ds.Tables[0].Rows[0]["fjy_yb_type"].ToString();
                model.fjy_收费类型ID = ds.Tables[0].Rows[0]["fjy_sf_type"].ToString();
                if (ds.Tables[0].Rows[0]["fjy_f"].ToString() != "")
                {
                    model.fjy_f = decimal.Parse(ds.Tables[0].Rows[0]["fjy_f"].ToString());
                }
                model.fjy_f_ok = ds.Tables[0].Rows[0]["fjy_f_ok"].ToString();
                model.fjy_zt检验状态 = ds.Tables[0].Rows[0]["fjy_zt"].ToString();
                model.fjy_md = ds.Tables[0].Rows[0]["fjy_md"].ToString();
                model.fjy_lczd = ds.Tables[0].Rows[0]["fjy_lczd"].ToString();
                model.fhz_id = ds.Tables[0].Rows[0]["fhz_id"].ToString();
                model.fhz_患者类型id = ds.Tables[0].Rows[0]["fhz_type_id"].ToString();
                model.fhz_住院号 = ds.Tables[0].Rows[0]["fhz_zyh"].ToString();
                model.fhz_姓名 = ds.Tables[0].Rows[0]["fhz_name"].ToString();
                model.fhz_性别 = ds.Tables[0].Rows[0]["fhz_sex"].ToString();
                if (ds.Tables[0].Rows[0]["fhz_age"].ToString() != "")
                {
                    model.fhz_年龄 = decimal.Parse(ds.Tables[0].Rows[0]["fhz_age"].ToString());
                }
                model.fhz_年龄单位 = ds.Tables[0].Rows[0]["fhz_age_unit"].ToString();
                model.fhz_生日 = ds.Tables[0].Rows[0]["fhz_age_date"].ToString();
                model.fhz_病区 = ds.Tables[0].Rows[0]["fhz_bq"].ToString();
                model.fhz_dept患者科室 = ds.Tables[0].Rows[0]["fhz_dept"].ToString();
                model.fhz_room = ds.Tables[0].Rows[0]["fhz_room"].ToString();
                model.fhz_床号 = ds.Tables[0].Rows[0]["fhz_bed"].ToString();
                model.fhz_volk = ds.Tables[0].Rows[0]["fhz_volk"].ToString();
                model.fhz_hy = ds.Tables[0].Rows[0]["fhz_hy"].ToString();
                model.fhz_job = ds.Tables[0].Rows[0]["fhz_job"].ToString();
                model.fhz_gms = ds.Tables[0].Rows[0]["fhz_gms"].ToString();
                model.fhz_add = ds.Tables[0].Rows[0]["fhz_add"].ToString();
                model.fhz_tel = ds.Tables[0].Rows[0]["fhz_tel"].ToString();
                model.fapply_申请单ID = ds.Tables[0].Rows[0]["fapply_id"].ToString();
                model.fapply_user_id = ds.Tables[0].Rows[0]["fapply_user_id"].ToString();
                model.fapply_dept_id = ds.Tables[0].Rows[0]["fapply_dept_id"].ToString();
                model.fapply_申请时间 = ds.Tables[0].Rows[0]["fapply_time"].ToString();
                model.fsampling_user_id = ds.Tables[0].Rows[0]["fsampling_user_id"].ToString();
                model.fsampling_采样时间 = ds.Tables[0].Rows[0]["fsampling_time"].ToString();
                model.fjy_user_id = ds.Tables[0].Rows[0]["fjy_user_id"].ToString();
                model.fchenk_审核医师ID = ds.Tables[0].Rows[0]["fchenk_user_id"].ToString();
                model.fcheck_审核时间 = ds.Tables[0].Rows[0]["fcheck_time"].ToString();
                model.freport_报告时间 = ds.Tables[0].Rows[0]["freport_time"].ToString();
                model.fprint_time = ds.Tables[0].Rows[0]["fprint_time"].ToString();
                model.fprint_zt = ds.Tables[0].Rows[0]["fprint_zt"].ToString();
                if (ds.Tables[0].Rows[0]["fprint_count"].ToString() != "")
                {
                    model.fprint_count = int.Parse(ds.Tables[0].Rows[0]["fprint_count"].ToString());
                }
                model.fsys_用户ID = ds.Tables[0].Rows[0]["fsys_user"].ToString();
                model.fsys_创建时间 = ds.Tables[0].Rows[0]["fsys_time"].ToString();
                model.fsys_部门ID = ds.Tables[0].Rows[0]["fsys_dept"].ToString();
                model.fremark_备注 = ds.Tables[0].Rows[0]["fremark"].ToString();
                model.f1 = ds.Tables[0].Rows[0]["f1"].ToString();
                model.f2 = ds.Tables[0].Rows[0]["f2"].ToString();
                model.f3 = ds.Tables[0].Rows[0]["f3"].ToString();
                model.f4 = ds.Tables[0].Rows[0]["f4"].ToString();
                model.f5 = ds.Tables[0].Rows[0]["f5"].ToString();
                model.f6 = ds.Tables[0].Rows[0]["f6"].ToString();
                model.f7 = ds.Tables[0].Rows[0]["f7"].ToString();
                model.f8 = ds.Tables[0].Rows[0]["f8"].ToString();
                model.f9 = ds.Tables[0].Rows[0]["f9"].ToString();
                model.f10 = ds.Tables[0].Rows[0]["f10"].ToString();
                model.S身份证号 = ds.Tables[0].Rows[0]["sfzh"].ToString();
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataTable  GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            //2015-06-03追加疾病名称的获取
            //20160517 changed by wjz 添加审核者的名称 ▽
            //strSql.Append("select (SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '病人类别' and z.fcode=t.fhz_type_id)) as 类型,(SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '性别' and z.fcode=t.fhz_sex)) as 性别,(SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '年龄单位' and z.fcode=t.fhz_age_unit)) as 年龄单位,(SELECT top 1 z.fname FROM WWF_DEPT z WHERE (z.fdept_id=t.fhz_dept)) as 科室,(SELECT top 1 z.fname FROM SAM_SAMPLE_TYPE z WHERE (z.fsample_type_id=t.fjy_yb_type)) as 样本,(SELECT top 1 z.fname FROM WWF_PERSON z WHERE (z.fperson_id=t.fjy_user_id)) as 检验医师,(SELECT top 1 z.fname FROM WWF_PERSON z WHERE (z.fperson_id=t.fapply_user_id)) as 申请医师,(select top 1 z.fname from SAM_DISEASE z where z.fcode = t.fjy_lczd) as 疾病名称,t.* ");
            strSql.Append("select (SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '病人类别' and z.fcode=t.fhz_type_id)) as 类型,(SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '性别' and z.fcode=t.fhz_sex)) as 性别,(SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '年龄单位' and z.fcode=t.fhz_age_unit)) as 年龄单位,(SELECT top 1 z.fname FROM WWF_DEPT z WHERE (z.fdept_id=t.fhz_dept)) as 科室,(SELECT top 1 z.fname FROM SAM_SAMPLE_TYPE z WHERE (z.fsample_type_id=t.fjy_yb_type)) as 样本,(SELECT top 1 z.fname FROM WWF_PERSON z WHERE (z.fperson_id=t.fjy_user_id)) as 检验医师,(SELECT top 1 z.fname FROM WWF_PERSON z WHERE (z.fperson_id=t.fapply_user_id)) as 申请医师,(select top 1 z.fname from SAM_DISEASE z where z.fcode = t.fjy_lczd) as 疾病名称,(select top 1 z.fname from WWF_PERSON Z WHERE (Z.fperson_id=t.fchenk_user_id)) 审核者,t.* ");
            //20160517 changed by wjz 添加审核者的名称 △
            strSql.Append(" FROM SAM_JY t");
           
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }

           // ww.wwf.wwfbll.WWFInit.WWFSaveLog("", strSql.ToString());
            return  ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());
        }

        //add by wjz 20160127 报告录入界面右侧，点击“刷新”时，显示当前设备、当前日期所有的样本检测结果 ▽
        public DataTable GetListALL(string strInsrt, DateTime date, string strSubWhere)
        {
            StringBuilder strSql = new StringBuilder();
            string dateOne = date.ToString("yyyy-MM-dd");
            string dateTwo = date.AddDays(1).ToString("yyyy-MM-dd");

            strSql.Append(@"select * from 
(
select (SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '病人类别' and z.fcode=t.fhz_type_id)) as 类型,(SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '性别' and z.fcode=t.fhz_sex)) as 性别,(SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '年龄单位' and z.fcode=t.fhz_age_unit)) as 年龄单位,(SELECT top 1 z.fname FROM WWF_DEPT z WHERE (z.fdept_id=t.fhz_dept)) as 科室,(SELECT top 1 z.fname FROM SAM_SAMPLE_TYPE z WHERE (z.fsample_type_id=t.fjy_yb_type)) as 样本,(SELECT top 1 z.fname FROM WWF_PERSON z WHERE (z.fperson_id=t.fjy_user_id)) as 检验医师,(SELECT top 1 z.fname FROM WWF_PERSON z WHERE (z.fperson_id=t.fapply_user_id)) as 申请医师,(select top 1 z.fname from SAM_DISEASE z where z.fcode = t.fjy_lczd) as 疾病名称,t.*
 FROM SAM_JY t
 where fjy_date = '" + dateOne + "' and fjy_instr = '"+strInsrt+@"'
 union all 
 select '' 类型,'' as 性别,'' 年龄单位,'' 科室,'' 样本,'' 检验医师,'' 申请医师,'' 疾病名称
	  ,'' [fjy_id]
      ,CONVERT(varchar(20),[FDateTime],23) [fjy_date]
      ,[FInstrID] [fjy_instr]
      ,[FResultID] [fjy_yb_code]
      ,'' [fjy_yb_tm]
      ,'' [fjy_yb_type]
      ,'' [fjy_sf_type]
      ,'' [fjy_f]
      ,'' [fjy_f_ok]
      ,'未审核' [fjy_zt]
      ,'' [fjy_md]
      ,'' [fjy_lczd]
      ,'' [fhz_id]
      ,'' [fhz_type_id]
      ,'' [fhz_zyh]
      ,'' [fhz_name]
      ,'' [fhz_sex]
      ,'' [fhz_age]
      ,'' [fhz_age_unit]
      ,'' [fhz_age_date]
      ,'' [fhz_bq]
      ,'' [fhz_dept]
      ,'' [fhz_room]
      ,'' [fhz_bed]
      ,'' [fhz_volk]
      ,'' [fhz_hy]
      ,'' [fhz_job]
      ,'' [fhz_gms]
      ,'' [fhz_add]
      ,'' [fhz_tel]
      ,'' [fapply_id]
      ,'' [fapply_user_id]
      ,'' [fapply_dept_id]
      ,'' [fapply_time]
      ,'' [fsampling_user_id]
      ,'' [fsampling_time]
      ,'' [fjy_user_id]
      ,'' [fchenk_user_id]
      ,'' [fcheck_time]
      ,'' [freport_time]
      ,'' [fprint_time]
      ,'未打印' [fprint_zt]
      ,0 [fprint_count]
      ,'' [fsys_user]
      ,'' [fsys_time]
      ,'' [fsys_dept]
      ,'' [fremark]
      ,ftaskid [finstr_result_id]
      ,'' [f1]
      ,'' [f2]
      ,'' [f3]
      ,'' [f4]
      ,'' [f5]
      ,'' [f6]
      ,'' [f7]
      ,'' [f8]
      ,'' [f9]
      ,'' [f10]
      ,'' [sfzh]
 FROM Lis_Ins_Result b
  where [FDateTime] >= '"+dateOne+"' and [FDateTime] < '"+dateTwo+"'  and [FInstrID] = '"+strInsrt+@"'
  and FTaskID not in ( select finstr_result_id FROM SAM_JY where fjy_date = '" + dateOne + "' and fjy_instr = '" + strInsrt + @"')

) aaaa
where 1=1 --and (ISNUMERIC(fjy_yb_code)=1 AND ROUND(fjy_yb_code,0)=fjy_yb_code) 
");
            
            //条件不为空时
            if (!string.IsNullOrWhiteSpace(strSubWhere))
            {
                strSql.Append(strSubWhere);
            }

            strSql.Append(" Order by convert(int,fjy_yb_code)");

            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }
        //add by wjz 20160127 报告录入界面右侧，点击“刷新”时，显示当前设备、当前日期所有的样本检测结果 △


        public DataTable GetList比较(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT     TOP (4) fjy_id, fjy_date, fjy_instr, fjy_yb_code  ");
            strSql.Append(" FROM SAM_JY  ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }
        public DataTable GetList比较值(string strfjy_id)
        {

            string strSql = "SELECT '' as 上一次,'' as 上二次,'' as 上三次,fitem_id AS 项目id, fitem_name AS 项目, fvalue AS 项目值 FROM    SAM_JY_RESULT WHERE     (fjy_id = '" + strfjy_id + "')  ORDER BY fjy_id, forder_by";
            
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql);
        }
        #endregion  成员方法


        #region 子表
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public string  AddS(jyresultmodel model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into SAM_JY_RESULT(");
            strSql.Append("fresult_id,fjy_id,fapply_id,fitem_id,fitem_code,fitem_name,fitem_unit,fitem_ref,fitem_badge,forder_by,fvalue,fod,fcutoff,fremark");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + model.fresult_id + "',");
            strSql.Append("'" + model.fjy_id + "',");
            strSql.Append("'" + model.fapply_id + "',");
            strSql.Append("'" + model.fitem_id + "',");
            strSql.Append("'" + model.fitem_code + "',");
            strSql.Append("'" + model.fitem_name + "',");
            strSql.Append("'" + model.fitem_unit + "',");
            strSql.Append("'" + model.fitem_ref + "',");
            strSql.Append("'" + model.fitem_badge + "',");
            strSql.Append("'" + model.forder_by + "',");
            strSql.Append("'" + model.fvalue + "',");
            strSql.Append("'" + model.fod + "',");
            strSql.Append("'" + model.fcutoff + "',");
            strSql.Append("'" + model.fremark + "'");
            strSql.Append(")");
            return strSql.ToString();
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public string UpdateS(jyresultmodel model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update SAM_JY_RESULT set ");
            strSql.Append("fjy_id='" + model.fjy_id + "',");
            strSql.Append("fapply_id='" + model.fapply_id + "',");
            strSql.Append("fitem_id='" + model.fitem_id + "',");
            strSql.Append("fitem_code='" + model.fitem_code + "',");
            strSql.Append("fitem_name='" + model.fitem_name + "',");
            strSql.Append("fitem_unit='" + model.fitem_unit + "',");
            strSql.Append("fitem_ref='" + model.fitem_ref + "',");
            strSql.Append("fitem_badge='" + model.fitem_badge + "',");
            strSql.Append("forder_by='" + model.forder_by + "',");
            strSql.Append("fvalue='" + model.fvalue + "',");
            strSql.Append("fod='" + model.fod + "',");
            strSql.Append("fcutoff='" + model.fcutoff + "',");
            strSql.Append("fremark='" + model.fremark + "'");
            strSql.Append(" where fresult_id='" + model.fresult_id + "' ");
            return strSql.ToString();
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public void DeleteS(string fresult_id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete SAM_JY_RESULT ");
            strSql.Append(" where fresult_id='" + fresult_id + "' ");
            WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, strSql.ToString());
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public string  DeleteByJyid(string strfjy_id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete SAM_JY_RESULT ");
            strSql.Append(" where fjy_id='" + strfjy_id + "' ");
            return strSql.ToString();
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataTable GetListS(string strWhere)
        {
            //SELECT t.* FROM SAM_JY_RESULT t where fjy_id='95cfb1a37f5d4084a30488ed71c9cd7b' order by (SELECT top 1 z.fprint_num FROM   SAM_ITEM z WHERE (z.fitem_id = t.fitem_id))
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select t.* ");
            strSql.Append(" FROM SAM_JY_RESULT t");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by (SELECT top 1 convert(int,z.fprint_num)  FROM   SAM_ITEM z WHERE (z.fitem_id = t.fitem_id))");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }

        //add wjz 20150701
        //当前以住院号为查询条件，后期，视图发生变更后，需要以申请单号为查询条件
        public DataTable Get检验结果View(string str申请单号, string str检验日期, string str设备ID, string str样本号)
        {
            string strSql = @"SELECT 申请单号,检验日期,检验设备,样本号,病人ID,住院号,病人姓名,年龄,身份证,检验ID,检验编码,检验名称,单位,参考值,升降,检验值,排序  ";
            strSql += "  FROM VW_检验结果   WHERE 申请单号 = '" + str申请单号 + "' and 检验日期='" + str检验日期 + "' and 检验设备 ='" + str设备ID + "' and 样本号='" + str样本号 + "'";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql);
        }

        // add by wjz 20160125 根据门诊住院号查询检验信息 ▽
        public DataTable Get检验结果ViewBy门诊住院号(string str门诊住院号, string str检验日期, string str设备ID, string str样本号)
        {
            string strSql = @"SELECT 申请单号,检验日期,检验设备,样本号,病人ID,住院号,病人姓名,年龄,身份证,检验ID,检验编码,检验名称,单位,参考值,升降,检验值,排序  ";
            strSql += "  FROM VW_检验结果   WHERE 住院号 = '" + str门诊住院号 + "' and 检验日期='" + str检验日期 + "' and 检验设备 ='" + str设备ID + "' and 样本号='" + str样本号 + "'";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql);
        }
        // add by wjz 20160125 根据门诊住院号查询检验信息 △

        //add wjz 20150702
        //判断HIS中是否存在符合查询条件的记录
        public bool IsExistInHis(string str申请单号, string str检验日期, string str设备ID, string str样本号)
        {
            string strSql = @"SELECT count(*) FROM JY检验结果 WHERE 申请单号 = '" + str申请单号 + "' and 检验日期='" + str检验日期 + "' and 检验设备 ='" + str设备ID + "' and 样本号='" + str样本号 + "'";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbRecordExists(ww.wwf.wwfbll.WWFInit.strCHisDBConn, strSql);
        }
        

        #endregion

        #region 计算 参考值 和 标识
        public string Bll参考值(string strItem_id, int intfage, string strfsex, string strfsample_type_id)
        {
            string fref = "";
            DataTable dtItem = WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, "SELECT * FROM SAM_ITEM WHERE (fitem_id = '" + strItem_id + "')");
            if (dtItem.Rows.Count > 0)
            {
                DataRow drItem = dtItem.Rows[0];
                string fitem_id = "";//项目ID
                string fitem_fname = "";//项目名称 
                string fref_if_age = "0";//年龄有关
                string fref_if_sex = "0";//性别有关
                string fref_if_sample = "0";//样本有关
                string fref_if_method = "0";//方法有关

                fref = drItem["fref"].ToString();
                fitem_id = drItem["fitem_id"].ToString();
                fitem_fname = drItem["fname"].ToString();
                fref_if_age = drItem["fref_if_age"].ToString();
                fref_if_sex = drItem["fref_if_sex"].ToString();
                fref_if_sample = drItem["fref_if_sample"].ToString();
                fref_if_method = drItem["fref_if_method"].ToString();

                fref = drItem["fref"].ToString();//如果参考值没找到对照的明细那么取项目上设置默认那个
                if (fref_if_age == "1" || fref_if_sex == "0" || fref_if_sample == "0" || fref_if_method == "0")
                {

                    DataTable dtSAM_ITEM_REF = null;//项目参考值明细
                    dtSAM_ITEM_REF = WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, "SELECT * FROM SAM_ITEM_REF WHERE (fitem_id = '" + strItem_id + "')");//取得参考明细表   

                    //string intfage = "0";//取得当前年龄
                    //string strfsex = "0";//取得当前性别
                    //string strfsample_type_id = "";//取得当前样本ID        
                    string strQuerySql = "";//查询参考值明细SQL构造
                    //年龄-------
                    if (fref_if_age == "1" & fref_if_sex == "0" & fref_if_sample == "0")//年龄
                    {
                        strQuerySql = "(" + intfage + ">=fage_low) AND (" + intfage + "<fage_high)";
                    }
                    if (fref_if_age == "1" & fref_if_sex == "1" & fref_if_sample == "0")//年龄+性别
                    {
                        strQuerySql = "('" + strfsex + "'=fsex) AND (" + intfage + ">=fage_low) AND (" + intfage + "<fage_high)";
                    }
                    if (fref_if_age == "1" & fref_if_sex == "1" & fref_if_sample == "1")//年龄+性别+样本
                    {
                        strQuerySql = "('" + strfsample_type_id + "'=fsample_type_id) AND ('" + strfsex + "'=fsex) AND (" + intfage + ">=fage_low) AND (" + intfage + "<fage_high)";
                    }
                    //性别-------
                    if (fref_if_age == "0" & fref_if_sex == "1" & fref_if_sample == "0")//性别
                    {
                        strQuerySql = "('" + strfsex + "'=fsex)";
                    }
                    if (fref_if_age == "0" & fref_if_sex == "1" & fref_if_sample == "1")//性别+样本
                    {
                        strQuerySql = "('" + strfsample_type_id + "'=fsample_type_id) AND ('" + strfsex + "'=fsex)";
                    }
                    //样本-------
                    if (fref_if_age == "0" & fref_if_sex == "0" & fref_if_sample == "1")//样本
                    {
                        strQuerySql = "('" + strfsample_type_id + "'=fsample_type_id)";
                    }
                    if (fref_if_age == "0" & fref_if_sex == "1" & fref_if_sample == "1")//样本+性别
                    {
                        strQuerySql = "('" + strfsample_type_id + "'=fsample_type_id) AND ('" + strfsex + "'=fsex)";
                    }
                    //------------------
                    DataRow[] frfList = dtSAM_ITEM_REF.Select(strQuerySql);
                    if (frfList.Length > 0)
                    {
                        foreach (DataRow drref in frfList)
                        {
                            fref = drref["fref"].ToString();

                        }
                    }

                }
            }
            return fref;
        }

        //add by wjz 20151202 根据性别、年龄判断参考值 ▽
        public string Bll参考值New(string strItem_id, float intfage, string strfsex, string strfsample_type_id)
        {
            string fref = "";

            //changed by wjz 20160226 优化数据查询 ▽
            //DataTable dtItem = WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, "SELECT * FROM SAM_ITEM WHERE (fitem_id = '" + strItem_id + "')");
            DataTable dtItem = WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, 
                "SELECT fitem_id,fname,fref,fref_if_age,fref_if_sex,fref_if_sample,fref_if_method FROM SAM_ITEM WHERE (fitem_id = '" + strItem_id + "')");
            //changed by wjz 20160226 优化数据查询 △

            if (dtItem.Rows.Count > 0)
            {
                DataRow drItem = dtItem.Rows[0];
                string fitem_id = "";//项目ID
                string fitem_fname = "";//项目名称 
                string fref_if_age = "0";//年龄有关
                string fref_if_sex = "0";//性别有关
                string fref_if_sample = "0";//样本有关
                string fref_if_method = "0";//方法有关

                fref = drItem["fref"].ToString();
                fitem_id = drItem["fitem_id"].ToString();
                fitem_fname = drItem["fname"].ToString();
                fref_if_age = drItem["fref_if_age"].ToString();
                fref_if_sex = drItem["fref_if_sex"].ToString();
                fref_if_sample = drItem["fref_if_sample"].ToString();
                fref_if_method = drItem["fref_if_method"].ToString();

                fref = drItem["fref"].ToString();//如果参考值没找到对照的明细那么取项目上设置默认那个

                //changed by wjz 20160227 本代码有逻辑漏洞，应该全是 “=1”的判断。之所以没有暴露出bug，是因为（fref_if_method == "0"）始终成立 ▽
                //由于（fref_if_method == "0"）始终成立，所以if里的内容始终会被执行，所以效率也会受影响。
                //if (fref_if_age == "1" || fref_if_sex == "0" || fref_if_sample == "0" || fref_if_method == "0")
                if (fref_if_age == "1" || fref_if_sex == "1" || fref_if_sample == "1" || fref_if_method == "1")
                //changed by wjz 20160227 本代码有逻辑漏洞，应该全是 “=1”的判断。之所以没有暴露出bug，是因为（fref_if_method == "0"）始终成立 △
                {

                    DataTable dtSAM_ITEM_REF = null;//项目参考值明细
                    dtSAM_ITEM_REF = WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, "SELECT * FROM SAM_ITEM_REF WHERE (fitem_id = '" + strItem_id + "')");//取得参考明细表   

                    //string intfage = "0";//取得当前年龄
                    //string strfsex = "0";//取得当前性别
                    //string strfsample_type_id = "";//取得当前样本ID        
                    string strQuerySql = "1=1 ";//查询参考值明细SQL构造
                    //年龄-------

                    #region 旧的实现方式，已被注释
                    //if (fref_if_age == "1" & fref_if_sex == "0" & fref_if_sample == "0")//年龄
                    //{
                    //    strQuerySql = "(" + intfage + ">=fage_low) AND (" + intfage + "<fage_high)";
                    //}
                    //else if (fref_if_age == "1" & fref_if_sex == "1" & fref_if_sample == "0")//年龄+性别
                    //{
                    //    strQuerySql = "('" + strfsex + "'=fsex) AND (" + intfage + ">=fage_low) AND (" + intfage + "<fage_high)";
                    //}
                    //else if (fref_if_age == "1" & fref_if_sex == "1" & fref_if_sample == "1")//年龄+性别+样本
                    //{
                    //    strQuerySql = "('" + strfsample_type_id + "'=fsample_type_id) AND ('" + strfsex + "'=fsex) AND (" + intfage + ">=fage_low) AND (" + intfage + "<fage_high)";
                    //}
                    ////性别-------
                    //else if (fref_if_age == "0" & fref_if_sex == "1" & fref_if_sample == "0")//性别
                    //{
                    //    strQuerySql = "('" + strfsex + "'=fsex)";
                    //}
                    //else if (fref_if_age == "0" & fref_if_sex == "1" & fref_if_sample == "1")//性别+样本
                    //{
                    //    strQuerySql = "('" + strfsample_type_id + "'=fsample_type_id) AND ('" + strfsex + "'=fsex)";
                    //}
                    ////样本-------
                    //else if (fref_if_age == "0" & fref_if_sex == "0" & fref_if_sample == "1")//样本
                    //{
                    //    strQuerySql = "('" + strfsample_type_id + "'=fsample_type_id)";
                    //}
                    //else if (fref_if_age == "0" & fref_if_sex == "1" & fref_if_sample == "1")//样本+性别
                    //{
                    //    strQuerySql = "('" + strfsample_type_id + "'=fsample_type_id) AND ('" + strfsex + "'=fsex)";
                    //}
                    //else { }
                    #endregion

                    //年龄
                    if(fref_if_age=="1")
                    {
                        strQuerySql += " AND (" + intfage + ">=fage_low) AND (" + intfage + "<fage_high) ";
                    }
                    else
                    {}

                    //性别
                    if(fref_if_sex=="1" && !string.IsNullOrWhiteSpace(strfsex))
                    {
                        strQuerySql += " AND ('" + strfsex + "' like '%'+fsex+'%') ";
                    }
                    else { }

                    //样本类型
                    if(fref_if_sample=="1")
                    {
                        strQuerySql += " AND ('" + strfsample_type_id + "'=fsample_type_id)";
                    }
                    else
                    { }

                    //------------------
                    if (strQuerySql.Trim().Length > 0 && strQuerySql!="1=1 ")
                    {
                        DataRow[] frfList = dtSAM_ITEM_REF.Select(strQuerySql);
                        if (frfList.Length > 0)
                        {
                            fref = frfList[0]["fref"].ToString();
                            //foreach (DataRow drref in frfList)
                            //{
                            //    fref = drref["fref"].ToString();
                            //}
                        }
                    }
                }
            }
            return fref;
        }
        //add by wjz 20151202 根据性别、年龄判断参考值 △


        /// <summary>
        /// 结果标记 编号
        /// </summary>
        /// <param name="fvalue">值</param>
        /// <param name="fref">参考值</param>
        /// <returns>1: 2:↓ 3:↑</returns>
        public string Bll结果标记(string fvalue, string fref)
        {
            string intBJ = "";
            string[] strRefList = System.Text.RegularExpressions.Regex.Split(fref, @"--");
            if (Public.IsNumber(fvalue))
            {
                Double douFvalue = Convert.ToDouble(fvalue);
                if (strRefList.Length == 2)
                {
                    if (Public.IsNumber(strRefList[0].ToString()) & Public.IsNumber(strRefList[1].ToString()))
                    {
                        if (douFvalue < Convert.ToDouble(strRefList[0].ToString()))//太小
                        {
                            intBJ = "↓";
                        }
                        else if (douFvalue > Convert.ToDouble(strRefList[1].ToString()))//太大
                        {
                            intBJ = "↑";
                        }

                    }
                }
            }           
            return intBJ;
        }
//        public string bll参考值(string str项目id, string str性别, string str年龄小, string str年龄大, string str样本id)
//        {
//            string strValue = "";         
//            DataTable dtItem = WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, "SELECT fref,fref_if_age,fref_if_sex,fref_if_sample,FROM SAM_ITEM WHERE (fitem_id = '" + str项目id + "')");
//            if (dtItem.Rows.Count > 0)
//            {
//                strValue = dtItem.Rows[0]["fref"].ToString();
//                string str性别否 = dtItem.Rows[0]["fref_if_sex"].ToString();
//                string str年龄否 = dtItem.Rows[0]["fref_if_age"].ToString();
//                string str样本否 = dtItem.Rows[0]["fref_if_sample"].ToString();
//                if (str性别否.Equals("1") || str年龄否.Equals("1") || str样本否.Equals("1"))
//                {
//                    DataTable dtRef = WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, "SELECT * FROM SAM_ITEM_REF WHERE (fitem_id = '" + str项目id + "')");
//                    if (dtRef.Rows.Count > 0)
//                    {
//                        //暂未做
//                    }
//                }

//            }
//            return strValue;

//            /*
//             Name	Code	Data Type	Primary	Foreign Key	Mandatory
//参考值	fref	varchar(200)	FALSE	FALSE	FALSE
//参考值勤与年龄有关	fref_if_age	int	FALSE	FALSE	FALSE
//参考值勤与性别有关	fref_if_sex	int	FALSE	FALSE	FALSE
//参考值勤与标本有关	fref_if_sample	int	FALSE	FALSE	FALSE
//             */
//        }
        #endregion

        /// <summary>
        /// 图
        /// </summary>
        /// <param name="strfapply_id"></param>
        /// <returns></returns>
        public DataTable BllImgDT(string strfsample_id, string strInsFTaskID)
        {
           // string sql = "SELECT * FROM SAM_JY_IMG where fsample_id='" + strfsample_id + "'";
            string sql = "SELECT  FImgID, FTaskID, FImg, FImgType, FImgNmae, FOrder, FRemark, FImgStroma, FImgModal FROM    Lis_Ins_Result_Img WHERE     (FTaskID = '" + strInsFTaskID + "') ORDER BY FOrder";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }


        /// <summary>
        /// 修改打印时间
        /// </summary>
        /// <param name="fjy_id"></param>
        /// <returns></returns>
        public int BllReportUpdatePrintTime(string fsample_id)
        {
           
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update SAM_JY set ");
            strSql.Append("fprint_time='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "',");
            strSql.Append("fprint_count=fprint_count+1,");
            strSql.Append("fprint_zt='已打印',");
            strSql.Append("fsys_user='" + LoginBLL.strPersonID + "'");
           // strSql.Append("fsys_time='" + strTime + "'");
            strSql.Append(" where fjy_id='" + fsample_id + "'");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }

        public int BllReportUpdate审核状态(string fsample_id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update SAM_JY set ");
            strSql.Append("fcheck_time='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "',");
            //strSql.Append("fprint_count=fprint_count+1,");
            strSql.Append("fjy_zt='已审核',");
            strSql.Append("fchenk_user_id='" + LoginBLL.strPersonID + "'");
            // strSql.Append("fsys_time='" + strTime + "'");
            strSql.Append(" where fjy_id='" + fsample_id + "' and fjy_zt != '已审核' "); //不要修改这块，这么写是有原因的
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        
        }
        
        /// <summary>
        /// 根据设备id， 日期，样本号获取检验结果的主键
        /// </summary>
        /// <param name="str日期">yyyy-MM-dd格式</param>
        /// <param name="str设备id"></param>
        /// <param name="intBegin样本号"></param>
        /// <param name="intEnd样本号"></param>
        /// <param name="strErrorMsg"></param>
        /// <returns></returns>
        public DataTable Get检验ID(string str日期, string str设备id, int intBegin样本号, int intEnd样本号, out string strErrorMsg)
        {
            DataTable dtResult = null;
            strErrorMsg = "";
            try
            {
                List<SqlParameter> paramsList = new List<SqlParameter>();

                // add 20150702 wjz 添加fapply_id字段，目的：向HIS回写检验数据
                string strSql = @"SELECT [fjy_id],fjy_instr,fjy_yb_code,fjy_zt,fprint_zt,fapply_id FROM [SAM_JY] where  [fjy_date] = @testDate and [fjy_instr] = @Instr and fjy_zt='已审核' ";
                paramsList.Add(new SqlParameter("@testDate", str日期));
                paramsList.Add(new SqlParameter("@Instr", str设备id));
                if(intBegin样本号 >= 0)
                {
                    strSql += " and cast([fjy_yb_code] as int) >= @BeginResultNo ";
                    paramsList.Add(new SqlParameter("@BeginResultNo", intBegin样本号));
                }
                if (intEnd样本号 >= 0)
                {
                    strSql += " and cast([fjy_yb_code] as int) <= @EndResultNo ";
                    paramsList.Add(new SqlParameter("@EndResultNo", intEnd样本号));
                }
                strSql +=" order by cast([fjy_yb_code] as int) ";

                //DataSet ds = ww.wwf.dao.SqlHelper.ExecuteDataset(GetLisDBConnString(), CommandType.Text, strSql, paramsList.ToArray());
                DataSet ds = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql, paramsList.ToArray());
                if (ds !=null && ds.Tables.Count > 0)
                {
                    dtResult = ds.Tables[0];
                }
                else
                {
                    strErrorMsg = "没有获取到样本数据！";
                }
            }
            catch(Exception ex)
            {
                dtResult = null;
                strErrorMsg = ex.Message;
            }
            return dtResult;
        }

        //add 20150701 wjz 解除结果时，将与此样本相对应的HIS数据库中的检验结果删除
        //正常情况下，“b是否回写” 应该有本方法直接从配置文件中获取，但是这个方法无法访问Form工程的配置文件，所以通过参数的形式来获取
        public void Del化验结果FromHis(string str申请单号, string str日期, string str设备ID, string str样本号)
        {

            //从数据库中获取标志，是否将化验结果回写至His数据库
            bool b回写 = false;
            try
            {
                string sql = "select top 1 参数值 from 全局参数 where 参数名称='结果回写HIS'";
                DataTable dt回写 = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql);
                if (dt回写 != null && dt回写.Rows.Count > 0 && dt回写.Rows[0]["参数值"].ToString().Equals("true"))
                {
                    b回写 = true;
                }
                else
                {
                    b回写 = false;
                }
            }
            catch
            {
                b回写 = false;
            }

            bool b是否回写 = b回写;

            if (!b是否回写 || string.IsNullOrWhiteSpace(str申请单号))
            {
                return;
            }

            string strDelSql = "DELETE FROM [JY检验结果] WHERE 申请单号 = @applyid and 检验日期 =@testDate and 检验设备 = @InstrID and 样本号 =@SampleID ";
            SqlParameter[] SqlParams = new SqlParameter[4];
            SqlParams[0] = new SqlParameter("@applyid", str申请单号);
            SqlParams[1] = new SqlParameter("@testDate", str日期);
            SqlParams[2] = new SqlParameter("@InstrID", str设备ID);
            SqlParams[3] = new SqlParameter("@SampleID", str样本号);

            ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strCHisDBConn, strDelSql, SqlParams);
        }

        //向His中回写检验数据 add 20160702 wjz
        //正常情况下，“b是否回写” 应该有本方法直接从配置文件中获取，但是这个方法无法访问Form工程的配置文件，所以通过参数的形式来获取
        public void WriteDataToHis(string str申请单号, string strDate, string str设备ID, string str样本号)
        {
            //从数据库中获取标志，是否将化验结果回写至His数据库
            bool b回写 = false;
            try
            {
                string sql = "select top 1 参数值 from 全局参数 where 参数名称='结果回写HIS'";
                DataTable dt回写 = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql);
                if(dt回写 !=null && dt回写.Rows.Count > 0 && dt回写.Rows[0]["参数值"].ToString().Equals("true"))
                {
                    b回写 = true;
                }
                else
                {
                    b回写 = false;
                }
            }
            catch
            {
                b回写 = false;
            }

            bool b是否回写 = b回写;

            if(!b是否回写)
            {
                return;
            }
            if (string.IsNullOrWhiteSpace(str申请单号))  //如果样本号是空的，则不进行任何处理
            {
                return;
            }

            //判断His数据库中是否已经有此条记录的信息，如果有则不进行回写操作
            bool ret = IsExistInHis(str申请单号, strDate, str设备ID, str样本号);
            if (ret)
            {
                return;
            }

            DataTable dt审核 = Get检验结果View(str申请单号, strDate, str设备ID, str样本号);
            //检验信息
            if (dt审核 != null && dt审核.Rows.Count > 0)
            {
                IList listForHisSql = new ArrayList();
                foreach (DataRow dr审核 in dt审核.Rows)
                {
                    StringBuilder str审核Sql = new StringBuilder();
                    str审核Sql.Append(@"INSERT INTO [JY检验结果]([申请单号],[检验日期],[检验设备],[样本号],[病人ID],[住院号],[病人姓名],[年龄],[身份证],[检验ID]");
                    str审核Sql.Append(",[检验编码],[检验名称],[单位],[参考值],[升降],[检验值],[排序])  VALUES( ");
                    str审核Sql.Append("'" + dr审核["申请单号"].ToString().Replace("'","’") + "',");
                    str审核Sql.Append("'" + dr审核["检验日期"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["检验设备"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["样本号"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["病人ID"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["住院号"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["病人姓名"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["年龄"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["身份证"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["检验ID"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["检验编码"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["检验名称"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["单位"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["参考值"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["升降"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["检验值"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["排序"].ToString().Replace("'", "’") + "')");
                    listForHisSql.Add(str审核Sql.ToString());
                }
                string strRet回传 = WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strCHisDBConn, listForHisSql);
                if (!(strRet回传.Equals("true")))
                {
                    WWMessage.MessageShowWarning("向His数据库回写数据失败");
                }
            }
        }
		
		//add by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 ▽
        //public void Del化验结果FromHis(string str申请单号, string str日期, string str设备ID, string str样本号)
		public void Del化验结果FromHisBy门诊住院号(string str门诊住院号, string str日期, string str设备ID, string str样本号)
        {

            //从数据库中获取标志，是否将化验结果回写至His数据库
            bool b回写 = false;
            try
            {
                string sql = "select top 1 参数值 from 全局参数 where 参数名称='结果回写HIS'";
                DataTable dt回写 = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql);
                if (dt回写 != null && dt回写.Rows.Count > 0 && dt回写.Rows[0]["参数值"].ToString().Equals("true"))
                {
                    b回写 = true;
                }
                else
                {
                    b回写 = false;
                }
            }
            catch
            {
                b回写 = false;
            }

            bool b是否回写 = b回写;

            //changed by wjz 20160125 不再根据申请单号回传化验结果，根据门诊号住院号回传结果 ▽
            //if (!b是否回写 || string.IsNullOrWhiteSpace(str申请单号))
            //{
            //    return;
            //}
            if (!b是否回写)
            {
                return;
            }
            //changed by wjz 20160125 不再根据申请单号回传化验结果，根据门诊号住院号回传结果 △

            //changed by wjz 20160125 不再根据申请单号回传化验结果，根据门诊号住院号回传结果 ▽
            //string strDelSql = "DELETE FROM [JY检验结果] WHERE 申请单号 = @applyid and 检验日期 =@testDate and 检验设备 = @InstrID and 样本号 =@SampleID ";
            string strDelSql = "DELETE FROM [JY检验结果] WHERE 住院号 = @mzzyh and 检验日期 =@testDate and 检验设备 = @InstrID and 样本号 =@SampleID ";
            //changed by wjz 20160125 不再根据申请单号回传化验结果，根据门诊号住院号回传结果 △
            SqlParameter[] SqlParams = new SqlParameter[4];
            //changed by wjz 20160125 不再根据申请单号回传化验结果，根据门诊号住院号回传结果 ▽
            //SqlParams[0] = new SqlParameter("@applyid", str申请单号);
            SqlParams[0] = new SqlParameter("@mzzyh", str门诊住院号);
            //changed by wjz 20160125 不再根据申请单号回传化验结果，根据门诊号住院号回传结果 △
            SqlParams[1] = new SqlParameter("@testDate", str日期);
            SqlParams[2] = new SqlParameter("@InstrID", str设备ID);
            SqlParams[3] = new SqlParameter("@SampleID", str样本号);

            ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strCHisDBConn, strDelSql, SqlParams);
        }

        //向His中回写检验数据 add 20160702 wjz
        //正常情况下，“b是否回写” 应该有本方法直接从配置文件中获取，但是这个方法无法访问Form工程的配置文件，所以通过参数的形式来获取
        //changed by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果
        //public void WriteDataToHis(string str申请单号, string strDate, string str设备ID, string str样本号)
        public void WriteDataToHisBy门诊住院号(string str门诊住院号, string strDate, string str设备ID, string str样本号)
        {
            //add by wjz 20160125 不再根据申请单号回传化验结果，根据门诊号住院号回传结果 ▽
            if (string.IsNullOrWhiteSpace(str门诊住院号))
            {
                return;
            }
            //add by wjz 20160125 不再根据申请单号回传化验结果，根据门诊号住院号回传结果 △

            //从数据库中获取标志，是否将化验结果回写至His数据库
            bool b回写 = false;
            try
            {
                string sql = "select top 1 参数值 from 全局参数 where 参数名称='结果回写HIS'";
                DataTable dt回写 = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql);
                if(dt回写 !=null && dt回写.Rows.Count > 0 && dt回写.Rows[0]["参数值"].ToString().Equals("true"))
                {
                    b回写 = true;
                }
                else
                {
                    b回写 = false;
                }
            }
            catch
            {
                b回写 = false;
            }

            bool b是否回写 = b回写;

            if(!b是否回写)
            {
                return;
            }

            //changed by wjz 20160125 不再根据申请单号回传化验结果，根据门诊号住院号回传结果 ▽
            //if (string.IsNullOrWhiteSpace(str申请单号))  //如果样本号是空的，则不进行任何处理
            //{
            //    return;
            //}

            //判断His数据库中是否已经有此条记录的信息，如果有则不进行回写操作
            
            //bool ret = IsExistInHis(str申请单号, strDate, str设备ID, str样本号);
            //if (ret)
            //{
            //    return;
            //}
            //changed by wjz 20160125 不再根据申请单号回传化验结果，根据门诊号住院号回传结果 △

            //changed by wjz 20160125 不再根据申请单号回传化验结果，根据门诊号住院号回传结果 ▽
            //DataTable dt审核 = Get检验结果View(str申请单号, strDate, str设备ID, str样本号);
            DataTable dt审核 = Get检验结果ViewBy门诊住院号(str门诊住院号, strDate, str设备ID, str样本号);
            //changed by wjz 20160125 不再根据申请单号回传化验结果，根据门诊号住院号回传结果 △



            //检验信息
            if (dt审核 != null && dt审核.Rows.Count > 0)
            {
                IList listForHisSql = new ArrayList();

                //add by wjz 20160125 回传结果前先删除旧结果 ▽
                string delSql = "DELETE FROM [JY检验结果] WHERE 住院号 = '" + str门诊住院号 + "' and 检验日期 ='" + strDate + "' and 检验设备 = '" + str设备ID + "' and 样本号 ='" + str样本号 + "'";
                listForHisSql.Add(delSql);
                //add by wjz 20160125 回传结果前先删除旧结果 △

                foreach (DataRow dr审核 in dt审核.Rows)
                {
                    StringBuilder str审核Sql = new StringBuilder();
                    str审核Sql.Append(@"INSERT INTO [JY检验结果]([申请单号],[检验日期],[检验设备],[样本号],[病人ID],[住院号],[病人姓名],[年龄],[身份证],[检验ID]");
                    str审核Sql.Append(",[检验编码],[检验名称],[单位],[参考值],[升降],[检验值],[排序])  VALUES( ");
                    str审核Sql.Append("'" + dr审核["申请单号"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["检验日期"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["检验设备"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["样本号"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["病人ID"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["住院号"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["病人姓名"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["年龄"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["身份证"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["检验ID"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["检验编码"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["检验名称"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["单位"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["参考值"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["升降"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["检验值"].ToString().Replace("'", "’") + "',");
                    str审核Sql.Append("'" + dr审核["排序"].ToString().Replace("'", "’") + "')");
                    listForHisSql.Add(str审核Sql.ToString());
                }
                string strRet回传 = WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strCHisDBConn, listForHisSql);
                if (!(strRet回传.Equals("true")))
                {
                    WWMessage.MessageShowWarning("向His数据库回写数据失败");
                }
            }
        }
		//add by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 △

    }
}

/*
       public string BllItemfref(string strItem_id, int intfage, string strfsex, string strfsample_type_id)
        {
            DataRow drItem = BllItemDT(strItem_id).Rows[0];
            string fref = "";
            string fitem_id = "";//项目ID
            string fitem_fname = "";//项目名称 
            string fref_if_age = "0";//年龄有关
            string fref_if_sex = "0";//性别有关
            string fref_if_sample = "0";//样本有关
            string fref_if_method = "0";//方法有关


            fitem_id = drItem["fitem_id"].ToString();
            fitem_fname = drItem["fname"].ToString();
            fref_if_age = drItem["fref_if_age"].ToString();
            fref_if_sex = drItem["fref_if_sex"].ToString();
            fref_if_sample = drItem["fref_if_sample"].ToString();
            fref_if_method = drItem["fref_if_method"].ToString();

            fref = drItem["fref"].ToString();//如果参考值没找到对照的明细那么取项目上设置默认那个
            if (fref_if_age == "0" & fref_if_sex == "0" & fref_if_sample == "0" & fref_if_method == "0")
            {
            }
            else
            {
                DataTable dtSAM_ITEM_REF = null;//项目参考值明细
                dtSAM_ITEM_REF = BllItemRefDT(fitem_id);//取得参考明细表   

                //string intfage = "0";//取得当前年龄
                //string strfsex = "0";//取得当前性别
                //string strfsample_type_id = "";//取得当前样本ID        
                string strQuerySql = "";//查询参考值明细SQL构造
                //年龄-------
                if (fref_if_age == "1" & fref_if_sex == "0" & fref_if_sample == "0")//年龄
                {
                    strQuerySql = "(" + intfage + ">=fage_low) AND (" + intfage + "<fage_high)";
                }
                if (fref_if_age == "1" & fref_if_sex == "1" & fref_if_sample == "0")//年龄+性别
                {
                    strQuerySql = "('" + strfsex + "'=fsex) AND (" + intfage + ">=fage_low) AND (" + intfage + "<fage_high)";
                }
                if (fref_if_age == "1" & fref_if_sex == "1" & fref_if_sample == "1")//年龄+性别+样本
                {
                    strQuerySql = "('" + strfsample_type_id + "'=fsample_type_id) AND ('" + strfsex + "'=fsex) AND (" + intfage + ">=fage_low) AND (" + intfage + "<fage_high)";
                }
                //性别-------
                if (fref_if_age == "0" & fref_if_sex == "1" & fref_if_sample == "0")//性别
                {
                    strQuerySql = "('" + strfsex + "'=fsex)";
                }
                if (fref_if_age == "0" & fref_if_sex == "1" & fref_if_sample == "1")//性别+样本
                {
                    strQuerySql = "('" + strfsample_type_id + "'=fsample_type_id) AND ('" + strfsex + "'=fsex)";
                }
                //样本-------
                if (fref_if_age == "0" & fref_if_sex == "0" & fref_if_sample == "1")//样本
                {
                    strQuerySql = "('" + strfsample_type_id + "'=fsample_type_id)";
                }
                if (fref_if_age == "0" & fref_if_sex == "1" & fref_if_sample == "1")//样本+性别
                {
                    strQuerySql = "('" + strfsample_type_id + "'=fsample_type_id) AND ('" + strfsex + "'=fsex)";
                }
                //------------------
                DataRow[] frfList = dtSAM_ITEM_REF.Select(strQuerySql);
                if (frfList.Length > 0)
                {
                    foreach (DataRow drref in frfList)
                    {
                        fref = drref["fref"].ToString();
                        //MessageBox.Show("参考值为:" + fref + "\n\n" + strQuerySql);
                    }
                }
                //else
                //{
                //fref = fref;//如果参考值没找到对照的明细那么取项目上设置默认那个
                //WWMessage.MessageShowWarning("项目："+fitem_fname+" 的参考值设置有误！\n\n" + strQuerySql);
                //}
                // ↑↓
            }
            return fref;
        }
 */