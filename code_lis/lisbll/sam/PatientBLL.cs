using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ww.wwf.dao;
using ww.wwf.wwfbll;
namespace ww.lis.lisbll.sam
{
    /// <summary>
    /// 
    /// </summary>
    public class PatientBLL : DAOWWF
    {
        public PatientBLL()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //	
        }
        ww.wwf.dao.Log ll = new Log();
        #region  患者
        public DataTable BllPatientDT(string strWhere)
        {
            string sql = "";
            DataTable dt = null;
            if (strWhere == "" || strWhere == null)
            {
                //sql = "SELECT * FROM SAM_PATIENT where fdept_id='" + fdept_id + "'";
            }
            else
            {
                strWhere = " where 1=1 " + strWhere;
                sql = "SELECT case when fsex='1' then '男' else '女' end fsex1,* FROM SAM_PATIENT " + strWhere;
                //ll.WWFSaveLog("",sql);
                
                //sql = "SELECT * FROM SAM_PATIENT where fdept_id='" + fdept_id + "'";
                dt = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
            }
            return dt;
        }

        public DataTable BllPatientDTFromHisDBby住院号(string zyh)
        {
            DataTable dt = null;
            if (string.IsNullOrWhiteSpace(zyh))
            {
                //sql = "SELECT * FROM SAM_PATIENT where fdept_id='" + fdept_id + "'";
            }
            else
            {
                dt = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.GetPatientInfoBy住院号(ww.wwf.wwfbll.WWFInit.strCHisDBConn, zyh).Tables[0];
            }
            return dt;
        }

        /// <summary>
        /// 2015-06-10 08:17:01 于凤海添加，检索档案号查找患者信息
        /// </summary>
        /// <param name="zyh"></param>
        /// <returns></returns>
        public DataTable BllPatientDTFromHisDBby档案号(string dnh)
        {
            DataTable dt = null;
            if (string.IsNullOrWhiteSpace(dnh))
            {
                //sql = "SELECT * FROM SAM_PATIENT where fdept_id='" + fdept_id + "'";
            }
            else
            {
                dt = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.GetPatientInfoBy档案号(ww.wwf.wwfbll.WWFInit.strDBConn, dnh).Tables[0];
            }
            return dt;
        }

        //add by wjz 20160226 公共卫生系统数据库变更，调整获取信息的数据源 ▽
        public DataTable BllPatientDTFromHisDBby档案号New(string dnh)
        {
            DataTable dt = null;
            if (string.IsNullOrWhiteSpace(dnh))
            {
                //sql = "SELECT * FROM SAM_PATIENT where fdept_id='" + fdept_id + "'";
            }
            else
            {
                //dt = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.GetPatientInfoBy档案号(ww.wwf.wwfbll.WWFInit.strDBConn, dnh).Tables[0];
                dt = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.GetPatientInfoBy档案号New(ww.wwf.wwfbll.WWFInit.strDBConn, dnh).Tables[0];
            }
            return dt;
        }
        //add by wjz 20160226 公共卫生系统数据库变更，调整获取信息的数据源 △

        /// <summary>
        /// 患者 增加一条数据
        /// </summary>
        public int BllPatientAdd(DataRow dr)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into SAM_PATIENT(");
            strSql.Append("fhz_id,fhz_zyh,fsex,fname,fvolk,fhymen,fbirthday,fjob,Pfgms,fjob_org,fadd,ftel,fward_num,froom_num,fdept_id,fbed_num,fblood_abo,fblood_rh,fdiagnose,fuse_if,fhelp_code,fremark,ftime_registration,ftime");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + dr["fhz_id"] + "',");
            strSql.Append("'" + dr["fhz_zyh"] + "',");
            strSql.Append("'" + dr["fsex"] + "',");
            strSql.Append("'" + dr["fname"] + "',");
            strSql.Append("'" + dr["fvolk"] + "',");
            strSql.Append("'" + dr["fhymen"] + "',");
            strSql.Append("'" + dr["fbirthday"] + "',");
            strSql.Append("'" + dr["fjob"] + "',");
            strSql.Append("'" + dr["Pfgms"] + "',");
            strSql.Append("'" + dr["fjob_org"] + "',");
            strSql.Append("'" + dr["fadd"] + "',");
            strSql.Append("'" + dr["ftel"] + "',");
            strSql.Append("'" + dr["fward_num"] + "',");
            strSql.Append("'" + dr["froom_num"] + "',");
            strSql.Append("'" + dr["fdept_id"] + "',");
            strSql.Append("'" + dr["fbed_num"] + "',");
            strSql.Append("'" + dr["fblood_abo"] + "',");
            strSql.Append("'" + dr["fblood_rh"] + "',");
            strSql.Append("'" + dr["fdiagnose"] + "',");
            strSql.Append("" + dr["fuse_if"] + ",");
            strSql.Append("'" + dr["fhelp_code"] + "',");
            strSql.Append("'" + dr["fremark"] + "',");
            strSql.Append("'" + dr["ftime_registration"] + "',");
            strSql.Append("'" + dr["ftime_registration"] + "'");
            strSql.Append(")");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }
        /// <summary>
        /// 患者 更新一条数据
        /// </summary>
        public int BllPatientUpdate(DataRow dr)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update SAM_PATIENT set ");
            strSql.Append("fhz_zyh='" + dr["fhz_zyh"] + "',");
            strSql.Append("fsex='" + dr["fsex"] + "',");
            strSql.Append("fname='" + dr["fname"] + "',");
            strSql.Append("fvolk='" + dr["fvolk"] + "',");
            strSql.Append("fhymen='" + dr["fhymen"] + "',");
            strSql.Append("fbirthday='" + dr["fbirthday"] + "',");
            strSql.Append("fjob='" + dr["fjob"] + "',");
            strSql.Append("Pfgms='" + dr["Pfgms"] + "',");
            strSql.Append("fjob_org='" + dr["fjob_org"] + "',");
            strSql.Append("fadd='" + dr["fadd"] + "',");
            strSql.Append("ftel='" + dr["ftel"] + "',");
            strSql.Append("fward_num='" + dr["fward_num"] + "',");
            strSql.Append("froom_num='" + dr["froom_num"] + "',");
            strSql.Append("fdept_id='" + dr["fdept_id"] + "',");
            strSql.Append("fbed_num='" + dr["fbed_num"] + "',");
            strSql.Append("fblood_abo='" + dr["fblood_abo"] + "',");
            strSql.Append("fblood_rh='" + dr["fblood_rh"] + "',");
            strSql.Append("fdiagnose='" + dr["fdiagnose"] + "',");
            strSql.Append("fuse_if=" + dr["fuse_if"] + ",");
            strSql.Append("fhelp_code='" + dr["fhelp_code"] + "',");
            strSql.Append("fremark='" + dr["fremark"] + "',");
            strSql.Append("ftime_registration='" + dr["ftime_registration"] + "',");
            strSql.Append("ftime='" + this.DbServerDateTim() + "'");
            strSql.Append(" where fhz_id='" + dr["fhz_id"] + "'");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());

        }
        /// <summary>
        /// 患者 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int BllPatientDel(string id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete FROM SAM_PATIENT ");
            strSql.Append(" where fhz_id='" + id + "'");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }

        /// <summary>
        /// 取得部门及患者树
        /// </summary>
        /// <param name="strWherePatient"></param>
        /// <returns></returns>
        public DataTable BllDeptAndPatientDT(string strWherePatient)
        {
            DataTable dt = new DataTable();
            DataColumn dataColumn1 = new DataColumn("fid", typeof(string));
            dt.Columns.Add(dataColumn1);

            DataColumn dataColumn2 = new DataColumn("fpid", typeof(string));
            dt.Columns.Add(dataColumn2);

            DataColumn dataColumn3 = new DataColumn("fname", typeof(string));
            dt.Columns.Add(dataColumn3);

            DataColumn dataColumn4 = new DataColumn("tag", typeof(string));
            dt.Columns.Add(dataColumn4);

            DataTable dtCheckType = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, "SELECT fdept_id , '['+fdept_id+']'+fname fname FROM WWF_DEPT WHERE fp_id='0' and (fuse_flag = 1) ORDER BY forder_by").Tables[0];
            string strdeptid = "";
            for (int i = 0; i < dtCheckType.Rows.Count; i++)
            {
                DataRow drNewRow = dt.NewRow();
                strdeptid = dtCheckType.Rows[i]["fdept_id"].ToString();
                drNewRow["fid"] = strdeptid;
                drNewRow["fpid"] = "-1";
                drNewRow["tag"] = "dept";
                drNewRow["fname"] = dtCheckType.Rows[i]["fname"];
                dt.Rows.Add(drNewRow);

                DataTable dtSampleType = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, "SELECT * FROM SAM_PATIENT WHERE (fdept_id = '" + strdeptid + "')" + strWherePatient).Tables[0];

                for (int ii = 0; ii < dtSampleType.Rows.Count; ii++)
                {
                    //WWMessage.MessageShowWarning("SELECT * FROM SAM_PATIENT WHERE (fdept_id = '" + strdeptid + "')" + strWherePatient);
                    DataRow drNewRowSame = dt.NewRow();
                    drNewRowSame["fid"] = dtSampleType.Rows[ii]["fhz_id"].ToString();
                    drNewRowSame["fpid"] = strdeptid;
                    drNewRowSame["tag"] = "patient";
                    if (dtSampleType.Rows[ii]["fbed_num"].ToString() == "")
                        drNewRowSame["fname"] = dtSampleType.Rows[ii]["fname"].ToString() + "," + dtSampleType.Rows[ii]["fhz_id"].ToString() + "";
                    else
                        drNewRowSame["fname"] = dtSampleType.Rows[ii]["fbed_num"].ToString() + "床," + dtSampleType.Rows[ii]["fname"].ToString() + "," + dtSampleType.Rows[ii]["fhz_id"].ToString() + "";
                    dt.Rows.Add(drNewRowSame);
                }
            }
            return dt;
        }

        /// <summary>
        /// 根据生日取得年龄
        /// </summary>
        /// <param name="strfbirthday">生日</param>
        /// <returns></returns>
        public string BllPatientAge(string strfbirthday)
        {
            string strRetAge = "";
            if (strfbirthday == "" || strfbirthday == null)
                strRetAge = "";
            else
            {
                DateTime b = DateTime.Parse(strfbirthday);
                double a = (DateTime.Now - b).TotalDays;
                double aYear = Math.Floor(a / 365);
                double C = a % 365;
                double aMonth = Math.Floor(C / 30);
                double aDay = Math.Floor(C % 30);
                // strRetAge = aYear.ToString() + "岁   零" + aMonth.ToString() + "个月   " + aDay.ToString() + "天";                
                if (aDay > 15)
                {
                    aMonth = aMonth + 1;
                    aDay = 0;
                }
                if (aMonth > 6)
                {
                    aYear = aYear + 1;
                    aMonth = 0;
                }

                if (aYear == 0 & aMonth == 0 & aDay != 0)
                    strRetAge = "天:" + aDay.ToString();//2 天
                if (aYear == 0 & aMonth != 0 & aDay <= 15)
                    strRetAge = "月:" + aMonth.ToString();//1 月
                if (aYear != 0 & aMonth <= 6)
                    strRetAge = "岁:" + aYear.ToString();//0 岁
            }
            return strRetAge;
        }
        /// <summary>
        /// 根据年龄取得生日
        /// </summary>
        /// <param name="dec年龄"></param>
        /// <param name="str年龄单位"></param>
        /// <returns>1岁，2月，3天</returns>
        public string BllBirthdayByAge(decimal dec年龄, string str年龄单位)
        {
            string str生日 = "";
            double douAge = Convert.ToDouble(dec年龄);
            int intXX = 1;
            if (str年龄单位.Equals("1") || str年龄单位.Equals("岁"))
            {
                intXX = 365;
            }
            else if (str年龄单位.Equals("2") || str年龄单位.Equals("月"))
            {
                intXX = 30;
            }
            else if (str年龄单位.Equals("3") || str年龄单位.Equals("天"))
            {
                intXX = 1;
            }
            else
            {
                intXX = 1;
            }
            double intDay = intXX * douAge;           
            str生日 = DateTime.Now.AddDays(-intDay).ToString("yyyy-MM-dd HH:mm:ss");
            return str生日;
        }
        /// <summary>
        /// 取得患者列表 患者ID
        /// </summary>
        /// <param name="fhz_id">患者ID</param>
        /// <returns></returns>
        public DataTable BllPatientDTByID(string fhz_id)
        {
            string sql = "SELECT * FROM SAM_PATIENT WHERE (fhz_id = '" + fhz_id + "')";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        /// <summary>
        /// 取得患者列表 住院号
        /// </summary>
        /// <param name="fhz_zyh">住院号</param>
        /// <returns></returns>
        public DataTable BllPatientDTByZYH(string fhz_zyh)
        {
            string sql = "SELECT * FROM SAM_PATIENT WHERE (fhz_id = '" + fhz_zyh + "')";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }

        /// <summary>
        /// 根据申请单号获取病人信息
        /// </summary>
        /// <param name="applyNo"></param>
        /// <returns></returns>
        public DataTable BllPatientDTByApplyNo(string applyNo)
        {
            string strSql = "select * from SAM_APPLY where fapply_id = '"+applyNo+"'";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql).Tables[0];
        }

        /// <summary>
        /// 根据申请单号获取病人检验组合信息
        /// </summary>
        /// <param name="applyNo"></param>
        /// <returns></returns>
        public DataTable BllPatientDTByApplyList(string applyNo)
        {
            string strSql = "select fapply_id,fitem_group from  SAM_APPLY where his_recordid='" + applyNo + "'";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql).Tables[0];
        }
        #endregion
    }
}
