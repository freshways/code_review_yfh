using System;
using System.Collections.Generic;
using System.Text;

namespace ww.lis.lisbll.sam
{
     
    public class jyresultmodel
    {
        public jyresultmodel()
        { }
        #region Model
        private string _fresult_id;
        private string _fjy_id;
        private string _fapply_id;
        private string _fitem_id;
        private string _fitem_code;
        private string _fitem_name;
        private string _fitem_unit;
        private string _fitem_ref;
        private string _fitem_badge;
        private string _forder_by;
        private string _fvalue;
        private string _fod;
        private string _fcutoff;
        private string _fremark;
        /// <summary>
        /// 
        /// </summary>
        public string fresult_id
        {
            set { _fresult_id = value; }
            get { return _fresult_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fjy_id
        {
            set { _fjy_id = value; }
            get { return _fjy_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fapply_id
        {
            set { _fapply_id = value; }
            get { return _fapply_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fitem_id
        {
            set { _fitem_id = value; }
            get { return _fitem_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fitem_code
        {
            set { _fitem_code = value; }
            get { return _fitem_code; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fitem_name
        {
            set { _fitem_name = value; }
            get { return _fitem_name; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fitem_unit
        {
            set { _fitem_unit = value; }
            get { return _fitem_unit; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fitem_ref
        {
            set { _fitem_ref = value; }
            get { return _fitem_ref; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fitem_badge
        {
            set { _fitem_badge = value; }
            get { return _fitem_badge; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string forder_by
        {
            set { _forder_by = value; }
            get { return _forder_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fvalue
        {
            set { _fvalue = value; }
            get { return _fvalue; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fod
        {
            set { _fod = value; }
            get { return _fod; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fcutoff
        {
            set { _fcutoff = value; }
            get { return _fcutoff; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fremark
        {
            set { _fremark = value; }
            get { return _fremark; }
        }
        #endregion Model

    }
}

