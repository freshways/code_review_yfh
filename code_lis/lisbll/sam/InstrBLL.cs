using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ww.wwf.dao;
using ww.wwf.wwfbll;
using System.IO;
namespace ww.lis.lisbll.sam
{
    /// <summary>
    /// 
    /// </summary>
    public class InstrBLL : DAOWWF
    {
        public InstrBLL()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //	
        }
        #region  仪器类型
        /// <summary>
        /// 仪器类型 列表 1为启用；0为未启用；其它如-1等为所有
        /// </summary>
        /// <returns></returns>
        public DataTable BllTypeDT(int fuse_if)
        {
            string sql = "";
            if (fuse_if == 1 || fuse_if == 0)
            {
                sql = "SELECT * FROM sam_instr_type where fuse_if=" + fuse_if + " ORDER BY forder_by";
            }
            else
            {
                sql = "SELECT * FROM sam_instr_type  ORDER BY forder_by";
            }
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }
        /// <summary>
        /// 上级取下级
        /// </summary>
        /// <param name="fp_id"></param>
        /// <returns></returns>
        public DataTable BllTypeDTByPid(string fp_id)
        {
            string sql = "SELECT * FROM sam_instr_type where fp_id='" + fp_id + "' ORDER BY forder_by";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }

        /// <summary>
        /// 仪器类型 增加一条数据
        /// </summary>
        public int BllTypeAdd(DataRowView dr)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into sam_instr_type(");
            strSql.Append("ftype_id,fp_id,fcode,fname,fname_e,fuse_if,forder_by,fhelp_code,fremark");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + dr["ftype_id"].ToString() + "',");
            strSql.Append("'" + dr["fp_id"].ToString() + "',");
            strSql.Append("'" + dr["fcode"].ToString() + "',");
            strSql.Append("'" + dr["fname"].ToString() + "',");
            strSql.Append("'" + dr["fname_e"].ToString() + "',");
            strSql.Append("" + dr["fuse_if"].ToString() + ",");
            strSql.Append("'" + dr["forder_by"].ToString() + "',");
            strSql.Append("'" + dr["fhelp_code"].ToString() + "',");
            strSql.Append("'" + dr["fremark"].ToString() + "'");
            strSql.Append(")");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());
        }


        /// <summary>
        /// 仪器类型 更新一条数据
        /// </summary>
        public int BllTypeUpdate(DataRowView dr)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update sam_instr_type set ");
            strSql.Append("fp_id='" + dr["fp_id"] + "',");
            strSql.Append("fcode='" + dr["fcode"] + "',");
            strSql.Append("fname='" + dr["fname"]+ "',");
            strSql.Append("fname_e='" + dr["fname_e"] + "',");
            strSql.Append("fuse_if=" + dr["fuse_if"] + ",");
            strSql.Append("forder_by='" + dr["forder_by"] + "',");
            strSql.Append("fhelp_code='" + dr["fhelp_code"] + "',");
            strSql.Append("fremark='" + dr["fremark"] + "'");
            strSql.Append(" where ftype_id='" + dr["ftype_id"] + "'");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());

        }

        /// <summary>
        /// 仪器类型 删除一条数据
        /// </summary>
        public int BllTypeDelete(string id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete FROM sam_instr_type ");
            strSql.Append(" where ftype_id='" + id + "'");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());
        }
        #endregion 

        #region  仪器
        /// <summary>
        /// 仪器 列表 1为启用；0为未启用；其它如-1等为所有
        /// </summary>
        /// <returns></returns>
        public DataTable BllInstrDT(int fuse_if)
        {

            string sql = "";
            if (fuse_if == 1 || fuse_if == 0)
            {
                sql = "SELECT * FROM sam_instr where fuse_if=" + fuse_if + " ORDER BY forder_by";
            }
            else
            {
                sql = "SELECT * FROM sam_instr  ORDER BY forder_by";
            }

            DataTable dtInst = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
            DataRow drnow = dtInst.NewRow();

            dtInst.Rows.Add(drnow);
            return dtInst;
        }
        /// <summary>
        /// 据类别ＩＤ
        /// </summary>
        /// <param name="ftype_id"></param>
        /// <returns></returns>
        public DataTable BllInstrDTByTypeID(string ftype_id)
        {
            string sql = "SELECT * FROM sam_instr where ftype_id='" + ftype_id + "' ORDER BY forder_by";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }
        

        /// <summary>
        /// 仪器 增加一条数据
        /// </summary>
        public int BllInstrAdd(DataRowView dr)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into sam_instr(");
            strSql.Append("finstr_id,ftype_id,fjygroup_id,fjytype_id,fuse_if,fname,forder_by,fremark");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + dr["finstr_id"].ToString() + "',");
            strSql.Append("'" + dr["ftype_id"].ToString() + "',");
            strSql.Append("'" + dr["fjygroup_id"].ToString() + "',");
            strSql.Append("'" + dr["fjytype_id"].ToString() + "',");
            strSql.Append("" + dr["fuse_if"].ToString() + ",");         
            strSql.Append("'" + dr["fname"].ToString() + "',");           
            strSql.Append("'" + dr["forder_by"].ToString() + "',");         
            strSql.Append("'" + dr["fremark"].ToString() + "'");
            strSql.Append(")");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());
            /*
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into sam_instr(");
            strSql.Append("finstr_id,ftype_id,fjygroup_id,fjytype_id,fuse_if,fcode,fname,fname_e,forder_by,fremark,fbody_no,fnew_old,fprice,fimg,funit,fy_value,fj_value,fzjl,fuse_year,fout_date,fdh_date,fty_date,fgys_id,fcj_id,fwz,fgb,fsz_date,fdah,fjfr,fuser,fdate,flx,fzjzz_date,fspec,fhelp_code,fxlzb");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + dr["finstr_id"].ToString() + "',");
            strSql.Append("'" + dr["ftype_id"].ToString() + "',");
            strSql.Append("'" + dr["fjygroup_id"].ToString() + "',");
            strSql.Append("'" + dr["fjytype_id"].ToString() + "',");
            strSql.Append("" + dr["fuse_if"].ToString() + ",");
            strSql.Append("'" + dr["fcode"].ToString() + "',");
            strSql.Append("'" + dr["fname"].ToString() + "',");
            strSql.Append("'" + dr["fname_e"].ToString() + "',");
            strSql.Append("'" + dr["forder_by"].ToString() + "',");
            strSql.Append("'" + dr["fremark"].ToString() + "',");
            strSql.Append("'" + dr["fbody_no"].ToString() + "',");
            strSql.Append("'" + dr["fnew_old"].ToString() + "',");
            strSql.Append("" + dr["fprice"].ToString() + ",");
            strSql.Append("'" + dr["fimg"].ToString() + "',");
            strSql.Append("'" + dr["funit"].ToString() + "',");
            strSql.Append("" + dr["fy_value"].ToString() + ",");
            strSql.Append("" + dr["fj_value"].ToString() + ",");
            strSql.Append("" + dr["fzjl"].ToString() + ",");
            strSql.Append("" + dr["fuse_year"].ToString() + ",");
            strSql.Append("'" + dr["fout_date"].ToString() + "',");
            strSql.Append("'" + dr["fdh_date"].ToString() + "',");
            strSql.Append("'" + dr["fty_date"].ToString() + "',");
            strSql.Append("'" + dr["fgys_id"].ToString() + "',");
            strSql.Append("'" + dr["fcj_id"].ToString() + "',");
            strSql.Append("'" + dr["fwz"].ToString() + "',");
            strSql.Append("'" + dr["fgb"].ToString() + "',");
            strSql.Append("'" + dr["fsz_date"].ToString() + "',");
            strSql.Append("'" + dr["fdah"].ToString() + "',");
            strSql.Append("'" + dr["fjfr"].ToString() + "',");
            strSql.Append("'" + dr["fuser"].ToString() + "',");
            strSql.Append("'" + dr["fdate"].ToString() + "',");
            strSql.Append("" + dr["flx"].ToString() + ",");
            strSql.Append("'" + dr["fzjzz_date"].ToString() + "',");
            strSql.Append("'" + dr["fspec"].ToString() + "',");
            strSql.Append("'" + dr["fhelp_code"].ToString() + "',");
            strSql.Append("'" + dr["fxlzb"].ToString() + "'");
            strSql.Append(")");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());
             * */
        }


        /// <summary>
        /// 仪器 更新一条数据
        /// </summary>
        public int BllInstrUpdate(DataRowView dr)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update sam_instr set ");
            strSql.Append("ftype_id='" + dr["ftype_id"] + "',");
            strSql.Append("fjygroup_id='" + dr["fjygroup_id"] + "',");
            strSql.Append("fjytype_id='" + dr["fjytype_id"] + "',");
            strSql.Append("fuse_if=" + dr["fuse_if"] + ",");          
            strSql.Append("fname='" + dr["fname"] + "',");          
            strSql.Append("forder_by='" + dr["forder_by"] + "',");
            strSql.Append("fremark='" + dr["fremark"] + "'");            
            strSql.Append(" where finstr_id='" + dr["finstr_id"] + "'");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());
            /*
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update sam_instr set ");
            strSql.Append("ftype_id='" + dr["ftype_id"].ToString() + "',");
            strSql.Append("fjygroup_id='" + dr["fjygroup_id"].ToString() + "',");
            strSql.Append("fjytype_id='" + dr["fjytype_id"].ToString() + "',");
            strSql.Append("fuse_if=" + dr["fuse_if"].ToString() + ",");
            strSql.Append("fcode='" + dr["fcode"].ToString() + "',");
            strSql.Append("fname='" + dr["fname"].ToString() + "',");
            strSql.Append("fname_e='" + dr["fname_e"].ToString() + "',");
            strSql.Append("forder_by='" + dr["forder_by"].ToString() + "',");
            strSql.Append("fremark='" + dr["fremark"].ToString() + "',");
            strSql.Append("fbody_no='" + dr["fbody_no"].ToString() + "',");
            strSql.Append("fnew_old='" + dr["fnew_old"].ToString() + "',");
            strSql.Append("fprice=" + dr["fprice"].ToString() + ",");
            strSql.Append("fimg='" + dr["fimg"].ToString() + "',");
            strSql.Append("funit='" + dr["funit"].ToString() + "',");
            strSql.Append("fy_value=" + dr["fy_value"].ToString() + ",");
            strSql.Append("fj_value=" + dr["fj_value"].ToString() + ",");
            strSql.Append("fzjl=" + dr["fzjl"].ToString() + ",");
            strSql.Append("fuse_year=" + dr["fuse_year"].ToString() + ",");
            strSql.Append("fout_date='" + dr["fout_date"].ToString() + "',");
            strSql.Append("fdh_date='" + dr["fdh_date"].ToString() + "',");
            strSql.Append("fty_date='" + dr["fty_date"].ToString() + "',");
            strSql.Append("fgys_id='" + dr["fgys_id"].ToString() + "',");
            strSql.Append("fcj_id='" + dr["fcj_id"].ToString() + "',");
            strSql.Append("fwz='" + dr["fwz"].ToString() + "',");
            strSql.Append("fgb='" + dr["fgb"].ToString() + "',");
            strSql.Append("fsz_date='" + dr["fsz_date"].ToString() + "',");
            strSql.Append("fdah='" + dr["fdah"].ToString() + "',");
            strSql.Append("fjfr='" + dr["fjfr"].ToString() + "',");
            strSql.Append("fuser='" + dr["fuser"].ToString() + "',");
            strSql.Append("fdate='" + dr["fdate"].ToString() + "',");
            strSql.Append("flx=" + dr["flx"].ToString() + ",");
            strSql.Append("fzjzz_date='" + dr["fzjzz_date"].ToString() + "',");
            strSql.Append("fspec='" + dr["fspec"].ToString() + "',");
            strSql.Append("fhelp_code='" + dr["fhelp_code"].ToString() + "',");
            strSql.Append("fxlzb='" + dr["fxlzb"].ToString() + "'");
            strSql.Append(" where finstr_id='" + dr["finstr_id"].ToString() + "'");
             return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());
            */
        }

        /// <summary>
        /// 仪器 删除一条数据
        /// </summary>
        public int BllInstrDelete(string id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete FROM sam_instr ");
            strSql.Append(" where finstr_id='" + id + "'");          
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());
        }
        #endregion 

        #region  仪器驱动
        /// <summary>
        /// 仪器驱动 列表 1为启用；0为未启用；其它如-1等为所有
        /// </summary>
        /// <returns></returns>
        public DataTable BllDriveDT(string finstr_id)
        {
            string sql = "SELECT * FROM sam_instr_drive where finstr_id='" + finstr_id + "'";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }

        /// <summary>
        /// 驱动记录存否
        /// </summary>
        /// <param name="finstr_id"></param>
        /// <returns></returns>
        public bool BllDriveOK(string finstr_id)
        {
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbRecordExists(ww.wwf.wwfbll.WWFInit.strDBConn, "SELECT count(1) FROM sam_instr_drive where finstr_id='" + finstr_id + "'");
        }

        /// <summary>
        /// 仪器驱动 增加一条数据
        /// </summary>
        public int BllDriveAdd(DataRow dr)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into SAM_INSTR_DRIVE(");
            strSql.Append("fdrive_id,finstr_id,fstart_bit,fend_bit,frequest_code,fresponse_code,fport,fbaud_rate,fparity,fdata_bit,fstop_bit,fstream,finfo_mode,fjz_flag,fimg_flag,fdrive,freadbufferSize,fwritebufferSize,freadtimeout,fwritetimeout,fy_mess,fry_mess,fhigh_mess,flow_mess,ferror_mess,fip,fpc_name,fremark");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + dr["fdrive_id"] + "',");
            strSql.Append("'" + dr["finstr_id"] + "',");
            strSql.Append("'" + dr["fstart_bit"] + "',");
            strSql.Append("'" + dr["fend_bit"] + "',");
            strSql.Append("'" + dr["frequest_code"] + "',");
            strSql.Append("'" + dr["fresponse_code"] + "',");
            strSql.Append("'" + dr["fport"] + "',");
            strSql.Append("'" + dr["fbaud_rate"] + "',");
            strSql.Append("'" + dr["fparity"] + "',");
            strSql.Append("'" + dr["fdata_bit"] + "',");
            strSql.Append("'" + dr["fstop_bit"] + "',");
            strSql.Append("'" + dr["fstream"] + "',");
            strSql.Append("'" + dr["finfo_mode"] + "',");
            strSql.Append("" + dr["fjz_flag"] + ",");
            strSql.Append("" + dr["fimg_flag"] + ",");
            strSql.Append("'" + dr["fdrive"] + "',");
            strSql.Append("" + dr["freadbufferSize"] + ",");
            strSql.Append("" + dr["fwritebufferSize"] + ",");
            strSql.Append("" + dr["freadtimeout"] + ",");
            strSql.Append("" + dr["fwritetimeout"] + ",");
            strSql.Append("'" + dr["fy_mess"] + "',");
            strSql.Append("'" + dr["fry_mess"] + "',");
            strSql.Append("'" + dr["fhigh_mess"] + "',");
            strSql.Append("'" + dr["flow_mess"] + "',");
            strSql.Append("'" + dr["ferror_mess"] + "',");
            strSql.Append("'" + dr["fip"] + "',");
            strSql.Append("'" + dr["fpc_name"] + "',");
            strSql.Append("'" + dr["fremark"] + "'");
            strSql.Append(")");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());
        }


        /// <summary>
        /// 仪器驱动 更新一条数据
        /// </summary>
        public int BllDriveUpdate(DataRow dr)
        {
            
             StringBuilder strSql=new StringBuilder();
             strSql.Append("update SAM_INSTR_DRIVE set ");
             strSql.Append("finstr_id='" + dr["finstr_id"] + "',");
             strSql.Append("fstart_bit='" + dr["fstart_bit"] + "',");
             strSql.Append("fend_bit='" + dr["fend_bit"] + "',");
             strSql.Append("frequest_code='" + dr["frequest_code"] + "',");
             strSql.Append("fresponse_code='" + dr["fresponse_code"] + "',");
             strSql.Append("fport='" + dr["fport"] + "',");
             strSql.Append("fbaud_rate='" + dr["fbaud_rate"] + "',");
             strSql.Append("fparity='" + dr["fparity"] + "',");
             strSql.Append("fdata_bit='" + dr["fdata_bit"] + "',");
             strSql.Append("fstop_bit='" + dr["fstop_bit"] + "',");
             strSql.Append("fstream='" + dr["fstream"] + "',");
             strSql.Append("finfo_mode='" + dr["finfo_mode"] + "',");
             strSql.Append("fjz_flag=" + dr["fjz_flag"] + ",");
             strSql.Append("fimg_flag=" + dr["fimg_flag"] + ",");
             strSql.Append("fdrive='" + dr["fdrive"] + "',");
             strSql.Append("freadbufferSize=" + dr["freadbufferSize"] + ",");
             strSql.Append("fwritebufferSize=" + dr["fwritebufferSize"] + ",");
             strSql.Append("freadtimeout=" + dr["freadtimeout"] + ",");
             strSql.Append("fwritetimeout=" + dr["fwritetimeout"] + ",");
             strSql.Append("fy_mess='" + dr["fy_mess"] + "',");
             strSql.Append("fry_mess='" + dr["fry_mess"] + "',");
             strSql.Append("fhigh_mess='" + dr["fhigh_mess"] + "',");
             strSql.Append("flow_mess='" + dr["flow_mess"] + "',");
             strSql.Append("ferror_mess='" + dr["ferror_mess"] + "',");
             strSql.Append("fip='" + dr["fip"] + "',");
             strSql.Append("fpc_name='" + dr["fpc_name"] + "',");
             strSql.Append("fremark='" + dr["fremark"] + "'");
             strSql.Append(" where fdrive_id='" + dr["fdrive_id"] + "'");
           
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());

        }

        /// <summary>
        /// 仪器驱动 删除一条数据
        /// </summary>
        public int BllDriveDelete(string id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete FROM sam_instr_drive ");
            strSql.Append(" where fdrive_id='" + id + "'");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());
        }
        #endregion 

        #region  已经启用的仪器 并且当前用户组
        /// <summary>
        ///  已经启用的仪器 并且当前用户
       /// </summary>
       /// <param name="fuse_if"></param>
       /// <param name="fjygroup_id"></param>
       /// <returns></returns>
        public DataTable BllInstrDTByUseAndGroupID(int fuse_if, string fjygroup_id)
        {
            DataTable dtInst = null;
            string sql = "";
            if (fjygroup_id == "kfz")
            {
                sql = "SELECT finstr_id,fname,fname+'('+finstr_id+')' as ShowName,fjytype_id FROM sam_instr WHERE (fuse_if = " + fuse_if + ") ORDER BY forder_by";
            }
            else
            {
                sql = "SELECT finstr_id,fname,fname+'('+finstr_id+')' as ShowName,fjytype_id FROM sam_instr WHERE (fuse_if = " + fuse_if + ") AND (fjygroup_id = '" + fjygroup_id + "') ORDER BY forder_by";
            }
            dtInst = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
            //DataRow drnow = dtInst.NewRow();
            //drnow["finstr_id"] = "";
            //drnow["fname"] = "";
            //dtInst.Rows.Add(drnow);
            return dtInst;
        }

        #endregion


        #region  仪器报表设置
        /// <summary>
        /// 取得报表
        /// </summary>
        /// <param name="finstr_id">仪器ＩＤ</param>
        /// <returns></returns>
        public DataTable BllReportDT(string  finstr_id)
        {
            string sql = "SELECT * FROM SAM_REPORT WHERE (finstr_id = '" + finstr_id + "')";            
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        /// <summary>
        /// 取报表，启用的或未启用的
        /// </summary>
        /// <param name="finstr_id"></param>
        /// <param name="fuse_if"></param>
        /// <returns></returns>
        public DataTable BllReportDT(string finstr_id, int fuse_if)
        {
            string sql = "SELECT * FROM SAM_REPORT WHERE (finstr_id = '" + finstr_id + "') and fuse_if=" + fuse_if + "";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        /// <summary>
        /// 增加一行
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public int BllReportAdd(DataRow dr)
        {          
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into SAM_REPORT(");
            strSql.Append("fsetting_id,finstr_id,fuse_if,fpage_width,fpage_height,fmargin_top,fmargin_bottom,fmargin_left,fmargin_right,frows,fone_count");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + dr["fsetting_id"].ToString() + "',");
            strSql.Append("'" + dr["finstr_id"].ToString() + "',");
            //strSql.Append("'" + dr["ftype_id"].ToString() + "',");
            strSql.Append("" + dr["fuse_if"].ToString() + ",");
            strSql.Append("" + dr["fpage_width"].ToString() + ",");
            strSql.Append("" + dr["fpage_height"].ToString() + ",");
            strSql.Append("" + dr["fmargin_top"].ToString() + ",");
            strSql.Append("" + dr["fmargin_bottom"].ToString() + ",");
            strSql.Append("" + dr["fmargin_left"].ToString() + ",");
            strSql.Append("" + dr["fmargin_right"].ToString() + ",");
            strSql.Append("" + dr["frows"].ToString() + ",");
            strSql.Append("" + dr["fone_count"].ToString() + "");            
            strSql.Append(")");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
            
        }
        /// <summary>
        /// 更新一条数据 报表
        /// </summary>
        public int BllReportUpdate(DataRow dr)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update SAM_REPORT set ");
            strSql.Append("ftype_id='" + dr["ftype_id"] + "',");
            strSql.Append("finstr_id='" + dr["finstr_id"] + "',");
            strSql.Append("fcode='" + dr["fcode"] + "',");
            strSql.Append("fname='" + dr["fname"] + "',");
            strSql.Append("fprinter_name='" + dr["fprinter_name"] + "',");
            strSql.Append("fpaper_name='" + dr["fpaper_name"] + "',");
            strSql.Append("fpage_width=" + dr["fpage_width"] + ",");
            strSql.Append("fpage_height=" + dr["fpage_height"] + ",");
            strSql.Append("fmargin_top=" + dr["fmargin_top"] + ",");
            strSql.Append("fmargin_bottom=" + dr["fmargin_bottom"] + ",");
            strSql.Append("fmargin_left=" + dr["fmargin_left"] + ",");
            strSql.Append("fmargin_right=" + dr["fmargin_right"] + ",");
            strSql.Append("forientation='" + dr["forientation"] + "',");
            strSql.Append("fuse_if=" + dr["fuse_if"] + ",");
            strSql.Append("forder_by='" + dr["forder_by"] + "',");
            strSql.Append("frows=" + dr["frows"] + ",");
            strSql.Append("fone_count=" + dr["fone_count"] + ",");
            strSql.Append("fy_mess='" + dr["fy_mess"] + "',");
            strSql.Append("fry_mess='" + dr["fry_mess"] + "',");
            strSql.Append("fhight_mess='" + dr["fhight_mess"] + "',");
            strSql.Append("flow_mess='" + dr["flow_mess"] + "',");
            strSql.Append("ferror_mess='" + dr["ferror_mess"] + "',");
            strSql.Append("f1='" + dr["f1"] + "',");
            strSql.Append("f2='" + dr["f2"] + "',");
            strSql.Append("f3='" + dr["f3"] + "',");
            strSql.Append("f4='" + dr["f4"] + "',");
            strSql.Append("f5='" + dr["f5"] + "',");
            strSql.Append("f6='" + dr["f6"] + "',");
            strSql.Append("f7='" + dr["f7"] + "',");
            strSql.Append("f8='" + dr["f8"] + "',");
            strSql.Append("f9='" + dr["f9"] + "',");
            strSql.Append("f10='" + dr["f10"] + "',");
            strSql.Append("fremark='" + dr["fremark"] + "'");
            strSql.Append(" where fsetting_id='" + dr["fsetting_id"] + "'");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }
        #endregion  仪器报表设置
    }
}
/*
 4.2.2.11	仪器类别 sam_instr_type
Name	Code	Data Type	Primary	Foreign Key	Mandatory
仪器类型_id	ftype_id	varchar(32)	TRUE	FALSE	TRUE
上级_id	fp_id	varchar(32)	FALSE	FALSE	FALSE
代号	fcode	varchar(32)	FALSE	FALSE	FALSE
名称	fname	varchar(220)	FALSE	FALSE	FALSE
英文名称	fname_e	varchar(200)	FALSE	FALSE	FALSE
启用否	fuse_if	int	FALSE	FALSE	FALSE
序号	forder_by	int	FALSE	FALSE	FALSE
助记符	fhelp_code	varchar(32)	FALSE	FALSE	FALSE
备注	fremark	varchar(200)	FALSE	FALSE	FALSE
4.2.2.12	仪器档案 sam_instr
Name	Code	Data Type	Primary	Foreign Key	Mandatory
仪器_id	finstr_id	varchar(32)	TRUE	FALSE	TRUE
仪器类型_id	ftype_id	varchar(32)	FALSE	TRUE	FALSE
检验组_id	fjygroup_id	varchar(32)	FALSE	FALSE	FALSE
检验类型_id	fjytype_id	varchar(32)	FALSE	FALSE	FALSE
启用否	fuse_if	int	FALSE	FALSE	FALSE
代号	fcode	varchar(32)	FALSE	FALSE	FALSE
名称	fname	varchar(220)	FALSE	FALSE	FALSE
英文名称	fname_e	varchar(200)	FALSE	FALSE	FALSE
序号	forder_by	int	FALSE	FALSE	FALSE
备注	fremark	varchar(200)	FALSE	FALSE	FALSE
机身号	fbody_no	varchar(32)	FALSE	FALSE	FALSE
新旧状态	fnew_old	varchar(32)	FALSE	FALSE	FALSE
购买价格	fprice	float	FALSE	FALSE	FALSE
图片	fimg	varchar(32)	FALSE	FALSE	FALSE
单位	funit	varchar(32)	FALSE	FALSE	FALSE
设备原值	fy_value	float(15)	FALSE	FALSE	FALSE
设备净值	fj_value	float(15)	FALSE	FALSE	FALSE
年折旧率	fzjl	float(15)	FALSE	FALSE	FALSE
已使用年限	fuse_year	int	FALSE	FALSE	FALSE
出厂日期	fout_date	varchar(32)	FALSE	FALSE	FALSE
到货日期	fdh_date	varchar(32)	FALSE	FALSE	FALSE
投用日期	fty_date	varchar(32)	FALSE	FALSE	FALSE
供应商_id	fgys_id	varchar(32)	FALSE	FALSE	FALSE
生产厂家_id	fcj_id	varchar(32)	FALSE	FALSE	FALSE
设备位置	fwz	varchar(32)	FALSE	FALSE	FALSE
国别	fgb	varchar(32)	FALSE	FALSE	FALSE
上帐日期	fsz_date	varchar(32)	FALSE	FALSE	FALSE
档案号	fdah	varchar(32)	FALSE	FALSE	FALSE
主要监护人	fjfr	varchar(32)	FALSE	FALSE	FALSE
创建人	fuser	varchar(32)	FALSE	FALSE	FALSE
创建时间	fdate	varchar(32)	FALSE	FALSE	FALSE
使用年限	flx	int	FALSE	FALSE	FALSE
折旧终止期	fzjzz_date	varchar(32)	FALSE	FALSE	FALSE
规格型号	fspec	varchar(32)	FALSE	FALSE	FALSE
助记符	fhelp_code	varchar(32)	FALSE	FALSE	FALSE
性能指标	fxlzb	varchar(32)	FALSE	FALSE	FALSE
 */