using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ww.wwf.dao;
using ww.wwf.wwfbll;
using System.Collections;
namespace ww.lis.lisbll.sam
{
    /// <summary>
    /// 
    /// </summary>
    public class TypeBLL : DAOWWF
    {
        public TypeBLL()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //	
        }
        #region  检验类型
        /// <summary>
        /// 检验类型 列表 1为启用；0为未启用；其它如-1等为所有
        /// </summary>
        /// <returns></returns>
        public DataTable BllCheckTypeDT(int fuse_if)
        {
            string sql = "";
            if (fuse_if == 1 || fuse_if == 0)
            {
                sql = "SELECT * FROM sam_check_type where fuse_if=" + fuse_if + " ORDER BY forder_by";
            }
            else
            {
                sql = "SELECT * FROM sam_check_type  ORDER BY forder_by";
            }
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }
        #endregion 
        #region  样本类型
        /// <summary>
        /// 检验类型 列表 1为启用；0为未启用；其它如-1等为所有
        /// </summary>
        /// <returns></returns>
        public DataTable BllSamTypeDT(int fuse_if)
        {
            string sql = "";
            if (fuse_if == 1 || fuse_if == 0)
            {
                sql = "SELECT * FROM sam_sample_type where fuse_if=" + fuse_if + " ORDER BY forder_by";
            }
            else
            {
                sql = "SELECT * FROM sam_sample_type  ORDER BY forder_by";
            }
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }
                #endregion 
        #region  样本类型 和检验类型关链查询 样本类型
        /// <summary>
        /// samID samName
        /// </summary>
        /// <returns></returns>
        public DataTable BllCheckAndSamTypeDT(string fcheck_type_id)
        {
            string sql = "SELECT t2.fcheck_type_id, t2.fsample_type_id, t2.fid, t1.fname, t1.forder_by,t1.fhelp_code FROM SAM_CHECK_SAMPLE t2,SAM_SAMPLE_TYPE t1 where t2.fsample_type_id = t1.fsample_type_id and t2.fcheck_type_id='" + fcheck_type_id + "' order by t1.forder_by";

            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="fid"></param>
        /// <returns></returns>
        public int BllCheckAndSamTypeDel(string fid)
        {
            string sql = "DELETE FROM SAM_CHECK_SAMPLE WHERE (fid = '" + fid + "')";

            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql);
        }
        /// <summary>
        /// 存在否
        /// </summary>
        /// <param name="fcheck_type_id"></param>
        /// <param name="fsample_type_id"></param>
        /// <returns></returns>
        public bool BllCheckAndSamTypeExists(string fcheck_type_id, string fsample_type_id)
        {
            string sql ="SELECT count(1) FROM SAM_CHECK_SAMPLE where fcheck_type_id='" + fcheck_type_id + "' and fsample_type_id='" + fsample_type_id + "'";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbRecordExists(ww.wwf.wwfbll.WWFInit.strDBConn, sql);
        }
        /// <summary>
        /// 增加记录
        /// </summary>
        /// <param name="fcheck_type_id"></param>
        /// <param name="?"></param>
        /// <returns></returns>
        public int BllCheckAndSamTypeAdd(string fcheck_type_id,string fsample_type_id)
        {
            string sql =
                "INSERT INTO SAM_CHECK_SAMPLE (fid, fcheck_type_id, fsample_type_id) VALUES ('" + this.DbGuid() + "', '" + fcheck_type_id + "', '" + fsample_type_id + "')";

            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql);
        }
        
        //
         #endregion 

        //SELECT fsample_type_id AS samID,(SELECT fname FROM sam_sample_type t1 WHERE (fsample_type_id = t.fsample_type_id)) AS samName FROM sam_check_sample t WHERE (fcheck_type_id = '01') ORDER BY samName

        #region  检验组
        /// <summary>
        /// 检验组 列表 1为启用；0为未启用；其它如-1等为所有
        /// </summary>
        /// <returns></returns>
        public DataTable BllCheckGroupDT(int fuse_if)
        {
            string sql = "";
            if (fuse_if == 1 || fuse_if == 0)
            {
                sql = "SELECT * FROM wwf_dept where fuse_flag=" + fuse_if + " ORDER BY forder_by"; //fp_id='jyk' and  
            }
            else
            {
                sql = "SELECT * FROM wwf_dept where fp_id='jyk' ORDER BY forder_by";
            }
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }
        #endregion 

        #region  公共类型
        
        
        /// <summary>
        /// 公共类型 列表 1为启用；0为未启用；其它如-1等为所有
        /// </summary>
        /// <returns></returns>
        public DataTable BllComTypeDT(string ftype)
        {
            string sql = "SELECT * FROM sam_type where ftype='" + ftype + "' ORDER BY forder_by";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }
        public DataTable BllComTypeDT(string ftype, int fuse_if,string strfhelp_code)
        {
            string sql = "SELECT * FROM sam_type where (ftype='" + ftype + "' and fuse_if='" + fuse_if + "') and ((fhelp_code like '%" + strfhelp_code + "%') or (fhelp_code IS NULL)) ORDER BY forder_by";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }
        public DataTable BllComTypeDTByCode(string ftype, string strfcode,string strfname)
        {
            string sql = "SELECT * FROM sam_type where (ftype='" + ftype + "') and (fcode='" + strfcode + "' or fname ='" + strfname + "')";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        /// <summary>
        /// 根据类别和编码取得名称
        /// </summary>
        /// <param name="ftype"></param>
        /// <param name="strfcode"></param>
        /// <returns></returns>
        public string  BllComTypeNameByCode(string ftype, string strfcode)
        {
            string sql = "SELECT fname FROM sam_type where (ftype='" + ftype + "') and (fcode='" + strfcode + "')";
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteScalarBySql(ww.wwf.wwfbll.WWFInit.strDBConn, sql);
        }
        /*
         3	ftype	varchar	32	0
2	fcode	varchar	64	0
0	fhelp_code	varchar	32	1
0	fname	varchar	200	1
0	forder_by	varchar	32	1
0	fuse_if	int	4	1
0	fremark	varchar	200	1
         */

        public string BllComTypeHelpCodeUpdate(IList lisSql)
        {
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(ww.wwf.wwfbll.WWFInit.strDBConn, lisSql);
        }
        /// <summary>
        /// 公共类型 增加一条数据
        /// </summary>
        public int BllComTypeAdd(DataRow dr)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into sam_type(");
            strSql.Append("fid,ftype,fcode,fhelp_code,fname,forder_by,fuse_if,fremark");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + dr["fid"].ToString() + "',");
            strSql.Append("'" + dr["ftype"].ToString() + "',");
            strSql.Append("'" + dr["fcode"].ToString() + "',");
           
            strSql.Append("'" + dr["fhelp_code"].ToString() + "',");
            strSql.Append("'" + dr["fname"].ToString() + "',");
            strSql.Append("'" + dr["forder_by"].ToString() + "',");
            strSql.Append("" + dr["fuse_if"].ToString() + ",");         
           
            strSql.Append("'" + dr["fremark"].ToString() + "'");
            strSql.Append(")");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());
        }


        /// <summary>
        /// 公共类型 更新一条数据
        /// </summary>
        public int BllComTypeUpdate(DataRow dr)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update sam_type set ");
            strSql.Append("fhelp_code='" + dr["fhelp_code"].ToString() + "',");           
            strSql.Append("fname='" + dr["fname"].ToString() + "',");         
            strSql.Append("forder_by='" + dr["forder_by"].ToString() + "',");
            strSql.Append("fuse_if=" + dr["fuse_if"].ToString() + ",");
            strSql.Append("fremark='" + dr["fremark"].ToString() + "'");
            strSql.Append(" where fid='" + dr["fid"].ToString() + "'");
            return ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn,strSql.ToString());

        }

   
        #endregion 

    }
}
/*
 4.2.2.11	仪器类别 sam_instr_type
Name	Code	Data Type	Primary	Foreign Key	Mandatory
仪器类型_id	ftype_id	varchar(32)	TRUE	FALSE	TRUE
上级_id	fp_id	varchar(32)	FALSE	FALSE	FALSE
代号	fcode	varchar(32)	FALSE	FALSE	FALSE
名称	fname	varchar(220)	FALSE	FALSE	FALSE
英文名称	fname_e	varchar(200)	FALSE	FALSE	FALSE
启用否	fuse_if	int	FALSE	FALSE	FALSE
序号	forder_by	int	FALSE	FALSE	FALSE
助记符	fhelp_code	varchar(32)	FALSE	FALSE	FALSE
备注	fremark	varchar(200)	FALSE	FALSE	FALSE
4.2.2.12	仪器档案 sam_instr
Name	Code	Data Type	Primary	Foreign Key	Mandatory
仪器_id	finstr_id	varchar(32)	TRUE	FALSE	TRUE
仪器类型_id	ftype_id	varchar(32)	FALSE	TRUE	FALSE
检验组_id	fjygroup_id	varchar(32)	FALSE	FALSE	FALSE
检验类型_id	fjytype_id	varchar(32)	FALSE	FALSE	FALSE
启用否	fuse_if	int	FALSE	FALSE	FALSE
代号	fcode	varchar(32)	FALSE	FALSE	FALSE
名称	fname	varchar(220)	FALSE	FALSE	FALSE
英文名称	fname_e	varchar(200)	FALSE	FALSE	FALSE
序号	forder_by	int	FALSE	FALSE	FALSE
备注	fremark	varchar(200)	FALSE	FALSE	FALSE
机身号	fbody_no	varchar(32)	FALSE	FALSE	FALSE
新旧状态	fnew_old	varchar(32)	FALSE	FALSE	FALSE
购买价格	fprice	float	FALSE	FALSE	FALSE
图片	fimg	varchar(32)	FALSE	FALSE	FALSE
单位	funit	varchar(32)	FALSE	FALSE	FALSE
设备原值	fy_value	float(15)	FALSE	FALSE	FALSE
设备净值	fj_value	float(15)	FALSE	FALSE	FALSE
年折旧率	fzjl	float(15)	FALSE	FALSE	FALSE
已使用年限	fuse_year	int	FALSE	FALSE	FALSE
出厂日期	fout_date	varchar(32)	FALSE	FALSE	FALSE
到货日期	fdh_date	varchar(32)	FALSE	FALSE	FALSE
投用日期	fty_date	varchar(32)	FALSE	FALSE	FALSE
供应商_id	fgys_id	varchar(32)	FALSE	FALSE	FALSE
生产厂家_id	fcj_id	varchar(32)	FALSE	FALSE	FALSE
设备位置	fwz	varchar(32)	FALSE	FALSE	FALSE
国别	fgb	varchar(32)	FALSE	FALSE	FALSE
上帐日期	fsz_date	varchar(32)	FALSE	FALSE	FALSE
档案号	fdah	varchar(32)	FALSE	FALSE	FALSE
主要监护人	fjfr	varchar(32)	FALSE	FALSE	FALSE
创建人	fuser	varchar(32)	FALSE	FALSE	FALSE
创建时间	fdate	varchar(32)	FALSE	FALSE	FALSE
使用年限	flx	int	FALSE	FALSE	FALSE
折旧终止期	fzjzz_date	varchar(32)	FALSE	FALSE	FALSE
规格型号	fspec	varchar(32)	FALSE	FALSE	FALSE
助记符	fhelp_code	varchar(32)	FALSE	FALSE	FALSE
性能指标	fxlzb	varchar(32)	FALSE	FALSE	FALSE
 */