using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ww.wwf.dao;
using ww.wwf.wwfbll;
using ww.wwf.com;
using System.Collections;
namespace ww.lis.lisbll.sam
{
    /// <summary>
    /// 检验申请 模型
    /// </summary>
    public class ApplyModel : DAOWWF
    {        
        public ApplyModel()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //           
                  
        }

        #region Model
        private string _fapply_id;
        private string _fjytype_id;
        private string _fsample_type_id;
        private int? _fjz_flag;
        private int? _fcharge_flag;
        private string _fcharge_id;
        private string _fstate;
        private decimal? _fjyf;
        private string _fapply_user_id;
        private string _fapply_dept_id;
        private string _fapply_time;
        private string _ftype_id;
        private string _fhz_id;
        private string _fhz_zyh;
        private string _fsex;
        private string _fname;
        private string _fvolk;
        private string _fhymen;
        private int? _fage;
        private string _fjob;
        private string _fgms;
        private string _fjob_org;
        private string _fadd;
        private string _ftel;
        private string _fage_unit;
        private int? _fage_year;
        private int? _fage_month;
        private int? _fage_day;
        private string _fward_num;
        private string _froom_num;
        private string _fbed_num;
        private string _fblood_abo;
        private string _fblood_rh;
        private string _fjymd;
        private string _fdiagnose;
        private decimal? _fheat;
        private decimal? _fxyld;
        private string _fcyy;
        private int? _fxhdb;
        private string _fcreate_user_id;
        private string _fcreate_time;
        private string _fupdate_user_id;
        private string _fupdate_time;
        private string _f检验项目名称;
        private string _fremark;
        private string _his申请单号;

        //add by wjz 20160122 添加身份证号 ▽
        private string _sfzh;
        //add by wjz 20160122 添加身份证号 △

        //=====2019年11月12日 yfh 添加电子健康卡号=====//
        private string _card_no;
        //=====2019年11月12日 yfh 添加电子健康卡号=====//

        /// <summary>
        /// 申请单号
        /// </summary>
        public string fapply_id
        {
            set { _fapply_id = value; }
            get { return _fapply_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fjytype_id
        {
            set { _fjytype_id = value; }
            get { return _fjytype_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fsample_type_id
        {
            set { _fsample_type_id = value; }
            get { return _fsample_type_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? fjz_flag
        {
            set { _fjz_flag = value; }
            get { return _fjz_flag; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? fcharge_flag
        {
            set { _fcharge_flag = value; }
            get { return _fcharge_flag; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fcharge_id
        {
            set { _fcharge_id = value; }
            get { return _fcharge_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fstate
        {
            set { _fstate = value; }
            get { return _fstate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? fjyf
        {
            set { _fjyf = value; }
            get { return _fjyf; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fapply_user_id
        {
            set { _fapply_user_id = value; }
            get { return _fapply_user_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fapply_dept_id
        {
            set { _fapply_dept_id = value; }
            get { return _fapply_dept_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fapply_time
        {
            set { _fapply_time = value; }
            get { return _fapply_time; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ftype_id
        {
            set { _ftype_id = value; }
            get { return _ftype_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fhz_id
        {
            set { _fhz_id = value; }
            get { return _fhz_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fhz_zyh
        {
            set { _fhz_zyh = value; }
            get { return _fhz_zyh; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fsex
        {
            set { _fsex = value; }
            get { return _fsex; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fname
        {
            set { _fname = value; }
            get { return _fname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fvolk
        {
            set { _fvolk = value; }
            get { return _fvolk; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fhymen
        {
            set { _fhymen = value; }
            get { return _fhymen; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? fage
        {
            set { _fage = value; }
            get { return _fage; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fjob
        {
            set { _fjob = value; }
            get { return _fjob; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fgms
        {
            set { _fgms = value; }
            get { return _fgms; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fjob_org
        {
            set { _fjob_org = value; }
            get { return _fjob_org; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fadd
        {
            set { _fadd = value; }
            get { return _fadd; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ftel
        {
            set { _ftel = value; }
            get { return _ftel; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fage_unit
        {
            set { _fage_unit = value; }
            get { return _fage_unit; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? fage_year
        {
            set { _fage_year = value; }
            get { return _fage_year; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? fage_month
        {
            set { _fage_month = value; }
            get { return _fage_month; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? fage_day
        {
            set { _fage_day = value; }
            get { return _fage_day; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fward_num
        {
            set { _fward_num = value; }
            get { return _fward_num; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string froom_num
        {
            set { _froom_num = value; }
            get { return _froom_num; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fbed_num
        {
            set { _fbed_num = value; }
            get { return _fbed_num; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fblood_abo
        {
            set { _fblood_abo = value; }
            get { return _fblood_abo; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fblood_rh
        {
            set { _fblood_rh = value; }
            get { return _fblood_rh; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fjymd
        {
            set { _fjymd = value; }
            get { return _fjymd; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fdiagnose
        {
            set { _fdiagnose = value; }
            get { return _fdiagnose; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? fheat
        {
            set { _fheat = value; }
            get { return _fheat; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? fxyld
        {
            set { _fxyld = value; }
            get { return _fxyld; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fcyy
        {
            set { _fcyy = value; }
            get { return _fcyy; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? fxhdb
        {
            set { _fxhdb = value; }
            get { return _fxhdb; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fcreate_user_id
        {
            set { _fcreate_user_id = value; }
            get { return _fcreate_user_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fcreate_time
        {
            set { _fcreate_time = value; }
            get { return _fcreate_time; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fupdate_user_id
        {
            set { _fupdate_user_id = value; }
            get { return _fupdate_user_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fupdate_time
        {
            set { _fupdate_time = value; }
            get { return _fupdate_time; }
        }

        /// <summary>
        /// 检验项目名称
        /// </summary>
        public string F检验项目名称
        {
            get { return _f检验项目名称; }
            set { _f检验项目名称 = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string fremark
        {
            set { _fremark = value; }
            get { return _fremark; }
        }

        /// <summary>
        /// HIS申请单号
        /// </summary>
        public string His申请单号
        {
            get { return _his申请单号; }
            set { _his申请单号 = value; }
        }

        //add by wjz 20160122 添加身份证号 ▽
        public string Sfzh
        {
            get { return _sfzh; }
            set { _sfzh = value; }
        }
        //add by wjz 20160122 添加身份证号 △

        //=====2019年11月12日 yfh 添加电子健康卡号=====//
        public string card_no
        {
            get { return _card_no; }
            set { _card_no = value; }
        }
        //=====2019年11月12日 yfh 添加电子健康卡号=====//
        #endregion Model
    }
}
