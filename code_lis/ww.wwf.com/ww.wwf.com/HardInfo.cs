using System;
using System.Management;
using System.Net;
using System.Runtime.InteropServices;

namespace ww.wwf.com
{
	public class HardInfo
	{
		[DllImport("kernel32.dll")]
		private static extern int GetVolumeInformation(string lpRootPathName, string lpVolumeNameBuffer, int nVolumeNameSize, ref int lpVolumeSerialNumber, int lpMaximumComponentLength, int lpFileSystemFlags, string lpFileSystemNameBuffer, int nFileSystemNameSize);

		public string GetCpuID()
		{
			string result;
			try
			{
				ManagementClass managementClass = new ManagementClass("Win32_Processor");
				ManagementObjectCollection instances = managementClass.GetInstances();
				string text = null;
				using (ManagementObjectCollection.ManagementObjectEnumerator enumerator = instances.GetEnumerator())
				{
					if (enumerator.MoveNext())
					{
						ManagementObject managementObject = (ManagementObject)enumerator.Current;
						text = managementObject.Properties["ProcessorId"].Value.ToString();
					}
				}
				result = text;
			}
			catch
			{
				result = "";
			}
			return result;
		}

		public string GetHardDiskID()
		{
			string result;
			try
			{
				ManagementObjectSearcher managementObjectSearcher = new ManagementObjectSearcher("SELECT * FROM Win32_PhysicalMedia");
				string text = null;
				using (ManagementObjectCollection.ManagementObjectEnumerator enumerator = managementObjectSearcher.Get().GetEnumerator())
				{
					if (enumerator.MoveNext())
					{
						ManagementObject managementObject = (ManagementObject)enumerator.Current;
						text = managementObject["SerialNumber"].ToString().Trim();
					}
				}
				result = text;
			}
			catch
			{
				result = "";
			}
			return result;
		}

		public string GetNetCardMAC()
		{
			string result;
			try
			{
				string text = "";
				ManagementClass managementClass = new ManagementClass("Win32_NetworkAdapterConfiguration");
				ManagementObjectCollection instances = managementClass.GetInstances();
				using (ManagementObjectCollection.ManagementObjectEnumerator enumerator = instances.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						ManagementObject managementObject = (ManagementObject)enumerator.Current;
						if ((bool)managementObject["IPEnabled"])
						{
							text += managementObject["MACAddress"].ToString();
						}
					}
				}
				result = text;
			}
			catch
			{
				result = "";
			}
			return result;
		}

		public string GetVolOf(string drvID)
		{
			string result;
			try
			{
				int num = 0;
				int lpMaximumComponentLength = 0;
				int lpFileSystemFlags = 0;
				string lpVolumeNameBuffer = null;
				string lpFileSystemNameBuffer = null;
				int volumeInformation = HardInfo.GetVolumeInformation(drvID + ":\\", lpVolumeNameBuffer, 256, ref num, lpMaximumComponentLength, lpFileSystemFlags, lpFileSystemNameBuffer, 256);
				result = num.ToString("x");
			}
			catch
			{
				result = "";
			}
			return result;
		}

		public string GetNetCardIP()
		{
			string result;
			try
			{
				string text = "";
				ManagementClass managementClass = new ManagementClass("Win32_NetworkAdapterConfiguration");
				ManagementObjectCollection instances = managementClass.GetInstances();
				using (ManagementObjectCollection.ManagementObjectEnumerator enumerator = instances.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						ManagementObject managementObject = (ManagementObject)enumerator.Current;
						if ((bool)managementObject["IPEnabled"])
						{
							string[] array = (string[])managementObject["IPAddress"];
							if (array.Length > 0)
							{
								text = array[0].ToString();
							}
						}
					}
				}
				result = text;
			}
			catch
			{
				result = "";
			}
			return result;
		}

		public string GetHostName()
		{
			return Dns.GetHostName();
		}

		public string GetHostIpAddress()
		{
			IPHostEntry hostByName = Dns.GetHostByName(Dns.GetHostName());
			return hostByName.AddressList[0].ToString();
		}

		public string GetUserName()
		{
			string result;
			try
			{
				string text = "";
				ManagementClass managementClass = new ManagementClass("Win32_ComputerSystem");
				ManagementObjectCollection instances = managementClass.GetInstances();
				using (ManagementObjectCollection.ManagementObjectEnumerator enumerator = instances.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						ManagementObject managementObject = (ManagementObject)enumerator.Current;
						text = managementObject["UserName"].ToString();
					}
				}
				result = text;
			}
			catch
			{
				result = "unknow";
			}
			finally
			{
			}
			return result;
		}

		public string GetSystemType()
		{
			string result;
			try
			{
				string text = "";
				ManagementClass managementClass = new ManagementClass("Win32_ComputerSystem");
				ManagementObjectCollection instances = managementClass.GetInstances();
				using (ManagementObjectCollection.ManagementObjectEnumerator enumerator = instances.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						ManagementObject managementObject = (ManagementObject)enumerator.Current;
						text = managementObject["SystemType"].ToString();
					}
				}
				result = text;
			}
			catch
			{
				result = "unknow";
			}
			finally
			{
			}
			return result;
		}

		public string GetTotalPhysicalMemory()
		{
			string result;
			try
			{
				string text = "";
				ManagementClass managementClass = new ManagementClass("Win32_ComputerSystem");
				ManagementObjectCollection instances = managementClass.GetInstances();
				using (ManagementObjectCollection.ManagementObjectEnumerator enumerator = instances.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						ManagementObject managementObject = (ManagementObject)enumerator.Current;
						text = managementObject["TotalPhysicalMemory"].ToString();
					}
				}
				result = text;
			}
			catch
			{
				result = "unknow";
			}
			finally
			{
			}
			return result;
		}

		public string GetDiskID()
		{
			string result;
			try
			{
				string text = "";
				ManagementClass managementClass = new ManagementClass("Win32_DiskDrive");
				ManagementObjectCollection instances = managementClass.GetInstances();
				using (ManagementObjectCollection.ManagementObjectEnumerator enumerator = instances.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						ManagementObject managementObject = (ManagementObject)enumerator.Current;
						text = (string)managementObject.Properties["Model"].Value;
					}
				}
				result = text;
			}
			catch
			{
				result = "unknow";
			}
			finally
			{
			}
			return result;
		}

		public string GetBaseBoardInfo()
		{
			string result = "";
			ManagementObjectSearcher managementObjectSearcher = new ManagementObjectSearcher("select * from Win32_BaseBoard");
			using (ManagementObjectCollection.ManagementObjectEnumerator enumerator = managementObjectSearcher.Get().GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					ManagementObject managementObject = (ManagementObject)enumerator.Current;
					result = (string)managementObject["Caption"];
				}
			}
			return result;
		}
	}
}
