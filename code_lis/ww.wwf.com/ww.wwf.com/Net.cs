using System;
using System.Net;

namespace ww.wwf.com
{
	public class Net
	{
		public static string GetHostName()
		{
			IPHostEntry hostByName = Dns.GetHostByName(Dns.GetHostName());
			return hostByName.HostName.ToString();
		}

		public static string GetHostIpAddress()
		{
			IPHostEntry hostByName = Dns.GetHostByName(Dns.GetHostName());
			return hostByName.AddressList[0].ToString();
		}
	}
}
