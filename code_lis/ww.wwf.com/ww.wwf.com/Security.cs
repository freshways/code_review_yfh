using System;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;

namespace ww.wwf.com
{
	public class Security
	{
		public static string StrToEncrypt(string format, string str)
		{
			byte[] bytes = new UnicodeEncoding().GetBytes(str);
			byte[] value = ((HashAlgorithm)CryptoConfig.CreateFromName(format)).ComputeHash(bytes);
			return BitConverter.ToString(value);
		}

		public static void PwdConfiguration()
		{
			string protectionProvider = "RsaProtectedConfigurationProvider";
			Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
			ConfigurationSection connectionStrings = configuration.ConnectionStrings;
			if (!connectionStrings.SectionInformation.IsProtected && !connectionStrings.ElementInformation.IsLocked)
			{
				connectionStrings.SectionInformation.ProtectSection(protectionProvider);
				connectionStrings.SectionInformation.ForceSave = true;
				configuration.Save(ConfigurationSaveMode.Full);
			}
		}
	}
}
