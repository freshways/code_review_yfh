using System;
using System.IO;
using System.Text.RegularExpressions;

namespace ww.wwf.com
{
	public class Public
	{
		public static string BllGuid()
		{
			string text = Guid.NewGuid().ToString();
			return text.Replace("-", "");
		}

		public static decimal BllGetXSWS(decimal decValue, int intWX)
		{
			decimal result;
			switch (intWX)
			{
			case 1:
				result = decimal.Parse(decValue.ToString("0.0"));
				break;
			case 2:
				result = decimal.Parse(decValue.ToString("0.00"));
				break;
			case 3:
				result = decimal.Parse(decValue.ToString("0.000"));
				break;
			case 4:
				result = decimal.Parse(decValue.ToString("0.0000"));
				break;
			case 5:
				result = decimal.Parse(decValue.ToString("0.00000"));
				break;
			case 6:
				result = decimal.Parse(decValue.ToString("0.000000"));
				break;
			case 7:
				result = decimal.Parse(decValue.ToString("0.0000000"));
				break;
			case 8:
				result = decimal.Parse(decValue.ToString("0.0000000"));
				break;
			default:
				result = decimal.Parse(decValue.ToString("0.00"));
				break;
			}
			return result;
		}

		public static bool IsNumber(string strNumber)
		{
			Regex regex = new Regex("[^0-9.-]");
			Regex regex2 = new Regex("[0-9]*[.][0-9]*[.][0-9]*");
			Regex regex3 = new Regex("[0-9]*[-][0-9]*[-][0-9]*");
			string text = "^([-]|[.]|[-.]|[0-9])[0-9]*[.]*[0-9]+$";
			string text2 = "^([-]|[0-9])[0-9]*$";
			Regex regex4 = new Regex(string.Concat(new string[]
			{
				"(",
				text,
				")|(",
				text2,
				")"
			}));
			return !regex.IsMatch(strNumber) && !regex2.IsMatch(strNumber) && !regex3.IsMatch(strNumber) && regex4.IsMatch(strNumber) && !strNumber.Trim().Equals("-");
		}

		public byte[] StreamToBytes(Stream stream)
		{
			byte[] array = new byte[stream.Length];
			stream.Read(array, 0, array.Length);
			stream.Seek(0L, SeekOrigin.Begin);
			return array;
		}

		public Stream BytesToStream(byte[] bytes)
		{
			return new MemoryStream(bytes);
		}

		public void StreamToFile(Stream stream, string fileName)
		{
			byte[] array = new byte[stream.Length];
			stream.Read(array, 0, array.Length);
			stream.Seek(0L, SeekOrigin.Begin);
			FileStream fileStream = new FileStream(fileName, FileMode.Create);
			BinaryWriter binaryWriter = new BinaryWriter(fileStream);
			binaryWriter.Write(array);
			binaryWriter.Close();
			fileStream.Close();
		}

		public Stream FileToStream(string fileName)
		{
			FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
			byte[] array = new byte[fileStream.Length];
			fileStream.Read(array, 0, array.Length);
			fileStream.Close();
			return new MemoryStream(array);
		}
	}
}
