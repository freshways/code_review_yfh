using System;
using System.Xml;

namespace ww.wwf.com
{
	public class AppConfig
	{
		public static void ConfigSetValue(string strExecutablePath, string AppKey, string AppValue)
		{
			XmlDocument xmlDocument = new XmlDocument();
			xmlDocument.Load(strExecutablePath + ".config");
			XmlNode xmlNode = xmlDocument.SelectSingleNode("//connectionStrings");
			XmlElement xmlElement = (XmlElement)xmlNode.SelectSingleNode("//add[@name='" + AppKey + "']");
			if (xmlElement != null)
			{
				xmlElement.SetAttribute("connectionString", AppValue);
			}
			else
			{
				XmlElement xmlElement2 = xmlDocument.CreateElement("add");
				xmlElement2.SetAttribute("name", AppKey);
				xmlElement2.SetAttribute("connectionString", AppValue);
				xmlNode.AppendChild(xmlElement2);
			}
			xmlDocument.Save(strExecutablePath + ".config");
		}

		public static void ConfigSetValueAppSettings(string strExecutablePath, string AppKey, string AppValue)
		{
			XmlDocument xmlDocument = new XmlDocument();
			xmlDocument.Load(strExecutablePath + ".config");
			XmlNode xmlNode = xmlDocument.SelectSingleNode("//appSettings");
			XmlElement xmlElement = (XmlElement)xmlNode.SelectSingleNode("//add[@key='" + AppKey + "']");
			if (xmlElement != null)
			{
				xmlElement.SetAttribute("value", AppValue);
			}
			else
			{
				XmlElement xmlElement2 = xmlDocument.CreateElement("add");
				xmlElement2.SetAttribute("key", AppKey);
				xmlElement2.SetAttribute("value", AppValue);
				xmlNode.AppendChild(xmlElement2);
			}
			xmlDocument.Save(strExecutablePath + ".config");
		}

		public static string ConfigGetValue(string strExecutablePath, string appKey)
		{
			XmlDocument xmlDocument = new XmlDocument();
			string result;
			try
			{
				xmlDocument.Load(strExecutablePath + ".config");
				XmlNode xmlNode = xmlDocument.SelectSingleNode("//appSettings");
				XmlElement xmlElement = (XmlElement)xmlNode.SelectSingleNode("//add[@key='" + appKey + "']");
				if (xmlElement != null)
				{
					result = xmlElement.GetAttribute("value");
				}
				else
				{
					result = "";
				}
			}
			catch (Exception)
			{
				result = "";
			}
			return result;
		}
	}
}
