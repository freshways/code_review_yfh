using System;

namespace ww.wwf.com
{
	[Flags]
	public enum SpellOptions
	{
		FirstLetterOnly = 1,
		TranslateUnknowWordToInterrogation = 2,
		EnableUnicodeLetter = 4
	}
}
