using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ww.wwf.com.Hardware
{
	public class Win32_TemperatureProbe : IWMI
	{
		private Connection WMIConnection;

		public Win32_TemperatureProbe(Connection WMIConnection)
		{
			this.WMIConnection = WMIConnection;
		}

		public IList<string> GetPropertyValues()
		{
			string value = Regex.Match(base.GetType().ToString(), "Win32_.*").Value;
			return WMIReader.GetPropertyValues(this.WMIConnection, "SELECT * FROM " + value, value);
		}
	}
}
