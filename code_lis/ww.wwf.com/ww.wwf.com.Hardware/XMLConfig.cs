using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace ww.wwf.com.Hardware
{
	public class XMLConfig
	{
		public static List<string> GetSettings(string WMIClassName)
		{
			string filename = Directory.GetCurrentDirectory() + "\\WMISettings.xml";
			List<string> list = new List<string>();
			XmlDocument xmlDocument = new XmlDocument();
			xmlDocument.Load(filename);
			XmlNode xmlNode = xmlDocument.SelectSingleNode("//" + WMIClassName);
			for (int i = 0; i < xmlNode.ChildNodes.Count; i++)
			{
				list.Add(xmlNode.ChildNodes[i].InnerText);
			}
			return list;
		}
	}
}
