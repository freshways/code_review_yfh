using System;
using System.Collections.Generic;
using System.Management;

namespace ww.wwf.com.Hardware
{
	public class WMIReader
	{
		public static IList<string> GetPropertyValues(Connection WMIConnection, string SelectQuery, string className)
		{
			ManagementScope getConnectionScope = WMIConnection.GetConnectionScope;
			List<string> list = new List<string>();
			SelectQuery query = new SelectQuery(SelectQuery);
			ManagementObjectSearcher managementObjectSearcher = new ManagementObjectSearcher(getConnectionScope, query);
			try
			{
				using (ManagementObjectCollection.ManagementObjectEnumerator enumerator = managementObjectSearcher.Get().GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						ManagementObject managementObject = (ManagementObject)enumerator.Current;
						foreach (string current in XMLConfig.GetSettings(className))
						{
							try
							{
								list.Add(current + ": " + managementObject[current].ToString());
							}
							catch (SystemException)
							{
							}
						}
					}
				}
			}
			catch (ManagementException var_6_CD)
			{
			}
			return list;
		}
	}
}
