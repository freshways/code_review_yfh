using System;
using System.Management;

namespace ww.wwf.com.Hardware
{
	public class Connection
	{
		private ManagementScope connectionScope;

		private ConnectionOptions options;

		public ManagementScope GetConnectionScope
		{
			get
			{
				return this.connectionScope;
			}
		}

		public ConnectionOptions GetOptions
		{
			get
			{
				return this.options;
			}
		}

		public static ConnectionOptions SetConnectionOptions()
		{
			return new ConnectionOptions
			{
				Impersonation = ImpersonationLevel.Impersonate,
				Authentication = AuthenticationLevel.Default,
				EnablePrivileges = true
			};
		}

		public static ManagementScope SetConnectionScope(string machineName, ConnectionOptions options)
		{
			ManagementScope managementScope = new ManagementScope();
			managementScope.Path = new ManagementPath("\\\\" + machineName + "\\root\\CIMV2");
			managementScope.Options = options;
			try
			{
				managementScope.Connect();
			}
			catch (ManagementException ex)
			{
				Console.WriteLine("An Error Occurred: " + ex.Message.ToString());
			}
			return managementScope;
		}

		public Connection()
		{
			this.EstablishConnection(null, null, null, Environment.MachineName);
		}

		public Connection(string userName, string password, string domain, string machineName)
		{
			this.EstablishConnection(userName, password, domain, machineName);
		}

		private void EstablishConnection(string userName, string password, string domain, string machineName)
		{
			this.options = Connection.SetConnectionOptions();
			if (domain != null || userName != null)
			{
				this.options.Username = domain + "\\" + userName;
				this.options.Password = password;
			}
			this.connectionScope = Connection.SetConnectionScope(machineName, this.options);
		}
	}
}
