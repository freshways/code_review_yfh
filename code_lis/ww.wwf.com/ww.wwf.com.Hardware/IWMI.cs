using System;
using System.Collections.Generic;

namespace ww.wwf.com.Hardware
{
	internal interface IWMI
	{
		IList<string> GetPropertyValues();
	}
}
