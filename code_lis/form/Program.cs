﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ww.wwf.wwfbll;
using ww;
using System.Configuration;

namespace ww.form
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // add by wjz 20151209 加密数据库连接 ▽
            try
            {
                //EncryptConfiguration();
            }
            catch//(Exception ex)
            {
                WWMessage.MessageShowWarning("请尝试以管理员身份运行Lis程序。如果还出现此错误，请联系技术人员。");
                return;
            }
            // add by wjz 20151209 加密数据库连接 △

            try
            {
                // System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("zh-CHS");
                //WWFInit wwfinit = new WWFInit();
                //  bool bolOK =  wwfinit.DbClientUser();
                // MessageBox.Show(bolOK.ToString());
                HIS.Model.Dal.SqlHelper.connectionString = "Data Source=192.168.10.57;Initial Catalog=LIS2703;Persist Security Info=True;User ID=yggsuser;Password=yggsuser";
                LoginBLL bll = new LoginBLL();
                bll.BllSetSysInfo();
                bll.BllSet系统参数();
                DateTime dt = ww.wwf.wwfbll.SysBLL.GetServerTime();
                ww.wwf.com.DateTimeControl.syncTime(dt);
                Application.Run(new wwf.MainForm());
                /*
                if (wwfinit.DbClientUser())
                {                    
                   // ww.wwf.com.Security.PwdConfiguration();
                    LoginBLL bll = new LoginBLL();
                    bll.BllSetSysInfo();
                    Application.Run(new wwf.MainForm());
                    //Application.Run(new wwf.Form1());

                }*/
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowWarning("启动失败的可能原因是：\n\n" + ex.Message.ToString() + "\n\n服务支持");
            }

        }

        // add by wjz 20151209 加密数据库连接 ▽
        static void EncryptConfiguration()
        {
            // 使用什么类型的加密
            //支持两种类型的加密：
            //DataProtectionConfigurationProvider：使用Windows Data Protection API (DPAPI)
            //RsaProtectedConfigurationProvider：使用RSA算法

            string provider = "RsaProtectedConfigurationProvider";
            Configuration config = null;
            config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            // 加密连接字符串
            ConfigurationSection section = config.ConnectionStrings;
            if ((section.SectionInformation.IsProtected == false) &&
                (section.ElementInformation.IsLocked == false))
            {
                section.SectionInformation.ProtectSection(provider);
                section.SectionInformation.ForceSave = true;
                config.Save(ConfigurationSaveMode.Full);
            }
            WWMessage.MessageShowWarning(config.ConnectionStrings.ConnectionStrings[2].ConnectionString);
            string hisconn = config.ConnectionStrings.ConnectionStrings[2].ConnectionString;
            if (hisconn.Contains("server=127.0.0.1;"))
            {
                hisconn = hisconn.Replace("server=127.0.0.1;", "server=192.168.10.57;");
            }
            if (hisconn.Contains("database=LIS;"))
            {
                hisconn = hisconn.Replace("database=LIS;", "database=LIS2721;");
            }
            if (hisconn.Contains("uid=sa;"))
            {
                hisconn = hisconn.Replace("uid=sa;", "uid=yggsuser;");
            }
            if (hisconn.Contains("pwd=sasa;"))
            {
                hisconn = hisconn.Replace("pwd=sasa;", "pwd=yggsuser;");
            }
            config.ConnectionStrings.ConnectionStrings[2].ConnectionString = hisconn;
            config.Save(ConfigurationSaveMode.Full);
        }
        // add by wjz 20151209 加密数据库连接 △
    }
}