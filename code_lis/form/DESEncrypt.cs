﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace ww.form
{
    public class DESEncrypt
    {
        #region DESjiava互通解密

        public const string THE_KEY = "zhonglianjiayu";

        #region 加密
        /// <summary>
        /// DES加密New
        /// </summary>
        /// <param name="pToEncrypt">要加密的字符串</param>
        /// <returns></returns>
        public static string DESEnCode(string pToEncrypt)
        {
            return DESEnCode(pToEncrypt, THE_KEY);
        }

        /// <summary>
        /// 加密算法
        /// </summary>
        /// <param name="pToEncrypt">要加密的字符串</param>
        /// <param name="Keys">密钥</param>
        /// <returns></returns>
        public static string DESEnCode(string pToEncrypt, string Keys)
        {
            DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
            provider.Mode = CipherMode.ECB;//C#默认CBC模式，解密java需要设置ECB模式
            byte[] inputByteArray = Encoding.GetEncoding("GB2312").GetBytes(pToEncrypt);

            //建立加密对象的密钥和偏移量    
            //原文使用ASCIIEncoding.ASCII方法的GetBytes方法    
            //使得输入密码必须输入英文文本    
            provider.Key = Encoding.ASCII.GetBytes(Keys.Substring(0, 8));
            provider.IV = Encoding.ASCII.GetBytes(Keys.Substring(0, 8));
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, provider.CreateEncryptor(), CryptoStreamMode.Write);

            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            return byteArr2HexStr(ms.ToArray());
        }

        /// <summary>
        /// 将byte数组转换为表示16进制值的字符串 和public static byte[]    hexStr2ByteArr(String strIn) 互为可逆的转换过程 
        /// </summary>
        /// <param name="arrB"></param>
        /// <returns></returns>
        public static String byteArr2HexStr(byte[] arrB)
        {
            int iLen = arrB.Length;
            // 每个byte用两个字符才能表示，所以字符串的长度是数组长度的两倍   
            StringBuilder sb = new StringBuilder(iLen * 2);
            for (int i = 0; i < iLen; i++)
            {
                int intTmp = arrB[i];
                // 把负数转换为正数   
                while (intTmp < 0)
                {
                    intTmp = intTmp + 256;
                }
                // 小于0F的数需要在前面补0   
                if (intTmp < 16)
                {
                    sb.Append("0");
                }
                sb.Append(Convert.ToString(intTmp, 16));
            }
            return sb.ToString();
        }
        #endregion

        #region 解密

        /// <summary>
        /// DES解密 -java des加密字符串
        /// </summary>
        /// <param name="str">加密字符串</param>
        /// <returns></returns>
        public static string DESDeCode(string pToDecrypt)
        {
            return DESDeCode(pToDecrypt, THE_KEY);
        }

        /// <summary>
        /// 解密java des加密字符串+1
        /// </summary>
        /// <param name="str">加密字符串</param>
        ///<param name="Keys">密钥</param>
        /// <returns></returns>
        public static string DESDeCode(string pToDecrypt, string Keys)
        {
            try
            {
                DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
                provider.Mode = CipherMode.ECB;//C#默认CBC模式，解密java需要设置ECB模式
                // 密钥
                provider.Key = Encoding.ASCII.GetBytes(Keys.Substring(0, 8));
                // 偏移量
                provider.IV = Encoding.ASCII.GetBytes(Keys.Substring(0, 8));
                byte[] buffer = hexStr2ByteArr(pToDecrypt);
                //byte[] buffer = new byte[pToDecrypt.Length / 2];
                //for (int i = 0; i < (pToDecrypt.Length / 2); i++)
                //{
                //    int num2 = Convert.ToInt32(pToDecrypt.Substring(i * 2, 2), 0x10);
                //    buffer[i] = (byte)num2;
                //}
                MemoryStream stream = new MemoryStream();
                CryptoStream stream2 = new CryptoStream(stream, provider.CreateDecryptor(), CryptoStreamMode.Write);
                stream2.Write(buffer, 0, buffer.Length);
                stream2.FlushFinalBlock();
                stream.Close();
                return Encoding.GetEncoding("GB2312").GetString(stream.ToArray());
            }
            catch (Exception) { return ""; }
        }

        /// <summary>
        ///将表示16进制值的字符串转换为byte数组 和public static String byteArr2HexStr(byte[] arrB)  * 互为可逆的转换过程 
        /// </summary>
        /// <param name="strIn"></param>
        /// <returns></returns>
        public static byte[] hexStr2ByteArr(String strIn)
        {
            // 两个字符表示一个字节，所以字节数组长度是字符串长度除以2   
            int iLen = strIn.Length / 2;
            byte[] arrOut = new byte[iLen];
            for (int i = 0; i < iLen; i++)
            {
                arrOut[i] = (byte)Convert.ToInt32(strIn.Substring(i * 2, 2), 16);
            }
            return arrOut;
        }
        #endregion

        /// <summary>
        /// 个人姓名解密
        /// </summary>
        /// <param name="s加密字符串"></param>
        /// <returns></returns>
        public static String DES解密(string s加密字符串)
        {
            String jiemi = "";
            int len = s加密字符串.Length / 16;
            for (int i = 0; i < len; i++)
            {
                jiemi += DESEncrypt.DESDeCode(s加密字符串.Substring(i * 16, 16));
            }
            return jiemi;
        }

        #endregion
    }
}
