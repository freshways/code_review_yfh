################################################
# 1.该配置文件定义资源,比如图片;并定义了菜单和工具栏的构成.
# 2.该文件被新工作流设计器使用,也被审批流设计器使用.
# 3.弹出菜单的额外信息.
################################################

Icon=nc/ui/wfengine/designer/resources/logo.gif
Logo=nc/ui/wfengine/designer/resources/splash.png
CloseBox=nc/ui/wfengine/designer/resources/closebox.gif
ToolbarMenu=nc/ui/wfengine/designer/resources/ToolbarMenu.gif
ToolBoxViewIcon=nc/ui/wfengine/designer/resources/toolbox.gif

# 定制ProcessExplorer树节点图标
CompanyTreeIcon=images/desktop30/product.gif
CompanyOpenTreeIcon=images/desktop30/productopen.gif
#PackageTreeIcon=nc/ui/wfengine/designer/resources/packageTreeIcon.gif
PackageTreeIcon=images/desktop30/productopen.gif
ProcessTreeIcon=nc/ui/wfengine/designer/resources/processTreeIcon.gif
ParticipantTreeIcon=nc/ui/wfengine/designer/resources/participantTreeIcon.gif
ActivityTreeIcon=nc/ui/wfengine/designer/resources/activityTreeIcon.gif
BlockActivityTreeIcon=nc/ui/wfengine/designer/resources/blockTreeIcon.gif
SubflowTreeIcon=nc/ui/wfengine/designer/resources/subflowactivity.gif
RouteTreeIcon=nc/ui/wfengine/designer/resources/routeactivity.gif
TransitionTreeIcon=nc/ui/wfengine/designer/resources/transitionTreeIcon.gif
StartTreeIcon=nc/ui/wfengine/designer/resources/start.gif
EndTreeIcon=nc/ui/wfengine/designer/resources/end.gif

# definition of PasteAt popup menu item
PasteAtImage=nc/ui/wfengine/designer/resources/paste.gif

# definition of addPoint popup menu item
AddPointImage=nc/ui/wfengine/designer/resources/addpoint.gif

# definition of removePoint popup menu item
RemovePointImage=nc/ui/wfengine/designer/resources/removepoint.gif

##################################################################
# 过程定义工具栏，分为三栏：绘图、编辑、视图
processToolbars=processtoolbox processedittoolbar processviewtoolbar

#
# “编辑”工具栏定义
#
processedittoolbar=Undo Redo - Cut Copy Paste Delete
processedittoolbarImage=nc/ui/wfengine/designer/resources/edittoolbar.gif
CutImage=nc/ui/wfengine/designer/resources/cut.gif
CopyImage=nc/ui/wfengine/designer/resources/copy.gif
PasteImage=nc/ui/wfengine/designer/resources/paste.gif
UndoImage=nc/ui/wfengine/designer/resources/undo.gif
RedoImage=nc/ui/wfengine/designer/resources/redo.gif
DeleteImage=nc/ui/wfengine/designer/resources/delete.gif

#
# “视图”工具栏定义
#
processviewtoolbar=ActualSize ZoomIn ZoomOut
processviewtoolbarImage=nc/ui/wfengine/designer/resources/viewtoolbar.gif
ActualSizeImage=nc/ui/wfengine/designer/resources/actualsize.gif
ZoomInImage=nc/ui/wfengine/designer/resources/zoomin.gif
ZoomOutImage=nc/ui/wfengine/designer/resources/zoomout.gif

#
# “绘图”工具栏定义
#
processtoolbox=selectTool participantTool startTool endTool subflowActivityTool blockActivityTool genericActivityTool routeActivityTool transitionTool selfRoutedTransitionTool
processtoolboxImage=nc/ui/wfengine/designer/resources/toolbox.gif
selectToolImage=nc/ui/wfengine/designer/resources/select.gif
participantToolImage=nc/ui/wfengine/designer/resources/participant.gif
participantOpenedImage=nc/ui/wfengine/designer/resources/participant_opened.gif
participantClosedImage=nc/ui/wfengine/designer/resources/participant_closed.gif
participantLeafImage=nc/ui/wfengine/designer/resources/participant_leaf.gif
subflowActivityToolImage=nc/ui/wfengine/designer/resources/subflowactivity.gif
blockActivityToolImage=nc/ui/wfengine/designer/resources/blockactivity.gif
startToolImage=nc/ui/wfengine/designer/resources/start.gif
endToolImage=nc/ui/wfengine/designer/resources/end.gif
genericActivityToolImage=nc/ui/wfengine/designer/resources/genericactivity.gif
routeActivityToolImage=nc/ui/wfengine/designer/resources/routeactivity.gif
transitionToolImage=nc/ui/wfengine/designer/resources/transition.gif
selfRoutedTransitionToolImage=nc/ui/wfengine/designer/resources/selfroutedtransition.gif

#
# 审批流“绘图”工具栏定义
#
approvetoolbox=selectTool startTool endTool subflowActivityTool routeActivityTool transitionTool selfRoutedTransitionTool
#approvetoolbox=selectTool startTool endTool routeActivityTool transitionTool selfRoutedTransitionTool
approvetoolboxImage=nc/ui/wfengine/designer/resources/toolbox.gif

##################################################################
# 菜单栏定义
processMenubars=file package process processEdit processView tool help

# 
# file Menu definition
#
file=NewPackage NewProcess Open ImportXPDL - Save - Exit
OpenImage=nc/ui/wfengine/designer/resources/open.gif
ImportXPDLImage=nc/ui/wfengine/designer/resources/open.gif
NewPackageImage=nc/ui/wfengine/designer/resources/new.gif
NewProcessImage=nc/ui/wfengine/designer/resources/new.gif
SaveImage=nc/ui/wfengine/designer/resources/save.gif
ExportToXPDLImage=nc/ui/wfengine/designer/resources/saveas.gif
ExitImage=nc/ui/wfengine/designer/resources/stop.gif

#
# package Menu definition
#
package=CheckValidity - Namespaces PackageProperties Processes ExternalPackages TypeDeclarations Participants Applications WorkflowRelevantData - ImportExternalProcess
CheckValidityImage=nc/ui/wfengine/designer/resources/check.gif
NamespacesImage=nc/ui/wfengine/designer/resources/namespaces.gif
PackagePropertiesImage=nc/ui/wfengine/designer/resources/properties.gif
ProcessesImage=nc/ui/wfengine/designer/resources/processes.gif
TypeDeclarationsImage=nc/ui/wfengine/designer/resources/typedeclarations.gif
ExternalPackagesImage=nc/ui/wfengine/designer/resources/externalpackages.gif
ImportExternalProcessImage=nc/ui/wfengine/designer/resources/importexternalprocess.gif
ParticipantsImage=nc/ui/wfengine/designer/resources/participants.gif
ApplicationsImage=nc/ui/wfengine/designer/resources/applications.gif
WorkflowRelevantDataImage=nc/ui/wfengine/designer/resources/workflowrelevantdata.gif

#
# process Menu definition
#
process=CheckValidity - ProcessProperties Participants Applications WorkflowRelevantData FormalParameters - ActivitiesOverview TransitionsOverview - SaveAsJPG SaveAsSVG
ProcessPropertiesImage=nc/ui/wfengine/designer/resources/properties.gif
FormalParametersImage=nc/ui/wfengine/designer/resources/formalparameters.gif
ActivitiesOverviewImage=nc/ui/wfengine/designer/resources/activities.gif
TransitionsOverviewImage=nc/ui/wfengine/designer/resources/transitions.gif
SaveAsJPGImage=nc/ui/wfengine/designer/resources/saveasjpg.gif
SaveAsSVGImage=nc/ui/wfengine/designer/resources/saveassvg.gif

#
# processEdit Menu definition
#
processEdit=Undo Redo - Cut Copy Paste Delete - EditProperties
EditPropertiesImage=nc/ui/wfengine/designer/resources/properties.gif
EditCellImage=nc/ui/wfengine/designer/resources/edit.gif
EditCellSmallImage=nc/ui/wfengine/designer/resources/edit_small.gif

#
# processView Menu definition
#
processView=ActualSize ZoomIn ZoomOut

#
# tool Menu definition
#
tool=ChangeLanguage - Look&Feel - Config
ChangeLanguageMenu=English Chinese
Look&FeelMenu=Metal Motif Windows

#
# help Menu definition
#
help=HelpTutorial HelpManual - HelpAbout
HelpTutorialImage=nc/ui/wfengine/designer/resources/help.gif
HelpManualImage=nc/ui/wfengine/designer/resources/help.gif
HelpAboutImage=nc/ui/wfengine/designer/resources/about.gif


##################################################################
# 使用拖拽添加图元时使用的默认图标
DefaultGenericCellIcon=nc/ui/wfengine/designer/resources/about.gif
DefaultRouteCellIcon=nc/ui/wfengine/designer/resources/routeOnFlow.gif
DefaultBlockCellIcon=nc/ui/wfengine/designer/resources/about.gif
DefaultSubflowCellIcon=nc/ui/wfengine/designer/resources/subflowOnFlow.gif
DefaultParticipantCellIcon=nc/ui/wfengine/designer/resources/participant.gif
DefaultStartCellIcon=nc/ui/wfengine/designer/resources/startOnFlow.gif
DefaultEndCellIcon=nc/ui/wfengine/designer/resources/endOnFlow.gif

##################################################################
# 菜单的额外信息
AddPointMnemonic=a
RemovePointMnemonic=r
SetSelfRoutingMnemonic=s
SetNoRoutingMnemonic=n
fileMnemonic=f
OpenMnemonic=o
OpenAccel=CTRL-O
ImportXPDLMnemonic=i
ImportXPDLAccel=CTRL-I
NewPackageMnemonic=k
NewPackageAccel=CTRL-K
NewProcessMnemonic=p
NewProcessAccel=CTRL-P
SaveMnemonic=s
SaveAccel=CTRL-S
ExportToXPDLMnemonic=e
ExportToXPDLAccel=CTRL-SHIFT-E
ExitMnemonic=x
ExitAccel=ALT-X
UndoMnemonic=u
UndoAccel=CTRL-Z
RedoMnemonic=r
RedoAccel=CTRL-Y
DeleteMnemonic=d
DeleteAccel=DELETE
EditPropertiesMnemonic=r
EditPropertiesAccel=CTRL-R
EditCellMnemonic=e
EditCellAccel=F2
CopyMnemonic=c
CopyAccel=CTRL-C
CutMnemonic=t
CutAccel=CTRL-X
PasteAtMnemonic=p
PasteAtAccel=CTRL-V

ZoomInMnemonic=i
ZoomInAccel=CTRL-SHIFT-I
ZoomOutMnemonic=o
ZoomOutAccel=CTRL-SHIFT-O
ActualSizeMnemonic=a
ActualSizeAccel=HOME

##################################################################
# Misc
ALLDescription=All files (*.*)
XMLDescription=XML files (*.xml)
XPDLDescription=XPDL files (*.xpdl)

