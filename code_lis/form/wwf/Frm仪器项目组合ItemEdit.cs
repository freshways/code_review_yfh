﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using ww.lis.lisbll.sam;
using System.Data.SqlClient;
namespace ww.form.wwf
{
    public partial class Frm仪器项目组合ItemEdit : Form
    {
        ItemBLL bllItem = new ItemBLL();//项目逻辑
        string m_instrid = "";//仪器ＩＤ
        string m_instrName = "";//仪器名称
        string m_groupid = "";

        public Frm仪器项目组合ItemEdit(string groupid, string instrid)
        {
            InitializeComponent();
            DataGridViewObject.AutoGenerateColumns = false;

            m_groupid = groupid;
            m_instrid = instrid;
        }
        
        
        private void ItemSelectForm_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + " " + m_instrName;
            GetItemDT("");
        }

        /// <summary>
        /// 取得仪器所检项目
        /// </summary>
        /// <param name="strInstrID"></param>
        private void GetItemDT(string fhelp_code)
        {
            try
            {
                this.sam_itemBindingSource.DataSource = this.bllItem.BllItemNew(m_groupid, m_instrid, fhelp_code);
                this.DataGridViewObject.DataSource = sam_itemBindingSource;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 过滤
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            GetItemDT(this.textBox1.Text.Trim());
        }

        private void GelSelValueList()
        {

            this.Validate();
            this.sam_itemBindingSource.EndEdit();
            int intSelValue = 0;

            List<string> valuesList = new List<string>();
            List<SqlParameter> paramlist = new List<SqlParameter>();
            paramlist.Add(new SqlParameter("@groupid", m_groupid));

            for (int inner = 0; inner < this.DataGridViewObject.Rows.Count; inner++)
            {
                Object objSel = this.DataGridViewObject.Rows[inner].Cells["fselect"].Value;
                intSelValue = (objSel == null) ? 0 : Convert.ToInt32(objSel);

                if (intSelValue == 1 && this.DataGridViewObject.Rows[inner].Cells["fitem_id"].Value != null)
                {
                    string strfitemid = this.DataGridViewObject.Rows[inner].Cells["fitem_id"].Value.ToString();
                    valuesList.Add("(@groupitemid" + inner.ToString() + ",@groupid,@fitemid" + inner.ToString() + ")");
                    paramlist.Add(new SqlParameter("@groupitemid" + inner.ToString(), Guid.NewGuid().ToString().Replace("-", "")));
                    paramlist.Add(new SqlParameter("@fitemid" + inner.ToString(), strfitemid));
                }
            }

            if (valuesList.Count > 0)
            {
                StringBuilder sbSql = new StringBuilder("insert into dbo.sam_instr_groups_rel([groupitemid],[groupid],[fitemid]) values");
                for(int index = 0; index < valuesList.Count; index++)
                {
                    if (index > 0)
                    {
                        sbSql.Append(",");
                    }
                    sbSql.Append(valuesList[index]);
                }

                int ret = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sbSql.ToString(), paramlist.ToArray());
                if(ret  > 0)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowInformation("保存成功");
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                }
                else
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowInformation("保存失败");
                }
            }
            else
            {
                this.Close();
            }

        }

        

      
        private void DataGridViewObject_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void toolStripButtonNo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bindingNavigatorOK_Click(object sender, EventArgs e)
        {
            SaveSelectedItem();
        }


        void SaveSelectedItem()
        {
            try
            {
                GelSelValueList();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void DataGridViewObject_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Space))//或(e.KeyCode==Keys.Enter) 
            {
                SaveSelectedItem();
            }
        }

        private void toolStripButtonAll_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.sam_itemBindingSource.EndEdit();
                for (int i = 0; i < this.DataGridViewObject.Rows.Count; i++)
                {
                    this.DataGridViewObject.Rows[i].Cells["fselect"].Value = "1";
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonNO1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.sam_itemBindingSource.EndEdit();
                for (int i = 0; i < this.DataGridViewObject.Rows.Count; i++)
                {
                    this.DataGridViewObject.Rows[i].Cells["fselect"].Value = "0";
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
    }
}