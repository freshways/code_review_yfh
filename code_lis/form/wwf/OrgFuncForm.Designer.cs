﻿namespace ww.form.wwf
{
    partial class OrgFuncForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrgFuncForm));
            this.wwTreeView1 = new ww.wwf.control.WWControlLib.WWTreeView();
            this.imageListTree = new System.Windows.Forms.ImageList(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.wwTreeView3 = new ww.wwf.control.WWControlLib.WWTreeView();
            this.panelTool = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.butRef = new System.Windows.Forms.Button();
            this.butDel = new System.Windows.Forms.Button();
            this.butAdd = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.wwTreeView2 = new ww.wwf.control.WWControlLib.WWTreeView();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panelTool.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // wwTreeView1
            // 
            this.wwTreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wwTreeView1.Dock = System.Windows.Forms.DockStyle.Left;
            this.wwTreeView1.ImageIndex = 0;
            this.wwTreeView1.ImageList = this.imageListTree;
            this.wwTreeView1.Location = new System.Drawing.Point(3, 17);
            this.wwTreeView1.Name = "wwTreeView1";
            this.wwTreeView1.SelectedImageIndex = 1;
            this.wwTreeView1.Size = new System.Drawing.Size(171, 438);
            this.wwTreeView1.TabIndex = 85;
            this.wwTreeView1.ZADataTable = null;
            this.wwTreeView1.ZADisplayFieldName = "";
            this.wwTreeView1.ZAKeyFieldName = "";
            this.wwTreeView1.ZAParentFieldName = "";
            this.wwTreeView1.ZAToolTipTextName = "";
            this.wwTreeView1.ZATreeViewRootValue = "";
            this.wwTreeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.wwTreeView1_AfterSelect);
            // 
            // imageListTree
            // 
            this.imageListTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTree.ImageStream")));
            this.imageListTree.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTree.Images.SetKeyName(0, "12.gif");
            this.imageListTree.Images.SetKeyName(1, "27.gif");
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.panelTool);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.wwTreeView1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(744, 458);
            this.groupBox1.TabIndex = 109;
            this.groupBox1.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.wwTreeView3);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox3.Location = new System.Drawing.Point(485, 17);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 438);
            this.groupBox3.TabIndex = 113;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "已分配";
            // 
            // wwTreeView3
            // 
            this.wwTreeView3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wwTreeView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wwTreeView3.ImageIndex = 0;
            this.wwTreeView3.ImageList = this.imageListTree;
            this.wwTreeView3.Location = new System.Drawing.Point(3, 17);
            this.wwTreeView3.Name = "wwTreeView3";
            this.wwTreeView3.SelectedImageIndex = 1;
            this.wwTreeView3.Size = new System.Drawing.Size(194, 418);
            this.wwTreeView3.TabIndex = 87;
            this.wwTreeView3.ZADataTable = null;
            this.wwTreeView3.ZADisplayFieldName = "";
            this.wwTreeView3.ZAKeyFieldName = "";
            this.wwTreeView3.ZAParentFieldName = "";
            this.wwTreeView3.ZAToolTipTextName = "";
            this.wwTreeView3.ZATreeViewRootValue = "";
            this.wwTreeView3.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.wwTreeView3_AfterSelect);
            // 
            // panelTool
            // 
            this.panelTool.Controls.Add(this.textBox1);
            this.panelTool.Controls.Add(this.butRef);
            this.panelTool.Controls.Add(this.butDel);
            this.panelTool.Controls.Add(this.butAdd);
            this.panelTool.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelTool.Location = new System.Drawing.Point(374, 17);
            this.panelTool.Name = "panelTool";
            this.panelTool.Size = new System.Drawing.Size(111, 438);
            this.panelTool.TabIndex = 111;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(15, 230);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(80, 21);
            this.textBox1.TabIndex = 6;
            this.textBox1.Visible = false;
            // 
            // butRef
            // 
            this.butRef.Image = ((System.Drawing.Image)(resources.GetObject("butRef.Image")));
            this.butRef.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butRef.Location = new System.Drawing.Point(15, 157);
            this.butRef.Name = "butRef";
            this.butRef.Size = new System.Drawing.Size(80, 23);
            this.butRef.TabIndex = 5;
            this.butRef.Text = "   刷新(&R)";
            this.butRef.UseVisualStyleBackColor = true;
            this.butRef.Click += new System.EventHandler(this.butRef_Click);
            // 
            // butDel
            // 
            this.butDel.Enabled = false;
            this.butDel.Image = ((System.Drawing.Image)(resources.GetObject("butDel.Image")));
            this.butDel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butDel.Location = new System.Drawing.Point(15, 99);
            this.butDel.Name = "butDel";
            this.butDel.Size = new System.Drawing.Size(80, 23);
            this.butDel.TabIndex = 1;
            this.butDel.Text = "   取消(&A)";
            this.butDel.UseVisualStyleBackColor = true;
            this.butDel.Click += new System.EventHandler(this.butDel_Click);
            // 
            // butAdd
            // 
            this.butAdd.Enabled = false;
            this.butAdd.Image = ((System.Drawing.Image)(resources.GetObject("butAdd.Image")));
            this.butAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butAdd.Location = new System.Drawing.Point(15, 50);
            this.butAdd.Name = "butAdd";
            this.butAdd.Size = new System.Drawing.Size(80, 23);
            this.butAdd.TabIndex = 0;
            this.butAdd.Text = "   分配(&A)";
            this.butAdd.UseVisualStyleBackColor = true;
            this.butAdd.Click += new System.EventHandler(this.butAdd_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.wwTreeView2);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox2.Location = new System.Drawing.Point(174, 17);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 438);
            this.groupBox2.TabIndex = 112;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "可分配";
            // 
            // wwTreeView2
            // 
            this.wwTreeView2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wwTreeView2.CheckBoxes = true;
            this.wwTreeView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wwTreeView2.ImageIndex = 0;
            this.wwTreeView2.ImageList = this.imageListTree;
            this.wwTreeView2.Location = new System.Drawing.Point(3, 17);
            this.wwTreeView2.Name = "wwTreeView2";
            this.wwTreeView2.SelectedImageIndex = 1;
            this.wwTreeView2.Size = new System.Drawing.Size(194, 418);
            this.wwTreeView2.TabIndex = 86;
            this.wwTreeView2.ZADataTable = null;
            this.wwTreeView2.ZADisplayFieldName = "";
            this.wwTreeView2.ZAKeyFieldName = "";
            this.wwTreeView2.ZAParentFieldName = "";
            this.wwTreeView2.ZAToolTipTextName = "";
            this.wwTreeView2.ZATreeViewRootValue = "";
            this.wwTreeView2.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.wwTreeView2_AfterCheck);
            // 
            // OrgFuncForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 458);
            this.Controls.Add(this.groupBox1);
            this.Name = "OrgFuncForm";
            this.Text = "OrgFuncForm";
            this.Load += new System.EventHandler(this.FuncForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.panelTool.ResumeLayout(false);
            this.panelTool.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ww.wwf.control.WWControlLib.WWTreeView wwTreeView1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panelTool;
        private System.Windows.Forms.Button butRef;
        private System.Windows.Forms.Button butDel;
        private System.Windows.Forms.Button butAdd;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private ww.wwf.control.WWControlLib.WWTreeView wwTreeView2;
        private ww.wwf.control.WWControlLib.WWTreeView wwTreeView3;
        private System.Windows.Forms.ImageList imageListTree;
        private System.Windows.Forms.TextBox textBox1;
    }
}