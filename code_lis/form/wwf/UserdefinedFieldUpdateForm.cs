﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;

namespace ww.form.wwf
{
    public partial class UserdefinedFieldUpdateForm : Form
    {
        protected UserdefinedBLL bll = new UserdefinedBLL();      
        string strTableID = "";//表ID
        DataTable dtCurr = new DataTable();
       
        public UserdefinedFieldUpdateForm(string tableid)
        {
            strTableID = tableid;
            InitializeComponent();
           
        }

        private void UserdefinedFieldUpdateForm_Load(object sender, EventArgs e)
        {
            GetFieldDT();
            dataGridView1.AutoGenerateColumns = false;
            wwfcolumnsBindingSource.DataSource = dtCurr;
            dataGridView1.DataSource = wwfcolumnsBindingSource;
           
        }
       

        private void GetFieldDT()
        {
            try
            {
                dtCurr = this.bll.BllColumnsByftable_id(this.strTableID);
                              
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void buttonNO_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.wwfcolumnsBindingSource.EndEdit();
                for (int i = 0; i < this.dtCurr.Rows.Count; i++)
                {
                    this.bll.BllColumnsUpdate(this.dtCurr.Rows[i]);
                }
                //GetFieldDT();
                this.Close();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        

        private void buttonRef_Click(object sender, EventArgs e)
        {
            GetFieldDT();
        }
              
    }
}