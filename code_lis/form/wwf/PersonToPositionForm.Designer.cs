﻿namespace ww.form.wwf
{
    partial class PersonToPositionForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PersonToPositionForm));
            this.butDel = new System.Windows.Forms.Button();
            this.listBoxOK = new System.Windows.Forms.ListBox();
            this.butRef = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.listBoxNO = new System.Windows.Forms.ListBox();
            this.wwTreeView1 = new ww.wwf.control.WWControlLib.WWTreeView();
            this.panelTool = new System.Windows.Forms.Panel();
            this.butAdd = new System.Windows.Forms.Button();
            this.imageListTree = new System.Windows.Forms.ImageList(this.components);
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panelTool.SuspendLayout();
            this.SuspendLayout();
            // 
            // butDel
            // 
            this.butDel.Enabled = false;
            this.butDel.Image = ((System.Drawing.Image)(resources.GetObject("butDel.Image")));
            this.butDel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butDel.Location = new System.Drawing.Point(15, 70);
            this.butDel.Name = "butDel";
            this.butDel.Size = new System.Drawing.Size(80, 23);
            this.butDel.TabIndex = 1;
            this.butDel.Text = "   取消(&A)";
            this.butDel.UseVisualStyleBackColor = true;
            this.butDel.Click += new System.EventHandler(this.butDel_Click);
            // 
            // listBoxOK
            // 
            this.listBoxOK.DisplayMember = "PersonName";
            this.listBoxOK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxOK.FormattingEnabled = true;
            this.listBoxOK.ItemHeight = 12;
            this.listBoxOK.Location = new System.Drawing.Point(3, 17);
            this.listBoxOK.Name = "listBoxOK";
            this.listBoxOK.Size = new System.Drawing.Size(194, 436);
            this.listBoxOK.TabIndex = 114;
            this.listBoxOK.ValueMember = "forg_id";
            // 
            // butRef
            // 
            this.butRef.Image = ((System.Drawing.Image)(resources.GetObject("butRef.Image")));
            this.butRef.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butRef.Location = new System.Drawing.Point(15, 117);
            this.butRef.Name = "butRef";
            this.butRef.Size = new System.Drawing.Size(80, 23);
            this.butRef.TabIndex = 6;
            this.butRef.Text = "   刷新(&R)";
            this.butRef.UseVisualStyleBackColor = true;
            this.butRef.Click += new System.EventHandler(this.butRef_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.listBoxOK);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox2.Location = new System.Drawing.Point(482, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 465);
            this.groupBox2.TabIndex = 120;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "已分配";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.listBoxNO);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(171, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 465);
            this.groupBox1.TabIndex = 119;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "可分配";
            // 
            // listBoxNO
            // 
            this.listBoxNO.DisplayMember = "PersonName";
            this.listBoxNO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxNO.FormattingEnabled = true;
            this.listBoxNO.ItemHeight = 12;
            this.listBoxNO.Location = new System.Drawing.Point(3, 17);
            this.listBoxNO.Name = "listBoxNO";
            this.listBoxNO.Size = new System.Drawing.Size(194, 436);
            this.listBoxNO.TabIndex = 113;
            this.listBoxNO.ValueMember = "fperson_id";
            // 
            // wwTreeView1
            // 
            this.wwTreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wwTreeView1.Dock = System.Windows.Forms.DockStyle.Left;
            this.wwTreeView1.ImageIndex = 0;
            this.wwTreeView1.ImageList = this.imageListTree;
            this.wwTreeView1.Location = new System.Drawing.Point(0, 0);
            this.wwTreeView1.Name = "wwTreeView1";
            this.wwTreeView1.SelectedImageIndex = 1;
            this.wwTreeView1.Size = new System.Drawing.Size(171, 465);
            this.wwTreeView1.TabIndex = 117;
            this.wwTreeView1.ZADataTable = null;
            this.wwTreeView1.ZADisplayFieldName = "";
            this.wwTreeView1.ZAKeyFieldName = "";
            this.wwTreeView1.ZAParentFieldName = "";
            this.wwTreeView1.ZAToolTipTextName = "";
            this.wwTreeView1.ZATreeViewRootValue = "";
            this.wwTreeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.wwTreeView1_AfterSelect);
            // 
            // panelTool
            // 
            this.panelTool.Controls.Add(this.butRef);
            this.panelTool.Controls.Add(this.butDel);
            this.panelTool.Controls.Add(this.butAdd);
            this.panelTool.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelTool.Location = new System.Drawing.Point(371, 0);
            this.panelTool.Name = "panelTool";
            this.panelTool.Size = new System.Drawing.Size(111, 465);
            this.panelTool.TabIndex = 118;
            // 
            // butAdd
            // 
            this.butAdd.Enabled = false;
            this.butAdd.Image = ((System.Drawing.Image)(resources.GetObject("butAdd.Image")));
            this.butAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butAdd.Location = new System.Drawing.Point(15, 23);
            this.butAdd.Name = "butAdd";
            this.butAdd.Size = new System.Drawing.Size(80, 23);
            this.butAdd.TabIndex = 0;
            this.butAdd.Text = "   分配(&A)";
            this.butAdd.UseVisualStyleBackColor = true;
            this.butAdd.Click += new System.EventHandler(this.butAdd_Click);
            // 
            // imageListTree
            // 
            this.imageListTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTree.ImageStream")));
            this.imageListTree.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTree.Images.SetKeyName(0, "12.gif");
            this.imageListTree.Images.SetKeyName(1, "27.gif");
            // 
            // PersonToPositionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(705, 465);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panelTool);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.wwTreeView1);
            this.Name = "PersonToPositionForm";
            this.Text = "PersonToPositionForm";
            this.Load += new System.EventHandler(this.PersonToPositionForm_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.panelTool.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button butDel;
        private System.Windows.Forms.ListBox listBoxOK;
        private System.Windows.Forms.Button butRef;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox listBoxNO;
        private ww.wwf.control.WWControlLib.WWTreeView wwTreeView1;
        private System.Windows.Forms.Panel panelTool;
        private System.Windows.Forms.Button butAdd;
        private System.Windows.Forms.ImageList imageListTree;
    }
}