﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ww.form.wwf
{
    public partial class DemoForm : SysBaseForm
    {
        ww.wwf.wwfbll.PersonBLL bll = new ww.wwf.wwfbll.PersonBLL();
        public DemoForm()
        {
            InitializeComponent();
        }

        private void DemoForm_Load(object sender, EventArgs e)
        {
            this.ActiveControl = this.dataGridView1;           
            this.bindingSource1.DataSource = this.bll.BllDT("");
            dataGridView1.DataSource = bindingSource1;
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            try
            {
                SolidBrush B = new SolidBrush(Color.Blue);
                e.Graphics.DrawString(Convert.ToString(e.RowIndex + 1, System.Globalization.CultureInfo.CurrentUICulture), e.InheritedRowStyle.Font, B, e.RowBounds.Location.X, e.RowBounds.Location.Y + 4);
            }
            catch { }
        }
        /// <summary>
        /// 热键
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref   System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            
            if (keyData == (Keys.F5))
            {
                toolStripButton1.PerformClick();
                return true;
            }
           
            return base.ProcessCmdKey(ref   msg, keyData);
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("测试a");
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("测试f5");
        }
        /*
  if (keyData == (Keys.Control | Keys.S))
            {
                MessageBox.Show("陶1");
                return true;
            }
            if (keyData == (Keys.Control | Keys.Shift | Keys.C))
            {
                MessageBox.Show("陶2");
                return true;
            }
            if (keyData == (Keys.Control | Keys.P))
            {
                MessageBox.Show("陶3");
                return true;
            }
            if (keyData == (Keys.F5))
            {              
                buttonU.PerformClick();
                return true;//此后还有一些信息需要处理，否则有异常
            }
            return base.ProcessCmdKey(ref   msg, keyData);
 */
    }
}