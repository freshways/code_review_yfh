﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ww.form.wwf.Customization
{
    class DefaultInstrument
    {
        private static string strInstrID = "";

        public static void SetInstrID(string instrid)
        {
            strInstrID = instrid;
        }

        public static string GetInstrID()
        {
            return strInstrID;
        }
    }
}
