﻿namespace ww.form.wwf
{
    partial class Frm_部门管理
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label fdept_idLabel;
            System.Windows.Forms.Label fnameLabel;
            System.Windows.Forms.Label forder_byLabel;
            System.Windows.Forms.Label fuse_flagLabel;
            System.Windows.Forms.Label fremarkLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_部门管理));
            this.wwTreeView1 = new ww.wwf.control.WWControlLib.WWTreeView();
            this.imageListTree = new System.Windows.Forms.ImageList(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.fremarkTextBox = new System.Windows.Forms.TextBox();
            this.wwf_deptBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wwfDataSet = new ww.form.wwf.wwfDataSet();
            this.fuse_flagCheckBox = new System.Windows.Forms.CheckBox();
            this.forder_byTextBox = new System.Windows.Forms.TextBox();
            this.fnameTextBox = new System.Windows.Forms.TextBox();
            this.fdept_idTextBox = new System.Windows.Forms.TextBox();
            this.bN = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonUpdate = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonS = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonDel = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonR = new System.Windows.Forms.ToolStripButton();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.toolStripButtonhelp_code = new System.Windows.Forms.ToolStripButton();
            fdept_idLabel = new System.Windows.Forms.Label();
            fnameLabel = new System.Windows.Forms.Label();
            forder_byLabel = new System.Windows.Forms.Label();
            fuse_flagLabel = new System.Windows.Forms.Label();
            fremarkLabel = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wwf_deptBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wwfDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).BeginInit();
            this.bN.SuspendLayout();
            this.SuspendLayout();
            // 
            // fdept_idLabel
            // 
            fdept_idLabel.AutoSize = true;
            fdept_idLabel.Location = new System.Drawing.Point(25, 19);
            fdept_idLabel.Name = "fdept_idLabel";
            fdept_idLabel.Size = new System.Drawing.Size(35, 12);
            fdept_idLabel.TabIndex = 0;
            fdept_idLabel.Text = "编号:";
            // 
            // fnameLabel
            // 
            fnameLabel.AutoSize = true;
            fnameLabel.Location = new System.Drawing.Point(25, 51);
            fnameLabel.Name = "fnameLabel";
            fnameLabel.Size = new System.Drawing.Size(35, 12);
            fnameLabel.TabIndex = 2;
            fnameLabel.Text = "名称:";
            // 
            // forder_byLabel
            // 
            forder_byLabel.AutoSize = true;
            forder_byLabel.Location = new System.Drawing.Point(25, 83);
            forder_byLabel.Name = "forder_byLabel";
            forder_byLabel.Size = new System.Drawing.Size(35, 12);
            forder_byLabel.TabIndex = 4;
            forder_byLabel.Text = "排序:";
            // 
            // fuse_flagLabel
            // 
            fuse_flagLabel.AutoSize = true;
            fuse_flagLabel.Location = new System.Drawing.Point(25, 115);
            fuse_flagLabel.Name = "fuse_flagLabel";
            fuse_flagLabel.Size = new System.Drawing.Size(35, 12);
            fuse_flagLabel.TabIndex = 6;
            fuse_flagLabel.Text = "启用:";
            // 
            // fremarkLabel
            // 
            fremarkLabel.AutoSize = true;
            fremarkLabel.Location = new System.Drawing.Point(13, 147);
            fremarkLabel.Name = "fremarkLabel";
            fremarkLabel.Size = new System.Drawing.Size(47, 12);
            fremarkLabel.TabIndex = 8;
            fremarkLabel.Text = "拼音码:";
            // 
            // wwTreeView1
            // 
            this.wwTreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wwTreeView1.Dock = System.Windows.Forms.DockStyle.Left;
            this.wwTreeView1.ImageIndex = 0;
            this.wwTreeView1.ImageList = this.imageListTree;
            this.wwTreeView1.Location = new System.Drawing.Point(0, 30);
            this.wwTreeView1.Name = "wwTreeView1";
            this.wwTreeView1.SelectedImageIndex = 1;
            this.wwTreeView1.Size = new System.Drawing.Size(200, 339);
            this.wwTreeView1.TabIndex = 85;
            this.wwTreeView1.ZADataTable = null;
            this.wwTreeView1.ZADisplayFieldName = "";
            this.wwTreeView1.ZAKeyFieldName = "";
            this.wwTreeView1.ZAParentFieldName = "";
            this.wwTreeView1.ZAToolTipTextName = "";
            this.wwTreeView1.ZATreeViewRootValue = "";
            this.wwTreeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.wwTreeView1_AfterSelect);
            // 
            // imageListTree
            // 
            this.imageListTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTree.ImageStream")));
            this.imageListTree.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTree.Images.SetKeyName(0, "12.gif");
            this.imageListTree.Images.SetKeyName(1, "27.gif");
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Window;
            this.panel1.Controls.Add(fremarkLabel);
            this.panel1.Controls.Add(this.fremarkTextBox);
            this.panel1.Controls.Add(fuse_flagLabel);
            this.panel1.Controls.Add(this.fuse_flagCheckBox);
            this.panel1.Controls.Add(forder_byLabel);
            this.panel1.Controls.Add(this.forder_byTextBox);
            this.panel1.Controls.Add(fnameLabel);
            this.panel1.Controls.Add(this.fnameTextBox);
            this.panel1.Controls.Add(fdept_idLabel);
            this.panel1.Controls.Add(this.fdept_idTextBox);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(200, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(463, 339);
            this.panel1.TabIndex = 110;
            // 
            // fremarkTextBox
            // 
            this.fremarkTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_deptBindingSource, "fremark", true));
            this.fremarkTextBox.Location = new System.Drawing.Point(71, 143);
            this.fremarkTextBox.Multiline = true;
            this.fremarkTextBox.Name = "fremarkTextBox";
            this.fremarkTextBox.Size = new System.Drawing.Size(237, 21);
            this.fremarkTextBox.TabIndex = 9;
            // 
            // wwf_deptBindingSource
            // 
            this.wwf_deptBindingSource.DataMember = "wwf_dept";
            this.wwf_deptBindingSource.DataSource = this.wwfDataSet;
            // 
            // wwfDataSet
            // 
            this.wwfDataSet.DataSetName = "wwfDataSet";
            this.wwfDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // fuse_flagCheckBox
            // 
            this.fuse_flagCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.wwf_deptBindingSource, "fuse_flag", true));
            this.fuse_flagCheckBox.Location = new System.Drawing.Point(71, 109);
            this.fuse_flagCheckBox.Name = "fuse_flagCheckBox";
            this.fuse_flagCheckBox.Size = new System.Drawing.Size(241, 24);
            this.fuse_flagCheckBox.TabIndex = 7;
            // 
            // forder_byTextBox
            // 
            this.forder_byTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_deptBindingSource, "forder_by", true));
            this.forder_byTextBox.Location = new System.Drawing.Point(71, 78);
            this.forder_byTextBox.Name = "forder_byTextBox";
            this.forder_byTextBox.Size = new System.Drawing.Size(237, 21);
            this.forder_byTextBox.TabIndex = 5;
            // 
            // fnameTextBox
            // 
            this.fnameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_deptBindingSource, "fname", true));
            this.fnameTextBox.Location = new System.Drawing.Point(71, 47);
            this.fnameTextBox.Name = "fnameTextBox";
            this.fnameTextBox.Size = new System.Drawing.Size(237, 21);
            this.fnameTextBox.TabIndex = 3;
            // 
            // fdept_idTextBox
            // 
            this.fdept_idTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_deptBindingSource, "fdept_id", true));
            this.fdept_idTextBox.Location = new System.Drawing.Point(71, 16);
            this.fdept_idTextBox.Name = "fdept_idTextBox";
            this.fdept_idTextBox.Size = new System.Drawing.Size(237, 21);
            this.fdept_idTextBox.TabIndex = 1;
            // 
            // bN
            // 
            this.bN.AddNewItem = null;
            this.bN.AutoSize = false;
            this.bN.BindingSource = this.wwf_deptBindingSource;
            this.bN.CountItem = this.bindingNavigatorCountItem;
            this.bN.DeleteItem = null;
            this.bN.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.toolStripButtonAdd,
            this.toolStripButtonUpdate,
            this.toolStripButtonS,
            this.toolStripButtonDel,
            this.toolStripSeparator3,
            this.toolStripButtonR,
            this.toolStripButtonhelp_code});
            this.bN.Location = new System.Drawing.Point(0, 0);
            this.bN.MoveFirstItem = null;
            this.bN.MoveLastItem = null;
            this.bN.MoveNextItem = null;
            this.bN.MovePreviousItem = null;
            this.bN.Name = "bN";
            this.bN.PositionItem = this.bindingNavigatorPositionItem;
            this.bN.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bN.Size = new System.Drawing.Size(663, 30);
            this.bN.TabIndex = 128;
            this.bN.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(48, 16);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 30);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonAdd
            // 
            this.toolStripButtonAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAdd.Image")));
            this.toolStripButtonAdd.Name = "toolStripButtonAdd";
            this.toolStripButtonAdd.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonAdd.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonAdd.Size = new System.Drawing.Size(100, 27);
            this.toolStripButtonAdd.Text = "新增(&A)  ";
            this.toolStripButtonAdd.Click += new System.EventHandler(this.toolStripButtonAdd_Click);
            // 
            // toolStripButtonUpdate
            // 
            this.toolStripButtonUpdate.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonUpdate.Image")));
            this.toolStripButtonUpdate.Name = "toolStripButtonUpdate";
            this.toolStripButtonUpdate.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonUpdate.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonUpdate.Size = new System.Drawing.Size(100, 27);
            this.toolStripButtonUpdate.Text = "修改(&U)  ";
            this.toolStripButtonUpdate.Click += new System.EventHandler(this.toolStripButtonUpdate_Click);
            // 
            // toolStripButtonS
            // 
            this.toolStripButtonS.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonS.Image")));
            this.toolStripButtonS.Name = "toolStripButtonS";
            this.toolStripButtonS.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonS.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonS.Size = new System.Drawing.Size(100, 27);
            this.toolStripButtonS.Text = "保存(&S)  ";
            this.toolStripButtonS.Click += new System.EventHandler(this.toolStripButtonS_Click);
            // 
            // toolStripButtonDel
            // 
            this.toolStripButtonDel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDel.Image")));
            this.toolStripButtonDel.Name = "toolStripButtonDel";
            this.toolStripButtonDel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonDel.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonDel.Size = new System.Drawing.Size(100, 27);
            this.toolStripButtonDel.Text = "删除(&D)  ";
            this.toolStripButtonDel.Click += new System.EventHandler(this.toolStripButtonDel_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonR
            // 
            this.toolStripButtonR.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonR.Image")));
            this.toolStripButtonR.Name = "toolStripButtonR";
            this.toolStripButtonR.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonR.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonR.Size = new System.Drawing.Size(100, 27);
            this.toolStripButtonR.Text = "刷新(&R)  ";
            this.toolStripButtonR.Click += new System.EventHandler(this.toolStripButtonR_Click);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(200, 30);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 339);
            this.splitter1.TabIndex = 129;
            this.splitter1.TabStop = false;
            // 
            // toolStripButtonhelp_code
            // 
            this.toolStripButtonhelp_code.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonhelp_code.Image")));
            this.toolStripButtonhelp_code.Name = "toolStripButtonhelp_code";
            this.toolStripButtonhelp_code.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonhelp_code.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonhelp_code.Size = new System.Drawing.Size(108, 27);
            this.toolStripButtonhelp_code.Text = "生成助记符";
            this.toolStripButtonhelp_code.Click += new System.EventHandler(this.toolStripButtonhelp_code_Click);
            // 
            // DeptForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(663, 369);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.wwTreeView1);
            this.Controls.Add(this.bN);
            this.Name = "DeptForm";
            this.Text = "DeptForm";
            this.Load += new System.EventHandler(this.DeptForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wwf_deptBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wwfDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).EndInit();
            this.bN.ResumeLayout(false);
            this.bN.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ww.wwf.control.WWControlLib.WWTreeView wwTreeView1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox fremarkTextBox;
        private System.Windows.Forms.BindingSource wwf_deptBindingSource;
        private ww.form.wwf.wwfDataSet wwfDataSet;
        private System.Windows.Forms.CheckBox fuse_flagCheckBox;
        private System.Windows.Forms.TextBox forder_byTextBox;
        private System.Windows.Forms.TextBox fnameTextBox;
        private System.Windows.Forms.TextBox fdept_idTextBox;
        private System.Windows.Forms.ImageList imageListTree;
        protected System.Windows.Forms.BindingNavigator bN;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonAdd;
        private System.Windows.Forms.ToolStripButton toolStripButtonUpdate;
        private System.Windows.Forms.ToolStripButton toolStripButtonS;
        private System.Windows.Forms.ToolStripButton toolStripButtonDel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        protected System.Windows.Forms.ToolStripButton toolStripButtonR;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.ToolStripButton toolStripButtonhelp_code;
    }
}