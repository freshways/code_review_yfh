﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;
using System.Collections;
using ww.form.sqz;
namespace ww.form.wwf
{
    public partial class Frm_人员管理 : SysBaseForm
    {
        DeptBll bll = new DeptBll();
        PersonBLL bllPerson = new PersonBLL();
        DataTable dtPerson = null;
        string strDeptID = "";//上级ID
        public Frm_人员管理()
        {
            InitializeComponent();
        }

        private void PersonForm_Load(object sender, EventArgs e)
        {
            GetTree("-1");
        }
        /// <summary>
        /// 列出系统部门树
        /// </summary>
        public void GetTree(string pid)
        {
            try
            {
                this.wwTreeView1.ZADataTable = this.bll.BllDeptDTuse();
                this.wwTreeView1.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeView1.ZAParentFieldName = "fp_id";//上级字段名称
                this.wwTreeView1.ZADisplayFieldName = "fname";//显示字段名称
                this.wwTreeView1.ZAKeyFieldName = "fdept_id";//主键字段名称
                this.wwTreeView1.ZATreeViewShow();//显示树        
                try
                {
                    this.wwTreeView1.SelectedNode = wwTreeView1.Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void wwTreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                strDeptID = e.Node.Name;
                GetPersonDT(strDeptID);
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 取得人员列表
        /// </summary>
        /// <param name="fdept_id"></param>
        private void GetPersonDT(string fdept_id)
        {
            dtPerson = bllPerson.BllDT(fdept_id);
            bindingSourcePerson.DataSource = dtPerson;
        }

       

        private void DataGridViewObject_DoubleClick(object sender, EventArgs e)
        {
            try
            {
            if (this.strDeptID == "" || this.strDeptID == null)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("请选择部门后重试！");
            }
            else
            {
                string fperson_id = this.DataGridViewObject.CurrentRow.Cells["fperson_id"].Value.ToString();
                PersonAddForm padd = new PersonAddForm(strDeptID, fperson_id, 1);
                padd.ShowDialog();
               
                   
                    GetPersonDT(strDeptID);
                

            }
             }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

       

       

        private void toolStripButtonR_Click(object sender, EventArgs e)
        {
            GetTree("-1");
        }

        private void toolStripButtonUpdate_Click(object sender, EventArgs e)
        {

            if (this.strDeptID == "" || this.strDeptID == null)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("请选择部门后重试！");
            }
            else
            {
                string fperson_id = this.DataGridViewObject.CurrentRow.Cells["fperson_id"].Value.ToString();
                PersonAddForm padd = new PersonAddForm(strDeptID, fperson_id, 1);
                padd.ShowDialog();
                
                    GetPersonDT(strDeptID);
               

            }
        }

        private void toolStripButtonUpdatepass_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.strDeptID == "" || this.strDeptID == null)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("请选择部门后重试！");
                }
                else
                {
                    string fperson_id = this.DataGridViewObject.CurrentRow.Cells["fperson_id"].Value.ToString();
                    PersonPassForm padd = new PersonPassForm(fperson_id);
                    padd.ShowDialog();

                    GetPersonDT(strDeptID);

                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonDel_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.DataGridViewObject.Rows.Count > 0)
                {
                    if (ww.wwf.wwfbll.WWMessage.MessageDialogResult("确认要删除此用户？用户删除后将不可恢复！"))
                    {
                        string pid = this.DataGridViewObject.CurrentRow.Cells["fperson_id"].Value.ToString();
                        if (pid == "admin" || pid == "ADMIN" || pid == "Admin")
                        {
                            ww.wwf.wwfbll.WWMessage.MessageShowWarning("登录名为" + pid + "的用户不可删除！");
                        }
                        else
                        {
                            if (this.bllPerson.BllDelete(pid) == "true")
                            {
                                GetPersonDT(strDeptID);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonAdd_Click(object sender, EventArgs e)
        {
            if (this.strDeptID == "" || this.strDeptID == null)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("请选择部门后重试！");
            }
            else
            {
                PersonAddForm padd = new PersonAddForm(strDeptID, "", 0);
                padd.ShowDialog();
                /*
                //if (dtPerson != null)
                   // dtPerson.Clear();
                DataRow dr = null;
                dr = dtPerson.NewRow();
                string guid = this.bll.Guid();
                dr["fperson_id"] = guid;
                dr["fdept_id"] = this.strDeptID;
                dr["fname"] = "";
                dr["forder_by"] = "0";
                dr["fuse_flag"] = "1";
                dtPerson.Rows.Add(dr);

                //this.bindingSourcePerson.DataSource = dtPerson;
                butSave.Enabled = true;
                 * */
            }
        }

        private void toolStripButtonhelp_code_Click(object sender, EventArgs e)
        {
            try
            {
                string sqlHelp = "";
                IList lissql = new ArrayList();
                string showName = "";
                for (int i = 0; i < DataGridViewObject.Rows.Count; i++)
                {
                    sqlHelp = "";
                    try
                    {
                        sqlHelp = ww.wwf.com.Spell.MakeSpellCodeFirstLetterOnly(DataGridViewObject.Rows[i].Cells["fname"].Value.ToString().Trim());
                    }
                    catch { }
                    try
                    {
                        showName = "(" + DataGridViewObject.Rows[i].Cells["fperson_id"].Value.ToString() + ")" + DataGridViewObject.Rows[i].Cells["fname"].Value.ToString();
                    }
                    catch {
                        showName = DataGridViewObject.Rows[i].Cells["fperson_id"].Value.ToString();
                    }
                    lissql.Add("UPDATE wwf_person SET fhelp_code = '" + sqlHelp + "',fshow_name='" + showName + "' WHERE (fperson_id = '" + DataGridViewObject.Rows[i].Cells["fperson_id"].Value.ToString() + "')");
                }
                this.bllPerson.BllPersonfhelp_codeUpdate(lissql);
                GetPersonDT(strDeptID);
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (this.DataGridViewObject.CurrentRow == null)
            {
                System.Windows.Forms.MessageBox.Show("没有选择任何行！");
            }
            else
            {
                string userid = this.DataGridViewObject.CurrentRow.Cells["fperson_id"].Value.ToString();
                string username = this.DataGridViewObject.CurrentRow.Cells["fname"].Value.ToString();
                FrmUploadSQZ frmUploadSQZ = new FrmUploadSQZ(userid, username);
                frmUploadSQZ.ShowDialog();
            }
        }

        
    }
}
/*
 * 
 *  try
            {
                Cursor = Cursors.WaitCursor;
                this.Validate();
                this.bindingSourceObject.EndEdit();
                this.rulesObject.ZADataSetUpdate(this.m_dsObject);

            }
            catch (Exception ex)
            {
                this.zaSaveLog("", ex.ToString());
            }
            finally
            {
                LoadDataSet();
                Cursor = Cursors.Arrow;

            }
   /// <summary>
        /// 新增一行 
       /// </summary>
       /// <param name="ds"></param>
       /// <param name="strKey"></param>    
        public void zaAddRow(DataSet ds, string strKey)
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt = ds.Tables[0];
            dr = dt.NewRow();
            dr[strKey] = zaStrGUID();
            dt.Rows.Add(dr);
        }
 */