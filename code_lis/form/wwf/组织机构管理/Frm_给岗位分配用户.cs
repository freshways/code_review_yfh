﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;

namespace ww.form.wwf
{
    public partial class Frm_给岗位分配用户 : SysBaseForm
    {
        OrgFuncBLL bll = new OrgFuncBLL();
        PersonBLL bllPerson = new PersonBLL();
        string orgid = "";
        public Frm_给岗位分配用户()
        {
            InitializeComponent();
        }

        private void PersonToPositionForm_Load(object sender, EventArgs e)
        {
            GetPNO();
            GetTreeOrg("-1");
        }
        /// <summary>
        /// 列出系统功能树
        /// </summary>
        public void GetTreeOrg(string pid)
        {
            try
            {
                this.wwTreeView1.ZADataTable = this.bll.BllDTAll();
                this.wwTreeView1.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeView1.ZAParentFieldName = "fp_id";//上级字段名称
                this.wwTreeView1.ZADisplayFieldName = "fname";//显示字段名称
                this.wwTreeView1.ZAKeyFieldName = "forg_id";//主键字段名称
                this.wwTreeView1.ZATreeViewShow();//显示树        
                try
                {
                    this.wwTreeView1.SelectedNode = wwTreeView1.Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        public void GetPNO()
        {
            try
            {
                DataTable dtNO = this.bllPerson.BllDTAllPerson("1");
                this.listBoxNO.DataSource = dtNO;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        public void GetPOK()
        {
            try
            {
                DataTable dtOK = this.bll.BllPersonOK(orgid);
                this.listBoxOK.DataSource = dtOK;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void wwTreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                orgid = e.Node.Name;
                if (this.bll.Bllftype(orgid) == "position")
                {
                    butAdd.Enabled = true;
                    butDel.Enabled = true;
                    GetPOK();


                }
                else
                {
                    GetPOK();
                    butAdd.Enabled = false;
                    butDel.Enabled = false;

                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void butAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string strID = listBoxNO.SelectedValue.ToString();
                this.bll.BllFPPerson(strID, orgid);
                GetPOK();

            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void butDel_Click(object sender, EventArgs e)
        {
            try
            {
                string strID = listBoxOK.SelectedValue.ToString();
                if (strID == "admin" || strID == "Admin" || strID == "ADMIN")
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("用户admin不可取消！");
                    return;
                }
                else
                {
                    if (ww.wwf.wwfbll.WWMessage.MessageDialogResult("确认要取消此用户？"))
                    {
                        this.bll.BllQXPerson(strID);
                        GetPOK();
                    }
                }

            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void butRef_Click(object sender, EventArgs e)
        {
            GetTreeOrg("-1");
        }
    }
}