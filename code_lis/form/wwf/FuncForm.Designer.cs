﻿namespace ww.form.wwf
{
    partial class FuncForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label fremarkLabel;
            System.Windows.Forms.Label fuse_flagLabel;
            System.Windows.Forms.Label forder_byLabel;
            System.Windows.Forms.Label ffunc_winformLabel;
            System.Windows.Forms.Label ficonLabel;
            System.Windows.Forms.Label fnameLabel;
            System.Windows.Forms.Label fhelpLabel;
            System.Windows.Forms.Label ffunc_idLabel;
            System.Windows.Forms.Label fcj_flagLabel;
            System.Windows.Forms.Label fcj_nameLabel;
            System.Windows.Forms.Label fcj_zpLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FuncForm));
            this.wwTreeView1 = new ww.wwf.control.WWControlLib.WWTreeView();
            this.imageListTree = new System.Windows.Forms.ImageList(this.components);
            this.fremarkRichTextBox = new System.Windows.Forms.RichTextBox();
            this.wwf_funcBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wwfDataSet = new ww.form.wwf.wwfDataSet();
            this.fuse_flagCheckBox = new System.Windows.Forms.CheckBox();
            this.forder_byTextBox = new System.Windows.Forms.TextBox();
            this.ffunc_winformTextBox = new System.Windows.Forms.TextBox();
            this.ficonTextBox = new System.Windows.Forms.TextBox();
            this.fnameTextBox = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.fopen_mode_CheckBox = new System.Windows.Forms.CheckBox();
            this.fcj_zpTextBox = new System.Windows.Forms.TextBox();
            this.fcj_nameTextBox = new System.Windows.Forms.TextBox();
            this.fcj_flagCheckBox = new System.Windows.Forms.CheckBox();
            this.ffunc_idTextBox = new System.Windows.Forms.TextBox();
            this.fhelpTextBox = new System.Windows.Forms.TextBox();
            this.bN = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonUpdate = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonS = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonDel = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonR = new System.Windows.Forms.ToolStripButton();
            this.splitter1 = new System.Windows.Forms.Splitter();
            fremarkLabel = new System.Windows.Forms.Label();
            fuse_flagLabel = new System.Windows.Forms.Label();
            forder_byLabel = new System.Windows.Forms.Label();
            ffunc_winformLabel = new System.Windows.Forms.Label();
            ficonLabel = new System.Windows.Forms.Label();
            fnameLabel = new System.Windows.Forms.Label();
            fhelpLabel = new System.Windows.Forms.Label();
            ffunc_idLabel = new System.Windows.Forms.Label();
            fcj_flagLabel = new System.Windows.Forms.Label();
            fcj_nameLabel = new System.Windows.Forms.Label();
            fcj_zpLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.wwf_funcBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wwfDataSet)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).BeginInit();
            this.bN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // fremarkLabel
            // 
            fremarkLabel.AutoSize = true;
            fremarkLabel.Location = new System.Drawing.Point(36, 296);
            fremarkLabel.Name = "fremarkLabel";
            fremarkLabel.Size = new System.Drawing.Size(35, 12);
            fremarkLabel.TabIndex = 106;
            fremarkLabel.Text = "描述:";
            // 
            // fuse_flagLabel
            // 
            fuse_flagLabel.AutoSize = true;
            fuse_flagLabel.Location = new System.Drawing.Point(36, 163);
            fuse_flagLabel.Name = "fuse_flagLabel";
            fuse_flagLabel.Size = new System.Drawing.Size(35, 12);
            fuse_flagLabel.TabIndex = 104;
            fuse_flagLabel.Text = "启用:";
            // 
            // forder_byLabel
            // 
            forder_byLabel.AutoSize = true;
            forder_byLabel.Location = new System.Drawing.Point(36, 133);
            forder_byLabel.Name = "forder_byLabel";
            forder_byLabel.Size = new System.Drawing.Size(35, 12);
            forder_byLabel.TabIndex = 102;
            forder_byLabel.Text = "排序:";
            // 
            // ffunc_winformLabel
            // 
            ffunc_winformLabel.AutoSize = true;
            ffunc_winformLabel.Location = new System.Drawing.Point(12, 74);
            ffunc_winformLabel.Name = "ffunc_winformLabel";
            ffunc_winformLabel.Size = new System.Drawing.Size(59, 12);
            ffunc_winformLabel.TabIndex = 100;
            ffunc_winformLabel.Text = "空间名称:";
            // 
            // ficonLabel
            // 
            ficonLabel.AutoSize = true;
            ficonLabel.Location = new System.Drawing.Point(36, 103);
            ficonLabel.Name = "ficonLabel";
            ficonLabel.Size = new System.Drawing.Size(35, 12);
            ficonLabel.TabIndex = 98;
            ficonLabel.Text = "图标:";
            // 
            // fnameLabel
            // 
            fnameLabel.AutoSize = true;
            fnameLabel.Location = new System.Drawing.Point(36, 45);
            fnameLabel.Name = "fnameLabel";
            fnameLabel.Size = new System.Drawing.Size(35, 12);
            fnameLabel.TabIndex = 97;
            fnameLabel.Text = "名称:";
            // 
            // fhelpLabel
            // 
            fhelpLabel.AutoSize = true;
            fhelpLabel.Location = new System.Drawing.Point(24, 191);
            fhelpLabel.Name = "fhelpLabel";
            fhelpLabel.Size = new System.Drawing.Size(47, 12);
            fhelpLabel.TabIndex = 108;
            fhelpLabel.Text = "帮助页:";
            // 
            // ffunc_idLabel
            // 
            ffunc_idLabel.AutoSize = true;
            ffunc_idLabel.Location = new System.Drawing.Point(36, 18);
            ffunc_idLabel.Name = "ffunc_idLabel";
            ffunc_idLabel.Size = new System.Drawing.Size(35, 12);
            ffunc_idLabel.TabIndex = 109;
            ffunc_idLabel.Text = "编号:";
            // 
            // fcj_flagLabel
            // 
            fcj_flagLabel.AutoSize = true;
            fcj_flagLabel.Location = new System.Drawing.Point(36, 221);
            fcj_flagLabel.Name = "fcj_flagLabel";
            fcj_flagLabel.Size = new System.Drawing.Size(35, 12);
            fcj_flagLabel.TabIndex = 110;
            fcj_flagLabel.Text = "插件:";
            // 
            // fcj_nameLabel
            // 
            fcj_nameLabel.AutoSize = true;
            fcj_nameLabel.Location = new System.Drawing.Point(12, 245);
            fcj_nameLabel.Name = "fcj_nameLabel";
            fcj_nameLabel.Size = new System.Drawing.Size(59, 12);
            fcj_nameLabel.TabIndex = 111;
            fcj_nameLabel.Text = "插件名称:";
            // 
            // fcj_zpLabel
            // 
            fcj_zpLabel.AutoSize = true;
            fcj_zpLabel.Location = new System.Drawing.Point(24, 272);
            fcj_zpLabel.Name = "fcj_zpLabel";
            fcj_zpLabel.Size = new System.Drawing.Size(47, 12);
            fcj_zpLabel.TabIndex = 112;
            fcj_zpLabel.Text = "插件包:";
            // 
            // wwTreeView1
            // 
            this.wwTreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wwTreeView1.Dock = System.Windows.Forms.DockStyle.Left;
            this.wwTreeView1.ImageIndex = 0;
            this.wwTreeView1.ImageList = this.imageListTree;
            this.wwTreeView1.Location = new System.Drawing.Point(0, 30);
            this.wwTreeView1.Name = "wwTreeView1";
            this.wwTreeView1.SelectedImageIndex = 1;
            this.wwTreeView1.Size = new System.Drawing.Size(200, 430);
            this.wwTreeView1.TabIndex = 85;
            this.wwTreeView1.ZADataTable = null;
            this.wwTreeView1.ZADisplayFieldName = "";
            this.wwTreeView1.ZAKeyFieldName = "";
            this.wwTreeView1.ZAParentFieldName = "";
            this.wwTreeView1.ZAToolTipTextName = "";
            this.wwTreeView1.ZATreeViewRootValue = "";
            this.wwTreeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.wwTreeView1_AfterSelect);
            // 
            // imageListTree
            // 
            this.imageListTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTree.ImageStream")));
            this.imageListTree.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTree.Images.SetKeyName(0, "12.gif");
            this.imageListTree.Images.SetKeyName(1, "27.gif");
            // 
            // fremarkRichTextBox
            // 
            this.fremarkRichTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_funcBindingSource, "fremark", true));
            this.fremarkRichTextBox.Location = new System.Drawing.Point(83, 296);
            this.fremarkRichTextBox.Name = "fremarkRichTextBox";
            this.fremarkRichTextBox.Size = new System.Drawing.Size(356, 120);
            this.fremarkRichTextBox.TabIndex = 108;
            this.fremarkRichTextBox.Text = "";
            // 
            // wwf_funcBindingSource
            // 
            this.wwf_funcBindingSource.DataMember = "wwf_func";
            this.wwf_funcBindingSource.DataSource = this.wwfDataSet;
            // 
            // wwfDataSet
            // 
            this.wwfDataSet.DataSetName = "wwfDataSet";
            this.wwfDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // fuse_flagCheckBox
            // 
            this.fuse_flagCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.wwf_funcBindingSource, "fuse_flag", true));
            this.fuse_flagCheckBox.Location = new System.Drawing.Point(83, 157);
            this.fuse_flagCheckBox.Name = "fuse_flagCheckBox";
            this.fuse_flagCheckBox.Size = new System.Drawing.Size(23, 24);
            this.fuse_flagCheckBox.TabIndex = 107;
            // 
            // forder_byTextBox
            // 
            this.forder_byTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_funcBindingSource, "forder_by", true));
            this.forder_byTextBox.Location = new System.Drawing.Point(83, 130);
            this.forder_byTextBox.Name = "forder_byTextBox";
            this.forder_byTextBox.Size = new System.Drawing.Size(266, 21);
            this.forder_byTextBox.TabIndex = 105;
            // 
            // ffunc_winformTextBox
            // 
            this.ffunc_winformTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_funcBindingSource, "ffunc_winform", true));
            this.ffunc_winformTextBox.Location = new System.Drawing.Point(83, 71);
            this.ffunc_winformTextBox.Name = "ffunc_winformTextBox";
            this.ffunc_winformTextBox.Size = new System.Drawing.Size(266, 21);
            this.ffunc_winformTextBox.TabIndex = 103;
            // 
            // ficonTextBox
            // 
            this.ficonTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_funcBindingSource, "ficon", true));
            this.ficonTextBox.Location = new System.Drawing.Point(83, 100);
            this.ficonTextBox.Name = "ficonTextBox";
            this.ficonTextBox.Size = new System.Drawing.Size(266, 21);
            this.ficonTextBox.TabIndex = 101;
            // 
            // fnameTextBox
            // 
            this.fnameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_funcBindingSource, "fname", true));
            this.fnameTextBox.Location = new System.Drawing.Point(83, 42);
            this.fnameTextBox.Name = "fnameTextBox";
            this.fnameTextBox.Size = new System.Drawing.Size(266, 21);
            this.fnameTextBox.TabIndex = 99;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Window;
            this.panel1.Controls.Add(this.fremarkRichTextBox);
            this.panel1.Controls.Add(this.fopen_mode_CheckBox);
            this.panel1.Controls.Add(fcj_zpLabel);
            this.panel1.Controls.Add(this.fcj_zpTextBox);
            this.panel1.Controls.Add(fcj_nameLabel);
            this.panel1.Controls.Add(this.fcj_nameTextBox);
            this.panel1.Controls.Add(fcj_flagLabel);
            this.panel1.Controls.Add(this.fcj_flagCheckBox);
            this.panel1.Controls.Add(ffunc_idLabel);
            this.panel1.Controls.Add(this.ffunc_idTextBox);
            this.panel1.Controls.Add(fhelpLabel);
            this.panel1.Controls.Add(this.fhelpTextBox);
            this.panel1.Controls.Add(this.fnameTextBox);
            this.panel1.Controls.Add(ffunc_winformLabel);
            this.panel1.Controls.Add(this.forder_byTextBox);
            this.panel1.Controls.Add(fremarkLabel);
            this.panel1.Controls.Add(this.ffunc_winformTextBox);
            this.panel1.Controls.Add(forder_byLabel);
            this.panel1.Controls.Add(ficonLabel);
            this.panel1.Controls.Add(fnameLabel);
            this.panel1.Controls.Add(this.fuse_flagCheckBox);
            this.panel1.Controls.Add(fuse_flagLabel);
            this.panel1.Controls.Add(this.ficonTextBox);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(200, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(513, 430);
            this.panel1.TabIndex = 110;
            // 
            // fopen_mode_CheckBox
            // 
            this.fopen_mode_CheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.wwf_funcBindingSource, "fopen_mode", true));
            this.fopen_mode_CheckBox.Location = new System.Drawing.Point(193, 157);
            this.fopen_mode_CheckBox.Name = "fopen_mode_CheckBox";
            this.fopen_mode_CheckBox.Size = new System.Drawing.Size(74, 24);
            this.fopen_mode_CheckBox.TabIndex = 114;
            this.fopen_mode_CheckBox.Text = "弹出窗口";
            // 
            // fcj_zpTextBox
            // 
            this.fcj_zpTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_funcBindingSource, "fcj_zp", true));
            this.fcj_zpTextBox.Location = new System.Drawing.Point(83, 268);
            this.fcj_zpTextBox.Name = "fcj_zpTextBox";
            this.fcj_zpTextBox.Size = new System.Drawing.Size(266, 21);
            this.fcj_zpTextBox.TabIndex = 113;
            this.fcj_zpTextBox.Text = "demo\\\\Form1.dll";
            // 
            // fcj_nameTextBox
            // 
            this.fcj_nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_funcBindingSource, "fcj_name", true));
            this.fcj_nameTextBox.Location = new System.Drawing.Point(83, 241);
            this.fcj_nameTextBox.Name = "fcj_nameTextBox";
            this.fcj_nameTextBox.Size = new System.Drawing.Size(266, 21);
            this.fcj_nameTextBox.TabIndex = 112;
            // 
            // fcj_flagCheckBox
            // 
            this.fcj_flagCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.wwf_funcBindingSource, "fcj_flag", true));
            this.fcj_flagCheckBox.Location = new System.Drawing.Point(83, 215);
            this.fcj_flagCheckBox.Name = "fcj_flagCheckBox";
            this.fcj_flagCheckBox.Size = new System.Drawing.Size(104, 24);
            this.fcj_flagCheckBox.TabIndex = 111;
            // 
            // ffunc_idTextBox
            // 
            this.ffunc_idTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_funcBindingSource, "ffunc_id", true));
            this.ffunc_idTextBox.Location = new System.Drawing.Point(83, 15);
            this.ffunc_idTextBox.Name = "ffunc_idTextBox";
            this.ffunc_idTextBox.Size = new System.Drawing.Size(266, 21);
            this.ffunc_idTextBox.TabIndex = 110;
            // 
            // fhelpTextBox
            // 
            this.fhelpTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_funcBindingSource, "fhelp", true));
            this.fhelpTextBox.Location = new System.Drawing.Point(83, 188);
            this.fhelpTextBox.Name = "fhelpTextBox";
            this.fhelpTextBox.Size = new System.Drawing.Size(266, 21);
            this.fhelpTextBox.TabIndex = 109;
            // 
            // bN
            // 
            this.bN.AddNewItem = null;
            this.bN.AutoSize = false;
            this.bN.BindingSource = this.bindingSource1;
            this.bN.CountItem = this.bindingNavigatorCountItem;
            this.bN.DeleteItem = null;
            this.bN.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.toolStripButtonAdd,
            this.toolStripButtonUpdate,
            this.toolStripButtonS,
            this.toolStripButtonDel,
            this.toolStripSeparator3,
            this.toolStripButtonR});
            this.bN.Location = new System.Drawing.Point(0, 0);
            this.bN.MoveFirstItem = null;
            this.bN.MoveLastItem = null;
            this.bN.MoveNextItem = null;
            this.bN.MovePreviousItem = null;
            this.bN.Name = "bN";
            this.bN.PositionItem = this.bindingNavigatorPositionItem;
            this.bN.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bN.Size = new System.Drawing.Size(713, 30);
            this.bN.TabIndex = 129;
            this.bN.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 27);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 30);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonAdd
            // 
            this.toolStripButtonAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAdd.Image")));
            this.toolStripButtonAdd.Name = "toolStripButtonAdd";
            this.toolStripButtonAdd.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonAdd.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonAdd.Size = new System.Drawing.Size(79, 27);
            this.toolStripButtonAdd.Text = "新增(&A)  ";
            this.toolStripButtonAdd.Click += new System.EventHandler(this.toolStripButtonAdd_Click);
            // 
            // toolStripButtonUpdate
            // 
            this.toolStripButtonUpdate.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonUpdate.Image")));
            this.toolStripButtonUpdate.Name = "toolStripButtonUpdate";
            this.toolStripButtonUpdate.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonUpdate.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonUpdate.Size = new System.Drawing.Size(79, 27);
            this.toolStripButtonUpdate.Text = "修改(&U)  ";
            this.toolStripButtonUpdate.Click += new System.EventHandler(this.toolStripButtonUpdate_Click);
            // 
            // toolStripButtonS
            // 
            this.toolStripButtonS.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonS.Image")));
            this.toolStripButtonS.Name = "toolStripButtonS";
            this.toolStripButtonS.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonS.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonS.Size = new System.Drawing.Size(79, 27);
            this.toolStripButtonS.Text = "保存(&S)  ";
            this.toolStripButtonS.Click += new System.EventHandler(this.toolStripButtonS_Click);
            // 
            // toolStripButtonDel
            // 
            this.toolStripButtonDel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDel.Image")));
            this.toolStripButtonDel.Name = "toolStripButtonDel";
            this.toolStripButtonDel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonDel.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonDel.Size = new System.Drawing.Size(79, 27);
            this.toolStripButtonDel.Text = "删除(&D)  ";
            this.toolStripButtonDel.Click += new System.EventHandler(this.toolStripButtonDel_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonR
            // 
            this.toolStripButtonR.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonR.Image")));
            this.toolStripButtonR.Name = "toolStripButtonR";
            this.toolStripButtonR.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonR.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonR.Size = new System.Drawing.Size(79, 27);
            this.toolStripButtonR.Text = "刷新(&R)  ";
            this.toolStripButtonR.Click += new System.EventHandler(this.toolStripButtonR_Click);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(200, 30);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 430);
            this.splitter1.TabIndex = 130;
            this.splitter1.TabStop = false;
            // 
            // FuncForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(713, 460);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.wwTreeView1);
            this.Controls.Add(this.bN);
            this.Name = "FuncForm";
            this.Text = "FuncForm";
            this.Load += new System.EventHandler(this.FuncForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.wwf_funcBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wwfDataSet)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).EndInit();
            this.bN.ResumeLayout(false);
            this.bN.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ww.wwf.control.WWControlLib.WWTreeView wwTreeView1;
        private ww.form.wwf.wwfDataSet wwfDataSet;
        private System.Windows.Forms.BindingSource wwf_funcBindingSource;
        private System.Windows.Forms.RichTextBox fremarkRichTextBox;
        private System.Windows.Forms.CheckBox fuse_flagCheckBox;
        private System.Windows.Forms.TextBox forder_byTextBox;
        private System.Windows.Forms.TextBox ffunc_winformTextBox;
        private System.Windows.Forms.TextBox ficonTextBox;
        private System.Windows.Forms.TextBox fnameTextBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox fhelpTextBox;
        private System.Windows.Forms.TextBox ffunc_idTextBox;
        private System.Windows.Forms.ImageList imageListTree;
        private System.Windows.Forms.TextBox fcj_zpTextBox;
        private System.Windows.Forms.TextBox fcj_nameTextBox;
        private System.Windows.Forms.CheckBox fcj_flagCheckBox;
        private System.Windows.Forms.CheckBox fopen_mode_CheckBox;
        protected System.Windows.Forms.BindingNavigator bN;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonAdd;
        private System.Windows.Forms.ToolStripButton toolStripButtonUpdate;
        private System.Windows.Forms.ToolStripButton toolStripButtonS;
        private System.Windows.Forms.ToolStripButton toolStripButtonDel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        protected System.Windows.Forms.ToolStripButton toolStripButtonR;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.BindingSource bindingSource1;
    }
}