﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;
namespace ww.form.wwf
{
    public partial class SysForm : SysBaseForm
    {
        SysBLL bll = new SysBLL();
        DataRowView rowCurrent = null;
        public SysForm()
        {
            InitializeComponent();
        }

        private void SysForm_Load(object sender, EventArgs e)
        {
            GetDT();
        }

        private void GetDT()
        {
            try
            {
                this.DataGridViewObject.AutoGenerateColumns = false;
                this.bindingSourceObject.DataSource = this.bll.BllDT();
                this.DataGridViewObject.DataSource = this.bindingSourceObject;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void bindingSourceObject_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                rowCurrent = (DataRowView)bindingSourceObject.Current;//行
                /*
                string fname = "";
                if (rowCurrent != null)
                {
                    fname = drObject["fname"].ToString();
                    MessageBox.Show(fname);
                }*/
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }


        private void toolStripButtonS_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.bindingSourceObject.EndEdit();
                if (rowCurrent != null)
                {
                    if (this.bll.BllUpdate(rowCurrent) > 0)
                        GetDT();
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonR_Click(object sender, EventArgs e)
        {
            GetDT();
        }

        
    }
}