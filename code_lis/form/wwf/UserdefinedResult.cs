﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
namespace ww.form.wwf
{
    public delegate void TextChangedHandlerString(string str1);
    public delegate void TextChangedHandlerDataTable(DataTable dt1);
    /// <summary>
    /// 弹出窗口返回值
    /// </summary>
    public class UserdefinedResult
    {
        public event TextChangedHandlerString TextChangedString;
        public event TextChangedHandlerDataTable TextChangedDataTable;
        public void ChangeTextString(string str1)
        {
            if (TextChangedString != null)
                TextChangedString(str1);
        }
        public void ChangeTextDataTable(DataTable dt1)
        {
            if (TextChangedDataTable != null)
                TextChangedDataTable(dt1);
        }
    }
}
