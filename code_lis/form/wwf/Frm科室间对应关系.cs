﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ww.form.wwf
{
    public partial class Frm科室间对应关系 : ww.form.wwf.SysBaseForm
    {
        public Frm科室间对应关系()
        {
            InitializeComponent();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            ShowList();
        }

        void ShowList()
        {
            try
            {
                string type = cboType.Text;
                string dept = txtDept.Text.Trim();

                string sql = @"  select top 200 AA.ID,AA.类型,AA.源科室,AA.目标科室,BB.科室名称 源科室名称, CC.科室名称 目标科室名称 
  from dbo.GY科室转换关系 AA
	LEFT OUTER JOIN GY科室设置 BB ON AA.源科室=BB.科室编码
	LEFT OUTER JOIN GY科室设置 CC ON AA.目标科室=CC.科室编码
  WHERE BB.是否禁用=0 AND CC.是否禁用=0 AND AA.类型 LIKE '%'+@type+'%' 
		and (AA.源科室 LIKE '%'+@dept+'%' OR AA.目标科室 LIKE '%'+@dept+'%' 
				OR BB.科室名称 LIKE '%'+@dept+'%' OR CC.科室名称 LIKE '%'+@dept+'%')";
                List<SqlParameter> paramlist = new List<SqlParameter>();
                paramlist.Add(new SqlParameter("@type", type));
                paramlist.Add(new SqlParameter("@dept", dept));

                DataTable data = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strCHisDBConn, sql, paramlist.ToArray()).Tables[0];
                dataGridView1.DataSource = data;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ShowEditForm(true, "");
        }

        void ShowEditForm(bool isAdd, string id)
        {
            try
            {
                Frm科室对应设置 frm = new Frm科室对应设置(isAdd, id);
                if(frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    ShowList();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if(dataGridView1.CurrentRow==null)
            {
                MessageBox.Show("请先查询出需要修改的科室对应关系。");
                return;
            }

            string id = dataGridView1.CurrentRow.Cells["ColumnID"].Value.ToString();
            ShowEditForm(false, id);
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow == null)
            {
                MessageBox.Show("请先查询出需要修改的科室对应关系。");
                return;
            }

            if(MessageBox.Show("确定要删除选中的记录吗？", "确认", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    string id = dataGridView1.CurrentRow.Cells["ColumnID"].Value.ToString();
                    string sqldel = "delete from dbo.GY科室转换关系 where ID=@id";
                    List<SqlParameter> paramlist = new List<SqlParameter>();
                    paramlist.Add(new SqlParameter("@id", id));
                    int ret = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strCHisDBConn, sqldel, paramlist.ToArray());
                    if(ret > 0)
                    {
                        MessageBox.Show("删除成功。");
                        ShowList();
                    }
                    else
                    {
                        MessageBox.Show("删除失败。");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Frm科室间对应关系_Load(object sender, EventArgs e)
        {
            ShowList();
        }
    }
}
