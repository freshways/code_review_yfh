﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;

namespace ww.form.wwf
{
    public partial class FuncForm : SysBaseForm
    {
        FuncBLL bll = new FuncBLL();
        DataTable dtFunc = null;
        string strFuncID = "";//上级ID
        public FuncForm()
        {
            InitializeComponent();
        }

        private void FuncForm_Load(object sender, EventArgs e)
        {
            GetTree("-1");
        }
        /// <summary>
        /// 列出系统功能树
        /// </summary>
        public void GetTree(string pid)
        {
            try
            {
                DataTable dtF = this.bll.BllFuncDT();
                bindingSource1.DataSource = dtF;
                this.wwTreeView1.ZADataTable = dtF;
                this.wwTreeView1.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeView1.ZAParentFieldName = "fp_id";//上级字段名称
                this.wwTreeView1.ZADisplayFieldName = "fname";//显示字段名称
                this.wwTreeView1.ZAKeyFieldName = "ffunc_id";//主键字段名称
                this.wwTreeView1.ZATreeViewShow();//显示树        
                try
                {
                    this.wwTreeView1.SelectedNode = wwTreeView1.Nodes[0].Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void wwTreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                strFuncID = e.Node.Name.ToString();
                GetFuncDec();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void GetFuncDec()
        {
            dtFunc = this.bll.BllFuncDTByID(strFuncID);
            wwf_funcBindingSource.DataSource = dtFunc;
        }

       



        private void toolStripButtonAdd_Click(object sender, EventArgs e)
        {
            if (this.strFuncID == "" || this.strFuncID == null)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("请选择上级功能后重试！");
            }
            else
            {
                if (dtFunc != null)
                    dtFunc.Clear();
                DataRow dr = null;
                dr = dtFunc.NewRow();
                string guid = this.bll.DbGuid();
                dr["ffunc_id"] = guid;
                dr["fp_id"] = this.strFuncID;
                dr["fname"] = "名称" + guid;
                dr["ffunc_winform"] = "功能ww.form.wwf.FuncForm";
                dr["fuse_flag"] = "1";
                dr["fcj_flag"] = "0";
                dtFunc.Rows.Add(dr);

                this.wwf_funcBindingSource.DataSource = dtFunc;
                toolStripButtonS.Enabled = true;
            }
        }

        private void toolStripButtonS_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.wwf_funcBindingSource.EndEdit();
                if (strFuncID == "" || strFuncID == null)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("未选择可修改的功能！");
                    return;
                }
                if (this.dtFunc != null)
                {
                    if (this.bll.BllAdd(this.dtFunc) > 0)
                        GetFuncDec();
                    toolStripButtonS.Enabled = false;
                }
            }
            catch
            {

                ww.wwf.wwfbll.WWMessage.MessageShowError("请新增功能后再保存！");
            }
        }

        private void toolStripButtonR_Click(object sender, EventArgs e)
        {
            GetTree("-1");
        }

        private void toolStripButtonUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.wwf_funcBindingSource.EndEdit();
                if (strFuncID == "" || strFuncID == null)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("未选择可修改的功能！");
                    return;
                }
                if (this.dtFunc != null)
                {
                    if (this.bll.BllUpdate(this.dtFunc) > 0)
                        GetFuncDec();
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonDel_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.strFuncID == "" || this.strFuncID == null)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("请选择要删除的功能后重试！");
                }
                else
                {
                    if (this.bll.BllChildExists(this.strFuncID))
                    {
                        ww.wwf.wwfbll.WWMessage.MessageShowWarning("此功能存在下级，请先删除其下级后重试！");
                    }
                    else
                    {
                        if (ww.wwf.wwfbll.WWMessage.MessageDialogResult("确认要删除此功能？功能删除后将不可恢复！"))
                        {
                            if (this.bll.BllDelete(this.strFuncID))
                            {
                                GetFuncDec();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

      
    }
}
/*
 * 
 *  try
            {
                Cursor = Cursors.WaitCursor;
                this.Validate();
                this.bindingSourceObject.EndEdit();
                this.rulesObject.ZADataSetUpdate(this.m_dsObject);

            }
            catch (Exception ex)
            {
                this.zaSaveLog("", ex.ToString());
            }
            finally
            {
                LoadDataSet();
                Cursor = Cursors.Arrow;

            }
   /// <summary>
        /// 新增一行 
       /// </summary>
       /// <param name="ds"></param>
       /// <param name="strKey"></param>    
        public void zaAddRow(DataSet ds, string strKey)
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt = ds.Tables[0];
            dr = dt.NewRow();
            dr[strKey] = zaStrGUID();
            dt.Rows.Add(dr);
        }
 */