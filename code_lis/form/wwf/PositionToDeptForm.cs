﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;

namespace ww.form.wwf
{
    public partial class PositionToDeptForm : SysBaseForm
    {
        OrgFuncBLL bll = new OrgFuncBLL();
        string orgid = "";

        public PositionToDeptForm()
        {
            InitializeComponent();
        }

        private void PositionToDeptForm_Load(object sender, EventArgs e)
        {
            GetPNO();
            GetTreeOrg("-1");
        }
        /// <summary>
        /// 列出系统功能树
        /// </summary>
        public void GetTreeOrg(string pid)
        {
            try
            {
                this.wwTreeView1.ZADataTable = this.bll.BllDTAll();
                this.wwTreeView1.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeView1.ZAParentFieldName = "fp_id";//上级字段名称
                this.wwTreeView1.ZADisplayFieldName = "fname";//显示字段名称
                this.wwTreeView1.ZAKeyFieldName = "forg_id";//主键字段名称
                this.wwTreeView1.ZATreeViewShow();//显示树        
                try
                {
                    this.wwTreeView1.SelectedNode = wwTreeView1.Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        public void GetPNO()
        {
            try
            {
                DataTable dtOK = this.bll.BllPositionNO();
                this.listBoxNO.DataSource = dtOK;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        public void GetPOK(string forg_id)
        {
            try
            {
                DataTable dtOK = this.bll.BllPositionOK(forg_id);
                this.listBoxOK.DataSource = dtOK;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void wwTreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                orgid = e.Node.Name;
                //MessageBox.Show(this.bll.Bllftype(orgid));
                if (this.bll.Bllftype(orgid) == "dept" || this.bll.Bllftype(orgid) == "Dept")
                {
                    butAdd.Enabled = true;
                    butDel.Enabled = true;
                    GetPOK(orgid);
                    //GetTreeFuncOK("-1");

                }
                else
                {
                    if (this.listBoxOK!=null)
                    this.listBoxOK.ClearSelected();

                    butAdd.Enabled = false;
                    butDel.Enabled = false;

                    // wwTreeView3.Nodes.Clear();
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void butAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string strID = listBoxNO.SelectedValue.ToString();
                this.bll.BllFP(strID, orgid);               
                GetPOK(orgid);
                
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void butDel_Click(object sender, EventArgs e)
        {
            try
            {
                string strID = listBoxOK.SelectedValue.ToString();
                if (this.bll.BllQXPersonOK(strID))
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("此岗位下已经分配用户，请先取消其下用户后重试！");
                }
                else
                {
                    if (ww.wwf.wwfbll.WWMessage.MessageDialogResult("确认要取消此岗位？"))
                    {
                        this.bll.BllQX(strID);
                        GetPOK(orgid);
                    }
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void butRef_Click(object sender, EventArgs e)
        {
            GetTreeOrg("-1");
        }
    }
}