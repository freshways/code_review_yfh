﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;

namespace ww.form.wwf
{
    public partial class OrgFuncForm : SysBaseForm
    {
        OrgFuncBLL bll = new OrgFuncBLL();
        FuncBLL bllFunc = new FuncBLL();
        string orgid = "";
        string strOKFuncID = "";
        public OrgFuncForm()
        {
            InitializeComponent();
        }

        private void FuncForm_Load(object sender, EventArgs e)
        {
            GetTreeOrg("-1");
            GetTreeFuncAll("-1");
            
        }
        /// <summary>
        /// 列出系统功能树
        /// </summary>
        public void GetTreeOrg(string pid)
        {
            try
            {
                this.wwTreeView1.ZADataTable = this.bll.BllDTAll();
                this.wwTreeView1.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeView1.ZAParentFieldName = "fp_id";//上级字段名称
                this.wwTreeView1.ZADisplayFieldName = "fname";//显示字段名称
                this.wwTreeView1.ZAKeyFieldName = "forg_id";//主键字段名称
                this.wwTreeView1.ZATreeViewShow();//显示树        
                try
                {
                    this.wwTreeView1.SelectedNode = wwTreeView1.Nodes[0].Nodes[0].Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 列出系统功能树
        /// </summary>
        public void GetTreeFuncAll(string pid)
        {
            try
            {
                this.wwTreeView2.ZADataTable = this.bllFunc.BllFuncDT();
                this.wwTreeView2.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeView2.ZAParentFieldName = "fp_id";//上级字段名称
                this.wwTreeView2.ZADisplayFieldName = "fname";//显示字段名称
                this.wwTreeView2.ZAKeyFieldName = "ffunc_id";//主键字段名称
                this.wwTreeView2.ZATreeViewShow();//显示树        
                try
                {
                    this.wwTreeView2.SelectedNode = wwTreeView2.Nodes[0].Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 列出系统功能树
        /// </summary>
        public void GetTreeFuncOK(string pid)
        {
            try
            {
                this.wwTreeView3.ZADataTable = this.bll.BllFuncOK(orgid);

                this.wwTreeView3.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeView3.ZAParentFieldName = "fp_id";//上级字段名称
                this.wwTreeView3.ZADisplayFieldName = "fname";//显示字段名称
                this.wwTreeView3.ZAKeyFieldName = "ffunc_id";//主键字段名称
                this.wwTreeView3.ZATreeViewShow();//显示树        
                try
                {
                    this.wwTreeView3.SelectedNode = wwTreeView3.Nodes[0].Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void wwTreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                 orgid = e.Node.Name;
                //MessageBox.Show(this.bll.Bllftype(orgid));
                if (this.bll.Bllftype(orgid) == "person")
                {
                     butAdd.Enabled = true;
                     butDel.Enabled = true;

                     GetTreeFuncOK("-1");

                }
                else {
                     butAdd.Enabled = false;
                     butDel.Enabled = false;

                     wwTreeView3.Nodes.Clear();
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void wwTreeView3_AfterSelect(object sender, TreeViewEventArgs e)
        {
            strOKFuncID = e.Node.Name;
        }

        private void butDel_Click(object sender, EventArgs e)
        {
            try
            {
                //MessageBox.Show(strOKFuncID);
               // MessageBox.Show(orgid);
                if (strOKFuncID == "" || strOKFuncID == null || orgid == "" || orgid == null)
                {

                }
                else
                {
                   //this.textBox1.Text = strOKFuncID;
                   // if (this.bllFunc.BllChildExists(strOKFuncID))
                    //{
                     //   ww.wwf.wwfbll.WWMessage.MessageShowWarning("此功能存在下级，请先取消其下级后重试！");
                   // }
                    //else
                    //{
                        this.bll.BllOkFuncDelete(orgid, strOKFuncID);
                        GetTreeFuncOK("-1");
                    //}
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void butRef_Click(object sender, EventArgs e)
        {
            GetTreeFuncOK("-1");
        }


        private void wwTreeView2_AfterCheck(object sender, TreeViewEventArgs e)
        {
            try
            {
                string selectFuncName = e.Node.Name;
                if (selectFuncName != "0")
                {
                    e.Node.Parent.Checked = true;
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void butAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (orgid == "" || orgid == null) { }
                else
                {
                    this.bll.BllOkFuncInsert(orgid, "0");
                    CheckAllChildNodes(this.wwTreeView2.Nodes[0], true);
                    GetTreeFuncOK("-1");
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        //选中子节点 
        public void CheckAllChildNodes(TreeNode treeNode, bool nodeChecked)
        {
            foreach (TreeNode node in treeNode.Nodes)
            {
                if (node.Checked)
                {
                    this.bll.BllOkFuncInsert(orgid, node.Name.ToString());
                }
                if (node.Nodes.Count > 0)
                {
                    this.CheckAllChildNodes(node, nodeChecked);
                   
                }
            }
        }
        
    }
}
/*
 * 
 *  try
            {
                Cursor = Cursors.WaitCursor;
                this.Validate();
                this.bindingSourceObject.EndEdit();
                this.rulesObject.ZADataSetUpdate(this.m_dsObject);

            }
            catch (Exception ex)
            {
                this.zaSaveLog("", ex.ToString());
            }
            finally
            {
                LoadDataSet();
                Cursor = Cursors.Arrow;

            }
   /// <summary>
        /// 新增一行 
       /// </summary>
       /// <param name="ds"></param>
       /// <param name="strKey"></param>    
        public void zaAddRow(DataSet ds, string strKey)
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt = ds.Tables[0];
            dr = dt.NewRow();
            dr[strKey] = zaStrGUID();
            dt.Rows.Add(dr);
        }
 */