﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;
namespace ww.form.wwf
{
    public partial class NumForm : SysBaseForm
    {
        NumBLL bll = new NumBLL();
        public NumForm()
        {
            InitializeComponent();
        }

        private void NumForm_Load(object sender, EventArgs e)
        {
            try
            {
                bindingSource1.DataSource = this.bll.BllDT();
                this.DataGridViewObject.AutoGenerateColumns = false;
                this.DataGridViewObject.DataSource = bindingSource1;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
    }
}