﻿namespace ww.form.wwf
{
    partial class UserdefinedTypeDemoForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserdefinedTypeDemoForm));
            this.panelTool = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.butExcel = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.wwTreeView1 = new ww.wwf.control.WWControlLib.WWTreeView();
            this.imageListTree = new System.Windows.Forms.ImageList(this.components);
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.contextMenuStripReport = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.列表设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.panelTool.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.contextMenuStripReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // panelTool
            // 
            this.panelTool.Controls.Add(this.button1);
            this.panelTool.Controls.Add(this.butExcel);
            this.panelTool.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelTool.Location = new System.Drawing.Point(3, 418);
            this.panelTool.Name = "panelTool";
            this.panelTool.Size = new System.Drawing.Size(865, 43);
            this.panelTool.TabIndex = 116;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(209, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // butExcel
            // 
            this.butExcel.Image = ((System.Drawing.Image)(resources.GetObject("butExcel.Image")));
            this.butExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butExcel.Location = new System.Drawing.Point(11, 12);
            this.butExcel.Name = "butExcel";
            this.butExcel.Size = new System.Drawing.Size(110, 23);
            this.butExcel.TabIndex = 5;
            this.butExcel.Text = "   导出Excel(&O)";
            this.butExcel.UseVisualStyleBackColor = true;
            this.butExcel.Click += new System.EventHandler(this.butExcel_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.wwTreeView1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(3, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 418);
            this.groupBox1.TabIndex = 126;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "表";
            // 
            // wwTreeView1
            // 
            this.wwTreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wwTreeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wwTreeView1.ImageIndex = 0;
            this.wwTreeView1.ImageList = this.imageListTree;
            this.wwTreeView1.Location = new System.Drawing.Point(3, 17);
            this.wwTreeView1.Name = "wwTreeView1";
            this.wwTreeView1.SelectedImageIndex = 1;
            this.wwTreeView1.Size = new System.Drawing.Size(194, 398);
            this.wwTreeView1.TabIndex = 118;
            this.wwTreeView1.ZADataTable = null;
            this.wwTreeView1.ZADisplayFieldName = "";
            this.wwTreeView1.ZAKeyFieldName = "";
            this.wwTreeView1.ZAParentFieldName = "";
            this.wwTreeView1.ZAToolTipTextName = "";
            this.wwTreeView1.ZATreeViewRootValue = "";
            this.wwTreeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.wwTreeView1_AfterSelect);
            // 
            // imageListTree
            // 
            this.imageListTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTree.ImageStream")));
            this.imageListTree.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTree.Images.SetKeyName(0, "12.gif");
            this.imageListTree.Images.SetKeyName(1, "27.gif");
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 461);
            this.splitter1.TabIndex = 128;
            this.splitter1.TabStop = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.ContextMenuStrip = this.contextMenuStripReport;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 21;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(865, 418);
            this.dataGridView1.TabIndex = 129;
            // 
            // contextMenuStripReport
            // 
            this.contextMenuStripReport.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripSeparator1,
            this.列表设置ToolStripMenuItem});
            this.contextMenuStripReport.Name = "contextMenuStripReport";
            this.contextMenuStripReport.Size = new System.Drawing.Size(125, 54);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem1.Text = "导出Excel";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(121, 6);
            // 
            // 列表设置ToolStripMenuItem
            // 
            this.列表设置ToolStripMenuItem.Name = "列表设置ToolStripMenuItem";
            this.列表设置ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.列表设置ToolStripMenuItem.Text = "列表设置";
            this.列表设置ToolStripMenuItem.Click += new System.EventHandler(this.报告列表设置ToolStripMenuItem_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(229, 275);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(599, 149);
            this.textBox1.TabIndex = 130;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(214, 138);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowTemplate.Height = 23;
            this.dataGridView2.Size = new System.Drawing.Size(632, 150);
            this.dataGridView2.TabIndex = 131;
            // 
            // UserdefinedTypeDemoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(868, 461);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panelTool);
            this.Controls.Add(this.splitter1);
            this.Name = "UserdefinedTypeDemoForm";
            this.Text = "用户自定义表演示";
            this.Load += new System.EventHandler(this.UserdefinedTypeDemoForm_Load);
            this.panelTool.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.contextMenuStripReport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelTool;
        private System.Windows.Forms.Button butExcel;
        private System.Windows.Forms.GroupBox groupBox1;
        private ww.wwf.control.WWControlLib.WWTreeView wwTreeView1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ImageList imageListTree;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripReport;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 列表设置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView2;
    }
}