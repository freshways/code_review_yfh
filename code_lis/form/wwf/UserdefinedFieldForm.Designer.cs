﻿namespace ww.form.wwf
{
    partial class UserdefinedFieldForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label fremarkLabel;
            System.Windows.Forms.Label fvalue_typeLabel;
            System.Windows.Forms.Label fvalue_contentLabel;
            System.Windows.Forms.Label fdata_typeLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserdefinedFieldForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.wwTreeView1 = new ww.wwf.control.WWControlLib.WWTreeView();
            this.imageListTree = new System.Windows.Forms.ImageList(this.components);
            this.panelTool = new System.Windows.Forms.Panel();
            this.buttonExcel = new System.Windows.Forms.Button();
            this.buttonDel = new System.Windows.Forms.Button();
            this.butRef = new System.Windows.Forms.Button();
            this.butSave = new System.Windows.Forms.Button();
            this.butAdd = new System.Windows.Forms.Button();
            this.DataGridViewObject = new System.Windows.Forms.DataGridView();
            this.wwf_columnsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wwfDataSet = new ww.form.wwf.wwfDataSet();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.fdata_typeComboBox = new System.Windows.Forms.ComboBox();
            this.fvalue_contentRichTextBox = new System.Windows.Forms.RichTextBox();
            this.fvalue_typeComboBox = new System.Windows.Forms.ComboBox();
            this.fremarkRichTextBox = new System.Windows.Forms.RichTextBox();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.forder_by = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fread_flage = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fshow_flag = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fsum_flag = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fxsws = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fshow_width = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fvaluetypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fdata_type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fvalue_content = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fgroup_flag = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fcolumns_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fdefault = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fremark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fold_columns = new System.Windows.Forms.DataGridViewTextBoxColumn();
            fremarkLabel = new System.Windows.Forms.Label();
            fvalue_typeLabel = new System.Windows.Forms.Label();
            fvalue_contentLabel = new System.Windows.Forms.Label();
            fdata_typeLabel = new System.Windows.Forms.Label();
            this.panelTool.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wwf_columnsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wwfDataSet)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // fremarkLabel
            // 
            fremarkLabel.AutoSize = true;
            fremarkLabel.Location = new System.Drawing.Point(19, 293);
            fremarkLabel.Name = "fremarkLabel";
            fremarkLabel.Size = new System.Drawing.Size(35, 12);
            fremarkLabel.TabIndex = 7;
            fremarkLabel.Text = "备注:";
            // 
            // fvalue_typeLabel
            // 
            fvalue_typeLabel.AutoSize = true;
            fvalue_typeLabel.Location = new System.Drawing.Point(19, 28);
            fvalue_typeLabel.Name = "fvalue_typeLabel";
            fvalue_typeLabel.Size = new System.Drawing.Size(71, 12);
            fvalue_typeLabel.TabIndex = 8;
            fvalue_typeLabel.Text = "值业务类型:";
            // 
            // fvalue_contentLabel
            // 
            fvalue_contentLabel.AutoSize = true;
            fvalue_contentLabel.Location = new System.Drawing.Point(19, 106);
            fvalue_contentLabel.Name = "fvalue_contentLabel";
            fvalue_contentLabel.Size = new System.Drawing.Size(47, 12);
            fvalue_contentLabel.TabIndex = 9;
            fvalue_contentLabel.Text = "值内容:";
            // 
            // fdata_typeLabel
            // 
            fdata_typeLabel.AutoSize = true;
            fdata_typeLabel.Location = new System.Drawing.Point(19, 55);
            fdata_typeLabel.Name = "fdata_typeLabel";
            fdata_typeLabel.Size = new System.Drawing.Size(71, 12);
            fdata_typeLabel.TabIndex = 10;
            fdata_typeLabel.Text = "值数据类型:";
            // 
            // wwTreeView1
            // 
            this.wwTreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wwTreeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wwTreeView1.ImageIndex = 0;
            this.wwTreeView1.ImageList = this.imageListTree;
            this.wwTreeView1.Location = new System.Drawing.Point(3, 17);
            this.wwTreeView1.Name = "wwTreeView1";
            this.wwTreeView1.SelectedImageIndex = 1;
            this.wwTreeView1.Size = new System.Drawing.Size(171, 400);
            this.wwTreeView1.TabIndex = 118;
            this.wwTreeView1.ZADataTable = null;
            this.wwTreeView1.ZADisplayFieldName = "";
            this.wwTreeView1.ZAKeyFieldName = "";
            this.wwTreeView1.ZAParentFieldName = "";
            this.wwTreeView1.ZAToolTipTextName = "";
            this.wwTreeView1.ZATreeViewRootValue = "";
            this.wwTreeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.wwTreeView1_AfterSelect);
            // 
            // imageListTree
            // 
            this.imageListTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTree.ImageStream")));
            this.imageListTree.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTree.Images.SetKeyName(0, "12.gif");
            this.imageListTree.Images.SetKeyName(1, "27.gif");
            // 
            // panelTool
            // 
            this.panelTool.Controls.Add(this.buttonExcel);
            this.panelTool.Controls.Add(this.buttonDel);
            this.panelTool.Controls.Add(this.butRef);
            this.panelTool.Controls.Add(this.butSave);
            this.panelTool.Controls.Add(this.butAdd);
            this.panelTool.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelTool.Location = new System.Drawing.Point(0, 420);
            this.panelTool.Name = "panelTool";
            this.panelTool.Size = new System.Drawing.Size(791, 27);
            this.panelTool.TabIndex = 119;
            // 
            // buttonExcel
            // 
            this.buttonExcel.Image = ((System.Drawing.Image)(resources.GetObject("buttonExcel.Image")));
            this.buttonExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonExcel.Location = new System.Drawing.Point(355, 3);
            this.buttonExcel.Name = "buttonExcel";
            this.buttonExcel.Size = new System.Drawing.Size(80, 23);
            this.buttonExcel.TabIndex = 7;
            this.buttonExcel.Text = "   导出(&E)";
            this.buttonExcel.UseVisualStyleBackColor = true;
            this.buttonExcel.Click += new System.EventHandler(this.buttonExcel_Click);
            // 
            // buttonDel
            // 
            this.buttonDel.Image = ((System.Drawing.Image)(resources.GetObject("buttonDel.Image")));
            this.buttonDel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonDel.Location = new System.Drawing.Point(269, 3);
            this.buttonDel.Name = "buttonDel";
            this.buttonDel.Size = new System.Drawing.Size(80, 23);
            this.buttonDel.TabIndex = 6;
            this.buttonDel.Text = "   删除(&D)";
            this.buttonDel.UseVisualStyleBackColor = true;
            this.buttonDel.Click += new System.EventHandler(this.buttonDel_Click);
            // 
            // butRef
            // 
            this.butRef.Image = ((System.Drawing.Image)(resources.GetObject("butRef.Image")));
            this.butRef.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butRef.Location = new System.Drawing.Point(183, 3);
            this.butRef.Name = "butRef";
            this.butRef.Size = new System.Drawing.Size(80, 23);
            this.butRef.TabIndex = 5;
            this.butRef.Text = "   刷新(&R)";
            this.butRef.UseVisualStyleBackColor = true;
            this.butRef.Click += new System.EventHandler(this.butRef_Click);
            // 
            // butSave
            // 
            this.butSave.Image = ((System.Drawing.Image)(resources.GetObject("butSave.Image")));
            this.butSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butSave.Location = new System.Drawing.Point(97, 3);
            this.butSave.Name = "butSave";
            this.butSave.Size = new System.Drawing.Size(80, 23);
            this.butSave.TabIndex = 2;
            this.butSave.Text = "   保存(&S)";
            this.butSave.UseVisualStyleBackColor = true;
            this.butSave.Click += new System.EventHandler(this.butSave_Click);
            // 
            // butAdd
            // 
            this.butAdd.Image = ((System.Drawing.Image)(resources.GetObject("butAdd.Image")));
            this.butAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butAdd.Location = new System.Drawing.Point(11, 3);
            this.butAdd.Name = "butAdd";
            this.butAdd.Size = new System.Drawing.Size(80, 23);
            this.butAdd.TabIndex = 0;
            this.butAdd.Text = "   新增(&A)";
            this.butAdd.UseVisualStyleBackColor = true;
            this.butAdd.Click += new System.EventHandler(this.butAdd_Click);
            // 
            // DataGridViewObject
            // 
            this.DataGridViewObject.AllowUserToAddRows = false;
            this.DataGridViewObject.AllowUserToOrderColumns = true;
            this.DataGridViewObject.AllowUserToResizeRows = false;
            this.DataGridViewObject.AutoGenerateColumns = false;
            this.DataGridViewObject.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DataGridViewObject.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridViewObject.ColumnHeadersHeight = 21;
            this.DataGridViewObject.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.forder_by,
            this.fcode,
            this.fname,
            this.fread_flage,
            this.fshow_flag,
            this.fsum_flag,
            this.fxsws,
            this.fshow_width,
            this.fvaluetypeDataGridViewTextBoxColumn,
            this.fdata_type,
            this.fvalue_content,
            this.fgroup_flag,
            this.fcolumns_id,
            this.fdefault,
            this.fremark,
            this.fold_columns});
            this.DataGridViewObject.DataSource = this.wwf_columnsBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridViewObject.DefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridViewObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridViewObject.EnableHeadersVisualStyles = false;
            this.DataGridViewObject.Location = new System.Drawing.Point(3, 17);
            this.DataGridViewObject.Name = "DataGridViewObject";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridViewObject.RowHeadersVisible = false;
            this.DataGridViewObject.RowHeadersWidth = 35;
            this.DataGridViewObject.RowTemplate.Height = 23;
            this.DataGridViewObject.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewObject.Size = new System.Drawing.Size(329, 400);
            this.DataGridViewObject.TabIndex = 124;
            this.DataGridViewObject.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataGridViewObject_DataError);
            // 
            // wwf_columnsBindingSource
            // 
            this.wwf_columnsBindingSource.DataMember = "wwf_columns";
            this.wwf_columnsBindingSource.DataSource = this.wwfDataSet;
            // 
            // wwfDataSet
            // 
            this.wwfDataSet.DataSetName = "wwfDataSet";
            this.wwfDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.wwTreeView1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(177, 420);
            this.groupBox1.TabIndex = 125;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "表";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.DataGridViewObject);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(177, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(335, 420);
            this.groupBox2.TabIndex = 126;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "字段";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(fdata_typeLabel);
            this.groupBox3.Controls.Add(this.fdata_typeComboBox);
            this.groupBox3.Controls.Add(fvalue_contentLabel);
            this.groupBox3.Controls.Add(this.fvalue_contentRichTextBox);
            this.groupBox3.Controls.Add(fvalue_typeLabel);
            this.groupBox3.Controls.Add(this.fvalue_typeComboBox);
            this.groupBox3.Controls.Add(fremarkLabel);
            this.groupBox3.Controls.Add(this.fremarkRichTextBox);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox3.Location = new System.Drawing.Point(512, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(279, 420);
            this.groupBox3.TabIndex = 127;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "属性";
            // 
            // fdata_typeComboBox
            // 
            this.fdata_typeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_columnsBindingSource, "fdata_type", true));
            this.fdata_typeComboBox.FormattingEnabled = true;
            this.fdata_typeComboBox.Items.AddRange(new object[] {
            "字符型",
            "数字型"});
            this.fdata_typeComboBox.Location = new System.Drawing.Point(96, 52);
            this.fdata_typeComboBox.Name = "fdata_typeComboBox";
            this.fdata_typeComboBox.Size = new System.Drawing.Size(121, 20);
            this.fdata_typeComboBox.TabIndex = 11;
            // 
            // fvalue_contentRichTextBox
            // 
            this.fvalue_contentRichTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_columnsBindingSource, "fvalue_content", true));
            this.fvalue_contentRichTextBox.Location = new System.Drawing.Point(21, 121);
            this.fvalue_contentRichTextBox.Name = "fvalue_contentRichTextBox";
            this.fvalue_contentRichTextBox.Size = new System.Drawing.Size(241, 161);
            this.fvalue_contentRichTextBox.TabIndex = 10;
            this.fvalue_contentRichTextBox.Text = "";
            // 
            // fvalue_typeComboBox
            // 
            this.fvalue_typeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_columnsBindingSource, "fvalue_type", true));
            this.fvalue_typeComboBox.FormattingEnabled = true;
            this.fvalue_typeComboBox.Items.AddRange(new object[] {
            "原始值",
            "科目值",
            "公式值",
            "合计值",
            "预定义",
            "主键"});
            this.fvalue_typeComboBox.Location = new System.Drawing.Point(96, 20);
            this.fvalue_typeComboBox.Name = "fvalue_typeComboBox";
            this.fvalue_typeComboBox.Size = new System.Drawing.Size(121, 20);
            this.fvalue_typeComboBox.TabIndex = 9;
            // 
            // fremarkRichTextBox
            // 
            this.fremarkRichTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_columnsBindingSource, "fremark", true));
            this.fremarkRichTextBox.Location = new System.Drawing.Point(21, 318);
            this.fremarkRichTextBox.Name = "fremarkRichTextBox";
            this.fremarkRichTextBox.Size = new System.Drawing.Size(241, 100);
            this.fremarkRichTextBox.TabIndex = 8;
            this.fremarkRichTextBox.Text = "";
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(509, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 420);
            this.splitter2.TabIndex = 129;
            this.splitter2.TabStop = false;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(177, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 420);
            this.splitter1.TabIndex = 130;
            this.splitter1.TabStop = false;
            // 
            // forder_by
            // 
            this.forder_by.DataPropertyName = "forder_by";
            this.forder_by.HeaderText = "序";
            this.forder_by.Name = "forder_by";
            this.forder_by.Width = 40;
            // 
            // fcode
            // 
            this.fcode.DataPropertyName = "fcode";
            this.fcode.HeaderText = "代码";
            this.fcode.Name = "fcode";
            this.fcode.Width = 80;
            // 
            // fname
            // 
            this.fname.DataPropertyName = "fname";
            this.fname.HeaderText = "名称";
            this.fname.Name = "fname";
            // 
            // fread_flage
            // 
            this.fread_flage.DataPropertyName = "fread_flage";
            this.fread_flage.FalseValue = "0";
            this.fread_flage.HeaderText = "只读";
            this.fread_flage.IndeterminateValue = "0";
            this.fread_flage.Name = "fread_flage";
            this.fread_flage.TrueValue = "1";
            this.fread_flage.Width = 40;
            // 
            // fshow_flag
            // 
            this.fshow_flag.DataPropertyName = "fshow_flag";
            this.fshow_flag.FalseValue = "0";
            this.fshow_flag.HeaderText = "显示";
            this.fshow_flag.IndeterminateValue = "0";
            this.fshow_flag.Name = "fshow_flag";
            this.fshow_flag.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fshow_flag.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fshow_flag.TrueValue = "1";
            this.fshow_flag.Width = 40;
            // 
            // fsum_flag
            // 
            this.fsum_flag.DataPropertyName = "fsum_flag";
            this.fsum_flag.FalseValue = "0";
            this.fsum_flag.HeaderText = "汇总";
            this.fsum_flag.IndeterminateValue = "0";
            this.fsum_flag.Name = "fsum_flag";
            this.fsum_flag.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fsum_flag.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fsum_flag.TrueValue = "1";
            this.fsum_flag.Width = 40;
            // 
            // fxsws
            // 
            this.fxsws.DataPropertyName = "fxsws";
            this.fxsws.HeaderText = "小数";
            this.fxsws.Name = "fxsws";
            this.fxsws.Width = 40;
            // 
            // fshow_width
            // 
            this.fshow_width.DataPropertyName = "fshow_width";
            this.fshow_width.HeaderText = "宽度";
            this.fshow_width.Name = "fshow_width";
            this.fshow_width.Width = 40;
            // 
            // fvaluetypeDataGridViewTextBoxColumn
            // 
            this.fvaluetypeDataGridViewTextBoxColumn.DataPropertyName = "fvalue_type";
            this.fvaluetypeDataGridViewTextBoxColumn.HeaderText = "类型";
            this.fvaluetypeDataGridViewTextBoxColumn.Name = "fvaluetypeDataGridViewTextBoxColumn";
            this.fvaluetypeDataGridViewTextBoxColumn.Width = 60;
            // 
            // fdata_type
            // 
            this.fdata_type.DataPropertyName = "fdata_type";
            this.fdata_type.HeaderText = "数据";
            this.fdata_type.Name = "fdata_type";
            this.fdata_type.Width = 60;
            // 
            // fvalue_content
            // 
            this.fvalue_content.DataPropertyName = "fvalue_content";
            this.fvalue_content.HeaderText = "内容";
            this.fvalue_content.Name = "fvalue_content";
            // 
            // fgroup_flag
            // 
            this.fgroup_flag.DataPropertyName = "fgroup_flag";
            this.fgroup_flag.FalseValue = "0";
            this.fgroup_flag.HeaderText = "分组";
            this.fgroup_flag.IndeterminateValue = "0";
            this.fgroup_flag.Name = "fgroup_flag";
            this.fgroup_flag.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fgroup_flag.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fgroup_flag.TrueValue = "1";
            this.fgroup_flag.Width = 40;
            // 
            // fcolumns_id
            // 
            this.fcolumns_id.DataPropertyName = "fcolumns_id";
            this.fcolumns_id.HeaderText = "fcolumns_id";
            this.fcolumns_id.Name = "fcolumns_id";
            this.fcolumns_id.Visible = false;
            // 
            // fdefault
            // 
            this.fdefault.DataPropertyName = "fdefault";
            this.fdefault.HeaderText = "默认值";
            this.fdefault.Name = "fdefault";
            // 
            // fremark
            // 
            this.fremark.DataPropertyName = "fremark";
            this.fremark.HeaderText = "备注";
            this.fremark.Name = "fremark";
            // 
            // fold_columns
            // 
            this.fold_columns.DataPropertyName = "fold_columns";
            this.fold_columns.HeaderText = "原字段";
            this.fold_columns.Name = "fold_columns";
            this.fold_columns.Visible = false;
            this.fold_columns.Width = 70;
            // 
            // UserdefinedFieldForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(791, 447);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panelTool);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UserdefinedFieldForm";
            this.ShowIcon = false;
            this.Text = "用户字段定义";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.UserdefinedFieldForm_Load);
            this.panelTool.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wwf_columnsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wwfDataSet)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ww.wwf.control.WWControlLib.WWTreeView wwTreeView1;
        private System.Windows.Forms.Panel panelTool;
        private System.Windows.Forms.Button butRef;
        private System.Windows.Forms.Button butSave;
        private System.Windows.Forms.Button butAdd;
        private System.Windows.Forms.ImageList imageListTree;
        private System.Windows.Forms.DataGridView DataGridViewObject;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.BindingSource wwf_columnsBindingSource;
        private ww.form.wwf.wwfDataSet wwfDataSet;
        
        private System.Windows.Forms.RichTextBox fremarkRichTextBox;
        private System.Windows.Forms.RichTextBox fvalue_contentRichTextBox;
        private System.Windows.Forms.ComboBox fvalue_typeComboBox;
        private System.Windows.Forms.ComboBox fdata_typeComboBox;
        private System.Windows.Forms.Button buttonDel;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Button buttonExcel;
        private System.Windows.Forms.DataGridViewTextBoxColumn forder_by;
        private System.Windows.Forms.DataGridViewTextBoxColumn fcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fread_flage;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fshow_flag;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fsum_flag;
        private System.Windows.Forms.DataGridViewTextBoxColumn fxsws;
        private System.Windows.Forms.DataGridViewTextBoxColumn fshow_width;
        private System.Windows.Forms.DataGridViewTextBoxColumn fvaluetypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fdata_type;
        private System.Windows.Forms.DataGridViewTextBoxColumn fvalue_content;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fgroup_flag;
        private System.Windows.Forms.DataGridViewTextBoxColumn fcolumns_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fdefault;
        private System.Windows.Forms.DataGridViewTextBoxColumn fremark;
        private System.Windows.Forms.DataGridViewTextBoxColumn fold_columns;
    }
}