﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;

namespace ww.form.wwf
{
    public partial class PersonPassForm : Form
    {
        PersonBLL bll = new PersonBLL();
        DataTable dtPerson = new DataTable();
        string strPersonID = "";//人员ID      
        public PersonPassForm(string fperson_id)
        {
            InitializeComponent();
            strPersonID = fperson_id;
        }

        private void PersonPassForm_Load(object sender, EventArgs e)
        {
            try
            {
                //修改记录
                dtPerson = this.bll.BllDTByID(strPersonID);
                wwf_personBindingSource.DataSource = dtPerson;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
       
        private void butSave_Click(object sender, EventArgs e)
        {
            try
            {
                dtPerson.Rows[0]["fpass"] = ww.wwf.com.Security.StrToEncrypt("MD5", this.fpassTextBox.Text);
                this.Validate();
                this.wwf_personBindingSource.EndEdit();
                if (this.bll.BllUpdatePass(dtPerson) > 0)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowInformation("密码修改成功！");
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void butReturn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}