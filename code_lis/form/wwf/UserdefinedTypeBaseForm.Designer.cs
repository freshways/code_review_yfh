﻿using System.Data;
namespace ww.form.wwf
{
    partial class UserdefinedTypeBaseForm
    {
        

        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserdefinedTypeBaseForm));
            this.panelTool = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxLeft = new System.Windows.Forms.TextBox();
            this.textBoxTop = new System.Windows.Forms.TextBox();
            this.buttonSet = new System.Windows.Forms.Button();
            this.butQuery = new System.Windows.Forms.Button();
            this.butExcel = new System.Windows.Forms.Button();
            this.contextMenuStripReport = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.列表设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridViewShow = new System.Windows.Forms.DataGridView();
            this.panelTool.SuspendLayout();
            this.contextMenuStripReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewShow)).BeginInit();
            this.SuspendLayout();
            // 
            // panelTool
            // 
            this.panelTool.Controls.Add(this.label2);
            this.panelTool.Controls.Add(this.label1);
            this.panelTool.Controls.Add(this.textBoxLeft);
            this.panelTool.Controls.Add(this.textBoxTop);
            this.panelTool.Controls.Add(this.buttonSet);
            this.panelTool.Controls.Add(this.butQuery);
            this.panelTool.Controls.Add(this.butExcel);
            this.panelTool.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelTool.Location = new System.Drawing.Point(0, 374);
            this.panelTool.Name = "panelTool";
            this.panelTool.Size = new System.Drawing.Size(643, 27);
            this.panelTool.TabIndex = 118;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(364, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 138;
            this.label2.Text = "Excel左开始行:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(235, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 137;
            this.label1.Text = "Excel上开始行:";
            // 
            // textBoxLeft
            // 
            this.textBoxLeft.Location = new System.Drawing.Point(455, 4);
            this.textBoxLeft.Name = "textBoxLeft";
            this.textBoxLeft.Size = new System.Drawing.Size(21, 21);
            this.textBoxLeft.TabIndex = 136;
            this.textBoxLeft.Text = "1";
            // 
            // textBoxTop
            // 
            this.textBoxTop.Location = new System.Drawing.Point(326, 4);
            this.textBoxTop.Name = "textBoxTop";
            this.textBoxTop.Size = new System.Drawing.Size(21, 21);
            this.textBoxTop.TabIndex = 135;
            this.textBoxTop.Text = "1";
            // 
            // buttonSet
            // 
            this.buttonSet.Image = ((System.Drawing.Image)(resources.GetObject("buttonSet.Image")));
            this.buttonSet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSet.Location = new System.Drawing.Point(551, 3);
            this.buttonSet.Name = "buttonSet";
            this.buttonSet.Size = new System.Drawing.Size(90, 23);
            this.buttonSet.TabIndex = 134;
            this.buttonSet.Text = "   列设置(&S)";
            this.buttonSet.UseVisualStyleBackColor = true;
            this.buttonSet.Click += new System.EventHandler(this.buttonSet_Click);
            // 
            // butQuery
            // 
            this.butQuery.Image = ((System.Drawing.Image)(resources.GetObject("butQuery.Image")));
            this.butQuery.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butQuery.Location = new System.Drawing.Point(11, 3);
            this.butQuery.Name = "butQuery";
            this.butQuery.Size = new System.Drawing.Size(80, 23);
            this.butQuery.TabIndex = 0;
            this.butQuery.Text = "   查询(&Q)";
            this.butQuery.UseVisualStyleBackColor = true;
            this.butQuery.Click += new System.EventHandler(this.butQuery_Click);
            // 
            // butExcel
            // 
            this.butExcel.Image = ((System.Drawing.Image)(resources.GetObject("butExcel.Image")));
            this.butExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butExcel.Location = new System.Drawing.Point(101, 3);
            this.butExcel.Name = "butExcel";
            this.butExcel.Size = new System.Drawing.Size(110, 23);
            this.butExcel.TabIndex = 133;
            this.butExcel.Text = "   导出Excel(&O)";
            this.butExcel.UseVisualStyleBackColor = true;
            this.butExcel.Click += new System.EventHandler(this.butExcel_Click);
            // 
            // contextMenuStripReport
            // 
            this.contextMenuStripReport.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem1,
            this.toolStripSeparator1,
            this.列表设置ToolStripMenuItem,
            this.退出ToolStripMenuItem});
            this.contextMenuStripReport.Name = "contextMenuStripReport";
            this.contextMenuStripReport.Size = new System.Drawing.Size(125, 98);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem2.Text = "查询";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem1.Text = "导出Excel";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(121, 6);
            // 
            // 列表设置ToolStripMenuItem
            // 
            this.列表设置ToolStripMenuItem.Name = "列表设置ToolStripMenuItem";
            this.列表设置ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.列表设置ToolStripMenuItem.Text = "列设置";
            this.列表设置ToolStripMenuItem.Click += new System.EventHandler(this.列表设置ToolStripMenuItem_Click);
            // 
            // 退出ToolStripMenuItem
            // 
            this.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            this.退出ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.退出ToolStripMenuItem.Text = "退出";
            this.退出ToolStripMenuItem.Click += new System.EventHandler(this.退出ToolStripMenuItem_Click);
            // 
            // dataGridViewShow
            // 
            this.dataGridViewShow.AllowUserToAddRows = false;
            this.dataGridViewShow.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewShow.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewShow.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewShow.ContextMenuStrip = this.contextMenuStripReport;
            this.dataGridViewShow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewShow.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewShow.Name = "dataGridViewShow";
            this.dataGridViewShow.RowHeadersWidth = 21;
            this.dataGridViewShow.RowTemplate.Height = 23;
            this.dataGridViewShow.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewShow.Size = new System.Drawing.Size(643, 374);
            this.dataGridViewShow.TabIndex = 131;
            // 
            // UserdefinedTypeBaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(643, 401);
            this.Controls.Add(this.dataGridViewShow);
            this.Controls.Add(this.panelTool);
            this.Name = "UserdefinedTypeBaseForm";
            this.ShowIcon = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.UserdefinedTypeBaseForm_Load);
            this.panelTool.ResumeLayout(false);
            this.panelTool.PerformLayout();
            this.contextMenuStripReport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewShow)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelTool;
        private System.Windows.Forms.Button buttonSet;
        private System.Windows.Forms.Button butExcel;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripReport;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 列表设置ToolStripMenuItem;
        private System.Windows.Forms.DataGridView dataGridViewShow;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxLeft;
        private System.Windows.Forms.TextBox textBoxTop;
        protected System.Windows.Forms.Button butQuery;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
    }
}