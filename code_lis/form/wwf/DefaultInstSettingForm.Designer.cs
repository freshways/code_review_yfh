﻿namespace ww.form.wwf
{
    partial class DefaultInstSettingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DefaultInstSettingForm));
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox仪器 = new System.Windows.Forms.ComboBox();
            this.btn保存 = new System.Windows.Forms.Button();
            this.btn关闭 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label默认仪器 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "本机默认仪器：";
            // 
            // comboBox仪器
            // 
            this.comboBox仪器.DisplayMember = "fname";
            this.comboBox仪器.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox仪器.FormattingEnabled = true;
            this.comboBox仪器.Location = new System.Drawing.Point(133, 34);
            this.comboBox仪器.Name = "comboBox仪器";
            this.comboBox仪器.Size = new System.Drawing.Size(209, 20);
            this.comboBox仪器.TabIndex = 1;
            this.comboBox仪器.ValueMember = "finstr_id";
            // 
            // btn保存
            // 
            this.btn保存.Location = new System.Drawing.Point(166, 103);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(75, 23);
            this.btn保存.TabIndex = 2;
            this.btn保存.Text = "保存";
            this.btn保存.UseVisualStyleBackColor = true;
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // btn关闭
            // 
            this.btn关闭.Location = new System.Drawing.Point(266, 103);
            this.btn关闭.Name = "btn关闭";
            this.btn关闭.Size = new System.Drawing.Size(75, 23);
            this.btn关闭.TabIndex = 4;
            this.btn关闭.Text = "关闭";
            this.btn关闭.UseVisualStyleBackColor = true;
            this.btn关闭.Click += new System.EventHandler(this.btn关闭_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "当前默认仪器：";
            // 
            // label默认仪器
            // 
            this.label默认仪器.AutoSize = true;
            this.label默认仪器.Location = new System.Drawing.Point(131, 72);
            this.label默认仪器.Name = "label默认仪器";
            this.label默认仪器.Size = new System.Drawing.Size(41, 12);
            this.label默认仪器.TabIndex = 6;
            this.label默认仪器.Text = "未设置";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(38, 139);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(191, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "设置完成后，请重新启动Lis系统。";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 159);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(209, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "注：登录过程中出现此画面时不用重启。";
            // 
            // DefaultInstSettingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(392, 178);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label默认仪器);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn关闭);
            this.Controls.Add(this.btn保存);
            this.Controls.Add(this.comboBox仪器);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DefaultInstSettingForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "设置本机默认仪器";
            this.Load += new System.EventHandler(this.DefaultInstSettingForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox仪器;
        private System.Windows.Forms.Button btn保存;
        private System.Windows.Forms.Button btn关闭;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label默认仪器;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}