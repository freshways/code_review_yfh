using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.IO;
using System.Drawing.Printing;
using System.Drawing.Imaging;

namespace ww.form.wwf.print
{
    public partial class ReportPrint : System.Windows.Forms.Form
    {
        private EMFStreamPrintDocument printDoc = null;
        public ReportPrint()
        {
            InitializeComponent();
                
        }

        private void ReportPrint_Load(object sender, EventArgs e)
        {
            try
            {
                this.Text = this.ReportName;
                this.AddReportDataSource(this.m_MainDataSet, this.m_MainDataSourceName, m_MainDataSourceName2);
                Print();
            }
            catch (Exception ex)
            {
               
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
               
            }
            finally
            {
                this.Close();
            }
        }

        private void Print()
        {
            try
            {
                if (this.printDoc == null)
                {
                    this.printDoc = new EMFStreamPrintDocument(this.rptViewer.LocalReport, this.m_ReportName);
                    if (this.printDoc.ErrorMessage != string.Empty)
                    {
                       // ww.wwf.wwfbll.WWMessage.MessageShowWarning("页面设置有误，请重新设置后重试 \r\n    " + this.printDoc.ErrorMessage);
                        ww.wwf.wwfbll.WWMessage.MessageShowWarning("页面设置有误，请重新设置后重试" );
                        this.printDoc = null;
                        PageSettings frm = new PageSettings(this.m_ReportName);
                        frm.ShowDialog();
                        frm.Dispose();
                        return;
                    }
                }

                this.printDoc.Print();
                this.printDoc = null;
            }
            catch (Exception ex)
            {
                //ww.wwf.wwfbll.WWMessage.MessageShowWarning(ex.Message.ToString());
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("页面设置有误，请重新设置后重试");
                PageSettings frm = new PageSettings(this.m_ReportName);
                frm.ShowDialog();
                frm.Dispose();                
            }
        }
        /// <summary>
        /// Set the DataSource for the report using a strong-typed dataset
        /// Call it in Form_Load event
        /// </summary>
        private void AddReportDataSource(object ReportSource, string ReportDataSetName1, string ReportDataSetName2)
        {
            rptViewer.LocalReport.DataSources.Clear();//tao2013 新加
            System.Type type = ReportSource.GetType();
            if (type == null)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n   The datasource is of the wrong type!");
                return;
            }
            else
            {
                System.Reflection.PropertyInfo[] picData = type.GetProperties();
                bool bolExist = false;
                foreach (System.Reflection.PropertyInfo piData in picData)
                {
                    if (piData.Name == "Tables")
                    {
                        bolExist = true;
                        if (ReportDataSetName1 == string.Empty)
                        {
                            ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The dataset name for the report does not exist or is empty!");
                            return;
                        }

                        this.rptViewer.LocalReport.DataSources.Add(
                            new Microsoft.Reporting.WinForms.ReportDataSource(ReportDataSetName1,
                            (piData.GetValue(ReportSource, null) as System.Data.DataTableCollection)[0])
                            );

                        this.rptViewer.LocalReport.DataSources.Add(
                          new Microsoft.Reporting.WinForms.ReportDataSource(ReportDataSetName2,
                          (piData.GetValue(ReportSource, null) as System.Data.DataTableCollection)[0])
                          );

                        this.rptViewer.RefreshReport();
                        break;
                    }
                }

                if (!bolExist)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The datasource is of the wrong type!");
                    return;
                }
            }
        }

        private System.Data.DataTableCollection GetTableCollection(object ReportSource)
        {
            System.Type type = ReportSource.GetType();

            if (type == null)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n     The datasource is of the wrong type!");
                return null;
            }
            else
            {
                System.Reflection.PropertyInfo[] picData = type.GetProperties();
                bool bolExist = false;
                foreach (System.Reflection.PropertyInfo piData in picData)
                {
                    if (piData.Name == "Tables")
                    {
                        bolExist = true;
                        return piData.GetValue(ReportSource, null) as System.Data.DataTableCollection;
                    }
                }

                if (!bolExist)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The datasource is of the wrong type!");
                    return null;
                }
            }
            return null;
        }

       
        
        private void rptViewer_Drillthrough(object sender, Microsoft.Reporting.WinForms.DrillthroughEventArgs e)
        {
            if (this.m_DrillDataSet == null)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The dataset name for the report does not exist or is empty!");
                return;
            }
            else
            {
                if (this.m_DrillDataSourceName == string.Empty)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The datasource is of the wrong type!");
                    return;
                }
                else
                {
                    Microsoft.Reporting.WinForms.LocalReport report = e.Report as Microsoft.Reporting.WinForms.LocalReport;
                    report.DataSources.Add(new Microsoft.Reporting.WinForms.ReportDataSource(this.m_DrillDataSourceName, this.GetTableCollection(this.m_DrillDataSet)[0]));
                }
            }
        }      


        private string GetTimeStamp()
        {
            string strRet = string.Empty;
            System.DateTime dtNow = System.DateTime.Now;
            strRet += dtNow.Year.ToString() +
                        dtNow.Month.ToString("00") +
                        dtNow.Day.ToString("00") +
                        dtNow.Hour.ToString("00") +
                        dtNow.Minute.ToString("00") +
                        dtNow.Second.ToString("00") +
                        System.DateTime.Now.Millisecond.ToString("000");
            return strRet;

        }
        
      
    }
}