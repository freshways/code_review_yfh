﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;
using ww.wwf.com;
using ww.form.Properties;
namespace ww.form.wwf
{
    public partial class LoginForm : Form
    {
        LoginBLL bll = new LoginBLL();
        DataTable dtorg = new DataTable();
        public LoginForm()
        {
            InitializeComponent();          
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            FormInitSet();
        }
        /// <summary>
        /// 开始窗口设置
        /// </summary>
        void FormInitSet()
        {
            try
            {
                this.Width = 425;
                this.Height = 280;
                this.DataGridViewObject.Visible = false;
                this.SysLoginUserName.Text = Settings.Default.strLoingName;
                this.labelSysName.Text = LoginBLL.sysName;
                //changed by wjz 20160306 修改版本号的获取 ▽
                //this.labelC.Text = LoginBLL.sysVar;
                this.labelC.Text = "Ver "+ Application.ProductVersion.ToString();
                //changed by wjz 20160306 修改版本号的获取 △
                this.label3.Text = LoginBLL.Copyright;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 登录确认
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SysLoginLogin_Click(object sender, EventArgs e)
        {
            try
            { 
                    Cursor = Cursors.WaitCursor;

                    //if (System.DateTime.Now.Year == 2013 & System.DateTime.Now.Month == 8)
                    //{
                        LoginOK();
                        SaveConfigSet();
                    //}
                    //else
                    //{
                    //    WWMessage.MessageShowWarning("试用版已过期，注册请联系手机:11111111111 QQ:249116367");
                    //}
                
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        /// <summary>
        /// 登录判断
        /// </summary>
        private void LoginOK()
        {           

            if (this.SysLoginUserName.Text == "" || SysLoginUserName.Text == null || SysLoginUserName.Text.Length == 0)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("用户名不能为空！");
                return;
            }
            else
            {
                int intOrgcount = 0;
                string userid = this.SysLoginUserName.Text;
                string pwd = this.SysLoginPassword.Text;
                
                //changed by wjz 20160210 修改登录方式，添加异常处理 ▽
                //changed by wjz 修改登录认证方式 ▽
                //if (this.bll.BllUserPassOk(userid, pwd))
                //if (this.bll.BllUserPassOkParams(userid, pwd))
                bool blogin = false;
                try
                {
                    blogin = this.bll.BllUserPassOkParams(userid, pwd);
                }
                //catch
                //{
                //    //出现此问题时，有可能是userLogin存储过程没有创建。
                //    ww.wwf.wwfbll.WWMessage.MessageShowWarning("登录失败，请先确保网络是否畅通。如果仍出现问题，请联系技术人员！");
                //    return;
                //}
                catch (System.Exception ex)
                {
                    WWMessage.MessageShowWarning("登录失败，请先确保网络是否畅通。如果仍出现问题，请联系技术人员！" + ex.Message);
                    return;
                }
                if(blogin)
                //changed by wjz 修改登录认证方式 △
                //changed by wjz 20160210 修改登录方式，添加异常处理 △
                {                   
                    dtorg = this.bll.BllUserOrg(userid);
                    intOrgcount = dtorg.Rows.Count;
                    if (dtorg.Rows.Count == 1)
                    {
                        ShowSysMain(dtorg.Rows[0]["forg_id"].ToString());
                    }
                    else if (intOrgcount > 1)
                    {
                        this.ActiveControl = this.DataGridViewObject;
                        DataGridViewObjectVisible();
                    }
                    else
                    {
                        ww.wwf.wwfbll.WWMessage.MessageShowWarning(this.SysLoginUserName.Text + " 未分配到部门、岗位，请联系管理员。");
                        return;
                    }
                }
                else
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("用户名或密码有误！请重新输入");
                    
                }

            }
        }
        /// <summary>
        /// 用户只在一个岗位时
        /// </summary>
        /// <param name="fperson_id"></param>
        private void ShowSysMain(string forg_id)
        {
            this.bll.BllSetLoginValue(dtorg, forg_id);
            this.DialogResult = DialogResult.OK;
        }

        /// <summary>
        /// 用户有多个岗位
        /// </summary>
        private void DataGridViewObjectVisible()
        {
            this.Width = 425;
            this.Height = 380;
            DataGridViewObject.Visible = true;
            this.DataGridViewObject.AutoGenerateColumns = false;
            this.DataGridViewObject.DataSource = dtorg;
        }

        /// <summary>
        /// 存储配置文件
        /// </summary>
        private void SaveConfigSet()
        {
            try
            {               
                Settings.Default.strLoingName = SysLoginUserName.Text;               
               
                Settings.Default.Save();                
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /*
        /// <summary>
        /// 动态修改业务数据库连接
        /// </summary>
        private void UpdateAppConfig()
        {

            try
            {
                if ((textBoxTZ.Text.Trim() == System.Configuration.ConfigurationSettings.AppSettings["BusinessTZ"]) & (System.Configuration.ConfigurationSettings.AppSettings["BusinessYear"] == comboBoxYear.SelectedItem.ToString().Trim())) { }
                else
                {
                    //数据库名称
                    string strDBName = System.Configuration.ConfigurationSettings.AppSettings["BusinessInitialCatalog"] + "_" + textBoxTZ.Text + "_" + comboBoxYear.SelectedItem.ToString();
                    string strSysDBValue = "data source=" + System.Configuration.ConfigurationSettings.AppSettings["BusinessDataSource"] + ";Initial Catalog=" + strDBName + ";User ID=" + System.Configuration.ConfigurationSettings.AppSettings["BusinessUserID"] + ";Password=" + System.Configuration.ConfigurationSettings.AppSettings["BusinessPassword"] + ";Provider=SQLOLEDB.1;Persist Security Info=True;";
                    AppConfig.ConfigSetValue(System.Windows.Forms.Application.ExecutablePath, "WW.DBConn.Business", strSysDBValue);

                    AppConfig.ConfigSetValueAppSettings(System.Windows.Forms.Application.ExecutablePath, "BusinessTZ", textBoxTZ.Text.Trim());
                    AppConfig.ConfigSetValueAppSettings(System.Windows.Forms.Application.ExecutablePath, "BusinessYear", comboBoxYear.SelectedItem.ToString().Trim());


                    try
                    {
                        ww.wwf.wwfbll.WWMessage.MessageShowInformation("账套或年度已经改变，请重新启动后生效！");
                        Application.Exit();
                        //Application.Run(new MainForm());
                    }
                    catch (Exception ex)
                    {
                        ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }*/
        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SysLoginCancel_Click(object sender, EventArgs e)
        {
            try
            {
               // SaveConfigSet();
               // this.Close();
                Application.Exit();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        
        private void DataGridViewObject_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                string orgid = this.DataGridViewObject.CurrentRow.Cells["forg_id"].Value.ToString();
                //MessageBox.Show(orgid);
                ShowSysMain(orgid);
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void SysLoginUserName_KeyDown(object sender, KeyEventArgs e)
        {

            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Space))//或(e.KeyCode==Keys.Enter) 
            {
                try
                {
                    Cursor = Cursors.WaitCursor;
                    LoginOK();
                    SaveConfigSet();
                }
                catch (Exception ex)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
                }
                finally
                {
                    Cursor = Cursors.Arrow;
                }
            }
        }

        private void SysLoginPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Space))//或(e.KeyCode==Keys.Enter) 
            {
                try
                {
                    Cursor = Cursors.WaitCursor;
                    LoginOK();
                    SaveConfigSet();
                }
                catch (Exception ex)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
                }
                finally
                {
                    Cursor = Cursors.Arrow;
                }
            }
        }

        private void LoginForm_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Space))//或(e.KeyCode==Keys.Enter) 
            {
                try
                {
                    Cursor = Cursors.WaitCursor;
                    LoginOK();
                    SaveConfigSet();

                }
                catch (Exception ex)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
                }
                finally
                {
                    Cursor = Cursors.Arrow;
                }
            }
        }

      
        private void DataGridViewObject_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Space))//或(e.KeyCode==Keys.Enter) 
            {
                try
                {
                    Cursor = Cursors.WaitCursor;
                    string orgid = this.DataGridViewObject.CurrentRow.Cells["forg_id"].Value.ToString();
                    //MessageBox.Show(orgid);
                    ShowSysMain(orgid);
                }
                catch (Exception ex)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
                }
                finally
                {
                    Cursor = Cursors.Arrow;
                }
            }
        }

        private void buttonHelp_Click(object sender, EventArgs e)
        {
            try
            {
                AboutDialog aboutDialog = new AboutDialog();
                aboutDialog.ShowDialog(this);
                //MessageBox.Show(LoginBLL.strHelpContact, "服务 联系方式", MessageBoxButtons.OK, MessageBoxIcon.Question);
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            if (System.IO.File.Exists("uplistdo.exe") == false)
            {
                MessageBox.Show("升级工具丢失，请联系软件维护人员处理故障!", "消息", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            Process.Start("uplistdo.exe");
        }
   
      
    }
}