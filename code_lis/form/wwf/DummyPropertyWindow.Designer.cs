namespace ww.form.wwf
{
    partial class DummyPropertyWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DummyPropertyWindow));
            this.zaSuiteTreeView1 = new ww.wwf.control.WWControlLib.WWTreeView();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // zaSuiteTreeView1
            // 
            this.zaSuiteTreeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zaSuiteTreeView1.ImageIndex = 1;
            this.zaSuiteTreeView1.ImageList = this.imageList;
            this.zaSuiteTreeView1.Location = new System.Drawing.Point(0, 3);
            this.zaSuiteTreeView1.Name = "zaSuiteTreeView1";
            this.zaSuiteTreeView1.SelectedImageIndex = 0;
            this.zaSuiteTreeView1.Size = new System.Drawing.Size(208, 283);
            this.zaSuiteTreeView1.TabIndex = 81;
            this.zaSuiteTreeView1.ZADataTable = null;
            this.zaSuiteTreeView1.ZADisplayFieldName = "";
            this.zaSuiteTreeView1.ZAKeyFieldName = "";
            this.zaSuiteTreeView1.ZAParentFieldName = "";
            this.zaSuiteTreeView1.ZAToolTipTextName = "";
            this.zaSuiteTreeView1.ZATreeViewRootValue = "";
            this.zaSuiteTreeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.zazaSuiteTreeView1_AfterSelect);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "SysBookOpen.png");
            this.imageList.Images.SetKeyName(1, "SysBookClose.png");
            // 
            // DummyPropertyWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.ClientSize = new System.Drawing.Size(208, 289);
            this.Controls.Add(this.zaSuiteTreeView1);
            this.HideOnClose = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DummyPropertyWindow";
            this.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.ShowHint = WeifenLuo.WinFormsUI.Docking.DockState.DockRight;
            this.TabText = "业务功能树";
            this.Text = "业务功能树";
            this.Load += new System.EventHandler(this.DummyPropertyWindow_Load);
            this.ResumeLayout(false);

		}
		#endregion

        private ww.wwf.control.WWControlLib.WWTreeView zaSuiteTreeView1;
        private System.Windows.Forms.ImageList imageList;

    }
}