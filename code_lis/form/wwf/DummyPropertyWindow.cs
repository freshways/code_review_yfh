using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using ww.wwf.wwfbll;
using System.Reflection;
namespace ww.form.wwf
{
    public partial class DummyPropertyWindow : ToolWindow
    {
        LoginBLL bll = new LoginBLL();//组织功能
        string thisStrUserOrgId = "";//"61590ec8451a4fc2aedf4139d9b37c10";
        public DummyPropertyWindow()
        {
            InitializeComponent();
        }

        private void DummyPropertyWindow_Load(object sender, EventArgs e)
        {
            thisStrUserOrgId = LoginBLL.strOrgID;//"61590ec8451a4fc2aedf4139d9b37c10";
            GetxPanderListTree();
        }
        public void GetxPanderListTree()
        {
            try
            {
                DataTable dtOrgFunc = new DataTable();
                dtOrgFunc = bll.BllOrgFuncDT(thisStrUserOrgId); ;//装载数据集
                this.zaSuiteTreeView1.ZADataTable = dtOrgFunc;//数据表
                this.zaSuiteTreeView1.ZATreeViewRootValue = "wwf1";//树 RootId　根节点值
                this.zaSuiteTreeView1.ZAParentFieldName = "fp_id";//上级字段名称
                this.zaSuiteTreeView1.ZADisplayFieldName = "fname";//显示字段名称
                this.zaSuiteTreeView1.ZAKeyFieldName = "ffunc_id";//主键字段名称
                this.zaSuiteTreeView1.ZATreeViewShow();//显示树                 
                try
                {
                    this.zaSuiteTreeView1.SelectedNode =zaSuiteTreeView1.Nodes[0].Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        DockContent formFun = null;
        private void zazaSuiteTreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;            
                DataTable dtthislist = bll.BllFuncListByID(e.Node.Name);
                //string opentype = dtthislist.Rows[0]["fopen_page"].ToString();
                string fpluggableUnit = dtthislist.Rows[0]["fcj_flag"].ToString();
                string fpluggableUnitAssembly = dtthislist.Rows[0]["fcj_zp"].ToString();
                string ffunc_winform = dtthislist.Rows[0]["ffunc_winform"].ToString();
                string fname = dtthislist.Rows[0]["fname"].ToString();
                if (ffunc_winform == "" || ffunc_winform == null) { }
                else
                {
                    if (fpluggableUnit == "0")
                    {
                        formFun = GetFormByFormNameDockContent(ffunc_winform);
                        if (formFun != null)
                        {
                            formFun.Text = fname;
                            formFun.TabText = fname;
                            formFun.ToolTipText = fname;
                            formFun.Show(MainForm.dockPanel);
                        }
                    }
                    else
                    {
                        Form fff = new Form();
                        fff = GetFormByFormNameForm(ffunc_winform, fpluggableUnitAssembly);
                        if (fff != null)
                        {
                            fff.Text = fname;
                            fff.Show();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
               ww.wwf.wwfbll.WWMessage.MessageShowError( ex.ToString());
           }
           finally
           {
               Cursor = Cursors.Arrow;
           }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strFormName">窗体名</param>     
        /// <returns></returns>
        private DockContent GetFormByFormNameDockContent(string strFormName)
        {
            DockContent form = null;
            try
            {
                Type type = Type.GetType(strFormName);
                form = (DockContent)Activator.CreateInstance(type);
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError("窗体有误，请联系管理员；\n当前窗体名为：" + strFormName + "\n" + ex.ToString());
            }
            return form;
        }

        /// <summary>
        /// /
        /// </summary>
        /// <param name="strFormName">窗体名</param>
        /// <param name="strFpluggableUnit">是否插件1为是；0不是</param>
        /// <param name="strFpluggableUnitAssembly">插件dll</param>
        /// <returns></returns>
        private Form GetFormByFormNameForm(string strFormName, string strFpluggableUnitAssembly)
        {
            Form form = null;
            try
            {

                Assembly assm = Assembly.LoadFrom(strFpluggableUnitAssembly);
                Type TypeToLoad = assm.GetType(strFormName);
                object obj;
                obj = Activator.CreateInstance(TypeToLoad);
                form = (Form)obj;

            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError("窗体有误，请联系管理员；\n当前窗体名为：" + strFormName + "\n" + ex.ToString());
            }
            return form;
        }
        /*
        /// <summary>
        /// 根据字符窗体名,实例化此名称的窗体
        /// </summary>
        /// <param name="strFuncID">功能ID</param>
        /// <param name="strFormName">窗体名</param>
        /// <param name="strFpluggableUnit">是否插件1为是；0不是</param>
        /// <param name="strFpluggableUnitAssembly">插件dll</param>
        /// <returns></returns>
        private DockContent GetFormByFormName(string strFormName)
        {
            DockContent form = null;

            //if (MainForm.dockPanel.DocumentStyle == DocumentStyle.SystemMdi)
            try
            {
                Type type = Type.GetType(strFormName);
                form = (DockContent)Activator.CreateInstance(type);
            }
            catch
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("业务功能设置有误，请联系管理员! ");

            }
            //form.DockPanel.set = DocumentStyle.SystemMdi;
            return form;
        }*/
    }
}