﻿namespace ww.form.wwf
{
    partial class Frm仪器项目组合
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.ColumnGroupitemid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnGroupidR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnRefValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnDelItem = new System.Windows.Forms.Button();
            this.btnAddItem = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ColumnGroupID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnInstrID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnGroupName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnDelGroup = new System.Windows.Forms.Button();
            this.btnAddGroup = new System.Windows.Forms.Button();
            this.btnEditGroup = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnQuery = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cboInstr = new System.Windows.Forms.ComboBox();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView2);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(279, 43);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(504, 397);
            this.panel2.TabIndex = 4;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnGroupitemid,
            this.ColumnGroupidR,
            this.ColumnItemID,
            this.ColumnItemCode,
            this.ColumnItemName,
            this.ColumnUnit,
            this.ColumnRefValue});
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(0, 0);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowTemplate.Height = 23;
            this.dataGridView2.Size = new System.Drawing.Size(504, 351);
            this.dataGridView2.TabIndex = 1;
            // 
            // ColumnGroupitemid
            // 
            this.ColumnGroupitemid.DataPropertyName = "groupitemid";
            this.ColumnGroupitemid.HeaderText = "groupitemid";
            this.ColumnGroupitemid.Name = "ColumnGroupitemid";
            this.ColumnGroupitemid.ReadOnly = true;
            this.ColumnGroupitemid.Visible = false;
            // 
            // ColumnGroupidR
            // 
            this.ColumnGroupidR.DataPropertyName = "groupid";
            this.ColumnGroupidR.HeaderText = "组合ID";
            this.ColumnGroupidR.Name = "ColumnGroupidR";
            this.ColumnGroupidR.ReadOnly = true;
            this.ColumnGroupidR.Visible = false;
            // 
            // ColumnItemID
            // 
            this.ColumnItemID.DataPropertyName = "fitemid";
            this.ColumnItemID.HeaderText = "fitemid";
            this.ColumnItemID.Name = "ColumnItemID";
            this.ColumnItemID.ReadOnly = true;
            this.ColumnItemID.Visible = false;
            // 
            // ColumnItemCode
            // 
            this.ColumnItemCode.DataPropertyName = "fitem_code";
            this.ColumnItemCode.HeaderText = "项目代码";
            this.ColumnItemCode.Name = "ColumnItemCode";
            this.ColumnItemCode.ReadOnly = true;
            this.ColumnItemCode.Width = 90;
            // 
            // ColumnItemName
            // 
            this.ColumnItemName.DataPropertyName = "fname";
            this.ColumnItemName.HeaderText = "项目名称";
            this.ColumnItemName.Name = "ColumnItemName";
            this.ColumnItemName.ReadOnly = true;
            this.ColumnItemName.Width = 120;
            // 
            // ColumnUnit
            // 
            this.ColumnUnit.DataPropertyName = "funit_name";
            this.ColumnUnit.HeaderText = "单位";
            this.ColumnUnit.Name = "ColumnUnit";
            this.ColumnUnit.ReadOnly = true;
            // 
            // ColumnRefValue
            // 
            this.ColumnRefValue.DataPropertyName = "fref";
            this.ColumnRefValue.HeaderText = "参考值";
            this.ColumnRefValue.Name = "ColumnRefValue";
            this.ColumnRefValue.ReadOnly = true;
            this.ColumnRefValue.Width = 110;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnDelItem);
            this.panel4.Controls.Add(this.btnAddItem);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 351);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(504, 46);
            this.panel4.TabIndex = 0;
            // 
            // btnDelItem
            // 
            this.btnDelItem.Location = new System.Drawing.Point(250, 7);
            this.btnDelItem.Name = "btnDelItem";
            this.btnDelItem.Size = new System.Drawing.Size(92, 28);
            this.btnDelItem.TabIndex = 1;
            this.btnDelItem.Text = "删除项目";
            this.btnDelItem.UseVisualStyleBackColor = true;
            this.btnDelItem.Click += new System.EventHandler(this.btnDelItem_Click);
            // 
            // btnAddItem
            // 
            this.btnAddItem.Location = new System.Drawing.Point(135, 7);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(92, 28);
            this.btnAddItem.TabIndex = 2;
            this.btnAddItem.Text = "添加项目";
            this.btnAddItem.UseVisualStyleBackColor = true;
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 43);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(279, 397);
            this.panel1.TabIndex = 3;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnGroupID,
            this.ColumnInstrID,
            this.ColumnIndex,
            this.ColumnGroupName});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(279, 351);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.DataSourceChanged += new System.EventHandler(this.dataGridView1_DataSourceChanged);
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // ColumnGroupID
            // 
            this.ColumnGroupID.DataPropertyName = "groupid";
            this.ColumnGroupID.HeaderText = "GroupID";
            this.ColumnGroupID.Name = "ColumnGroupID";
            this.ColumnGroupID.ReadOnly = true;
            this.ColumnGroupID.Visible = false;
            // 
            // ColumnInstrID
            // 
            this.ColumnInstrID.DataPropertyName = "finstrid";
            this.ColumnInstrID.HeaderText = "仪器ID";
            this.ColumnInstrID.Name = "ColumnInstrID";
            this.ColumnInstrID.ReadOnly = true;
            this.ColumnInstrID.Visible = false;
            // 
            // ColumnIndex
            // 
            this.ColumnIndex.DataPropertyName = "orderby";
            this.ColumnIndex.HeaderText = "序号";
            this.ColumnIndex.Name = "ColumnIndex";
            this.ColumnIndex.ReadOnly = true;
            this.ColumnIndex.Width = 80;
            // 
            // ColumnGroupName
            // 
            this.ColumnGroupName.DataPropertyName = "groupname";
            this.ColumnGroupName.HeaderText = "组合名称";
            this.ColumnGroupName.Name = "ColumnGroupName";
            this.ColumnGroupName.ReadOnly = true;
            this.ColumnGroupName.Width = 150;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnDelGroup);
            this.panel3.Controls.Add(this.btnAddGroup);
            this.panel3.Controls.Add(this.btnEditGroup);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 351);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(279, 46);
            this.panel3.TabIndex = 0;
            // 
            // btnDelGroup
            // 
            this.btnDelGroup.Location = new System.Drawing.Point(180, 7);
            this.btnDelGroup.Name = "btnDelGroup";
            this.btnDelGroup.Size = new System.Drawing.Size(68, 28);
            this.btnDelGroup.TabIndex = 0;
            this.btnDelGroup.Text = "删除组合";
            this.btnDelGroup.UseVisualStyleBackColor = true;
            this.btnDelGroup.Click += new System.EventHandler(this.btnDelGroup_Click);
            // 
            // btnAddGroup
            // 
            this.btnAddGroup.Location = new System.Drawing.Point(12, 7);
            this.btnAddGroup.Name = "btnAddGroup";
            this.btnAddGroup.Size = new System.Drawing.Size(68, 28);
            this.btnAddGroup.TabIndex = 0;
            this.btnAddGroup.Text = "添加组合";
            this.btnAddGroup.UseVisualStyleBackColor = true;
            this.btnAddGroup.Click += new System.EventHandler(this.btnAddGroup_Click);
            // 
            // btnEditGroup
            // 
            this.btnEditGroup.Location = new System.Drawing.Point(97, 7);
            this.btnEditGroup.Name = "btnEditGroup";
            this.btnEditGroup.Size = new System.Drawing.Size(68, 28);
            this.btnEditGroup.TabIndex = 0;
            this.btnEditGroup.Text = "修改组合";
            this.btnEditGroup.UseVisualStyleBackColor = true;
            this.btnEditGroup.Click += new System.EventHandler(this.btnEditGroup_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnQuery);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cboInstr);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(783, 43);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "仪器";
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(414, 14);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(75, 23);
            this.btnQuery.TabIndex = 2;
            this.btnQuery.Text = "查询";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "仪器名称：";
            // 
            // cboInstr
            // 
            this.cboInstr.FormattingEnabled = true;
            this.cboInstr.Location = new System.Drawing.Point(112, 16);
            this.cboInstr.Name = "cboInstr";
            this.cboInstr.Size = new System.Drawing.Size(242, 20);
            this.cboInstr.TabIndex = 0;
            this.cboInstr.SelectedIndexChanged += new System.EventHandler(this.cboInstr_SelectedIndexChanged);
            // 
            // Frm仪器项目组合
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(783, 440);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.Name = "Frm仪器项目组合";
            this.Text = "Frm仪器项目组合";
            this.Load += new System.EventHandler(this.Frm仪器项目组合_Load);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cboInstr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button btnDelGroup;
        private System.Windows.Forms.Button btnEditGroup;
        private System.Windows.Forms.Button btnAddGroup;
        private System.Windows.Forms.Button btnDelItem;
        private System.Windows.Forms.Button btnAddItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnGroupID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnInstrID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnGroupName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnGroupitemid;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnGroupidR;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnItemID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnRefValue;
    }
}