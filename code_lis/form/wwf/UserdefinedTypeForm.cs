﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;
namespace ww.form.wwf
{
    public partial class UserdefinedTypeForm : SysBaseForm
    {
        UserdefinedBLL bllTtpe = new UserdefinedBLL();
        string strGUID = "";//当前选中
        DataRowView rowCurrent =null;//当前行
        DataTable dtCurr = new DataTable();
        public UserdefinedTypeForm()
        {
            InitializeComponent();
        }

        private void UserdefinedTypeForm_Load(object sender, EventArgs e)
        {
            GetTree("-1");
        }
        /// <summary>
        /// 列出系统部门树
        /// </summary>
        public void GetTree(string pid)
        {
            try
            {
                this.wwTreeView1.ZADataTable = this.bllTtpe.BllTypeDT(2);
                this.wwTreeView1.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeView1.ZAParentFieldName = "fp_id";//上级字段名称
                this.wwTreeView1.ZADisplayFieldName = "fname";//显示字段名称
                this.wwTreeView1.ZAKeyFieldName = "ftable_id";//主键字段名称
                this.wwTreeView1.ZATreeViewShow();//显示树        
                try
                {
                    this.wwTreeView1.SelectedNode = wwTreeView1.Nodes[0].Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void GetType(string fp_id)
        {
            try
            {
                this.dtCurr = this.bllTtpe.BllTypeDTByfpid(fp_id);
                this.bindingSourceType.DataSource = dtCurr;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void wwTreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                strGUID = e.Node.Name.ToString();
                GetType(strGUID);
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void bindingSourceType_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                rowCurrent = (DataRowView)bindingSourceType.Current;//行
                /*
                string fname = "";
                if (rowCurrent != null)
                {
                    fname = drObject["fname"].ToString();
                    MessageBox.Show(fname);
                }*/
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void butAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (strGUID == "" || strGUID == null)
                { }
                else
                {
                    if(this.bllTtpe.BllTypeAdd(this.strGUID)>0)
                        GetType(strGUID);
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void butSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.bindingSourceType.EndEdit();
                this.DataGridViewObject.EndEdit();
                for (int i = 0; i < this.dtCurr.Rows.Count; i++)
                {
                    // if (rowCurrent != null)
                    // {
                    this.bllTtpe.BllTypeUpdate(this.dtCurr.Rows[i]);

                    // }/
                }
                GetType(strGUID);

            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void butRef_Click(object sender, EventArgs e)
        {
            GetTree("-1");
            GetType(strGUID);
        }

    }
}