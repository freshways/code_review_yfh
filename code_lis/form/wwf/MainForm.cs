using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Forms;
using System.IO;
using WeifenLuo.WinFormsUI.Docking;
using System.Data;
using System.Diagnostics;
using ww.form.wwf.Customization;
using ww.wwf.wwfbll;
using ww.form.Properties;
namespace ww.form.wwf
{
    public partial class MainForm : Form
    {
        LoginBLL bll = new LoginBLL();//组织功能
        private bool m_bSaveLayout = true;
		private DeserializeDockContent m_deserializeDockContent;
		private DummySolutionExplorer m_solutionExplorer = new DummySolutionExplorer();
		//private DummyPropertyWindow m_propertyWindow = new DummyPropertyWindow();
		private DummyToolbox m_toolbox = new DummyToolbox();
      
       // private DummyToolboxTree m_toolboxtree = new DummyToolboxTree();
		private DummyOutputWindow m_outputWindow = new DummyOutputWindow();
		private DummyTaskList m_taskList = new DummyTaskList();
        public static WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel;

        public MainForm()
        {
            try
            {
                //-----------------------------------------------------------------------
                // 
                // dockPanel
                // 
                MainForm.dockPanel = new DockPanel();
                MainForm.dockPanel.Dock = System.Windows.Forms.DockStyle.Fill;
                //MainForm.dockPanel.DockBottomPortion = 150;
                //MainForm.dockPanel.DockLeftPortion = 200;
                //MainForm.dockPanel.DockRightPortion = 200;
                //MainForm.dockPanel.DockTopPortion = 150;
                //  MainForm.dockPanel.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(0)));
                //MainForm.dockPanel.Location = new System.Drawing.Point(0, 49);
                MainForm.dockPanel.Name = "dockPanel";
                // MainForm.dockPanel.RightToLeftLayout = true;
                // MainForm.dockPanel.Size = new System.Drawing.Size(579, 338);
                MainForm.dockPanel.TabIndex = 0;
                this.Controls.Add(MainForm.dockPanel);              
              
                InitializeComponent();
                
                showRightToLeft.Checked = (RightToLeft == RightToLeft.Yes);
                RightToLeftLayout = showRightToLeft.Checked;
                m_solutionExplorer = new DummySolutionExplorer();
                m_solutionExplorer.RightToLeftLayout = RightToLeftLayout;
                m_deserializeDockContent = new DeserializeDockContent(GetContentFromPersistString);

                dockPanel.AllowEndUserDocking = !dockPanel.AllowEndUserDocking;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void MainForm_Load(object sender, System.EventArgs e)
        {
            try
            {
                this.Text =  LoginBLL.sysName;
                menuItemAbout.Text = "关于 " + LoginBLL.sysName;

                //20150610 begin wjz //添加默认仪器的设置窗口
                bool loginResult = ShowLoginForm();
                if (loginResult == false)
                {
                    return;
                }
                //20150610 end wjz //添加默认仪器的设置窗口

                toolStripStatusLabelPositionName.Text = toolStripStatusLabelPositionName.Text + LoginBLL.strPositionName;
                toolStripStatusLabelDeptName.Text = toolStripStatusLabelDeptName.Text + LoginBLL.strDeptName;
                toolStripStatusLabelUser.Text = toolStripStatusLabelUser.Text + LoginBLL.strPersonName;
                toolStripStatusLabelLoginName.Text = toolStripStatusLabelLoginName.Text + LoginBLL.strPersonID;

                //add by wjz 20160306 修改版本号的获取 ▽
                toolStripStatusLabelVersion.Text = "版本号：" + Application.ProductVersion.ToString();
                //add by wjz 20160306 修改版本号的获取 △

                string configFile = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "DockPanel.config");

                if (File.Exists(configFile))
                    dockPanel.LoadFromXml(configFile, m_deserializeDockContent);
                m_toolbox.Show(dockPanel);

               
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }

            // changed by wjz 20151225 为护理、收款修改起始页 ▽
            #region old code
            //if (LoginBLL.SysStartPage == "" || LoginBLL.SysStartPage == null) { }
            //else
            //{
            //    DockContent formStart = new DockContent();
            //    try
            //    {
            //        Type type = Type.GetType(LoginBLL.SysStartPage);
            //        formStart = (DockContent)Activator.CreateInstance(type);
            //        formStart.TabText = "起始页";
            //        formStart.Show(dockPanel);
            //    }
            //    catch (Exception ex)
            //    {
            //        WWMessage.MessageShowError(ex.Message.ToString());
            //    }
            //}
            #endregion
            if (LoginBLL.strPositionID == "8" || LoginBLL.strPositionID == "9")
            {
                toolStripDropDownButtonGo.Enabled = false;
                toolStripButton_ReportIn.Enabled = false;
                toolStripButton_ReportQuery.Enabled = false;
                toolStripButton_Item.Enabled = false;
                toolStripButton_ReportSet.Enabled = false;
                toolStripButton默认仪器.Enabled = false;

                LoginBLL.SysStartPage = "";

                //DockContent formStart = new DockContent();
                //try
                //{
                //    Type type = typeof(FrmWelcome);
                //    formStart = (DockContent)Activator.CreateInstance(type);
                //    formStart.TabText = "起始页";
                //    formStart.Show(dockPanel);
                //}
                //catch (Exception ex)
                //{
                //    WWMessage.MessageShowError(ex.Message.ToString());
                //}
            }
            else
            {
                #region 原来的代码
                if (LoginBLL.SysStartPage == "" || LoginBLL.SysStartPage == null) { }
                else
                {
                    DockContent formStart = new DockContent();
                    try
                    {
                        Type type = Type.GetType(LoginBLL.SysStartPage);
                        formStart = (DockContent)Activator.CreateInstance(type);
                        formStart.TabText = "起始页";
                        formStart.Show(dockPanel);
                    }
                    catch (Exception ex)
                    {
                        WWMessage.MessageShowError(ex.Message.ToString());
                    }
                }
                #endregion
            }
            // changed by wjz 20151225 为护理、收款修改起始页 △
        }

        /// <summary>
        /// 设置第一级菜单
        /// </summary>
        private void SetOneMenu()
        {
            try
            {
                DataTable dt = this.bll.BllOrgFuncDT(LoginBLL.strOrgID, LoginBLL.pidBusiness);
                string strid = "";//功能ID
                string strName = "";//功能名称
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    strid = dt.Rows[i]["ffunc_id"].ToString();
                    strName = dt.Rows[i]["fname"].ToString();                   
                    ToolStripMenuItem childMenuItem = new ToolStripMenuItem();
                    childMenuItem.Name = strid;                   
                    childMenuItem.Text = strName;
                    this.toolStripDropDownButtonGo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            childMenuItem});

                    childMenuItem.Click += new System.EventHandler(this.childMenuItem_Click);                    
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 动态下级事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void childMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                ToolStripMenuItem control = (ToolStripMenuItem)sender;
                string strName = control.Name;
                Settings.Default.strBusinessPID = strName;
                m_toolbox.Show(dockPanel);
               // m_toolbox.GetxPanderList(strName);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }


		private void menuItemExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void menuItemSolutionExplorer_Click(object sender, System.EventArgs e)
		{
			m_solutionExplorer.Show(dockPanel);
		}

		private void menuItemPropertyWindow_Click(object sender, System.EventArgs e)
		{
			//m_propertyWindow.Show(dockPanel);
		}

		private void menuItemToolbox_Click(object sender, System.EventArgs e)
        {
            //DummyToolbox.intFuncShowType = 0;
			m_toolbox.Show(dockPanel);
		}

		private void menuItemOutputWindow_Click(object sender, System.EventArgs e)
		{
			m_outputWindow.Show(dockPanel);
		}

		private void menuItemTaskList_Click(object sender, System.EventArgs e)
		{
			m_taskList.Show(dockPanel);
		}

		private void menuItemAbout_Click(object sender, System.EventArgs e)
		{
			AboutDialog aboutDialog = new AboutDialog();
			aboutDialog.ShowDialog(this);
		}
        /// <summary>
        /// 查找窗口打开否
        /// </summary>
        /// <param name="fname"></param>
        private bool DocOK(string fname)
        {
            bool bl = false;
            foreach (IDockContent content in MainForm.dockPanel.Documents)
            {
                if (content.DockHandler.TabText == fname)
                {
                    content.DockHandler.Show();
                    bl = true;
                }
            }
            return bl;
        }
		public  IDockContent FindDocument(string text)
		{
			if (dockPanel.DocumentStyle == DocumentStyle.SystemMdi)
			{
				foreach (Form form in MdiChildren)
					if (form.Text == text)
						return form as IDockContent;
				
				return null;
			}
			else
			{
				foreach (IDockContent content in dockPanel.Documents)
					if (content.DockHandler.TabText == text)
						return content;

				return null;
			}
		}

		private void menuItemNew_Click(object sender, System.EventArgs e)
		{
			DummyDoc dummyDoc = CreateNewDocument();
			if (dockPanel.DocumentStyle == DocumentStyle.SystemMdi)
			{
				dummyDoc.MdiParent = this;
				dummyDoc.Show();
			}
			else
				dummyDoc.Show(dockPanel);
		}

		private DummyDoc CreateNewDocument()
		{
			DummyDoc dummyDoc = new DummyDoc();

			int count = 1;
			//string text = "C:\\MADFDKAJ\\ADAKFJASD\\ADFKDSAKFJASD\\ASDFKASDFJASDF\\ASDFIJADSFJ\\ASDFKDFDA" + count.ToString();
            string text = "Document" + count.ToString();
            while (FindDocument(text) != null)
			{
				count ++;
                //text = "C:\\MADFDKAJ\\ADAKFJASD\\ADFKDSAKFJASD\\ASDFKASDFJASDF\\ASDFIJADSFJ\\ASDFKDFDA" + count.ToString();
                text = "Document" + count.ToString();
            }
			dummyDoc.Text = text;
			return dummyDoc;
		}

		private DummyDoc CreateNewDocument(string text)
		{
			DummyDoc dummyDoc = new DummyDoc();
			dummyDoc.Text = text;
			return dummyDoc;
		}

		private void menuItemOpen_Click(object sender, System.EventArgs e)
		{
			OpenFileDialog openFile = new OpenFileDialog();

			openFile.InitialDirectory = Application.ExecutablePath;
			openFile.Filter = "rtf files (*.rtf)|*.rtf|txt files (*.txt)|*.txt|All files (*.*)|*.*" ;
			openFile.FilterIndex = 1;
			openFile.RestoreDirectory = true ;

			if(openFile.ShowDialog() == DialogResult.OK)
			{
				string fullName = openFile.FileName;
				string fileName = Path.GetFileName(fullName);

				if (FindDocument(fileName) != null)
				{
					MessageBox.Show("The document: " + fileName + " has already opened!");
					return;
				}

				DummyDoc dummyDoc = new DummyDoc();
				dummyDoc.Text = fileName;
				dummyDoc.Show(dockPanel);
				try
				{
					dummyDoc.FileName = fullName;
				}
				catch (Exception exception)
				{
					dummyDoc.Close();
					MessageBox.Show(exception.Message);
				}

			}
		}

		private void menuItemFile_Popup(object sender, System.EventArgs e)
		{
			if (dockPanel.DocumentStyle == DocumentStyle.SystemMdi)
			{
				menuItemClose.Enabled = menuItemCloseAll.Enabled = (ActiveMdiChild != null);
			}
			else
			{
				menuItemClose.Enabled = (dockPanel.ActiveDocument != null);
				menuItemCloseAll.Enabled = (dockPanel.DocumentsCount > 0);
			}
		}

		private void menuItemClose_Click(object sender, System.EventArgs e)
		{
			if (dockPanel.DocumentStyle == DocumentStyle.SystemMdi)
				ActiveMdiChild.Close();
			else if (dockPanel.ActiveDocument != null)
				dockPanel.ActiveDocument.DockHandler.Close();
		}

		private void menuItemCloseAll_Click(object sender, System.EventArgs e)
		{
			CloseAllDocuments();
		}

		private void CloseAllDocuments()
		{
			if (dockPanel.DocumentStyle == DocumentStyle.SystemMdi)
			{
				foreach (Form form in MdiChildren)
					form.Close();
			}
			else
			{
                IDockContent[] documents = dockPanel.DocumentsToArray();
                foreach (IDockContent content in documents)
                    content.DockHandler.Close();
			}
		}

        private IDockContent GetContentFromPersistString(string persistString)
        {

            if (persistString == typeof(DummySolutionExplorer).ToString())
                return m_solutionExplorer;
            //se if (persistString == typeof(DummyPropertyWindow).ToString())
            //return m_propertyWindow;
            else if (persistString == typeof(DummyToolbox).ToString())
                return m_toolbox;
            else if (persistString == typeof(DummyOutputWindow).ToString())
                return m_outputWindow;
            else if (persistString == typeof(DummyTaskList).ToString())
                return m_taskList;
            else
            {
                string[] parsedStrings = persistString.Split(new char[] { ',' });
                if (parsedStrings.Length != 3)
                    return null;

                if (parsedStrings[0] != typeof(DummyDoc).ToString())
                    return null;

                DummyDoc dummyDoc = new DummyDoc();
                if (parsedStrings[1] != string.Empty)
                    dummyDoc.FileName = parsedStrings[1];
                if (parsedStrings[2] != string.Empty)
                    dummyDoc.Text = parsedStrings[2];

                return dummyDoc;
            }
        }
        /// <summary>
        /// 打开登录Form
        /// </summary>
        /// 20150610 添加返回值 wjz begin
        private bool ShowLoginForm()
        {
            bool ret = false;
            try
            {
                this.Hide();
                LoginForm frmlogin = new LoginForm();
                if (frmlogin.ShowDialog() == DialogResult.OK)
                {
                    frmlogin.Dispose();
                    //20150610 begin wjz //添加默认仪器的设置窗口
                    SetDefaultInstrument(0);
                    //20150610 end wjz //添加默认仪器的设置窗口
                    this.Show();
                    ret = true;
                }
                else
                {
                    frmlogin.Dispose();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            return ret;
        }
        // 20150610 添加返回值 wjz end

		

		

		private void menuItemToolBar_Click(object sender, System.EventArgs e)
		{
			toolBar.Visible = menuItemToolBar.Checked = !menuItemToolBar.Checked;
		}

		private void menuItemStatusBar_Click(object sender, System.EventArgs e)
		{
			statusBar.Visible = menuItemStatusBar.Checked = !menuItemStatusBar.Checked;
		}

        

		private void menuItemNewWindow_Click(object sender, System.EventArgs e)
		{
			MainForm newWindow = new MainForm();
			newWindow.Text = newWindow.Text + " - New";
			newWindow.Show();
		}

		private void menuItemTools_Popup(object sender, System.EventArgs e)
		{
            menuItemLockLayout.Checked = !MainForm.dockPanel.AllowEndUserDocking;
		}

		private void menuItemLockLayout_Click(object sender, System.EventArgs e)
		{
			dockPanel.AllowEndUserDocking = !dockPanel.AllowEndUserDocking;
		}

		private void menuItemLayoutByCode_Click(object sender, System.EventArgs e)
		{
			dockPanel.SuspendLayout(true);

			m_solutionExplorer.Show(dockPanel, DockState.DockRight);
		//	m_propertyWindow.Show(m_solutionExplorer.Pane, m_solutionExplorer);
			m_toolbox.Show(dockPanel, new Rectangle(98, 133, 200, 383));
			m_outputWindow.Show(m_solutionExplorer.Pane, DockAlignment.Bottom, 0.35);
			m_taskList.Show(m_toolbox.Pane, DockAlignment.Left, 0.4);

			CloseAllDocuments();
			DummyDoc doc1 = CreateNewDocument("Document1");
			DummyDoc doc2 = CreateNewDocument("Document2");
			DummyDoc doc3 = CreateNewDocument("Document3");
			DummyDoc doc4 = CreateNewDocument("Document4");
			doc1.Show(dockPanel, DockState.Document);
			doc2.Show(doc1.Pane, null);
			doc3.Show(doc1.Pane, DockAlignment.Bottom, 0.5);
			doc4.Show(doc3.Pane, DockAlignment.Right, 0.5);

			dockPanel.ResumeLayout(true, true);
		}

		private void menuItemLayoutByXml_Click(object sender, System.EventArgs e)
		{
			dockPanel.SuspendLayout(true);

			// In order to load layout from XML, we need to close all the DockContents
			CloseAllContents();

			Assembly assembly = Assembly.GetAssembly(typeof(MainForm));
			Stream xmlStream = assembly.GetManifestResourceStream("DockSample.Resources.DockPanel.xml");
			dockPanel.LoadFromXml(xmlStream, m_deserializeDockContent);
			xmlStream.Close();

			dockPanel.ResumeLayout(true, true);
		}

		private void CloseAllContents()
		{
			// we don't want to create another instance of tool window, set DockPanel to null
			m_solutionExplorer.DockPanel = null;
			//m_propertyWindow.DockPanel = null;
			m_toolbox.DockPanel = null;
			m_outputWindow.DockPanel = null;
			m_taskList.DockPanel = null;

			// Close all other document windows
			CloseAllDocuments();
		}

		private void SetSchema(object sender, System.EventArgs e)
		{
			CloseAllContents();

			if (sender == menuItemSchemaVS2005)
				Extender.SetSchema(dockPanel, Extender.Schema.VS2005);
			else if (sender == menuItemSchemaVS2003)
				Extender.SetSchema(dockPanel, Extender.Schema.VS2003);

            menuItemSchemaVS2005.Checked = (sender == menuItemSchemaVS2005);
            menuItemSchemaVS2003.Checked = (sender == menuItemSchemaVS2003);
		}

		private void SetDocumentStyle(object sender, System.EventArgs e)
		{
			DocumentStyle oldStyle = dockPanel.DocumentStyle;
			DocumentStyle newStyle;
			if (sender == menuItemDockingMdi)
				newStyle = DocumentStyle.DockingMdi;
			else if (sender == menuItemDockingWindow)
				newStyle = DocumentStyle.DockingWindow;
			else if (sender == menuItemDockingSdi)
				newStyle = DocumentStyle.DockingSdi;
			else
				newStyle = DocumentStyle.SystemMdi;
			
			if (oldStyle == newStyle)
				return;

			if (oldStyle == DocumentStyle.SystemMdi || newStyle == DocumentStyle.SystemMdi)
				CloseAllDocuments();

			dockPanel.DocumentStyle = newStyle;
			menuItemDockingMdi.Checked = (newStyle == DocumentStyle.DockingMdi);
			menuItemDockingWindow.Checked = (newStyle == DocumentStyle.DockingWindow);
			menuItemDockingSdi.Checked = (newStyle == DocumentStyle.DockingSdi);
			menuItemSystemMdi.Checked = (newStyle == DocumentStyle.SystemMdi);
			menuItemLayoutByCode.Enabled = (newStyle != DocumentStyle.SystemMdi);
			menuItemLayoutByXml.Enabled = (newStyle != DocumentStyle.SystemMdi);
			//toolBarButtonLayoutByCode.Enabled = (newStyle != DocumentStyle.SystemMdi);
			//toolBarButtonLayoutByXml.Enabled = (newStyle != DocumentStyle.SystemMdi);
		}

		private void menuItemCloseAllButThisOne_Click(object sender, System.EventArgs e)
		{
			if (dockPanel.DocumentStyle == DocumentStyle.SystemMdi)
			{
				Form activeMdi = ActiveMdiChild;
				foreach (Form form in MdiChildren)
				{
					if (form != activeMdi)
						form.Close();
				}
			}
			else
			{
				foreach (IDockContent document in dockPanel.Documents)
				{
					if (!document.DockHandler.IsActivated)
						document.DockHandler.Close();
				}
			}
		}

		private void menuItemShowDocumentIcon_Click(object sender, System.EventArgs e)
		{
			dockPanel.ShowDocumentIcon = menuItemShowDocumentIcon.Checked= !menuItemShowDocumentIcon.Checked;
		}

        private void showRightToLeft_Click(object sender, EventArgs e)
        {
            CloseAllContents();
            if (showRightToLeft.Checked)
            {
                this.RightToLeft = RightToLeft.No;
                this.RightToLeftLayout = false;
            }
            else
            {
                this.RightToLeft = RightToLeft.Yes;
                this.RightToLeftLayout = true;
            }
            m_solutionExplorer.RightToLeftLayout = this.RightToLeftLayout;
            showRightToLeft.Checked = !showRightToLeft.Checked;
        }

        private void exitWithoutSavingLayout_Click(object sender, EventArgs e)
        {
            m_bSaveLayout = false;
            Close();
            m_bSaveLayout = true;
        }

       
  
       
        private void toolBar_ButtonClick(object sender, System.Windows.Forms.ToolStripItemClickedEventArgs e)
        {
           // if (e.ClickedItem == toolStripSplitButtonStart)
             // /  menuItemToolbox_Click(null, null);

          //  else if (e.Cli/ckedItem == toolStripButtonFuncTree)
              //  menuItemPropertyWindow_Click(null, null);
           //else if (e.ClickedItem == toolBarButtonNew)
               // menuItemNew_Click(null, null);
           // else if (e.ClickedItem == toolBarButtonOpen)
               // menuItemOpen_Click(null, null);
          //  else if (e.ClickedItem == toolBarButtonSolutionExplorer)
               // menuItemSolutionExplorer_Click(null, null);
            //else if (e.ClickedItem == toolBarButtonPropertyWindow)
               // menuItemPropertyWindow_Click(null, null);
            
            //else if (e.ClickedItem == toolBarButtonOutputWindow)
               // menuItemOutputWindow_Click(null, null);
            //else if (e.ClickedItem == toolBarButtonTaskList)
               // menuItemTaskList_Click(null, null);
           // else if (e.ClickedItem == toolBarButtonLayoutByCode)
               // menuItemLayoutByCode_Click(null, null);
            //else if (e.ClickedItem == toolBarButtonLayoutByXml)
                //menuItemLayoutByXml_Click(null, null);
                /*
            else if (e.ClickedItem == toolStripButtonReportIn)
            {
                Lis.JY.ReportInForm fun = new TaoNet.Client.WinForm.Lis.JY.ReportInForm();
              
                fun.TabText = "报告录入";
                fun.Show(dockPanel);
            }
            else if (e.ClickedItem == toolStripButtonHelp)
            {

                Sys.HelpCat abo = new TaoNet.Client.WinForm.Sys.HelpCat();    
                abo.TabText = "帮助";
                abo.Show(dockPanel);
            }*/
        }

        private void toolStripButtonExit_Click(object sender, EventArgs e)
        {
            if (WWMessage.MessageDialogResult("确定退出系统？"))
            {
                Close();
            }
        }

        
        private void 关闭所有功能ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllDocuments();
        }


        private void MainForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

            Settings.Default.Save();
            string configFile = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "DockPanel.config");
            if (m_bSaveLayout)
                dockPanel.SaveAsXml(configFile);
            else if (File.Exists(configFile))
                File.Delete(configFile);
        }

        private void toolStripMenuItemAbout_Click(object sender, EventArgs e)
        {
           // Sys.HelpCat abo = new Sys.HelpCat();
            //bo.TabText = "帮助";
            //abo.Show();
            try
            {
                HelpForm help = new HelpForm("");
                help.Show();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

     

        private void toolStripMenuItemPass_Click(object sender, EventArgs e)
        {
            try
            {
                PersonPassForm padd = new PersonPassForm(ww.wwf.wwfbll.LoginBLL.strPersonID);
                padd.ShowDialog();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        


        private void toolStripButtonHelp_Click(object sender, EventArgs e)
        {
            try
            {
                HelpForm help = new HelpForm("");
                help.Show();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }


        /// <summary>
        /// 快捷键 设置
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref   System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            try
            {
               
                if (keyData == (Keys.F4))
                {
                    toolStripDropDownButtonGo.ShowDropDown();
                    return true;
                }
                if (keyData == (Keys.F5))
                {
                    toolStripButton_ReportIn.PerformClick();
                    return true;
                }

                if (keyData == (Keys.F6))
                {
                    toolStripButton_ReportQuery.PerformClick();
                    return true;
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            return base.ProcessCmdKey(ref   msg, keyData);
        }
         
        private void toolStripMenuItemHardware_Click(object sender, EventArgs e)
        {
            try
            {
                ww.wwf.wwfbll.HardwareInfoForm hif = new HardwareInfoForm();
                hif.Show();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 显示窗口
        /// </summary>
        /// <param name="strFormName"></param>
        /// <returns></returns>
        private void GetFormByFormNameDockContent(string strFormName, string fname)
        {
            DockContent form = null;
            try
            {
                Type type = Type.GetType(strFormName);
                form = (DockContent)Activator.CreateInstance(type);
                if (DocOK(fname) == false)
                {
                    form.Text = fname;
                    form.TabText = fname;
                    form.ToolTipText = fname;
                    form.Show(dockPanel);
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError("窗体有误，请联系管理员；\n当前窗体名为：" + strFormName + "\n" + ex.ToString());
            }
        }
        private void toolStripButton_ReportIn_Click(object sender, EventArgs e)
        {
            GetFormByFormNameDockContent("ww.form.lis.sam.jy.JYForm", "报告录入");
        }

        private void toolStripButton_ReportQuery_Click(object sender, EventArgs e)
        {
            GetFormByFormNameDockContent("ww.form.lis.sam.jy.JYQueryForm", "报告查询");
        }

        private void toolStripButton_Item_Click(object sender, EventArgs e)
        {
            GetFormByFormNameDockContent("ww.form.lis.sam.dic.ItemForm", "项目维护");
        }

        private void toolStripButton_Intr_Click(object sender, EventArgs e)
        {
            GetFormByFormNameDockContent("ww.form.lis.sam.dic.InstrForm", "仪器维护");
        }
        private void toolStripButton_ReportSet_Click(object sender, EventArgs e)
        {
            GetFormByFormNameDockContent("ww.form.lis.sam.dic.InstrReportSetForm", "报表设置");
        }

        private void toolStripButton_Person_Click(object sender, EventArgs e)
        {
            try
            {
                //string fname = "人员";
                //if (DocOK(fname) == false)
                //{
                //    ww.form.wwf.Frm_人员管理 k_r = new ww.form.wwf.Frm_人员管理();
                //    k_r.TabText = fname;
                //    k_r.Show(dockPanel);
                //}
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButton_Dept_Click(object sender, EventArgs e)
        {
            try
            {
                //string fname = "科室";
                //if (DocOK(fname) == false)
                //{
                //    ww.form.wwf.Frm_部门管理 k_r = new ww.form.wwf.Frm_部门管理();
                //    k_r.TabText = fname;
                //    k_r.Show(dockPanel);
                //}
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void toolStripDropDownButtonGo_Click(object sender, EventArgs e)
        {
            m_toolbox.Show(dockPanel);
        }

        //20150610 begin wjz //添加默认仪器的设置窗口
        /// <summary>
        /// 设置本机默认仪器id
        /// </summary>
        /// <param name="type">0--表示设置入口是Login画面之后，1--表示设置画面启动入口是MainForm</param>
        private void SetDefaultInstrument(int type)
        {
            string InstrumentConfig = Application.StartupPath + "\\DefaultInstrument.cfg";
            if (type == 0)
            {
                if (File.Exists(InstrumentConfig))
                {
                    //判断文件中的数据是否正确,暂时不进行判断
                    //从文件中取出值
                    string instrID = "";
                    try
                    {
                        StreamReader reader = new StreamReader(InstrumentConfig);
                        instrID = reader.ReadLine();
                        reader.Close();
                    }
                    catch(Exception ex)
                    {
                        WWMessage.MessageShowError(ex.ToString());
                    }
                    //将默认仪器id存入静态类
                    ww.form.wwf.Customization.DefaultInstrument.SetInstrID(instrID);
                }
                else
                {
                    //新建文件，并写入默认设备id
                    DefaultInstSettingForm frm = new DefaultInstSettingForm(InstrumentConfig);
                    frm.ShowDialog();
                    frm.Dispose();
                }
            }
            else if (type == 1)
            {
                //新建文件，并写入默认设备id
                DefaultInstSettingForm frm = new DefaultInstSettingForm(InstrumentConfig);
                frm.ShowDialog();
                frm.Dispose();
            }
            else { }
        }

        private void toolStripButton默认仪器_Click(object sender, EventArgs e)
        {
            SetDefaultInstrument(1);
        }

        private void toolStripButtonUpdate_Click(object sender, EventArgs e)
        {
            if (System.IO.File.Exists("uplistdo.exe") == false)
            {
                MessageBox.Show("升级工具丢失，请联系软件维护人员处理故障!", "消息", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            Process.Start("uplistdo.exe");
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FrmSignChecker frm = new FrmSignChecker();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                toolStripStatusLabelChecker.Text = "　　审核医师：" + LoginBLL.strCheckName;
            }
        }
        //20150610 end wjz //添加默认仪器的设置窗口
      
    }
}