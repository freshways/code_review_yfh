﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;
namespace ww.form.wwf
{
    public partial class PositionForm : SysBaseForm
    {
        PositionBLL bll = new PositionBLL();
        DataRowView rowCurrent = null;
        DataTable dt = new DataTable();
        public PositionForm()
        {
            InitializeComponent();
        }

        private void PositionForm_Load(object sender, EventArgs e)
        {
            try
            {

                this.DataGridViewObject.AutoGenerateColumns = false;
                DataGridViewObject.DataSource = this.wwf_positionBindingSource;
                GetDT();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 取得所有列表
        /// </summary>
        public void GetDT()
        {
            if (dt != null)
                dt.Clear();
            dt = this.bll.BllDT();
            this.wwf_positionBindingSource.DataSource = dt;
        }
        /// <summary>
        /// 位置改变时，到得当前记录对象
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wwf_positionBindingSource_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                rowCurrent = (DataRowView)wwf_positionBindingSource.Current;//行
                /*
                string fname = "";
                if (rowCurrent != null)
                {
                    fname = drObject["fname"].ToString();
                    MessageBox.Show(fname);
                }*/
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
       
       

       

        private void toolStripButtonR_Click(object sender, EventArgs e)
        {
            try
            {

                this.DataGridViewObject.AutoGenerateColumns = false;
                DataGridViewObject.DataSource = this.wwf_positionBindingSource;
                GetDT();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonDel_Click(object sender, EventArgs e)
        {
            try
            {
                if (rowCurrent != null)
                {
                    string fposition_id = "";
                    fposition_id = rowCurrent["fposition_id"].ToString();
                    if (fposition_id == "1")
                    {
                        ww.wwf.wwfbll.WWMessage.MessageShowWarning("此岗位不可删除！");
                    }
                    else
                    {
                        if (this.bll.BllDelete(fposition_id) > 0)
                            GetDT();
                    }
                }

            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (rowCurrent != null)
                {
                    if (this.bll.BllUpdate(rowCurrent) > 0)
                        GetDT();
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonAdd_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow dr = null;
                dr = dt.NewRow();
                string guid = this.bll.DbGuid();
                dr["fposition_id"] = guid;
                dr["fname"] = "";
                dr["forder_by"] = "0";
                dr["fuse_flag"] = "1";
                dt.Rows.Add(dr);
                this.wwf_positionBindingSource.Position = wwf_positionBindingSource.Count;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonS_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.bll.BllAdd(rowCurrent) > 0)
                    GetDT();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

       
    }
}