﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ww.wwf.com;
using ww.wwf.wwfbll;

namespace ww.form
{
    /// <summary>
    /// 2019年11月19日 yfh 添加公共类，主要处理一些第三方的调用问题
    /// 重复调用的方法，提取到本类下面
    /// </summary>
    public class Common
    {
        /// <summary>
        /// 读电子健康卡
        /// </summary>
        /// <param name="cardno"></param>
        /// <returns></returns>
        public static JObject ExecuteReq(string cardno)
        {
            try
            {
                Ylzehc.Ylzehc.ApiUrl = LoginBLL.readurl;   // 可按系统配置赋值
                Ylzehc.Ylzehc.AppKey = LoginBLL.readappkey;   // 可按系统配置赋值

                // 请求头
                JObject request = new JObject();
                request["app_id"] = LoginBLL.readappId;   // 可按系统配置赋值
                request["term_id"] = LoginBLL.term_id;   // 可按系统配置赋值
                request["method"] = "ehc.ehealthcode.verify"; // *** 根据具体调用接口入参 *** 
                request["timestamp"] = DateTime.Now.ToString("yyyyMMddHHmmss");   // *** 根据具体调用时间入参 *** 
                request["sign_type"] = "MD5";   // 固定，支持MD5\SM3，SDK内部会自动根据该算法计算sign
                request["version"] = "X.M.0.1";   // 固定
                request["enc_type"] = "AES";    //  固定，支持AES\SM4，SDK内部会自动根据该算法加密

                // 业务参数

                // **********根据接口文档入参，当前仅为二维码验证示例**********//
                JObject bizParam = new JObject();
                bizParam["ehealth_code"] = cardno;//"3C7600201CD41759A266852EB9FF24A5B486290A778E31ED68EA25DEC1D370F4:1::3502A0001:";
                bizParam["out_verify_time"] = DateTime.Now.ToString("yyyyMMddHHmmss");
                bizParam["out_verify_no"] = System.Guid.NewGuid().ToString("N"); // 唯一编号
                bizParam["operator_id"] = "001";
                bizParam["operator_name"] = "测试";
                bizParam["treatment_code"] = "010101";
                // **********根据接口文档入参，当前仅为二维码验证示例**********//

                request["biz_content"] = JsonConvert.SerializeObject(bizParam);

                JObject res = Ylzehc.Ylzehc.Execute(request);
                if (res["ret_code"].ToString().Equals("0000"))
                {
                    JObject user = JObject.Parse(res["biz_content"].ToString());
                    return user;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// 结果推送
        /// </summary>
        /// <param name="user"></param>
        public static void SendEcardMessage(JObject user)
        {
            try
            {
                string s姓名 = user["Name"].ToString();
                string bcard = user["bcard"].ToString();
                string reportid = user["reportid"].ToString();
                //MessageBox.Show("推送成功！");
                string message = "";
                string url = LoginBLL.sendurl;//"http://192.168.10.171:1811/ehc-portal-push/sms/send";
                // 请求头
                JObject request = new JObject();
                request["appId"] = LoginBLL.sendappId;//"1DNUJKJV00000100007F0000F0308269";
                request["transType"] = "sms.send.checkresult";
                // **********子参数**********//
                JObject param = new JObject();
                //三级参数
                JObject content = new JObject();
                content["first"] = "sms.send.checkresult.first";
                content["keyword1"] = s姓名;//"赵立华";
                content["keyword2"] = bcard;//"17Y9400000448";
                content["keyword3"] = "检查检验";
                content["keyword4"] = "化验室";
                content["remark"] = "sms.send.checkresult.remark";
                param["content"] = content;//三级参数填充到二级参数

                param["redirectUrl"] = "";
                param["cardNo"] = bcard;//"17Y9400000448";
                param["cardType"] = "11";
                //三级参数
                JObject extraParams = new JObject();
                extraParams["reportNo"] = reportid;
                extraParams["reportType"] = "2";
                extraParams["reportCategoryCode"] = "2";
                param["extraParams"] = extraParams;//三级参数填充到二级参数
                //最后汇总
                request["param"] = param;
                // **********子参数**********//
                string data = JsonConvert.SerializeObject(request);

                WebApiMethod.RequestJsonPostMethod(url, data, out message);
            }
            catch
            {
                
            }
        }
    }
}
