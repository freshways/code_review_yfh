﻿namespace ww.form.lis.zk
{
    partial class gzlForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(gzlForm));
            this.bN = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonQuery = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPrint = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPrintYL = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonJCSH = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSplitButtonGZ = new System.Windows.Forms.ToolStripSplitButton();
            this.报表格式ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.结果模式ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButtonHelp = new System.Windows.Forms.ToolStripButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dtReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).BeginInit();
            this.bN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // bN
            // 
            this.bN.AddNewItem = null;
            this.bN.AutoSize = false;
            this.bN.CountItem = this.bindingNavigatorCountItem;
            this.bN.DeleteItem = null;
            this.bN.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.toolStripSeparator5,
            this.toolStripButtonQuery,
            this.toolStripButtonPrint,
            this.toolStripButtonPrintYL,
            this.toolStripButtonJCSH,
            this.toolStripSeparator6,
            this.toolStripSplitButtonGZ,
            this.toolStripButtonHelp});
            this.bN.Location = new System.Drawing.Point(0, 0);
            this.bN.MoveFirstItem = null;
            this.bN.MoveLastItem = null;
            this.bN.MoveNextItem = null;
            this.bN.MovePreviousItem = null;
            this.bN.Name = "bN";
            this.bN.PositionItem = this.bindingNavigatorPositionItem;
            this.bN.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bN.Size = new System.Drawing.Size(864, 30);
            this.bN.TabIndex = 148;
            this.bN.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(48, 27);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 30);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonQuery
            // 
            this.toolStripButtonQuery.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonQuery.Image")));
            this.toolStripButtonQuery.Name = "toolStripButtonQuery";
            this.toolStripButtonQuery.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonQuery.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonQuery.Size = new System.Drawing.Size(100, 27);
            this.toolStripButtonQuery.Text = "查询(F5) ";
            // 
            // toolStripButtonPrint
            // 
            this.toolStripButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPrint.Image")));
            this.toolStripButtonPrint.Name = "toolStripButtonPrint";
            this.toolStripButtonPrint.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonPrint.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonPrint.Size = new System.Drawing.Size(100, 27);
            this.toolStripButtonPrint.Text = "打印(F8) ";
            // 
            // toolStripButtonPrintYL
            // 
            this.toolStripButtonPrintYL.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPrintYL.Image")));
            this.toolStripButtonPrintYL.Name = "toolStripButtonPrintYL";
            this.toolStripButtonPrintYL.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonPrintYL.Size = new System.Drawing.Size(132, 27);
            this.toolStripButtonPrintYL.Text = "打印预览(F9) ";
            // 
            // toolStripButtonJCSH
            // 
            this.toolStripButtonJCSH.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonJCSH.Image")));
            this.toolStripButtonJCSH.Name = "toolStripButtonJCSH";
            this.toolStripButtonJCSH.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonJCSH.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonJCSH.Size = new System.Drawing.Size(132, 27);
            this.toolStripButtonJCSH.Text = "解除审核(F10)";
            this.toolStripButtonJCSH.Visible = false;
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripSplitButtonGZ
            // 
            this.toolStripSplitButtonGZ.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.报表格式ToolStripMenuItem,
            this.结果模式ToolStripMenuItem});
            this.toolStripSplitButtonGZ.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButtonGZ.Image")));
            this.toolStripSplitButtonGZ.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButtonGZ.Name = "toolStripSplitButtonGZ";
            this.toolStripSplitButtonGZ.Size = new System.Drawing.Size(88, 27);
            this.toolStripSplitButtonGZ.Text = "格式  ";
            // 
            // 报表格式ToolStripMenuItem
            // 
            this.报表格式ToolStripMenuItem.Name = "报表格式ToolStripMenuItem";
            this.报表格式ToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.报表格式ToolStripMenuItem.Text = "报表格式(&R)";
            // 
            // 结果模式ToolStripMenuItem
            // 
            this.结果模式ToolStripMenuItem.Name = "结果模式ToolStripMenuItem";
            this.结果模式ToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.结果模式ToolStripMenuItem.Text = "结果模式(&S)";
            // 
            // toolStripButtonHelp
            // 
            this.toolStripButtonHelp.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHelp.Image")));
            this.toolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonHelp.Name = "toolStripButtonHelp";
            this.toolStripButtonHelp.Size = new System.Drawing.Size(108, 27);
            this.toolStripButtonHelp.Text = "帮助(F1)  ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 30);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(913, 431);
            this.pictureBox1.TabIndex = 149;
            this.pictureBox1.TabStop = false;
            // 
            // gzlForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(864, 461);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.bN);
            this.Name = "gzlForm";
            this.Text = "gzlForm";
            this.Load += new System.EventHandler(this.gzlForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).EndInit();
            this.bN.ResumeLayout(false);
            this.bN.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.BindingNavigator bN;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        protected System.Windows.Forms.ToolStripButton toolStripButtonQuery;
        private System.Windows.Forms.ToolStripButton toolStripButtonPrint;
        private System.Windows.Forms.ToolStripButton toolStripButtonPrintYL;
        private System.Windows.Forms.ToolStripButton toolStripButtonJCSH;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButtonGZ;
        private System.Windows.Forms.ToolStripMenuItem 报表格式ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 结果模式ToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButtonHelp;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}