﻿namespace ww.form.lis.sam.barcode
{
    partial class FrmOutPatBarcodeQuery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dataGridViewPrintedList = new System.Windows.Forms.DataGridView();
            this.ColumnDateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnID2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnbarcodePrinted = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column状态 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnCardNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnSex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column套餐编号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column套餐名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column收费编码2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnItemType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPrintedList)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataGridViewPrintedList);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 65);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(778, 370);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "条码列表";
            // 
            // dataGridViewPrintedList
            // 
            this.dataGridViewPrintedList.AllowUserToAddRows = false;
            this.dataGridViewPrintedList.AllowUserToDeleteRows = false;
            this.dataGridViewPrintedList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPrintedList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnDateTime,
            this.ColumnID2,
            this.ColumnbarcodePrinted,
            this.Column状态,
            this.ColumnCardNo,
            this.ColumnName,
            this.ColumnSex,
            this.Column套餐编号,
            this.Column套餐名称,
            this.Column收费编码2,
            this.ColumnItemType});
            this.dataGridViewPrintedList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewPrintedList.Location = new System.Drawing.Point(3, 17);
            this.dataGridViewPrintedList.Name = "dataGridViewPrintedList";
            this.dataGridViewPrintedList.RowTemplate.Height = 23;
            this.dataGridViewPrintedList.Size = new System.Drawing.Size(772, 350);
            this.dataGridViewPrintedList.TabIndex = 0;
            // 
            // ColumnDateTime
            // 
            this.ColumnDateTime.DataPropertyName = "时间";
            this.ColumnDateTime.HeaderText = "时间";
            this.ColumnDateTime.Name = "ColumnDateTime";
            this.ColumnDateTime.ReadOnly = true;
            this.ColumnDateTime.Width = 80;
            // 
            // ColumnID2
            // 
            this.ColumnID2.DataPropertyName = "ID";
            this.ColumnID2.HeaderText = "ID";
            this.ColumnID2.Name = "ColumnID2";
            this.ColumnID2.ReadOnly = true;
            this.ColumnID2.Visible = false;
            this.ColumnID2.Width = 50;
            // 
            // ColumnbarcodePrinted
            // 
            this.ColumnbarcodePrinted.DataPropertyName = "条码";
            this.ColumnbarcodePrinted.HeaderText = "条码";
            this.ColumnbarcodePrinted.Name = "ColumnbarcodePrinted";
            this.ColumnbarcodePrinted.Width = 120;
            // 
            // Column状态
            // 
            this.Column状态.DataPropertyName = "状态";
            this.Column状态.HeaderText = "状态";
            this.Column状态.Name = "Column状态";
            this.Column状态.ReadOnly = true;
            this.Column状态.Width = 60;
            // 
            // ColumnCardNo
            // 
            this.ColumnCardNo.DataPropertyName = "卡号";
            this.ColumnCardNo.HeaderText = "卡号";
            this.ColumnCardNo.Name = "ColumnCardNo";
            this.ColumnCardNo.ReadOnly = true;
            this.ColumnCardNo.Width = 90;
            // 
            // ColumnName
            // 
            this.ColumnName.DataPropertyName = "姓名";
            this.ColumnName.HeaderText = "姓名";
            this.ColumnName.Name = "ColumnName";
            this.ColumnName.ReadOnly = true;
            this.ColumnName.Width = 65;
            // 
            // ColumnSex
            // 
            this.ColumnSex.DataPropertyName = "性别";
            this.ColumnSex.HeaderText = "性别";
            this.ColumnSex.Name = "ColumnSex";
            this.ColumnSex.ReadOnly = true;
            this.ColumnSex.Width = 60;
            // 
            // Column套餐编号
            // 
            this.Column套餐编号.DataPropertyName = "套餐编号";
            this.Column套餐编号.HeaderText = "套餐编号";
            this.Column套餐编号.Name = "Column套餐编号";
            this.Column套餐编号.ReadOnly = true;
            this.Column套餐编号.Visible = false;
            this.Column套餐编号.Width = 80;
            // 
            // Column套餐名称
            // 
            this.Column套餐名称.DataPropertyName = "套餐名称";
            this.Column套餐名称.HeaderText = "套餐名称";
            this.Column套餐名称.Name = "Column套餐名称";
            this.Column套餐名称.ReadOnly = true;
            this.Column套餐名称.Width = 80;
            // 
            // Column收费编码2
            // 
            this.Column收费编码2.DataPropertyName = "收费编码";
            this.Column收费编码2.HeaderText = "收费编码";
            this.Column收费编码2.Name = "Column收费编码2";
            this.Column收费编码2.ReadOnly = true;
            this.Column收费编码2.Visible = false;
            this.Column收费编码2.Width = 80;
            // 
            // ColumnItemType
            // 
            this.ColumnItemType.DataPropertyName = "收费名称";
            this.ColumnItemType.HeaderText = "收费名称";
            this.ColumnItemType.Name = "ColumnItemType";
            this.ColumnItemType.ReadOnly = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(778, 65);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "查询条件";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(347, 23);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 21);
            this.textBox1.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(300, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "姓名：";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(493, 23);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "查询";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(112, 23);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(147, 21);
            this.dateTimePicker1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "检验申请日期：";
            // 
            // FrmOutPatBarcodeQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(778, 435);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmOutPatBarcodeQuery";
            this.Text = "门诊条码查询";
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPrintedList)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dataGridViewPrintedList;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnID2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnbarcodePrinted;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column状态;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnCardNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSex;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column套餐编号;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column套餐名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column收费编码2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnItemType;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
    }
}