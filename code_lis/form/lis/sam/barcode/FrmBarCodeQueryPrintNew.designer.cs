﻿namespace ww.form.lis.sam.barcode
{
    partial class FrmBarCodeQueryPrintNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxBed = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxRoom = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxDept = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxAge = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxSex = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnQuery = new System.Windows.Forms.Button();
            this.textBoxJZH = new System.Windows.Forms.TextBox();
            this.dataGridViewDetailList = new System.Windows.Forms.DataGridView();
            this.ColumnCheckbox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColumnJYID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col套餐名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column收费编码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnSFMC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnFyje = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Columnysbh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Columnysxm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Columnbgdbh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Columnbgdmc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dataGridViewPrintedList = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.复制条码ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnBarCodeListQuery = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridViewPeople = new System.Windows.Forms.DataGridView();
            this.Column收款日期 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnMZID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column姓名 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.套餐名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column收费名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column金额 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column医生姓名 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column科室名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnPeopleQuery = new System.Windows.Forms.Button();
            this.checkBoxQueryAll = new System.Windows.Forms.CheckBox();
            this.textBoxSNO = new System.Windows.Forms.TextBox();
            this.textBoxQueryName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dateTimePickerQueryDate = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ColumnDateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnID2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnbarcodePrinted = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column状态 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnCardNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnSex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column套餐编号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column套餐名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column收费编码2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnItemType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetailList)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPrintedList)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPeople)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "就诊号：";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxBed);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.textBoxRoom);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBoxDept);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBoxAge);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBoxSex);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBoxName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnQuery);
            this.groupBox1.Controls.Add(this.textBoxJZH);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(892, 95);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "基本信息";
            // 
            // textBoxBed
            // 
            this.textBoxBed.BackColor = System.Drawing.Color.Black;
            this.textBoxBed.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBoxBed.ForeColor = System.Drawing.Color.Yellow;
            this.textBoxBed.Location = new System.Drawing.Point(757, 63);
            this.textBoxBed.Name = "textBoxBed";
            this.textBoxBed.ReadOnly = true;
            this.textBoxBed.Size = new System.Drawing.Size(96, 21);
            this.textBoxBed.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(710, 66);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 13;
            this.label7.Text = "床号：";
            // 
            // textBoxRoom
            // 
            this.textBoxRoom.BackColor = System.Drawing.Color.Black;
            this.textBoxRoom.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBoxRoom.ForeColor = System.Drawing.Color.Yellow;
            this.textBoxRoom.Location = new System.Drawing.Point(608, 63);
            this.textBoxRoom.Name = "textBoxRoom";
            this.textBoxRoom.ReadOnly = true;
            this.textBoxRoom.Size = new System.Drawing.Size(96, 21);
            this.textBoxRoom.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(561, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 11;
            this.label6.Text = "病室：";
            // 
            // textBoxDept
            // 
            this.textBoxDept.BackColor = System.Drawing.Color.Black;
            this.textBoxDept.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBoxDept.ForeColor = System.Drawing.Color.Yellow;
            this.textBoxDept.Location = new System.Drawing.Point(459, 63);
            this.textBoxDept.Name = "textBoxDept";
            this.textBoxDept.ReadOnly = true;
            this.textBoxDept.Size = new System.Drawing.Size(96, 21);
            this.textBoxDept.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(388, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "科室名称：";
            // 
            // textBoxAge
            // 
            this.textBoxAge.BackColor = System.Drawing.Color.Black;
            this.textBoxAge.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBoxAge.ForeColor = System.Drawing.Color.Yellow;
            this.textBoxAge.Location = new System.Drawing.Point(333, 63);
            this.textBoxAge.Name = "textBoxAge";
            this.textBoxAge.ReadOnly = true;
            this.textBoxAge.Size = new System.Drawing.Size(49, 21);
            this.textBoxAge.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(286, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "年龄：";
            // 
            // textBoxSex
            // 
            this.textBoxSex.BackColor = System.Drawing.Color.Black;
            this.textBoxSex.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBoxSex.ForeColor = System.Drawing.Color.Yellow;
            this.textBoxSex.Location = new System.Drawing.Point(234, 63);
            this.textBoxSex.Name = "textBoxSex";
            this.textBoxSex.ReadOnly = true;
            this.textBoxSex.Size = new System.Drawing.Size(46, 21);
            this.textBoxSex.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(184, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "性别：";
            // 
            // textBoxName
            // 
            this.textBoxName.BackColor = System.Drawing.Color.Black;
            this.textBoxName.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBoxName.ForeColor = System.Drawing.Color.Yellow;
            this.textBoxName.Location = new System.Drawing.Point(89, 63);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.ReadOnly = true;
            this.textBoxName.Size = new System.Drawing.Size(89, 21);
            this.textBoxName.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "病人姓名：";
            // 
            // btnQuery
            // 
            this.btnQuery.Image = global::ww.form.Properties.Resources.wwNavigatorQuery;
            this.btnQuery.Location = new System.Drawing.Point(316, 24);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(120, 23);
            this.btnQuery.TabIndex = 2;
            this.btnQuery.Text = "查看人员信息";
            this.btnQuery.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // textBoxJZH
            // 
            this.textBoxJZH.Location = new System.Drawing.Point(89, 26);
            this.textBoxJZH.Name = "textBoxJZH";
            this.textBoxJZH.Size = new System.Drawing.Size(221, 21);
            this.textBoxJZH.TabIndex = 1;
            // 
            // dataGridViewDetailList
            // 
            this.dataGridViewDetailList.AllowUserToAddRows = false;
            this.dataGridViewDetailList.AllowUserToDeleteRows = false;
            this.dataGridViewDetailList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDetailList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnCheckbox,
            this.ColumnJYID,
            this.Col套餐名称,
            this.Column收费编码,
            this.ColumnSFMC,
            this.ColumnFyje,
            this.Columnysbh,
            this.Columnysxm,
            this.Columnbgdbh,
            this.Columnbgdmc});
            this.dataGridViewDetailList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewDetailList.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewDetailList.Name = "dataGridViewDetailList";
            this.dataGridViewDetailList.RowTemplate.Height = 23;
            this.dataGridViewDetailList.Size = new System.Drawing.Size(892, 373);
            this.dataGridViewDetailList.TabIndex = 2;
            this.dataGridViewDetailList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewDetailList_CellClick);
            this.dataGridViewDetailList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewDetailList_CellContentClick);
            this.dataGridViewDetailList.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewDetailList_CellContentDoubleClick);
            this.dataGridViewDetailList.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewDetailList_CellValueChanged);
            // 
            // ColumnCheckbox
            // 
            this.ColumnCheckbox.HeaderText = "";
            this.ColumnCheckbox.Name = "ColumnCheckbox";
            this.ColumnCheckbox.Width = 30;
            // 
            // ColumnJYID
            // 
            this.ColumnJYID.DataPropertyName = "ID";
            this.ColumnJYID.HeaderText = "ID";
            this.ColumnJYID.Name = "ColumnJYID";
            this.ColumnJYID.ReadOnly = true;
            // 
            // Col套餐名称
            // 
            this.Col套餐名称.DataPropertyName = "套餐名称";
            this.Col套餐名称.HeaderText = "套餐名称";
            this.Col套餐名称.Name = "Col套餐名称";
            this.Col套餐名称.ReadOnly = true;
            // 
            // Column收费编码
            // 
            this.Column收费编码.DataPropertyName = "收费编码";
            this.Column收费编码.HeaderText = "收费编码";
            this.Column收费编码.Name = "Column收费编码";
            this.Column收费编码.ReadOnly = true;
            // 
            // ColumnSFMC
            // 
            this.ColumnSFMC.DataPropertyName = "收费名称";
            this.ColumnSFMC.HeaderText = "收费名称";
            this.ColumnSFMC.Name = "ColumnSFMC";
            this.ColumnSFMC.ReadOnly = true;
            this.ColumnSFMC.Width = 150;
            // 
            // ColumnFyje
            // 
            this.ColumnFyje.DataPropertyName = "金额";
            this.ColumnFyje.HeaderText = "金额";
            this.ColumnFyje.Name = "ColumnFyje";
            this.ColumnFyje.ReadOnly = true;
            // 
            // Columnysbh
            // 
            this.Columnysbh.DataPropertyName = "医生编码";
            this.Columnysbh.HeaderText = "医生编码";
            this.Columnysbh.Name = "Columnysbh";
            this.Columnysbh.ReadOnly = true;
            this.Columnysbh.Width = 90;
            // 
            // Columnysxm
            // 
            this.Columnysxm.DataPropertyName = "医生姓名";
            this.Columnysxm.HeaderText = "医师姓名";
            this.Columnysxm.Name = "Columnysxm";
            this.Columnysxm.ReadOnly = true;
            this.Columnysxm.Width = 90;
            // 
            // Columnbgdbh
            // 
            this.Columnbgdbh.DataPropertyName = "bgdbh";
            this.Columnbgdbh.HeaderText = "报告单编号";
            this.Columnbgdbh.Name = "Columnbgdbh";
            this.Columnbgdbh.ReadOnly = true;
            this.Columnbgdbh.Visible = false;
            // 
            // Columnbgdmc
            // 
            this.Columnbgdmc.DataPropertyName = "bgdmc";
            this.Columnbgdmc.HeaderText = "报告单名称";
            this.Columnbgdmc.Name = "Columnbgdmc";
            this.Columnbgdmc.ReadOnly = true;
            this.Columnbgdmc.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.dataGridViewDetailList);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(6, 101);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(892, 373);
            this.panel1.TabIndex = 3;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataGridViewPrintedList);
            this.groupBox3.Location = new System.Drawing.Point(3, 52);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(619, 305);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "条码列表";
            this.groupBox3.Visible = false;
            // 
            // dataGridViewPrintedList
            // 
            this.dataGridViewPrintedList.AllowUserToAddRows = false;
            this.dataGridViewPrintedList.AllowUserToDeleteRows = false;
            this.dataGridViewPrintedList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPrintedList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnDateTime,
            this.ColumnID2,
            this.ColumnbarcodePrinted,
            this.Column状态,
            this.ColumnCardNo,
            this.ColumnName,
            this.ColumnSex,
            this.Column套餐编号,
            this.Column套餐名称,
            this.Column收费编码2,
            this.ColumnItemType});
            this.dataGridViewPrintedList.ContextMenuStrip = this.contextMenuStrip1;
            this.dataGridViewPrintedList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewPrintedList.Location = new System.Drawing.Point(3, 17);
            this.dataGridViewPrintedList.Name = "dataGridViewPrintedList";
            this.dataGridViewPrintedList.RowTemplate.Height = 23;
            this.dataGridViewPrintedList.Size = new System.Drawing.Size(613, 285);
            this.dataGridViewPrintedList.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.复制条码ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(125, 26);
            // 
            // 复制条码ToolStripMenuItem
            // 
            this.复制条码ToolStripMenuItem.Name = "复制条码ToolStripMenuItem";
            this.复制条码ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.复制条码ToolStripMenuItem.Text = "复制条码";
            this.复制条码ToolStripMenuItem.Click += new System.EventHandler(this.复制条码ToolStripMenuItem_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Image = global::ww.form.Properties.Resources.button_print;
            this.btnPrint.Location = new System.Drawing.Point(258, 6);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(124, 31);
            this.btnPrint.TabIndex = 0;
            this.btnPrint.Text = "打印条码";
            this.btnPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnBarCodeListQuery
            // 
            this.btnBarCodeListQuery.Image = global::ww.form.Properties.Resources.wwNavigatorQuery;
            this.btnBarCodeListQuery.Location = new System.Drawing.Point(431, 6);
            this.btnBarCodeListQuery.Name = "btnBarCodeListQuery";
            this.btnBarCodeListQuery.Size = new System.Drawing.Size(124, 31);
            this.btnBarCodeListQuery.TabIndex = 1;
            this.btnBarCodeListQuery.Text = "条码列表查询";
            this.btnBarCodeListQuery.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBarCodeListQuery.UseVisualStyleBackColor = true;
            this.btnBarCodeListQuery.Click += new System.EventHandler(this.btnBarCodeListQuery_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.LightCyan;
            this.groupBox2.Controls.Add(this.dataGridViewPeople);
            this.groupBox2.Controls.Add(this.btnPeopleQuery);
            this.groupBox2.Controls.Add(this.checkBoxQueryAll);
            this.groupBox2.Controls.Add(this.textBoxSNO);
            this.groupBox2.Controls.Add(this.textBoxQueryName);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.dateTimePickerQueryDate);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(32, 53);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(844, 428);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "人员信息查询";
            this.groupBox2.Visible = false;
            // 
            // dataGridViewPeople
            // 
            this.dataGridViewPeople.AllowUserToAddRows = false;
            this.dataGridViewPeople.AllowUserToDeleteRows = false;
            this.dataGridViewPeople.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPeople.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column收款日期,
            this.ColumnMZID,
            this.Column姓名,
            this.套餐名称,
            this.Column收费名称,
            this.Column金额,
            this.Column医生姓名,
            this.Column科室名称});
            this.dataGridViewPeople.Location = new System.Drawing.Point(20, 54);
            this.dataGridViewPeople.Name = "dataGridViewPeople";
            this.dataGridViewPeople.ReadOnly = true;
            this.dataGridViewPeople.RowTemplate.Height = 23;
            this.dataGridViewPeople.Size = new System.Drawing.Size(797, 355);
            this.dataGridViewPeople.TabIndex = 6;
            this.dataGridViewPeople.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewPeople_CellDoubleClick);
            // 
            // Column收款日期
            // 
            this.Column收款日期.DataPropertyName = "收款日期";
            this.Column收款日期.HeaderText = "收款日期";
            this.Column收款日期.Name = "Column收款日期";
            this.Column收款日期.ReadOnly = true;
            this.Column收款日期.Width = 90;
            // 
            // ColumnMZID
            // 
            this.ColumnMZID.DataPropertyName = "MZID";
            this.ColumnMZID.HeaderText = "门诊号";
            this.ColumnMZID.Name = "ColumnMZID";
            this.ColumnMZID.ReadOnly = true;
            this.ColumnMZID.Width = 95;
            // 
            // Column姓名
            // 
            this.Column姓名.DataPropertyName = "姓名";
            this.Column姓名.HeaderText = "姓名";
            this.Column姓名.Name = "Column姓名";
            this.Column姓名.ReadOnly = true;
            this.Column姓名.Width = 80;
            // 
            // 套餐名称
            // 
            this.套餐名称.DataPropertyName = "套餐名称";
            this.套餐名称.HeaderText = "套餐名称";
            this.套餐名称.Name = "套餐名称";
            this.套餐名称.ReadOnly = true;
            // 
            // Column收费名称
            // 
            this.Column收费名称.DataPropertyName = "收费名称";
            this.Column收费名称.HeaderText = "收费名称";
            this.Column收费名称.Name = "Column收费名称";
            this.Column收费名称.ReadOnly = true;
            this.Column收费名称.Width = 200;
            // 
            // Column金额
            // 
            this.Column金额.DataPropertyName = "金额";
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = null;
            this.Column金额.DefaultCellStyle = dataGridViewCellStyle1;
            this.Column金额.HeaderText = "金额";
            this.Column金额.Name = "Column金额";
            this.Column金额.ReadOnly = true;
            this.Column金额.Width = 70;
            // 
            // Column医生姓名
            // 
            this.Column医生姓名.DataPropertyName = "医生姓名";
            this.Column医生姓名.HeaderText = "医生姓名";
            this.Column医生姓名.Name = "Column医生姓名";
            this.Column医生姓名.ReadOnly = true;
            this.Column医生姓名.Width = 80;
            // 
            // Column科室名称
            // 
            this.Column科室名称.DataPropertyName = "科室";
            this.Column科室名称.HeaderText = "科室";
            this.Column科室名称.Name = "Column科室名称";
            this.Column科室名称.ReadOnly = true;
            // 
            // btnPeopleQuery
            // 
            this.btnPeopleQuery.Image = global::ww.form.Properties.Resources.wwNavigatorQuery;
            this.btnPeopleQuery.Location = new System.Drawing.Point(553, 20);
            this.btnPeopleQuery.Name = "btnPeopleQuery";
            this.btnPeopleQuery.Size = new System.Drawing.Size(75, 23);
            this.btnPeopleQuery.TabIndex = 5;
            this.btnPeopleQuery.Text = "查询";
            this.btnPeopleQuery.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPeopleQuery.UseVisualStyleBackColor = true;
            this.btnPeopleQuery.Click += new System.EventHandler(this.btnPeopleQuery_Click);
            // 
            // checkBoxQueryAll
            // 
            this.checkBoxQueryAll.AutoSize = true;
            this.checkBoxQueryAll.Location = new System.Drawing.Point(490, 24);
            this.checkBoxQueryAll.Name = "checkBoxQueryAll";
            this.checkBoxQueryAll.Size = new System.Drawing.Size(48, 16);
            this.checkBoxQueryAll.TabIndex = 4;
            this.checkBoxQueryAll.Text = "全部";
            this.checkBoxQueryAll.UseVisualStyleBackColor = true;
            // 
            // textBoxSNO
            // 
            this.textBoxSNO.Location = new System.Drawing.Point(241, 22);
            this.textBoxSNO.Name = "textBoxSNO";
            this.textBoxSNO.Size = new System.Drawing.Size(84, 21);
            this.textBoxSNO.TabIndex = 3;
            // 
            // textBoxQueryName
            // 
            this.textBoxQueryName.Location = new System.Drawing.Point(396, 22);
            this.textBoxQueryName.Name = "textBoxQueryName";
            this.textBoxQueryName.Size = new System.Drawing.Size(84, 21);
            this.textBoxQueryName.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(182, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 12);
            this.label10.TabIndex = 2;
            this.label10.Text = "门诊号：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(349, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 2;
            this.label9.Text = "姓名：";
            // 
            // dateTimePickerQueryDate
            // 
            this.dateTimePickerQueryDate.Location = new System.Drawing.Point(59, 21);
            this.dateTimePickerQueryDate.Name = "dateTimePickerQueryDate";
            this.dateTimePickerQueryDate.Size = new System.Drawing.Size(104, 21);
            this.dateTimePickerQueryDate.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "日期:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnPrint);
            this.panel2.Controls.Add(this.btnBarCodeListQuery);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(6, 474);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(892, 49);
            this.panel2.TabIndex = 16;
            // 
            // ColumnDateTime
            // 
            this.ColumnDateTime.DataPropertyName = "时间";
            this.ColumnDateTime.HeaderText = "时间";
            this.ColumnDateTime.Name = "ColumnDateTime";
            this.ColumnDateTime.ReadOnly = true;
            this.ColumnDateTime.Width = 80;
            // 
            // ColumnID2
            // 
            this.ColumnID2.DataPropertyName = "ID";
            this.ColumnID2.HeaderText = "ID";
            this.ColumnID2.Name = "ColumnID2";
            this.ColumnID2.ReadOnly = true;
            this.ColumnID2.Visible = false;
            this.ColumnID2.Width = 50;
            // 
            // ColumnbarcodePrinted
            // 
            this.ColumnbarcodePrinted.DataPropertyName = "条码";
            this.ColumnbarcodePrinted.HeaderText = "条码";
            this.ColumnbarcodePrinted.Name = "ColumnbarcodePrinted";
            this.ColumnbarcodePrinted.Width = 120;
            // 
            // Column状态
            // 
            this.Column状态.DataPropertyName = "状态";
            this.Column状态.HeaderText = "状态";
            this.Column状态.Name = "Column状态";
            this.Column状态.ReadOnly = true;
            this.Column状态.Width = 60;
            // 
            // ColumnCardNo
            // 
            this.ColumnCardNo.DataPropertyName = "卡号";
            this.ColumnCardNo.HeaderText = "卡号";
            this.ColumnCardNo.Name = "ColumnCardNo";
            this.ColumnCardNo.ReadOnly = true;
            this.ColumnCardNo.Width = 90;
            // 
            // ColumnName
            // 
            this.ColumnName.DataPropertyName = "姓名";
            this.ColumnName.HeaderText = "姓名";
            this.ColumnName.Name = "ColumnName";
            this.ColumnName.ReadOnly = true;
            this.ColumnName.Width = 65;
            // 
            // ColumnSex
            // 
            this.ColumnSex.DataPropertyName = "性别";
            this.ColumnSex.HeaderText = "性别";
            this.ColumnSex.Name = "ColumnSex";
            this.ColumnSex.ReadOnly = true;
            this.ColumnSex.Width = 60;
            // 
            // Column套餐编号
            // 
            this.Column套餐编号.DataPropertyName = "套餐编号";
            this.Column套餐编号.HeaderText = "套餐编号";
            this.Column套餐编号.Name = "Column套餐编号";
            this.Column套餐编号.ReadOnly = true;
            this.Column套餐编号.Visible = false;
            this.Column套餐编号.Width = 80;
            // 
            // Column套餐名称
            // 
            this.Column套餐名称.DataPropertyName = "套餐名称";
            this.Column套餐名称.HeaderText = "套餐名称";
            this.Column套餐名称.Name = "Column套餐名称";
            this.Column套餐名称.ReadOnly = true;
            this.Column套餐名称.Width = 80;
            // 
            // Column收费编码2
            // 
            this.Column收费编码2.DataPropertyName = "收费编码";
            this.Column收费编码2.HeaderText = "收费编码";
            this.Column收费编码2.Name = "Column收费编码2";
            this.Column收费编码2.ReadOnly = true;
            this.Column收费编码2.Visible = false;
            this.Column收费编码2.Width = 80;
            // 
            // ColumnItemType
            // 
            this.ColumnItemType.DataPropertyName = "收费名称";
            this.ColumnItemType.HeaderText = "收费名称";
            this.ColumnItemType.Name = "ColumnItemType";
            this.ColumnItemType.ReadOnly = true;
            // 
            // FrmBarCodeQueryPrintNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(904, 529);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel2);
            this.Name = "FrmBarCodeQueryPrintNew";
            this.Padding = new System.Windows.Forms.Padding(6);
            this.Text = "条码打印";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetailList)).EndInit();
            this.panel1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPrintedList)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPeople)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxJZH;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxSex;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxAge;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxDept;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxRoom;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxBed;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dataGridViewDetailList;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnBarCodeListQuery;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dateTimePickerQueryDate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxQueryName;
        private System.Windows.Forms.Button btnPeopleQuery;
        private System.Windows.Forms.DataGridView dataGridViewPeople;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dataGridViewPrintedList;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 复制条码ToolStripMenuItem;
        private System.Windows.Forms.CheckBox checkBoxQueryAll;
        private System.Windows.Forms.TextBox textBoxSNO;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column收款日期;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnMZID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column姓名;
        private System.Windows.Forms.DataGridViewTextBoxColumn 套餐名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column收费名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column金额;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column医生姓名;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column科室名称;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColumnCheckbox;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnJYID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col套餐名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column收费编码;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSFMC;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnFyje;
        private System.Windows.Forms.DataGridViewTextBoxColumn Columnysbh;
        private System.Windows.Forms.DataGridViewTextBoxColumn Columnysxm;
        private System.Windows.Forms.DataGridViewTextBoxColumn Columnbgdbh;
        private System.Windows.Forms.DataGridViewTextBoxColumn Columnbgdmc;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnID2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnbarcodePrinted;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column状态;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnCardNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSex;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column套餐编号;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column套餐名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column收费编码2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnItemType;
    }
}