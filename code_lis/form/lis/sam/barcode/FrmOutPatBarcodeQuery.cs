﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ww.form.wwf;
using ww.lis.lisbll.sam;

namespace ww.form.lis.sam.barcode
{
    public partial class FrmOutPatBarcodeQuery : SysBaseForm
    {
        private CHIS barcodebll = new CHIS();
        public FrmOutPatBarcodeQuery()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = barcodebll.GetBarCodeListPrintedNew2(dateTimePicker1.Value.ToString("yyyy-MM-dd"), textBox1.Text.Trim());
                this.dataGridViewPrintedList.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show("出现异常，异常信息：\r\n" + ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }
    }
}
