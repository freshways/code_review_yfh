﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using ww.form.lis.sam.Report;
using ww.form.wwf;
using ww.lis.lisbll.sam;

namespace ww.form.lis.sam.barcode
{
    public partial class FrmBarCodeQueryPrintNew : SysBaseForm
    {
        private string m_BarCodePrinterName = System.Configuration.ConfigurationManager.AppSettings["CodePrinter"].ToString();
        private string m_OneMoreBarcode;
        private CHIS barcodebll = new CHIS();
        private jybll blljy = new jybll();
        private string m_mzid = "";
        public FrmBarCodeQueryPrintNew()
        {
            InitializeComponent();

            this.dataGridViewDetailList.AutoGenerateColumns = false;
            this.dataGridViewPeople.AutoGenerateColumns = false;

            m_OneMoreBarcode = GetBarcodeSetting();

            groupBox3.Dock = DockStyle.Fill;

        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            groupBox3.Visible = false;
            if(groupBox2.Visible)
            {
                groupBox2.Visible = false;
            }
            else
            {
                groupBox2.Visible = true;
                btnPeopleQuery_Click(null, null);
            }
        }

        private void btnPeopleQuery_Click(object sender, EventArgs e)
        {
            //string date = dateTimePickerQueryDate.Value.ToString("yyyy-MM-dd");
            string name = textBoxQueryName.Text.Trim();
            bool bCheckAll = checkBoxQueryAll.Checked;

            string mzidsno = textBoxSNO.Text.Trim();

            if (mzidsno.Length!=0 && !Regex.IsMatch(mzidsno, @"^\d{4}$") && !Regex.IsMatch(mzidsno, @"^\d{8}.\d{4}$"))
            {
                MessageBox.Show("请确认门诊号是否输入正确。");
                return;
            }

            if(mzidsno.Length==4)
            {
                mzidsno = DateTime.Now.ToString("yyyyMMdd")+"."+mzidsno;
            }

            try
            {
                DataTable dt = barcodebll.GetMZPatInfosNew(dateTimePickerQueryDate.Value, mzidsno, name, bCheckAll);
                this.dataGridViewPeople.DataSource = dt;
            }
            catch(Exception ex)
            {
                this.dataGridViewPeople.DataSource = null;
                MessageBox.Show(ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }


        }

        private void dataGridViewPeople_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = this.dataGridViewPeople.CurrentRow;
            if(row!=null)
            {
                m_mzid = row.Cells["ColumnMZID"].Value.ToString();
                try
                {
                    #region 获取病人疾病信息
                    DataTable dtRet = barcodebll.GetPatInfoByMZID(m_mzid);

                    textBoxJZH.Text = m_mzid;
                    textBoxName.Text = dtRet.Rows[0]["病人姓名"].ToString();
                    textBoxSex.Text = dtRet.Rows[0]["性别"].ToString();
                    textBoxAge.Text = dtRet.Rows[0]["年龄"].ToString() + dtRet.Rows[0]["年龄单位"].ToString();

                    if(textBoxAge.Text.Trim()=="")
                    {
                        string sfzh = dtRet.Rows[0]["身份证号"].ToString();
                        if(sfzh!=null && sfzh.Length==18)
                        {
                            string strBirth = sfzh.Substring(6, 4) + "-" + sfzh.Substring(10, 2) + "-" + sfzh.Substring(12, 2);
                            string age = barcodebll.GetAgeAndUnit(strBirth);
                            textBoxAge.Text = age;
                        }
                    }

                    textBoxDept.Text = dtRet.Rows[0]["科室名称"].ToString();
                    #endregion

                    #region 隐藏人员列表查询框
                    this.groupBox2.Visible = false;
                    #endregion

                    #region 获取费用明细（化验项目列表）
                    GetChargeInfo(m_mzid);
                    #endregion

                    #region 设置勾选框
                    for (int index = 0; index < dataGridViewDetailList.Rows.Count; index++)
                    {
                        this.dataGridViewDetailList.Rows[index].Cells[0].Value = true;
                    }
                    #endregion
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
        }

        private void GetChargeInfo(string mzid)
        {
            DataTable dtDetail = barcodebll.GetOrderItemDetailsByMZIDNew(mzid);
            this.dataGridViewDetailList.DataSource = dtDetail;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            this.dataGridViewDetailList.EndEdit();
            if(this.dataGridViewDetailList.Rows.Count ==0 || string.IsNullOrWhiteSpace(m_mzid))
            {
                MessageBox.Show("化验列表是空的，没有需要打印的条码", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
            
            //获取所有已经勾选的项目的费用id
            //string fycodes = "";
            List<string> jyidlist = new List<string>();
            for(int index =0; index < this.dataGridViewDetailList.Rows.Count; index++)
            {
                DataGridViewCheckBoxCell cell = this.dataGridViewDetailList.Rows[index].Cells["ColumnCheckbox"] as DataGridViewCheckBoxCell;
                //if (cell.Selected)
                string jyidtemp = this.dataGridViewDetailList.Rows[index].Cells["ColumnJYID"].Value.ToString();
                if (cell.Value!=null && cell.Value.ToString().ToLower() == "true" && !jyidlist.Contains(jyidtemp))
                {
                    jyidlist.Add(jyidtemp);
                }
            }

            //没有需要打印到条码上的化验项目时
            if (jyidlist.Count == 0)
            {
                MessageBox.Show("未选择需要打印条码的化验项目", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            try
            {
                //生成条码，打印条码
                List<string> mzidlist = new List<string>();
                mzidlist.Add(m_mzid);
                GetAndPrintBarCode(m_mzid, jyidlist);
                GetChargeInfo(m_mzid);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        private void btnBarCodeListQuery_Click(object sender, EventArgs e)
        {
            if(groupBox3.Visible)
            {
                groupBox3.Visible = false;
            }
            else
            {
                try
                {
                    DataTable dt = barcodebll.GetBarCodeListPrintedNew();
                    this.dataGridViewPrintedList.DataSource = dt;
                    
                    groupBox3.Visible = true;
                }
                catch(Exception ex)
                {
                    MessageBox.Show("出现异常，异常信息：\r\n"+ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
        }

        private string GetBarcodeSetting()
        {
            string value = "";
            //barcodesetting.cfg
            string barcodeSettingPath = Application.StartupPath + "\\barcodesetting.cfg";
            if (File.Exists(barcodeSettingPath))
            {
                //判断文件中的数据是否正确,暂时不进行判断
                //从文件中取出值
                string instrID = "";
                try
                {
                    StreamReader reader = new StreamReader(barcodeSettingPath);
                    instrID = reader.ReadLine();
                    reader.Close();

                    value = instrID;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(),"提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            //else
            //{
            //    //新建文件，并写入默认设备id
            //    DefaultInstSettingForm frm = new DefaultInstSettingForm(barcodeSettingPath);
            //    frm.ShowDialog();
            //    frm.Dispose();
            //}

            return value;
        }

        private bool PrinterExists(string printerName)
        {
            //PrintDocument prtdoc = new PrintDocument();
            //string strDefaultPrinter = prtdoc.PrinterSettings.PrinterName;      //获取默认的打印机名    

            System.Drawing.Printing.PrinterSettings.StringCollection snames = System.Drawing.Printing.PrinterSettings.InstalledPrinters;

            foreach (string s in snames)
            {
                if (s.ToLower().Trim() == printerName.Trim().ToLower())
                {
                    return true;
                }
            }
            return false;
        }

        private void 复制条码ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = this.dataGridViewPrintedList.CurrentRow;
            if (row != null)
            {
                string barcode = row.Cells["ColumnbarcodePrinted"].Value.ToString();
                Clipboard.Clear();
                Clipboard.SetText(barcode);
            }
        }

        private void GetAndPrintBarCode(string mzid, List<string> jyidlist)
        {
            //遍历每一个病人
            //for (int index = 0; index < mzidlist.Count; index++)
            do
            {
            //{
            //    string mzid = mzidlist[index];
                Hashtable hashBarcodeJYid = new Hashtable();//存储条码号与JY检验申请单摘要表的ID间的对象关系，键为条码号， 值为List<string>存储JY检验申请单摘要表的ID

                //获取这个病人的病人信息
                DataTable dtPatInfo = barcodebll.GetPatInfoFromHisByMZIDNew(mzid);

                //获取这个病人的化验明细
                DataTable dtitem = barcodebll.GetMZItemListNew(mzid, jyidlist);

                if (dtitem == null)
                {
                    continue;
                }

                #region 取出这个病人的化验项目编码列表
                List<string> itemcode = new List<string>();
                List<string> groupcode = new List<string>();
                for (int inner = 0; inner < dtitem.Rows.Count; inner++)
                {
                    if (dtitem.Rows[inner]["项目编码"] == null || dtitem.Rows[inner]["项目编码"]==DBNull.Value)
                    { }
                    else if (dtitem.Rows[inner]["是否组合"].ToString() == "组合套餐")
                    {
                        groupcode.Add(dtitem.Rows[inner]["项目编码"].ToString());
                    }
                    else
                    {
                        itemcode.Add(dtitem.Rows[inner]["项目编码"].ToString());
                    }
                }

                if (itemcode.Count == 0 && groupcode.Count ==0)
                {
                    continue;
                }

                //从LIS里获取的收费项目的条码打印组合信息
                DataSet dsLisChargeInfo = blljy.GetListByItemCodeNew(itemcode, groupcode);
                #endregion

                //记录条码，打印时使用
                List<string> barcodeList = new List<string>();

                //
                if (dsLisChargeInfo == null || dsLisChargeInfo.Tables.Count != 2 || dsLisChargeInfo.Tables[0].Rows.Count == 0)//不再对dsLisChargeInfo.Tables[1]多判断，逻辑上，dsLisChargeInfo.Tables两个表一定会同时有数据
                {
                    //continue;
                }
                else
                {

                    //DataSet dsThisPatBarCode = dsBarCodes.Clone();

                    DataTable dtDetail = dsLisChargeInfo.Tables[0];
                    DataTable dtGroup = dsLisChargeInfo.Tables[1];

                    
                    for (int inner = 0; inner < dtGroup.Rows.Count; inner++)
                    {
                        //获取最新的条码号 根据[检验类型]
                        string str检验类型 = dtGroup.Rows[inner]["检验类型"].ToString();
                        string str检验类型名称 = dtGroup.Rows[inner]["检验类型名称"].ToString();
                        string str条码分组 = dtGroup.Rows[inner]["条码分组"].ToString();

                        if (string.IsNullOrWhiteSpace(str检验类型))
                        {
                            continue;
                        }

                        //获取该检查类型的编码明细
                        //List<string> drcodes = new List<string>();

                        DataRow[] drdetials = dtDetail.Select("检验类型='" + str检验类型 + "' and 条码分组='" + str条码分组 + "'");

                        StringBuilder codesStr = new StringBuilder();
                        StringBuilder groupStr = new StringBuilder();
                        for (int iii = 0; iii < drdetials.Length; iii++)
                        {
                            if (drdetials[iii]["是否套餐"].ToString() == "组合套餐")
                            {
                                if (groupStr.Length > 0)
                                {
                                    groupStr.Append(",");
                                }
                                groupStr.Append(drdetials[iii]["收费编码"].ToString());
                            }
                            else if (drdetials[iii]["是否套餐"].ToString() == "收费小项")
                            {
                                if (codesStr.Length > 0)
                                {
                                    codesStr.Append(",");
                                }
                                codesStr.Append(drdetials[iii]["收费编码"].ToString());
                            }
                        }

                        //一般情况下，下面if内的语句不会执行到
                        if (codesStr.Length == 0 && groupStr.Length == 0)
                        {
                            MessageBox.Show("存在未设置条码分组的化验项目，请进行设置。");
                            return;
                        }

                        StringBuilder sbSelect = new StringBuilder();
                        if (codesStr.Length > 0)
                        {
                            sbSelect.Append("(是否组合='收费小项' and 项目编码 in (" + codesStr.ToString() + "))");
                        }

                        if (groupStr.Length > 0)
                        {
                            if (sbSelect.Length > 0)
                            {
                                sbSelect.Append(" or ");
                            }
                            sbSelect.Append("(是否组合='组合套餐' and 项目编码 in (" + groupStr.ToString() + "))");
                        }

                        //组合化验明细
                        DataRow[] drPatItems = dtitem.Select(sbSelect.ToString());


                        List<string> jyidlistforHash = new List<string>();
                        for (int innerforHash = 0; innerforHash < drPatItems.Length; innerforHash++)
                        {
                            jyidlistforHash.Add(drPatItems[innerforHash]["ID"].ToString());
                        }

                        ////从LIS数据库中获取条码号
                        //string strbarcode = blljy.GetBarCodeByLisProc(dtGroup.Rows[inner]["检验类型"].ToString());

                        //从HIS数据库中获取条码号
                        string strbarcode = barcodebll.GetBarCode(2);

                        hashBarcodeJYid[strbarcode] = jyidlistforHash;

                        barcodeList.Add(strbarcode);

                        //移除已经处理的
                        foreach (DataRow dr in drPatItems)
                        {
                            dtitem.Rows.Remove(dr);
                        }
                    }
                }

                //患者的化验明细中，可能存在LIS未设置打印的项目。如果这样，每一条生成一个条码
                for (int lastItemIndex = 0; lastItemIndex < dtitem.Rows.Count; lastItemIndex++)
                {
                    //string strbarcode = blljy.GetBarCodeByLisProc("");
                    string strbarcode = barcodebll.GetBarCode(2); 

                    //InsertIntoBarCodeDetail(dtitem.Rows[lastItemIndex], dsThisPatBarCode.Tables[1], strbarcode);
                    //barcodebll.InsertIntoBarCodeDetail(dtitem.Rows[lastItemIndex], dsThisPatBarCode.Tables[1], dtPatInfo.Rows[0]["MZID"].ToString(), strbarcode, dtPatInfo.Rows[0]["医生编码"].ToString(), dtPatInfo.Rows[0]["医生姓名"].ToString());

                    //InsertIntoBarCodeMain(dtPatInfo, dsThisPatBarCode.Tables[0], strbarcode, "", "",
                    //                        dtitem.Rows[lastItemIndex]["开嘱医生编码"].ToString(), dtitem.Rows[lastItemIndex]["开嘱医生"].ToString());
                    //barcodebll.InsertIntoBarCodeMain(dtPatInfo, dsThisPatBarCode.Tables[0], strbarcode, "", "");
                    List<string> jyidlistforHash = new List<string>();
                    jyidlistforHash.Add(dtitem.Rows[lastItemIndex]["ID"].ToString());
                    hashBarcodeJYid[strbarcode] = jyidlistforHash;

                    barcodeList.Add(strbarcode);
                }

                //更新条码信息至HIS数据库
                string errMsg = "";
                bool updateResult = barcodebll.UpdateBarCodeTableNew(hashBarcodeJYid, out errMsg);
                if(!updateResult)
                {
                    MessageBox.Show(errMsg);
                    return;
                }

                //打印条码
                PrintByBarCode(barcodeList);

                //修改此人的申请标记:"已申请"-->"打印"
                //UpdateJY申请摘要(zyid, _databegin, _dateend);

            } while (false);

            //打印完成后更新显示列表
            
        }

        private void PrintByBarCode(List<string> barcodelist)
        {
            if (barcodelist.Count == 0)
            {
                return;
            }

            DataTable dataForReport = new DataTable();
            dataForReport.Columns.Add("barcode");
            dataForReport.Columns.Add("peopleinfo");
            dataForReport.Columns.Add("items");

            for (int index = 0; index < barcodelist.Count; index++)
            {
                //根据条码号获取病人信息
                DataTable dtPatient = barcodebll.GetBarCodeMainNew(barcodelist[index]);
                if (dtPatient == null || dtPatient.Rows.Count == 0)
                {
                    continue;
                }

                //根据条码号获取条码明细项目信息
                string stritems = barcodebll.GetYzmcByBarCodeInStrNew(barcodelist[index]);

                //向dataForReport中添加条码的详细信息
                DataRow dr = dataForReport.NewRow();
                dr["barcode"] = barcodelist[index];
                dr["peopleinfo"] = dtPatient.Rows[0]["MZID"].ToString() + " " + dtPatient.Rows[0]["病人姓名"].ToString() + " " + dtPatient.Rows[0]["病人类型"].ToString() + "\n"
                                  + dtPatient.Rows[0]["性别"].ToString() + "/"
                                  + dtPatient.Rows[0]["年龄"].ToString() + dtPatient.Rows[0]["年龄单位"].ToString() + " "
                                  + dtPatient.Rows[0]["科室名称"].ToString() + "(" + dtPatient.Rows[0]["医生姓名"].ToString() + ") ";
                                  //+ dtPatient.Rows[0]["床号"].ToString() + (dtPatient.Rows[0]["床号"].ToString().Contains("床") ? "" : "床");
                dr["items"] = stritems;
                dataForReport.Rows.Add(dr);
            }


            //根据获取到的条码信息打印条码
            XtraReportBarCode repBarCode = new XtraReportBarCode(dataForReport);
            DevExpress.XtraReports.UI.ReportPrintTool tool = new DevExpress.XtraReports.UI.ReportPrintTool(repBarCode);

            if (PrinterExists(m_BarCodePrinterName))
            {
                tool.Print(m_BarCodePrinterName);
            }
            else
            {
                MessageBox.Show("没有找打LIS条码打印机: " + m_BarCodePrinterName);
            }
        }

        private void dataGridViewDetailList_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void dataGridViewDetailList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void dataGridViewDetailList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0 && this.dataGridViewDetailList.Columns[e.ColumnIndex] == ColumnCheckbox)
            {
                bool tempvalue = Convert.ToBoolean(this.dataGridViewDetailList.Rows[e.RowIndex].Cells[e.ColumnIndex].Value);
                string jyid = this.dataGridViewDetailList.Rows[e.RowIndex].Cells["ColumnJYID"].Value.ToString();
                for (int index = 0; index < this.dataGridViewDetailList.Rows.Count; index++)
                {
                    string jyidtemp = this.dataGridViewDetailList.Rows[index].Cells["ColumnJYID"].Value.ToString();
                    if (jyidtemp == jyid)
                    {
                        this.dataGridViewDetailList.Rows[index].Cells[e.ColumnIndex].Value = !tempvalue;
                    }
                }
            }
        }

        private void dataGridViewDetailList_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0 && this.dataGridViewDetailList.Columns[e.ColumnIndex] == ColumnCheckbox)
            {
                bool tempvalue = Convert.ToBoolean(this.dataGridViewDetailList.Rows[e.RowIndex].Cells[e.ColumnIndex].Value);
                string jyid = this.dataGridViewDetailList.Rows[e.RowIndex].Cells["ColumnJYID"].Value.ToString();
                for (int index = 0; index < this.dataGridViewDetailList.Rows.Count; index++)
                {
                    string jyidtemp = this.dataGridViewDetailList.Rows[index].Cells["ColumnJYID"].Value.ToString();
                    if (jyidtemp == jyid)
                    {
                        this.dataGridViewDetailList.Rows[index].Cells[e.ColumnIndex].Value = !tempvalue;
                    }
                }
            }
        }
    }
}
