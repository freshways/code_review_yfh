﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using ww.form.lis.sam.Report;
using ww.form.wwf;
using ww.lis.lisbll.sam;

namespace ww.form.lis.sam.barcode
{
    public partial class FrmBarCodeQueryPrint : SysBaseForm
    {
        private string m_BarCodePrinterName = System.Configuration.ConfigurationManager.AppSettings["CodePrinter"].ToString();
        //private string m_OneMoreBarcode = System.Configuration.ConfigurationManager.AppSettings["OneMoreBarcode"].ToString();
        private string m_OneMoreBarcode;
        private CHIS barcodebll = new CHIS();
        private jybll blljy = new jybll();
        private string m_mzid = "";
        DataSet dsBarCodes=new DataSet();
        public FrmBarCodeQueryPrint()
        {
            InitializeComponent();

            this.dataGridViewDetailList.AutoGenerateColumns = false;
            this.dataGridViewPeople.AutoGenerateColumns = false;

            m_OneMoreBarcode = GetBarcodeSetting();

            //获取条码表结构
            dsBarCodes = barcodebll.GetBarCodeTableInfo();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            if(groupBox2.Visible)
            {
                groupBox2.Visible = false;
            }
            else
            {
                groupBox2.Visible = true;
                btnPeopleQuery_Click(null, null);
            }
        }

        private void btnPeopleQuery_Click(object sender, EventArgs e)
        {
            //string date = dateTimePickerQueryDate.Value.ToString("yyyy-MM-dd");
            string name = textBoxQueryName.Text.Trim();
            bool bCheckAll = checkBoxQueryAll.Checked;

            string mzidsno = textBoxSNO.Text.Trim();

            if (mzidsno.Length!=0 && !Regex.IsMatch(mzidsno, @"^\d{4}$") && !Regex.IsMatch(mzidsno, @"^\d{8}.\d{4}$"))
            {
                MessageBox.Show("请确认门诊号是否输入正确。");
                return;
            }

            if(mzidsno.Length==4)
            {
                mzidsno = DateTime.Now.ToString("yyyyMMdd")+"."+mzidsno;
            }

            try
            {
                DataTable dt = barcodebll.GetMZPatInfos(dateTimePickerQueryDate.Value, mzidsno, name);
                this.dataGridViewPeople.DataSource = dt;
            }
            catch(Exception ex)
            {
                this.dataGridViewPeople.DataSource = null;
                MessageBox.Show(ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }


        }

        private void dataGridViewPeople_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = this.dataGridViewPeople.CurrentRow;
            if(row!=null)
            {
                m_mzid = row.Cells["ColumnMZID"].Value.ToString();
                try
                {
                    #region 获取病人疾病信息
                    DataTable dtRet = barcodebll.GetPatInfoByMZID(m_mzid);

                    textBoxJZH.Text = m_mzid;
                    textBoxName.Text = dtRet.Rows[0]["病人姓名"].ToString();
                    textBoxSex.Text = dtRet.Rows[0]["性别"].ToString();
                    textBoxAge.Text = dtRet.Rows[0]["年龄"].ToString() + dtRet.Rows[0]["年龄单位"].ToString();

                    if(textBoxAge.Text.Trim()=="")
                    {
                        string sfzh = dtRet.Rows[0]["身份证号"].ToString();
                        if(sfzh!=null && sfzh.Length==18)
                        {
                            string strBirth = sfzh.Substring(6, 4) + "-" + sfzh.Substring(10, 2) + "-" + sfzh.Substring(12, 2);
                            string age = barcodebll.GetAgeAndUnit(strBirth);
                            textBoxAge.Text = age;
                        }
                    }

                    textBoxDept.Text = dtRet.Rows[0]["科室名称"].ToString();
                    //textBoxRoom.Text = dtRet.Rows[0]["Room"].ToString();
                    //textBoxBed.Text = dtRet.Rows[0]["Bed"].ToString();
                    #endregion

                    #region 隐藏人员列表查询框
                    this.groupBox2.Visible = false;
                    #endregion

                    #region 获取费用明细（化验项目列表）
                    GetChargeInfo(m_mzid);
                    #endregion

                    #region 设置勾选框
                    //if (dtDetail.Rows.Count > 0)
                    //{
                    //    this.dataGridViewDetailList.Rows[0].Cells[0].Value = true;
                    //    //DataGridViewCheckBoxCell cell = this.dataGridViewDetailList.Rows[0].Cells["ColumnCheckbox"] as DataGridViewCheckBoxCell;
                    //    ////cell.Selected = true;
                    //    //cell.Value = true;
                    //}
                    for (int index = 0; index < dataGridViewDetailList.Rows.Count; index++)
                    {
                        this.dataGridViewDetailList.Rows[index].Cells[0].Value = true;
                    }
                    #endregion
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
        }

        private void GetChargeInfo(string mzid)
        {
            DataTable dtDetail = barcodebll.GetOrderItemDetailsByMZID(mzid);
            this.dataGridViewDetailList.DataSource = dtDetail;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            this.dataGridViewDetailList.EndEdit();
            if(this.dataGridViewDetailList.Rows.Count ==0 || string.IsNullOrWhiteSpace(m_mzid))
            {
                MessageBox.Show("化验列表是空的，没有需要打印的条码", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            
            //获取所有已经勾选的项目的费用id
            //string fycodes = "";
            List<string> fycodeslist = new List<string>();
            for(int index =0; index < this.dataGridViewDetailList.Rows.Count; index++)
            {
                DataGridViewCheckBoxCell cell = this.dataGridViewDetailList.Rows[index].Cells["ColumnCheckbox"] as DataGridViewCheckBoxCell;
                //if (cell.Selected)
                if (cell.Value!=null && cell.Value.ToString().ToLower() == "true")
                {
                    fycodeslist.Add(this.dataGridViewDetailList.Rows[index].Cells["Column收费编码"].Value.ToString());
                }
            }

            //没有需要打印到条码上的化验项目时
            if (fycodeslist.Count == 0)
            {
                MessageBox.Show("未选择需要打印条码的化验项目", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            try
            {
                //fycodes = fycodes.Substring(0, fycodes.Length - 1);

                ////去掉已经打印过的项目,      =======查询明细时，已经排除已打印项目，故不需要此代码
                //DataTable dtCharged = barcodebll.GetChargedList(m_mzid, fycodes);
                //for (int index = 0; index < dtCharged.Rows.Count; index++)
                //{
                //    string temp = dtCharged.Rows[index]["ChargeID"].ToString();
                //    fycodes = fycodes.Replace(temp + ",", "").Replace(temp, "");
                //}

                //生成条码，打印条码
                List<string> mzidlist = new List<string>();
                mzidlist.Add(m_mzid);
                GetAndPrintBarCode(mzidlist, fycodeslist);

                GetChargeInfo(m_mzid);

                ////生成条码
                //DataTable dtbarCodeInfo = barcodebll.GetBarCodes(m_mzid, fycodes, LoginBLL.strPersonName);
                //if (dtbarCodeInfo.Rows.Count > 0)
                //{
                //    //生成条码成功的情况
                //    if (dtbarCodeInfo.Rows[0][0].ToString() == "1" || dtbarCodeInfo.Rows[0][0].ToString().ToLower() == "true")
                //    {
                //        //执行打印条码的工作
                //        PrintBarCodes(dtbarCodeInfo.Rows[0][2].ToString());

                //        GetChargeInfo(m_mzid);
                //        //打印完后将病人的patid清空
                //        //del 20161020 wjz 不能清空，原因：如果只是打印了其中的一个条码，打印第二个条码时会受到影响
                //        //m_patid = "";

                //        //this.DialogResult = DialogResult.OK;
                //    }
                //    else
                //    {
                //        //生成条码失败的情况下
                //        MessageBox.Show(dtbarCodeInfo.Rows[0][1].ToString(), "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                //        return;
                //    }
                //}
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        private void PrintBarCodes(string barcodes)
        {
            char[] carr = { ',' };
            string temp = barcodes;

            string[] arrcodes = temp.Split(carr, StringSplitOptions.RemoveEmptyEntries);

            DataTable dataForReport = new DataTable();
            dataForReport.Columns.Add("barcode");
            dataForReport.Columns.Add("peopleinfo");
            dataForReport.Columns.Add("items");

            for (int index = 0; index < arrcodes.Length; index++)
            {
                //根据条码号获取病人信息
                DataTable dtPatient = barcodebll.GetPatInfoByBarCode(arrcodes[index]);
                if (dtPatient == null || dtPatient.Rows.Count == 0)
                {
                    continue;
                }

                //根据条码号获取条码项目信息
                string stritems = barcodebll.GetYzmcByBarCodeInStr(arrcodes[index]);

                //向dataForReport中添加条码的详细信息
                DataRow dr = dataForReport.NewRow();
                dr["barcode"] = arrcodes[index];
                dr["peopleinfo"] = dtPatient.Rows[0]["AttendNo"].ToString() +" "+ dtPatient.Rows[0]["PatientName"].ToString()+"\r\n"
                                  + dtPatient.Rows[0]["PatientSex"].ToString() + "/"
                                  + dtPatient.Rows[0]["PatientAge"].ToString() + dtPatient.Rows[0]["PAgeUnit"].ToString() +" "
                                  + dtPatient.Rows[0]["Dept"].ToString();
                dr["items"] = stritems;
                dataForReport.Rows.Add(dr);

                //扫码 add by wjz 20170117 ▽
                if (m_OneMoreBarcode == "1" && ((arrcodes.Length-1) == index))
                {
                    DataRow dr2 = dataForReport.NewRow();
                    dr2["barcode"] = arrcodes[index];
                    dr2["peopleinfo"] = dtPatient.Rows[0]["AttendNo"].ToString() + " " + dtPatient.Rows[0]["PatientName"].ToString() + "\r\n"
                                      + dtPatient.Rows[0]["PatientSex"].ToString() + "/"
                                      + dtPatient.Rows[0]["PatientAge"].ToString() + dtPatient.Rows[0]["PAgeUnit"].ToString() + " "
                                      + dtPatient.Rows[0]["Dept"].ToString();
                    dr2["items"] = "自助打印";
                    dataForReport.Rows.Add(dr2);
                }
                //扫码 add by wjz 20170117 △
            }

            
            //根据获取到的条码信息打印条码
            Report.XtraReportBarCode repBarCode = new Report.XtraReportBarCode(dataForReport);
            DevExpress.XtraReports.UI.ReportPrintTool tool = new DevExpress.XtraReports.UI.ReportPrintTool(repBarCode);
            //tool.ShowPreviewDialog();
            if (PrinterExists(m_BarCodePrinterName))
            {
                tool.Print(m_BarCodePrinterName);
            }

            //打印完成后更新数据
            //barcodebll.UpdateINVOICE_DETAILS_CHARGE(m_patid);
        }

        private void btnBarCodeListQuery_Click(object sender, EventArgs e)
        {
            if(groupBox3.Visible)
            {
                groupBox3.Visible = false;
            }
            else
            {
                try
                {
                    DataTable dt = barcodebll.GetBarCodeListPrinted();
                    this.dataGridViewPrintedList.DataSource = dt;
                    
                    groupBox3.Visible = true;
                }
                catch(Exception ex)
                {
                    MessageBox.Show("出现异常，异常信息：\r\n"+ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
        }

        private string GetBarcodeSetting()
        {
            string value = "";
            //barcodesetting.cfg
            string barcodeSettingPath = Application.StartupPath + "\\barcodesetting.cfg";
            if (File.Exists(barcodeSettingPath))
            {
                //判断文件中的数据是否正确,暂时不进行判断
                //从文件中取出值
                string instrID = "";
                try
                {
                    StreamReader reader = new StreamReader(barcodeSettingPath);
                    instrID = reader.ReadLine();
                    reader.Close();

                    value = instrID;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(),"提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            //else
            //{
            //    //新建文件，并写入默认设备id
            //    DefaultInstSettingForm frm = new DefaultInstSettingForm(barcodeSettingPath);
            //    frm.ShowDialog();
            //    frm.Dispose();
            //}

            return value;
        }

        private bool PrinterExists(string printerName)
        {
            //PrintDocument prtdoc = new PrintDocument();
            //string strDefaultPrinter = prtdoc.PrinterSettings.PrinterName;      //获取默认的打印机名    

            System.Drawing.Printing.PrinterSettings.StringCollection snames = System.Drawing.Printing.PrinterSettings.InstalledPrinters;

            foreach (string s in snames)
            {
                if (s.ToLower().Trim() == printerName.Trim().ToLower())
                {
                    return true;
                }
            }
            return false;
        }

        private void 复制条码ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = this.dataGridViewPrintedList.CurrentRow;
            if (row != null)
            {
                string barcode = row.Cells["ColumnbarcodePrinted"].Value.ToString();
                Clipboard.Clear();
                Clipboard.SetText(barcode);
            }
        }

        private void GetAndPrintBarCode(List<string> mzidlist, List<string> fycodeslist)
        {
            //遍历每一个病人
            for (int index = 0; index < mzidlist.Count; index++)
            {
                string mzid = mzidlist[index];

                //获取这个病人的病人信息
                DataTable dtPatInfo = barcodebll.GetPatInfoFromHisByMZID(mzid);

                //获取这个病人的化验明细
                DataTable dtitem = barcodebll.GetMZItemList(mzid, fycodeslist);

                if (dtitem == null)
                {
                    continue;
                }

                //取出这个病人的化验项目编码列表
                List<string> itemcode = new List<string>();
                for (int inner = 0; inner < dtitem.Rows.Count; inner++)
                {
                    if (dtitem.Rows[inner]["收费编码"] == null)
                    { }
                    else
                    {
                        itemcode.Add(dtitem.Rows[inner]["收费编码"].ToString());
                    }
                }
                if (itemcode.Count == 0)
                {
                    continue;
                }

                //从LIS里获取的收费项目的条码打印组合信息
                DataSet dsLisChargeInfo = blljy.GetListByItemCode(itemcode);

                //TODO:
                if (dsLisChargeInfo == null || dsLisChargeInfo.Tables.Count != 2 || dsLisChargeInfo.Tables[0].Rows.Count == 0)//不再对dsLisChargeInfo.Tables[1]多判断，逻辑上，dsLisChargeInfo.Tables两个表一定会同时有数据
                {
                    continue;
                }

                DataSet dsThisPatBarCode = dsBarCodes.Clone();

                DataTable dtDetail = dsLisChargeInfo.Tables[0];
                DataTable dtGroup = dsLisChargeInfo.Tables[1];

                //记录条码，打印时使用
                List<string> barcodeList = new List<string>();
                for (int inner = 0; inner < dtGroup.Rows.Count; inner++)
                {
                    //获取最新的条码号 根据[检验类型]
                    string str检验类型 = dtGroup.Rows[inner]["检验类型"].ToString();
                    string str检验类型名称 = dtGroup.Rows[inner]["检验类型名称"].ToString();
                    string str条码分组 = dtGroup.Rows[inner]["条码分组"].ToString();

                    if (string.IsNullOrWhiteSpace(str检验类型))
                    {
                        continue;
                    }

                    //获取该检查类型的编码明细
                    //List<string> drcodes = new List<string>();
                    StringBuilder codesStr = new StringBuilder();
                    DataRow[] drdetials = dtDetail.Select("检验类型='" + str检验类型 + "' and 条码分组='" + str条码分组 + "'");
                    for (int iii = 0; iii < drdetials.Length; iii++)
                    {
                        if (iii > 0)
                        {
                            codesStr.Append(",");
                        }
                        codesStr.Append(drdetials[iii]["收费编码"].ToString());
                    }

                    //一般情况下，下面if内的语句不会执行到
                    if (codesStr.Length == 0)
                    {
                        MessageBox.Show("存在未设置条码分组的化验项目，请进行设置。");
                        return;
                    }

                    //组合化验明细
                    DataRow[] drPatItems = dtitem.Select("收费编码 in (" + codesStr.ToString() + ")");

                    //从LIS数据库中获取条码号
                    string strbarcode = blljy.GetBarCodeByLisProc(dtGroup.Rows[inner]["检验类型"].ToString());

                    //组织条码明细表
                    for (int itemp = 0; itemp < drPatItems.Length; itemp++)
                    {
                        barcodebll.InsertIntoBarCodeDetail(drPatItems[itemp], dsThisPatBarCode.Tables[1], dtPatInfo.Rows[0]["MZID"].ToString(), strbarcode, dtPatInfo.Rows[0]["医生编码"].ToString(), dtPatInfo.Rows[0]["医生姓名"].ToString());
                    }

                    //组织条码主表
                    barcodebll.InsertIntoBarCodeMain(dtPatInfo, dsThisPatBarCode.Tables[0], strbarcode, str检验类型, str检验类型名称);

                    barcodeList.Add(strbarcode);

                    //移除已经处理的
                    foreach (DataRow dr in drPatItems)
                    {
                        dtitem.Rows.Remove(dr);
                    }
                }

                //患者的化验明细中，可能存在LIS未设置打印的项目。如果这样，每一条生成一个条码
                for (int lastItemIndex = 0; lastItemIndex < dtitem.Rows.Count; lastItemIndex++)
                {
                    string strbarcode = blljy.GetBarCodeByLisProc("");

                    //InsertIntoBarCodeDetail(dtitem.Rows[lastItemIndex], dsThisPatBarCode.Tables[1], strbarcode);
                    barcodebll.InsertIntoBarCodeDetail(dtitem.Rows[lastItemIndex], dsThisPatBarCode.Tables[1], dtPatInfo.Rows[0]["MZID"].ToString(), strbarcode, dtPatInfo.Rows[0]["医生编码"].ToString(), dtPatInfo.Rows[0]["医生姓名"].ToString());

                    //InsertIntoBarCodeMain(dtPatInfo, dsThisPatBarCode.Tables[0], strbarcode, "", "",
                    //                        dtitem.Rows[lastItemIndex]["开嘱医生编码"].ToString(), dtitem.Rows[lastItemIndex]["开嘱医生"].ToString());
                    barcodebll.InsertIntoBarCodeMain(dtPatInfo, dsThisPatBarCode.Tables[0], strbarcode, "", "");

                    barcodeList.Add(strbarcode);
                }

                //上传条码信息至LIS数据库
                barcodebll.UpdateBarCodeTable(dsThisPatBarCode);

                //打印条码
                //MessageBox.Show("打印");
                PrintByBarCode(barcodeList);

                //修改此人的申请标记:"已申请"-->"打印"
                //UpdateJY申请摘要(zyid, _databegin, _dateend);
            }

            //打印完成后更新显示列表
            
        }

        private void PrintByBarCode(List<string> barcodelist)
        {
            if (barcodelist.Count == 0)
            {
                return;
            }

            DataTable dataForReport = new DataTable();
            dataForReport.Columns.Add("barcode");
            dataForReport.Columns.Add("peopleinfo");
            dataForReport.Columns.Add("items");

            for (int index = 0; index < barcodelist.Count; index++)
            {
                //根据条码号获取病人信息
                DataTable dtPatient = barcodebll.GetBarCodeMain(barcodelist[index]);
                if (dtPatient == null || dtPatient.Rows.Count == 0)
                {
                    continue;
                }

                //根据条码号获取条码明细项目信息
                string stritems = barcodebll.GetYzmcByBarCodeInStr(barcodelist[index]);

                //向dataForReport中添加条码的详细信息
                DataRow dr = dataForReport.NewRow();
                dr["barcode"] = barcodelist[index];
                dr["peopleinfo"] = dtPatient.Rows[0]["病历号"].ToString() + " " + dtPatient.Rows[0]["病人姓名"].ToString() + " " + dtPatient.Rows[0]["病人类型"].ToString() + "\n"
                                  + dtPatient.Rows[0]["性别"].ToString() + "/"
                                  + dtPatient.Rows[0]["年龄"].ToString() + dtPatient.Rows[0]["年龄单位"].ToString() + " "
                                  + dtPatient.Rows[0]["科室名称"].ToString() + "(" + dtPatient.Rows[0]["医生姓名"].ToString() + ") ";
                                  //+ dtPatient.Rows[0]["床号"].ToString() + (dtPatient.Rows[0]["床号"].ToString().Contains("床") ? "" : "床");
                dr["items"] = stritems;
                dataForReport.Rows.Add(dr);
            }


            //根据获取到的条码信息打印条码
            XtraReportBarCode repBarCode = new XtraReportBarCode(dataForReport);
            DevExpress.XtraReports.UI.ReportPrintTool tool = new DevExpress.XtraReports.UI.ReportPrintTool(repBarCode);

            if (PrinterExists(m_BarCodePrinterName))
            {
                tool.Print(m_BarCodePrinterName);
            }
            else
            {
                MessageBox.Show("没有找打LIS条码打印机: " + m_BarCodePrinterName);
            }
        }
    }
}
