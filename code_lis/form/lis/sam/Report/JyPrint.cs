﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ww.form.wwf.print;
using ww.wwf.wwfbll;
using ww.lis.lisbll.sam;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;

namespace ww.form.lis.sam.Report
{
    public class JyPrint
    { 
        /// <summary>
        /// 报告规则
        /// </summary>
        private jybll bllReport = new jybll();
        private InstrBLL bllInstr = new InstrBLL();
        private PersonBLL bllPerson = new PersonBLL();
        private SampleTypeBLL bllSample = new SampleTypeBLL();
        DeptBll bllDept = new DeptBll();
        DataSet dsPrint = new DataSet();
        samDataSet.sam_jy_printDataTable dtPrintDataTable = new samDataSet.sam_jy_printDataTable();
        string strMainDataSourceName = "samDataSet_sam_jy_print";//sam_jy_printDataTable
        public JyPrint()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //	
            dtPrintDataTable.Columns.Remove("结果图1");
            dtPrintDataTable.Columns.Remove("结果图2");
            dtPrintDataTable.Columns.Remove("结果图3");
            dtPrintDataTable.Columns.Remove("结果图4");
            dtPrintDataTable.Columns.Remove("结果图5");
            DataColumn D1 = new DataColumn("结果图1", typeof(System.Byte[]));
            dtPrintDataTable.Columns.Add(D1);

            DataColumn D2 = new DataColumn("结果图2", typeof(System.Byte[]));
            dtPrintDataTable.Columns.Add(D2);

            DataColumn D3 = new DataColumn("结果图3", typeof(System.Byte[]));
            dtPrintDataTable.Columns.Add(D3);

            DataColumn D4 = new DataColumn("结果图4", typeof(System.Byte[]));
            dtPrintDataTable.Columns.Add(D4);

            DataColumn D5 = new DataColumn("结果图5", typeof(System.Byte[]));
            dtPrintDataTable.Columns.Add(D5);


            if (dtPrintDataTable.Columns.Contains("检验者手签照"))
            {
                dtPrintDataTable.Columns.Remove("检验者手签照");
            }
            if (dtPrintDataTable.Columns.Contains("审核者手签照"))
            {
                dtPrintDataTable.Columns.Remove("审核者手签照");
            }

            DataColumn D6 = new DataColumn("检验者手签照", typeof(System.Byte[]));
            dtPrintDataTable.Columns.Add(D6);

            DataColumn D7 = new DataColumn("审核者手签照", typeof(System.Byte[]));
            dtPrintDataTable.Columns.Add(D7);
        }

        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="printType">1：打印预览；0：打印</param>
        /// <param name="strfsample_id">样本ID</param>
        public bool BllPrintViewer(int printType, string strfsample_id)//20150611 wjz 是否打印成功，修改返回类型 void ==> bool
        {
            bool printSuccess = false;
            try
            {
                DataTable dtReport = this.bllReport.GetList(" fjy_id='" + strfsample_id + "'");
                if (dtReport.Rows.Count > 0)
                {
                    string str仪器结果id = "";
                    DataTable dtIMG = new DataTable();// 
                    if (dtReport.Rows[0]["finstr_result_id"] != null)
                    {
                        str仪器结果id = dtReport.Rows[0]["finstr_result_id"].ToString();
                        dtIMG = this.bllReport.BllImgDT(strfsample_id,str仪器结果id);
                    }

                    DataTable dtResult = this.bllReport.GetListS(" t.fjy_id='" + strfsample_id + "'");
                    

                    string strfinstr_id = "";//仪器ID 
                    if (dtReport.Rows[0]["fjy_instr"] != null)
                        strfinstr_id = dtReport.Rows[0]["fjy_instr"].ToString();
                    DataTable dtInstrReport = this.bllInstr.BllReportDT(strfinstr_id, 1);

                    //changed by wjz 20151203 报表动态打印 ▽

                    //string strReportCode = "ww.form.lis.sam.Report.ReportCom.rdlc";//报表代码
                    string strReportCode = "ww.form.lis.sam.Report.ReportComLittle";//报表代码
                    //changed by wjz 20151203 报表动态打印 △

                    string strReportName = "";//报表名称
                    string strTimeAP = "";//检验时间安排 f1
                    string strCYdd = "";//采样地点 f2
                    if (dtInstrReport.Rows.Count > 0)
                    {
                        strReportCode = dtInstrReport.Rows[0]["fcode"].ToString();
                        strReportName = dtInstrReport.Rows[0]["fname"].ToString();
                        strTimeAP = dtInstrReport.Rows[0]["f1"].ToString();
                        strCYdd = dtInstrReport.Rows[0]["f2"].ToString();
                    }
                   

                    int intResultCount = dtResult.Rows.Count;//结果记录数
                    if (intResultCount > 0)
                    {
                        //---------结果值
                        int int序号 = 0;
                        for (int i = 0; i < intResultCount; i++)
                        {
                            int序号 = i + 1;
                            DataRow printRow = dtPrintDataTable.NewRow();
                            printRow["结果序号"] = int序号;
                            printRow["结果项目代码"] = dtResult.Rows[i]["fitem_code"];
                            printRow["结果项目名称"] = dtResult.Rows[i]["fitem_name"];
                            printRow["结果值"] = dtResult.Rows[i]["fvalue"];
                            printRow["结果项目单位"] = dtResult.Rows[i]["fitem_unit"];
                            printRow["结果标记"] = dtResult.Rows[i]["fitem_badge"];
                            printRow["结果参考值"] = dtResult.Rows[i]["fitem_ref"];
                            dtPrintDataTable.Rows.Add(printRow);
                        }

                        //----------报告值
                        dtPrintDataTable.Rows[0]["医院名称"] = LoginBLL.CustomerName;
                        dtPrintDataTable.Rows[0]["医院地址"] = LoginBLL.CustomerAdd;
                        dtPrintDataTable.Rows[0]["检验实验室"] = LoginBLL.strDeptName;
                        dtPrintDataTable.Rows[0]["医院联系电话"] = LoginBLL.CustomerTel;
                        dtPrintDataTable.Rows[0]["检验时间安排"] = strTimeAP;
                        dtPrintDataTable.Rows[0]["报表名称"] = strReportName;
                        dtPrintDataTable.Rows[0]["采样地点"] = strCYdd;
                        dtPrintDataTable.Rows[0]["姓名"] = dtReport.Rows[0]["fhz_name"];
                        //dtPrintDataTable.Rows[0]["性别"] = dtReport.Rows[0]["fhz_sex"];
                        dtPrintDataTable.Rows[0]["性别"] = dtReport.Rows[0]["性别"];
                        dtPrintDataTable.Rows[0]["住院号"] = dtReport.Rows[0]["fhz_zyh"];
                        dtPrintDataTable.Rows[0]["年龄"] = dtReport.Rows[0]["fhz_age"];
                        dtPrintDataTable.Rows[0]["年龄单位"] = dtReport.Rows[0]["年龄单位"];
                        dtPrintDataTable.Rows[0]["申请单号"] = dtReport.Rows[0]["fapply_id"];
                        dtPrintDataTable.Rows[0]["样本条码号"] = dtReport.Rows[0]["fapply_id"];
                        dtPrintDataTable.Rows[0]["样本号"] = dtReport.Rows[0]["fjy_yb_code"];
                        dtPrintDataTable.Rows[0]["检验类别"] = dtReport.Rows[0]["类型"];
                        dtPrintDataTable.Rows[0]["样本类别"] = dtReport.Rows[0]["样本"];
                        dtPrintDataTable.Rows[0]["送检科室"] = dtReport.Rows[0]["科室"];
                        dtPrintDataTable.Rows[0]["申请者"] = dtReport.Rows[0]["申请医师"];
                        dtPrintDataTable.Rows[0]["检验者"] = dtReport.Rows[0]["检验医师"];

                        dtPrintDataTable.Rows[0]["检验者手签照"] = bllPerson.GetSQZ(dtReport.Rows[0]["fjy_user_id"].ToString());

                        //20160517 changed by wjz 将审核者id改为审核者姓名的名称 ▽
                        //dtPrintDataTable.Rows[0]["审核者"] = dtReport.Rows[0]["fchenk_user_id"];
                        dtPrintDataTable.Rows[0]["审核者"] = dtReport.Rows[0]["审核者"];

                        dtPrintDataTable.Rows[0]["审核者手签照"] = bllPerson.GetSQZ(dtReport.Rows[0]["fchenk_user_id"].ToString());
                        //20160517 changed by wjz 将审核者id改为审核者姓名的名称 △

                        dtPrintDataTable.Rows[0]["床号"] = dtReport.Rows[0]["fhz_bed"];
                        dtPrintDataTable.Rows[0]["病人ID"] = dtReport.Rows[0]["fhz_id"];
                        //开始2015-06-03修改疾病名称的获取方式
                        //dtPrintDataTable.Rows[0]["诊断"] = dtReport.Rows[0]["fjy_lczd"];
                        dtPrintDataTable.Rows[0]["诊断"] = dtReport.Rows[0]["疾病名称"].ToString();
                        //结束修改
                        dtPrintDataTable.Rows[0]["申请时间"] = dtReport.Rows[0]["fapply_time"];
                        dtPrintDataTable.Rows[0]["采样时间"] = dtReport.Rows[0]["fsampling_time"];
                        //dtPrintDataTable.Rows[0]["收样时间"] = dtReport.Rows[0]["fget_user_time"];
                        try
                        {
                            //if (dtReport.Rows[0]["fsys_time"].ToString().Equals("")) { }
                            //else
                            //{
                            //    dtPrintDataTable.Rows[0]["检验时间"] = Convert.ToDateTime(dtReport.Rows[0]["fsys_time"].ToString()).ToString("MM-dd HH:mm");
                            //}
                            dtPrintDataTable.Rows[0]["检验时间"] = dtReport.Rows[0]["fjy_date"].ToString();
                        }
                        catch { }
                        try
                        {
                            if (dtReport.Rows[0]["freport_time"].ToString().Equals("")) { }
                            else
                            {
                                //dtPrintDataTable.Rows[0]["审核时间"] = Convert.ToDateTime(dtReport.Rows[0]["fcheck_time"].ToString()).ToString("MM-dd HH:mm");
                                this.dtPrintDataTable.Rows[0]["审核时间"] = System.Convert.ToDateTime(dtReport.Rows[0]["freport_time"].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
                            }
                        }
                        catch { }
                        if (printType == 1)
                        {
                            if (dtReport.Rows[0]["fprint_time"].ToString().Equals("")) { }
                            else
                            {
                                try
                                {
                                    dtPrintDataTable.Rows[0]["打印时间"] = Convert.ToDateTime(dtReport.Rows[0]["fprint_time"].ToString()).ToString("MM-dd HH:mm");
                                }
                                catch { }
                            }
                        }
                        else
                        {
                            dtPrintDataTable.Rows[0]["打印时间"] = DateTime.Now.ToString("MM-dd HH:mm");//dtReport.Rows[0]["fprint_time"];
                        }
                        dtPrintDataTable.Rows[0]["备注"] = dtReport.Rows[0]["fremark"];

                       

                        dsPrint.Tables.Add(dtPrintDataTable);
                        //----------图值
                        if (dtIMG.Rows.Count > 0)
                        {
                            for (int iimg = 0; iimg < dtIMG.Rows.Count; iimg++)
                            {
                                switch (iimg)
                                {
                                    case 0:
                                        dtPrintDataTable.Rows[0]["结果图1"] = dtIMG.Rows[iimg]["FImg"];
                                        dtPrintDataTable.Rows[0]["结果图1_名称"] = dtIMG.Rows[iimg]["FImgNmae"];
                                        break;
                                    case 1:
                                        dtPrintDataTable.Rows[0]["结果图2"] = dtIMG.Rows[iimg]["FImg"];
                                        dtPrintDataTable.Rows[0]["结果图2_名称"] = dtIMG.Rows[iimg]["FImgNmae"];
                                        break;
                                    case 2:
                                        dtPrintDataTable.Rows[0]["结果图3"] = dtIMG.Rows[iimg]["FImg"];
                                        dtPrintDataTable.Rows[0]["结果图3_名称"] = dtIMG.Rows[iimg]["FImgNmae"];
                                        break;
                                    case 3:
                                        dtPrintDataTable.Rows[0]["结果图4"] = dtIMG.Rows[iimg]["FImg"];
                                        dtPrintDataTable.Rows[0]["结果图4_名称"] = dtIMG.Rows[iimg]["FImgNmae"];
                                        break;
                                    case 4:
                                        dtPrintDataTable.Rows[0]["结果图5"] = dtIMG.Rows[iimg]["FImg"];
                                        dtPrintDataTable.Rows[0]["结果图5_名称"] = dtIMG.Rows[iimg]["FImgNmae"];
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                        printSuccess = WWPrintViewer(printType, strfsample_id, strReportName, strReportCode);
                    }
                    else
                    {
                        WWMessage.MessageShowWarning("报告结果为空！");
                    }
                }
                else
                {
                    WWMessage.MessageShowWarning("报告为空，操作失败！请选择报告后重试。");
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            return printSuccess;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strReportName">报表数据源名</param>
        /// <param name="strReportPath">报表文档</param>
        private bool WWPrintViewer(int printType, string fjy_id, string strReportName, string strReportPath) //20150611 wjz 是否打印成功，修改返回类型 void ==> bool
        {
            bool printSuccess = false;
            try
            {
                if (printType == 1)
                {
                    // //报表公共窗口
                    // ReportViewer feportviewer = new ReportViewer();
                    // //报表数据集
                    // feportviewer.MainDataSet = dsPrint;
                    // //报表名
                    // feportviewer.ReportName = strReportName;
                    // //报表数据源名
                    // feportviewer.MainDataSourceName = strMainDataSourceName;
                    // //报表文档
                    // feportviewer.ReportPath = strReportPath;
                    // //显示报表窗口
                    // feportviewer.ShowDialog();
                    //// this.bllReport.BllReportUpdatePrintTime(fjy_id);



                    string str样本号 = dtPrintDataTable.Rows[0]["样本号"].ToString();

                    //added by wjz 20151203 报表修改 ▽
                    if (string.IsNullOrWhiteSpace(strReportPath) || strReportPath == "ww.form.lis.sam.Report.ReportCom" || strReportPath == "ww.form.lis.sam.Report.ReportComLittle")
                    {
                        //added by wjz 20151203 报表修改 △
                        DialogResult dialogResult = DialogResult.No;
                        //changed by wjz 20160308 修改了ReportComLittle报表中的行间距，一页ReportComLittle可以放下36条数据，故调整if中的判断
                        if (dtPrintDataTable.Rows.Count > 34)//样本项目的记录大于30条的情况下  ：由30-->34 20160308
                        {
                            dialogResult = MessageBox.Show("当前样本(样本号：" + str样本号 + ")中的检查项目较多，是否使用A4纸打印？\n选择“是”,使用A4纸打印；\n选择“否”,使用A5纸打印。", "确认窗口", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        }

                        if (dialogResult == DialogResult.Yes)
                        {
                            ReportCom rep = new ReportCom(fjy_id, dtPrintDataTable);
                            ReportPrintTool tool = new ReportPrintTool(rep);
                            tool.ShowPreviewDialog();

                            ///TODO: 这部分需要根据打印状态进行更新
                            printSuccess = rep.GetPrintResult();
                        }
                        else
                        {
                            ReportComLittle repLittle = new ReportComLittle(fjy_id, dtPrintDataTable);
                            ReportPrintTool tool = new ReportPrintTool(repLittle);
                            tool.ShowPreviewDialog();

                            ///TODO: 这部分需要根据打印状态进行更新
                            printSuccess = repLittle.GetPrintResult();
                        }
                        //added by wjz 20151203 报表修改 ▽
                    }
                    else if (strReportPath == "ww.form.lis.sam.Report.ReportComImgLittle")
                    {
                        //Type type = Type.GetType("ww.form.lis.sam.Report.ReportComImgLittle");
                        //ReportBase rep = (type.Assembly.CreateInstance("ww.form.lis.sam.Report.ReportComImgLittle")) as ReportBase;
                        //rep.SetDataSource(fjy_id, dtPrintDataTable);
                        ReportComImgLittle rep = new ReportComImgLittle(fjy_id, dtPrintDataTable);
                        ReportPrintTool tool = new ReportPrintTool(rep);
                        tool.ShowPreviewDialog();

                        ///TODO: 这部分需要根据打印状态进行更新
                        printSuccess = rep.GetPrintResult();
                    }
                    else if(strReportPath == "ww.form.lis.sam.Report.ReportComImgNoChColorLittle")
                    {
                        ReportComImgNoChColorLittle rep = new ReportComImgNoChColorLittle(fjy_id, dtPrintDataTable);
                        ReportPrintTool tool = new ReportPrintTool(rep);
                        tool.ShowPreviewDialog();

                        ///TODO: 这部分需要根据打印状态进行更新
                        printSuccess = rep.GetPrintResult();
                    }
                    //added by wjz 20151203 报表修改 △
                    // add by wjz 20151222 报表通过反射机制加载 ▽
                    else
                    {
                        try
                        {
                            Type type = Type.GetType(strReportPath);
                            object[] parameters = new object[2];
                            parameters[0] = fjy_id;
                            parameters[1] = dtPrintDataTable;
                            object obj = System.Activator.CreateInstance(type, parameters);

                            DevExpress.XtraReports.UI.XtraReport rep = obj as DevExpress.XtraReports.UI.XtraReport;
                            ReportPrintTool tool = new ReportPrintTool(rep);
                            tool.ShowPreviewDialog();

                            //TODO:
                            //此处没有对 printSuccess赋值，由于使用了反射机制，暂时未找到合适的方式去获取report对象里的printSuccess值
                            //
                            //上述TODO中的问题已解决，实现如下：
                            System.Reflection.MethodInfo method = type.GetMethod("GetPrintResult");
                            Object ret = method.Invoke(obj, null);
                            printSuccess = (bool)ret;
                        }
                        catch
                        {
                            MessageBox.Show("打印报告单时出现异常，请联系技术人员。");
                            printSuccess = false;
                        }
                    }
                    // add by wjz 20151222 报表通过反射机制加载 △
                }
                else
                {
                    ////报表公共窗口
                    //ReportPrint feportviewer = new ReportPrint();
                    ////报表数据集
                    //feportviewer.MainDataSet = dsPrint;
                    ////报表名
                    //feportviewer.ReportName = strReportName;
                    ////报表数据源名
                    //feportviewer.MainDataSourceName = strMainDataSourceName;
                    ////报表文档
                    //feportviewer.ReportPath = strReportPath;
                    ////显示报表窗口
                    //feportviewer.ShowDialog();

                    //added by wjz 20151203 报表修改 ▽
                    if (string.IsNullOrWhiteSpace(strReportPath) || strReportPath == "ww.form.lis.sam.Report.ReportCom" || strReportPath == "ww.form.lis.sam.Report.ReportComLittle")
                    {
                    //added by wjz 20151203 报表修改 △
                        DialogResult dialogResult = DialogResult.No;

                        //changed by wjz 20160308 修改了ReportComLittle报表中的行间距，一页ReportComLittle可以放下36条数据，故调整if中的判断
                        if (dtPrintDataTable.Rows.Count > 34)//样本项目的记录大于30条的情况下: 由30-->34 20160308
                        {
                            dialogResult = MessageBox.Show("当前样本中的检查项目较多，是否使用A4纸打印？", "确认窗口", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        }

                        if (dialogResult == DialogResult.Yes)//样本项目的记录大于30条的情况下
                        {
                            ReportCom rep = new ReportCom(null, dtPrintDataTable);
                            rep.Print();
                        }
                        else
                        {
                            ReportComLittle rep = new ReportComLittle(null, dtPrintDataTable);
                            rep.Print();
                        }

                        //误删除，已经还原
                        //DEL BY WJZ 20151222 report打印时已经调用了此方法，此处是重复调用，所以删去 ▽
                        this.bllReport.BllReportUpdatePrintTime(fjy_id);
                        //DEL BY WJZ 20151222 report打印时已经调用了此方法，此处是重复调用，所以删去 △

                        printSuccess = true;
                    //added by wjz 20151203 报表修改 ▽
                    }
                    else if (strReportPath == "ww.form.lis.sam.Report.ReportComImgLittle")
                    {
                        //Type type = Type.GetType("ww.form.lis.sam.Report.ReportComImgLittle");
                        //ReportBase rep = (type.Assembly.CreateInstance("ww.form.lis.sam.Report.ReportComImgLittle")) as ReportBase;
                        //rep.SetDataSource(fjy_id, dtPrintDataTable);
                        ReportComImgLittle rep = new ReportComImgLittle(fjy_id, dtPrintDataTable);
                        //ReportPrintTool tool = new ReportPrintTool(rep);
                        //tool.ShowPreviewDialog();
                        rep.Print();

                        ///TODO: 这部分需要根据打印状态进行更新
                        printSuccess = rep.GetPrintResult();
                    }
                    else if (strReportPath == "ww.form.lis.sam.Report.ReportComImgNoChColorLittle")
                    {
                        ReportComImgNoChColorLittle rep = new ReportComImgNoChColorLittle(fjy_id, dtPrintDataTable);
                        //ReportPrintTool tool = new ReportPrintTool(rep);
                        //tool.ShowPreviewDialog();
                        rep.Print();
						
						printSuccess = rep.GetPrintResult();
                    }
                    //added by wjz 20151203 报表修改 △
                    //added by wjz 20151203 报表修改 △

                    // add by wjz 20151222 报表通过反射机制加载 ▽
                    else
                    {
                        try
                        {
                            Type type = Type.GetType(strReportPath);
                            object[] parameters = new object[2];
                            parameters[0] = fjy_id;
                            parameters[1] = dtPrintDataTable;

                            object obj = System.Activator.CreateInstance(type, parameters);
                            DevExpress.XtraReports.UI.XtraReport rep = obj as DevExpress.XtraReports.UI.XtraReport;
                            rep.Print();

                            printSuccess = true;
                        }
                        catch
                        {
                            MessageBox.Show("打印报告单时出现异常，请联系技术人员。");
                            printSuccess = false;
                        }
                    }
                    // add by wjz 20151222 报表通过反射机制加载 △
                }
               
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            return printSuccess;
        }
        

    }
}
