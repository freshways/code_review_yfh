﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace ww.form.lis.sam.Report
{
    public partial class ReportComImgLittle : DevExpress.XtraReports.UI.XtraReport
    {
        //public ReportComImgLittle()
        //{
        //    InitializeComponent();
        //}

        private string strJYid = null;

        private bool printSuccess = false;

        public bool GetPrintResult()
        {
            return printSuccess;
        }
        public ReportComImgLittle()
        {
            InitializeComponent();
        }
        public ReportComImgLittle(string strid, DataTable dtDataSource)
        {
            InitializeComponent();
            SetDataSource(strid, dtDataSource);
            #region old code
            //strJYid = strid;

            ////设置报告头信息
            //xrLabelTitle.Text = dtDataSource.Rows[0]["医院名称"].ToString() + "检验报告单";//+ dtDataSource.Rows[0]["报表名称"].ToString();
            //xrLabel姓名.Text = dtDataSource.Rows[0]["姓名"].ToString();
            //xrLabel性别.Text=dtDataSource.Rows[0]["性别"].ToString();
            //xrLabel年龄.Text=dtDataSource.Rows[0]["年龄"].ToString()+dtDataSource.Rows[0]["年龄单位"].ToString();
            //xrLabel科室.Text=dtDataSource.Rows[0]["送检科室"].ToString();
            //xrLabel床号.Text =dtDataSource.Rows[0]["床号"].ToString();

            //xrPictureBox1.DataBindings.Add("Image", dtDataSource.Rows[0], "结果图1");
            //xrPictureBox2.DataBindings.Add("Image", dtDataSource.Rows[0], "结果图2");
            //xrPictureBox3.DataBindings.Add("Image", dtDataSource.Rows[0], "结果图3");
            //xrPictureBox4.DataBindings.Add("Image", dtDataSource.Rows[0], "结果图4");

            //imgName1.Text = dtDataSource.Rows[0]["结果图1_名称"].ToString();
            //imgName2.Text = dtDataSource.Rows[0]["结果图2_名称"].ToString();
            //imgName3.Text = dtDataSource.Rows[0]["结果图3_名称"].ToString();
            //imgName4.Text = dtDataSource.Rows[0]["结果图4_名称"].ToString();

            ////xrLabel样本类型.Text = dtDataSource.Rows[0]["样本类别"].ToString();
            ////xrLabel患者类别.Text = dtDataSource.Rows[0]["检验类别"].ToString();
            ////xrLabel病历号.Text=
            //xrLabel临床诊断.Text=dtDataSource.Rows[0]["诊断"].ToString();
            //xrLabel样本号.Text=dtDataSource.Rows[0]["样本号"].ToString();
            //xrLabel条码.Text=dtDataSource.Rows[0]["样本条码号"].ToString();
            //xrLabel送检医师.Text=dtDataSource.Rows[0]["申请者"].ToString();

            //xrLabel备注.Text = dtDataSource.Rows[0]["备注"].ToString();
            //xrLabel检验时间.Text =dtDataSource.Rows[0]["检验时间"].ToString();
            //xrLabel检验者.Text = dtDataSource.Rows[0]["检验者"].ToString();
            //xrLabel医院地址.Text = dtDataSource.Rows[0]["医院地址"].ToString();
            ////xrLabel检验时间安排.Text = dtDataSource.Rows[0]["检验时间安排"].ToString();
            ////xrLabel采样地点.Text = dtDataSource.Rows[0]["采样地点"].ToString();

            //xrLabel报告时间.Text = dtDataSource.Rows[0]["审核时间"].ToString();
            ////xrLabel审核者.Text = dtDataSource.Rows[0]["审核者"].ToString();

            ////xrLabel打印时间.Text = dtDataSource.Rows[0]["打印时间"].ToString();
            //xrLabel打印时间.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //xrLabel实验室.Text = dtDataSource.Rows[0]["检验实验室"].ToString();
            //xrLabel联系电话.Text = dtDataSource.Rows[0]["医院联系电话"].ToString();

            //xrTableCell序号.DataBindings.Add("Text", null, "结果序号");
            //xrTableCell项目代码.DataBindings.Add("Text", null, "结果项目代码");
            //xrTableCell项目名称.DataBindings.Add("Text", null, "结果项目名称");
            //xrTableCell结果.DataBindings.Add("Text", null, "结果值");
            //xrTableCell单位.DataBindings.Add("Text", null, "结果项目单位");
            //xrTableCell标记.DataBindings.Add("Text", null, "结果标记");
            //xrTableCell参考值.DataBindings.Add("Text", null, "结果参考值");

            ////printRow["结果序号"] = int序号;
            ////printRow["结果项目代码"] = dtResult.Rows[i]["fitem_code"];
            ////printRow["结果项目名称"] = dtResult.Rows[i]["fitem_name"];
            ////printRow["结果值"] = dtResult.Rows[i]["fvalue"];
            ////printRow["结果项目单位"] = dtResult.Rows[i]["fitem_unit"];
            ////printRow["结果标记"] = dtResult.Rows[i]["fitem_badge"];
            ////printRow["结果参考值"] = dtResult.Rows[i]["fitem_ref"];
            //this.DataSource = dtDataSource;
            #endregion
        }

        public void SetDataSource(string strid, DataTable dtDataSource)
        {
            strJYid = strid;

            //设置报告头信息
            xrLabelTitle.Text = dtDataSource.Rows[0]["医院名称"].ToString() + "检验报告单";//+ dtDataSource.Rows[0]["报表名称"].ToString();
            xrLabel姓名.Text = dtDataSource.Rows[0]["姓名"].ToString();
            xrLabel性别.Text = dtDataSource.Rows[0]["性别"].ToString();
            xrLabel年龄.Text = dtDataSource.Rows[0]["年龄"].ToString() + dtDataSource.Rows[0]["年龄单位"].ToString();
            xrLabel科室.Text = dtDataSource.Rows[0]["送检科室"].ToString();
            xrLabel床号.Text = dtDataSource.Rows[0]["床号"].ToString();

            //xrPictureBox1.DataBindings.Add("Image", dtDataSource, "结果图1");
            //xrPictureBox2.DataBindings.Add("Image", dtDataSource, "结果图2");
            //xrPictureBox3.DataBindings.Add("Image", dtDataSource, "结果图3");
            //xrPictureBox4.DataBindings.Add("Image", dtDataSource, "结果图4");

            try
            {
                xrPictureBox1.Image = ImageHelper.ImageToGray((byte[])(dtDataSource.Rows[0]["结果图1"]));
                xrPictureBox2.Image = ImageHelper.ImageToGray((byte[])(dtDataSource.Rows[0]["结果图2"]));
                xrPictureBox3.Image = ImageHelper.ImageToGray((byte[])(dtDataSource.Rows[0]["结果图3"]));
                xrPictureBox4.Image = ImageHelper.ImageToGray((byte[])(dtDataSource.Rows[0]["结果图4"]));

                imgName1.Text = dtDataSource.Rows[0]["结果图1_名称"].ToString();
                imgName2.Text = dtDataSource.Rows[0]["结果图2_名称"].ToString();
                imgName3.Text = dtDataSource.Rows[0]["结果图3_名称"].ToString();
                imgName4.Text = dtDataSource.Rows[0]["结果图4_名称"].ToString();
            }
            catch
            {
                xrLine3.Visible = false;
                xrPictureBox1.Visible = false;
                xrPictureBox2.Visible = false;
                xrPictureBox3.Visible = false;
                xrPictureBox4.Visible = false;
                imgName1.Visible = false;
                imgName2.Visible = false;
                imgName3.Visible = false;
                imgName4.Visible = false;

                this.xrLine4.LocationF = new PointF(this.xrLine4.LocationF.X, this.xrLine4.LocationF.Y - 116);
                this.xrLabel检验时间.LocationF = new PointF(this.xrLabel检验时间.LocationF.X, this.xrLabel检验时间.LocationF.Y - 116);
                this.xrLabel38.LocationF = new PointF(this.xrLabel38.LocationF.X, this.xrLabel38.LocationF.Y - 116);
                this.xrLabel27.LocationF = new PointF(this.xrLabel27.LocationF.X, this.xrLabel27.LocationF.Y - 116);
                this.xrLabel28.LocationF = new PointF(this.xrLabel28.LocationF.X, this.xrLabel28.LocationF.Y - 116);
                this.xrLabel29.LocationF = new PointF(this.xrLabel29.LocationF.X, this.xrLabel29.LocationF.Y - 116);
                this.xrLabel1.LocationF = new PointF(this.xrLabel1.LocationF.X, this.xrLabel1.LocationF.Y - 116);
                this.xrLabel检验者.LocationF = new PointF(this.xrLabel检验者.LocationF.X, this.xrLabel检验者.LocationF.Y - 116);
                this.xrLabel医院地址.LocationF = new PointF(this.xrLabel医院地址.LocationF.X, this.xrLabel医院地址.LocationF.Y - 116);
                this.xrLabel报告时间.LocationF = new PointF(this.xrLabel报告时间.LocationF.X, this.xrLabel报告时间.LocationF.Y - 116);
                this.xrLabel联系电话.LocationF = new PointF(this.xrLabel联系电话.LocationF.X, this.xrLabel联系电话.LocationF.Y - 116);
                this.xrLabel40.LocationF = new PointF(this.xrLabel40.LocationF.X, this.xrLabel40.LocationF.Y - 116);
                this.xrLabel审核者.LocationF = new PointF(this.xrLabel审核者.LocationF.X, this.xrLabel审核者.LocationF.Y - 116);
                this.xrLabel42.LocationF = new PointF(this.xrLabel42.LocationF.X, this.xrLabel42.LocationF.Y - 116);
                this.xrLabel打印时间.LocationF = new PointF(this.xrLabel打印时间.LocationF.X, this.xrLabel打印时间.LocationF.Y - 116);
                this.xrLabel44.LocationF = new PointF(this.xrLabel44.LocationF.X, this.xrLabel44.LocationF.Y - 116);
                this.xrLabel实验室.LocationF = new PointF(this.xrLabel实验室.LocationF.X, this.xrLabel实验室.LocationF.Y - 116);
                this.xrLabel46.LocationF = new PointF(this.xrLabel46.LocationF.X, this.xrLabel46.LocationF.Y - 116);
                this.PageFooter.HeightF -= 116;
            }
            //xrLabel样本类型.Text = dtDataSource.Rows[0]["样本类别"].ToString();
            xrLabel患者类别.Text = dtDataSource.Rows[0]["检验类别"].ToString();
            //xrLabel病历号.Text=
            xrLabel病历号.Text = dtDataSource.Rows[0]["住院号"].ToString();
            xrLabel临床诊断.Text = dtDataSource.Rows[0]["诊断"].ToString();
            xrLabel样本号.Text = dtDataSource.Rows[0]["样本号"].ToString();
            xrLabel条码.Text = dtDataSource.Rows[0]["样本条码号"].ToString();
            xrLabel送检医师.Text = dtDataSource.Rows[0]["申请者"].ToString();

            xrLabel备注.Text = dtDataSource.Rows[0]["备注"].ToString();
            xrLabel检验时间.Text = dtDataSource.Rows[0]["检验时间"].ToString();
            xrLabel检验者.Text = dtDataSource.Rows[0]["检验者"].ToString();
            xrLabel医院地址.Text = dtDataSource.Rows[0]["医院地址"].ToString();
            //xrLabel检验时间安排.Text = dtDataSource.Rows[0]["检验时间安排"].ToString();
            //xrLabel采样地点.Text = dtDataSource.Rows[0]["采样地点"].ToString();

            xrLabel报告时间.Text = dtDataSource.Rows[0]["审核时间"].ToString();
            //xrLabel审核者.Text = dtDataSource.Rows[0]["审核者"].ToString();

            if (dtDataSource.Rows[0]["检验者手签照"] == null || dtDataSource.Rows[0]["检验者手签照"].ToString() == "")
            {
                xrPictureBox检验者.Visible = false;
            }
            else
            {
                xrLabel检验者.Visible = false;
                xrPictureBox检验者.Image = ImageHelper.GetImageBytes((byte[])(dtDataSource.Rows[0]["检验者手签照"]));
            }

            if (dtDataSource.Rows[0]["审核者手签照"] == null || dtDataSource.Rows[0]["审核者手签照"].ToString() == "")
            {
                xrPictureBox审核者.Visible = false;
            }
            else
            {
                xrLabel审核者.Visible = false;
                xrPictureBox审核者.Image = ImageHelper.GetImageBytes((byte[])(dtDataSource.Rows[0]["审核者手签照"]));
            }

            //xrLabel打印时间.Text = dtDataSource.Rows[0]["打印时间"].ToString();
            //xrLabel打印时间.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            xrLabel实验室.Text = dtDataSource.Rows[0]["检验实验室"].ToString();
            xrLabel联系电话.Text = dtDataSource.Rows[0]["医院联系电话"].ToString();

            xrTableCell序号.DataBindings.Add("Text", null, "结果序号");
            xrTableCell项目代码.DataBindings.Add("Text", null, "结果项目代码");
            xrTableCell项目名称.DataBindings.Add("Text", null, "结果项目名称");
            xrTableCell结果.DataBindings.Add("Text", null, "结果值");
            xrTableCell单位.DataBindings.Add("Text", null, "结果项目单位");
            xrTableCell标记.DataBindings.Add("Text", null, "结果标记");
            xrTableCell参考值.DataBindings.Add("Text", null, "结果参考值");

            //printRow["结果序号"] = int序号;
            //printRow["结果项目代码"] = dtResult.Rows[i]["fitem_code"];
            //printRow["结果项目名称"] = dtResult.Rows[i]["fitem_name"];
            //printRow["结果值"] = dtResult.Rows[i]["fvalue"];
            //printRow["结果项目单位"] = dtResult.Rows[i]["fitem_unit"];
            //printRow["结果标记"] = dtResult.Rows[i]["fitem_badge"];
            //printRow["结果参考值"] = dtResult.Rows[i]["fitem_ref"];
            this.DataSource = dtDataSource;
        }

        private void ReportComImgLittle_PrintProgress(object sender, DevExpress.XtraPrinting.PrintProgressEventArgs e)
        {
            if (!printSuccess && !(string.IsNullOrWhiteSpace(strJYid)))
            {
                ww.lis.lisbll.sam.jybll bllReport = new ww.lis.lisbll.sam.jybll();
                bllReport.BllReportUpdatePrintTime(strJYid);
            }
            printSuccess = true;
        }

    }
}
