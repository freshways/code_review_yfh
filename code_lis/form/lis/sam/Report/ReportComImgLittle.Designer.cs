﻿namespace ww.form.lis.sam.Report
{
    partial class ReportComImgLittle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell序号 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell项目代码 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell项目名称 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell结果 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell标记 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell单位 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell参考值 = new DevExpress.XtraReports.UI.XRTableCell();
            this.formattingRuleCenterLine = new DevExpress.XtraReports.UI.FormattingRule();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrLabel患者类别 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabelTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel样本号 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel床号 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel科室 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel年龄 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel性别 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel临床诊断 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel病历号 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel条码 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel送检医师 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel备注 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel实验室 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel打印时间 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel审核者 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel联系电话 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel报告时间 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel医院地址 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel检验者 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel检验时间 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrPictureBox4 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.imgName4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox3 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.imgName3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox2 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.imgName2 = new DevExpress.XtraReports.UI.XRLabel();
            this.imgName1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrPictureBox审核者 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrPictureBox检验者 = new DevExpress.XtraReports.UI.XRPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.Detail.HeightF = 17F;
            this.Detail.MultiColumn.ColumnCount = 2;
            this.Detail.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnCount;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(1.000007F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(386.5005F, 17F);
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell序号,
            this.xrTableCell项目代码,
            this.xrTableCell项目名称,
            this.xrTableCell结果,
            this.xrTableCell标记,
            this.xrTableCell单位,
            this.xrTableCell参考值});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell序号
            // 
            this.xrTableCell序号.Name = "xrTableCell序号";
            this.xrTableCell序号.Weight = 0.20890808708402359D;
            // 
            // xrTableCell项目代码
            // 
            this.xrTableCell项目代码.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCell项目代码.Name = "xrTableCell项目代码";
            this.xrTableCell项目代码.StylePriority.UseFont = false;
            this.xrTableCell项目代码.Weight = 0.5602991943557446D;
            // 
            // xrTableCell项目名称
            // 
            this.xrTableCell项目名称.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrTableCell项目名称.Name = "xrTableCell项目名称";
            this.xrTableCell项目名称.StylePriority.UseFont = false;
            this.xrTableCell项目名称.Weight = 0.92168231265491862D;
            // 
            // xrTableCell结果
            // 
            this.xrTableCell结果.Name = "xrTableCell结果";
            this.xrTableCell结果.Weight = 0.53988694663524317D;
            // 
            // xrTableCell标记
            // 
            this.xrTableCell标记.Name = "xrTableCell标记";
            this.xrTableCell标记.StylePriority.UseTextAlignment = false;
            this.xrTableCell标记.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell标记.Weight = 0.30498786206385242D;
            // 
            // xrTableCell单位
            // 
            this.xrTableCell单位.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell单位.Name = "xrTableCell单位";
            this.xrTableCell单位.StylePriority.UseFont = false;
            this.xrTableCell单位.Weight = 0.56954197120427041D;
            // 
            // xrTableCell参考值
            // 
            this.xrTableCell参考值.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCell参考值.Name = "xrTableCell参考值";
            this.xrTableCell参考值.StylePriority.UseFont = false;
            this.xrTableCell参考值.Weight = 0.74196825132534427D;
            // 
            // formattingRuleCenterLine
            // 
            this.formattingRuleCenterLine.Condition = "((ToInt([结果序号])   %  15)  <= 14) And (((ToInt([结果序号]) -1)/ 15) % 2 == 0)";
            // 
            // 
            // 
            this.formattingRuleCenterLine.Formatting.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.formattingRuleCenterLine.Name = "formattingRuleCenterLine";
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel患者类别,
            this.xrTable2,
            this.xrLine1,
            this.xrTable1,
            this.xrLine2,
            this.xrLabelTitle,
            this.xrLabel样本号,
            this.xrLabel床号,
            this.xrLabel科室,
            this.xrLabel年龄,
            this.xrLabel性别,
            this.xrLabel临床诊断,
            this.xrLabel病历号,
            this.xrLabel16,
            this.xrLabel15,
            this.xrLabel条码,
            this.xrLabel14,
            this.xrLabel11,
            this.xrLabel10,
            this.xrLabel姓名,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel4,
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel送检医师,
            this.xrLabel17,
            this.xrLabel备注});
            this.PageHeader.HeightF = 124.0833F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrLabel患者类别
            // 
            this.xrLabel患者类别.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel患者类别.LocationFloat = new DevExpress.Utils.PointFloat(589.3323F, 15.99995F);
            this.xrLabel患者类别.Name = "xrLabel患者类别";
            this.xrLabel患者类别.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel患者类别.SizeF = new System.Drawing.SizeF(136.7251F, 18F);
            this.xrLabel患者类别.StylePriority.UseFont = false;
            this.xrLabel患者类别.StylePriority.UseTextAlignment = false;
            this.xrLabel患者类别.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 102F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(387.5005F, 20F);
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell9,
            this.xrTableCell10});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "No";
            this.xrTableCell4.Weight = 0.20592867105496435D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "项目代码";
            this.xrTableCell5.Weight = 0.577029249689329D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "项目名称";
            this.xrTableCell6.Weight = 0.94920278137164515D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Text = "结果";
            this.xrTableCell7.Weight = 0.55600740288024231D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Text = "标记";
            this.xrTableCell8.Weight = 0.31409407494506392D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Text = "单位";
            this.xrTableCell9.Weight = 0.58654821624743714D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.Text = "参考值";
            this.xrTableCell10.Weight = 0.80106245633405115D;
            // 
            // xrLine1
            // 
            this.xrLine1.LineWidth = 2;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 100F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(776F, 2F);
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(389.4594F, 102.0001F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(386.5005F, 20F);
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3,
            this.xrTableCell11,
            this.xrTableCell12,
            this.xrTableCell13,
            this.xrTableCell14});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "No";
            this.xrTableCell1.Weight = 0.18125022888183592D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "项目代码";
            this.xrTableCell2.Weight = 0.57916641235351574D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "项目名称";
            this.xrTableCell3.Weight = 0.92187519073486346D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Text = "结果";
            this.xrTableCell11.Weight = 0.53999997666810029D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Text = "标记";
            this.xrTableCell12.Weight = 0.30505170294310557D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Text = "单位";
            this.xrTableCell13.Weight = 0.5696615266799927D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Text = "参考值";
            this.xrTableCell14.Weight = 0.76799970071865353D;
            // 
            // xrLine2
            // 
            this.xrLine2.BorderWidth = 1.5F;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 122F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(775.9999F, 2.083328F);
            this.xrLine2.StylePriority.UseBorderWidth = false;
            // 
            // xrLabelTitle
            // 
            this.xrLabelTitle.LocationFloat = new DevExpress.Utils.PointFloat(220.3125F, 15.99995F);
            this.xrLabelTitle.Name = "xrLabelTitle";
            this.xrLabelTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelTitle.SizeF = new System.Drawing.SizeF(316.6667F, 23F);
            this.xrLabelTitle.StylePriority.UseTextAlignment = false;
            this.xrLabelTitle.Text = "xrLabelTitle";
            this.xrLabelTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel样本号
            // 
            this.xrLabel样本号.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel样本号.LocationFloat = new DevExpress.Utils.PointFloat(655.9989F, 45.99997F);
            this.xrLabel样本号.Name = "xrLabel样本号";
            this.xrLabel样本号.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel样本号.SizeF = new System.Drawing.SizeF(111.001F, 18F);
            this.xrLabel样本号.StylePriority.UseFont = false;
            this.xrLabel样本号.StylePriority.UseTextAlignment = false;
            this.xrLabel样本号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel床号
            // 
            this.xrLabel床号.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel床号.LocationFloat = new DevExpress.Utils.PointFloat(456.0843F, 64.00003F);
            this.xrLabel床号.Name = "xrLabel床号";
            this.xrLabel床号.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel床号.SizeF = new System.Drawing.SizeF(132.8683F, 18F);
            this.xrLabel床号.StylePriority.UseFont = false;
            this.xrLabel床号.StylePriority.UseTextAlignment = false;
            this.xrLabel床号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel科室
            // 
            this.xrLabel科室.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel科室.LocationFloat = new DevExpress.Utils.PointFloat(253.7344F, 63.99997F);
            this.xrLabel科室.Name = "xrLabel科室";
            this.xrLabel科室.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel科室.SizeF = new System.Drawing.SizeF(136.7249F, 18F);
            this.xrLabel科室.StylePriority.UseFont = false;
            this.xrLabel科室.StylePriority.UseTextAlignment = false;
            this.xrLabel科室.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel年龄
            // 
            this.xrLabel年龄.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel年龄.LocationFloat = new DevExpress.Utils.PointFloat(456.0844F, 46F);
            this.xrLabel年龄.Name = "xrLabel年龄";
            this.xrLabel年龄.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel年龄.SizeF = new System.Drawing.SizeF(71.87508F, 18F);
            this.xrLabel年龄.StylePriority.UseFont = false;
            this.xrLabel年龄.StylePriority.UseTextAlignment = false;
            this.xrLabel年龄.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel性别
            // 
            this.xrLabel性别.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel性别.LocationFloat = new DevExpress.Utils.PointFloat(253.7344F, 45.99997F);
            this.xrLabel性别.Name = "xrLabel性别";
            this.xrLabel性别.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel性别.SizeF = new System.Drawing.SizeF(71.87508F, 18F);
            this.xrLabel性别.StylePriority.UseFont = false;
            this.xrLabel性别.StylePriority.UseTextAlignment = false;
            this.xrLabel性别.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel临床诊断
            // 
            this.xrLabel临床诊断.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel临床诊断.LocationFloat = new DevExpress.Utils.PointFloat(253.7344F, 82F);
            this.xrLabel临床诊断.Name = "xrLabel临床诊断";
            this.xrLabel临床诊断.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel临床诊断.SizeF = new System.Drawing.SizeF(133.7661F, 18.00001F);
            this.xrLabel临床诊断.StylePriority.UseFont = false;
            this.xrLabel临床诊断.StylePriority.UseTextAlignment = false;
            this.xrLabel临床诊断.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel病历号
            // 
            this.xrLabel病历号.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel病历号.LocationFloat = new DevExpress.Utils.PointFloat(86.62491F, 64F);
            this.xrLabel病历号.Name = "xrLabel病历号";
            this.xrLabel病历号.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel病历号.SizeF = new System.Drawing.SizeF(101.4844F, 18F);
            this.xrLabel病历号.StylePriority.UseFont = false;
            this.xrLabel病历号.StylePriority.UseTextAlignment = false;
            this.xrLabel病历号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(588.9526F, 64F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(66.66663F, 18F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "送检医师:";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(20F, 82F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(66.62492F, 18F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "条　　码:";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel条码
            // 
            this.xrLabel条码.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel条码.LocationFloat = new DevExpress.Utils.PointFloat(86.62504F, 82F);
            this.xrLabel条码.Name = "xrLabel条码";
            this.xrLabel条码.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel条码.SizeF = new System.Drawing.SizeF(101.4843F, 18F);
            this.xrLabel条码.StylePriority.UseFont = false;
            this.xrLabel条码.StylePriority.UseTextAlignment = false;
            this.xrLabel条码.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(589.3323F, 45.99997F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(66.66663F, 18F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "样  本  号:";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(188.1094F, 82F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(65.62502F, 17.99999F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "临床诊断:";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(21F, 64F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(65.62494F, 18F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "病  历  号:";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel姓名
            // 
            this.xrLabel姓名.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel姓名.LocationFloat = new DevExpress.Utils.PointFloat(86.62494F, 46F);
            this.xrLabel姓名.Name = "xrLabel姓名";
            this.xrLabel姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel姓名.SizeF = new System.Drawing.SizeF(101.4844F, 18F);
            this.xrLabel姓名.StylePriority.UseFont = false;
            this.xrLabel姓名.StylePriority.UseTextAlignment = false;
            this.xrLabel姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(390.4594F, 64F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(65.625F, 18F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "床　　号:";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(188.1094F, 63.99997F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(65.625F, 17.99999F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "科　　室:";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(390.4594F, 46F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(65.625F, 18F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "年　　龄:";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(188.1094F, 45.99997F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(65.625F, 18F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "性　　别:";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(20.99994F, 46F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(65.625F, 18F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "姓　　名:";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel送检医师
            // 
            this.xrLabel送检医师.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel送检医师.LocationFloat = new DevExpress.Utils.PointFloat(655.6193F, 64F);
            this.xrLabel送检医师.Name = "xrLabel送检医师";
            this.xrLabel送检医师.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel送检医师.SizeF = new System.Drawing.SizeF(111.3805F, 18F);
            this.xrLabel送检医师.StylePriority.UseFont = false;
            this.xrLabel送检医师.StylePriority.UseTextAlignment = false;
            this.xrLabel送检医师.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(390.4594F, 82.00003F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(65.625F, 18F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "备　　注:";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel备注
            // 
            this.xrLabel备注.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel备注.LocationFloat = new DevExpress.Utils.PointFloat(456.0843F, 82.00003F);
            this.xrLabel备注.Name = "xrLabel备注";
            this.xrLabel备注.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel备注.SizeF = new System.Drawing.SizeF(310.9157F, 18F);
            this.xrLabel备注.StylePriority.UseFont = false;
            this.xrLabel备注.StylePriority.UseTextAlignment = false;
            this.xrLabel备注.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine3
            // 
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(0.0401001F, 0F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(776.9599F, 2F);
            // 
            // xrLabel46
            // 
            this.xrLabel46.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(494.9583F, 118F);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(65.625F, 18F);
            this.xrLabel46.StylePriority.UseFont = false;
            this.xrLabel46.StylePriority.UseTextAlignment = false;
            this.xrLabel46.Text = "联系电话:";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel实验室
            // 
            this.xrLabel实验室.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel实验室.LocationFloat = new DevExpress.Utils.PointFloat(747.5906F, 118F);
            this.xrLabel实验室.Name = "xrLabel实验室";
            this.xrLabel实验室.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel实验室.SizeF = new System.Drawing.SizeF(27.3822F, 17.99999F);
            this.xrLabel实验室.StylePriority.UseFont = false;
            this.xrLabel实验室.StylePriority.UseTextAlignment = false;
            this.xrLabel实验室.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel实验室.Visible = false;
            // 
            // xrLabel44
            // 
            this.xrLabel44.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel44.LocationFloat = new DevExpress.Utils.PointFloat(681.9656F, 118F);
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel44.SizeF = new System.Drawing.SizeF(65.625F, 18F);
            this.xrLabel44.StylePriority.UseFont = false;
            this.xrLabel44.StylePriority.UseTextAlignment = false;
            this.xrLabel44.Text = "实 验  室:";
            this.xrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel44.Visible = false;
            // 
            // xrLabel打印时间
            // 
            this.xrLabel打印时间.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel打印时间.LocationFloat = new DevExpress.Utils.PointFloat(322.0417F, 118F);
            this.xrLabel打印时间.Name = "xrLabel打印时间";
            this.xrLabel打印时间.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel打印时间.SizeF = new System.Drawing.SizeF(133.3333F, 18F);
            this.xrLabel打印时间.StylePriority.UseFont = false;
            this.xrLabel打印时间.StylePriority.UseTextAlignment = false;
            this.xrLabel打印时间.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel42
            // 
            this.xrLabel42.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel42.LocationFloat = new DevExpress.Utils.PointFloat(256.4165F, 118F);
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel42.SizeF = new System.Drawing.SizeF(65.625F, 18F);
            this.xrLabel42.StylePriority.UseFont = false;
            this.xrLabel42.StylePriority.UseTextAlignment = false;
            this.xrLabel42.Text = "打印时间:";
            this.xrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel审核者
            // 
            this.xrLabel审核者.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel审核者.LocationFloat = new DevExpress.Utils.PointFloat(322.0417F, 136F);
            this.xrLabel审核者.Name = "xrLabel审核者";
            this.xrLabel审核者.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel审核者.SizeF = new System.Drawing.SizeF(133.3333F, 18F);
            this.xrLabel审核者.StylePriority.UseFont = false;
            this.xrLabel审核者.StylePriority.UseTextAlignment = false;
            this.xrLabel审核者.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel40
            // 
            this.xrLabel40.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(256.4165F, 136F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(65.625F, 18F);
            this.xrLabel40.StylePriority.UseFont = false;
            this.xrLabel40.StylePriority.UseTextAlignment = false;
            this.xrLabel40.Text = "审 核  者:";
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel联系电话
            // 
            this.xrLabel联系电话.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel联系电话.LocationFloat = new DevExpress.Utils.PointFloat(560.5833F, 118F);
            this.xrLabel联系电话.Name = "xrLabel联系电话";
            this.xrLabel联系电话.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel联系电话.SizeF = new System.Drawing.SizeF(115.6252F, 17.99999F);
            this.xrLabel联系电话.StylePriority.UseFont = false;
            this.xrLabel联系电话.StylePriority.UseTextAlignment = false;
            this.xrLabel联系电话.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel报告时间
            // 
            this.xrLabel报告时间.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel报告时间.LocationFloat = new DevExpress.Utils.PointFloat(560.5835F, 136F);
            this.xrLabel报告时间.Name = "xrLabel报告时间";
            this.xrLabel报告时间.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel报告时间.SizeF = new System.Drawing.SizeF(115.6251F, 18F);
            this.xrLabel报告时间.StylePriority.UseFont = false;
            this.xrLabel报告时间.StylePriority.UseTextAlignment = false;
            this.xrLabel报告时间.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel医院地址
            // 
            this.xrLabel医院地址.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel医院地址.LocationFloat = new DevExpress.Utils.PointFloat(747.5906F, 136F);
            this.xrLabel医院地址.Name = "xrLabel医院地址";
            this.xrLabel医院地址.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel医院地址.SizeF = new System.Drawing.SizeF(11.45819F, 17.99998F);
            this.xrLabel医院地址.StylePriority.UseFont = false;
            this.xrLabel医院地址.StylePriority.UseTextAlignment = false;
            this.xrLabel医院地址.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel医院地址.Visible = false;
            // 
            // xrLabel检验者
            // 
            this.xrLabel检验者.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel检验者.LocationFloat = new DevExpress.Utils.PointFloat(86.6251F, 136F);
            this.xrLabel检验者.Name = "xrLabel检验者";
            this.xrLabel检验者.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel检验者.SizeF = new System.Drawing.SizeF(129.1666F, 18F);
            this.xrLabel检验者.StylePriority.UseFont = false;
            this.xrLabel检验者.StylePriority.UseTextAlignment = false;
            this.xrLabel检验者.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel检验时间
            // 
            this.xrLabel检验时间.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel检验时间.LocationFloat = new DevExpress.Utils.PointFloat(86.62504F, 118F);
            this.xrLabel检验时间.Name = "xrLabel检验时间";
            this.xrLabel检验时间.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel检验时间.SizeF = new System.Drawing.SizeF(129.1666F, 18F);
            this.xrLabel检验时间.StylePriority.UseFont = false;
            this.xrLabel检验时间.StylePriority.UseTextAlignment = false;
            this.xrLabel检验时间.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(681.9656F, 136F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(65.625F, 18F);
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.StylePriority.UseTextAlignment = false;
            this.xrLabel29.Text = "医院地址:";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel29.Visible = false;
            // 
            // xrLabel28
            // 
            this.xrLabel28.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(21.00002F, 136F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(65.625F, 18F);
            this.xrLabel28.StylePriority.UseFont = false;
            this.xrLabel28.StylePriority.UseTextAlignment = false;
            this.xrLabel28.Text = "检  验  者:";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(21.00002F, 118F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(65.625F, 18F);
            this.xrLabel27.StylePriority.UseFont = false;
            this.xrLabel27.StylePriority.UseTextAlignment = false;
            this.xrLabel27.Text = "检验日期:";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel38
            // 
            this.xrLabel38.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(494.9583F, 136F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(65.625F, 18F);
            this.xrLabel38.StylePriority.UseFont = false;
            this.xrLabel38.StylePriority.UseTextAlignment = false;
            this.xrLabel38.Text = "报告时间:";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(494.9583F, 154F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(255.2082F, 20F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "注：此检测结果仅对本次检测样品有效！";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox审核者,
            this.xrPictureBox检验者,
            this.xrLine4,
            this.xrPictureBox4,
            this.imgName4,
            this.xrPictureBox3,
            this.imgName3,
            this.xrPictureBox2,
            this.imgName2,
            this.imgName1,
            this.xrPictureBox1,
            this.xrLabel检验时间,
            this.xrLabel38,
            this.xrLabel27,
            this.xrLabel28,
            this.xrLabel29,
            this.xrLabel1,
            this.xrLabel检验者,
            this.xrLabel医院地址,
            this.xrLabel报告时间,
            this.xrLabel联系电话,
            this.xrLabel40,
            this.xrLabel审核者,
            this.xrLabel42,
            this.xrLabel打印时间,
            this.xrLabel44,
            this.xrLabel实验室,
            this.xrLabel46,
            this.xrLine3});
            this.PageFooter.HeightF = 186.4583F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrLine4
            // 
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(0.04010201F, 116F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(776.9599F, 2F);
            // 
            // xrPictureBox4
            // 
            this.xrPictureBox4.LocationFloat = new DevExpress.Utils.PointFloat(576.5687F, 20.00001F);
            this.xrPictureBox4.Name = "xrPictureBox4";
            this.xrPictureBox4.SizeF = new System.Drawing.SizeF(176.4223F, 92.59853F);
            this.xrPictureBox4.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // imgName4
            // 
            this.imgName4.CanGrow = false;
            this.imgName4.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.imgName4.LocationFloat = new DevExpress.Utils.PointFloat(576.5687F, 2.000014F);
            this.imgName4.Name = "imgName4";
            this.imgName4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.imgName4.SizeF = new System.Drawing.SizeF(145.3406F, 18F);
            this.imgName4.StylePriority.UseFont = false;
            this.imgName4.StylePriority.UseTextAlignment = false;
            this.imgName4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrPictureBox3
            // 
            this.xrPictureBox3.LocationFloat = new DevExpress.Utils.PointFloat(356.7455F, 20.00001F);
            this.xrPictureBox3.Name = "xrPictureBox3";
            this.xrPictureBox3.SizeF = new System.Drawing.SizeF(180.2336F, 92.59853F);
            this.xrPictureBox3.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // imgName3
            // 
            this.imgName3.CanGrow = false;
            this.imgName3.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.imgName3.LocationFloat = new DevExpress.Utils.PointFloat(356.7455F, 2.000014F);
            this.imgName3.Name = "imgName3";
            this.imgName3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.imgName3.SizeF = new System.Drawing.SizeF(143.6565F, 18F);
            this.imgName3.StylePriority.UseFont = false;
            this.imgName3.StylePriority.UseTextAlignment = false;
            this.imgName3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrPictureBox2
            // 
            this.xrPictureBox2.BorderColor = System.Drawing.Color.Gray;
            this.xrPictureBox2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPictureBox2.LocationFloat = new DevExpress.Utils.PointFloat(175.4457F, 20.00001F);
            this.xrPictureBox2.Name = "xrPictureBox2";
            this.xrPictureBox2.SizeF = new System.Drawing.SizeF(150.1638F, 92.59854F);
            this.xrPictureBox2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            this.xrPictureBox2.StylePriority.UseBorderColor = false;
            this.xrPictureBox2.StylePriority.UseBorders = false;
            // 
            // imgName2
            // 
            this.imgName2.CanGrow = false;
            this.imgName2.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.imgName2.LocationFloat = new DevExpress.Utils.PointFloat(175.4457F, 2.000014F);
            this.imgName2.Name = "imgName2";
            this.imgName2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.imgName2.SizeF = new System.Drawing.SizeF(115.0262F, 18F);
            this.imgName2.StylePriority.UseFont = false;
            this.imgName2.StylePriority.UseTextAlignment = false;
            this.imgName2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // imgName1
            // 
            this.imgName1.CanGrow = false;
            this.imgName1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.imgName1.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 2.000014F);
            this.imgName1.Name = "imgName1";
            this.imgName1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.imgName1.SizeF = new System.Drawing.SizeF(112.2193F, 18F);
            this.imgName1.StylePriority.UseFont = false;
            this.imgName1.StylePriority.UseTextAlignment = false;
            this.imgName1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.BorderColor = System.Drawing.Color.Gray;
            this.xrPictureBox1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 20.00001F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(140.5048F, 92.59853F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            this.xrPictureBox1.StylePriority.UseBorderColor = false;
            this.xrPictureBox1.StylePriority.UseBorders = false;
            // 
            // xrPictureBox审核者
            // 
            this.xrPictureBox审核者.LocationFloat = new DevExpress.Utils.PointFloat(322.0415F, 136F);
            this.xrPictureBox审核者.Name = "xrPictureBox审核者";
            this.xrPictureBox审核者.SizeF = new System.Drawing.SizeF(80F, 25F);
            this.xrPictureBox审核者.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // xrPictureBox检验者
            // 
            this.xrPictureBox检验者.LocationFloat = new DevExpress.Utils.PointFloat(86.62491F, 136F);
            this.xrPictureBox检验者.Name = "xrPictureBox检验者";
            this.xrPictureBox检验者.SizeF = new System.Drawing.SizeF(80F, 25F);
            this.xrPictureBox检验者.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // ReportComImgLittle
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.PageHeader,
            this.PageFooter,
            this.TopMargin,
            this.Detail,
            this.BottomMargin});
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRuleCenterLine});
            this.Margins = new System.Drawing.Printing.Margins(25, 25, 0, 0);
            this.PageHeight = 583;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A5Rotated;
            this.ShowPrintMarginsWarning = false;
            this.Version = "13.2";
            this.PrintProgress += new DevExpress.XtraPrinting.PrintProgressEventHandler(this.ReportComImgLittle_PrintProgress);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTitle;
        private DevExpress.XtraReports.UI.XRLabel xrLabel样本号;
        private DevExpress.XtraReports.UI.XRLabel xrLabel床号;
        private DevExpress.XtraReports.UI.XRLabel xrLabel科室;
        private DevExpress.XtraReports.UI.XRLabel xrLabel年龄;
        private DevExpress.XtraReports.UI.XRLabel xrLabel性别;
        private DevExpress.XtraReports.UI.XRLabel xrLabel临床诊断;
        private DevExpress.XtraReports.UI.XRLabel xrLabel病历号;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel条码;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel姓名;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel送检医师;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell序号;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell项目代码;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell项目名称;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell结果;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell标记;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell单位;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell参考值;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRLabel xrLabel实验室;
        private DevExpress.XtraReports.UI.XRLabel xrLabel44;
        private DevExpress.XtraReports.UI.XRLabel xrLabel打印时间;
        private DevExpress.XtraReports.UI.XRLabel xrLabel42;
        private DevExpress.XtraReports.UI.XRLabel xrLabel审核者;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLabel xrLabel联系电话;
        private DevExpress.XtraReports.UI.XRLabel xrLabel报告时间;
        private DevExpress.XtraReports.UI.XRLabel xrLabel医院地址;
        private DevExpress.XtraReports.UI.XRLabel xrLabel检验者;
        private DevExpress.XtraReports.UI.XRLabel xrLabel检验时间;
        private DevExpress.XtraReports.UI.XRLabel xrLabel备注;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.FormattingRule formattingRuleCenterLine;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel imgName1;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox2;
        private DevExpress.XtraReports.UI.XRLabel imgName2;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox3;
        private DevExpress.XtraReports.UI.XRLabel imgName3;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox4;
        private DevExpress.XtraReports.UI.XRLabel imgName4;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel患者类别;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox审核者;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox检验者;
    }
}
