using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.IO;
using ww.form.wwf.print;
using ww.wwf.wwfbll;
using ZedGraph;
using ww.lis.lisbll.sam;
namespace ww.form.lis.sam.qc
{
    public partial class QC_LJForm : ww.form.wwf.SysBaseForm
    {
        private EMFStreamPrintDocument printDoc = null;
        DataTable dtDeptList = new DataTable();

        DataTable dtInstr = new DataTable(); //fjytype_id     
        InstrBLL bllInstr = new InstrBLL();

        ItemBLL bllItem = new ItemBLL();//项目逻辑        
        DataTable dtItem = new DataTable();//当前项目表    

        DataRowView rowCurrItem = null;//当前项目行
        string strCurrItemGuid = "";//当前项目id
        string strCurrItemCode= "";//当前项目代号
        string strCurrItemName = "";//当前项目名称

        DataRowView rowCurrItem样本 = null;
        string str质控品 = "";
        string str起始日期 = "";
        string str批号 = "";
        Double dou靶值 = 0;
        Double douSD = 0;
        public QC_LJForm()
        {
            InitializeComponent();                
        }
        private void ReportViewer_Load(object sender, EventArgs e)
        {
            bllInit();
        }

        #region 方法
        /// <summary>
        /// 系统初始化
        /// </summary>
        private void bllInit()
        {
            try
            {
                DateTime dtime = System.DateTime.Now;
                string strtime = dtime.ToString("yyyy") + "-" + dtime.ToString("MM") + "-" + "01";
                dtime_s.Value = Convert.ToDateTime(strtime);
                zedGraphControl1.Visible = false;
                DataGridViewObject.AutoGenerateColumns = false;
                dataGridView质控样本.AutoGenerateColumns = false;
                GetInstrDT();

                comboBox次数.SelectedItem = "平均值";
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowWarning(ex.Message.ToString());
            }
        }
        private void buttonQuery_Click(object sender, EventArgs e)
        {
            bllReport();
        }
        private void Bll_画图(DateTime dttime_开始日期, DateTime dttime_结束日期, DataTable dt质控数据)
        {
            /*
            double dou靶值 = 10;//靶值
            double douSD值 = 1;//SD值
            Double[] dou项目值 = {9,10};
             
            DateTime[] dtime项目值日期 = { dttime_开始日期,dttime_结束日期 };
            Bll_L_J_Chart(dou项目值, dtime项目值日期, dou靶值, douSD值);
             */

            int int记录数 = dt质控数据.Rows.Count;
            //double dou靶值 = 10;//靶值
            //double douSD值 = 1;//SD值
            Double[] dou项目值 = new Double[int记录数];
            DateTime[] dtime项目值日期 = new DateTime[int记录数];


            for (int i = 0; i < dt质控数据.Rows.Count; i++)
            {

                string str日 = "";
                string str值 = "";
                Double dou值 = 0;
                if (dt质控数据.Rows[i]["日期"] != null)
                {
                    str日 = Convert.ToDateTime(dt质控数据.Rows[i]["日期"].ToString()).ToString("yyyy-MM-dd");
                    str值 = dt质控数据.Rows[i]["值"].ToString();
                    try
                    {
                        dou值 = Convert.ToDouble(str值);
                        dou值 = Math.Round(dou值, 2);
                    }
                    catch { }

                }
                dou项目值[i] = dou值;
                dtime项目值日期[i] = Convert.ToDateTime(str日);

            }
            
            Bll_L_J_Chart(dou项目值, dtime项目值日期, this.dou靶值, this.douSD);
        }

        private void bllReport()
        {
            try
            {

                //条件
                #region 条件
               // string str质控品id = "1";
                string str次数 = comboBox次数.SelectedItem.ToString();
                int int次数 = 1;                
                //this.strCurrItemGuid
                strInstrID = this.comboBoxInstr.SelectedValue.ToString();

                DateTime dttime_开始日期 = dtime_s.Value.Date;
                DateTime dttime_结束日期 = dtime_e.Value.Date;

                if (str次数.Equals("第一次"))
                {
                    int次数 = 1;
                }
                else if (str次数.Equals("第二次"))
                {
                    int次数 = 2;
                }
                else if (str次数.Equals("第三次"))
                {
                    int次数 = 3;
                }
                else if (str次数.Equals("第四次"))
                {
                    int次数 = 4;
                }
                else if (str次数.Equals("第五次"))
                {
                    int次数 = 5;
                }
                else if (str次数.Equals("第六次"))
                {
                    int次数 = 6;
                }
                else if (str次数.Equals("第七次"))
                {
                    int次数 =7;
                }
               
                #endregion


                #region 取数

                string strSql = "";
                if (str次数.Equals("平均值"))
                {
                    strSql = "SELECT  t.fzk_date as 日期,sum(t.fvalue)/(SELECT  count(z.fvalue) as 次数 FROM SAM_ZK_VALUE  z where z.finstr_id='" + strInstrID + "' and z.fitem_id='" + strCurrItemGuid + "' and fzkp_id='" + this.str质控品 + "' and z.fzk_date=t.fzk_date and z.fvalue<>0 group by z.fzk_date) as 值 FROM SAM_ZK_VALUE  t where t.finstr_id='" + strInstrID + "' and t.fitem_id='" + strCurrItemGuid + "' and fzkp_id='" + this.str质控品 + "' and t.fzk_date>='" + dttime_开始日期.ToString("yyyy-MM-dd") + "' and t.fzk_date<='" + dttime_结束日期.ToString("yyyy-MM-dd") + "' and t.fvalue<>0 group by t.fzk_date";
                }
                else
                {
                    strSql = "SELECT t.fzk_date as 日期, t.fvalue  as 值 FROM SAM_ZK_VALUE  t where t.finstr_id='" + strInstrID + "' and t.fitem_id='" + strCurrItemGuid + "' and fzkp_id='" + this.str质控品 + "' and t.fzk_date>='" + dttime_开始日期.ToString("yyyy-MM-dd") + "' and t.fzk_date<='" + dttime_结束日期.ToString("yyyy-MM-dd") + "' and t.fcs=" + int次数 + " and t.fvalue<>0";
                }

                //richTextBox1.Clear();
                //richTextBox1.AppendText(strSql + "\r");            
               
                DataTable dt质控数据 = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql);
                
                #endregion

                //数据
                DataSet dsPrint = new DataSet();
                samDataSet.sam_zkDataTable dtPrintDataTable = new samDataSet.sam_zkDataTable();
                if (dt质控数据.Rows.Count > 0)
                {
                    //画图
                    Bll_画图(dttime_开始日期, dttime_结束日期, dt质控数据);


                    DataRow printRow = dtPrintDataTable.NewRow();
                    printRow["标题"] = LoginBLL.CustomerName + " 质控图";
                    printRow["仪器"] = this.comboBoxInstr.Text.ToString();
                    printRow["项目"] = "[" + this.strCurrItemCode + "]" + this.strCurrItemName;
                    printRow["日期"] = dttime_开始日期.ToString("yyyy-MM-dd") + " 至 " + dttime_结束日期.ToString("yyyy-MM-dd");


                    ImageConverter ic = new ImageConverter();
                    printRow["L_J图"] = (byte[])ic.ConvertTo(zedGraphControl1.GetImage(), typeof(byte[]));
                    dtPrintDataTable.Rows.Add(printRow);

                    dsPrint.Tables.Add(dtPrintDataTable);


                }
                else
                {
                    dtPrintDataTable = new samDataSet.sam_zkDataTable();
                    dsPrint.Tables.Add(dtPrintDataTable);
                    WWMessage.MessageShowWarning("质控数据为空，质控图画制失败。");
                }

                #region 报表设置值
                MainDataSet = dsPrint;//报表数集               
                ReportName = "QCImgReport"; //报表名              
                MainDataSourceName = "samDataSet_sam_zk";  //报表数据源名
                //MainDataSourceName2 = "DataSetHCMS_CMS_REPORT_TYPE1";               
                ReportPath = "ww.form.lis.sam.qc.QCImgReport.rdlc"; //报表文档
                this.AddReportDataSource(this.m_MainDataSet, this.m_MainDataSourceName, m_MainDataSourceName2);
                rptViewer.LocalReport.Refresh();
                rptViewer.RefreshReport();
                #endregion
               
                
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowWarning(ex.ToString());
            }
        }

       

        /// <summary>
        /// L_J质控图
        /// </summary>
        /// <param name="dou项目值"></param>
        /// <param name="dtime项目值日期"></param>
        /// <param name="dou靶值"></param>
        /// <param name="douSD值"></param>
        private void Bll_L_J_Chart(Double[] dou项目值, DateTime[] dtime项目值日期, double dou靶值, double douSD值)
        {
            try
            {
                int cntDays = 1;
                int inumber2 = 1;

                zedGraphControl1.Dock = DockStyle.Fill;
                zedGraphControl1.Visible = true;
                zedGraphControl1.IsShowPointValues = true;
                inumber2 = dou项目值.Length;//4
                if (dtime项目值日期.Length <= 0)
                {
                    return;
                }

                //时间区间 比如2013-08-01 至 2013-08-06 为1----6
                DateTime dtime1;
                DateTime dtime2;
                dtime1 = dtime_s.Value.Date;
                dtime2 = dtime_e.Value.Date;
                double time1 = dtime1.ToOADate();
                double time2 = dtime2.Date.AddDays(1).ToOADate();
                int itime1 = Convert.ToInt32(time1);
                int itime2 = Convert.ToInt32(time2);
                cntDays = itime2 - itime1;

                double[] arrDaysSelected = new double[cntDays];
                arrDaysSelected[0] = time1;
                arrDaysSelected[cntDays - 1] = time1 + cntDays;

                Double[] avg_l = new Double[cntDays];
                Double[] std_l1_1 = new Double[cntDays];
                Double[] std_l1_2 = new Double[cntDays];
                Double[] std_l2_1 = new Double[cntDays];
                Double[] std_l2_2 = new Double[cntDays];
                Double[] std_l3_1 = new Double[cntDays];
                Double[] std_l3_2 = new Double[cntDays];
                Double[] intd = new Double[cntDays];

                #region    绘图相关属性
                GraphPane myPane = zedGraphControl1.GraphPane;
                myPane.CurveList.Clear();
                myPane.GraphObjList.Clear();
                myPane.XAxis.Type = AxisType.Date;      //数据显示方式:按日期方式显示
                myPane.XAxis.Scale.Format = "dd";       //X轴的显示格式         
                myPane.Title.Text = "L_J质控图";
                myPane.Title.FontSpec.Size = 14;
                myPane.Title.IsVisible = false;
                myPane.Border.IsVisible = false;

                //myPane.XAxis.Title.Text = Convert.ToString(dtime1.Date.ToShortDateString() + "至" + dtime2.Date.ToShortDateString());
                myPane.XAxis.Title.IsVisible = false;
                myPane.YAxis.Title.Text = "";
                myPane.Margin.Left = 20;
                myPane.Chart.Fill = new Fill(Color.White);

                #region    相关数据的准备
                PointPairList list = new PointPairList();
                PointPairList lstAvg = new PointPairList();
                PointPairList lstSd1_1 = new PointPairList();
                PointPairList lstSd1_2 = new PointPairList();
                PointPairList lstSd2_1 = new PointPairList();
                PointPairList lstSd2_2 = new PointPairList();
                PointPairList lstSd3_1 = new PointPairList();
                PointPairList lstSd3_2 = new PointPairList();
                for (int i = 0; i < inumber2; i++)
                {
                    double x = (double)new XDate(dtime项目值日期[i].Date);
                    double y = dou项目值[i];
                    list.Add(x, y);
                }
                for (int i = 0; i < cntDays; i++)
                {
                    avg_l[i] = dou靶值;// Sender.ItemProperty.FAvg;
                    std_l1_1[i] = dou靶值 + douSD值;
                    std_l1_2[i] = dou靶值 - douSD值;
                    std_l2_1[i] = dou靶值 + 2 * douSD值;
                    std_l2_2[i] = dou靶值 - 2 * douSD值;
                    std_l3_1[i] = dou靶值 + 3 * douSD值;
                    std_l3_2[i] = dou靶值 - 3 * douSD值;
                }
                lstAvg.Add(arrDaysSelected, avg_l);
                lstSd1_1.Add(arrDaysSelected, std_l1_1);
                lstSd1_2.Add(arrDaysSelected, std_l1_2);
                lstSd2_1.Add(arrDaysSelected, std_l2_1);
                lstSd2_2.Add(arrDaysSelected, std_l2_2);
                lstSd3_1.Add(arrDaysSelected, std_l3_1);
                lstSd3_2.Add(arrDaysSelected, std_l3_2);
                #endregion

                #region    在点旁标注值
                if (chkShowValue.Checked == true)
                {
                    for (int i = 0; i < dou项目值.Length; i++)
                    {
                        double x = (double)new XDate(dtime项目值日期[i].Date);
                        TextObj theText = new TextObj(dou项目值[i].ToString(), x + 0.01, dou项目值[i] + dou项目值[i] * 0.003, CoordType.AxisXY2Scale, AlignH.Left, AlignV.Center);
                        theText.FontSpec.FontColor = Color.Blue;
                        theText.ZOrder = ZOrder.A_InFront;
                        theText.FontSpec.Border.IsVisible = false;
                        theText.FontSpec.Fill.IsVisible = false;
                        theText.FontSpec.Size = 13;
                        myPane.GraphObjList.Add(theText);
                    }
                }
                #endregion

                LineItem myCurve = myPane.AddCurve("", list, Color.Green, SymbolType.Circle);
                myCurve.Line.IsVisible = true;
                myCurve.Symbol.Size = 5;
                myCurve.Symbol.Fill = new Fill(Color.Green);
                myCurve.IsY2Axis = true;
                myCurve.YAxisIndex = 1;

                #region    X轴的相关设置
                myPane.XAxis.Scale.MajorStep = 1;       //X轴的最大刻度单位
                myPane.XAxis.Scale.MinorStep = 1;       //X轴的最小刻度单位
                myPane.XAxis.MajorGrid.IsVisible = true;        //虚线网格是否显示
                myPane.XAxis.MajorGrid.DashOn = 2;
                myPane.XAxis.MajorGrid.DashOff = 2;
                myPane.XAxis.MajorGrid.PenWidth = 1.5f;
                myPane.XAxis.MajorGrid.Color = Color.LightGray;
                myPane.XAxis.Scale.IsSkipLastLabel = true;
                myPane.XAxis.Scale.BaseTic = time1;
                myPane.XAxis.Scale.Max = time2;
                myPane.XAxis.Scale.Min = time1 - 1;
                #endregion

                #region    绘制AVG、SD、2SD、3SD、-SD、-2SD、-3SD线
                LineItem myCurve1 = myPane.AddCurve("", lstAvg, Color.Black, SymbolType.None);
                myCurve1.IsY2Axis = true;
                myCurve1.YAxisIndex = 1;

                LineItem myCurve2 = myPane.AddCurve("", lstSd1_1, Color.DarkGoldenrod, SymbolType.None);
                myCurve2.IsY2Axis = true;
                myCurve2.YAxisIndex = 1;

                LineItem myCurve3 = myPane.AddCurve("", lstSd1_2, Color.DarkGoldenrod, SymbolType.None);
                myCurve3.IsY2Axis = true;
                myCurve3.YAxisIndex = 1;

                LineItem myCurve4 = myPane.AddCurve("", lstSd2_1, Color.Red, SymbolType.None);
                myCurve4.IsY2Axis = true;
                myCurve4.YAxisIndex = 1;

                LineItem myCurve5 = myPane.AddCurve("", lstSd2_2, Color.Red, SymbolType.None);
                myCurve5.IsY2Axis = true;
                myCurve5.YAxisIndex = 1;

                LineItem myCurve6 = myPane.AddCurve("", lstSd3_1, Color.Blue, SymbolType.None);
                myCurve6.IsY2Axis = true;
                myCurve6.YAxisIndex = 1;

                LineItem myCurve7 = myPane.AddCurve("", lstSd3_2, Color.Blue, SymbolType.None);
                myCurve7.IsY2Axis = true;
                myCurve7.YAxisIndex = 1;
                #endregion

                #region    第一个Y轴坐标的相关设置
                myPane.YAxis.Scale.FontSpec.FontColor = Color.Red;
                myPane.YAxis.MajorTic.IsInside = false;
                myPane.YAxis.MajorTic.IsOutside = false;
                myPane.YAxis.MinorTic.IsInside = false;
                myPane.YAxis.MinorTic.IsOutside = false;
                myPane.YAxis.MajorTic.IsOpposite = false;
                myPane.YAxis.MinorTic.IsOpposite = false;
                myPane.YAxis.MajorGrid.IsZeroLine = false;
                myPane.YAxis.MajorGrid.IsVisible = false;        //虚线网格是否显示
                myPane.YAxis.Type = AxisType.Text;

                string[] ylabs = new string[8];
                ylabs[0] = "-3SD";
                ylabs[1] = "-2SD";
                ylabs[2] = "-1SD";
                ylabs[3] = "AVG";
                ylabs[4] = "+1SD";
                ylabs[5] = "+2SD";
                ylabs[6] = "+3SD";
                ylabs[7] = "";

                myPane.YAxis.Scale.Min = 0;
                myPane.YAxis.Scale.Max = 8;
                myPane.YAxis.Scale.TextLabels = ylabs;
                myPane.YAxis.Scale.Align = AlignP.Inside;
                #endregion

                #region    第二个Y轴坐标的相关设置
                myPane.Y2Axis.Scale.FontSpec.FontColor = Color.Blue;
                myPane.Y2Axis.Title.FontSpec.FontColor = Color.Blue;
                myPane.Y2Axis.IsVisible = true;
                myPane.Y2Axis.MajorTic.IsOpposite = false;
                myPane.Y2Axis.MajorGrid.IsZeroLine = false;
                myPane.Y2Axis.Scale.Align = AlignP.Inside;
                myPane.Y2Axis.MajorGrid.IsZeroLine = false;
                myPane.Y2Axis.Scale.Align = AlignP.Inside;
                myPane.Y2Axis.Scale.Min = dou靶值 - 4 * douSD值;
                myPane.Y2Axis.Scale.Max = dou靶值 + 4 * douSD值;
                myPane.Y2Axis.MajorGrid.IsZeroLine = false;
                myPane.Y2Axis.MajorGrid.Color = Color.LightGray;
                #endregion

                zedGraphControl1.AxisChange();
                zedGraphControl1.Refresh();
                #endregion

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowWarning(ex.ToString());
            }
        }





        protected override bool ProcessCmdKey(ref   System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == (Keys.F5))
            {
                buttonQuery.PerformClick();
                return true;
            }
            //else if (keyData == (Keys.F9))
            //{
            //    button审核.PerformClick();
            //    return true;
            //}

            return base.ProcessCmdKey(ref   msg, keyData);
        }

       
        /// <summary>
        /// 已经启用的仪器
        /// </summary>
        private void GetInstrDT()
        {
            try
            {
                if (dtInstr.Rows.Count > 0)
                    dtInstr.Clear();
                //   dtInstr = this.bllItem.BllInstrDT(1, LoginBLL.strDeptID);
                this.dtInstr = this.bllInstr.BllInstrDTByUseAndGroupID(1, ww.wwf.wwfbll.LoginBLL.strDeptID);//仪器列表
                // dtInstr = this.bllItem.BllInstrDT(1, "kfz");
                this.comboBoxInstr.DataSource = dtInstr;

                //20150610 begin  add by wjz 添加本机默认仪器功能
                string strInstrID = ww.form.wwf.Customization.DefaultInstrument.GetInstrID();
                if (!(string.IsNullOrWhiteSpace(strInstrID)))
                {
                    this.comboBoxInstr.SelectedItem = strInstrID;
                }
                //20150610 end    add by wjz 添加本机默认仪器功能
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        /// <summary>
        /// 取得仪器所检项目
        /// </summary>
        /// <param name="strInstrID"></param>
        private void GetItemDT(string strInstrID)
        {
            try
            {
                if (dtItem != null)
                    dtItem.Clear();
                dtItem = this.bllItem.BllItemZK(1, strInstrID);
                this.bindingSource项目.DataSource = dtItem;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 取得仪器质控样本
        /// </summary>
        /// <param name="strInstrID"></param>
        private void GetDTSAM_ZK_SAMPLE()
        {
            try
            {
                string sql = "SELECT fzkp_id as 质控品,fzk_date as 起始日期,fzkp_code as 批号,fbz as 靶值,fsd as SD,* FROM SAM_ZK_SAMPLE WHERE  finstr_id = '" + strInstrID + "' AND (fitem_id = '" + this.strCurrItemGuid + "') order by fzk_date desc";
                DataTable dt质控样本 = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
                this.bindingSource质控样本.DataSource = dt质控样本;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        #endregion


      
        #region 事件      

        /// <summary>
        /// Set the DataSource for the report using a strong-typed dataset
        /// Call it in Form_Load event
        /// </summary>
        private void AddReportDataSource(object ReportSource, string ReportDataSetName1, string ReportDataSetName2)
        {

            rptViewer.LocalReport.DataSources.Clear();//tao2013 新加
            System.Type type = ReportSource.GetType();
            if (type == null)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n   The datasource is of the wrong type!");
                return;
            }
            else
            {
                System.Reflection.PropertyInfo[] picData = type.GetProperties();
                bool bolExist = false;
                foreach (System.Reflection.PropertyInfo piData in picData)
                {
                    if (piData.Name == "Tables")
                    {
                        bolExist = true;
                        if (ReportDataSetName1 == string.Empty)
                        {
                            ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The dataset name for the report does not exist or is empty!");
                            return;
                        }

                        this.rptViewer.LocalReport.DataSources.Add(
                            new Microsoft.Reporting.WinForms.ReportDataSource(ReportDataSetName1,
                            (piData.GetValue(ReportSource, null) as System.Data.DataTableCollection)[0])
                            );

                        this.rptViewer.LocalReport.DataSources.Add(
                          new Microsoft.Reporting.WinForms.ReportDataSource(ReportDataSetName2,
                          (piData.GetValue(ReportSource, null) as System.Data.DataTableCollection)[0])
                          );

                        this.rptViewer.RefreshReport();
                        break;
                    }
                }

                if (!bolExist)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The datasource is of the wrong type!");
                    return;
                }
            }
        }

        private System.Data.DataTableCollection GetTableCollection(object ReportSource)
        {
            System.Type type = ReportSource.GetType();

            if (type == null)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n     The datasource is of the wrong type!");
                return null;
            }
            else
            {
                System.Reflection.PropertyInfo[] picData = type.GetProperties();
                bool bolExist = false;
                foreach (System.Reflection.PropertyInfo piData in picData)
                {
                    if (piData.Name == "Tables")
                    {
                        bolExist = true;
                        return piData.GetValue(ReportSource, null) as System.Data.DataTableCollection;
                    }
                }

                if (!bolExist)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The datasource is of the wrong type!");
                    return null;
                }
            }
            return null;
        }

       
        
        private void rptViewer_Drillthrough(object sender, Microsoft.Reporting.WinForms.DrillthroughEventArgs e)
        {
            if (this.m_DrillDataSet == null)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The dataset name for the report does not exist or is empty!");
                return;
            }
            else
            {
                if (this.m_DrillDataSourceName == string.Empty)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The datasource is of the wrong type!");
                    return;
                }
                else
                {
                    Microsoft.Reporting.WinForms.LocalReport report = e.Report as Microsoft.Reporting.WinForms.LocalReport;
                    report.DataSources.Add(new Microsoft.Reporting.WinForms.ReportDataSource(this.m_DrillDataSourceName, this.GetTableCollection(this.m_DrillDataSet)[0]));
                }
            }
        }


  


        private void printView()
        {
            try
            {
                if (this.printDoc == null)
                {
                    this.printDoc = new EMFStreamPrintDocument(this.rptViewer.LocalReport, this.m_ReportName);
                    if (this.printDoc.ErrorMessage != string.Empty)
                    {
                       
                        this.printDoc = null;
                        ww.wwf.wwfbll.WWMessage.MessageShowWarning("页面设置有误，请重新设置后重试");
                        PageSettings frm = new PageSettings(this.m_ReportName);
                        frm.ShowDialog();
                        frm.Dispose();     
                        return;
                    }
                }

                this.PreviewDialog.Document = this.printDoc;
                this.PreviewDialog.ShowDialog();
               
               // this.PreviewDialog.Dispose();
               // this.printDoc = null;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("页面设置有误，请重新设置后重试");
                PageSettings frm = new PageSettings(this.m_ReportName);
                frm.ShowDialog();
                frm.Dispose();     
            }
        }

        private string GetTimeStamp()
        {
            string strRet = string.Empty;
            System.DateTime dtNow = System.DateTime.Now;
            strRet += dtNow.Year.ToString() +
                        dtNow.Month.ToString("00") +
                        dtNow.Day.ToString("00") +
                        dtNow.Hour.ToString("00") +
                        dtNow.Minute.ToString("00") +
                        dtNow.Second.ToString("00") +
                        System.DateTime.Now.Millisecond.ToString("000");
            return strRet;

        }
        private void ExcelOut()
        {
            try
            {
                Microsoft.Reporting.WinForms.Warning[] Warnings;
                string[] strStreamIds;
                string strMimeType;
                string strEncoding;
                string strFileNameExtension;

                byte[] bytes = this.rptViewer.LocalReport.Render("Excel", null, out strMimeType, out strEncoding, out strFileNameExtension, out strStreamIds, out Warnings);

                string strFilePath = @"C:\" + this.GetTimeStamp() + ".xls";
                //*
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "XLS文件|*.xls|所有文件|*.*";
                saveFileDialog.FilterIndex = 1;
                saveFileDialog.RestoreDirectory = true;
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    strFilePath = saveFileDialog.FileName;
                    using (System.IO.FileStream fs = new FileStream(strFilePath, FileMode.Create))
                    {
                        fs.Write(bytes, 0, bytes.Length);
                    }
                    
                    if (ww.wwf.wwfbll.WWMessage.MessageDialogResult("导出Excel文件成功：" + strFilePath + "\r\n\n 是否打开此文件：" + strFilePath + "?"))
                    {
                        System.Diagnostics.Process.Start(strFilePath);
                    }
                }
                //*                
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        


        

        private void toolLast_Click(object sender, EventArgs e)
        {

        }

        private void tool25_Click(object sender, EventArgs e)
        {
            this.rptViewer.ZoomMode = ZoomMode.Percent;
            this.rptViewer.ZoomPercent = 25;
        }

        private void tool50_Click(object sender, EventArgs e)
        {
            this.rptViewer.ZoomMode = ZoomMode.Percent;
            this.rptViewer.ZoomPercent = 50;
        }

        private void tool100_Click(object sender, EventArgs e)
        {
            this.rptViewer.ZoomMode = ZoomMode.Percent;
            this.rptViewer.ZoomPercent = 100;
        }

        private void tool200_Click(object sender, EventArgs e)
        {
            this.rptViewer.ZoomMode = ZoomMode.Percent;
            this.rptViewer.ZoomPercent = 200;
        }

        private void tool400_Click(object sender, EventArgs e)
        {
            this.rptViewer.ZoomMode = ZoomMode.Percent;
            this.rptViewer.ZoomPercent = 400;
        }

        private void toolWhole_Click(object sender, EventArgs e)
        {
            this.rptViewer.ZoomMode = ZoomMode.FullPage;
        }

        private void toolPageWidth_Click(object sender, EventArgs e)
        {
            this.rptViewer.ZoomMode = ZoomMode.PageWidth;
        }


        

        private void toolPrevious_Click(object sender, EventArgs e)
        {

        }

        private void toolNext_Click(object sender, EventArgs e)
        {

        }
 

        private void 导出PDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Microsoft.Reporting.WinForms.Warning[] Warnings;
                string[] strStreamIds;
                string strMimeType;
                string strEncoding;
                string strFileNameExtension;

                byte[] bytes = this.rptViewer.LocalReport.Render("PDF", null, out strMimeType, out strEncoding, out strFileNameExtension, out strStreamIds, out Warnings);

                string strFilePath = @"C:\" + this.GetTimeStamp() + ".PDF";
                //*
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "PDF文件|*.pdf|所有文件|*.*";
                saveFileDialog.FilterIndex = 1;
                saveFileDialog.RestoreDirectory = true;
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    strFilePath = saveFileDialog.FileName;
                    using (System.IO.FileStream fs = new FileStream(strFilePath, FileMode.Create))
                    {
                        fs.Write(bytes, 0, bytes.Length);
                    }

                    if (ww.wwf.wwfbll.WWMessage.MessageDialogResult("导出PDF文件成功：" + strFilePath + "\r\n\n 是否打开此文件：" + strFilePath + "?"))
                    {
                        System.Diagnostics.Process.Start(strFilePath);
                    }
                }
                //*
                
            }
            catch (Exception ex)
            {
                 ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

       

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            try
            {
                PageSettings frm = new PageSettings(this.m_ReportName);
                frm.ShowDialog();
                frm.Dispose();
            }
            catch (Exception ex)
            {
                 ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.printDoc == null)
                {
                    this.printDoc = new EMFStreamPrintDocument(this.rptViewer.LocalReport, this.m_ReportName);
                    if (this.printDoc.ErrorMessage != string.Empty)
                    {
                        ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewer: \r\n    " + this.printDoc.ErrorMessage);
                        this.printDoc = null;
                        return;
                    }
                }

                this.printDoc.Print();
                this.printDoc = null;
            }
            catch (Exception ex)
            {
                 ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 刷新ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.rptViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                 ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void ssToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.rptViewer.CancelRendering(0);
        }

        private void 回退ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (this.rptViewer.LocalReport.IsDrillthroughReport)
                this.rptViewer.PerformBack();
        }

         

        
        private void toolExcel_Click(object sender, EventArgs e)
        {
            ExcelOut();
        }

       
   
        private void toolFirst_Click(object sender, EventArgs e)
        {

        }

        private void toolJump_Click(object sender, EventArgs e)
        {

        }

        private void toolFirst_Click_1(object sender, EventArgs e)
        {

        }

        private void toolStripButtonHelp_Click(object sender, EventArgs e)
        {
            try
            {
                HelpForm help = new HelpForm("herp-qcb-report-02");
                help.Show();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolFirst_Click_2(object sender, EventArgs e)
        {

        }

        private void toolLast_Click_1(object sender, EventArgs e)
        {

        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            try
            {
                printView();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());

            }
        }

#endregion
        
        string strInstrID = "";//仪器ID
        private void comboBoxInstr_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                strInstrID = this.comboBoxInstr.SelectedValue.ToString();
                GetItemDT(strInstrID);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void bll取得当前项目id()
        {
            try
            {
                rowCurrItem = (DataRowView)bindingSource项目.Current;//行
                if (rowCurrItem != null)
                {
                    strCurrItemGuid = rowCurrItem["fitem_id"].ToString();
                    strCurrItemName = rowCurrItem["fname"].ToString();
                    strCurrItemCode = rowCurrItem["fitem_code"].ToString();
                }
                else
                {
                    strCurrItemGuid = "";
                    strCurrItemName = "";
                    strCurrItemCode = "";
                }
                GetDTSAM_ZK_SAMPLE();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void bll取得样本id()
        {
            try
            {
                rowCurrItem样本 = (DataRowView)bindingSource质控样本.Current;//行
                if (rowCurrItem样本 != null)
                {
                    str质控品 = rowCurrItem样本["质控品"].ToString();
                    str起始日期 = rowCurrItem样本["起始日期"].ToString();
                    str批号 = rowCurrItem样本["批号"].ToString();
                    dou靶值 = Convert.ToDouble(rowCurrItem样本["靶值"].ToString());
                    douSD = Convert.ToDouble(rowCurrItem样本["SD"].ToString());

                    dtime_s.Value = Convert.ToDateTime(str起始日期);
                }
                else
                {
                    str质控品 = "";
                    str起始日期 = "";
                    str批号 = "";
                    dou靶值 = 0;
                    douSD = 0;
                }

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void bindingSource项目_PositionChanged(object sender, EventArgs e)
        {
            bll取得当前项目id();
        }

        private void DataGridViewObject_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            bll取得当前项目id();
        }

        private void bindingSource质控样本_PositionChanged(object sender, EventArgs e)
        {
            bll取得样本id();
        }

        private void dataGridView质控样本_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            bll取得样本id();
        }

        

       
       
    }
}

/*
 101	ICU
102	病理科
103	超声科
104	儿科
105	耳鼻喉科
106	放射科
107	妇产科
108	干部保健科
109	高压氧室
110	功能科
111	供应室
112	购药
113	核医学科
114	急诊科
115	检验科
116	口腔1诊室
117	口腔2诊室
118	口腔3诊室
119	口腔颌面外科
120	口腔科
121	临床科室
122	麻醉科
123	美容科
124	门诊办公室
125	门诊专家1
126	门诊专家2
127	门诊专家3
128	门诊专家4
129	男科
130	脑电图室
131	内二科
132	内二科（2）
133	内六科
134	内七科
135	内三科
136	内四科
137	内五科
138	内一科
139	皮肤科
140	烧伤整形科
141	生殖中心
142	输血科
143	体检中心
144	外八科
145	外二科
146	外六科
147	外七科
148	外三科
149	外四科
150	外五科
151	外一科
152	胃镜室
153	心电图室
154	血透中心
155	眼科
156	药剂科
157	医技科室
158	远程会诊中心
159	中医康复科
160	中医科

 */


/*
   if (radioButtonMonth.Checked)
            {
                //月报   
                strSql = "SELECT  FYEAR, FMONTH, FDEPT_ID, FDEPT_TYPE_ID, fitem1_qcb,convert(numeric(18,2),(CASE fitem8_sum  WHEN '0' THEN '0' ELSE fitem1_qcb/fitem8_sum END))*100 as fitem1_qcb_bl,fitem2_qcb,convert(numeric(18,2),(CASE fitem8_sum  WHEN '0' THEN '0' ELSE fitem2_qcb/fitem8_sum END))*100 as fitem2_qcb_bl,fitem3_qcb,convert(numeric(18,2),(CASE fitem8_sum  WHEN '0' THEN '0' ELSE fitem3_qcb/fitem8_sum END))*100 as fitem3_qcb_bl,fitem4_qcb,convert(numeric(18,2),(CASE fitem8_sum  WHEN '0' THEN '0' ELSE fitem4_qcb/fitem8_sum END))*100 as fitem4_qcb_bl,fitem5_qcb,convert(numeric(18,2),(CASE fitem8_sum  WHEN '0' THEN '0' ELSE fitem5_qcb/fitem8_sum END))*100 as fitem5_qcb_bl,fitem6_qcb,convert(numeric(18,2),(CASE fitem8_sum  WHEN '0' THEN '0' ELSE fitem6_qcb/fitem8_sum END))*100 as fitem6_qcb_bl, fitem7_qcb,convert(numeric(18,2),(CASE fitem8_sum  WHEN '0' THEN '0' ELSE fitem7_qcb/fitem8_sum END))*100 as fitem7_qcb_bl, fitem8_sum,fitem9_kssr,fitem10_srjcb, fitem11_crcb, fitem12_zccb,0 as fitem13_all_dept_sum,0 as fitem13_all_dept_sum_bl FROM  v_cms_data_item WHERE  (FDEPT_TYPE_ID = '1') AND (FYEAR = '" + this.comboBoxDateS.SelectedItem.ToString() + "') AND (FMONTH = '" + this.comboBoxDateE.SelectedItem.ToString() + "')";
                         
            }
            else if (this.radioButtonYear.Checked)
            {
                //年报
                strSql = "SELECT FYEAR, FMONTH, FDEPT_ID,sum(fitem1_qcb) as fitem1_qcb,convert(numeric(18,2),(CASE sum(fitem8_sum)  WHEN '0' THEN '0' ELSE (sum(fitem1_qcb)/sum(fitem8_sum)) END))*100 as fitem1_qcb_bl,sum(fitem2_qcb) as fitem2_qcb,convert(numeric(18,2),(CASE sum(fitem8_sum)  WHEN '0' THEN '0' ELSE (sum(fitem2_qcb)/sum(fitem8_sum)) END))*100 as fitem2_qcb_bl,sum(fitem3_qcb) as fitem3_qcb,convert(numeric(18,2),(CASE sum(fitem8_sum)  WHEN '0' THEN '0' ELSE (sum(fitem3_qcb)/sum(fitem8_sum)) END))*100 as fitem3_qcb_bl,sum(fitem4_qcb) as fitem4_qcb, convert(numeric(18,2),(CASE sum(fitem8_sum)  WHEN '0' THEN '0' ELSE (sum(fitem4_qcb)/sum(fitem8_sum)) END))*100 as fitem4_qcb_bl,sum(fitem5_qcb) as fitem5_qcb,convert(numeric(18,2),(CASE sum(fitem8_sum)  WHEN '0' THEN '0' ELSE (sum(fitem5_qcb)/sum(fitem8_sum)) END))*100 as fitem5_qcb_bl,sum(fitem6_qcb) as fitem6_qcb,convert(numeric(18,2),(CASE sum(fitem8_sum)  WHEN '0' THEN '0' ELSE (sum(fitem6_qcb)/sum(fitem8_sum)) END))*100 as fitem6_qcb_bl, sum(fitem7_qcb) as fitem7_qcb,convert(numeric(18,2),(CASE sum(fitem8_sum)  WHEN '0' THEN '0' ELSE (sum(fitem7_qcb)/sum(fitem8_sum)) END))*100 as fitem7_qcb_bl, sum(fitem8_sum) as fitem8_sum,sum(fitem9_kssr) as fitem9_kssr,sum(fitem10_srjcb) as fitem10_srjcb, sum(fitem11_crcb) as fitem11_crcb,sum(fitem12_zccb) as fitem12_zccb,0 as fitem13_all_dept_sum,0 as fitem13_all_dept_sum_bl FROM  v_cms_data_item WHERE  (FDEPT_TYPE_ID = '1')  AND (FYEAR = '" + this.comboBoxDateS.SelectedItem.ToString() + "') AND (FMONTH = '" + this.comboBoxDateE.SelectedItem.ToString() + "') group by FYEAR, FMONTH, FDEPT_ID";
            }
 */