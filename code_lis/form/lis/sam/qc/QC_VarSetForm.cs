﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;
using ww.lis.lisbll.sam;
using System.Collections;
namespace ww.form.lis.sam.qc
{
    public partial class QC_VarSetForm : ww.form.wwf.SysBaseForm
    {
        ItemBLL bllItem = new ItemBLL();//项目逻辑        
        DataTable dtItem = new DataTable();//当前项目表    
        DataRowView rowCurrItem = null;//当前项目行
        string strCurrItemGuid = "";
        string strInstrID = "";//仪器ID
        DataTable dtInstr = new DataTable(); //fjytype_id     
        InstrBLL bllInstr = new InstrBLL();

        DataTable dt参数 = new DataTable();
        string str日期 = "";
        int int质控品 = 0;
        public QC_VarSetForm()
        {
            InitializeComponent();
        }

        private void ItemSetForm_Load(object sender, EventArgs e)
        {
            comboBox质控品.SelectedItem = "1";
            GetInstrDT();
        }


        private void GetItemDT()
        {
            try
            {
                if (dtItem != null)
                    dtItem.Clear();
                dtItem = this.bllItem.BllItemZK(1, strInstrID);
                this.bindingSource项目.DataSource = dtItem;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        /// <summary>
        /// 参数
        /// </summary>
        private void GetVarDT()
        {
            try
            {
                string sql = "SELECT  *  FROM  SAM_ZK_SAMPLE where finstr_id='" + this.strInstrID + "' and fitem_id='" + strCurrItemGuid + "' order by fzk_date  ";
                dt参数 = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];

                this.bindingSource参数.DataSource = dt参数;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }


        /// <summary>
        /// 已经启用的仪器
        /// </summary>
        private void GetInstrDT()
        {
            try
            {
                if (dtInstr.Rows.Count > 0)
                    dtInstr.Clear();
                //   dtInstr = this.bllItem.BllInstrDT(1, LoginBLL.strDeptID);
                this.dtInstr = this.bllInstr.BllInstrDTByUseAndGroupID(1, ww.wwf.wwfbll.LoginBLL.strDeptID);//仪器列表
                // dtInstr = this.bllItem.BllInstrDT(1, "kfz");
                this.comboBoxInstr.DataSource = dtInstr;

                //20150610 begin  add by wjz 添加本机默认仪器功能
                string strInstrID = ww.form.wwf.Customization.DefaultInstrument.GetInstrID();
                if (!(string.IsNullOrWhiteSpace(strInstrID)))
                {
                    this.comboBoxInstr.SelectedItem = strInstrID;
                }
                //20150610 end    add by wjz 添加本机默认仪器功能
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }


        private void comboBoxInstr_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                strInstrID = this.comboBoxInstr.SelectedValue.ToString();
                GetItemDT();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }



        private void toolStripButtonS_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                this.Validate();
                this.bindingSource参数.EndEdit();
                this.DataGridViewObject.EndEdit();
                IList lisSql = new ArrayList();
                for (int i = 0; i < this.dt参数.Rows.Count; i++)
                {
                    StringBuilder strSql = new StringBuilder();
                    strSql.Append("update SAM_ZK_SAMPLE set ");
                    strSql.Append("fzkp_code='" + this.dt参数.Rows[i]["fzkp_code"].ToString() + "',");
                    strSql.Append("fld='" + this.dt参数.Rows[i]["fld"].ToString() + "',");
                    strSql.Append("fio_code='" + this.dt参数.Rows[i]["fio_code"].ToString() + "',");
                    if (this.dt参数.Rows[i]["fcv"] != null)
                        strSql.Append("fcv=" + this.dt参数.Rows[i]["fcv"] + ",");
                    if (this.dt参数.Rows[i]["fsd"] != null)
                        strSql.Append("fsd=" + this.dt参数.Rows[i]["fsd"] + ",");
                    if (this.dt参数.Rows[i]["fbz"] != null)
                        strSql.Append("fbz=" + this.dt参数.Rows[i]["fbz"] + ",");
                    if (this.dt参数.Rows[i]["fbc"] != null)
                        strSql.Append("fbc='" + this.dt参数.Rows[i]["fbc"] + "'");
                    strSql.Append(" where finstr_id='" + this.dt参数.Rows[i]["finstr_id"].ToString() + "' and fitem_id='" + this.dt参数.Rows[i]["fitem_id"].ToString() + "' and fzkp_id='" + this.dt参数.Rows[i]["fzkp_id"].ToString() + "' and fzk_date='" + this.dt参数.Rows[i]["fzk_date"].ToString() + "' ");

                    lisSql.Add(strSql.ToString());
                    
                }
                if (lisSql.Count > 0)
                {
                    string strRRR = WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, lisSql);

                    if (strRRR.Equals("true"))
                    {
                        WWMessage.MessageShowWarning("记录保存成功！");
                    }
                    else
                    {
                        WWMessage.MessageShowWarning(strRRR);
                    }

                    GetItemDT();
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void bll取得当前项目id()
        {
            try
            {
                rowCurrItem = (DataRowView)bindingSource项目.Current;//行
                if (rowCurrItem != null)
                {
                    strCurrItemGuid = rowCurrItem["fitem_id"].ToString();

                }
                else
                {
                    strCurrItemGuid = "";

                }
                this.GetVarDT();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void bindingSource项目_PositionChanged(object sender, EventArgs e)
        {
            bll取得当前项目id();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            bll取得当前项目id();
        }

        private void toolStripButtonR_Click(object sender, EventArgs e)
        {
            this.GetVarDT();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("insert into SAM_ZK_SAMPLE(");
                strSql.Append("finstr_id,fitem_id,fzkp_id,fzk_date,fzkp_code,fld,fcv,fsd,fbz,fbc,fff,fsj_id,f1,f2,f3,f4,f5");
                strSql.Append(")");
                strSql.Append(" values (");
                strSql.Append("'" + this.strInstrID + "',");
                strSql.Append("'" + this.strCurrItemGuid + "',");
                strSql.Append("'" + comboBox质控品.SelectedItem.ToString() + "',");//fzk_date
                strSql.Append("'" + dtime_s.Value.ToString("yyyy-MM-dd") + "',");//fzk_date
                strSql.Append("'',");//fzkp_code
                strSql.Append("'高',");//fld
                strSql.Append("1,");//fcv
                strSql.Append("1,");//fsd
                strSql.Append("0,");//fbz
                strSql.Append("'',");//fbc
                strSql.Append("'',");//fff
                strSql.Append("'',");//fsj_id
                strSql.Append("'',");
                strSql.Append("'',");
                strSql.Append("'',");
                strSql.Append("'',");
                strSql.Append("''");
                strSql.Append(")");
                WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, strSql.ToString());
                this.GetVarDT();
            }
            catch (Exception)
            {
                WWMessage.MessageShowWarning("当前日期参数已经存在，请重新选择日期或质控品后重试。");
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            try
            {
                if (WWMessage.MessageDialogResult("确认要删除当前记录？"))
                {
                    StringBuilder strSql = new StringBuilder();
                    strSql.Append("delete SAM_ZK_SAMPLE ");
                    strSql.Append(" where finstr_id='" + this.strInstrID + "' and fitem_id='" + this.strCurrItemGuid + "' and fzkp_id='" + this.strVar_质控品 + "' and fzk_date='" + this.strVar_日期 + "' ");
                    WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, strSql.ToString());
                    this.GetVarDT();
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowWarning(ex.ToString());
            }
        }
        string strVar_日期 = "";
        string strVar_质控品 = "";
        private void bll取得当前参数id()
        {
            try
            {
                DataRowView rowCurrItem参数 = (DataRowView)bindingSource参数.Current;//行
                if (rowCurrItem参数 != null)
                {
                    strVar_日期 = rowCurrItem参数["fzk_date"].ToString();
                    strVar_质控品 = rowCurrItem参数["fzkp_id"].ToString();

                }
                else
                {
                    strVar_日期 = "";
                    strVar_质控品 = "";

                }
              
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void bindingSource参数_PositionChanged(object sender, EventArgs e)
        {
            bll取得当前参数id();
        }

        private void DataGridViewObject_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            bll取得当前参数id();
        }

        private void DataGridViewObject_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

    }
}