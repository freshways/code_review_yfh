﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;
using ww.lis.lisbll.sam;
using System.Collections;
namespace ww.form.lis.sam.qc
{
    public partial class QC_DataForm : ww.form.wwf.SysBaseForm
    {
        ItemBLL bllItem = new ItemBLL();//项目逻辑        
        DataTable dtItem = new DataTable();//当前项目表    
        DataRowView rowCurrItem = null;//当前项目行
        string strCurrItemGuid = "";
        string strInstrID = "";//仪器ID
        DataTable dtInstr = new DataTable(); //fjytype_id     
        InstrBLL bllInstr = new InstrBLL();


        string str日期 = "";
        int int质控品 = 0;
        public QC_DataForm()
        {
            InitializeComponent();
        }

        private void ItemSetForm_Load(object sender, EventArgs e)
        {
            comboBox质控品.SelectedItem = "1号质控品";
            GetInstrDT();
        }


      
        /// <summary>
        /// 取得仪器所检项目
        /// </summary>
        /// <param name="strInstrID"></param>
        private void GetItemDT(string strInstrID)
        {
            try
            {
                if (dtItem != null)
                    dtItem.Clear();

                string str质控品 = comboBox质控品.SelectedItem.ToString();
                  str日期 = dtime_s.Value.ToString("yyyy-MM-dd");
                  int质控品 = 0;
                if (str质控品.Equals("1号质控品"))
                {
                    int质控品 = 1;
                }
                else if (str质控品.Equals("2号质控品"))
                {
                    int质控品 = 2;
                }
                else if (str质控品.Equals("3号质控品"))
                {
                    int质控品 = 3;
                }
                else if (str质控品.Equals("4号质控品"))
                {
                    int质控品 = 4;
                }
                else if (str质控品.Equals("5号质控品"))
                {
                    int质控品 = 5;
                }
                else if (str质控品.Equals("6号质控品"))
                {
                    int质控品 = 6;
                }
                else if (str质控品.Equals("7号质控品"))
                {
                    int质控品 = 7;
                }
                else if (str质控品.Equals("8号质控品"))
                {
                    int质控品 = 8;
                }
                else if (str质控品.Equals("9号质控品"))
                {
                    int质控品 = 9;
                }
                else if (str质控品.Equals("10号质控品"))
                {
                    int质控品 = 10;
                }

                string sql = "SELECT t.fitem_code,t.fname,t.fitem_id,(SELECT  z.fvalue FROM  SAM_ZK_VALUE z where z.finstr_id=t.finstr_id and z.fitem_id=t.fitem_id and z.fzk_date='" + str日期 + "' and z.fzkp_id='" + int质控品 + "'  and z.fcs=1) as 次数1,(SELECT  z.fvalue FROM  SAM_ZK_VALUE z where z.finstr_id=t.finstr_id and z.fitem_id=t.fitem_id and z.fzk_date='" + str日期 + "' and z.fzkp_id='" + int质控品 + "'  and z.fcs=2) as 次数2,(SELECT  z.fvalue FROM  SAM_ZK_VALUE z where z.finstr_id=t.finstr_id and z.fitem_id=t.fitem_id and z.fzk_date='" + str日期 + "' and z.fzkp_id='" + int质控品 + "'  and z.fcs=3) as 次数3,(SELECT  z.fvalue FROM  SAM_ZK_VALUE z where z.finstr_id=t.finstr_id and z.fitem_id=t.fitem_id and z.fzk_date='" + str日期 + "' and z.fzkp_id='" + int质控品 + "'  and z.fcs=4) as 次数4,(SELECT  z.fvalue FROM  SAM_ZK_VALUE z where z.finstr_id=t.finstr_id and z.fitem_id=t.fitem_id and z.fzk_date='" + str日期 + "' and z.fzkp_id='" + int质控品 + "'  and z.fcs=5) as 次数5,(SELECT  z.fvalue FROM  SAM_ZK_VALUE z where z.finstr_id=t.finstr_id and z.fitem_id=t.fitem_id and z.fzk_date='" + str日期 + "' and z.fzkp_id='" + int质控品 + "'  and z.fcs=6) as 次数6,(SELECT  z.fvalue FROM  SAM_ZK_VALUE z where z.finstr_id=t.finstr_id and z.fitem_id=t.fitem_id and z.fzk_date='" + str日期 + "' and z.fzkp_id='" + int质控品 + "'  and z.fcs=7) as 次数7 FROM sam_item t where t.fzk_if=1 and t.finstr_id='" + strInstrID + "' order by t.fprint_num";
                //this.richTextBox1.Clear();
                //richTextBox1.AppendText(sql);
                dtItem = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];

                this.sam_itemBindingSource.DataSource = dtItem;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        /// <summary>
        /// 已经启用的仪器
        /// </summary>
        private void GetInstrDT()
        {
            try
            {
                if (dtInstr.Rows.Count > 0)
                    dtInstr.Clear();
             //   dtInstr = this.bllItem.BllInstrDT(1, LoginBLL.strDeptID);
                this.dtInstr = this.bllInstr.BllInstrDTByUseAndGroupID(1, ww.wwf.wwfbll.LoginBLL.strDeptID);//仪器列表
                // dtInstr = this.bllItem.BllInstrDT(1, "kfz");
                this.comboBoxInstr.DataSource = dtInstr;

                //20150610 begin  add by wjz 添加本机默认仪器功能
                string strInstrID = ww.form.wwf.Customization.DefaultInstrument.GetInstrID();
                if (!(string.IsNullOrWhiteSpace(strInstrID)))
                {
                    this.comboBoxInstr.SelectedItem = strInstrID;
                }
                //20150610 end    add by wjz 添加本机默认仪器功能
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        

        private void comboBoxInstr_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                strInstrID = this.comboBoxInstr.SelectedValue.ToString();
                GetItemDT(strInstrID);              
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private IList bll设置Add(IList lisSql, int int次数)
        {
            for (int i = 0; i < this.dtItem.Rows.Count; i++)
            {
                double dou值 = 0;
                if (this.dtItem.Rows[i]["次数" + int次数.ToString()] != null)
                {
                    try
                    {
                        dou值 = Convert.ToDouble(this.dtItem.Rows[i]["次数" + int次数.ToString()].ToString());
                    }
                    catch { }
                }
               
                StringBuilder strSql = new StringBuilder();
                strSql.Append("insert into SAM_ZK_VALUE(");
                strSql.Append("finstr_id,fitem_id,fzkp_id,fzk_date,fcs,fvalue,fbz,f1,f2,f3,f4,f5");
                strSql.Append(")");
                strSql.Append(" values (");
                strSql.Append("'" + this.strInstrID + "',");
                strSql.Append("'" + this.dtItem.Rows[i]["fitem_id"].ToString() + "',");
                strSql.Append("'" + this.int质控品 + "',");
                strSql.Append("'" + this.str日期 + "',");
                strSql.Append("" + int次数 + ",");//fcs
                strSql.Append("" + dou值 + ",");//fvalue
                strSql.Append("'',");//fbz
                strSql.Append("'',");//f1
                strSql.Append("'',");
                strSql.Append("'',");
                strSql.Append("'',");
                strSql.Append("''");
                strSql.Append(")");
                lisSql.Add(strSql.ToString());

               
            }
            return lisSql;
        }

        private void toolStripButtonS_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                this.Validate();
                this.sam_itemBindingSource.EndEdit();

               
                IList lisSql = new ArrayList();
                
                string strDelSql = "DELETE FROM SAM_ZK_VALUE WHERE finstr_id='" + strInstrID + "' and fzkp_id='" + int质控品 + "' and fzk_date='" + str日期 + "'";
                
                lisSql.Add(strDelSql);
                for (int i = 1; i < 8; i++)
                {
                    bll设置Add(lisSql, i);
                }
                if (lisSql.Count > 0)
                {
                    string strRRR = WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, lisSql);
                    if (strRRR.Equals("true"))
                    {
                        WWMessage.MessageShowWarning("记录保存成功！");
                    }
                    
                    GetItemDT(strInstrID);
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void bll取项目值()
        {
            try
            {
                rowCurrItem = (DataRowView)sam_itemBindingSource.Current;//行
                if (rowCurrItem != null)
                {
                    strCurrItemGuid = rowCurrItem["fitem_id"].ToString();
                }
                else
                {
                    strCurrItemGuid = "";
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void sam_itemBindingSource_PositionChanged(object sender, EventArgs e)
        {
            bll取项目值();
        }

        private void toolStripButtonR_Click(object sender, EventArgs e)
        {
            GetItemDT(strInstrID);          
        }

        private void comboBox质控品_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetItemDT(strInstrID);          
        }

        

        private void dtime_s_ValueChanged(object sender, EventArgs e)
        {
            GetItemDT(strInstrID);   
        }

        private void DataGridViewObject_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            bll取项目值();
        }

        private void DataGridViewObject_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        

    }
}