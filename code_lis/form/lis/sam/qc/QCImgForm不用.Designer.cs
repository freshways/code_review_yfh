namespace ww.form.lis.sam.qc
{
    partial class QCImgForm
    {
        #region  用户定义
        /// <summary>
        /// 报表路径
        /// </summary>
        public string ReportPath
        {
            /*
            get
            {
                return this.rptViewer.LocalReport.ReportPath;
            }
            set
            {
                this.rptViewer.LocalReport.ReportPath = value;
            }*/
            get
            {
                return this.rptViewer.LocalReport.ReportEmbeddedResource;
            }
            set
            {
                this.rptViewer.LocalReport.ReportEmbeddedResource = value;
            }
        }

        private string m_ReportName = string.Empty;
        /// <summary>
        /// 报表名称
        /// </summary>
        public string ReportName
        {
            get
            {
                return this.m_ReportName;
            }
            set
            {
                this.m_ReportName = value;
            }
        }

        /// <summary>
        /// DataSource of the Main Report
        /// </summary>
        private object m_MainDataSet = null;
        /// <summary>
        /// 报表数据集  主
        /// </summary>
        public object MainDataSet
        {
            get
            {
                return this.m_MainDataSet;
            }
            set
            {
                this.m_MainDataSet = value;
            }
        }

        /// <summary>
        /// DataSource of the DrillThrough Report
        /// </summary>
        private object m_DrillDataSet = null;
        /// <summary>
        /// 报表数据集  子
        /// </summary>
        public object DrillDataSet
        {
            get
            {
                return this.m_DrillDataSet;
            }
            set
            {
                this.m_DrillDataSet = value;
            }
        }
        /// <summary>
        /// Data Source Name of the Main Report
        /// </summary>
        private string m_MainDataSourceName = string.Empty;
        /// <summary>
        /// 主数据源名
        /// </summary>
        public string MainDataSourceName
        {
            get
            {
                return this.m_MainDataSourceName;
            }
            set
            {
                this.m_MainDataSourceName = value;
            }
        }

        /// <summary>
        /// Data Source Name of the DrillThrough Report
        /// </summary>
        private string m_DrillDataSourceName = string.Empty;
        /// <summary>
        /// 子数据源名
        /// </summary>
        public string DrillDataSourceName
        {
            get
            {
                return this.m_DrillDataSourceName;
            }
            set
            {
                this.m_DrillDataSourceName = value;
            }
        }



        /// <summary>
        /// Data Source Name of the Main Report
        /// </summary>
        private string m_MainDataSourceName2 = string.Empty;
        public string MainDataSourceName2
        {
            get
            {
                return this.m_MainDataSourceName2;
            }
            set
            {
                this.m_MainDataSourceName2 = value;
            }
        }
        #endregion 
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QCImgForm));
            this.buttonQuery = new System.Windows.Forms.Button();
            this.rptViewer = new Microsoft.Reporting.WinForms.ReportViewer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.chkShowValue = new System.Windows.Forms.CheckBox();
            this.dtime_e = new System.Windows.Forms.DateTimePicker();
            this.lblNote2 = new System.Windows.Forms.Label();
            this.dtime_s = new System.Windows.Forms.DateTimePicker();
            this.lblNote1 = new System.Windows.Forms.Label();
            this.dnReport = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.tspbZoom = new System.Windows.Forms.ToolStripSplitButton();
            this.tool25 = new System.Windows.Forms.ToolStripMenuItem();
            this.tool50 = new System.Windows.Forms.ToolStripMenuItem();
            this.tool100 = new System.Windows.Forms.ToolStripMenuItem();
            this.tool200 = new System.Windows.Forms.ToolStripMenuItem();
            this.tool400 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolWhole = new System.Windows.Forms.ToolStripMenuItem();
            this.toolPageWidth = new System.Windows.Forms.ToolStripMenuItem();
            this.toolExport = new System.Windows.Forms.ToolStripSplitButton();
            this.toolExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.导出PDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonHelp = new System.Windows.Forms.ToolStripButton();
            this.panel_图 = new System.Windows.Forms.Panel();
            this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this.PreviewDialog = new System.Windows.Forms.PrintPreviewDialog();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dnReport)).BeginInit();
            this.dnReport.SuspendLayout();
            this.panel_图.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonQuery
            // 
            this.buttonQuery.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonQuery.Location = new System.Drawing.Point(592, 9);
            this.buttonQuery.Name = "buttonQuery";
            this.buttonQuery.Size = new System.Drawing.Size(97, 27);
            this.buttonQuery.TabIndex = 17;
            this.buttonQuery.Text = "画图(F5)";
            this.buttonQuery.UseVisualStyleBackColor = true;
            this.buttonQuery.Click += new System.EventHandler(this.buttonQuery_Click);
            // 
            // rptViewer
            // 
            this.rptViewer.AutoScroll = true;
            this.rptViewer.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.rptViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptViewer.DocumentMapCollapsed = true;
            this.rptViewer.IsDocumentMapWidthFixed = true;
            this.rptViewer.LocalReport.ReportEmbeddedResource = "RDLCPrint.rptWuLiao.rdlc";
            this.rptViewer.Location = new System.Drawing.Point(0, 81);
            this.rptViewer.Name = "rptViewer";
            this.rptViewer.ShowToolBar = false;
            this.rptViewer.Size = new System.Drawing.Size(795, 377);
            this.rptViewer.TabIndex = 1;
            this.rptViewer.Drillthrough += new Microsoft.Reporting.WinForms.DrillthroughEventHandler(this.rptViewer_Drillthrough);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.checkBox1);
            this.panel1.Controls.Add(this.chkShowValue);
            this.panel1.Controls.Add(this.dtime_e);
            this.panel1.Controls.Add(this.lblNote2);
            this.panel1.Controls.Add(this.dtime_s);
            this.panel1.Controls.Add(this.lblNote1);
            this.panel1.Controls.Add(this.buttonQuery);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 35);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(795, 46);
            this.panel1.TabIndex = 160;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.BackColor = System.Drawing.SystemColors.Control;
            this.checkBox1.Location = new System.Drawing.Point(105, 13);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(152, 18);
            this.checkBox1.TabIndex = 160;
            this.checkBox1.Text = "只显示每天最后一次";
            this.checkBox1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox1.UseVisualStyleBackColor = false;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // chkShowValue
            // 
            this.chkShowValue.AutoSize = true;
            this.chkShowValue.BackColor = System.Drawing.SystemColors.Control;
            this.chkShowValue.Location = new System.Drawing.Point(14, 13);
            this.chkShowValue.Name = "chkShowValue";
            this.chkShowValue.Size = new System.Drawing.Size(82, 18);
            this.chkShowValue.TabIndex = 159;
            this.chkShowValue.Text = "显示数值";
            this.chkShowValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkShowValue.UseVisualStyleBackColor = false;
            this.chkShowValue.CheckedChanged += new System.EventHandler(this.chkShowValue_CheckedChanged);
            // 
            // dtime_e
            // 
            this.dtime_e.CustomFormat = "yyyy-MM-dd";
            this.dtime_e.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtime_e.Location = new System.Drawing.Point(480, 11);
            this.dtime_e.Name = "dtime_e";
            this.dtime_e.Size = new System.Drawing.Size(103, 23);
            this.dtime_e.TabIndex = 156;
            this.dtime_e.ValueChanged += new System.EventHandler(this.dtime_e_ValueChanged);
            // 
            // lblNote2
            // 
            this.lblNote2.AutoSize = true;
            this.lblNote2.BackColor = System.Drawing.SystemColors.Window;
            this.lblNote2.Location = new System.Drawing.Point(450, 15);
            this.lblNote2.Name = "lblNote2";
            this.lblNote2.Size = new System.Drawing.Size(21, 14);
            this.lblNote2.TabIndex = 157;
            this.lblNote2.Text = "--";
            this.lblNote2.Click += new System.EventHandler(this.lblNote2_Click);
            // 
            // dtime_s
            // 
            this.dtime_s.CustomFormat = "yyyy-MM-dd";
            this.dtime_s.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtime_s.Location = new System.Drawing.Point(338, 11);
            this.dtime_s.Name = "dtime_s";
            this.dtime_s.Size = new System.Drawing.Size(103, 23);
            this.dtime_s.TabIndex = 155;
            this.dtime_s.ValueChanged += new System.EventHandler(this.dtime_s_ValueChanged);
            // 
            // lblNote1
            // 
            this.lblNote1.AutoSize = true;
            this.lblNote1.BackColor = System.Drawing.SystemColors.Control;
            this.lblNote1.Location = new System.Drawing.Point(266, 15);
            this.lblNote1.Name = "lblNote1";
            this.lblNote1.Size = new System.Drawing.Size(63, 14);
            this.lblNote1.TabIndex = 158;
            this.lblNote1.Text = "选择日期";
            this.lblNote1.Click += new System.EventHandler(this.lblNote1_Click);
            // 
            // dnReport
            // 
            this.dnReport.AddNewItem = null;
            this.dnReport.AutoSize = false;
            this.dnReport.CountItem = null;
            this.dnReport.DeleteItem = null;
            this.dnReport.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton3,
            this.toolStripButton2,
            this.toolStripButton4,
            this.tspbZoom,
            this.toolExport,
            this.toolStripSeparator5,
            this.toolStripButtonHelp});
            this.dnReport.Location = new System.Drawing.Point(0, 0);
            this.dnReport.MoveFirstItem = null;
            this.dnReport.MoveLastItem = null;
            this.dnReport.MoveNextItem = null;
            this.dnReport.MovePreviousItem = null;
            this.dnReport.Name = "dnReport";
            this.dnReport.PositionItem = null;
            this.dnReport.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.dnReport.Size = new System.Drawing.Size(795, 35);
            this.dnReport.TabIndex = 162;
            this.dnReport.Text = "bindingNavigator1";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(84, 32);
            this.toolStripButton3.Text = "打印预览  ";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click_1);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = global::ww.form.Properties.Resources.config;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(84, 32);
            this.toolStripButton2.Text = "打印设置  ";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(52, 32);
            this.toolStripButton4.Text = "打 印   ";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // tspbZoom
            // 
            this.tspbZoom.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tool25,
            this.tool50,
            this.tool100,
            this.tool200,
            this.tool400,
            this.toolWhole,
            this.toolPageWidth});
            this.tspbZoom.Image = ((System.Drawing.Image)(resources.GetObject("tspbZoom.Image")));
            this.tspbZoom.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tspbZoom.Name = "tspbZoom";
            this.tspbZoom.Size = new System.Drawing.Size(96, 32);
            this.tspbZoom.Text = "显示比例  ";
            // 
            // tool25
            // 
            this.tool25.Name = "tool25";
            this.tool25.Size = new System.Drawing.Size(152, 22);
            this.tool25.Text = "25%";
            this.tool25.Click += new System.EventHandler(this.tool25_Click);
            // 
            // tool50
            // 
            this.tool50.Name = "tool50";
            this.tool50.Size = new System.Drawing.Size(152, 22);
            this.tool50.Text = "50%";
            this.tool50.Click += new System.EventHandler(this.tool50_Click);
            // 
            // tool100
            // 
            this.tool100.Name = "tool100";
            this.tool100.Size = new System.Drawing.Size(152, 22);
            this.tool100.Text = "100%";
            // 
            // tool200
            // 
            this.tool200.Name = "tool200";
            this.tool200.Size = new System.Drawing.Size(152, 22);
            this.tool200.Text = "200%";
            // 
            // tool400
            // 
            this.tool400.Name = "tool400";
            this.tool400.Size = new System.Drawing.Size(152, 22);
            this.tool400.Text = "400%";
            // 
            // toolWhole
            // 
            this.toolWhole.Name = "toolWhole";
            this.toolWhole.Size = new System.Drawing.Size(152, 22);
            this.toolWhole.Text = "整页";
            // 
            // toolPageWidth
            // 
            this.toolPageWidth.Name = "toolPageWidth";
            this.toolPageWidth.Size = new System.Drawing.Size(152, 22);
            this.toolPageWidth.Text = "页宽";
            // 
            // toolExport
            // 
            this.toolExport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolExcel,
            this.导出PDFToolStripMenuItem});
            this.toolExport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolExport.Name = "toolExport";
            this.toolExport.Size = new System.Drawing.Size(60, 32);
            this.toolExport.Text = "导 出  ";
            this.toolExport.Visible = false;
            // 
            // toolExcel
            // 
            this.toolExcel.Name = "toolExcel";
            this.toolExcel.Size = new System.Drawing.Size(129, 22);
            this.toolExcel.Text = "导出Excel";
            // 
            // 导出PDFToolStripMenuItem
            // 
            this.导出PDFToolStripMenuItem.Name = "导出PDFToolStripMenuItem";
            this.导出PDFToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.导出PDFToolStripMenuItem.Text = "导出PDF";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 35);
            // 
            // toolStripButtonHelp
            // 
            this.toolStripButtonHelp.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHelp.Image")));
            this.toolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonHelp.Name = "toolStripButtonHelp";
            this.toolStripButtonHelp.Size = new System.Drawing.Size(74, 32);
            this.toolStripButtonHelp.Text = "帮助(&F)  ";
            this.toolStripButtonHelp.Visible = false;
            // 
            // panel_图
            // 
            this.panel_图.Controls.Add(this.zedGraphControl1);
            this.panel_图.Location = new System.Drawing.Point(123, 87);
            this.panel_图.Name = "panel_图";
            this.panel_图.Size = new System.Drawing.Size(850, 450);
            this.panel_图.TabIndex = 163;
            // 
            // zedGraphControl1
            // 
            this.zedGraphControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.zedGraphControl1.IsEnableHZoom = false;
            this.zedGraphControl1.IsEnableVZoom = false;
            this.zedGraphControl1.Location = new System.Drawing.Point(0, 0);
            this.zedGraphControl1.Name = "zedGraphControl1";
            this.zedGraphControl1.ScrollGrace = 0;
            this.zedGraphControl1.ScrollMaxX = 0;
            this.zedGraphControl1.ScrollMaxY = 0;
            this.zedGraphControl1.ScrollMaxY2 = 0;
            this.zedGraphControl1.ScrollMinX = 0;
            this.zedGraphControl1.ScrollMinY = 0;
            this.zedGraphControl1.ScrollMinY2 = 0;
            this.zedGraphControl1.Size = new System.Drawing.Size(436, 450);
            this.zedGraphControl1.TabIndex = 3;
            // 
            // PreviewDialog
            // 
            this.PreviewDialog.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.PreviewDialog.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.PreviewDialog.ClientSize = new System.Drawing.Size(396, 296);
            this.PreviewDialog.Enabled = true;
            this.PreviewDialog.Icon = ((System.Drawing.Icon)(resources.GetObject("PreviewDialog.Icon")));
            this.PreviewDialog.Name = "printPreviewDialog1";
            this.PreviewDialog.ShowIcon = false;
            this.PreviewDialog.Visible = false;
            // 
            // QCImgForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(795, 458);
            this.Controls.Add(this.rptViewer);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dnReport);
            this.Controls.Add(this.panel_图);
            this.Font = new System.Drawing.Font("宋体", 10F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "QCImgForm";
            this.ShowIcon = false;
            this.Text = "医院各科室直接成本表";
            this.Load += new System.EventHandler(this.ReportViewer_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dnReport)).EndInit();
            this.dnReport.ResumeLayout(false);
            this.dnReport.PerformLayout();
            this.panel_图.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonQuery;
        private Microsoft.Reporting.WinForms.ReportViewer rptViewer;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox chkShowValue;
        private System.Windows.Forms.DateTimePicker dtime_e;
        private System.Windows.Forms.Label lblNote2;
        private System.Windows.Forms.DateTimePicker dtime_s;
        private System.Windows.Forms.Label lblNote1;
        private System.Windows.Forms.BindingNavigator dnReport;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripSplitButton tspbZoom;
        private System.Windows.Forms.ToolStripMenuItem tool25;
        private System.Windows.Forms.ToolStripMenuItem tool50;
        private System.Windows.Forms.ToolStripMenuItem tool100;
        private System.Windows.Forms.ToolStripMenuItem tool200;
        private System.Windows.Forms.ToolStripMenuItem tool400;
        private System.Windows.Forms.ToolStripMenuItem toolWhole;
        private System.Windows.Forms.ToolStripMenuItem toolPageWidth;
        private System.Windows.Forms.ToolStripSplitButton toolExport;
        private System.Windows.Forms.ToolStripMenuItem toolExcel;
        private System.Windows.Forms.ToolStripMenuItem 导出PDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton toolStripButtonHelp;
        private System.Windows.Forms.Panel panel_图;
        private ZedGraph.ZedGraphControl zedGraphControl1;
        private System.Windows.Forms.PrintPreviewDialog PreviewDialog;

    }
}