namespace ww.form.lis.sam.qc
{
    partial class QC_LJForm
    {
        #region  用户定义
        /// <summary>
        /// 报表路径
        /// </summary>
        public string ReportPath
        {
            /*
            get
            {
                return this.rptViewer.LocalReport.ReportPath;
            }
            set
            {
                this.rptViewer.LocalReport.ReportPath = value;
            }*/
            get
            {
                return this.rptViewer.LocalReport.ReportEmbeddedResource;
            }
            set
            {
                this.rptViewer.LocalReport.ReportEmbeddedResource = value;
            }
        }

        private string m_ReportName = string.Empty;
        /// <summary>
        /// 报表名称
        /// </summary>
        public string ReportName
        {
            get
            {
                return this.m_ReportName;
            }
            set
            {
                this.m_ReportName = value;
            }
        }

        /// <summary>
        /// DataSource of the Main Report
        /// </summary>
        private object m_MainDataSet = null;
        /// <summary>
        /// 报表数据集  主
        /// </summary>
        public object MainDataSet
        {
            get
            {
                return this.m_MainDataSet;
            }
            set
            {
                this.m_MainDataSet = value;
            }
        }

        /// <summary>
        /// DataSource of the DrillThrough Report
        /// </summary>
        private object m_DrillDataSet = null;
        /// <summary>
        /// 报表数据集  子
        /// </summary>
        public object DrillDataSet
        {
            get
            {
                return this.m_DrillDataSet;
            }
            set
            {
                this.m_DrillDataSet = value;
            }
        }
        /// <summary>
        /// Data Source Name of the Main Report
        /// </summary>
        private string m_MainDataSourceName = string.Empty;
        /// <summary>
        /// 主数据源名
        /// </summary>
        public string MainDataSourceName
        {
            get
            {
                return this.m_MainDataSourceName;
            }
            set
            {
                this.m_MainDataSourceName = value;
            }
        }

        /// <summary>
        /// Data Source Name of the DrillThrough Report
        /// </summary>
        private string m_DrillDataSourceName = string.Empty;
        /// <summary>
        /// 子数据源名
        /// </summary>
        public string DrillDataSourceName
        {
            get
            {
                return this.m_DrillDataSourceName;
            }
            set
            {
                this.m_DrillDataSourceName = value;
            }
        }



        /// <summary>
        /// Data Source Name of the Main Report
        /// </summary>
        private string m_MainDataSourceName2 = string.Empty;
        public string MainDataSourceName2
        {
            get
            {
                return this.m_MainDataSourceName2;
            }
            set
            {
                this.m_MainDataSourceName2 = value;
            }
        }
        #endregion 
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QC_LJForm));
            this.dnReport = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.tspbZoom = new System.Windows.Forms.ToolStripSplitButton();
            this.tool25 = new System.Windows.Forms.ToolStripMenuItem();
            this.tool50 = new System.Windows.Forms.ToolStripMenuItem();
            this.tool100 = new System.Windows.Forms.ToolStripMenuItem();
            this.tool200 = new System.Windows.Forms.ToolStripMenuItem();
            this.tool400 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolWhole = new System.Windows.Forms.ToolStripMenuItem();
            this.toolPageWidth = new System.Windows.Forms.ToolStripMenuItem();
            this.PreviewDialog = new System.Windows.Forms.PrintPreviewDialog();
            this.groupBoxif = new System.Windows.Forms.GroupBox();
            this.comboBoxInstr = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox次数 = new System.Windows.Forms.ComboBox();
            this.lblNote2 = new System.Windows.Forms.Label();
            this.chkShowValue = new System.Windows.Forms.CheckBox();
            this.dtime_e = new System.Windows.Forms.DateTimePicker();
            this.dtime_s = new System.Windows.Forms.DateTimePicker();
            this.buttonQuery = new System.Windows.Forms.Button();
            this.panel_图 = new System.Windows.Forms.Panel();
            this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.DataGridViewObject = new System.Windows.Forms.DataGridView();
            this.fitem_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSource项目 = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rptViewer = new Microsoft.Reporting.WinForms.ReportViewer();
            this.dataGridView质控样本 = new System.Windows.Forms.DataGridView();
            this.质控品 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.起始日期 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.批号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.靶值 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSource质控样本 = new System.Windows.Forms.BindingSource(this.components);
            this.splitter1 = new System.Windows.Forms.Splitter();
            ((System.ComponentModel.ISupportInitialize)(this.dnReport)).BeginInit();
            this.dnReport.SuspendLayout();
            this.groupBoxif.SuspendLayout();
            this.panel_图.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource项目)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView质控样本)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource质控样本)).BeginInit();
            this.SuspendLayout();
            // 
            // dnReport
            // 
            this.dnReport.AddNewItem = null;
            this.dnReport.AutoSize = false;
            this.dnReport.CountItem = null;
            this.dnReport.DeleteItem = null;
            this.dnReport.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton3,
            this.toolStripButton2,
            this.toolStripButton4,
            this.tspbZoom});
            this.dnReport.Location = new System.Drawing.Point(0, 0);
            this.dnReport.MoveFirstItem = null;
            this.dnReport.MoveLastItem = null;
            this.dnReport.MoveNextItem = null;
            this.dnReport.MovePreviousItem = null;
            this.dnReport.Name = "dnReport";
            this.dnReport.PositionItem = null;
            this.dnReport.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.dnReport.Size = new System.Drawing.Size(784, 35);
            this.dnReport.TabIndex = 0;
            this.dnReport.Text = "bindingNavigator1";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(84, 32);
            this.toolStripButton3.Text = "打印预览  ";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = global::ww.form.Properties.Resources.config;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(84, 32);
            this.toolStripButton2.Text = "打印设置  ";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.Image = global::ww.form.Properties.Resources.button_print;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(68, 32);
            this.toolStripButton4.Text = "打 印   ";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // tspbZoom
            // 
            this.tspbZoom.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tool25,
            this.tool50,
            this.tool100,
            this.tool200,
            this.tool400,
            this.toolWhole,
            this.toolPageWidth});
            this.tspbZoom.Image = ((System.Drawing.Image)(resources.GetObject("tspbZoom.Image")));
            this.tspbZoom.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tspbZoom.Name = "tspbZoom";
            this.tspbZoom.Size = new System.Drawing.Size(96, 32);
            this.tspbZoom.Text = "显示比例  ";
            // 
            // tool25
            // 
            this.tool25.Name = "tool25";
            this.tool25.Size = new System.Drawing.Size(108, 22);
            this.tool25.Text = "25%";
            this.tool25.Click += new System.EventHandler(this.tool25_Click);
            // 
            // tool50
            // 
            this.tool50.Name = "tool50";
            this.tool50.Size = new System.Drawing.Size(108, 22);
            this.tool50.Text = "50%";
            this.tool50.Click += new System.EventHandler(this.tool50_Click);
            // 
            // tool100
            // 
            this.tool100.Name = "tool100";
            this.tool100.Size = new System.Drawing.Size(108, 22);
            this.tool100.Text = "100%";
            this.tool100.Click += new System.EventHandler(this.tool100_Click);
            // 
            // tool200
            // 
            this.tool200.Name = "tool200";
            this.tool200.Size = new System.Drawing.Size(108, 22);
            this.tool200.Text = "200%";
            this.tool200.Click += new System.EventHandler(this.tool200_Click);
            // 
            // tool400
            // 
            this.tool400.Name = "tool400";
            this.tool400.Size = new System.Drawing.Size(108, 22);
            this.tool400.Text = "400%";
            this.tool400.Click += new System.EventHandler(this.tool400_Click);
            // 
            // toolWhole
            // 
            this.toolWhole.Name = "toolWhole";
            this.toolWhole.Size = new System.Drawing.Size(108, 22);
            this.toolWhole.Text = "整页";
            this.toolWhole.Click += new System.EventHandler(this.toolWhole_Click);
            // 
            // toolPageWidth
            // 
            this.toolPageWidth.Name = "toolPageWidth";
            this.toolPageWidth.Size = new System.Drawing.Size(108, 22);
            this.toolPageWidth.Text = "页宽";
            this.toolPageWidth.Click += new System.EventHandler(this.toolPageWidth_Click);
            // 
            // PreviewDialog
            // 
            this.PreviewDialog.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.PreviewDialog.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.PreviewDialog.ClientSize = new System.Drawing.Size(396, 296);
            this.PreviewDialog.Enabled = true;
            this.PreviewDialog.Icon = ((System.Drawing.Icon)(resources.GetObject("PreviewDialog.Icon")));
            this.PreviewDialog.Name = "printPreviewDialog1";
            this.PreviewDialog.ShowIcon = false;
            this.PreviewDialog.Visible = false;
            // 
            // groupBoxif
            // 
            this.groupBoxif.Controls.Add(this.comboBoxInstr);
            this.groupBoxif.Controls.Add(this.label3);
            this.groupBoxif.Controls.Add(this.label2);
            this.groupBoxif.Controls.Add(this.label1);
            this.groupBoxif.Controls.Add(this.comboBox次数);
            this.groupBoxif.Controls.Add(this.lblNote2);
            this.groupBoxif.Controls.Add(this.chkShowValue);
            this.groupBoxif.Controls.Add(this.dtime_e);
            this.groupBoxif.Controls.Add(this.dtime_s);
            this.groupBoxif.Controls.Add(this.buttonQuery);
            this.groupBoxif.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxif.Location = new System.Drawing.Point(0, 35);
            this.groupBoxif.Name = "groupBoxif";
            this.groupBoxif.Size = new System.Drawing.Size(784, 51);
            this.groupBoxif.TabIndex = 146;
            this.groupBoxif.TabStop = false;
            // 
            // comboBoxInstr
            // 
            this.comboBoxInstr.DisplayMember = "ShowName";
            this.comboBoxInstr.FormattingEnabled = true;
            this.comboBoxInstr.Location = new System.Drawing.Point(46, 19);
            this.comboBoxInstr.Name = "comboBoxInstr";
            this.comboBoxInstr.Size = new System.Drawing.Size(136, 20);
            this.comboBoxInstr.TabIndex = 170;
            this.comboBoxInstr.ValueMember = "finstr_id";
            this.comboBoxInstr.SelectedIndexChanged += new System.EventHandler(this.comboBoxInstr_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 169;
            this.label3.Text = "仪器：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(277, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 168;
            this.label2.Text = "期间";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(185, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 167;
            this.label1.Text = "次数";
            // 
            // comboBox次数
            // 
            this.comboBox次数.FormattingEnabled = true;
            this.comboBox次数.Items.AddRange(new object[] {
            "平均值",
            "第一次",
            "第二次",
            "第三次",
            "第四次",
            "第五次",
            "第六次",
            "第七次"});
            this.comboBox次数.Location = new System.Drawing.Point(214, 19);
            this.comboBox次数.Name = "comboBox次数";
            this.comboBox次数.Size = new System.Drawing.Size(60, 20);
            this.comboBox次数.TabIndex = 166;
            // 
            // lblNote2
            // 
            this.lblNote2.AutoSize = true;
            this.lblNote2.Location = new System.Drawing.Point(389, 23);
            this.lblNote2.Name = "lblNote2";
            this.lblNote2.Size = new System.Drawing.Size(11, 12);
            this.lblNote2.TabIndex = 165;
            this.lblNote2.Text = "-";
            // 
            // chkShowValue
            // 
            this.chkShowValue.AutoSize = true;
            this.chkShowValue.BackColor = System.Drawing.SystemColors.Control;
            this.chkShowValue.Location = new System.Drawing.Point(482, 21);
            this.chkShowValue.Name = "chkShowValue";
            this.chkShowValue.Size = new System.Drawing.Size(72, 16);
            this.chkShowValue.TabIndex = 163;
            this.chkShowValue.Text = "显示数值";
            this.chkShowValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkShowValue.UseVisualStyleBackColor = false;
            // 
            // dtime_e
            // 
            this.dtime_e.CustomFormat = "yyyy-MM-dd";
            this.dtime_e.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtime_e.Location = new System.Drawing.Point(399, 19);
            this.dtime_e.Name = "dtime_e";
            this.dtime_e.Size = new System.Drawing.Size(80, 21);
            this.dtime_e.TabIndex = 162;
            // 
            // dtime_s
            // 
            this.dtime_s.CustomFormat = "yyyy-MM-dd";
            this.dtime_s.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtime_s.Location = new System.Drawing.Point(306, 19);
            this.dtime_s.Name = "dtime_s";
            this.dtime_s.Size = new System.Drawing.Size(80, 21);
            this.dtime_s.TabIndex = 161;
            // 
            // buttonQuery
            // 
            this.buttonQuery.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonQuery.Location = new System.Drawing.Point(554, 17);
            this.buttonQuery.Name = "buttonQuery";
            this.buttonQuery.Size = new System.Drawing.Size(88, 25);
            this.buttonQuery.TabIndex = 17;
            this.buttonQuery.Text = "画图(F5)";
            this.buttonQuery.UseVisualStyleBackColor = true;
            this.buttonQuery.Click += new System.EventHandler(this.buttonQuery_Click);
            // 
            // panel_图
            // 
            this.panel_图.Controls.Add(this.zedGraphControl1);
            this.panel_图.Location = new System.Drawing.Point(77, 311);
            this.panel_图.Name = "panel_图";
            this.panel_图.Size = new System.Drawing.Size(800, 380);
            this.panel_图.TabIndex = 164;
            // 
            // zedGraphControl1
            // 
            this.zedGraphControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.zedGraphControl1.IsEnableHZoom = false;
            this.zedGraphControl1.IsEnableVZoom = false;
            this.zedGraphControl1.Location = new System.Drawing.Point(0, 0);
            this.zedGraphControl1.Name = "zedGraphControl1";
            this.zedGraphControl1.ScrollGrace = 0;
            this.zedGraphControl1.ScrollMaxX = 0;
            this.zedGraphControl1.ScrollMaxY = 0;
            this.zedGraphControl1.ScrollMaxY2 = 0;
            this.zedGraphControl1.ScrollMinX = 0;
            this.zedGraphControl1.ScrollMinY = 0;
            this.zedGraphControl1.ScrollMinY2 = 0;
            this.zedGraphControl1.Size = new System.Drawing.Size(436, 380);
            this.zedGraphControl1.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.DataGridViewObject);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 86);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(159, 335);
            this.groupBox1.TabIndex = 166;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "质控项目";
            // 
            // DataGridViewObject
            // 
            this.DataGridViewObject.AllowUserToAddRows = false;
            this.DataGridViewObject.AllowUserToResizeRows = false;
            this.DataGridViewObject.AutoGenerateColumns = false;
            this.DataGridViewObject.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DataGridViewObject.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DataGridViewObject.ColumnHeadersHeight = 21;
            this.DataGridViewObject.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fitem_code,
            this.fname,
            this.fitem_id});
            this.DataGridViewObject.DataSource = this.bindingSource项目;
            this.DataGridViewObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridViewObject.EnableHeadersVisualStyles = false;
            this.DataGridViewObject.Location = new System.Drawing.Point(3, 17);
            this.DataGridViewObject.MultiSelect = false;
            this.DataGridViewObject.Name = "DataGridViewObject";
            this.DataGridViewObject.RowHeadersVisible = false;
            this.DataGridViewObject.RowHeadersWidth = 35;
            this.DataGridViewObject.RowTemplate.Height = 23;
            this.DataGridViewObject.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewObject.Size = new System.Drawing.Size(153, 315);
            this.DataGridViewObject.TabIndex = 166;
            this.DataGridViewObject.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewObject_CellContentClick);
            // 
            // fitem_code
            // 
            this.fitem_code.DataPropertyName = "fitem_code";
            this.fitem_code.HeaderText = "代号";
            this.fitem_code.Name = "fitem_code";
            this.fitem_code.Width = 50;
            // 
            // fname
            // 
            this.fname.DataPropertyName = "fname";
            this.fname.HeaderText = "名称";
            this.fname.Name = "fname";
            this.fname.Width = 120;
            // 
            // fitem_id
            // 
            this.fitem_id.DataPropertyName = "fitem_id";
            this.fitem_id.HeaderText = "fitem_id";
            this.fitem_id.Name = "fitem_id";
            this.fitem_id.Visible = false;
            // 
            // bindingSource项目
            // 
            this.bindingSource项目.PositionChanged += new System.EventHandler(this.bindingSource项目_PositionChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rptViewer);
            this.groupBox2.Controls.Add(this.dataGridView质控样本);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(159, 86);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(625, 335);
            this.groupBox2.TabIndex = 167;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "质控图";
            // 
            // rptViewer
            // 
            this.rptViewer.AutoScroll = true;
            this.rptViewer.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.rptViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptViewer.DocumentMapCollapsed = true;
            this.rptViewer.IsDocumentMapWidthFixed = true;
            this.rptViewer.LocalReport.ReportEmbeddedResource = "RDLCPrint.rptWuLiao.rdlc";
            this.rptViewer.Location = new System.Drawing.Point(3, 87);
            this.rptViewer.Name = "rptViewer";
            this.rptViewer.ShowToolBar = false;
            this.rptViewer.Size = new System.Drawing.Size(619, 245);
            this.rptViewer.TabIndex = 2;
            // 
            // dataGridView质控样本
            // 
            this.dataGridView质控样本.AllowUserToAddRows = false;
            this.dataGridView质控样本.AllowUserToResizeRows = false;
            this.dataGridView质控样本.AutoGenerateColumns = false;
            this.dataGridView质控样本.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView质控样本.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView质控样本.ColumnHeadersHeight = 21;
            this.dataGridView质控样本.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.质控品,
            this.起始日期,
            this.批号,
            this.靶值,
            this.SD});
            this.dataGridView质控样本.DataSource = this.bindingSource质控样本;
            this.dataGridView质控样本.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataGridView质控样本.EnableHeadersVisualStyles = false;
            this.dataGridView质控样本.Location = new System.Drawing.Point(3, 17);
            this.dataGridView质控样本.MultiSelect = false;
            this.dataGridView质控样本.Name = "dataGridView质控样本";
            this.dataGridView质控样本.RowHeadersVisible = false;
            this.dataGridView质控样本.RowHeadersWidth = 35;
            this.dataGridView质控样本.RowTemplate.Height = 23;
            this.dataGridView质控样本.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView质控样本.Size = new System.Drawing.Size(619, 70);
            this.dataGridView质控样本.TabIndex = 167;
            this.dataGridView质控样本.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView质控样本_CellContentClick);
            // 
            // 质控品
            // 
            this.质控品.DataPropertyName = "质控品";
            this.质控品.HeaderText = "质控品";
            this.质控品.Name = "质控品";
            this.质控品.Width = 50;
            // 
            // 起始日期
            // 
            this.起始日期.DataPropertyName = "起始日期";
            this.起始日期.HeaderText = "起始日期";
            this.起始日期.Name = "起始日期";
            // 
            // 批号
            // 
            this.批号.DataPropertyName = "批号";
            this.批号.HeaderText = "批号";
            this.批号.Name = "批号";
            // 
            // 靶值
            // 
            this.靶值.DataPropertyName = "靶值";
            this.靶值.HeaderText = "靶值";
            this.靶值.Name = "靶值";
            // 
            // SD
            // 
            this.SD.DataPropertyName = "SD";
            this.SD.HeaderText = "SD";
            this.SD.Name = "SD";
            // 
            // bindingSource质控样本
            // 
            this.bindingSource质控样本.PositionChanged += new System.EventHandler(this.bindingSource质控样本_PositionChanged);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(159, 86);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 335);
            this.splitter1.TabIndex = 168;
            this.splitter1.TabStop = false;
            // 
            // QC_LJForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 421);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel_图);
            this.Controls.Add(this.groupBoxif);
            this.Controls.Add(this.dnReport);
            this.Font = new System.Drawing.Font("宋体", 9F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "QC_LJForm";
            this.ShowIcon = false;
            this.Text = "L_J质控图";
            this.Load += new System.EventHandler(this.ReportViewer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dnReport)).EndInit();
            this.dnReport.ResumeLayout(false);
            this.dnReport.PerformLayout();
            this.groupBoxif.ResumeLayout(false);
            this.groupBoxif.PerformLayout();
            this.panel_图.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource项目)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView质控样本)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource质控样本)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingNavigator dnReport;
        private System.Windows.Forms.PrintPreviewDialog PreviewDialog;
        private System.Windows.Forms.ToolStripSplitButton tspbZoom;
        private System.Windows.Forms.ToolStripMenuItem tool25;
        private System.Windows.Forms.ToolStripMenuItem tool50;
        private System.Windows.Forms.ToolStripMenuItem tool100;
        private System.Windows.Forms.ToolStripMenuItem tool200;
        private System.Windows.Forms.ToolStripMenuItem tool400;
        private System.Windows.Forms.ToolStripMenuItem toolWhole;
        private System.Windows.Forms.ToolStripMenuItem toolPageWidth;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.GroupBox groupBoxif;
        private System.Windows.Forms.Button buttonQuery;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.CheckBox chkShowValue;
        private System.Windows.Forms.DateTimePicker dtime_e;
        private System.Windows.Forms.DateTimePicker dtime_s;
        private System.Windows.Forms.Label lblNote2;
        private System.Windows.Forms.Panel panel_图;
        private ZedGraph.ZedGraphControl zedGraphControl1;
        private System.Windows.Forms.ComboBox comboBox次数;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxInstr;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView DataGridViewObject;
        private System.Windows.Forms.GroupBox groupBox2;
        private Microsoft.Reporting.WinForms.ReportViewer rptViewer;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.BindingSource bindingSource项目;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_id;
        private System.Windows.Forms.DataGridView dataGridView质控样本;
        private System.Windows.Forms.BindingSource bindingSource质控样本;
        private System.Windows.Forms.DataGridViewTextBoxColumn 质控品;
        private System.Windows.Forms.DataGridViewTextBoxColumn 起始日期;
        private System.Windows.Forms.DataGridViewTextBoxColumn 批号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 靶值;
        private System.Windows.Forms.DataGridViewTextBoxColumn SD;

    }
}