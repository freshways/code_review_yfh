﻿namespace ww.form.lis.sam.sq
{
    partial class ApplyForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ApplyForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            this.imageListTree = new System.Windows.Forms.ImageList(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.wwTreeViewCheckType = new ww.wwf.control.WWControlLib.WWTreeView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.radioButtonJZ = new System.Windows.Forms.RadioButton();
            this.radioButtonCG = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbfroom_num = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tbfage = new System.Windows.Forms.TextBox();
            this.buttonDept = new System.Windows.Forms.Button();
            this.buttonuser_id = new System.Windows.Forms.Button();
            this.tbfdiagnose = new System.Windows.Forms.TextBox();
            this.tbfapply_user_name = new System.Windows.Forms.TextBox();
            this.tbfapply_user_id = new System.Windows.Forms.TextBox();
            this.cbftype_id = new System.Windows.Forms.ComboBox();
            this.tbfdept_name = new System.Windows.Forms.TextBox();
            this.tbfdept_id = new System.Windows.Forms.TextBox();
            this.tbfbed_num = new System.Windows.Forms.TextBox();
            this.tbfhz_zyh = new System.Windows.Forms.TextBox();
            this.cbfage_unit = new System.Windows.Forms.ComboBox();
            this.cbfsex = new System.Windows.Forms.ComboBox();
            this.tbfhz_id = new System.Windows.Forms.TextBox();
            this.tbfname = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.barcodeNETWindows1 = new BarcodeNETWorkShop.BarcodeNETWindows();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dataGridViewAllItem = new System.Windows.Forms.DataGridView();
            this.sel_item = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fitem_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fname_item = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.funit_name_zw = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forder_by_item = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.DataGridViewObject = new System.Windows.Forms.DataGridView();
            this.sel = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fsam_type_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fcheck_type_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fgroup_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dataGridViewOK = new System.Windows.Forms.DataGridView();
            this.fitem_code_ok = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_name_ok = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_id_ok = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forder_by = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel4 = new System.Windows.Forms.Panel();
            this.wwTreeViewHZ = new ww.wwf.control.WWControlLib.WWTreeView();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.fjy_dateDateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.fjy_dateDateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.bN = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonS = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonC = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonHelp = new System.Windows.Forms.ToolStripButton();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAllItem)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOK)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).BeginInit();
            this.bN.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageListTree
            // 
            this.imageListTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTree.ImageStream")));
            this.imageListTree.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTree.Images.SetKeyName(0, "12.gif");
            this.imageListTree.Images.SetKeyName(1, "27.gif");
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(197, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(185, 476);
            this.panel1.TabIndex = 134;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.wwTreeViewCheckType);
            this.groupBox2.Controls.Add(this.panel3);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 210);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(185, 266);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "检验类别";
            // 
            // wwTreeViewCheckType
            // 
            this.wwTreeViewCheckType.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wwTreeViewCheckType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wwTreeViewCheckType.ImageIndex = 0;
            this.wwTreeViewCheckType.ImageList = this.imageListTree;
            this.wwTreeViewCheckType.Location = new System.Drawing.Point(3, 39);
            this.wwTreeViewCheckType.Name = "wwTreeViewCheckType";
            this.wwTreeViewCheckType.SelectedImageIndex = 1;
            this.wwTreeViewCheckType.Size = new System.Drawing.Size(179, 224);
            this.wwTreeViewCheckType.TabIndex = 134;
            this.wwTreeViewCheckType.ZADataTable = null;
            this.wwTreeViewCheckType.ZADisplayFieldName = "";
            this.wwTreeViewCheckType.ZAKeyFieldName = "";
            this.wwTreeViewCheckType.ZAParentFieldName = "";
            this.wwTreeViewCheckType.ZAToolTipTextName = "";
            this.wwTreeViewCheckType.ZATreeViewRootValue = "";
            this.wwTreeViewCheckType.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.wwTreeViewCheckType_AfterSelect);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.radioButtonJZ);
            this.panel3.Controls.Add(this.radioButtonCG);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 17);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(179, 22);
            this.panel3.TabIndex = 0;
            // 
            // radioButtonJZ
            // 
            this.radioButtonJZ.AutoSize = true;
            this.radioButtonJZ.Location = new System.Drawing.Point(79, 4);
            this.radioButtonJZ.Name = "radioButtonJZ";
            this.radioButtonJZ.Size = new System.Drawing.Size(47, 16);
            this.radioButtonJZ.TabIndex = 1;
            this.radioButtonJZ.Text = "急诊";
            this.radioButtonJZ.UseVisualStyleBackColor = true;
            // 
            // radioButtonCG
            // 
            this.radioButtonCG.AutoSize = true;
            this.radioButtonCG.Checked = true;
            this.radioButtonCG.Location = new System.Drawing.Point(14, 4);
            this.radioButtonCG.Name = "radioButtonCG";
            this.radioButtonCG.Size = new System.Drawing.Size(47, 16);
            this.radioButtonCG.TabIndex = 0;
            this.radioButtonCG.TabStop = true;
            this.radioButtonCG.Text = "常规";
            this.radioButtonCG.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbfroom_num);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.tbfage);
            this.groupBox1.Controls.Add(this.buttonDept);
            this.groupBox1.Controls.Add(this.buttonuser_id);
            this.groupBox1.Controls.Add(this.tbfdiagnose);
            this.groupBox1.Controls.Add(this.tbfapply_user_name);
            this.groupBox1.Controls.Add(this.tbfapply_user_id);
            this.groupBox1.Controls.Add(this.cbftype_id);
            this.groupBox1.Controls.Add(this.tbfdept_name);
            this.groupBox1.Controls.Add(this.tbfdept_id);
            this.groupBox1.Controls.Add(this.tbfbed_num);
            this.groupBox1.Controls.Add(this.tbfhz_zyh);
            this.groupBox1.Controls.Add(this.cbfage_unit);
            this.groupBox1.Controls.Add(this.cbfsex);
            this.groupBox1.Controls.Add(this.tbfhz_id);
            this.groupBox1.Controls.Add(this.tbfname);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(185, 210);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "患者信息";
            // 
            // tbfroom_num
            // 
            this.tbfroom_num.Location = new System.Drawing.Point(50, 53);
            this.tbfroom_num.Name = "tbfroom_num";
            this.tbfroom_num.Size = new System.Drawing.Size(44, 21);
            this.tbfroom_num.TabIndex = 29;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 57);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 12);
            this.label12.TabIndex = 27;
            this.label12.Text = "房间:";
            // 
            // tbfage
            // 
            this.tbfage.Location = new System.Drawing.Point(117, 141);
            this.tbfage.Name = "tbfage";
            this.tbfage.Size = new System.Drawing.Size(25, 21);
            this.tbfage.TabIndex = 13;
            // 
            // buttonDept
            // 
            this.buttonDept.Image = ((System.Drawing.Image)(resources.GetObject("buttonDept.Image")));
            this.buttonDept.Location = new System.Drawing.Point(153, 30);
            this.buttonDept.Name = "buttonDept";
            this.buttonDept.Size = new System.Drawing.Size(25, 23);
            this.buttonDept.TabIndex = 26;
            this.buttonDept.UseVisualStyleBackColor = true;
            this.buttonDept.Click += new System.EventHandler(this.buttonDept_Click);
            // 
            // buttonuser_id
            // 
            this.buttonuser_id.Image = ((System.Drawing.Image)(resources.GetObject("buttonuser_id.Image")));
            this.buttonuser_id.Location = new System.Drawing.Point(153, 184);
            this.buttonuser_id.Name = "buttonuser_id";
            this.buttonuser_id.Size = new System.Drawing.Size(25, 23);
            this.buttonuser_id.TabIndex = 25;
            this.buttonuser_id.UseVisualStyleBackColor = true;
            this.buttonuser_id.Click += new System.EventHandler(this.buttonuser_id_Click);
            // 
            // tbfdiagnose
            // 
            this.tbfdiagnose.Location = new System.Drawing.Point(50, 163);
            this.tbfdiagnose.Name = "tbfdiagnose";
            this.tbfdiagnose.Size = new System.Drawing.Size(128, 21);
            this.tbfdiagnose.TabIndex = 24;
            // 
            // tbfapply_user_name
            // 
            this.tbfapply_user_name.Location = new System.Drawing.Point(69, 185);
            this.tbfapply_user_name.Name = "tbfapply_user_name";
            this.tbfapply_user_name.ReadOnly = true;
            this.tbfapply_user_name.Size = new System.Drawing.Size(81, 21);
            this.tbfapply_user_name.TabIndex = 23;
            this.tbfapply_user_name.DoubleClick += new System.EventHandler(this.tbfapply_user_name_DoubleClick);
            // 
            // tbfapply_user_id
            // 
            this.tbfapply_user_id.Location = new System.Drawing.Point(116, 185);
            this.tbfapply_user_id.Name = "tbfapply_user_id";
            this.tbfapply_user_id.Size = new System.Drawing.Size(34, 21);
            this.tbfapply_user_id.TabIndex = 22;
            this.tbfapply_user_id.Visible = false;
            // 
            // cbftype_id
            // 
            this.cbftype_id.DisplayMember = "fname";
            this.cbftype_id.FormattingEnabled = true;
            this.cbftype_id.Location = new System.Drawing.Point(50, 10);
            this.cbftype_id.Name = "cbftype_id";
            this.cbftype_id.Size = new System.Drawing.Size(128, 20);
            this.cbftype_id.TabIndex = 21;
            this.cbftype_id.ValueMember = "fname";
            // 
            // tbfdept_name
            // 
            this.tbfdept_name.Location = new System.Drawing.Point(50, 31);
            this.tbfdept_name.Name = "tbfdept_name";
            this.tbfdept_name.ReadOnly = true;
            this.tbfdept_name.Size = new System.Drawing.Size(100, 21);
            this.tbfdept_name.TabIndex = 19;
            this.tbfdept_name.DoubleClick += new System.EventHandler(this.tbfdept_name_DoubleClick);
            // 
            // tbfdept_id
            // 
            this.tbfdept_id.Location = new System.Drawing.Point(58, 31);
            this.tbfdept_id.Name = "tbfdept_id";
            this.tbfdept_id.Size = new System.Drawing.Size(52, 21);
            this.tbfdept_id.TabIndex = 17;
            this.tbfdept_id.Visible = false;
            // 
            // tbfbed_num
            // 
            this.tbfbed_num.Location = new System.Drawing.Point(134, 53);
            this.tbfbed_num.Name = "tbfbed_num";
            this.tbfbed_num.Size = new System.Drawing.Size(44, 21);
            this.tbfbed_num.TabIndex = 16;
            // 
            // tbfhz_zyh
            // 
            this.tbfhz_zyh.Location = new System.Drawing.Point(50, 75);
            this.tbfhz_zyh.Name = "tbfhz_zyh";
            this.tbfhz_zyh.Size = new System.Drawing.Size(128, 21);
            this.tbfhz_zyh.TabIndex = 15;
            // 
            // cbfage_unit
            // 
            this.cbfage_unit.FormattingEnabled = true;
            this.cbfage_unit.Items.AddRange(new object[] {
            "岁",
            "月",
            "天",
            "?"});
            this.cbfage_unit.Location = new System.Drawing.Point(143, 141);
            this.cbfage_unit.Name = "cbfage_unit";
            this.cbfage_unit.Size = new System.Drawing.Size(35, 20);
            this.cbfage_unit.TabIndex = 14;
            // 
            // cbfsex
            // 
            this.cbfsex.FormattingEnabled = true;
            this.cbfsex.Items.AddRange(new object[] {
            "男",
            "女",
            "?"});
            this.cbfsex.Location = new System.Drawing.Point(50, 141);
            this.cbfsex.Name = "cbfsex";
            this.cbfsex.Size = new System.Drawing.Size(35, 20);
            this.cbfsex.TabIndex = 12;
            // 
            // tbfhz_id
            // 
            this.tbfhz_id.Location = new System.Drawing.Point(50, 97);
            this.tbfhz_id.Name = "tbfhz_id";
            this.tbfhz_id.Size = new System.Drawing.Size(128, 21);
            this.tbfhz_id.TabIndex = 11;
            this.tbfhz_id.TextChanged += new System.EventHandler(this.tbfhz_id_TextChanged);
            // 
            // tbfname
            // 
            this.tbfname.Location = new System.Drawing.Point(50, 119);
            this.tbfname.Name = "tbfname";
            this.tbfname.Size = new System.Drawing.Size(128, 21);
            this.tbfname.TabIndex = 10;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 189);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 12);
            this.label10.TabIndex = 9;
            this.label10.Text = "主管医生:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 167);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 12);
            this.label9.TabIndex = 8;
            this.label9.Text = "诊断:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(86, 145);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 12);
            this.label8.TabIndex = 7;
            this.label8.Text = "年龄:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 145);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 12);
            this.label7.TabIndex = 6;
            this.label7.Text = "性别:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 123);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 12);
            this.label6.TabIndex = 5;
            this.label6.Text = "姓名:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 101);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 12);
            this.label5.TabIndex = 4;
            this.label5.Text = "患者ID:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "住院号:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(96, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "床号:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "科室:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "类别:";
            // 
            // barcodeNETWindows1
            // 
            this.barcodeNETWindows1.AntiAlias = false;
            this.barcodeNETWindows1.AutoSize = true;
            this.barcodeNETWindows1.BackColor = System.Drawing.Color.White;
            this.barcodeNETWindows1.BarcodeColor = System.Drawing.Color.Black;
            this.barcodeNETWindows1.BarcodeGap = new BarcodeNETWorkShop.Core.TextGap(0, 0, 1, 0);
            this.barcodeNETWindows1.BarcodeMargins = new BarcodeNETWorkShop.Core.MarginBound(2, 2, 0, 0);
            this.barcodeNETWindows1.BarcodeText = "000000";
            this.barcodeNETWindows1.BarcodeType = BarcodeNETWorkShop.Core.BARCODE_TYPE.EAN128C;
            this.barcodeNETWindows1.BarHeight = 20;
            this.barcodeNETWindows1.BarWidth = 1;
            this.barcodeNETWindows1.BgColor = System.Drawing.Color.White;
            this.barcodeNETWindows1.CustomText = "";
            this.barcodeNETWindows1.ExceptionType = BarcodeNETWorkShop.Core.EXCEPTION_TYPE.DEFAULT_MSG;
            this.barcodeNETWindows1.FileFormat = BarcodeNETWorkShop.Core.FILE_FORMAT.JPG;
            this.barcodeNETWindows1.Font = new System.Drawing.Font("华文中宋", 9F, System.Drawing.FontStyle.Bold);
            this.barcodeNETWindows1.IncludeChecksumDigit = true;
            this.barcodeNETWindows1.IsRounded = false;
            this.barcodeNETWindows1.Location = new System.Drawing.Point(124, 159);
            this.barcodeNETWindows1.Name = "barcodeNETWindows1";
            this.barcodeNETWindows1.RotateAngle = BarcodeNETWorkShop.Core.ROTATE_ANGLE.R0;
            this.barcodeNETWindows1.ShowBarcodeText = true;
            this.barcodeNETWindows1.ShowBorder = false;
            this.barcodeNETWindows1.SilentMode = false;
            this.barcodeNETWindows1.Size = new System.Drawing.Size(83, 47);
            this.barcodeNETWindows1.SupplementalText = "";
            this.barcodeNETWindows1.SupplementalTextStyle = BarcodeNETWorkShop.Core.SUPPLEMENTAL_TEXT_STYLE.TOP;
            this.barcodeNETWindows1.TabIndex = 62;
            this.barcodeNETWindows1.Tag = ".";
            this.barcodeNETWindows1.TextColor = System.Drawing.Color.Black;
            this.barcodeNETWindows1.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.barcodeNETWindows1.TopText = ".";
            this.barcodeNETWindows1.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox4);
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(382, 30);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(457, 330);
            this.panel2.TabIndex = 135;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.barcodeNETWindows1);
            this.groupBox4.Controls.Add(this.dataGridViewAllItem);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(180, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(277, 330);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "单项";
            // 
            // dataGridViewAllItem
            // 
            this.dataGridViewAllItem.AllowUserToAddRows = false;
            this.dataGridViewAllItem.AllowUserToResizeRows = false;
            this.dataGridViewAllItem.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewAllItem.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewAllItem.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewAllItem.ColumnHeadersHeight = 21;
            this.dataGridViewAllItem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sel_item,
            this.fitem_code,
            this.fname_item,
            this.funit_name_zw,
            this.forder_by_item,
            this.fitem_id});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewAllItem.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewAllItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewAllItem.EnableHeadersVisualStyles = false;
            this.dataGridViewAllItem.Location = new System.Drawing.Point(3, 17);
            this.dataGridViewAllItem.MultiSelect = false;
            this.dataGridViewAllItem.Name = "dataGridViewAllItem";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewAllItem.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewAllItem.RowHeadersVisible = false;
            this.dataGridViewAllItem.RowHeadersWidth = 35;
            this.dataGridViewAllItem.RowTemplate.Height = 23;
            this.dataGridViewAllItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewAllItem.Size = new System.Drawing.Size(271, 310);
            this.dataGridViewAllItem.TabIndex = 134;
            this.dataGridViewAllItem.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewAllItem_DataError);
            this.dataGridViewAllItem.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewAllItem_CellContentClick);
            // 
            // sel_item
            // 
            this.sel_item.FalseValue = "0";
            this.sel_item.HeaderText = "选";
            this.sel_item.IndeterminateValue = "0";
            this.sel_item.Name = "sel_item";
            this.sel_item.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.sel_item.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.sel_item.TrueValue = "1";
            this.sel_item.Width = 25;
            // 
            // fitem_code
            // 
            this.fitem_code.DataPropertyName = "fitem_code";
            this.fitem_code.HeaderText = "代号";
            this.fitem_code.Name = "fitem_code";
            this.fitem_code.ReadOnly = true;
            this.fitem_code.Width = 75;
            // 
            // fname_item
            // 
            this.fname_item.DataPropertyName = "fname";
            this.fname_item.HeaderText = "名称";
            this.fname_item.Name = "fname_item";
            this.fname_item.ReadOnly = true;
            this.fname_item.Width = 130;
            // 
            // funit_name_zw
            // 
            this.funit_name_zw.DataPropertyName = "funit_name_zw";
            this.funit_name_zw.HeaderText = "单位";
            this.funit_name_zw.Name = "funit_name_zw";
            this.funit_name_zw.Width = 80;
            // 
            // forder_by_item
            // 
            this.forder_by_item.DataPropertyName = "fprint_num";
            this.forder_by_item.HeaderText = "forder_by";
            this.forder_by_item.Name = "forder_by_item";
            this.forder_by_item.Visible = false;
            // 
            // fitem_id
            // 
            this.fitem_id.DataPropertyName = "fitem_id";
            this.fitem_id.HeaderText = "fitem_id";
            this.fitem_id.Name = "fitem_id";
            this.fitem_id.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.DataGridViewObject);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(180, 330);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "组合项目";
            // 
            // DataGridViewObject
            // 
            this.DataGridViewObject.AllowUserToAddRows = false;
            this.DataGridViewObject.AllowUserToResizeRows = false;
            this.DataGridViewObject.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DataGridViewObject.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.DataGridViewObject.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sel,
            this.fname,
            this.fsam_type_id,
            this.fcheck_type_id,
            this.fgroup_id});
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridViewObject.DefaultCellStyle = dataGridViewCellStyle14;
            this.DataGridViewObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridViewObject.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.DataGridViewObject.EnableHeadersVisualStyles = false;
            this.DataGridViewObject.Location = new System.Drawing.Point(3, 17);
            this.DataGridViewObject.MultiSelect = false;
            this.DataGridViewObject.Name = "DataGridViewObject";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.DataGridViewObject.RowHeadersVisible = false;
            this.DataGridViewObject.RowTemplate.Height = 23;
            this.DataGridViewObject.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewObject.Size = new System.Drawing.Size(174, 310);
            this.DataGridViewObject.TabIndex = 133;
            this.DataGridViewObject.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataGridViewObject_DataError);
            this.DataGridViewObject.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewObject_CellContentClick);
            // 
            // sel
            // 
            this.sel.DataPropertyName = "sel";
            this.sel.FalseValue = "0";
            this.sel.HeaderText = "选";
            this.sel.IndeterminateValue = "0";
            this.sel.Name = "sel";
            this.sel.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.sel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.sel.TrueValue = "1";
            this.sel.Width = 25;
            // 
            // fname
            // 
            this.fname.DataPropertyName = "fname";
            this.fname.HeaderText = "名称";
            this.fname.Name = "fname";
            this.fname.ReadOnly = true;
            this.fname.Width = 200;
            // 
            // fsam_type_id
            // 
            this.fsam_type_id.DataPropertyName = "fsam_type_id";
            this.fsam_type_id.HeaderText = "fsam_type_id";
            this.fsam_type_id.Name = "fsam_type_id";
            this.fsam_type_id.Visible = false;
            // 
            // fcheck_type_id
            // 
            this.fcheck_type_id.DataPropertyName = "fcheck_type_id";
            this.fcheck_type_id.HeaderText = "fcheck_type_id";
            this.fcheck_type_id.Name = "fcheck_type_id";
            this.fcheck_type_id.Visible = false;
            // 
            // fgroup_id
            // 
            this.fgroup_id.DataPropertyName = "fgroup_id";
            this.fgroup_id.HeaderText = "fgroup_id";
            this.fgroup_id.Name = "fgroup_id";
            this.fgroup_id.Visible = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.dataGridViewOK);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(382, 360);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(457, 146);
            this.groupBox5.TabIndex = 136;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "选中项目";
            // 
            // dataGridViewOK
            // 
            this.dataGridViewOK.AllowUserToAddRows = false;
            this.dataGridViewOK.AllowUserToResizeRows = false;
            this.dataGridViewOK.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewOK.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewOK.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewOK.ColumnHeadersHeight = 21;
            this.dataGridViewOK.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fitem_code_ok,
            this.fitem_name_ok,
            this.fitem_unit,
            this.fitem_id_ok,
            this.forder_by});
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewOK.DefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewOK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewOK.EnableHeadersVisualStyles = false;
            this.dataGridViewOK.Location = new System.Drawing.Point(3, 17);
            this.dataGridViewOK.MultiSelect = false;
            this.dataGridViewOK.Name = "dataGridViewOK";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewOK.RowHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridViewOK.RowHeadersVisible = false;
            this.dataGridViewOK.RowHeadersWidth = 35;
            this.dataGridViewOK.RowTemplate.Height = 23;
            this.dataGridViewOK.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewOK.Size = new System.Drawing.Size(451, 126);
            this.dataGridViewOK.TabIndex = 135;
            // 
            // fitem_code_ok
            // 
            this.fitem_code_ok.DataPropertyName = "fitem_code";
            this.fitem_code_ok.HeaderText = "代号";
            this.fitem_code_ok.Name = "fitem_code_ok";
            this.fitem_code_ok.ReadOnly = true;
            // 
            // fitem_name_ok
            // 
            this.fitem_name_ok.DataPropertyName = "fitem_name";
            this.fitem_name_ok.HeaderText = "名称";
            this.fitem_name_ok.Name = "fitem_name_ok";
            this.fitem_name_ok.ReadOnly = true;
            this.fitem_name_ok.Width = 200;
            // 
            // fitem_unit
            // 
            this.fitem_unit.DataPropertyName = "fitem_unit";
            this.fitem_unit.HeaderText = "单位";
            this.fitem_unit.Name = "fitem_unit";
            this.fitem_unit.Width = 80;
            // 
            // fitem_id_ok
            // 
            this.fitem_id_ok.DataPropertyName = "fitem_id";
            this.fitem_id_ok.HeaderText = "fitem_id";
            this.fitem_id_ok.Name = "fitem_id_ok";
            this.fitem_id_ok.Visible = false;
            // 
            // forder_by
            // 
            this.forder_by.DataPropertyName = "forder_by";
            this.forder_by.HeaderText = "排序";
            this.forder_by.Name = "forder_by";
            this.forder_by.Visible = false;
            this.forder_by.Width = 40;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.wwTreeViewHZ);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 30);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(197, 476);
            this.panel4.TabIndex = 137;
            // 
            // wwTreeViewHZ
            // 
            this.wwTreeViewHZ.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wwTreeViewHZ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wwTreeViewHZ.ImageIndex = 0;
            this.wwTreeViewHZ.ImageList = this.imageListTree;
            this.wwTreeViewHZ.Location = new System.Drawing.Point(0, 43);
            this.wwTreeViewHZ.Name = "wwTreeViewHZ";
            this.wwTreeViewHZ.SelectedImageIndex = 1;
            this.wwTreeViewHZ.Size = new System.Drawing.Size(197, 433);
            this.wwTreeViewHZ.TabIndex = 133;
            this.wwTreeViewHZ.ZADataTable = null;
            this.wwTreeViewHZ.ZADisplayFieldName = "";
            this.wwTreeViewHZ.ZAKeyFieldName = "";
            this.wwTreeViewHZ.ZAParentFieldName = "";
            this.wwTreeViewHZ.ZAToolTipTextName = "";
            this.wwTreeViewHZ.ZATreeViewRootValue = "";
            this.wwTreeViewHZ.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.wwTreeViewHZ_AfterSelect);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.fjy_dateDateTimePicker2);
            this.panel5.Controls.Add(this.fjy_dateDateTimePicker1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(197, 43);
            this.panel5.TabIndex = 134;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(94, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(11, 12);
            this.label11.TabIndex = 18;
            this.label11.Text = "-";
            // 
            // fjy_dateDateTimePicker2
            // 
            this.fjy_dateDateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker2.Location = new System.Drawing.Point(106, 12);
            this.fjy_dateDateTimePicker2.Name = "fjy_dateDateTimePicker2";
            this.fjy_dateDateTimePicker2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker2.Size = new System.Drawing.Size(85, 21);
            this.fjy_dateDateTimePicker2.TabIndex = 17;
            this.fjy_dateDateTimePicker2.CloseUp += new System.EventHandler(this.fjy_dateDateTimePicker2_CloseUp);
            // 
            // fjy_dateDateTimePicker1
            // 
            this.fjy_dateDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker1.Location = new System.Drawing.Point(6, 12);
            this.fjy_dateDateTimePicker1.Name = "fjy_dateDateTimePicker1";
            this.fjy_dateDateTimePicker1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker1.Size = new System.Drawing.Size(85, 21);
            this.fjy_dateDateTimePicker1.TabIndex = 15;
            this.fjy_dateDateTimePicker1.CloseUp += new System.EventHandler(this.fjy_dateDateTimePicker1_CloseUp);
            // 
            // bN
            // 
            this.bN.AddNewItem = null;
            this.bN.AutoSize = false;
            this.bN.CountItem = null;
            this.bN.DeleteItem = null;
            this.bN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.toolStripButtonS,
            this.toolStripSeparator2,
            this.toolStripButtonC,
            this.toolStripSeparator1,
            this.toolStripButton1,
            this.toolStripSeparator3,
            this.toolStripButtonHelp});
            this.bN.Location = new System.Drawing.Point(0, 0);
            this.bN.MoveFirstItem = null;
            this.bN.MoveLastItem = null;
            this.bN.MoveNextItem = null;
            this.bN.MovePreviousItem = null;
            this.bN.Name = "bN";
            this.bN.PositionItem = null;
            this.bN.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bN.Size = new System.Drawing.Size(839, 30);
            this.bN.TabIndex = 138;
            this.bN.Text = "bindingNavigator1";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonS
            // 
            this.toolStripButtonS.Image = global::ww.form.Properties.Resources.button_tj;
            this.toolStripButtonS.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonS.Name = "toolStripButtonS";
            this.toolStripButtonS.Size = new System.Drawing.Size(125, 27);
            this.toolStripButtonS.Text = "提交申请单(F5)    ";
            this.toolStripButtonS.Click += new System.EventHandler(this.toolStripButtonS_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonC
            // 
            this.toolStripButtonC.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonC.Image")));
            this.toolStripButtonC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonC.Name = "toolStripButtonC";
            this.toolStripButtonC.Size = new System.Drawing.Size(129, 27);
            this.toolStripButtonC.Text = "取消所选项目(F2)  ";
            this.toolStripButtonC.Click += new System.EventHandler(this.toolStripButtonC_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::ww.form.Properties.Resources.button_print1;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(89, 27);
            this.toolStripButton1.Text = "打印(F3)    ";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonHelp
            // 
            this.toolStripButtonHelp.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHelp.Image")));
            this.toolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonHelp.Name = "toolStripButtonHelp";
            this.toolStripButtonHelp.Size = new System.Drawing.Size(81, 27);
            this.toolStripButtonHelp.Text = "帮助(F1)  ";
            this.toolStripButtonHelp.Click += new System.EventHandler(this.toolStripButtonHelp_Click);
            // 
            // ApplyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 506);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.bN);
            this.Name = "ApplyForm";
            this.Text = "ApplyForm";
            this.Load += new System.EventHandler(this.ApplyForm_Load);
            this.panel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAllItem)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).EndInit();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOK)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).EndInit();
            this.bN.ResumeLayout(false);
            this.bN.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton radioButtonJZ;
        private System.Windows.Forms.RadioButton radioButtonCG;
        private System.Windows.Forms.GroupBox groupBox5;
        private ww.wwf.control.WWControlLib.WWTreeView wwTreeViewCheckType;
        private System.Windows.Forms.ImageList imageListTree;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker2;
        private System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker1;
        private System.Windows.Forms.TextBox tbfname;
        private System.Windows.Forms.TextBox tbfhz_id;
        private System.Windows.Forms.ComboBox cbfsex;
        private System.Windows.Forms.ComboBox cbfage_unit;
        private System.Windows.Forms.TextBox tbfage;
        private System.Windows.Forms.TextBox tbfhz_zyh;
        private System.Windows.Forms.TextBox tbfbed_num;
        private System.Windows.Forms.TextBox tbfdept_id;
        private System.Windows.Forms.TextBox tbfdept_name;
        private System.Windows.Forms.ComboBox cbftype_id;
        private System.Windows.Forms.TextBox tbfapply_user_id;
        private System.Windows.Forms.TextBox tbfapply_user_name;
        private System.Windows.Forms.TextBox tbfdiagnose;
        private System.Windows.Forms.Button buttonuser_id;
        private System.Windows.Forms.Button buttonDept;
        private ww.wwf.control.WWControlLib.WWTreeView wwTreeViewHZ;
        private System.Windows.Forms.DataGridView DataGridViewObject;
        private System.Windows.Forms.DataGridView dataGridViewAllItem;
        private System.Windows.Forms.DataGridView dataGridViewOK;
        private System.Windows.Forms.BindingNavigator bN;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripButton toolStripButtonS;
        private System.Windows.Forms.ToolStripButton toolStripButtonC;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn sel;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname;
        private System.Windows.Forms.DataGridViewTextBoxColumn fsam_type_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fcheck_type_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fgroup_id;
        private System.Windows.Forms.ToolStripButton toolStripButtonHelp;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbfroom_num;
        private System.Windows.Forms.DataGridViewCheckBoxColumn sel_item;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname_item;
        private System.Windows.Forms.DataGridViewTextBoxColumn funit_name_zw;
        private System.Windows.Forms.DataGridViewTextBoxColumn forder_by_item;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_code_ok;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_name_ok;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_id_ok;
        private System.Windows.Forms.DataGridViewTextBoxColumn forder_by;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private BarcodeNETWorkShop.BarcodeNETWindows barcodeNETWindows1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;

    }
}