﻿namespace ww.form.lis.sam.sq
{
    partial class ApplyQueryNewForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ApplyQueryNewForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bindingSourceApply = new System.Windows.Forms.BindingSource(this.components);
            this.imageListTree = new System.Windows.Forms.ImageList(this.components);
            this.contextMenuStripApplyQuery = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem_applygs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.帮助ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.关闭ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sam_typeBindingSource_fstate = new System.Windows.Forms.BindingSource(this.components);
            this.samDataSet = new ww.form.lis.sam.samDataSet();
            this.panel5 = new System.Windows.Forms.Panel();
            this.labelNotify = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxSFZH = new System.Windows.Forms.TextBox();
            this.textBox门诊住院号 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btn生成条码 = new System.Windows.Forms.Button();
            this.btn读电子健康卡 = new System.Windows.Forms.Button();
            this.btnReadCardAndQuery = new System.Windows.Forms.Button();
            this.btn查询 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox病人类型 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.fjy_dateDateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.fjy_dateDateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridViewApply = new System.Windows.Forms.DataGridView();
            this.fstate = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridViewItem = new System.Windows.Forms.DataGridView();
            this.bN = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSplitButtonGS = new System.Windows.Forms.ToolStripSplitButton();
            this.申请格式ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.项目模式ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSplitButtonDC = new System.Windows.Forms.ToolStripSplitButton();
            this.申请导出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.项目导出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonQuery = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton打印预览 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonPrintAll = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPrintTwo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonHelp = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonClose = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceApply)).BeginInit();
            this.contextMenuStripApplyQuery.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sam_typeBindingSource_fstate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).BeginInit();
            this.panel5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewApply)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).BeginInit();
            this.bN.SuspendLayout();
            this.SuspendLayout();
            // 
            // bindingSourceApply
            // 
            this.bindingSourceApply.PositionChanged += new System.EventHandler(this.bindingSourceApply_PositionChanged);
            // 
            // imageListTree
            // 
            this.imageListTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTree.ImageStream")));
            this.imageListTree.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTree.Images.SetKeyName(0, "12.gif");
            this.imageListTree.Images.SetKeyName(1, "27.gif");
            // 
            // contextMenuStripApplyQuery
            // 
            this.contextMenuStripApplyQuery.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStripApplyQuery.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_applygs,
            this.toolStripSeparator3,
            this.toolStripMenuItem2,
            this.toolStripSeparator1,
            this.帮助ToolStripMenuItem,
            this.toolStripSeparator2,
            this.关闭ToolStripMenuItem});
            this.contextMenuStripApplyQuery.Name = "contextMenuStripReport";
            this.contextMenuStripApplyQuery.Size = new System.Drawing.Size(166, 110);
            // 
            // toolStripMenuItem_applygs
            // 
            this.toolStripMenuItem_applygs.Name = "toolStripMenuItem_applygs";
            this.toolStripMenuItem_applygs.Size = new System.Drawing.Size(165, 22);
            this.toolStripMenuItem_applygs.Text = "申请单格式";
            this.toolStripMenuItem_applygs.Click += new System.EventHandler(this.toolStripMenuItem_applygs_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(162, 6);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(165, 22);
            this.toolStripMenuItem2.Text = "导出申请到Excel";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(162, 6);
            // 
            // 帮助ToolStripMenuItem
            // 
            this.帮助ToolStripMenuItem.Name = "帮助ToolStripMenuItem";
            this.帮助ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.帮助ToolStripMenuItem.Text = "帮助";
            this.帮助ToolStripMenuItem.Click += new System.EventHandler(this.帮助ToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(162, 6);
            // 
            // 关闭ToolStripMenuItem
            // 
            this.关闭ToolStripMenuItem.Name = "关闭ToolStripMenuItem";
            this.关闭ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.关闭ToolStripMenuItem.Text = "关闭";
            this.关闭ToolStripMenuItem.Click += new System.EventHandler(this.关闭ToolStripMenuItem_Click);
            // 
            // sam_typeBindingSource_fstate
            // 
            this.sam_typeBindingSource_fstate.DataMember = "sam_type";
            this.sam_typeBindingSource_fstate.DataSource = this.samDataSet;
            // 
            // samDataSet
            // 
            this.samDataSet.DataSetName = "samDataSet";
            this.samDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // panel5
            // 
            this.panel5.AutoScroll = true;
            this.panel5.Controls.Add(this.labelNotify);
            this.panel5.Controls.Add(this.textBoxName);
            this.panel5.Controls.Add(this.textBoxSFZH);
            this.panel5.Controls.Add(this.textBox门诊住院号);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.btn生成条码);
            this.panel5.Controls.Add(this.btn读电子健康卡);
            this.panel5.Controls.Add(this.btnReadCardAndQuery);
            this.panel5.Controls.Add(this.btn查询);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.comboBox病人类型);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.fjy_dateDateTimePicker2);
            this.panel5.Controls.Add(this.fjy_dateDateTimePicker1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 30);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(940, 59);
            this.panel5.TabIndex = 134;
            // 
            // labelNotify
            // 
            this.labelNotify.AutoSize = true;
            this.labelNotify.Location = new System.Drawing.Point(479, 39);
            this.labelNotify.Name = "labelNotify";
            this.labelNotify.Size = new System.Drawing.Size(0, 12);
            this.labelNotify.TabIndex = 26;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(352, 34);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(121, 21);
            this.textBoxName.TabIndex = 25;
            this.textBoxName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox门诊住院号_KeyPress);
            // 
            // textBoxSFZH
            // 
            this.textBoxSFZH.Location = new System.Drawing.Point(80, 34);
            this.textBoxSFZH.Name = "textBoxSFZH";
            this.textBoxSFZH.Size = new System.Drawing.Size(185, 21);
            this.textBoxSFZH.TabIndex = 25;
            this.textBoxSFZH.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox门诊住院号_KeyPress);
            // 
            // textBox门诊住院号
            // 
            this.textBox门诊住院号.Location = new System.Drawing.Point(566, 6);
            this.textBox门诊住院号.Name = "textBox门诊住院号";
            this.textBox门诊住院号.Size = new System.Drawing.Size(100, 21);
            this.textBox门诊住院号.TabIndex = 25;
            this.textBox门诊住院号.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox门诊住院号_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(479, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 12);
            this.label3.TabIndex = 24;
            this.label3.Text = "门诊/住院号：";
            // 
            // btn生成条码
            // 
            this.btn生成条码.Location = new System.Drawing.Point(808, 4);
            this.btn生成条码.Name = "btn生成条码";
            this.btn生成条码.Size = new System.Drawing.Size(84, 23);
            this.btn生成条码.TabIndex = 23;
            this.btn生成条码.Text = "生成条码";
            this.btn生成条码.UseVisualStyleBackColor = true;
            this.btn生成条码.Click += new System.EventHandler(this.btn生成条码_Click);
            // 
            // btn读电子健康卡
            // 
            this.btn读电子健康卡.BackColor = System.Drawing.Color.Transparent;
            this.btn读电子健康卡.Image = global::ww.form.Properties.Resources.wwNavigatorQuery;
            this.btn读电子健康卡.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btn读电子健康卡.Location = new System.Drawing.Point(587, 33);
            this.btn读电子健康卡.Name = "btn读电子健康卡";
            this.btn读电子健康卡.Size = new System.Drawing.Size(79, 23);
            this.btn读电子健康卡.TabIndex = 22;
            this.btn读电子健康卡.Text = "读电子卡";
            this.btn读电子健康卡.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn读电子健康卡.UseVisualStyleBackColor = false;
            this.btn读电子健康卡.Click += new System.EventHandler(this.btn读电子健康卡_Click);
            // 
            // btnReadCardAndQuery
            // 
            this.btnReadCardAndQuery.BackColor = System.Drawing.Color.Transparent;
            this.btnReadCardAndQuery.Image = global::ww.form.Properties.Resources.wwNavigatorQuery;
            this.btnReadCardAndQuery.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnReadCardAndQuery.Location = new System.Drawing.Point(689, 34);
            this.btnReadCardAndQuery.Name = "btnReadCardAndQuery";
            this.btnReadCardAndQuery.Size = new System.Drawing.Size(113, 23);
            this.btnReadCardAndQuery.TabIndex = 22;
            this.btnReadCardAndQuery.Text = "读社保卡查询";
            this.btnReadCardAndQuery.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReadCardAndQuery.UseVisualStyleBackColor = false;
            this.btnReadCardAndQuery.Click += new System.EventHandler(this.btnReadCardAndQuery_Click);
            // 
            // btn查询
            // 
            this.btn查询.BackColor = System.Drawing.Color.Transparent;
            this.btn查询.Image = global::ww.form.Properties.Resources.wwNavigatorQuery;
            this.btn查询.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btn查询.Location = new System.Drawing.Point(689, 4);
            this.btn查询.Name = "btn查询";
            this.btn查询.Size = new System.Drawing.Size(113, 23);
            this.btn查询.TabIndex = 22;
            this.btn查询.Text = "普通查询";
            this.btn查询.UseVisualStyleBackColor = false;
            this.btn查询.Click += new System.EventHandler(this.btn查询_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(284, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 20;
            this.label5.Text = "病人姓名：";
            // 
            // comboBox病人类型
            // 
            this.comboBox病人类型.DisplayMember = "fname";
            this.comboBox病人类型.FormattingEnabled = true;
            this.comboBox病人类型.Location = new System.Drawing.Point(352, 7);
            this.comboBox病人类型.Name = "comboBox病人类型";
            this.comboBox病人类型.Size = new System.Drawing.Size(121, 20);
            this.comboBox病人类型.TabIndex = 21;
            this.comboBox病人类型.ValueMember = "fname";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 20;
            this.label4.Text = "身份证号：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(284, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 20;
            this.label2.Text = "病人类型：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 19;
            this.label1.Text = "日期范围：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(168, 10);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(11, 12);
            this.label11.TabIndex = 18;
            this.label11.Text = "-";
            // 
            // fjy_dateDateTimePicker2
            // 
            this.fjy_dateDateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker2.Location = new System.Drawing.Point(180, 6);
            this.fjy_dateDateTimePicker2.Name = "fjy_dateDateTimePicker2";
            this.fjy_dateDateTimePicker2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker2.Size = new System.Drawing.Size(85, 21);
            this.fjy_dateDateTimePicker2.TabIndex = 17;
            // 
            // fjy_dateDateTimePicker1
            // 
            this.fjy_dateDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker1.Location = new System.Drawing.Point(80, 6);
            this.fjy_dateDateTimePicker1.Name = "fjy_dateDateTimePicker1";
            this.fjy_dateDateTimePicker1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker1.Size = new System.Drawing.Size(85, 21);
            this.fjy_dateDateTimePicker1.TabIndex = 15;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.dataGridViewApply);
            this.groupBox1.Location = new System.Drawing.Point(0, 95);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(940, 318);
            this.groupBox1.TabIndex = 142;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "申请单";
            // 
            // dataGridViewApply
            // 
            this.dataGridViewApply.AllowUserToAddRows = false;
            this.dataGridViewApply.AllowUserToResizeRows = false;
            this.dataGridViewApply.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewApply.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewApply.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewApply.ColumnHeadersHeight = 21;
            this.dataGridViewApply.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fstate});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewApply.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewApply.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewApply.EnableHeadersVisualStyles = false;
            this.dataGridViewApply.Location = new System.Drawing.Point(3, 17);
            this.dataGridViewApply.MultiSelect = false;
            this.dataGridViewApply.Name = "dataGridViewApply";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewApply.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewApply.RowHeadersVisible = false;
            this.dataGridViewApply.RowHeadersWidth = 35;
            this.dataGridViewApply.RowTemplate.Height = 23;
            this.dataGridViewApply.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewApply.Size = new System.Drawing.Size(934, 298);
            this.dataGridViewApply.TabIndex = 137;
            this.dataGridViewApply.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewApply_DataError);
            // 
            // fstate
            // 
            this.fstate.DataPropertyName = "fstate";
            this.fstate.DataSource = this.sam_typeBindingSource_fstate;
            this.fstate.DisplayMember = "fname";
            this.fstate.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.fstate.HeaderText = "fstate";
            this.fstate.Name = "fstate";
            this.fstate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fstate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fstate.ValueMember = "fcode";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridViewItem);
            this.groupBox2.Location = new System.Drawing.Point(197, 247);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(698, 169);
            this.groupBox2.TabIndex = 143;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "项目";
            this.groupBox2.Visible = false;
            // 
            // dataGridViewItem
            // 
            this.dataGridViewItem.AllowUserToAddRows = false;
            this.dataGridViewItem.AllowUserToResizeRows = false;
            this.dataGridViewItem.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewItem.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewItem.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewItem.ColumnHeadersHeight = 21;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewItem.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewItem.EnableHeadersVisualStyles = false;
            this.dataGridViewItem.Location = new System.Drawing.Point(3, 17);
            this.dataGridViewItem.MultiSelect = false;
            this.dataGridViewItem.Name = "dataGridViewItem";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewItem.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewItem.RowHeadersVisible = false;
            this.dataGridViewItem.RowHeadersWidth = 35;
            this.dataGridViewItem.RowTemplate.Height = 23;
            this.dataGridViewItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewItem.Size = new System.Drawing.Size(692, 149);
            this.dataGridViewItem.TabIndex = 136;
            this.dataGridViewItem.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewItem_DataError);
            // 
            // bN
            // 
            this.bN.AddNewItem = null;
            this.bN.AutoSize = false;
            this.bN.CountItem = this.bindingNavigatorCountItem;
            this.bN.DeleteItem = null;
            this.bN.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.bN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.toolStripSeparator8,
            this.toolStripSplitButtonGS,
            this.toolStripSeparator4,
            this.toolStripSplitButtonDC,
            this.toolStripSeparator5,
            this.toolStripButtonQuery,
            this.toolStripSeparator6,
            this.toolStripButton1,
            this.toolStripSeparator9,
            this.toolStripButton打印预览,
            this.toolStripSeparator11,
            this.toolStripButtonPrintAll,
            this.toolStripButtonPrintTwo,
            this.toolStripSeparator10,
            this.toolStripButtonHelp,
            this.toolStripSeparator7,
            this.toolStripButtonClose});
            this.bN.Location = new System.Drawing.Point(0, 0);
            this.bN.MoveFirstItem = null;
            this.bN.MoveLastItem = null;
            this.bN.MoveNextItem = null;
            this.bN.MovePreviousItem = null;
            this.bN.Name = "bN";
            this.bN.PositionItem = this.bindingNavigatorPositionItem;
            this.bN.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bN.Size = new System.Drawing.Size(940, 30);
            this.bN.TabIndex = 139;
            this.bN.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(32, 27);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(30, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripSplitButtonGS
            // 
            this.toolStripSplitButtonGS.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.申请格式ToolStripMenuItem,
            this.项目模式ToolStripMenuItem});
            this.toolStripSplitButtonGS.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButtonGS.Image")));
            this.toolStripSplitButtonGS.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButtonGS.Name = "toolStripSplitButtonGS";
            this.toolStripSplitButtonGS.Size = new System.Drawing.Size(92, 27);
            this.toolStripSplitButtonGS.Text = "格式设置";
            this.toolStripSplitButtonGS.Visible = false;
            // 
            // 申请格式ToolStripMenuItem
            // 
            this.申请格式ToolStripMenuItem.Name = "申请格式ToolStripMenuItem";
            this.申请格式ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.申请格式ToolStripMenuItem.Text = "申请格式";
            this.申请格式ToolStripMenuItem.Click += new System.EventHandler(this.申请格式ToolStripMenuItem_Click);
            // 
            // 项目模式ToolStripMenuItem
            // 
            this.项目模式ToolStripMenuItem.Name = "项目模式ToolStripMenuItem";
            this.项目模式ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.项目模式ToolStripMenuItem.Text = "项目模式";
            this.项目模式ToolStripMenuItem.Click += new System.EventHandler(this.项目模式ToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripSplitButtonDC
            // 
            this.toolStripSplitButtonDC.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.申请导出ToolStripMenuItem,
            this.项目导出ToolStripMenuItem});
            this.toolStripSplitButtonDC.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButtonDC.Image")));
            this.toolStripSplitButtonDC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButtonDC.Name = "toolStripSplitButtonDC";
            this.toolStripSplitButtonDC.Size = new System.Drawing.Size(97, 27);
            this.toolStripSplitButtonDC.Text = "导出Excel";
            // 
            // 申请导出ToolStripMenuItem
            // 
            this.申请导出ToolStripMenuItem.Name = "申请导出ToolStripMenuItem";
            this.申请导出ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.申请导出ToolStripMenuItem.Text = "申请导出";
            this.申请导出ToolStripMenuItem.Click += new System.EventHandler(this.申请导出ToolStripMenuItem_Click);
            // 
            // 项目导出ToolStripMenuItem
            // 
            this.项目导出ToolStripMenuItem.Name = "项目导出ToolStripMenuItem";
            this.项目导出ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.项目导出ToolStripMenuItem.Text = "项目导出";
            this.项目导出ToolStripMenuItem.Click += new System.EventHandler(this.项目导出ToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonQuery
            // 
            this.toolStripButtonQuery.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonQuery.Image")));
            this.toolStripButtonQuery.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonQuery.Name = "toolStripButtonQuery";
            this.toolStripButtonQuery.Size = new System.Drawing.Size(85, 27);
            this.toolStripButtonQuery.Text = "查询(F8)  ";
            this.toolStripButtonQuery.Visible = false;
            this.toolStripButtonQuery.Click += new System.EventHandler(this.toolStripButtonQuery_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 30);
            this.toolStripSeparator6.Visible = false;
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::ww.form.Properties.Resources.button_print1;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(93, 27);
            this.toolStripButton1.Text = "打印(F3)    ";
            this.toolStripButton1.ToolTipText = "单个打印(F3)    ";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButton打印预览
            // 
            this.toolStripButton打印预览.Image = global::ww.form.Properties.Resources.button_print2;
            this.toolStripButton打印预览.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton打印预览.Name = "toolStripButton打印预览";
            this.toolStripButton打印预览.Size = new System.Drawing.Size(80, 27);
            this.toolStripButton打印预览.Text = "打印预览";
            this.toolStripButton打印预览.ToolTipText = "单个预览";
            this.toolStripButton打印预览.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonPrintAll
            // 
            this.toolStripButtonPrintAll.Image = global::ww.form.Properties.Resources.button_print;
            this.toolStripButtonPrintAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPrintAll.Name = "toolStripButtonPrintAll";
            this.toolStripButtonPrintAll.Size = new System.Drawing.Size(128, 27);
            this.toolStripButtonPrintAll.Text = "打印病人全部条码";
            this.toolStripButtonPrintAll.ToolTipText = "打印某个病人的全部条码";
            this.toolStripButtonPrintAll.Click += new System.EventHandler(this.toolStripButtonPrintAll_Click);
            // 
            // toolStripButtonPrintTwo
            // 
            this.toolStripButtonPrintTwo.Image = global::ww.form.Properties.Resources.button_print;
            this.toolStripButtonPrintTwo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPrintTwo.Name = "toolStripButtonPrintTwo";
            this.toolStripButtonPrintTwo.Size = new System.Drawing.Size(76, 27);
            this.toolStripButtonPrintTwo.Text = "打印(二)";
            this.toolStripButtonPrintTwo.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonHelp
            // 
            this.toolStripButtonHelp.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHelp.Image")));
            this.toolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonHelp.Name = "toolStripButtonHelp";
            this.toolStripButtonHelp.Size = new System.Drawing.Size(85, 27);
            this.toolStripButtonHelp.Text = "帮助(F1)  ";
            this.toolStripButtonHelp.Visible = false;
            this.toolStripButtonHelp.Click += new System.EventHandler(this.toolStripButtonHelp_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonClose
            // 
            this.toolStripButtonClose.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonClose.Image")));
            this.toolStripButtonClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonClose.Name = "toolStripButtonClose";
            this.toolStripButtonClose.Size = new System.Drawing.Size(64, 27);
            this.toolStripButtonClose.Text = "关闭  ";
            this.toolStripButtonClose.Click += new System.EventHandler(this.toolStripButtonClose_Click);
            // 
            // ApplyQueryNewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(940, 416);
            this.ContextMenuStrip = this.contextMenuStripApplyQuery;
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.bN);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ApplyQueryNewForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "申请查询基础窗口";
            this.Load += new System.EventHandler(this.ApplyQueryNewForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceApply)).EndInit();
            this.contextMenuStripApplyQuery.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sam_typeBindingSource_fstate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewApply)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).EndInit();
            this.bN.ResumeLayout(false);
            this.bN.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ImageList imageListTree;
        protected System.Windows.Forms.BindingNavigator bN;
        protected System.Windows.Forms.ToolStripButton toolStripButtonQuery;
        protected System.Windows.Forms.ToolStripButton toolStripButtonHelp;
        protected System.Windows.Forms.ContextMenuStrip contextMenuStripApplyQuery;
        protected System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_applygs;
        protected System.Windows.Forms.ToolStripMenuItem 帮助ToolStripMenuItem;
        protected System.Windows.Forms.ToolStripMenuItem 关闭ToolStripMenuItem;
        private System.Windows.Forms.BindingSource sam_typeBindingSource_fstate;
        private ww.form.lis.sam.samDataSet samDataSet;
        private System.Windows.Forms.BindingSource bindingSourceApply;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButtonGS;
        private System.Windows.Forms.ToolStripMenuItem 申请格式ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 项目模式ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButtonDC;
        private System.Windows.Forms.ToolStripMenuItem 申请导出ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 项目导出ToolStripMenuItem;
        protected System.Windows.Forms.ToolStripButton toolStripButtonClose;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        protected System.Windows.Forms.GroupBox groupBox2;
        protected System.Windows.Forms.GroupBox groupBox1;
        protected System.Windows.Forms.DataGridView dataGridViewApply;
        protected System.Windows.Forms.DataGridView dataGridViewItem;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.DataGridViewComboBoxColumn fstate;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        protected System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        protected System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker2;
        protected System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker1;
        private System.Windows.Forms.ComboBox comboBox病人类型;
        private System.Windows.Forms.Button btn生成条码;
        private System.Windows.Forms.Button btn查询;
        private System.Windows.Forms.TextBox textBox门诊住院号;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripButton toolStripButton打印预览;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripButton toolStripButtonPrintAll;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxSFZH;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnReadCardAndQuery;
        private System.Windows.Forms.Label labelNotify;
        private System.Windows.Forms.ToolStripButton toolStripButtonPrintTwo;
        private System.Windows.Forms.Button btn读电子健康卡;
    }
}