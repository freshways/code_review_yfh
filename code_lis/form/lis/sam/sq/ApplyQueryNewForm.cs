﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.form.wwf;
using ww.lis.lisbll.sam;
using ww.wwf.wwfbll;
using System.Collections;
using TableXtraReport;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System.Drawing.Printing;
using ww.wwf.com;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ww.form.lis.sam.sq
{
    public partial class ApplyQueryNewForm : SysBaseForm
    {
        protected ww.wwf.wwfbll.UserdefinedFieldUpdateForm showApply = null;
        protected ww.wwf.wwfbll.UserdefinedFieldUpdateForm showResult = null;
        protected PatientBLL bllPatient = new PatientBLL();
        protected TypeBLL bllType = new TypeBLL();//公共类型逻辑  
        protected ApplyBLL bllApply = new ApplyBLL();//申请规则
        protected string strHelpID = "";//帮助ID

        protected DataTable dtMain;
        protected DataTable dtResult = new DataTable();

        string str申请单id = "";

        //changed by wjz 20150520 将旧的导出方式替换为NPOI的实现方式 ▽
        //ExcelHelper excelhelp = new ExcelHelper("", "");
        //changed by wjz 20150520 将旧的导出方式替换为NPOI的实现方式 △
        //2019-10-30 yfh 添加，读取电子健康卡
        private ScanerHook listener = new ScanerHook();
        public ApplyQueryNewForm()
        {
            InitializeComponent();
            dataGridViewApply.AutoGenerateColumns = false;
            this.dataGridViewItem.AutoGenerateColumns = false;

            ReadBarcodeprintCfg();
            listener.ScanerEvent += Listener_ScanerEvent;
        }
        private void Listener_ScanerEvent(ScanerHook.ScanerCodes codes)
        {
            string code = codes.Result.Replace(';', ':');
            if (!string.IsNullOrEmpty(code))
            {
                JObject user = Common.ExecuteReq(code);
                if (user!=null)
                {
                    this.textBoxName.Text = user["user_name"].ToString();
                    this.textBoxSFZH.Text = user["id_no"].ToString();
                    listener.Stop();
                }
                //ExecuteReq(code);
            }
        }

        bool bBarcodePrintOneMore = false;
        private void ReadBarcodeprintCfg()
        {
            try
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader("barcodesetting.cfg"))
                {
                    string line = reader.ReadLine();
                    reader.Close();
                    if(line == "1")
                    {
                        bBarcodePrintOneMore = true;
                    }
                    else
                    {
                        bBarcodePrintOneMore = false;
                    }
                }
            }
            catch
            {
                bBarcodePrintOneMore = false;
            }
        }

        //获取读卡标志
        
        private void GetReadFlag()
        {
            bool readflag = false;
            try
            {
                object obj = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn,
                    "select [参数值] from [dbo].[全局参数] where [参数名称]='read'");
                if (obj.ToString() == "1")
                {
                    readflag = true;
                }
            }
            catch{}

            btnReadCardAndQuery.Visible = readflag;
            if(readflag==false)
            {
                btn查询.Height = 52;
                btn生成条码.Height = 52;
            }
            else
            {
                btn查询.Height = 23;
                btn生成条码.Height = 23;
            }
        }

        private void ApplyQueryNewForm_Load(object sender, EventArgs e)
        {
            //add by wjz 20160902 添加是否读卡的判断▽
            GetReadFlag();
            //add by wjz 20160902 添加是否读卡的判断△

            this.WWComTypeList();
            //this.WWGetTreeHZ();
            this.WWSetShowApply("lis_sam_apply_query_apply");
            //this.WWSetShowResult("lis_sam_apply_query_result");
            this.Set病人类型();
            this.strHelpID = "78b5005b3a4f40b39d8390902c7201b1";

            //this.dataGridViewApply.CellPainting += dataGridView1_CellPainting;
        }

        private void Set病人类型()
        {
            DataTable dt = this.bllType.BllComTypeDT("病人类别", 1, "");
            this.comboBox病人类型.DataSource = dt;
        }

        #region        用户自定义方法
        protected void WWComTypeList()
        {
            try
            {
                this.sam_typeBindingSource_fstate.DataSource = this.bllType.BllComTypeDT("样本状态", 1, "");
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        /// <summary>
        /// 设置风格 申请
        /// </summary>
        /// <param name="strID"></param>
        protected void WWSetShowApply(string strID)
        {
            try
            {
                //dataGridViewApply.Columns.RemoveAt(0);
                dtMain = new DataTable();
                showApply = new ww.wwf.wwfbll.UserdefinedFieldUpdateForm(strID);
                dtMain = this.bllApply.BllSamplingMainDTFromHis(" fapply_time >'" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "'", ww.form.Properties.Settings.Default.HisDBName);
                bindingSourceApply.DataSource = dtMain;
                dataGridViewApply.DataSource = bindingSourceApply;

                this.showApply.DataGridViewSetStyleNew(this.dataGridViewApply);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        /// <summary>
        /// 取得申请列表
        /// </summary>
        private void WWGetApplyList()
        {
            //add by wjz 20160428 添加查询提示，提示是否查到了数据 ▽
            labelNotify.ForeColor = Color.Black;
            //add by wjz 20160428 添加查询提示，提示是否查到了数据 △

            try
            {
                //string fjy_date1 = this.bllApply.DbDateTime1(fjy_dateDateTimePicker1);
                //string fjy_date2 = this.bllApply.DbDateTime2(fjy_dateDateTimePicker2);
                //string strDateif = "";
                //if (strfhz_id == "" & strfdept_id!="")
                //    strDateif = "(t1.fapply_time>='" + fjy_date1 + "' and t1.fapply_time<='" + fjy_date2 + "')";
                //else if (strfhz_id != "" & strfdept_id == "")
                //    strDateif = " (fhz_id='" + strfhz_id + "')and(t1.fapply_time>='" + fjy_date1 + " 00:00:00" + "' and t1.fapply_time<='" + fjy_date2 + " 23:59:59" + "')";
                dtMain = new DataTable();
                string fjy_date1 = this.fjy_dateDateTimePicker1.Value.ToString("yyyy-MM-dd");
                string fjy_date2 = this.fjy_dateDateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59");
                string fjy_病人类型 = this.comboBox病人类型.SelectedValue.ToString();

                //20150629 add wjz
                string fjy_门诊住院号 = this.textBox门诊住院号.Text;
                //add by wjz 20160125 修改查询条件，如果门诊主要号输入的数据是四位，则将其转换为门诊号 ▽
                if(string.IsNullOrWhiteSpace(this.textBox门诊住院号.Text))
                {
                }
                else if(this.textBox门诊住院号.Text.Trim().Length == 4)
                {
                    this.textBox门诊住院号.Text = DateTime.Now.ToString("yyyyMMdd")+"."+ this.textBox门诊住院号.Text.Trim();
                    fjy_门诊住院号 = this.textBox门诊住院号.Text;
                }
                //add by wjz 20160125 修改查询条件，如果门诊主要号输入的数据是四位，则将其转换为门诊号 △
                string strDateif = " cast(fapply_time as datetime)>='" + fjy_date1 + "' and cast(fapply_time as datetime)<='" + fjy_date2 + "' and ftype_id='" + fjy_病人类型 + "' and fhz_zyh like '%" + fjy_门诊住院号 + "%'";

                //add by wjz 20160427 添加姓名、身份证号查询条件 ▽
                if(string.IsNullOrWhiteSpace(textBoxName.Text))
                { }
                else
                {
                    strDateif += " and fname='"+textBoxName.Text+"'";
                }

                if(string.IsNullOrWhiteSpace(textBoxSFZH.Text))
                { }
                else
                {
                    strDateif += " and sfzh='"+textBoxSFZH.Text+"' ";
                }
                //add by wjz 20160427 添加姓名、身份证号查询条件 △

                //add by wjz 20160125 修改门诊申请单的查询条件，对于门诊，只查询“已收款”的申请单信息。此处需要修改HIS视图 ▽
                if (fjy_病人类型.Trim() == "门诊")
                {
                    strDateif += " and 状态='已收款'";
                }
                //add by wjz 20160125 修改门诊申请单的查询条件，对于门诊，只查询“已收款”的申请单信息。此处需要修改HIS视图 △
                dtMain = this.bllApply.BllSamplingMainDTFromHis(strDateif, ww.form.Properties.Settings.Default.HisDBName);

                //add by wjz 20160428 添加查询提示，提示是否查到了数据 ▽
                if (dtMain == null || dtMain.Rows.Count == 0)
                {
                    labelNotify.ForeColor = Color.Red;
                    labelNotify.Text = "未查到任何数据";
                }
                //add by wjz 20160428 添加查询提示，提示是否查到了数据 △

                #region Lis端只负责数据的显示，申请号的生成放到了His端，所以不再需要这部分代码  20150630 wjz
                ////从LIS数据库中SAM_APPLY表中取出的数据
                //DataTable dtFromLisApply = this.bllApply.BllSamplingMainDTFromLis(strDateif);

                ////组合两组数据, 将dtFromLisApply中的申请单号、状态值，添加到dtMain中
                //for (int index = 0; index < dtMain.Rows.Count; index++ )
                //{
                //    //ftype_id, fhz_id, fhz_zyh,his_recordid
                //    string strTemp = "ftype_id='" + dtMain.Rows[index]["ftype_id"].ToString() + "' and fhz_id='" + dtMain.Rows[index]["fhz_id"].ToString()
                //        + "' and fhz_zyh='" + dtMain.Rows[index]["fhz_zyh"].ToString() + "' and his_recordid='" + dtMain.Rows[index]["his_recordid"].ToString() + "'";
                //    DataRow[] rows = dtFromLisApply.Select(strTemp);

                //    if(rows.Length > 0)
                //    {
                //        //fapply_id, fstate
                //        dtMain.Rows[index]["fapply_id"] = rows[0]["fapply_id"].ToString();
                //        dtMain.Rows[index]["fstate"] = rows[0]["fstate"].ToString();
                //    }
                //}
                #endregion

                this.bindingSourceApply.DataSource = dtMain;
                dataGridViewApply.DataSource = bindingSourceApply;
                this.bN.BindingSource = bindingSourceApply;
                int intfstate = 0;
                for (int intGrid = 0; intGrid < this.dataGridViewApply.Rows.Count; intGrid++)
                {
                    intfstate = 0;
                    //修改此部分的代码，基本逻辑没有变化 20150624 wjz begin
                    if (this.dataGridViewApply.Rows[intGrid].Cells["fstate"].Value != null)
                    {
                        try
                        {
                            if (this.dataGridViewApply.Rows[intGrid].Cells["fstate"].Value!="")
                            intfstate = Convert.ToInt32(this.dataGridViewApply.Rows[intGrid].Cells["fstate"].Value.ToString());
                        }
                        catch
                        { }
                    }
                    //if (intfstate == 1)//已执行申请
                    //this.dataGridViewApply.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Red;
                    if (intfstate == 2)//已打印条码
                    {
                        this.dataGridViewApply.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Blue;
                    }
                    else if (intfstate == 3)//已打印条码
                    {
                        this.dataGridViewApply.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Red;
                    }
                    //修改此部分的代码，基本逻辑没有变化 20150624 wjz end
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            /*
             	70	样本状态	0		已作废申请	0	1	
	71	样本状态	1		已执行申请	1	1	
	72	样本状态	2		已打印条码	1	1	
	73	样本状态	3		已采集	1	1	
	74	样本状态	4		已接收	1	1	
             */
        }
        /// <summary>
        /// 导出Excel
        /// </summary>
        private void WWExcelOutApply()
        {
            try
            {
                this.Validate();
                this.bindingSourceApply.EndEdit();
                if (this.dataGridViewApply.Rows.Count > 0)
                {
                    //changed by wjz 20150520 将旧的导出方式替换为NPOI的实现方式 ▽
                    //excelhelp.GridViewToExcel(this.dataGridViewApply, "", 1, 1);
                    ExcelHelper.DataGridViewToExcel(this.dataGridViewApply);
                    //changed by wjz 20150520 将旧的导出方式替换为NPOI的实现方式 △
                }
                else
                {
                    WWMessage.MessageShowWarning("暂无记录,不可导出!");
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void WWExcelOutResult()
        {
            try
            {
                this.Validate();
                this.dataGridViewItem.EndEdit();
                if (this.dataGridViewItem.Rows.Count > 0)
                {
                    //changed by wjz 20150520 将旧的导出方式替换为NPOI的实现方式 ▽
                    //excelhelp.GridViewToExcel(this.dataGridViewItem, "", 1, 1);
                    ExcelHelper.DataGridViewToExcel(this.dataGridViewItem);
                    //changed by wjz 20150520 将旧的导出方式替换为NPOI的实现方式 △
                }
                else
                {
                    WWMessage.MessageShowWarning("暂无记录,不可导出!");
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 快捷键 设置
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref   System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {

            if (keyData == (Keys.F8))
            {
                toolStripButtonQuery.PerformClick();
                return true;
            }
            if (keyData == (Keys.F1))
            {
                toolStripButtonHelp.PerformClick();
                return true;
            }
            if (keyData == (Keys.F3))
            {
                toolStripButton1.PerformClick();
                return true;
            }
            return base.ProcessCmdKey(ref   msg, keyData);
        }
        #endregion

        private void toolStripButtonHelp_Click(object sender, EventArgs e)
        {
            try
            {
                HelpForm help = new HelpForm(this.strHelpID);
                help.Show();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 关闭ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripMenuItem_applygs_Click(object sender, EventArgs e)
        {
            try
            {
                showApply.ShowDialog();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }


        private void 帮助ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                HelpForm help = new HelpForm(strHelpID);
                help.Show();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }


        private void bindingSourceApply_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                DataRowView rowCurrent = (DataRowView)this.bindingSourceApply.Current;
                if (this.dtResult.Rows.Count > 0)
                    this.dtResult.Clear();
                if (rowCurrent != null)
                {
                    string strfapply_id = rowCurrent["fapply_id"].ToString();

                    str申请单id = strfapply_id;
                    //WWGetResultList(strfapply_id); //20150625 wjz del
                }

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonQuery_Click(object sender, EventArgs e)
        {
            WWGetApplyList();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            WWExcelOutApply();
        }

        private void 申请格式ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                showApply.ShowDialog();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 项目模式ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                showResult.ShowDialog();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 申请导出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WWExcelOutApply();
        }

        private void 项目导出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WWExcelOutResult();
        }

        private void toolStripButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridViewApply_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void dataGridViewItem_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            ToolStripButton but = (ToolStripButton)sender;
            try
            {
                str申请单id = dataGridViewApply.CurrentRow.Cells["fapply_id"].Value.ToString();
                string s分类 = dataGridViewApply.CurrentRow.Cells["分类名称"].Value.ToString();
                string s住院号 = dataGridViewApply.CurrentRow.Cells["fhz_zyh"].Value.ToString();
                string 检查项目 = dataGridViewApply.CurrentRow.Cells["fitem_group"].Value.ToString();
                if (string.IsNullOrEmpty(s分类))
                {
                    //如果分类是空的则不处理，使用当前行的申请单id和检查项目，否则重新根据分类获取、、2015-11-9 09:52:41 yufh添加
                    //如果分类是空的情况下，不进行任何操作
                }
                else
                {
                    //按照住院号和检验分类进行筛选数据，组织打印项目
                    DataRow[] drs = dtMain.Select("fhz_zyh='" + s住院号 + "' and 分类名称='" + s分类 + "'", "fapply_id asc");
                    //重新排序后，取第一个申请单号作为lis申请单号，这样就可以不用重新生成
                    str申请单id = drs[0]["fapply_id"].ToString();
                    检查项目 = "";
                    foreach (DataRow dr in drs)
                        检查项目 += dr["fitem_group"].ToString() + ",";
                    检查项目 = 检查项目.Substring(0, 检查项目.Length - 1);
                }
                if (!(string.IsNullOrWhiteSpace(str申请单id)))
                {
                    int index = dataGridViewApply.CurrentRow.Index;
                    //DataGridViewRow row = this.dataGridViewApply.CurrentRow;
                    //string s申请单号 = dataGridViewApply.Rows[index].Cells["fapply_id"].Value.ToString();// rowCurrent["fhz_zyh"].ToString();
                    string s姓名 = dataGridViewApply.Rows[index].Cells["fname"].Value.ToString(); //rowCurrent["fname"].ToString();
                    string s性别 = dataGridViewApply.Rows[index].Cells["fsex"].Value.ToString();
                    string s年龄 = dataGridViewApply.Rows[index].Cells["fage"].Value.ToString();
                    string s类型 = dataGridViewApply.Rows[index].Cells["ftype_id"].Value.ToString();
                    //string s检查 = dataGridViewApply.Rows[index].Cells["fitem_group"].Value.ToString();
                    string s申请医生 = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn, "SELECT fname FROM WWF_PERSON where fperson_id='" +
                        dataGridViewApply.Rows[index].Cells["req_docno"].Value.ToString() + "'");
                    try
                    {
                        if (but.Name == "toolStripButton打印预览")
                        {
                            Print(true, s姓名, s性别, s年龄, s类型, 检查项目, str申请单id, s申请医生, false);
                        }
                        else if (but.Name == "toolStripButtonPrintTwo")
                        {
                            Print(false, s姓名, s性别, s年龄, s类型, 检查项目, str申请单id, s申请医生, true);
                        }
                        else
                        {
                            Print(false, s姓名, s性别, s年龄, s类型, 检查项目, str申请单id, s申请医生, false);
                        }

                        //add by wjz 打印后刷新查询页面▽
                        WWGetApplyList();
                        //add by wjz 打印后刷新查询页面△

                    }
                    catch (Exception ex)
                    {
                        WWMessage.MessageShowWarning("打印失败！" + ex.Message);
                    }
                }
                else
                {
                    WWMessage.MessageShowWarning("所要打印的申请项目没有申请号。");
                }
            }
            catch(Exception ex)
            {
                WWMessage.MessageShowWarning("打印过程出现异常，异常信息如下：\n"+ex.Message);
            }
        }

        private void btn查询_Click(object sender, EventArgs e)
        {
            WWGetApplyList();
        }

        private void btn生成条码_Click(object sender, EventArgs e)
        {
            try
            {
                SaveHisApply();
            }
            catch(Exception ex)
            {
                WWMessage.MessageShowWarning("打印过程出现异常，异常信息如下：\n" + ex.Message);
            }
            //DataGridViewRow row = this.dataGridViewApply.CurrentRow;
            //if (row != null && (string.IsNullOrWhiteSpace(row.Cells["fapply_id"].Value.ToString())))
            //{
            //    SaveApply();
            //}
            //else
            //{
            //    WWMessage.MessageShowWarning("条码不能重复生成。");
            //}

            try
            {
                //刷新
                WWGetApplyList();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowWarning("刷新列表时出现异常，异常信息如下：\n" + ex.Message);
            }
        }

        /// <summary>
        /// 保存申请单，生成条码号
        /// </summary>
        private void SaveApply()
        {
            DataGridViewRow row = this.dataGridViewApply.CurrentRow;
            int selectedIndex = this.dataGridViewApply.CurrentRow.Index;

            if (row == null)
            {
                WWMessage.MessageShowWarning("没有选中任何行");
                return;
            }
            //string hisRecordid = row.Cells["his_recordid"].Value.ToString();
            //DataRow[] SelectedRows = dtMain.Select("his_recordid='"+hisRecordid+"'");
            DataRow SelectedRow = dtMain.Rows[selectedIndex];

            if (SelectedRow == null)
            {
                WWMessage.MessageShowWarning("操作数据出现异常。");
                return;
            }
            string str病人类型 = SelectedRow["ftype_id"].ToString();

            //string strTime = this.bllApply.DbServerDateTim();
            ApplyModel applyModel = new ApplyModel();

            //this.str申请单id = this.bllApply.DbNum("sam_fapply_id"); //需要修改
            this.str申请单id = this.bllApply.Get最新申请单号(str病人类型, DateTime.Now.ToString("yyMMdd"));

            //申请单号：使用存储过程生成的申请单号
            applyModel.fapply_id = this.str申请单id;
            applyModel.His申请单号 = SelectedRow["apply_id"].ToString();

            //barcodeNETWindows1.BarcodeText = str申请单id;
            //applyModel.fjytype_id = strfcheck_type_id;
            applyModel.fjytype_id = "";//暂时设成空值

            //applyModel.fsample_type_id = strfsample_type_id;
            applyModel.fsample_type_id = "";//暂时设成空值

            //if (radioButtonJZ.Checked)
            //    applyModel.fjz_flag = 2;//急诊否
            //else
            //    applyModel.fjz_flag = 1;
            applyModel.fjz_flag = 1;

            applyModel.fcharge_flag = 0;//收费否
            applyModel.fstate = "1";//状态
            //applyModel.fapply_user_id = row.Cells["req_docno"].ToString();
            applyModel.fapply_user_id = SelectedRow["req_docno"].ToString();

            //applyModel.fapply_dept_id = this.tbfdept_id.Text;
            //applyModel.fapply_dept_id = row.Cells["fapply_dept_id"].ToString();
            applyModel.fapply_dept_id = SelectedRow["fapply_dept_id"].ToString();

            //applyModel.fapply_time = strTime;
            //applyModel.fapply_time = row.Cells["fapply_time"].ToString();
            applyModel.fapply_time = SelectedRow["fapply_time"].ToString();

            //applyModel.ftype_id = this.cbftype_id.SelectedValue.ToString();
            applyModel.ftype_id = str病人类型;

            //applyModel.fhz_id = this.tbfhz_id.Text;
            applyModel.fhz_id = SelectedRow["fhz_id"].ToString();

            //applyModel.fhz_zyh = this.tbfhz_zyh.Text;
            applyModel.fhz_zyh = SelectedRow["fhz_zyh"].ToString();

            //applyModel.fsex = this.cbfsex.SelectedItem.ToString();
            applyModel.fsex = SelectedRow["fsex"].ToString();

            //applyModel.fname = this.tbfname.Text;
            applyModel.fname = SelectedRow["fname"].ToString();

            //applyModel.fage = Convert.ToInt32(this.tbfage.Text);
            applyModel.fage = Convert.ToInt32(SelectedRow["fage"].ToString());

            //applyModel.fage_unit = this.cbfage_unit.SelectedItem.ToString();
            applyModel.fage_unit = "岁";

            //applyModel.froom_num = this.tbfroom_num.Text;
            applyModel.froom_num = "";

            //applyModel.fbed_num = this.tbfbed_num.Text;
            applyModel.fbed_num = SelectedRow["fbed_num"].ToString();

            //applyModel.fdiagnose = this.tbfdiagnose.Text;
            applyModel.fdiagnose = SelectedRow["fdiagnose"].ToString();
            applyModel.fcreate_user_id = LoginBLL.strPersonID;
            //applyModel.fcreate_time = strTime;
            applyModel.fupdate_user_id = LoginBLL.strPersonID;
            //applyModel.fupdate_time = strTime;
            //
            applyModel.fxhdb = 0;
            applyModel.fxyld = 0;
            applyModel.fheat = 0;
            applyModel.fage_day = 0;
            applyModel.fage_month = 0;
            applyModel.fage_year = 0;
            applyModel.fjyf = 0;//检验费

            //add by wjz 20160122 添加身份证号 ▽
            applyModel.Sfzh = SelectedRow["sfzh"].ToString();
            //add by wjz 20160122 添加身份证号 △
            

            applyModel.F检验项目名称 = SelectedRow["fitem_group"].ToString();


            string strSave = this.bllApply.BllApplyAdd_New(applyModel);
            if (strSave == "true")
            {
                ////
                ////保存检验申请条码号    
                ////  dr["FBarCodeImg"] = barcodeNETWindows1.GetBarcodeBitmap(BarcodeNETWorkShop.Core.FILE_FORMAT.JPG); ;//条形码  
                //int strSaveBCode = this.bllApply.BllApplyBCodeAdd(str申请单id, barcodeNETWindows1.GetBarcodeBitmap(BarcodeNETWorkShop.Core.FILE_FORMAT.JPG)); //条形码  );

                //DelOKItem(0);

                SelectedRow["fapply_id"] = applyModel.fapply_id;
                SelectedRow["fstate"] = applyModel.fstate;
            }
            else
            {
                WWMessage.MessageShowError(strSave);
            }
        }

        private void Print(bool 是否预览, string s姓名, string s性别, string s年龄, string s类型, string s检查, string s申请单号, string s申请医生, bool btwo)//btwo 是否需要判断打印两份
        {
            #region add by wjz 修改此方法的实现，化验项目如果在一个条码纸上打不开的话，可以用分页打印 ▽
            #region old code
            //TableXReport xtr = new TableXReport();

            //Font ft = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            //Font ft7 = new System.Drawing.Font("宋体", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            //xtr.SetReportTitle("打印时间 " + System.DateTime.Now.ToString(), true, ft, TextAlignment.TopLeft, Size.Empty);
            //xtr.SetReportTitle(s姓名 + "/" + s性别 + "/" + s年龄 + "岁  " + s类型 + " " + s申请医生, true, ft, TextAlignment.TopLeft, Size.Empty);

            ////xtr.SetReportTitle(s检查, true, ft, TextAlignment.TopLeft, Size.Empty);
            //int size = 20;
            //if (s检查.Length > size)
            //{
            //    xtr.SetReportTitle(s检查.Substring(0, size), true, ft7, TextAlignment.TopLeft, Size.Empty);
            //    xtr.SetReportTitle(s检查.Substring(size), true, ft7, TextAlignment.TopLeft, Size.Empty);
            //}
            //else
            //{
            //    xtr.SetReportTitle(s检查, true, ft7, TextAlignment.TopLeft, Size.Empty);
            //}

            //xtr.SetReportBarCode(s申请单号, new Point(5, 150), new Size(400, 125), null, true);
            //xtr.SetReportMain(0, 0, 1, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(20, 20, 10, 10), System.Drawing.Printing.PaperKind.Custom, new Size(500, 315), 0, 0, "");
            ////xtr.UnitAfterPrint += new XReportUnitAfterPrint(xtr_UnitAfterPrint);
            ////xtr.Print("ZDesigner GK888t");
            #endregion

            Font ft = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            Font ft7 = new System.Drawing.Font("宋体", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            int size = 20;

            //add by wjz 20160122 添加检查项目的排序 ▽
            string[] sub检查项目 = s检查.Split(',');
            if (sub检查项目.Length == 1)
            { }
            else
            {
                Array.Sort(sub检查项目);
                s检查 = sub检查项目[0];
                for(int index = 1; index < sub检查项目.Length;index++)
                {
                    s检查 += "," + sub检查项目[index];
                }
            }
            //add by wjz 20160122 添加检查项目的排序 △

            TableXReport xtr = new TableXReport();

            if (s检查.Length < 2 * size)
            {
                xtr.SetReportTitle("打印时间 " + System.DateTime.Now.ToString(), true, ft, TextAlignment.TopLeft, Size.Empty);
                xtr.SetReportTitle(s姓名 + "/" + s性别 + "/" + s年龄 + "岁  " + s类型 + " " + s申请医生, true, ft, TextAlignment.TopLeft, Size.Empty);


                if (s检查.Length > size)
                {
                    xtr.SetReportTitle(s检查.Substring(0, size), true, ft7, TextAlignment.TopLeft, Size.Empty);
                    xtr.SetReportTitle(s检查.Substring(size), true, ft7, TextAlignment.TopLeft, Size.Empty);
                }
                else
                {
                    xtr.SetReportTitle(s检查, true, ft7, TextAlignment.TopLeft, Size.Empty);
                }

                xtr.SetReportBarCode(s申请单号, new Point(5, 150), new Size(400, 125), null, true);
                xtr.SetReportMain(0, 0, 1, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(10, 10, 10, 10), System.Drawing.Printing.PaperKind.Custom, new Size(500, 315), 0, 0, "");
            }
            else
            {
                string[] arr检查 = s检查.Split(',');
                List<string> list检验 = new List<string>();

                list检验.Add(arr检查[0]);
                for(int index = 1; index < arr检查.Length; index++)
                {
                    if (list检验[list检验.Count - 1].Length + arr检查[index].Length < size * 2)
                    {
                        list检验[list检验.Count - 1] = list检验[list检验.Count - 1] + ","+ arr检查[index];
                    }
                    else
                    {
                        list检验.Add(arr检查[index]);
                    }
                }

                xtr.SetReportTitle("打印时间 " + System.DateTime.Now.ToString(), true, ft, TextAlignment.TopLeft, Size.Empty);
                xtr.SetReportTitle(s姓名 + "/" + s性别 + "/" + s年龄 + "岁  " + s类型 + " " + s申请医生, true, ft, TextAlignment.TopLeft, Size.Empty);


                if (list检验[0].Length > size)
                {
                    xtr.SetReportTitle(list检验[0].Substring(0, size), true, ft7, TextAlignment.TopLeft, Size.Empty);
                    xtr.SetReportTitle(list检验[0].Substring(size), true, ft7, TextAlignment.TopLeft, Size.Empty);
                }
                else
                {
                    xtr.SetReportTitle(list检验[0], true, ft7, TextAlignment.TopLeft, Size.Empty);
                }

                xtr.SetReportBarCode(s申请单号, new Point(5, 150), new Size(400, 125), null, true);
                xtr.SetReportMain(0, 0, 1, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(10, 10, 10, 10), System.Drawing.Printing.PaperKind.Custom, new Size(500, 315), 0, 0, "");
                
                xtr.CreateDocument();

                for (int index = 1; index < list检验.Count; index++ )
                {
                    CombineReports(xtr, ft, ft7, s姓名, s性别, s年龄, s类型, list检验[index], s申请单号, s申请医生);
                }

                xtr.PrintingSystem.ContinuousPageNumbering = true;
            }
            #endregion

            xtr.PrintProgress += new PrintProgressEventHandler(xtr_PrintProgress);

            if (是否预览)
            {
                #region 隐藏打印按钮
                //创建报表示例，指定到一个打印工具
                ReportPrintTool pt = new ReportPrintTool(xtr);

                // 获取打印工具的PrintingSystem
                PrintingSystemBase ps = pt.PrintingSystem;

                //// 隐藏Watermark 工具按钮和菜单项.
                //if (ps.GetCommandVisibility(PrintingSystemCommand.Watermark) != CommandVisibility.None)
                //{
                //    ps.SetCommandVisibility(PrintingSystemCommand.Watermark,CommandVisibility.None);                    
                //}
                //// 显示Document Map工具按钮和菜单项
                //ps.SetCommandVisibility(PrintingSystemCommand.DocumentMap, CommandVisibility.All);

                //隐藏打印按钮
                ps.SetCommandVisibility(new DevExpress.XtraPrinting.PrintingSystemCommand[] 
    { 
        DevExpress.XtraPrinting.PrintingSystemCommand.Background , 
        DevExpress.XtraPrinting.PrintingSystemCommand.ClosePreview ,
            DevExpress.XtraPrinting.PrintingSystemCommand.Customize ,
            DevExpress.XtraPrinting.PrintingSystemCommand.DocumentMap ,
            DevExpress.XtraPrinting.PrintingSystemCommand.File ,
            DevExpress.XtraPrinting.PrintingSystemCommand.FillBackground ,
            DevExpress.XtraPrinting.PrintingSystemCommand.Open ,
            DevExpress.XtraPrinting.PrintingSystemCommand.PageSetup ,
            //DevExpress.XtraPrinting.PrintingSystemCommand.Print ,
            //DevExpress.XtraPrinting.PrintingSystemCommand.PrintDirect ,
            DevExpress.XtraPrinting.PrintingSystemCommand.Save,
            DevExpress.XtraPrinting.PrintingSystemCommand.Watermark,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportCsv,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportFile,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportGraphic,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportHtm,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportMht,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportPdf,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportRtf,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportTxt,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportXls,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportXlsx,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportXps,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendCsv,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendFile,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendGraphic,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendMht,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendPdf,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendRtf,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendTxt,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendXls,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendXlsx,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendXps,
            DevExpress.XtraPrinting.PrintingSystemCommand.SubmitParameters
        }, CommandVisibility.None);

                #endregion
                //显示报表预览.
                pt.ShowPreview();
            }
            else if (btwo && bBarcodePrintOneMore)
            {
                if (PrinterExists(ww.form.Properties.Settings.Default.CodePrit))
                {
                    xtr.Print(ww.form.Properties.Settings.Default.CodePrit);
                    xtr.Print(ww.form.Properties.Settings.Default.CodePrit);
                }
                else//兼容没有条码打印机时的操作
                {
                    xtr_PrintProgress(null, null);
                }
            }
            else
            {
                if (PrinterExists(ww.form.Properties.Settings.Default.CodePrit))
                {
                    xtr.Print(ww.form.Properties.Settings.Default.CodePrit);
                }
                else//兼容没有条码打印机时的操作
                {
                    xtr_PrintProgress(null, null);
                }
            }
        }

        //add by wjz 20151226 一个条码纸上可能无法将所有的项目全部打印出来，添加此方法的目的：实现分页打印 ▽
        private void CombineReports(TableXReport firstRep, Font ft9, Font ft7,
                                    string s姓名, string s性别, string s年龄, string s类型, string sub检查, string s申请单号, string s申请医生)
        {
            TableXReport subRep = new TableXReport(); 
            subRep.SetReportTitle("打印时间 " + System.DateTime.Now.ToString(), true, ft9, TextAlignment.TopLeft, Size.Empty);
            subRep.SetReportTitle(s姓名 + "/" + s性别 + "/" + s年龄 + "岁  " + s类型 + " " + s申请医生, true, ft9, TextAlignment.TopLeft, Size.Empty);

            //xtr.SetReportTitle(s检查, true, ft, TextAlignment.TopLeft, Size.Empty);
            int size = 20;
            if (sub检查.Length > size)
            {
                subRep.SetReportTitle(sub检查.Substring(0, size), true, ft7, TextAlignment.TopLeft, Size.Empty);
                subRep.SetReportTitle(sub检查.Substring(size), true, ft7, TextAlignment.TopLeft, Size.Empty);
            }
            else
            {
                subRep.SetReportTitle(sub检查, true, ft7, TextAlignment.TopLeft, Size.Empty);
            }

            subRep.SetReportBarCode(s申请单号, new Point(5, 150), new Size(400, 125), null, true);
            subRep.SetReportMain(0, 0, 1, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(10, 10, 10, 10), System.Drawing.Printing.PaperKind.Custom, new Size(500, 315), 0, 0, "");

            subRep.CreateDocument();

            firstRep.Pages.AddRange(subRep.Pages);
        }
        //add by wjz 20151226 一个条码纸上可能无法将所有的项目全部打印出来，添加此方法的目的：实现分页打印 △

        private void xtr_PrintProgress(object sender, PrintProgressEventArgs e)
        {
            #region HIS负责条码的生成，LIS段负责打印条码后，将申请信息写入LIS数据库，这部分代码是新增的
            string strUpdate = SaveHisApply();
            //更新显示状态
            DataGridViewRow row = this.dataGridViewApply.CurrentRow;
            if (strUpdate.Equals("true") && row != null)
            {
                //this.dtMain.Rows[applySelectedIndex]["fstate"] = 2;
                row.Cells["fstate"].Value = "2";
                row.DefaultCellStyle.ForeColor = Color.Blue;
            }
            #endregion
        }

        /// <summary>
        /// 此方法负责保存从HIS中取出的化验申请，申请条码的生成由HIS段负责（Lis段不再操作申请条码）
        /// 2015-08-20 18:49:48 yufh 调整按照分类进行生成条码，打印并保存
        /// </summary>
        private string SaveHisApply()
        {
            DataGridViewRow row = this.dataGridViewApply.CurrentRow;
            int selectedIndex = this.dataGridViewApply.CurrentRow.Index;

            if (row == null)
            {
                WWMessage.MessageShowWarning("没有选中任何行");
                return "false";
            }
            //string hisRecordid = row.Cells["his_recordid"].Value.ToString();         
            string s检查分类 = row.Cells["分类名称"].Value.ToString();
            string s住院号 = row.Cells["fhz_zyh"].Value.ToString();

            //changed by wjz 20151109 条码打印时，分类名称是空白的项目，回打到一个条码上，修改此bug ▽
            #region old code
            //DataRow[] drs = dtMain.Select("fhz_zyh='" + s住院号 + "' and 分类名称='" + s检查分类 + "'");
            #endregion
            string strID = row.Cells["fapply_id"].Value.ToString();

            //
            DataRow[] drs;
            if (s检查分类 == null || s检查分类 == "")
            {
                drs = dtMain.Select("fapply_id='" + strID + "'");
            }
            else
            {
                drs = dtMain.Select("fhz_zyh='" + s住院号 + "' and 分类名称='" + s检查分类 + "'");
            }
            //changed by wjz 20151109 条码打印时，分类名称是空白的项目，回打到一个条码上，修改此bug △

            this.str申请单id = drs[0]["fapply_id"].ToString();

            //循环保存单据
            string 执行状态 = "false";
            foreach (DataRow dr in drs)
            {
                DataRow SelectedRow = dr;//dtMain.Rows[selectedIndex];

                if (SelectedRow == null)
                {
                    WWMessage.MessageShowWarning("操作数据出现异常。");
                    return "false";
                }

                //验证Lis数据库中是不是有这个申请单号的信息，如果有，则不再向LIS数据库中插入这条申请的信息 add 20150630 wjz
                DataTable dtFromLis = this.bllApply.BllSamplingMainDTFromLis("fapply_id='" + SelectedRow["fapply_id"].ToString() + "'");
                if (dtFromLis != null && dtFromLis.Rows.Count > 0)
                {
                    //return "false";
                    continue;
                }
                
                //string strTime = this.bllApply.DbServerDateTim();
                ApplyModel applyModel = new ApplyModel();
                                
                //申请单号：使用存储过程生成的申请单号
                applyModel.fapply_id = SelectedRow["fapply_id"].ToString();

                //判断分类是否为空！如果是空（没有分类的情况）则需要单独生成一个申请 2015-11-9 09:44:38 yufh 添加
                if (s检查分类 == "" || s检查分类 == null)
                    applyModel.His申请单号 = SelectedRow["fapply_id"].ToString();
                else
                    applyModel.His申请单号 = this.str申请单id;

                applyModel.fjytype_id = "";//暂时设成空值

                applyModel.fsample_type_id = "";//暂时设成空值
                                
                applyModel.fjz_flag = 1; //急诊否

                applyModel.fcharge_flag = 0;//收费否
                applyModel.fstate = "2";//状态
                applyModel.fapply_user_id = SelectedRow["req_docno"].ToString();

                applyModel.fapply_dept_id = SelectedRow["fapply_dept_id"].ToString(); //申请部门

                applyModel.fapply_time = SelectedRow["fapply_time"].ToString(); //申请时间

                applyModel.ftype_id = SelectedRow["ftype_id"].ToString(); //病人类型

                applyModel.fhz_id = SelectedRow["fhz_id"].ToString(); //患者id

                applyModel.fhz_zyh = SelectedRow["fhz_zyh"].ToString(); //住院号

                applyModel.fsex = SelectedRow["fsex"].ToString(); //性别

                applyModel.fname = SelectedRow["fname"].ToString(); //姓名

                //add by wjz 20160122 添加身份证号 ▽
                applyModel.Sfzh = SelectedRow["sfzh"].ToString();
                //add by wjz 20160122 添加身份证号 △

                //===================2019年11月12日 yfh 添加电子健康卡号=====================//
                if (dtMain.Columns.Contains("card_no"))
                {
                    applyModel.card_no = SelectedRow["card_no"].ToString();
                }
                //===================2019年11月12日 yfh 添加电子健康卡号=====================//

                //changed by wjz 20160122 修改年龄的计算方式 ▽
                //applyModel.fage = Convert.ToInt32(SelectedRow["fage"]); //年龄
                //applyModel.fage_unit = "岁";

                try
                {
                    if (string.IsNullOrWhiteSpace(applyModel.Sfzh) || applyModel.Sfzh.Length != 18)
                    {
                        applyModel.fage = Convert.ToInt32(SelectedRow["fage"]); //年龄
                        applyModel.fage_unit = SelectedRow["fage_unit"].ToString();
                        if(string.IsNullOrWhiteSpace(applyModel.fage_unit))//如果没有取到年龄，则默认为“岁”
                        {
                            applyModel.fage_unit = "岁";
                        }
                    }
                    else
                    {
                        string birthday = applyModel.Sfzh.Substring(6, 4) + "-" 
                                        + applyModel.Sfzh.Substring(10, 2) + "-" 
                                        + applyModel.Sfzh.Substring(12, 2);

                        string ages = this.bllPatient.BllPatientAge(birthday);
                        string[] arrage = ages.Split(':');

                        applyModel.fage = Convert.ToInt32(arrage[1]);
                        applyModel.fage_unit = arrage[0];
                    }
                }
                catch
                {
                    applyModel.fage = Convert.ToInt32(SelectedRow["fage"]); //年龄
                    applyModel.fage_unit = SelectedRow["fage_unit"].ToString();
                    if (string.IsNullOrWhiteSpace(applyModel.fage_unit))//如果没有取到年龄，则默认为“岁”
                    {
                        applyModel.fage_unit = "岁";
                    }
                }
                //changed by wjz 20160122 修改年龄的计算方式 △
                //岁	1
                //月	2
                //天	3

                applyModel.froom_num = "";

                applyModel.fbed_num = SelectedRow["fbed_num"].ToString();

                applyModel.fdiagnose = SelectedRow["fdiagnose"].ToString();
                applyModel.fcreate_user_id = LoginBLL.strPersonID;
                //applyModel.fcreate_time = strTime;
                applyModel.fupdate_user_id = LoginBLL.strPersonID;
                //applyModel.fupdate_time = strTime;
                //
                applyModel.fxhdb = 0;
                applyModel.fxyld = 0;
                applyModel.fheat = 0;
                applyModel.fage_day = 0;
                applyModel.fage_month = 0;
                applyModel.fage_year = 0;
                applyModel.fjyf = 0;//检验费
                
                applyModel.F检验项目名称 = SelectedRow["fitem_group"].ToString();
                applyModel.fremark = SelectedRow["分类名称"].ToString();


                string strSave = this.bllApply.BllApplyAdd_New(applyModel);
                if (strSave == "true")
                {
                    执行状态 = strSave;
                }
            }
            return 执行状态;
        }


        #region"合并单元格的测试"
        private int? nextrow = null;
        private int? nextcol = null;
        private void dataGridView1_CellFormatting(object sender, System.Windows.Forms.DataGridViewCellFormattingEventArgs e)
        {
            if (this.dataGridViewApply.Columns["分类名称"].Index == e.ColumnIndex && e.RowIndex >= 0)
            {
                if (this.nextcol != null & e.ColumnIndex == this.nextcol)
                {
                    e.CellStyle.BackColor = Color.LightBlue;
                    this.nextcol = null;
                }
                if (this.nextrow != null & e.RowIndex == nextrow)
                {
                    e.CellStyle.BackColor = Color.LightPink;
                    this.nextrow = null;
                }
                if (e.RowIndex != this.dataGridViewApply.RowCount - 1)
                {
                    if (e.Value.ToString() == this.dataGridViewApply.Rows[e.RowIndex + 1].Cells[e.ColumnIndex].Value.ToString())
                    {
                        e.CellStyle.BackColor = Color.LightPink;
                        nextrow = e.RowIndex + 1;
                    }
                }

            }
            if (this.dataGridViewApply.Columns["name"].Index == e.ColumnIndex && e.RowIndex >= 0)
            {
                if (e.Value.ToString() == this.dataGridViewApply.Rows[e.RowIndex].Cells[e.ColumnIndex + 1].Value.ToString())
                {
                    e.CellStyle.BackColor = Color.LightBlue;
                    nextcol = e.ColumnIndex + 1;
                }
            }
        }
        //==========================

        //绘制单元格
        private void dataGridView1_CellPainting(object sender, System.Windows.Forms.DataGridViewCellPaintingEventArgs e)
        {
            #region 纵向合并
            //纵向合并
            if (this.dataGridViewApply.Columns["分类名称"].Index == e.ColumnIndex && e.RowIndex >= 0)
            {
                using (
                    Brush gridBrush = new SolidBrush(this.dataGridViewApply.GridColor),
                    backColorBrush = new SolidBrush(e.CellStyle.BackColor))
                {
                    using (Pen gridLinePen = new Pen(gridBrush))
                    {
                        // 擦除原单元格背景
                        e.Graphics.FillRectangle(backColorBrush, e.CellBounds);
                        ////绘制线条,这些线条是单元格相互间隔的区分线条,
                        ////因为我们只对列name做处理,所以datagridview自己会处理左侧和上边缘的线条
                        if (e.RowIndex != this.dataGridViewApply.RowCount - 1)
                        {
                            if (e.Value.ToString() != this.dataGridViewApply.Rows[e.RowIndex + 1].Cells[e.ColumnIndex].Value.ToString())
                            {

                                e.Graphics.DrawLine(gridLinePen, e.CellBounds.Left, e.CellBounds.Bottom - 1,
                                e.CellBounds.Right - 1, e.CellBounds.Bottom - 1);//下边缘的线
                                //绘制值
                                if (e.Value != null)
                                {
                                    e.Graphics.DrawString((String)e.Value, e.CellStyle.Font,
                                        Brushes.Crimson, e.CellBounds.X + 2,
                                        e.CellBounds.Y + 2, StringFormat.GenericDefault);
                                }
                            }
                        }
                        else
                        {
                            e.Graphics.DrawLine(gridLinePen, e.CellBounds.Left, e.CellBounds.Bottom - 1,
                                e.CellBounds.Right - 1, e.CellBounds.Bottom - 1);//下边缘的线
                            //绘制值
                            if (e.Value != null)
                            {
                                e.Graphics.DrawString((String)e.Value, e.CellStyle.Font,
                                    Brushes.Crimson, e.CellBounds.X + 2,
                                    e.CellBounds.Y + 2, StringFormat.GenericDefault);
                            }
                        }
                        //右侧的线
                        e.Graphics.DrawLine(gridLinePen, e.CellBounds.Right - 1,
                            e.CellBounds.Top, e.CellBounds.Right - 1,
                            e.CellBounds.Bottom - 1);

                        e.Handled = true;
                    }
                }
            } 
            #endregion

            #region 横向合并
            ////横向合并
            //if (this.dataGridViewApply.Columns["name"].Index == e.ColumnIndex && e.RowIndex >= 0)
            //{

            //    using (
            //        Brush gridBrush = new SolidBrush(this.dataGridViewApply.GridColor),
            //        backColorBrush = new SolidBrush(e.CellStyle.BackColor))
            //    {
            //        using (Pen gridLinePen = new Pen(gridBrush))
            //        {
            //            // 擦除原单元格背景
            //            e.Graphics.FillRectangle(backColorBrush, e.CellBounds);

            //            if (e.Value.ToString() != this.dataGridViewApply.Rows[e.RowIndex].Cells[e.ColumnIndex + 1].Value.ToString())
            //            {

            //                //右侧的线
            //                e.Graphics.DrawLine(gridLinePen, e.CellBounds.Right - 1, e.CellBounds.Top,
            //                    e.CellBounds.Right - 1, e.CellBounds.Bottom - 1);
            //                //绘制值
            //                if (e.Value != null)
            //                {
            //                    e.Graphics.DrawString((String)e.Value, e.CellStyle.Font,
            //                        Brushes.Crimson, e.CellBounds.X + 2,
            //                        e.CellBounds.Y + 2, StringFormat.GenericDefault);
            //                }
            //            }

            //            //下边缘的线
            //            e.Graphics.DrawLine(gridLinePen, e.CellBounds.Left, e.CellBounds.Bottom - 1,
            //                                        e.CellBounds.Right - 1, e.CellBounds.Bottom - 1);
            //            e.Handled = true;
            //        }
            //    }
            //} 
            #endregion

        }
        
        #endregion

        private void textBox门诊住院号_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == (char)Keys.Enter)
            {
                WWGetApplyList();
            }
        }

        //add by wjz 20160504 打印时将该病人所有的化验项目都打印出来 ▽
        private void toolStripButtonPrintAll_Click(object sender, EventArgs e)
        {
            PrintAll(false);
        }

        private void PrintAll(bool b是否预览)
        {
            if (dataGridViewApply.CurrentRow == null)
            {
                WWMessage.MessageShowWarning("没有选择任何行！");
                return;
            }

            #region 获取病人基本信息
            string s住院号 = dataGridViewApply.CurrentRow.Cells["fhz_zyh"].Value.ToString();
            string s姓名 = dataGridViewApply.CurrentRow.Cells["fname"].Value.ToString();
            string s性别 = dataGridViewApply.CurrentRow.Cells["fsex"].Value.ToString();
            string s年龄 = dataGridViewApply.CurrentRow.Cells["fage"].Value.ToString();
            string s类型 = dataGridViewApply.CurrentRow.Cells["ftype_id"].Value.ToString();
            #endregion

            //获取医师列表的代码已经注释
            #region 获取医生列表
            DataTable dtDoctors = null;
            try
            {
                dtDoctors = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, "SELECT [fperson_id],fname FROM WWF_PERSON").Tables[0];
            }
            catch
            {
                //防止后续使用此DataTable时，程序报错。
                dtDoctors = new DataTable();
                dtDoctors.Columns.Add("fperson_id");
                dtDoctors.Columns.Add("fname");
            }
            #endregion

            DataRow[] drs = dtMain.Select("fhz_zyh='" + s住院号 + "'", "分类名称,fitem_group");
            //----List<string> applyidList = new List<string>();//目的：用于存放“fitem_group”不为空时的fapply_id。HIS异常时可能会生成化验项目为空的申请信息
            Dictionary<String, String> applyiddic = new Dictionary<string, string>();
            TableXReport report = new TableXReport();

            //add by wjz 20160620 为自助打印机添加条码打印 ▽
            if (s类型.Contains("门诊"))
            {
                AppendReport(ref report, s姓名, s性别, s年龄, s类型, "自助打印", drs[0]["fapply_id"].ToString(), s住院号, "");
            }
            //add by wjz 20160620 为自助打印机添加条码打印 △

            #region 目的：记录申请项目不为空的申请号,同时将申请项目按照“分类名称”进行汇总
            for (int index = 0; index < drs.Length; index++)
            {
                //检查项目是空的情况下，直接跳过此项
                if (string.IsNullOrWhiteSpace(drs[index]["fitem_group"].ToString()))
                {
                    continue;
                }

                #region 获取基本信息
                string s申请单号 = drs[index]["fapply_id"].ToString();
                string s检查 = drs[index]["fitem_group"].ToString();

                //applyidList.Add(drs[index]["fapply_id"].ToString());//记录fapply_id，方便记录申请单信息
                applyiddic.Add(s申请单号, s申请单号);

                //打印的条码中，不再显示医师姓名
                DataRow[] docs = dtDoctors.Select("fperson_id='" + drs[index]["req_docno"].ToString() + "'");
                string s申请医生 = docs.Length > 0 ? docs[0]["fname"].ToString() : "";
                #endregion

                //分类名称是空的，每个单独汇总
                if (string.IsNullOrWhiteSpace(drs[index]["分类名称"].ToString()))
                {
                    //追加报表，修改记录
                    AppendReport(ref report, s姓名, s性别, s年龄, s类型, s检查, s申请单号, s住院号,s申请医生);
                }
                else
                {   //分类不是空的，需要找到相同分类的化验项目
                    string s分类Temp = drs[index]["分类名称"].ToString();

                    #region 找到相同分类的项目
                    index++;
                    while (index < drs.Length)
                    {
                        if (s分类Temp == drs[index]["分类名称"].ToString())
                        {
                            s检查 += "," + drs[index]["fitem_group"].ToString();
                            //applyidList.Add(drs[index]["fapply_id"].ToString());
                            applyiddic.Add(drs[index]["fapply_id"].ToString(), s申请单号);
                        }
                        else
                        {
                            break;
                        }
                        index++;
                    }
                    index--;
                    #endregion

                    //追加报表，修改记录
                    AppendReport(ref report, s姓名, s性别, s年龄, s类型, s检查, s申请单号, s住院号, s申请医生);
                }
            }
            #endregion

            #region 打印操作
            bool bPrintSuccess = false;
            report.PrintProgress += (sender, e) => { bPrintSuccess = true; };

            if (b是否预览)
            {
                report.PrinterName = ww.form.Properties.Settings.Default.CodePrit;
                report.ShowPreviewDialog();
            }
            else
            {
                if (PrinterExists(ww.form.Properties.Settings.Default.CodePrit))
                {
                    report.Print(ww.form.Properties.Settings.Default.CodePrit);
                }
                else//兼容没有条码打印机时的操作
                {
                    bPrintSuccess = true;
                }
            }
            #endregion

            if (bPrintSuccess)
            {
                //applyidList记录了已经打印出来的检验项目的申请号。接下来的操作：根据申请号向LIS中写入申请信息
                foreach(var item in applyiddic)
                {
                    SaveHisApplyNew(item.Key, item.Value);
                }

                //刷新查询结果
                WWGetApplyList();
            }
            else
            {
                WWMessage.MessageShowWarning("打印失败！");
            }
        }


        //Print(bool 是否预览, string s姓名, string s性别, string s年龄, string s类型, string s检查, string s申请单号, string s申请医生)
        private void AppendReport(ref TableXReport report, string s姓名, string s性别, string s年龄, string s类型, string s检查, string s申请单号, string s住院号, string s申请医生)
        {
            TableXReport rep = GetSubReport(s姓名, s性别, s年龄, s类型, s检查, s申请单号, s住院号, s申请医生);

            report.Pages.AddRange(rep.Pages);
        }

        private TableXReport GetSubReport(string s姓名, string s性别, string s年龄, string s类型, string s检查, string s申请单号, string s住院号, string s申请医生)
        {
            Font ft = new System.Drawing.Font("宋体", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            Font ft7 = new System.Drawing.Font("宋体", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            int size = 20;

            TableXReport xtr = new TableXReport();

            if (s检查.Length < 2 * size)
            {
                SetFirstReport(ref xtr, ft, ft7, size, s姓名, s性别, s年龄, s类型, s检查, s申请单号, s住院号, s申请医生);
            }
            else
            {
                List<string> list检验 = new List<string>();

                SplitItems(ref list检验, s检查, size);

                SetFirstReport(ref xtr, ft, ft7, size, s姓名, s性别, s年龄, s类型, list检验[0], s申请单号, s住院号,s申请医生);

                for (int index = 1; index < list检验.Count; index++)
                {
                    CombineReportsNew(xtr, ft, ft7, size, s姓名, s性别, s年龄, s类型, list检验[index], s申请单号, s住院号,s申请医生);
                }
            }
            return xtr;
        }

        private void CombineReportsNew(TableXReport firstRep, Font ft9, Font ft7, int colMaxSize,
                                    string s姓名, string s性别, string s年龄, string s类型, string sub检查, string s申请单号, string s住院号, string s申请医生)
        {
            TableXReport subRep = new TableXReport();

            SetFirstReport(ref subRep, ft9, ft7, colMaxSize, s姓名, s性别, s年龄, s类型, sub检查, s申请单号, s住院号, s申请医生);

            firstRep.Pages.AddRange(subRep.Pages);
        }

        private void SplitItems(ref List<string> list检验, string s检查, int colMaxSize)
        {
            string[] arr检查 = s检查.Split(',');

            list检验.Add(arr检查[0]);
            for (int index = 1; index < arr检查.Length; index++)
            {
                if (list检验[list检验.Count - 1].Length + arr检查[index].Length < colMaxSize * 2)
                {
                    list检验[list检验.Count - 1] = list检验[list检验.Count - 1] + "," + arr检查[index];
                }
                else
                {
                    list检验.Add(arr检查[index]);
                }
            }
        }

        private void SetFirstReport(ref TableXReport xtr, Font ft, Font ft7, int colMaxSize, string s姓名, string s性别, string s年龄, string s类型, string s检查, string s申请单号, string s住院号, string s申请医生)
        {

            xtr.SetReportTitle("打印时间" + System.DateTime.Now.ToString() + " " + s申请医生, true, ft, TextAlignment.TopLeft, Size.Empty);
            xtr.SetReportTitle(s姓名 + "/" + s性别 + "/" + s年龄 + "岁 " + s类型 + s住院号, true, ft, TextAlignment.TopLeft, Size.Empty);


            if (s检查.Length > colMaxSize)
            {
                xtr.SetReportTitle(s检查.Substring(0, colMaxSize), true, ft7, TextAlignment.TopLeft, Size.Empty);
                xtr.SetReportTitle(s检查.Substring(colMaxSize), true, ft7, TextAlignment.TopLeft, Size.Empty);
            }
            else
            {
                xtr.SetReportTitle(s检查, true, ft7, TextAlignment.TopLeft, Size.Empty);
            }

            xtr.SetReportBarCode(s申请单号, new Point(5, 150), new Size(400, 125), null, true);
            xtr.SetReportMain(0, 0, 1, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(10, 10, 10, 10), System.Drawing.Printing.PaperKind.Custom, new Size(500, 315), 0, 0, "");

            xtr.CreateDocument();//如果不加这一行，report可能不能显示
        }

        private string SaveHisApplyNew(string strID, string str条码)
        {
            DataRow[] drs = dtMain.Select("fapply_id='" + strID + "'");

            //循环保存单据
            string 执行状态 = "false";

            DataRow SelectedRow;
            if (drs.Length > 0)
            {
                SelectedRow = drs[0];
            }
            else
            {
                return 执行状态;
            }

            //验证Lis数据库中是不是有这个申请单号的信息，如果有，则不再向LIS数据库中插入这条申请的信息 add 20150630 wjz
            DataTable dtFromLis = this.bllApply.BllSamplingMainDTFromLis("fapply_id='" + SelectedRow["fapply_id"].ToString() + "'");
            if (dtFromLis != null && dtFromLis.Rows.Count > 0)
            {
                return "false";
            }

            //string strTime = this.bllApply.DbServerDateTim();
            ApplyModel applyModel = new ApplyModel();

            //申请单号：使用存储过程生成的申请单号
            applyModel.fapply_id = SelectedRow["fapply_id"].ToString();

            //判断分类是否为空！如果是空（没有分类的情况）则需要单独生成一个申请 2015-11-9 09:44:38 yufh 添加
            //if (s检查分类 == "" || s检查分类 == null)
            //    applyModel.His申请单号 = SelectedRow["fapply_id"].ToString();
            //else
            //    applyModel.His申请单号 = this.str申请单id;
            applyModel.His申请单号 = str条码;

            applyModel.fjytype_id = "";//暂时设成空值

            applyModel.fsample_type_id = "";//暂时设成空值

            applyModel.fjz_flag = 1; //急诊否

            applyModel.fcharge_flag = 0;//收费否
            applyModel.fstate = "2";//状态
            applyModel.fapply_user_id = SelectedRow["req_docno"].ToString();

            applyModel.fapply_dept_id = SelectedRow["fapply_dept_id"].ToString(); //申请部门

            applyModel.fapply_time = SelectedRow["fapply_time"].ToString(); //申请时间

            applyModel.ftype_id = SelectedRow["ftype_id"].ToString(); //病人类型

            applyModel.fhz_id = SelectedRow["fhz_id"].ToString(); //患者id

            applyModel.fhz_zyh = SelectedRow["fhz_zyh"].ToString(); //住院号

            applyModel.fsex = SelectedRow["fsex"].ToString(); //性别

            applyModel.fname = SelectedRow["fname"].ToString(); //姓名

            //add by wjz 20160122 添加身份证号 ▽
            applyModel.Sfzh = SelectedRow["sfzh"].ToString();
            //add by wjz 20160122 添加身份证号 △

            //changed by wjz 20160122 修改年龄的计算方式 ▽
            //applyModel.fage = Convert.ToInt32(SelectedRow["fage"]); //年龄
            //applyModel.fage_unit = "岁";
            try
            {
                if (string.IsNullOrWhiteSpace(applyModel.Sfzh) || applyModel.Sfzh.Length != 18)
                {
                    applyModel.fage = Convert.ToInt32(SelectedRow["fage"]); //年龄
                    applyModel.fage_unit = SelectedRow["fage_unit"].ToString();
                    if (string.IsNullOrWhiteSpace(applyModel.fage_unit))//如果没有取到年龄，则默认为“岁”
                    {
                        applyModel.fage_unit = "岁";
                    }
                }
                else
                {
                    string birthday = applyModel.Sfzh.Substring(6, 4) + "-"
                                    + applyModel.Sfzh.Substring(10, 2) + "-"
                                    + applyModel.Sfzh.Substring(12, 2);

                    string ages = this.bllPatient.BllPatientAge(birthday);
                    string[] arrage = ages.Split(':');

                    applyModel.fage = Convert.ToInt32(arrage[1]);
                    applyModel.fage_unit = arrage[0];
                }
            }
            catch
            {
                applyModel.fage = Convert.ToInt32(SelectedRow["fage"]); //年龄
                applyModel.fage_unit = SelectedRow["fage_unit"].ToString();
                if (string.IsNullOrWhiteSpace(applyModel.fage_unit))//如果没有取到年龄，则默认为“岁”
                {
                    applyModel.fage_unit = "岁";
                }
            }
            //changed by wjz 20160122 修改年龄的计算方式 △
            //岁	1
            //月	2
            //天	3

            applyModel.froom_num = "";
            applyModel.fbed_num = SelectedRow["fbed_num"].ToString();

            applyModel.fdiagnose = SelectedRow["fdiagnose"].ToString();
            applyModel.fcreate_user_id = LoginBLL.strPersonID;
            //applyModel.fcreate_time = strTime;
            applyModel.fupdate_user_id = LoginBLL.strPersonID;
            //applyModel.fupdate_time = strTime;
            //
            applyModel.fxhdb = 0;
            applyModel.fxyld = 0;
            applyModel.fheat = 0;
            applyModel.fage_day = 0;
            applyModel.fage_month = 0;
            applyModel.fage_year = 0;
            applyModel.fjyf = 0;//检验费

            applyModel.F检验项目名称 = SelectedRow["fitem_group"].ToString();
            applyModel.fremark = SelectedRow["分类名称"].ToString();

            string strSave = this.bllApply.BllApplyAdd_New(applyModel);
            if (strSave == "true")
            {
                执行状态 = strSave;
            }

            return 执行状态;
        }
        //add by wjz 20160504 打印时将该病人所有的化验项目都打印出来 △

        //add by wjz 20160427 为杨庄医院添加读社保卡的功能 ▽
        private void btnReadCardAndQuery_Click(object sender, EventArgs e)
        {
            WWGetApplyListNew();
        }

        //此方法的逻辑与WWGetApplyList中的基本相同
        private void WWGetApplyListNew()
        {
            //add by wjz 20160428 添加查询提示，提示是否查到了数据 ▽
            labelNotify.ForeColor = Color.Black;
            //add by wjz 20160428 添加查询提示，提示是否查到了数据 △

            ClassYBJK.personYB person = new ClassYBJK.personYB();
            ClassYBJK.ybBusiness bus = new ClassYBJK.ybBusiness();
            try
            {
                if (bus.readIC芯片(ref person) == false)
                {
                    MessageBox.Show("读取社保卡卡中基本信息失败" + person.sbErrInfo.ToString(), "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.IsNullOrWhiteSpace(person.S姓名) || string.IsNullOrWhiteSpace(person.S身份证号))
                {
                    MessageBox.Show("未读取到社保卡信息");
                    return;
                }

                textBoxName.Text = person.S姓名;
                textBoxSFZH.Text = person.S身份证号;
            }
            catch(Exception ex)
            {
                MessageBox.Show("读取社保卡卡中基本信息失败\n异常信息："+ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            try
            {
                dtMain = new DataTable();
                string fjy_date1 = this.fjy_dateDateTimePicker1.Value.ToString("yyyy-MM-dd");
                string fjy_date2 = this.fjy_dateDateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59");
                //string fjy_病人类型 = this.comboBox病人类型.SelectedValue.ToString();

                //20150629 add wjz
                string fjy_门诊住院号 = this.textBox门诊住院号.Text;

                //add by wjz 20160125 修改查询条件，如果门诊主要号输入的数据是四位，则将其转换为门诊号 ▽
                //del by wjz 20160427 由于夏蔚医院的住院号是4位，在这里删除对门诊住院号的判断 ▽
                //if (string.IsNullOrWhiteSpace(this.textBox门诊住院号.Text))
                //{
                //}
                //else if (this.textBox门诊住院号.Text.Trim().Length == 4)
                //{
                //    this.textBox门诊住院号.Text = DateTime.Now.ToString("yyyyMMdd") + "." + this.textBox门诊住院号.Text.Trim();
                //    fjy_门诊住院号 = this.textBox门诊住院号.Text;
                //}
                //del by wjz 20160427 由于夏蔚医院的住院号是4位，在这里删除对门诊住院号的判断 △
                //add by wjz 20160125 修改查询条件，如果门诊主要号输入的数据是四位，则将其转换为门诊号 △
                string strDateif = " cast(fapply_time as datetime)>='" + fjy_date1 
                    + "' and cast(fapply_time as datetime)<='" + fjy_date2 
                    //+ "' and ftype_id='" + fjy_病人类型    //根据医保卡查询时，不再判断病人类型
                    + "' and fhz_zyh like '%" + fjy_门诊住院号 + "%'";

                strDateif += " and (sfzh='"+person.S身份证号+"' and fname='"+person.S姓名+"') ";

                //add by wjz 20160125 修改门诊申请单的查询条件，对于门诊，只查询“已收款”的申请单信息。此处需要修改HIS视图 ▽
                //if (fjy_病人类型.Trim() == "门诊")
                //{
                //    strDateif += " and 状态='已收款'";
                //}
                //add by wjz 20160125 修改门诊申请单的查询条件，对于门诊，只查询“已收款”的申请单信息。此处需要修改HIS视图 △
                dtMain = this.bllApply.BllSamplingMainDTFromHis(strDateif, ww.form.Properties.Settings.Default.HisDBName);

                //add by wjz 20160428 添加查询提示，提示是否查到了数据 ▽
                if(dtMain==null || dtMain.Rows.Count == 0)
                {
                    labelNotify.ForeColor = Color.Red;
                    labelNotify.Text = "未查到任何数据";
                }
                //add by wjz 20160428 添加查询提示，提示是否查到了数据 △
                //add by wjz 20160506 修改查询条件里的病人类型 ▽
                else
                {
                    string type = dtMain.Rows[0]["ftype_id"].ToString();
                    this.comboBox病人类型.SelectedValue = type;
                }
                //add by wjz 20160506 修改查询条件里的病人类型 △

                #region Lis端只负责数据的显示，申请号的生成放到了His端，所以不再需要这部分代码  20150630 wjz
                ////从LIS数据库中SAM_APPLY表中取出的数据
                //DataTable dtFromLisApply = this.bllApply.BllSamplingMainDTFromLis(strDateif);

                ////组合两组数据, 将dtFromLisApply中的申请单号、状态值，添加到dtMain中
                //for (int index = 0; index < dtMain.Rows.Count; index++ )
                //{
                //    //ftype_id, fhz_id, fhz_zyh,his_recordid
                //    string strTemp = "ftype_id='" + dtMain.Rows[index]["ftype_id"].ToString() + "' and fhz_id='" + dtMain.Rows[index]["fhz_id"].ToString()
                //        + "' and fhz_zyh='" + dtMain.Rows[index]["fhz_zyh"].ToString() + "' and his_recordid='" + dtMain.Rows[index]["his_recordid"].ToString() + "'";
                //    DataRow[] rows = dtFromLisApply.Select(strTemp);

                //    if(rows.Length > 0)
                //    {
                //        //fapply_id, fstate
                //        dtMain.Rows[index]["fapply_id"] = rows[0]["fapply_id"].ToString();
                //        dtMain.Rows[index]["fstate"] = rows[0]["fstate"].ToString();
                //    }
                //}
                #endregion

                this.bindingSourceApply.DataSource = dtMain;
                dataGridViewApply.DataSource = bindingSourceApply;
                this.bN.BindingSource = bindingSourceApply;
                int intfstate = 0;
                for (int intGrid = 0; intGrid < this.dataGridViewApply.Rows.Count; intGrid++)
                {
                    intfstate = 0;
                    //修改此部分的代码，基本逻辑没有变化 20150624 wjz begin
                    if (this.dataGridViewApply.Rows[intGrid].Cells["fstate"].Value != null)
                    {
                        try
                        {
                            intfstate = Convert.ToInt32(this.dataGridViewApply.Rows[intGrid].Cells["fstate"].Value.ToString());
                        }
                        catch
                        { }
                    }
                    //if (intfstate == 1)//已执行申请
                    //this.dataGridViewApply.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Red;
                    if (intfstate == 2)//已打印条码
                    {
                        this.dataGridViewApply.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Blue;
                    }
                    else if (intfstate == 3)//已打印条码
                    {
                        this.dataGridViewApply.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Red;
                    }
                    //修改此部分的代码，基本逻辑没有变化 20150624 wjz end
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        // add by wjz 20160427 为杨庄医院添加读社保卡的功能 △

        private bool PrinterExists(string printerName)
        {
            //PrintDocument prtdoc = new PrintDocument();
            //string strDefaultPrinter = prtdoc.PrinterSettings.PrinterName;      //获取默认的打印机名    

            PrinterSettings.StringCollection snames = PrinterSettings.InstalledPrinters;

            foreach (string s in snames)
            {
                if (s.ToLower().Trim() == printerName.Trim().ToLower())
                {
                    return true;
                }
            }
            return false;
        }

        private void btn读电子健康卡_Click(object sender, EventArgs e)
        {
            //启动钩子服务
            listener.Start();
        }

    }
}