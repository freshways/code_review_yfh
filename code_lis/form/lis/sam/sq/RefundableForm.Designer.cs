﻿namespace ww.form.lis.sam.sq
{
    partial class RefundableForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RefundableForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridViewApply = new System.Windows.Forms.DataGridView();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.dataGridViewItem = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.labelMess = new System.Windows.Forms.Label();
            this.Refund_barcode = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDoRefund = new System.Windows.Forms.Button();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.contextMenuStripReport = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem_applygs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_checkidtemgs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.帮助ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.关闭ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewApply)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewItem)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.contextMenuStripReport.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridViewApply);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 47);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(414, 444);
            this.groupBox1.TabIndex = 145;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "申请单";
            // 
            // dataGridViewApply
            // 
            this.dataGridViewApply.AllowUserToAddRows = false;
            this.dataGridViewApply.AllowUserToResizeRows = false;
            this.dataGridViewApply.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewApply.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewApply.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewApply.ColumnHeadersHeight = 21;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewApply.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewApply.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewApply.EnableHeadersVisualStyles = false;
            this.dataGridViewApply.Location = new System.Drawing.Point(3, 17);
            this.dataGridViewApply.MultiSelect = false;
            this.dataGridViewApply.Name = "dataGridViewApply";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewApply.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewApply.RowHeadersVisible = false;
            this.dataGridViewApply.RowHeadersWidth = 35;
            this.dataGridViewApply.RowTemplate.Height = 23;
            this.dataGridViewApply.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewApply.Size = new System.Drawing.Size(408, 424);
            this.dataGridViewApply.TabIndex = 137;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(414, 47);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 444);
            this.splitter1.TabIndex = 147;
            this.splitter1.TabStop = false;
            // 
            // dataGridViewItem
            // 
            this.dataGridViewItem.AllowUserToAddRows = false;
            this.dataGridViewItem.AllowUserToResizeRows = false;
            this.dataGridViewItem.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewItem.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewItem.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewItem.ColumnHeadersHeight = 21;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewItem.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewItem.EnableHeadersVisualStyles = false;
            this.dataGridViewItem.Location = new System.Drawing.Point(3, 17);
            this.dataGridViewItem.MultiSelect = false;
            this.dataGridViewItem.Name = "dataGridViewItem";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewItem.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewItem.RowHeadersVisible = false;
            this.dataGridViewItem.RowHeadersWidth = 35;
            this.dataGridViewItem.RowTemplate.Height = 23;
            this.dataGridViewItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewItem.Size = new System.Drawing.Size(284, 424);
            this.dataGridViewItem.TabIndex = 136;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridViewItem);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox2.Location = new System.Drawing.Point(417, 47);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(290, 444);
            this.groupBox2.TabIndex = 146;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "项目";
            // 
            // labelMess
            // 
            this.labelMess.AutoSize = true;
            this.labelMess.ForeColor = System.Drawing.Color.Red;
            this.labelMess.Location = new System.Drawing.Point(438, 15);
            this.labelMess.Name = "labelMess";
            this.labelMess.Size = new System.Drawing.Size(29, 12);
            this.labelMess.TabIndex = 24;
            this.labelMess.Text = "消息";
            // 
            // Refund_barcode
            // 
            this.Refund_barcode.Location = new System.Drawing.Point(145, 9);
            this.Refund_barcode.Multiline = true;
            this.Refund_barcode.Name = "Refund_barcode";
            this.Refund_barcode.Size = new System.Drawing.Size(180, 25);
            this.Refund_barcode.TabIndex = 23;
            this.Refund_barcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Refund_barcode_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label4.Location = new System.Drawing.Point(14, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 16);
            this.label4.TabIndex = 22;
            this.label4.Text = "请输入条码号:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnDoRefund);
            this.panel1.Controls.Add(this.labelMess);
            this.panel1.Controls.Add(this.Refund_barcode);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(707, 47);
            this.panel1.TabIndex = 144;
            // 
            // btnDoRefund
            // 
            this.btnDoRefund.Location = new System.Drawing.Point(342, 5);
            this.btnDoRefund.Name = "btnDoRefund";
            this.btnDoRefund.Size = new System.Drawing.Size(75, 32);
            this.btnDoRefund.TabIndex = 25;
            this.btnDoRefund.Text = "取消登记";
            this.btnDoRefund.UseVisualStyleBackColor = true;
            this.btnDoRefund.Click += new System.EventHandler(this.btnDoRefund_Click);
            // 
            // bindingSource1
            // 
            this.bindingSource1.PositionChanged += new System.EventHandler(this.bindingSource1_PositionChanged);
            // 
            // contextMenuStripReport
            // 
            this.contextMenuStripReport.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_applygs,
            this.toolStripMenuItem_checkidtemgs,
            this.toolStripSeparator1,
            this.帮助ToolStripMenuItem,
            this.toolStripSeparator2,
            this.关闭ToolStripMenuItem});
            this.contextMenuStripReport.Name = "contextMenuStripReport";
            this.contextMenuStripReport.Size = new System.Drawing.Size(137, 104);
            // 
            // toolStripMenuItem_applygs
            // 
            this.toolStripMenuItem_applygs.Name = "toolStripMenuItem_applygs";
            this.toolStripMenuItem_applygs.Size = new System.Drawing.Size(136, 22);
            this.toolStripMenuItem_applygs.Text = "申请单格式";
            this.toolStripMenuItem_applygs.Click += new System.EventHandler(this.toolStripMenuItem_applygs_Click);
            // 
            // toolStripMenuItem_checkidtemgs
            // 
            this.toolStripMenuItem_checkidtemgs.Name = "toolStripMenuItem_checkidtemgs";
            this.toolStripMenuItem_checkidtemgs.Size = new System.Drawing.Size(136, 22);
            this.toolStripMenuItem_checkidtemgs.Text = "项目格式";
            this.toolStripMenuItem_checkidtemgs.Click += new System.EventHandler(this.toolStripMenuItem_checkidtemgs_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(133, 6);
            // 
            // 帮助ToolStripMenuItem
            // 
            this.帮助ToolStripMenuItem.Name = "帮助ToolStripMenuItem";
            this.帮助ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.帮助ToolStripMenuItem.Text = "帮助";
            this.帮助ToolStripMenuItem.Click += new System.EventHandler(this.帮助ToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(133, 6);
            // 
            // 关闭ToolStripMenuItem
            // 
            this.关闭ToolStripMenuItem.Name = "关闭ToolStripMenuItem";
            this.关闭ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.关闭ToolStripMenuItem.Text = "关闭";
            this.关闭ToolStripMenuItem.Click += new System.EventHandler(this.关闭ToolStripMenuItem_Click);
            // 
            // RefundableForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(707, 491);
            this.ContextMenuStrip = this.contextMenuStripReport;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RefundableForm";
            this.Text = "取消扫码登记";
            this.Load += new System.EventHandler(this.RefundableForm_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewApply)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewItem)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.contextMenuStripReport.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridViewApply;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.DataGridView dataGridViewItem;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label labelMess;
        private System.Windows.Forms.TextBox Refund_barcode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Button btnDoRefund;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripReport;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_applygs;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_checkidtemgs;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 帮助ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem 关闭ToolStripMenuItem;
    }
}