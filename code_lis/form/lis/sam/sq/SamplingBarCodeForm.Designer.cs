﻿namespace ww.form.lis.sam.sq
{
    partial class SamplingBarCodeForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SamplingBarCodeForm));
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.contextMenuStripReport = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem_applygs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_checkidtemgs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.帮助ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.关闭ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridViewApply = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridViewItem = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_读电子健康卡 = new System.Windows.Forms.Button();
            this.sbtnExport = new System.Windows.Forms.Button();
            this.labelNotify = new System.Windows.Forms.Label();
            this.comboBoxDateQueryType = new System.Windows.Forms.ComboBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxSFZH = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnReadCardAndQuery = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txt门诊住院号 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.fjy_dateDateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.fjy_dateDateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton打印预览 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.contextMenuStripReport.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewApply)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewItem)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bindingSource1
            // 
            this.bindingSource1.PositionChanged += new System.EventHandler(this.bindingSource1_PositionChanged);
            // 
            // contextMenuStripReport
            // 
            this.contextMenuStripReport.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_applygs,
            this.toolStripMenuItem_checkidtemgs,
            this.toolStripSeparator1,
            this.帮助ToolStripMenuItem,
            this.toolStripSeparator3,
            this.关闭ToolStripMenuItem});
            this.contextMenuStripReport.Name = "contextMenuStripReport";
            this.contextMenuStripReport.Size = new System.Drawing.Size(137, 104);
            // 
            // toolStripMenuItem_applygs
            // 
            this.toolStripMenuItem_applygs.Name = "toolStripMenuItem_applygs";
            this.toolStripMenuItem_applygs.Size = new System.Drawing.Size(136, 22);
            this.toolStripMenuItem_applygs.Text = "申请单格式";
            this.toolStripMenuItem_applygs.Click += new System.EventHandler(this.toolStripMenuItem_applygs_Click);
            // 
            // toolStripMenuItem_checkidtemgs
            // 
            this.toolStripMenuItem_checkidtemgs.Name = "toolStripMenuItem_checkidtemgs";
            this.toolStripMenuItem_checkidtemgs.Size = new System.Drawing.Size(136, 22);
            this.toolStripMenuItem_checkidtemgs.Text = "项目格式";
            this.toolStripMenuItem_checkidtemgs.Click += new System.EventHandler(this.toolStripMenuItem_checkidtemgs_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(133, 6);
            // 
            // 帮助ToolStripMenuItem
            // 
            this.帮助ToolStripMenuItem.Name = "帮助ToolStripMenuItem";
            this.帮助ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.帮助ToolStripMenuItem.Text = "帮助";
            this.帮助ToolStripMenuItem.Click += new System.EventHandler(this.帮助ToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(133, 6);
            // 
            // 关闭ToolStripMenuItem
            // 
            this.关闭ToolStripMenuItem.Name = "关闭ToolStripMenuItem";
            this.关闭ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.关闭ToolStripMenuItem.Text = "关闭";
            this.关闭ToolStripMenuItem.Click += new System.EventHandler(this.关闭ToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridViewApply);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 98);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(560, 338);
            this.groupBox1.TabIndex = 141;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "申请单";
            // 
            // dataGridViewApply
            // 
            this.dataGridViewApply.AllowUserToAddRows = false;
            this.dataGridViewApply.AllowUserToResizeRows = false;
            this.dataGridViewApply.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewApply.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewApply.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewApply.ColumnHeadersHeight = 21;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewApply.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewApply.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewApply.EnableHeadersVisualStyles = false;
            this.dataGridViewApply.Location = new System.Drawing.Point(3, 17);
            this.dataGridViewApply.MultiSelect = false;
            this.dataGridViewApply.Name = "dataGridViewApply";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewApply.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewApply.RowHeadersVisible = false;
            this.dataGridViewApply.RowHeadersWidth = 35;
            this.dataGridViewApply.RowTemplate.Height = 23;
            this.dataGridViewApply.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewApply.Size = new System.Drawing.Size(554, 318);
            this.dataGridViewApply.TabIndex = 137;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridViewItem);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox2.Location = new System.Drawing.Point(560, 98);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(317, 338);
            this.groupBox2.TabIndex = 142;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "项目";
            // 
            // dataGridViewItem
            // 
            this.dataGridViewItem.AllowUserToAddRows = false;
            this.dataGridViewItem.AllowUserToResizeRows = false;
            this.dataGridViewItem.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewItem.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewItem.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewItem.ColumnHeadersHeight = 21;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewItem.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewItem.EnableHeadersVisualStyles = false;
            this.dataGridViewItem.Location = new System.Drawing.Point(3, 17);
            this.dataGridViewItem.MultiSelect = false;
            this.dataGridViewItem.Name = "dataGridViewItem";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewItem.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewItem.RowHeadersVisible = false;
            this.dataGridViewItem.RowHeadersWidth = 35;
            this.dataGridViewItem.RowTemplate.Height = 23;
            this.dataGridViewItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewItem.Size = new System.Drawing.Size(311, 318);
            this.dataGridViewItem.TabIndex = 136;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_读电子健康卡);
            this.panel1.Controls.Add(this.sbtnExport);
            this.panel1.Controls.Add(this.labelNotify);
            this.panel1.Controls.Add(this.comboBoxDateQueryType);
            this.panel1.Controls.Add(this.textBoxName);
            this.panel1.Controls.Add(this.textBoxSFZH);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.btnReadCardAndQuery);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.txt门诊住院号);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.fjy_dateDateTimePicker2);
            this.panel1.Controls.Add(this.fjy_dateDateTimePicker1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(877, 68);
            this.panel1.TabIndex = 140;
            // 
            // btn_读电子健康卡
            // 
            this.btn_读电子健康卡.Location = new System.Drawing.Point(771, 4);
            this.btn_读电子健康卡.Name = "btn_读电子健康卡";
            this.btn_读电子健康卡.Size = new System.Drawing.Size(92, 53);
            this.btn_读电子健康卡.TabIndex = 32;
            this.btn_读电子健康卡.Text = "读电子健康卡";
            this.btn_读电子健康卡.UseVisualStyleBackColor = true;
            this.btn_读电子健康卡.Click += new System.EventHandler(this.btn_读电子健康卡_Click);
            // 
            // sbtnExport
            // 
            this.sbtnExport.Location = new System.Drawing.Point(673, 5);
            this.sbtnExport.Name = "sbtnExport";
            this.sbtnExport.Size = new System.Drawing.Size(92, 53);
            this.sbtnExport.TabIndex = 32;
            this.sbtnExport.Text = "导出";
            this.sbtnExport.UseVisualStyleBackColor = true;
            this.sbtnExport.Click += new System.EventHandler(this.sbtnExport_Click);
            // 
            // labelNotify
            // 
            this.labelNotify.AutoSize = true;
            this.labelNotify.Location = new System.Drawing.Point(682, 10);
            this.labelNotify.Name = "labelNotify";
            this.labelNotify.Size = new System.Drawing.Size(0, 12);
            this.labelNotify.TabIndex = 31;
            // 
            // comboBoxDateQueryType
            // 
            this.comboBoxDateQueryType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDateQueryType.FormattingEnabled = true;
            this.comboBoxDateQueryType.Items.AddRange(new object[] {
            "条码生成时间:",
            "申请时间:"});
            this.comboBoxDateQueryType.Location = new System.Drawing.Point(16, 7);
            this.comboBoxDateQueryType.Name = "comboBoxDateQueryType";
            this.comboBoxDateQueryType.Size = new System.Drawing.Size(96, 20);
            this.comboBoxDateQueryType.TabIndex = 30;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(407, 37);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(128, 21);
            this.textBoxName.TabIndex = 28;
            // 
            // textBoxSFZH
            // 
            this.textBoxSFZH.Location = new System.Drawing.Point(117, 37);
            this.textBoxSFZH.Name = "textBoxSFZH";
            this.textBoxSFZH.Size = new System.Drawing.Size(185, 21);
            this.textBoxSFZH.TabIndex = 29;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(337, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 26;
            this.label1.Text = "病人姓名：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(53, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 27;
            this.label2.Text = "身份证号:";
            // 
            // btnReadCardAndQuery
            // 
            this.btnReadCardAndQuery.Location = new System.Drawing.Point(551, 35);
            this.btnReadCardAndQuery.Name = "btnReadCardAndQuery";
            this.btnReadCardAndQuery.Size = new System.Drawing.Size(90, 23);
            this.btnReadCardAndQuery.TabIndex = 25;
            this.btnReadCardAndQuery.Text = "读社保卡查询";
            this.btnReadCardAndQuery.UseVisualStyleBackColor = true;
            this.btnReadCardAndQuery.Click += new System.EventHandler(this.btnReadCardAndQuery_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(551, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 23);
            this.button1.TabIndex = 25;
            this.button1.Text = "普通查询";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txt门诊住院号
            // 
            this.txt门诊住院号.Location = new System.Drawing.Point(407, 5);
            this.txt门诊住院号.Name = "txt门诊住院号";
            this.txt门诊住院号.Size = new System.Drawing.Size(128, 21);
            this.txt门诊住院号.TabIndex = 24;
            this.txt门诊住院号.TextChanged += new System.EventHandler(this.tbfhz_id_TextChanged);
            this.txt门诊住院号.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbfhz_id_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(321, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 12);
            this.label5.TabIndex = 23;
            this.label5.Text = "门诊/住院号：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(205, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(11, 12);
            this.label11.TabIndex = 21;
            this.label11.Text = "-";
            // 
            // fjy_dateDateTimePicker2
            // 
            this.fjy_dateDateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker2.Location = new System.Drawing.Point(217, 5);
            this.fjy_dateDateTimePicker2.Name = "fjy_dateDateTimePicker2";
            this.fjy_dateDateTimePicker2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker2.Size = new System.Drawing.Size(85, 21);
            this.fjy_dateDateTimePicker2.TabIndex = 20;
            this.fjy_dateDateTimePicker2.CloseUp += new System.EventHandler(this.fjy_dateDateTimePicker2_CloseUp);
            // 
            // fjy_dateDateTimePicker1
            // 
            this.fjy_dateDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker1.Location = new System.Drawing.Point(117, 5);
            this.fjy_dateDateTimePicker1.Name = "fjy_dateDateTimePicker1";
            this.fjy_dateDateTimePicker1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker1.Size = new System.Drawing.Size(85, 21);
            this.fjy_dateDateTimePicker1.TabIndex = 19;
            this.fjy_dateDateTimePicker1.CloseUp += new System.EventHandler(this.fjy_dateDateTimePicker1_CloseUp);
            this.fjy_dateDateTimePicker1.ValueChanged += new System.EventHandler(this.fjy_dateDateTimePicker1_ValueChanged);
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = null;
            this.bindingNavigator1.AutoSize = false;
            this.bindingNavigator1.BindingSource = this.bindingSource1;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator1.DeleteItem = null;
            this.bindingNavigator1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator6,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.toolStripButton2,
            this.toolStripSeparator2,
            this.toolStripButton打印预览,
            this.toolStripSeparator7,
            this.toolStripButton3});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator1.MoveFirstItem = null;
            this.bindingNavigator1.MoveLastItem = null;
            this.bindingNavigator1.MoveNextItem = null;
            this.bindingNavigator1.MovePreviousItem = null;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bindingNavigator1.Size = new System.Drawing.Size(877, 30);
            this.bindingNavigator1.TabIndex = 143;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(32, 27);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 30);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = global::ww.form.Properties.Resources.button_print1;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(75, 27);
            this.toolStripButton2.Text = "打印(&P)  ";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButton打印预览
            // 
            this.toolStripButton打印预览.Image = global::ww.form.Properties.Resources.button_print2;
            this.toolStripButton打印预览.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton打印预览.Name = "toolStripButton打印预览";
            this.toolStripButton打印预览.Size = new System.Drawing.Size(76, 27);
            this.toolStripButton打印预览.Text = "打印预览";
            this.toolStripButton打印预览.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(74, 27);
            this.toolStripButton3.Text = "帮助(&F)  ";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // SamplingBarCodeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(877, 436);
            this.ContextMenuStrip = this.contextMenuStripReport;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bindingNavigator1);
            this.Name = "SamplingBarCodeForm";
            this.Text = "条码打印查询";
            this.Load += new System.EventHandler(this.SamplingBarCodeForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.contextMenuStripReport.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewApply)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewItem)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker2;
        private System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridViewApply;
        private System.Windows.Forms.DataGridView dataGridViewItem;
        private System.Windows.Forms.TextBox txt门诊住院号;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripReport;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_applygs;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_checkidtemgs;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 帮助ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem 关闭ToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        protected System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton打印预览;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxSFZH;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnReadCardAndQuery;
        private System.Windows.Forms.ComboBox comboBoxDateQueryType;
        private System.Windows.Forms.Label labelNotify;
        private System.Windows.Forms.Button sbtnExport;
        private System.Windows.Forms.Button btn_读电子健康卡;
    }
}