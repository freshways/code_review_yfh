﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;
using ww.form.lis.com;
using System.Collections;
namespace ww.form.lis.sam.sq
{
  
    public partial class ReportQueryForm : com.ReportBaseForm
    {
        #region 自定义变量
        /// <summary>
        /// 报告格式 
        /// </summary>
        private UserdefinedFieldUpdateForm gsRepot = new UserdefinedFieldUpdateForm("lis_sam_report_query_report_sq");
        /// <summary>
        /// 结果格式 
        /// </summary>
        private UserdefinedFieldUpdateForm gsResult = new UserdefinedFieldUpdateForm("lis_sam_report_query_result_sq");
        /// <summary>
        /// 当前行 样本
        /// </summary>
        DataRowView rowCurrent = null;
        /// <summary>
        /// 样本ID
        /// </summary>
        string strfsample_id = "";
        /// <summary>
        /// 申请ID
        /// </summary>
        string strfapply_id = "";
        /// <summary>
        /// 审核标记
        /// </summary>
        string strfexamine_flag = "0";
        /// <summary>
        /// 仪器列表
        /// </summary>
        DataTable dtInstr = null;
        #endregion

        #region 自定义方法
        /// <summary>
        /// 本页初始化方法
        /// </summary>
        private void WWInit()
        {
            try
            {
                this.com_listBindingSource_fstate.DataSource = this.WWComTypeSampleState();//检验状态
                this.com_listBindingSource_fjytype_id.DataSource = this.WWCheckType();//检验类别
                this.com_listBindingSource_fsample_type_id.DataSource = this.WWSampleType();//样本类别
                this.com_listBindingSource_fapply_dept_id.DataSource = this.WWApplyDept();//申请部门
                this.com_listBindingSource_fapply_user_id.DataSource = this.WWUserApply();//申请人
                this.com_listBindingSource_fjy_user_id.DataSource = this.WWUserCheck();//检验人
                dtInstr = this.bllInstr.BllInstrDTByUseAndGroupID(1, ww.wwf.wwfbll.LoginBLL.strDeptID);//仪器列表
                this.fjy_instrComboBox.DataSource = dtInstr;
                fjy_instrComboBox.SelectedValue = "";

                //20150610 begin  add by wjz 添加本机默认仪器功能
                string strInstrID = ww.form.wwf.Customization.DefaultInstrument.GetInstrID();
                if (!(string.IsNullOrWhiteSpace(strInstrID)))
                {
                    this.fjy_instrComboBox.SelectedValue = strInstrID;
                }
                //20150610 end    add by wjz 添加本机默认仪器功能

                this.ftype_idComboBox.DataSource = this.WWComTypeftype_id();//病人类别 
                ftype_idComboBox.SelectedValue = "";

                dataGridViewReport.AutoGenerateColumns = false;
                dataGridViewResult.AutoGenerateColumns = false;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 取得报告列表
        /// </summary>
        private void WWSetReportList()
        {
            if (this.dtReportResult != null)
                this.dtReportResult.Clear();
           
            try
            {
                Cursor = Cursors.WaitCursor;
                string fhz_id = "";//患者ID
                string fapply_code = "";//申请单号
                string fzyh = "";//住院号
                string fsample_code = "";//样本号
                string fname = "";//姓名                
                string finstr_id = "";//仪器ID
                string fhz_type_id = "";//患者类别
                string fjy_date1 = this.bllReport.DbDateTime1(this.fjy_dateDateTimePicker1);
                string fjy_date2 = this.bllReport.DbDateTime1(this.fjy_dateDateTimePicker2);
                string fjy_dateWhere = "";
                fsample_code = this.fsample_codeTextBox.Text;
                fname = this.fnameTextBox.Text;
                fzyh = fzyhtextBox.Text;
                fhz_id = this.fhz_idtextBox.Text;
                fapply_code = this.fapply_codetextBox.Text;

                try
                {
                    finstr_id = fjy_instrComboBox.SelectedValue.ToString();
                }
                catch { }
                try
                {
                    fhz_type_id = ftype_idComboBox.SelectedValue.ToString();
                }
                catch { }
                
                if (fjy_instrComboBox.Text == "" || fjy_instrComboBox.Text == null)
                    finstr_id = " (fjy_instr is not null) ";
                else
                    finstr_id = " (fjy_instr='" + finstr_id + "') ";

                if (this.ftype_idComboBox.Text == "" || this.ftype_idComboBox.Text == null)
                    fhz_type_id = " and (ftype_id is not null)";
                else
                    fhz_type_id = " and (ftype_id='" + fhz_type_id + "') ";

                fjy_dateWhere = " and (fjy_date>='" + fjy_date1 + "' and fjy_date<='" + fjy_date2 + "') ";

                if (fsample_code == "" || fsample_code == null)
                    fsample_code = " and (fsample_code is not null) ";
                else
                    fsample_code = " and (fsample_code='" + fsample_code + "')";
                if (fname == "" || fname == null)
                    fname = " and (fname is not null) ";
                else
                    fname = " and (fname like '" + fname + "%')";

                if (fzyh == "" || fzyh == null)
                    fzyh = " and (fhz_zyh is not null) ";
                else
                    fzyh = " and (fhz_zyh='" + fzyh + "')";


                if (fhz_id == "" || fhz_id == null)
                    fhz_id = " and (fhz_id is not null) ";
                else
                    fhz_id = " and (fhz_id='" + fhz_id + "')";

                if (fapply_code == "" || fapply_code == null)
                    fapply_code = " and (fapply_id is not null) ";
                else
                    fapply_code = " and (fapply_id='" + fapply_code + "')";

                string strWhere = finstr_id + fhz_type_id + fjy_dateWhere + fsample_code + fname + fzyh + fhz_id + fapply_code;
                this.WWSetdtReport(" where "+ strWhere);
                this.lIS_REPORTBindingSource.DataSource = this.dtReport;
                dataGridViewReport.DataSource = this.lIS_REPORTBindingSource;
                WWReportGridViesStyle();
               
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        /// <summary>
        ///报告 列表风格设置
        /// </summary>
        private void WWReportGridViesStyle()
        {
            try
            {
                int intfstate = 0;
                for (int i = 0; i < this.dataGridViewReport.Rows.Count; i++)
                {
                    if (this.dataGridViewReport.Rows[i].Cells["fexamine_flag"].Value != null)
                        intfstate = Convert.ToInt32(this.dataGridViewReport.Rows[i].Cells["fexamine_flag"].Value.ToString());
                    switch (intfstate)
                    {
                        case 1:
                            this.dataGridViewReport.Rows[i].DefaultCellStyle.ForeColor = Color.Blue; //System.Drawing.SystemColors.Desktop;
                            break;
                        default:
                            this.dataGridViewReport.Rows[i].DefaultCellStyle.ForeColor = Color.Black; //System.Drawing.SystemColors.Desktop;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void WWGridViewResultValueSet()
        {
            try
            {
                if (this.dataGridViewResult.Rows.Count > 0)
                {
                    string strItem_id = "";
                    string strValue = "";//值；                   
                    for (int intGrid = 0; intGrid < this.dataGridViewResult.Rows.Count; intGrid++)
                    {
                        if (this.dataGridViewResult.Rows[intGrid].Cells["fitem_id"].Value != null)
                            strItem_id = this.dataGridViewResult.Rows[intGrid].Cells["fitem_id"].Value.ToString();
                        else
                            strItem_id = "";

                        if (this.dataGridViewResult.Rows[intGrid].Cells["fvalue"].Value != null)
                            strValue = this.dataGridViewResult.Rows[intGrid].Cells["fvalue"].Value.ToString();
                        else
                            strValue = "";                       
                        //标记
                        int intBJ = 0;
                        if (this.dataGridViewResult.Rows[intGrid].Cells["fitem_ref"].Value == null)
                        { }
                        else
                        {
                            intBJ = this.bllReport.BllItemValueBJ(strValue, this.dataGridViewResult.Rows[intGrid].Cells["fitem_ref"].Value.ToString());
                        }
                        this.dataGridViewResult.Rows[intGrid].Cells["fitem_badge"].Value = this.bllReport.BllItemValueBJFname(intBJ);
                        //
                        if (intBJ == 2)
                        {
                            this.dataGridViewResult.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Blue; //System.Drawing.
                        }
                        if (intBJ == 3)
                        {
                            this.dataGridViewResult.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Red; //System.Drawing.
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void WWResultList()
        {
            try
            {
              
                this.WWSetdtReportResult(strfapply_id);
                this.bindingSourceResult.DataSource = this.dtReportResult;
                this.dataGridViewResult.DataSource = this.bindingSourceResult;
                WWGridViewResultValueSet();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
     
        /// <summary>
        /// 快捷键 设置
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref   System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == (Keys.F1))
            {
                toolStripButtonHelp.PerformClick();
                return true;
            }
            if (keyData == (Keys.F5))
            {
                toolStripButtonQuery.PerformClick();
                return true;
            } if (keyData == (Keys.F8))
            {
                toolStripButtonPrint.PerformClick();
                return true;
            } if (keyData == (Keys.F9))
            {
                toolStripButtonPrintYL.PerformClick();
                return true;
            } 
            return base.ProcessCmdKey(ref   msg, keyData);
        }

        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="printType"></param>
        private void WWPrint(int printType)
        {
            try
            {
                ww.form.lis.sam.Report.PrintCom pr = new ww.form.lis.sam.Report.PrintCom();
                pr.BllPrintViewer(printType, this.strfsample_id);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        #endregion

        public ReportQueryForm()
        {
            InitializeComponent();
            WWInit();
        }

        private void ReportQueryForm_Load(object sender, EventArgs e)
        {
            try
            {
                WWSetReportList();
                this.gsRepot.DataGridViewSetStyleNew(this.dataGridViewReport);
                this.gsResult.DataGridViewSetStyleNew(this.dataGridViewResult);

            }

            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        
        
        private void lIS_REPORTBindingSource_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                rowCurrent = (DataRowView)this.lIS_REPORTBindingSource.Current;
                if (this.dtReportResult.Rows.Count > 0)
                    this.dtReportResult.Clear();
                if (rowCurrent != null)
                {
                    strfapply_id = rowCurrent["fapply_id"].ToString();
                    strfsample_id = rowCurrent["fsample_id"].ToString();
                    if (rowCurrent["fexamine_flag"] != null)
                        strfexamine_flag = rowCurrent["fexamine_flag"].ToString();
                    WWResultList();
                }

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }

            
        }

        private void dataGridViewResult_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void toolStripButtonHelp_Click(object sender, EventArgs e)
        {
            this.WWOpenHelp("78b5005b3a4f40b39d8390902c7201b1");
        }

        private void toolStripButtonPrintYL_Click(object sender, EventArgs e)
        {


            WWPrint(1);
        }

        private void toolStripButtonPrint_Click(object sender, EventArgs e)
        {
            WWPrint(0);
        }

        private void toolStripButtonQuery_Click(object sender, EventArgs e)
        {
            WWSetReportList();
           
        }

        private void 报表格式ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                gsRepot.ShowDialog();

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 结果模式ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                gsResult.ShowDialog();

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void dataGridViewReport_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridViewReport.Rows.Count > 0)
                {

                   strfapply_id = this.dataGridViewReport.CurrentRow.Cells["fapply_id"].Value.ToString();
                    WWResultList();
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void dataGridViewReport_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

       
       
       
    }
}