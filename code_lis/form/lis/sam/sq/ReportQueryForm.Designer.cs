﻿namespace ww.form.lis.sam.sq
{
    partial class ReportQueryForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label fnameLabel;
            System.Windows.Forms.Label fsample_codeLabel;
            System.Windows.Forms.Label fhz_type_idLabel;
            System.Windows.Forms.Label fjy_dateLabel;
            System.Windows.Forms.Label finstr_idLabel;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportQueryForm));
            this.lIS_REPORTBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.samDataSet = new ww.form.lis.sam.samDataSet();
            this.dataGridViewReport = new System.Windows.Forms.DataGridView();
            this.com_listBindingSource_fstate = new System.Windows.Forms.BindingSource(this.components);
            this.com_listBindingSource_fjytype_id = new System.Windows.Forms.BindingSource(this.components);
            this.com_listBindingSource_fsample_type_id = new System.Windows.Forms.BindingSource(this.components);
            this.com_listBindingSource_fapply_user_id = new System.Windows.Forms.BindingSource(this.components);
            this.com_listBindingSource_fapply_dept_id = new System.Windows.Forms.BindingSource(this.components);
            this.com_listBindingSource_fjy_user_id = new System.Windows.Forms.BindingSource(this.components);
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.dataGridViewResult = new System.Windows.Forms.DataGridView();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.bindingSourceResult = new System.Windows.Forms.BindingSource(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBoxif = new System.Windows.Forms.GroupBox();
            this.fhz_idtextBox = new System.Windows.Forms.TextBox();
            this.fapply_codetextBox = new System.Windows.Forms.TextBox();
            this.fzyhtextBox = new System.Windows.Forms.TextBox();
            this.fnameTextBox = new System.Windows.Forms.TextBox();
            this.fsample_codeTextBox = new System.Windows.Forms.TextBox();
            this.ftype_idComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.fjy_dateDateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.fjy_dateDateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.fjy_instrComboBox = new System.Windows.Forms.ComboBox();
            this.bN = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonQuery = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPrint = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPrintYL = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonJCSH = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSplitButtonGZ = new System.Windows.Forms.ToolStripSplitButton();
            this.报表格式ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.结果模式ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButtonHelp = new System.Windows.Forms.ToolStripButton();
            this.fread_flag = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fexamine_flag = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fprint_flag = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fstate = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.fjytype_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.fsample_type_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.fapply_user_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.fapply_dept_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.fjy_user_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            label4 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            fnameLabel = new System.Windows.Forms.Label();
            fsample_codeLabel = new System.Windows.Forms.Label();
            fhz_type_idLabel = new System.Windows.Forms.Label();
            fjy_dateLabel = new System.Windows.Forms.Label();
            finstr_idLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIS_REPORTBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fstate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fjytype_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fsample_type_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fapply_user_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fapply_dept_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fjy_user_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceResult)).BeginInit();
            this.groupBoxif.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).BeginInit();
            this.bN.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(263, 42);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(47, 12);
            label4.TabIndex = 29;
            label4.Text = "病人ID:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(567, 17);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(59, 12);
            label3.TabIndex = 27;
            label3.Text = "申请单号:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(579, 42);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(47, 12);
            label2.TabIndex = 25;
            label2.Text = "住院号:";
            // 
            // fnameLabel
            // 
            fnameLabel.AutoSize = true;
            fnameLabel.Location = new System.Drawing.Point(7, 42);
            fnameLabel.Name = "fnameLabel";
            fnameLabel.Size = new System.Drawing.Size(59, 12);
            fnameLabel.TabIndex = 22;
            fnameLabel.Text = "病人姓名:";
            // 
            // fsample_codeLabel
            // 
            fsample_codeLabel.AutoSize = true;
            fsample_codeLabel.Location = new System.Drawing.Point(420, 42);
            fsample_codeLabel.Name = "fsample_codeLabel";
            fsample_codeLabel.Size = new System.Drawing.Size(47, 12);
            fsample_codeLabel.TabIndex = 21;
            fsample_codeLabel.Text = "样本号:";
            // 
            // fhz_type_idLabel
            // 
            fhz_type_idLabel.AutoSize = true;
            fhz_type_idLabel.Location = new System.Drawing.Point(408, 17);
            fhz_type_idLabel.Name = "fhz_type_idLabel";
            fhz_type_idLabel.Size = new System.Drawing.Size(59, 12);
            fhz_type_idLabel.TabIndex = 19;
            fhz_type_idLabel.Text = "病人类别:";
            // 
            // fjy_dateLabel
            // 
            fjy_dateLabel.AutoSize = true;
            fjy_dateLabel.Location = new System.Drawing.Point(7, 17);
            fjy_dateLabel.Name = "fjy_dateLabel";
            fjy_dateLabel.Size = new System.Drawing.Size(59, 12);
            fjy_dateLabel.TabIndex = 12;
            fjy_dateLabel.Text = "检验日期:";
            // 
            // finstr_idLabel
            // 
            finstr_idLabel.AutoSize = true;
            finstr_idLabel.Location = new System.Drawing.Point(275, 17);
            finstr_idLabel.Name = "finstr_idLabel";
            finstr_idLabel.Size = new System.Drawing.Size(35, 12);
            finstr_idLabel.TabIndex = 10;
            finstr_idLabel.Text = "仪器:";
            // 
            // lIS_REPORTBindingSource
            // 
            this.lIS_REPORTBindingSource.DataMember = "LIS_REPORT";
            this.lIS_REPORTBindingSource.DataSource = this.samDataSet;
            this.lIS_REPORTBindingSource.PositionChanged += new System.EventHandler(this.lIS_REPORTBindingSource_PositionChanged);
            // 
            // samDataSet
            // 
            this.samDataSet.DataSetName = "samDataSet";
            this.samDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataGridViewReport
            // 
            this.dataGridViewReport.AllowUserToAddRows = false;
            this.dataGridViewReport.AllowUserToDeleteRows = false;
            this.dataGridViewReport.AllowUserToOrderColumns = true;
            this.dataGridViewReport.AllowUserToResizeRows = false;
            this.dataGridViewReport.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewReport.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewReport.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewReport.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewReport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fread_flag,
            this.fexamine_flag,
            this.fprint_flag,
            this.fstate,
            this.fjytype_id,
            this.fsample_type_id,
            this.fapply_user_id,
            this.fapply_dept_id,
            this.fjy_user_id});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewReport.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewReport.Location = new System.Drawing.Point(0, 94);
            this.dataGridViewReport.MultiSelect = false;
            this.dataGridViewReport.Name = "dataGridViewReport";
            this.dataGridViewReport.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dataGridViewReport.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewReport.RowHeadersVisible = false;
            this.dataGridViewReport.RowHeadersWidth = 20;
            this.dataGridViewReport.RowTemplate.Height = 23;
            this.dataGridViewReport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewReport.Size = new System.Drawing.Size(399, 444);
            this.dataGridViewReport.TabIndex = 12;
            this.dataGridViewReport.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewReport_DataError);
            this.dataGridViewReport.Click += new System.EventHandler(this.dataGridViewReport_Click);
            // 
            // com_listBindingSource_fstate
            // 
            this.com_listBindingSource_fstate.DataMember = "com_list";
            this.com_listBindingSource_fstate.DataSource = this.samDataSet;
            // 
            // com_listBindingSource_fjytype_id
            // 
            this.com_listBindingSource_fjytype_id.DataMember = "com_list";
            this.com_listBindingSource_fjytype_id.DataSource = this.samDataSet;
            // 
            // com_listBindingSource_fsample_type_id
            // 
            this.com_listBindingSource_fsample_type_id.DataMember = "com_list";
            this.com_listBindingSource_fsample_type_id.DataSource = this.samDataSet;
            // 
            // com_listBindingSource_fapply_user_id
            // 
            this.com_listBindingSource_fapply_user_id.DataMember = "com_list";
            this.com_listBindingSource_fapply_user_id.DataSource = this.samDataSet;
            // 
            // com_listBindingSource_fapply_dept_id
            // 
            this.com_listBindingSource_fapply_dept_id.DataMember = "com_list";
            this.com_listBindingSource_fapply_dept_id.DataSource = this.samDataSet;
            // 
            // com_listBindingSource_fjy_user_id
            // 
            this.com_listBindingSource_fjy_user_id.DataMember = "com_list";
            this.com_listBindingSource_fjy_user_id.DataSource = this.samDataSet;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(785, 94);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 444);
            this.splitter1.TabIndex = 130;
            this.splitter1.TabStop = false;
            // 
            // dataGridViewResult
            // 
            this.dataGridViewResult.AllowUserToAddRows = false;
            this.dataGridViewResult.AllowUserToResizeRows = false;
            this.dataGridViewResult.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewResult.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewResult.Dock = System.Windows.Forms.DockStyle.Right;
            this.dataGridViewResult.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridViewResult.Location = new System.Drawing.Point(402, 94);
            this.dataGridViewResult.MultiSelect = false;
            this.dataGridViewResult.Name = "dataGridViewResult";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewResult.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewResult.RowHeadersVisible = false;
            this.dataGridViewResult.RowHeadersWidth = 30;
            this.dataGridViewResult.RowTemplate.Height = 23;
            this.dataGridViewResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewResult.ShowCellToolTips = false;
            this.dataGridViewResult.ShowEditingIcon = false;
            this.dataGridViewResult.Size = new System.Drawing.Size(383, 444);
            this.dataGridViewResult.TabIndex = 131;
            this.dataGridViewResult.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewResult_DataError);
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(399, 94);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 444);
            this.splitter2.TabIndex = 133;
            this.splitter2.TabStop = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(229, 344);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(225, 170);
            this.textBox1.TabIndex = 134;
            this.textBox1.Visible = false;
            // 
            // groupBoxif
            // 
            this.groupBoxif.Controls.Add(label4);
            this.groupBoxif.Controls.Add(this.fhz_idtextBox);
            this.groupBoxif.Controls.Add(label3);
            this.groupBoxif.Controls.Add(this.fapply_codetextBox);
            this.groupBoxif.Controls.Add(label2);
            this.groupBoxif.Controls.Add(this.fzyhtextBox);
            this.groupBoxif.Controls.Add(this.fnameTextBox);
            this.groupBoxif.Controls.Add(fnameLabel);
            this.groupBoxif.Controls.Add(this.fsample_codeTextBox);
            this.groupBoxif.Controls.Add(fsample_codeLabel);
            this.groupBoxif.Controls.Add(this.ftype_idComboBox);
            this.groupBoxif.Controls.Add(fhz_type_idLabel);
            this.groupBoxif.Controls.Add(this.label1);
            this.groupBoxif.Controls.Add(this.fjy_dateDateTimePicker2);
            this.groupBoxif.Controls.Add(this.fjy_dateDateTimePicker1);
            this.groupBoxif.Controls.Add(fjy_dateLabel);
            this.groupBoxif.Controls.Add(this.fjy_instrComboBox);
            this.groupBoxif.Controls.Add(finstr_idLabel);
            this.groupBoxif.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxif.Location = new System.Drawing.Point(0, 30);
            this.groupBoxif.Name = "groupBoxif";
            this.groupBoxif.Size = new System.Drawing.Size(788, 64);
            this.groupBoxif.TabIndex = 145;
            this.groupBoxif.TabStop = false;
            // 
            // fhz_idtextBox
            // 
            this.fhz_idtextBox.Location = new System.Drawing.Point(314, 38);
            this.fhz_idtextBox.Name = "fhz_idtextBox";
            this.fhz_idtextBox.Size = new System.Drawing.Size(87, 21);
            this.fhz_idtextBox.TabIndex = 28;
            // 
            // fapply_codetextBox
            // 
            this.fapply_codetextBox.Location = new System.Drawing.Point(629, 13);
            this.fapply_codetextBox.Name = "fapply_codetextBox";
            this.fapply_codetextBox.Size = new System.Drawing.Size(87, 21);
            this.fapply_codetextBox.TabIndex = 26;
            // 
            // fzyhtextBox
            // 
            this.fzyhtextBox.Location = new System.Drawing.Point(629, 38);
            this.fzyhtextBox.Name = "fzyhtextBox";
            this.fzyhtextBox.Size = new System.Drawing.Size(87, 21);
            this.fzyhtextBox.TabIndex = 24;
            // 
            // fnameTextBox
            // 
            this.fnameTextBox.Location = new System.Drawing.Point(72, 38);
            this.fnameTextBox.Name = "fnameTextBox";
            this.fnameTextBox.Size = new System.Drawing.Size(185, 21);
            this.fnameTextBox.TabIndex = 23;
            // 
            // fsample_codeTextBox
            // 
            this.fsample_codeTextBox.ForeColor = System.Drawing.SystemColors.Desktop;
            this.fsample_codeTextBox.Location = new System.Drawing.Point(470, 38);
            this.fsample_codeTextBox.Name = "fsample_codeTextBox";
            this.fsample_codeTextBox.Size = new System.Drawing.Size(87, 21);
            this.fsample_codeTextBox.TabIndex = 20;
            // 
            // ftype_idComboBox
            // 
            this.ftype_idComboBox.DisplayMember = "fname";
            this.ftype_idComboBox.FormattingEnabled = true;
            this.ftype_idComboBox.Items.AddRange(new object[] {
            ""});
            this.ftype_idComboBox.Location = new System.Drawing.Point(470, 13);
            this.ftype_idComboBox.Name = "ftype_idComboBox";
            this.ftype_idComboBox.Size = new System.Drawing.Size(87, 20);
            this.ftype_idComboBox.TabIndex = 18;
            this.ftype_idComboBox.ValueMember = "fcode";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(160, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 12);
            this.label1.TabIndex = 14;
            this.label1.Text = "-";
            // 
            // fjy_dateDateTimePicker2
            // 
            this.fjy_dateDateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker2.Location = new System.Drawing.Point(172, 13);
            this.fjy_dateDateTimePicker2.Name = "fjy_dateDateTimePicker2";
            this.fjy_dateDateTimePicker2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker2.Size = new System.Drawing.Size(85, 21);
            this.fjy_dateDateTimePicker2.TabIndex = 13;
            // 
            // fjy_dateDateTimePicker1
            // 
            this.fjy_dateDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker1.Location = new System.Drawing.Point(72, 13);
            this.fjy_dateDateTimePicker1.Name = "fjy_dateDateTimePicker1";
            this.fjy_dateDateTimePicker1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker1.Size = new System.Drawing.Size(85, 21);
            this.fjy_dateDateTimePicker1.TabIndex = 11;
            // 
            // fjy_instrComboBox
            // 
            this.fjy_instrComboBox.DisplayMember = "ShowName";
            this.fjy_instrComboBox.FormattingEnabled = true;
            this.fjy_instrComboBox.Items.AddRange(new object[] {
            ""});
            this.fjy_instrComboBox.Location = new System.Drawing.Point(314, 13);
            this.fjy_instrComboBox.Name = "fjy_instrComboBox";
            this.fjy_instrComboBox.Size = new System.Drawing.Size(87, 20);
            this.fjy_instrComboBox.TabIndex = 9;
            this.fjy_instrComboBox.ValueMember = "finstr_id";
            // 
            // bN
            // 
            this.bN.AddNewItem = null;
            this.bN.AutoSize = false;
            this.bN.BindingSource = this.lIS_REPORTBindingSource;
            this.bN.CountItem = this.bindingNavigatorCountItem;
            this.bN.DeleteItem = null;
            this.bN.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.toolStripSeparator5,
            this.toolStripButtonQuery,
            this.toolStripButtonPrint,
            this.toolStripButtonPrintYL,
            this.toolStripButtonJCSH,
            this.toolStripSeparator6,
            this.toolStripSplitButtonGZ,
            this.toolStripButtonHelp});
            this.bN.Location = new System.Drawing.Point(0, 0);
            this.bN.MoveFirstItem = null;
            this.bN.MoveLastItem = null;
            this.bN.MoveNextItem = null;
            this.bN.MovePreviousItem = null;
            this.bN.Name = "bN";
            this.bN.PositionItem = this.bindingNavigatorPositionItem;
            this.bN.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bN.Size = new System.Drawing.Size(788, 30);
            this.bN.TabIndex = 146;
            this.bN.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 27);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 30);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonQuery
            // 
            this.toolStripButtonQuery.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonQuery.Image")));
            this.toolStripButtonQuery.Name = "toolStripButtonQuery";
            this.toolStripButtonQuery.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonQuery.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonQuery.Size = new System.Drawing.Size(79, 27);
            this.toolStripButtonQuery.Text = "查询(F5) ";
            this.toolStripButtonQuery.Click += new System.EventHandler(this.toolStripButtonQuery_Click);
            // 
            // toolStripButtonPrint
            // 
            this.toolStripButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPrint.Image")));
            this.toolStripButtonPrint.Name = "toolStripButtonPrint";
            this.toolStripButtonPrint.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonPrint.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonPrint.Size = new System.Drawing.Size(79, 27);
            this.toolStripButtonPrint.Text = "打印(F8) ";
            this.toolStripButtonPrint.Click += new System.EventHandler(this.toolStripButtonPrint_Click);
            // 
            // toolStripButtonPrintYL
            // 
            this.toolStripButtonPrintYL.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPrintYL.Image")));
            this.toolStripButtonPrintYL.Name = "toolStripButtonPrintYL";
            this.toolStripButtonPrintYL.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonPrintYL.Size = new System.Drawing.Size(103, 27);
            this.toolStripButtonPrintYL.Text = "打印预览(F9) ";
            this.toolStripButtonPrintYL.Click += new System.EventHandler(this.toolStripButtonPrintYL_Click);
            // 
            // toolStripButtonJCSH
            // 
            this.toolStripButtonJCSH.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonJCSH.Image")));
            this.toolStripButtonJCSH.Name = "toolStripButtonJCSH";
            this.toolStripButtonJCSH.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonJCSH.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonJCSH.Size = new System.Drawing.Size(103, 27);
            this.toolStripButtonJCSH.Text = "解除审核(F10)";
            this.toolStripButtonJCSH.Visible = false;
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripSplitButtonGZ
            // 
            this.toolStripSplitButtonGZ.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.报表格式ToolStripMenuItem,
            this.结果模式ToolStripMenuItem});
            this.toolStripSplitButtonGZ.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButtonGZ.Image")));
            this.toolStripSplitButtonGZ.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButtonGZ.Name = "toolStripSplitButtonGZ";
            this.toolStripSplitButtonGZ.Size = new System.Drawing.Size(73, 27);
            this.toolStripSplitButtonGZ.Text = "格式  ";
            // 
            // 报表格式ToolStripMenuItem
            // 
            this.报表格式ToolStripMenuItem.Name = "报表格式ToolStripMenuItem";
            this.报表格式ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.报表格式ToolStripMenuItem.Text = "报表格式(&R)";
            this.报表格式ToolStripMenuItem.Click += new System.EventHandler(this.报表格式ToolStripMenuItem_Click);
            // 
            // 结果模式ToolStripMenuItem
            // 
            this.结果模式ToolStripMenuItem.Name = "结果模式ToolStripMenuItem";
            this.结果模式ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.结果模式ToolStripMenuItem.Text = "结果模式(&S)";
            this.结果模式ToolStripMenuItem.Click += new System.EventHandler(this.结果模式ToolStripMenuItem_Click);
            // 
            // toolStripButtonHelp
            // 
            this.toolStripButtonHelp.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHelp.Image")));
            this.toolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonHelp.Name = "toolStripButtonHelp";
            this.toolStripButtonHelp.Size = new System.Drawing.Size(85, 27);
            this.toolStripButtonHelp.Text = "帮助(F1)  ";
            this.toolStripButtonHelp.Click += new System.EventHandler(this.toolStripButtonHelp_Click);
            // 
            // fread_flag
            // 
            this.fread_flag.DataPropertyName = "fread_flag";
            this.fread_flag.FalseValue = "0";
            this.fread_flag.HeaderText = "fread_flag";
            this.fread_flag.IndeterminateValue = "0";
            this.fread_flag.Name = "fread_flag";
            this.fread_flag.TrueValue = "1";
            // 
            // fexamine_flag
            // 
            this.fexamine_flag.DataPropertyName = "fexamine_flag";
            this.fexamine_flag.FalseValue = "0";
            this.fexamine_flag.HeaderText = "fexamine_flag";
            this.fexamine_flag.IndeterminateValue = "0";
            this.fexamine_flag.Name = "fexamine_flag";
            this.fexamine_flag.ReadOnly = true;
            this.fexamine_flag.TrueValue = "1";
            // 
            // fprint_flag
            // 
            this.fprint_flag.DataPropertyName = "fprint_flag";
            this.fprint_flag.FalseValue = "0";
            this.fprint_flag.HeaderText = "fprint_flag";
            this.fprint_flag.IndeterminateValue = "0";
            this.fprint_flag.Name = "fprint_flag";
            this.fprint_flag.ReadOnly = true;
            this.fprint_flag.TrueValue = "1";
            // 
            // fstate
            // 
            this.fstate.DataPropertyName = "fstate";
            this.fstate.DataSource = this.com_listBindingSource_fstate;
            this.fstate.DisplayMember = "fname";
            this.fstate.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.fstate.HeaderText = "fstate";
            this.fstate.Name = "fstate";
            this.fstate.ValueMember = "fcode";
            // 
            // fjytype_id
            // 
            this.fjytype_id.DataPropertyName = "fjytype_id";
            this.fjytype_id.DataSource = this.com_listBindingSource_fjytype_id;
            this.fjytype_id.DisplayMember = "fname";
            this.fjytype_id.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.fjytype_id.HeaderText = "fjytype_id";
            this.fjytype_id.Name = "fjytype_id";
            this.fjytype_id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fjytype_id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fjytype_id.ValueMember = "fcheck_type_id";
            // 
            // fsample_type_id
            // 
            this.fsample_type_id.DataPropertyName = "fsample_type_id";
            this.fsample_type_id.DataSource = this.com_listBindingSource_fsample_type_id;
            this.fsample_type_id.DisplayMember = "fname";
            this.fsample_type_id.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.fsample_type_id.HeaderText = "fsample_type_id";
            this.fsample_type_id.Name = "fsample_type_id";
            this.fsample_type_id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fsample_type_id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fsample_type_id.ValueMember = "fsample_type_id";
            // 
            // fapply_user_id
            // 
            this.fapply_user_id.DataPropertyName = "fapply_user_id";
            this.fapply_user_id.DataSource = this.com_listBindingSource_fapply_user_id;
            this.fapply_user_id.DisplayMember = "fname";
            this.fapply_user_id.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.fapply_user_id.HeaderText = "fapply_user_id";
            this.fapply_user_id.Name = "fapply_user_id";
            this.fapply_user_id.ValueMember = "fperson_id";
            // 
            // fapply_dept_id
            // 
            this.fapply_dept_id.DataPropertyName = "fapply_dept_id";
            this.fapply_dept_id.DataSource = this.com_listBindingSource_fapply_dept_id;
            this.fapply_dept_id.DisplayMember = "fname";
            this.fapply_dept_id.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.fapply_dept_id.HeaderText = "fapply_dept_id";
            this.fapply_dept_id.Name = "fapply_dept_id";
            this.fapply_dept_id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fapply_dept_id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fapply_dept_id.ValueMember = "fdept_id";
            // 
            // fjy_user_id
            // 
            this.fjy_user_id.DataPropertyName = "fjy_user_id";
            this.fjy_user_id.DataSource = this.com_listBindingSource_fjy_user_id;
            this.fjy_user_id.DisplayMember = "fname";
            this.fjy_user_id.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.fjy_user_id.HeaderText = "fjy_user_id";
            this.fjy_user_id.Name = "fjy_user_id";
            this.fjy_user_id.ValueMember = "fperson_id";
            // 
            // ReportQueryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 538);
            this.Controls.Add(this.dataGridViewReport);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dataGridViewResult);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.groupBoxif);
            this.Controls.Add(this.bN);
            this.Name = "ReportQueryForm";
            this.Text = "ReportQueryForm";
            this.Load += new System.EventHandler(this.ReportQueryForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIS_REPORTBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fstate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fjytype_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fsample_type_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fapply_user_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fapply_dept_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fjy_user_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceResult)).EndInit();
            this.groupBoxif.ResumeLayout(false);
            this.groupBoxif.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).EndInit();
            this.bN.ResumeLayout(false);
            this.bN.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewReport;
        private samDataSet samDataSet;
        private System.Windows.Forms.BindingSource lIS_REPORTBindingSource;
        private System.Windows.Forms.BindingSource com_listBindingSource_fstate;
        private System.Windows.Forms.BindingSource com_listBindingSource_fjytype_id;
        private System.Windows.Forms.BindingSource com_listBindingSource_fsample_type_id;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.BindingSource com_listBindingSource_fjy_user_id;
        private System.Windows.Forms.BindingSource com_listBindingSource_fapply_user_id;
        private System.Windows.Forms.BindingSource com_listBindingSource_fapply_dept_id;
        private System.Windows.Forms.DataGridView dataGridViewResult;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.BindingSource bindingSourceResult;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox groupBoxif;
        private System.Windows.Forms.TextBox fhz_idtextBox;
        private System.Windows.Forms.TextBox fapply_codetextBox;
        private System.Windows.Forms.TextBox fzyhtextBox;
        private System.Windows.Forms.TextBox fnameTextBox;
        private System.Windows.Forms.TextBox fsample_codeTextBox;
        private System.Windows.Forms.ComboBox ftype_idComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker2;
        private System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker1;
        private System.Windows.Forms.ComboBox fjy_instrComboBox;
        protected System.Windows.Forms.BindingNavigator bN;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        protected System.Windows.Forms.ToolStripButton toolStripButtonQuery;
        private System.Windows.Forms.ToolStripButton toolStripButtonPrint;
        private System.Windows.Forms.ToolStripButton toolStripButtonPrintYL;
        private System.Windows.Forms.ToolStripButton toolStripButtonJCSH;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButtonGZ;
        private System.Windows.Forms.ToolStripMenuItem 报表格式ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 结果模式ToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButtonHelp;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fread_flag;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fexamine_flag;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fprint_flag;
        private System.Windows.Forms.DataGridViewComboBoxColumn fstate;
        private System.Windows.Forms.DataGridViewComboBoxColumn fjytype_id;
        private System.Windows.Forms.DataGridViewComboBoxColumn fsample_type_id;
        private System.Windows.Forms.DataGridViewComboBoxColumn fapply_user_id;
        private System.Windows.Forms.DataGridViewComboBoxColumn fapply_dept_id;
        private System.Windows.Forms.DataGridViewComboBoxColumn fjy_user_id;
    }
}