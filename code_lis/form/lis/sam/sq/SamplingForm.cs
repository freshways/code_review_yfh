﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.form.wwf;
using ww.lis.lisbll.sam;
using ww.wwf.wwfbll;
using System.Collections;


namespace ww.form.lis.sam.sq
{
    public partial class SamplingForm : SysBaseForm
    {
        ApplyBLL bllApply = new ApplyBLL();//申请规则
        DataTable dtMain = new DataTable();
        DataTable dtResult = new DataTable();
        private ww.wwf.wwfbll.UserdefinedFieldUpdateForm gridMain = new ww.wwf.wwfbll.UserdefinedFieldUpdateForm("6");
        private ww.wwf.wwfbll.UserdefinedFieldUpdateForm gridResult = new ww.wwf.wwfbll.UserdefinedFieldUpdateForm("7");
        public SamplingForm()
        {
            InitializeComponent();
            //this.dataGridViewApply.AutoGenerateColumns = false;
            //this.dataGridViewItem.AutoGenerateColumns = false;
        }

        private void SamplingForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.ActiveControl = this.tbfsample_barcode;
                this.labelMess.Text = "";
                string strDateif = " (t1.fapply_time>='" +  System.DateTime.Now + "')";
                dtMain = this.bllApply.BllSamplingMainDT(strDateif);                //" t2.fsample_barcode='-1'"
                dataGridViewApply.DataSource = dtMain;

                this.dtResult = this.bllApply.BllSamplingResultDT("-1");
                this.dataGridViewItem.DataSource = this.dtResult;

                this.gridMain.DataGridViewSetStyle(this.dataGridViewApply);
                this.gridResult.DataGridViewSetStyle(this.dataGridViewItem);
                
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        #region 方法
        private void GetMailList()
        {
            this.bindingSource1.DataSource = this.bllApply.BllSamplingMainDT("t2.fsample_barcode='" + this.tbfsample_barcode.Text + "'");
            this.dataGridViewApply.DataSource = this.bindingSource1;
            this.tbfsample_barcode.Text = "";
            this.ActiveControl = this.tbfsample_barcode;
        }
        private void GetResultList(string fapply_id)
        {
            this.dtResult = this.bllApply.BllSamplingResultDT(fapply_id);
            this.dataGridViewItem.DataSource = this.dtResult;
        }
        private void SaveSampling()
        {
            this.Validate();
            this.bindingSource1.EndEdit();
            IList ilistfsample_id = new ArrayList();
            for (int i = 0; i < this.dataGridViewApply.Rows.Count; i++)
            {
                //fsample_id
                string strid = dataGridViewApply.Rows[i].Cells["fsample_id"].Value.ToString();
                ilistfsample_id.Add(strid);
              
            }
            string strsaveret = this.bllApply.BllSamplingSave(ilistfsample_id);
            if (strsaveret == "true")
                this.labelMess.Text = "采样记录成功！";
            else
                this.labelMess.Text = "采样记录失败！" + strsaveret;
        }
        #endregion


        private void bindingSource1_PositionChanged(object sender, EventArgs e)
        {
            try
            {

                DataRowView rowCurrent = (DataRowView)this.bindingSource1.Current;
                if (this.dtResult.Rows.Count>0)
                    this.dtResult.Clear();
                if (rowCurrent != null)
                {
                    string strfapply_id = rowCurrent["fapply_id"].ToString();
                    GetResultList(strfapply_id);
                }               

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void tbfsample_barcode_KeyDown(object sender, KeyEventArgs e)
        {
            this.labelMess.Text = "";
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Space))//或(e.KeyCode==Keys.Enter) 
            {
                try
                {
                    Cursor = Cursors.WaitCursor;
                    GetMailList();
                    SaveSampling();
                }
                catch (Exception ex)
                {
                    WWMessage.MessageShowError(ex.ToString());
                }
                finally
                {
                    Cursor = Cursors.Arrow;
                }
            }
        }

        private void toolStripMenuItem_applygs_Click(object sender, EventArgs e)
        {
            try
            {
               // UserdefinedFieldUpdateForm userset1 = new UserdefinedFieldUpdateForm(gridMain);//主表ID 报告
                gridMain.ShowDialog();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripMenuItem_checkidtemgs_Click(object sender, EventArgs e)
        {
            try
            {
                //UserdefinedFieldUpdateForm userset1 = new UserdefinedFieldUpdateForm(this.gridResult);
                gridResult.ShowDialog();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 关闭ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void 帮助ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                HelpForm help = new HelpForm("c629e6a854134cb2a24fc6465bc35b77");
                help.Show();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        

        private void radioButtonDay_Click(object sender, EventArgs e)
        {
            try
            {
                this.labelMess.Text = "";
                DateTime dtp = Convert.ToDateTime(this.bllApply.DbServerDateTim());
                string strYear = dtp.Year.ToString();
                string strMonth = dtp.Month.ToString();
                string strDay = dtp.Day.ToString();

                string strok = "";
                if (strMonth.Length == 1)
                    strMonth = "0" + strMonth;

                if (strDay.Length == 1)
                    strDay = "0" + strDay;

                strok = strYear + "-" + strMonth + "-" + strDay;




                string strDateif = " (t1.fapply_time>='" + strok + " 00:00:00" + "' and t1.fapply_time<='" + strok + " 23:59:59" + "')";

                dtMain = this.bllApply.BllSamplingMainDT(strDateif);
                this.bindingSource1.DataSource = dtMain;
                dataGridViewApply.DataSource = bindingSource1;







                string strfstate="";
                for (int intGrid = 0; intGrid < this.dataGridViewApply.Rows.Count; intGrid++)
                {
                    strfstate = "";
                    if (this.dataGridViewApply.Rows[intGrid].Cells["fsampling_time"].Value != null)
                        strfstate = this.dataGridViewApply.Rows[intGrid].Cells["fsampling_time"].Value.ToString();
                    else
                        strfstate = "";

                    if (strfstate == "" || strfstate == null)
                        this.dataGridViewApply.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Red; //System.Drawing.
                    else
                        this.dataGridViewApply.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Blue; //System.Drawing.
                }


            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
    }
}