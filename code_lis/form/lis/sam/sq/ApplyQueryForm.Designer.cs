﻿namespace ww.form.lis.sam.sq
{
    partial class ApplyQueryForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.dtMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtResult)).BeginInit();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // wwTreeViewHZ
            // 
            this.wwTreeViewHZ.LineColor = System.Drawing.Color.Black;
            this.wwTreeViewHZ.Size = new System.Drawing.Size(197, 324);
            // 
            // ApplyQueryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 397);
            this.Name = "ApplyQueryForm";
            this.Text = "ApplyQueryForm";
            this.Load += new System.EventHandler(this.ApplyQueryForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtResult)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}