﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.form.wwf;
using ww.lis.lisbll.sam;
using ww.wwf.wwfbll;
using System.Collections;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;
using TableXtraReport;
using ww.wwf.com;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
namespace ww.form.lis.sam.sq
{
    public partial class SamplingBarCodeForm : SysBaseForm
    {
        ApplyBLL bllApply = new ApplyBLL();//申请规则
        DataTable dtMain = new DataTable();
        DataTable dtResult = new DataTable();
        private ww.wwf.wwfbll.UserdefinedFieldUpdateForm gridMain = new ww.wwf.wwfbll.UserdefinedFieldUpdateForm("8");
        private ww.wwf.wwfbll.UserdefinedFieldUpdateForm gridResult = new ww.wwf.wwfbll.UserdefinedFieldUpdateForm("9");
        string str申请单id = "";

        //2019-10-30 yfh 添加，读取电子健康卡
        private ScanerHook listener = new ScanerHook();
        public SamplingBarCodeForm()
        {
            InitializeComponent();
            listener.ScanerEvent += Listener_ScanerEvent;
        }
        private void Listener_ScanerEvent(ScanerHook.ScanerCodes codes)
        {
            string code = codes.Result.Replace(';', ':');
            if (!string.IsNullOrEmpty(code))
            {
                JObject user = Common.ExecuteReq(code);
                if (user != null)
                {
                    this.textBoxName.Text = user["user_name"].ToString();
                    this.textBoxSFZH.Text = user["id_no"].ToString();
                    listener.Stop();
                }
                //ExecuteReq(code);
            }
        }

        //获取读卡标志
        private void GetReadFlag()
        {
            bool readflag = false;
            try
            {
                object obj = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn,
                    "select [参数值] from [dbo].[全局参数] where [参数名称]='read'");
                if (obj.ToString() == "1")
                {
                    readflag = true;
                }
            }
            catch
            {}

            btnReadCardAndQuery.Visible = readflag;
            if (readflag == false)
            {
                button1.Height = 54;
            }
            else
            {
                button1.Height = 23;
            }
        }

        private void SamplingBarCodeForm_Load(object sender, EventArgs e)
        {
            GetReadFlag();
            //del by wjz 20160427 画面加载时不再自动查询 ▽
            try
            {
                dtMain = new DataTable();
                string fjy_date1 = this.bllApply.DbDateTime1(fjy_dateDateTimePicker1);
                string fjy_date2 = this.bllApply.DbDateTime2(fjy_dateDateTimePicker2);
                string strDateif = " (t1.fapply_time>='" + fjy_date1 + "' and t1.fapply_time<='" + fjy_date2 + "')";
                if (txt门诊住院号.Text == "" || txt门诊住院号.Text == null)
                    strDateif = " (t1.fapply_time>='" + fjy_date1 + "' and t1.fapply_time<='" + fjy_date2 + "')";
                else
                    strDateif = " (t1.fhz_id='" + txt门诊住院号.Text + "')";
                dtMain = this.bllApply.BllSamplingMainDT(strDateif);//" t2.fsample_barcode='-1'"
                dataGridViewApply.DataSource = dtMain;
                this.dtResult = this.bllApply.BllSamplingResultDT("-1");
                this.dataGridViewItem.DataSource = this.dtResult;
                this.gridMain.DataGridViewSetStyle(this.dataGridViewApply);
                this.gridResult.DataGridViewSetStyle(this.dataGridViewItem);

                //GetDT();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            //del by wjz 20160427 画面加载时不再自动查询 △

            //add by wjz 20160427 画面查询时设置默认时间的查询方式 ▽
            comboBoxDateQueryType.SelectedIndex = 0;
            //add by wjz 20160427 画面查询时设置默认时间的查询方式 △
        }
        private void GetDT()
        {
            //add by wjz 20160428 添加查询提示，提示是否查到了数据 ▽
            labelNotify.ForeColor = Color.Black;
            //add by wjz 20160428 添加查询提示，提示是否查到了数据 △

            try
            {
                dtMain = new DataTable();
                string fjy_date1 = this.bllApply.DbDateTime1(fjy_dateDateTimePicker1);
                string fjy_date2 = this.bllApply.DbDateTime2(fjy_dateDateTimePicker2);

                //changed by wjz 20160427 添加社保卡的使用，顺便修改查询条件 ▽
                //string strDateif = " (t1.fapply_time>='" + fjy_date1 +  "' and t1.fapply_time<='" + fjy_date2  + "')";
                //if (txt门诊住院号.Text == "" || txt门诊住院号.Text==null)
                //    strDateif = " (t1.fapply_time>='" + fjy_date1 +  "' and t1.fapply_time<='" + fjy_date2  + "')";
                //else
                //    strDateif = " (t1.fhz_zyh='" + txt门诊住院号.Text + "')";
                StringBuilder strDateif = new StringBuilder("1=1 ");
                if (comboBoxDateQueryType.SelectedIndex == 0)//表示现在的查询条件是 条码生成时间
                {
                    strDateif.Append(" and (t1.fcreate_time>='" + fjy_date1 + "' and t1.fcreate_time<='" + fjy_date2 + "')");
                }
                else if (comboBoxDateQueryType.SelectedIndex == 1)//表示现在的查询条件为申请时间
                {
                    strDateif.Append(" and (t1.fapply_time>='" + fjy_date1 + "' and t1.fapply_time<='" + fjy_date2 + "')");
                }
                else
                {
                    MessageBox.Show("请选择时间类型。（条码生成时间 或 申请时间）");
                    return;
                }

                if (string.IsNullOrWhiteSpace(txt门诊住院号.Text) || txt门诊住院号.Text.Contains("--")
                    || txt门诊住院号.Text.Contains("\n") || txt门诊住院号.Text.Contains("\r")
                    || txt门诊住院号.Text.Contains(" ") || txt门诊住院号.Text.Length > 15)
                { }
                else
                {
                    strDateif.Append(" and (t1.fhz_zyh='" + txt门诊住院号.Text + "') ");
                }

                if(string.IsNullOrWhiteSpace(textBoxName.Text) || textBoxName.Text.Length>6
                    || textBoxName.Text.Contains("--") || textBoxName.Text.Contains(" ")
                    || textBoxName.Text.Contains("\n") || textBoxName.Text.Contains("\r"))
                {
                }
                else
                {
                    strDateif.Append(" and t1.fname='"+textBoxName.Text.Trim()+"' ");
                }

                if (string.IsNullOrWhiteSpace(textBoxSFZH.Text) || textBoxSFZH.Text.Length > 6
                    || textBoxSFZH.Text.Contains("--") || textBoxSFZH.Text.Contains(" ")
                    || textBoxSFZH.Text.Contains("\n") || textBoxSFZH.Text.Contains("\r"))
                {
                }
                else
                {
                    strDateif.Append(" and t1.sfzh='"+textBoxSFZH.Text.Trim()+"' ");
                }
                //changed by wjz 20160427 添加社保卡的使用，顺便修改查询条件 △

                dtMain = this.bllApply.BllSamplingMainDT(strDateif.ToString());

                //add by wjz 20160428 添加查询提示，提示是否查到了数据 ▽
                if (dtMain == null || dtMain.Rows.Count == 0)
                {
                    labelNotify.ForeColor = Color.Red;
                    labelNotify.Text = "未查到任何数据";
                }
                //add by wjz 20160428 添加查询提示，提示是否查到了数据 △

                this.bindingSource1.DataSource = dtMain;
                dataGridViewApply.DataSource = bindingSource1;
                //int intfstate = 0;
                //for (int intGrid = 0; intGrid < this.dataGridViewApply.Rows.Count; intGrid++)
                //{
                //    intfstate = 0;
                //    if (this.dataGridViewApply.Rows[intGrid].Cells["fstate"].Value != null)
                //        intfstate = Convert.ToInt32(this.dataGridViewApply.Rows[intGrid].Cells["fstate"].Value.ToString());
                //    //MessageBox.Show(intfstate.ToString());
                //    if (intfstate == 1)
                //        this.dataGridViewApply.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Red;
                //    if (intfstate == 2)
                //        this.dataGridViewApply.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Blue;
                //}
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void GetResultList(string fapply_id)
        {
            this.dtResult = this.bllApply.BllSamplingResultDT(fapply_id);
            this.dataGridViewItem.DataSource = this.dtResult;
        }
        private void toolStripMenuItem_applygs_Click(object sender, EventArgs e)
        {
            try
            {
                // UserdefinedFieldUpdateForm userset1 = new UserdefinedFieldUpdateForm(gridMain);//主表ID 报告
                gridMain.ShowDialog();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripMenuItem_checkidtemgs_Click(object sender, EventArgs e)
        {
            try
            {
                //UserdefinedFieldUpdateForm userset1 = new UserdefinedFieldUpdateForm(this.gridResult);
                gridResult.ShowDialog();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 帮助ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                HelpForm help = new HelpForm("68f5df6b5aa14f86829ab0995b58dc59");
                help.Show();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 关闭ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void fjy_dateDateTimePicker1_CloseUp(object sender, EventArgs e)
        {
            GetDT();
        }

        private void fjy_dateDateTimePicker2_CloseUp(object sender, EventArgs e)
        {
            GetDT();
        }

        private void tbfhz_id_KeyDown(object sender, KeyEventArgs e)
        {
           
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Space))//或(e.KeyCode==Keys.Enter) 
            {
                try
                {
                    Cursor = Cursors.WaitCursor;
                    GetDT();
                    txt门诊住院号.Text = "";
                   
                }
                catch (Exception ex)
                {
                    WWMessage.MessageShowError(ex.ToString());
                }
                finally
                {
                    Cursor = Cursors.Arrow;
                }
            }
        }

        private void bindingSource1_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                DataRowView rowCurrent = (DataRowView)this.bindingSource1.Current;
                if (dtResult!=null && this.dtResult.Rows.Count > 0)
                    this.dtResult.Clear();
                if (rowCurrent != null)
                {
                    string strfapply_id = rowCurrent["fapply_id"].ToString();
                    str申请单id = strfapply_id;
                    GetResultList(strfapply_id);
                }

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonHelp_Click(object sender, EventArgs e)
        {
            try
            {
                HelpForm help = new HelpForm("68f5df6b5aa14f86829ab0995b58dc59");
                help.Show();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

         

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                ww.form.lis.sam.sq.ApplyPrint pr = new ww.form.lis.sam.sq.ApplyPrint();
                pr.BllPrintApplyBCode(1, this.str申请单id);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void fjy_dateDateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            GetDT();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            try
            {
                HelpForm help = new HelpForm("68f5df6b5aa14f86829ab0995b58dc59");
                help.Show();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            try
            {
                ToolStripButton but = (ToolStripButton)sender;

                string str条码 = dataGridViewApply.CurrentRow.Cells["his_recordid"].Value.ToString();

                DataRow[] drs = dtMain.Select("his_recordid='" + str条码 + "'", "fapply_id asc");

                string 检查项目 = "";
                foreach (DataRow dr in drs)
                    检查项目 += dr["fitem_group"].ToString() + ",";
                检查项目 = 检查项目.Substring(0, 检查项目.Length - 1);

                int index = dataGridViewApply.CurrentRow.Index;
                //string s申请单号 = dataGridViewApply.Rows[index].Cells["fapply_id"].Value.ToString();// rowCurrent["fhz_zyh"].ToString();
                string s姓名 = dataGridViewApply.Rows[index].Cells["fname"].Value.ToString(); //rowCurrent["fname"].ToString();
                string s性别 = dataGridViewApply.Rows[index].Cells["fsex"].Value.ToString();
                string s年龄 = dataGridViewApply.Rows[index].Cells["fage"].Value.ToString();
                string s类型 = dataGridViewApply.Rows[index].Cells["ftype_id"].Value.ToString();
                //string s检查 = dataGridViewApply.Rows[index].Cells["fitem_group"].Value.ToString();
                string s申请医生 = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn, "SELECT fname FROM WWF_PERSON where fperson_id='" +
                    dataGridViewApply.Rows[index].Cells["fapply_user_id"].Value.ToString() + "'");
                if (but.Name == "toolStripButton打印预览")
                    Print(s姓名, s性别, s年龄, s类型, 检查项目, str条码, true, s申请医生);
                else
                    Print(s姓名, s性别, s年龄, s类型, 检查项目, str条码, false, s申请医生);                
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void Print(string s姓名, string s性别, string s年龄, string s类型, string s检查, string s申请单号, bool 是否预览, string s申请医生)
        {
            TableXReport xtr = new TableXReport();
            Font ft = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            Font ft7 = new System.Drawing.Font("宋体", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));

            //add by wjz 20160122 添加检查项目的排序 ▽
            string[] sub检查项目 = s检查.Split(',');
            if (sub检查项目.Length == 1)
            { }
            else
            {
                Array.Sort(sub检查项目);
                s检查 = sub检查项目[0];
                for (int index = 1; index < sub检查项目.Length; index++)
                {
                    s检查 += "," + sub检查项目[index];
                }
            }
            //add by wjz 20160122 添加检查项目的排序 △

            #region add by wjz 修改此方法的实现，化验项目如果在一个条码纸上打不开的话，可以用分页打印 ▽
            #region olde code
            //xtr.SetReportTitle("打印时间 " + System.DateTime.Now.ToString(), true, ft, TextAlignment.TopLeft, Size.Empty);
            //xtr.SetReportTitle(s姓名 + "/" + s性别 + "/" + s年龄 + "岁  " + s类型 + " " + s申请医生, true, ft, TextAlignment.TopLeft, Size.Empty);

            //int size = 20;

            //if (s检查.Length > size)
            //{
            //    xtr.SetReportTitle(s检查.Substring(0, size), true, ft7, TextAlignment.TopLeft, Size.Empty);
            //    xtr.SetReportTitle(s检查.Substring(size), true, ft7, TextAlignment.TopLeft, Size.Empty);
            //}
            //else
            //{
            //    xtr.SetReportTitle(s检查, true, ft7, TextAlignment.TopLeft, Size.Empty);
            //}
            
            ////xtr.SetReportTitle("测试用下2 ", false, ft, TextAlignment.TopRight, Size.Empty);
            //xtr.SetReportBarCode(s申请单号, new Point(5, 150), new Size(400, 125), null, true);
            ////SetReportMain只能最后设置一次
            ////xtr.SetReportMain(100, 12, 2, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(10, 100, 10, 100), System.Drawing.Printing.PaperKind.A4, Size.Empty, 0, 0, "");           
            //xtr.SetReportMain(0, 0, 1, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(20, 20, 10, 10), System.Drawing.Printing.PaperKind.Custom, new Size(500, 315), 0, 0, "");
            ////xtr.UnitAfterPrint += new XReportUnitAfterPrint(xtr_UnitAfterPrint);
            ////xtr.Print("ZDesigner GK888t");
            #endregion

            int size = 20;
            if (s检查.Length < size * 2)
            {

                xtr.SetReportTitle("打印时间 " + System.DateTime.Now.ToString(), true, ft, TextAlignment.TopLeft, Size.Empty);
                xtr.SetReportTitle(s姓名 + "/" + s性别 + "/" + s年龄 + "岁  " + s类型 + " " + s申请医生, true, ft, TextAlignment.TopLeft, Size.Empty);

                if (s检查.Length > size)
                {
                    xtr.SetReportTitle(s检查.Substring(0, size), true, ft7, TextAlignment.TopLeft, Size.Empty);
                    xtr.SetReportTitle(s检查.Substring(size), true, ft7, TextAlignment.TopLeft, Size.Empty);
                }
                else
                {
                    xtr.SetReportTitle(s检查, true, ft7, TextAlignment.TopLeft, Size.Empty);
                }

                //xtr.SetReportTitle("测试用下2 ", false, ft, TextAlignment.TopRight, Size.Empty);
                xtr.SetReportBarCode(s申请单号, new Point(5, 150), new Size(400, 125), null, true);
                //SetReportMain只能最后设置一次
                //xtr.SetReportMain(100, 12, 2, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(10, 100, 10, 100), System.Drawing.Printing.PaperKind.A4, Size.Empty, 0, 0, "");           
                xtr.SetReportMain(0, 0, 1, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(10, 10, 10, 10), System.Drawing.Printing.PaperKind.Custom, new Size(500, 315), 0, 0, "");
                //xtr.UnitAfterPrint += new XReportUnitAfterPrint(xtr_UnitAfterPrint);
                //xtr.Print("ZDesigner GK888t");
            }
            else
            {
                string[] arr检查 = s检查.Split(',');
                List<string> list检验 = new List<string>();

                list检验.Add(arr检查[0]);
                for (int index = 1; index < arr检查.Length; index++)
                {
                    if (list检验[list检验.Count - 1].Length + arr检查[index].Length < size * 2)
                    {
                        list检验[list检验.Count - 1] = list检验[list检验.Count - 1] + "," + arr检查[index];
                    }
                    else
                    {
                        list检验.Add(arr检查[index]);
                    }
                }

                xtr.SetReportTitle("打印时间 " + System.DateTime.Now.ToString(), true, ft, TextAlignment.TopLeft, Size.Empty);
                xtr.SetReportTitle(s姓名 + "/" + s性别 + "/" + s年龄 + "岁  " + s类型 + " " + s申请医生, true, ft, TextAlignment.TopLeft, Size.Empty);


                if (list检验[0].Length > size)
                {
                    xtr.SetReportTitle(list检验[0].Substring(0, size), true, ft7, TextAlignment.TopLeft, Size.Empty);
                    xtr.SetReportTitle(list检验[0].Substring(size), true, ft7, TextAlignment.TopLeft, Size.Empty);
                }
                else
                {
                    xtr.SetReportTitle(list检验[0], true, ft7, TextAlignment.TopLeft, Size.Empty);
                }

                xtr.SetReportBarCode(s申请单号, new Point(5, 150), new Size(400, 125), null, true);
                xtr.SetReportMain(0, 0, 1, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(10, 10, 10, 10), System.Drawing.Printing.PaperKind.Custom, new Size(500, 315), 0, 0, "");

                xtr.CreateDocument();

                for (int index = 1; index < list检验.Count; index++)
                {
                    CombineReports(xtr, ft, ft7, s姓名, s性别, s年龄, s类型, list检验[index], s申请单号, s申请医生);
                }

                xtr.PrintingSystem.ContinuousPageNumbering = true;
            }

            #endregion

            xtr.PrintProgress +=xtr_PrintProgress;
            if (是否预览)
                xtr.ShowPreviewDialog();
            else
                xtr.Print(ww.form.Properties.Settings.Default.CodePrit);
        }

        //add by wjz 20151226 一个条码纸上可能无法将所有的项目全部打印出来，添加此方法的目的：实现分页打印 ▽
        private void CombineReports(TableXReport firstRep, Font ft9, Font ft7,
                                    string s姓名, string s性别, string s年龄, string s类型, string sub检查, string s申请单号, string s申请医生)
        {
            TableXReport subRep = new TableXReport();
            subRep.SetReportTitle("打印时间 " + System.DateTime.Now.ToString(), true, ft9, TextAlignment.TopLeft, Size.Empty);
            subRep.SetReportTitle(s姓名 + "/" + s性别 + "/" + s年龄 + "岁  " + s类型 + " " + s申请医生, true, ft9, TextAlignment.TopLeft, Size.Empty);

            //xtr.SetReportTitle(s检查, true, ft, TextAlignment.TopLeft, Size.Empty);
            int size = 20;
            if (sub检查.Length > size)
            {
                subRep.SetReportTitle(sub检查.Substring(0, size), true, ft7, TextAlignment.TopLeft, Size.Empty);
                subRep.SetReportTitle(sub检查.Substring(size), true, ft7, TextAlignment.TopLeft, Size.Empty);
            }
            else
            {
                subRep.SetReportTitle(sub检查, true, ft7, TextAlignment.TopLeft, Size.Empty);
            }

            subRep.SetReportBarCode(s申请单号, new Point(5, 150), new Size(400, 125), null, true);
            subRep.SetReportMain(0, 0, 1, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(10, 10, 10, 10), System.Drawing.Printing.PaperKind.Custom, new Size(500, 315), 0, 0, "");

            subRep.CreateDocument();

            firstRep.Pages.AddRange(subRep.Pages);
        }
        //add by wjz 20151226 一个条码纸上可能无法将所有的项目全部打印出来，添加此方法的目的：实现分页打印 △

        private void tbfhz_id_TextChanged(object sender, EventArgs e)
        {

        }

        private void xtr_PrintProgress(object sender, PrintProgressEventArgs e)
        {
            //throw new NotImplementedException();
            //更新数据库中的打印状态

            string strUpdate = this.bllApply.Update更新申请状态(this.str申请单id, "2", LoginBLL.strPersonID);

            //更新显示状态
            DataGridViewRow row = this.dataGridViewApply.CurrentRow;
            if (strUpdate.Equals("true") && row != null)
            {
                //this.dtMain.Rows[applySelectedIndex]["fstate"] = 2;
                row.Cells["fstate"].Value = "2";
                row.DefaultCellStyle.ForeColor = Color.Blue;
            }

        }

        //add by wjz 20160427 为杨庄医院添加读社保卡的功能 ▽
        private void btnReadCardAndQuery_Click(object sender, EventArgs e)
        {
            GetDTNew();
        }

        private void GetDTNew()
        {
            //add by wjz 20160428 添加查询提示，提示是否查到了数据 ▽
            labelNotify.ForeColor = Color.Black;
            //add by wjz 20160428 添加查询提示，提示是否查到了数据 △

            ClassYBJK.personYB person = new ClassYBJK.personYB();
            ClassYBJK.ybBusiness bus = new ClassYBJK.ybBusiness();
            try
            {
                if (bus.readIC芯片(ref person) == false)
                {
                    MessageBox.Show("读取社保卡卡中基本信息失败" + person.sbErrInfo.ToString(), "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.IsNullOrWhiteSpace(person.S姓名) || string.IsNullOrWhiteSpace(person.S身份证号))
                {
                    MessageBox.Show("未读取到社保卡信息");
                    return;
                }

                textBoxName.Text = person.S姓名;
                textBoxSFZH.Text = person.S身份证号;
            }
            catch (Exception ex)
            {
                MessageBox.Show("读取社保卡卡中基本信息失败\n异常信息：" + ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            try
            {
                dtMain = new DataTable();
                string fjy_date1 = fjy_dateDateTimePicker1.Value.ToString("yyyy-MM-dd");
                string fjy_date2 = fjy_dateDateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59");

                StringBuilder builder = new StringBuilder(" 1=1 ");
                //[fcreate_time]
                if(comboBoxDateQueryType.SelectedIndex == 0)//表示现在的查询条件是 条码生成时间
                {
                    builder.Append(" and (t1.fcreate_time>='" + fjy_date1 + "' and t1.fcreate_time<='" + fjy_date2 + "')");
                }
                else if(comboBoxDateQueryType.SelectedIndex == 1)//表示现在的查询条件为申请时间
                {
                    builder.Append(" and (t1.fapply_time>='" + fjy_date1 + "' and t1.fapply_time<='" + fjy_date2 + "')");
                }
                else
                {
                    MessageBox.Show("请选择时间类型。（条码生成时间 或 申请时间）");
                    return;
                }

                if (string.IsNullOrWhiteSpace(txt门诊住院号.Text) || txt门诊住院号.Text.Contains("--")
                    || txt门诊住院号.Text.Contains("\n") || txt门诊住院号.Text.Contains("\r") 
                    || txt门诊住院号.Text.Contains(" ")  || txt门诊住院号.Text.Length > 15)
                { }
                else
                {
                    builder.Append(" and (t1.fhz_zyh='" + txt门诊住院号.Text + "') ");
                }

                builder.Append(" and t1.fname='"+person.S姓名+"' and t1.sfzh='"+person.S身份证号+"' ");

                //string strDateif = " (t1.fapply_time>='" + fjy_date1 + "' and t1.fapply_time<='" + fjy_date2 + "')";
                //if (txt门诊住院号.Text == "" || txt门诊住院号.Text == null)
                //{
                //    strDateif = " (t1.fapply_time>='" + fjy_date1 + "' and t1.fapply_time<='" + fjy_date2 + "')";
                //}
                //else
                //{
                //    strDateif = " (t1.fhz_zyh='" + txt门诊住院号.Text + "')";
                //}
                dtMain = this.bllApply.BllSamplingMainDT(builder.ToString());

                //add by wjz 20160428 添加查询提示，提示是否查到了数据 ▽
                if (dtMain == null || dtMain.Rows.Count == 0)
                {
                    labelNotify.ForeColor = Color.Red;
                    labelNotify.Text = "未查到任何数据";
                }
                //add by wjz 20160428 添加查询提示，提示是否查到了数据 △

                this.bindingSource1.DataSource = dtMain;
                dataGridViewApply.DataSource = bindingSource1;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void sbtnExport_Click(object sender, EventArgs e)
        {
            ExcelHelper.DataGridViewToExcel(dataGridViewApply);
        }
        //add by wjz 20160427 为杨庄医院添加读社保卡的功能 △
        
        private void btn_读电子健康卡_Click(object sender, EventArgs e)
        {
            //启动钩子服务
            listener.Start();
        }

    }
}