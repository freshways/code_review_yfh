﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ww.form.lis.sam.dic
{
    public partial class RemarkApplyForm : ComTypeForm
    {
        public RemarkApplyForm()
        {
            InitializeComponent();
        }

        private void RemarkApplyForm_Load(object sender, EventArgs e)
        {
            this.strType = "申请常见备注";
            this.GetComCheckType();
        }
    }
}