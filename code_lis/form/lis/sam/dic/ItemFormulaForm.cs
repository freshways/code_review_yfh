﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
namespace ww.form.lis.sam.dic
{
    public partial class ItemFormulaForm : Form
    {
        public ItemFormulaForm()
        {
            InitializeComponent();
        }
        private ItemFormulaResult r;
        private DataTable dtItem = new DataTable();
        public ItemFormulaForm(ItemFormulaResult r, DataTable dtItem)
            : this()
        {
            this.r = r;
            this.dtItem = dtItem;
        }
        private void ItemFormulaForm_Load(object sender, EventArgs e)
        {
            this.DataGridViewObject.AutoGenerateColumns = false;
            this.DataGridViewObject.DataSource = this.dtItem;
        }

        private void butSave_Click(object sender, EventArgs e)
        {
            r.ChangeText(this.richTextBoxValue.Text);
            this.Close();
        }

        private void butDel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DataGridViewObject_Click(object sender, EventArgs e)
        {
            try
            {
                string strcode = this.DataGridViewObject.CurrentRow.Cells["fitem_code"].Value.ToString();
                this.richTextBoxValue.SelectedText = ("[" + strcode + "]");
                this.ActiveControl = this.richTextBoxValue;
                //cursor
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonJ_Click(object sender, EventArgs e)
        {
            try
            {
                this.richTextBoxValue.SelectedText = (" + ");
                this.ActiveControl = this.richTextBoxValue;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 减
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonJian_Click(object sender, EventArgs e)
        {
            try
            {
                this.richTextBoxValue.SelectedText = (" - ");
                this.ActiveControl = this.richTextBoxValue;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void buttonC_Click(object sender, EventArgs e)
        {
            try
            {
                this.richTextBoxValue.SelectedText = (" * ");
                this.ActiveControl = this.richTextBoxValue;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCu_Click(object sender, EventArgs e)
        {
            try
            {
                this.richTextBoxValue.SelectedText = (" / ");
                this.ActiveControl = this.richTextBoxValue;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 括号(
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonKH1_Click(object sender, EventArgs e)
        {
            try
            {
                this.richTextBoxValue.SelectedText = (" (");
                this.ActiveControl = this.richTextBoxValue;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 括号)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonKH2_Click(object sender, EventArgs e)
        {
            try
            {
                this.richTextBoxValue.SelectedText = (") ");
                this.ActiveControl = this.richTextBoxValue;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

      
    }
}


/*
  string str = this.richTextBoxValue.Text;
            string[] array = Regex.Split(str, "[ ]");
            listBox1.DataSource = array;
            for (int i = 0; i < array.Length; i++)
            {
                MessageBox.Show(array[i].ToString());
            }
 */