﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.form.wwf;
using ww.lis.lisbll.sam;
using ww.wwf.wwfbll;
using System.Collections;
namespace ww.form.lis.sam.dic
{
    public partial class ItemGroupForm : SysBaseForm
    {
        ItemBLL bllItem = new ItemBLL();//项目逻辑
        DataTable dtItemGroup = new DataTable();
        string strfgroup_id = "";//组ID
        public ItemGroupForm()
        {
            InitializeComponent();
            this.DataGridViewObject.AutoGenerateColumns = false;
            this.dataGridViewAllItem.AutoGenerateColumns = false;
            this.dataGridViewOKItem.AutoGenerateColumns = false;
        }

        private void ItemGroupForm_Load(object sender, EventArgs e)
        {
            GetTree("-1");
        }
        /// <summary>
        /// 列出系统部门树
        /// </summary>
        public void GetTree(string pid)
        {
            try
            {
                this.wwTreeView1.ZADataTable = this.bllItem.BllCheckAndSampleTypeDT();
                this.wwTreeView1.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeView1.ZAParentFieldName = "fpid";//上级字段名称
                this.wwTreeView1.ZADisplayFieldName = "fname";//显示字段名称
                this.wwTreeView1.ZAKeyFieldName = "fid";//主键字段名称
                this.wwTreeView1.ZAToolTipTextName = "tag";
                this.wwTreeView1.ZATreeViewShow();//显示树        
                try
                {
                    this.wwTreeView1.SelectedNode = wwTreeView1.Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
       
        string strfcheck_type_id = "";
        string strfsample_type_id = "";
        private void wwTreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (this.dtItemGroup.Rows.Count > 0)
                this.dtItemGroup.Clear();
            strfcheck_type_id = "";
            strfsample_type_id = "";
            string strEdit = e.Node.ToolTipText;
            if (strEdit == "CheckType")
            {
                bN.Enabled = false;
                
            }
            else
            {
                bN.Enabled = true;
                strfcheck_type_id = e.Node.Parent.Name.ToString();
                strfsample_type_id = e.Node.Name.ToString();
                GetItemGroup();
                GetItemOne();
                //MessageBox.Show("检验类别：" + strfcheck_type_id);
                //MessageBox.Show("样本类别为：" + strfsample_type_id);
            }
        }
        /// <summary>
        /// 取得 项目组
        /// </summary>
        private void GetItemOne()
        {
            try
            {
              
                this.dataGridViewAllItem.DataSource = this.bllItem.BllItem(" fcheck_type_id='" + strfcheck_type_id + "' and fsam_type_id='" +
                    strfsample_type_id + "' and finstr_id in(select finstr_id from sam_instr where fuse_if=1)");
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }

        }
        /// <summary>
        /// 取得 项目组
        /// </summary>
        private void GetItemGroup()
        {
            try
            {
                this.dtItemGroup = this.bllItem.BllItemGroupDT("fcheck_type_id='" + strfcheck_type_id + "' and fsam_type_id='" + strfsample_type_id + "'");
                this.bindingSourceItemGroup.DataSource = dtItemGroup;
            }
            catch (Exception ex)
            {
              WWMessage.MessageShowError(ex.Message.ToString());
            }

        }
        private void GetItemOk(string fgroup_id)
        {
            try
            {
                this.dataGridViewOKItem.DataSource = this.bllItem.BllItemAndGroupRefDT(fgroup_id);
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }

        }
        private void toolStripButtonHelpCode_Click(object sender, EventArgs e)
        {
            try
            {
                string sqlHelp = "";
                IList lissql = new ArrayList();

                //add by wjz 20151106 点击“生成助记符”时，新增的项目名称变为空，修改此问题，变更内容：添加一行代码(更新数据库的代码) ▽
                //this.bindingSourceItemGroup.EndEdit();
                this.DataGridViewObject.EndEdit();
                //add by wjz 20151106 点击“生成助记符”时，新增的项目名称变为空，修改此问题，变更内容：添加一行代码(更新数据库的代码) △

                for (int i = 0; i < DataGridViewObject.Rows.Count; i++)
                {
                    sqlHelp = "";
                    try
                    {
                        sqlHelp = ww.wwf.com.Spell.MakeSpellCodeFirstLetterOnly(DataGridViewObject.Rows[i].Cells["fname"].Value.ToString().Trim());

                        //add by wjz 20151106 点击“生成助记符”时，新增的项目名称变为空，修改此问题，变更内容：添加一行代码(更新数据库的代码) ▽
                        DataGridViewObject.Rows[i].Cells["fhelp_code"].Value = sqlHelp;
                        //add by wjz 20151106 点击“生成助记符”时，新增的项目名称变为空，修改此问题，变更内容：添加一行代码(更新数据库的代码) △
                    }
                    catch { }

                    //changed by wjz 20151106 点击“生成助记符”时，新增的项目名称变为空，修改此问题，变更内容：注释掉下面一行代码(更新数据库的代码) ▽
                    lissql.Add("UPDATE SAM_ITEM_GROUP SET fhelp_code = '" + sqlHelp + "' WHERE (fgroup_id = '" + DataGridViewObject.Rows[i].Cells["fgroup_id"].Value.ToString() + "')");
                    //changed by wjz 20151106 点击“生成助记符”时，新增的项目名称变为空，修改此问题 △
                    //string ss = DataGridViewObject.Rows[i].Cells["fname"].Value.ToString().Trim() +"-"+ i.ToString();
                    // lissql.Add("UPDATE sam_sample_type SET fname = '" + ss + "' WHERE (fsample_type_id = '" + DataGridViewObject.Rows[i].Cells["fsample_type_id"].Value.ToString() + "')");
                }
                //delete by wjz 20151106 点击“生成助记符”时，新增的项目名称变为空，修改此问题 变更内容：注释掉下面2行代码(更新数据库的代码) ▽
                //this.bllItem.BllItemGroupHelpCodeUpdate(lissql);
                //GetItemGroup();
                //delete by wjz 20151106 点击“生成助记符”时，新增的项目名称变为空，修改此问题 △
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void toolStripButtonAdd_Click(object sender, EventArgs e)
        {
            try
            {               
                if (strfsample_type_id == "" || strfsample_type_id == null)
                {
                    WWMessage.MessageShowWarning("样本类别不能为空，请选择后重试！");
                    return;
                }
                DataRow dr = null;
                dr = this.dtItemGroup.NewRow();
                dr["fjz_if"] = "0";
                dr["fjz_no_if"] = "1";
                dr["fuse_if"] = "1";
                dr["fcheck_type_id"] = strfcheck_type_id;
                dr["fsam_type_id"] = strfsample_type_id;

                if (this.bllItem.BllItemGroupAdd(dr) > 0)
                {
                    this.dtItemGroup.Rows.Add(dr);
                    this.bindingSourceItemGroup.Position = bindingSourceItemGroup.Count;
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void toolStripButtonS_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.bindingSourceItemGroup.EndEdit();
                for (int i = 0; i < this.dtItemGroup.Rows.Count; i++)
                {
                    this.bllItem.BllItemGroupUpdate(this.dtItemGroup.Rows[i]);
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonDel_Click(object sender, EventArgs e)
        {
            try
            {
                if (DataGridViewObject.Rows.Count > 0)
                {
                    if (WWMessage.MessageDialogResult("确定删除？记录删除后将不可恢复！"))
                    {
                        string strid = this.DataGridViewObject.CurrentRow.Cells["fgroup_id"].Value.ToString();
                        this.bllItem.BllItemGroupDel(strid);
                        GetItemGroup();
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void bindingSourceItemGroup_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                strfgroup_id = "";
                DataRowView rowCurrent = (DataRowView)bindingSourceItemGroup.Current;
                if (rowCurrent != null)
                {
                    strfgroup_id = rowCurrent["fgroup_id"].ToString();
                    GetItemOk(strfgroup_id);
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (dataGridViewAllItem.Rows.Count > 0)
                {
                    string sfgroup_id = this.DataGridViewObject.CurrentRow.Cells["fgroup_id"].Value.ToString();
                    string sfitem_id = this.dataGridViewAllItem.CurrentRow.Cells["fitem_id"].Value.ToString();
                    if (this.bllItem.BllItemGroupRefExists(sfgroup_id, sfitem_id))
                    {
                        ww.wwf.wwfbll.WWMessage.MessageShowWarning("此项目已经存！");
                    }
                    else
                    {
                        this.bllItem.BllItemGroupRefAdd(sfgroup_id, sfitem_id);
                        GetItemOk(strfgroup_id);
                    }
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void buttonNo_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (this.dataGridViewOKItem.Rows.Count > 0)
                {
                    if (ww.wwf.wwfbll.WWMessage.MessageDialogResult("确定取消？"))
                    {
                        string fid = this.dataGridViewOKItem.CurrentRow.Cells["fid"].Value.ToString();
                        this.bllItem.BllItemGroupRefDel(fid);
                        GetItemOk(strfgroup_id);
                    }
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

       
    }
}