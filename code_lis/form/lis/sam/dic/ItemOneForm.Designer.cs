﻿namespace ww.form.lis.sam.dic
{
    partial class ItemOneForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemOneForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bN = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingSourceItemOne = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonS = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonHelpCode = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonDel = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonDec = new System.Windows.Forms.ToolStripButton();
            this.wwTreeView1 = new ww.wwf.control.WWControlLib.WWTreeView();
            this.imageListTree = new System.Windows.Forms.ImageList(this.components);
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.DataGridViewObject = new System.Windows.Forms.DataGridView();
            this.com_listBindingSourceUnit = new System.Windows.Forms.BindingSource(this.components);
            this.samDataSet = new ww.form.lis.sam.samDataSet();
            this.bindingSource_fcheck_type_id = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource_fsam_type_id = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource_finstr_id = new System.Windows.Forms.BindingSource(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonAll = new System.Windows.Forms.Button();
            this.comboBoxInstr = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.fuse_if = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fjz_if = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fjz_no_if = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fprint_num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.funit_name = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.fcheck_type_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.fsam_type_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.finstr_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.fref = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fname_e = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhis_item_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fprice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhelp_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).BeginInit();
            this.bN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceItemOne)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSourceUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_fcheck_type_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_fsam_type_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_finstr_id)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bN
            // 
            this.bN.AddNewItem = null;
            this.bN.AutoSize = false;
            this.bN.BindingSource = this.bindingSourceItemOne;
            this.bN.CountItem = this.bindingNavigatorCountItem;
            this.bN.DeleteItem = null;
            this.bN.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.toolStripButtonAdd,
            this.toolStripButtonS,
            this.toolStripButtonHelpCode,
            this.toolStripButtonDel,
            this.toolStripButtonDec});
            this.bN.Location = new System.Drawing.Point(0, 0);
            this.bN.MoveFirstItem = null;
            this.bN.MoveLastItem = null;
            this.bN.MoveNextItem = null;
            this.bN.MovePreviousItem = null;
            this.bN.Name = "bN";
            this.bN.PositionItem = this.bindingNavigatorPositionItem;
            this.bN.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bN.Size = new System.Drawing.Size(633, 30);
            this.bN.TabIndex = 129;
            this.bN.Text = "bindingNavigator1";
            // 
            // bindingSourceItemOne
            // 
            this.bindingSourceItemOne.PositionChanged += new System.EventHandler(this.bindingSourceItemOne_PositionChanged);
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 27);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 30);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonAdd
            // 
            this.toolStripButtonAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAdd.Image")));
            this.toolStripButtonAdd.Name = "toolStripButtonAdd";
            this.toolStripButtonAdd.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonAdd.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonAdd.Size = new System.Drawing.Size(79, 27);
            this.toolStripButtonAdd.Text = "新增(&A)  ";
            this.toolStripButtonAdd.Click += new System.EventHandler(this.toolStripButtonAdd_Click);
            // 
            // toolStripButtonS
            // 
            this.toolStripButtonS.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonS.Image")));
            this.toolStripButtonS.Name = "toolStripButtonS";
            this.toolStripButtonS.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonS.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonS.Size = new System.Drawing.Size(79, 27);
            this.toolStripButtonS.Text = "保存(&S)  ";
            this.toolStripButtonS.Click += new System.EventHandler(this.toolStripButtonS_Click);
            // 
            // toolStripButtonHelpCode
            // 
            this.toolStripButtonHelpCode.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHelpCode.Image")));
            this.toolStripButtonHelpCode.Name = "toolStripButtonHelpCode";
            this.toolStripButtonHelpCode.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonHelpCode.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonHelpCode.Size = new System.Drawing.Size(97, 27);
            this.toolStripButtonHelpCode.Text = "生成助记符  ";
            this.toolStripButtonHelpCode.Click += new System.EventHandler(this.toolStripButtonHelpCode_Click);
            // 
            // toolStripButtonDel
            // 
            this.toolStripButtonDel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDel.Image")));
            this.toolStripButtonDel.Name = "toolStripButtonDel";
            this.toolStripButtonDel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonDel.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonDel.Size = new System.Drawing.Size(79, 27);
            this.toolStripButtonDel.Text = "删除(&D)  ";
            this.toolStripButtonDel.Click += new System.EventHandler(this.toolStripButtonDel_Click);
            // 
            // toolStripButtonDec
            // 
            this.toolStripButtonDec.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDec.Image")));
            this.toolStripButtonDec.Name = "toolStripButtonDec";
            this.toolStripButtonDec.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonDec.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonDec.Size = new System.Drawing.Size(73, 27);
            this.toolStripButtonDec.Text = "详细参数";
            this.toolStripButtonDec.Click += new System.EventHandler(this.toolStripButtonDec_Click);
            // 
            // wwTreeView1
            // 
            this.wwTreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wwTreeView1.Dock = System.Windows.Forms.DockStyle.Left;
            this.wwTreeView1.ImageIndex = 0;
            this.wwTreeView1.ImageList = this.imageListTree;
            this.wwTreeView1.Location = new System.Drawing.Point(0, 30);
            this.wwTreeView1.Name = "wwTreeView1";
            this.wwTreeView1.SelectedImageIndex = 1;
            this.wwTreeView1.Size = new System.Drawing.Size(180, 401);
            this.wwTreeView1.TabIndex = 130;
            this.wwTreeView1.ZADataTable = null;
            this.wwTreeView1.ZADisplayFieldName = "";
            this.wwTreeView1.ZAKeyFieldName = "";
            this.wwTreeView1.ZAParentFieldName = "";
            this.wwTreeView1.ZAToolTipTextName = "";
            this.wwTreeView1.ZATreeViewRootValue = "";
            this.wwTreeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.wwTreeView1_AfterSelect);
            // 
            // imageListTree
            // 
            this.imageListTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTree.ImageStream")));
            this.imageListTree.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTree.Images.SetKeyName(0, "12.gif");
            this.imageListTree.Images.SetKeyName(1, "27.gif");
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(180, 30);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 401);
            this.splitter1.TabIndex = 131;
            this.splitter1.TabStop = false;
            // 
            // DataGridViewObject
            // 
            this.DataGridViewObject.AllowUserToAddRows = false;
            this.DataGridViewObject.AllowUserToResizeRows = false;
            this.DataGridViewObject.AutoGenerateColumns = false;
            this.DataGridViewObject.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DataGridViewObject.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridViewObject.ColumnHeadersHeight = 21;
            this.DataGridViewObject.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fuse_if,
            this.fjz_if,
            this.fjz_no_if,
            this.fprint_num,
            this.fitem_code,
            this.fname,
            this.funit_name,
            this.fcheck_type_id,
            this.fsam_type_id,
            this.finstr_id,
            this.fref,
            this.fname_e,
            this.fhis_item_code,
            this.fprice,
            this.fhelp_code,
            this.fitem_id});
            this.DataGridViewObject.DataSource = this.bindingSourceItemOne;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridViewObject.DefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridViewObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridViewObject.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.DataGridViewObject.EnableHeadersVisualStyles = false;
            this.DataGridViewObject.Location = new System.Drawing.Point(183, 66);
            this.DataGridViewObject.MultiSelect = false;
            this.DataGridViewObject.Name = "DataGridViewObject";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridViewObject.RowHeadersVisible = false;
            this.DataGridViewObject.RowHeadersWidth = 35;
            this.DataGridViewObject.RowTemplate.Height = 23;
            this.DataGridViewObject.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewObject.Size = new System.Drawing.Size(450, 365);
            this.DataGridViewObject.TabIndex = 132;
            this.DataGridViewObject.DoubleClick += new System.EventHandler(this.DataGridViewObject_DoubleClick);
            this.DataGridViewObject.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataGridViewObject_DataError);
            // 
            // com_listBindingSourceUnit
            // 
            this.com_listBindingSourceUnit.DataMember = "com_list";
            this.com_listBindingSourceUnit.DataSource = this.samDataSet;
            // 
            // samDataSet
            // 
            this.samDataSet.DataSetName = "samDataSet";
            this.samDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingSource_fcheck_type_id
            // 
            this.bindingSource_fcheck_type_id.DataMember = "com_list";
            this.bindingSource_fcheck_type_id.DataSource = this.samDataSet;
            // 
            // bindingSource_fsam_type_id
            // 
            this.bindingSource_fsam_type_id.DataMember = "com_list";
            this.bindingSource_fsam_type_id.DataSource = this.samDataSet;
            // 
            // bindingSource_finstr_id
            // 
            this.bindingSource_finstr_id.DataMember = "com_list";
            this.bindingSource_finstr_id.DataSource = this.samDataSet;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonAll);
            this.panel1.Controls.Add(this.comboBoxInstr);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(183, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(450, 36);
            this.panel1.TabIndex = 133;
            // 
            // buttonAll
            // 
            this.buttonAll.Image = ((System.Drawing.Image)(resources.GetObject("buttonAll.Image")));
            this.buttonAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAll.Location = new System.Drawing.Point(275, 5);
            this.buttonAll.Name = "buttonAll";
            this.buttonAll.Size = new System.Drawing.Size(85, 23);
            this.buttonAll.TabIndex = 4;
            this.buttonAll.Text = "所有项目 ";
            this.buttonAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAll.UseVisualStyleBackColor = true;
            this.buttonAll.Click += new System.EventHandler(this.buttonAll_Click);
            // 
            // comboBoxInstr
            // 
            this.comboBoxInstr.DisplayMember = "ShowName";
            this.comboBoxInstr.FormattingEnabled = true;
            this.comboBoxInstr.Location = new System.Drawing.Point(83, 6);
            this.comboBoxInstr.Name = "comboBoxInstr";
            this.comboBoxInstr.Size = new System.Drawing.Size(186, 20);
            this.comboBoxInstr.TabIndex = 3;
            this.comboBoxInstr.ValueMember = "finstr_id";
            this.comboBoxInstr.DropDownClosed += new System.EventHandler(this.comboBoxInstr_DropDownClosed);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "仪器过滤:";
            // 
            // fuse_if
            // 
            this.fuse_if.DataPropertyName = "fuse_if";
            this.fuse_if.FalseValue = "0";
            this.fuse_if.HeaderText = "启用";
            this.fuse_if.Name = "fuse_if";
            this.fuse_if.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fuse_if.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fuse_if.TrueValue = "1";
            this.fuse_if.Width = 40;
            // 
            // fjz_if
            // 
            this.fjz_if.DataPropertyName = "fjz_if";
            this.fjz_if.FalseValue = "0";
            this.fjz_if.HeaderText = "急诊";
            this.fjz_if.IndeterminateValue = "0";
            this.fjz_if.Name = "fjz_if";
            this.fjz_if.TrueValue = "1";
            this.fjz_if.Width = 40;
            // 
            // fjz_no_if
            // 
            this.fjz_no_if.DataPropertyName = "fjz_no_if";
            this.fjz_no_if.FalseValue = "0";
            this.fjz_no_if.HeaderText = "非急诊";
            this.fjz_no_if.IndeterminateValue = "0";
            this.fjz_no_if.Name = "fjz_no_if";
            this.fjz_no_if.TrueValue = "1";
            this.fjz_no_if.Width = 50;
            // 
            // fprint_num
            // 
            this.fprint_num.DataPropertyName = "fprint_num";
            this.fprint_num.HeaderText = "序";
            this.fprint_num.Name = "fprint_num";
            this.fprint_num.Width = 40;
            // 
            // fitem_code
            // 
            this.fitem_code.DataPropertyName = "fitem_code";
            this.fitem_code.HeaderText = "代号";
            this.fitem_code.Name = "fitem_code";
            this.fitem_code.Width = 75;
            // 
            // fname
            // 
            this.fname.DataPropertyName = "fname";
            this.fname.HeaderText = "名称";
            this.fname.Name = "fname";
            this.fname.Width = 90;
            // 
            // funit_name
            // 
            this.funit_name.DataPropertyName = "funit_name";
            this.funit_name.DataSource = this.com_listBindingSourceUnit;
            this.funit_name.DisplayMember = "fname";
            this.funit_name.HeaderText = "单位";
            this.funit_name.Name = "funit_name";
            this.funit_name.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.funit_name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.funit_name.ValueMember = "fcode";
            this.funit_name.Width = 80;
            // 
            // fcheck_type_id
            // 
            this.fcheck_type_id.DataPropertyName = "fcheck_type_id";
            this.fcheck_type_id.DataSource = this.bindingSource_fcheck_type_id;
            this.fcheck_type_id.DisplayMember = "fname";
            this.fcheck_type_id.HeaderText = "检验类别";
            this.fcheck_type_id.Name = "fcheck_type_id";
            this.fcheck_type_id.ValueMember = "fcheck_type_id";
            this.fcheck_type_id.Width = 80;
            // 
            // fsam_type_id
            // 
            this.fsam_type_id.DataPropertyName = "fsam_type_id";
            this.fsam_type_id.DataSource = this.bindingSource_fsam_type_id;
            this.fsam_type_id.DisplayMember = "fname";
            this.fsam_type_id.HeaderText = "样本类别";
            this.fsam_type_id.Name = "fsam_type_id";
            this.fsam_type_id.ValueMember = "fsample_type_id";
            this.fsam_type_id.Width = 80;
            // 
            // finstr_id
            // 
            this.finstr_id.DataPropertyName = "finstr_id";
            this.finstr_id.DataSource = this.bindingSource_finstr_id;
            this.finstr_id.DisplayMember = "fname";
            this.finstr_id.HeaderText = "检验仪器";
            this.finstr_id.Name = "finstr_id";
            this.finstr_id.ValueMember = "finstr_id";
            // 
            // fref
            // 
            this.fref.DataPropertyName = "fref";
            this.fref.HeaderText = "参考值";
            this.fref.Name = "fref";
            this.fref.Width = 80;
            // 
            // fname_e
            // 
            this.fname_e.DataPropertyName = "fname_e";
            this.fname_e.HeaderText = "英文名";
            this.fname_e.Name = "fname_e";
            // 
            // fhis_item_code
            // 
            this.fhis_item_code.DataPropertyName = "fhis_item_code";
            this.fhis_item_code.HeaderText = "HIS代码";
            this.fhis_item_code.Name = "fhis_item_code";
            // 
            // fprice
            // 
            this.fprice.DataPropertyName = "fprice";
            this.fprice.HeaderText = "价格";
            this.fprice.Name = "fprice";
            // 
            // fhelp_code
            // 
            this.fhelp_code.DataPropertyName = "fhelp_code";
            this.fhelp_code.HeaderText = "助记符";
            this.fhelp_code.Name = "fhelp_code";
            // 
            // fitem_id
            // 
            this.fitem_id.DataPropertyName = "fitem_id";
            this.fitem_id.HeaderText = "fitem_id";
            this.fitem_id.Name = "fitem_id";
            this.fitem_id.Visible = false;
            // 
            // ItemOneForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(633, 431);
            this.Controls.Add(this.DataGridViewObject);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.wwTreeView1);
            this.Controls.Add(this.bN);
            this.Name = "ItemOneForm";
            this.Text = "ItemOneForm";
            this.Load += new System.EventHandler(this.ItemOneForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bN)).EndInit();
            this.bN.ResumeLayout(false);
            this.bN.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceItemOne)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSourceUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_fcheck_type_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_fsam_type_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_finstr_id)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.BindingNavigator bN;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonAdd;
        private System.Windows.Forms.ToolStripButton toolStripButtonS;
        private System.Windows.Forms.ToolStripButton toolStripButtonDel;
        private ww.wwf.control.WWControlLib.WWTreeView wwTreeView1;
        private System.Windows.Forms.ImageList imageListTree;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.BindingSource bindingSourceItemOne;
        private System.Windows.Forms.ToolStripButton toolStripButtonHelpCode;
        private System.Windows.Forms.DataGridView DataGridViewObject;
        private samDataSet samDataSet;
        private System.Windows.Forms.BindingSource com_listBindingSourceUnit;
        private System.Windows.Forms.ToolStripButton toolStripButtonDec;
        private System.Windows.Forms.BindingSource bindingSource_fcheck_type_id;
        private System.Windows.Forms.BindingSource bindingSource_fsam_type_id;
        private System.Windows.Forms.BindingSource bindingSource_finstr_id;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox comboBoxInstr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonAll;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fuse_if;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fjz_if;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fjz_no_if;
        private System.Windows.Forms.DataGridViewTextBoxColumn fprint_num;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname;
        private System.Windows.Forms.DataGridViewComboBoxColumn funit_name;
        private System.Windows.Forms.DataGridViewComboBoxColumn fcheck_type_id;
        private System.Windows.Forms.DataGridViewComboBoxColumn fsam_type_id;
        private System.Windows.Forms.DataGridViewComboBoxColumn finstr_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fref;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname_e;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhis_item_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn fprice;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhelp_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_id;
    }
}