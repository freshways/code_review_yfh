﻿namespace ww.form.lis.sam.dic
{
    partial class ItemGroupForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemGroupForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bN = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingSourceItemGroup = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonS = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonHelpCode = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonDel = new System.Windows.Forms.ToolStripButton();
            this.wwTreeView1 = new ww.wwf.control.WWControlLib.WWTreeView();
            this.imageListTree = new System.Windows.Forms.ImageList(this.components);
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.DataGridViewObject = new System.Windows.Forms.DataGridView();
            this.fuse_if = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fjz_if = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fjz_no_if = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhelp_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forder_by = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fremark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fsam_type_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fcheck_type_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fgroup_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridViewAllItem = new System.Windows.Forms.DataGridView();
            this.fitem_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fprint_num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridViewOKItem = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonNo = new System.Windows.Forms.Button();
            this.buttonOk = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).BeginInit();
            this.bN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceItemGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAllItem)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOKItem)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bN
            // 
            this.bN.AddNewItem = null;
            this.bN.AutoSize = false;
            this.bN.BindingSource = this.bindingSourceItemGroup;
            this.bN.CountItem = this.bindingNavigatorCountItem;
            this.bN.DeleteItem = null;
            this.bN.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bN.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.bN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.toolStripButtonAdd,
            this.toolStripButtonS,
            this.toolStripButtonHelpCode,
            this.toolStripButtonDel});
            this.bN.Location = new System.Drawing.Point(0, 0);
            this.bN.MoveFirstItem = null;
            this.bN.MoveLastItem = null;
            this.bN.MoveNextItem = null;
            this.bN.MovePreviousItem = null;
            this.bN.Name = "bN";
            this.bN.PositionItem = this.bindingNavigatorPositionItem;
            this.bN.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bN.Size = new System.Drawing.Size(969, 38);
            this.bN.TabIndex = 129;
            this.bN.Text = "bindingNavigator1";
            // 
            // bindingSourceItemGroup
            // 
            this.bindingSourceItemGroup.PositionChanged += new System.EventHandler(this.bindingSourceItemGroup_PositionChanged);
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(38, 35);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 38);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(65, 27);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 38);
            // 
            // toolStripButtonAdd
            // 
            this.toolStripButtonAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAdd.Image")));
            this.toolStripButtonAdd.Name = "toolStripButtonAdd";
            this.toolStripButtonAdd.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonAdd.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonAdd.Size = new System.Drawing.Size(92, 35);
            this.toolStripButtonAdd.Text = "新增(&A)  ";
            this.toolStripButtonAdd.Click += new System.EventHandler(this.toolStripButtonAdd_Click);
            // 
            // toolStripButtonS
            // 
            this.toolStripButtonS.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonS.Image")));
            this.toolStripButtonS.Name = "toolStripButtonS";
            this.toolStripButtonS.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonS.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonS.Size = new System.Drawing.Size(90, 35);
            this.toolStripButtonS.Text = "保存(&S)  ";
            this.toolStripButtonS.Click += new System.EventHandler(this.toolStripButtonS_Click);
            // 
            // toolStripButtonHelpCode
            // 
            this.toolStripButtonHelpCode.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHelpCode.Image")));
            this.toolStripButtonHelpCode.Name = "toolStripButtonHelpCode";
            this.toolStripButtonHelpCode.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonHelpCode.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonHelpCode.Size = new System.Drawing.Size(116, 35);
            this.toolStripButtonHelpCode.Text = "生成助记符  ";
            this.toolStripButtonHelpCode.Click += new System.EventHandler(this.toolStripButtonHelpCode_Click);
            // 
            // toolStripButtonDel
            // 
            this.toolStripButtonDel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDel.Image")));
            this.toolStripButtonDel.Name = "toolStripButtonDel";
            this.toolStripButtonDel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonDel.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonDel.Size = new System.Drawing.Size(92, 35);
            this.toolStripButtonDel.Text = "删除(&D)  ";
            this.toolStripButtonDel.Click += new System.EventHandler(this.toolStripButtonDel_Click);
            // 
            // wwTreeView1
            // 
            this.wwTreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wwTreeView1.Dock = System.Windows.Forms.DockStyle.Left;
            this.wwTreeView1.ImageIndex = 0;
            this.wwTreeView1.ImageList = this.imageListTree;
            this.wwTreeView1.Location = new System.Drawing.Point(0, 38);
            this.wwTreeView1.Margin = new System.Windows.Forms.Padding(4);
            this.wwTreeView1.Name = "wwTreeView1";
            this.wwTreeView1.SelectedImageIndex = 1;
            this.wwTreeView1.Size = new System.Drawing.Size(240, 598);
            this.wwTreeView1.TabIndex = 130;
            this.wwTreeView1.ZADataTable = null;
            this.wwTreeView1.ZADisplayFieldName = "";
            this.wwTreeView1.ZAKeyFieldName = "";
            this.wwTreeView1.ZAParentFieldName = "";
            this.wwTreeView1.ZAToolTipTextName = "";
            this.wwTreeView1.ZATreeViewRootValue = "";
            this.wwTreeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.wwTreeView1_AfterSelect);
            // 
            // imageListTree
            // 
            this.imageListTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTree.ImageStream")));
            this.imageListTree.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTree.Images.SetKeyName(0, "12.gif");
            this.imageListTree.Images.SetKeyName(1, "27.gif");
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(240, 38);
            this.splitter1.Margin = new System.Windows.Forms.Padding(4);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(4, 598);
            this.splitter1.TabIndex = 131;
            this.splitter1.TabStop = false;
            // 
            // DataGridViewObject
            // 
            this.DataGridViewObject.AllowUserToAddRows = false;
            this.DataGridViewObject.AllowUserToResizeRows = false;
            this.DataGridViewObject.AutoGenerateColumns = false;
            this.DataGridViewObject.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DataGridViewObject.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridViewObject.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fuse_if,
            this.fjz_if,
            this.fjz_no_if,
            this.fname,
            this.fhelp_code,
            this.forder_by,
            this.fremark,
            this.fsam_type_id,
            this.fcheck_type_id,
            this.fcode,
            this.fgroup_id});
            this.DataGridViewObject.DataSource = this.bindingSourceItemGroup;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridViewObject.DefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridViewObject.Dock = System.Windows.Forms.DockStyle.Top;
            this.DataGridViewObject.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.DataGridViewObject.EnableHeadersVisualStyles = false;
            this.DataGridViewObject.Location = new System.Drawing.Point(244, 38);
            this.DataGridViewObject.Margin = new System.Windows.Forms.Padding(4);
            this.DataGridViewObject.MultiSelect = false;
            this.DataGridViewObject.Name = "DataGridViewObject";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridViewObject.RowHeadersVisible = false;
            this.DataGridViewObject.RowTemplate.Height = 23;
            this.DataGridViewObject.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewObject.Size = new System.Drawing.Size(725, 274);
            this.DataGridViewObject.TabIndex = 132;
            //this.DataGridViewObject.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataGridViewObject_DataError);
            // 
            // fuse_if
            // 
            this.fuse_if.DataPropertyName = "fuse_if";
            this.fuse_if.FalseValue = "0";
            this.fuse_if.HeaderText = "启用";
            this.fuse_if.IndeterminateValue = "0";
            this.fuse_if.Name = "fuse_if";
            this.fuse_if.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fuse_if.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fuse_if.TrueValue = "1";
            this.fuse_if.Width = 40;
            // 
            // fjz_if
            // 
            this.fjz_if.DataPropertyName = "fjz_if";
            this.fjz_if.FalseValue = "0";
            this.fjz_if.HeaderText = "急诊";
            this.fjz_if.IndeterminateValue = "0";
            this.fjz_if.Name = "fjz_if";
            this.fjz_if.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fjz_if.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fjz_if.TrueValue = "1";
            this.fjz_if.Width = 40;
            // 
            // fjz_no_if
            // 
            this.fjz_no_if.DataPropertyName = "fjz_no_if";
            this.fjz_no_if.FalseValue = "0";
            this.fjz_no_if.HeaderText = "非急诊";
            this.fjz_no_if.IndeterminateValue = "0";
            this.fjz_no_if.Name = "fjz_no_if";
            this.fjz_no_if.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fjz_no_if.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fjz_no_if.TrueValue = "1";
            this.fjz_no_if.Width = 50;
            // 
            // fname
            // 
            this.fname.DataPropertyName = "fname";
            this.fname.HeaderText = "名称";
            this.fname.Name = "fname";
            this.fname.Width = 200;
            // 
            // fhelp_code
            // 
            this.fhelp_code.DataPropertyName = "fhelp_code";
            this.fhelp_code.HeaderText = "助记符";
            this.fhelp_code.Name = "fhelp_code";
            this.fhelp_code.Width = 80;
            // 
            // forder_by
            // 
            this.forder_by.DataPropertyName = "forder_by";
            this.forder_by.HeaderText = "序";
            this.forder_by.Name = "forder_by";
            this.forder_by.Width = 60;
            // 
            // fremark
            // 
            this.fremark.DataPropertyName = "fremark";
            this.fremark.HeaderText = "备注";
            this.fremark.Name = "fremark";
            // 
            // fsam_type_id
            // 
            this.fsam_type_id.DataPropertyName = "fsam_type_id";
            this.fsam_type_id.HeaderText = "fsam_type_id";
            this.fsam_type_id.Name = "fsam_type_id";
            this.fsam_type_id.Visible = false;
            // 
            // fcheck_type_id
            // 
            this.fcheck_type_id.DataPropertyName = "fcheck_type_id";
            this.fcheck_type_id.HeaderText = "fcheck_type_id";
            this.fcheck_type_id.Name = "fcheck_type_id";
            this.fcheck_type_id.Visible = false;
            // 
            // fcode
            // 
            this.fcode.DataPropertyName = "fcode";
            this.fcode.HeaderText = "fcode";
            this.fcode.Name = "fcode";
            this.fcode.Visible = false;
            // 
            // fgroup_id
            // 
            this.fgroup_id.DataPropertyName = "fgroup_id";
            this.fgroup_id.HeaderText = "fgroup_id";
            this.fgroup_id.Name = "fgroup_id";
            this.fgroup_id.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridViewAllItem);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(244, 312);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(347, 324);
            this.groupBox1.TabIndex = 133;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "所有单项";
            // 
            // dataGridViewAllItem
            // 
            this.dataGridViewAllItem.AllowUserToAddRows = false;
            this.dataGridViewAllItem.AllowUserToResizeRows = false;
            this.dataGridViewAllItem.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewAllItem.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewAllItem.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewAllItem.ColumnHeadersHeight = 21;
            this.dataGridViewAllItem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fitem_code,
            this.dataGridViewTextBoxColumn1,
            this.fprint_num,
            this.fitem_id});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewAllItem.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewAllItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewAllItem.EnableHeadersVisualStyles = false;
            this.dataGridViewAllItem.Location = new System.Drawing.Point(4, 22);
            this.dataGridViewAllItem.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewAllItem.MultiSelect = false;
            this.dataGridViewAllItem.Name = "dataGridViewAllItem";
            this.dataGridViewAllItem.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewAllItem.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewAllItem.RowHeadersVisible = false;
            this.dataGridViewAllItem.RowHeadersWidth = 35;
            this.dataGridViewAllItem.RowTemplate.Height = 23;
            this.dataGridViewAllItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewAllItem.Size = new System.Drawing.Size(339, 298);
            this.dataGridViewAllItem.TabIndex = 133;
            // 
            // fitem_code
            // 
            this.fitem_code.DataPropertyName = "fitem_code";
            this.fitem_code.HeaderText = "代号";
            this.fitem_code.Name = "fitem_code";
            this.fitem_code.ReadOnly = true;
            this.fitem_code.Width = 75;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "fname";
            this.dataGridViewTextBoxColumn1.HeaderText = "名称";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 130;
            // 
            // fprint_num
            // 
            this.fprint_num.DataPropertyName = "fprint_num";
            this.fprint_num.HeaderText = "序";
            this.fprint_num.Name = "fprint_num";
            this.fprint_num.ReadOnly = true;
            this.fprint_num.Width = 40;
            // 
            // fitem_id
            // 
            this.fitem_id.DataPropertyName = "fitem_id";
            this.fitem_id.HeaderText = "fitem_id";
            this.fitem_id.Name = "fitem_id";
            this.fitem_id.ReadOnly = true;
            this.fitem_id.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridViewOKItem);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(706, 312);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(263, 324);
            this.groupBox2.TabIndex = 134;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "已经分配单项";
            // 
            // dataGridViewOKItem
            // 
            this.dataGridViewOKItem.AllowUserToAddRows = false;
            this.dataGridViewOKItem.AllowUserToResizeRows = false;
            this.dataGridViewOKItem.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewOKItem.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewOKItem.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewOKItem.ColumnHeadersHeight = 21;
            this.dataGridViewOKItem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn10,
            this.fid});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewOKItem.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewOKItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewOKItem.EnableHeadersVisualStyles = false;
            this.dataGridViewOKItem.Location = new System.Drawing.Point(4, 22);
            this.dataGridViewOKItem.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewOKItem.MultiSelect = false;
            this.dataGridViewOKItem.Name = "dataGridViewOKItem";
            this.dataGridViewOKItem.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewOKItem.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewOKItem.RowHeadersVisible = false;
            this.dataGridViewOKItem.RowHeadersWidth = 35;
            this.dataGridViewOKItem.RowTemplate.Height = 23;
            this.dataGridViewOKItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewOKItem.Size = new System.Drawing.Size(255, 298);
            this.dataGridViewOKItem.TabIndex = 134;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "fitem_code";
            this.dataGridViewTextBoxColumn4.HeaderText = "代号";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 75;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "fname";
            this.dataGridViewTextBoxColumn5.HeaderText = "名称";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 130;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "fprint_num";
            this.dataGridViewTextBoxColumn3.HeaderText = "序";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 40;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "fitem_id";
            this.dataGridViewTextBoxColumn10.HeaderText = "fitem_id";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Visible = false;
            // 
            // fid
            // 
            this.fid.DataPropertyName = "fid";
            this.fid.HeaderText = "fid";
            this.fid.Name = "fid";
            this.fid.ReadOnly = true;
            this.fid.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonNo);
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(591, 312);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(115, 324);
            this.panel1.TabIndex = 135;
            // 
            // buttonNo
            // 
            this.buttonNo.Location = new System.Drawing.Point(8, 139);
            this.buttonNo.Margin = new System.Windows.Forms.Padding(4);
            this.buttonNo.Name = "buttonNo";
            this.buttonNo.Size = new System.Drawing.Size(100, 29);
            this.buttonNo.TabIndex = 3;
            this.buttonNo.Text = "取消";
            this.buttonNo.UseVisualStyleBackColor = true;
            this.buttonNo.Click += new System.EventHandler(this.buttonNo_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(8, 62);
            this.buttonOk.Margin = new System.Windows.Forms.Padding(4);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(100, 29);
            this.buttonOk.TabIndex = 2;
            this.buttonOk.Text = "分配";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // ItemGroupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 636);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.DataGridViewObject);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.wwTreeView1);
            this.Controls.Add(this.bN);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "ItemGroupForm";
            this.Text = "ItemGroupForm";
            this.Load += new System.EventHandler(this.ItemGroupForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bN)).EndInit();
            this.bN.ResumeLayout(false);
            this.bN.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceItemGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAllItem)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOKItem)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.BindingNavigator bN;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonAdd;
        private System.Windows.Forms.ToolStripButton toolStripButtonS;
        private System.Windows.Forms.ToolStripButton toolStripButtonDel;
        private ww.wwf.control.WWControlLib.WWTreeView wwTreeView1;
        private System.Windows.Forms.ImageList imageListTree;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.DataGridView DataGridViewObject;
        private System.Windows.Forms.BindingSource bindingSourceItemGroup;
        private System.Windows.Forms.ToolStripButton toolStripButtonHelpCode;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fuse_if;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fjz_if;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fjz_no_if;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhelp_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn forder_by;
        private System.Windows.Forms.DataGridViewTextBoxColumn fremark;
        private System.Windows.Forms.DataGridViewTextBoxColumn fsam_type_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fcheck_type_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn fgroup_id;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridViewAllItem;
        private System.Windows.Forms.Button buttonNo;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.DataGridView dataGridViewOKItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fprint_num;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn fid;
    }
}