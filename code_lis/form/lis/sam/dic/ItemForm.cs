﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ww.lis.lisbll.sam;

namespace ww.form.lis.sam.dic
{
   // public partial class ItemForm : SysBaseForm
    public partial class ItemForm : ww.form.wwf.SysBaseForm
    {
        ItemBLL bllItem = new ItemBLL();//项目逻辑
        TypeBLL bllType = new TypeBLL();//公共类型逻辑
        DataTable dtItem = new DataTable();//当前项目表     
        DataTable dtRef = new DataTable();//当前参考值明细
        DataRowView rowCurrItem = null;//当前项目行
        string strCurrItemGuid = "";//当前行Guid
        string strInstrID = "";//仪器ID

        DataTable dtInstr = new DataTable(); //fjytype_id
        string strfjytype_id = "";//检验类别为
        public ItemForm()
        {
            InitializeComponent();
            this.fcheck_type_idComboBox.DisplayMember = "fname";
            this.fcheck_type_idComboBox.ValueMember = "fcheck_type_id";

            this.fsam_type_idComboBox.DisplayMember = "fname";
            this.fsam_type_idComboBox.ValueMember = "fsample_type_id";

            this.fitem_type_idComboBox.DisplayMember = "fname";
            this.fitem_type_idComboBox.ValueMember = "fcode";

            this.funit_nameComboBox.DisplayMember = "fname";
            this.funit_nameComboBox.ValueMember = "fname";

            this.fprint_type_idComboBox.DisplayMember = "fname";
            this.fprint_type_idComboBox.ValueMember = "fcode";

            this.fvalue_ddComboBox.DisplayMember = "fname";
            this.fvalue_ddComboBox.ValueMember = "fcode";

            this.fcheck_method_idComboBox.DisplayMember = "fname";
            this.fcheck_method_idComboBox.ValueMember = "fcode";

            this.DataGridViewObject.AutoGenerateColumns = false;
            this.DataGridViewObject.DataSource = this.sam_itemBindingSource;

            //this.bindingSource_ref.DataSource = dtRef;     
            this.dataGridViewRef.AutoGenerateColumns = false;
            this.dataGridViewRef.DataSource = bindingSource_ref;

            this.fsex.DisplayMember = "fname";
            this.fsex.ValueMember = "fname";

            this.fvalue_flag.DisplayMember = "fname";
            this.fvalue_flag.ValueMember = "fcode";
            this.dataGridViewValue.AutoGenerateColumns = false;

            this.fsample_type_id.DisplayMember = "fname";
            this.fsample_type_id.ValueMember = "fsample_type_id";

            this.dataGridViewIO.AutoGenerateColumns = false;
        }

        private void ItemForm_Load(object sender, EventArgs e)
        {
            GetInstrDT();
            GetCheckType();
            GetSamType();
            GetComItemType();

            try
            {
                strInstrID = this.comboBoxInstr.SelectedValue.ToString();
                GetItemDT(strInstrID);
               
                strfjytype_id = "";
                DataRow[] colList = this.dtInstr.Select("(finstr_id = '" + strInstrID + "') ");
                foreach (DataRow drC in colList)
                {
                    strfjytype_id = drC["fjytype_id"].ToString();
                }

               
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 取得仪器所检项目
        /// </summary>
        /// <param name="strInstrID"></param>
        private void GetItemDT(string strInstrID)
        {
            try
            {
                if (dtItem != null)
                    dtItem.Clear();
                dtItem = this.bllItem.BllItem(2, strInstrID);
                this.sam_itemBindingSource.DataSource = dtItem;


            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 取得参考值明细
        /// </summary>
        private void GetRef()
        {
            try
            {
                this.dtRef = this.bllItem.BllItemRefDT(this.strCurrItemGuid);//当前参考值明细
                this.bindingSource_ref.DataSource = dtRef;
                SetrefShow();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void GetPrintList()
        {
            try
            {
                this.dataGridViewPrint.AutoGenerateColumns = false;
                DataTable dtPrint = this.bllItem.BllItem(2, strInstrID);
                this.dataGridViewPrint.DataSource = dtPrint;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary> 
        /// 取得公共类型 1。 结果类型；2。单位
        /// </summary>
        private void GetComItemType()
        {
            try
            {
                this.fitem_type_idComboBox.DataSource = this.bllType.BllComTypeDT("结果类型", 1,"");
                this.funit_nameComboBox.DataSource = this.bllType.BllComTypeDT("常用单位", 1,"");
                this.fprint_type_idComboBox.DataSource = this.bllType.BllComTypeDT("项目结果打印类型", 1,"");
                this.fvalue_ddComboBox.DataSource = this.bllType.BllComTypeDT("项目结果小数位数", 1,"");
                this.fcheck_method_idComboBox.DataSource = this.bllType.BllComTypeDT("项目检验方法", 1,"");
                this.fsex.DataSource = this.bllType.BllComTypeDT("性别", 1,"");
                this.fvalue_flag.DataSource = this.bllType.BllComTypeDT("结果标记", 1,"");
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }

        }
        /// <summary>
        /// 取得检验类别
        /// </summary>
        private void GetCheckType()
        {
            try
            {

                this.fcheck_type_idComboBox.DataSource = this.bllType.BllCheckTypeDT(1);
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }

        }
        /// <summary>
        /// 取得样本类型
        /// </summary>
        private void GetSamType()
        {
            try
            {
                DataTable dtsam = this.bllType.BllSamTypeDT(1);
                this.fsam_type_idComboBox.DataSource = dtsam;
                this.fsample_type_id.DataSource = dtsam;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }

        }

       
        /// <summary>
        /// 已经启用的仪器
        /// </summary>
        private void GetInstrDT()
        {
            try
            {
                if (dtInstr.Rows.Count>0)
                    dtInstr.Clear();
               
                dtInstr = this.bllItem.BllInstrDT(1, ww.wwf.wwfbll.LoginBLL.strDeptID);
                // dtInstr = this.bllItem.BllInstrDT(1, "kfz");
                this.comboBoxInstr.DataSource = dtInstr;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

       
        /// <summary>
        /// 取得接口 参数
        /// </summary>
        private void GetIO()
        {
            try
            {
                this.dataGridViewIO.DataSource = this.bllItem.BllItemIODT(this.strCurrItemGuid);
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 项目常用取值
        /// </summary>
        private void GetItemValue()
        {
            try
            {
                this.dataGridViewValue.DataSource = this.bllItem.BllItemValueDT(this.strCurrItemGuid);
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 取得当前行
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sam_itemBindingSource_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                rowCurrItem = (DataRowView)sam_itemBindingSource.Current;//行
                if (rowCurrItem != null)
                {
                    strCurrItemGuid = rowCurrItem["fitem_id"].ToString();
                   // textBox1.Text = strCurrItemGuid;
                    GetRef();
                    GetPrintList();
                    GetIO();
                    GetItemValue();
                }


                else
                {
                   // textBox1.Text = "";
                    strCurrItemGuid = "";
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        /// <summary>
        /// 自动加帮助符
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fnameTextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                this.fhelp_codeTextBox.Text = ww.wwf.com.Spell.MakeSpellCodeFirstLetterOnly(this.fnameTextBox.Text.Trim());
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        

      

        private void DataGridViewObject_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }
        /// <summary>
        /// 设计计算公式
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void labelformula_Click(object sender, EventArgs e)
        {
            ItemFormulaResult r = new ItemFormulaResult();
            r.TextChanged += new TextChangedHandler1(this.EventResultChanged);
            ItemFormulaForm fc = new ItemFormulaForm(r, dtItem);
            fc.ShowDialog();
        }
        private void EventResultChanged(string s)
        {
            this.fjx_formulaTextBox.Text = s;
        }

        private void labelref_Click(object sender, EventArgs e)
        {
            ItemFormulaResult r = new ItemFormulaResult();
            r.TextChanged += new TextChangedHandler1(this.EventResultChangedManyRef);
            ItemManyRefForm fc = new ItemManyRefForm(r);
            fc.ShowDialog();
        }
        private void EventResultChangedManyRef(string s)
        {
            this.frefTextBox.Text = s;
        }

        private void SetrefShow()
        {
            if (fref_if_sexCheckBox.Checked)
                this.fsex.Visible = true;
            else
                this.fsex.Visible = false;
            if (fref_if_ageCheckBox.Checked)
            {
                this.fage_high.Visible = true;
                this.fage_low.Visible = true;
            }
            else
            {
                this.fage_high.Visible = false;
                this.fage_low.Visible = false;
            }
            if (fref_if_sampleCheckBox.Checked)
                this.fsample_type_id.Visible = true;
            else
                this.fsample_type_id.Visible = false;
        }

        private void fref_if_sexCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            SetrefShow();

        }

        private void fref_if_ageCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            SetrefShow();
        }

        private void fref_if_sampleCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            SetrefShow();
        }
        /// <summary>
        /// 新增参考值明细
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonrefAdd_Click(object sender, EventArgs e)
        {
            try
            {

                this.bllItem.BllItemRefAdd(strCurrItemGuid);
                GetRef();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }

        }
        /// <summary>
        /// 删除参考明细
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonrefDel_Click(object sender, EventArgs e)
        {
            try
            {
                if (DataGridViewObject.Rows.Count > 0)
                {
                    if (ww.wwf.wwfbll.WWMessage.MessageDialogResult("确认要删除此参考值明细？"))
                    {
                        string fref_id = this.dataGridViewRef.CurrentRow.Cells["fref_id"].Value.ToString();
                        this.bllItem.BllItemRefDel(fref_id);
                        GetRef();
                    }
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }

        }
        /// <summary>
        /// 保存参考值明细
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonrefSave_Click(object sender, EventArgs e)
        {
            try
            {

                for (int i = 0; i < this.dtRef.Rows.Count; i++)
                {
                    this.bllItem.BllItemRefUpdate(this.dtRef.Rows[i]);
                }
                GetRef();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
      

       
        private void fref_highTextBox_Leave(object sender, EventArgs e)
        {
            frefTextBox.Text = fref_lowTextBox.Text + "--" + fref_highTextBox.Text;
        }

        private void fref_lowTextBox_Leave(object sender, EventArgs e)
        {
            frefTextBox.Text = fref_lowTextBox.Text + "--" + fref_highTextBox.Text;
        }

        private void dataGridViewRef_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }
        /// <summary>
        /// 新增接口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonIOAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.bllItem.BllItemIOAdd(strCurrItemGuid);
                GetIO();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 保存接口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonIOSave_Click(object sender, EventArgs e)
        {
            try
            {
                //Cursor = Cursors.WaitCursor;
                this.Validate();
                this.dataGridViewIO.EndEdit();
               string str仪器id= comboBoxInstr.SelectedValue.ToString();
                for (int i = 0; i < this.dataGridViewIO.Rows.Count; i++)
                {
                    this.bllItem.BllItemIOUpdate(this.dataGridViewIO.Rows[i].Cells["fio_id"].Value.ToString(), this.dataGridViewIO.Rows[i].Cells["fcode"].Value.ToString(), str仪器id);
                }
                GetIO();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 删除接口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonIODel_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridViewIO.Rows.Count > 0)
                {
                    if (ww.wwf.wwfbll.WWMessage.MessageDialogResult("确认要删除？"))
                    {
                        string fio_id = this.dataGridViewIO.CurrentRow.Cells["fio_id"].Value.ToString();
                        this.bllItem.BllItemIODel(fio_id);
                        GetIO();
                    }
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void dataGridView2_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void buttonPrintRef_Click(object sender, EventArgs e)
        {
            try
            {
                GetPrintList();
                GetItemDT(strInstrID);
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void buttonPrintSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                //this.Validate();
                //this.sam_itemBindingSource.EndEdit();
                if (dataGridViewPrint.Rows.Count > 0)
                {
                    string strnum = "";
                    string strid = "";
                    for (int i = 0; i < this.dataGridViewPrint.Rows.Count; i++)
                    {
                        strid = this.dataGridViewPrint.Rows[i].Cells["fitem_id"].Value.ToString();
                        strnum = this.dataGridViewPrint.Rows[i].Cells["Itemfprint_num"].Value.ToString();
                        this.bllItem.BllItemUpdatePrintNum(strid, strnum);
                    }
                }
                GetPrintList();
                GetItemDT(strInstrID);
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void buttonItemValueAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.bllItem.BllItemValueAdd(strCurrItemGuid);
                GetItemValue();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void buttonItemValueSave_Click(object sender, EventArgs e)
        {
            try
            {
                //Cursor = Cursors.WaitCursor;
                this.Validate();
                this.dataGridViewValue.EndEdit();
                for (int i = 0; i < this.dataGridViewValue.Rows.Count; i++)
                {
                    //MessageBox.Show(this.dataGridViewValue.Rows[i].Cells["fvalue_flag"].Value.ToString());
                    this.bllItem.BllItemValueUpdate(this.dataGridViewValue.Rows[i].Cells["fvalue_id"].Value.ToString(), this.dataGridViewValue.Rows[i].Cells["fvalue"].Value.ToString(), this.dataGridViewValue.Rows[i].Cells["fvalue_flag"].Value.ToString(), this.dataGridViewValue.Rows[i].Cells["forder_by"].Value.ToString());
                }
                GetItemValue();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void buttonItemValueDel_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridViewValue.Rows.Count > 0)
                {
                    if (ww.wwf.wwfbll.WWMessage.MessageDialogResult("确认要删除？"))
                    {
                        string fio_id = this.dataGridViewValue.CurrentRow.Cells["fvalue_id"].Value.ToString();
                        this.bllItem.BllItemValueDel(fio_id);
                        GetItemValue();
                    }
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void comboBoxInstr_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                strInstrID = this.comboBoxInstr.SelectedValue.ToString();
                GetItemDT(strInstrID);
                strfjytype_id = "";
                DataRow[] colList = this.dtInstr.Select("(finstr_id = '" + strInstrID + "') ");
                foreach (DataRow drC in colList)
                {
                    strfjytype_id = drC["fjytype_id"].ToString();
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonAdd_Click(object sender, EventArgs e)
        {
            try
            {
                strCurrItemGuid = this.bllItem.DbGuid();
                this.bllItem.BllItemAdd(strInstrID, strCurrItemGuid, strfjytype_id,"");
                GetItemDT(strInstrID);
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonS_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                this.Validate();
                this.sam_itemBindingSource.EndEdit();
               // MessageBox.Show(fsam_type_idComboBox.SelectedValue.ToString());
                for (int i = 0; i < this.dtItem.Rows.Count; i++)
                {
                    this.bllItem.BllItemUpdate(this.dtItem.Rows[i]);

                }
                GetItemDT(strInstrID);
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
           
        }

        private void toolStripButtonR_Click(object sender, EventArgs e)
        {
            try
            {

                GetItemDT(strInstrID);
                GetPrintList();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonDel_Click(object sender, EventArgs e)
        {
            try
            {
                if (DataGridViewObject.Rows.Count > 0)
                {
                    if (ww.wwf.wwfbll.WWMessage.MessageDialogResult("确认删除？"))
                    {
                        string strRet = this.bllItem.BllItemDel(this.strCurrItemGuid);
                        if (strRet == "true")
                            GetItemDT(this.strInstrID);
                        else
                            ww.wwf.wwfbll.WWMessage.MessageShowError(strRet);
                    }
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        protected override bool ProcessCmdKey(ref   System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            Type type = this.ActiveControl.GetType();
            //如果当前焦点在仪器、日期两个控件上，则禁止两个控件的操作
            if ((keyData == (Keys.Down) || keyData == (Keys.Up))
                 && (type.Name=="ComboBox")
                )
            {
                return true;
            }

            return base.ProcessCmdKey(ref   msg, keyData);
        }
    }
}