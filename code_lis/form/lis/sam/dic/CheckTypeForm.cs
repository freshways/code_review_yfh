﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.lis.lisbll.sam;

namespace ww.form.lis.sam.dic
{
    public partial class CheckTypeForm :ww.form.wwf.SysBaseForm
    {
        TypeBLL bllType = new TypeBLL();//类别
        public CheckTypeForm()
        {
            InitializeComponent();
        }

        private void CheckTypeForm_Load(object sender, EventArgs e)
        {
            GetCheckType();
        }
        /// <summary>
        /// 取得检验类别
        /// </summary>
        private void GetCheckType()
        {
            try
            {
                bindingSource1.DataSource = this.bllType.BllCheckTypeDT(1);
                this.DataGridViewObject.AutoGenerateColumns = false;
                this.DataGridViewObject.DataSource = bindingSource1;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }

        }
    }
}