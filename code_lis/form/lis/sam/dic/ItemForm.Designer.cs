﻿namespace ww.form.lis.sam.dic
{
    partial class ItemForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label fitem_codeLabel;
            System.Windows.Forms.Label fcheck_type_idLabel;
            System.Windows.Forms.Label fitem_type_idLabel;
            System.Windows.Forms.Label fnameLabel;
            System.Windows.Forms.Label fname_jLabel;
            System.Windows.Forms.Label fhelp_codeLabel;
            System.Windows.Forms.Label fhis_item_codeLabel;
            System.Windows.Forms.Label funit_nameLabel;
            System.Windows.Forms.Label fxsLabel;
            System.Windows.Forms.Label fvalue_ddLabel;
            System.Windows.Forms.Label fprint_type_idLabel;
            System.Windows.Forms.Label fpriceLabel;
            System.Windows.Forms.Label fcheck_method_idLabel;
            System.Windows.Forms.Label fuse_ifLabel;
            System.Windows.Forms.Label fjx_ifLabel;
            System.Windows.Forms.Label fjx_formulaLabel;
            System.Windows.Forms.Label fref_highLabel;
            System.Windows.Forms.Label fref_lowLabel;
            System.Windows.Forms.Label frefLabel;
            System.Windows.Forms.Label fname_eLabel;
            System.Windows.Forms.Label fjg_value_sxLabel;
            System.Windows.Forms.Label fjg_value_xxLabel;
            System.Windows.Forms.Label fvalue_day_numLabel;
            System.Windows.Forms.Label fvalue_day_noLabel;
            System.Windows.Forms.Label fsam_type_idLabel;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.DataGridViewObject = new System.Windows.Forms.DataGridView();
            this.fuse_if = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fitem_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.funit_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fref = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fprint_num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fxs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.comboBoxInstr = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.sam_itemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.samDataSet = new ww.form.lis.sam.samDataSet();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.fref_if_sampleCheckBox = new System.Windows.Forms.CheckBox();
            this.fref_if_ageCheckBox = new System.Windows.Forms.CheckBox();
            this.fref_if_sexCheckBox = new System.Windows.Forms.CheckBox();
            this.labelref = new System.Windows.Forms.Label();
            this.frefTextBox = new System.Windows.Forms.TextBox();
            this.fref_lowTextBox = new System.Windows.Forms.TextBox();
            this.fref_highTextBox = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.fvalue_day_noTextBox = new System.Windows.Forms.TextBox();
            this.fvalue_day_numTextBox = new System.Windows.Forms.TextBox();
            this.fjg_value_xxTextBox = new System.Windows.Forms.TextBox();
            this.fjg_value_sxTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.fsam_type_idComboBox = new System.Windows.Forms.ComboBox();
            this.fname_eTextBox = new System.Windows.Forms.TextBox();
            this.labelformula = new System.Windows.Forms.Label();
            this.fjx_formulaTextBox = new System.Windows.Forms.TextBox();
            this.fjx_ifCheckBox = new System.Windows.Forms.CheckBox();
            this.fuse_ifCheckBox = new System.Windows.Forms.CheckBox();
            this.fcheck_method_idComboBox = new System.Windows.Forms.ComboBox();
            this.fpriceTextBox = new System.Windows.Forms.TextBox();
            this.fprint_type_idComboBox = new System.Windows.Forms.ComboBox();
            this.fvalue_ddComboBox = new System.Windows.Forms.ComboBox();
            this.fxsTextBox = new System.Windows.Forms.TextBox();
            this.funit_nameComboBox = new System.Windows.Forms.ComboBox();
            this.fhis_item_codeTextBox = new System.Windows.Forms.TextBox();
            this.fhelp_codeTextBox = new System.Windows.Forms.TextBox();
            this.fname_jTextBox = new System.Windows.Forms.TextBox();
            this.fnameTextBox = new System.Windows.Forms.TextBox();
            this.fitem_type_idComboBox = new System.Windows.Forms.ComboBox();
            this.fcheck_type_idComboBox = new System.Windows.Forms.ComboBox();
            this.fitem_codeTextBox = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridViewRef = new System.Windows.Forms.DataGridView();
            this.fsex = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.fage_low = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fage_high = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fsample_type_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.fref_low = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fref_high = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fref1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fref_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.buttonrefSave = new System.Windows.Forms.Button();
            this.buttonrefDel = new System.Windows.Forms.Button();
            this.buttonrefAdd = new System.Windows.Forms.Button();
            this.tabPagefyy = new System.Windows.Forms.TabPage();
            this.fyyRichTextBox = new System.Windows.Forms.RichTextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dataGridViewIO = new System.Windows.Forms.DataGridView();
            this.fcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iofitem_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iofuse_if = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fio_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.finstr_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel4 = new System.Windows.Forms.Panel();
            this.buttonIOSave = new System.Windows.Forms.Button();
            this.buttonIODel = new System.Windows.Forms.Button();
            this.buttonIOAdd = new System.Windows.Forms.Button();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.dataGridViewPrint = new System.Windows.Forms.DataGridView();
            this.ItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Itemfprint_num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel5 = new System.Windows.Forms.Panel();
            this.buttonPrintSave = new System.Windows.Forms.Button();
            this.buttonPrintRef = new System.Windows.Forms.Button();
            this.tabPageValue = new System.Windows.Forms.TabPage();
            this.dataGridViewValue = new System.Windows.Forms.DataGridView();
            this.fvalue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fvalue_flag = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.forder_by = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fvalue_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valuefitem_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel6 = new System.Windows.Forms.Panel();
            this.buttonItemValueSave = new System.Windows.Forms.Button();
            this.buttonItemValueDel = new System.Windows.Forms.Button();
            this.buttonItemValueAdd = new System.Windows.Forms.Button();
            this.bindingSource_ref = new System.Windows.Forms.BindingSource(this.components);
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.bindingSource_fsex = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource_fsample_type_id = new System.Windows.Forms.BindingSource(this.components);
            this.bN = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonS = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonDel = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonR = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            fitem_codeLabel = new System.Windows.Forms.Label();
            fcheck_type_idLabel = new System.Windows.Forms.Label();
            fitem_type_idLabel = new System.Windows.Forms.Label();
            fnameLabel = new System.Windows.Forms.Label();
            fname_jLabel = new System.Windows.Forms.Label();
            fhelp_codeLabel = new System.Windows.Forms.Label();
            fhis_item_codeLabel = new System.Windows.Forms.Label();
            funit_nameLabel = new System.Windows.Forms.Label();
            fxsLabel = new System.Windows.Forms.Label();
            fvalue_ddLabel = new System.Windows.Forms.Label();
            fprint_type_idLabel = new System.Windows.Forms.Label();
            fpriceLabel = new System.Windows.Forms.Label();
            fcheck_method_idLabel = new System.Windows.Forms.Label();
            fuse_ifLabel = new System.Windows.Forms.Label();
            fjx_ifLabel = new System.Windows.Forms.Label();
            fjx_formulaLabel = new System.Windows.Forms.Label();
            fref_highLabel = new System.Windows.Forms.Label();
            fref_lowLabel = new System.Windows.Forms.Label();
            frefLabel = new System.Windows.Forms.Label();
            fname_eLabel = new System.Windows.Forms.Label();
            fjg_value_sxLabel = new System.Windows.Forms.Label();
            fjg_value_xxLabel = new System.Windows.Forms.Label();
            fvalue_day_numLabel = new System.Windows.Forms.Label();
            fvalue_day_noLabel = new System.Windows.Forms.Label();
            fsam_type_idLabel = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sam_itemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRef)).BeginInit();
            this.panel3.SuspendLayout();
            this.tabPagefyy.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIO)).BeginInit();
            this.panel4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPrint)).BeginInit();
            this.panel5.SuspendLayout();
            this.tabPageValue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewValue)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_ref)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_fsex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_fsample_type_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).BeginInit();
            this.bN.SuspendLayout();
            this.SuspendLayout();
            // 
            // fitem_codeLabel
            // 
            fitem_codeLabel.AutoSize = true;
            fitem_codeLabel.Location = new System.Drawing.Point(20, 58);
            fitem_codeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fitem_codeLabel.Name = "fitem_codeLabel";
            fitem_codeLabel.Size = new System.Drawing.Size(75, 15);
            fitem_codeLabel.TabIndex = 0;
            fitem_codeLabel.Text = "项目代码:";
            // 
            // fcheck_type_idLabel
            // 
            fcheck_type_idLabel.AutoSize = true;
            fcheck_type_idLabel.Location = new System.Drawing.Point(20, 30);
            fcheck_type_idLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fcheck_type_idLabel.Name = "fcheck_type_idLabel";
            fcheck_type_idLabel.Size = new System.Drawing.Size(75, 15);
            fcheck_type_idLabel.TabIndex = 2;
            fcheck_type_idLabel.Text = "检验类型:";
            // 
            // fitem_type_idLabel
            // 
            fitem_type_idLabel.AutoSize = true;
            fitem_type_idLabel.Location = new System.Drawing.Point(280, 58);
            fitem_type_idLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fitem_type_idLabel.Name = "fitem_type_idLabel";
            fitem_type_idLabel.Size = new System.Drawing.Size(75, 15);
            fitem_type_idLabel.TabIndex = 4;
            fitem_type_idLabel.Text = "结果类型:";
            // 
            // fnameLabel
            // 
            fnameLabel.AutoSize = true;
            fnameLabel.Location = new System.Drawing.Point(20, 86);
            fnameLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fnameLabel.Name = "fnameLabel";
            fnameLabel.Size = new System.Drawing.Size(75, 15);
            fnameLabel.TabIndex = 8;
            fnameLabel.Text = "项目名称:";
            // 
            // fname_jLabel
            // 
            fname_jLabel.AutoSize = true;
            fname_jLabel.Location = new System.Drawing.Point(20, 115);
            fname_jLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fname_jLabel.Name = "fname_jLabel";
            fname_jLabel.Size = new System.Drawing.Size(75, 15);
            fname_jLabel.TabIndex = 10;
            fname_jLabel.Text = "项目简称:";
            // 
            // fhelp_codeLabel
            // 
            fhelp_codeLabel.AutoSize = true;
            fhelp_codeLabel.Location = new System.Drawing.Point(36, 144);
            fhelp_codeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fhelp_codeLabel.Name = "fhelp_codeLabel";
            fhelp_codeLabel.Size = new System.Drawing.Size(60, 15);
            fhelp_codeLabel.TabIndex = 12;
            fhelp_codeLabel.Text = "助记符:";
            // 
            // fhis_item_codeLabel
            // 
            fhis_item_codeLabel.AutoSize = true;
            fhis_item_codeLabel.Location = new System.Drawing.Point(288, 230);
            fhis_item_codeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fhis_item_codeLabel.Name = "fhis_item_codeLabel";
            fhis_item_codeLabel.Size = new System.Drawing.Size(69, 15);
            fhis_item_codeLabel.TabIndex = 14;
            fhis_item_codeLabel.Text = "HIS代码:";
            // 
            // funit_nameLabel
            // 
            funit_nameLabel.AutoSize = true;
            funit_nameLabel.Location = new System.Drawing.Point(52, 172);
            funit_nameLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            funit_nameLabel.Name = "funit_nameLabel";
            funit_nameLabel.Size = new System.Drawing.Size(45, 15);
            funit_nameLabel.TabIndex = 16;
            funit_nameLabel.Text = "单位:";
            // 
            // fxsLabel
            // 
            fxsLabel.AutoSize = true;
            fxsLabel.Location = new System.Drawing.Point(280, 144);
            fxsLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fxsLabel.Name = "fxsLabel";
            fxsLabel.Size = new System.Drawing.Size(75, 15);
            fxsLabel.TabIndex = 18;
            fxsLabel.Text = "调整系数:";
            // 
            // fvalue_ddLabel
            // 
            fvalue_ddLabel.AutoSize = true;
            fvalue_ddLabel.Location = new System.Drawing.Point(20, 201);
            fvalue_ddLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fvalue_ddLabel.Name = "fvalue_ddLabel";
            fvalue_ddLabel.Size = new System.Drawing.Size(75, 15);
            fvalue_ddLabel.TabIndex = 20;
            fvalue_ddLabel.Text = "小数位数:";
            // 
            // fprint_type_idLabel
            // 
            fprint_type_idLabel.AutoSize = true;
            fprint_type_idLabel.Location = new System.Drawing.Point(280, 172);
            fprint_type_idLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fprint_type_idLabel.Name = "fprint_type_idLabel";
            fprint_type_idLabel.Size = new System.Drawing.Size(75, 15);
            fprint_type_idLabel.TabIndex = 22;
            fprint_type_idLabel.Text = "打印类型:";
            // 
            // fpriceLabel
            // 
            fpriceLabel.AutoSize = true;
            fpriceLabel.Location = new System.Drawing.Point(52, 230);
            fpriceLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fpriceLabel.Name = "fpriceLabel";
            fpriceLabel.Size = new System.Drawing.Size(45, 15);
            fpriceLabel.TabIndex = 24;
            fpriceLabel.Text = "价格:";
            // 
            // fcheck_method_idLabel
            // 
            fcheck_method_idLabel.AutoSize = true;
            fcheck_method_idLabel.Location = new System.Drawing.Point(280, 201);
            fcheck_method_idLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fcheck_method_idLabel.Name = "fcheck_method_idLabel";
            fcheck_method_idLabel.Size = new System.Drawing.Size(75, 15);
            fcheck_method_idLabel.TabIndex = 29;
            fcheck_method_idLabel.Text = "检验方法:";
            // 
            // fuse_ifLabel
            // 
            fuse_ifLabel.AutoSize = true;
            fuse_ifLabel.Location = new System.Drawing.Point(432, 86);
            fuse_ifLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fuse_ifLabel.Name = "fuse_ifLabel";
            fuse_ifLabel.Size = new System.Drawing.Size(60, 15);
            fuse_ifLabel.TabIndex = 30;
            fuse_ifLabel.Text = "启用否:";
            // 
            // fjx_ifLabel
            // 
            fjx_ifLabel.AutoSize = true;
            fjx_ifLabel.Location = new System.Drawing.Point(20, 260);
            fjx_ifLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fjx_ifLabel.Name = "fjx_ifLabel";
            fjx_ifLabel.Size = new System.Drawing.Size(75, 15);
            fjx_ifLabel.TabIndex = 31;
            fjx_ifLabel.Text = "计算项目:";
            // 
            // fjx_formulaLabel
            // 
            fjx_formulaLabel.AutoSize = true;
            fjx_formulaLabel.Location = new System.Drawing.Point(152, 260);
            fjx_formulaLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fjx_formulaLabel.Name = "fjx_formulaLabel";
            fjx_formulaLabel.Size = new System.Drawing.Size(45, 15);
            fjx_formulaLabel.TabIndex = 32;
            fjx_formulaLabel.Text = "公式:";
            // 
            // fref_highLabel
            // 
            fref_highLabel.AutoSize = true;
            fref_highLabel.Location = new System.Drawing.Point(276, 22);
            fref_highLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fref_highLabel.Name = "fref_highLabel";
            fref_highLabel.Size = new System.Drawing.Size(90, 15);
            fref_highLabel.TabIndex = 0;
            fref_highLabel.Text = "参考值上线:";
            // 
            // fref_lowLabel
            // 
            fref_lowLabel.AutoSize = true;
            fref_lowLabel.Location = new System.Drawing.Point(0, 22);
            fref_lowLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fref_lowLabel.Name = "fref_lowLabel";
            fref_lowLabel.Size = new System.Drawing.Size(90, 15);
            fref_lowLabel.TabIndex = 2;
            fref_lowLabel.Text = "参考值下线:";
            // 
            // frefLabel
            // 
            frefLabel.AutoSize = true;
            frefLabel.Location = new System.Drawing.Point(40, 56);
            frefLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            frefLabel.Name = "frefLabel";
            frefLabel.Size = new System.Drawing.Size(60, 15);
            frefLabel.TabIndex = 4;
            frefLabel.Text = "参考值:";
            // 
            // fname_eLabel
            // 
            fname_eLabel.AutoSize = true;
            fname_eLabel.Location = new System.Drawing.Point(296, 115);
            fname_eLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fname_eLabel.Name = "fname_eLabel";
            fname_eLabel.Size = new System.Drawing.Size(60, 15);
            fname_eLabel.TabIndex = 34;
            fname_eLabel.Text = "英文名:";
            // 
            // fjg_value_sxLabel
            // 
            fjg_value_sxLabel.AutoSize = true;
            fjg_value_sxLabel.Location = new System.Drawing.Point(265, 29);
            fjg_value_sxLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fjg_value_sxLabel.Name = "fjg_value_sxLabel";
            fjg_value_sxLabel.Size = new System.Drawing.Size(90, 15);
            fjg_value_sxLabel.TabIndex = 0;
            fjg_value_sxLabel.Text = "警告值上限:";
            // 
            // fjg_value_xxLabel
            // 
            fjg_value_xxLabel.AutoSize = true;
            fjg_value_xxLabel.Location = new System.Drawing.Point(32, 29);
            fjg_value_xxLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fjg_value_xxLabel.Name = "fjg_value_xxLabel";
            fjg_value_xxLabel.Size = new System.Drawing.Size(90, 15);
            fjg_value_xxLabel.TabIndex = 2;
            fjg_value_xxLabel.Text = "警告值下限:";
            // 
            // fvalue_day_numLabel
            // 
            fvalue_day_numLabel.AutoSize = true;
            fvalue_day_numLabel.Location = new System.Drawing.Point(240, 66);
            fvalue_day_numLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fvalue_day_numLabel.Name = "fvalue_day_numLabel";
            fvalue_day_numLabel.Size = new System.Drawing.Size(112, 15);
            fvalue_day_numLabel.TabIndex = 4;
            fvalue_day_numLabel.Text = "天内不可能相差";
            // 
            // fvalue_day_noLabel
            // 
            fvalue_day_noLabel.AutoSize = true;
            fvalue_day_noLabel.Location = new System.Drawing.Point(25, 66);
            fvalue_day_noLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fvalue_day_noLabel.Name = "fvalue_day_noLabel";
            fvalue_day_noLabel.Size = new System.Drawing.Size(142, 15);
            fvalue_day_noLabel.TabIndex = 6;
            fvalue_day_noLabel.Text = "病人两次检验结果在";
            // 
            // fsam_type_idLabel
            // 
            fsam_type_idLabel.AutoSize = true;
            fsam_type_idLabel.Location = new System.Drawing.Point(280, 30);
            fsam_type_idLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fsam_type_idLabel.Name = "fsam_type_idLabel";
            fsam_type_idLabel.Size = new System.Drawing.Size(75, 15);
            fsam_type_idLabel.TabIndex = 35;
            fsam_type_idLabel.Text = "样本类型:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.DataGridViewObject);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(4, 38);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(441, 710);
            this.panel1.TabIndex = 0;
            // 
            // DataGridViewObject
            // 
            this.DataGridViewObject.AllowUserToAddRows = false;
            this.DataGridViewObject.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DataGridViewObject.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridViewObject.ColumnHeadersHeight = 21;
            this.DataGridViewObject.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fuse_if,
            this.fitem_code,
            this.fname,
            this.funit_name,
            this.fref,
            this.fprint_num,
            this.fxs});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridViewObject.DefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridViewObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridViewObject.EnableHeadersVisualStyles = false;
            this.DataGridViewObject.Location = new System.Drawing.Point(0, 40);
            this.DataGridViewObject.Margin = new System.Windows.Forms.Padding(4);
            this.DataGridViewObject.MultiSelect = false;
            this.DataGridViewObject.Name = "DataGridViewObject";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridViewObject.RowHeadersVisible = false;
            this.DataGridViewObject.RowHeadersWidth = 35;
            this.DataGridViewObject.RowTemplate.Height = 23;
            this.DataGridViewObject.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewObject.Size = new System.Drawing.Size(441, 670);
            this.DataGridViewObject.TabIndex = 121;
            this.DataGridViewObject.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataGridViewObject_DataError);
            // 
            // fuse_if
            // 
            this.fuse_if.DataPropertyName = "fuse_if";
            this.fuse_if.FalseValue = "0";
            this.fuse_if.HeaderText = "启用";
            this.fuse_if.Name = "fuse_if";
            this.fuse_if.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fuse_if.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fuse_if.TrueValue = "1";
            this.fuse_if.Width = 40;
            // 
            // fitem_code
            // 
            this.fitem_code.DataPropertyName = "fitem_code";
            this.fitem_code.HeaderText = "代号";
            this.fitem_code.Name = "fitem_code";
            this.fitem_code.Width = 60;
            // 
            // fname
            // 
            this.fname.DataPropertyName = "fname";
            this.fname.HeaderText = "名称";
            this.fname.Name = "fname";
            this.fname.Width = 140;
            // 
            // funit_name
            // 
            this.funit_name.DataPropertyName = "funit_name";
            this.funit_name.HeaderText = "单位";
            this.funit_name.Name = "funit_name";
            this.funit_name.Width = 80;
            // 
            // fref
            // 
            this.fref.DataPropertyName = "fref";
            this.fref.HeaderText = "参考值";
            this.fref.Name = "fref";
            this.fref.Width = 80;
            // 
            // fprint_num
            // 
            this.fprint_num.DataPropertyName = "fprint_num";
            this.fprint_num.HeaderText = "排序";
            this.fprint_num.Name = "fprint_num";
            this.fprint_num.Width = 55;
            // 
            // fxs
            // 
            this.fxs.DataPropertyName = "fxs";
            this.fxs.HeaderText = "调整系数";
            this.fxs.Name = "fxs";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.comboBoxInstr);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(441, 40);
            this.panel2.TabIndex = 0;
            // 
            // comboBoxInstr
            // 
            this.comboBoxInstr.DisplayMember = "ShowName";
            this.comboBoxInstr.FormattingEnabled = true;
            this.comboBoxInstr.Location = new System.Drawing.Point(69, 11);
            this.comboBoxInstr.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxInstr.Name = "comboBoxInstr";
            this.comboBoxInstr.Size = new System.Drawing.Size(365, 23);
            this.comboBoxInstr.TabIndex = 1;
            this.comboBoxInstr.ValueMember = "finstr_id";
            this.comboBoxInstr.DropDownClosed += new System.EventHandler(this.comboBoxInstr_DropDownClosed);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 16);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "仪器：";
            // 
            // sam_itemBindingSource
            // 
            this.sam_itemBindingSource.DataMember = "sam_item";
            this.sam_itemBindingSource.DataSource = this.samDataSet;
            this.sam_itemBindingSource.PositionChanged += new System.EventHandler(this.sam_itemBindingSource_PositionChanged);
            // 
            // samDataSet
            // 
            this.samDataSet.DataSetName = "samDataSet";
            this.samDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPagefyy);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPageValue);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.tabControl1.Location = new System.Drawing.Point(445, 38);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(608, 710);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(600, 681);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "项目信息";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.fref_if_sampleCheckBox);
            this.groupBox2.Controls.Add(this.fref_if_ageCheckBox);
            this.groupBox2.Controls.Add(this.fref_if_sexCheckBox);
            this.groupBox2.Controls.Add(this.labelref);
            this.groupBox2.Controls.Add(frefLabel);
            this.groupBox2.Controls.Add(this.frefTextBox);
            this.groupBox2.Controls.Add(fref_lowLabel);
            this.groupBox2.Controls.Add(this.fref_lowTextBox);
            this.groupBox2.Controls.Add(fref_highLabel);
            this.groupBox2.Controls.Add(this.fref_highTextBox);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(4, 295);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(592, 131);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "参考值";
            // 
            // fref_if_sampleCheckBox
            // 
            this.fref_if_sampleCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.sam_itemBindingSource, "fref_if_sample", true));
            this.fref_if_sampleCheckBox.Location = new System.Drawing.Point(399, 85);
            this.fref_if_sampleCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.fref_if_sampleCheckBox.Name = "fref_if_sampleCheckBox";
            this.fref_if_sampleCheckBox.Size = new System.Drawing.Size(167, 30);
            this.fref_if_sampleCheckBox.TabIndex = 40;
            this.fref_if_sampleCheckBox.Text = "参考值与样本有关";
            this.fref_if_sampleCheckBox.CheckedChanged += new System.EventHandler(this.fref_if_sampleCheckBox_CheckedChanged);
            // 
            // fref_if_ageCheckBox
            // 
            this.fref_if_ageCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.sam_itemBindingSource, "fref_if_age", true));
            this.fref_if_ageCheckBox.Location = new System.Drawing.Point(228, 85);
            this.fref_if_ageCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.fref_if_ageCheckBox.Name = "fref_if_ageCheckBox";
            this.fref_if_ageCheckBox.Size = new System.Drawing.Size(163, 30);
            this.fref_if_ageCheckBox.TabIndex = 38;
            this.fref_if_ageCheckBox.Text = "参考值与年龄有关";
            this.fref_if_ageCheckBox.CheckedChanged += new System.EventHandler(this.fref_if_ageCheckBox_CheckedChanged);
            // 
            // fref_if_sexCheckBox
            // 
            this.fref_if_sexCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.sam_itemBindingSource, "fref_if_sex", true));
            this.fref_if_sexCheckBox.Location = new System.Drawing.Point(49, 85);
            this.fref_if_sexCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.fref_if_sexCheckBox.Name = "fref_if_sexCheckBox";
            this.fref_if_sexCheckBox.Size = new System.Drawing.Size(171, 30);
            this.fref_if_sexCheckBox.TabIndex = 36;
            this.fref_if_sexCheckBox.Text = "参考值与性别有关";
            this.fref_if_sexCheckBox.CheckedChanged += new System.EventHandler(this.fref_if_sexCheckBox_CheckedChanged);
            // 
            // labelref
            // 
            this.labelref.AutoSize = true;
            this.labelref.ForeColor = System.Drawing.Color.Navy;
            this.labelref.Location = new System.Drawing.Point(479, 56);
            this.labelref.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelref.Name = "labelref";
            this.labelref.Size = new System.Drawing.Size(106, 15);
            this.labelref.TabIndex = 35;
            this.labelref.Text = "多行参考值...";
            this.labelref.Click += new System.EventHandler(this.labelref_Click);
            // 
            // frefTextBox
            // 
            this.frefTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fref", true));
            this.frefTextBox.Location = new System.Drawing.Point(103, 51);
            this.frefTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.frefTextBox.Name = "frefTextBox";
            this.frefTextBox.Size = new System.Drawing.Size(367, 25);
            this.frefTextBox.TabIndex = 5;
            // 
            // fref_lowTextBox
            // 
            this.fref_lowTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fref_low", true));
            this.fref_lowTextBox.Location = new System.Drawing.Point(103, 18);
            this.fref_lowTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.fref_lowTextBox.Name = "fref_lowTextBox";
            this.fref_lowTextBox.Size = new System.Drawing.Size(160, 25);
            this.fref_lowTextBox.TabIndex = 3;
            this.fref_lowTextBox.Leave += new System.EventHandler(this.fref_lowTextBox_Leave);
            // 
            // fref_highTextBox
            // 
            this.fref_highTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fref_high", true));
            this.fref_highTextBox.Location = new System.Drawing.Point(379, 18);
            this.fref_highTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.fref_highTextBox.Name = "fref_highTextBox";
            this.fref_highTextBox.Size = new System.Drawing.Size(160, 25);
            this.fref_highTextBox.TabIndex = 1;
            this.fref_highTextBox.Leave += new System.EventHandler(this.fref_highTextBox_Leave);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(fvalue_day_noLabel);
            this.groupBox3.Controls.Add(this.fvalue_day_noTextBox);
            this.groupBox3.Controls.Add(fvalue_day_numLabel);
            this.groupBox3.Controls.Add(this.fvalue_day_numTextBox);
            this.groupBox3.Controls.Add(fjg_value_xxLabel);
            this.groupBox3.Controls.Add(this.fjg_value_xxTextBox);
            this.groupBox3.Controls.Add(fjg_value_sxLabel);
            this.groupBox3.Controls.Add(this.fjg_value_sxTextBox);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox3.Location = new System.Drawing.Point(4, 445);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(592, 232);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "医疗审核条件";
            // 
            // fvalue_day_noTextBox
            // 
            this.fvalue_day_noTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fvalue_day_no", true));
            this.fvalue_day_noTextBox.Location = new System.Drawing.Point(192, 61);
            this.fvalue_day_noTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.fvalue_day_noTextBox.Name = "fvalue_day_noTextBox";
            this.fvalue_day_noTextBox.Size = new System.Drawing.Size(39, 25);
            this.fvalue_day_noTextBox.TabIndex = 7;
            // 
            // fvalue_day_numTextBox
            // 
            this.fvalue_day_numTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fvalue_day_num", true));
            this.fvalue_day_numTextBox.Location = new System.Drawing.Point(365, 61);
            this.fvalue_day_numTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.fvalue_day_numTextBox.Name = "fvalue_day_numTextBox";
            this.fvalue_day_numTextBox.Size = new System.Drawing.Size(124, 25);
            this.fvalue_day_numTextBox.TabIndex = 5;
            // 
            // fjg_value_xxTextBox
            // 
            this.fjg_value_xxTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fjg_value_xx", true));
            this.fjg_value_xxTextBox.Location = new System.Drawing.Point(132, 24);
            this.fjg_value_xxTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.fjg_value_xxTextBox.Name = "fjg_value_xxTextBox";
            this.fjg_value_xxTextBox.Size = new System.Drawing.Size(124, 25);
            this.fjg_value_xxTextBox.TabIndex = 3;
            // 
            // fjg_value_sxTextBox
            // 
            this.fjg_value_sxTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fjg_value_sx", true));
            this.fjg_value_sxTextBox.Location = new System.Drawing.Point(365, 24);
            this.fjg_value_sxTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.fjg_value_sxTextBox.Name = "fjg_value_sxTextBox";
            this.fjg_value_sxTextBox.Size = new System.Drawing.Size(124, 25);
            this.fjg_value_sxTextBox.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(fsam_type_idLabel);
            this.groupBox1.Controls.Add(this.fsam_type_idComboBox);
            this.groupBox1.Controls.Add(fname_eLabel);
            this.groupBox1.Controls.Add(this.fname_eTextBox);
            this.groupBox1.Controls.Add(this.labelformula);
            this.groupBox1.Controls.Add(fjx_formulaLabel);
            this.groupBox1.Controls.Add(this.fjx_formulaTextBox);
            this.groupBox1.Controls.Add(fjx_ifLabel);
            this.groupBox1.Controls.Add(this.fjx_ifCheckBox);
            this.groupBox1.Controls.Add(fuse_ifLabel);
            this.groupBox1.Controls.Add(this.fuse_ifCheckBox);
            this.groupBox1.Controls.Add(fcheck_method_idLabel);
            this.groupBox1.Controls.Add(this.fcheck_method_idComboBox);
            this.groupBox1.Controls.Add(fpriceLabel);
            this.groupBox1.Controls.Add(this.fpriceTextBox);
            this.groupBox1.Controls.Add(fprint_type_idLabel);
            this.groupBox1.Controls.Add(this.fprint_type_idComboBox);
            this.groupBox1.Controls.Add(fvalue_ddLabel);
            this.groupBox1.Controls.Add(this.fvalue_ddComboBox);
            this.groupBox1.Controls.Add(fxsLabel);
            this.groupBox1.Controls.Add(this.fxsTextBox);
            this.groupBox1.Controls.Add(funit_nameLabel);
            this.groupBox1.Controls.Add(this.funit_nameComboBox);
            this.groupBox1.Controls.Add(fhis_item_codeLabel);
            this.groupBox1.Controls.Add(this.fhis_item_codeTextBox);
            this.groupBox1.Controls.Add(fhelp_codeLabel);
            this.groupBox1.Controls.Add(this.fhelp_codeTextBox);
            this.groupBox1.Controls.Add(fname_jLabel);
            this.groupBox1.Controls.Add(this.fname_jTextBox);
            this.groupBox1.Controls.Add(fnameLabel);
            this.groupBox1.Controls.Add(this.fnameTextBox);
            this.groupBox1.Controls.Add(fitem_type_idLabel);
            this.groupBox1.Controls.Add(this.fitem_type_idComboBox);
            this.groupBox1.Controls.Add(fcheck_type_idLabel);
            this.groupBox1.Controls.Add(this.fcheck_type_idComboBox);
            this.groupBox1.Controls.Add(fitem_codeLabel);
            this.groupBox1.Controls.Add(this.fitem_codeTextBox);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(4, 4);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(592, 291);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "基础信息";
            // 
            // fsam_type_idComboBox
            // 
            this.fsam_type_idComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.sam_itemBindingSource, "fsam_type_id", true));
            this.fsam_type_idComboBox.FormattingEnabled = true;
            this.fsam_type_idComboBox.Location = new System.Drawing.Point(365, 25);
            this.fsam_type_idComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.fsam_type_idComboBox.Name = "fsam_type_idComboBox";
            this.fsam_type_idComboBox.Size = new System.Drawing.Size(160, 23);
            this.fsam_type_idComboBox.TabIndex = 36;
            // 
            // fname_eTextBox
            // 
            this.fname_eTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fname_e", true));
            this.fname_eTextBox.Location = new System.Drawing.Point(365, 110);
            this.fname_eTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.fname_eTextBox.Name = "fname_eTextBox";
            this.fname_eTextBox.Size = new System.Drawing.Size(160, 25);
            this.fname_eTextBox.TabIndex = 35;
            // 
            // labelformula
            // 
            this.labelformula.AutoSize = true;
            this.labelformula.ForeColor = System.Drawing.Color.Navy;
            this.labelformula.Location = new System.Drawing.Point(497, 260);
            this.labelformula.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelformula.Name = "labelformula";
            this.labelformula.Size = new System.Drawing.Size(61, 15);
            this.labelformula.TabIndex = 34;
            this.labelformula.Text = "设置...";
            this.labelformula.Click += new System.EventHandler(this.labelformula_Click);
            // 
            // fjx_formulaTextBox
            // 
            this.fjx_formulaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fjx_formula", true));
            this.fjx_formulaTextBox.Location = new System.Drawing.Point(207, 255);
            this.fjx_formulaTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.fjx_formulaTextBox.Name = "fjx_formulaTextBox";
            this.fjx_formulaTextBox.ReadOnly = true;
            this.fjx_formulaTextBox.Size = new System.Drawing.Size(267, 25);
            this.fjx_formulaTextBox.TabIndex = 33;
            // 
            // fjx_ifCheckBox
            // 
            this.fjx_ifCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.sam_itemBindingSource, "fjx_if", true));
            this.fjx_ifCheckBox.Location = new System.Drawing.Point(107, 252);
            this.fjx_ifCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.fjx_ifCheckBox.Name = "fjx_ifCheckBox";
            this.fjx_ifCheckBox.Size = new System.Drawing.Size(25, 30);
            this.fjx_ifCheckBox.TabIndex = 32;
            // 
            // fuse_ifCheckBox
            // 
            this.fuse_ifCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.sam_itemBindingSource, "fuse_if", true));
            this.fuse_ifCheckBox.Location = new System.Drawing.Point(496, 79);
            this.fuse_ifCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.fuse_ifCheckBox.Name = "fuse_ifCheckBox";
            this.fuse_ifCheckBox.Size = new System.Drawing.Size(36, 30);
            this.fuse_ifCheckBox.TabIndex = 31;
            // 
            // fcheck_method_idComboBox
            // 
            this.fcheck_method_idComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.sam_itemBindingSource, "fcheck_method_id", true));
            this.fcheck_method_idComboBox.FormattingEnabled = true;
            this.fcheck_method_idComboBox.Location = new System.Drawing.Point(365, 196);
            this.fcheck_method_idComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.fcheck_method_idComboBox.Name = "fcheck_method_idComboBox";
            this.fcheck_method_idComboBox.Size = new System.Drawing.Size(160, 23);
            this.fcheck_method_idComboBox.TabIndex = 30;
            // 
            // fpriceTextBox
            // 
            this.fpriceTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fprice", true));
            this.fpriceTextBox.Location = new System.Drawing.Point(107, 225);
            this.fpriceTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.fpriceTextBox.Name = "fpriceTextBox";
            this.fpriceTextBox.Size = new System.Drawing.Size(160, 25);
            this.fpriceTextBox.TabIndex = 25;
            // 
            // fprint_type_idComboBox
            // 
            this.fprint_type_idComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.sam_itemBindingSource, "fprint_type_id", true));
            this.fprint_type_idComboBox.FormattingEnabled = true;
            this.fprint_type_idComboBox.Location = new System.Drawing.Point(365, 168);
            this.fprint_type_idComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.fprint_type_idComboBox.Name = "fprint_type_idComboBox";
            this.fprint_type_idComboBox.Size = new System.Drawing.Size(160, 23);
            this.fprint_type_idComboBox.TabIndex = 23;
            // 
            // fvalue_ddComboBox
            // 
            this.fvalue_ddComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.sam_itemBindingSource, "fvalue_dd", true));
            this.fvalue_ddComboBox.FormattingEnabled = true;
            this.fvalue_ddComboBox.Location = new System.Drawing.Point(107, 196);
            this.fvalue_ddComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.fvalue_ddComboBox.Name = "fvalue_ddComboBox";
            this.fvalue_ddComboBox.Size = new System.Drawing.Size(160, 23);
            this.fvalue_ddComboBox.TabIndex = 21;
            // 
            // fxsTextBox
            // 
            this.fxsTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fxs", true));
            this.fxsTextBox.Location = new System.Drawing.Point(365, 139);
            this.fxsTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.fxsTextBox.Name = "fxsTextBox";
            this.fxsTextBox.Size = new System.Drawing.Size(160, 25);
            this.fxsTextBox.TabIndex = 19;
            // 
            // funit_nameComboBox
            // 
            this.funit_nameComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.sam_itemBindingSource, "funit_name", true));
            this.funit_nameComboBox.FormattingEnabled = true;
            this.funit_nameComboBox.Location = new System.Drawing.Point(107, 168);
            this.funit_nameComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.funit_nameComboBox.Name = "funit_nameComboBox";
            this.funit_nameComboBox.Size = new System.Drawing.Size(160, 23);
            this.funit_nameComboBox.TabIndex = 17;
            // 
            // fhis_item_codeTextBox
            // 
            this.fhis_item_codeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fhis_item_code", true));
            this.fhis_item_codeTextBox.Location = new System.Drawing.Point(365, 225);
            this.fhis_item_codeTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.fhis_item_codeTextBox.Name = "fhis_item_codeTextBox";
            this.fhis_item_codeTextBox.Size = new System.Drawing.Size(160, 25);
            this.fhis_item_codeTextBox.TabIndex = 15;
            // 
            // fhelp_codeTextBox
            // 
            this.fhelp_codeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fhelp_code", true));
            this.fhelp_codeTextBox.Location = new System.Drawing.Point(107, 139);
            this.fhelp_codeTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.fhelp_codeTextBox.Name = "fhelp_codeTextBox";
            this.fhelp_codeTextBox.Size = new System.Drawing.Size(160, 25);
            this.fhelp_codeTextBox.TabIndex = 13;
            // 
            // fname_jTextBox
            // 
            this.fname_jTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fname_j", true));
            this.fname_jTextBox.Location = new System.Drawing.Point(107, 110);
            this.fname_jTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.fname_jTextBox.Name = "fname_jTextBox";
            this.fname_jTextBox.Size = new System.Drawing.Size(160, 25);
            this.fname_jTextBox.TabIndex = 11;
            // 
            // fnameTextBox
            // 
            this.fnameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fname", true));
            this.fnameTextBox.Location = new System.Drawing.Point(107, 81);
            this.fnameTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.fnameTextBox.Name = "fnameTextBox";
            this.fnameTextBox.Size = new System.Drawing.Size(319, 25);
            this.fnameTextBox.TabIndex = 9;
            this.fnameTextBox.Leave += new System.EventHandler(this.fnameTextBox_Leave);
            // 
            // fitem_type_idComboBox
            // 
            this.fitem_type_idComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.sam_itemBindingSource, "fitem_type_id", true));
            this.fitem_type_idComboBox.FormattingEnabled = true;
            this.fitem_type_idComboBox.Location = new System.Drawing.Point(365, 52);
            this.fitem_type_idComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.fitem_type_idComboBox.Name = "fitem_type_idComboBox";
            this.fitem_type_idComboBox.Size = new System.Drawing.Size(160, 23);
            this.fitem_type_idComboBox.TabIndex = 5;
            // 
            // fcheck_type_idComboBox
            // 
            this.fcheck_type_idComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.sam_itemBindingSource, "fcheck_type_id", true));
            this.fcheck_type_idComboBox.FormattingEnabled = true;
            this.fcheck_type_idComboBox.Location = new System.Drawing.Point(107, 25);
            this.fcheck_type_idComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.fcheck_type_idComboBox.Name = "fcheck_type_idComboBox";
            this.fcheck_type_idComboBox.Size = new System.Drawing.Size(160, 23);
            this.fcheck_type_idComboBox.TabIndex = 3;
            // 
            // fitem_codeTextBox
            // 
            this.fitem_codeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fitem_code", true));
            this.fitem_codeTextBox.Location = new System.Drawing.Point(107, 52);
            this.fitem_codeTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.fitem_codeTextBox.Name = "fitem_codeTextBox";
            this.fitem_codeTextBox.Size = new System.Drawing.Size(160, 25);
            this.fitem_codeTextBox.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridViewRef);
            this.tabPage2.Controls.Add(this.panel3);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage2.Size = new System.Drawing.Size(600, 681);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "参考值明细";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridViewRef
            // 
            this.dataGridViewRef.AllowUserToAddRows = false;
            this.dataGridViewRef.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewRef.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewRef.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewRef.ColumnHeadersHeight = 35;
            this.dataGridViewRef.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fsex,
            this.fage_low,
            this.fage_high,
            this.fsample_type_id,
            this.fref_low,
            this.fref_high,
            this.fref1,
            this.fref_id});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewRef.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewRef.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewRef.EnableHeadersVisualStyles = false;
            this.dataGridViewRef.Location = new System.Drawing.Point(4, 4);
            this.dataGridViewRef.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewRef.MultiSelect = false;
            this.dataGridViewRef.Name = "dataGridViewRef";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewRef.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewRef.RowHeadersVisible = false;
            this.dataGridViewRef.RowHeadersWidth = 45;
            this.dataGridViewRef.RowTemplate.Height = 23;
            this.dataGridViewRef.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewRef.Size = new System.Drawing.Size(472, 673);
            this.dataGridViewRef.TabIndex = 122;
            this.dataGridViewRef.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewRef_DataError);
            // 
            // fsex
            // 
            this.fsex.DataPropertyName = "fsex";
            this.fsex.HeaderText = "性别";
            this.fsex.Name = "fsex";
            this.fsex.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fsex.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fsex.Width = 40;
            // 
            // fage_low
            // 
            this.fage_low.DataPropertyName = "fage_low";
            this.fage_low.HeaderText = "年龄下限";
            this.fage_low.Name = "fage_low";
            this.fage_low.Width = 40;
            // 
            // fage_high
            // 
            this.fage_high.DataPropertyName = "fage_high";
            this.fage_high.HeaderText = "年龄上限";
            this.fage_high.Name = "fage_high";
            this.fage_high.Width = 40;
            // 
            // fsample_type_id
            // 
            this.fsample_type_id.DataPropertyName = "fsample_type_id";
            this.fsample_type_id.HeaderText = "样本类型";
            this.fsample_type_id.Name = "fsample_type_id";
            this.fsample_type_id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fsample_type_id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fsample_type_id.Width = 80;
            // 
            // fref_low
            // 
            this.fref_low.DataPropertyName = "fref_low";
            this.fref_low.HeaderText = "参考下限";
            this.fref_low.Name = "fref_low";
            this.fref_low.Width = 60;
            // 
            // fref_high
            // 
            this.fref_high.DataPropertyName = "fref_high";
            this.fref_high.HeaderText = "参考上限";
            this.fref_high.Name = "fref_high";
            this.fref_high.Width = 60;
            // 
            // fref1
            // 
            this.fref1.DataPropertyName = "fref";
            this.fref1.HeaderText = "参考值";
            this.fref1.Name = "fref1";
            this.fref1.Width = 120;
            // 
            // fref_id
            // 
            this.fref_id.DataPropertyName = "fref_id";
            this.fref_id.HeaderText = "fref_id";
            this.fref_id.Name = "fref_id";
            this.fref_id.Visible = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.buttonrefSave);
            this.panel3.Controls.Add(this.buttonrefDel);
            this.panel3.Controls.Add(this.buttonrefAdd);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(476, 4);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(120, 673);
            this.panel3.TabIndex = 125;
            // 
            // buttonrefSave
            // 
            this.buttonrefSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonrefSave.Location = new System.Drawing.Point(8, 74);
            this.buttonrefSave.Margin = new System.Windows.Forms.Padding(4);
            this.buttonrefSave.Name = "buttonrefSave";
            this.buttonrefSave.Size = new System.Drawing.Size(107, 29);
            this.buttonrefSave.TabIndex = 127;
            this.buttonrefSave.Text = "保存";
            this.buttonrefSave.UseVisualStyleBackColor = true;
            this.buttonrefSave.Click += new System.EventHandler(this.buttonrefSave_Click);
            // 
            // buttonrefDel
            // 
            this.buttonrefDel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonrefDel.Location = new System.Drawing.Point(8, 134);
            this.buttonrefDel.Margin = new System.Windows.Forms.Padding(4);
            this.buttonrefDel.Name = "buttonrefDel";
            this.buttonrefDel.Size = new System.Drawing.Size(107, 29);
            this.buttonrefDel.TabIndex = 126;
            this.buttonrefDel.Text = "删除";
            this.buttonrefDel.UseVisualStyleBackColor = true;
            this.buttonrefDel.Click += new System.EventHandler(this.buttonrefDel_Click);
            // 
            // buttonrefAdd
            // 
            this.buttonrefAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonrefAdd.Location = new System.Drawing.Point(8, 14);
            this.buttonrefAdd.Margin = new System.Windows.Forms.Padding(4);
            this.buttonrefAdd.Name = "buttonrefAdd";
            this.buttonrefAdd.Size = new System.Drawing.Size(107, 29);
            this.buttonrefAdd.TabIndex = 125;
            this.buttonrefAdd.Text = "新增";
            this.buttonrefAdd.UseVisualStyleBackColor = true;
            this.buttonrefAdd.Click += new System.EventHandler(this.buttonrefAdd_Click);
            // 
            // tabPagefyy
            // 
            this.tabPagefyy.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabPagefyy.Controls.Add(this.fyyRichTextBox);
            this.tabPagefyy.Location = new System.Drawing.Point(4, 25);
            this.tabPagefyy.Margin = new System.Windows.Forms.Padding(4);
            this.tabPagefyy.Name = "tabPagefyy";
            this.tabPagefyy.Padding = new System.Windows.Forms.Padding(4);
            this.tabPagefyy.Size = new System.Drawing.Size(600, 681);
            this.tabPagefyy.TabIndex = 2;
            this.tabPagefyy.Text = "临床意义";
            this.tabPagefyy.UseVisualStyleBackColor = true;
            // 
            // fyyRichTextBox
            // 
            this.fyyRichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.fyyRichTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fyy", true));
            this.fyyRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fyyRichTextBox.Location = new System.Drawing.Point(4, 4);
            this.fyyRichTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.fyyRichTextBox.Name = "fyyRichTextBox";
            this.fyyRichTextBox.Size = new System.Drawing.Size(588, 669);
            this.fyyRichTextBox.TabIndex = 2;
            this.fyyRichTextBox.Text = "";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dataGridViewIO);
            this.tabPage4.Controls.Add(this.panel4);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage4.Size = new System.Drawing.Size(600, 681);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "仪器接口";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dataGridViewIO
            // 
            this.dataGridViewIO.AllowUserToAddRows = false;
            this.dataGridViewIO.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewIO.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewIO.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewIO.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fcode,
            this.iofitem_id,
            this.iofuse_if,
            this.fio_id,
            this.finstr_id});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewIO.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewIO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewIO.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridViewIO.EnableHeadersVisualStyles = false;
            this.dataGridViewIO.Location = new System.Drawing.Point(4, 4);
            this.dataGridViewIO.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewIO.MultiSelect = false;
            this.dataGridViewIO.Name = "dataGridViewIO";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewIO.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewIO.RowHeadersVisible = false;
            this.dataGridViewIO.RowHeadersWidth = 45;
            this.dataGridViewIO.RowTemplate.Height = 23;
            this.dataGridViewIO.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewIO.Size = new System.Drawing.Size(472, 673);
            this.dataGridViewIO.TabIndex = 123;
            // 
            // fcode
            // 
            this.fcode.DataPropertyName = "fcode";
            this.fcode.HeaderText = "代号";
            this.fcode.Name = "fcode";
            // 
            // iofitem_id
            // 
            this.iofitem_id.DataPropertyName = "fitem_id";
            this.iofitem_id.HeaderText = "fitem_id";
            this.iofitem_id.Name = "iofitem_id";
            this.iofitem_id.Visible = false;
            // 
            // iofuse_if
            // 
            this.iofuse_if.DataPropertyName = "fuse_if";
            this.iofuse_if.HeaderText = "fuse_if";
            this.iofuse_if.Name = "iofuse_if";
            this.iofuse_if.Visible = false;
            // 
            // fio_id
            // 
            this.fio_id.DataPropertyName = "fio_id";
            this.fio_id.HeaderText = "fio_id";
            this.fio_id.Name = "fio_id";
            this.fio_id.Visible = false;
            // 
            // finstr_id
            // 
            this.finstr_id.DataPropertyName = "finstr_id";
            this.finstr_id.HeaderText = "仪器";
            this.finstr_id.Name = "finstr_id";
            this.finstr_id.ReadOnly = true;
            this.finstr_id.Width = 120;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.buttonIOSave);
            this.panel4.Controls.Add(this.buttonIODel);
            this.panel4.Controls.Add(this.buttonIOAdd);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(476, 4);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(120, 673);
            this.panel4.TabIndex = 126;
            // 
            // buttonIOSave
            // 
            this.buttonIOSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonIOSave.Location = new System.Drawing.Point(8, 74);
            this.buttonIOSave.Margin = new System.Windows.Forms.Padding(4);
            this.buttonIOSave.Name = "buttonIOSave";
            this.buttonIOSave.Size = new System.Drawing.Size(107, 29);
            this.buttonIOSave.TabIndex = 127;
            this.buttonIOSave.Text = "保存";
            this.buttonIOSave.UseVisualStyleBackColor = true;
            this.buttonIOSave.Click += new System.EventHandler(this.buttonIOSave_Click);
            // 
            // buttonIODel
            // 
            this.buttonIODel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonIODel.Location = new System.Drawing.Point(8, 134);
            this.buttonIODel.Margin = new System.Windows.Forms.Padding(4);
            this.buttonIODel.Name = "buttonIODel";
            this.buttonIODel.Size = new System.Drawing.Size(107, 29);
            this.buttonIODel.TabIndex = 126;
            this.buttonIODel.Text = "删除";
            this.buttonIODel.UseVisualStyleBackColor = true;
            this.buttonIODel.Click += new System.EventHandler(this.buttonIODel_Click);
            // 
            // buttonIOAdd
            // 
            this.buttonIOAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonIOAdd.Location = new System.Drawing.Point(8, 14);
            this.buttonIOAdd.Margin = new System.Windows.Forms.Padding(4);
            this.buttonIOAdd.Name = "buttonIOAdd";
            this.buttonIOAdd.Size = new System.Drawing.Size(107, 29);
            this.buttonIOAdd.TabIndex = 125;
            this.buttonIOAdd.Text = "新增";
            this.buttonIOAdd.UseVisualStyleBackColor = true;
            this.buttonIOAdd.Click += new System.EventHandler(this.buttonIOAdd_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.dataGridViewPrint);
            this.tabPage5.Controls.Add(this.panel5);
            this.tabPage5.Location = new System.Drawing.Point(4, 25);
            this.tabPage5.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage5.Size = new System.Drawing.Size(600, 681);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "打印排序";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // dataGridViewPrint
            // 
            this.dataGridViewPrint.AllowUserToAddRows = false;
            this.dataGridViewPrint.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewPrint.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewPrint.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewPrint.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemCode,
            this.ItemName,
            this.Itemfprint_num,
            this.fitem_id});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewPrint.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewPrint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewPrint.EnableHeadersVisualStyles = false;
            this.dataGridViewPrint.Location = new System.Drawing.Point(4, 4);
            this.dataGridViewPrint.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewPrint.MultiSelect = false;
            this.dataGridViewPrint.Name = "dataGridViewPrint";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewPrint.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewPrint.RowHeadersVisible = false;
            this.dataGridViewPrint.RowHeadersWidth = 45;
            this.dataGridViewPrint.RowTemplate.Height = 23;
            this.dataGridViewPrint.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewPrint.Size = new System.Drawing.Size(472, 673);
            this.dataGridViewPrint.TabIndex = 124;
            this.dataGridViewPrint.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView2_DataError);
            // 
            // ItemCode
            // 
            this.ItemCode.DataPropertyName = "fitem_code";
            this.ItemCode.HeaderText = "项目代号";
            this.ItemCode.Name = "ItemCode";
            this.ItemCode.ReadOnly = true;
            // 
            // ItemName
            // 
            this.ItemName.DataPropertyName = "fname";
            this.ItemName.HeaderText = "项目名称";
            this.ItemName.Name = "ItemName";
            this.ItemName.ReadOnly = true;
            this.ItemName.Width = 160;
            // 
            // Itemfprint_num
            // 
            this.Itemfprint_num.DataPropertyName = "fprint_num";
            this.Itemfprint_num.HeaderText = "打印序号";
            this.Itemfprint_num.Name = "Itemfprint_num";
            this.Itemfprint_num.Width = 80;
            // 
            // fitem_id
            // 
            this.fitem_id.DataPropertyName = "fitem_id";
            this.fitem_id.HeaderText = "fitem_id";
            this.fitem_id.Name = "fitem_id";
            this.fitem_id.Visible = false;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.buttonPrintSave);
            this.panel5.Controls.Add(this.buttonPrintRef);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(476, 4);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(120, 673);
            this.panel5.TabIndex = 127;
            // 
            // buttonPrintSave
            // 
            this.buttonPrintSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonPrintSave.Location = new System.Drawing.Point(8, 14);
            this.buttonPrintSave.Margin = new System.Windows.Forms.Padding(4);
            this.buttonPrintSave.Name = "buttonPrintSave";
            this.buttonPrintSave.Size = new System.Drawing.Size(107, 29);
            this.buttonPrintSave.TabIndex = 127;
            this.buttonPrintSave.Text = "保存";
            this.buttonPrintSave.UseVisualStyleBackColor = true;
            this.buttonPrintSave.Click += new System.EventHandler(this.buttonPrintSave_Click);
            // 
            // buttonPrintRef
            // 
            this.buttonPrintRef.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonPrintRef.Location = new System.Drawing.Point(8, 74);
            this.buttonPrintRef.Margin = new System.Windows.Forms.Padding(4);
            this.buttonPrintRef.Name = "buttonPrintRef";
            this.buttonPrintRef.Size = new System.Drawing.Size(107, 29);
            this.buttonPrintRef.TabIndex = 126;
            this.buttonPrintRef.Text = "刷新";
            this.buttonPrintRef.UseVisualStyleBackColor = true;
            this.buttonPrintRef.Click += new System.EventHandler(this.buttonPrintRef_Click);
            // 
            // tabPageValue
            // 
            this.tabPageValue.Controls.Add(this.dataGridViewValue);
            this.tabPageValue.Controls.Add(this.panel6);
            this.tabPageValue.Location = new System.Drawing.Point(4, 25);
            this.tabPageValue.Margin = new System.Windows.Forms.Padding(4);
            this.tabPageValue.Name = "tabPageValue";
            this.tabPageValue.Padding = new System.Windows.Forms.Padding(4);
            this.tabPageValue.Size = new System.Drawing.Size(600, 681);
            this.tabPageValue.TabIndex = 5;
            this.tabPageValue.Text = "常用取值";
            this.tabPageValue.UseVisualStyleBackColor = true;
            // 
            // dataGridViewValue
            // 
            this.dataGridViewValue.AllowUserToAddRows = false;
            this.dataGridViewValue.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewValue.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewValue.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fvalue,
            this.fvalue_flag,
            this.forder_by,
            this.fvalue_id,
            this.valuefitem_id});
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewValue.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewValue.EnableHeadersVisualStyles = false;
            this.dataGridViewValue.Location = new System.Drawing.Point(4, 4);
            this.dataGridViewValue.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewValue.MultiSelect = false;
            this.dataGridViewValue.Name = "dataGridViewValue";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewValue.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewValue.RowHeadersVisible = false;
            this.dataGridViewValue.RowHeadersWidth = 45;
            this.dataGridViewValue.RowTemplate.Height = 23;
            this.dataGridViewValue.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewValue.Size = new System.Drawing.Size(472, 673);
            this.dataGridViewValue.TabIndex = 127;
            // 
            // fvalue
            // 
            this.fvalue.DataPropertyName = "fvalue";
            this.fvalue.HeaderText = "值";
            this.fvalue.Name = "fvalue";
            // 
            // fvalue_flag
            // 
            this.fvalue_flag.DataPropertyName = "fvalue_flag";
            this.fvalue_flag.HeaderText = "标记";
            this.fvalue_flag.Name = "fvalue_flag";
            this.fvalue_flag.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fvalue_flag.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fvalue_flag.Width = 80;
            // 
            // forder_by
            // 
            this.forder_by.DataPropertyName = "forder_by";
            this.forder_by.HeaderText = "排序";
            this.forder_by.Name = "forder_by";
            this.forder_by.Width = 50;
            // 
            // fvalue_id
            // 
            this.fvalue_id.DataPropertyName = "fvalue_id";
            this.fvalue_id.HeaderText = "fvalue_id";
            this.fvalue_id.Name = "fvalue_id";
            this.fvalue_id.Visible = false;
            // 
            // valuefitem_id
            // 
            this.valuefitem_id.DataPropertyName = "fitem_id";
            this.valuefitem_id.HeaderText = "fitem_id";
            this.valuefitem_id.Name = "valuefitem_id";
            this.valuefitem_id.Visible = false;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.buttonItemValueSave);
            this.panel6.Controls.Add(this.buttonItemValueDel);
            this.panel6.Controls.Add(this.buttonItemValueAdd);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(476, 4);
            this.panel6.Margin = new System.Windows.Forms.Padding(4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(120, 673);
            this.panel6.TabIndex = 128;
            // 
            // buttonItemValueSave
            // 
            this.buttonItemValueSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonItemValueSave.Location = new System.Drawing.Point(8, 74);
            this.buttonItemValueSave.Margin = new System.Windows.Forms.Padding(4);
            this.buttonItemValueSave.Name = "buttonItemValueSave";
            this.buttonItemValueSave.Size = new System.Drawing.Size(107, 29);
            this.buttonItemValueSave.TabIndex = 127;
            this.buttonItemValueSave.Text = "保存";
            this.buttonItemValueSave.UseVisualStyleBackColor = true;
            this.buttonItemValueSave.Click += new System.EventHandler(this.buttonItemValueSave_Click);
            // 
            // buttonItemValueDel
            // 
            this.buttonItemValueDel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonItemValueDel.Location = new System.Drawing.Point(8, 134);
            this.buttonItemValueDel.Margin = new System.Windows.Forms.Padding(4);
            this.buttonItemValueDel.Name = "buttonItemValueDel";
            this.buttonItemValueDel.Size = new System.Drawing.Size(107, 29);
            this.buttonItemValueDel.TabIndex = 126;
            this.buttonItemValueDel.Text = "删除";
            this.buttonItemValueDel.UseVisualStyleBackColor = true;
            this.buttonItemValueDel.Click += new System.EventHandler(this.buttonItemValueDel_Click);
            // 
            // buttonItemValueAdd
            // 
            this.buttonItemValueAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonItemValueAdd.Location = new System.Drawing.Point(8, 14);
            this.buttonItemValueAdd.Margin = new System.Windows.Forms.Padding(4);
            this.buttonItemValueAdd.Name = "buttonItemValueAdd";
            this.buttonItemValueAdd.Size = new System.Drawing.Size(107, 29);
            this.buttonItemValueAdd.TabIndex = 125;
            this.buttonItemValueAdd.Text = "新增";
            this.buttonItemValueAdd.UseVisualStyleBackColor = true;
            this.buttonItemValueAdd.Click += new System.EventHandler(this.buttonItemValueAdd_Click);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 38);
            this.splitter1.Margin = new System.Windows.Forms.Padding(4);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(4, 710);
            this.splitter1.TabIndex = 2;
            this.splitter1.TabStop = false;
            // 
            // button1
            // 
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(261, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "   修改(&U)";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(177, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(80, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "   刷新(&R)";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(93, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(80, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "   保存(&S)";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(345, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(80, 23);
            this.button4.TabIndex = 1;
            this.button4.Text = "   删除(&A)";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(9, 3);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(80, 23);
            this.button5.TabIndex = 0;
            this.button5.Text = "   新增(&A)";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // bN
            // 
            this.bN.AddNewItem = null;
            this.bN.AutoSize = false;
            this.bN.BindingSource = this.sam_itemBindingSource;
            this.bN.CountItem = this.bindingNavigatorCountItem;
            this.bN.DeleteItem = null;
            this.bN.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bN.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.bN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.toolStripButtonAdd,
            this.toolStripButtonS,
            this.toolStripButtonDel,
            this.toolStripSeparator3,
            this.toolStripButtonR,
            this.toolStripSeparator1,
            this.toolStripLabel1});
            this.bN.Location = new System.Drawing.Point(0, 0);
            this.bN.MoveFirstItem = null;
            this.bN.MoveLastItem = null;
            this.bN.MoveNextItem = null;
            this.bN.MovePreviousItem = null;
            this.bN.Name = "bN";
            this.bN.PositionItem = this.bindingNavigatorPositionItem;
            this.bN.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bN.Size = new System.Drawing.Size(1053, 38);
            this.bN.TabIndex = 124;
            this.bN.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(38, 35);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 38);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(65, 27);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 38);
            // 
            // toolStripButtonAdd
            // 
            this.toolStripButtonAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAdd.Image")));
            this.toolStripButtonAdd.Name = "toolStripButtonAdd";
            this.toolStripButtonAdd.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonAdd.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonAdd.Size = new System.Drawing.Size(92, 35);
            this.toolStripButtonAdd.Text = "新增(&A)  ";
            this.toolStripButtonAdd.Click += new System.EventHandler(this.toolStripButtonAdd_Click);
            // 
            // toolStripButtonS
            // 
            this.toolStripButtonS.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonS.Image")));
            this.toolStripButtonS.Name = "toolStripButtonS";
            this.toolStripButtonS.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonS.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonS.Size = new System.Drawing.Size(90, 35);
            this.toolStripButtonS.Text = "保存(&S)  ";
            this.toolStripButtonS.Click += new System.EventHandler(this.toolStripButtonS_Click);
            // 
            // toolStripButtonDel
            // 
            this.toolStripButtonDel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDel.Image")));
            this.toolStripButtonDel.Name = "toolStripButtonDel";
            this.toolStripButtonDel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonDel.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonDel.Size = new System.Drawing.Size(92, 35);
            this.toolStripButtonDel.Text = "删除(&D)  ";
            this.toolStripButtonDel.Click += new System.EventHandler(this.toolStripButtonDel_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 38);
            // 
            // toolStripButtonR
            // 
            this.toolStripButtonR.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonR.Image")));
            this.toolStripButtonR.Name = "toolStripButtonR";
            this.toolStripButtonR.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonR.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonR.Size = new System.Drawing.Size(91, 35);
            this.toolStripButtonR.Text = "刷新(&R)  ";
            this.toolStripButtonR.Click += new System.EventHandler(this.toolStripButtonR_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 38);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.ForeColor = System.Drawing.Color.Blue;
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(315, 35);
            this.toolStripLabel1.Text = "新增、删除、修改项目后，需要重启LIS系统。";
            // 
            // ItemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1053, 748);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.bN);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "ItemForm";
            this.Text = "ItemForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ItemForm_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sam_itemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRef)).EndInit();
            this.panel3.ResumeLayout(false);
            this.tabPagefyy.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIO)).EndInit();
            this.panel4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPrint)).EndInit();
            this.panel5.ResumeLayout(false);
            this.tabPageValue.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewValue)).EndInit();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_ref)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_fsex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_fsample_type_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).EndInit();
            this.bN.ResumeLayout(false);
            this.bN.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox comboBoxInstr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.DataGridView DataGridViewObject;
        private System.Windows.Forms.TabPage tabPagefyy;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox fitem_codeTextBox;
        private System.Windows.Forms.BindingSource sam_itemBindingSource;
        private ww.form.lis.sam.samDataSet samDataSet;
        private System.Windows.Forms.ComboBox fcheck_type_idComboBox;
        private System.Windows.Forms.ComboBox fitem_type_idComboBox;
        private System.Windows.Forms.TextBox fhelp_codeTextBox;
        private System.Windows.Forms.TextBox fname_jTextBox;
        private System.Windows.Forms.TextBox fnameTextBox;
        private System.Windows.Forms.TextBox fhis_item_codeTextBox;
        private System.Windows.Forms.ComboBox funit_nameComboBox;
        private System.Windows.Forms.ComboBox fvalue_ddComboBox;
        private System.Windows.Forms.TextBox fxsTextBox;
        private System.Windows.Forms.ComboBox fprint_type_idComboBox;
        private System.Windows.Forms.TextBox fpriceTextBox;
        private System.Windows.Forms.ComboBox fcheck_method_idComboBox;
        private System.Windows.Forms.CheckBox fuse_ifCheckBox;
        private System.Windows.Forms.TextBox fjx_formulaTextBox;
        private System.Windows.Forms.CheckBox fjx_ifCheckBox;
        private System.Windows.Forms.Label labelformula;
        private System.Windows.Forms.TextBox fref_lowTextBox;
        private System.Windows.Forms.TextBox fref_highTextBox;
        private System.Windows.Forms.TextBox frefTextBox;
        private System.Windows.Forms.Label labelref;
        private System.Windows.Forms.CheckBox fref_if_sexCheckBox;
        private System.Windows.Forms.CheckBox fref_if_ageCheckBox;
        private System.Windows.Forms.CheckBox fref_if_sampleCheckBox;
        private System.Windows.Forms.TextBox fname_eTextBox;
        private System.Windows.Forms.TextBox fvalue_day_noTextBox;
        private System.Windows.Forms.TextBox fvalue_day_numTextBox;
        private System.Windows.Forms.TextBox fjg_value_xxTextBox;
        private System.Windows.Forms.TextBox fjg_value_sxTextBox;
        private System.Windows.Forms.ComboBox fsam_type_idComboBox;
        private System.Windows.Forms.DataGridView dataGridViewRef;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button buttonrefDel;
        private System.Windows.Forms.Button buttonrefAdd;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.BindingSource bindingSource_fsex;
        private System.Windows.Forms.BindingSource bindingSource_ref;
        private System.Windows.Forms.BindingSource bindingSource_fsample_type_id;
        private System.Windows.Forms.Button buttonrefSave;
        private System.Windows.Forms.DataGridViewComboBoxColumn fsex;
        private System.Windows.Forms.DataGridViewTextBoxColumn fage_low;
        private System.Windows.Forms.DataGridViewTextBoxColumn fage_high;
        private System.Windows.Forms.DataGridViewComboBoxColumn fsample_type_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fref_low;
        private System.Windows.Forms.DataGridViewTextBoxColumn fref_high;
        private System.Windows.Forms.DataGridViewTextBoxColumn fref1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fref_id;
        private System.Windows.Forms.RichTextBox fyyRichTextBox;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button buttonIOSave;
        private System.Windows.Forms.Button buttonIODel;
        private System.Windows.Forms.Button buttonIOAdd;
        private System.Windows.Forms.DataGridView dataGridViewIO;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button buttonPrintSave;
        private System.Windows.Forms.Button buttonPrintRef;
        private System.Windows.Forms.DataGridView dataGridViewPrint;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Itemfprint_num;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_id;
        private System.Windows.Forms.TabPage tabPageValue;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button buttonItemValueSave;
        private System.Windows.Forms.Button buttonItemValueDel;
        private System.Windows.Forms.Button buttonItemValueAdd;
        private System.Windows.Forms.DataGridView dataGridViewValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn fvalue;
        private System.Windows.Forms.DataGridViewComboBoxColumn fvalue_flag;
        private System.Windows.Forms.DataGridViewTextBoxColumn forder_by;
        private System.Windows.Forms.DataGridViewTextBoxColumn fvalue_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn valuefitem_id;
        protected System.Windows.Forms.BindingNavigator bN;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonAdd;
        private System.Windows.Forms.ToolStripButton toolStripButtonS;
        private System.Windows.Forms.ToolStripButton toolStripButtonDel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        protected System.Windows.Forms.ToolStripButton toolStripButtonR;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fuse_if;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname;
        private System.Windows.Forms.DataGridViewTextBoxColumn funit_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn fref;
        private System.Windows.Forms.DataGridViewTextBoxColumn fprint_num;
        private System.Windows.Forms.DataGridViewTextBoxColumn fxs;
        private System.Windows.Forms.DataGridViewTextBoxColumn fcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn iofitem_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn iofuse_if;
        private System.Windows.Forms.DataGridViewTextBoxColumn fio_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn finstr_id;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    }
}