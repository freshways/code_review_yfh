﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;
using ww.form.wwf;
using ww.lis.lisbll.sam;
namespace ww.form.lis.sam.dic
{
    public partial class PatientForm : SysBaseForm
    {
        PatientBLL bllPatient = new PatientBLL();
        string strfdept_id = "";
        DeptBll bll = new DeptBll();
        DataTable dtCurr = new DataTable();
        public PatientForm()
        {
            InitializeComponent();
            dataGridView1.AutoGenerateColumns = false;
        }

        private void PatientForm_Load(object sender, EventArgs e)
        {
            GetTree("-1");
        }
        /// <summary>
        /// 列出系统部门树
        /// </summary>
        public void GetTree(string pid)
        {
            try
            {
                this.wwTreeView1.ZADataTable = this.bll.BllDeptDT();
                this.wwTreeView1.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeView1.ZAParentFieldName = "fp_id";//上级字段名称
                this.wwTreeView1.ZADisplayFieldName = "fname_show";//显示字段名称
                this.wwTreeView1.ZAKeyFieldName = "fdept_id";//主键字段名称
                this.wwTreeView1.ZATreeViewShow();//显示树        
                try
                {
                    this.wwTreeView1.SelectedNode = wwTreeView1.Nodes[0].Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void GetCurrDT()
        {
            try
            {
                string fjy_date1 = this.bllPatient.DbDateTime1(fjy_dateDateTimePicker1);
                string fjy_date2 = this.bllPatient.DbDateTime2(fjy_dateDateTimePicker2);
                string strDateif = " and (ftime_registration>='" + fjy_date1 + "' and ftime_registration<='" + fjy_date2 + "')";
                string strDept = "";
                if (dtCurr != null)
                    dtCurr.Clear();
                if (strfdept_id != "0")
                    strDept = " and fdept_id='" + strfdept_id + "'";
                dtCurr = this.bllPatient.BllPatientDT(strDept + strDateif);


                //textBox1.Text = "fdept_id='" + strfdept_id + "'" + strDateif;
                bindingSourceObject.DataSource = dtCurr;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void wwTreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                strfdept_id = e.Node.Name.ToString();
                GetCurrDT();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonAdd_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow dr = null;
                dr = this.dtCurr.NewRow();
                dr["fhz_id"] = this.bllPatient.DbNum("sam_patient_id");
                dr["fdept_id"] = strfdept_id;
                dr["fuse_if"] = "1";
                dr["ftime_registration"] = this.bllPatient.DbServerDateTim();
                dr["ftime"] = this.bllPatient.DbServerDateTim();
                if (this.bllPatient.BllPatientAdd(dr) > 0)
                {
                    this.dtCurr.Rows.Add(dr);
                    this.bindingSourceObject.Position = bindingSourceObject.Count;
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonS_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.bindingSourceObject.EndEdit();
                for (int i = 0; i < this.dtCurr.Rows.Count; i++)
                {
                    this.bllPatient.BllPatientUpdate(this.dtCurr.Rows[i]);

                }
                GetCurrDT();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonDel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.bindingSourceObject.EndEdit();
                if (ww.wwf.wwfbll.WWMessage.MessageDialogResult("是否真要删除此类型？"))
                {
                    string strid = this.dataGridView1.CurrentRow.Cells["fhz_id"].Value.ToString();
                    if (this.bllPatient.BllPatientDel(strid) > 0)
                        GetCurrDT();
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }


        private void fjy_dateDateTimePicker1_CloseUp(object sender, EventArgs e)
        {
            GetCurrDT();
        }

        private void fjy_dateDateTimePicker2_CloseUp(object sender, EventArgs e)
        {
            GetCurrDT();
        }

        

        private string getAge(string bornDate)
        {
            string strRetAge = "";
            if (bornDate == "" || bornDate == null)
                strRetAge = "";
            else
            {
                DateTime b = DateTime.Parse(bornDate);
                double a = (DateTime.Now - b).TotalDays;
                double aYear = Math.Floor(a / 365);
                double C = a % 365;
                double aMonth = Math.Floor(C / 30);
                double aDay = Math.Floor(C % 30);
                //textBox3.Text = aYear.ToString() + "岁   零" + aMonth.ToString() + "个月   " + aDay.ToString() + "天";
                // strRetAge = aYear.ToString() + "岁   零" + aMonth.ToString() + "个月   " + aDay.ToString() + "天"; 
                //return aYear.ToString() + "岁   零" + aMonth.ToString() + "个月   " + aDay.ToString() + "天";
                if (aDay > 15)
                {
                    aMonth = aMonth + 1;
                    aDay = 0;
                }
                if (aMonth > 6)
                {
                    aYear = aYear + 1;
                    aMonth = 0;
                }

                if (aYear == 0 & aMonth == 0 & aDay != 0)
                    strRetAge = "天:" + aDay.ToString();//2 天
                if (aYear == 0 & aMonth != 0 & aDay <= 15)
                    strRetAge = "月:" + aMonth.ToString();//1 月
                if (aYear != 0 & aMonth <= 6)
                    strRetAge = "岁:" + aYear.ToString();//0 岁
            }
            return strRetAge;
        }
    }
}