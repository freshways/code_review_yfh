﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using ww.lis.lisbll.sam;
namespace ww.form.lis.sam.dic
{
    public partial class SampleTypeForm : ww.form.wwf.SysBaseForm
    {
        SampleTypeBLL bll = new SampleTypeBLL();
        DataTable dtCurr = new DataTable();
       
        public SampleTypeForm()
        {
            InitializeComponent();
            this.DataGridViewObject.AutoGenerateColumns = false;
            
        }
        private void ApplyUserForm_Load(object sender, EventArgs e)
        {
            GetDT();
        }

       

        private void ReturnValue()
        {
            try
            {
                this.Validate();
                this.bindingSourceObject.EndEdit();
                for(int i=0;i<this.dtCurr.Rows.Count;i++)
                {
                    this.bll.BllUpdate(this.dtCurr.Rows[i]);
                    
                }
                GetDT();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        

        private void GetDT()
        {
            try
            {
                this.dtCurr = this.bll.BllDT(" WHERE (fhelp_code LIKE '%" + this.textBox1.Text + "%') "); // (fuse_if = 1) AND
               
                bindingSourceObject.DataSource = dtCurr;

                this.DataGridViewObject.DataSource = bindingSourceObject;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            GetDT();
        }

        

        private void DataGridViewObject_DoubleClick(object sender, EventArgs e)
        {
            ReturnValue();
        }

        private void DataGridViewObject_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Space))//或(e.KeyCode==Keys.Enter) 
            {
                ReturnValue();
            }
        }

        
        private void toolStripButtonAdd_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow dr = null;
                dr = this.dtCurr.NewRow();

                dr["fsample_type_id"] = this.bll.DbGuid();
                dr["forder_by"] = "0";
                dr["fuse_if"] = "1";

                if (this.bll.BllAdd(dr) > 0)
                {
                    this.dtCurr.Rows.Add(dr);

                    this.bindingSourceObject.Position = bindingSourceObject.Count;
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void toolStripButtonS_Click(object sender, EventArgs e)
        {
            ReturnValue();
        }

        private void toolStripButtonHelpCode_Click(object sender, EventArgs e)
        {
            try
            {
                string sqlHelp = "";
                IList lissql = new ArrayList();
                for (int i = 0; i < DataGridViewObject.Rows.Count; i++)
                {
                    sqlHelp = "";
                    try
                    {
                        sqlHelp = ww.wwf.com.Spell.MakeSpellCodeFirstLetterOnly(DataGridViewObject.Rows[i].Cells["fname"].Value.ToString().Trim());
                    }
                    catch { }
                    lissql.Add("UPDATE sam_sample_type SET fhelp_code = '" + sqlHelp + "' WHERE (fsample_type_id = '" + DataGridViewObject.Rows[i].Cells["fsample_type_id"].Value.ToString() + "')");
                    //string ss = DataGridViewObject.Rows[i].Cells["fname"].Value.ToString().Trim() +"-"+ i.ToString();
                    // lissql.Add("UPDATE sam_sample_type SET fname = '" + ss + "' WHERE (fsample_type_id = '" + DataGridViewObject.Rows[i].Cells["fsample_type_id"].Value.ToString() + "')");
                }
                this.bll.BllPersonfhelp_codeUpdate(lissql);
                GetDT();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void toolStripButtonR_Click(object sender, EventArgs e)
        {
            GetDT();
        }

      

        
    }
}