﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.lis.lisbll.sam;

namespace ww.form.lis.sam.dic
{
    public partial class CheckTypeAndSampleTypeForm :ww.form.wwf.SysBaseForm
    {
        TypeBLL bllType = new TypeBLL();//类别
        SampleTypeBLL bllSample = new SampleTypeBLL();
        public CheckTypeAndSampleTypeForm()
        {
            InitializeComponent();
            DataGridViewObject.AutoGenerateColumns = false;
            this.dataGridViewSampletypeAll.AutoGenerateColumns = false;
            this.dataGridViewSampletypeOk.AutoGenerateColumns = false;
        }

        private void CheckTypeAndSampleTypeForm_Load(object sender, EventArgs e)
        {
            GetCheckType();
            GetSampleAll();
        }
        /// <summary>
        /// 取得检验类别
        /// </summary>
        private void GetCheckType()
        {
            try
            {
                bindingSourceCheckType.DataSource = this.bllType.BllCheckTypeDT(1);
                this.DataGridViewObject.AutoGenerateColumns = false;
                this.DataGridViewObject.DataSource = bindingSourceCheckType;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }

        }
        /// <summary>
        /// 
        /// </summary>
        private void GetSampleAll()
        {
            try
            {

                this.dataGridViewSampletypeAll.DataSource =
                    this.bllSample.BllDT(" WHERE (fuse_if = 1) AND (fhelp_code LIKE '%" + this.textBox1.Text + "%') ");
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void GetSampleOk(string fcheck_type_id)
        {          
            try
            {                
                this.dataGridViewSampletypeOk.DataSource = this.bllType.BllCheckAndSamTypeDT(fcheck_type_id);
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }

        }
        string strfcheck_type_id = "";
        private void bindingSourceCheckType_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                strfcheck_type_id = "";
                DataRowView rowCurrent = (DataRowView)bindingSourceCheckType.Current;
                if (rowCurrent != null)
                {
                    strfcheck_type_id = rowCurrent["fcheck_type_id"].ToString();
                    GetSampleOk(strfcheck_type_id);
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            GetSampleAll();
        }

       

        private void buttonNo_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (this.dataGridViewSampletypeOk.Rows.Count > 0)
                {
                    if (ww.wwf.wwfbll.WWMessage.MessageDialogResult("确定取消？"))
                    {
                        string fid = this.dataGridViewSampletypeOk.CurrentRow.Cells["fid_ok"].Value.ToString();
                        this.bllType.BllCheckAndSamTypeDel(fid);
                        GetSampleOk(strfcheck_type_id);
                    }
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (dataGridViewSampletypeAll.Rows.Count > 0)
                {
                    string jytype = this.DataGridViewObject.CurrentRow.Cells["fcheck_type_id_01"].Value.ToString();
                    string ybtype = this.dataGridViewSampletypeAll.CurrentRow.Cells["fsample_type_id_all"].Value.ToString();
                    if (this.bllType.BllCheckAndSamTypeExists(jytype, ybtype))
                    {
                        ww.wwf.wwfbll.WWMessage.MessageShowWarning("此样本类别已经存！");
                    }
                    else
                    {
                        this.bllType.BllCheckAndSamTypeAdd(jytype, ybtype);
                        GetSampleOk(strfcheck_type_id);
                    }
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}