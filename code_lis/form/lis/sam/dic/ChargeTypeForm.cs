﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ww.form.lis.sam.dic
{
    public partial class ChargeTypeForm : ComTypeForm
    {
        public ChargeTypeForm()
        {
            InitializeComponent();
        }

        private void ChargeTypeForm_Load(object sender, EventArgs e)
        {
            this.strType = "检验收费类型";
            this.GetComCheckType();
        }
    }
}