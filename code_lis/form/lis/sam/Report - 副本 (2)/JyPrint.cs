﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ww.form.wwf.print;
using ww.wwf.wwfbll;
using ww.lis.lisbll.sam;
using System.Windows.Forms;

namespace ww.form.lis.sam.Report
{
    public class JyPrint
    { 
        /// <summary>
        /// 报告规则
        /// </summary>
        private jybll bllReport = new jybll();
        private InstrBLL bllInstr = new InstrBLL();
        private PersonBLL bllPerson = new PersonBLL();
        private SampleTypeBLL bllSample = new SampleTypeBLL();
        DeptBll bllDept = new DeptBll();
        DataSet dsPrint = new DataSet();
        samDataSet.sam_jy_printDataTable dtPrintDataTable = new samDataSet.sam_jy_printDataTable();
        string strMainDataSourceName = "samDataSet_sam_jy_print";//sam_jy_printDataTable
        public JyPrint()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //	
            dtPrintDataTable.Columns.Remove("结果图1");
            dtPrintDataTable.Columns.Remove("结果图2");
            dtPrintDataTable.Columns.Remove("结果图3");
            dtPrintDataTable.Columns.Remove("结果图4");
            dtPrintDataTable.Columns.Remove("结果图5");
            DataColumn D1 = new DataColumn("结果图1", typeof(System.Byte[]));
            dtPrintDataTable.Columns.Add(D1);

            DataColumn D2 = new DataColumn("结果图2", typeof(System.Byte[]));
            dtPrintDataTable.Columns.Add(D2);

            DataColumn D3 = new DataColumn("结果图3", typeof(System.Byte[]));
            dtPrintDataTable.Columns.Add(D3);

            DataColumn D4 = new DataColumn("结果图4", typeof(System.Byte[]));
            dtPrintDataTable.Columns.Add(D4);

            DataColumn D5 = new DataColumn("结果图5", typeof(System.Byte[]));
            dtPrintDataTable.Columns.Add(D5);
        }

        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="printType">1：打印预览；0：打印</param>
        /// <param name="strfsample_id">样本ID</param>
        public void BllPrintViewer(int printType, string strfsample_id)
        {
            try
            {
                DataTable dtReport = this.bllReport.GetList(" fjy_id='" + strfsample_id + "'");
                if (dtReport.Rows.Count > 0)
                {
                    string str仪器结果id = "";
                    DataTable dtIMG = new DataTable();// 
                    if (dtReport.Rows[0]["finstr_result_id"] != null)
                    {
                        str仪器结果id = dtReport.Rows[0]["finstr_result_id"].ToString();
                        dtIMG = this.bllReport.BllImgDT(strfsample_id,str仪器结果id);
                    }

                    DataTable dtResult = this.bllReport.GetListS(" t.fjy_id='" + strfsample_id + "'");
                    

                    string strfinstr_id = "";//仪器ID 
                    if (dtReport.Rows[0]["fjy_instr"] != null)
                        strfinstr_id = dtReport.Rows[0]["fjy_instr"].ToString();
                    DataTable dtInstrReport = this.bllInstr.BllReportDT(strfinstr_id, 1);
                    string strReportCode = "ww.form.lis.sam.Report.ReportCom.rdlc";//报表代码

                    string strReportName = "";//报表名称
                    string strTimeAP = "";//检验时间安排 f1
                    string strCYdd = "";//采样地点 f2
                    if (dtInstrReport.Rows.Count > 0)
                    {
                        strReportCode = dtInstrReport.Rows[0]["fcode"].ToString();
                        strReportName = dtInstrReport.Rows[0]["fname"].ToString();
                        strTimeAP = dtInstrReport.Rows[0]["f1"].ToString();
                        strCYdd = dtInstrReport.Rows[0]["f2"].ToString();
                    }
                   

                    int intResultCount = dtResult.Rows.Count;//结果记录数
                    if (intResultCount > 0)
                    {
                        //---------结果值
                        int int序号 = 0;
                        for (int i = 0; i < intResultCount; i++)
                        {
                            int序号 = i + 1;
                            DataRow printRow = dtPrintDataTable.NewRow();
                            printRow["结果序号"] = int序号;
                            printRow["结果项目代码"] = dtResult.Rows[i]["fitem_code"];
                            printRow["结果项目名称"] = dtResult.Rows[i]["fitem_name"];
                            printRow["结果值"] = dtResult.Rows[i]["fvalue"];
                            printRow["结果项目单位"] = dtResult.Rows[i]["fitem_unit"];
                            printRow["结果标记"] = dtResult.Rows[i]["fitem_badge"];
                            printRow["结果参考值"] = dtResult.Rows[i]["fitem_ref"];
                            dtPrintDataTable.Rows.Add(printRow);
                        }

                        //----------报告值
                        dtPrintDataTable.Rows[0]["医院名称"] = LoginBLL.CustomerName;
                        dtPrintDataTable.Rows[0]["医院地址"] = LoginBLL.CustomerAdd;
                        dtPrintDataTable.Rows[0]["检验实验室"] = LoginBLL.strDeptName;
                        dtPrintDataTable.Rows[0]["医院联系电话"] = LoginBLL.CustomerTel;
                        dtPrintDataTable.Rows[0]["检验时间安排"] = strTimeAP;
                        dtPrintDataTable.Rows[0]["报表名称"] = strReportName;
                        dtPrintDataTable.Rows[0]["采样地点"] = strCYdd;
                        dtPrintDataTable.Rows[0]["姓名"] = dtReport.Rows[0]["fhz_name"];
                        dtPrintDataTable.Rows[0]["性别"] = dtReport.Rows[0]["fhz_sex"];
                        dtPrintDataTable.Rows[0]["年龄"] = dtReport.Rows[0]["fhz_age"];
                        dtPrintDataTable.Rows[0]["年龄单位"] = dtReport.Rows[0]["年龄单位"];
                        dtPrintDataTable.Rows[0]["申请单号"] = dtReport.Rows[0]["fapply_id"];
                        dtPrintDataTable.Rows[0]["样本条码号"] = dtReport.Rows[0]["fapply_id"];
                        dtPrintDataTable.Rows[0]["样本号"] = dtReport.Rows[0]["fjy_yb_code"];
                        dtPrintDataTable.Rows[0]["样本类别"] = dtReport.Rows[0]["类型"];
                        dtPrintDataTable.Rows[0]["送检科室"] = dtReport.Rows[0]["科室"];
                        dtPrintDataTable.Rows[0]["申请者"] = dtReport.Rows[0]["申请医师"];
                        dtPrintDataTable.Rows[0]["检验者"] = dtReport.Rows[0]["检验医师"];
                        dtPrintDataTable.Rows[0]["审核者"] = dtReport.Rows[0]["fchenk_user_id"];

                        dtPrintDataTable.Rows[0]["床号"] = dtReport.Rows[0]["fhz_bed"];
                        dtPrintDataTable.Rows[0]["病人ID"] = dtReport.Rows[0]["fhz_id"];
                        dtPrintDataTable.Rows[0]["诊断"] = dtReport.Rows[0]["fjy_lczd"];

                        dtPrintDataTable.Rows[0]["申请时间"] = dtReport.Rows[0]["fapply_time"];
                        dtPrintDataTable.Rows[0]["采样时间"] = dtReport.Rows[0]["fsampling_time"];
                        //dtPrintDataTable.Rows[0]["收样时间"] = dtReport.Rows[0]["fget_user_time"];
                        try
                        {
                            if (dtReport.Rows[0]["fsys_time"].ToString().Equals("")) { }
                            else
                            {
                                dtPrintDataTable.Rows[0]["检验时间"] = Convert.ToDateTime(dtReport.Rows[0]["fsys_time"].ToString()).ToString("MM-dd HH:mm");
                            }
                        }
                        catch { }
                        try
                        {
                            if (dtReport.Rows[0]["fcheck_time"].ToString().Equals("")) { }
                            else
                            {
                                dtPrintDataTable.Rows[0]["审核时间"] = Convert.ToDateTime(dtReport.Rows[0]["fcheck_time"].ToString()).ToString("MM-dd HH:mm");
                            }
                        }
                        catch { }
                        if (printType == 1)
                        {
                            if (dtReport.Rows[0]["fprint_time"].ToString().Equals("")) { }
                            else
                            {
                                try
                                {
                                    dtPrintDataTable.Rows[0]["打印时间"] = Convert.ToDateTime(dtReport.Rows[0]["fprint_time"].ToString()).ToString("MM-dd HH:mm");
                                }
                                catch { }
                            }
                        }
                        else
                        {
                            dtPrintDataTable.Rows[0]["打印时间"] = DateTime.Now.ToString("MM-dd HH:mm");//dtReport.Rows[0]["fprint_time"];
                        }
                        dtPrintDataTable.Rows[0]["备注"] = dtReport.Rows[0]["fremark"];

                       

                        dsPrint.Tables.Add(dtPrintDataTable);
                        //----------图值
                        if (dtIMG.Rows.Count > 0)
                        {
                            for (int iimg = 0; iimg < dtIMG.Rows.Count; iimg++)
                            {
                                switch (iimg)
                                {
                                    case 0:
                                        dtPrintDataTable.Rows[0]["结果图1"] = dtIMG.Rows[iimg]["FImg"];
                                        dtPrintDataTable.Rows[0]["结果图1_名称"] = dtIMG.Rows[iimg]["FImgNmae"];
                                        break;
                                    case 1:
                                        dtPrintDataTable.Rows[0]["结果图2"] = dtIMG.Rows[iimg]["FImg"];
                                        dtPrintDataTable.Rows[0]["结果图2_名称"] = dtIMG.Rows[iimg]["FImgNmae"];
                                        break;
                                    case 2:
                                        dtPrintDataTable.Rows[0]["结果图3"] = dtIMG.Rows[iimg]["FImg"];
                                        dtPrintDataTable.Rows[0]["结果图3_名称"] = dtIMG.Rows[iimg]["FImgNmae"];
                                        break;
                                    case 3:
                                        dtPrintDataTable.Rows[0]["结果图4"] = dtIMG.Rows[iimg]["FImg"];
                                        dtPrintDataTable.Rows[0]["结果图4_名称"] = dtIMG.Rows[iimg]["FImgNmae"];
                                        break;
                                    case 4:
                                        dtPrintDataTable.Rows[0]["结果图5"] = dtIMG.Rows[iimg]["FImg"];
                                        dtPrintDataTable.Rows[0]["结果图5_名称"] = dtIMG.Rows[iimg]["FImgNmae"];
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                        WWPrintViewer(printType, strfsample_id, strReportName, strReportCode);
                    }
                    else
                    {
                        WWMessage.MessageShowWarning("报告结果为空！");
                    }
                }
                else
                {
                    WWMessage.MessageShowWarning("报告为空，操作失败！请选择报告后重试。");
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strReportName">报表数据源名</param>
        /// <param name="strReportPath">报表文档</param>
        private void WWPrintViewer(int printType,string fjy_id,string strReportName, string strReportPath)
        {
            try
            {
               
                if (printType == 1)
                {
                    //报表公共窗口
                    ReportViewer feportviewer = new ReportViewer();
                    //报表数据集
                    feportviewer.MainDataSet = dsPrint;
                    //报表名
                    feportviewer.ReportName = strReportName;
                    //报表数据源名
                    feportviewer.MainDataSourceName = strMainDataSourceName;
                    //报表文档
                    feportviewer.ReportPath = strReportPath;
                    //显示报表窗口
                    feportviewer.ShowDialog();
                   // this.bllReport.BllReportUpdatePrintTime(fjy_id);
                }
                else {
                    //报表公共窗口
                    ReportPrint feportviewer = new ReportPrint();
                    //报表数据集
                    feportviewer.MainDataSet = dsPrint;
                    //报表名
                    feportviewer.ReportName = strReportName;
                    //报表数据源名
                    feportviewer.MainDataSourceName = strMainDataSourceName;
                    //报表文档
                    feportviewer.ReportPath = strReportPath;
                    //显示报表窗口
                    feportviewer.ShowDialog();
                    this.bllReport.BllReportUpdatePrintTime(fjy_id);
                }
               
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        

    }
}
