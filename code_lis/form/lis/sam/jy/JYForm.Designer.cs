﻿namespace ww.form.lis.sam.jy
{
    partial class JYForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label fjy_dateLabel;
            System.Windows.Forms.Label fjy_instrLabel;
            System.Windows.Forms.Label fsample_codeLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txt电子健康卡 = new System.Windows.Forms.TextBox();
            this.txt身份证号 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.comboBoxAgeUnit = new System.Windows.Forms.ComboBox();
            this.cboPatientType = new System.Windows.Forms.ComboBox();
            this.fhz_sex性别Combo = new System.Windows.Forms.ComboBox();
            this.fcheck_timelabel = new System.Windows.Forms.Label();
            this.label状态 = new System.Windows.Forms.Label();
            this.fjy_lczdtextBox = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.fchenk_user_idlabel = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.fprint_ztlabel = new System.Windows.Forms.Label();
            this.fjy_ztlabel = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.fremarktextBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.fchenk_user_idComboBox = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.freport_timedateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.fjy_user_idtextBoxCbo = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.fsampling_timedateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.fapply_timeDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.fjy_yb_typeComboBox = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.fapply_idtextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.fhz_bedtextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.fhz_dept科室cbo = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.fjy_sf_typetextBoxCombox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.fhz_age年龄 = new System.Windows.Forms.TextBox();
            this.fhz_name姓名 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.fhz_zyh住院号 = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.fjy_yb_code样本 = new System.Windows.Forms.TextBox();
            this.fjy_date样本日期 = new System.Windows.Forms.DateTimePicker();
            this.fjy_instrComboBox_仪器 = new System.Windows.Forms.ComboBox();
            this.dataGridViewResult = new System.Windows.Forms.DataGridView();
            this.fitem_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fvalue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fcutoff = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_badge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_ref = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forder_by = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fresult_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fremark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.打印ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.解除审核ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView样本 = new System.Windows.Forms.DataGridView();
            this.fjy_yb_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhz_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhz_zyh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhz_bed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.性别 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhz_age = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.年龄单位 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.科室 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.样本 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.检验医师 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fprint_zt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fjy_zt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.类型 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fapply_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fjy_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSource样本 = new System.Windows.Forms.BindingSource(this.components);
            this.panel5 = new System.Windows.Forms.Panel();
            this.comboBox过滤样本 = new System.Windows.Forms.ComboBox();
            this.button删除样本 = new System.Windows.Forms.Button();
            this.button刷新样本 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.项目 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.最近 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.上一次 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.上二次 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.上三次 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.项目id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel6 = new System.Windows.Forms.Panel();
            this.button比较 = new System.Windows.Forms.Button();
            this.label21比较住院号 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button读卡 = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.button刷新结果 = new System.Windows.Forms.Button();
            this.button仪器数据 = new System.Windows.Forms.Button();
            this.button删除 = new System.Windows.Forms.Button();
            this.button打印 = new System.Windows.Forms.Button();
            this.button下一个 = new System.Windows.Forms.Button();
            this.button上一个 = new System.Windows.Forms.Button();
            this.button审核 = new System.Windows.Forms.Button();
            this.button新增 = new System.Windows.Forms.Button();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.fapply_user_id送检医师cbo = new DevExpress.XtraEditors.LookUpEdit();
            fjy_dateLabel = new System.Windows.Forms.Label();
            fjy_instrLabel = new System.Windows.Forms.Label();
            fsample_codeLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView样本)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource样本)).BeginInit();
            this.panel5.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel6.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fapply_user_id送检医师cbo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // fjy_dateLabel
            // 
            fjy_dateLabel.AutoSize = true;
            fjy_dateLabel.Location = new System.Drawing.Point(7, 40);
            fjy_dateLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fjy_dateLabel.Name = "fjy_dateLabel";
            fjy_dateLabel.Size = new System.Drawing.Size(72, 16);
            fjy_dateLabel.TabIndex = 6;
            fjy_dateLabel.Text = "样本日期";
            // 
            // fjy_instrLabel
            // 
            fjy_instrLabel.AutoSize = true;
            fjy_instrLabel.Location = new System.Drawing.Point(7, 9);
            fjy_instrLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fjy_instrLabel.Name = "fjy_instrLabel";
            fjy_instrLabel.Size = new System.Drawing.Size(72, 16);
            fjy_instrLabel.TabIndex = 4;
            fjy_instrLabel.Text = "检验仪器";
            // 
            // fsample_codeLabel
            // 
            fsample_codeLabel.AutoSize = true;
            fsample_codeLabel.Location = new System.Drawing.Point(24, 70);
            fsample_codeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fsample_codeLabel.Name = "fsample_codeLabel";
            fsample_codeLabel.Size = new System.Drawing.Size(56, 16);
            fsample_codeLabel.TabIndex = 8;
            fsample_codeLabel.Text = "样本号";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(4, 36);
            label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(72, 16);
            label1.TabIndex = 12;
            label1.Text = "病人类型";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("宋体", 8F);
            label2.Location = new System.Drawing.Point(0, 66);
            label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(84, 14);
            label2.TabIndex = 14;
            label2.Text = "门诊/住院号";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Font = new System.Drawing.Font("宋体", 9.5F);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(299, 784);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.txt电子健康卡);
            this.panel3.Controls.Add(this.txt身份证号);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.comboBoxAgeUnit);
            this.panel3.Controls.Add(this.cboPatientType);
            this.panel3.Controls.Add(this.fhz_sex性别Combo);
            this.panel3.Controls.Add(this.fcheck_timelabel);
            this.panel3.Controls.Add(label2);
            this.panel3.Controls.Add(this.label状态);
            this.panel3.Controls.Add(this.fjy_lczdtextBox);
            this.panel3.Controls.Add(this.label20);
            this.panel3.Controls.Add(this.fchenk_user_idlabel);
            this.panel3.Controls.Add(this.label19);
            this.panel3.Controls.Add(this.fprint_ztlabel);
            this.panel3.Controls.Add(this.fjy_ztlabel);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.fremarktextBox);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.fchenk_user_idComboBox);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.freport_timedateTimePicker);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.fjy_user_idtextBoxCbo);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.fsampling_timedateTimePicker);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.fapply_timeDateTimePicker);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.fjy_yb_typeComboBox);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.fapply_idtextBox);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.fhz_bedtextBox);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.fhz_dept科室cbo);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.fjy_sf_typetextBoxCombox);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.fhz_age年龄);
            this.panel3.Controls.Add(this.fhz_name姓名);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.fhz_zyh住院号);
            this.panel3.Controls.Add(label1);
            this.panel3.Controls.Add(this.fapply_user_id送检医师cbo);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 100);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(299, 684);
            this.panel3.TabIndex = 1;
            // 
            // txt电子健康卡
            // 
            this.txt电子健康卡.Location = new System.Drawing.Point(92, 524);
            this.txt电子健康卡.Margin = new System.Windows.Forms.Padding(4);
            this.txt电子健康卡.Name = "txt电子健康卡";
            this.txt电子健康卡.Size = new System.Drawing.Size(195, 26);
            this.txt电子健康卡.TabIndex = 160;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(92, 494);
            this.txt身份证号.Margin = new System.Windows.Forms.Padding(4);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Size = new System.Drawing.Size(195, 26);
            this.txt身份证号.TabIndex = 159;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(7, 528);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(72, 16);
            this.label22.TabIndex = 158;
            this.label22.Text = "电子卡号";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(4, 498);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(72, 16);
            this.label21.TabIndex = 157;
            this.label21.Text = "身份证号";
            // 
            // comboBoxAgeUnit
            // 
            this.comboBoxAgeUnit.DisplayMember = "fname";
            this.comboBoxAgeUnit.FormattingEnabled = true;
            this.comboBoxAgeUnit.Location = new System.Drawing.Point(241, 116);
            this.comboBoxAgeUnit.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxAgeUnit.Name = "comboBoxAgeUnit";
            this.comboBoxAgeUnit.Size = new System.Drawing.Size(45, 24);
            this.comboBoxAgeUnit.TabIndex = 22;
            this.comboBoxAgeUnit.ValueMember = "fcode";
            this.comboBoxAgeUnit.SelectedValueChanged += new System.EventHandler(this.comboBoxAgeUnit_SelectedValueChanged);
            this.comboBoxAgeUnit.Enter += new System.EventHandler(this.comboBoxAgeUnit_Enter);
            this.comboBoxAgeUnit.Leave += new System.EventHandler(this.comboBoxAgeUnit_Leave);
            // 
            // cboPatientType
            // 
            this.cboPatientType.DisplayMember = "fname";
            this.cboPatientType.FormattingEnabled = true;
            this.cboPatientType.Location = new System.Drawing.Point(92, 30);
            this.cboPatientType.Margin = new System.Windows.Forms.Padding(4);
            this.cboPatientType.Name = "cboPatientType";
            this.cboPatientType.Size = new System.Drawing.Size(195, 24);
            this.cboPatientType.TabIndex = 11;
            this.cboPatientType.ValueMember = "fcode";
            this.cboPatientType.SelectedValueChanged += new System.EventHandler(this.cboPatientType_SelectedValueChanged);
            this.cboPatientType.Enter += new System.EventHandler(this.cboPatientType_Enter);
            this.cboPatientType.Leave += new System.EventHandler(this.cboPatientType_Leave);
            // 
            // fhz_sex性别Combo
            // 
            this.fhz_sex性别Combo.DisplayMember = "fname";
            this.fhz_sex性别Combo.FormattingEnabled = true;
            this.fhz_sex性别Combo.Location = new System.Drawing.Point(92, 116);
            this.fhz_sex性别Combo.Margin = new System.Windows.Forms.Padding(4);
            this.fhz_sex性别Combo.Name = "fhz_sex性别Combo";
            this.fhz_sex性别Combo.Size = new System.Drawing.Size(45, 24);
            this.fhz_sex性别Combo.TabIndex = 20;
            this.fhz_sex性别Combo.ValueMember = "fcode";
            this.fhz_sex性别Combo.SelectedValueChanged += new System.EventHandler(this.fhz_sex性别Combo_SelectedValueChanged);
            this.fhz_sex性别Combo.Enter += new System.EventHandler(this.fhz_sex性别Combo_Enter);
            this.fhz_sex性别Combo.Leave += new System.EventHandler(this.fhz_sex性别Combo_Leave);
            // 
            // fcheck_timelabel
            // 
            this.fcheck_timelabel.AutoSize = true;
            this.fcheck_timelabel.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.fcheck_timelabel.Location = new System.Drawing.Point(92, 628);
            this.fcheck_timelabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.fcheck_timelabel.Name = "fcheck_timelabel";
            this.fcheck_timelabel.Size = new System.Drawing.Size(16, 16);
            this.fcheck_timelabel.TabIndex = 156;
            this.fcheck_timelabel.Text = " ";
            // 
            // label状态
            // 
            this.label状态.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label状态.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label状态.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.label状态.ForeColor = System.Drawing.Color.Red;
            this.label状态.Location = new System.Drawing.Point(1, 85);
            this.label状态.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label状态.Name = "label状态";
            this.label状态.Size = new System.Drawing.Size(29, 152);
            this.label状态.TabIndex = 155;
            this.label状态.Text = "当\r\n前\r\n样\r\n本\r\n不\r\n可\r\n修\r\n改";
            this.label状态.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label状态.Visible = false;
            // 
            // fjy_lczdtextBox
            // 
            this.fjy_lczdtextBox.Location = new System.Drawing.Point(92, 434);
            this.fjy_lczdtextBox.Margin = new System.Windows.Forms.Padding(4);
            this.fjy_lczdtextBox.Name = "fjy_lczdtextBox";
            this.fjy_lczdtextBox.Size = new System.Drawing.Size(195, 26);
            this.fjy_lczdtextBox.TabIndex = 145;
            this.fjy_lczdtextBox.DoubleClick += new System.EventHandler(this.fjy_lczdtextBox_DoubleClick);
            this.fjy_lczdtextBox.Enter += new System.EventHandler(this.fjy_lczdtextBox_Enter);
            this.fjy_lczdtextBox.Leave += new System.EventHandler(this.fjy_lczdtextBox_Leave);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(4, 440);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(72, 16);
            this.label20.TabIndex = 153;
            this.label20.Text = "临床诊断";
            // 
            // fchenk_user_idlabel
            // 
            this.fchenk_user_idlabel.AutoSize = true;
            this.fchenk_user_idlabel.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.fchenk_user_idlabel.Location = new System.Drawing.Point(92, 595);
            this.fchenk_user_idlabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.fchenk_user_idlabel.Name = "fchenk_user_idlabel";
            this.fchenk_user_idlabel.Size = new System.Drawing.Size(16, 16);
            this.fchenk_user_idlabel.TabIndex = 151;
            this.fchenk_user_idlabel.Text = " ";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(4, 595);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(72, 16);
            this.label19.TabIndex = 150;
            this.label19.Text = "最后修改";
            // 
            // fprint_ztlabel
            // 
            this.fprint_ztlabel.AutoSize = true;
            this.fprint_ztlabel.ForeColor = System.Drawing.Color.Red;
            this.fprint_ztlabel.Location = new System.Drawing.Point(165, 566);
            this.fprint_ztlabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.fprint_ztlabel.Name = "fprint_ztlabel";
            this.fprint_ztlabel.Size = new System.Drawing.Size(56, 16);
            this.fprint_ztlabel.TabIndex = 149;
            this.fprint_ztlabel.Text = "未打印";
            // 
            // fjy_ztlabel
            // 
            this.fjy_ztlabel.AutoSize = true;
            this.fjy_ztlabel.ForeColor = System.Drawing.Color.Blue;
            this.fjy_ztlabel.Location = new System.Drawing.Point(92, 566);
            this.fjy_ztlabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.fjy_ztlabel.Name = "fjy_ztlabel";
            this.fjy_ztlabel.Size = new System.Drawing.Size(56, 16);
            this.fjy_ztlabel.TabIndex = 148;
            this.fjy_ztlabel.Text = "未审核";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(4, 566);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(72, 16);
            this.label18.TabIndex = 147;
            this.label18.Text = "样本状态";
            // 
            // fremarktextBox
            // 
            this.fremarktextBox.Location = new System.Drawing.Point(92, 464);
            this.fremarktextBox.Margin = new System.Windows.Forms.Padding(4);
            this.fremarktextBox.Name = "fremarktextBox";
            this.fremarktextBox.Size = new System.Drawing.Size(195, 26);
            this.fremarktextBox.TabIndex = 146;
            this.fremarktextBox.Enter += new System.EventHandler(this.fremarktextBox_Enter);
            this.fremarktextBox.Leave += new System.EventHandler(this.fremarktextBox_Leave);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("宋体", 8F);
            this.label17.Location = new System.Drawing.Point(0, 470);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(91, 14);
            this.label17.TabIndex = 145;
            this.label17.Text = "备注(档案号)";
            // 
            // fchenk_user_idComboBox
            // 
            this.fchenk_user_idComboBox.DisplayMember = "fname";
            this.fchenk_user_idComboBox.Location = new System.Drawing.Point(92, 406);
            this.fchenk_user_idComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.fchenk_user_idComboBox.Name = "fchenk_user_idComboBox";
            this.fchenk_user_idComboBox.Size = new System.Drawing.Size(195, 24);
            this.fchenk_user_idComboBox.TabIndex = 144;
            this.fchenk_user_idComboBox.ValueMember = "fperson_id";
            this.fchenk_user_idComboBox.DoubleClick += new System.EventHandler(this.fchenk_user_idtextBox_DoubleClick);
            this.fchenk_user_idComboBox.SelectedValueChanged += new System.EventHandler(this.fchenk_user_idComboBox_SelectedValueChanged);
            this.fchenk_user_idComboBox.Enter += new System.EventHandler(this.fchenk_user_idtextBox_Enter);
            this.fchenk_user_idComboBox.Leave += new System.EventHandler(this.fchenk_user_idtextBox_Leave);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(4, 411);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(72, 16);
            this.label16.TabIndex = 143;
            this.label16.Text = "核对医师";
            // 
            // freport_timedateTimePicker
            // 
            this.freport_timedateTimePicker.CustomFormat = "yyyy-MM-dd HH:mm";
            this.freport_timedateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.freport_timedateTimePicker.Location = new System.Drawing.Point(92, 348);
            this.freport_timedateTimePicker.Margin = new System.Windows.Forms.Padding(4);
            this.freport_timedateTimePicker.Name = "freport_timedateTimePicker";
            this.freport_timedateTimePicker.ShowUpDown = true;
            this.freport_timedateTimePicker.Size = new System.Drawing.Size(195, 26);
            this.freport_timedateTimePicker.TabIndex = 140;
            this.freport_timedateTimePicker.Enter += new System.EventHandler(this.freport_timedateTimePicker_Enter);
            this.freport_timedateTimePicker.Leave += new System.EventHandler(this.freport_timedateTimePicker_Leave);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(4, 354);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 16);
            this.label15.TabIndex = 141;
            this.label15.Text = "报告时间";
            // 
            // fjy_user_idtextBoxCbo
            // 
            this.fjy_user_idtextBoxCbo.DisplayMember = "fname";
            this.fjy_user_idtextBoxCbo.Location = new System.Drawing.Point(92, 378);
            this.fjy_user_idtextBoxCbo.Margin = new System.Windows.Forms.Padding(4);
            this.fjy_user_idtextBoxCbo.Name = "fjy_user_idtextBoxCbo";
            this.fjy_user_idtextBoxCbo.Size = new System.Drawing.Size(195, 24);
            this.fjy_user_idtextBoxCbo.TabIndex = 142;
            this.fjy_user_idtextBoxCbo.ValueMember = "fperson_id";
            this.fjy_user_idtextBoxCbo.DoubleClick += new System.EventHandler(this.fjy_user_idtextBox_DoubleClick);
            this.fjy_user_idtextBoxCbo.SelectedValueChanged += new System.EventHandler(this.fjy_user_idtextBoxCbo_SelectedValueChanged);
            this.fjy_user_idtextBoxCbo.Enter += new System.EventHandler(this.fjy_user_idtextBox_Enter);
            this.fjy_user_idtextBoxCbo.Leave += new System.EventHandler(this.fjy_user_idtextBox_Leave);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(4, 382);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(72, 16);
            this.label14.TabIndex = 139;
            this.label14.Text = "检验医师";
            // 
            // fsampling_timedateTimePicker
            // 
            this.fsampling_timedateTimePicker.CustomFormat = "yyyy-MM-dd HH:mm";
            this.fsampling_timedateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.fsampling_timedateTimePicker.Location = new System.Drawing.Point(92, 319);
            this.fsampling_timedateTimePicker.Margin = new System.Windows.Forms.Padding(4);
            this.fsampling_timedateTimePicker.Name = "fsampling_timedateTimePicker";
            this.fsampling_timedateTimePicker.ShowUpDown = true;
            this.fsampling_timedateTimePicker.Size = new System.Drawing.Size(195, 26);
            this.fsampling_timedateTimePicker.TabIndex = 138;
            this.fsampling_timedateTimePicker.Enter += new System.EventHandler(this.fsampling_timedateTimePicker_Enter);
            this.fsampling_timedateTimePicker.Leave += new System.EventHandler(this.fsampling_timedateTimePicker_Leave);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(4, 325);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 16);
            this.label13.TabIndex = 137;
            this.label13.Text = "采样时间";
            // 
            // fapply_timeDateTimePicker
            // 
            this.fapply_timeDateTimePicker.CustomFormat = "yyyy-MM-dd HH:mm";
            this.fapply_timeDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.fapply_timeDateTimePicker.Location = new System.Drawing.Point(92, 290);
            this.fapply_timeDateTimePicker.Margin = new System.Windows.Forms.Padding(4);
            this.fapply_timeDateTimePicker.Name = "fapply_timeDateTimePicker";
            this.fapply_timeDateTimePicker.ShowUpDown = true;
            this.fapply_timeDateTimePicker.Size = new System.Drawing.Size(195, 26);
            this.fapply_timeDateTimePicker.TabIndex = 136;
            this.fapply_timeDateTimePicker.Enter += new System.EventHandler(this.fapply_timeDateTimePicker_Enter);
            this.fapply_timeDateTimePicker.Leave += new System.EventHandler(this.fapply_timeDateTimePicker_Leave);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(4, 296);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 16);
            this.label12.TabIndex = 35;
            this.label12.Text = "送检时间";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 268);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 16);
            this.label9.TabIndex = 33;
            this.label9.Text = "送检医师";
            // 
            // fjy_yb_typeComboBox
            // 
            this.fjy_yb_typeComboBox.DisplayMember = "fname";
            this.fjy_yb_typeComboBox.Location = new System.Drawing.Point(92, 232);
            this.fjy_yb_typeComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.fjy_yb_typeComboBox.Name = "fjy_yb_typeComboBox";
            this.fjy_yb_typeComboBox.Size = new System.Drawing.Size(195, 24);
            this.fjy_yb_typeComboBox.TabIndex = 32;
            this.fjy_yb_typeComboBox.ValueMember = "fsample_type_id";
            this.fjy_yb_typeComboBox.DoubleClick += new System.EventHandler(this.fjy_yb_typetextBox_DoubleClick);
            this.fjy_yb_typeComboBox.SelectedValueChanged += new System.EventHandler(this.fjy_yb_typeComboBox_SelectedValueChanged);
            this.fjy_yb_typeComboBox.Enter += new System.EventHandler(this.fjy_yb_typetextBox_Enter);
            this.fjy_yb_typeComboBox.Leave += new System.EventHandler(this.fjy_yb_typetextBox_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 239);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 16);
            this.label10.TabIndex = 31;
            this.label10.Text = "样本类型";
            // 
            // fapply_idtextBox
            // 
            this.fapply_idtextBox.Location = new System.Drawing.Point(92, 1);
            this.fapply_idtextBox.Margin = new System.Windows.Forms.Padding(4);
            this.fapply_idtextBox.Name = "fapply_idtextBox";
            this.fapply_idtextBox.Size = new System.Drawing.Size(195, 26);
            this.fapply_idtextBox.TabIndex = 10;
            this.fapply_idtextBox.TextChanged += new System.EventHandler(this.fapply_idtextBox_TextChanged);
            this.fapply_idtextBox.Enter += new System.EventHandler(this.fapply_idtextBox_Enter);
            this.fapply_idtextBox.Leave += new System.EventHandler(this.fapply_idtextBox_Leave);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(21, 8);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 16);
            this.label11.TabIndex = 29;
            this.label11.Text = "申请号";
            // 
            // fhz_bedtextBox
            // 
            this.fhz_bedtextBox.Location = new System.Drawing.Point(92, 202);
            this.fhz_bedtextBox.Margin = new System.Windows.Forms.Padding(4);
            this.fhz_bedtextBox.Name = "fhz_bedtextBox";
            this.fhz_bedtextBox.Size = new System.Drawing.Size(195, 26);
            this.fhz_bedtextBox.TabIndex = 28;
            this.fhz_bedtextBox.Enter += new System.EventHandler(this.fhz_bedtextBox_Enter);
            this.fhz_bedtextBox.Leave += new System.EventHandler(this.fhz_bedtextBox_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(39, 209);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 16);
            this.label8.TabIndex = 27;
            this.label8.Text = "床号";
            // 
            // fhz_dept科室cbo
            // 
            this.fhz_dept科室cbo.DisplayMember = "fname";
            this.fhz_dept科室cbo.Location = new System.Drawing.Point(92, 174);
            this.fhz_dept科室cbo.Margin = new System.Windows.Forms.Padding(4);
            this.fhz_dept科室cbo.Name = "fhz_dept科室cbo";
            this.fhz_dept科室cbo.Size = new System.Drawing.Size(195, 24);
            this.fhz_dept科室cbo.TabIndex = 26;
            this.fhz_dept科室cbo.ValueMember = "fdept_id";
            this.fhz_dept科室cbo.DoubleClick += new System.EventHandler(this.fhz_depttextBox_DoubleClick);
            this.fhz_dept科室cbo.SelectedValueChanged += new System.EventHandler(this.fhz_dept科室cbo_SelectedValueChanged);
            this.fhz_dept科室cbo.TextChanged += new System.EventHandler(this.fhz_depttextBox_TextChanged);
            this.fhz_dept科室cbo.Click += new System.EventHandler(this.fhz_dept科室cbo_Click);
            this.fhz_dept科室cbo.Enter += new System.EventHandler(this.fhz_depttextBox_Enter);
            this.fhz_dept科室cbo.Leave += new System.EventHandler(this.fhz_depttextBox_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(39, 180);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 16);
            this.label7.TabIndex = 25;
            this.label7.Text = "科室";
            // 
            // fjy_sf_typetextBoxCombox
            // 
            this.fjy_sf_typetextBoxCombox.DisplayMember = "fname";
            this.fjy_sf_typetextBoxCombox.Location = new System.Drawing.Point(92, 145);
            this.fjy_sf_typetextBoxCombox.Margin = new System.Windows.Forms.Padding(4);
            this.fjy_sf_typetextBoxCombox.Name = "fjy_sf_typetextBoxCombox";
            this.fjy_sf_typetextBoxCombox.Size = new System.Drawing.Size(195, 24);
            this.fjy_sf_typetextBoxCombox.TabIndex = 24;
            this.fjy_sf_typetextBoxCombox.ValueMember = "fcode";
            this.fjy_sf_typetextBoxCombox.DoubleClick += new System.EventHandler(this.fjy_sf_typetextBox_DoubleClick);
            this.fjy_sf_typetextBoxCombox.SelectedValueChanged += new System.EventHandler(this.fjy_sf_typetextBoxCombox_SelectedValueChanged);
            this.fjy_sf_typetextBoxCombox.Enter += new System.EventHandler(this.fjy_sf_typetextBox_Enter);
            this.fjy_sf_typetextBoxCombox.Leave += new System.EventHandler(this.fjy_sf_typetextBox_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(39, 151);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 16);
            this.label6.TabIndex = 23;
            this.label6.Text = "费别";
            // 
            // fhz_age年龄
            // 
            this.fhz_age年龄.Location = new System.Drawing.Point(192, 116);
            this.fhz_age年龄.Margin = new System.Windows.Forms.Padding(4);
            this.fhz_age年龄.Name = "fhz_age年龄";
            this.fhz_age年龄.Size = new System.Drawing.Size(45, 26);
            this.fhz_age年龄.TabIndex = 21;
            this.fhz_age年龄.TextChanged += new System.EventHandler(this.fhz_age年龄_TextChanged);
            this.fhz_age年龄.Enter += new System.EventHandler(this.fhz_agetextBox_Enter);
            this.fhz_age年龄.Leave += new System.EventHandler(this.fhz_agetextBox_Leave);
            // 
            // fhz_name姓名
            // 
            this.fhz_name姓名.Location = new System.Drawing.Point(92, 88);
            this.fhz_name姓名.Margin = new System.Windows.Forms.Padding(4);
            this.fhz_name姓名.Name = "fhz_name姓名";
            this.fhz_name姓名.Size = new System.Drawing.Size(195, 26);
            this.fhz_name姓名.TabIndex = 19;
            this.fhz_name姓名.Enter += new System.EventHandler(this.fhz_nametextBox_Enter);
            this.fhz_name姓名.Leave += new System.EventHandler(this.fhz_nametextBox_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(143, 122);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 16);
            this.label5.TabIndex = 17;
            this.label5.Text = "年龄";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(39, 122);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 16);
            this.label4.TabIndex = 16;
            this.label4.Text = "性别";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 94);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 16);
            this.label3.TabIndex = 15;
            this.label3.Text = "姓名";
            // 
            // fhz_zyh住院号
            // 
            this.fhz_zyh住院号.Location = new System.Drawing.Point(92, 59);
            this.fhz_zyh住院号.Margin = new System.Windows.Forms.Padding(4);
            this.fhz_zyh住院号.Name = "fhz_zyh住院号";
            this.fhz_zyh住院号.Size = new System.Drawing.Size(195, 26);
            this.fhz_zyh住院号.TabIndex = 13;
            this.fhz_zyh住院号.TextChanged += new System.EventHandler(this.fhz_zyh住院号_TextChanged);
            this.fhz_zyh住院号.Enter += new System.EventHandler(this.fhz_zyhtextBox_Enter);
            this.fhz_zyh住院号.Leave += new System.EventHandler(this.fhz_zyhtextBox_Leave);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(fsample_codeLabel);
            this.panel2.Controls.Add(this.fjy_yb_code样本);
            this.panel2.Controls.Add(fjy_dateLabel);
            this.panel2.Controls.Add(this.fjy_date样本日期);
            this.panel2.Controls.Add(fjy_instrLabel);
            this.panel2.Controls.Add(this.fjy_instrComboBox_仪器);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(299, 100);
            this.panel2.TabIndex = 0;
            // 
            // fjy_yb_code样本
            // 
            this.fjy_yb_code样本.Location = new System.Drawing.Point(92, 64);
            this.fjy_yb_code样本.Margin = new System.Windows.Forms.Padding(4);
            this.fjy_yb_code样本.Name = "fjy_yb_code样本";
            this.fjy_yb_code样本.Size = new System.Drawing.Size(195, 26);
            this.fjy_yb_code样本.TabIndex = 9;
            this.fjy_yb_code样本.Text = "1";
            this.fjy_yb_code样本.Enter += new System.EventHandler(this.fjy_yb_code样本_Enter);
            this.fjy_yb_code样本.Leave += new System.EventHandler(this.fjy_yb_codeTextBox_Leave);
            // 
            // fjy_date样本日期
            // 
            this.fjy_date样本日期.Location = new System.Drawing.Point(92, 34);
            this.fjy_date样本日期.Margin = new System.Windows.Forms.Padding(4);
            this.fjy_date样本日期.Name = "fjy_date样本日期";
            this.fjy_date样本日期.Size = new System.Drawing.Size(195, 26);
            this.fjy_date样本日期.TabIndex = 7;
            this.fjy_date样本日期.ValueChanged += new System.EventHandler(this.fjy_dateDateTimePicker_ValueChanged);
            this.fjy_date样本日期.Enter += new System.EventHandler(this.fjy_date样本日期_Enter);
            this.fjy_date样本日期.Leave += new System.EventHandler(this.fjy_date样本日期_Leave);
            // 
            // fjy_instrComboBox_仪器
            // 
            this.fjy_instrComboBox_仪器.DisplayMember = "fname";
            this.fjy_instrComboBox_仪器.FormattingEnabled = true;
            this.fjy_instrComboBox_仪器.Location = new System.Drawing.Point(92, 4);
            this.fjy_instrComboBox_仪器.Margin = new System.Windows.Forms.Padding(4);
            this.fjy_instrComboBox_仪器.Name = "fjy_instrComboBox_仪器";
            this.fjy_instrComboBox_仪器.Size = new System.Drawing.Size(195, 24);
            this.fjy_instrComboBox_仪器.TabIndex = 5;
            this.fjy_instrComboBox_仪器.ValueMember = "finstr_id";
            this.fjy_instrComboBox_仪器.SelectedIndexChanged += new System.EventHandler(this.fjy_instrComboBox_仪器_SelectedIndexChanged);
            // 
            // dataGridViewResult
            // 
            this.dataGridViewResult.AllowUserToAddRows = false;
            this.dataGridViewResult.AllowUserToResizeRows = false;
            this.dataGridViewResult.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fitem_code,
            this.fitem_name,
            this.fvalue,
            this.fod,
            this.fcutoff,
            this.fitem_badge,
            this.fitem_ref,
            this.fitem_unit,
            this.forder_by,
            this.fitem_id,
            this.fresult_id,
            this.fremark});
            this.dataGridViewResult.ContextMenuStrip = this.contextMenuStrip1;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewResult.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewResult.Dock = System.Windows.Forms.DockStyle.Left;
            this.dataGridViewResult.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridViewResult.Location = new System.Drawing.Point(303, 0);
            this.dataGridViewResult.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewResult.MultiSelect = false;
            this.dataGridViewResult.Name = "dataGridViewResult";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewResult.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewResult.RowHeadersVisible = false;
            this.dataGridViewResult.RowHeadersWidth = 30;
            this.dataGridViewResult.RowTemplate.Height = 23;
            this.dataGridViewResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewResult.ShowCellToolTips = false;
            this.dataGridViewResult.ShowEditingIcon = false;
            this.dataGridViewResult.Size = new System.Drawing.Size(531, 784);
            this.dataGridViewResult.TabIndex = 132;
            this.dataGridViewResult.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewResult_CellDoubleClick);
            this.dataGridViewResult.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewResult_CellMouseDoubleClick);
            this.dataGridViewResult.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewResult_CellValueChanged);
            // 
            // fitem_code
            // 
            this.fitem_code.DataPropertyName = "fitem_code";
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            this.fitem_code.DefaultCellStyle = dataGridViewCellStyle2;
            this.fitem_code.HeaderText = "编码";
            this.fitem_code.Name = "fitem_code";
            this.fitem_code.ReadOnly = true;
            this.fitem_code.Width = 60;
            // 
            // fitem_name
            // 
            this.fitem_name.DataPropertyName = "fitem_name";
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            this.fitem_name.DefaultCellStyle = dataGridViewCellStyle3;
            this.fitem_name.HeaderText = "检验项目";
            this.fitem_name.Name = "fitem_name";
            this.fitem_name.ReadOnly = true;
            this.fitem_name.Width = 120;
            // 
            // fvalue
            // 
            this.fvalue.DataPropertyName = "fvalue";
            this.fvalue.HeaderText = "结果";
            this.fvalue.Name = "fvalue";
            this.fvalue.Width = 70;
            // 
            // fod
            // 
            this.fod.DataPropertyName = "fod";
            this.fod.HeaderText = "OD值";
            this.fod.Name = "fod";
            this.fod.Visible = false;
            this.fod.Width = 65;
            // 
            // fcutoff
            // 
            this.fcutoff.DataPropertyName = "fcutoff";
            this.fcutoff.HeaderText = "Cutoff";
            this.fcutoff.Name = "fcutoff";
            this.fcutoff.Visible = false;
            this.fcutoff.Width = 70;
            // 
            // fitem_badge
            // 
            this.fitem_badge.DataPropertyName = "fitem_badge";
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            this.fitem_badge.DefaultCellStyle = dataGridViewCellStyle4;
            this.fitem_badge.HeaderText = ".";
            this.fitem_badge.Name = "fitem_badge";
            this.fitem_badge.ReadOnly = true;
            this.fitem_badge.Width = 25;
            // 
            // fitem_ref
            // 
            this.fitem_ref.DataPropertyName = "fitem_ref";
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            this.fitem_ref.DefaultCellStyle = dataGridViewCellStyle5;
            this.fitem_ref.HeaderText = "参考值";
            this.fitem_ref.Name = "fitem_ref";
            this.fitem_ref.ReadOnly = true;
            this.fitem_ref.Width = 80;
            // 
            // fitem_unit
            // 
            this.fitem_unit.DataPropertyName = "fitem_unit";
            this.fitem_unit.HeaderText = "项目单位";
            this.fitem_unit.Name = "fitem_unit";
            this.fitem_unit.ReadOnly = true;
            this.fitem_unit.Visible = false;
            // 
            // forder_by
            // 
            this.forder_by.DataPropertyName = "forder_by";
            this.forder_by.HeaderText = "序号";
            this.forder_by.Name = "forder_by";
            this.forder_by.Visible = false;
            this.forder_by.Width = 60;
            // 
            // fitem_id
            // 
            this.fitem_id.DataPropertyName = "fitem_id";
            this.fitem_id.HeaderText = "项目id";
            this.fitem_id.Name = "fitem_id";
            this.fitem_id.ReadOnly = true;
            this.fitem_id.Visible = false;
            // 
            // fresult_id
            // 
            this.fresult_id.DataPropertyName = "fresult_id";
            this.fresult_id.HeaderText = "fresult_id";
            this.fresult_id.Name = "fresult_id";
            this.fresult_id.ReadOnly = true;
            this.fresult_id.Visible = false;
            // 
            // fremark
            // 
            this.fremark.DataPropertyName = "fremark";
            this.fremark.HeaderText = "修改标识";
            this.fremark.Name = "fremark";
            this.fremark.Visible = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.打印ToolStripMenuItem,
            this.toolStripSeparator1,
            this.解除审核ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(139, 82);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(138, 24);
            this.toolStripMenuItem1.Text = "打印预览";
            this.toolStripMenuItem1.Visible = false;
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // 打印ToolStripMenuItem
            // 
            this.打印ToolStripMenuItem.Name = "打印ToolStripMenuItem";
            this.打印ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.打印ToolStripMenuItem.Text = "打印";
            this.打印ToolStripMenuItem.Visible = false;
            this.打印ToolStripMenuItem.Click += new System.EventHandler(this.打印ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(135, 6);
            this.toolStripSeparator1.Visible = false;
            // 
            // 解除审核ToolStripMenuItem
            // 
            this.解除审核ToolStripMenuItem.Name = "解除审核ToolStripMenuItem";
            this.解除审核ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.解除审核ToolStripMenuItem.Text = "解除审核";
            this.解除审核ToolStripMenuItem.Click += new System.EventHandler(this.解除审核ToolStripMenuItem_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabControl1.Location = new System.Drawing.Point(838, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(387, 784);
            this.tabControl1.TabIndex = 135;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView样本);
            this.tabPage1.Controls.Add(this.panel5);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(379, 755);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "列表";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView样本
            // 
            this.dataGridView样本.AllowUserToAddRows = false;
            this.dataGridView样本.AllowUserToDeleteRows = false;
            this.dataGridView样本.AllowUserToResizeRows = false;
            this.dataGridView样本.AutoGenerateColumns = false;
            this.dataGridView样本.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView样本.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView样本.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridView样本.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView样本.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fjy_yb_code,
            this.fhz_name,
            this.fhz_zyh,
            this.fhz_bed,
            this.性别,
            this.fhz_age,
            this.年龄单位,
            this.科室,
            this.样本,
            this.检验医师,
            this.fprint_zt,
            this.fjy_zt,
            this.类型,
            this.fapply_id,
            this.fjy_id});
            this.dataGridView样本.ContextMenuStrip = this.contextMenuStrip1;
            this.dataGridView样本.DataSource = this.bindingSource样本;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView样本.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridView样本.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView样本.Location = new System.Drawing.Point(4, 46);
            this.dataGridView样本.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView样本.MultiSelect = false;
            this.dataGridView样本.Name = "dataGridView样本";
            this.dataGridView样本.ReadOnly = true;
            this.dataGridView样本.RowHeadersVisible = false;
            this.dataGridView样本.RowHeadersWidth = 30;
            this.dataGridView样本.RowTemplate.Height = 23;
            this.dataGridView样本.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView样本.ShowCellToolTips = false;
            this.dataGridView样本.ShowEditingIcon = false;
            this.dataGridView样本.Size = new System.Drawing.Size(371, 705);
            this.dataGridView样本.TabIndex = 133;
            this.dataGridView样本.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dataGridView样本_DataBindingComplete);
            this.dataGridView样本.Click += new System.EventHandler(this.dataGridView样本_Click);
            this.dataGridView样本.DoubleClick += new System.EventHandler(this.dataGridView样本_DoubleClick);
            // 
            // fjy_yb_code
            // 
            this.fjy_yb_code.DataPropertyName = "fjy_yb_code";
            this.fjy_yb_code.HeaderText = "样本号";
            this.fjy_yb_code.Name = "fjy_yb_code";
            this.fjy_yb_code.ReadOnly = true;
            this.fjy_yb_code.Width = 64;
            // 
            // fhz_name
            // 
            this.fhz_name.DataPropertyName = "fhz_name";
            this.fhz_name.HeaderText = "姓名";
            this.fhz_name.Name = "fhz_name";
            this.fhz_name.ReadOnly = true;
            this.fhz_name.Width = 55;
            // 
            // fhz_zyh
            // 
            this.fhz_zyh.DataPropertyName = "fhz_zyh";
            this.fhz_zyh.HeaderText = "住院号";
            this.fhz_zyh.Name = "fhz_zyh";
            this.fhz_zyh.ReadOnly = true;
            this.fhz_zyh.Width = 65;
            // 
            // fhz_bed
            // 
            this.fhz_bed.DataPropertyName = "fhz_bed";
            this.fhz_bed.HeaderText = "床号";
            this.fhz_bed.Name = "fhz_bed";
            this.fhz_bed.ReadOnly = true;
            this.fhz_bed.Width = 55;
            // 
            // 性别
            // 
            this.性别.DataPropertyName = "性别";
            this.性别.HeaderText = "性别";
            this.性别.Name = "性别";
            this.性别.ReadOnly = true;
            this.性别.Width = 55;
            // 
            // fhz_age
            // 
            this.fhz_age.DataPropertyName = "fhz_age";
            this.fhz_age.HeaderText = "年";
            this.fhz_age.Name = "fhz_age";
            this.fhz_age.ReadOnly = true;
            this.fhz_age.Width = 30;
            // 
            // 年龄单位
            // 
            this.年龄单位.DataPropertyName = "年龄单位";
            this.年龄单位.HeaderText = "龄";
            this.年龄单位.Name = "年龄单位";
            this.年龄单位.ReadOnly = true;
            this.年龄单位.Width = 30;
            // 
            // 科室
            // 
            this.科室.DataPropertyName = "科室";
            this.科室.HeaderText = "科室";
            this.科室.Name = "科室";
            this.科室.ReadOnly = true;
            this.科室.Width = 55;
            // 
            // 样本
            // 
            this.样本.DataPropertyName = "样本";
            this.样本.HeaderText = "样本";
            this.样本.Name = "样本";
            this.样本.ReadOnly = true;
            this.样本.Width = 55;
            // 
            // 检验医师
            // 
            this.检验医师.DataPropertyName = "检验医师";
            this.检验医师.HeaderText = "检验";
            this.检验医师.Name = "检验医师";
            this.检验医师.ReadOnly = true;
            this.检验医师.Width = 55;
            // 
            // fprint_zt
            // 
            this.fprint_zt.DataPropertyName = "fprint_zt";
            this.fprint_zt.HeaderText = "打印";
            this.fprint_zt.Name = "fprint_zt";
            this.fprint_zt.ReadOnly = true;
            this.fprint_zt.Width = 55;
            // 
            // fjy_zt
            // 
            this.fjy_zt.DataPropertyName = "fjy_zt";
            this.fjy_zt.HeaderText = "审核";
            this.fjy_zt.Name = "fjy_zt";
            this.fjy_zt.ReadOnly = true;
            this.fjy_zt.Width = 55;
            // 
            // 类型
            // 
            this.类型.DataPropertyName = "类型";
            this.类型.HeaderText = "类型";
            this.类型.Name = "类型";
            this.类型.ReadOnly = true;
            this.类型.Width = 55;
            // 
            // fapply_id
            // 
            this.fapply_id.DataPropertyName = "fapply_id";
            this.fapply_id.HeaderText = "申请号";
            this.fapply_id.Name = "fapply_id";
            this.fapply_id.ReadOnly = true;
            this.fapply_id.Width = 65;
            // 
            // fjy_id
            // 
            this.fjy_id.DataPropertyName = "fjy_id";
            this.fjy_id.HeaderText = "fjy_id";
            this.fjy_id.Name = "fjy_id";
            this.fjy_id.ReadOnly = true;
            this.fjy_id.Visible = false;
            // 
            // bindingSource样本
            // 
            this.bindingSource样本.PositionChanged += new System.EventHandler(this.bindingSource样本_PositionChanged);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.comboBox过滤样本);
            this.panel5.Controls.Add(this.button删除样本);
            this.panel5.Controls.Add(this.button刷新样本);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(4, 4);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(371, 42);
            this.panel5.TabIndex = 0;
            // 
            // comboBox过滤样本
            // 
            this.comboBox过滤样本.DisplayMember = "fname";
            this.comboBox过滤样本.Font = new System.Drawing.Font("宋体", 10.5F);
            this.comboBox过滤样本.FormattingEnabled = true;
            this.comboBox过滤样本.Items.AddRange(new object[] {
            "所有",
            "门诊",
            "住院",
            "急诊",
            "未打印",
            "已打印",
            "未审核",
            "已审核",
            "未打印未审核",
            "未打印已审核",
            "已打印未审核",
            "已打印已审核"});
            this.comboBox过滤样本.Location = new System.Drawing.Point(221, 5);
            this.comboBox过滤样本.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox过滤样本.Name = "comboBox过滤样本";
            this.comboBox过滤样本.Size = new System.Drawing.Size(177, 25);
            this.comboBox过滤样本.TabIndex = 6;
            this.comboBox过滤样本.ValueMember = "finstr_id";
            this.comboBox过滤样本.DropDownClosed += new System.EventHandler(this.comboBox过滤样本_DropDownClosed);
            // 
            // button删除样本
            // 
            this.button删除样本.Location = new System.Drawing.Point(113, 5);
            this.button删除样本.Margin = new System.Windows.Forms.Padding(4);
            this.button删除样本.Name = "button删除样本";
            this.button删除样本.Size = new System.Drawing.Size(100, 29);
            this.button删除样本.TabIndex = 1;
            this.button删除样本.Text = "删除(&&D)";
            this.button删除样本.UseVisualStyleBackColor = true;
            this.button删除样本.Click += new System.EventHandler(this.button删除样本_Click);
            // 
            // button刷新样本
            // 
            this.button刷新样本.Location = new System.Drawing.Point(5, 5);
            this.button刷新样本.Margin = new System.Windows.Forms.Padding(4);
            this.button刷新样本.Name = "button刷新样本";
            this.button刷新样本.Size = new System.Drawing.Size(100, 29);
            this.button刷新样本.TabIndex = 0;
            this.button刷新样本.Text = "刷新(&R)";
            this.button刷新样本.UseVisualStyleBackColor = true;
            this.button刷新样本.Click += new System.EventHandler(this.button刷新样本_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView1);
            this.tabPage2.Controls.Add(this.panel6);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage2.Size = new System.Drawing.Size(379, 755);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "比较";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.项目,
            this.最近,
            this.上一次,
            this.上二次,
            this.上三次,
            this.项目id});
            this.dataGridView1.ContextMenuStrip = this.contextMenuStrip1;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(4, 46);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidth = 30;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.ShowCellToolTips = false;
            this.dataGridView1.ShowEditingIcon = false;
            this.dataGridView1.Size = new System.Drawing.Size(371, 705);
            this.dataGridView1.TabIndex = 134;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            // 
            // 项目
            // 
            this.项目.DataPropertyName = "项目";
            this.项目.HeaderText = "项目";
            this.项目.Name = "项目";
            this.项目.ReadOnly = true;
            this.项目.Width = 80;
            // 
            // 最近
            // 
            this.最近.DataPropertyName = "项目值";
            this.最近.HeaderText = "最近";
            this.最近.Name = "最近";
            this.最近.ReadOnly = true;
            // 
            // 上一次
            // 
            this.上一次.DataPropertyName = "上一次";
            this.上一次.HeaderText = "上一次";
            this.上一次.Name = "上一次";
            this.上一次.ReadOnly = true;
            // 
            // 上二次
            // 
            this.上二次.DataPropertyName = "上二次";
            this.上二次.HeaderText = "上二次";
            this.上二次.Name = "上二次";
            this.上二次.ReadOnly = true;
            // 
            // 上三次
            // 
            this.上三次.DataPropertyName = "上三次";
            this.上三次.HeaderText = "上三次";
            this.上三次.Name = "上三次";
            this.上三次.ReadOnly = true;
            // 
            // 项目id
            // 
            this.项目id.DataPropertyName = "项目id";
            this.项目id.HeaderText = "项目id";
            this.项目id.Name = "项目id";
            this.项目id.ReadOnly = true;
            this.项目id.Visible = false;
            this.项目id.Width = 95;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.button比较);
            this.panel6.Controls.Add(this.label21比较住院号);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(4, 4);
            this.panel6.Margin = new System.Windows.Forms.Padding(4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(371, 42);
            this.panel6.TabIndex = 135;
            // 
            // button比较
            // 
            this.button比较.Location = new System.Drawing.Point(5, 5);
            this.button比较.Margin = new System.Windows.Forms.Padding(4);
            this.button比较.Name = "button比较";
            this.button比较.Size = new System.Drawing.Size(100, 29);
            this.button比较.TabIndex = 3;
            this.button比较.Text = "刷新(&R)";
            this.button比较.UseVisualStyleBackColor = true;
            this.button比较.Click += new System.EventHandler(this.button1_Click);
            // 
            // label21比较住院号
            // 
            this.label21比较住院号.AutoSize = true;
            this.label21比较住院号.Location = new System.Drawing.Point(133, 11);
            this.label21比较住院号.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21比较住院号.Name = "label21比较住院号";
            this.label21比较住院号.Size = new System.Drawing.Size(67, 15);
            this.label21比较住院号.TabIndex = 2;
            this.label21比较住院号.Text = "住院号：";
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(1225, 0);
            this.splitter2.Margin = new System.Windows.Forms.Padding(4);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(4, 784);
            this.splitter2.TabIndex = 136;
            this.splitter2.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.button读卡);
            this.panel4.Controls.Add(this.btnSave);
            this.panel4.Controls.Add(this.button刷新结果);
            this.panel4.Controls.Add(this.button仪器数据);
            this.panel4.Controls.Add(this.button删除);
            this.panel4.Controls.Add(this.button打印);
            this.panel4.Controls.Add(this.button下一个);
            this.panel4.Controls.Add(this.button上一个);
            this.panel4.Controls.Add(this.button审核);
            this.panel4.Controls.Add(this.button新增);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 784);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1229, 42);
            this.panel4.TabIndex = 137;
            // 
            // button读卡
            // 
            this.button读卡.Image = global::ww.form.Properties.Resources.wwNavigatorQuery;
            this.button读卡.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button读卡.Location = new System.Drawing.Point(1081, 4);
            this.button读卡.Margin = new System.Windows.Forms.Padding(4);
            this.button读卡.Name = "button读卡";
            this.button读卡.Size = new System.Drawing.Size(96, 35);
            this.button读卡.TabIndex = 10;
            this.button读卡.Text = " 读电子卡";
            this.button读卡.UseVisualStyleBackColor = true;
            this.button读卡.Visible = false;
            this.button读卡.Click += new System.EventHandler(this.button读卡_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = global::ww.form.Properties.Resources.button_save;
            this.btnSave.Location = new System.Drawing.Point(349, 4);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(83, 35);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "保存";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // button刷新结果
            // 
            this.button刷新结果.Image = global::ww.form.Properties.Resources.refresh;
            this.button刷新结果.Location = new System.Drawing.Point(948, 4);
            this.button刷新结果.Margin = new System.Windows.Forms.Padding(4);
            this.button刷新结果.Name = "button刷新结果";
            this.button刷新结果.Size = new System.Drawing.Size(113, 35);
            this.button刷新结果.TabIndex = 8;
            this.button刷新结果.Text = "刷新结果";
            this.button刷新结果.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button刷新结果.UseVisualStyleBackColor = true;
            this.button刷新结果.Click += new System.EventHandler(this.button刷新结果_Click);
            // 
            // button仪器数据
            // 
            this.button仪器数据.Image = global::ww.form.Properties.Resources.button_tj;
            this.button仪器数据.Location = new System.Drawing.Point(807, 4);
            this.button仪器数据.Margin = new System.Windows.Forms.Padding(4);
            this.button仪器数据.Name = "button仪器数据";
            this.button仪器数据.Size = new System.Drawing.Size(133, 35);
            this.button仪器数据.TabIndex = 7;
            this.button仪器数据.Text = "选择结果(F8)";
            this.button仪器数据.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button仪器数据.UseVisualStyleBackColor = true;
            this.button仪器数据.Click += new System.EventHandler(this.button仪器数据_Click);
            // 
            // button删除
            // 
            this.button删除.Image = global::ww.form.Properties.Resources.button_del;
            this.button删除.Location = new System.Drawing.Point(125, 4);
            this.button删除.Margin = new System.Windows.Forms.Padding(4);
            this.button删除.Name = "button删除";
            this.button删除.Size = new System.Drawing.Size(104, 35);
            this.button删除.TabIndex = 6;
            this.button删除.Text = "删除(F7)";
            this.button删除.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button删除.UseVisualStyleBackColor = true;
            this.button删除.Click += new System.EventHandler(this.button删除_Click);
            // 
            // button打印
            // 
            this.button打印.Image = global::ww.form.Properties.Resources.button_print;
            this.button打印.Location = new System.Drawing.Point(695, 4);
            this.button打印.Margin = new System.Windows.Forms.Padding(4);
            this.button打印.Name = "button打印";
            this.button打印.Size = new System.Drawing.Size(104, 35);
            this.button打印.TabIndex = 5;
            this.button打印.Text = "打印(F2)";
            this.button打印.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button打印.UseVisualStyleBackColor = true;
            this.button打印.Click += new System.EventHandler(this.button打印_Click);
            // 
            // button下一个
            // 
            this.button下一个.Image = global::ww.form.Properties.Resources.button_Next;
            this.button下一个.Location = new System.Drawing.Point(568, 4);
            this.button下一个.Margin = new System.Windows.Forms.Padding(4);
            this.button下一个.Name = "button下一个";
            this.button下一个.Size = new System.Drawing.Size(119, 35);
            this.button下一个.TabIndex = 4;
            this.button下一个.Text = "下一个(F5)";
            this.button下一个.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button下一个.UseVisualStyleBackColor = true;
            this.button下一个.Click += new System.EventHandler(this.button下一个_Click);
            // 
            // button上一个
            // 
            this.button上一个.Image = global::ww.form.Properties.Resources.button_Previous;
            this.button上一个.Location = new System.Drawing.Point(440, 4);
            this.button上一个.Margin = new System.Windows.Forms.Padding(4);
            this.button上一个.Name = "button上一个";
            this.button上一个.Size = new System.Drawing.Size(120, 35);
            this.button上一个.TabIndex = 3;
            this.button上一个.Text = "上一个(F4)";
            this.button上一个.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button上一个.UseVisualStyleBackColor = true;
            this.button上一个.Click += new System.EventHandler(this.button上一个_Click);
            // 
            // button审核
            // 
            this.button审核.Image = global::ww.form.Properties.Resources.button_save;
            this.button审核.Location = new System.Drawing.Point(237, 4);
            this.button审核.Margin = new System.Windows.Forms.Padding(4);
            this.button审核.Name = "button审核";
            this.button审核.Size = new System.Drawing.Size(104, 35);
            this.button审核.TabIndex = 2;
            this.button审核.Text = "审核(F9)";
            this.button审核.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button审核.UseVisualStyleBackColor = true;
            this.button审核.Click += new System.EventHandler(this.button审核_Click);
            // 
            // button新增
            // 
            this.button新增.Image = global::ww.form.Properties.Resources.button_add;
            this.button新增.Location = new System.Drawing.Point(13, 4);
            this.button新增.Margin = new System.Windows.Forms.Padding(4);
            this.button新增.Name = "button新增";
            this.button新增.Size = new System.Drawing.Size(104, 35);
            this.button新增.TabIndex = 0;
            this.button新增.Text = "新增(F6)";
            this.button新增.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button新增.UseVisualStyleBackColor = true;
            this.button新增.Click += new System.EventHandler(this.button新增_Click);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(299, 0);
            this.splitter1.Margin = new System.Windows.Forms.Padding(4);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(4, 784);
            this.splitter1.TabIndex = 138;
            this.splitter1.TabStop = false;
            // 
            // splitter3
            // 
            this.splitter3.Location = new System.Drawing.Point(834, 0);
            this.splitter3.Margin = new System.Windows.Forms.Padding(4);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(4, 784);
            this.splitter3.TabIndex = 139;
            this.splitter3.TabStop = false;
            // 
            // fapply_user_id送检医师cbo
            // 
            this.fapply_user_id送检医师cbo.Location = new System.Drawing.Point(92, 261);
            this.fapply_user_id送检医师cbo.Margin = new System.Windows.Forms.Padding(4);
            this.fapply_user_id送检医师cbo.Name = "fapply_user_id送检医师cbo";
            this.fapply_user_id送检医师cbo.Properties.AutoHeight = false;
            this.fapply_user_id送检医师cbo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.fapply_user_id送检医师cbo.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("用户编码", "医生编码"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("用户名", "医生姓名")});
            this.fapply_user_id送检医师cbo.Properties.DisplayMember = "用户名";
            this.fapply_user_id送检医师cbo.Properties.NullText = "";
            this.fapply_user_id送检医师cbo.Properties.ValueMember = "用户编码";
            this.fapply_user_id送检医师cbo.Size = new System.Drawing.Size(195, 24);
            this.fapply_user_id送检医师cbo.TabIndex = 34;
            this.fapply_user_id送检医师cbo.Click += new System.EventHandler(this.fapply_user_id送检医师cbo_Click);
            this.fapply_user_id送检医师cbo.DoubleClick += new System.EventHandler(this.fapply_user_idtextBox_DoubleClick);
            this.fapply_user_id送检医师cbo.Enter += new System.EventHandler(this.fapply_user_idtextBox_Enter);
            this.fapply_user_id送检医师cbo.Leave += new System.EventHandler(this.fapply_user_idtextBox_Leave);
            // 
            // JYForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1229, 826);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.splitter3);
            this.Controls.Add(this.dataGridViewResult);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel4);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "JYForm";
            this.Text = "检验报告输入";
            this.Load += new System.EventHandler(this.JYForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView样本)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource样本)).EndInit();
            this.panel5.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fapply_user_id送检医师cbo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridViewResult;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.DateTimePicker fjy_date样本日期;
        private System.Windows.Forms.ComboBox fjy_instrComboBox_仪器;
        private System.Windows.Forms.TextBox fjy_yb_code样本;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button新增;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ComboBox comboBox过滤样本;
        private System.Windows.Forms.Button button删除样本;
        private System.Windows.Forms.Button button刷新样本;
        private System.Windows.Forms.DataGridView dataGridView样本;
        private System.Windows.Forms.Button button审核;
        private System.Windows.Forms.Button button上一个;
        private System.Windows.Forms.Button button下一个;
        private System.Windows.Forms.Button button删除;
        private System.Windows.Forms.BindingSource bindingSource样本;
        private System.Windows.Forms.TextBox fhz_zyh住院号;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox fhz_age年龄;
        private System.Windows.Forms.TextBox fhz_name姓名;
        private System.Windows.Forms.ComboBox fhz_dept科室cbo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox fjy_sf_typetextBoxCombox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox fjy_yb_typeComboBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox fapply_idtextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox fhz_bedtextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker fapply_timeDateTimePicker;
        private System.Windows.Forms.DateTimePicker fsampling_timedateTimePicker;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox fjy_user_idtextBoxCbo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker freport_timedateTimePicker;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox fremarktextBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox fchenk_user_idComboBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label fprint_ztlabel;
        private System.Windows.Forms.Label fjy_ztlabel;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label fchenk_user_idlabel;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox fjy_lczdtextBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label状态;
        private System.Windows.Forms.DataGridViewTextBoxColumn fjy_yb_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhz_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhz_zyh;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhz_bed;
        private System.Windows.Forms.DataGridViewTextBoxColumn 性别;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhz_age;
        private System.Windows.Forms.DataGridViewTextBoxColumn 年龄单位;
        private System.Windows.Forms.DataGridViewTextBoxColumn 科室;
        private System.Windows.Forms.DataGridViewTextBoxColumn 样本;
        private System.Windows.Forms.DataGridViewTextBoxColumn 检验医师;
        private System.Windows.Forms.DataGridViewTextBoxColumn fprint_zt;
        private System.Windows.Forms.DataGridViewTextBoxColumn fjy_zt;
        private System.Windows.Forms.DataGridViewTextBoxColumn 类型;
        private System.Windows.Forms.DataGridViewTextBoxColumn fapply_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fjy_id;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 打印ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 解除审核ToolStripMenuItem;
        private System.Windows.Forms.Label fcheck_timelabel;
        private System.Windows.Forms.Button button仪器数据;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label21比较住院号;
        private System.Windows.Forms.Button button比较;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.Button button打印;
        private System.Windows.Forms.Button button刷新结果;
        private System.Windows.Forms.DataGridViewTextBoxColumn 项目;
        private System.Windows.Forms.DataGridViewTextBoxColumn 最近;
        private System.Windows.Forms.DataGridViewTextBoxColumn 上一次;
        private System.Windows.Forms.DataGridViewTextBoxColumn 上二次;
        private System.Windows.Forms.DataGridViewTextBoxColumn 上三次;
        private System.Windows.Forms.DataGridViewTextBoxColumn 项目id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn fvalue;
        private System.Windows.Forms.DataGridViewTextBoxColumn fod;
        private System.Windows.Forms.DataGridViewTextBoxColumn fcutoff;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_badge;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_ref;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn forder_by;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fresult_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fremark;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button button读卡;
        private System.Windows.Forms.ComboBox fhz_sex性别Combo;
        private System.Windows.Forms.ComboBox cboPatientType;
        private System.Windows.Forms.ComboBox comboBoxAgeUnit;
        private System.Windows.Forms.TextBox txt电子健康卡;
        private System.Windows.Forms.TextBox txt身份证号;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private DevExpress.XtraEditors.LookUpEdit fapply_user_id送检医师cbo;
    }
}