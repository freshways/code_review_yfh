namespace ww.form.lis.sam.jy
{
    partial class TJCostForm����
    {
       
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label fnameLabel;
            System.Windows.Forms.Label fsample_codeLabel;
            System.Windows.Forms.Label fhz_type_idLabel;
            System.Windows.Forms.Label fjy_dateLabel;
            System.Windows.Forms.Label finstr_idLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TJCostForm����));
            this.groupBoxif = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.fhz_idtextBox = new System.Windows.Forms.TextBox();
            this.fapply_codetextBox = new System.Windows.Forms.TextBox();
            this.fzyhtextBox = new System.Windows.Forms.TextBox();
            this.fnameTextBox = new System.Windows.Forms.TextBox();
            this.fsample_codeTextBox = new System.Windows.Forms.TextBox();
            this.ftype_idComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.fjy_dateDateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.fjy_dateDateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.fjy_instrComboBox = new System.Windows.Forms.ComboBox();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            label4 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            fnameLabel = new System.Windows.Forms.Label();
            fsample_codeLabel = new System.Windows.Forms.Label();
            fhz_type_idLabel = new System.Windows.Forms.Label();
            fjy_dateLabel = new System.Windows.Forms.Label();
            finstr_idLabel = new System.Windows.Forms.Label();
            this.groupBoxif.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(286, 42);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(47, 12);
            label4.TabIndex = 29;
            label4.Text = "����ID:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(590, 17);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(59, 12);
            label3.TabIndex = 27;
            label3.Text = "���뵥��:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(602, 42);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(47, 12);
            label2.TabIndex = 25;
            label2.Text = "סԺ��:";
            // 
            // fnameLabel
            // 
            fnameLabel.AutoSize = true;
            fnameLabel.Location = new System.Drawing.Point(7, 42);
            fnameLabel.Name = "fnameLabel";
            fnameLabel.Size = new System.Drawing.Size(59, 12);
            fnameLabel.TabIndex = 22;
            fnameLabel.Text = "��������:";
            // 
            // fsample_codeLabel
            // 
            fsample_codeLabel.AutoSize = true;
            fsample_codeLabel.Location = new System.Drawing.Point(443, 42);
            fsample_codeLabel.Name = "fsample_codeLabel";
            fsample_codeLabel.Size = new System.Drawing.Size(47, 12);
            fsample_codeLabel.TabIndex = 21;
            fsample_codeLabel.Text = "������:";
            // 
            // fhz_type_idLabel
            // 
            fhz_type_idLabel.AutoSize = true;
            fhz_type_idLabel.Location = new System.Drawing.Point(431, 17);
            fhz_type_idLabel.Name = "fhz_type_idLabel";
            fhz_type_idLabel.Size = new System.Drawing.Size(59, 12);
            fhz_type_idLabel.TabIndex = 19;
            fhz_type_idLabel.Text = "�������:";
            // 
            // fjy_dateLabel
            // 
            fjy_dateLabel.AutoSize = true;
            fjy_dateLabel.Location = new System.Drawing.Point(7, 17);
            fjy_dateLabel.Name = "fjy_dateLabel";
            fjy_dateLabel.Size = new System.Drawing.Size(59, 12);
            fjy_dateLabel.TabIndex = 12;
            fjy_dateLabel.Text = "��������:";
            // 
            // finstr_idLabel
            // 
            finstr_idLabel.AutoSize = true;
            finstr_idLabel.Location = new System.Drawing.Point(298, 17);
            finstr_idLabel.Name = "finstr_idLabel";
            finstr_idLabel.Size = new System.Drawing.Size(35, 12);
            finstr_idLabel.TabIndex = 10;
            finstr_idLabel.Text = "����:";
            // 
            // groupBoxif
            // 
            this.groupBoxif.Controls.Add(this.button1);
            this.groupBoxif.Controls.Add(label4);
            this.groupBoxif.Controls.Add(this.fhz_idtextBox);
            this.groupBoxif.Controls.Add(label3);
            this.groupBoxif.Controls.Add(this.fapply_codetextBox);
            this.groupBoxif.Controls.Add(label2);
            this.groupBoxif.Controls.Add(this.fzyhtextBox);
            this.groupBoxif.Controls.Add(this.fnameTextBox);
            this.groupBoxif.Controls.Add(fnameLabel);
            this.groupBoxif.Controls.Add(this.fsample_codeTextBox);
            this.groupBoxif.Controls.Add(fsample_codeLabel);
            this.groupBoxif.Controls.Add(this.ftype_idComboBox);
            this.groupBoxif.Controls.Add(fhz_type_idLabel);
            this.groupBoxif.Controls.Add(this.label1);
            this.groupBoxif.Controls.Add(this.fjy_dateDateTimePicker2);
            this.groupBoxif.Controls.Add(this.fjy_dateDateTimePicker1);
            this.groupBoxif.Controls.Add(fjy_dateLabel);
            this.groupBoxif.Controls.Add(this.fjy_instrComboBox);
            this.groupBoxif.Controls.Add(finstr_idLabel);
            this.groupBoxif.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxif.Location = new System.Drawing.Point(0, 0);
            this.groupBoxif.Name = "groupBoxif";
            this.groupBoxif.Size = new System.Drawing.Size(849, 64);
            this.groupBoxif.TabIndex = 146;
            this.groupBoxif.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(745, 37);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 30;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // fhz_idtextBox
            // 
            this.fhz_idtextBox.Location = new System.Drawing.Point(337, 38);
            this.fhz_idtextBox.Name = "fhz_idtextBox";
            this.fhz_idtextBox.Size = new System.Drawing.Size(87, 21);
            this.fhz_idtextBox.TabIndex = 28;
            // 
            // fapply_codetextBox
            // 
            this.fapply_codetextBox.Location = new System.Drawing.Point(652, 13);
            this.fapply_codetextBox.Name = "fapply_codetextBox";
            this.fapply_codetextBox.Size = new System.Drawing.Size(87, 21);
            this.fapply_codetextBox.TabIndex = 26;
            // 
            // fzyhtextBox
            // 
            this.fzyhtextBox.Location = new System.Drawing.Point(652, 38);
            this.fzyhtextBox.Name = "fzyhtextBox";
            this.fzyhtextBox.Size = new System.Drawing.Size(87, 21);
            this.fzyhtextBox.TabIndex = 24;
            this.fzyhtextBox.Text = "12";
            // 
            // fnameTextBox
            // 
            this.fnameTextBox.Location = new System.Drawing.Point(72, 38);
            this.fnameTextBox.Name = "fnameTextBox";
            this.fnameTextBox.Size = new System.Drawing.Size(211, 21);
            this.fnameTextBox.TabIndex = 23;
            // 
            // fsample_codeTextBox
            // 
            this.fsample_codeTextBox.ForeColor = System.Drawing.SystemColors.Desktop;
            this.fsample_codeTextBox.Location = new System.Drawing.Point(493, 38);
            this.fsample_codeTextBox.Name = "fsample_codeTextBox";
            this.fsample_codeTextBox.Size = new System.Drawing.Size(87, 21);
            this.fsample_codeTextBox.TabIndex = 20;
            // 
            // ftype_idComboBox
            // 
            this.ftype_idComboBox.DisplayMember = "fname";
            this.ftype_idComboBox.FormattingEnabled = true;
            this.ftype_idComboBox.Items.AddRange(new object[] {
            ""});
            this.ftype_idComboBox.Location = new System.Drawing.Point(493, 13);
            this.ftype_idComboBox.Name = "ftype_idComboBox";
            this.ftype_idComboBox.Size = new System.Drawing.Size(87, 20);
            this.ftype_idComboBox.TabIndex = 18;
            this.ftype_idComboBox.ValueMember = "fname";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(171, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 12);
            this.label1.TabIndex = 14;
            this.label1.Text = "-";
            // 
            // fjy_dateDateTimePicker2
            // 
            this.fjy_dateDateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker2.Location = new System.Drawing.Point(186, 13);
            this.fjy_dateDateTimePicker2.Name = "fjy_dateDateTimePicker2";
            this.fjy_dateDateTimePicker2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker2.Size = new System.Drawing.Size(97, 21);
            this.fjy_dateDateTimePicker2.TabIndex = 13;
            // 
            // fjy_dateDateTimePicker1
            // 
            this.fjy_dateDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker1.Location = new System.Drawing.Point(72, 13);
            this.fjy_dateDateTimePicker1.Name = "fjy_dateDateTimePicker1";
            this.fjy_dateDateTimePicker1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker1.Size = new System.Drawing.Size(97, 21);
            this.fjy_dateDateTimePicker1.TabIndex = 11;
            // 
            // fjy_instrComboBox
            // 
            this.fjy_instrComboBox.DisplayMember = "ShowName";
            this.fjy_instrComboBox.FormattingEnabled = true;
            this.fjy_instrComboBox.Items.AddRange(new object[] {
            ""});
            this.fjy_instrComboBox.Location = new System.Drawing.Point(337, 13);
            this.fjy_instrComboBox.Name = "fjy_instrComboBox";
            this.fjy_instrComboBox.Size = new System.Drawing.Size(87, 20);
            this.fjy_instrComboBox.TabIndex = 9;
            this.fjy_instrComboBox.ValueMember = "finstr_id";
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportViewer1.Location = new System.Drawing.Point(0, 64);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(849, 442);
            this.reportViewer1.TabIndex = 147;
            // 
            // TJCostForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 506);
            this.Controls.Add(this.reportViewer1);
            this.Controls.Add(this.groupBoxif);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TJCostForm";
            this.ShowIcon = false;
            this.Text = "��ӡԤ��";
            this.Load += new System.EventHandler(this.TJCostForm_Load);
            this.groupBoxif.ResumeLayout(false);
            this.groupBoxif.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxif;
        private System.Windows.Forms.TextBox fhz_idtextBox;
        private System.Windows.Forms.TextBox fapply_codetextBox;
        private System.Windows.Forms.TextBox fzyhtextBox;
        private System.Windows.Forms.TextBox fnameTextBox;
        private System.Windows.Forms.TextBox fsample_codeTextBox;
        private System.Windows.Forms.ComboBox ftype_idComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker2;
        private System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker1;
        private System.Windows.Forms.ComboBox fjy_instrComboBox;
        private System.Windows.Forms.Button button1;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;

    }
}