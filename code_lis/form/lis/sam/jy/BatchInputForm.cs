﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;

namespace ww.form.lis.sam.jy
{
    public partial class BatchInputForm : Form
    {
        private string strInstr仪器ID = null;
        public BatchInputForm(string strid)
        {
            InitializeComponent();
            strInstr仪器ID = strid;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(this.comboBox设备型号.SelectedIndex == -1)
            {
                MessageBox.Show("请选择仪器型号！","提示",MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                textBox提示信息.Text = "请选择仪器型号！";
                return;
            }

            //获取两个样本号，并检查样本号的合法性
            int intBegin样本号 = -1;
            int intEnd样本号 = -1;
            try
            {
                intBegin样本号 = Convert.ToInt32(this.textBoxBegin样本号.Text);
                intEnd样本号 = Convert.ToInt32(this.textBoxEnd样本号.Text);
                if ((intBegin样本号 > 0) && (intEnd样本号 >= intBegin样本号))
                { }
                else
                {
                    throw new Exception("请确保样本号符合下面的规范：\n①样本号大于0；\n②第二个样本号>=第一个样本号。");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("样本号输入异常！\n"+ex.Message, "提示",MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                textBox提示信息.Text = "样本号输入异常！";
                return;
            }

            //获取仪器、日期
            string str仪器ID = this.comboBox设备型号.SelectedValue.ToString();
            string str检验日期 = this.dateTimePicker样本日期.Value.ToString("yyyy-MM-dd");

            //获取输入值，以后需要添加输入合法性检查
            string strValue = this.textBoxValue.Text;

            //取得需要插入值的项目名称
            int selectedItemCount = this.checkedListBoxItems.CheckedItems.Count;
            if(selectedItemCount <= 0)
            {
                MessageBox.Show("请选择需要项目信息！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                textBox提示信息.Text = "请选择需要项目信息！";
                return;
            }

            //this.labelMessage.Text = "正在执行...";
            //this.labelMessage.Visible = true;
            //this.Invoke(new Action(() => { labelMessage.Text = "正在执行..."; labelMessage.Visible = true; }));
            //this.Invoke(new EventHandler(delegate(object sender1, EventArgs e1)
            //{
            //    labelMessage.Text = "正在执行..."; 
            //    labelMessage.Visible = true;
            //}));
            //SetControlStatus(false);

            string[] strItems = new string[selectedItemCount];
            for(int index = 0; index < selectedItemCount; index++)
            {
                strItems[index] = this.checkedListBoxItems.CheckedItems[index].ToString();
            }

            //循环样本插入项目信息
            for(int index样本 = intBegin样本号; index样本<=intEnd样本号; index样本++)
            {
                string strMsg = "";
                InsertDataToLisDB(str检验日期, str仪器ID, index样本, strValue, strItems, out strMsg);
                if(!(string.IsNullOrWhiteSpace(strMsg)))
                {
                    string msg = "为样本号：" + index样本 + " 插入项目信息时出现异常。\n此样本及其后续的样本插入操作均已停止。\n异常信息如下：\n" + strMsg;
                    MessageBox.Show(msg, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    textBox提示信息.Text = "样本：" + index样本 + " 及其之后的样本添加失败！";
                    return;
                }
            }

            textBox提示信息.Text = "样本：" + textBoxBegin样本号.Text + "-" + textBoxEnd样本号.Text + " 项目：" + string.Join(" ", strItems) + " 输入成功!";
            //MessageBox.Show("输入成功。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            //显示成功状态，定时隐藏
            //labelMessage.Text = "输入成功!";
            //labelMessage.Visible = true;
            //int count = 0;
            //System.Windows.Forms.Timer t = new System.Windows.Forms.Timer();
            //t.Interval = 1000;
            //t.Enabled = true;
            //t.Tick += delegate(object sender1, EventArgs e1)
            //{
            //    count++;
            //    if (count == 3)
            //    {
            //        t.Enabled = false;
            //        labelMessage.Text = "状态";
            //        labelMessage.Visible = true;
            //        t.Dispose();
            //        sender1 = null;
            //    }
            //};
            this.ActiveControl = textBoxBegin样本号;
        }

        private void BatchInputForm_Load(object sender, EventArgs e)
        {
            try
            {
                //仪器
                ww.lis.lisbll.sam.InstrBLL bllInstr = new ww.lis.lisbll.sam.InstrBLL();
                DataTable dtInstr = bllInstr.BllInstrDTByUseAndGroupID(1, ww.wwf.wwfbll.LoginBLL.strDeptID);//仪器列表

                this.comboBox设备型号.DataSource = dtInstr;
                if (!(string.IsNullOrWhiteSpace(strInstr仪器ID)))
                {
                    this.comboBox设备型号.SelectedValue = strInstr仪器ID;
                }
                //this.comboBox设备型号.SelectedValue = "cs800b";
                this.ActiveControl = textBoxBegin样本号;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        //向特定日期、特定仪器、特定样本的记录中批量插入项目信息
        private void InsertDataToLisDB(string strDate, string instrId,int int样本号, string itemValue, string[] itemNames, out string strErrorMsg)
        {
            strErrorMsg = null;
            if(itemNames.Length <= 0)
            {
                strErrorMsg = "请选择项目信息！";
                return;
            }
            try
            {
                ArrayList SqlList = new ArrayList();

                //判断SAM_JY结果表是否存在此项记录 主键：日期、仪器、样本号
                ww.lis.lisbll.sam.jybll jybllOption = new ww.lis.lisbll.sam.jybll();
                ww.lis.lisbll.sam.jymodel model = jybllOption.GetModel(strDate, instrId, int样本号);
                if((model !=null) && (model.fjy_zt检验状态.Equals("已审核")))
                {
                    strErrorMsg = "样本："+int样本号.ToString()+" 当期处于“已审核”状态，已经审核通过的项目不允许再修改。";
                    return;
                }
                else if ((model != null) && !(model.fjy_zt检验状态.Equals("已审核")))
                {//SAM_JY检验表中存在样本病人对应记录的话，需要向检验子表SAM_JY_DETAIL表中插入项目信息记录
                    for (int jyresultIndex = 0; jyresultIndex < itemNames.Length; jyresultIndex++)
                    {
                        string strSqlTemp = @"
UPDATE SAM_JY_RESULT set fvalue = '" + itemValue + @"' WHERE [fjy_id]='" + model.fjy_检验id + @"' and fitem_code = '" + itemNames[jyresultIndex] + @"'
IF @@ROWCOUNT= 0
BEGIN
insert into [SAM_JY_RESULT]([fresult_id],[fjy_id],[fapply_id],[fitem_id],[fitem_code],[fitem_name],
            [fitem_unit],[fitem_ref],[fitem_badge],[forder_by],[fvalue],[fod],[fcutoff],[fremark])
values(replace(NEWID(),'-',''),'" + model.fjy_检验id+@"','',
(select top 1 [fitem_id] from SAM_ITEM where [fitem_code] = '" + itemNames[jyresultIndex] + @"' and [finstr_id] = '"+instrId+@"'),
'" +itemNames[jyresultIndex]+@"',
(select top 1 [fname] from SAM_ITEM where [fitem_code] = '" + itemNames[jyresultIndex] + @"' and [finstr_id] = '" + instrId + @"'),
(select top 1 [funit_name] from SAM_ITEM where [fitem_code] = '" + itemNames[jyresultIndex] + @"' and [finstr_id] = '" + instrId + @"'),
(select top 1 [fref] from SAM_ITEM where [fitem_code] = '" + itemNames[jyresultIndex] + @"' and [finstr_id] = '" + instrId + @"'),
dbo.GetArrow('" + itemValue + @"',(select top 1 [fref] from SAM_ITEM where [fitem_code] = '" + itemNames[jyresultIndex] + @"' and [finstr_id] = '" + instrId + @"')),
(select top 1 fprint_num from SAM_ITEM where [fitem_code] = '" + itemNames[jyresultIndex] + @"' and [finstr_id] = '" + instrId + @"'),
'" + itemValue + @"','','','')
end";
                        SqlList.Add(strSqlTemp);
                    }
                }
                //else //SAM_JY检验表中没有样本病人对应记录的话，不向SAM_JY表中插入数据
                //{}
                


                

                ww.lis.lisbll.sam.JYInstrDataBll instrResultBll = new ww.lis.lisbll.sam.JYInstrDataBll();
                string ftaskid = instrResultBll.GetInsResultID(strDate, instrId, int样本号);
                
                if (string.IsNullOrWhiteSpace(ftaskid))
                {
                    //插入样本概要信息
                    //并取得id
                    ftaskid = System.Guid.NewGuid().ToString().Replace("-", "");
                    SqlList.Add("INSERT INTO [Lis_Ins_Result]([FTaskID],[FInstrID],[FResultID],[FValidity],[FDateTime],[FTransFlag],[FCreateTime],sdpljr) VALUES('" + ftaskid + "','" + instrId + "','" + int样本号 + "',1,'" + strDate + "','false',getdate(),'1')");
                }
                for (int index = 0; index < itemNames.Length; index++)
                {
                    string insresultSql = @"
UPDATE Lis_Ins_Result_Detail set FValue = '" + itemValue + @"' WHERE FTaskID='" + ftaskid + @"' and FItem = '" + itemNames[index] + @"'
IF @@ROWCOUNT= 0
BEGIN
insert into Lis_Ins_Result_Detail(FID,FTaskID,FItem,FValue,FTime,[FCreateTime],sdpljr) values(replace(newid(),'-',''), '" + ftaskid + "', '" + itemNames[index] + "', '" + itemValue + "','" + strDate + @"',getdate(),'1')
END";
                    SqlList.Add(insresultSql);
                }
                strErrorMsg = ExecuteInsertOption(SqlList);

                #region 备份处理过程 此部分代码只是用作备份
                //if(string.IsNullOrWhiteSpace(ftaskid))
                //{
                //    //插入样本概要信息
                //    //并取得id
                //    ftaskid = System.Guid.NewGuid().ToString().Replace("-","");
                //    SqlList.Add("INSERT INTO [Lis_Ins_Result]([FTaskID],[FInstrID],[FResultID],[FValidity],[FDateTime],[FTransFlag]) VALUES('"+ftaskid+"','"+instrId+"','"+int样本号+"',1,'"+strDate+"','false')");
                //    for (int index = 0; index < itemNames.Length; index++ )
                //    {
                //        SqlList.Add("insert into Lis_Ins_Result_Detail(FID,FTaskID,FItem,FValue,FTime) values(replace(newid(),'-',''), '" + ftaskid + "', '"+itemNames[index]+"', '"+itemValue+"','"+strDate+"')");
                //    }
                //    ExecuteInsertOption(SqlList);
                //}
                //else
                //{
                //    //数据库已经存在记录的情况下
                //    for (int index = 0; index < itemNames.Length; index++)
                //    {
                //        SqlList.Add("insert into Lis_Ins_Result_Detail(FID,FTaskID,FItem,FValue,FTime) values(replace(newid(),'-',''), '" + ftaskid + "', '" + itemNames[index] + "', '" + itemValue + "','" + strDate + "'");
                //    }
                //    ExecuteInsertOption(SqlList);
                //}
                #endregion
            }
            catch (Exception ex)
            {
                strErrorMsg = ex.Message;
            }
        }

        //执行插入操作
        private string ExecuteInsertOption(IList sqlList)
        {
            string strRet = null;
            string strTemp = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(ww.wwf.wwfbll.WWFInit.strDBConn, sqlList);
            if (strTemp.Trim().Equals("true"))
            {
                strRet = null;
            }
            else
            {
                strRet = strTemp;
            }
            return strRet;
        }


        #region 快捷设置
        /// <summary>
        /// 快捷键 设置
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref   System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                if (this.ActiveControl == textBoxValue)
                {
                    this.btnAdd.PerformClick();
                    return true;
                }
                SendKeys.Send("{TAB}");
                return true;
            }
            return base.ProcessCmdKey(ref   msg, keyData);
        }


        private void SelectAll(object sender, EventArgs e)
        {
            TextBox obj = (TextBox)sender;
            if (!obj.Focused)
                obj.Focus();
            obj.SelectAll();
        }

        #endregion


        private void SetControlStatus(bool status)
        {
            this.btnAdd.Enabled = status;
            this.btnClose.Enabled = status;
            this.comboBox设备型号.Enabled = status;
            this.dateTimePicker样本日期.Enabled = status;
            this.checkedListBoxItems.Enabled = status;
            this.textBoxBegin样本号.Enabled = status;
            this.textBoxEnd样本号.Enabled = status;
            this.textBoxValue.Enabled = status;
        }

        private void SetNotifyMessage(string str)
        {
            this.labelMessage.Text = str;
            this.labelMessage.Visible = true;
        }
    }
}
