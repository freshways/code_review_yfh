﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ww.lis.lisbll.sam;

namespace ww.form.lis.sam.jy
{
    public partial class JYPLDYForm : Form
    {
        jybll blljy = new jybll();
        public JYPLDYForm()
        {
            InitializeComponent();
        }

        private void btn确定_Click(object sender, EventArgs e)
        {
            string strErrorMsg = "";
            string str设备id = this.comboBox设备型号.SelectedValue.ToString();
            string str样本日期 = dateTimePicker样本日期.Value.ToString("yyyy-MM-dd");
            string str开始样本号 = textBoxBegin样本号.Text;
            string str截止样本号 = textBoxEnd样本号.Text;
            int intBeginNo = -1;
            int intEndNo = -1;
            int PrintCount = 0;

            Get样本号范围(str开始样本号, str截止样本号,out intBeginNo, out intEndNo);
            

            DataTable dt = blljy.Get检验ID(str样本日期, str设备id, intBeginNo, intEndNo, out strErrorMsg);
            if(string.IsNullOrWhiteSpace(strErrorMsg))
            {
                if(dt!= null && dt.Rows.Count > 0)
                {
                    Set提示信息("开始打印...");
                    for (int index = 0; index < dt.Rows.Count; index++ )
                    {
                        string strID = dt.Rows[index]["fjy_id"].ToString();
                        if (string.IsNullOrWhiteSpace(strID))
                        {
                            continue;
                        }
                        PrintCount++;
                        Set提示信息("正在打印" + dt.Rows[index]["fjy_yb_code"].ToString() + "号样本...");

                        Print(strID);

                        //add 20150702 wjz 向His回写检验数据  begin --批量打印不用做调整
                        //string str申请单号 = dt.Rows[index]["fapply_id"].ToString();
                        ////string str日期 = dt.Rows[index]["fapply_id"].ToString();
                        //string str设备ID = dt.Rows[index]["fjy_instr"].ToString();
                        //string str样本号 = dt.Rows[index]["fjy_yb_code"].ToString();
                        //this.blljy.WriteDataToHis(str申请单号, str样本日期, str设备ID, str样本号);
                        //add 20150702 wjz 向His回写检验数据 end
                    }
                    Set提示信息("打印成功，共打印"+PrintCount+"个样本");
                }
                else
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowError("没有取到符合条件的样本信息，请确认所要打印的项目是否已经审核。");
                }
            }
            else
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(strErrorMsg);
            }
        }

        private void btn取消_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void JYPLDYForm_Load(object sender, EventArgs e)
        {
            try
            {
                //仪器
                InstrBLL bllInstr = new InstrBLL();
                DataTable dtInstr = bllInstr.BllInstrDTByUseAndGroupID(1, ww.wwf.wwfbll.LoginBLL.strDeptID);//仪器列表

                this.comboBox设备型号.DataSource = dtInstr;

                //20150610 begin  add by wjz 添加本机默认仪器功能
                string strInstrID = ww.form.wwf.Customization.DefaultInstrument.GetInstrID();
                if (!(string.IsNullOrWhiteSpace(strInstrID)))
                {
                    this.comboBox设备型号.SelectedValue = strInstrID;
                }
                //this.comboBox设备型号.Enabled = false; //20150615 wjz 注释掉
                //20150610 end    add by wjz 添加本机默认仪器功能

                this.ActiveControl = textBoxBegin样本号;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void Get样本号范围(string str开始样本号, string str截止样本号, out int intBeginNo, out int intEndNo)
        {
            intBeginNo = -1;
            intEndNo = -1;
            try
            {
                if (string.IsNullOrWhiteSpace(str开始样本号))
                { }
                else
                {
                    intBeginNo = Convert.ToInt32(str开始样本号);
                }
            }
            catch
            {
                intBeginNo = -1;
            }

            try
            {
                if (string.IsNullOrWhiteSpace(str截止样本号))
                { }
                else
                {
                    intEndNo = Convert.ToInt32(str截止样本号);
                }
            }
            catch
            {
                intEndNo = -1;
            }
        }

        private void Print(string strCurrSelfjy_id)
        {
            try
            {
                ww.form.lis.sam.Report.JyPrint pr = new ww.form.lis.sam.Report.JyPrint();
                pr.BllPrintViewer(0, strCurrSelfjy_id);
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void Set提示信息(string strmsg)
        {
            textBox提示信息.Text = strmsg;
            int count = 0;
            System.Windows.Forms.Timer t = new System.Windows.Forms.Timer();
            t.Interval = 5000;
            t.Enabled = true;
            t.Tick += delegate(object sender1, EventArgs e1)
            {
                count++;
                if (count == 3)
                {
                    t.Enabled = false;
                    textBox提示信息.Text = "";
                    t.Dispose();
                    sender1 = null;
                }
            };
        }
        #region 快捷设置
        /// <summary>
        /// 快捷键 设置
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref   System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                if (this.ActiveControl == textBoxEnd样本号)
                {
                    this.btn确定.PerformClick();
                    return true;
                }
                SendKeys.Send("{TAB}");
                return true;
            }
            return base.ProcessCmdKey(ref   msg, keyData);
        }

        private void SelectAll(object sender, EventArgs e)
        {
            TextBox obj = (TextBox)sender;
            if (!obj.Focused)
                obj.Focus();
            obj.SelectAll();
        } 
        #endregion
    }
}
