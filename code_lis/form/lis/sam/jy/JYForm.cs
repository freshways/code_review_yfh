using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;
using ww.lis.lisbll.sam;
using System.Collections;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Net;
using System.IO;
using ww.wwf.com;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using HIS.Model;
using HIS.COMM;
using System.Linq;

namespace ww.form.lis.sam.jy
{
    public partial class JYForm : ww.form.wwf.SysBaseForm
    {
        /// <summary>
        /// 当前选择的样的本id
        /// </summary>
        string strCurrSelfjy_id = "";

        //changed by wjz 20160306 夏蔚医院的住院号是4位数，在这里可能会将其误认为是门诊号，针对夏蔚调整 ▽
        string strHosptialCode = "";
        //changed by wjz 20160306 夏蔚医院的住院号是4位数，在这里可能会将其误认为是门诊号，针对夏蔚调整 △

        jybll blljy = new jybll();
        /// <summary>
        /// 当前样本 主表
        /// </summary>
        jymodel modeljy = new jymodel();
        TypeBLL bllType = new TypeBLL();
        DeptBll bllDept = new DeptBll();
        PersonBLL bllPerson = new PersonBLL();
        DiseaseBLL bllDisease = new DiseaseBLL();
        PatientBLL bllPatient = new PatientBLL();
        SampleTypeBLL bllSampleType = new SampleTypeBLL();
        /// <summary>
        /// 结果
        /// </summary>
        DataTable dtResult = new DataTable();
        DataTable dtItemList = new DataTable();

        //add by wjz 20160216 申请医师、检验医师、核对医师 控件由TextBox替换为ComboBox ▽
        DataTable dtDoctor = null;
        //add by wjz 20160216 申请医师、检验医师、核对医师 控件由TextBox替换为ComboBox △
        private bool print = false;

        List<HIS.Model.pubUser> list医生 = new List<HIS.Model.pubUser>();
        public string S_医院代码
        {
            get;
            set;
        }
        public string S_医院名称
        {
            get;
            set;
        }

        string str仪器结果FTaskID = "";

        public string Get_全局参数(string 参数名称)
        {
            object obj = "";
            try
            {
                obj = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn, "select [参数值] from [dbo].[全局参数] where [参数名称]='" + 参数名称 + "'");
            }
            catch
            {
            }
            return obj.ToString();
        }

        //2019-10-30 yfh 添加，读取电子健康卡
        private ScanerHook listener = new ScanerHook();
        public JYForm()
        {
            InitializeComponent();
            listener.ScanerEvent += Listener_ScanerEvent;
        }
        private void Listener_ScanerEvent(ScanerHook.ScanerCodes codes)
        {
            string code = codes.Result.Replace(';', ':');
            if (!string.IsNullOrEmpty(code))
            {
                ExecuteReq(code);
            }
        }
        void ExecuteReq(string cardno)
        {
            Ylzehc.Ylzehc.ApiUrl = "http://192.168.10.171:1811/ehcService/gateway.do";   // 可按系统配置赋值
            Ylzehc.Ylzehc.AppKey = "1DOB630TT0510100007F0000D6D2E81F";   // 可按系统配置赋值

            // 请求头
            JObject request = new JObject();
            request["app_id"] = "1DOB630TT0500100007F0000CC065635";   // 可按系统配置赋值
            request["term_id"] = "371300210001";   // 可按系统配置赋值
            request["method"] = "ehc.ehealthcode.verify"; // *** 根据具体调用接口入参 *** 
            request["timestamp"] = DateTime.Now.ToString("yyyyMMddHHmmss");   // *** 根据具体调用时间入参 *** 
            request["sign_type"] = "MD5";   // 固定，支持MD5\SM3，SDK内部会自动根据该算法计算sign
            request["version"] = "X.M.0.1";   // 固定
            request["enc_type"] = "AES";    //  固定，支持AES\SM4，SDK内部会自动根据该算法加密

            // 业务参数

            // **********根据接口文档入参，当前仅为二维码验证示例**********//
            JObject bizParam = new JObject();
            bizParam["ehealth_code"] = cardno;//"3C7600201CD41759A266852EB9FF24A5B486290A778E31ED68EA25DEC1D370F4:1::3502A0001:";
            bizParam["out_verify_time"] = DateTime.Now.ToString("yyyyMMddHHmmss");
            bizParam["out_verify_no"] = System.Guid.NewGuid().ToString("N"); // 唯一编号
            bizParam["operator_id"] = "001";
            bizParam["operator_name"] = "测试";
            bizParam["treatment_code"] = "010101";
            // **********根据接口文档入参，当前仅为二维码验证示例**********//

            request["biz_content"] = JsonConvert.SerializeObject(bizParam);

            JObject res = Ylzehc.Ylzehc.Execute(request);
            if (res["ret_code"].ToString().Equals("0000"))
            {
                JObject user = JObject.Parse(res["biz_content"].ToString());
                this.fhz_name姓名.Text = user["user_name"].ToString();
                if (modeljy != null)
                {
                    modeljy.S身份证号 = user["id_no"].ToString();
                    this.txt身份证号.Text = user["id_no"].ToString();
                    modeljy.f1 = user["card_no"].ToString();
                    this.txt电子健康卡.Text = user["card_no"].ToString();
                }
                //this.txt_手机号.Text = user["mobile_phone"].ToString();
                //this.textBoxSFZH.Text = user["id_no"].ToString();
                //MessageBox.Show("电子健康卡读取成功！\n\r[请求参数：]" + bizParam.ToString());
                listener.Stop();
            }
            //Console.WriteLine(JsonConvert.SerializeObject(res));
            //Console.ReadLine();
        }


        bool b扫码计费 = false;
        //获取读卡标志
        private void GetReadFlag()
        {
            //bool readflag = false;
            try
            {
                //object obj = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn,
                //    "select [参数值] from [dbo].[全局参数] where [参数名称]='read'");

                object obj2 = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn,
                     "select [参数值] from [dbo].[全局参数] where [参数名称]='扫码计费'");
                //if (obj.ToString() == "1")
                //{
                //    readflag = true;
                //}
                if (obj2.ToString() == "1")
                {
                    b扫码计费 = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            button读卡.Visible = LoginBLL.sendauto;
        }

        private void JYForm_Load(object sender, EventArgs e)
        {
            using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
            {
                list医生 = chis.pubUsers.Where(c => c.是否有处方权 == true && c.是否禁用 == false).OrderBy(c => c.用户名).ToList();
            }
            //add by wjz 20160902 添加是否读卡的判断▽
            GetReadFlag();
            //add by wjz 20160902 添加是否读卡的判断△
            this.S_医院代码 = this.Get_全局参数("医院代码");
            this.S_医院名称 = this.Get_全局参数("医院名称");

            //changed by wjz 20160306 夏蔚医院的住院号是4位数，在这里可能会将其误认为是门诊号，针对夏蔚调整 ▽
            try
            {
                strHosptialCode = ww.form.Properties.Settings.Default.HisDBName.Trim().ToUpper();
            }
            catch
            {
                strHosptialCode = "";
            }
            //changed by wjz 20160306 夏蔚医院的住院号是4位数，在这里可能会将其误认为是门诊号，针对夏蔚调整 △

            try
            {
                dataGridView样本.AutoGenerateColumns = false;
                dataGridViewResult.AutoGenerateColumns = false;
                dataGridView1.AutoGenerateColumns = false;

                comboBox过滤样本.SelectedItem = "所有";

                //del by wjz 20160308 画面第一次加载时,修改样本号，样本信息不变更 ▽
                //fjy_yb_code样本的Enter事件触发时，tag中会记录当前样本号，此时执行下面语句，tag中记录的样本号为1，而不是对应样本的样本号，这种情况容易引起bug.故将此句下移
                //this.ActiveControl = fjy_yb_code样本;
                //del by wjz 20160308 画面第一次加载时,修改样本号，样本信息不变更 △

                //add by wjz 20160128 性别等控件使用ComboBox控件来实现 ▽
                InitSexComboBox();
                //add by wjz 20160128 性别等控件使用ComboBox控件来实现 △

                //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 ▽
                Init病人类型();
                //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 △

                //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
                Init年龄单位();
                //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △

                //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 ▽
                InitDept();
                //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 △

                //add by wjz 20160216 申请医师、检验医师、核对医师 控件由TextBox替换为ComboBox ▽
                InitDoctorInfo();
                //add by wjz 20160216 申请医师、检验医师、核对医师 控件由TextBox替换为ComboBox △

                //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
                InitApplyUserID();
                //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 △

                //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
                InitWorkUser();
                //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △

                //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
                InitCheckUser();
                //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △

                //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
                InitSampleType();
                //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 △

                //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 ▽
                Init费别控件();
                //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 △

                bllInit初始化();

                //del by wjz 20160128 画面加载时，事件fjy_instrComboBox_仪器_SelectedIndexChanged会被触发3次，先移除此事件，本方法结束时在附加此事件 ▽
                //方法（bllInit初始化）会调用事件（fjy_instrComboBox_仪器_SelectedIndexChanged）
                //fjy_instrComboBox_仪器_SelectedIndexChanged 里又调用了 bllYBList方法，此处的调用重复，故删除
                //bllYBList();
                //del by wjz 20160128 画面加载时，事件fjy_instrComboBox_仪器_SelectedIndexChanged会被触发3次，先移除此事件，本方法结束时在附加此事件 △

                //add by wjz 20160308 画面第一次加载时,修改样本号，样本信息不变更 ▽
                this.ActiveControl = fjy_yb_code样本;
                //add by wjz 20160308 画面第一次加载时,修改样本号，样本信息不变更 △

            }
            catch (Exception ex)
            {

                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }
        #region 比较
        private void bll比较()
        {
            try
            {

                DataTable dtBJValue = new DataTable();

                if (fhz_zyh住院号.Text == "")
                {
                    this.上一次.HeaderText = "上一次";
                    this.上二次.HeaderText = "上二次";
                    this.上三次.HeaderText = "上三次";
                }
                else
                {
                    string strWhere = " (fhz_zyh = '" + fhz_zyh住院号.Text + "') AND (fjy_instr = '" + fjy_instrComboBox_仪器.SelectedValue.ToString() + "') AND (fjy_date <= '" + fjy_date样本日期.Value.ToString("yyyy-MM-dd") + "') ORDER BY fjy_date DESC";
                    DataTable dtBJ = this.blljy.GetList比较(strWhere);
                    int intBJCount = dtBJ.Rows.Count;
                    //MessageBox.Show(intBJCount.ToString());
                    dtBJValue = bll比较值(dtBJ.Rows[0]["fjy_id"].ToString());
                    this.最近.HeaderText = dtBJ.Rows[0]["fjy_date"].ToString() + "[" + dtBJ.Rows[0]["fjy_yb_code"].ToString() + "]";

                    if (intBJCount == 1)
                    {
                        this.上一次.HeaderText = "上一次";
                        this.上二次.HeaderText = "上二次";
                        this.上三次.HeaderText = "上三次";
                    }
                    else if (intBJCount == 2)
                    {
                        this.上一次.HeaderText = dtBJ.Rows[1]["fjy_date"].ToString() + "[" + dtBJ.Rows[1]["fjy_yb_code"].ToString() + "]";
                        this.上二次.HeaderText = "上二次";
                        this.上三次.HeaderText = "上三次";

                        dtBJValue = bll比较设置返回(dtBJValue, dtBJ.Rows[1]["fjy_id"].ToString(), "上一次");

                    }

                    else if (intBJCount == 3)
                    {
                        this.上一次.HeaderText = dtBJ.Rows[1]["fjy_date"].ToString() + "[" + dtBJ.Rows[1]["fjy_yb_code"].ToString() + "]";
                        this.上二次.HeaderText = dtBJ.Rows[2]["fjy_date"].ToString() + "[" + dtBJ.Rows[2]["fjy_yb_code"].ToString() + "]";
                        this.上三次.HeaderText = "上三次";

                        dtBJValue = bll比较设置返回(dtBJValue, dtBJ.Rows[1]["fjy_id"].ToString(), "上一次");
                        dtBJValue = bll比较设置返回(dtBJValue, dtBJ.Rows[2]["fjy_id"].ToString(), "上二次");

                    }
                    else if (intBJCount == 4)
                    {
                        this.上一次.HeaderText = dtBJ.Rows[1]["fjy_date"].ToString() + "[" + dtBJ.Rows[1]["fjy_yb_code"].ToString() + "]";
                        this.上二次.HeaderText = dtBJ.Rows[2]["fjy_date"].ToString() + "[" + dtBJ.Rows[2]["fjy_yb_code"].ToString() + "]";
                        this.上三次.HeaderText = dtBJ.Rows[3]["fjy_date"].ToString() + "[" + dtBJ.Rows[3]["fjy_yb_code"].ToString() + "]";

                        dtBJValue = bll比较设置返回(dtBJValue, dtBJ.Rows[1]["fjy_id"].ToString(), "上一次");
                        dtBJValue = bll比较设置返回(dtBJValue, dtBJ.Rows[2]["fjy_id"].ToString(), "上二次");
                        dtBJValue = bll比较设置返回(dtBJValue, dtBJ.Rows[3]["fjy_id"].ToString(), "上三次");
                    }
                    else
                    {
                        this.上一次.HeaderText = "上一次";
                        this.上二次.HeaderText = "上二次";
                        this.上三次.HeaderText = "上三次";
                    }
                }
                dataGridView1.DataSource = dtBJValue;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }

        }
        private DataTable bll比较设置返回(DataTable dtBJValue, string strfjy_id, string str第几次)
        {
            //   DataTable dt1 = bll比较值(dtBJ.Rows[1]["fjy_id"].ToString());
            DataTable dt1 = bll比较值(strfjy_id);
            for (int i = 0; i < dtBJValue.Rows.Count; i++)
            {
                string stritemid = dtBJValue.Rows[i]["项目id"].ToString();
                DataRow[] rows = dt1.Select("项目id='" + stritemid + "'");
                if (rows.Length > 0)
                {
                    foreach (DataRow dr in rows)
                    {
                        dtBJValue.Rows[i][str第几次] = dr["项目值"].ToString();
                    }

                }
            }
            return dtBJValue;
        }
        private DataTable bll比较值(string str样本id)
        {
            DataTable dt = new DataTable();
            try
            {

                dt = this.blljy.GetList比较值(str样本id);
                // MessageBox.Show(str样本id+dtBJValue.Rows.Count.ToString());
                //  data//GridView1.DataSource = dtBJValue;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
            return dt;
        }
        #endregion
        #region 结果方法
        /// <summary>
        /// 结果列表
        /// </summary>
        private void bllResultList获取检验结果()
        {
            #region 显示结果列表
            string strWhereResult = " (t.fjy_id = '" + modeljy.fjy_检验id + "') ";
            //  MessageBox.Show("显示结果列表");
            dtResult = this.blljy.GetListS(strWhereResult);
            //当单据未审核时，比对结果，没有的进行添加或更新值
            if (fjy_ztlabel.Text == "未审核")//(dtResult == null || dtResult.Rows.Count == 0)
            {
                //if (modeljy.fjy_检验id!=null)
                Get检验结果();
            }
            #endregion

            bllResultShow显示结果颜色();
        }
        /// <summary>
        /// 结果列表显示
        /// </summary>       
        private void bllResultShow显示结果颜色()
        {
            try
            {
                #region 给列设置值
                //2015-05-27 14:47:34 yufh注释：不用每次都计算参考值，保存时计算
                //for (int i = 0; i < dtResult.Rows.Count; i++)
                //{
                //    string strItemId = dtResult.Rows[i]["fitem_id"].ToString();
                //    string strfvalue = dtResult.Rows[i]["fvalue"].ToString();
                //    DataTable dtItem = WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, "SELECT fitem_code,fname,funit_name,fprint_num,forder_by,fref FROM SAM_ITEM WHERE (fitem_id = '" + strItemId + "')");
                //    if (dtItem.Rows.Count > 0)
                //    {

                //        dtResult.Rows[i]["fitem_code"] = dtItem.Rows[0]["fitem_code"];
                //        dtResult.Rows[i]["fitem_name"] = dtItem.Rows[0]["fname"];
                //        dtResult.Rows[i]["fitem_unit"] = dtItem.Rows[0]["funit_name"];
                //        dtResult.Rows[i]["forder_by"] = dtItem.Rows[0]["fprint_num"];

                //        string str参考值 = this.blljy.Bll参考值(strItemId, 0, "", "");
                //        dtResult.Rows[i]["fitem_ref"] = str参考值;
                //        if (strfvalue.Equals("") || strfvalue.Length == 0 || strfvalue == null) { }
                //        else
                //        {
                //            dtResult.Rows[i]["fitem_badge"] = this.blljy.Bll结果标记(strfvalue, str参考值);
                //        }
                //        //fitem_ref
                //    }
                //    else
                //    {
                //        dtResult.Rows[i]["fitem_code"] = "";
                //        dtResult.Rows[i]["fitem_name"] = "";
                //        dtResult.Rows[i]["fitem_unit"] = "";
                //        dtResult.Rows[i]["forder_by"] = "0";
                //        dtResult.Rows[i]["fitem_ref"] = "";
                //        dtResult.Rows[i]["fitem_badge"] = "";

                //    }
                //}
                #endregion
                dataGridViewResult.DataSource = dtResult;

                #region 根据标识显示颜色
                if (this.dataGridViewResult.Rows.Count > 0)
                {
                    for (int intGrid = 0; intGrid < this.dataGridViewResult.Rows.Count; intGrid++)
                    {
                        string strBJ = this.dataGridViewResult.Rows[intGrid].Cells["fitem_badge"].Value.ToString();

                        if (strBJ.Equals("↑"))
                        {
                            this.dataGridViewResult.Rows[intGrid].Cells["fvalue"].Style.ForeColor = Color.Red;
                            this.dataGridViewResult.Rows[intGrid].Cells["fitem_badge"].Style.ForeColor = Color.Red;
                        }
                        else if (strBJ.Equals("↓"))
                        {
                            this.dataGridViewResult.Rows[intGrid].Cells["fvalue"].Style.ForeColor = Color.Blue;
                            this.dataGridViewResult.Rows[intGrid].Cells["fitem_badge"].Style.ForeColor = Color.Blue;
                        }
                        else if (strBJ.Equals("↑↑"))
                        {
                            this.dataGridViewResult.Rows[intGrid].Cells["fitem_ref"].Style.BackColor = Color.Red;
                        }
                        else if (strBJ.Equals("↓↓"))
                        {
                            this.dataGridViewResult.Rows[intGrid].Cells["fitem_ref"].Style.BackColor = Color.Blue;
                        }
                    }
                }
                #endregion
                //  MessageBox.Show("结果风格");
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 保存结果明细 ，先删除，再新增。
        /// </summary>
        /// <param name="lisResultSql"></param>
        /// <param name="str主表id"></param>
        /// <returns></returns>
        private IList bllResultSave保存子表(IList lisResultSql, string str主表id)
        {
            //子表保存             
            this.Validate();
            dataGridViewResult.EndEdit();
            //bllResultShow显示结果();
            lisResultSql.Add(this.blljy.DeleteByJyid(str主表id));
            for (int i = 0; i < dataGridViewResult.Rows.Count; i++)
            {
                string strItemid = "";
                string strItemValue = "";

                if (dataGridViewResult.Rows[i].Cells["fitem_id"] != null)
                {
                    strItemid = dataGridViewResult.Rows[i].Cells["fitem_id"].Value.ToString();
                }
                if (dataGridViewResult.Rows[i].Cells["fvalue"] != null)
                {
                    strItemValue = dataGridViewResult.Rows[i].Cells["fvalue"].Value.ToString();
                }

                //
                //fjx_if fjx_formula fitem_id fitem_code
                string str是否计算 = WWFInit.wwfRemotingDao.DbDTSelect(dtItemList, "fitem_id='" + strItemid + "'", "fjx_if");
                if (str是否计算.Equals("1"))
                {
                    string str公式 = WWFInit.wwfRemotingDao.DbDTSelect(dtItemList, "fitem_id='" + strItemid + "'", "fjx_formula");
                    strItemValue = bll公式计算值(str公式);
                }


                jyresultmodel resultModel = new jyresultmodel();
                //changed by wjz 20151202 根据性别、年龄获取参考值 ▽

                #region old code
                //string str参考值 = this.blljy.Bll参考值(dataGridViewResult.Rows[i].Cells["fitem_id"].Value.ToString(), 0, "", "");
                #endregion

                #region new code
                string str参考值;
                //if (string.IsNullOrWhiteSpace(fhz_age年龄.Text) || string.IsNullOrWhiteSpace(fhz_sex性别.Text)
                //    || string.IsNullOrWhiteSpace(fhz_age_unittextBox.Text)|| (fhz_age_unittextBox.Text == "岁" && fhz_age年龄.Text.Trim()=="0"))
                //{
                //    //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。▽
                //    //str参考值 = this.blljy.Bll参考值(dataGridViewResult.Rows[i].Cells["fitem_id"].Value.ToString(), 0, "", "");
                //    str参考值 = this.blljy.Bll参考值(dataGridViewResult.Rows[i].Cells["fitem_id"].Value.ToString(), -1, "", "");
                //    //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。△
                //}
                //else
                //{
                //    //float age = -1;
                //    //string strsex = "";
                //    //try
                //    //{
                //    //    if (fhz_age_unittextBox.Text.Trim() == "月")
                //    //    {
                //    //        age = Convert.ToInt32(fhz_age年龄.Text.Trim()) / 12.0F;
                //    //    }
                //    //    else if (fhz_age_unittextBox.Text.Trim() == "天")
                //    //    {
                //    //        age = Convert.ToInt32(fhz_age年龄.Text.Trim()) / 365.0F;
                //    //    }
                //    //    else if (fhz_age_unittextBox.Text.Trim() == "岁")
                //    //    {
                //    //        age = Convert.ToInt32(fhz_age年龄.Text.Trim());
                //    //    }

                //    //    strsex = fhz_sex性别.Text.Trim();
                //    //    //str参考值 = this.blljy.Bll参考值New(dataGridViewResult.Rows[i].Cells["fitem_id"].Value.ToString(), age, fhz_sex性别.Text.Trim(), "");
                //    //}
                //    //catch(Exception ex)
                //    //{
                //    //    age = -1;
                //    //    strsex = "";
                //    //    //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。▽
                //    //    //str参考值 = this.blljy.Bll参考值(dataGridViewResult.Rows[i].Cells["fitem_id"].Value.ToString(), 0, "", "");
                //    //    //str参考值 = this.blljy.Bll参考值(dataGridViewResult.Rows[i].Cells["fitem_id"].Value.ToString(), -1, "", "");
                //    //    //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。△
                //    //}

                //    //str参考值 = this.blljy.Bll参考值New(dataGridViewResult.Rows[i].Cells["fitem_id"].Value.ToString(), age, strsex, "");

                //    str参考值 = GetRefValueByItemCode(dataGridViewResult.Rows[i].Cells["fitem_id"].Value.ToString());
                //}
                str参考值 = GetRefValueByItemCode(dataGridViewResult.Rows[i].Cells["fitem_id"].Value.ToString());
                #endregion
                //changed by wjz 20151202 根据性别、年龄获取参考值 △
                if (str参考值.Equals("") || str参考值.Length == 0 || str参考值 == null) { }
                else
                {
                    resultModel.fitem_badge = this.blljy.Bll结果标记(strItemValue, str参考值);
                }
                resultModel.fresult_id = this.blljy.DbGuid();
                resultModel.fjy_id = str主表id;
                resultModel.fitem_id = dataGridViewResult.Rows[i].Cells["fitem_id"].Value.ToString();
                resultModel.fitem_code = dataGridViewResult.Rows[i].Cells["fitem_code"].Value.ToString();
                resultModel.fitem_name = dataGridViewResult.Rows[i].Cells["fitem_name"].Value.ToString();
                resultModel.fitem_unit = dataGridViewResult.Rows[i].Cells["fitem_unit"].Value.ToString();
                resultModel.fitem_ref = dataGridViewResult.Rows[i].Cells["fitem_ref"].Value.ToString();
                //resultModel.fitem_badge = dataGridViewResult.Rows[i].Cells["fitem_badge"].Value.ToString();
                resultModel.fvalue = strItemValue;
                resultModel.fod = dataGridViewResult.Rows[i].Cells["fod"].Value.ToString();
                resultModel.fcutoff = dataGridViewResult.Rows[i].Cells["fcutoff"].Value.ToString();
                resultModel.forder_by = dataGridViewResult.Rows[i].Cells["forder_by"].Value.ToString();
                resultModel.fremark = dataGridViewResult.Rows[i].Cells["fremark"].Value.ToString();
                lisResultSql.Add(this.blljy.AddS(resultModel));
            }
            return lisResultSql;
        }

        //add by wjz 根据datatable中的数据进行计算 ▽
        private string bll公式计算值ByDataTable(string str公式, DataTable dtable)
        {
            string strR = "";
            try
            {
                string strgs_新 = str公式;
                for (int i = 0; i < dtable.Rows.Count; i++)
                {
                    string strItemCode = "";
                    string strItemValue = "";
                    if (dtable.Rows[i]["fitem_code"] != null)
                    {
                        strItemCode = "[" + dtable.Rows[i]["fitem_code"].ToString() + "]";
                    }
                    if (dtable.Rows[i]["fvalue"] != null)
                    {
                        strItemValue = dtable.Rows[i]["fvalue"].ToString();
                    }
                    strgs_新 = WWFInit.wwfRemotingDao.DbIndexOfStrRep(strgs_新, strItemCode, strItemValue);
                }
                strgs_新 = strgs_新.Replace("[", "").Trim();
                strgs_新 = strgs_新.Replace("]", "").Trim();

                strR = ww.wwf.wwfbll.FormulaGenerator.EvaluationResult(strgs_新);
                try
                {
                    double dou1 = Convert.ToDouble(strR.ToString());
                    strR = Math.Round(dou1, 2, MidpointRounding.AwayFromZero).ToString();
                }
                catch
                {
                    strR = "";
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowWarning(ex.Message.ToString());
            }
            return strR;
        }
        //add by wjz 根据datatable中的数据进行计算 △

        private string bll公式计算值(string str公式)
        {
            string strR = "";
            try
            {
                this.Validate();
                dataGridViewResult.EndEdit();
                string strgs_新 = str公式;
                for (int i = 0; i < dataGridViewResult.Rows.Count; i++)
                {
                    string strItemCode = "";
                    string strItemValue = "";
                    if (dataGridViewResult.Rows[i].Cells["fitem_code"] != null)
                    {
                        strItemCode = "[" + dataGridViewResult.Rows[i].Cells["fitem_code"].Value.ToString() + "]";
                    }
                    if (dataGridViewResult.Rows[i].Cells["fvalue"] != null)
                    {
                        strItemValue = dataGridViewResult.Rows[i].Cells["fvalue"].Value.ToString();
                    }
                    strgs_新 = WWFInit.wwfRemotingDao.DbIndexOfStrRep(strgs_新, strItemCode, strItemValue);
                }
                strgs_新 = strgs_新.Replace("[", "").Trim();
                strgs_新 = strgs_新.Replace("]", "").Trim();

                strR = ww.wwf.wwfbll.FormulaGenerator.EvaluationResult(strgs_新);
                try
                {
                    double dou1 = Convert.ToDouble(strR.ToString());
                    strR = Math.Round(dou1, 2, MidpointRounding.AwayFromZero).ToString();
                }
                catch
                {
                    strR = "";
                }
                //MessageBox.Show("公式：" + strgs_新 + " 计算结果：" + strR);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowWarning(ex.Message.ToString());
            }
            return strR;
        }

        /// <summary>
        /// 取得当前仪器项目列表
        /// </summary>
        private void BllItemList仪器项目列表()
        {
            try
            {
                string strInstrId = this.fjy_instrComboBox_仪器.SelectedValue.ToString();
                string sql = "SELECT * FROM sam_item t where finstr_id='" + strInstrId + "' order by fprint_num";
                dtItemList = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql).Tables[0];
                //dataGridView2.DataSource = dtItemList;
                // MessageBox.Show("到此了。");
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }




        /// <summary>
        /// 新增结果
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button新增_Click(object sender, EventArgs e)
        {
            //20160612 wjz 已经审核通过的项目不允许再添加新项目
            if (modeljy.fjy_zt检验状态.Equals("已审核"))
            {
                MessageBox.Show("当期项目已经审核通过，若要修改此样本的检验项目，请先解除审核。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            try
            {
                using (ww.form.lis.com.JyItemSelForm jsf = new ww.form.lis.com.JyItemSelForm())
                {
                    jsf.strfinstr_id = this.fjy_instrComboBox_仪器.SelectedValue.ToString();
                    jsf.strfinstr_name = this.fjy_instrComboBox_仪器.SelectedText;

                    //add by wjz 20151122 在日期和样本号不为空的情况下，只显示列表中没有的化验项目▽
                    jsf.strDate = fjy_date样本日期.Value.ToString("yyyy-MM-dd");
                    try
                    {
                        jsf.strSampleNo = Convert.ToInt64(fjy_yb_code样本.Text).ToString();
                    }
                    catch
                    {
                        jsf.strSampleNo = "";
                    }
                    //add by wjz 20151122 在日期和样本号不为空的情况下，只显示列表中没有的化验项目△

                    jsf.ShowDialog();
                    IList lisSel = jsf.selItemList;
                    if (lisSel != null)
                    {
                        for (int i = 0; i < lisSel.Count; i++)
                        {
                            DataRow[] rows = dtResult.Select("fitem_id='" + lisSel[i].ToString() + "'");

                            if (rows.Length > 0) { }
                            else
                            {
                                //richTextBox2.AppendText(lisSel[i].ToString() + "\r");
                                DataRow drNew = this.dtResult.NewRow();
                                drNew["fresult_id"] = this.blljy.DbGuid();
                                drNew["fitem_id"] = lisSel[i].ToString();
                                DataTable dtItem = WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn,
                                    "SELECT fitem_code,fname,funit_name,fprint_num,forder_by,fref FROM SAM_ITEM WHERE (fitem_id = '" + lisSel[i].ToString() + "')");
                                if (dtItem.Rows.Count > 0)
                                {
                                    drNew["fitem_code"] = dtItem.Rows[0]["fitem_code"];
                                    drNew["fitem_name"] = dtItem.Rows[0]["fname"];
                                    drNew["fitem_unit"] = dtItem.Rows[0]["funit_name"];
                                    drNew["forder_by"] = dtItem.Rows[0]["fprint_num"];

                                    //changed by wjz 20151202 根据性别、年龄获取参考值 ▽

                                    #region old code
                                    //string str参考值 = this.blljy.Bll参考值(lisSel[i].ToString(), 0, "", "");
                                    #endregion

                                    #region new code
                                    string str参考值;
                                    //if (string.IsNullOrWhiteSpace(fhz_age年龄.Text) || string.IsNullOrWhiteSpace(fhz_sex性别.Text) 
                                    //    ||string.IsNullOrWhiteSpace(fhz_age_unittextBox.Text) || (fhz_age_unittextBox.Text.Trim() == "岁" && fhz_age年龄.Text.Trim() == "0"))
                                    //{
                                    //    //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。▽
                                    //    //str参考值 = this.blljy.Bll参考值(lisSel[i].ToString(), 0, "", "");
                                    //    str参考值 = this.blljy.Bll参考值(lisSel[i].ToString(), -1, "", "");
                                    //    //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。△
                                    //}
                                    //else
                                    //{
                                    //    //float age = -1;
                                    //    //string strsex = "";
                                    //    //try
                                    //    //{
                                    //    //    //age = Convert.ToInt32(fhz_age年龄.Text.Trim());
                                    //    //    //str参考值 = this.blljy.Bll参考值New(lisSel[i].ToString(), age, fhz_sex性别.Text.Trim(), "");
                                    //    //    if (fhz_age_unittextBox.Text.Trim() == "月")
                                    //    //    {
                                    //    //        age = Convert.ToInt32(fhz_age年龄.Text.Trim()) / 12.0F;
                                    //    //    }
                                    //    //    else if (fhz_age_unittextBox.Text.Trim() == "天")
                                    //    //    {
                                    //    //        age = Convert.ToInt32(fhz_age年龄.Text.Trim()) / 365.0F;
                                    //    //    }
                                    //    //    else if (fhz_age_unittextBox.Text.Trim() == "岁")
                                    //    //    {
                                    //    //        age = Convert.ToInt32(fhz_age年龄.Text.Trim());
                                    //    //    }

                                    //    //    strsex = fhz_sex性别.Text.Trim();
                                    //    //}
                                    //    //catch (Exception ex)
                                    //    //{
                                    //    //    age = -1;
                                    //    //    strsex = "";
                                    //    //    ////changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。▽
                                    //    //    ////str参考值 = this.blljy.Bll参考值(lisSel[i].ToString(), 0, "", "");
                                    //    //    //str参考值 = this.blljy.Bll参考值(lisSel[i].ToString(), -1, "", "");
                                    //    //    ////changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。△
                                    //    //}

                                    //    //str参考值 = this.blljy.Bll参考值New(lisSel[i].ToString(), age, strsex, "");
                                    //    str参考值 = GetRefValueByItemCode(lisSel[i].ToString());
                                    //}
                                    str参考值 = GetRefValueByItemCode(lisSel[i].ToString());
                                    #endregion
                                    //changed by wjz 20151202 根据性别、年龄获取参考值 △


                                    drNew["fitem_ref"] = str参考值;

                                    //add by wjz 20151122 项目添加后，值设定为默认值 ▽
                                    if (str参考值.Contains("--") && Regex.IsMatch(str参考值, @"\d+"))
                                    { }
                                    else
                                    {
                                        drNew["fvalue"] = str参考值;
                                    }
                                    //add by wjz 20151122 项目添加后，值设定为默认值 △

                                }
                                this.dtResult.Rows.Add(drNew);
                            }
                        }
                    }
                    //bllResultShow显示结果();
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        /// <summary>
        ///  删除结果
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button删除_Click(object sender, EventArgs e)
        {
            if (label状态.Visible)
            {
                WWMessage.MessageShowWarning("当期样本已经审核，若要编辑此样本的项目信息，请先解除审核。");
                return;
            }
            try
            {
                if (WWMessage.MessageDialogResult("你确认删除当前项目？"))
                {
                    this.Validate();
                    dataGridViewResult.EndEdit();
                    if (dtResult.Rows.Count > 0)
                    {
                        if (dataGridViewResult.Rows.Count > 0)
                        {
                            // string strid = this.dataGridViewResult.CurrentRow.Cells["fresult_id"].Value.ToString();
                            int intHH = this.dataGridViewResult.CurrentRow.Index;
                            //MessageBox.Show(intHH.ToString());
                            dtResult.Rows[intHH].Delete();
                            dtResult.AcceptChanges();
                        }
                    }
                    //MessageBox.Show("明细数："+dtResult.Rows.Count.ToString());
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        #endregion



        #region 主、结果 保存、删除

        // int int样本号 = 1;
        /// <summary>
        /// 保存样本
        /// </summary>
        private void billSave保存样本(int intMoveType, string strSelRelt)
        {
            try
            {
                //  richTextBox1.Clear();
                bool addbool = false;

                if (fjy_yb_code样本.Text.Equals("") || fjy_yb_code样本.Text.Length == 0) //|| fhz_name姓名.Text.Length == 0 || fhz_name姓名.Text.Equals("")
                { }
                else
                {
                    if (strSelRelt.Equals("选择结果")) { }
                    else
                    {
                        //bll仪器取数(0); //2015-05-27 14:04:42 yufh注释：取数方法重定义
                    }

                    int int样本号 = Convert.ToInt32(fjy_yb_code样本.Text);
                    fjy_yb_code样本.Text = str取得样本号(int样本号);

                    #region 判断病人信息、化验结果是否为空

                    DataTable dttempdetail = dataGridViewResult.DataSource as DataTable;
                    //if (string.IsNullOrWhiteSpace(modeljy.fhz_住院号) && string.IsNullOrWhiteSpace(modeljy.fhz_姓名)
                    //    && string.IsNullOrWhiteSpace(modeljy.fapply_申请单ID) && (dttempdetail == null || dttempdetail.Rows.Count == 0))
                    if (string.IsNullOrWhiteSpace(this.fapply_idtextBox.Text.Trim()) && string.IsNullOrWhiteSpace(this.fhz_name姓名.Text.Trim())
                        && string.IsNullOrWhiteSpace(this.fhz_zyh住院号.Text.Trim()) && (dttempdetail == null || dttempdetail.Rows.Count == 0))
                    {
                        #region 变更样本号
                        try
                        {
                            if (intMoveType == 1)
                            {
                                int样本号 = Convert.ToInt32(fjy_yb_code样本.Text) + 1;
                            }
                            //2015-06-09 21;38 begin  wjz 修改目的：点击“打印”按钮后，先执行审批操作，在执行打印操作。
                            else if (intMoveType == 2)
                            {
                                int样本号 = Convert.ToInt32(fjy_yb_code样本.Text);
                            }
                            //2015-06-09 21;38 end  wjz 修改目的：点击“打印”按钮后，先执行审批操作，在执行打印操作。
                            else
                            {
                                if (Convert.ToInt32(fjy_yb_code样本.Text) > 1)
                                {
                                    int样本号 = Convert.ToInt32(fjy_yb_code样本.Text) - 1;
                                }
                            }

                        }
                        catch { }
                        #endregion

                        fjy_yb_code样本.Text = str取得样本号(int样本号);
                        this.ActiveControl = fjy_yb_code样本;

                        bllYBList();//billSave保存样本，间接调用：btnSave_Click(保存)，button审核_Click，button打印_Click，button下一个_Click，button上一个_Click，ProcessCmdKey，SaveSampleInfo未用，toolStripMenuItem1_Click(右键打印预览-已隐藏)，打印ToolStripMenuItem_Click(右键打印-已隐藏)
                        //bllYBListByYbCode();

                        return;
                    }
                    #endregion

                    IList lisSql = new ArrayList();

                    #region 主表赋值
                    modeljy.fhz_住院号 = this.fhz_zyh住院号.Text;
                    modeljy.fhz_姓名 = this.fhz_name姓名.Text;
                    if (this.fhz_age年龄.Text == "" || this.fhz_age年龄.Text.Length == 0)
                    {
                        this.fhz_age年龄.Text = "0";
                    }
                    modeljy.fhz_年龄 = Convert.ToDecimal(this.fhz_age年龄.Text);
                    modeljy.fhz_床号 = this.fhz_bedtextBox.Text;
                    modeljy.fapply_申请单ID = this.fapply_idtextBox.Text;
                    modeljy.fapply_申请时间 = this.fapply_timeDateTimePicker.Value.ToString("yyyy-MM-dd HH:mm:ss");
                    modeljy.fsampling_采样时间 = this.fsampling_timedateTimePicker.Value.ToString("yyyy-MM-dd HH:mm:ss");
                    modeljy.freport_报告时间 = this.freport_timedateTimePicker.Value.ToString("yyyy-MM-dd HH:mm:ss");
                    modeljy.fremark_备注 = this.fremarktextBox.Text;
                    //页面添加身份证和电子卡号控件，用页面覆盖model
                    modeljy.S身份证号 = this.txt身份证号.Text.Trim();
                    modeljy.f1 = this.txt电子健康卡.Text.Trim();

                    //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
                    //modeljy.fhz_生日 = this.bllPatient.BllBirthdayByAge(modeljy.fhz_年龄, fhz_age_unittextBox.Text);
                    modeljy.fhz_生日 = this.bllPatient.BllBirthdayByAge(modeljy.fhz_年龄, this.comboBoxAgeUnit.SelectedValue.ToString());
                    //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △

                    modeljy.fsys_用户ID = LoginBLL.strPersonID;
                    modeljy.fsys_部门ID = LoginBLL.strDeptID;
                    modeljy.fsys_创建时间 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    modeljy.finstr_result_id = this.str仪器结果FTaskID;

                    //if (dataGridViewResult.Rows.Count > 0 && strSelRelt.Equals("审核"))
                    //{
                    //    modeljy.fjy_zt检验状态 = "已审核";//如果有结果明细时，保存就把主表设置为已经审核。
                    //    modeljy.fchenk_审核医师ID = LoginBLL.strPersonID;
                    //    modeljy.fcheck_审核时间 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    //}

                    if (dataGridViewResult.Rows.Count > 0 && strSelRelt.Equals("审核"))
                    {
                        modeljy.fjy_zt检验状态 = "已审核";//如果有结果明细时，保存就把主表设置为已经审核。

                        if (string.IsNullOrWhiteSpace(LoginBLL.strCheckID))
                        {
                            modeljy.fchenk_审核医师ID = LoginBLL.strPersonID;
                            //modeljy.Checker审核者 = LoginBLL.strPersonName;
                        }
                        else
                        {
                            modeljy.fchenk_审核医师ID = LoginBLL.strCheckID;
                            //modeljy.Checker审核者 = LoginBLL.strCheckName;
                        }

                        modeljy.fcheck_审核时间 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    }

                    #endregion

                    //取出检验id判断是否新增/修改
                    string strSql = "SELECT fjy_id,fjy_zt FROM SAM_JY where fjy_date='" + fjy_date样本日期.Value.ToString("yyyy-MM-dd") + "' and fjy_instr='" + fjy_instrComboBox_仪器.SelectedValue.ToString() + "' and fjy_yb_code='" + fjy_yb_code样本.Text + "' ";
                    DataTable dtOK = WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, strSql);
                    string strValue = "";
                    string strValueZT = "";
                    if (dtOK.Rows.Count > 0)
                    {
                        strValue = dtOK.Rows[0]["fjy_id"].ToString();
                        strValueZT = dtOK.Rows[0]["fjy_zt"].ToString();
                    }
                    if (Convert.ToString(modeljy.fhz_id) == "")
                        modeljy.fhz_id = this.blljy.DbGuid();//于凤海2015-06-01 20:36:51修改患者id为guid  fhz_zyh住院号.Text;
                    #region 新增/修改
                    if (strValue.Equals("") || strValue.Length == 0)
                    {//新增
                        modeljy.fjy_检验id = this.blljy.DbGuid();
                        //子-s                       
                        lisSql = bllResultSave保存子表(lisSql, modeljy.fjy_检验id);
                        //主                     
                        lisSql.Add(this.blljy.AddSql(this.modeljy));
                        addbool = true;
                    }
                    else
                    {//修改
                        if (strValueZT.Equals("未审核"))
                        {
                            modeljy.fjy_检验id = strValue;
                            string strUpdateSql = this.blljy.UpdateSql(this.modeljy);
                            lisSql.Add(strUpdateSql);
                            //子-s                       
                            lisSql = bllResultSave保存子表(lisSql, modeljy.fjy_检验id);
                        }
                    }
                    //保存患者信息 于凤海2015-06-01 20:36:51修改以患者id为主（调整保存方法） 
                    //不管是 新增还是修改都在最后执行sql语句
                    //不管是新增还是保存都保存病人信息
                    //changed by wjz 20160121 当病人住院号是空的，或病人的身份证号是空的，则不保存病人信息 ▽
                    if (string.IsNullOrWhiteSpace(this.modeljy.S身份证号) || string.IsNullOrWhiteSpace(this.modeljy.fhz_住院号))
                    { }
                    else
                    {
                        if (strSelRelt.Equals("审核"))
                        {
                            string strPatSql = this.blljy.AddSqlPatient(this.modeljy);
                            if (strPatSql == "" || strPatSql.Length == 0) { }
                            else
                            {
                                lisSql.Add(strPatSql);
                            }
                        }
                    }
                    //changed by wjz 20160121 当病人住院号是空的，或病人的身份证号是空的，则不保存病人信息 △
                    string strRRR = WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, lisSql);
                    if (strRRR == "true" && (strSelRelt == "审核" || this.print))
                    {
                        try
                        {
                            //微信推送:这个地方做个说明吧，接口地址是陈忠华、东辉他们弄的，我负责调用
                            //原来代码丢了，现在找回来一版，能推送成功，yfh 371122201403077416 这个测试身份证
                            string text5 = "http://192.168.10.118/weixintuisongController.do?mycheckts&idcard={0}&fjyid={1}&rq={2}&mc={3}&flag=1";
                            if (!string.IsNullOrEmpty(this.S_医院代码))
                            {
                                text5 = string.Format(text5, new object[]
                                {
                                    this.modeljy.S身份证号,
                                    this.S_医院代码 + "_" + this.modeljy.fjy_检验id,
                                    this.modeljy.fjy_检验日期,
                                    this.S_医院名称
                                });
                                string text6 = JYForm.SendRequest(text5, Encoding.UTF8);
                            }
                        }
                        catch
                        {
                        }
                        //电子健康卡推送消息
                        if (LoginBLL.sendauto)
                        {
                            #region 电子健康卡推送消息
                            //患者信息
                            JObject user = new JObject();
                            user["Name"] = modeljy.fhz_姓名;
                            user["bcard"] = modeljy.f1;
                            user["reportid"] = modeljy.fjy_检验id;
                            Common.SendEcardMessage(user);
                            #endregion
                        }
                    }
                    //add wjz 从LIS中取出已经审核过的数据，并将其写入HIS ==============================================
                    #region 20150701 wjz 审核时向His写入检验数据 
                    if (strRRR.Equals("true") && strSelRelt.Equals("审核"))
                    {
                        //从LIS中取出已经审核过的数据，并将其写入HIS，只有申请单号不为空时才允许回写数据
                        //DataTable dt审核 = jybll接口数据读取.BllResultDT(modeljy.fjy_检验id);
                        string str日期 = fjy_date样本日期.Value.ToString("yyyy-MM-dd");

                        //changed 20150814 wjz 以下两行
                        //bool b是否回写 = ww.form.Properties.Settings.Default.WriteJYDataToHis;
                        //this.blljy.WriteDataToHis(modeljy.fapply_申请单ID, str日期, fjy_instrComboBox_仪器.SelectedValue.ToString(), fjy_yb_code样本.Text,b是否回写);

                        //changed by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 ▽
                        //this.blljy.WriteDataToHis(modeljy.fapply_申请单ID, str日期, fjy_instrComboBox_仪器.SelectedValue.ToString(), fjy_yb_code样本.Text);
                        this.blljy.WriteDataToHisBy门诊住院号(modeljy.fhz_住院号, str日期, fjy_instrComboBox_仪器.SelectedValue.ToString(), fjy_yb_code样本.Text);
                        //changed by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 △
                    }
                    #endregion 20150701 wjz 审核时向His写入检验数据
                    //=================================================================================================


                    if (strRRR == "true" && addbool)
                    {
                        ////添加新行
                        //DataTable dt = (DataTable)bindingSource样本.DataSource;
                        //DataRow dr = dt.NewRow();
                        //dr["fjy_yb_code"] = fjy_yb_code样本.Text;
                        //dr["fhz_name"] = modeljy.fhz_姓名;
                        //dr["fhz_zyh"] = modeljy.fhz_住院号;
                        //dr["fhz_bed"] = modeljy.fhz_床号;
                        //dr["性别"] = fhz_sex性别.Text;
                        //dr["fhz_age"] = modeljy.fhz_年龄;
                        //dr["年龄单位"] = fhz_age_unittextBox.Text;
                        //dr["科室"] = fhz_dept科室.Text;
                        //dr["样本"] = fjy_yb_typetextBox.Text;
                        //dr["检验医师"] = fjy_user_idtextBox.Text;
                        //dr["fprint_zt"] = "未打印";
                        //dr["fjy_zt"] = "未审核";
                        //dr["类型"] = fhz_type_idtextBox_name.Text;
                        //dr["fapply_id"] = modeljy.fapply_申请单ID;
                        //dr["fjy_id"] = modeljy.fjy_检验id;
                        //dt.Rows.Add(dr);
                        //bindingSource样本.DataSource = dt;
                        //bindingSource样本.MoveLast();
                    }
                    #endregion


                    try
                    {
                        if (intMoveType == 1)
                        {
                            int样本号 = Convert.ToInt32(fjy_yb_code样本.Text) + 1;
                        }
                        //2015-06-09 21;38 begin  wjz 修改目的：点击“打印”按钮后，先执行审批操作，在执行打印操作。
                        else if (intMoveType == 2)
                        {
                            int样本号 = Convert.ToInt32(fjy_yb_code样本.Text);
                        }
                        //2015-06-09 21;38 end  wjz 修改目的：点击“打印”按钮后，先执行审批操作，在执行打印操作。
                        else
                        {
                            if (Convert.ToInt32(fjy_yb_code样本.Text) > 1)
                            {
                                int样本号 = Convert.ToInt32(fjy_yb_code样本.Text) - 1;
                            }
                        }

                    }
                    catch { }

                    //20160618 wjz begin 添加 目的：焦点位于备注，回车显示先一个样本信息
                    if (intMoveType == 1 && strSelRelt == "保存")
                    {
                        fjy_yb_code样本.Text = str取得样本号(int样本号);
                        this.ActiveControl = fjy_yb_code样本;
                    }
                    //20160618 wjz end 添加 目的：焦点位于备注，回车显示先一个样本信息

                    if (strSelRelt == "保存" && strValueZT != "已审核")
                    {
                        //bll显示样本();
                    }
                    else
                    {
                        fjy_yb_code样本.Text = str取得样本号(int样本号);

                        this.ActiveControl = fjy_yb_code样本;
                    }

                    bllYBList();//billSave保存样本，间接调用：btnSave_Click(保存)，button审核_Click，button打印_Click，button下一个_Click，button上一个_Click，ProcessCmdKey，SaveSampleInfo未用，toolStripMenuItem1_Click(右键打印预览-已隐藏)，打印ToolStripMenuItem_Click(右键打印-已隐藏)
                    //bllYBListByYbCode();

                }

                //fhz_type_idtextBox_name
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowWarning(ex.ToString());
            }
        }
        private string str取得样本号(int int样本号)
        {
            string str样本号设置 = "";
            //if (int样本号.ToString().Length == 1)
            //{
            //    str样本号设置 = "00" + int样本号.ToString();
            //}
            //else if (int样本号.ToString().Length == 2)
            //{
            //    str样本号设置 = "0" + int样本号.ToString();
            //}
            //else
            //{
            //    str样本号设置 = int样本号.ToString();
            //}
            str样本号设置 = int样本号.ToString();

            return str样本号设置;

        }

        ////向His中回写检验数据
        //private void WriteDataToHis(string str申请单号, string strDate, string str设备ID, string str样本号)
        //{
        //    if (string.IsNullOrWhiteSpace(str申请单号))  //如果样本号是空的，则不进行任何处理
        //    {
        //        return;
        //    }

        //    //判断His数据库中是否已经有此条记录的信息，如果有则不进行回写操作
        //    bool ret = this.blljy.IsExistInHis(str申请单号, strDate, str设备ID, str样本号);
        //    if(ret)
        //    {
        //        return;
        //    }

        //    DataTable dt审核 = this.blljy.Get检验结果View(str申请单号, strDate, str设备ID, str样本号);
        //    //检验信息
        //    if (dt审核 != null && dt审核.Rows.Count > 0)
        //    {
        //        IList listForHisSql = new ArrayList();
        //        foreach (DataRow dr审核 in dt审核.Rows)
        //        {
        //            StringBuilder str审核Sql = new StringBuilder();
        //            str审核Sql.Append(@"INSERT INTO [JY检验结果]([申请单号],[检验日期],[检验设备],[样本号],[病人ID],[住院号],[病人姓名],[年龄],[身份证],[检验ID]");
        //            str审核Sql.Append(",[检验编码],[检验名称],[单位],[参考值],[升降],[检验值],[排序])  VALUES( ");
        //            str审核Sql.Append("'" + dr审核["申请单号"].ToString() + "',");
        //            str审核Sql.Append("'" + dr审核["检验日期"].ToString() + "',");
        //            str审核Sql.Append("'" + dr审核["检验设备"].ToString() + "',");
        //            str审核Sql.Append("'" + dr审核["样本号"].ToString() + "',");
        //            str审核Sql.Append("'" + dr审核["病人ID"].ToString() + "',");
        //            str审核Sql.Append("'" + dr审核["住院号"].ToString() + "',");
        //            str审核Sql.Append("'" + dr审核["病人姓名"].ToString() + "',");
        //            str审核Sql.Append("'" + dr审核["年龄"].ToString() + "',");
        //            str审核Sql.Append("'" + dr审核["身份证"].ToString() + "',");
        //            str审核Sql.Append("'" + dr审核["检验ID"].ToString() + "',");
        //            str审核Sql.Append("'" + dr审核["检验编码"].ToString() + "',");
        //            str审核Sql.Append("'" + dr审核["检验名称"].ToString() + "',");
        //            str审核Sql.Append("'" + dr审核["单位"].ToString() + "',");
        //            str审核Sql.Append("'" + dr审核["参考值"].ToString() + "',");
        //            str审核Sql.Append("'" + dr审核["升降"].ToString() + "',");
        //            str审核Sql.Append("'" + dr审核["检验值"].ToString() + "',");
        //            str审核Sql.Append("'" + dr审核["排序"].ToString() + "')");
        //            listForHisSql.Add(str审核Sql.ToString());
        //        }
        //        string strRet回传 = WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strCHisDBConn, listForHisSql);
        //        if (!(strRet回传.Equals("true")))
        //        {
        //            WWMessage.MessageShowWarning("向His数据库回写数据失败");
        //        }
        //    }
        //}


        /// <summary>
        /// 删除样本
        /// </summary>
        private void bllDel()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (WWMessage.MessageDialogResult("你确认删除当前 " + fjy_yb_code样本.Text + " 号样本？"))
                {
                    // MessageBox.Show(strCurrSelfjy_id);
                    if (this.strCurrSelfjy_id.Equals("") || strCurrSelfjy_id.Length == 0)
                    { }
                    else
                    {
                        string strSql = "SELECT fjy_id FROM SAM_JY where fjy_id='" + strCurrSelfjy_id + "' and fjy_zt='已审核' ";
                        string strValue = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteScalarBySql(ww.wwf.wwfbll.WWFInit.strDBConn, strSql);
                        if (strValue.Equals("") || strValue.Length == 0)
                        {
                            this.blljy.Delete(strCurrSelfjy_id);

                            //changed by wjz 20160128 修改逻辑错误，改之。删除样本时，用户信息的删除，不能依据下面三个控件：姓名、性别、科室。 ▽
                            #region old code
                            //if (fhz_name姓名.Text != "" && fhz_sex性别.Text != "" && fhz_dept科室.Text != "")////////////////////////
                            //{ //如果三者都不为空，默认是有效数据不进行删除，否则删除样本时同时删除病人信息
                            //}
                            #endregion

                            //if (fhz_name姓名.Text != "" && fhz_sex性别.Text != "" && fhz_dept科室.Text != "")//===============
                            if (dataGridView样本.CurrentRow.Cells["fhz_name"].ToString() != "" && dataGridView样本.CurrentRow.Cells["性别"].ToString() != "" && dataGridView样本.CurrentRow.Cells["科室"].ToString() != "")
                            {
                            }
                            //changed by wjz 20160128 修改逻辑错误，改之。删除样本时，用户信息的删除，不能依据下面三个控件：姓名、性别、科室。 △
                            else
                                bllPatient.BllPatientDel(modeljy.fhz_id);
                            //bllYBList();
                            bindingSource样本.RemoveAt(dataGridView样本.CurrentRow.Index);
                            //add by wjz 20160227 取消审核无反应，改之 ▽
                            //dataGridView样本_Click(null, null);

                            RemoveSexAgeTextChangedEvent();

                            //add by wjz 20160228 删除样本时，中间的样本项目信息不更新，改之 ▽
                            //bll显示样本();
                            bllYBListByYbCode();
                            //add by wjz 20160228 删除样本时，中间的样本项目信息不更新，改之 △

                            AddSexAgeTextChangedEvent();
                            //add by wjz 20160227 取消审核无反应，改之 △
                        }
                        else
                        {
                            WWMessage.MessageShowWarning("当前样本 " + this.fhz_zyh住院号.Text + " 已审核，不可删除。 ");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        /// <summary>
        /// 取消审核
        /// </summary>
        private void bll取消审核()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (this.strCurrSelfjy_id.Equals("") || strCurrSelfjy_id.Length == 0)
                { }
                else
                {
                    #region  此部分的逻辑做调整  已经注释，修改后的逻辑参考下方的 #region
                    //string strSql = "SELECT fjy_id FROM SAM_JY where fjy_id='" + strCurrSelfjy_id + "' and fjy_zt='已审核' ";
                    //string strValue = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteScalarBySql(ww.wwf.wwfbll.WWFInit.strDBConn, strSql);
                    //if (strValue.Equals("") || strValue.Length == 0)
                    //{
                    //    WWMessage.MessageShowWarning("当前样本 " + this.fhz_zyh住院号.Text + " 未审核，不可解除审核。 ");

                    //}
                    //else
                    //{
                    //    int intRRR = this.blljy.UpdateZT(strCurrSelfjy_id);
                    //    if (intRRR > 0)
                    //    {
                    //        // this.label状态.Visible = false;
                    //        //bllYBListByYbCode();
                    //        dataGridView样本_Click(null, null);
                    //    }

                    //}
                    #endregion
                    #region  修改后的逻辑
                    string strSql = "SELECT fjy_id,fjy_date,fjy_instr,fjy_yb_code,fapply_id,fhz_zyh FROM SAM_JY where fjy_id='" + strCurrSelfjy_id + "' and fjy_zt='已审核' ";//调整 20150702 wjz
                    DataTable dtTemp检验 = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql);   //调整过 20150702 wjz
                    if (dtTemp检验 == null || dtTemp检验.Rows.Count == 0) //调整过 20150702 wjz
                    //if (strValue.Equals("") || strValue.Length == 0)
                    {
                        WWMessage.MessageShowWarning("当前样本 " + this.fhz_zyh住院号.Text + " 未审核，不可解除审核。 ");

                    }
                    else
                    {
                        int intRRR = this.blljy.UpdateZT(strCurrSelfjy_id);
                        if (intRRR > 0)
                        {
                            //添加 20150702 wjz begin
                            string str申请单号 = dtTemp检验.Rows[0]["fapply_id"].ToString();
                            string str日期 = dtTemp检验.Rows[0]["fjy_date"].ToString();
                            string str设备ID = dtTemp检验.Rows[0]["fjy_instr"].ToString();
                            string str样本号 = dtTemp检验.Rows[0]["fjy_yb_code"].ToString();

                            //add by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 ▽
                            string str门诊住院号 = dtTemp检验.Rows[0]["fhz_zyh"].ToString();
                            //add by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 △

                            //changed 20150814 wjz 以下两行
                            //bool b是否回写 = ww.form.Properties.Settings.Default.WriteJYDataToHis;
                            //this.blljy.Del化验结果FromHis(str申请单号, str日期, str设备ID, str样本号, b是否回写);
                            //changed by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 ▽
                            //this.blljy.Del化验结果FromHis(str申请单号, str日期, str设备ID, str样本号);
                            this.blljy.Del化验结果FromHisBy门诊住院号(str门诊住院号, str日期, str设备ID, str样本号);
                            //changed by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 △
                            //添加 20150702 wjz end

                            // this.label状态.Visible = false;
                            //bllYBListByYbCode();

                            //add by wjz 20160227 取消审核无反应，改之 ▽
                            //dataGridView样本_Click(null, null);

                            RemoveSexAgeTextChangedEvent();

                            bll显示样本();

                            AddSexAgeTextChangedEvent();
                            //add by wjz 20160227 取消审核无反应，改之 △
                        }

                    }
                    #endregion
                }

            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }

        }



        /// <summary>
        /// 结果列显示 
        /// </summary>
        private void bllResultShow(string str仪器id)
        {
            if (str仪器id.Equals("mby_lb_mk3"))
            {
                this.fod.Visible = true;
                this.fcutoff.Visible = true;
            }
            else
            {
                this.fod.Visible = false;
                this.fcutoff.Visible = false;
            }
        }
        #endregion


        #region 方法
        /// <summary>
        /// 初始化
        /// </summary>
        private void bllInit初始化()
        {

            //add by wjz 20160128 画面加载时，事件fjy_instrComboBox_仪器_SelectedIndexChanged会被触发3次，先移除此事件，本方法结束时在附加此事件 ▽
            this.fjy_instrComboBox_仪器.SelectedIndexChanged -= fjy_instrComboBox_仪器_SelectedIndexChanged;
            //add by wjz 20160128 画面加载时，事件fjy_instrComboBox_仪器_SelectedIndexChanged会被触发3次，先移除此事件，本方法结束时在附加此事件 △

            try
            {
                //仪器
                InstrBLL bllInstr = new InstrBLL();
                DataTable dtInstr = bllInstr.BllInstrDTByUseAndGroupID(1, ww.wwf.wwfbll.LoginBLL.strDeptID);//仪器列表
                                                                                                            // MessageBox.Show(ww.wwf.wwfbll.LoginBLL.strDeptID);
                this.fjy_instrComboBox_仪器.DataSource = dtInstr;
                this.fjy_instrComboBox_仪器.ValueMember = "finstr_id";
                this.fjy_instrComboBox_仪器.DisplayMember = "ShowName";
                //20150610 begin  add by wjz 添加本机默认仪器功能

                string strInstrID = ww.form.wwf.Customization.DefaultInstrument.GetInstrID();
                if (!(string.IsNullOrWhiteSpace(strInstrID)))
                {
                    //changed by wjz 20160127 修改默认设备的异常处理 ▽
                    //this.fjy_instrComboBox_仪器.SelectedValue = strInstrID;
                    DataRow[] drInstr = dtInstr.Select("finstr_id='" + strInstrID + "'");
                    if (drInstr.Length > 0)
                    {
                        this.fjy_instrComboBox_仪器.SelectedValue = strInstrID;
                    }
                    //changed by wjz 20160127 修改默认设备的异常处理 △
                }
                //add by wjz 20160214 画面加载时，事件fjy_instrComboBox_仪器_SelectedIndexChanged会被触发3次，先移除此事件，本方法结束时在附加此事件 ▽
                //调用此事件方法的目的：调用方法（BllItemList仪器项目列表），从而为DataTable(dtItemList)赋值；
                //点击“审核”按钮时，需要使用此DataTable(dtItemList)。
                fjy_instrComboBox_仪器_SelectedIndexChanged(null, null);
                //add by wjz 20160214 画面加载时，事件fjy_instrComboBox_仪器_SelectedIndexChanged会被触发3次，先移除此事件，本方法结束时在附加此事件 △
                //20150610 end    add by wjz 添加本机默认仪器功能
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }

            //add by wjz 20160128 画面加载时，事件fjy_instrComboBox_仪器_SelectedIndexChanged会被触发3次，先移除此事件，本方法结束时在附加此事件 ▽
            this.fjy_instrComboBox_仪器.SelectedIndexChanged -= fjy_instrComboBox_仪器_SelectedIndexChanged;
            this.fjy_instrComboBox_仪器.SelectedIndexChanged += fjy_instrComboBox_仪器_SelectedIndexChanged;
            //add by wjz 20160128 画面加载时，事件fjy_instrComboBox_仪器_SelectedIndexChanged会被触发3次，先移除此事件，本方法结束时在附加此事件 △
        }

        //add by wjz 20160128 性别选择用下拉菜单来实现 ▽
        private void InitSexComboBox()
        {
            //try
            //{
            //    DataTable dtSex = bllType.BllComTypeDT("性别");

            //    //空白行
            //    DataRow drSpace = dtSex.NewRow();
            //    drSpace["fcode"] = "";
            //    drSpace["fname"] = "";
            //    dtSex.Rows.InsertAt(drSpace, 0);

            //    this.fhz_sex性别Combo.DataSource = dtSex;

            //    this.fhz_sex性别Combo.SelectedIndex = 0;
            //}
            //catch
            //{
            DataTable dtSex = new DataTable();
            dtSex.Columns.Add("fcode");
            dtSex.Columns.Add("fname");

            //空白行
            DataRow drSpace = dtSex.NewRow();
            drSpace["fcode"] = "";
            drSpace["fname"] = "";
            dtSex.Rows.Add(drSpace);

            //添加男性
            DataRow drMale = dtSex.NewRow();
            drMale["fcode"] = "1";
            drMale["fname"] = "男";
            dtSex.Rows.Add(drMale);

            //添加女性
            DataRow drFemale = dtSex.NewRow();
            drFemale["fcode"] = "2";
            drFemale["fname"] = "女";
            dtSex.Rows.Add(drFemale);

            this.fhz_sex性别Combo.DataSource = dtSex;
            //}
        }
        //add by wjz 20160128 性别选择用下拉菜单来实现 △

        //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 ▽
        private void Init病人类型()
        {
            DataTable dtPatientType = new DataTable();
            dtPatientType.Columns.Add("fcode");
            dtPatientType.Columns.Add("fname");

            //空白行
            DataRow drSpace = dtPatientType.NewRow();
            drSpace["fcode"] = "";
            drSpace["fname"] = "";
            dtPatientType.Rows.Add(drSpace);

            DataRow drMZ = dtPatientType.NewRow();
            drMZ["fcode"] = "1";
            drMZ["fname"] = "门诊";
            dtPatientType.Rows.Add(drMZ);

            DataRow drJZ = dtPatientType.NewRow();
            drJZ["fcode"] = "2";
            drJZ["fname"] = "急诊";
            dtPatientType.Rows.Add(drJZ);

            DataRow drZY = dtPatientType.NewRow();
            drZY["fcode"] = "3";
            drZY["fname"] = "住院";
            dtPatientType.Rows.Add(drZY);

            DataRow drCT = dtPatientType.NewRow();
            drCT["fcode"] = "4";
            drCT["fname"] = "查体";
            dtPatientType.Rows.Add(drCT);

            //this.cboPatientType.DisplayMember = "fname";
            //this.cboPatientType.ValueMember = "fcode";
            this.cboPatientType.DataSource = dtPatientType;

        }
        //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 △

        //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
        private void Init年龄单位()
        {
            DataTable dtAgeUnit = new DataTable();
            dtAgeUnit.Columns.Add("fcode");
            dtAgeUnit.Columns.Add("fname");

            //空白行
            DataRow drSpace = dtAgeUnit.NewRow();
            drSpace["fcode"] = "";
            drSpace["fname"] = "";
            dtAgeUnit.Rows.Add(drSpace);

            DataRow drYear = dtAgeUnit.NewRow();
            drYear["fcode"] = "1";
            drYear["fname"] = "岁";
            dtAgeUnit.Rows.Add(drYear);

            DataRow drMonth = dtAgeUnit.NewRow();
            drMonth["fcode"] = "2";
            drMonth["fname"] = "月";
            dtAgeUnit.Rows.Add(drMonth);

            DataRow drDay = dtAgeUnit.NewRow();
            drDay["fcode"] = "3";
            drDay["fname"] = "天";
            dtAgeUnit.Rows.Add(drDay);

            //this.comboBoxAgeUnit.DisplayMember = "fname";
            //this.comboBoxAgeUnit.ValueMember = "fcode";
            this.comboBoxAgeUnit.DataSource = dtAgeUnit;
        }
        //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △

        /// <summary>
        /// 取得当前仪器+日期样本列表
        /// </summary>
        private void bllYBList()
        {

            string strWhere = " (t.fjy_date='" + fjy_date样本日期.Value.ToString("yyyy-MM-dd") + "' and t.fjy_instr='" + fjy_instrComboBox_仪器.SelectedValue.ToString() + "') ";
            string strWhere_1 = "";
            string strTJ = comboBox过滤样本.SelectedItem.ToString();
            if (strTJ.Equals("门诊"))
            {
                strWhere_1 = " and (fhz_type_id='1')";
            }
            else if (strTJ.Equals("急诊"))
            {
                strWhere_1 = " and (fhz_type_id='2')";
            }
            else if (strTJ.Equals("住院"))
            {
                strWhere_1 = " and (fhz_type_id='3')";
            }
            else if (strTJ.Equals("未打印"))
            {
                strWhere_1 = " and (fprint_zt='未打印')";
            }
            else if (strTJ.Equals("已打印"))
            {
                strWhere_1 = " and (fprint_zt='已打印')";
            }
            else if (strTJ.Equals("未审核"))
            {
                strWhere_1 = " and (fjy_zt='未审核')";
            }
            else if (strTJ.Equals("已审核"))
            {
                strWhere_1 = " and (fjy_zt='已审核')";
            }
            else if (strTJ.Equals("未打印未审核"))
            {
                strWhere_1 = " and (fprint_zt='未打印' and fjy_zt='未审核')";
            }
            else if (strTJ.Equals("未打印已审核"))
            {
                strWhere_1 = " and (fprint_zt='未打印' and fjy_zt='已审核')";
            }
            else if (strTJ.Equals("已打印未审核"))
            {
                strWhere_1 = " and (fprint_zt='已打印' and fjy_zt='未审核')";
            }
            else if (strTJ.Equals("已打印已审核"))
            {
                strWhere_1 = " and (fprint_zt='已打印' and fjy_zt='已审核')";
            }
            else
            {
                strWhere_1 = " and 1=1 ";
            }

            //changed by wjz 20160127 报告录入界面右侧，点击“刷新”时，显示当前设备、当前日期所有的样本检测结果 ▽
            //DataTable dtYB = this.blljy.GetList(strWhere + strWhere_1 + " Order by convert(int,fjy_yb_code)");
            //DataTable dtYB = this.blljy.GetListALL(fjy_instrComboBox_仪器.SelectedValue.ToString(), fjy_date样本日期.Value, strWhere_1);
            DataTable dtYB = this.blljy.GetList(strWhere + strWhere_1 + " Order by convert(int,fjy_yb_code)");
            //changed by wjz 20160127 报告录入界面右侧，点击“刷新”时，显示当前设备、当前日期所有的样本检测结果 △

            //add by wjz 20160308 设备名称报告时，bllYBListByYbCode可能会被调用2次，更改“bindingSource样本”数据源时可能会触发postionchanged事件，此事件也会调用一次 ▽
            int iOldCurrentIndex = this.bindingSource样本.Position;//记录旧的位置索引
            //add by wjz 20160308 设备名称报告时，bllYBListByYbCode可能会被调用2次，更改“bindingSource样本”数据源时可能会触发postionchanged事件，此事件也会调用一次 △

            //add by wjz 20160614 点击右侧样本信息概要时，会同时触发bindingSource样本_PositionChanged、dataGridView样本_Click，此处变量，用于控制是否执行bindingSource样本_PositionChanged内的操作 ▽
            bFlagForPostionChanged = true;
            //add by wjz 20160614 点击右侧样本信息概要时，会同时触发bindingSource样本_PositionChanged、dataGridView样本_Click，此处变量，用于控制是否执行bindingSource样本_PositionChanged内的操作 △

            this.bindingSource样本.DataSource = dtYB;
            this.dataGridView样本.AutoGenerateColumns = false;
            this.dataGridView样本.DataSource = bindingSource样本;

            //add by wjz 20160614 点击右侧样本信息概要时，会同时触发bindingSource样本_PositionChanged、dataGridView样本_Click，此处变量，用于控制是否执行bindingSource样本_PositionChanged内的操作 ▽
            bFlagForPostionChanged = false;
            //add by wjz 20160614 点击右侧样本信息概要时，会同时触发bindingSource样本_PositionChanged、dataGridView样本_Click，此处变量，用于控制是否执行bindingSource样本_PositionChanged内的操作 △

            //this.dataGridView样本_Click(null, null);
            //foreach (DataGridViewRow row in dataGridView样本.Rows)
            //{
            //    string str打印状态 = row.Cells["fprint_zt"].Value.ToString();
            //    if(str打印状态.Equals("已打印"))
            //    {
            //        row.DefaultCellStyle.BackColor = Color.LightSteelBlue;
            //    }
            //}

            //20150612 del wjz 删除：因为重复调用
            //bllResultList获取检验结果();
            //bllResultShow显示结果();

            //add by wjz 20160308 设备名称报告时，bllYBListByYbCode可能会被调用2次，更改“bindingSource样本”数据源时可能会触发postionchanged事件，此事件也会调用一次 ▽
            //bllYBListByYbCode();
            int iNewCurrentIndex = this.bindingSource样本.Position;//记录新的位置索引
            if ((iNewCurrentIndex == iOldCurrentIndex) || (iNewCurrentIndex == -1))//新数据源当前行为-1时（新数据源为空），postionchanged事件，bllYBListByYbCode也未被调用，此处需要补上
            {
                bllYBListByYbCode();//位置没变的情况下，“bindingSource样本”的postionchanged事件未触发，bllYBListByYbCode未被调用，此时需主动调用一次
            }
            else
            {
                //位置变更时，不做任何处理。“bindingSource样本”的postionchanged事件中已调用bllYBListByYbCode
            }

            ////changed by wjz 20160614 “bindingSource样本”的postionchanged事件内的操作被注释。数据源发生变更时(数据行数由40->30，原位置在40,新位置为30），此时需要触发postionchanged事件，在此只变更样本号 ▽
            ////if ((iNewCurrentIndex == iOldCurrentIndex) || (iNewCurrentIndex == -1))//新数据源当前行为-1时（新数据源为空），postionchanged事件，bllYBListByYbCode也未被调用，此处需要补上
            ////{
            ////    bllYBListByYbCode();//位置没变的情况下，“bindingSource样本”的postionchanged事件未触发，bllYBListByYbCode未被调用，此时需主动调用一次
            ////}
            ////else
            ////{
            ////    //位置变更时，不做任何处理。“bindingSource样本”的postionchanged事件中已调用bllYBListByYbCode
            ////}

            ////if (iNewCurrentIndex == iOldCurrentIndex)
            ////{}
            ////else 
            //if(iNewCurrentIndex==-1)
            //{
            //    this.fjy_yb_code样本.Text= "1";
            //}
            //else
            //{
            //    //this.fjy_yb_code样本.Text = dataGridView样本.Rows[iNewCurrentIndex].Cells["fjy_yb_code"].Value.ToString();
            //    DataRowView drv = (DataRowView)bindingSource样本.Current;
            //    this.fjy_yb_code样本.Text = drv["fjy_yb_code"].ToString();
            //}

            ////add by wjz 20160614 由于“bindingSource样本”的postionchanged事件内的操作已被注释，bllYBListByYbCode不会再在postionchanged事件中触发，故修改此处的逻辑。
            //bllYBListByYbCode();//上方既有的实现(原来因postionchanged事件内调用过一次，将此句做了注释，现在恢复)，迁移至了此处。
            ////changed by wjz 20160614 “bindingSource样本”的postionchanged事件内的操作被注释。数据源发生变更时(数据行数由40->30，原位置在40,新位置为30），此时需要触发postionchanged事件，在此只变更样本号 △

            //add by wjz 20160308 设备名称报告时，bllYBListByYbCode可能会被调用2次，更改“bindingSource样本”数据源时可能会触发postionchanged事件，此事件也会调用一次 △

        }

        private bool bFlagForPostionChanged = false;

        /// <summary>
        /// 取得一个样本内容
        /// </summary>
        /// <param name="strid"></param>
        private void bllYBListByYbCode()
        {
            try
            {
                try
                {
                    Convert.ToInt32(this.fjy_yb_code样本.Text);
                }
                catch
                {
                    WWMessage.MessageShowError("样本号录入有误，请录入数值后重试。");
                    this.ActiveControl = fjy_yb_code样本;
                    return;
                }
                modeljy = new jymodel();

                string strWhere = "fjy_date='" + fjy_date样本日期.Value.ToString("yyyy-MM-dd") + "' and fjy_instr='" + fjy_instrComboBox_仪器.SelectedValue.ToString() + "' and fjy_yb_code='" + fjy_yb_code样本.Text + "'";

                DataTable dtYBById = this.blljy.GetList(strWhere);
                if (dtYBById.Rows.Count > 0)
                {
                    DataRow drYB = dtYBById.Rows[0];
                    modeljy.fjy_检验id = drYB["fjy_id"].ToString();
                    modeljy.fjy_仪器ID = drYB["fjy_instr"].ToString(); //fjy_instrComboBox.SelectedValue;//fjy_dateDateTimePicker.Value.ToString("yyyy-MM-dd") 
                    fjy_instrComboBox_仪器.SelectedValue = modeljy.fjy_仪器ID;

                    modeljy.S身份证号 = drYB["sfzh"].ToString();
                    if (!string.IsNullOrEmpty(modeljy.S身份证号))
                    {
                        this.txt身份证号.Text = modeljy.S身份证号;
                    }
                    else
                    {
                        this.txt身份证号.Text = "";
                    }
                    //电子健康卡号
                    modeljy.f1 = drYB["f1"].ToString();
                    if (!string.IsNullOrEmpty(modeljy.f1))
                    {
                        this.txt电子健康卡.Text = modeljy.f1;
                    }
                    else
                    {
                        this.txt电子健康卡.Text = "";
                    }

                    modeljy.fjy_检验日期 = drYB["fjy_date"].ToString();
                    if (!modeljy.fjy_检验日期.Equals("") & modeljy.fjy_检验日期.Length > 0)
                    {
                        fjy_date样本日期.Value = Convert.ToDateTime(modeljy.fjy_检验日期);
                    }

                    modeljy.fjy_yb_code = this.str取得样本号(Convert.ToInt32(drYB["fjy_yb_code"].ToString()));
                    this.fjy_yb_code样本.Text = modeljy.fjy_yb_code;

                    modeljy.fhz_id = drYB["fhz_id"].ToString();//2015-06-01 21:15:56于凤海 添加患者id绑定
                    modeljy.fhz_患者类型id = drYB["fhz_type_id"].ToString();

                    //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 ▽
                    //fhz_type_idtextBox_name.Text = this.bllType.BllComTypeNameByCode("病人类别", modeljy.fhz_患者类型id);
                    this.cboPatientType.SelectedValue = modeljy.fhz_患者类型id;
                    //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 △
                    modeljy.fhz_住院号 = drYB["fhz_zyh"].ToString();
                    this.fhz_zyh住院号.Text = modeljy.fhz_住院号;

                    modeljy.fhz_姓名 = drYB["fhz_name"].ToString();
                    this.fhz_name姓名.Text = modeljy.fhz_姓名;

                    modeljy.fhz_性别 = drYB["fhz_sex"].ToString();

                    //changed by wjz 20160128 性别控件由TextBox换为ComboBox ▽
                    //fhz_sex性别.Text = this.bllType.BllComTypeNameByCode("性别", modeljy.fhz_性别);//================
                    fhz_sex性别Combo.SelectedValue = modeljy.fhz_性别;
                    //changed by wjz 20160128 性别控件由TextBox换为ComboBox △

                    modeljy.fhz_年龄 = Convert.ToDecimal(drYB["fhz_age"].ToString());
                    this.fhz_age年龄.Text = modeljy.fhz_年龄.ToString();

                    modeljy.fhz_年龄单位 = drYB["fhz_age_unit"].ToString();

                    //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
                    //fhz_age_unittextBox.Text = this.bllType.BllComTypeNameByCode("年龄单位", modeljy.fhz_年龄单位);
                    this.comboBoxAgeUnit.SelectedValue = modeljy.fhz_年龄单位;
                    //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △

                    modeljy.fjy_收费类型ID = drYB["fjy_sf_type"].ToString();//收费类型

                    //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 ▽
                    //this.fjy_sf_typetextBox.Text = this.bllType.BllComTypeNameByCode("费别", modeljy.fjy_收费类型ID);
                    try
                    {
                        this.fjy_sf_typetextBoxCombox.SelectedValue = modeljy.fjy_收费类型ID;
                    }
                    catch
                    {
                        this.fjy_sf_typetextBoxCombox.SelectedValue = "";
                    }
                    //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 △

                    modeljy.fhz_dept患者科室 = drYB["fhz_dept"].ToString();
                    //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 ▽
                    //fhz_dept科室.Text = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn, "SELECT fname FROM WWF_DEPT where fdept_id='" + modeljy.fhz_dept患者科室 + "'");
                    try
                    {
                        this.fhz_dept科室cbo.SelectedValue = this.modeljy.fhz_dept患者科室;
                    }
                    catch
                    {
                        this.fhz_dept科室cbo.SelectedValue = "";
                    }
                    //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 △

                    modeljy.fhz_床号 = drYB["fhz_bed"].ToString();//床号
                    this.fhz_bedtextBox.Text = modeljy.fhz_床号;

                    modeljy.fapply_申请单ID = drYB["fapply_id"].ToString();

                    //changed by wjz 20160214 申请号发生变更时，程序可能会重新获取病人信息（调用fapply_idtextBox的TextChanged事件），再次获取病人信息是多余的，故改之 ▽
                    //对控件赋值的操作有3处，本方法中有两处，另外一处不用做修改；第三处在fhz_zyhtextBox_Leave方法中，也不用做修改。（原因：赋值空字符串，不会重获信息）
                    this.fapply_idtextBox.TextChanged -= fapply_idtextBox_TextChanged;//新增的代码
                    this.fapply_idtextBox.Text = modeljy.fapply_申请单ID;
                    this.fapply_idtextBox.TextChanged -= fapply_idtextBox_TextChanged;//新增的代码
                    this.fapply_idtextBox.TextChanged += fapply_idtextBox_TextChanged;//新增的代码
                    //changed by wjz 20160214 申请号发生变更时，程序可能会重新获取病人信息（调用fapply_idtextBox的TextChanged事件），再次获取病人信息是多余的，故改之 △

                    modeljy.fjy_yb_type = drYB["fjy_yb_type"].ToString();//样本类型

                    //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
                    //this.fjy_yb_typetextBox.Text = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn, "SELECT fname FROM SAM_SAMPLE_TYPE WHERE (fsample_type_id = '" + modeljy.fjy_yb_type + "')");
                    this.fjy_yb_typeComboBox.SelectedValue = this.modeljy.fjy_yb_type;
                    //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 △

                    modeljy.fapply_user_id = drYB["fapply_user_id"].ToString();//送检 人 申请人
                    //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
                    //this.fapply_user_id送检医师.Text = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn, "SELECT fname FROM WWF_PERSON where fperson_id='" + modeljy.fapply_user_id + "'");
                    try
                    {
                        this.fapply_user_id送检医师cbo.EditValue = modeljy.fapply_user_id;
                    }
                    catch
                    {
                        this.fapply_user_id送检医师cbo.EditValue = "";
                    }
                    //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 △
                    modeljy.fapply_申请时间 = drYB["fapply_time"].ToString();
                    if (modeljy.fapply_申请时间 != "" & modeljy.fapply_申请时间.Length > 0)
                    {
                        fapply_timeDateTimePicker.Value = Convert.ToDateTime(modeljy.fapply_申请时间);
                    }
                    modeljy.fsampling_采样时间 = drYB["fsampling_time"].ToString();
                    if (modeljy.fsampling_采样时间 != null & modeljy.fsampling_采样时间.Length > 0)
                    {
                        fsampling_timedateTimePicker.Value = Convert.ToDateTime(modeljy.fsampling_采样时间);
                    }

                    modeljy.freport_报告时间 = drYB["freport_time"].ToString();
                    if (modeljy.freport_报告时间 != "" & modeljy.freport_报告时间.Length > 0)
                    {
                        freport_timedateTimePicker.Value = Convert.ToDateTime(modeljy.freport_报告时间);
                    }

                    //检验医师
                    modeljy.fjy_user_id = drYB["fjy_user_id"].ToString();

                    //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
                    //this.fjy_user_idtextBox.Text = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn, "SELECT fname FROM WWF_PERSON where fperson_id='" + modeljy.fjy_user_id + "'");
                    try
                    {
                        this.fjy_user_idtextBoxCbo.SelectedValue = this.modeljy.fjy_user_id;
                    }
                    catch
                    {
                        this.fjy_user_idtextBoxCbo.SelectedValue = "";
                    }
                    //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △

                    //核对医师
                    modeljy.fchenk_审核医师ID = drYB["fchenk_user_id"].ToString();

                    //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
                    //this.fchenk_user_idtextBox.Text = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn, "SELECT fname FROM WWF_PERSON where fperson_id='" + modeljy.fchenk_审核医师ID + "'");
                    try
                    {
                        this.fchenk_user_idComboBox.SelectedValue = this.modeljy.fchenk_审核医师ID;
                    }
                    catch
                    {
                        this.fchenk_user_idComboBox.SelectedValue = "";
                    }
                    //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △

                    //del by wjz 20160216 删除临床诊断的获取 ▽
                    ////临床诊断
                    modeljy.fjy_lczd = drYB["fjy_lczd"].ToString();
                    this.fjy_lczdtextBox.Text = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn, "SELECT fname FROM SAM_DISEASE where fcode='" + modeljy.fjy_lczd + "'");
                    //del by wjz 20160216 删除临床诊断的获取 △

                    modeljy.fremark_备注 = drYB["fremark"].ToString();
                    this.fremarktextBox.Text = modeljy.fremark_备注;

                    modeljy.fjy_zt检验状态 = drYB["fjy_zt"].ToString();
                    fjy_ztlabel.Text = modeljy.fjy_zt检验状态;
                    if (modeljy.fjy_zt检验状态.Equals("已审核"))
                    {
                        label状态.Visible = true;
                        this.dataGridViewResult.Columns["fvalue"].ReadOnly = true;
                    }
                    else
                    {
                        label状态.Visible = false;
                        this.dataGridViewResult.Columns["fvalue"].ReadOnly = false;
                    }


                    modeljy.fprint_zt = drYB["fprint_zt"].ToString();
                    fprint_ztlabel.Text = modeljy.fprint_zt;

                    modeljy.fsys_用户ID = drYB["fsys_user"].ToString();
                    fchenk_user_idlabel.Text = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn, "SELECT fname FROM WWF_PERSON where fperson_id='" + modeljy.fchenk_审核医师ID + "'");

                    modeljy.fsys_创建时间 = drYB["fsys_time"].ToString();
                    fcheck_timelabel.Text = drYB["fcheck_time"].ToString();

                }
                else
                {
                    label状态.Visible = false;
                    this.dataGridViewResult.Columns["fvalue"].ReadOnly = false;

                    //modeljy.fjy_id = this.blljy.DbGuid();
                    modeljy.fjy_仪器ID = fjy_instrComboBox_仪器.SelectedValue.ToString();//fjy_dateDateTimePicker.Value.ToString("yyyy-MM-dd") 
                    modeljy.fjy_检验日期 = fjy_date样本日期.Value.ToString("yyyy-MM-dd");

                    // fjy_yb_codeTextBox.Text = "1";//默认 样本号为1，当前无样本时。
                    modeljy.fjy_yb_code = this.str取得样本号(Convert.ToInt32(this.fjy_yb_code样本.Text));

                    modeljy.fhz_年龄单位 = "1";//年龄单位默认为1

                    //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
                    //fhz_age_unittextBox.Text = this.bllType.BllComTypeNameByCode("年龄单位", modeljy.fhz_年龄单位);
                    this.comboBoxAgeUnit.SelectedValue = modeljy.fhz_年龄单位;
                    //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △

                    modeljy.fjy_user_id = LoginBLL.strPersonID;//检验医师

                    //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
                    //fjy_user_idtextBox.Text = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn, "SELECT fname FROM WWF_PERSON where fperson_id='" + modeljy.fjy_user_id + "'");
                    try
                    {
                        this.fjy_user_idtextBoxCbo.SelectedValue = this.modeljy.fjy_user_id;
                    }
                    catch
                    {
                        this.fjy_user_idtextBoxCbo.SelectedValue = "";
                    }
                    //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △

                    modeljy.fjy_zt检验状态 = "未审核";
                    fjy_ztlabel.Text = modeljy.fjy_zt检验状态;

                    modeljy.fprint_zt = "未打印";
                    fprint_ztlabel.Text = modeljy.fprint_zt;

                    //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 ▽
                    //fhz_type_idtextBox_name.Text = "";
                    this.cboPatientType.SelectedValue = "";
                    //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 △
                    fhz_zyh住院号.Text = "";
                    fhz_name姓名.Text = "";

                    //changed by wjz 20160128 性别控件由TextBox换为ComboBox ▽
                    //fhz_sex性别.Text = "";//===================
                    fhz_sex性别Combo.SelectedValue = "";
                    //changed by wjz 20160128 性别控件由TextBox换为ComboBox △

                    fhz_age年龄.Text = "";
                    //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 ▽
                    //fjy_sf_typetextBox.Text = "";
                    this.fjy_sf_typetextBoxCombox.SelectedValue = "";
                    //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 △

                    //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 ▽
                    //fhz_dept科室.Text = "";
                    this.fhz_dept科室cbo.SelectedValue = "";
                    //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 △

                    fhz_bedtextBox.Text = "";
                    fapply_idtextBox.Text = "";

                    //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
                    //fjy_yb_typetextBox.Text = "";
                    this.fjy_yb_typeComboBox.SelectedValue = "";
                    //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 △

                    //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
                    //fapply_user_id送检医师.Text = "";
                    this.fapply_user_id送检医师cbo.EditValue = "";
                    //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 △
                    fapply_timeDateTimePicker.Value = System.DateTime.Now;
                    fsampling_timedateTimePicker.Value = System.DateTime.Now;
                    freport_timedateTimePicker.Value = System.DateTime.Now;
                    // fjy_user_idtextBox.Text = "";

                    //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
                    //fchenk_user_idtextBox.Text = "";
                    this.fchenk_user_idComboBox.SelectedValue = "";
                    //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △

                    fjy_lczdtextBox.Text = "";
                    fremarktextBox.Text = "";

                    this.txt身份证号.Text = "";
                    this.txt电子健康卡.Text = "";
                }
                bllResultList获取检验结果();
                //bllResultShow显示结果();

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 根据住院号取得患者信息
        /// </summary>
        /// <param name="strPatCode"></param>
        private void bllPatInfo(string strPatCode)
        {
            try
            {
                if ((modeljy.fjy_检验id == null || fjy_ztlabel.Text == "未审核") && strPatCode != "")
                {
                    // MessageBox.Show("第一次来计算年龄");
                    DataTable dtPat = new DataTable();
                    //2015年5月18日注视掉
                    //2015-06-02 08:20:43于凤海修改，先取本地数据库，如果不存在则从接口视图中获取
                    dtPat = this.bllPatient.BllPatientDT(" and fhz_zyh='" + strPatCode + "'");
                    if (dtPat == null || dtPat.Rows.Count == 0)
                        dtPat = this.bllPatient.BllPatientDTFromHisDBby住院号(strPatCode);

                    if ((dtPat != null) && (dtPat.Rows.Count > 0))
                    {
                        modeljy.fhz_id = dtPat.Rows[0]["fhz_id"].ToString();

                        #region 加载病人信息时，防止对检验项目列表中的参考值多次更新，所以将TextChanged参考值去掉 add by wjz 20151204
                        //fhz_age年龄.TextChanged -= fhz_age年龄_TextChanged;
                        //fhz_sex性别.TextChanged -= fhz_sex性别_TextChanged;
                        //fhz_age_unittextBox.TextChanged -= fhz_age_unittextBox_TextChanged;
                        RemoveSexAgeTextChangedEvent();
                        #endregion

                        //2015-12-02 changed by wjz 门诊病人的年龄、年龄单位 ▽
                        //从HIS视图中查询出的门诊病人信息中，remark字段的值格式为：  “身份证号|年龄单位:年龄”（不包含引号）
                        if (dtPat.Rows[0]["ftype_id"].ToString() == "1")
                        {
                            string remarktemp = dtPat.Rows[0]["fremark"].ToString();

                            string[] remarks = remarktemp.Split('|');
                            if (remarks.Length == 2)
                            {
                                //remarks[0]里存储的是身份证号，先根据身份证号计算年龄，若身份证号是空，再取门诊里的年龄、年龄单位
                                if (remarks[0].Length == 18)
                                {
                                    string mzbirthday = remarks[0].Substring(6, 4) + "-" + remarks[0].Substring(10, 2) + "-" + remarks[0].Substring(12, 2);

                                    string mzGetAge = this.bllPatient.BllPatientAge(mzbirthday);
                                    if (!(string.IsNullOrWhiteSpace(mzGetAge)))
                                    {
                                        string[] sPatientAge = mzGetAge.Split(':');

                                        //2015-05-18代码位置做了调整
                                        //this.fhz_nametextBox.Text = dtPat.Rows[0]["fname"].ToString();//姓名
                                        //this.fhz_bedtextBox.Text = dtPat.Rows[0]["fbed_num"].ToString();//床号

                                        this.fhz_age年龄.Text = sPatientAge[1].ToString();//年龄

                                        //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
                                        //年龄单位
                                        //this.fhz_age_unittextBox.Text = sPatientAge[0].ToString();
                                        modeljy.fhz_年龄单位 = bllPatNameByCode("年龄单位", sPatientAge[0].ToString());
                                        this.comboBoxAgeUnit.SelectedValue = modeljy.fhz_年龄单位;
                                        //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △
                                        //2015-05-18注视掉
                                    }
                                }
                                else if (remarks[1].Length > 1)
                                {
                                    string[] mzage = remarks[1].Split(':');
                                    this.fhz_age年龄.Text = mzage[1].ToString();//年龄

                                    //年龄单位
                                    //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
                                    //this.fhz_age_unittextBox.Text = mzage[0].ToString();
                                    modeljy.fhz_年龄单位 = bllPatNameByCode("年龄单位", mzage[0].ToString());
                                    this.comboBoxAgeUnit.SelectedValue = this.modeljy.fhz_年龄单位;
                                    //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △
                                }
                                else { }
                            }
                        }
                        else //此处的else 与下方的if是匹配的
                             //2015-12-02 changed by wjz 门诊病人的年龄、年龄单位 △
                             //2015-05-18注视掉
                             //if (dtPat.Rows[0]["fbirthday"] != null)
                            if (!(string.IsNullOrWhiteSpace(dtPat.Rows[0]["fbirthday"].ToString())))
                        {
                            string strfbirthday = dtPat.Rows[0]["fbirthday"].ToString();
                            string strGetAge = this.bllPatient.BllPatientAge(strfbirthday);
                            //2015-05-18注视掉 下方的if内语句也做了调整
                            //if (strGetAge != "" || strGetAge != null)
                            if (!(string.IsNullOrWhiteSpace(strGetAge)))
                            {
                                string[] sPatientAge = strGetAge.Split(':');

                                //2015-05-18代码位置做了调整
                                //this.fhz_nametextBox.Text = dtPat.Rows[0]["fname"].ToString();//姓名
                                //this.fhz_bedtextBox.Text = dtPat.Rows[0]["fbed_num"].ToString();//床号

                                this.fhz_age年龄.Text = sPatientAge[1].ToString();//年龄

                                //年龄单位
                                //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
                                //this.fhz_age_unittextBox.Text = sPatientAge[0].ToString();
                                modeljy.fhz_年龄单位 = bllPatNameByCode("年龄单位", sPatientAge[0].ToString());
                                this.comboBoxAgeUnit.SelectedValue = modeljy.fhz_年龄单位;
                                //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △
                                //2015-05-18注视掉
                            }
                        }




                        //2015-05-18代码位置做了调整
                        this.fhz_name姓名.Text = dtPat.Rows[0]["fname"].ToString();//姓名
                        this.fhz_bedtextBox.Text = dtPat.Rows[0]["fbed_num"].ToString();//床号

                        //性别
                        //his数据库中的视图，直接取出的是汉字男女，在这需要转换一下，对应关系1-男 2-女，
                        //string str性别 = dtPat.Rows[0]["fsex"].ToString();
                        //modeljy.fhz_性别 = (str性别.Equals("男")) ? "1" : "2";
                        modeljy.fhz_性别 = dtPat.Rows[0]["fsex"].ToString();

                        //changed by wjz 20160128 性别控件由TextBox换为ComboBox ▽
                        //this.fhz_sex性别.Text = bllPatNameByCode("性别", modeljy.fhz_性别);//=============
                        this.fhz_sex性别Combo.SelectedValue = modeljy.fhz_性别;
                        //changed by wjz 20160128 性别控件由TextBox换为ComboBox △

                        //病人类别
                        //HIS数据库的视图中，门诊病人的类型设定为了1，住院病人的类型为3，这点与页面程序的选项保持一致
                        modeljy.fhz_患者类型id = dtPat.Rows[0]["ftype_id"].ToString();

                        //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 ▽
                        //this.fhz_type_idtextBox_name.Text = bllPatNameByCode("病人类别", modeljy.fhz_患者类型id);
                        this.cboPatientType.SelectedValue = modeljy.fhz_患者类型id;
                        //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 △

                        this.fjy_sf_typetextBoxCombox.Text = dtPat.Rows[0]["ffb_id"].ToString();
                        this.modeljy.fjy_收费类型ID = this.fjy_sf_typetextBoxCombox.SelectedValue.ToString();

                        //科室
                        modeljy.fhz_dept患者科室 = dtPat.Rows[0]["fdept_id"].ToString();

                        //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 ▽
                        //this.fhz_dept科室.Text = bllPatNameByCode("科室", modeljy.fhz_dept患者科室);
                        try
                        {
                            this.fhz_dept科室cbo.SelectedValue = this.modeljy.fhz_dept患者科室;
                        }
                        catch
                        {
                            this.fhz_dept科室cbo.SelectedValue = "";
                        }
                        //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 △

                        //人员
                        modeljy.fapply_user_id = dtPat.Rows[0]["fsjys_id"].ToString();
                        //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
                        //this.fapply_user_id送检医师.Text = bllPatNameByCode("人员", modeljy.fapply_user_id);
                        try
                        {
                            this.fapply_user_id送检医师cbo.EditValue = this.modeljy.fapply_user_id;
                        }
                        catch
                        {
                            this.fapply_user_id送检医师cbo.EditValue = "";
                        }
                        //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 △

                        //临床诊断
                        //chanaged begin 2015-05-28
                        //del by wjz 20160216 删除临床诊断的获取 ▽
                        modeljy.fjy_lczd = dtPat.Rows[0]["fdiagnose"].ToString();
                        this.fjy_lczdtextBox.Text = bllPatNameByCode("临床诊断", modeljy.fjy_lczd);
                        //del by wjz 20160216 删除临床诊断的获取 △
                        //string strICD = dtPat.Rows[0]["ICD码"].ToString();
                        //if(string.IsNullOrWhiteSpace(strICD) || strICD.Trim().Equals("-1"))
                        //{
                        //    strICD = dtPat.Rows[0]["fdiagnose"].ToString();
                        //}
                        //modeljy.fjy_lczd = strICD;
                        //this.fjy_lczdtextBox.Text = dtPat.Rows[0]["fdiagnose"].ToString();
                        //changed end 2015-05-28

                        modeljy.S身份证号 = dtPat.Rows[0]["sfzh"].ToString();
                        this.txt身份证号.Text = modeljy.S身份证号;
                        if (dtPat.Columns.Contains("card_no"))
                        {
                            modeljy.f1 = dtPat.Rows[0]["card_no"].ToString();
                            this.txt电子健康卡.Text = modeljy.f1;
                        }
                        //2015-05-18注视掉
                        //}
                        //}

                        #region 加载病人信息时，防止对检验项目列表中的参考值多次更新，所以将TextChanged参考值去掉 add by wjz 20151204
                        //加载性别前，将TextChanged事件再还原回去
                        //fhz_age年龄.TextChanged += fhz_age年龄_TextChanged;
                        //fhz_sex性别.TextChanged += fhz_sex性别_TextChanged;
                        //fhz_age_unittextBox.TextChanged += fhz_age_unittextBox_TextChanged;
                        AddSexAgeTextChangedEvent();
                        #endregion
                    }
                    else
                    {
                        modeljy.fhz_id = ""; //如果没有获取到患者信息，把id置空让系统重新生成guid
                        this.fhz_name姓名.Text = "";//姓名
                        this.fhz_age年龄.Text = "";//年龄

                        //年龄单位
                        modeljy.fhz_年龄单位 = "1";

                        //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
                        //this.fhz_age_unittextBox.Text = "岁";
                        this.comboBoxAgeUnit.SelectedValue = modeljy.fhz_年龄单位;
                        //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △


                        //性别
                        modeljy.fhz_性别 = "";

                        //changed by wjz 20160128 性别控件由TextBox换为ComboBox ▽
                        //this.fhz_sex性别.Text = "";//===========
                        this.fhz_sex性别Combo.SelectedValue = "";
                        //changed by wjz 20160128 性别控件由TextBox换为ComboBox △

                        //病人类别
                        //  modeljy.fhz_type_id = "";
                        // this.fhz_type_idtextBox_name.Text = "";


                        //科室
                        modeljy.fhz_dept患者科室 = "";

                        //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 ▽
                        //this.fhz_dept科室.Text = "";
                        this.fhz_dept科室cbo.SelectedValue = "";
                        //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 △

                        //人员
                        modeljy.fapply_user_id = "";
                        //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
                        //this.fapply_user_id送检医师.Text = "";
                        this.fapply_user_id送检医师cbo.EditValue = "";
                        //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 △

                        //临床诊断
                        modeljy.fjy_lczd = "";
                        this.fjy_lczdtextBox.Text = "";
                        //身份证
                        this.txt身份证号.Text = "";
                        //电子卡
                        this.txt电子健康卡.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        //20150629 wjz add
        private string blPatCodeByName(string strType, string strName)
        {
            string strCode = "";
            if (strType.Equals("科室"))
            {
                strCode = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn, "SELECT fdept_id FROM WWF_DEPT WHERE (fname = '" + strName + "')");
            }
            else if (strType.Equals("性别"))
            {
                if (strName.Equals("男"))
                {
                    strCode = "1";
                }
                else if (strName.Equals("女"))
                {
                    strCode = "2";
                }
            }
            else if (strType.Equals("病人类型"))
            {
                if (strName.Equals("门诊"))
                {
                    strCode = "1";
                }
                else if (strName.Equals("急诊"))
                {
                    strCode = "2";
                }
                else if (strName.Equals("住院"))
                {
                    strCode = "3";
                }
                else if (strName.Equals("查体"))
                {
                    strCode = "4";
                }
            }

            return strCode;
        }

        private string bllPatNameByCode(string strType, string strCode)
        {
            string strName = "";
            if (strType.Equals("年龄单位"))
            {
                if (strCode.Equals("岁"))
                {
                    strName = "1";
                }
                else if (strCode.Equals("月"))
                {
                    strName = "2";
                }
                else if (strCode.Equals("天"))
                {
                    strName = "3";
                }
            }
            else if (strType.Equals("性别"))
            {
                if (strCode.Equals("1"))
                {
                    strName = "男";
                }
                else if (strCode.Equals("2"))
                {
                    strName = "女";
                }
                ////2015-05-18添加else
                //else
                //{
                //    strName = strCode;
                //}

            }
            else if (strType.Equals("病人类别"))
            {
                if (strCode.Equals("1"))
                {
                    strName = "门诊";
                }
                else if (strCode.Equals("2"))
                {
                    strName = "急诊";
                }
                else if (strCode.Equals("3"))
                {
                    strName = "住院";
                }
                else if (strCode.Equals("4"))
                {
                    strName = "查体";
                }
                ////2015-05-18添加else
                //else
                //{
                //    strName = strCode;
                //}
            }
            else if (strType.Equals("科室"))
            {
                strName = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn, "SELECT fname FROM WWF_DEPT WHERE (fdept_id = '" + strCode + "')");
                ////2015-05-18添加if处理
                //if(string.IsNullOrWhiteSpace((strName)))
                //{
                //    strName = strCode;
                //}
            }
            else if (strType.Equals("人员"))
            {
                strName = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn, "SELECT  fname FROM   WWF_PERSON WHERE  (fperson_id = '" + strCode + "')");
                ////2015-05-18添加if处理
                //if (string.IsNullOrWhiteSpace((strName)))
                //{
                //    strName = strCode;
                //}
            }
            else if (strType.Equals("临床诊断"))
            {
                strName = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn, "SELECT  fname FROM  SAM_DISEASE WHERE  (fcode = '" + strCode + "')");
                //2015-05-18添加if处理
                if (string.IsNullOrWhiteSpace((strName)))
                {
                    strName = strCode;
                }
            }
            else
            {
                ////2015-05-18注释掉
                strName = "";
                //strName = strCode;
            }
            //
            return strName;
        }
        /*
         1	病人类别	1	MZ	门诊	1	1	
3	病人类别	3	ZY	住院	3	1	NULL
2	病人类别	2	JZ	急诊	2	1	
         */

        /// <summary>
        /// 快捷键 设置
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            //如果当前焦点在仪器、日期两个控件上，则禁止两个控件的操作
            if ((keyData == (Keys.Down) || keyData == (Keys.Up))
                && (this.ActiveControl == this.fjy_instrComboBox_仪器 || this.ActiveControl == this.fjy_date样本日期
                    || this.ActiveControl == this.comboBoxAgeUnit || this.ActiveControl == this.fchenk_user_idComboBox
                    || this.ActiveControl == this.fhz_sex性别Combo || this.ActiveControl == this.fjy_yb_typeComboBox
                    || this.ActiveControl == this.fapply_user_id送检医师cbo || this.ActiveControl == this.fjy_user_idtextBoxCbo
                    || this.ActiveControl == this.fchenk_user_idComboBox || this.ActiveControl == this.fapply_timeDateTimePicker
                    || this.ActiveControl == this.fsampling_timedateTimePicker || this.ActiveControl == this.freport_timedateTimePicker
                    || this.ActiveControl == this.cboPatientType
                    || this.ActiveControl == this.fjy_sf_typetextBoxCombox
                    || this.ActiveControl == this.fhz_dept科室cbo
                    )
                )
            {
                return true;
            }
            else if (keyData == (Keys.F6))
            {
                button新增.PerformClick();
                return true;
            }
            else if (keyData == (Keys.F9))
            {
                button审核.PerformClick();
                return true;
            }
            else if (keyData == (Keys.F7))
            {
                this.button删除.PerformClick();
                return true;
            }
            else if (keyData == (Keys.F8))
            {
                this.button仪器数据.PerformClick();
                return true;
            }

            else if (keyData == (Keys.F4) || keyData == (Keys.PageUp))
            {
                this.button上一个.PerformClick();
                return true;
            }
            else if (keyData == (Keys.F5) || keyData == (Keys.PageDown))
            {
                this.button下一个.PerformClick();
                return true;
            }
            else if (keyData == (Keys.F2))
            {
                this.button打印.PerformClick();
                return true;
            }
            else if (keyData == Keys.Enter)
            {
                ////20150618 wjz add 当焦点在备注文本框上，回车时，自动跳转到下一个样本
                //if (this.ActiveControl == fremarktextBox)
                //{
                //    SaveSampleInfo();
                //}
                //else 
                //

                if (this.ActiveControl.Parent.Parent.Name == "dataGridViewResult")
                    SendKeys.Send("{Down}");
                else
                    SendKeys.Send("{TAB}");
                return true;
            }
            else if (keyData == (Keys.Control | Keys.S))
            {
                //20150618 wjz 修改第一个参数 1-->2
                billSave保存样本(2, "保存");
            }
            //if ( this.textBox1.Focused)
            //{
            //    this.ActiveControl.BackColor = System.Drawing.Color.Bisque;
            //}
            //else
            //{
            //    this.ActiveControl.BackColor = System.Drawing.Color.White;
            //}

            //  private void fhz_type_idtextBox_name_Enter(object sender, EventArgs e)
            // {
            // this.fhz_type_idtextBox_name.BackColor = System.Drawing.Color.Bisque;  
            //this.ActiveControl.BackColor = System.Drawing.Color.Bisque;
            //  }



            return base.ProcessCmdKey(ref msg, keyData);
        }

        #endregion


        #region 打印
        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="printType"></param>
        private bool WWPrint(int printType, string str检验id) //20150611 wjz 是否打印成功，修改返回类型
        {
            bool printSuccess = false;
            try
            {
                ww.form.lis.sam.Report.JyPrint pr = new ww.form.lis.sam.Report.JyPrint();

                //20150618 wjz 修改打印第二个参数
                //printSuccess = pr.BllPrintViewer(printType, this.strCurrSelfjy_id);
                printSuccess = pr.BllPrintViewer(printType, str检验id);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }

            return printSuccess;
        }
        #endregion


        private void button上一个_Click(object sender, EventArgs e)
        {
            //add by wjz 20151205 移除年龄、性别的TextChanged事件 ▽
            RemoveSexAgeTextChangedEvent();
            //add by wjz 20151205 移除年龄、性别的TextChanged事件 △

            billSave保存样本(0, "");
            //this.ActiveControl = fhz_type_idtextBox_name;  //20150613 wjz 
            //bindingSource样本.MovePrevious();  //20150614 wjz   20150618 wjz del
            this.ActiveControl = fjy_yb_code样本;

            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 ▽
            AddSexAgeTextChangedEvent();
            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 △
        }

        private void button下一个_Click(object sender, EventArgs e)
        {
            //add by wjz 20151205 移除年龄、性别的TextChanged事件 ▽
            RemoveSexAgeTextChangedEvent();
            //add by wjz 20151205 移除年龄、性别的TextChanged事件 △

            billSave保存样本(1, "下一个");
            //bindingSource样本.MoveNext();  //20150614 wjz   20150618 wjz del
            //this.ActiveControl = fhz_type_idtextBox_name;  //20150613 wjz 
            this.ActiveControl = fjy_yb_code样本;

            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 ▽
            AddSexAgeTextChangedEvent();
            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 △
        }

        private void button打印_Click(object sender, EventArgs e)
        {
            //add by wjz 20151205 移除年龄、性别的TextChanged事件 ▽
            RemoveSexAgeTextChangedEvent();
            //add by wjz 20151205 移除年龄、性别的TextChanged事件 △

            //2015-06-09 21;38 begin  wjz 修改目的：点击“打印”按钮后，先执行审批操作，在执行打印操作。
            if (!(modeljy.fjy_zt检验状态.Equals("已审核")))
            {
                this.print = true;
                billSave保存样本(2, "保存");
            }
            //2015-06-09 21;38 end  wjz 修改目的：点击“打印”按钮后，先执行审批操作，在执行打印操作。

            bool printSuccess = WWPrint(0, modeljy.fjy_检验id);

            //20150618 wjz 代码注释修改，此部分代码已经迁移到了WWPrint函数中 begin 
            if (printSuccess)
            {
                int rowIndex = -1;
                for (int index = 0; index < this.dataGridView样本.Rows.Count; index++)
                {
                    string strTemp = this.dataGridView样本.Rows[index].Cells["fjy_id"].Value.ToString();
                    if (modeljy.fjy_检验id == strTemp)
                    {
                        rowIndex = index;
                        break;
                    }
                }
                if (rowIndex != -1)
                {
                    this.dataGridView样本.Rows[rowIndex].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                    this.dataGridView样本.Rows[rowIndex].Cells["fprint_zt"].Value = "已打印";

                    string str状态 = this.dataGridView样本.Rows[rowIndex].Cells["fjy_zt"].Value.ToString();
                    if (!(str状态.Equals("已审核"))) //状态当前不是“已审核”状态
                    {
                        this.blljy.BllReportUpdate审核状态(modeljy.fjy_检验id);
                        this.dataGridView样本.Rows[rowIndex].Cells["fjy_zt"].Value = "已审核";
                        modeljy.fjy_zt检验状态 = "已审核";
                        label状态.Visible = true;
                        this.dataGridViewResult.Columns["fvalue"].ReadOnly = true;

                        //ADD wjz 20150702 begin  打印样本信息后，判断此样本之前是否已经审核，如果是则不进行
                        string str日期 = fjy_date样本日期.Value.ToString("yyyy-MM-dd");

                        //changed 20150814 wjz 以下两行
                        //bool b是否回写 = ww.form.Properties.Settings.Default.WriteJYDataToHis;
                        //this.blljy.WriteDataToHis(modeljy.fapply_申请单ID, str日期, fjy_instrComboBox_仪器.SelectedValue.ToString(), fjy_yb_code样本.Text, b是否回写);
                        //changed by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 ▽
                        //this.blljy.WriteDataToHis(modeljy.fapply_申请单ID, str日期, fjy_instrComboBox_仪器.SelectedValue.ToString(), fjy_yb_code样本.Text);
                        this.blljy.WriteDataToHisBy门诊住院号(modeljy.fhz_住院号, str日期, fjy_instrComboBox_仪器.SelectedValue.ToString(), fjy_yb_code样本.Text);
                        //changed by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 △
                        //ADD wjz 20150702 end    打印样本信息后，判断此样本之前是否已经审核，如果是则不进行

                    }
                }
                //DataGridViewRow row = this.dataGridView样本.CurrentRow;
                //if (row != null)
                //{
                //    row.DefaultCellStyle.BackColor = Color.LightSteelBlue;
                //    row.Cells["fjy_zt"].Value = "已审核";
                //    row.Cells["fprint_zt"].Value = "已打印";
                //}
            }
            //20150618 wjz 代码注释，此部分代码已经迁移到了WWPrint函数中 end

            //打印后不跳转套下一个 20150613 wjz 代码注释掉
            //bindingSource样本.MoveNext();

            this.Focus();
            this.ActiveControl = button打印;

            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 ▽
            AddSexAgeTextChangedEvent();
            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 △
            this.print = false;
        }

        private void bll显示样本()
        {
            try
            {
                this.Validate();
                bindingSource样本.EndEdit();
                this.dataGridView样本.EndEdit();
                rowCurrent = (DataRowView)bindingSource样本.Current;
                if (rowCurrent != null)
                {
                    string strid = rowCurrent["fjy_yb_code"].ToString();
                    strCurrSelfjy_id = rowCurrent["fjy_id"].ToString();


                    this.fjy_yb_code样本.Text = strid;
                    bllYBListByYbCode();
                    bll比较();
                    label21比较住院号.Text = "住院号：" + fhz_zyh住院号.Text;
                }
                else
                {
                    label21比较住院号.Text = "住院号：";
                    DataTable dtBJValue = new DataTable();
                    this.dataGridView1.DataSource = dtBJValue;
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }


            //try
            //{

            //    this.Validate();
            //    bindingSource样本.EndEdit();
            //    this.dataGridView样本.EndEdit();
            //    if (dataGridView样本.Rows.Count > 0)
            //    {
            //        string strid = this.dataGridView样本.CurrentRow.Cells["fjy_yb_code"].Value.ToString();
            //        strCurrSelfjy_id = this.dataGridView样本.CurrentRow.Cells["fjy_id"].Value.ToString();
            //        this.fjy_yb_codeTextBox.Text = strid;
            //        bllYBListByYbCode();
            //        bll比较();
            //        label21比较住院号.Text = "住院号：" + fhz_zyhtextBox.Text;
            //    }
            //    else
            //    {
            //        label21比较住院号.Text = "住院号：";
            //        DataTable dtBJValue = new DataTable();
            //        this.dataGridView1.DataSource = dtBJValue;
            //    }

            //}
            //catch (Exception ex)
            //{
            //    ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            //}

        }
        DataRowView rowCurrent = null;
        /// <summary>
        /// 数据源上移下移
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bindingSource样本_PositionChanged(object sender, EventArgs e)
        {
            //add by wjz 20160614 只有当bFlagForPostionChanged为true时，才执行此事件内的操作 ▽
            if (bFlagForPostionChanged)
            {
                //Console.WriteLine("bindingSource样本_PositionChanged start");
                //add by wjz 20151205 移除年龄、性别的TextChanged事件 ▽
                RemoveSexAgeTextChangedEvent();
                //add by wjz 20151205 移除年龄、性别的TextChanged事件 △

                bll显示样本();

                //add by wjz 20151205 恢复年龄、性别的TextChanged事件 ▽
                AddSexAgeTextChangedEvent();
                //add by wjz 20151205 恢复年龄、性别的TextChanged事件 △

                //Console.WriteLine("bindingSource样本_PositionChanged end");
            }
            //add by wjz 20160614 只有当bFlagForPostionChanged为true时，才执行此事件内的操作 ▽

            //del by wjz 20160614 bindingSource样本_PositionChanged 与 dataGridView样本_Click之间会重复调用，故删之（其他地方也有逻辑变更） ▽
            ////add by wjz 20151205 移除年龄、性别的TextChanged事件 ▽
            //RemoveSexAgeTextChangedEvent();
            ////add by wjz 20151205 移除年龄、性别的TextChanged事件 △

            //bll显示样本();

            ////add by wjz 20151205 恢复年龄、性别的TextChanged事件 ▽
            //AddSexAgeTextChangedEvent();
            ////add by wjz 20151205 恢复年龄、性别的TextChanged事件 △
            //del by wjz 20160614 bindingSource样本_PositionChanged 与 dataGridView样本_Click之间会重复调用，故删之（其他地方也有逻辑变更） △
        }
        /// <summary>
        /// 样本日期
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fjy_dateDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            //del 20151022 wjz
            //bllYBList();
            //del 20151022 wjz
        }

        /// <summary>
        /// 样本号 鼠标移开
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fjy_yb_codeTextBox_Leave(object sender, EventArgs e)
        {
            //changed by wjz 20151022 ▽
            //bllYBListByYbCode(); old code
            if (fjy_yb_code样本.Text.Equals(fjy_yb_code样本.Tag.ToString()) || string.IsNullOrWhiteSpace(fjy_yb_code样本.Text))
            {
                fjy_yb_code样本.Tag = "";
            }
            else
            {
                //add by wjz 20151205 移除年龄、性别的TextChanged事件 ▽
                RemoveSexAgeTextChangedEvent();
                //add by wjz 20151205 移除年龄、性别的TextChanged事件 △

                bllYBListByYbCode();

                //add by wjz 20151205 恢复年龄、性别的TextChanged事件 ▽
                AddSexAgeTextChangedEvent();
                //add by wjz 20151205 恢复年龄、性别的TextChanged事件 △

            }
        }



        // <summary>
        // 样本号 回车
        // </summary>
        // <param name="sender"></param>
        // <param name="e"></param>
        //private void fjy_yb_codeTextBox_KeyDown(object sender, KeyEventArgs e)
        //{
        //    MessageBox.Show("到此");
        //   if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Space))//或(e.KeyCode==Keys.Enter) 

        //    {
        //        this.ActiveControl = fsample_barcodeTextBox;
        //        bllYBListByYbCode();
        //    }
        //}

        #region  病人类型
        /// <summary>
        /// 病人类型
        /// </summary>
        /// <param name="intType">1:双击一定打开弹出窗口 0:移动</param>
        private void bll病人类型(int intType)
        {
            //del by wjz 20160210 病人类型控件由TextBox调整为combobox控件 ▽
            //try
            //{
            //    string str默认 = "";
            //    string strtype = "病人类别";
            //    if (intType == 1)
            //    {
            //        using (com.HZTypeForm fm = new ww.form.lis.com.HZTypeForm(strtype))
            //        {
            //            if (fm.ShowDialog() != DialogResult.No)
            //            {
            //                modeljy.fhz_患者类型id = fm.strid;

            //                this.fhz_type_idtextBox_name.Text = fm.strname;

            //                str默认 = fm.str备注;
            //            }
            //        }
            //    }
            //    else
            //    {
            //        //if (fhz_type_idtextBox_name.Text == "")
            //        //    fhz_type_idtextBox_name.Text = "1";
            //        DataTable dt = this.bllType.BllComTypeDTByCode(strtype, this.fhz_type_idtextBox_name.Text.Trim(), this.fhz_type_idtextBox_name.Text.Trim());
            //        if (dt.Rows.Count > 0)
            //        {
            //            modeljy.fhz_患者类型id = dt.Rows[0]["fcode"].ToString();
            //            this.fhz_type_idtextBox_name.Text = dt.Rows[0]["fname"].ToString();
            //            str默认 = dt.Rows[0]["fremark"].ToString();
            //        }
            //        else
            //        {
            //            //using (com.HZTypeForm fm = new ww.form.lis.com.HZTypeForm(strtype))
            //            //{
            //            //    fm.ShowDialog();
            //            //    modeljy.fhz_患者类型id = fm.strid;
            //            //    this.fhz_type_idtextBox_name.Text = fm.strname;
            //            //}
            //        }
            //    }
            //    try
            //    {
            //        if (str默认 != "")
            //        {//添加体检类型默认处理
            //            string[] aa = str默认.Split(';');
            //            if (aa.Length > 0)
            //            {
            //                foreach (string ss in aa)
            //                {
            //                    string[] bb = ss.Split(',');
            //                    if (bb.Length > 0)
            //                    {
            //                        if (bb[0].ToString() == "科室")
            //                            fhz_dept科室.Text = bb[1].ToString();
            //                        if (bb[0].ToString() == "送检医师")
            //                            fapply_user_id送检医师.Text = bb[1].ToString();
            //                    }
            //                }
            //            }
            //        }
            //    }
            //    catch   { }
            //    this.ActiveControl = fhz_zyh住院号;
            //}
            //catch (Exception ex)
            //{
            //    WWMessage.MessageShowError(ex.ToString());
            //}
            //del by wjz 20160210 病人类型控件由TextBox调整为combobox控件 △
        }
        /// <summary>
        /// 病人类型
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fhz_type_idtextBox_name_DoubleClick(object sender, EventArgs e)
        {
            //del by wjz 20160210 病人类型控件由TextBox调整为combobox控件 ▽
            //bll病人类型(1);
            //del by wjz 20160210 病人类型控件由TextBox调整为combobox控件 △
        }
        private void fhz_type_idtextBox_name_Enter(object sender, EventArgs e)
        {
            //del by wjz 20160210 病人类型控件由TextBox调整为combobox控件 ▽
            //this.fhz_type_idtextBox_name.BackColor = System.Drawing.Color.Bisque;
            //del by wjz 20160210 病人类型控件由TextBox调整为combobox控件 △
        }
        private void fhz_type_idtextBox_name_Leave(object sender, EventArgs e)
        {
            //del by wjz 20160210 病人类型控件由TextBox调整为combobox控件 ▽
            //bll病人类型(0);
            //this.fhz_type_idtextBox_name.BackColor = System.Drawing.Color.White;
            //del by wjz 20160210 病人类型控件由TextBox调整为combobox控件 △
        }

        #endregion

        #region 住院号

        private void fhz_zyhtextBox_Enter(object sender, EventArgs e)
        {
            this.fhz_zyh住院号.BackColor = System.Drawing.Color.Bisque;

            //add by wjz 20160121 如果值没有变化，则不执行病人信息的查询 ▽
            this.fhz_zyh住院号.Tag = this.fhz_zyh住院号.Text.Trim();
            //add by wjz 20160121 如果值没有变化，则不执行病人信息的查询 △
        }
        private void fhz_zyhtextBox_Leave(object sender, EventArgs e)
        {
            this.fhz_zyh住院号.BackColor = System.Drawing.Color.White;

            //add by wjz 20160121 如果值没有变化，则不执行病人信息的查询 ▽
            try
            {
                if ((this.fhz_zyh住院号.Tag.ToString() == this.fhz_zyh住院号.Text.Trim())
                    || string.IsNullOrWhiteSpace(this.fhz_zyh住院号.Text))
                {
                    return;
                }
            }
            catch
            {
                return;
            }
            //add by wjz 20160121 如果值没有变化，则不执行病人信息的查询 △

            //如果是通过申请号获取了病人信息，则不再通过住院号获取病人信息
            //changed by wjz 20160121 修改逻辑处理，如果住院号发生变更的话，不管是否有申请号，都重新查询病人信息 ▽
            //string str申请号 = this.fapply_idtextBox.Text;
            //if(str申请号.Length > 0)
            //{
            //    return;
            //}
            this.fapply_idtextBox.Text = "";
            //changed by wjz 20160121 修改逻辑处理，如果住院号发生变更的话，不管是否有申请号，都重新查询病人信息 △

            //add by wjz 20151205 移除年龄、性别的TextChanged事件 ▽
            RemoveSexAgeTextChangedEvent();
            //add by wjz 20151205 移除年龄、性别的TextChanged事件 △

            //Begin 2015-05-28 修改
            string str门诊住院号 = fhz_zyh住院号.Text;
            if (str门诊住院号.Length == 4)
            {
                //changed by wjz 20160306 夏蔚医院的住院号是4位数，在这里可能会将其误认为是门诊号，针对夏蔚调整 ▽
                //根据数据库里的数据及医院的反馈得出一下结论：
                //      ①门诊病人一般不会超过200个；
                //      ②新HIS系统的住院号从2065开始，目前已到5000以上。
                //针对以上情况，做出如下措施：
                //   当输入的门诊住院号长度是4时，先将其转换为数字，如果数字<=2000，则按门诊号处理，数字大于2000，按住院号处理
                //如果转换失败，直接按住院号处理。
                //
                //str门诊住院号 = DateTime.Now.ToString("yyyyMMdd.")+str门诊住院号;
                //fhz_zyh住院号.Text = str门诊住院号;
                if (strHosptialCode == "CHIS2716")
                {
                    int ihosptial = -1;
                    try
                    {
                        ihosptial = Convert.ToInt32(str门诊住院号.Trim());
                    }
                    catch
                    {
                        //ihosptial = -1;
                    }
                    if (ihosptial <= 2000 && ihosptial >= 0)
                    {
                        str门诊住院号 = DateTime.Now.ToString("yyyyMMdd.") + str门诊住院号;
                        fhz_zyh住院号.Text = str门诊住院号;
                    }
                }
                else
                {
                    str门诊住院号 = DateTime.Now.ToString("yyyyMMdd.") + str门诊住院号;
                    fhz_zyh住院号.Text = str门诊住院号;
                }
                //changed by wjz 20160306 夏蔚医院的住院号是4位数，在这里可能会将其误认为是门诊号，针对夏蔚调整 △
            }
            //bllPatInfo(fhz_zyh住院号.Text);

            bllPatInfo(str门诊住院号);
            //end 

            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 ▽
            SetRef();
            AddSexAgeTextChangedEvent();
            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 △
        }
        #endregion

        #region  性别
        private void bll病人性别(int intType)
        {
            try
            {
                string strtype = "性别";
                if (intType == 1)
                {
                    using (com.HZTypeForm fm = new ww.form.lis.com.HZTypeForm(strtype))
                    {
                        if (fm.ShowDialog() != DialogResult.No)
                        {
                            modeljy.fhz_性别 = fm.strid;

                            //changed by wjz 20160128 性别控件由TextBox换为ComboBox ▽
                            //this.fhz_sex性别.Text = fm.strname;//==========
                            this.fhz_sex性别Combo.SelectedValue = modeljy.fhz_性别;
                            //changed by wjz 20160128 性别控件由TextBox换为ComboBox △
                        }
                    }
                }
                else
                {
                    //changed by wjz 20160128 性别控件由TextBox换为ComboBox ▽
                    //DataTable dt = this.bllType.BllComTypeDTByCode(strtype, this.fhz_sex性别.Text.Trim(), this.fhz_sex性别.Text.Trim());//=========
                    DataTable dt = this.bllType.BllComTypeDTByCode(strtype, this.fhz_sex性别Combo.Text.Trim(), this.fhz_sex性别Combo.Text.Trim());
                    //changed by wjz 20160128 性别控件由TextBox换为ComboBox △
                    if (dt.Rows.Count > 0)
                    {
                        modeljy.fhz_性别 = dt.Rows[0]["fcode"].ToString();

                        //changed by wjz 20160128 性别控件由TextBox换为ComboBox ▽
                        //this.fhz_sex性别.Text = dt.Rows[0]["fname"].ToString();//===================
                        this.fhz_sex性别Combo.SelectedValue = modeljy.fhz_性别;
                        //changed by wjz 20160128 性别控件由TextBox换为ComboBox △
                    }
                    else
                    {
                        //20150618 wjz 注释掉， 原因：化验室希望离开性别文本框时，没有窗口弹出
                        //using (com.HZTypeForm fm = new ww.form.lis.com.HZTypeForm(strtype))
                        //{
                        //    fm.ShowDialog();
                        //    modeljy.fhz_性别 = fm.strid;
                        //    this.fhz_sex性别.Text = fm.strname;
                        //}
                    }
                }
                //this.ActiveControl = fhz_age年龄;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }

        }


        private void fhz_sextextBox_DoubleClick(object sender, EventArgs e)
        {
            //del by wjz 20160128 性别控件由TextBox换为ComboBox ▽
            //bll病人性别(1);
            //del by wjz 20160128 性别控件由TextBox换为ComboBox △
        }
        private void fhz_sextextBox_Enter(object sender, EventArgs e)
        {
            //del by wjz 20160128 性别控件由TextBox换为ComboBox ▽
            //this.fhz_sex性别.BackColor = System.Drawing.Color.Bisque;
            //del by wjz 20160128 性别控件由TextBox换为ComboBox △
        }

        private void fhz_sextextBox_Leave(object sender, EventArgs e)
        {
            //del by wjz 20160128 性别控件由TextBox换为ComboBox ▽
            //bll病人性别(0);
            //this.fhz_sex性别.BackColor = System.Drawing.Color.White;
            //del by wjz 20160128 性别控件由TextBox换为ComboBox △
        }

        #endregion

        #region 年龄单位
        private void bll年龄单位(int intType)
        {
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
            //try
            //{
            //    string strtype = "年龄单位";
            //    if (intType == 1)
            //    {
            //        using (com.HZTypeForm fm = new ww.form.lis.com.HZTypeForm(strtype))
            //        {
            //            fm.ShowDialog();
            //            modeljy.fhz_年龄单位 = fm.strid;
            //            this.fhz_age_unittextBox.Text = fm.strname;
            //        }
            //    }
            //    else
            //    {
            //        DataTable dt = this.bllType.BllComTypeDTByCode(strtype, this.fhz_age_unittextBox.Text.Trim(), this.fhz_age_unittextBox.Text.Trim());
            //        if (dt.Rows.Count > 0)
            //        {
            //            modeljy.fhz_年龄单位 = dt.Rows[0]["fcode"].ToString();
            //            this.fhz_age_unittextBox.Text = dt.Rows[0]["fname"].ToString();
            //        }
            //        else
            //        {
            //            using (com.HZTypeForm fm = new ww.form.lis.com.HZTypeForm(strtype))
            //            {
            //                fm.ShowDialog();
            //                modeljy.fhz_年龄单位 = fm.strid;
            //                this.fhz_age_unittextBox.Text = fm.strname;
            //            }
            //        }
            //    }
            //    //this.ActiveControl = fjy_sf_typetextBox;
            //}
            //catch (Exception ex)
            //{
            //    WWMessage.MessageShowError(ex.ToString());
            //}
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △

        }
        private void fhz_age_unittextBox_DoubleClick(object sender, EventArgs e)
        {
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
            //bll年龄单位(1);
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △
        }

        private void fhz_age_unittextBox_Enter(object sender, EventArgs e)
        {
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
            //this.fhz_age_unittextBox.BackColor = System.Drawing.Color.Bisque;
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △
        }

        private void fhz_age_unittextBox_Leave(object sender, EventArgs e)
        {
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
            //bll年龄单位(0);
            //this.fhz_age_unittextBox.BackColor = System.Drawing.Color.White;
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △
        }
        #endregion

        #region 年龄
        private void fhz_agetextBox_Enter(object sender, EventArgs e)
        {
            this.fhz_age年龄.BackColor = System.Drawing.Color.Bisque;

            //add by wjz 20151205 防止年龄修改时，TextChanged事件被多次调用 ▽
            fhz_age年龄.TextChanged -= fhz_age年龄_TextChanged;
            this.fhz_age年龄.Tag = this.fhz_age年龄.Text;
            //add by wjz 20151205 防止年龄修改时，TextChanged事件被多次调用 △
        }

        private void fhz_agetextBox_Leave(object sender, EventArgs e)
        {
            this.fhz_age年龄.BackColor = System.Drawing.Color.White;

            //add by wjz 20151205 防止年龄修改时，TextChanged事件被多次调用 ▽
            if (this.fhz_age年龄.Text.Trim() == this.fhz_age年龄.Tag.ToString().Trim())
            { }
            else
            {
                fhz_age年龄_TextChanged(null, null);
            }
            fhz_age年龄.TextChanged -= fhz_age年龄_TextChanged;
            fhz_age年龄.TextChanged += fhz_age年龄_TextChanged;
            //add by wjz 20151205 防止年龄修改时，TextChanged事件被多次调用 △
        }
        #endregion

        #region 姓名
        private void fhz_nametextBox_Enter(object sender, EventArgs e)
        {
            this.fhz_name姓名.BackColor = System.Drawing.Color.Bisque;
        }

        private void fhz_nametextBox_Leave(object sender, EventArgs e)
        {
            this.fhz_name姓名.BackColor = System.Drawing.Color.White;
        }
        #endregion

        #region 费别
        private void bll费别(int intType)
        {
            try
            {
                string strtype = "费别";
                if (intType == 1)
                {
                    using (com.HZTypeForm fm = new ww.form.lis.com.HZTypeForm(strtype))
                    {
                        if (fm.ShowDialog() != DialogResult.No)
                        {
                            modeljy.fjy_收费类型ID = fm.strid;
                            //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 ▽
                            //this.fjy_sf_typetextBox.Text = fm.strname;
                            this.fjy_sf_typetextBoxCombox.SelectedValue = fm.strid;
                            //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 △
                        }
                    }
                }
                else
                {
                    //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 ▽
                    DataTable dt = this.bllType.BllComTypeDTByCode(strtype, this.fjy_sf_typetextBoxCombox.Text.Trim(), this.fjy_sf_typetextBoxCombox.Text.Trim());
                    //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 △
                    if (dt.Rows.Count > 0)
                    {
                        modeljy.fjy_收费类型ID = dt.Rows[0]["fcode"].ToString();
                        //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 ▽
                        //this.fjy_sf_typetextBox.Text = dt.Rows[0]["fname"].ToString();
                        this.fjy_sf_typetextBoxCombox.SelectedValue = dt.Rows[0]["fcode"].ToString();
                        //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 △
                    }
                    else
                    {
                        //using (com.HZTypeForm fm = new ww.form.lis.com.HZTypeForm(strtype))
                        //{
                        //    fm.ShowDialog();
                        //    modeljy.fjy_收费类型ID = fm.strid;
                        //    this.fjy_sf_typetextBox.Text = fm.strname;
                        //}
                    }
                }
                //this.ActiveControl = fhz_dept科室;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }

        }
        private void fjy_sf_typetextBox_DoubleClick(object sender, EventArgs e)
        {
            bll费别(1);
        }
        private void fjy_sf_typetextBox_Enter(object sender, EventArgs e)
        {
            //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 ▽
            //this.fjy_sf_typetextBox.BackColor = System.Drawing.Color.Bisque;
            this.fjy_sf_typetextBoxCombox.BackColor = System.Drawing.Color.Bisque;
            //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 △
        }
        private void fjy_sf_typetextBox_Leave(object sender, EventArgs e)
        {
            //del by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 ▽
            //bll费别(0);
            //del by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 △
            //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 ▽
            //this.fjy_sf_typetextBox.BackColor = System.Drawing.Color.White;
            this.fjy_sf_typetextBoxCombox.BackColor = System.Drawing.Color.White;
            //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 △
        }
        #endregion

        #region 科室
        private void bll科室(int intType)
        {
            //del by wjz 20160215 部门控件由TextBox调整为comboBox控件 ▽
            //try
            //{
            //    if (intType == 1)
            //    {
            //        using (com.HZDeptSelForm fm = new ww.form.lis.com.HZDeptSelForm())
            //        {
            //            if (fm.ShowDialog() != DialogResult.No)
            //            {
            //                modeljy.fhz_dept患者科室 = fm.strid;
            //                this.fhz_dept科室.Text = fm.strname;
            //            }
            //        }
            //    }
            //    else
            //    {
            //        DataTable dt = this.bllDept.BllDeptDTByCode(this.fhz_dept科室.Text.Trim(), this.fhz_dept科室.Text.Trim());
            //        if (dt.Rows.Count > 0)
            //        {
            //            modeljy.fhz_dept患者科室 = dt.Rows[0]["fdept_id"].ToString();
            //            this.fhz_dept科室.Text = dt.Rows[0]["fname"].ToString();
            //        }
            //        else
            //        {
            //            //using (com.HZDeptSelForm fm = new ww.form.lis.com.HZDeptSelForm())
            //            //{
            //            //    fm.ShowDialog();
            //            //    modeljy.fhz_dept = fm.strid;
            //            //    this.fhz_depttextBox.Text = fm.strname;
            //            //}
            //        }
            //    }
            //    //this.ActiveControl = fhz_bedtextBox;
            //}
            //catch (Exception ex)
            //{
            //    WWMessage.MessageShowError(ex.ToString());
            //}
            //del by wjz 20160215 部门控件由TextBox调整为comboBox控件 △
        }
        private void fhz_depttextBox_DoubleClick(object sender, EventArgs e)
        {
            //del by wjz 20160215 部门控件由TextBox调整为comboBox控件 △
            //bll科室(1);
            //del by wjz 20160215 部门控件由TextBox调整为comboBox控件 △
        }

        private void fhz_depttextBox_Enter(object sender, EventArgs e)
        {
            //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 ▽
            //this.fhz_dept科室.BackColor = System.Drawing.Color.Bisque;
            this.fhz_dept科室cbo.BackColor = System.Drawing.Color.Bisque;
            //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 △
        }

        private void fhz_depttextBox_Leave(object sender, EventArgs e)
        {
            //del by wjz 20160215 部门控件由TextBox调整为comboBox控件 ▽
            //bll科室(0);
            //del by wjz 20160215 部门控件由TextBox调整为comboBox控件 △

            //add by wjz 20160520 输入科室编码或名称，自动修改fhz_dept科室cbo的SelectedValue ▽
            try
            {
                string str科室Text = this.fhz_dept科室cbo.Text;
                DataTable dtDept = this.fhz_dept科室cbo.DataSource as DataTable;
                if (dtDept != null)
                {
                    DataRow[] drs = dtDept.Select("fdept_id='" + str科室Text + "' or fname='" + str科室Text + "'");
                    if (drs.Length == 0)
                    {
                        this.fhz_dept科室cbo.SelectedValue = "";
                    }
                    else if ((this.fhz_dept科室cbo.SelectedValue == null)
                        || ((this.fhz_dept科室cbo.SelectedValue != null) && (drs[0]["fdept_id"].ToString() != this.fhz_dept科室cbo.SelectedValue.ToString())))
                    {
                        this.fhz_dept科室cbo.SelectedValue = drs[0]["fdept_id"].ToString();
                    }
                }
            }
            catch
            {
                this.fhz_dept科室cbo.SelectedValue = "";
            }
            //add by wjz 20160520 输入科室编码或名称，自动修改fhz_dept科室cbo的SelectedValue △

            //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 ▽
            //this.fhz_dept科室.BackColor = System.Drawing.Color.White;
            this.fhz_dept科室cbo.BackColor = System.Drawing.Color.White;
            //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 △
        }

        //add by wjz 20160520 输入科室编码或名称，自动修改fhz_dept科室cbo的SelectedValue ▽
        private DateTime dt科室1;
        private DateTime dt科室2;
        private void fhz_dept科室cbo_Click(object sender, EventArgs e)
        {
            dt科室2 = DateTime.Now;
            if (dt科室1 != null && (dt科室2 - dt科室1).TotalSeconds < 0.5)
            {
                using (com.HZDeptSelForm fm = new ww.form.lis.com.HZDeptSelForm())
                {
                    if (fm.ShowDialog() != DialogResult.No)
                    {
                        this.fhz_dept科室cbo.SelectedValue = fm.strid;
                    }
                }
            }
            dt科室1 = dt科室2;
        }
        //add by wjz 20160520 输入科室编码或名称，自动修改fhz_dept科室cbo的SelectedValue △

        #endregion

        private void fhz_bedtextBox_Enter(object sender, EventArgs e)
        {
            this.fhz_bedtextBox.BackColor = System.Drawing.Color.Bisque;
        }

        private void fhz_bedtextBox_Leave(object sender, EventArgs e)
        {
            this.fhz_bedtextBox.BackColor = System.Drawing.Color.White;
        }

        private void fapply_idtextBox_Enter(object sender, EventArgs e)
        {
            this.fapply_idtextBox.BackColor = System.Drawing.Color.Bisque;
        }

        private void fapply_idtextBox_Leave(object sender, EventArgs e)
        {
            this.fapply_idtextBox.BackColor = System.Drawing.Color.White;
            //本方法的以下内容是新建的 20150629 wjz

            //del by wjz 20151208 参考值更新迁移到TextChanged事件 ▽
            ////add by wjz 20151206 更新参考值
            //SetRef();
            ////add by wjz 20151206 更新参考值
            //del by wjz 20151208 参考值更新迁移到TextChanged事件 △
        }
        #region 样本类型
        //  WindowsSampleTypeForm fcac = new WindowsSampleTypeForm(r);
        private void bll样本类型(int intType)
        {
            try
            {
                if (intType == 1)
                {
                    using (com.WindowsSampleTypeForm fm = new ww.form.lis.com.WindowsSampleTypeForm())
                    {
                        if (fm.ShowDialog() != DialogResult.No)
                        {
                            modeljy.fjy_yb_type = fm.strid;

                            //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
                            //this.fjy_yb_typetextBox.Text = fm.strname;
                            this.fjy_yb_typeComboBox.SelectedValue = fm.strid;
                            //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 △
                        }

                        //add by wjz 化验项目参考值与样本类型有关系 ▽
                        else
                        {
                            modeljy.fjy_yb_type = "";
                            //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
                            //this.fjy_yb_typetextBox.Text = "";
                            this.fjy_yb_typeComboBox.SelectedValue = "";
                            //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 △
                        }
                        //add by wjz 化验项目参考值与样本类型有关系 △
                    }
                }
                else
                {
                    //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
                    //DataTable dt = this.bllSampleType.BllDTByCode(this.fjy_yb_typetextBox.Text.Trim(), this.fjy_yb_typetextBox.Text.Trim());
                    //if (dt.Rows.Count > 0)
                    //{
                    //    modeljy.fjy_yb_type = dt.Rows[0]["fsample_type_id"].ToString();
                    //    this.fjy_yb_typetextBox.Text = dt.Rows[0]["fname"].ToString();
                    //}
                    DataTable dt = this.bllSampleType.BllDTByCode(this.fjy_yb_typeComboBox.Text.Trim(), this.fjy_yb_typeComboBox.Text.Trim());
                    if (dt.Rows.Count > 0)
                    {
                        modeljy.fjy_yb_type = dt.Rows[0]["fsample_type_id"].ToString();
                        this.fjy_yb_typeComboBox.SelectedValue = modeljy.fjy_yb_type;
                    }
                    //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 △
                    //else
                    //{
                    //    using (com.WindowsSampleTypeForm fm = new ww.form.lis.com.WindowsSampleTypeForm())
                    //    {
                    //        fm.ShowDialog();
                    //        modeljy.fjy_yb_type = fm.strid;
                    //        this.fjy_yb_typetextBox.Text = fm.strname;
                    //    }
                    //}
                }
                //this.ActiveControl = fapply_user_id送检医师;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }

        }
        private void fjy_yb_typetextBox_DoubleClick(object sender, EventArgs e)
        {
            //del by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
            //bll样本类型(1);
            //del by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 △

            // add by wjz 王庄大生化设备即化验血又化验尿，而且“淀粉酶”在血、尿化验中均有，且参考值不一样，针对此情况进行调整 ▽
            if (string.IsNullOrWhiteSpace(modeljy.fjy_yb_type))
            {
            }
            else
            {
                SetRef();
            }
            // add by wjz 王庄大生化设备即化验血又化验尿，而且“淀粉酶”在血、尿化验中均有，且参考值不一样，针对此情况进行调整 △
        }

        private void fjy_yb_typetextBox_Enter(object sender, EventArgs e)
        {
            //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
            //this.fjy_yb_typetextBox.BackColor = System.Drawing.Color.Bisque;
            this.fjy_yb_typeComboBox.BackColor = System.Drawing.Color.Bisque;
            //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 △
        }

        private void fjy_yb_typetextBox_Leave(object sender, EventArgs e)
        {
            //del by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
            //bll样本类型(0);
            //del by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 △
            //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
            //this.fjy_yb_typetextBox.BackColor = System.Drawing.Color.White;
            this.fjy_yb_typeComboBox.BackColor = System.Drawing.Color.White;
            //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 △
        }
        #endregion


        #region 送检医师
        private void bll人员(int intType, string strPersonType)
        {
            try
            {
                if (intType == 1)
                {
                    using (com.WindowsPersonForm fm = new ww.form.lis.com.WindowsPersonForm())
                    {
                        if (fm.ShowDialog() != DialogResult.No)
                        {
                            if (strPersonType.Equals("送检医师"))
                            {
                                modeljy.fapply_user_id = fm.strid;
                                //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
                                //this.fapply_user_id送检医师.Text = fm.strname;
                                try
                                {
                                    this.fapply_user_id送检医师cbo.EditValue = fm.strid;
                                }
                                catch
                                {
                                    this.fapply_user_id送检医师cbo.EditValue = "";
                                }
                                //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 △
                            }
                            else if (strPersonType.Equals("检验医师"))
                            {
                                //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
                                modeljy.fjy_user_id = fm.strid;
                                //this.fjy_user_idtextBox.Text = fm.strname;
                                try
                                {
                                    this.fjy_user_idtextBoxCbo.SelectedValue = fm.strid;
                                }
                                catch
                                {
                                    this.fjy_user_idtextBoxCbo.SelectedValue = "";
                                }
                                //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △
                            }
                            else if (strPersonType.Equals("核对医师"))
                            {
                                modeljy.fchenk_审核医师ID = fm.strid;

                                //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
                                //this.fchenk_user_idtextBox.Text = fm.strname;
                                try
                                {
                                    this.fchenk_user_idComboBox.SelectedValue = fm.strid;
                                }
                                catch
                                {
                                    this.fchenk_user_idComboBox.SelectedValue = "";
                                }
                                //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △
                            }
                        }
                    }
                }
                else
                {
                    DataTable dt = new DataTable();
                    if (strPersonType.Equals("送检医师"))
                    {
                        //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
                        //dt = this.bllPerson.BllDTByCode(this.fapply_user_id送检医师.Text.Trim(), this.fapply_user_id送检医师.Text.Trim());
                        //if (dt.Rows.Count > 0)
                        //{
                        //    modeljy.fapply_user_id = dt.Rows[0]["fperson_id"].ToString();
                        //    this.fapply_user_id送检医师.Text = dt.Rows[0]["fname"].ToString();
                        //}
                        //else
                        //{
                        //    //using (com.WindowsPersonForm fm = new ww.form.lis.com.WindowsPersonForm())
                        //    //{
                        //    //    fm.ShowDialog();
                        //    //    modeljy.fapply_user_id = fm.strid;
                        //    //    this.fapply_user_id送检医师.Text = fm.strname;
                        //    //}
                        //}
                        dt = this.bllPerson.BllDTByCode(this.fapply_user_id送检医师cbo.Text.Trim(), this.fapply_user_id送检医师cbo.Text.Trim());
                        if (dt.Rows.Count > 0)
                        {
                            modeljy.fapply_user_id = dt.Rows[0]["fperson_id"].ToString();
                            try
                            {
                                this.fapply_user_id送检医师cbo.EditValue = dt.Rows[0]["fperson_id"].ToString();
                            }
                            catch
                            {
                                this.fapply_user_id送检医师cbo.EditValue = "";
                            }
                        }
                        else
                        {
                            //using (com.WindowsPersonForm fm = new ww.form.lis.com.WindowsPersonForm())
                            //{
                            //    fm.ShowDialog();
                            //    modeljy.fapply_user_id = fm.strid;
                            //    this.fapply_user_id送检医师.Text = fm.strname;
                            //}
                        }
                        //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 △
                    }
                    else if (strPersonType.Equals("检验医师"))
                    {
                        //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
                        //dt = this.bllPerson.BllDTByCode(this.fjy_user_idtextBox.Text.Trim(), this.fjy_user_idtextBox.Text.Trim());
                        //if (dt.Rows.Count > 0)
                        //{
                        //    modeljy.fjy_user_id = dt.Rows[0]["fperson_id"].ToString();
                        //    this.fjy_user_idtextBox.Text = dt.Rows[0]["fname"].ToString();
                        //}
                        //else
                        //{
                        //    //using (com.WindowsPersonForm fm = new ww.form.lis.com.WindowsPersonForm())
                        //    //{
                        //    //    fm.ShowDialog();
                        //    //    modeljy.fjy_user_id = fm.strid;
                        //    //    this.fjy_user_idtextBox.Text = fm.strname;
                        //    //}
                        //}
                        dt = this.bllPerson.BllDTByCode(this.fjy_user_idtextBoxCbo.Text.Trim(), this.fjy_user_idtextBoxCbo.Text.Trim());
                        if (dt.Rows.Count > 0)
                        {
                            modeljy.fjy_user_id = dt.Rows[0]["fperson_id"].ToString();
                            try
                            {
                                this.fjy_user_idtextBoxCbo.SelectedValue = dt.Rows[0]["fperson_id"].ToString();
                            }
                            catch
                            {
                                this.fjy_user_idtextBoxCbo.SelectedValue = "";
                            }
                        }
                        else
                        {
                            //using (com.WindowsPersonForm fm = new ww.form.lis.com.WindowsPersonForm())
                            //{
                            //    fm.ShowDialog();
                            //    modeljy.fjy_user_id = fm.strid;
                            //    this.fjy_user_idtextBox.Text = fm.strname;
                            //}
                        }
                        //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △
                    }
                    else if (strPersonType.Equals("核对医师"))
                    {
                        //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
                        //dt = this.bllPerson.BllDTByCode(this.fchenk_user_idtextBox.Text.Trim(), this.fchenk_user_idtextBox.Text.Trim());
                        //if (dt.Rows.Count > 0)
                        //{
                        //    modeljy.fchenk_审核医师ID = dt.Rows[0]["fperson_id"].ToString();
                        //    this.fchenk_user_idtextBox.Text = dt.Rows[0]["fname"].ToString();
                        //}
                        //else
                        //{
                        //    //using (com.WindowsPersonForm fm = new ww.form.lis.com.WindowsPersonForm())
                        //    //{
                        //    //    fm.ShowDialog();
                        //    //    modeljy.fchenk_审核医师ID = fm.strid;
                        //    //    this.fchenk_user_idtextBox.Text = fm.strname;
                        //    //}
                        //}
                        dt = this.bllPerson.BllDTByCode(this.fchenk_user_idComboBox.Text.Trim(), this.fchenk_user_idComboBox.Text.Trim());
                        if (dt.Rows.Count > 0)
                        {
                            modeljy.fchenk_审核医师ID = dt.Rows[0]["fperson_id"].ToString();
                            this.fchenk_user_idComboBox.SelectedValue = dt.Rows[0]["fperson_id"].ToString();
                        }
                        else
                        {
                            //using (com.WindowsPersonForm fm = new ww.form.lis.com.WindowsPersonForm())
                            //{
                            //    fm.ShowDialog();
                            //    modeljy.fchenk_审核医师ID = fm.strid;
                            //    this.fchenk_user_idtextBox.Text = fm.strname;
                            //}
                        }
                        //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △
                    }
                }
                //if (strPersonType.Equals("送检医师"))
                //{
                //    this.ActiveControl = fapply_timeDateTimePicker;
                //}
                //else if (strPersonType.Equals("检验医师"))
                //{
                //    this.ActiveControl = fchenk_user_idtextBox;
                //}
                //else if (strPersonType.Equals("核对医师"))
                //{
                //    this.ActiveControl = fjy_lczdtextBox;
                //}
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }

        }
        private void fapply_user_idtextBox_DoubleClick(object sender, EventArgs e)
        {
            //del by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
            //bll人员(1, "送检医师");
            //del by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 △
        }

        private void fapply_user_idtextBox_Enter(object sender, EventArgs e)
        {
            //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
            //this.fapply_user_id送检医师.BackColor = System.Drawing.Color.Bisque;
            this.fapply_user_id送检医师cbo.BackColor = System.Drawing.Color.Bisque;
            //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 △
        }

        private void fapply_user_idtextBox_Leave(object sender, EventArgs e)
        {
            //del by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
            //bll人员(0, "送检医师");
            //del by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 △

            //add by wjz 允许输入医师姓名或医师编码 ▽
            try
            {
                //string strApplyuser = fapply_user_id送检医师cbo.Text;
                //DataTable dtApplyUser = this.fapply_user_id送检医师cbo.DataSource as DataTable;
                //DataRow[] drs = dtApplyUser.Select("fperson_id='" + strApplyuser + "' or fname='" + strApplyuser + "' ");
                //if (drs.Length == 0)
                //{
                //    this.fapply_user_id送检医师cbo.SelectedValue = "";
                //}
                //else// if (drs[0]["fperson_id"].ToString() != strApplyuser)
                //{
                //    this.fapply_user_id送检医师cbo.SelectedValue = drs[0]["fperson_id"].ToString();
                //}
            }
            catch
            {
                this.fapply_user_id送检医师cbo.EditValue = "";
            }
            //add by wjz 允许输入医师姓名或医师编码 △

            //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
            //this.fapply_user_id送检医师.BackColor = System.Drawing.Color.White;
            this.fapply_user_id送检医师cbo.BackColor = System.Drawing.Color.White;
            //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
        }
        #endregion

        private void fapply_timeDateTimePicker_Enter(object sender, EventArgs e)
        {
            this.fapply_timeDateTimePicker.BackColor = System.Drawing.Color.Bisque;
        }

        private void fapply_timeDateTimePicker_Leave(object sender, EventArgs e)
        {
            this.fapply_timeDateTimePicker.BackColor = System.Drawing.Color.White;
        }

        private void fsampling_timedateTimePicker_Enter(object sender, EventArgs e)
        {
            this.fsampling_timedateTimePicker.BackColor = System.Drawing.Color.Bisque;
        }

        private void fsampling_timedateTimePicker_Leave(object sender, EventArgs e)
        {
            this.fsampling_timedateTimePicker.BackColor = System.Drawing.Color.White;
        }

        private void freport_timedateTimePicker_Enter(object sender, EventArgs e)
        {
            this.freport_timedateTimePicker.BackColor = System.Drawing.Color.Bisque;
        }

        private void freport_timedateTimePicker_Leave(object sender, EventArgs e)
        {
            this.freport_timedateTimePicker.BackColor = System.Drawing.Color.White;
        }
        #region 检验医师
        private void fjy_user_idtextBox_DoubleClick(object sender, EventArgs e)
        {
            //del by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
            //bll人员(1, "检验医师");
            //del by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △
        }
        private void fjy_user_idtextBox_Enter(object sender, EventArgs e)
        {
            //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
            //this.fjy_user_idtextBox.BackColor = System.Drawing.Color.Bisque;
            this.fjy_user_idtextBoxCbo.BackColor = System.Drawing.Color.Bisque;
            //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △
        }

        private void fjy_user_idtextBox_Leave(object sender, EventArgs e)
        {
            //del by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
            //bll人员(0, "检验医师");
            //del by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △
            //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
            //this.fjy_user_idtextBox.BackColor = System.Drawing.Color.White;
            this.fjy_user_idtextBoxCbo.BackColor = System.Drawing.Color.White;
            //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △

        }
        #endregion
        #region 核对医师
        private void fchenk_user_idtextBox_DoubleClick(object sender, EventArgs e)
        {
            //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
            //bll人员(1, "核对医师");
            //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △
        }

        private void fchenk_user_idtextBox_Enter(object sender, EventArgs e)
        {
            //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
            //this.fchenk_user_idtextBox.BackColor = System.Drawing.Color.Bisque;
            this.fchenk_user_idComboBox.BackColor = System.Drawing.Color.Bisque;
            //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △
        }

        private void fchenk_user_idtextBox_Leave(object sender, EventArgs e)
        {
            //del by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
            //bll人员(0, "核对医师");
            //del by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △

            //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
            //this.fchenk_user_idtextBox.BackColor = System.Drawing.Color.White;
            this.fchenk_user_idComboBox.BackColor = System.Drawing.Color.White;
            //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △
        }
        #endregion

        #region 临床诊断
        private void bll临床诊断(int intType)
        {
            try
            {
                if (intType == 1)
                {
                    using (com.HZDiseaseForm fm = new ww.form.lis.com.HZDiseaseForm())
                    {
                        if (fm.ShowDialog() != DialogResult.No)
                        {
                            modeljy.fjy_lczd = fm.strid;
                            this.fjy_lczdtextBox.Text = fm.strname;
                        }
                    }
                }
                else
                {
                    DataTable dt = this.bllDisease.BllDTByCode(this.fjy_lczdtextBox.Text.Trim(), this.fjy_lczdtextBox.Text.Trim());
                    if (dt.Rows.Count > 0)
                    {
                        modeljy.fjy_lczd = dt.Rows[0]["fcode"].ToString();
                        this.fjy_lczdtextBox.Text = dt.Rows[0]["fname"].ToString();
                    }
                    else
                    {
                        //using (com.HZDiseaseForm fm = new ww.form.lis.com.HZDiseaseForm())
                        //{
                        //    fm.ShowDialog();
                        //    modeljy.fjy_lczd = fm.strid;
                        //    this.fjy_lczdtextBox.Text = fm.strname;
                        //}
                    }
                }
                //this.ActiveControl = fremarktextBox;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }

        }
        private void fjy_lczdtextBox_DoubleClick(object sender, EventArgs e)
        {
            bll临床诊断(1);
        }

        private void fjy_lczdtextBox_Enter(object sender, EventArgs e)
        {
            this.fjy_lczdtextBox.BackColor = System.Drawing.Color.Bisque;
        }

        private void fjy_lczdtextBox_Leave(object sender, EventArgs e)
        {
            //del by wjz 20160216 删除临床诊断的获取 ▽
            //bll临床诊断(0);
            //del by wjz 20160216 删除临床诊断的获取 △
            this.fjy_lczdtextBox.BackColor = System.Drawing.Color.White;
        }
        #endregion

        private void fremarktextBox_Enter(object sender, EventArgs e)
        {
            this.fremarktextBox.BackColor = System.Drawing.Color.Bisque;
        }


        private void fremarktextBox_Leave(object sender, EventArgs e)
        {
            //return; // 以下无法访问代码无关紧要，因为没有添加开关，暂时手动跳出
            //add by wjz 20151224 ▽
            RemoveSexAgeTextChangedEvent();
            //add by wjz 20151224 △

            this.fremarktextBox.BackColor = System.Drawing.Color.White;
            //#region 暂时这部分代码注释，注释原因：健康档案借口暂未做调整，此处需要控制病人信息的显示与加载，待调整后再恢复此部分代码 del by wjz 20151224
            //changed by 2016-02-26 重新添加公共卫生借口，只根据健康档案号获取查体人员信息 ▽
            //if ((Regex.IsMatch(fremarktextBox.Text, @"^(^\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$", RegexOptions.IgnoreCase)))
            //{//简单验证身份证
            //    modeljy.S身份证号 = fremarktextBox.Text;
            //    bll获取查体患者(fremarktextBox.Text);
            //}
            //else
            //{
            //    if (fremarktextBox.Text.Length == 12)
            //    {
            //        fremarktextBox.Text = "371323" + fremarktextBox.Text;
            //        bll获取查体患者(fremarktextBox.Text);
            //    }
            //}
            if (fremarktextBox.Text.Trim().Length == 12)
            {
                fremarktextBox.Text = "371323" + fremarktextBox.Text.Trim();
                bll获取查体患者(fremarktextBox.Text.Trim());
            }
            else if (fremarktextBox.Text.Trim().Length == 16)
            {
                fremarktextBox.Text = "37" + fremarktextBox.Text.Trim();
                bll获取查体患者(fremarktextBox.Text.Trim());
            }
            else if (fremarktextBox.Text.Trim().Length == 18)
            {
                bll获取查体患者(fremarktextBox.Text.Trim());
            }
            else if (fremarktextBox.Text.Trim().Length > 0)
            {
                bll获取查体患者(fremarktextBox.Text.Trim());
            }
            //changed by 2016-02-26 重新添加公共卫生借口，只根据健康档案号获取查体人员信息 △
            //#endregion

            //add by wjz 20151224 ▽
            AddSexAgeTextChangedEvent();
            //add by wjz 20151224 △

            this.ActiveControl = button审核;
        }
        /// <summary>
        /// 根据身份证号取得患者信息
        /// </summary>
        /// <param name="strPatCode"></param>
        private void bll获取查体患者(string strPatCode)
        {
            try
            {
                if ((modeljy.fjy_检验id == null || fjy_ztlabel.Text == "未审核") && strPatCode != "")
                {
                    DataTable dtPat = new DataTable();
                    //changed by wjz 20160226 公共卫生系统变更，修改获取查体人员个人信息的方式，不要被函数名称迷惑，数据是从公共卫生数据库获取的，不是His数据库 ▽
                    //dtPat = this.bllPatient.BllPatientDT(" and fhz_id='" + strPatCode + "'");
                    //if (dtPat == null || dtPat.Rows.Count == 0)
                    //{
                    //    dtPat = this.bllPatient.BllPatientDTFromHisDBby档案号(strPatCode);
                    dtPat = this.bllPatient.BllPatientDTFromHisDBby档案号New(strPatCode);
                    //}
                    //changed by wjz 20160226 公共卫生系统变更，修改获取查体人员个人信息的方式，不要被函数名称迷惑，数据是从公共卫生数据库获取的，不是His数据库 △

                    if ((dtPat != null) && (dtPat.Rows.Count > 0))
                    {
                        modeljy.fhz_id = dtPat.Rows[0]["fhz_id"].ToString();
                        if (!(string.IsNullOrWhiteSpace(dtPat.Rows[0]["fbirthday"].ToString())))
                        {
                            string strfbirthday = dtPat.Rows[0]["fbirthday"].ToString();
                            string strGetAge = this.bllPatient.BllPatientAge(strfbirthday);
                            if (!(string.IsNullOrWhiteSpace(strGetAge)))
                            {
                                string[] sPatientAge = strGetAge.Split(':');

                                this.fhz_age年龄.Text = sPatientAge[1].ToString();//年龄
                                //年龄单位
                                //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
                                //this.fhz_age_unittextBox.Text = sPatientAge[0].ToString();
                                modeljy.fhz_年龄单位 = bllPatNameByCode("年龄单位", sPatientAge[0].ToString());
                                this.comboBoxAgeUnit.SelectedValue = modeljy.fhz_年龄单位;
                                //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △
                            }
                        }
                        //判断姓名是否需要解密
                        if (dtPat.Rows[0]["fname"].ToString().Length > 10)
                            this.fhz_name姓名.Text = DESEncrypt.DES解密(dtPat.Rows[0]["fname"].ToString());//姓名
                        else
                            this.fhz_name姓名.Text = dtPat.Rows[0]["fname"].ToString();//姓名
                        this.fhz_bedtextBox.Text = dtPat.Rows[0]["fbed_num"].ToString();//床号

                        modeljy.fhz_性别 = dtPat.Rows[0]["fsex"].ToString();

                        //changed by wjz 20160128 性别控件由TextBox换为ComboBox ▽
                        //this.fhz_sex性别.Text = bllPatNameByCode("性别", modeljy.fhz_性别);//===========
                        this.fhz_sex性别Combo.SelectedValue = modeljy.fhz_性别;
                        //changed by wjz 20160128 性别控件由TextBox换为ComboBox △

                        modeljy.fhz_患者类型id = dtPat.Rows[0]["ftype_id"].ToString();
                        //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 ▽
                        //this.fhz_type_idtextBox_name.Text = bllPatNameByCode("病人类别", modeljy.fhz_患者类型id);
                        this.cboPatientType.SelectedValue = modeljy.fhz_患者类型id;
                        //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 △
                        //科室
                        modeljy.fhz_dept患者科室 = dtPat.Rows[0]["fdept_id"].ToString();

                        //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 △
                        //this.fhz_dept科室.Text = bllPatNameByCode("科室", modeljy.fhz_dept患者科室);
                        try
                        {
                            this.fhz_dept科室cbo.SelectedValue = this.modeljy.fhz_dept患者科室;
                        }
                        catch
                        {
                            this.fhz_dept科室cbo.SelectedValue = "";
                        }
                        //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 △

                        //人员
                        modeljy.fapply_user_id = dtPat.Rows[0]["fsjys_id"].ToString();
                        //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
                        //this.fapply_user_id送检医师.Text = bllPatNameByCode("人员", modeljy.fapply_user_id);
                        try
                        {
                            this.fapply_user_id送检医师cbo.EditValue = this.modeljy.fapply_user_id;
                        }
                        catch
                        {
                            this.fapply_user_id送检医师cbo.EditValue = "";
                        }
                        //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 △

                        //临床诊断
                        //chanaged begin 2015-05-28
                        //del by wjz 20160216 删除临床诊断的获取 ▽
                        //modeljy.fjy_lczd = dtPat.Rows[0]["fdiagnose"].ToString();
                        //this.fjy_lczdtextBox.Text = bllPatNameByCode("临床诊断", modeljy.fjy_lczd);
                        //del by wjz 20160216 删除临床诊断的获取 △

                        modeljy.S身份证号 = dtPat.Rows[0]["sfzh"].ToString();
                        this.txt身份证号.Text = modeljy.S身份证号;
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void button审核_Click(object sender, EventArgs e)
        {
            //add by wjz 20151205 移除年龄、性别的TextChanged事件 ▽
            RemoveSexAgeTextChangedEvent();
            //add by wjz 20151205 移除年龄、性别的TextChanged事件 △

            billSave保存样本(1, "审核");
            //bindingSource样本.MoveNext();  //20150614 wjz  20150618 wjz del

            //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 ▽
            //this.ActiveControl = fhz_type_idtextBox_name;
            this.ActiveControl = cboPatientType;
            //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 △

            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 ▽
            AddSexAgeTextChangedEvent();
            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 △
        }

        private void dataGridView样本_Click(object sender, EventArgs e)
        {
            //add by wjz 20160227 点击右侧样本概要信息时，bll显示样本方法会被调用2次(dataGridView样本_Click、bindingSource样本_PositionChanged各调一次)，解决此问题 ▽
            //这是临时性解决方案，最优方案应该是：根据点击坐标，判断点击位置是否在“bindingSource样本”的数据行上，如果是，则不执行此方法，否，则继续执行此方法。
            //del by wjz 20160228 原因：左侧选中的为3号样本，左侧显示的可能为4号样本，所以点击“dataGridView样本”时，还需要去更新显示的样本。
            //if(this.bindingSource样本.Position >= 0)
            //{
            //    return;
            //}
            //add by wjz 20160227 点击右侧样本概要信息时，bll显示样本方法会被调用2次(dataGridView样本_Click、bindingSource样本_PositionChanged各调一次)，解决此问题 △
            //add by wjz 20151205 移除年龄、性别的TextChanged事件 ▽
            RemoveSexAgeTextChangedEvent();
            //add by wjz 20151205 移除年龄、性别的TextChanged事件 △

            bll显示样本();

            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 ▽
            AddSexAgeTextChangedEvent();
            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 △
        }

        private void button刷新样本_Click(object sender, EventArgs e)
        {
            //add by wjz 20151205 移除年龄、性别的TextChanged事件 ▽
            RemoveSexAgeTextChangedEvent();
            //add by wjz 20151205 移除年龄、性别的TextChanged事件 △

            bllYBList();//右上角“刷新”按钮

            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 ▽
            AddSexAgeTextChangedEvent();
            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 △
        }





        private void comboBox过滤样本_DropDownClosed(object sender, EventArgs e)
        {
            bllYBList();//右上角下拉列表的选择列表关闭时触发
        }

        private void button删除样本_Click(object sender, EventArgs e)
        {
            bllDel();
        }

        private void 解除审核ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bll取消审核();
        }

        private void 打印ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //add by wjz 20160127 此事件关联的按钮不再显示，但为防止意外，先修改此代码 ▽
            //报告录入界面右侧，点击“刷新”时，显示当前设备、当前日期所有的样本检测结果，点击右侧结果时，strCurrSelfjy_id获取的值可能为空字符。
            if (string.IsNullOrWhiteSpace(strCurrSelfjy_id))
            {
                ww.wwf.wwfbll.WWMessage.MessageShowInformation("请先审核，然后再打印。");
            }
            //add by wjz 20160127 此事件关联的按钮不再显示，但为防止意外，先修改此代码 △

            //2015-06-11 10;03 begin  wjz 修改目的：点击“打印”按钮后，先执行审批操作，在执行打印操作。
            if (!(modeljy.fjy_zt检验状态.Equals("已审核")))
            {
                billSave保存样本(2, "审核");
            }
            //2015-06-11 10;03 end  wjz 修改目的：点击“打印”按钮后，先执行审批操作，在执行打印操作。

            bool printSuccess = WWPrint(0, strCurrSelfjy_id);//获取是否打印成功

            //2015-06-11 10;03 begin  wjz 修改目的：右键打印后，更改数据行的颜色。
            //rowCurrent = (DataRowView)bindingSource样本.Current;
            DataGridViewRow row = dataGridView样本.CurrentRow;
            //20150618 wjz 逻辑做了调整 begin 
            if (printSuccess && row != null)
            {
                if (!(row.Cells["fprint_zt"].Value.ToString().Equals("已打印")))
                {
                    row.DefaultCellStyle.BackColor = Color.LightSteelBlue;
                    row.Cells["fprint_zt"].Value = "已打印";
                }

                if (!(row.Cells["fjy_zt"].Value.ToString().Equals("已审核")))
                {
                    this.blljy.BllReportUpdate审核状态(modeljy.fjy_检验id);
                    row.Cells["fjy_zt"].Value = "已审核";
                }
            }
            //20150618 wjz 逻辑做了调整 end 
            //2015-06-11 10;03 begin  wjz 修改目的：右键打印后，更改数据行的颜色。
        }
        private void bll仪器取数(int int选择结果值)
        {

            try
            {
                str仪器结果FTaskID = "";
                string strSql = "SELECT  COUNT(1) AS Expr1 FROM   SAM_JY WHERE (fjy_date = '" + fjy_date样本日期.Value.ToString("yyyy-MM-dd") + "') AND (fjy_instr = '" + fjy_instrComboBox_仪器.SelectedValue.ToString() + "') AND (fjy_yb_code = '" + fjy_yb_code样本.Text + "') and fjy_zt='已审核'";
                //fjy_yb_codeTextBox
                //string strSql = "SELECT fjy_id,fjy_zt FROM SAM_JY where fjy_date='" + fjy_dateDateTimePicker.Value.ToString("yyyy-MM-dd") + "' and fjy_instr='" + fjy_instrComboBox.SelectedValue.ToString() + "' and fjy_yb_code='" + fjy_yb_codeTextBox.Text + "' ";


                bool bool报告审核否 = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbRecordExists(ww.wwf.wwfbll.WWFInit.strDBConn, strSql);

                if (bool报告审核否 == false)
                {
                    //MessageBox.Show("到此了。");
                    using (com.JyInstrDataForm jsi = new ww.form.lis.com.JyInstrDataForm())
                    {
                        jsi.str仪器id = this.fjy_instrComboBox_仪器.SelectedValue.ToString();
                        jsi.str仪器名称 = this.fjy_instrComboBox_仪器.Text.ToString();
                        jsi.str样本号 = this.fjy_yb_code样本.Text;

                        //changed by wjz 调整日期格式 ▽
                        //jsi.str检验日期 = this.fjy_date样本日期.Text;
                        if (this.fjy_date样本日期.Value == null)
                        {
                            jsi.str检验日期 = DateTime.Now.ToString("yyyy-MM-dd");
                        }
                        else
                        {
                            jsi.str检验日期 = this.fjy_date样本日期.Value.ToString("yyyy-MM-dd");
                        }
                        //changed by wjz 调整日期格式 △

                        jsi.int选择结果否 = int选择结果值;
                        if (jsi.ShowDialog() == DialogResult.Cancel)
                            return;

                        str仪器结果FTaskID = jsi.str仪器结果FTaskID;
                        // textBox1.Text = str仪器结果FTaskID;
                        IList lisSel = jsi.selItemList;
                        #region 处理结果
                        if (lisSel != null)
                        {
                            for (int i = 0; i < lisSel.Count; i++)
                            {
                                string strItemidAndName = lisSel[i].ToString();
                                string[] sArray = strItemidAndName.Split(';');
                                string strItemId = "";
                                string strItemValue = "";
                                //foreach (string zz in sArray)
                                //{
                                if (sArray.Length > 0)
                                {
                                    strItemId = sArray[0].ToString();
                                    strItemValue = sArray[1].ToString();
                                    //}
                                    if (strItemId != "" & strItemId.Length > 0)
                                    {
                                        DataRow[] rows = dtResult.Select("fitem_id='" + strItemId + "'");
                                        if (rows.Length > 0) { }
                                        else
                                        {
                                            //richTextBox2.AppendText(lisSel[i].ToString() + "\r");
                                            DataRow drNew = this.dtResult.NewRow();
                                            drNew["fresult_id"] = this.blljy.DbGuid();
                                            drNew["fitem_id"] = strItemId;
                                            drNew["fvalue"] = strItemValue;
                                            drNew["fitem_code"] = sArray[2].ToString();
                                            drNew["fitem_name"] = sArray[3].ToString();
                                            drNew["fitem_unit"] = sArray[4].ToString();
                                            drNew["forder_by"] = sArray[5].ToString();

                                            //changed by wjz 20151202 根据性别、年龄获取参考值 ▽

                                            #region old code
                                            //string str参考值 = this.blljy.Bll参考值(strItemId, 0, "", "");
                                            #endregion

                                            #region new code
                                            string str参考值;
                                            //if (string.IsNullOrWhiteSpace(fhz_age年龄.Text) || string.IsNullOrWhiteSpace(fhz_sex性别.Text)
                                            //    || string.IsNullOrWhiteSpace(fhz_age_unittextBox.Text) || (fhz_age_unittextBox.Text == "岁" && fhz_age年龄.Text.Trim() == "0"))
                                            //{
                                            //    //str参考值 = this.blljy.Bll参考值(strItemId, 0, "", "");
                                            //    //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。▽
                                            //    //str参考值 = this.blljy.Bll参考值(strItemId, 0, "", "");
                                            //    str参考值 = this.blljy.Bll参考值(strItemId, -1, "", "");
                                            //    //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。△
                                            //}
                                            //else
                                            //{
                                            //    //float age = -1;
                                            //    //string strsex = "";
                                            //    //try
                                            //    //{
                                            //    //    age = Convert.ToInt32(fhz_age年龄.Text.Trim());
                                            //    //    str参考值 = this.blljy.Bll参考值New(strItemId, age, fhz_sex性别.Text.Trim(), "");
                                            //    //}
                                            //    //catch (FormatException ex)
                                            //    //{
                                            //    //    //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。▽
                                            //    //    //str参考值 = this.blljy.Bll参考值(strItemId, 0, "", "");
                                            //    //    str参考值 = this.blljy.Bll参考值(strItemId, -1, "", "");
                                            //    //    //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。△
                                            //    //}

                                            //    //str参考值 = this.blljy.Bll参考值New(strItemId, age, fhz_sex性别.Text.Trim(), "");
                                            //    str参考值 = GetRefValueByItemCode(strItemId);
                                            //}
                                            str参考值 = GetRefValueByItemCode(strItemId);
                                            #endregion
                                            //changed by wjz 20151202 根据性别、年龄获取参考值 △

                                            drNew["fitem_ref"] = str参考值;
                                            this.dtResult.Rows.Add(drNew);
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        //  textBox1.Text = str仪器结果FTaskID;
                        bllResultShow显示结果颜色();
                    }
                }
                else
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowInformation("当前报告已审核。");
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void button仪器数据_Click(object sender, EventArgs e)
        {
            //add by wjz 20151205 移除年龄、性别的TextChanged事件 ▽
            RemoveSexAgeTextChangedEvent();
            //add by wjz 20151205 移除年龄、性别的TextChanged事件 △

            bll仪器取数(1);

            //billSave保存样本(1,"选择结果");

            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 ▽
            AddSexAgeTextChangedEvent();
            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 △
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            label21比较住院号.Text = "住院号：" + fhz_zyh住院号.Text;
            bll比较();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void fhz_depttextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void fhz_zyh住院号_TextChanged(object sender, EventArgs e)
        {
            if (fhz_zyh住院号.Text == "")
                modeljy.fhz_id = "";
        }


        /// <summary>
        /// 核心代码为 bllYBList()
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fjy_instrComboBox_仪器_SelectedIndexChanged(object sender, EventArgs e)
        {
            string str仪器id = "";//
            {
                str仪器id = this.fjy_instrComboBox_仪器.SelectedValue.ToString();
            }

            //add by wjz 20151205 移除年龄、性别的TextChanged事件 ▽
            RemoveSexAgeTextChangedEvent();
            //add by wjz 20151205 移除年龄、性别的TextChanged事件 △

            try
            {
                //BllItemList仪器项目列表 这个方法不可以忽略
                //点击“审核”按钮时，需要使用此函数的(dtItemList)。
                BllItemList仪器项目列表();

                //bllResultShow 这个方法的作用可以忽略，mby_lb_mk3这个设备id不知道是干什么的
                bllResultShow(str仪器id);
                bllYBList();//设备ID的SelectedIndexChanged事件

            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowInformation(ex.Message);
            }
            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 ▽
            AddSexAgeTextChangedEvent();
            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 △
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //add by wjz 20160127 此事件关联的按钮不再显示，但为防止意外，先修改此代码 ▽
            //报告录入界面右侧，点击“刷新”时，显示当前设备、当前日期所有的样本检测结果，点击右侧结果时，strCurrSelfjy_id获取的值可能为空字符。
            if (string.IsNullOrWhiteSpace(strCurrSelfjy_id))
            {
                ww.wwf.wwfbll.WWMessage.MessageShowInformation("请先审核，然后再打印。");
            }
            //add by wjz 20160127 此事件关联的按钮不再显示，但为防止意外，先修改此代码 △

            //if(dtResult)
            //先判断项目信息是否有变更，如果有则执行保存审核操作
            if (IsDataTableChanged(dtResult) && !(modeljy.fjy_zt检验状态.Equals("已审核")) && dtResult.Rows.Count > 0)
            {
                //billSave保存样本(2, "审核");
                billSave保存样本(2, "保存");
            }
            //判断项目是否已经审核，若是，不保存修改内容
            //else if (!(IsDataTableChanged(dtResult)) && !(modeljy.fjy_zt检验状态.Equals("已审核")) && dtResult.Rows.Count > 0)
            //{
            //    //先审核后打印
            //    int updateRowCount = this.blljy.BllReportUpdate审核状态(strCurrSelfjy_id);
            //}

            bool printSuccess = WWPrint(1, strCurrSelfjy_id);

            //2015-06-11 10;03 begin  wjz 修改目的：右键打印后，更改数据行的颜色。
            //20150618 wjz  逻辑做了调整 begin 
            DataGridViewRow row = dataGridView样本.CurrentRow;
            //if (printSuccess)
            if (printSuccess && (row != null))
            {
                //ADD wjz 20150702 begin  打印样本信息后，判断此样本之前是否已经审核，如果是则不进行==========
                string str日期 = fjy_date样本日期.Value.ToString("yyyy-MM-dd");
                string str申请单号 = row.Cells["fapply_id"].Value.ToString();
                string str样本号 = row.Cells["fjy_yb_code"].Value.ToString();
                //add by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 ▽
                string str门诊住院号 = row.Cells["fhz_zyh"].Value.ToString();
                //add by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 △

                //changed 20150814 wjz 以下两行
                //bool b是否回写 = ww.form.Properties.Settings.Default.WriteJYDataToHis;
                //this.blljy.WriteDataToHis(str申请单号, str日期, fjy_instrComboBox_仪器.SelectedValue.ToString(), str样本号, b是否回写);

                //changed by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 ▽
                //this.blljy.WriteDataToHis(str申请单号, str日期, fjy_instrComboBox_仪器.SelectedValue.ToString(), str样本号);
                this.blljy.WriteDataToHisBy门诊住院号(str门诊住院号, str日期, fjy_instrComboBox_仪器.SelectedValue.ToString(), str样本号);
                //changed by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 △
                //ADD wjz 20150702 end    打印样本信息后，判断此样本之前是否已经审核，如果是则不进行==========

                //DataGridViewRow row = dataGridView样本.CurrentRow;
                if (!(row.Cells["fjy_zt"].Value.ToString().Equals("已审核")))
                {
                    this.blljy.BllReportUpdate审核状态(strCurrSelfjy_id);
                    row.Cells["fjy_zt"].Value = "已审核";
                }

                if (!(row.Cells["fprint_zt"].Value.ToString().Equals("已打印")))
                {
                    row.DefaultCellStyle.BackColor = Color.LightSteelBlue;
                    row.Cells["fprint_zt"].Value = "已打印";
                }

                //if (!(modeljy.fjy_zt检验状态.Equals("已审核")))
                //{
                //    //此处打印后再修改后审核状态
                //    int updateRowCount = this.blljy.BllReportUpdate审核状态(strCurrSelfjy_id);
                //    if (row != null)
                //    {
                //        row.Cells["fjy_zt"].Value = "已审核";
                //    }
                //}

                //if (row != null)
                //{
                //    row.DefaultCellStyle.BackColor = Color.LightSteelBlue;
                //    row.Cells["fprint_zt"].Value = "已打印";
                //}
            }
            //20150618 wjz 逻辑做了调整 end
            //2015-06-11 10;03 begin  wjz 修改目的：右键打印后，更改数据行的颜色。
        }

        private void button刷新结果_Click(object sender, EventArgs e)
        {
            //Get检验结果();
            //billSave保存样本(1, "");

            //add by wjz 20151224 刷新化验项目明细 ▽
            bllResultList获取检验结果();
            //add by wjz 20151224 刷新化验项目明细 △
        }

        private void dataGridView样本_DoubleClick(object sender, EventArgs e)
        {
            //add by wjz 20151205 移除年龄、性别的TextChanged事件 ▽
            RemoveSexAgeTextChangedEvent();
            //add by wjz 20151205 移除年龄、性别的TextChanged事件 △

            bll显示样本();

            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 ▽
            AddSexAgeTextChangedEvent();
            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 △
        }

        //add by wjz 20160226 修正数值小数位数 ▽
        private string Set小数格式(string strfitem_id, string value)
        {
            //小数位数
            //fcode	fhelp_code	fname
            //1	    NULL	保持原始值
            //2	    NULL	0 位
            //3	    NULL	1 位
            //4	    NULL	2 位
            //5	    NULL	3 位
            //6	    NULL	4 位
            //7	    NULL	5 位
            //8	    NULL	6 位

            //结果类型
            //ftype	fcode	fhelp_code	fname
            //结果类型	1	NULL	数值型
            //结果类型	2	NULL	文字型
            //结果类型	3	NULL	阴阳性型
            //结果类型	4	NULL	数值文字混合型
            string strfitem_value = "";
            //string strjglx = WWFInit.wwfRemotingDao.DbDTSelect(dtItemList, "fitem_id='" + strfitem_id + "'", "fitem_type_id");
            string strxsws = WWFInit.wwfRemotingDao.DbDTSelect(dtItemList, "fitem_id='" + strfitem_id + "'", "fvalue_dd");

            //↓ 2016-10-10 11:42:28 yufh 调整系数  添加调整系数计算 
            string strfxs = WWFInit.wwfRemotingDao.DbDTSelect(dtItemList, "fitem_id='" + strfitem_id + "'", "fxs");
            if (strfxs != "1")
                value = (Convert.ToDecimal(value) * Convert.ToDecimal(strfxs)).ToString();
            //↑ 2016-10-10 11:42:28 yufh 如果项目维护了调整系数且不等于1则参与计算，配合小数位使用

            if (strxsws == "2")
            {
                strfitem_value = Math.Round(Convert.ToDecimal(value), 0).ToString();
            }
            else if (strxsws == "3")
            {
                strfitem_value = Math.Round(Convert.ToDecimal(value), 1).ToString("0.0");
            }
            else if (strxsws == "4")
            {
                strfitem_value = Math.Round(Convert.ToDecimal(value), 2).ToString("0.00");
            }
            else if (strxsws == "5")
            {
                strfitem_value = Math.Round(Convert.ToDecimal(value), 3).ToString("0.000");
            }
            else if (strxsws == "6")
            {
                strfitem_value = Math.Round(Convert.ToDecimal(value), 4).ToString("0.0000");
            }
            else if (strxsws == "7")
            {
                strfitem_value = Math.Round(Convert.ToDecimal(value), 5).ToString("0.00000");
            }
            else if (strxsws == "8")
            {
                strfitem_value = Math.Round(Convert.ToDecimal(value), 6).ToString("0.000000");
            }
            else if (strxsws == "1")
            {
                strfitem_value = value;
            }
            else
            {
                strfitem_value = Math.Round(Convert.ToDecimal(value), 2).ToString("0.00");
            }


            return strfitem_value;
        }
        //add by wjz 20160226 修正数值小数位数 △

        private void Get检验结果()
        {
            try
            {
                JYInstrDataBll jybll接口数据读取 = new JYInstrDataBll();
                DataTable dt结果主表 = new DataTable();
                string strWhere = "";
                if (true)
                {
                    //changed by wjz 2016-02-24 样本号如果不是整数，引起异常，暂时先调整where里的条件顺序 ▽
                    //strWhere = " where 1=1 and ISNUMERIC(FResultID)<>0 and finstrid='" + this.fjy_instrComboBox_仪器.SelectedValue.ToString() + "' and fresultid = " + 
                    //    this.fjy_yb_code样本.Text + " and (CONVERT(varchar(100), FDateTime, 23) >='" + fjy_date样本日期.Value.ToString("yyyy-MM-dd") + 
                    //    "' and CONVERT(varchar(100), FDateTime, 23) <='" + fjy_date样本日期.Value.ToString("yyyy-MM-dd") + "' )  order by FResultID,FDateTime desc";

                    //changed by wjz 2016-02-20 样本号如果不是整数，引起异常（第二次修改）。对策：数据库中添加自定义函数，防止where中“fresultid = 整数”的隐式转换 ▽
                    //如果样本号不是整数，而是类似于“E01”形式的字符，则后续还需要修改此问题。
                    //strWhere = " where finstrid='" + this.fjy_instrComboBox_仪器.SelectedValue.ToString() + "' "
                    //     + " and (CONVERT(varchar(100), FDateTime, 23) >='" + fjy_date样本日期.Value.ToString("yyyy-MM-dd") +
                    //    "' and CONVERT(varchar(100), FDateTime, 23) <='" + fjy_date样本日期.Value.ToString("yyyy-MM-dd") + "' ) "
                    //    + " and (ISNUMERIC(FResultID)<>0 AND ROUND(FResultID,0)=FResultID and fresultid = " + this.fjy_yb_code样本.Text + " )"
                    //    +"  order by FResultID,FDateTime desc";
                    strWhere = " where finstrid='" + this.fjy_instrComboBox_仪器.SelectedValue.ToString() + "' "
                         + " and (CONVERT(varchar(100), FDateTime, 23) >='" + fjy_date样本日期.Value.ToString("yyyy-MM-dd") +
                        "' and CONVERT(varchar(100), FDateTime, 23) <='" + fjy_date样本日期.Value.ToString("yyyy-MM-dd") + "' ) "
                        + " and dbo.[IsCumstomEqual](FResultID," + this.fjy_yb_code样本.Text.Trim() + ")=1 "
                        + "  order by FResultID,FDateTime desc ";
                    //changed by wjz 2016-02-20 样本号如果不是整数，引起异常（第二次修改）。对策：数据库中添加自定义函数，防止where中“fresultid = 整数”的隐式转换 △
                    //changed by wjz 2016-02-24 样本号如果不是整数，引起异常，暂时先调整where里的条件顺序 △
                }
                dt结果主表 = jybll接口数据读取.BllIODT(strWhere);
                //根据主表获取明细
                if (dt结果主表 != null && dt结果主表.Rows.Count > 0)
                {
                    ArrayList lisUpdateFlag = new ArrayList();

                    for (int i = 0; i < dt结果主表.Rows.Count; i++)
                    {
                        string str结果id = dt结果主表.Rows[i]["FTaskID"].ToString();

                        this.str仪器结果FTaskID = str结果id;

                        lisUpdateFlag.Add("update Lis_Ins_Result set FTransFlag='True' FROM Lis_Ins_Result where FTaskID='" + str结果id + "'");

                        DataTable dt结果明细 = jybll接口数据读取.BllResultDT(str结果id, fjy_instrComboBox_仪器.SelectedValue.ToString());
                        //dataGridView1.DataSource = dt结果明细;
                        for (int ii = 0; ii < dt结果明细.Rows.Count; ii++)
                        {
                            if (Convert.ToString(dt结果明细.Rows[ii]["fvalue"]) == "")
                                continue;
                            string strfitem_id = "";
                            string strfitem_value = "";
                            if (dt结果明细.Rows[ii]["fitem_id"] != null)
                            {
                                strfitem_id = dt结果明细.Rows[ii]["fitem_id"].ToString();
                            }
                            if (dt结果明细.Rows[ii]["fvalue"] != null)
                            {
                                //changed by wjz 20160226 修正数值小数位数 ▽
                                //if (ww.wwf.com.Public.IsNumber(dt结果明细.Rows[ii]["fvalue"].ToString()))
                                //    strfitem_value = Convert.ToDecimal(dt结果明细.Rows[ii]["fvalue"]).ToString("0.00");
                                //else
                                //    strfitem_value = dt结果明细.Rows[ii]["fvalue"].ToString();

                                strfitem_value = dt结果明细.Rows[ii]["fvalue"].ToString();

                                if (ww.wwf.com.Public.IsNumber(strfitem_value))
                                {
                                    //strfitem_value = Convert.ToDecimal(dt结果明细.Rows[ii]["fvalue"]).ToString("0.00");
                                    strfitem_value = Set小数格式(strfitem_id, strfitem_value);
                                }
                                //changed by wjz 20160226 修正数值小数位数 △
                            }

                            DataRow[] rows = dtResult.Select("fitem_id='" + strfitem_id + "'");
                            if (rows.Length > 0)
                            {
                                //20150616 wjz 修改if中的条件 目的：允许更新项目的值（以前只允许更新空值）  20150627 对修改内容进行还原

                                //double dvalue = 0.00;
                                //try
                                //{
                                //    dvalue = Convert.ToDouble(rows[0]["fvalue"].ToString());
                                //}
                                //catch
                                //{
                                //    dvalue = 100;
                                //}

                                //changed by wjz 20160113 当结果是0的情况下，重新取结果 ▽
                                //if ((Convert.ToString(rows[0]["fvalue"]) == "" || (rows[0]["fvalue"].ToString() == "0.00")) && strfitem_value != "")

                                //del by wjz 20161031 重做的频率比较频繁
                                //if ((Convert.ToString(rows[0]["fvalue"]) == "" || (dvalue < 0.00001 && dvalue > -0.00001)) && strfitem_value != "")

                                //changed by wjz 20160113 当结果是0的情况下，重新取结果 △
                                //if (strfitem_value != "" && )  
                                string fremark = "";
                                if (rows[0]["fremark"] != null)
                                {
                                    fremark = rows[0]["fremark"].ToString();
                                }

                                if (fremark != "1")
                                {
                                    rows[0]["fvalue"] = strfitem_value;

                                    //changed by wjz 20151202 根据性别、年龄获取参考值 ▽
                                    #region old code
                                    //string str参考值 = this.blljy.Bll参考值(strfitem_id, 0, "", "");
                                    #endregion

                                    #region new code
                                    string str参考值;
                                    //if (string.IsNullOrWhiteSpace(fhz_age年龄.Text) || string.IsNullOrWhiteSpace(fhz_sex性别.Text) || (fhz_age_unittextBox.Text != "岁"))
                                    //{
                                    //    //str参考值 = this.blljy.Bll参考值(strfitem_id, 0, "", "");
                                    //    //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。▽
                                    //    //str参考值 = this.blljy.Bll参考值(strfitem_id, 0, "", "");
                                    //    str参考值 = this.blljy.Bll参考值(strfitem_id, -1, "", "");
                                    //    //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。△
                                    //}
                                    //else
                                    //{
                                    //    int age = 0;
                                    //    try
                                    //    {
                                    //        age = Convert.ToInt32(fhz_age年龄.Text.Trim());
                                    //        str参考值 = this.blljy.Bll参考值New(strfitem_id, age, fhz_sex性别.Text.Trim(), "");
                                    //    }
                                    //    catch (FormatException ex)
                                    //    {
                                    //        //str参考值 = this.blljy.Bll参考值(strfitem_id, 0, "", "");
                                    //        //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。▽
                                    //        //str参考值 = this.blljy.Bll参考值(strfitem_id, 0, "", "");
                                    //        str参考值 = this.blljy.Bll参考值(strfitem_id, -1, "", "");
                                    //        //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。△
                                    //    }
                                    //}
                                    str参考值 = GetRefValueByItemCode(strfitem_id);
                                    #endregion
                                    //changed by wjz 20151202 根据性别、年龄获取参考值 △

                                    if (!(string.IsNullOrWhiteSpace(strfitem_value)))
                                    {
                                        rows[0]["fitem_badge"] = this.blljy.Bll结果标记(strfitem_value, str参考值);
                                    }
                                }
                            }
                            else
                            {
                                DataRow drNew = this.dtResult.NewRow();
                                drNew["fresult_id"] = this.blljy.DbGuid();
                                drNew["fitem_id"] = strfitem_id;
                                drNew["fvalue"] = strfitem_value;
                                drNew["fitem_code"] = dt结果明细.Rows[ii]["Fitem"].ToString();
                                drNew["fitem_name"] = dt结果明细.Rows[ii]["fitem_name"];
                                drNew["fitem_unit"] = dt结果明细.Rows[ii]["funit_name"];
                                drNew["forder_by"] = dt结果明细.Rows[ii]["fprint_num"];

                                //changed by wjz 20151202 根据性别、年龄获取参考值 ▽
                                #region old code
                                //string str参考值 = this.blljy.Bll参考值(strfitem_id, 0, "", "");
                                #endregion

                                #region new code
                                string str参考值;
                                //if (string.IsNullOrWhiteSpace(fhz_age年龄.Text) || string.IsNullOrWhiteSpace(fhz_sex性别.Text) || (fhz_age_unittextBox.Text != "岁"))
                                //{
                                //    //str参考值 = this.blljy.Bll参考值(strfitem_id, 0, "", "");
                                //    //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。▽
                                //    //str参考值 = this.blljy.Bll参考值(strfitem_id, 0, "", "");
                                //    str参考值 = this.blljy.Bll参考值(strfitem_id, -1, "", "");
                                //    //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。△
                                //}
                                //else
                                //{
                                //    int age = 0;
                                //    try
                                //    {
                                //        age = Convert.ToInt32(fhz_age年龄.Text.Trim());
                                //        str参考值 = this.blljy.Bll参考值New(strfitem_id, age, fhz_sex性别.Text.Trim(), "");
                                //    }
                                //    catch (FormatException ex)
                                //    {
                                //        //str参考值 = this.blljy.Bll参考值(strfitem_id, 0, "", "");
                                //        //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。▽
                                //        //str参考值 = this.blljy.Bll参考值(strfitem_id, 0, "", "");
                                //        str参考值 = this.blljy.Bll参考值(strfitem_id, -1, "", "");
                                //        //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。△
                                //    }
                                //}
                                str参考值 = GetRefValueByItemCode(strfitem_id);
                                #endregion
                                //changed by wjz 20151202 根据性别、年龄获取参考值 △

                                drNew["fitem_ref"] = str参考值;
                                if (strfitem_value.Equals("") || strfitem_value.Length == 0 || strfitem_value == null) { }
                                else
                                {
                                    drNew["fitem_badge"] = this.blljy.Bll结果标记(strfitem_value, str参考值);
                                }
                                this.dtResult.Rows.Add(drNew);
                            }
                        }
                        //bllResultShow显示结果颜色();
                    }

                    //add by wjz 计算项目的添加 ▽
                    //获取计算项目列表
                    //循环列表，判断dtResult中是否存在计算项目，筛选出未出现过的计算项目
                    //循环未出现过的计算项目，提取计算公式中的所有的项目名称，并判断所有的项目名称是否存在于dtResult列表中，若是将此计算项目加入dtResult，若否跳过。
                    //从计算公式中提取项目名称：公式中的项目名称都被[]包裹着（正常情况下是这样的），根据此特征去提取字符串

                    //如何将dtResult中的项目按照print_num进行排序？

                    // fitem_id,FItem,fitem_name,funit_name,fprint_num,fref,fjx_formula
                    DataTable dtCal = jybll接口数据读取.BllGetCaluatedItems(fjy_instrComboBox_仪器.SelectedValue.ToString());
                    List<string> list = new List<string>();

                    //bool isAddItem = false;
                    for (int index = 0; index < dtCal.Rows.Count; index++)
                    {
                        // fitem_id,FItem,fitem_name,funit_name,fprint_num,fref,fjx_formula
                        DataRow[] rows = dtResult.Select("fitem_id='" + dtCal.Rows[index]["fitem_id"].ToString() + "'");
                        if (rows.Length > 0)
                        {
                            continue;
                        }
                        else
                        {
                            list.Clear();
                            SplitItems(dtCal.Rows[index]["fjx_formula"].ToString(), list);

                            //判断列表中的项目是否全部存在于dtResult
                            bool ret = CheckExist(dtResult, list);
                            if (ret)
                            {
                                //调整排序时使用
                                //isAddItem = true;

                                string strItemValue = bll公式计算值ByDataTable(dtCal.Rows[index]["fjx_formula"].ToString(), dtResult);

                                DataRow drNew = this.dtResult.NewRow();
                                drNew["fresult_id"] = this.blljy.DbGuid();
                                drNew["fitem_id"] = dtCal.Rows[index]["fitem_id"].ToString();

                                //计算参考值
                                drNew["fvalue"] = strItemValue;

                                drNew["fitem_code"] = dtCal.Rows[index]["Fitem"].ToString();
                                drNew["fitem_name"] = dtCal.Rows[index]["fitem_name"];
                                drNew["fitem_unit"] = dtCal.Rows[index]["funit_name"];
                                drNew["forder_by"] = dtCal.Rows[index]["fprint_num"];
                                string strTemp参考值 = GetRefValueByItemCode(dtCal.Rows[index]["fitem_id"].ToString());

                                drNew["fitem_ref"] = strTemp参考值;
                                if (string.IsNullOrWhiteSpace(strItemValue)) { }
                                else
                                {
                                    drNew["fitem_badge"] = this.blljy.Bll结果标记(strItemValue, strTemp参考值);
                                }
                                this.dtResult.Rows.Add(drNew);
                            }
                        }
                    }

                    //未找到合适的方案，暂时不调整排序
                    //if (isAddItem)
                    //{
                    //    dtResult.DefaultView.Sort = "forder_by asc";
                    //}

                    //add by wjz 计算项目的添加 △

                    if (lisUpdateFlag.Count > 0)
                    {
                        ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(ww.wwf.wwfbll.WWFInit.strDBConn, lisUpdateFlag);
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        //add by wjz 计算项目的添加 ▽
        private bool CheckExist(DataTable dt, List<string> list)
        {
            if (list.Count == 0)
            {
                return false;
            }

            bool ret = true;

            for (int inner = 0; inner < list.Count; inner++)
            {
                DataRow[] releatedItem = dt.Select("fitem_code ='" + list[inner] + "'");
                if (releatedItem.Length == 0)
                {
                    ret = false;
                    break;
                }
            }
            return ret;
        }

        private void SplitItems(string formula, List<string> items)
        {
            int beg = -1;

            for (int index = 0; index < formula.Length; index++)
            {
                if (formula[index] == '[')
                {
                    beg = index;
                }
                else if (formula[index] == ']')
                {
                    //end = index;
                    if (beg != -1 && index > beg)
                    {
                        string sub = formula.Substring(beg + 1, index - beg - 1);
                        items.Add(sub);
                        beg = -1;
                    }
                }
                else
                { }
            }
        }
        //add by wjz 计算项目的添加 △

        /// <summary>
        /// 双击选择常用取值
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewResult_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                if (Convert.ToString(dataGridViewResult["fvalue", e.RowIndex].EditedFormattedValue) == "")
                {
                    MessageBox.Show("选择常用取值！，该功能暂不完善，未开放！");
                    //dataGridViewResult["fvalue", e.RowIndex].Value = "112";
                    #region 结束当前行编辑，用于刷新缓存值，解决赋值问题
                    if (e.RowIndex == dataGridViewResult.Rows.Count - 1)
                        this.dataGridViewResult.CurrentCell = null;
                    else
                        SendKeys.Send("{Down}");
                    #endregion
                }
            }

        }

        private void dataGridView样本_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView样本.Rows)
            {
                string str打印状态 = row.Cells["fprint_zt"].Value.ToString();
                if (str打印状态.Equals("已打印"))
                {
                    row.DefaultCellStyle.BackColor = Color.LightSteelBlue;
                }
            }
        }

        private bool IsDataTableChanged(DataTable dt)
        {
            bool ret = false;

            foreach (DataRow dr in dt.Rows)
            {
                if (dr.RowState != DataRowState.Unchanged)
                {
                    ret = true;
                    break;
                }
            }

            return ret;
        }

        //当焦点位于“备注”文本框，按下回车调用此函数
        private void SaveSampleInfo()
        {
            //首先判断左侧病人信息一栏 任一文本框手动输入了信息 或者 中间检验项目是否有值
            //如果是，则执行保存操作，否则不执行
            //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 ▽
            //if (fhz_type_idtextBox_name.Text != "" ||
            if (this.cboPatientType.Text != "" ||
                //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 △
                fhz_zyh住院号.Text != "" ||
                fhz_name姓名.Text != "" ||
                //changed by wjz 20160128 性别控件由TextBox换为ComboBox ▽
                //fhz_sex性别.Text != "" ||     //=================================
                this.fhz_sex性别Combo.Text != "" ||
                //changed by wjz 20160128 性别控件由TextBox换为ComboBox △
                fhz_age年龄.Text != "" ||

                //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 ▽
                //fjy_sf_typetextBox.Text != "" ||
                this.fjy_sf_typetextBoxCombox.Text != "" ||
                //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 △

                //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 ▽
                //fhz_dept科室.Text != "" ||
                this.fhz_dept科室cbo.Text != "" ||
                //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 △
                fhz_bedtextBox.Text != "" ||
                fapply_idtextBox.Text != "" ||

                //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
                //fjy_yb_typetextBox.Text != "" ||
                this.fjy_yb_typeComboBox.Text != "" ||
                //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 △

                //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
                //fapply_user_id送检医师.Text != "" ||
                fapply_user_id送检医师cbo.Text != "" ||
                //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 △
                //fjy_user_idtextBox.Text != "" ||

                //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
                //fchenk_user_idtextBox.Text != "" ||
                this.fchenk_user_idComboBox.Text != "" ||
                //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △

                fjy_lczdtextBox.Text != "" ||
                fremarktextBox.Text != "" ||
                dtResult.Rows.Count > 0 //或者检验项目表中有检验项目
                )
            {
                billSave保存样本(1, "保存");

            }
            else
            {
                try
                {
                    int int样本号 = Convert.ToInt32(fjy_yb_code样本.Text) + 1;
                    fjy_yb_code样本.Text = int样本号.ToString();
                }
                catch (Exception ex)
                {
                    WWMessage.MessageShowError("样本号不是数字。\n" + ex.Message.ToString());
                }
            }
            this.ActiveControl = fjy_yb_code样本;
        }

        //20150629 wjz add
        private void fapply_idtextBox_TextChanged(object sender, EventArgs e)
        {
            string str住院号 = this.fhz_zyh住院号.Text;
            string str备注 = this.fremarktextBox.Text;
            string str申请单号 = this.fapply_idtextBox.Text;

            //申请单号长度不是12的时候，不进行任何处理
            if (str申请单号.Length != 12)
            {
                return;
            }
            //MessageBox.Show("去掉扫码枪的回车！", "消息", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            //add by wjz 20151205 移除年龄、性别的TextChanged事件 ▽
            RemoveSexAgeTextChangedEvent();
            //add by wjz 20151205 移除年龄、性别的TextChanged事件 △

            //changed by wjz 20160121 去掉对住院号、备注信息的判断 ▽
            //if (string.IsNullOrWhiteSpace(str住院号) && string.IsNullOrWhiteSpace(str备注))
            //{
            DataTable dtApply病人信息 = this.bllPatient.BllPatientDTByApplyNo(str申请单号);
            if (dtApply病人信息 != null && dtApply病人信息.Rows.Count > 0)
            {
                //取到住院病人信息后进行计费
                if (b扫码计费)
                {
                    try
                    {
                        //处理有组合的计费条码
                        DataTable dtApplyList = bllPatient.BllPatientDTByApplyList(str申请单号); //病人检验组合列表
                        if (dtApplyList != null && dtApplyList.Rows.Count > 0)
                        {
                            StringBuilder sb = new StringBuilder();
                            foreach (DataRow dr in dtApplyList.Rows)
                            {
                                sb.Append(dr["fitem_group"]);
                                string strProc = @"exec dbo.uSp_化验项目计费 '0','" + LoginBLL.strPersonID + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "',3,'" + dr["fapply_id"].ToString() + "','" + dtApply病人信息.Rows[0]["fhz_zyh"].ToString() + "',1 ";
                                ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strCHisDBConn, strProc);
                            }
                            MessageBox.Show("计费成功！计费项目：【" + sb.ToString() + "】");
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("计费失败！请联系护理处进行重新计费！");
                    }
                }
                //住院号
                modeljy.fhz_住院号 = dtApply病人信息.Rows[0]["fhz_zyh"].ToString();
                this.fhz_zyh住院号.Text = dtApply病人信息.Rows[0]["fhz_zyh"].ToString();

                modeljy.fhz_id = dtApply病人信息.Rows[0]["fhz_id"].ToString();

                this.fhz_age年龄.Text = dtApply病人信息.Rows[0]["fage"].ToString();//年龄

                //changed by wjz 20160122 修改年龄单位的赋值 ▽
                //年龄单位
                //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
                //this.fhz_age_unittextBox.Text = dtApply病人信息.Rows[0]["fage_unit"].ToString();
                //modeljy.fhz_年龄单位 = dtApply病人信息.Rows[0]["fage_unit"].ToString();
                modeljy.fhz_年龄单位 = bllPatNameByCode("年龄单位", dtApply病人信息.Rows[0]["fage_unit"].ToString());
                this.comboBoxAgeUnit.SelectedValue = modeljy.fhz_年龄单位;
                //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △
                //if (this.fhz_age_unittextBox.Text.Trim() == "岁")
                //{
                //    modeljy.fhz_年龄单位 = "1";
                //}
                //else if (this.fhz_age_unittextBox.Text.Trim() == "月")
                //{
                //    modeljy.fhz_年龄单位 = "2";
                //}
                //else if (this.fhz_age_unittextBox.Text.Trim() == "天")
                //{
                //    modeljy.fhz_年龄单位 = "3";
                //}
                //changed by wjz 20160122 修改年龄单位的赋值 △

                this.fhz_name姓名.Text = dtApply病人信息.Rows[0]["fname"].ToString();//姓名
                this.fhz_bedtextBox.Text = dtApply病人信息.Rows[0]["fbed_num"].ToString();//床号

                //性别
                modeljy.fhz_性别 = blPatCodeByName("性别", dtApply病人信息.Rows[0]["fsex"].ToString());

                //changed by wjz 20160128 性别控件由TextBox换为ComboBox ▽
                //this.fhz_sex性别.Text = dtApply病人信息.Rows[0]["fsex"].ToString();//================
                this.fhz_sex性别Combo.SelectedValue = modeljy.fhz_性别;
                //changed by wjz 20160128 性别控件由TextBox换为ComboBox △

                //病人类别
                //门诊病人的类型设定为了1，住院病人的类型为3，这点与页面程序的选项保持一致
                modeljy.fhz_患者类型id = blPatCodeByName("病人类型", dtApply病人信息.Rows[0]["ftype_id"].ToString());
                //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 ▽
                //this.fhz_type_idtextBox_name.Text = dtApply病人信息.Rows[0]["ftype_id"].ToString();
                this.cboPatientType.SelectedValue = modeljy.fhz_患者类型id;
                //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 △
                //科室
                modeljy.fhz_dept患者科室 = blPatCodeByName("科室", dtApply病人信息.Rows[0]["fapply_dept_id"].ToString());

                //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 ▽
                //this.fhz_dept科室.Text = dtApply病人信息.Rows[0]["fapply_dept_id"].ToString();
                try
                {
                    this.fhz_dept科室cbo.SelectedValue = this.modeljy.fhz_dept患者科室;
                }
                catch
                {
                    this.fhz_dept科室cbo.SelectedValue = "";
                }
                //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 △

                //人员
                modeljy.fapply_user_id = dtApply病人信息.Rows[0]["fapply_user_id"].ToString();

                //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
                //this.fapply_user_id送检医师.Text = bllPatNameByCode("人员", modeljy.fapply_user_id);
                try
                {
                    this.fapply_user_id送检医师cbo.EditValue = this.modeljy.fapply_user_id;
                }
                catch
                {
                    this.fapply_user_id送检医师cbo.EditValue = "";
                }
                //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 △

                //临床诊断
                //modeljy.fjy_lczd = "";
                //changed by wjz 20160122 修改疾病名称的赋值，对于通过申请号获取的病人信息，不再保存疾病信息 ▽
                //this.fjy_lczdtextBox.Text = dtApply病人信息.Rows[0]["fdiagnose"].ToString();
                //this.fjy_lczdtextBox.Text = "";
                //changed by wjz 20160122 修改疾病名称的赋值，对于通过申请号获取的病人信息，不再保存疾病信息 △

                this.modeljy.fjy_lczd = dtApply病人信息.Rows[0]["fdiagnose"].ToString();
                this.fjy_lczdtextBox.Text = this.bllPatNameByCode("临床诊断", this.modeljy.fjy_lczd);

                //身份证号暂时设置为空
                //changed by wjz 20160122 修改身份证的获取 ▽
                //modeljy.S身份证号 = "";
                modeljy.S身份证号 = dtApply病人信息.Rows[0]["sfzh"].ToString();
                this.txt身份证号.Text = dtApply病人信息.Rows[0]["sfzh"].ToString();
                if (dtApply病人信息.Columns.Contains("card_no"))
                {
                    modeljy.f1 = dtApply病人信息.Rows[0]["card_no"].ToString();
                    this.txt电子健康卡.Text = dtApply病人信息.Rows[0]["card_no"].ToString();
                }
                //changed by wjz 20160122 修改身份证的获取 △

                //add by wjz 20160526 根据条码，调整申请送检时间 ▽
                try
                {
                    DateTime datetime送检 = Convert.ToDateTime(dtApply病人信息.Rows[0]["fcreate_time"].ToString());
                    fapply_timeDateTimePicker.Value = datetime送检;
                    fsampling_timedateTimePicker.Value = datetime送检;
                }
                catch
                {

                }
                //fapply_timeDateTimePicker.Value = 
                //    fsampling_timedateTimePicker
                //add by wjz 20160526 根据条码，调整申请送检时间 △

                //add by wjz 20151208 更新病人信息的同时，更新参考值 ▽
                SetRef();
                //add by wjz 20151208 更新病人信息的同时，更新参考值 △
            }
            //}
            //changed by wjz 20160121 去掉对住院号、备注信息的判断 △

            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 ▽

            AddSexAgeTextChangedEvent();
            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 △
        }


        //add by wjz 20151022 ▽
        private void fjy_date样本日期_Enter(object sender, EventArgs e)
        {
            fjy_date样本日期.Tag = fjy_date样本日期.Text;
        }

        private void fjy_date样本日期_Leave(object sender, EventArgs e)
        {
            if (fjy_date样本日期.Text.Equals(fjy_date样本日期.Tag.ToString()))
            {

                //不进行任何处理
                fjy_date样本日期.Tag = "";
            }
            else
            {
                //add by wjz 20151205 移除年龄、性别的TextChanged事件 ▽
                RemoveSexAgeTextChangedEvent();
                //add by wjz 20151205 移除年龄、性别的TextChanged事件 △

                bllYBList();//左上角日期控件Leave事件

                //add by wjz 20151205 恢复年龄、性别的TextChanged事件 ▽
                AddSexAgeTextChangedEvent();
                //add by wjz 20151205 恢复年龄、性别的TextChanged事件 △
            }
        }

        private void fjy_yb_code样本_Enter(object sender, EventArgs e)
        {
            fjy_yb_code样本.Tag = fjy_yb_code样本.Text;
        }
        //add by wjz 20151022 △

        //add by wjz 20151202 项目参考值根据性别、年龄变化 ▽
        private void fhz_sex性别_TextChanged(object sender, EventArgs e)
        {
            //if (string.IsNullOrWhiteSpace(fhz_sex性别.Text) || string.IsNullOrWhiteSpace(fhz_age年龄.Text) || (fhz_age_unittextBox.Text != "岁"))
            //{
            //}
            //else
            //{
            //    //设定参考值
            //    SetRef();
            //}

            //del by wjz 20160128 性别控件由TextBox换为ComboBox ▽
            //string temp = fhz_sex性别.Text;//======================================
            //del by wjz 20160128 性别控件由TextBox换为ComboBox △
            SetRef();
        }

        private void fhz_age年龄_TextChanged(object sender, EventArgs e)
        {
            //if (string.IsNullOrWhiteSpace(fhz_sex性别.Text) || string.IsNullOrWhiteSpace(fhz_age年龄.Text) || (fhz_age_unittextBox.Text != "岁"))
            //{
            //}
            //else
            //{
            //    //设定参考值
            //    SetRef();
            //}
            SetRef();
        }

        private void fhz_age_unittextBox_TextChanged(object sender, EventArgs e)
        {
            //if (string.IsNullOrWhiteSpace(fhz_sex性别.Text) || string.IsNullOrWhiteSpace(fhz_age年龄.Text) || (fhz_age_unittextBox.Text != "岁"))
            //{
            //}
            //else
            //{
            //    //设定参考值
            //    SetRef();
            //}
            SetRef();
        }


        private void RemoveSexAgeTextChangedEvent()
        {
            fhz_age年龄.TextChanged -= fhz_age年龄_TextChanged;
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
            //fhz_age_unittextBox.TextChanged -= fhz_age_unittextBox_TextChanged;
            this.comboBoxAgeUnit.SelectedValueChanged -= comboBoxAgeUnit_SelectedValueChanged;
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △

            //changed by wjz 20160128 性别控件由TextBox换为ComboBox ▽
            //fhz_sex性别.TextChanged -= fhz_sex性别_TextChanged;
            fhz_sex性别Combo.SelectedValueChanged -= fhz_sex性别Combo_SelectedValueChanged;
            //changed by wjz 20160128 性别控件由TextBox换为ComboBox △
        }

        private void AddSexAgeTextChangedEvent()
        {
            //防止重复添加事件 ▽
            fhz_age年龄.TextChanged -= fhz_age年龄_TextChanged;
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
            //fhz_age_unittextBox.TextChanged -= fhz_age_unittextBox_TextChanged;
            this.comboBoxAgeUnit.SelectedValueChanged -= comboBoxAgeUnit_SelectedValueChanged;
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △

            //changed by wjz 20160128 性别控件由TextBox换为ComboBox ▽
            //fhz_sex性别.TextChanged -= fhz_sex性别_TextChanged;
            fhz_sex性别Combo.SelectedValueChanged -= fhz_sex性别Combo_SelectedValueChanged;
            //changed by wjz 20160128 性别控件由TextBox换为ComboBox △
            //防止重复添加事件 △

            fhz_age年龄.TextChanged += fhz_age年龄_TextChanged;
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
            //fhz_age_unittextBox.TextChanged += fhz_age_unittextBox_TextChanged;
            this.comboBoxAgeUnit.SelectedValueChanged += comboBoxAgeUnit_SelectedValueChanged;
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △

            //changed by wjz 20160128 性别控件由TextBox换为ComboBox ▽
            //fhz_sex性别.TextChanged += fhz_sex性别_TextChanged;
            fhz_sex性别Combo.SelectedValueChanged += fhz_sex性别Combo_SelectedValueChanged;
            //changed by wjz 20160128 性别控件由TextBox换为ComboBox △
        }


        private void SetRef()
        {
            string strfitem_id = null;
            string strfitem_value = null;
            string str参考值 = null;
            float age = -1;
            try
            {
                //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
                //if (fhz_age_unittextBox.Text == "岁")
                //{
                //    age = Convert.ToInt32(fhz_age年龄.Text.Trim());
                //    if(age == 0)
                //    {
                //        age = -1;
                //    }
                //}
                //else if(fhz_age_unittextBox.Text == "月")
                //{
                //    age = Convert.ToInt32(fhz_age年龄.Text.Trim()) / 12.0F;
                //}
                //else if(fhz_age_unittextBox.Text == "天")
                //{
                //    age = Convert.ToInt32(fhz_age年龄.Text.Trim()) / 365F;
                //}
                if (comboBoxAgeUnit.Text == "岁")
                {
                    age = Convert.ToInt32(fhz_age年龄.Text.Trim());
                    if (age == 0)
                    {
                        age = -1;
                    }
                }
                else if (comboBoxAgeUnit.Text == "月")
                {
                    age = Convert.ToInt32(fhz_age年龄.Text.Trim()) / 12.0F;
                }
                else if (comboBoxAgeUnit.Text == "天")
                {
                    age = Convert.ToInt32(fhz_age年龄.Text.Trim()) / 365F;
                }
                //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △
            }
            catch
            {
                age = -1;
                //return;
            }

            // add by wjz 王庄大生化设备即化验血又化验尿，而且“淀粉酶”在血、尿化验中均有，且参考值不一样，针对此情况进行调整，类似于此情况都适用 ▽
            string strSampleTypeid = "";
            //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
            //if (string.IsNullOrWhiteSpace(modeljy.fjy_yb_type) || string.IsNullOrWhiteSpace(fjy_yb_typetextBox.Text))
            if (string.IsNullOrWhiteSpace(modeljy.fjy_yb_type) || string.IsNullOrWhiteSpace(this.fjy_yb_typeComboBox.Text))
            //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
            { }
            else
            {
                strSampleTypeid = modeljy.fjy_yb_type.Trim();
            }
            // add by wjz 王庄大生化设备即化验血又化验尿，而且“淀粉酶”在血、尿化验中均有，且参考值不一样，针对此情况进行调整，类似于此情况都适用 △

            for (int index = 0; index < dtResult.Rows.Count; index++)
            {
                strfitem_value = dtResult.Rows[index]["fvalue"].ToString();
                strfitem_id = dtResult.Rows[index]["fitem_id"].ToString();

                // add by wjz 王庄大生化设备即化验血又化验尿，而且“淀粉酶”在血、尿化验中均有，且参考值不一样，针对此情况进行调整 ▽
                #region old code
                //str参考值 = this.blljy.Bll参考值New(strfitem_id, age, fhz_sex性别.Text.Trim(), "");
                #endregion

                //changed by wjz 20160128 性别控件由TextBox换为ComboBox ▽
                //str参考值 = this.blljy.Bll参考值New(strfitem_id, age, fhz_sex性别.Text.Trim(), strSampleTypeid);//==============
                str参考值 = this.blljy.Bll参考值New(strfitem_id, age, fhz_sex性别Combo.Text.Trim(), strSampleTypeid);
                //changed by wjz 20160128 性别控件由TextBox换为ComboBox △
                // add by wjz 王庄大生化设备即化验血又化验尿，而且“淀粉酶”在血、尿化验中均有，且参考值不一样，针对此情况进行调整 △

                dtResult.Rows[index]["fitem_ref"] = str参考值;

                if (!(string.IsNullOrWhiteSpace(strfitem_value)))
                {
                    dtResult.Rows[index]["fitem_badge"] = this.blljy.Bll结果标记(strfitem_value, str参考值);
                }
            }
        }


        private string GetRefValueByItemCode(string itemid)
        {
            #region 修改此方法的内部实现，原因：如果化验项目的参考值只与性别有关，参考值可能回取错
            //string str参考值 = "";
            //if (string.IsNullOrWhiteSpace(fhz_age年龄.Text) || string.IsNullOrWhiteSpace(fhz_sex性别.Text)
            //        || string.IsNullOrWhiteSpace(fhz_age_unittextBox.Text) || (fhz_age_unittextBox.Text == "岁" && fhz_age年龄.Text.Trim() == "0"))
            //{
            //    //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。▽
            //    //str参考值 = this.blljy.Bll参考值(dataGridViewResult.Rows[i].Cells["fitem_id"].Value.ToString(), 0, "", "");
            //    str参考值 = this.blljy.Bll参考值(itemid, -1, "", "");
            //    //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。△
            //}
            //else
            //{
            //    float age = -1;
            //    string strsex = "";
            //    try
            //    {
            //        if (fhz_age_unittextBox.Text.Trim() == "月")
            //        {
            //            age = Convert.ToInt32(fhz_age年龄.Text.Trim()) / 12.0F;
            //        }
            //        else if (fhz_age_unittextBox.Text.Trim() == "天")
            //        {
            //            age = Convert.ToInt32(fhz_age年龄.Text.Trim()) / 365.0F;
            //        }
            //        else if (fhz_age_unittextBox.Text.Trim() == "岁")
            //        {
            //            age = Convert.ToInt32(fhz_age年龄.Text.Trim());
            //        }

            //        strsex = fhz_sex性别.Text.Trim();
            //        //str参考值 = this.blljy.Bll参考值New(dataGridViewResult.Rows[i].Cells["fitem_id"].Value.ToString(), age, fhz_sex性别.Text.Trim(), "");
            //    }
            //    catch (Exception ex)
            //    {
            //        age = -1;
            //        strsex = "";
            //        //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。▽
            //        //str参考值 = this.blljy.Bll参考值(dataGridViewResult.Rows[i].Cells["fitem_id"].Value.ToString(), 0, "", "");
            //        //str参考值 = this.blljy.Bll参考值(dataGridViewResult.Rows[i].Cells["fitem_id"].Value.ToString(), -1, "", "");
            //        //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。△
            //    }

            //    str参考值 = this.blljy.Bll参考值New(itemid, age, strsex, "");
            //}
            //return str参考值;
            #endregion

            //add by wjz 20160227 修改获取参考值的方式，，先取dtItemList里的数据，如果参考值不跟性别、年龄、样本等关联，则直接从dtItemList里取参考值 ▽
            DataRow[] drItems = dtItemList.Select("fitem_id='" + itemid + "'");
            DataRow drItem = drItems[0];
            string fref_if_age = drItem["fref_if_age"].ToString();
            string fref_if_sex = drItem["fref_if_sex"].ToString();
            string fref_if_sample = drItem["fref_if_sample"].ToString();
            string fref_if_method = drItem["fref_if_method"].ToString();
            string fref = drItem["fref"].ToString();

            if (fref_if_age == "0" && fref_if_sex == "0" && fref_if_sample == "0" && fref_if_method == "0")
            {
                return fref;
            }
            //add by wjz 20160227 修改获取参考值的方式，，先取dtItemList里的数据，如果参考值不跟性别、年龄、样本等关联，则直接从dtItemList里取参考值 △

            //add by wjz 20151224 调整此方法的内部实现 ▽
            string strSex = "";
            string strSampleTypeid = "";
            float fAge = -1;
            string str参考值 = "";

            #region 计算年龄
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
            //if (string.IsNullOrWhiteSpace(fhz_age年龄.Text) || string.IsNullOrWhiteSpace(fhz_age_unittextBox.Text) 
            //    || (fhz_age_unittextBox.Text == "岁" && fhz_age年龄.Text.Trim() == "0"))
            //{
            //    fAge = -1f;
            //}
            //else
            //{
            //    try
            //    {
            //        if (fhz_age_unittextBox.Text.Trim() == "月")
            //        {
            //            fAge = Convert.ToInt32(fhz_age年龄.Text.Trim()) / 12.0F;
            //        }
            //        else if (fhz_age_unittextBox.Text.Trim() == "天")
            //        {
            //            fAge = Convert.ToInt32(fhz_age年龄.Text.Trim()) / 365.0F;
            //        }
            //        else if (fhz_age_unittextBox.Text.Trim() == "岁")
            //        {
            //            fAge = Convert.ToInt32(fhz_age年龄.Text.Trim());
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        fAge = -1f;
            //    }
            //}
            if (string.IsNullOrWhiteSpace(fhz_age年龄.Text) || string.IsNullOrWhiteSpace(comboBoxAgeUnit.Text)
                || (comboBoxAgeUnit.Text == "岁" && fhz_age年龄.Text.Trim() == "0"))
            {
                fAge = -1f;
            }
            else
            {
                try
                {
                    if (comboBoxAgeUnit.Text.Trim() == "月")
                    {
                        fAge = Convert.ToInt32(fhz_age年龄.Text.Trim()) / 12.0F;
                    }
                    else if (comboBoxAgeUnit.Text.Trim() == "天")
                    {
                        fAge = Convert.ToInt32(fhz_age年龄.Text.Trim()) / 365.0F;
                    }
                    else if (comboBoxAgeUnit.Text.Trim() == "岁")
                    {
                        fAge = Convert.ToInt32(fhz_age年龄.Text.Trim());
                    }
                }
                catch (Exception ex)
                {
                    fAge = -1f;
                }
            }
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △
            #endregion

            #region 计算性别
            //changed by wjz 20160128 性别控件由TextBox换为ComboBox ▽
            //if (string.IsNullOrWhiteSpace(fhz_sex性别.Text))//================
            //{
            //    strSex = "";
            //}
            //else
            //{
            //    strSex = fhz_sex性别.Text.Trim();//===========================
            //}
            strSex = this.fhz_sex性别Combo.Text.Trim();
            //changed by wjz 20160128 性别控件由TextBox换为ComboBox △
            #endregion

            #region 获取样本
            //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
            //if (string.IsNullOrWhiteSpace(fjy_yb_typetextBox.Text))
            //{
            //    strSampleTypeid = "";
            //}
            if (string.IsNullOrWhiteSpace(this.fjy_yb_typeComboBox.Text))
            {
                strSampleTypeid = "";
            }
            //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 △
            else
            {
                strSampleTypeid = modeljy.fjy_yb_type;
            }
            #endregion

            str参考值 = this.blljy.Bll参考值New(itemid, fAge, strSex, strSampleTypeid);
            return str参考值;
            //add by wjz 20151224 调整此方法的内部实现 △

        }

        //
        private void btnSave_Click(object sender, EventArgs e)
        {
            //add by wjz 20151205 移除年龄、性别的TextChanged事件 ▽
            RemoveSexAgeTextChangedEvent();
            //add by wjz 20151205 移除年龄、性别的TextChanged事件 △

            if (fjy_ztlabel.Text == "已审核")
            {
                WWMessage.MessageShowWarning("样本已审核，不再允许执行“保存”操作。");
            }
            else
            {
                billSave保存样本(2, "保存");
            }
            //this.ActiveControl = fhz_type_idtextBox_name;  //20150613 wjz 
            this.ActiveControl = fjy_yb_code样本;

            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 ▽
            AddSexAgeTextChangedEvent();
            //add by wjz 20151205 恢复年龄、性别的TextChanged事件 △
        }
        //add by wjz 20151202 项目参考值根据性别、年龄变化 △

        //add by wjz 20160128 性别控件由TextBox换为ComboBox ▽
        private void fhz_sex性别Combo_SelectedValueChanged(object sender, EventArgs e)
        {
            //add by wjz 20160308 手工录入时性别不能保存 ▽
            modeljy.fhz_性别 = this.fhz_sex性别Combo.SelectedValue.ToString();
            //add by wjz 20160308 手工录入时性别不能保存 △
            SetRef();
        }

        private void fhz_sex性别Combo_Enter(object sender, EventArgs e)
        {
            this.fhz_sex性别Combo.BackColor = System.Drawing.Color.Bisque;
        }

        private void fhz_sex性别Combo_Leave(object sender, EventArgs e)
        {
            string sexText = this.fhz_sex性别Combo.Text.Trim();

            if (sexText == "1")
            {
                this.fhz_sex性别Combo.SelectedValue = "1";
                modeljy.fhz_性别 = "1";
            }
            if (sexText == "2")
            {
                this.fhz_sex性别Combo.SelectedValue = "2";
                modeljy.fhz_性别 = "2";
            }
            else if (sexText == "")
            {
                this.fhz_sex性别Combo.SelectedValue = "";
                modeljy.fhz_性别 = "";
            }
            else
            { }

            this.fhz_sex性别Combo.BackColor = System.Drawing.Color.White;
        }
        //add by wjz 20160128 性别控件由TextBox换为ComboBox △

        //add by wjz 20160128 病人类型控件由TextBox换为ComboBox ▽
        private void cboPatientType_Enter(object sender, EventArgs e)
        {
            this.cboPatientType.BackColor = System.Drawing.Color.Bisque;
        }

        private void cboPatientType_Leave(object sender, EventArgs e)
        {
            string type = this.cboPatientType.Text.Trim();
            if (type == "1")
            {
                this.cboPatientType.SelectedValue = "1";
            }
            else if (type == "2")
            {
                this.cboPatientType.SelectedValue = "2";
            }
            else if (type == "3")
            {
                this.cboPatientType.SelectedValue = "3";
            }
            else if (type == "4")
            {
                this.cboPatientType.SelectedValue = "4";
            }
            else if (type == "")
            {
                this.cboPatientType.SelectedValue = "";
            }
            else
            { }
            modeljy.fhz_患者类型id = this.cboPatientType.SelectedValue.ToString();
            this.cboPatientType.BackColor = System.Drawing.Color.White;
        }

        private void cboPatientType_SelectedValueChanged(object sender, EventArgs e)
        {
            this.modeljy.fhz_患者类型id = this.cboPatientType.SelectedValue.ToString();
        }
        //add by wjz 20160128 病人类型控件由TextBox换为ComboBox △

        //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
        private void comboBoxAgeUnit_Enter(object sender, EventArgs e)
        {
            this.comboBoxAgeUnit.BackColor = System.Drawing.Color.Bisque;
        }

        private void comboBoxAgeUnit_Leave(object sender, EventArgs e)
        {
            string ageunit = this.comboBoxAgeUnit.Text.Trim();
            if (ageunit == "1")
            {
                this.comboBoxAgeUnit.SelectedValue = "1";
            }
            else if (ageunit == "2")
            {
                this.comboBoxAgeUnit.SelectedValue = "2";
            }
            else if (ageunit == "3")
            {
                this.comboBoxAgeUnit.SelectedValue = "3";
            }
            else if (ageunit == "")
            {
                this.comboBoxAgeUnit.SelectedValue = "";
            }
            else
            { }

            this.modeljy.fhz_年龄单位 = this.comboBoxAgeUnit.SelectedValue.ToString();

            this.comboBoxAgeUnit.BackColor = System.Drawing.Color.White;
        }

        private void comboBoxAgeUnit_SelectedValueChanged(object sender, EventArgs e)
        {
            this.modeljy.fhz_年龄单位 = this.comboBoxAgeUnit.SelectedValue.ToString();
            SetRef();
        }

        //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △
        //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 ▽
        private void InitDept()
        {
            DataTable dtDept = null;
            try
            {

                DataSet ds = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn,
                    "SELECT '' fdept_id,'' fname union all SELECT fdept_id,fname FROM WWF_DEPT where fuse_flag='1'");
                dtDept = ds.Tables[0];

                this.fhz_dept科室cbo.DataSource = dtDept;
            }
            catch
            {
                dtDept = new DataTable();
                dtDept.Columns.Add("fdept_id");
                dtDept.Columns.Add("fname");

                DataRow drSpace = dtDept.NewRow();
                drSpace["fdept_id"] = "";
                drSpace["fname"] = "";
                dtDept.Rows.Add(drSpace);

                this.fhz_dept科室cbo.DataSource = dtDept;

                fhz_dept科室cbo.SelectedValueChanged -= fhz_dept科室cbo_SelectedValueChanged;
                this.fhz_dept科室cbo.SelectedValue = "";
                fhz_dept科室cbo.SelectedValueChanged += fhz_dept科室cbo_SelectedValueChanged;
            }
        }

        private void fhz_dept科室cbo_SelectedValueChanged(object sender, EventArgs e)
        {
            this.modeljy.fhz_dept患者科室 = this.fhz_dept科室cbo.SelectedValue.ToString();
        }
        //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 △

        //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
        private void InitApplyUserID()
        {
            DataTable dtApplyUser = null;
            try
            {

                //DataSet ds = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn,
                //    "SELECT '' fperson_id,'' fname union all SELECT [fperson_id],[fname] FROM [WWF_PERSON] WHERE fuse_flag = 1 order by fname ");
                //dtApplyUser = ds.Tables[0];
                //dtApplyUser = dtDoctor.Copy();//减少数据库访问次数

                this.fapply_user_id送检医师cbo.Properties.DataSource = list医生;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //dtApplyUser = new DataTable();
                //dtApplyUser.Columns.Add("fperson_id");
                //dtApplyUser.Columns.Add("fname");

                //DataRow drSpace = dtApplyUser.NewRow();
                //drSpace["fperson_id"] = "";
                //drSpace["fname"] = "";
                //dtApplyUser.Rows.Add(drSpace);

                //this.fapply_user_id送检医师cbo.DataSource = dtApplyUser;
            }
        }
        private void fapply_user_id送检医师cbo_SelectedValueChanged(object sender, EventArgs e)
        {
            this.modeljy.fapply_user_id = this.fapply_user_id送检医师cbo.EditValue.ToString();
        }
        //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 △

        //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
        private void InitWorkUser()
        {
            DataTable dtWorkUser = null;
            try
            {
                //DataSet ds = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn,
                //    "SELECT '' fperson_id,'' fname union all SELECT [fperson_id],[fname] FROM [WWF_PERSON] WHERE fuse_flag = 1 order by fname ");
                //dtWorkUser = ds.Tables[0];
                dtWorkUser = dtDoctor.Copy();

                this.fjy_user_idtextBoxCbo.DataSource = dtWorkUser;
            }
            catch
            {
                dtWorkUser = new DataTable();
                dtWorkUser.Columns.Add("fperson_id");
                dtWorkUser.Columns.Add("fname");

                DataRow drSpace = dtWorkUser.NewRow();
                drSpace["fperson_id"] = "";
                drSpace["fname"] = "";
                dtWorkUser.Rows.Add(drSpace);

                this.fjy_user_idtextBoxCbo.DataSource = dtWorkUser;
            }
        }
        private void fjy_user_idtextBoxCbo_SelectedValueChanged(object sender, EventArgs e)
        {
            this.modeljy.fjy_user_id = this.fjy_user_idtextBoxCbo.SelectedValue.ToString();
        }
        //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △

        //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
        private void InitDoctorInfo()
        {
            try
            {
                DataSet ds = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn,
                    "SELECT '' fperson_id,'' fname union all SELECT [fperson_id],[fname] FROM [WWF_PERSON] WHERE fuse_flag = 1 order by fname ");
                dtDoctor = ds.Tables[0];
            }
            catch
            {
                dtDoctor = new DataTable();
                dtDoctor.Columns.Add("fperson_id");
                dtDoctor.Columns.Add("fname");

                DataRow drSpace = dtDoctor.NewRow();
                drSpace["fperson_id"] = "";
                drSpace["fname"] = "";
                dtDoctor.Rows.Add(drSpace);
            }
        }
        private void InitCheckUser()
        {
            DataTable dtCheckUser = null;
            try
            {
                dtCheckUser = dtDoctor.Copy();

                this.fchenk_user_idComboBox.DataSource = dtCheckUser;
            }
            catch
            {
                dtCheckUser = new DataTable();
                dtCheckUser.Columns.Add("fperson_id");
                dtCheckUser.Columns.Add("fname");

                DataRow drSpace = dtCheckUser.NewRow();
                drSpace["fperson_id"] = "";
                drSpace["fname"] = "";
                dtCheckUser.Rows.Add(drSpace);

                this.fchenk_user_idComboBox.DataSource = dtCheckUser;
            }
        }

        private void fchenk_user_idComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            this.modeljy.fchenk_审核医师ID = this.fchenk_user_idComboBox.SelectedValue.ToString();
        }
        //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △

        //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
        private void InitSampleType()
        {
            DataTable dtSampleType = null;
            try
            {
                DataSet ds = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn,
                    "SELECT '' [fsample_type_id],'' fname, '' forder_by union all SELECT [fsample_type_id],fname,forder_by FROM sam_sample_type  WHERE (fuse_if = 1)  ORDER BY forder_by");
                dtSampleType = ds.Tables[0];
            }
            catch
            {
                dtSampleType = new DataTable();
                dtSampleType.Columns.Add("fsample_type_id");
                dtSampleType.Columns.Add("fname");

                DataRow drSpace = dtSampleType.NewRow();
                drSpace["fsample_type_id"] = "";
                drSpace["fname"] = "";
                dtSampleType.Rows.Add(drSpace);
            }
            this.fjy_yb_typeComboBox.DataSource = dtSampleType;
        }

        private void fjy_yb_typeComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            this.modeljy.fjy_yb_type = this.fjy_yb_typeComboBox.SelectedValue.ToString();
            SetRef();
        }
        //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 △

        //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 ▽
        private void Init费别控件()
        {
            DataTable dt = null;
            try
            {
                DataSet ds = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn,
                    "select '' fcode,'' fname,'0' forder_by union all SELECT [fcode],[fname],forder_by FROM [SAM_TYPE] where ftype = '费别' order by forder_by");
                dt = ds.Tables[0];
            }
            catch
            {
                dt = new DataTable();
                dt.Columns.Add("fsample_type_id");
                dt.Columns.Add("fname");

                DataRow drSpace = dt.NewRow();
                drSpace["fsample_type_id"] = "";
                drSpace["fname"] = "";
                dt.Rows.Add(drSpace);
            }
            this.fjy_sf_typetextBoxCombox.DataSource = dt;
        }

        private void fjy_sf_typetextBoxCombox_SelectedValueChanged(object sender, EventArgs e)
        {
            this.modeljy.fjy_收费类型ID = this.fjy_sf_typetextBoxCombox.SelectedValue.ToString();
        }
        //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 △

        //add by wjz 双击送检医师时，弹出医师选择窗口 ▽
        private DateTime dt送检医生1;
        private DateTime dt送检医生2;
        private void fapply_user_id送检医师cbo_Click(object sender, EventArgs e)
        {
            dt送检医生2 = DateTime.Now;
            if (dt送检医生1 != null && (dt送检医生2 - dt送检医生1).TotalSeconds < 0.5)
            {
                using (com.WindowsPersonForm fm = new ww.form.lis.com.WindowsPersonForm())
                {
                    if (fm.ShowDialog() != DialogResult.No)
                    {
                        fapply_user_id送检医师cbo.EditValue = fm.strid;
                    }
                }
            }
            dt送检医生1 = dt送检医生2;
        }
        //add by wjz 双击送检医师时，弹出医师选择窗口 △
        private void dataGridViewResult_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //结果值在第2列中，如果显示结果的DataGridView中结果列的位置发生了变更，则下方if中的2也需要变更。
            if (e.RowIndex >= 0 && e.ColumnIndex == 2)
            {
                if (this.dataGridViewResult.Rows[e.RowIndex].Cells[e.ColumnIndex] != this.dataGridViewResult.Rows[e.RowIndex].Cells["fvalue"])
                {
                    return;
                }
                string newvalue = this.dataGridViewResult.Rows[e.RowIndex].Cells["fvalue"].Value.ToString();
                string refvalue = this.dataGridViewResult.Rows[e.RowIndex].Cells["fitem_ref"].Value.ToString();//参考值位于第6列中

                string[] strArr = { "--" };
                string[] lowhigh = refvalue.Split(strArr, StringSplitOptions.RemoveEmptyEntries);
                if (lowhigh.Length != 2)
                {
                    return;
                }

                string biaoji = "";
                try
                {
                    double dnewvalue = Convert.ToDouble(newvalue);
                    double dlow = Convert.ToDouble(lowhigh[0]);
                    double dhigh = Convert.ToDouble(lowhigh[1]);

                    if (dnewvalue < dlow && dnewvalue < dhigh)
                    {
                        //箭头向下"↓"
                        biaoji = "↓";
                    }
                    else if (dnewvalue > dhigh && dnewvalue > dlow)
                    {
                        //箭头向上"↑"
                        biaoji = "↑";
                    }
                }
                catch
                { }

                this.dataGridViewResult.Rows[e.RowIndex].Cells["fremark"].Value = "1";
                this.dataGridViewResult.Rows[e.RowIndex].Cells["fitem_badge"].Value = biaoji;

                if (biaoji.Equals("↑"))
                {
                    this.dataGridViewResult.Rows[e.RowIndex].Cells["fvalue"].Style.ForeColor = Color.Blue;
                    this.dataGridViewResult.Rows[e.RowIndex].Cells["fitem_badge"].Style.ForeColor = Color.Blue;
                }
                else if (biaoji.Equals("↓"))
                {
                    this.dataGridViewResult.Rows[e.RowIndex].Cells["fvalue"].Style.ForeColor = Color.Red;
                    this.dataGridViewResult.Rows[e.RowIndex].Cells["fitem_badge"].Style.ForeColor = Color.Red;
                }
            }
        }

        private void dataGridViewResult_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ///fitem_id
            try
            {
                if (this.modeljy.fjy_zt检验状态 == "已审核")
                {
                    return;
                }

                ItemBLL itembllTemp = new ItemBLL();
                string itemidTemp = this.dataGridViewResult.Rows[e.RowIndex].Cells["fitem_id"].Value.ToString();
                if (string.IsNullOrWhiteSpace(itemidTemp))
                {
                    return;
                }

                DataTable dtItemValueList = itembllTemp.GetItemValueList(itemidTemp);
                if (dtItemValueList == null || dtItemValueList.Rows.Count == 0)
                {
                    return;
                }

                FrmItemValueList frm = new FrmItemValueList(dtItemValueList);
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    this.dataGridViewResult.Rows[e.RowIndex].Cells["fvalue"].Value = frm.itemValue;

                    this.dataGridViewResult.CurrentCell = null;
                    this.dataGridViewResult.CurrentCell = this.dataGridViewResult.Rows[e.RowIndex].Cells[e.ColumnIndex];
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }
        private void button读卡_Click(object sender, EventArgs e)
        {
            try
            {
                //启动钩子服务
                listener.Start();
                //HIS.readPersonInfo.Form读卡 frm = new HIS.readPersonInfo.Form读卡();
                //frm.ShowDialog();
                //if (frm.person is ClassYBJK.basePerson)
                //{
                //    this.fremarktextBox.Text = frm.person.S身份证号;
                //    this.fhz_name姓名.Text = frm.person.S姓名;
                //}
            }
            catch (Exception ex)
            {
                //throw ex;
                WWMessage.MessageShowWarning(ex.Message);
            }
        }

        public static string SendRequest(string url, Encoding encoding)
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Method = "GET";
            HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), encoding);
            string result;
            try
            {
                result = streamReader.ReadLine();
            }
            finally
            {
                if (streamReader != null)
                {
                    streamReader.Close();
                }
            }
            return result;
        }

    }
}