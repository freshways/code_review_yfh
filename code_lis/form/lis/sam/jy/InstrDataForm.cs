﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;
using ww.lis.lisbll.sam;
using System.Collections;
using System.IO;

namespace ww.form.lis.sam.jy
{
    public partial class InstrDataForm : ww.form.wwf.SysBaseForm
    { /// <summary>
        /// 逻辑_仪器
        /// </summary>
    
        private JYInstrDataBll bllInstrIO = new JYInstrDataBll();
        
     
        private DataTable dtResult = new DataTable();
       public IList selItemList = null;
        ImgBLL img = new ImgBLL();
        DataTable dtIMG = new DataTable();
        DataTable dtIMG_1 = new DataTable();//返回的图列表

     
        public InstrDataForm()
        {
            InitializeComponent();
            this.dataGridViewReport.AutoGenerateColumns = false;
            this.dataGridViewResult.AutoGenerateColumns = false;


            DataColumn D0 = new DataColumn("FImgID", typeof(System.String));
            dtIMG.Columns.Add(D0);

            DataColumn D1 = new DataColumn("结果图1", typeof(System.Byte[]));
            dtIMG.Columns.Add(D1);

            DataColumn D2 = new DataColumn("结果图2", typeof(System.Byte[]));
            dtIMG.Columns.Add(D2);

            DataColumn D3 = new DataColumn("结果图3", typeof(System.Byte[]));
            dtIMG.Columns.Add(D3);

            DataColumn D4 = new DataColumn("结果图4", typeof(System.Byte[]));
            dtIMG.Columns.Add(D4);

            DataColumn D5 = new DataColumn("结果图5", typeof(System.Byte[]));
            dtIMG.Columns.Add(D5);

          
         
        }
       
        private void InstrDataForm_Load(object sender, EventArgs e)
        {
            try
            {
                bllInit();
                 
                WWQuery();

                //bindingSourceIO_PositionChanged(null, null);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 初始化
        /// </summary>
        private void bllInit()
        {
            try
            {
                //仪器
                InstrBLL bllInstr = new InstrBLL();
                DataTable dtInstr = bllInstr.BllInstrDTByUseAndGroupID(1, ww.wwf.wwfbll.LoginBLL.strDeptID);//仪器列表
                this.fjy_instrComboBox.DataSource = dtInstr;

                //20150610 begin  add by wjz 添加本机默认仪器功能
                string strInstrID = ww.form.wwf.Customization.DefaultInstrument.GetInstrID();
                if(!(string.IsNullOrWhiteSpace(strInstrID)))
                {
                    this.fjy_instrComboBox.SelectedValue = strInstrID;
                }
                //20150610 end    add by wjz 添加本机默认仪器功能
                //测试代码
                //this.fjy_instrComboBox.SelectedIndex = 1;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }


        private void WWImg(string strid)
        {
            try
            {
                if (this.dtIMG.Rows.Count > 0)
                    this.dtIMG.Clear();
                if (this.dtIMG_1.Rows.Count > 0)
                    this.dtIMG_1.Clear();
                dtIMG_1 = this.bllInstrIO.BllImgDT(strid);              
                DataRow drimg = dtIMG.NewRow();               
                int intImgCount = dtIMG_1.Rows.Count;
                if (intImgCount > 0)
                {
                    groupBoxImg.Visible = true;
                    drimg["FImgID"] = dtIMG_1.Rows[0]["FTaskID"];
                    for (int iimg = 0; iimg < intImgCount; iimg++)
                    {
                        switch (iimg)
                        {
                            case 0:
                                img1.Visible = true;
                                img2.Visible = false;
                                img3.Visible = false;
                                img4.Visible = false;
                                img5.Visible = false;
                                drimg["结果图1"] = dtIMG_1.Rows[iimg]["FImg"];
                                break;
                            case 1:
                                img2.Visible = true;
                                img3.Visible = false;
                                img4.Visible = false;
                                img5.Visible = false;
                                drimg["结果图2"] = dtIMG_1.Rows[iimg]["FImg"];
                                break;
                            case 2:
                                img3.Visible = true;
                                img4.Visible = false;
                                img5.Visible = false;
                                drimg["结果图3"] = dtIMG_1.Rows[iimg]["FImg"];
                                break;
                            case 3:
                                img4.Visible = true;
                                img5.Visible = false;
                                drimg["结果图4"] = dtIMG_1.Rows[iimg]["FImg"];
                                break;
                            case 4:
                                img5.Visible = true;
                                drimg["结果图5"] = dtIMG_1.Rows[iimg]["FImg"];
                                break;
                            default:
                                break;
                        }
                    }

                }
                else
                {
                    groupBoxImg.Visible = false;
                    img1.Visible = false;
                    img2.Visible = false;
                    img3.Visible = false;
                    img4.Visible = false;
                    img5.Visible = false;
                }
                dtIMG.Rows.Add(drimg);
                lIS_REPORT_IMGBindingSource.DataSource = this.dtIMG;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void GelSelValueList()
        {
            try
            {
                this.Validate();
                this.bindingSourceRelust.EndEdit();
                selItemList = new ArrayList();
                string strfitem_id = "";
                string strfitem_value = "";               

                for (int i = 0; i < this.dataGridViewResult.Rows.Count; i++)
                {
                    strfitem_id = "";
                    strfitem_value = "";
                    if (this.dataGridViewResult.Rows[i].Cells["fitem_id"].Value != null)
                    {
                        strfitem_id = this.dataGridViewResult.Rows[i].Cells["fitem_id"].Value.ToString();
                    }
                    if (this.dataGridViewResult.Rows[i].Cells["fvalue"].Value != null)
                    {
                        strfitem_value = this.dataGridViewResult.Rows[i].Cells["fvalue"].Value.ToString();
                    }
                    if (strfitem_id.Equals("") || strfitem_id.Length == 0) { }
                    else
                    {
                        this.selItemList.Add(strfitem_id + ";" + strfitem_value);
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void bindingNavigatorOK_Click(object sender, EventArgs e)
        {
            try
            {
                GelSelValueList();                
                this.Close();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        
        private void toolStripButtonQ_Click(object sender, EventArgs e)
        {
            WWQuery();
            bindingSourceIO_PositionChanged(null, null);
        }

        /// <summary>
        /// 
        /// </summary>
        private void WWQuery()
        {
            try
            {
                if (this.fjy_instrComboBox.SelectedValue == null)
                {
                    WWMessage.MessageShowInformation("请选择设备！");
                    return;
                }
                //string strWhere = " where FInstrID='" + this.fjy_instrComboBox.SelectedValue.ToString() + "' and fresultid like '%" + this.fsample_codeTextBox.Text + "%' and (CONVERT(varchar(100), FCreateTime, 23) >='" + fjy_dateDateTimePicker1.Value.ToString("yyyy-MM-dd") + "' and CONVERT(varchar(100), FCreateTime, 23) <='" + fjy_dateDateTimePicker2.Value.ToString("yyyy-MM-dd") + "' ) order by [FDateTime], cast(FResultID as int)";
                string strWhere = " where  ISNUMERIC(FResultID)<>0 and FInstrID='" + this.fjy_instrComboBox.SelectedValue.ToString() + "' and fresultid like '%" + this.fsample_codeTextBox.Text + "%' and (cast(FDateTime as DateTime) >='" + fjy_dateDateTimePicker1.Value.ToString("yyyy-MM-dd") + "' and cast(FDateTime as DateTime) < cast('" + fjy_dateDateTimePicker2.Value.ToString("yyyy-MM-dd") + "' as DateTime)+1) order by [FDateTime], cast(FResultID as int)";
               // this.richTextBox1.AppendText(strWhere);
                this.bindingSourceIO.DataSource = this.bllInstrIO.BllIODT(strWhere);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
           
        }
        private void WWResult(string strid)
        {
            try
            {
                this.bindingSourceRelust.DataSource = this.bllInstrIO.BllResultDT(strid, fjy_instrComboBox.SelectedValue.ToString());
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }

             
        }
        
        
       

        private void finstr_idComboBox_DropDownClosed(object sender, EventArgs e)
        {
            WWQuery();
            bindingSourceIO_PositionChanged(null, null);
        }

        private void bindingSourceIO_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.dtResult != null)
                    this.dtResult .Clear();
                bindingSourceRelust.DataSource = this.dtResult;

                DataRowView rowCurrent = (DataRowView)bindingSourceIO.Current;               
                if (rowCurrent != null)
                {
                    string strfio_id = rowCurrent["FTaskID"].ToString();
                    WWResult(strfio_id);
                    WWImg(strfio_id);
                }

            }

            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void dataGridViewReport_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                //GelSelValueList();
                
                //this.Close();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void fjy_dateDateTimePicker1_CloseUp(object sender, EventArgs e)
        {
            WWQuery();
            bindingSourceIO_PositionChanged(null, null);
        }

        private void fjy_dateDateTimePicker2_CloseUp(object sender, EventArgs e)
        {
            WWQuery();
            bindingSourceIO_PositionChanged(null, null);
        }

        private void 导入图片ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {                
                img.ImgIn(img1);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 导出图片ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                img.ImgOut(img1);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 清空图片ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                img.ImgNull(img1);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 图片浏览ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Bitmap bit = new Bitmap(img1.Image);
                ImgShowForm imgform = new ImgShowForm(bit);
                imgform.ShowDialog();
                imgform.Dispose();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void img1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                Bitmap bit = new Bitmap(img1.Image);
                ImgShowForm imgform = new ImgShowForm(bit);
                imgform.ShowDialog();
                imgform.Dispose();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void fsample_codeTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                WWQuery();
                bindingSourceIO_PositionChanged(null, null);
            }  
        }

        private void fjy_instrComboBox_DropDownClosed(object sender, EventArgs e)
        {
           
            WWQuery();
            bindingSourceIO_PositionChanged(null, null);
        }

        private void toolStripButtonQuery_Click(object sender, EventArgs e)
        {
            WWQuery();
            bindingSourceIO_PositionChanged(null, null);
        }

        private void toolStripButtonHelp_Click(object sender, EventArgs e)
        {
            this.WWOpenHelp("2bc53c765a76417c976d97098fc0c0c9");
        }
        /// <summary>
        /// 打开帮助
        /// </summary>
        /// <param name="strHelpID"></param>
        protected void WWOpenHelp(string strHelpID)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                HelpForm help = new HelpForm(strHelpID);
                help.Show();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void dataGridViewReport_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void dataGridViewResult_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void toolStripButtonBatchInput_Click(object sender, EventArgs e)
        {
            BatchInputForm frm = new BatchInputForm(this.fjy_instrComboBox.SelectedValue.ToString());
            frm.ShowDialog();
        }

        private void toolStripButtonDel_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dtResult != null)
                    this.dtResult.Clear();
                bindingSourceRelust.DataSource = this.dtResult;

                DataRowView rowCurrent = (DataRowView)bindingSourceIO.Current;
                if (rowCurrent != null)
                {
                    string strfio_id = rowCurrent["FTaskID"].ToString();
                    if (MessageBox.Show("是否删除样本号：【" + rowCurrent["FResultID"].ToString() + "】的数据？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.Yes)
                    {
                        string ss = this.bllInstrIO.BllODel(strfio_id);
                        if (ss == "true")
                        {
                            MessageBox.Show("删除成功！");
                            WWQuery();
                        }
                        else
                            MessageBox.Show("删除失败！");
                    }
                }

            }

            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

    }
}