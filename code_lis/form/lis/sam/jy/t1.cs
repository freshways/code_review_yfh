using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ww.form.lis.sam.jy
{
    public partial class t1 : Form
    {
        public t1()
        {
            InitializeComponent();
        }

        private void t1_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void t1_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void t1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{Tab}");
            } 
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}