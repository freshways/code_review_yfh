﻿namespace ww.form.lis.sam.jy
{
    partial class InstrDataForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label fsample_codeLabel;
            System.Windows.Forms.Label fjy_dateLabel;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InstrDataForm));
            this.bindingSourceIO = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewReport = new System.Windows.Forms.DataGridView();
            this.FInstrID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FDateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FResultID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FTransFlag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FValidity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FTaskID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSourceRelust = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewResult = new System.Windows.Forms.DataGridView();
            this.fitem_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FOD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FCutoff = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FItem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBoxif = new System.Windows.Forms.GroupBox();
            this.fjy_instrComboBox = new System.Windows.Forms.ComboBox();
            this.fsample_codeTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.fjy_dateDateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.fjy_dateDateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBoxImg = new System.Windows.Forms.GroupBox();
            this.img5 = new System.Windows.Forms.PictureBox();
            this.contextMenuStripimg1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.导入图片ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.导出图片ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.清空图片ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.图片浏览ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lIS_REPORT_IMGBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.samDataSet = new ww.form.lis.sam.samDataSet();
            this.img4 = new System.Windows.Forms.PictureBox();
            this.img3 = new System.Windows.Forms.PictureBox();
            this.img2 = new System.Windows.Forms.PictureBox();
            this.img1 = new System.Windows.Forms.PictureBox();
            this.bN = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonQuery = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonBatchInput = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonHelp = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonDel = new System.Windows.Forms.ToolStripButton();
            fsample_codeLabel = new System.Windows.Forms.Label();
            fjy_dateLabel = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceIO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceRelust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).BeginInit();
            this.groupBoxif.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBoxImg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.img5)).BeginInit();
            this.contextMenuStripimg1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lIS_REPORT_IMGBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).BeginInit();
            this.bN.SuspendLayout();
            this.SuspendLayout();
            // 
            // fsample_codeLabel
            // 
            fsample_codeLabel.AutoSize = true;
            fsample_codeLabel.Location = new System.Drawing.Point(693, 22);
            fsample_codeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fsample_codeLabel.Name = "fsample_codeLabel";
            fsample_codeLabel.Size = new System.Drawing.Size(60, 15);
            fsample_codeLabel.TabIndex = 21;
            fsample_codeLabel.Text = "样本号:";
            // 
            // fjy_dateLabel
            // 
            fjy_dateLabel.AutoSize = true;
            fjy_dateLabel.Location = new System.Drawing.Point(301, 22);
            fjy_dateLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fjy_dateLabel.Name = "fjy_dateLabel";
            fjy_dateLabel.Size = new System.Drawing.Size(75, 15);
            fjy_dateLabel.TabIndex = 12;
            fjy_dateLabel.Text = "检验日期:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(11, 22);
            label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(75, 15);
            label2.TabIndex = 23;
            label2.Text = "检验仪器:";
            // 
            // bindingSourceIO
            // 
            this.bindingSourceIO.PositionChanged += new System.EventHandler(this.bindingSourceIO_PositionChanged);
            // 
            // dataGridViewReport
            // 
            this.dataGridViewReport.AllowUserToAddRows = false;
            this.dataGridViewReport.AllowUserToDeleteRows = false;
            this.dataGridViewReport.AllowUserToOrderColumns = true;
            this.dataGridViewReport.AllowUserToResizeColumns = false;
            this.dataGridViewReport.AllowUserToResizeRows = false;
            this.dataGridViewReport.AutoGenerateColumns = false;
            this.dataGridViewReport.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewReport.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewReport.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewReport.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewReport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FInstrID,
            this.FDateTime,
            this.FResultID,
            this.FTransFlag,
            this.FValidity,
            this.FType,
            this.FTaskID});
            this.dataGridViewReport.DataSource = this.bindingSourceIO;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewReport.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewReport.Location = new System.Drawing.Point(0, 92);
            this.dataGridViewReport.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewReport.MultiSelect = false;
            this.dataGridViewReport.Name = "dataGridViewReport";
            this.dataGridViewReport.ReadOnly = true;
            this.dataGridViewReport.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dataGridViewReport.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewReport.RowHeadersVisible = false;
            this.dataGridViewReport.RowHeadersWidth = 20;
            this.dataGridViewReport.RowTemplate.Height = 23;
            this.dataGridViewReport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewReport.Size = new System.Drawing.Size(452, 540);
            this.dataGridViewReport.TabIndex = 131;
            this.dataGridViewReport.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewReport_DataError);
            this.dataGridViewReport.DoubleClick += new System.EventHandler(this.dataGridViewReport_DoubleClick);
            // 
            // FInstrID
            // 
            this.FInstrID.DataPropertyName = "FInstrID";
            this.FInstrID.HeaderText = "仪器";
            this.FInstrID.Name = "FInstrID";
            this.FInstrID.ReadOnly = true;
            this.FInstrID.Width = 120;
            // 
            // FDateTime
            // 
            this.FDateTime.DataPropertyName = "FDateTime";
            this.FDateTime.HeaderText = "日期";
            this.FDateTime.Name = "FDateTime";
            this.FDateTime.ReadOnly = true;
            this.FDateTime.Width = 115;
            // 
            // FResultID
            // 
            this.FResultID.DataPropertyName = "FResultID";
            this.FResultID.HeaderText = "样本号";
            this.FResultID.Name = "FResultID";
            this.FResultID.ReadOnly = true;
            this.FResultID.Width = 75;
            // 
            // FTransFlag
            // 
            this.FTransFlag.DataPropertyName = "FTransFlag";
            this.FTransFlag.HeaderText = "处理否";
            this.FTransFlag.Name = "FTransFlag";
            this.FTransFlag.ReadOnly = true;
            this.FTransFlag.Visible = false;
            // 
            // FValidity
            // 
            this.FValidity.DataPropertyName = "FValidity";
            this.FValidity.HeaderText = "有效否";
            this.FValidity.Name = "FValidity";
            this.FValidity.ReadOnly = true;
            this.FValidity.Visible = false;
            // 
            // FType
            // 
            this.FType.DataPropertyName = "FType";
            this.FType.HeaderText = "类型";
            this.FType.Name = "FType";
            this.FType.ReadOnly = true;
            this.FType.Visible = false;
            // 
            // FTaskID
            // 
            this.FTaskID.DataPropertyName = "FTaskID";
            this.FTaskID.HeaderText = "结果id";
            this.FTaskID.Name = "FTaskID";
            this.FTaskID.ReadOnly = true;
            this.FTaskID.Visible = false;
            // 
            // dataGridViewResult
            // 
            this.dataGridViewResult.AllowUserToAddRows = false;
            this.dataGridViewResult.AllowUserToResizeColumns = false;
            this.dataGridViewResult.AllowUserToResizeRows = false;
            this.dataGridViewResult.AutoGenerateColumns = false;
            this.dataGridViewResult.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fitem_name,
            this.FValue,
            this.FOD,
            this.FCutoff,
            this.FItem,
            this.fitem_id});
            this.dataGridViewResult.DataSource = this.bindingSourceRelust;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewResult.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewResult.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridViewResult.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewResult.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewResult.MultiSelect = false;
            this.dataGridViewResult.Name = "dataGridViewResult";
            this.dataGridViewResult.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewResult.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewResult.RowHeadersVisible = false;
            this.dataGridViewResult.RowHeadersWidth = 30;
            this.dataGridViewResult.RowTemplate.Height = 23;
            this.dataGridViewResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewResult.ShowCellToolTips = false;
            this.dataGridViewResult.ShowEditingIcon = false;
            this.dataGridViewResult.Size = new System.Drawing.Size(604, 420);
            this.dataGridViewResult.TabIndex = 132;
            this.dataGridViewResult.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewResult_DataError);
            // 
            // fitem_name
            // 
            this.fitem_name.DataPropertyName = "fitem_name";
            this.fitem_name.HeaderText = "检验项目";
            this.fitem_name.Name = "fitem_name";
            this.fitem_name.ReadOnly = true;
            this.fitem_name.Width = 140;
            // 
            // FValue
            // 
            this.FValue.DataPropertyName = "FValue";
            this.FValue.HeaderText = "值";
            this.FValue.Name = "FValue";
            this.FValue.ReadOnly = true;
            this.FValue.Width = 70;
            // 
            // FOD
            // 
            this.FOD.DataPropertyName = "FOD";
            this.FOD.HeaderText = "OD";
            this.FOD.Name = "FOD";
            this.FOD.ReadOnly = true;
            this.FOD.Width = 70;
            // 
            // FCutoff
            // 
            this.FCutoff.DataPropertyName = "FCutoff";
            this.FCutoff.HeaderText = "Cutoff";
            this.FCutoff.Name = "FCutoff";
            this.FCutoff.ReadOnly = true;
            this.FCutoff.Width = 80;
            // 
            // FItem
            // 
            this.FItem.DataPropertyName = "FItem";
            this.FItem.HeaderText = "代码";
            this.FItem.Name = "FItem";
            this.FItem.ReadOnly = true;
            this.FItem.Width = 70;
            // 
            // fitem_id
            // 
            this.fitem_id.DataPropertyName = "fitem_id";
            this.fitem_id.HeaderText = "fitem_id";
            this.fitem_id.Name = "fitem_id";
            this.fitem_id.ReadOnly = true;
            this.fitem_id.Visible = false;
            // 
            // groupBoxif
            // 
            this.groupBoxif.Controls.Add(label2);
            this.groupBoxif.Controls.Add(this.fjy_instrComboBox);
            this.groupBoxif.Controls.Add(this.fsample_codeTextBox);
            this.groupBoxif.Controls.Add(fsample_codeLabel);
            this.groupBoxif.Controls.Add(this.label1);
            this.groupBoxif.Controls.Add(this.fjy_dateDateTimePicker2);
            this.groupBoxif.Controls.Add(this.fjy_dateDateTimePicker1);
            this.groupBoxif.Controls.Add(fjy_dateLabel);
            this.groupBoxif.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxif.Location = new System.Drawing.Point(0, 38);
            this.groupBoxif.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxif.Name = "groupBoxif";
            this.groupBoxif.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxif.Size = new System.Drawing.Size(1056, 54);
            this.groupBoxif.TabIndex = 130;
            this.groupBoxif.TabStop = false;
            // 
            // fjy_instrComboBox
            // 
            this.fjy_instrComboBox.DisplayMember = "fname";
            this.fjy_instrComboBox.FormattingEnabled = true;
            this.fjy_instrComboBox.Location = new System.Drawing.Point(97, 18);
            this.fjy_instrComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.fjy_instrComboBox.Name = "fjy_instrComboBox";
            this.fjy_instrComboBox.Size = new System.Drawing.Size(195, 23);
            this.fjy_instrComboBox.TabIndex = 22;
            this.fjy_instrComboBox.ValueMember = "finstr_id";
            this.fjy_instrComboBox.DropDownClosed += new System.EventHandler(this.fjy_instrComboBox_DropDownClosed);
            // 
            // fsample_codeTextBox
            // 
            this.fsample_codeTextBox.ForeColor = System.Drawing.SystemColors.Desktop;
            this.fsample_codeTextBox.Location = new System.Drawing.Point(764, 18);
            this.fsample_codeTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.fsample_codeTextBox.Name = "fsample_codeTextBox";
            this.fsample_codeTextBox.Size = new System.Drawing.Size(115, 25);
            this.fsample_codeTextBox.TabIndex = 20;
            this.fsample_codeTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fsample_codeTextBox_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(529, 22);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 15);
            this.label1.TabIndex = 14;
            this.label1.Text = "-";
            // 
            // fjy_dateDateTimePicker2
            // 
            this.fjy_dateDateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker2.Location = new System.Drawing.Point(552, 18);
            this.fjy_dateDateTimePicker2.Margin = new System.Windows.Forms.Padding(4);
            this.fjy_dateDateTimePicker2.Name = "fjy_dateDateTimePicker2";
            this.fjy_dateDateTimePicker2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker2.Size = new System.Drawing.Size(132, 25);
            this.fjy_dateDateTimePicker2.TabIndex = 13;
            this.fjy_dateDateTimePicker2.CloseUp += new System.EventHandler(this.fjy_dateDateTimePicker2_CloseUp);
            // 
            // fjy_dateDateTimePicker1
            // 
            this.fjy_dateDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker1.Location = new System.Drawing.Point(388, 18);
            this.fjy_dateDateTimePicker1.Margin = new System.Windows.Forms.Padding(4);
            this.fjy_dateDateTimePicker1.Name = "fjy_dateDateTimePicker1";
            this.fjy_dateDateTimePicker1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker1.Size = new System.Drawing.Size(132, 25);
            this.fjy_dateDateTimePicker1.TabIndex = 11;
            this.fjy_dateDateTimePicker1.CloseUp += new System.EventHandler(this.fjy_dateDateTimePicker1_CloseUp);
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(448, 92);
            this.splitter1.Margin = new System.Windows.Forms.Padding(4);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(4, 540);
            this.splitter1.TabIndex = 133;
            this.splitter1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dataGridViewResult);
            this.panel1.Controls.Add(this.groupBoxImg);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(452, 92);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(604, 540);
            this.panel1.TabIndex = 134;
            // 
            // groupBoxImg
            // 
            this.groupBoxImg.Controls.Add(this.img5);
            this.groupBoxImg.Controls.Add(this.img4);
            this.groupBoxImg.Controls.Add(this.img3);
            this.groupBoxImg.Controls.Add(this.img2);
            this.groupBoxImg.Controls.Add(this.img1);
            this.groupBoxImg.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBoxImg.Location = new System.Drawing.Point(0, 420);
            this.groupBoxImg.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxImg.Name = "groupBoxImg";
            this.groupBoxImg.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxImg.Size = new System.Drawing.Size(604, 120);
            this.groupBoxImg.TabIndex = 133;
            this.groupBoxImg.TabStop = false;
            this.groupBoxImg.Text = "图";
            this.groupBoxImg.Visible = false;
            // 
            // img5
            // 
            this.img5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img5.ContextMenuStrip = this.contextMenuStripimg1;
            this.img5.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图5", true));
            this.img5.Location = new System.Drawing.Point(416, 24);
            this.img5.Margin = new System.Windows.Forms.Padding(4);
            this.img5.Name = "img5";
            this.img5.Size = new System.Drawing.Size(93, 87);
            this.img5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img5.TabIndex = 50;
            this.img5.TabStop = false;
            this.img5.Visible = false;
            // 
            // contextMenuStripimg1
            // 
            this.contextMenuStripimg1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStripimg1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.导入图片ToolStripMenuItem,
            this.导出图片ToolStripMenuItem,
            this.清空图片ToolStripMenuItem,
            this.toolStripSeparator1,
            this.图片浏览ToolStripMenuItem});
            this.contextMenuStripimg1.Name = "contextMenuStripLICENSE_PIC";
            this.contextMenuStripimg1.Size = new System.Drawing.Size(145, 114);
            this.contextMenuStripimg1.Text = "图片管理";
            // 
            // 导入图片ToolStripMenuItem
            // 
            this.导入图片ToolStripMenuItem.Name = "导入图片ToolStripMenuItem";
            this.导入图片ToolStripMenuItem.Size = new System.Drawing.Size(144, 26);
            this.导入图片ToolStripMenuItem.Text = "导入图片";
            this.导入图片ToolStripMenuItem.Click += new System.EventHandler(this.导入图片ToolStripMenuItem_Click);
            // 
            // 导出图片ToolStripMenuItem
            // 
            this.导出图片ToolStripMenuItem.Name = "导出图片ToolStripMenuItem";
            this.导出图片ToolStripMenuItem.Size = new System.Drawing.Size(144, 26);
            this.导出图片ToolStripMenuItem.Text = "导出图片";
            this.导出图片ToolStripMenuItem.Click += new System.EventHandler(this.导出图片ToolStripMenuItem_Click);
            // 
            // 清空图片ToolStripMenuItem
            // 
            this.清空图片ToolStripMenuItem.Name = "清空图片ToolStripMenuItem";
            this.清空图片ToolStripMenuItem.Size = new System.Drawing.Size(144, 26);
            this.清空图片ToolStripMenuItem.Text = "清空图片";
            this.清空图片ToolStripMenuItem.Click += new System.EventHandler(this.清空图片ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(141, 6);
            // 
            // 图片浏览ToolStripMenuItem
            // 
            this.图片浏览ToolStripMenuItem.Name = "图片浏览ToolStripMenuItem";
            this.图片浏览ToolStripMenuItem.Size = new System.Drawing.Size(144, 26);
            this.图片浏览ToolStripMenuItem.Text = "图片浏览";
            this.图片浏览ToolStripMenuItem.Click += new System.EventHandler(this.图片浏览ToolStripMenuItem_Click);
            // 
            // lIS_REPORT_IMGBindingSource
            // 
            this.lIS_REPORT_IMGBindingSource.DataMember = "LIS_REPORT_IMG";
            this.lIS_REPORT_IMGBindingSource.DataSource = this.samDataSet;
            // 
            // samDataSet
            // 
            this.samDataSet.DataSetName = "samDataSet";
            this.samDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // img4
            // 
            this.img4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img4.ContextMenuStrip = this.contextMenuStripimg1;
            this.img4.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图4", true));
            this.img4.Location = new System.Drawing.Point(316, 24);
            this.img4.Margin = new System.Windows.Forms.Padding(4);
            this.img4.Name = "img4";
            this.img4.Size = new System.Drawing.Size(93, 87);
            this.img4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img4.TabIndex = 49;
            this.img4.TabStop = false;
            this.img4.Visible = false;
            // 
            // img3
            // 
            this.img3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img3.ContextMenuStrip = this.contextMenuStripimg1;
            this.img3.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图3", true));
            this.img3.Location = new System.Drawing.Point(216, 24);
            this.img3.Margin = new System.Windows.Forms.Padding(4);
            this.img3.Name = "img3";
            this.img3.Size = new System.Drawing.Size(93, 87);
            this.img3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img3.TabIndex = 48;
            this.img3.TabStop = false;
            this.img3.Visible = false;
            // 
            // img2
            // 
            this.img2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img2.ContextMenuStrip = this.contextMenuStripimg1;
            this.img2.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图2", true));
            this.img2.Location = new System.Drawing.Point(116, 24);
            this.img2.Margin = new System.Windows.Forms.Padding(4);
            this.img2.Name = "img2";
            this.img2.Size = new System.Drawing.Size(93, 87);
            this.img2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img2.TabIndex = 47;
            this.img2.TabStop = false;
            this.img2.Visible = false;
            // 
            // img1
            // 
            this.img1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img1.ContextMenuStrip = this.contextMenuStripimg1;
            this.img1.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图1", true));
            this.img1.Location = new System.Drawing.Point(16, 24);
            this.img1.Margin = new System.Windows.Forms.Padding(4);
            this.img1.Name = "img1";
            this.img1.Size = new System.Drawing.Size(93, 87);
            this.img1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img1.TabIndex = 46;
            this.img1.TabStop = false;
            this.img1.Visible = false;
            this.img1.DoubleClick += new System.EventHandler(this.img1_DoubleClick);
            // 
            // bN
            // 
            this.bN.AddNewItem = null;
            this.bN.AutoSize = false;
            this.bN.CountItem = this.bindingNavigatorCountItem;
            this.bN.DeleteItem = null;
            this.bN.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bN.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.bN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.toolStripSeparator5,
            this.toolStripButtonQuery,
            this.toolStripSeparator6,
            this.toolStripButtonBatchInput,
            this.toolStripSeparator2,
            this.toolStripButtonHelp,
            this.toolStripSeparator3,
            this.toolStripButtonDel});
            this.bN.Location = new System.Drawing.Point(0, 0);
            this.bN.MoveFirstItem = null;
            this.bN.MoveLastItem = null;
            this.bN.MoveNextItem = null;
            this.bN.MovePreviousItem = null;
            this.bN.Name = "bN";
            this.bN.PositionItem = this.bindingNavigatorPositionItem;
            this.bN.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bN.Size = new System.Drawing.Size(1056, 38);
            this.bN.TabIndex = 147;
            this.bN.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(38, 35);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 38);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(65, 27);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 38);
            // 
            // toolStripButtonQuery
            // 
            this.toolStripButtonQuery.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonQuery.Image")));
            this.toolStripButtonQuery.Name = "toolStripButtonQuery";
            this.toolStripButtonQuery.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonQuery.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonQuery.Size = new System.Drawing.Size(85, 35);
            this.toolStripButtonQuery.Text = "查询(&Q)";
            this.toolStripButtonQuery.Click += new System.EventHandler(this.toolStripButtonQuery_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 38);
            // 
            // toolStripButtonBatchInput
            // 
            this.toolStripButtonBatchInput.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonBatchInput.Image")));
            this.toolStripButtonBatchInput.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonBatchInput.Name = "toolStripButtonBatchInput";
            this.toolStripButtonBatchInput.Size = new System.Drawing.Size(93, 35);
            this.toolStripButtonBatchInput.Text = "批量输入";
            this.toolStripButtonBatchInput.Click += new System.EventHandler(this.toolStripButtonBatchInput_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 38);
            // 
            // toolStripButtonHelp
            // 
            this.toolStripButtonHelp.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHelp.Image")));
            this.toolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonHelp.Name = "toolStripButtonHelp";
            this.toolStripButtonHelp.Size = new System.Drawing.Size(85, 35);
            this.toolStripButtonHelp.Text = "帮助(&H)";
            this.toolStripButtonHelp.Click += new System.EventHandler(this.toolStripButtonHelp_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 38);
            // 
            // toolStripButtonDel
            // 
            this.toolStripButtonDel.Image = global::ww.form.Properties.Resources.del;
            this.toolStripButtonDel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDel.Name = "toolStripButtonDel";
            this.toolStripButtonDel.Size = new System.Drawing.Size(123, 35);
            this.toolStripButtonDel.Text = "删除选中样本";
            this.toolStripButtonDel.Click += new System.EventHandler(this.toolStripButtonDel_Click);
            // 
            // InstrDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1056, 632);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.dataGridViewReport);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBoxif);
            this.Controls.Add(this.bN);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "InstrDataForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "仪器接收数据";
            this.Load += new System.EventHandler(this.InstrDataForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceIO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceRelust)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).EndInit();
            this.groupBoxif.ResumeLayout(false);
            this.groupBoxif.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBoxImg.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.img5)).EndInit();
            this.contextMenuStripimg1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lIS_REPORT_IMGBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).EndInit();
            this.bN.ResumeLayout(false);
            this.bN.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewReport;
        private System.Windows.Forms.DataGridView dataGridViewResult;
        private System.Windows.Forms.GroupBox groupBoxif;
        private System.Windows.Forms.TextBox fsample_codeTextBox;
        private System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.BindingSource bindingSourceIO;
        private System.Windows.Forms.BindingSource bindingSourceRelust;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBoxImg;
        private System.Windows.Forms.PictureBox img1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripimg1;
        private System.Windows.Forms.ToolStripMenuItem 导入图片ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 导出图片ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 清空图片ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 图片浏览ToolStripMenuItem;
        private System.Windows.Forms.PictureBox img5;
        private System.Windows.Forms.PictureBox img4;
        private System.Windows.Forms.PictureBox img3;
        private System.Windows.Forms.PictureBox img2;
        private System.Windows.Forms.BindingSource lIS_REPORT_IMGBindingSource;
        private ww.form.lis.sam.samDataSet samDataSet;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker2;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn FValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn FOD;
        private System.Windows.Forms.DataGridViewTextBoxColumn FCutoff;
        private System.Windows.Forms.DataGridViewTextBoxColumn FItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_id;
        private System.Windows.Forms.ComboBox fjy_instrComboBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn FInstrID;
        private System.Windows.Forms.DataGridViewTextBoxColumn FDateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn FResultID;
        private System.Windows.Forms.DataGridViewTextBoxColumn FTransFlag;
        private System.Windows.Forms.DataGridViewTextBoxColumn FValidity;
        private System.Windows.Forms.DataGridViewTextBoxColumn FType;
        private System.Windows.Forms.DataGridViewTextBoxColumn FTaskID;
        protected System.Windows.Forms.BindingNavigator bN;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        protected System.Windows.Forms.ToolStripButton toolStripButtonQuery;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton toolStripButtonHelp;
        private System.Windows.Forms.ToolStripButton toolStripButtonBatchInput;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButtonDel;
    }
}