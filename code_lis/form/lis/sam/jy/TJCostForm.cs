using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.IO;
using ww.form.wwf.print;
using ww.wwf.wwfbll;
 
using ww.lis.lisbll.sam;

namespace ww.form.lis.sam.jy
{
    public partial class TJCostForm : ww.form.wwf.SysBaseForm
    {
        private EMFStreamPrintDocument printDoc = null;
         protected InstrBLL bllInstr = new InstrBLL();
        protected TypeBLL bllType = new TypeBLL();
        public TJCostForm()
        {
            InitializeComponent();
                
        }

        private void ReportViewer_Load(object sender, EventArgs e)
        {
            try
            { 
                this.fjy_instrComboBox.DataSource = this.bllInstr.BllInstrDTByUseAndGroupID(1, ww.wwf.wwfbll.LoginBLL.strDeptID);//仪器列表
                fjy_instrComboBox.SelectedValue = "";

                //20150610 begin  add by wjz 添加本机默认仪器功能
                string strInstrID = ww.form.wwf.Customization.DefaultInstrument.GetInstrID();
                if (!(string.IsNullOrWhiteSpace(strInstrID)))
                {
                    this.fjy_instrComboBox.SelectedValue = strInstrID;
                }
                //20150610 end    add by wjz 添加本机默认仪器功能
                this.ftype_idComboBox.DataSource = this.bllType.BllComTypeDT("病人类别", 1, "");
                ftype_idComboBox.SelectedValue = "";
              
                
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());

               
            }
        }
        
        private void bllReport()
        {
            try
            {
              
                DataSet dsPrint = new DataSet();
                samDataSet.lis_tjDataTable dtPrintDataTable = new samDataSet.lis_tjDataTable();

                //数据

                DataTable dtData = dtDataRet();
                for (int i = 0; i < dtData.Rows.Count; i++)
                {
                    DataRow printRow = dtPrintDataTable.NewRow();
                    printRow["f1"] = LoginBLL.CustomerName + " - 病人费用统计表";// i.ToString();
                    printRow["f2"] = dtData.Rows[i]["科室"].ToString();
                    printRow["f3"] = dtData.Rows[i]["住院号"].ToString();
                    printRow["f4"] = dtData.Rows[i]["病人姓名"].ToString();
                    printRow["f5"] = dtData.Rows[i]["性别"].ToString();
                    printRow["f6"] = dtData.Rows[i]["类型"].ToString();
                    printRow["f7"] = dtData.Rows[i]["床号"].ToString();
                    printRow["f8"] = dtData.Rows[i]["项目数"].ToString();
                    printRow["f9"] = dtData.Rows[i]["金额"].ToString();
                    printRow["f10"] = dtData.Rows[i]["样本日期"].ToString();
                    dtPrintDataTable.Rows.Add(printRow);
                }
                dsPrint.Tables.Add(dtPrintDataTable);

                MainDataSet = dsPrint;
                //报表名
                ReportName = "lis检验费用统计";
                //报表数据源名
                MainDataSourceName = "samDataSet_lis_tj";
                //报表文档
                ReportPath = "ww.form.lis.sam.Report.TJCostReport.rdlc";

                this.AddReportDataSource(this.m_MainDataSet, this.m_MainDataSourceName, m_MainDataSourceName2);

                rptViewer.LocalReport.Refresh();
                rptViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowWarning(ex.ToString());
            }
        }
        private DataTable  dtDataRet()
        {
            string fhz_id = "";//患者ID
            string fapply_code = "";//申请单号
            string fzyh = "";//住院号
            string fjy_yb_code = "";//样本号
            string fname = "";//姓名                
            string finstr_id = "";//仪器ID
            string fhz_type_id = "";//患者类别
            string fjy_date1 = this.fjy_dateDateTimePicker1.Value.ToString("yyyy-MM-dd");
            string fjy_date2 = this.fjy_dateDateTimePicker2.Value.ToString("yyyy-MM-dd");
            string fjy_dateWhere = "";
            fjy_yb_code = this.fsample_codeTextBox.Text;
            fname = this.fnameTextBox.Text;
            fzyh = fzyhtextBox.Text;
            fhz_id = this.fhz_idtextBox.Text;
            fapply_code = this.fapply_codetextBox.Text;

            try
            {
                finstr_id = fjy_instrComboBox.SelectedValue.ToString();
            }
            catch { }
            try
            {
                fhz_type_id = ftype_idComboBox.SelectedValue.ToString();
            }
            catch { }

            if (fjy_instrComboBox.Text == "" || fjy_instrComboBox.Text == null)
                finstr_id = " (fjy_instr is not null) ";
            else
                finstr_id = " (fjy_instr='" + finstr_id + "') ";

            if (this.ftype_idComboBox.Text == "" || this.ftype_idComboBox.Text == null)
                fhz_type_id = " and (fhz_type_id is not null)";
            else
                fhz_type_id = " and (fhz_type_id='" + fhz_type_id + "') ";

            fjy_dateWhere = " and (fjy_date>='" + fjy_date1 + "' and fjy_date<='" + fjy_date2 + "') ";

            if (fjy_yb_code == "" || fjy_yb_code == null)
                fjy_yb_code = " and (fjy_yb_code is not null) ";
            else
                fjy_yb_code = " and (fjy_yb_code='" + fjy_yb_code + "')";
            if (fname == "" || fname == null)
                fname = " and (fhz_name is not null) ";
            else
                fname = " and (fhz_name like '" + fname + "%')";

            if (fzyh == "" || fzyh == null)
                fzyh = " and (fhz_zyh is not null) ";
            else
                fzyh = " and (fhz_zyh='" + fzyh + "')";


            if (fhz_id == "" || fhz_id == null)
                fhz_id = " and (fhz_id is not null) ";
            else
                fhz_id = " and (fhz_id='" + fhz_id + "')";

            if (fapply_code == "" || fapply_code == null)
                fapply_code = " and (fjy_id is not null) ";
            else
                fapply_code = " and (fjy_id='" + fapply_code + "')";

            string strWhere = finstr_id + fhz_type_id + fjy_dateWhere + fjy_yb_code + fname + fzyh + fhz_id + fapply_code;
            string sqlData = "select t.fjy_date as 样本日期,(SELECT top 1 z.fname FROM WWF_DEPT z WHERE (z.fdept_id=t.fhz_dept)) as 科室,t.fhz_zyh as 住院号,t.fhz_name as 病人姓名,(SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '性别' and z.fcode=t.fhz_sex)) as 性别,(SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '病人类别' and z.fcode=t.fhz_sex)) as 类型,t.fhz_bed as 床号,(SELECT count(z1.fresult_id) FROM SAM_JY_RESULT z1 where z1.fjy_id=t.fjy_id) as 项目数,(SELECT sum(z2.fprice) FROM SAM_JY_RESULT z1,SAM_ITEM z2 where z1.fitem_id=z2.fitem_id and z1.fjy_id=t.fjy_id) as 金额  FROM SAM_JY t where " + strWhere + " order by t.fjy_date ";


            DataTable dtData = WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, sqlData);
            // Declare an object variable. 
            object obj项目数 = dtData.Compute("Sum(项目数)", "");;
            string str项目数 = "0";
            if (obj项目数 != null)
            {
                str项目数 = obj项目数.ToString();
            }
            else
            {
                str项目数 = "0";
            }
            object obj金额 = dtData.Compute("Sum(金额)", ""); ;
            string str金额 = "";
            if (obj金额 != null)
            {
                str金额 = obj金额.ToString();
            }
           
            DataRow drSum = dtData.NewRow();
            if (str项目数 != null & str项目数.Length > 0)
            {
                drSum["项目数"] = str项目数;
            }
            if (str金额 != null & str金额.Length > 0)
            {
                drSum["金额"] = str金额;
            }
            drSum["床号"] = "合计：";
            dtData.Rows.Add(drSum); 
            return dtData;
        }
        /// <summary>
        /// Set the DataSource for the report using a strong-typed dataset
        /// Call it in Form_Load event
        /// </summary>
        private void AddReportDataSource(object ReportSource, string ReportDataSetName1, string ReportDataSetName2)
        {

            rptViewer.LocalReport.DataSources.Clear();//tao2013 新加
            System.Type type = ReportSource.GetType();
            if (type == null)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n   The datasource is of the wrong type!");
                return;
            }
            else
            {
                System.Reflection.PropertyInfo[] picData = type.GetProperties();
                bool bolExist = false;
                foreach (System.Reflection.PropertyInfo piData in picData)
                {
                    if (piData.Name == "Tables")
                    {
                        bolExist = true;
                        if (ReportDataSetName1 == string.Empty)
                        {
                            ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The dataset name for the report does not exist or is empty!");
                            return;
                        }

                        this.rptViewer.LocalReport.DataSources.Add(
                            new Microsoft.Reporting.WinForms.ReportDataSource(ReportDataSetName1,
                            (piData.GetValue(ReportSource, null) as System.Data.DataTableCollection)[0])
                            );

                        this.rptViewer.LocalReport.DataSources.Add(
                          new Microsoft.Reporting.WinForms.ReportDataSource(ReportDataSetName2,
                          (piData.GetValue(ReportSource, null) as System.Data.DataTableCollection)[0])
                          );

                        this.rptViewer.RefreshReport();
                        break;
                    }
                }

                if (!bolExist)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The datasource is of the wrong type!");
                    return;
                }
            }
        }

        private System.Data.DataTableCollection GetTableCollection(object ReportSource)
        {
            System.Type type = ReportSource.GetType();

            if (type == null)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n     The datasource is of the wrong type!");
                return null;
            }
            else
            {
                System.Reflection.PropertyInfo[] picData = type.GetProperties();
                bool bolExist = false;
                foreach (System.Reflection.PropertyInfo piData in picData)
                {
                    if (piData.Name == "Tables")
                    {
                        bolExist = true;
                        return piData.GetValue(ReportSource, null) as System.Data.DataTableCollection;
                    }
                }

                if (!bolExist)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The datasource is of the wrong type!");
                    return null;
                }
            }
            return null;
        }

       
        
        private void rptViewer_Drillthrough(object sender, Microsoft.Reporting.WinForms.DrillthroughEventArgs e)
        {
            if (this.m_DrillDataSet == null)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The dataset name for the report does not exist or is empty!");
                return;
            }
            else
            {
                if (this.m_DrillDataSourceName == string.Empty)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The datasource is of the wrong type!");
                    return;
                }
                else
                {
                    Microsoft.Reporting.WinForms.LocalReport report = e.Report as Microsoft.Reporting.WinForms.LocalReport;
                    report.DataSources.Add(new Microsoft.Reporting.WinForms.ReportDataSource(this.m_DrillDataSourceName, this.GetTableCollection(this.m_DrillDataSet)[0]));
                }
            }
        }



     



        private void printView()
        {
            try
            {
                if (this.printDoc == null)
                {
                    this.printDoc = new EMFStreamPrintDocument(this.rptViewer.LocalReport, this.m_ReportName);
                    if (this.printDoc.ErrorMessage != string.Empty)
                    {
                       
                        this.printDoc = null;
                        ww.wwf.wwfbll.WWMessage.MessageShowWarning("页面设置有误，请重新设置后重试");
                        PageSettings frm = new PageSettings(this.m_ReportName);
                        frm.ShowDialog();
                        frm.Dispose();     
                        return;
                    }
                }

                this.PreviewDialog.Document = this.printDoc;
                this.PreviewDialog.ShowDialog();
               
               // this.PreviewDialog.Dispose();
               // this.printDoc = null;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("页面设置有误，请重新设置后重试");
                PageSettings frm = new PageSettings(this.m_ReportName);
                frm.ShowDialog();
                frm.Dispose();     
            }
        }

        private string GetTimeStamp()
        {
            string strRet = string.Empty;
            System.DateTime dtNow = System.DateTime.Now;
            strRet += dtNow.Year.ToString() +
                        dtNow.Month.ToString("00") +
                        dtNow.Day.ToString("00") +
                        dtNow.Hour.ToString("00") +
                        dtNow.Minute.ToString("00") +
                        dtNow.Second.ToString("00") +
                        System.DateTime.Now.Millisecond.ToString("000");
            return strRet;

        }
        private void ExcelOut()
        {
            try
            {
                Microsoft.Reporting.WinForms.Warning[] Warnings;
                string[] strStreamIds;
                string strMimeType;
                string strEncoding;
                string strFileNameExtension;

                byte[] bytes = this.rptViewer.LocalReport.Render("Excel", null, out strMimeType, out strEncoding, out strFileNameExtension, out strStreamIds, out Warnings);

                string strFilePath = @"C:\" + this.GetTimeStamp() + ".xls";
                //*
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "XLS文件|*.xls|所有文件|*.*";
                saveFileDialog.FilterIndex = 1;
                saveFileDialog.RestoreDirectory = true;
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    strFilePath = saveFileDialog.FileName;
                    using (System.IO.FileStream fs = new FileStream(strFilePath, FileMode.Create))
                    {
                        fs.Write(bytes, 0, bytes.Length);
                    }
                    
                    if (ww.wwf.wwfbll.WWMessage.MessageDialogResult("导出Excel文件成功：" + strFilePath + "\r\n\n 是否打开此文件：" + strFilePath + "?"))
                    {
                        System.Diagnostics.Process.Start(strFilePath);
                    }
                }
                //*                
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        


        

        private void toolLast_Click(object sender, EventArgs e)
        {
            this.rptViewer.CurrentPage = this.rptViewer.LocalReport.GetTotalPages();
        }

        private void tool25_Click(object sender, EventArgs e)
        {
            this.rptViewer.ZoomMode = ZoomMode.Percent;
            this.rptViewer.ZoomPercent = 25;
        }

        private void tool50_Click(object sender, EventArgs e)
        {
            this.rptViewer.ZoomMode = ZoomMode.Percent;
            this.rptViewer.ZoomPercent = 50;
        }

        private void tool100_Click(object sender, EventArgs e)
        {
            this.rptViewer.ZoomMode = ZoomMode.Percent;
            this.rptViewer.ZoomPercent = 100;
        }

        private void tool200_Click(object sender, EventArgs e)
        {
            this.rptViewer.ZoomMode = ZoomMode.Percent;
            this.rptViewer.ZoomPercent = 200;
        }

        private void tool400_Click(object sender, EventArgs e)
        {
            this.rptViewer.ZoomMode = ZoomMode.Percent;
            this.rptViewer.ZoomPercent = 400;
        }

        private void toolWhole_Click(object sender, EventArgs e)
        {
            this.rptViewer.ZoomMode = ZoomMode.FullPage;
        }

        private void toolPageWidth_Click(object sender, EventArgs e)
        {
            this.rptViewer.ZoomMode = ZoomMode.PageWidth;
        }


        

        private void toolPrevious_Click(object sender, EventArgs e)
        {
            if (this.rptViewer.CurrentPage != 1)
                this.rptViewer.CurrentPage--;
        }

        private void toolNext_Click(object sender, EventArgs e)
        {
            if (this.rptViewer.CurrentPage != this.rptViewer.LocalReport.GetTotalPages())
                this.rptViewer.CurrentPage++;
        }
 

        private void 导出PDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Microsoft.Reporting.WinForms.Warning[] Warnings;
                string[] strStreamIds;
                string strMimeType;
                string strEncoding;
                string strFileNameExtension;

                byte[] bytes = this.rptViewer.LocalReport.Render("PDF", null, out strMimeType, out strEncoding, out strFileNameExtension, out strStreamIds, out Warnings);

                string strFilePath = @"C:\" + this.GetTimeStamp() + ".PDF";
                //*
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "PDF文件|*.pdf|所有文件|*.*";
                saveFileDialog.FilterIndex = 1;
                saveFileDialog.RestoreDirectory = true;
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    strFilePath = saveFileDialog.FileName;
                    using (System.IO.FileStream fs = new FileStream(strFilePath, FileMode.Create))
                    {
                        fs.Write(bytes, 0, bytes.Length);
                    }

                    if (ww.wwf.wwfbll.WWMessage.MessageDialogResult("导出PDF文件成功：" + strFilePath + "\r\n\n 是否打开此文件：" + strFilePath + "?"))
                    {
                        System.Diagnostics.Process.Start(strFilePath);
                    }
                }
                //*
                
            }
            catch (Exception ex)
            {
                 ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

       

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            try
            {
                PageSettings frm = new PageSettings(this.m_ReportName);
                frm.ShowDialog();
                frm.Dispose();
            }
            catch (Exception ex)
            {
                 ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.printDoc == null)
                {
                    this.printDoc = new EMFStreamPrintDocument(this.rptViewer.LocalReport, this.m_ReportName);
                    if (this.printDoc.ErrorMessage != string.Empty)
                    {
                        ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewer: \r\n    " + this.printDoc.ErrorMessage);
                        this.printDoc = null;
                        return;
                    }
                }

                this.printDoc.Print();
                this.printDoc = null;
            }
            catch (Exception ex)
            {
                 ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 刷新ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.rptViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                 ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void ssToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.rptViewer.CancelRendering(0);
        }

        private void 回退ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (this.rptViewer.LocalReport.IsDrillthroughReport)
                this.rptViewer.PerformBack();
        }

         

        
        private void toolExcel_Click(object sender, EventArgs e)
        {
            ExcelOut();
        }

        private void toolStripButtonQuery_Click(object sender, EventArgs e)
        {
            bllReport();
        }
   
        private void toolFirst_Click(object sender, EventArgs e)
        {

        }

        private void toolJump_Click(object sender, EventArgs e)
        {

        }

        private void toolFirst_Click_1(object sender, EventArgs e)
        {

        }
         
       
       
        
       
    }
}