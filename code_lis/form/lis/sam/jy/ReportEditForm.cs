﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;
using ww.form.lis.com;
using System.Collections;
namespace ww.form.lis.sam.jy
{
   
    public partial class ReportEditForm : com.ReportBaseForm
    {
        #region 自定义变量
        /// <summary>
        /// 报告格式 
        /// </summary>
        private UserdefinedFieldUpdateForm gsRepot = new UserdefinedFieldUpdateForm("lis_sam_report_edit_report");
        /// <summary>
        /// 结果格式 
        /// </summary>
        private UserdefinedFieldUpdateForm gsResult = new UserdefinedFieldUpdateForm("lis_sam_report_edit_result");
        /// <summary>
        /// 当前行 样本
        /// </summary>
        DataRowView rowCurrent = null;
        /// <summary>
        /// 样本ID
        /// </summary>
        string strfsample_id = "";
        /// <summary>
        /// 申请ID
        /// </summary>
        string strfapply_id = "";
        /// <summary>
        /// 审核标记
        /// </summary>
        string strfexamine_flag = "0";
        /// <summary>
        /// 仪器列表
        /// </summary>
        DataTable dtInstr = null;
        DataTable dtIMG = new DataTable();
        int intInit = 0;// 初始进入时不要显示结果
        #endregion

        #region 自定义方法
        /// <summary>
        /// 本页初始化方法
        /// </summary>
        private void WWInit()
        {
            try
            {

                this.com_listBindingSource_fstate.DataSource = this.WWComTypeSampleState();//检验状态
                this.com_listBindingSource_fjytype_id.DataSource = this.WWCheckType();//检验类别
                this.com_listBindingSource_fsample_type_id.DataSource = this.WWSampleType();//样本类别
                this.com_listBindingSource_fapply_dept_id.DataSource = this.WWApplyDept();//申请部门
                this.com_listBindingSource_fapply_user_id.DataSource = this.WWUserApply();//申请人
                this.com_listBindingSource_fjy_user_id.DataSource = this.WWUserCheck();//检验人
                dtInstr = this.bllInstr.BllInstrDTByUseAndGroupID(1, ww.wwf.wwfbll.LoginBLL.strDeptID);//仪器列表
                this.fjy_instrComboBox.DataSource = dtInstr;

                //20150610 begin  add by wjz 添加本机默认仪器功能
                string strInstrID = ww.form.wwf.Customization.DefaultInstrument.GetInstrID();
                if (!(string.IsNullOrWhiteSpace(strInstrID)))
                {
                    this.fjy_instrComboBox.SelectedValue = strInstrID;
                }
                //20150610 end    add by wjz 添加本机默认仪器功能

                this.ftype_idComboBox.DataSource = this.WWComTypeftype_id();//病人类别
                this.fsexComboBox.DataSource = this.WWComTypefsex();//性别
                this.fage_unitComboBox.DataSource = this.WWComTypefage_unit();//年龄单位
                this.fapply_dept_idComboBox.DataSource = this.com_listBindingSource_fapply_dept_id;//申请部门
                this.fsample_type_idComboBox.DataSource = this.com_listBindingSource_fsample_type_id;//样本
                this.fapply_user_idComboBox.DataSource = com_listBindingSource_fapply_user_id;//申请人
                DataTable dtSampling_user_id = this.WWUserCheck();
                this.fsampling_user_idComboBox.DataSource = dtSampling_user_id;//采样人
                this.fjy_user_idComboBox.DataSource = com_listBindingSource_fjy_user_id;//检验人
                DataTable dtexamine_user_id = this.WWUserCheck();
                this.fexamine_user_idComboBox.DataSource = dtexamine_user_id;//审核人
                dataGridViewReport.AutoGenerateColumns = false;
                dataGridViewResult.AutoGenerateColumns = false;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }

            DataColumn D0 = new DataColumn("FID", typeof(System.String));
            dtIMG.Columns.Add(D0);

            DataColumn D1 = new DataColumn("结果图1", typeof(System.Byte[]));
            dtIMG.Columns.Add(D1);

            DataColumn D2 = new DataColumn("结果图2", typeof(System.Byte[]));
            dtIMG.Columns.Add(D2);

            DataColumn D3 = new DataColumn("结果图3", typeof(System.Byte[]));
            dtIMG.Columns.Add(D3);

            DataColumn D4 = new DataColumn("结果图4", typeof(System.Byte[]));
            dtIMG.Columns.Add(D4);

            DataColumn D5 = new DataColumn("结果图5", typeof(System.Byte[]));
            dtIMG.Columns.Add(D5);
        }
        /// <summary>
        /// 取得报告列表
        /// </summary>
        private void WWSetReportList()
        {
            try
            {
                string strfjy_date = this.bllReport.DbDateTime1(this.fjy_dateDateTimePicker);
                string strfinstr_id = "";
                string strfinstrWhere = "";
                try
                {
                    strfinstr_id = fjy_instrComboBox.SelectedValue.ToString();//仪器ID   
                   
                }
                catch (Exception ex)
                {
                    WWMessage.MessageShowError(ex.ToString());
                }
                if (strfinstr_id == "" || strfinstr_id == null)
                    strfinstrWhere = "";
                else
                    strfinstrWhere = " and fjy_instr='" + strfinstr_id + "'";
                string strWhere = " where fjy_date = '" + strfjy_date + "' " + strfinstrWhere + " order by fsample_code";
                //string strWhere = " order by fsample_code";

               // string strsql = "SELECT * FROM LIS_REPORT_2008" + strWhere;
               // this.textBox1.Text = strsql;

                this.WWSetdtReport(strWhere);
                this.lIS_REPORTBindingSource.DataSource = this.dtReport;
                dataGridViewReport.DataSource = this.lIS_REPORTBindingSource;
                WWReportGridViesStyle();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        ///报告 列表风格设置
        /// </summary>
        private void WWReportGridViesStyle()
        {
            try
            {
                int intfstate = 0;
                for (int i = 0; i < this.dataGridViewReport.Rows.Count; i++)
                {
                    if (this.dataGridViewReport.Rows[i].Cells["fexamine_flag"].Value != null)
                        intfstate = Convert.ToInt32(this.dataGridViewReport.Rows[i].Cells["fexamine_flag"].Value.ToString());
                    switch (intfstate)
                    {
                        case 1:
                            this.dataGridViewReport.Rows[i].DefaultCellStyle.ForeColor = Color.Blue; //System.Drawing.SystemColors.Desktop;
                            break;
                        default:
                            this.dataGridViewReport.Rows[i].DefaultCellStyle.ForeColor = Color.Black; //System.Drawing.SystemColors.Desktop;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        /// <summary>
        ///  结果值设置
        /// </summary>
        private void WWGridViewResultValueSet()
        {
            try
            {
                if (this.dataGridViewResult.Rows.Count > 0)
                {
                    string strItem_id = "";
                    string strValue = "";//值；
                    string strFref = "";//参考值；
                    for (int intGrid = 0; intGrid < this.dataGridViewResult.Rows.Count; intGrid++)
                    {
                        if (this.dataGridViewResult.Rows[intGrid].Cells["fitem_id"].Value != null)
                            strItem_id = this.dataGridViewResult.Rows[intGrid].Cells["fitem_id"].Value.ToString();
                        else
                            strItem_id = "";

                        if (this.dataGridViewResult.Rows[intGrid].Cells["fvalue"].Value != null)
                            strValue = this.dataGridViewResult.Rows[intGrid].Cells["fvalue"].Value.ToString();
                        else
                            strValue = "";
                        int intfage = 0;
                        string strfsex = "";
                        string strfsample_type_id = "";
                        if (rowCurrent != null)
                        {
                            intfage = Convert.ToInt32(rowCurrent["fage"].ToString());
                            strfsex = rowCurrent["fsex"].ToString();
                            strfsample_type_id = rowCurrent["fsample_type_id"].ToString();
                        }
                        strFref = this.bllReport.BllItemfref(strItem_id, intfage, strfsex, strfsample_type_id);

                        //参考值
                        this.dataGridViewResult.Rows[intGrid].Cells["fitem_ref"].Value = strFref;
                        //标记                        
                        int intBJ = this.bllReport.BllItemValueBJ(strValue, strFref);
                        this.dataGridViewResult.Rows[intGrid].Cells["fitem_badge"].Value = this.bllReport.BllItemValueBJFname(intBJ);
                        //
                        if (intBJ == 2)
                        {
                            this.dataGridViewResult.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Blue; //System.Drawing.
                        }
                        if (intBJ == 3)
                        {
                            this.dataGridViewResult.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Red; //System.Drawing.
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        

        /// <summary>
        /// 增加一个报告
        /// </summary>
        /// <param name="upORdown"></param>
        private void WWReportAdd(string upORdown)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                string fjy_instr = "";//仪器ID
                string fjy_date = "";//检验日期

                try
                {
                    fjy_instr = this.fjy_instrComboBox.SelectedValue.ToString();
                }
                catch { }
                if (fjy_instr == "" || fjy_instr == null)
                {
                    WWMessage.MessageShowWarning("仪器为必选项，请选择仪器后重试！");
                    this.ActiveControl = fjy_instrComboBox;
                    return;
                }
                string fjytype_id = "";//检验类别
                DataRow[] drIN = this.dtInstr.Select("(finstr_id = '" + fjy_instr + "')");
                if (drIN.Length > 0)
                {
                    fjytype_id = drIN[0]["fjytype_id"].ToString();
                }

                String strYear = fjy_dateDateTimePicker.Value.Year.ToString();
                String strMonth = fjy_dateDateTimePicker.Value.Month.ToString();
                string strDay = fjy_dateDateTimePicker.Value.Day.ToString();
                if (strMonth.Length == 1)
                    strMonth = "0" + strMonth;
                if (strDay.Length == 1)
                    strDay = "0" + strDay;
                fjy_date = strYear + "-" + strMonth + "-" + strDay;

                string strAddRet = WWReportAddRow(this.bllReport.BllGetfsampleCodeUpOrDown(this.fsample_codeTextBox.Text, upORdown), upORdown, fjy_instr, fjytype_id, fjy_date);
                if (strAddRet == "true")
                { }
                else
                {
                    WWMessage.MessageShowError(strAddRet);
                }
                WWMoveReport();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;

            }
        }


        private string WWReportAddRow(string fsample_code, string upORdown, string fjy_instr, string fjytype_id, string fjy_date)
        {
            DataRow drReport = null;
            string strRet = "";
            //查找样本号存在否
            DataRow[] colList = this.dtReport.Select("(fjy_instr = '" + fjy_instr + "') AND (fjy_date = '" + fjy_date + "') AND (fsample_code = '" + fsample_code + "')");          
            foreach (DataRow drC in colList)
            {
                drReport = drC;
            }
            if (colList.Length > 0)//如果样本存在
            {
                strRet = "true";
                try
                {
                    this.lIS_REPORTBindingSource.Position = this.dtReport.Rows.IndexOf(drReport); //如何定位到当前存的行
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            else
            {
                drReport = dtReport.NewRow();               
                string strfapplyid = this.bllReport.DbNum("sam_jy_fapply_code");//申请单号
                string strTime = this.bllReport.DbServerDateTim();               
                string strApplyID = this.bllReport.DbNum("sam_fapply_id");
                drReport["fapply_id"] = strApplyID;
                drReport["fjytype_id"] = fjytype_id;// strfcheck_type_id;
                // drReport["fsample_type_id"] = "";// strfsample_type_id;
                if (radioButtonJZ.Checked)
                    drReport["fjz_flag"] = "1";//急诊否
                else
                    drReport["fjz_flag"] = 0;
                drReport["fcharge_flag"] = 0;//收费否
                drReport["fstate"] = "1";//状态                
                drReport["ftype_id"] = "门诊";
                drReport["fage"] = "0";
                drReport["fage_unit"] = "岁";
                drReport["fsex"] = "男";
               

                drReport["fcreate_user_id"] = LoginBLL.strPersonID;
                drReport["fcreate_time"] = strTime;
                drReport["fupdate_user_id"] = LoginBLL.strPersonID;
                drReport["fupdate_time"] = strTime;
                //
                drReport["fxhdb"] = 0;
                drReport["fxyld"] = 0;
                drReport["fheat"] = 0;
                drReport["fage_day"] = 0;
                drReport["fage_month"] = 0;
                drReport["fage_year"] = 0;
                drReport["fjyf"] = 0;//检验费                   

                //样本
                drReport["fsample_id"] = this.bllReport.DbGuid();
                drReport["fapply_id"] = strApplyID;
                drReport["fsample_barcode"] = strApplyID;

                drReport["fsample_code"] = fsample_code;
                drReport["fjy_instr"] = fjy_instr;
                drReport["fjy_date"] = fjy_date;
                drReport["fjy_group"] = LoginBLL.strDeptID;
                drReport["fjy_user_id"] = LoginBLL.strPersonID;

                //DateTime dtnow = Convert.ToDateTime(strTime);

                drReport["fjy_user_id"] = LoginBLL.strPersonID;
                drReport["fexamine_user_id"] = LoginBLL.strPersonID;
                drReport["fjy_time"] = strTime;
                drReport["fexamine_time"] = strTime;

                drReport["fexamine_flag"] = "0";
                drReport["fprint_flag"] = "0";
                drReport["fprint_count"] = "0";
                drReport["fread_flag"] = "0";
                drReport["fstate"] = "1";
                strRet = this.bllReport.BllReportAdd(drReport);
               
                if (strRet == "true")
                {
                    this.dtReport.Rows.Add(drReport);
                    
                    if (this.dataGridViewReport.Rows.Count > 0)
                    {                        
                        int intIndex = this.dataGridViewReport.CurrentRow.Index;
                        if (upORdown == "up")
                            intIndex = intIndex - 1;
                        else
                            intIndex = intIndex + 1;
                       // this.lIS_REPORTBindingSource.Position = this.lIS_REPORTBindingSource.Position;
                        this.lIS_REPORTBindingSource.Position = intIndex;
                    }
                }
            }
            return strRet;
        }
        
        /// <summary>
        /// 报告审核
        /// </summary>
        private void WWReportExamine()
        {
          
            try
            {
                Cursor = Cursors.WaitCursor;
                WWGridViewResultValueSet();
                if (ww.wwf.com.Public.IsNumber(this.fageTextBox.Text) == false)
                {
                    WWMessage.MessageShowWarning("年龄必须是数字！请填写后重试。");
                    return;
                }
                if (this.rowCurrent != null)
                {
                    if (this.dtReportResult != null)
                    {
                        if (this.dtReportResult.Rows.Count > 0)
                        {
                            rowCurrent["fexamine_flag"] = "1";
                            rowCurrent["fstate"] = "5";
                            WWReportUpdateSave();
                        }
                        else
                        {
                            WWMessage.MessageShowWarning("检验项目不能为空！");
                        }
                    }
                    
                }
                toolStripButtonSave.Enabled = false;
                
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        /// <summary>
        /// 报告 保存
        /// </summary>
        private void WWReportSave()
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                WWGridViewResultValueSet();
                if (ww.wwf.com.Public.IsNumber(this.fageTextBox.Text) == false)
                {
                    WWMessage.MessageShowWarning("年龄必须是数字！请填写后重试。");
                    return;
                }
                if (this.rowCurrent != null)
                {
                    if (this.dtReportResult != null)
                    {
                        if (this.dtReportResult.Rows.Count > 0)
                        {
                            rowCurrent["fexamine_flag"] = "0";
                            rowCurrent["fstate"] = "1";
                            WWReportUpdateSave();
                        }
                        else
                        {
                            WWMessage.MessageShowWarning("检验项目不能为空！");
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        /// <summary>
        /// 报告审核 取消
        /// </summary>
        private void WWReportExamineNO()
        {
            /*
             	70	样本状态	0		已作废申请	0	1	
	71	样本状态	1		已执行申请	1	1	
	72	样本状态	2		已打印条码	1	1	
	73	样本状态	3		已采集	1	1	
	74	样本状态	4		已接收	1	1	
	75	样本状态	5		已审核	1	1	
             */
            try
            {
                Cursor = Cursors.WaitCursor;
                if (this.dtReport.Rows.Count > 0)
                {
                    rowCurrent["fexamine_flag"] = "0";
                    rowCurrent["fstate"] = "1";
                }
                this.Validate();
                this.lIS_REPORTBindingSource.EndEdit();
                lIS_REPORTBindingSource.MoveNext();
                WWReportGridViesStyle();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        /// <summary>
        /// 报告修改保存
        /// </summary>
        private void WWReportUpdateSave()
        {
            this.Validate();
            this.lIS_REPORTBindingSource.EndEdit();
            this.dataGridViewResult.EndEdit();

            string strRet = this.bllReport.BllReportUpdate(this.rowCurrent, this.dtReportResult);
            if (strRet == "true")
            {
                //lIS_REPORTBindingSource.MoveNext();
                WWReportGridViesStyle();
                WWGridViewResultValueSet();                
            }
            else
            {
                WWMessage.MessageShowError(strRet);
            }
        }
        /// <summary>
        /// 报告删除
        /// </summary>
        private void WWReportDel()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (this.dtReport.Rows.Count > 0)
                {

                    if (WWMessage.MessageDialogResult("确定删除选中的报告？"))
                    {
                        string strRet = this.bllReport.BllReportDel(this.strfapply_id, this.strfsample_id);
                        if (strRet == "true")
                        {
                            WWSetReportList();
                        }
                        else
                        {
                            WWMessage.MessageShowError(strRet);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void WWResultList()
        {
           
            try
            {
                this.WWSetdtReportResult(strfapply_id);
                this.bindingSourceResult.DataSource = this.dtReportResult;
                this.dataGridViewResult.DataSource = this.bindingSourceResult;
                WWGridViewResultValueSet();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        ///结果_添加项目
        /// </summary>
        private void WWResultAddItem()
        {
            if (this.strfsample_id == "" || this.strfsample_id == null)
            {
                WWMessage.MessageShowWarning("对不起，报告记录为空，不能增加项目！\n\n请用鼠标单击列表中记录后重试");
                return;
            }
            else
            {
                string strfinstr_id = "";
                string strfinstr_name = "";
                try
                {
                    strfinstr_id = fjy_instrComboBox.SelectedValue.ToString();//仪器ID    
                    strfinstr_name = fjy_instrComboBox.Text;//仪器名称                
                }
                catch (Exception ex)
                {
                    WWMessage.MessageShowError(ex.ToString());
                }
                try
                {
                    WindowsResult r = new WindowsResult();
                    r.IlistChangedIlistAndDataTable += new IlistChangedIlistAndDataTable(this.EventResultChangedStringItem);
                    ItemSelectForm item = new ItemSelectForm(r, strfinstr_id, strfinstr_name);
                    item.ShowDialog();

                }
                catch (Exception ex)
                {
                    WWMessage.MessageShowError(ex.ToString());
                }
            }
        }
        /// <summary>
        ///选择仪器数据
        /// </summary>
        private void WWInstrData()
        {
            if (this.strfsample_id == "" || this.strfsample_id == null)
            {
                WWMessage.MessageShowWarning("对不起，报告记录为空，不能增加项目！\n\n请用鼠标单击列表中记录后重试");
                return;
            }
            else
            {
                string strfinstr_id = "";     
                try
                {
                    strfinstr_id = fjy_instrComboBox.SelectedValue.ToString();//仪器ID   
                }
                catch (Exception ex)
                {
                    WWMessage.MessageShowError(ex.ToString());
                }
                try
                {

                    WindowsResult r = new WindowsResult();
                    // r.IlistChangedIlist += new IlistChangedHandlerIlist(this.EventResultChangedStringItem);
                    r.IlistChangedIlistAndDataTable += new IlistChangedIlistAndDataTable(this.EventResultChangedStringItem);
                   // InstrDataForm item = new InstrDataForm(r, strfinstr_id);
                   // item.ShowDialog();

                }
                catch (Exception ex)
                {
                    WWMessage.MessageShowError(ex.ToString());
                }
            }
        }
        /// <summary>
        /// 返回 的项目ＩＤ列表
        /// </summary>
        /// <param name="s"></param>
        private void EventResultChangedStringItem(IList ilist, int intEidt,DataTable dtImgRet)
        {
            try
            {
                if (dtImgRet != null)
                {
                    /*
                    for (int intImg = 0; intImg < dtImgRet.Rows.Count; intImg++)
                    {
                        MessageBox.Show("返回的图：" + intImg.ToString());
                    }*/
                    if (dtImgRet.Rows.Count > 0)
                    {
                        string strret = this.bllReport.BllReportIMGAdd(dtImgRet, this.strfsample_id);
                        if (strret == "true")
                        {
                            this.WWImg();
                        }
                        else
                        {
                            WWMessage.MessageShowError("图形添加失败！" + strret);
                        }
                    }
                }
                string strItemID = "";//项目ＩＤ
                string strItemValue = "";//项目值
                DataRow[] drItem = null;
                for (int i = 0; i < ilist.Count; i++)
                {
                    drItem = null;
                    strItemID = ilist[i].ToString();
                    strItemValue = "";
                    string[] strItemIDxz = strItemID.Split(';');
                    if (strItemIDxz.Length > 1)
                    {
                        strItemID = strItemIDxz[0].ToString();
                        strItemValue = strItemIDxz[1].ToString();

                        
                    }
                    DataTable dtItem = this.bllItem.BllItem(" fitem_id='" + strItemID + "'");
                    drItem = this.dtReportResult.Select("fitem_id='" + strItemID + "'");
                    if (drItem.Length > 0)
                    {

                        foreach (DataRow drd in drItem)
                        {
                            drd["fvalue"] = strItemValue;
                        }

                    }
                    else
                    {
                        //dtItem = this.bllJY.BllItemDT(strItemID);
                        DataRow drResult = this.dtReportResult.NewRow();
                        drResult["fresult_id"] = this.bllReport.DbGuid();
                        drResult["fapply_id"] = this.strfapply_id;
                        drResult["fitem_id"] = strItemID;
                        drResult["fitem_code"] = dtItem.Rows[0]["fitem_code"];
                        drResult["fitem_name"] = dtItem.Rows[0]["fname"];
                        drResult["fitem_unit"] = dtItem.Rows[0]["funit_name_zw"];
                        drResult["forder_by"] = dtItem.Rows[0]["fprint_num"];
                        drResult["fvalue"] = strItemValue;
                        if (this.bllReport.BllReportResultAdd(drResult) > 0)
                        {
                            this.dtReportResult.Rows.Add(drResult);
                        }
                        else
                        {
                            WWMessage.MessageShowWarning(dtItem.Rows[0]["fname"] + " 添加失败！");
                        }

                    }
                }
                //this.bindingSourceResult.DataSource = this.dtReportResult;
                //this.dataGridViewResult.DataSource = this.bindingSourceResult;
                /*
                if (intEidt == 1)
                {
                    WWGridViewResultValueSet();
                }
                else
                {
                    if (ilist.Count > 0)
                    {
                        BllGetResult(this.jyfjy_id);
                        WWGridViewResultValueSet();
                    }
                }
                */
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 结果_删除
        /// </summary>
        private void WWResultDel()
        {
            try
            {

                if (this.dataGridViewResult.Rows.Count > 0)
                {
                    if (WWMessage.MessageDialogResult("确定删除选中的项目？"))
                    {
                        DataRowView drvCurr = (DataRowView)this.bindingSourceResult.Current;//行
                       
                        //string fresult_id = "";
                        //fresult_id = this.dataGridViewResult.CurrentRow.Cells["fresult_id"].ToString();
                        // MessageBox.Show(fresult_id);

                        if (this.bllReport.BllReportResultDel(drvCurr["fresult_id"].ToString()) > 0)
                            WWResultList();
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 快捷键 设置
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref   System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == (Keys.F1))
            {
                toolStripButtonAddItem.PerformClick();
                return true;
            }
            if (keyData == (Keys.F2))
            {
                toolStripButtonDelItem.PerformClick();
                return true;
            }
            if (keyData == (Keys.F3))
            {
                toolStripButtoninstrDate.PerformClick();
                return true;
            }
            if (keyData == (Keys.F4))
            {
                toolStripButtonU.PerformClick();
                return true;
            }
            if (keyData == (Keys.F5))
            {
                toolStripButtonD.PerformClick();
                return true;
            }
            if (keyData == (Keys.F6))
            {
                toolStripButtonSave.PerformClick();
                return true;
            }
            if (keyData == (Keys.F8))
            {
                toolStripButtonJCSH.PerformClick();
                return true;
            }
            if (keyData == (Keys.F9))
            {
                toolStripButtonPrint.PerformClick();
                return true;
            }
           
            return base.ProcessCmdKey(ref   msg, keyData);
        }

        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="printType"></param>
        private void WWPrint(int printType)
        {
            try
            {
                ww.form.lis.sam.Report.PrintCom pr = new ww.form.lis.sam.Report.PrintCom();
                pr.BllPrintViewer(printType, this.strfsample_id);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        DataTable dtIMG_1 = new DataTable();
        /// <summary>
        /// 结果图
        /// </summary>
        private void WWImg()
        {
            try
            {
                if (this.dtIMG.Rows.Count > 0)
                    this.dtIMG.Clear();
                if (this.dtIMG_1.Rows.Count > 0)
                    this.dtIMG_1.Clear();
                dtIMG_1 = this.bllReport.BllReportImgDT(this.strfsample_id);
                DataRow drimg = dtIMG.NewRow();
                int intImgCount = dtIMG_1.Rows.Count;
                if (intImgCount > 0)
                {

                    drimg["FID"] = dtIMG_1.Rows[0]["fsample_id"];
                    for (int iimg = 0; iimg < intImgCount; iimg++)
                    {
                        switch (iimg)
                        {
                            case 0:
                                img1.Visible = true;
                                img2.Visible = false;
                                img3.Visible = false;
                                img4.Visible = false;
                                img5.Visible = false;
                                drimg["结果图1"] = dtIMG_1.Rows[iimg]["fimg"];
                                break;
                            case 1:
                                img2.Visible = true;
                                img3.Visible = false;
                                img4.Visible = false;
                                img5.Visible = false;
                                drimg["结果图2"] = dtIMG_1.Rows[iimg]["fimg"];
                                break;
                            case 2:
                                img3.Visible = true;
                                img4.Visible = false;
                                img5.Visible = false;
                                drimg["结果图3"] = dtIMG_1.Rows[iimg]["fimg"];
                                break;
                            case 3:
                                img4.Visible = true;
                                img5.Visible = false;
                                drimg["结果图4"] = dtIMG_1.Rows[iimg]["fimg"];
                                break;
                            case 4:
                                img5.Visible = true;
                                drimg["结果图5"] = dtIMG_1.Rows[iimg]["fimg"];
                                break;
                            default:
                                break;
                        }
                    }

                }
                else
                {
                   
                    img1.Visible = false;
                    img2.Visible = false;
                    img3.Visible = false;
                    img4.Visible = false;
                    img5.Visible = false;
                }
                dtIMG.Rows.Add(drimg);
                lIS_REPORT_IMGBindingSource.DataSource = this.dtIMG;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 移动报告 时发生
        /// </summary>
        private void WWMoveReport()
        {
            intInit = intInit + 1;
            if (intInit == 1)
            {
                return;
            }
            try
            {
                strfsample_id = "";
                strfapply_id = "";
                strfexamine_flag = "0";
                rowCurrent = (DataRowView)lIS_REPORTBindingSource.Current;
                if (this.dtReportResult != null)
                    this.dtReportResult.Clear();
                if (rowCurrent != null)
                {
                    strfapply_id = rowCurrent["fapply_id"].ToString();
                    strfsample_id = rowCurrent["fsample_id"].ToString();
                    if (rowCurrent["fexamine_flag"] != null)
                        strfexamine_flag = rowCurrent["fexamine_flag"].ToString();
                    WWResultList();
                    WWImg();
                }
                if (strfexamine_flag == "1")
                {
                    toolStripButtonSave.Enabled = false;//报表保存
                    toolStripMenuItemReportDel.Enabled = false;//报告删除

                    toolStripMenuItemExamine.Enabled = false;//报告审核1
                    toolStripButtonJCSH.Enabled = false;//报告审核2

                    toolStripMenuItemPrint.Enabled = true;//报告打印
                    toolStripMenuItemResultAdd.Enabled = false;//结果增加
                    toolStripMenuItemIntData.Enabled = false;//仪器数据
                    toolStripMenuItemResultValue.Enabled = false;//结果常用值
                    toolStripMenuItemResultDel.Enabled = false;//结果删除
                    dataGridViewResult.ReadOnly = true;
                    toolStripButtonAddItem.Enabled = false;
                    toolStripButtonDelItem.Enabled = false;

                    toolStripMenuItemExamineNO.Enabled = true;
                    toolStripButtoninstrDate.Enabled = false;
                    toolStripButtonPrint.Enabled = true;

                }
                else
                {
                    toolStripButtonSave.Enabled = true;//报表保存
                    toolStripButtonPrint.Enabled = false;

                    toolStripMenuItemReportDel.Enabled = true;//报告删除
                    toolStripMenuItemExamine.Enabled = true;//报告审核
                    toolStripButtonJCSH.Enabled = true;//报告审核2
                    toolStripMenuItemPrint.Enabled = false;//报告打印
                    toolStripMenuItemExamineNO.Enabled = false;//报告解出审核
                    toolStripMenuItemResultAdd.Enabled = true;//结果增加
                    toolStripMenuItemIntData.Enabled = true;//仪器数据
                    toolStripMenuItemResultValue.Enabled = true;//结果常用值
                    toolStripMenuItemResultDel.Enabled = true;//结果删除

                    dataGridViewResult.ReadOnly = false;
                    toolStripButtonAddItem.Enabled = true;
                    toolStripButtonDelItem.Enabled = true;

                    toolStripButtoninstrDate.Enabled = true;

                }
                WWReportGridViesStyle();
            }

            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        #endregion

        public ReportEditForm()
        {
            InitializeComponent();
            WWInit();
        }

        private void ReportEditForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.ActiveControl = fsample_codeTextBox;
                WWSetReportList();
                this.gsRepot.DataGridViewSetStyleNew(this.dataGridViewReport);
                this.gsResult.DataGridViewSetStyleNew(this.dataGridViewResult);

            }

            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripMenuItem_ReportCol_Click(object sender, EventArgs e)
        {
            try
            {
                gsRepot.ShowDialog();

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void dataGridViewReport_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void toolStripMenuItem_result_Click(object sender, EventArgs e)
        {
            try
            {
                gsResult.ShowDialog();

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
       
        

        private void dataGridViewResult_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void toolStripButtonHelp_Click(object sender, EventArgs e)
        {
            this.WWOpenHelp("b1f98d8e8c9849d0b28d234435cb4930");
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void fjy_user_idComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonfapply_dept_id_Click(object sender, EventArgs e)
        {
            try
            {
                WindowsResult r = new WindowsResult();
                r.TextChangedString += new TextChangedHandlerString(this.EventResultChangedStringDept);

                WindowsDeptForm fca = new WindowsDeptForm(r);
                fca.ShowDialog();

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 返回表ID
        /// </summary>
        /// <param name="s"></param>
        private void EventResultChangedStringDept(string s)
        {
            //  MessageBox.Show(s);
            this.fapply_dept_idComboBox.SelectedValue = s;
            this.ActiveControl = froom_numTextBox;
        }

        private void buttonSample_Click(object sender, EventArgs e)
        {

            try
            {
                WindowsResult r = new WindowsResult();
                r.TextChangedString += new TextChangedHandlerString(this.EventResultChangedStringSample);

                WindowsSampleTypeForm fcac = new WindowsSampleTypeForm(r);
                fcac.ShowDialog();

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 返回表ID
        /// </summary>
        /// <param name="s"></param>
        private void EventResultChangedStringSample(string s)
        {
            //  MessageBox.Show(s);
            this.fsample_type_idComboBox.SelectedValue = s;
        }

        private void buttonfapply_user_id_Click(object sender, EventArgs e)
        {
            try
            {
                WindowsResult r = new WindowsResult();
                r.TextChangedString += new TextChangedHandlerString(this.EventResultChangedString);
                WindowsPersonForm fc = new WindowsPersonForm(r);
                fc.ShowDialog();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 返回表ID
        /// </summary>
        /// <param name="s"></param>
        private void EventResultChangedString(string s)
        {
            this.fapply_user_idComboBox.SelectedValue = s;
        }

        private void fjy_dateDateTimePicker_CloseUp(object sender, EventArgs e)
        {
            WWSetReportList();
        }

        private void toolStripButtonD_Click(object sender, EventArgs e)
        {
            WWReportAdd("down");            
        }

        private void toolStripButtonU_Click(object sender, EventArgs e)
        {
            WWReportAdd("up");
        }

        private void toolStripButtonJCSH_Click(object sender, EventArgs e)
        {
            WWReportExamine();
        }

        private void toolStripMenuItemSHJC_Click(object sender, EventArgs e)
        {
            WWReportExamineNO();
        }

        private void toolStripMenuItem_Ref_Click(object sender, EventArgs e)
        {
            try
            {
                WWSetReportList();          
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        
        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            WWReportDel();
        }

        private void toolStripButtonAddItem_Click(object sender, EventArgs e)
        {
            WWResultAddItem();
        }

        private void toolStripMenuItemResultAdd_Click(object sender, EventArgs e)
        {
            WWResultAddItem();
        }

        private void toolStripButtoninstrDate_Click(object sender, EventArgs e)
        {
            WWInstrData();
        }

        private void toolStripButtonDelItem_Click(object sender, EventArgs e)
        {
            WWResultDel();
        }

        private void toolStripMenuItemResultDel_Click(object sender, EventArgs e)
        {
            WWResultDel();
        }

        /// <summary>
        /// 当关闭窗口时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReportEditForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (this.dtReport.Rows.Count > 0)
                {
                    if (rowCurrent != null)
                    {
                        if (rowCurrent["fexamine_flag"].ToString() == "1")
                        { }
                        else
                        {
                            WWReportUpdateSave();
                        }
                    }
                    
                }
               
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void toolStripMenuItemIntData_Click(object sender, EventArgs e)
        {
            WWInstrData();

        }
        /// <summary>
        /// 结果列
        /// </summary>
        int intResultCol = 0;
        /// <summary>
        /// 结果行 
        /// </summary>
        int intResultRow = 0;       

        private void dataGridViewResult_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (this.dataGridViewResult.Rows.Count > 0)
                {
                    if (this.rowCurrent != null)
                    {
                        if (rowCurrent["fexamine_flag"].ToString() == "1")
                        {
                            WWMessage.MessageShowWarning("报告已经审核！不能再选择常用取值。");
                        }
                        else
                        {
                            intResultCol = e.ColumnIndex;//结果列
                            intResultRow = e.RowIndex;
                            string strColName = this.dataGridViewResult.Columns[intResultCol].Name.ToString();
                            string strID = "";
                            if (strColName == "fvalue")
                            {
                                strID = this.dataGridViewResult.Rows[intResultRow].Cells["fitem_id"].Value.ToString();

                                WindowsResult r = new WindowsResult();
                                r.TextChangedString += new TextChangedHandlerString(this.EventResultChangedStringItemValue);

                                ItemValueSelectForm fca = new ItemValueSelectForm(r, strID);
                                fca.ShowDialog();


                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 返回 常用取值
        /// </summary>
        /// <param name="s"></param>
        private void EventResultChangedStringItemValue(string s)
        {
            this.dataGridViewResult.Rows[intResultRow].Cells[intResultCol].Value = s;
            this.Validate();
            this.bindingSourceResult.EndEdit();
        }

        private void toolStripMenuItemResultValue_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dataGridViewResult.Rows.Count > 0)
                {
                    if (this.rowCurrent != null)
                    {
                        if (rowCurrent["fexamine_flag"].ToString() == "1")
                        {
                            WWMessage.MessageShowWarning("报告已经审核！不能再选择常用取值。");
                        }
                        else
                        {
                            intResultCol = this.dataGridViewResult.CurrentCell.ColumnIndex;//结果列
                            intResultRow = this.dataGridViewResult.CurrentCell.RowIndex;
                            string strColName = this.dataGridViewResult.Columns[intResultCol].Name.ToString();
                            string strID = "";
                            if (strColName == "fvalue")
                            {
                                strID = this.dataGridViewResult.Rows[intResultRow].Cells["fitem_id"].Value.ToString();

                                WindowsResult r = new WindowsResult();
                                r.TextChangedString += new TextChangedHandlerString(this.EventResultChangedStringItemValue);

                                ItemValueSelectForm fca = new ItemValueSelectForm(r, strID);
                                fca.ShowDialog();


                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonPrint_Click(object sender, EventArgs e)
        {
            WWPrint(0);
        }

        private void toolStripMenuItemPrinYL_Click(object sender, EventArgs e)
        {
            WWPrint(1);
        }

        private void toolStripMenuItemPrint_Click(object sender, EventArgs e)
        {
            WWPrint(0);
        }

        private void 导入图片ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                ImgBLL img = new ImgBLL();
                img.ImgIn(img1);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 导出图片ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                ImgBLL img = new ImgBLL();
                img.ImgOut(img1);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 清空图片ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                ImgBLL img = new ImgBLL();
                img.ImgNull(img1);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 图片浏览ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Bitmap bit = new Bitmap(img1.Image);
                ImgShowForm imgform = new ImgShowForm(bit);
                imgform.ShowDialog();
                imgform.Dispose();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void lIS_REPORTBindingSource_PositionChanged(object sender, EventArgs e)
        {
           //WWMoveReport();
        }
        private void dataGridViewReport_Click(object sender, EventArgs e)
        {
            WWMoveReport();
        }

        private void img1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                Bitmap bit = new Bitmap(img1.Image);
                ImgShowForm imgform = new ImgShowForm(bit);
                imgform.ShowDialog();
                imgform.Dispose();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripMenuItemExamine_Click(object sender, EventArgs e)
        {
            WWReportExamine();
        }
        /// <summary>
        /// 样本号:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fsample_codeTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ActiveControl = fsample_barcodeTextBox;
            }
        }
        /// <summary>
        /// 条码号:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fsample_barcodeTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ActiveControl = ftype_idComboBox;
            }
             
        }

        private void ftype_idComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ActiveControl = fhz_zyhTextBox;
            }
        }

        private void fhz_zyhTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ActiveControl = fhz_idTextBox;
            }
             
        }

        private void fhz_idTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ActiveControl = fnameTextBox;
            }           
        }

        private void fnameTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ActiveControl = fsexComboBox;
            }   
        }

        private void fsexComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ActiveControl = fageTextBox;
            }   
        }

        private void fageTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ActiveControl = fage_unitComboBox;
            }   
        }

        private void fage_unitComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ActiveControl = fapply_dept_idComboBox;
            }   
        }

        private void fapply_dept_idComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                 this.ActiveControl = buttonfapply_dept_id;
            }   
        }

        private void buttonfapply_dept_id_KeyDown(object sender, KeyEventArgs e)
        {           
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    WindowsResult r = new WindowsResult();
                    r.TextChangedString += new TextChangedHandlerString(this.EventResultChangedStringDept);

                    WindowsDeptForm fca = new WindowsDeptForm(r);
                    fca.ShowDialog();
                    this.ActiveControl = froom_numTextBox;
                }
                catch (Exception ex)
                {
                    WWMessage.MessageShowError(ex.ToString());
                }
            }   
        }

        private void froom_numTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ActiveControl = fbed_numTextBox;
            }   
        }

        private void fbed_numTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ActiveControl = fdiagnoseTextBox;
            }   
        }

        private void fdiagnoseTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ActiveControl = fsample_type_idComboBox;
            }   
        }

        private void fsample_type_idComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ActiveControl = buttonSample;
            }   
        }

        private void buttonSample_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                try
                {
                    WindowsResult r = new WindowsResult();
                    r.TextChangedString += new TextChangedHandlerString(this.EventResultChangedStringSample);

                    WindowsSampleTypeForm fcac = new WindowsSampleTypeForm(r);
                    fcac.ShowDialog();

                }
                catch (Exception ex)
                {
                    WWMessage.MessageShowError(ex.ToString());
                }
            }   
        }

        private void fapply_user_idComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ActiveControl = buttonfapply_user_id;
            }   
        }

        private void buttonfapply_user_id_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    WindowsResult r = new WindowsResult();
                    r.TextChangedString += new TextChangedHandlerString(this.EventResultChangedString);
                    WindowsPersonForm fc = new WindowsPersonForm(r);
                    fc.ShowDialog();
                }
                catch (Exception ex)
                {
                    WWMessage.MessageShowError(ex.ToString());
                }
            }  
        }

        private void fapply_timeDateTimePicker_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ActiveControl = fsampling_user_idComboBox;
            }  
        }

        private void fsampling_user_idComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ActiveControl = fsampling_timeDateTimePicker;
            }  
        }

        private void fsampling_timeDateTimePicker_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ActiveControl = fjy_user_idComboBox;
            }  
        }

        private void fjy_user_idComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ActiveControl = fjy_timeDateTimePicker;
            }  
        }

        private void fjy_timeDateTimePicker_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ActiveControl = fjy_timeDateTimePicker;
            }  
        }

        private void fexamine_user_idComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ActiveControl = fexamine_timeDateTimePicker;
            }  
        }

        private void fexamine_timeDateTimePicker_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ActiveControl = fremarkTextBox;
            }  
        }

        private void fremarkTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ActiveControl = this.fsample_codeTextBox;
            }  
        }

        private void toolStripButtonSave_Click(object sender, EventArgs e)
        {
            WWReportSave();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("测试源码");
        }
    }
} //b1f98d8e8c9849d0b28d234435cb4930
/*
  //this.Validate();
            //this.lIS_REPORTBindingSource.EndEdit();
 * 下表列出所有的有效格式字符串及其说明。

格式字符串 
说明 
 
d 
一位数或两位数的天数。 
 
dd 
两位数的天数。一位数天数的前面加一个零。 
 
ddd 
三个字符的星期几缩写。 
 
dddd 
完整的星期几名称。 
 
h 
12 小时格式的一位数或两位数小时数。 
 
hh 
12 小时格式的两位数小时数。一位数数值前面加一个 0。 
 
H 
24 小时格式的一位数或两位数小时数。 
 
HH 
24 小时格式的两位数小时数。一位数数值前面加一个 0。 
 
m 
一位数或两位数分钟值。 
 
mm 
两位数分钟值。一位数数值前面加一个 0。 
 
M 
一位数或两位数月份值。 
 
MM 
两位数月份值。一位数数值前面加一个 0。 
 
MMM 
三个字符的月份缩写。 
 
MMMM 
完整的月份名。 
 
s 
一位数或两位数秒数。 
 
ss 
两位数秒数。一位数数值前面加一个 0。 
 
t 
单字母 A.M./P.M. 缩写（A.M. 将显示为“A”）。 
 
tt 
两字母 A.M./P.M. 缩写（A.M. 将显示为“AM”）。 
 
y 
一位数的年份（2001 显示为“1”）。 
 
yy 
年份的最后两位数（2001 显示为“01”）。 
 
yyyy 
完整的年份（2001 显示为“2001”）。 
 

 */