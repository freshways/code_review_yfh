using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.IO;
using ww.form.wwf.print;
using ww.wwf.wwfbll;
 
using ww.lis.lisbll.sam;

namespace ww.form.lis.sam.jy
{
    public partial class TJGzlInstrForm : ww.form.wwf.SysBaseForm
    {
        private EMFStreamPrintDocument printDoc = null;
         protected InstrBLL bllInstr = new InstrBLL();
        protected TypeBLL bllType = new TypeBLL();
        string strReportName = "仪器工作量统计";
        public TJGzlInstrForm()
        {
            InitializeComponent();
                
        }

        private void ReportViewer_Load(object sender, EventArgs e)
        {
            try
            { 
                this.fjy_instrComboBox.DataSource = this.bllInstr.BllInstrDTByUseAndGroupID(1, ww.wwf.wwfbll.LoginBLL.strDeptID);//仪器列表
                fjy_instrComboBox.SelectedValue = "";

                //20150610 begin  add by wjz 添加本机默认仪器功能
                string strInstrID = ww.form.wwf.Customization.DefaultInstrument.GetInstrID();
                if (!(string.IsNullOrWhiteSpace(strInstrID)))
                {
                    this.fjy_instrComboBox.SelectedValue = strInstrID;
                }
                //20150610 end    add by wjz 添加本机默认仪器功能

                this.ftype_idComboBox.DataSource = this.bllType.BllComTypeDT("病人类别", 1, "");
                ftype_idComboBox.SelectedValue = "";
              
                
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());

               
            }
        }
        
        private void bllReport()
        {
            try
            {
              
                DataSet dsPrint = new DataSet();
                samDataSet.lis_tjDataTable dtPrintDataTable = new samDataSet.lis_tjDataTable();

                //数据

                DataTable dtData = dtDataRet();
                for (int i = 0; i < dtData.Rows.Count; i++)
                {
                    DataRow printRow = dtPrintDataTable.NewRow();
                    printRow["f1"] = LoginBLL.CustomerName + " - " + strReportName;// i.ToString();
                    printRow["f2"] = dtData.Rows[i]["仪器"].ToString();
                    printRow["f3"] = dtData.Rows[i]["样本人次"].ToString();
                    printRow["f4"] =  dtData.Rows[i]["项目数量"].ToString();
                    printRow["f5"] = dtData.Rows[i]["项目金额"].ToString();
     
                    dtPrintDataTable.Rows.Add(printRow);
                }
                dsPrint.Tables.Add(dtPrintDataTable);

                MainDataSet = dsPrint;
                //报表名
                ReportName = "lis" + strReportName;
                //报表数据源名
                MainDataSourceName = "samDataSet_lis_tj";
                //报表文档
                ReportPath = "ww.form.lis.sam.Report.TJGzlInstrReport.rdlc";

                this.AddReportDataSource(this.m_MainDataSet, this.m_MainDataSourceName, m_MainDataSourceName2);

                rptViewer.LocalReport.Refresh();
                rptViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowWarning(ex.ToString());
            }
        }
        private DataTable  dtDataRet()
        {  
           
            string fjy_date1 = this.fjy_dateDateTimePicker1.Value.ToString("yyyy-MM-dd");
            string fjy_date2 = this.fjy_dateDateTimePicker2.Value.ToString("yyyy-MM-dd");


            string strWhere = " and (样本日期>='" + fjy_date1 + "' and 样本日期<='" + fjy_date2 + "') ";
            if (this.fjy_instrComboBox.Text.Length > 0)
            {
                strWhere = strWhere + " and (仪器='" + fjy_instrComboBox.Text + "') ";
            }
            if (this.ftype_idComboBox.Text.Length > 0)
            {
                strWhere = strWhere + " and (类型='" + ftype_idComboBox.Text + "') ";
            }
            string sqlData = "SELECT  仪器, COUNT(样本号) AS 样本人次,sum(项目数) as 项目数量,sum(金额) as 项目金额 FROM  v_sam_jy where 1=1 " + strWhere + " GROUP BY 仪器";

            
            DataTable dtData = WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, sqlData);            
            object obj样本人次 = dtData.Compute("Sum(样本人次)", ""); 
            string str样本人次 = "0";
            if (obj样本人次 != null)
            {
                str样本人次 = obj样本人次.ToString();
            }
            else
            {
                str样本人次 = "0";
            }



            object obj项目数 = dtData.Compute("Sum(项目数量)", ""); 
            string str项目数 = "0";
            if (obj项目数 != null)
            { 
                str项目数 = obj项目数.ToString();
            }
            else
            {
                str项目数 = "0";
            }

            object obj金额 = dtData.Compute("Sum(项目金额)", ""); ;
            string str金额 = "";
            if (obj金额 != null)
            {
                str金额 = obj金额.ToString();
            }
           


            DataRow drSum = dtData.NewRow();
            if (str样本人次 != null & str样本人次.Length > 0)
            {
                drSum["样本人次"] = str样本人次;
            }
            if (str项目数 != null & str项目数.Length > 0)
            {
                drSum["项目数量"] = str项目数;
            }
            if (str金额 != null & str金额.Length > 0)
            {
                drSum["项目金额"] = str金额;
            }
            drSum["仪器"] = "合计：";
            dtData.Rows.Add(drSum); 
            return dtData;
        }
        /// <summary>
        /// Set the DataSource for the report using a strong-typed dataset
        /// Call it in Form_Load event
        /// </summary>
        private void AddReportDataSource(object ReportSource, string ReportDataSetName1, string ReportDataSetName2)
        {

            rptViewer.LocalReport.DataSources.Clear();//tao2013 新加
            System.Type type = ReportSource.GetType();
            if (type == null)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n   The datasource is of the wrong type!");
                return;
            }
            else
            {
                System.Reflection.PropertyInfo[] picData = type.GetProperties();
                bool bolExist = false;
                foreach (System.Reflection.PropertyInfo piData in picData)
                {
                    if (piData.Name == "Tables")
                    {
                        bolExist = true;
                        if (ReportDataSetName1 == string.Empty)
                        {
                            ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The dataset name for the report does not exist or is empty!");
                            return;
                        }

                        this.rptViewer.LocalReport.DataSources.Add(
                            new Microsoft.Reporting.WinForms.ReportDataSource(ReportDataSetName1,
                            (piData.GetValue(ReportSource, null) as System.Data.DataTableCollection)[0])
                            );

                        this.rptViewer.LocalReport.DataSources.Add(
                          new Microsoft.Reporting.WinForms.ReportDataSource(ReportDataSetName2,
                          (piData.GetValue(ReportSource, null) as System.Data.DataTableCollection)[0])
                          );

                        this.rptViewer.RefreshReport();
                        break;
                    }
                }

                if (!bolExist)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The datasource is of the wrong type!");
                    return;
                }
            }
        }

        private System.Data.DataTableCollection GetTableCollection(object ReportSource)
        {
            System.Type type = ReportSource.GetType();

            if (type == null)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n     The datasource is of the wrong type!");
                return null;
            }
            else
            {
                System.Reflection.PropertyInfo[] picData = type.GetProperties();
                bool bolExist = false;
                foreach (System.Reflection.PropertyInfo piData in picData)
                {
                    if (piData.Name == "Tables")
                    {
                        bolExist = true;
                        return piData.GetValue(ReportSource, null) as System.Data.DataTableCollection;
                    }
                }

                if (!bolExist)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The datasource is of the wrong type!");
                    return null;
                }
            }
            return null;
        }

       
        
        private void rptViewer_Drillthrough(object sender, Microsoft.Reporting.WinForms.DrillthroughEventArgs e)
        {
            if (this.m_DrillDataSet == null)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The dataset name for the report does not exist or is empty!");
                return;
            }
            else
            {
                if (this.m_DrillDataSourceName == string.Empty)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The datasource is of the wrong type!");
                    return;
                }
                else
                {
                    Microsoft.Reporting.WinForms.LocalReport report = e.Report as Microsoft.Reporting.WinForms.LocalReport;
                    report.DataSources.Add(new Microsoft.Reporting.WinForms.ReportDataSource(this.m_DrillDataSourceName, this.GetTableCollection(this.m_DrillDataSet)[0]));
                }
            }
        }



     



        private void printView()
        {
            try
            {
                if (this.printDoc == null)
                {
                    this.printDoc = new EMFStreamPrintDocument(this.rptViewer.LocalReport, this.m_ReportName);
                    if (this.printDoc.ErrorMessage != string.Empty)
                    {
                       
                        this.printDoc = null;
                        ww.wwf.wwfbll.WWMessage.MessageShowWarning("页面设置有误，请重新设置后重试");
                        PageSettings frm = new PageSettings(this.m_ReportName);
                        frm.ShowDialog();
                        frm.Dispose();     
                        return;
                    }
                }

                this.PreviewDialog.Document = this.printDoc;
                this.PreviewDialog.ShowDialog();
               
               // this.PreviewDialog.Dispose();
               // this.printDoc = null;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("页面设置有误，请重新设置后重试");
                PageSettings frm = new PageSettings(this.m_ReportName);
                frm.ShowDialog();
                frm.Dispose();     
            }
        }

        private string GetTimeStamp()
        {
            string strRet = string.Empty;
            System.DateTime dtNow = System.DateTime.Now;
            strRet += dtNow.Year.ToString() +
                        dtNow.Month.ToString("00") +
                        dtNow.Day.ToString("00") +
                        dtNow.Hour.ToString("00") +
                        dtNow.Minute.ToString("00") +
                        dtNow.Second.ToString("00") +
                        System.DateTime.Now.Millisecond.ToString("000");
            return strRet;

        }
        private void ExcelOut()
        {
            try
            {
                Microsoft.Reporting.WinForms.Warning[] Warnings;
                string[] strStreamIds;
                string strMimeType;
                string strEncoding;
                string strFileNameExtension;

                byte[] bytes = this.rptViewer.LocalReport.Render("Excel", null, out strMimeType, out strEncoding, out strFileNameExtension, out strStreamIds, out Warnings);

                string strFilePath = @"C:\" + this.GetTimeStamp() + ".xls";
                //*
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "XLS文件|*.xls|所有文件|*.*";
                saveFileDialog.FilterIndex = 1;
                saveFileDialog.RestoreDirectory = true;
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    strFilePath = saveFileDialog.FileName;
                    using (System.IO.FileStream fs = new FileStream(strFilePath, FileMode.Create))
                    {
                        fs.Write(bytes, 0, bytes.Length);
                    }
                    
                    if (ww.wwf.wwfbll.WWMessage.MessageDialogResult("导出Excel文件成功：" + strFilePath + "\r\n\n 是否打开此文件：" + strFilePath + "?"))
                    {
                        System.Diagnostics.Process.Start(strFilePath);
                    }
                }
                //*                
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        


        

        private void toolLast_Click(object sender, EventArgs e)
        {
            this.rptViewer.CurrentPage = this.rptViewer.LocalReport.GetTotalPages();
        }

        private void tool25_Click(object sender, EventArgs e)
        {
            this.rptViewer.ZoomMode = ZoomMode.Percent;
            this.rptViewer.ZoomPercent = 25;
        }

        private void tool50_Click(object sender, EventArgs e)
        {
            this.rptViewer.ZoomMode = ZoomMode.Percent;
            this.rptViewer.ZoomPercent = 50;
        }

        private void tool100_Click(object sender, EventArgs e)
        {
            this.rptViewer.ZoomMode = ZoomMode.Percent;
            this.rptViewer.ZoomPercent = 100;
        }

        private void tool200_Click(object sender, EventArgs e)
        {
            this.rptViewer.ZoomMode = ZoomMode.Percent;
            this.rptViewer.ZoomPercent = 200;
        }

        private void tool400_Click(object sender, EventArgs e)
        {
            this.rptViewer.ZoomMode = ZoomMode.Percent;
            this.rptViewer.ZoomPercent = 400;
        }

        private void toolWhole_Click(object sender, EventArgs e)
        {
            this.rptViewer.ZoomMode = ZoomMode.FullPage;
        }

        private void toolPageWidth_Click(object sender, EventArgs e)
        {
            this.rptViewer.ZoomMode = ZoomMode.PageWidth;
        }


        

        private void toolPrevious_Click(object sender, EventArgs e)
        {
            if (this.rptViewer.CurrentPage != 1)
                this.rptViewer.CurrentPage--;
        }

        private void toolNext_Click(object sender, EventArgs e)
        {
            if (this.rptViewer.CurrentPage != this.rptViewer.LocalReport.GetTotalPages())
                this.rptViewer.CurrentPage++;
        }
 

        private void 导出PDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Microsoft.Reporting.WinForms.Warning[] Warnings;
                string[] strStreamIds;
                string strMimeType;
                string strEncoding;
                string strFileNameExtension;

                byte[] bytes = this.rptViewer.LocalReport.Render("PDF", null, out strMimeType, out strEncoding, out strFileNameExtension, out strStreamIds, out Warnings);

                string strFilePath = @"C:\" + this.GetTimeStamp() + ".PDF";
                //*
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "PDF文件|*.pdf|所有文件|*.*";
                saveFileDialog.FilterIndex = 1;
                saveFileDialog.RestoreDirectory = true;
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    strFilePath = saveFileDialog.FileName;
                    using (System.IO.FileStream fs = new FileStream(strFilePath, FileMode.Create))
                    {
                        fs.Write(bytes, 0, bytes.Length);
                    }

                    if (ww.wwf.wwfbll.WWMessage.MessageDialogResult("导出PDF文件成功：" + strFilePath + "\r\n\n 是否打开此文件：" + strFilePath + "?"))
                    {
                        System.Diagnostics.Process.Start(strFilePath);
                    }
                }
                //*
                
            }
            catch (Exception ex)
            {
                 ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

       

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            try
            {
                PageSettings frm = new PageSettings(this.m_ReportName);
                frm.ShowDialog();
                frm.Dispose();
            }
            catch (Exception ex)
            {
                 ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.printDoc == null)
                {
                    this.printDoc = new EMFStreamPrintDocument(this.rptViewer.LocalReport, this.m_ReportName);
                    if (this.printDoc.ErrorMessage != string.Empty)
                    {
                        ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewer: \r\n    " + this.printDoc.ErrorMessage);
                        this.printDoc = null;
                        return;
                    }
                }

                this.printDoc.Print();
                this.printDoc = null;
            }
            catch (Exception ex)
            {
                 ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 刷新ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.rptViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                 ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void ssToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.rptViewer.CancelRendering(0);
        }

        private void 回退ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (this.rptViewer.LocalReport.IsDrillthroughReport)
                this.rptViewer.PerformBack();
        }

         

        
        private void toolExcel_Click(object sender, EventArgs e)
        {
            ExcelOut();
        }

        private void toolStripButtonQuery_Click(object sender, EventArgs e)
        {
            bllReport();
        }
   
        private void toolFirst_Click(object sender, EventArgs e)
        {

        }

        private void toolJump_Click(object sender, EventArgs e)
        {

        }

        private void toolFirst_Click_1(object sender, EventArgs e)
        {

        }
         
       
       
        
       
    }
}