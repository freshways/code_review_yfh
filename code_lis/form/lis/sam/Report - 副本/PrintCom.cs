﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ww.form.wwf.print;
using ww.wwf.wwfbll;
using ww.lis.lisbll.sam;
using System.Windows.Forms;

namespace ww.form.lis.sam.Report
{
    public class PrintCom
    { 
        /// <summary>
        /// 报告规则
        /// </summary>
        private ReportBLL bllReport = new ReportBLL();
        private InstrBLL bllInstr = new InstrBLL();
        private PersonBLL bllPerson = new PersonBLL();
        private SampleTypeBLL bllSample = new SampleTypeBLL();
        DeptBll bllDept = new DeptBll();
        DataSet dsPrint = new DataSet();
        samDataSet.sam_jy_printDataTable dtPrintDataTable = new samDataSet.sam_jy_printDataTable();
        string strMainDataSourceName = "samDataSet_sam_jy_print";//sam_jy_printDataTable
        public PrintCom()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //	
            dtPrintDataTable.Columns.Remove("结果图1");
            dtPrintDataTable.Columns.Remove("结果图2");
            dtPrintDataTable.Columns.Remove("结果图3");
            dtPrintDataTable.Columns.Remove("结果图4");
            dtPrintDataTable.Columns.Remove("结果图5");
            DataColumn D1 = new DataColumn("结果图1", typeof(System.Byte[]));
            dtPrintDataTable.Columns.Add(D1);

            DataColumn D2 = new DataColumn("结果图2", typeof(System.Byte[]));
            dtPrintDataTable.Columns.Add(D2);

            DataColumn D3 = new DataColumn("结果图3", typeof(System.Byte[]));
            dtPrintDataTable.Columns.Add(D3);

            DataColumn D4 = new DataColumn("结果图4", typeof(System.Byte[]));
            dtPrintDataTable.Columns.Add(D4);

            DataColumn D5 = new DataColumn("结果图5", typeof(System.Byte[]));
            dtPrintDataTable.Columns.Add(D5);
        }

        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="printType">1：打印预览；0：打印</param>
        /// <param name="strfsample_id">样本ID</param>
        public void BllPrintViewer(int printType, string strfsample_id)
        {
            try
            {
                DataTable dtReport = this.bllReport.BllReportDT(" where fsample_id='" + strfsample_id + "'");
                if (dtReport.Rows.Count > 0)
                {
                    DataTable dtResult = this.bllReport.BllReportResultDT(dtReport.Rows[0]["fapply_id"].ToString());
                    DataTable dtIMG = this.bllReport.BllReportImgDT(strfsample_id);

                    string strfinstr_id = "";//仪器ID 
                    if (dtReport.Rows[0]["fjy_instr"] != null)
                        strfinstr_id = dtReport.Rows[0]["fjy_instr"].ToString();
                    DataTable dtInstrReport = this.bllInstr.BllReportDT(strfinstr_id, 1);
                    string strReportCode = "ww.form.lis.sam.Report.ReportCom.rdlc";//报表代码

                    string strReportName = "";//报表名称
                    string strTimeAP = "";//检验时间安排 f1
                    string strCYdd = "";//采样地点 f2
                    if (dtInstrReport.Rows.Count > 0)
                    {
                        strReportCode = dtInstrReport.Rows[0]["fcode"].ToString();
                        strReportName = dtInstrReport.Rows[0]["fname"].ToString();
                        strTimeAP = dtInstrReport.Rows[0]["f1"].ToString();
                        strCYdd = dtInstrReport.Rows[0]["f2"].ToString();
                    }

                    int intResultCount = dtResult.Rows.Count;//结果记录数
                    if (intResultCount > 0)
                    {
                        //---------结果值
                        for (int i = 0; i < intResultCount; i++)
                        {
                            DataRow printRow = dtPrintDataTable.NewRow();
                            printRow["结果序号"] = i.ToString();
                            printRow["结果项目代码"] = dtResult.Rows[i]["fitem_code"];
                            printRow["结果项目名称"] = dtResult.Rows[i]["fitem_name"];
                            printRow["结果值"] = dtResult.Rows[i]["fvalue"];
                            printRow["结果项目单位"] = dtResult.Rows[i]["fitem_unit"];
                            printRow["结果标记"] = dtResult.Rows[i]["fitem_badge"];
                            printRow["结果参考值"] = dtResult.Rows[i]["fitem_ref"];
                            dtPrintDataTable.Rows.Add(printRow);
                        }

                        //----------报告值
                        dtPrintDataTable.Rows[0]["医院名称"] = LoginBLL.CustomerName;
                        dtPrintDataTable.Rows[0]["医院地址"] = LoginBLL.CustomerAdd;
                        dtPrintDataTable.Rows[0]["检验实验室"] = LoginBLL.strDeptName;
                        dtPrintDataTable.Rows[0]["医院联系电话"] = LoginBLL.CustomerTel;
                        dtPrintDataTable.Rows[0]["检验时间安排"] = strTimeAP;
                        dtPrintDataTable.Rows[0]["报表名称"] = strReportName;
                        dtPrintDataTable.Rows[0]["采样地点"] = strCYdd;
                        dtPrintDataTable.Rows[0]["姓名"] = dtReport.Rows[0]["fname"];
                        dtPrintDataTable.Rows[0]["性别"] = dtReport.Rows[0]["fsex"];
                        dtPrintDataTable.Rows[0]["年龄"] = dtReport.Rows[0]["fage"];
                        dtPrintDataTable.Rows[0]["年龄单位"] = dtReport.Rows[0]["fage_unit"];
                        dtPrintDataTable.Rows[0]["申请单号"] = dtReport.Rows[0]["fapply_id"];
                        dtPrintDataTable.Rows[0]["样本条码号"] = dtReport.Rows[0]["fsample_barcode"];
                        dtPrintDataTable.Rows[0]["样本号"] = dtReport.Rows[0]["fsample_code"];
                        if (dtReport.Rows[0]["fsample_type_id"] != null)
                            dtPrintDataTable.Rows[0]["样本类别"] = this.bllSample.BllSampleNameByID(dtReport.Rows[0]["fsample_type_id"].ToString());//样本类型
                        if (dtReport.Rows[0]["fapply_dept_id"] != null)
                            dtPrintDataTable.Rows[0]["送检科室"] = this.bllDept.BllDeptName(dtReport.Rows[0]["fapply_dept_id"].ToString());//申请部门
                        if (dtReport.Rows[0]["fapply_user_id"] != null)
                            dtPrintDataTable.Rows[0]["申请者"] = bllPerson.BllPersonNameByID(dtReport.Rows[0]["fapply_user_id"].ToString());//申请人
                        if (dtReport.Rows[0]["fjy_user_id"] != null)
                            dtPrintDataTable.Rows[0]["检验者"] = bllPerson.BllPersonNameByID(dtReport.Rows[0]["fjy_user_id"].ToString());//检验人
                        if (dtReport.Rows[0]["fexamine_user_id"] != null)
                            dtPrintDataTable.Rows[0]["审核者"] = bllPerson.BllPersonNameByID(dtReport.Rows[0]["fexamine_user_id"].ToString());//申核人
                        dtPrintDataTable.Rows[0]["床号"] = dtReport.Rows[0]["fbed_num"];
                        dtPrintDataTable.Rows[0]["病人ID"] = dtReport.Rows[0]["fhz_id"];
                        dtPrintDataTable.Rows[0]["诊断"] = dtReport.Rows[0]["fdiagnose"];
                        dtPrintDataTable.Rows[0]["申请时间"] = dtReport.Rows[0]["fapply_time"];
                        dtPrintDataTable.Rows[0]["采样时间"] = dtReport.Rows[0]["fsampling_time"];
                        dtPrintDataTable.Rows[0]["收样时间"] = dtReport.Rows[0]["fget_user_time"];
                        dtPrintDataTable.Rows[0]["检验时间"] = dtReport.Rows[0]["fjy_time"];
                        dtPrintDataTable.Rows[0]["审核时间"] = dtReport.Rows[0]["fexamine_time"];
                        if (printType == 1)
                        {
                            dtPrintDataTable.Rows[0]["打印时间"] = dtReport.Rows[0]["fprint_time"];
                        }
                        else
                        {
                            dtPrintDataTable.Rows[0]["打印时间"] = this.bllPerson.DbServerDateTim();//dtReport.Rows[0]["fprint_time"];
                        }
                        dtPrintDataTable.Rows[0]["备注"] = dtReport.Rows[0]["fremark"];
                        dsPrint.Tables.Add(dtPrintDataTable);
                        //----------图值
                        if (dtIMG.Rows.Count > 0)
                        {
                            for (int iimg = 0; iimg < dtIMG.Rows.Count; iimg++)
                            {
                                switch (iimg)
                                {
                                    case 0:
                                        dtPrintDataTable.Rows[0]["结果图1"] = dtIMG.Rows[iimg]["fimg"];
                                        break;
                                    case 1:
                                        dtPrintDataTable.Rows[0]["结果图2"] = dtIMG.Rows[iimg]["fimg"];
                                        break;
                                    case 2:
                                        dtPrintDataTable.Rows[0]["结果图3"] = dtIMG.Rows[iimg]["fimg"];
                                        break;
                                    case 3:
                                        dtPrintDataTable.Rows[0]["结果图4"] = dtIMG.Rows[iimg]["fimg"];
                                        break;
                                    case 4:
                                        dtPrintDataTable.Rows[0]["结果图5"] = dtIMG.Rows[iimg]["fimg"];
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                        WWPrintViewer(printType, strfsample_id, strReportName, strReportCode);
                    }
                    else
                    {
                        WWMessage.MessageShowWarning("报告结果为空！");
                    }
                }
                else
                {
                    WWMessage.MessageShowWarning("报告为空，操作失败！请选择报告后重试。");
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strReportName">报表数据源名</param>
        /// <param name="strReportPath">报表文档</param>
        private void WWPrintViewer(int printType,string fjy_id,string strReportName, string strReportPath)
        {
            try
            {
                if (printType == 1)
                {
                    //报表公共窗口
                    ReportViewer feportviewer = new ReportViewer();
                    //报表数据集
                    feportviewer.MainDataSet = dsPrint;
                    //报表名
                    feportviewer.ReportName = strReportName;
                    //报表数据源名
                    feportviewer.MainDataSourceName = strMainDataSourceName;
                    //报表文档
                    feportviewer.ReportPath = strReportPath;
                    //显示报表窗口
                    feportviewer.ShowDialog();
                }
                else {
                    //报表公共窗口
                    ReportPrint feportviewer = new ReportPrint();
                    //报表数据集
                    feportviewer.MainDataSet = dsPrint;
                    //报表名
                    feportviewer.ReportName = strReportName;
                    //报表数据源名
                    feportviewer.MainDataSourceName = strMainDataSourceName;
                    //报表文档
                    feportviewer.ReportPath = strReportPath;
                    //显示报表窗口
                    feportviewer.ShowDialog();
                    this.bllReport.BllReportUpdatePrintTime(fjy_id);
                }
               
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        

    }
}

/*
 string 医院名称, 
                        string 医院地址, 
                        string 检验实验室, 
                        string 医院联系电话, 
                        string 检验时间安排, 
                        string 报表名称, 
                        string 采样地点, 
                        string 姓名, 
                        string 性别, 
                        string 年龄, 
                        string 年龄单位, 
                        string 病人ID, 
                        string 申请单号, 
                        string 样本号, 
                        string 病历号, 
                        string 送检科室, 
                        string 病区, 
                        string 床号, 
                        string 房间号, 
                        string 检验类别, 
                        string 样本类别, 
                        string 申请时间, 
                        string 采样时间, 
                        string 收样时间, 
                        string 打印时间, 
                        string 检验时间, 
                        string 报告时间, 
                        string 申请者, 
                        string 采样者, 
                        string 接收者, 
                        string 检验者, 
                        string 审核者, 
                        string 诊断, 
                        string 备注, 
                        string 结果序号, 
                        string 结果项目代码, 
                        string 结果项目名称, 
                        string 结果项目单位, 
                        string 结果值, 
                        string 结果参考值, 
                        string 结果标记
 */
/*
Name	Code	Data Type	Primary	Foreign Key	Mandatory
检验_id	fjy_id	varchar(32)	TRUE	FALSE	TRUE
仪器_id	finstr_id	varchar(32)	FALSE	FALSE	TRUE
检验日期	fjy_date	varchar(32)	FALSE	FALSE	TRUE
样本号	fsample_code	varchar(32)	FALSE	FALSE	TRUE
打印否	fprint_flag	int	FALSE	FALSE	FALSE
审核否	fexamine_flag	int	FALSE	FALSE	FALSE
状态	fstate	varchar(32)	FALSE	FALSE	FALSE
检验组_id	fjygroup_id	varchar(32)	FALSE	FALSE	FALSE
检验类型_id	fjytype_id	varchar(32)	FALSE	FALSE	FALSE
样本类型_id	fsample_type_id	varchar(32)	FALSE	FALSE	FALSE
P患者类型_id	fhz_type_id	varchar(32)	FALSE	FALSE	FALSE
P患者_id	fhz_id	varchar(32)	FALSE	FALSE	FALSE
P性别	fsex	varchar(32)	FALSE	FALSE	FALSE
P患者姓名	fname	varchar(32)	FALSE	FALSE	FALSE
P年龄	fage	int	FALSE	FALSE	FALSE
P年龄单	fage_year	int	FALSE	FALSE	FALSE
F年龄(月)	位	fage_unit	varchar(32)	FALSE	FALSE	FALSE
F年龄(年)fage_month	int	FALSE	FALSE	FALSE
F年龄(天)	fage_day	int	FALSE	FALSE	FALSE
P病区	fward_num	varchar(32)	FALSE	FALSE	FALSE
P房间号	froom_num	varchar(32)	FALSE	FALSE	FALSE
P床号	fbed_num	varchar(32)	FALSE	FALSE	FALSE
PABO血型	fblood_abo	varchar(32)	FALSE	FALSE	FALSE
PRH血型	fblood_rh	varchar(32)	FALSE	FALSE	FALSE
P检验目的	fjymd	varchar(200)	FALSE	FALSE	FALSE
P临床诊断	fdiagnose	varchar(200)	FALSE	FALSE	FALSE
P住院号	fzyh	varchar(32)	FALSE	FALSE	FALSE
P体温	fheat	float	FALSE	FALSE	FALSE
P吸氧浓度	fxyld	float	FALSE	FALSE	FALSE
P曾用药	fcyy	varchar(200)	FALSE	FALSE	FALSE
P血红蛋白	fxhdb	int	FALSE	FALSE	FALSE
R申请号	fapply_code	varchar(32)	FALSE	FALSE	FALSE
R申请人	fapply_user_id	varchar(32)	FALSE	FALSE	FALSE
R申请科室_id	fapply_dept_id	varchar(32)	FALSE	FALSE	FALSE
R申请时间	fapply_time	varchar(32)	FALSE	FALSE	FALSE
R采样人_id	fsampling_user_id	varchar(32)	FALSE	FALSE	FALSE
R采样时间	fsampling_time	varchar(32)	FALSE	FALSE	FALSE
R接收人_id	fget_user_id	varchar(32)	FALSE	FALSE	FALSE
R接样时间	fget_user_time	varchar(32)	FALSE	FALSE	FALSE
R送样人_id	fsend_user_id	varchar(32)	FALSE	FALSE	FALSE
R送样时间	fsend_user_time	varchar(32)	FALSE	FALSE	FALSE
R检验人_id	fcheck_user_id	varchar(32)	FALSE	FALSE	FALSE
R检验时间	fcheck_time	varchar(32)	FALSE	FALSE	FALSE
R审核医师_id	fexamine_user_id	varchar(32)	FALSE	FALSE	FALSE
R审核时	fexamine_time	varchar(32)	FALSE	FALSE	FALSE
R处理人_id	fhandling_user_id	varchar(32)	FALSE	FALSE	FALSE
R处理时间间	fhandling_user_time	varchar(32)	FALSE	FALSE	FALSE
P打印时间	fprint_time	varchar(32)	FALSE	FALSE	FALSE
P打印次数	fprint_count	int	FALSE	FALSE	FALSE
R报告科室	freport_dept_id	varchar(32)	FALSE	FALSE	FALSE
排序号	forder_by	varchar(32)	FALSE	FALSE	FALSE
收费否	fcharge_flag	int	FALSE	FALSE	FALSE
检验费	fjyf	float	FALSE	FALSE	FALSE
费别_id	fcharge_id	varchar(32)	FALSE	FALSE	FALSE
创建人_id	fcreate_user_id	varchar(32)	FALSE	FALSE	FALSE
创建时间	fcreate_time	varchar(32)	FALSE	FALSE	FALSE
修改人_id	fupdate_user_id	varchar(32)	FALSE	FALSE	FALSE
修改时间	fupdate_time	varchar(32)	FALSE	FALSE	FALSE
备注	fremark	varchar(200)	FALSE	FALSE	FALSE
*/