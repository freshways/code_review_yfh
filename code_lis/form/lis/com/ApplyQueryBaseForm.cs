﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.form.wwf;
using ww.lis.lisbll.sam;
using ww.wwf.wwfbll;
using System.Collections;

namespace ww.form.lis.com
{
    public partial class ApplyQueryBaseForm : SysBaseForm
    {
        protected ww.wwf.wwfbll.UserdefinedFieldUpdateForm showApply = null;
        protected ww.wwf.wwfbll.UserdefinedFieldUpdateForm showResult = null;
        protected PatientBLL bllPatient = new PatientBLL();
        protected TypeBLL bllType = new TypeBLL();//公共类型逻辑  
        protected ApplyBLL bllApply = new ApplyBLL();//申请规则
        protected string strHelpID = "";//帮助ID

        protected DataTable dtMain = new DataTable();
        protected DataTable dtResult = new DataTable();

        string str申请单id = "";

        //changed by wjz 20150520 将旧的导出方式替换为NPOI的实现方式 ▽
        //ExcelHelper excelhelp = new ExcelHelper("","");
        //changed by wjz 20150520 将旧的导出方式替换为NPOI的实现方式 △
        public ApplyQueryBaseForm()
        {
            InitializeComponent();
            dataGridViewApply.AutoGenerateColumns = false;
            this.dataGridViewItem.AutoGenerateColumns = false;
        }

        private void ApplyQueryBaseForm_Load(object sender, EventArgs e)
        {

        }
        #region        用户自定义方法
        protected void WWComTypeList()
        {
            try
            {
                this.sam_typeBindingSource_fstate.DataSource = this.bllType.BllComTypeDT("样本状态", 1,"");
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 取得患者树
        /// </summary>
        protected void WWGetTreeHZ()
        {
            try
            {
                string pid = "-1";
                string fjy_date1 = this.bllPatient.DbDateTime1(fjy_dateDateTimePicker1);
                string fjy_date2 = this.bllPatient.DbDateTime2(fjy_dateDateTimePicker2);
                string strDateif = " and (ftime_registration>='" + fjy_date1 + "' and ftime_registration<='" + fjy_date2 + "')";
                this.wwTreeViewHZ.ZADataTable = this.bllPatient.BllDeptAndPatientDT(strDateif);
                this.wwTreeViewHZ.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeViewHZ.ZAParentFieldName = "fpid";//上级字段名称
                this.wwTreeViewHZ.ZADisplayFieldName = "fname";//显示字段名称
                this.wwTreeViewHZ.ZAKeyFieldName = "fid";//主键字段名称
                this.wwTreeViewHZ.ZAToolTipTextName = "tag";
                this.wwTreeViewHZ.ZATreeViewShow();//显示树        
                //try
                //{
                // this.wwTreeViewHZ.SelectedNode = wwTreeViewHZ.Nodes[0];
                //}
                //catch { }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 设置风格 申请
        /// </summary>
        /// <param name="strID"></param>
        protected void WWSetShowApply(string strID)
        {
            try
            {
                showApply = new ww.wwf.wwfbll.UserdefinedFieldUpdateForm(strID);
                dtMain = this.bllApply.BllSamplingMainDT(" t2.fsample_barcode='-1'");
                bindingSourceApply.DataSource = dtMain;
                dataGridViewApply.DataSource = bindingSourceApply;

                this.showApply.DataGridViewSetStyleNew(this.dataGridViewApply);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        ///  设置风格 结果
        /// </summary>
        /// <param name="strID"></param>
        protected void WWSetShowResult(string strID)
        {
            showResult = new ww.wwf.wwfbll.UserdefinedFieldUpdateForm(strID);
            this.dtResult = this.bllApply.BllSamplingResultDT("-1");
            this.dataGridViewItem.DataSource = this.dtResult;
            this.showResult.DataGridViewSetStyleNew(this.dataGridViewItem);
        }
        
        string strfhz_id = "";
        string strfdept_id = "";
        /// <summary>
        /// 取得申请列表
        /// </summary>
        private void WWGetApplyList()
        {
            try
            {
                string fjy_date1 = this.bllApply.DbDateTime1(fjy_dateDateTimePicker1);
                string fjy_date2 = this.bllApply.DbDateTime2(fjy_dateDateTimePicker2);
                string strDateif = "";
                if (strfhz_id == "" & strfdept_id!="")
                    strDateif = " (fapply_dept_id='" + strfdept_id + "')and(t1.fapply_time>='" + fjy_date1 + "' and t1.fapply_time<='" + fjy_date2 + "')";
                else if (strfhz_id != "" & strfdept_id == "")
                    strDateif = " (fhz_id='" + strfhz_id + "')and(t1.fapply_time>='" + fjy_date1 + " 00:00:00" + "' and t1.fapply_time<='" + fjy_date2 + " 23:59:59" + "')";
                dtMain = this.bllApply.BllSamplingMainDT(strDateif);
                this.bindingSourceApply.DataSource = dtMain;
                dataGridViewApply.DataSource = bindingSourceApply;
                this.bN.BindingSource = bindingSourceApply;
                int intfstate = 0;
                for (int intGrid = 0; intGrid < this.dataGridViewApply.Rows.Count; intGrid++)
                {
                    intfstate = 0;
                    if (this.dataGridViewApply.Rows[intGrid].Cells["fstate"].Value != null)
                        intfstate = Convert.ToInt32(this.dataGridViewApply.Rows[intGrid].Cells["fstate"].Value.ToString());
                    //if (intfstate == 1)//已执行申请
                        //this.dataGridViewApply.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Red;
                    if (intfstate == 2)//已打印条码
                        this.dataGridViewApply.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Blue;
                    if (intfstate ==3)//已打印条码
                        this.dataGridViewApply.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Red;
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            /*
             	70	样本状态	0		已作废申请	0	1	
	71	样本状态	1		已执行申请	1	1	
	72	样本状态	2		已打印条码	1	1	
	73	样本状态	3		已采集	1	1	
	74	样本状态	4		已接收	1	1	
             */
        }
        private void WWGetResultList(string fapply_id)
        {
            this.dtResult = this.bllApply.BllSamplingResultDT(fapply_id);
            this.dataGridViewItem.DataSource = this.dtResult;
        }
        /// <summary>
        /// 导出Excel
        /// </summary>
        private void WWExcelOutApply()
        {
            try
            {
                this.Validate();
                this.bindingSourceApply.EndEdit();
                if (this.dataGridViewApply.Rows.Count > 0)
                {
                    //changed by wjz 20150520 将旧的导出方式替换为NPOI的实现方式 ▽
                    //excelhelp.GridViewToExcel(this.dataGridViewApply, "", 1, 1);
                    ExcelHelper.DataGridViewToExcel(this.dataGridViewApply);
                    //changed by wjz 20150520 将旧的导出方式替换为NPOI的实现方式 △
                }
                else
                {
                    WWMessage.MessageShowWarning("暂无记录,不可导出!");
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void WWExcelOutResult()
        {
            try
            {
                this.Validate();
                this.dataGridViewItem.EndEdit();
                if (this.dataGridViewItem.Rows.Count > 0)
                {
                    //changed by wjz 20150520 将旧的导出方式替换为NPOI的实现方式 ▽
                    //excelhelp.GridViewToExcel(this.dataGridViewItem, "", 1, 1);
                    ExcelHelper.DataGridViewToExcel(this.dataGridViewItem);
                    //changed by wjz 20150520 将旧的导出方式替换为NPOI的实现方式 △
                }
                else
                {
                    WWMessage.MessageShowWarning("暂无记录,不可导出!");
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 快捷键 设置
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref   System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {

            if (keyData == (Keys.F8))
            {
                toolStripButtonQuery.PerformClick();
                return true;
            }
            if (keyData == (Keys.F1))
            {
                toolStripButtonHelp.PerformClick();
                return true;
            }
            if (keyData == (Keys.F3))
            {
                toolStripButton1.PerformClick();
                return true;
            }
            return base.ProcessCmdKey(ref   msg, keyData);
        }
        #endregion

        private void toolStripButtonHelp_Click(object sender, EventArgs e)
        {
            try
            {
                HelpForm help = new HelpForm(this.strHelpID);
                help.Show();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 关闭ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void fjy_dateDateTimePicker1_CloseUp(object sender, EventArgs e)
        {
            WWGetTreeHZ();
        }

        private void fjy_dateDateTimePicker2_CloseUp(object sender, EventArgs e)
        {
            WWGetTreeHZ();
        }

        private void toolStripMenuItem_applygs_Click(object sender, EventArgs e)
        {
            try
            {
                showApply.ShowDialog();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }


        private void toolStripMenuItem_checkidtemgs_Click(object sender, EventArgs e)
        {
            try
            {
                showResult.ShowDialog();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 帮助ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                HelpForm help = new HelpForm(strHelpID);
                help.Show();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
      
        private void wwTreeViewHZ_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
              string  strEdit = "";
                strfhz_id = "";
                strfdept_id = "";
                strEdit = e.Node.ToolTipText;
                if (strEdit == "patient")
                {
                    strfhz_id = e.Node.Name;
                }
                else if (strEdit == "dept")
                {
                    strfdept_id = e.Node.Name;
                }
                WWGetApplyList();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
       
        private void bindingSourceApply_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                DataRowView rowCurrent = (DataRowView)this.bindingSourceApply.Current;
                if (this.dtResult.Rows.Count > 0)
                    this.dtResult.Clear();
                if (rowCurrent != null)
                {
                    string strfapply_id = rowCurrent["fapply_id"].ToString();
                    
                    str申请单id = strfapply_id;
                    WWGetResultList(strfapply_id);
                }

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonQuery_Click(object sender, EventArgs e)
        {
            WWGetApplyList();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            WWExcelOutApply();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            WWExcelOutResult();
        }

        private void 申请格式ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                showApply.ShowDialog();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 项目模式ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                showResult.ShowDialog();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 申请导出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WWExcelOutApply();
        }

        private void 项目导出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WWExcelOutResult();
        }

        private void toolStripButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridViewApply_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void dataGridViewItem_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                ww.form.lis.sam.sq.ApplyPrint pr = new ww.form.lis.sam.sq.ApplyPrint();
                pr.BllPrintViewer(1, this.str申请单id);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

    }
}