﻿namespace ww.form.lis.com
{
    partial class ApplyQueryBaseForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ApplyQueryBaseForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bN = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSplitButtonGS = new System.Windows.Forms.ToolStripSplitButton();
            this.申请格式ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.项目模式ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSplitButtonDC = new System.Windows.Forms.ToolStripSplitButton();
            this.申请导出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.项目导出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonQuery = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonHelp = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonClose = new System.Windows.Forms.ToolStripButton();
            this.bindingSourceApply = new System.Windows.Forms.BindingSource(this.components);
            this.panel4 = new System.Windows.Forms.Panel();
            this.wwTreeViewHZ = new ww.wwf.control.WWControlLib.WWTreeView();
            this.imageListTree = new System.Windows.Forms.ImageList(this.components);
            this.panel5 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.fjy_dateDateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.fjy_dateDateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.contextMenuStripApplyQuery = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem_applygs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_checkidtemgs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.帮助ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.关闭ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridViewApply = new System.Windows.Forms.DataGridView();
            this.fstate = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.sam_typeBindingSource_fstate = new System.Windows.Forms.BindingSource(this.components);
            this.samDataSet = new ww.form.lis.sam.samDataSet();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridViewItem = new System.Windows.Forms.DataGridView();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).BeginInit();
            this.bN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceApply)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.contextMenuStripApplyQuery.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewApply)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sam_typeBindingSource_fstate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewItem)).BeginInit();
            this.SuspendLayout();
            // 
            // bN
            // 
            this.bN.AddNewItem = null;
            this.bN.AutoSize = false;
            this.bN.CountItem = this.bindingNavigatorCountItem;
            this.bN.DeleteItem = null;
            this.bN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.toolStripSeparator8,
            this.toolStripSplitButtonGS,
            this.toolStripSeparator4,
            this.toolStripSplitButtonDC,
            this.toolStripSeparator5,
            this.toolStripButtonQuery,
            this.toolStripSeparator6,
            this.toolStripButton1,
            this.toolStripButtonHelp,
            this.toolStripSeparator7,
            this.toolStripButtonClose});
            this.bN.Location = new System.Drawing.Point(0, 0);
            this.bN.MoveFirstItem = null;
            this.bN.MoveLastItem = null;
            this.bN.MoveNextItem = null;
            this.bN.MovePreviousItem = null;
            this.bN.Name = "bN";
            this.bN.PositionItem = this.bindingNavigatorPositionItem;
            this.bN.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bN.Size = new System.Drawing.Size(895, 30);
            this.bN.TabIndex = 139;
            this.bN.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(32, 27);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(30, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripSplitButtonGS
            // 
            this.toolStripSplitButtonGS.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.申请格式ToolStripMenuItem,
            this.项目模式ToolStripMenuItem});
            this.toolStripSplitButtonGS.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButtonGS.Image")));
            this.toolStripSplitButtonGS.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButtonGS.Name = "toolStripSplitButtonGS";
            this.toolStripSplitButtonGS.Size = new System.Drawing.Size(88, 27);
            this.toolStripSplitButtonGS.Text = "格式设置";
            // 
            // 申请格式ToolStripMenuItem
            // 
            this.申请格式ToolStripMenuItem.Name = "申请格式ToolStripMenuItem";
            this.申请格式ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.申请格式ToolStripMenuItem.Text = "申请格式";
            this.申请格式ToolStripMenuItem.Click += new System.EventHandler(this.申请格式ToolStripMenuItem_Click);
            // 
            // 项目模式ToolStripMenuItem
            // 
            this.项目模式ToolStripMenuItem.Name = "项目模式ToolStripMenuItem";
            this.项目模式ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.项目模式ToolStripMenuItem.Text = "项目模式";
            this.项目模式ToolStripMenuItem.Click += new System.EventHandler(this.项目模式ToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripSplitButtonDC
            // 
            this.toolStripSplitButtonDC.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.申请导出ToolStripMenuItem,
            this.项目导出ToolStripMenuItem});
            this.toolStripSplitButtonDC.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButtonDC.Image")));
            this.toolStripSplitButtonDC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButtonDC.Name = "toolStripSplitButtonDC";
            this.toolStripSplitButtonDC.Size = new System.Drawing.Size(93, 27);
            this.toolStripSplitButtonDC.Text = "导出Excel";
            // 
            // 申请导出ToolStripMenuItem
            // 
            this.申请导出ToolStripMenuItem.Name = "申请导出ToolStripMenuItem";
            this.申请导出ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.申请导出ToolStripMenuItem.Text = "申请导出";
            this.申请导出ToolStripMenuItem.Click += new System.EventHandler(this.申请导出ToolStripMenuItem_Click);
            // 
            // 项目导出ToolStripMenuItem
            // 
            this.项目导出ToolStripMenuItem.Name = "项目导出ToolStripMenuItem";
            this.项目导出ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.项目导出ToolStripMenuItem.Text = "项目导出";
            this.项目导出ToolStripMenuItem.Click += new System.EventHandler(this.项目导出ToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonQuery
            // 
            this.toolStripButtonQuery.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonQuery.Image")));
            this.toolStripButtonQuery.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonQuery.Name = "toolStripButtonQuery";
            this.toolStripButtonQuery.Size = new System.Drawing.Size(81, 27);
            this.toolStripButtonQuery.Text = "查询(F8)  ";
            this.toolStripButtonQuery.Click += new System.EventHandler(this.toolStripButtonQuery_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonHelp
            // 
            this.toolStripButtonHelp.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHelp.Image")));
            this.toolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonHelp.Name = "toolStripButtonHelp";
            this.toolStripButtonHelp.Size = new System.Drawing.Size(81, 27);
            this.toolStripButtonHelp.Text = "帮助(F1)  ";
            this.toolStripButtonHelp.Click += new System.EventHandler(this.toolStripButtonHelp_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonClose
            // 
            this.toolStripButtonClose.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonClose.Image")));
            this.toolStripButtonClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonClose.Name = "toolStripButtonClose";
            this.toolStripButtonClose.Size = new System.Drawing.Size(60, 27);
            this.toolStripButtonClose.Text = "关闭  ";
            this.toolStripButtonClose.Click += new System.EventHandler(this.toolStripButtonClose_Click);
            // 
            // bindingSourceApply
            // 
            this.bindingSourceApply.PositionChanged += new System.EventHandler(this.bindingSourceApply_PositionChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.wwTreeViewHZ);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 30);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(197, 386);
            this.panel4.TabIndex = 140;
            // 
            // wwTreeViewHZ
            // 
            this.wwTreeViewHZ.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wwTreeViewHZ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wwTreeViewHZ.ImageIndex = 0;
            this.wwTreeViewHZ.ImageList = this.imageListTree;
            this.wwTreeViewHZ.Location = new System.Drawing.Point(0, 38);
            this.wwTreeViewHZ.Name = "wwTreeViewHZ";
            this.wwTreeViewHZ.SelectedImageIndex = 1;
            this.wwTreeViewHZ.Size = new System.Drawing.Size(197, 348);
            this.wwTreeViewHZ.TabIndex = 133;
            this.wwTreeViewHZ.ZADataTable = null;
            this.wwTreeViewHZ.ZADisplayFieldName = "";
            this.wwTreeViewHZ.ZAKeyFieldName = "";
            this.wwTreeViewHZ.ZAParentFieldName = "";
            this.wwTreeViewHZ.ZAToolTipTextName = "";
            this.wwTreeViewHZ.ZATreeViewRootValue = "";
            this.wwTreeViewHZ.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.wwTreeViewHZ_AfterSelect);
            // 
            // imageListTree
            // 
            this.imageListTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTree.ImageStream")));
            this.imageListTree.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTree.Images.SetKeyName(0, "12.gif");
            this.imageListTree.Images.SetKeyName(1, "27.gif");
            // 
            // panel5
            // 
            this.panel5.AutoScroll = true;
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.fjy_dateDateTimePicker2);
            this.panel5.Controls.Add(this.fjy_dateDateTimePicker1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(197, 38);
            this.panel5.TabIndex = 134;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(94, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(11, 12);
            this.label11.TabIndex = 18;
            this.label11.Text = "-";
            // 
            // fjy_dateDateTimePicker2
            // 
            this.fjy_dateDateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker2.Location = new System.Drawing.Point(106, 12);
            this.fjy_dateDateTimePicker2.Name = "fjy_dateDateTimePicker2";
            this.fjy_dateDateTimePicker2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker2.Size = new System.Drawing.Size(85, 21);
            this.fjy_dateDateTimePicker2.TabIndex = 17;
            this.fjy_dateDateTimePicker2.CloseUp += new System.EventHandler(this.fjy_dateDateTimePicker2_CloseUp);
            // 
            // fjy_dateDateTimePicker1
            // 
            this.fjy_dateDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker1.Location = new System.Drawing.Point(6, 12);
            this.fjy_dateDateTimePicker1.Name = "fjy_dateDateTimePicker1";
            this.fjy_dateDateTimePicker1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker1.Size = new System.Drawing.Size(85, 21);
            this.fjy_dateDateTimePicker1.TabIndex = 15;
            this.fjy_dateDateTimePicker1.CloseUp += new System.EventHandler(this.fjy_dateDateTimePicker1_CloseUp);
            // 
            // contextMenuStripApplyQuery
            // 
            this.contextMenuStripApplyQuery.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_applygs,
            this.toolStripMenuItem_checkidtemgs,
            this.toolStripSeparator3,
            this.toolStripMenuItem2,
            this.toolStripMenuItem1,
            this.toolStripSeparator1,
            this.帮助ToolStripMenuItem,
            this.toolStripSeparator2,
            this.关闭ToolStripMenuItem});
            this.contextMenuStripApplyQuery.Name = "contextMenuStripReport";
            this.contextMenuStripApplyQuery.Size = new System.Drawing.Size(166, 154);
            // 
            // toolStripMenuItem_applygs
            // 
            this.toolStripMenuItem_applygs.Name = "toolStripMenuItem_applygs";
            this.toolStripMenuItem_applygs.Size = new System.Drawing.Size(165, 22);
            this.toolStripMenuItem_applygs.Text = "申请单格式";
            this.toolStripMenuItem_applygs.Click += new System.EventHandler(this.toolStripMenuItem_applygs_Click);
            // 
            // toolStripMenuItem_checkidtemgs
            // 
            this.toolStripMenuItem_checkidtemgs.Name = "toolStripMenuItem_checkidtemgs";
            this.toolStripMenuItem_checkidtemgs.Size = new System.Drawing.Size(165, 22);
            this.toolStripMenuItem_checkidtemgs.Text = "项目格式";
            this.toolStripMenuItem_checkidtemgs.Click += new System.EventHandler(this.toolStripMenuItem_checkidtemgs_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(162, 6);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(165, 22);
            this.toolStripMenuItem2.Text = "导出申请到Excel";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(165, 22);
            this.toolStripMenuItem1.Text = "导出项目到Excel";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(162, 6);
            // 
            // 帮助ToolStripMenuItem
            // 
            this.帮助ToolStripMenuItem.Name = "帮助ToolStripMenuItem";
            this.帮助ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.帮助ToolStripMenuItem.Text = "帮助";
            this.帮助ToolStripMenuItem.Click += new System.EventHandler(this.帮助ToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(162, 6);
            // 
            // 关闭ToolStripMenuItem
            // 
            this.关闭ToolStripMenuItem.Name = "关闭ToolStripMenuItem";
            this.关闭ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.关闭ToolStripMenuItem.Text = "关闭";
            this.关闭ToolStripMenuItem.Click += new System.EventHandler(this.关闭ToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridViewApply);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(197, 30);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(698, 87);
            this.groupBox1.TabIndex = 142;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "申请单";
            // 
            // dataGridViewApply
            // 
            this.dataGridViewApply.AllowUserToAddRows = false;
            this.dataGridViewApply.AllowUserToResizeRows = false;
            this.dataGridViewApply.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewApply.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewApply.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewApply.ColumnHeadersHeight = 21;
            this.dataGridViewApply.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fstate});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewApply.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewApply.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewApply.EnableHeadersVisualStyles = false;
            this.dataGridViewApply.Location = new System.Drawing.Point(3, 17);
            this.dataGridViewApply.MultiSelect = false;
            this.dataGridViewApply.Name = "dataGridViewApply";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewApply.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewApply.RowHeadersVisible = false;
            this.dataGridViewApply.RowHeadersWidth = 35;
            this.dataGridViewApply.RowTemplate.Height = 23;
            this.dataGridViewApply.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewApply.Size = new System.Drawing.Size(692, 67);
            this.dataGridViewApply.TabIndex = 137;
            this.dataGridViewApply.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewApply_DataError);
            // 
            // fstate
            // 
            this.fstate.DataPropertyName = "fstate";
            this.fstate.DataSource = this.sam_typeBindingSource_fstate;
            this.fstate.DisplayMember = "fname";
            this.fstate.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.fstate.HeaderText = "fstate";
            this.fstate.Name = "fstate";
            this.fstate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fstate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fstate.ValueMember = "fcode";
            // 
            // sam_typeBindingSource_fstate
            // 
            this.sam_typeBindingSource_fstate.DataMember = "sam_type";
            this.sam_typeBindingSource_fstate.DataSource = this.samDataSet;
            // 
            // samDataSet
            // 
            this.samDataSet.DataSetName = "samDataSet";
            this.samDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridViewItem);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox2.Location = new System.Drawing.Point(197, 117);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(698, 299);
            this.groupBox2.TabIndex = 143;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "项目";
            // 
            // dataGridViewItem
            // 
            this.dataGridViewItem.AllowUserToAddRows = false;
            this.dataGridViewItem.AllowUserToResizeRows = false;
            this.dataGridViewItem.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewItem.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewItem.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewItem.ColumnHeadersHeight = 21;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewItem.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewItem.EnableHeadersVisualStyles = false;
            this.dataGridViewItem.Location = new System.Drawing.Point(3, 17);
            this.dataGridViewItem.MultiSelect = false;
            this.dataGridViewItem.Name = "dataGridViewItem";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewItem.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewItem.RowHeadersVisible = false;
            this.dataGridViewItem.RowHeadersWidth = 35;
            this.dataGridViewItem.RowTemplate.Height = 23;
            this.dataGridViewItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewItem.Size = new System.Drawing.Size(692, 279);
            this.dataGridViewItem.TabIndex = 136;
            this.dataGridViewItem.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewItem_DataError);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::ww.form.Properties.Resources.button_print1;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(89, 27);
            this.toolStripButton1.Text = "打印(F3)    ";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // ApplyQueryBaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(895, 416);
            this.ContextMenuStrip = this.contextMenuStripApplyQuery;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.bN);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ApplyQueryBaseForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "申请查询基础窗口";
            this.Load += new System.EventHandler(this.ApplyQueryBaseForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bN)).EndInit();
            this.bN.ResumeLayout(false);
            this.bN.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceApply)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.contextMenuStripApplyQuery.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewApply)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sam_typeBindingSource_fstate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ImageList imageListTree;
        protected ww.wwf.control.WWControlLib.WWTreeView wwTreeViewHZ;
        protected System.Windows.Forms.BindingNavigator bN;
        protected System.Windows.Forms.ToolStripButton toolStripButtonQuery;
        protected System.Windows.Forms.ToolStripButton toolStripButtonHelp;
        protected System.Windows.Forms.Panel panel5;
        protected System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker2;
        protected System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker1;
        protected System.Windows.Forms.ContextMenuStrip contextMenuStripApplyQuery;
        protected System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_applygs;
        protected System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_checkidtemgs;
        protected System.Windows.Forms.ToolStripMenuItem 帮助ToolStripMenuItem;
        protected System.Windows.Forms.ToolStripMenuItem 关闭ToolStripMenuItem;
        private System.Windows.Forms.BindingSource sam_typeBindingSource_fstate;
        private ww.form.lis.sam.samDataSet samDataSet;
        private System.Windows.Forms.BindingSource bindingSourceApply;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButtonGS;
        private System.Windows.Forms.ToolStripMenuItem 申请格式ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 项目模式ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButtonDC;
        private System.Windows.Forms.ToolStripMenuItem 申请导出ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 项目导出ToolStripMenuItem;
        protected System.Windows.Forms.ToolStripButton toolStripButtonClose;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        protected System.Windows.Forms.GroupBox groupBox2;
        protected System.Windows.Forms.GroupBox groupBox1;
        protected System.Windows.Forms.DataGridView dataGridViewApply;
        protected System.Windows.Forms.DataGridView dataGridViewItem;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.DataGridViewComboBoxColumn fstate;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
    }
}