﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

using ww.wwf.wwfbll;
using ww.lis.lisbll.sam;
namespace ww.form.lis.com
{
    /// <summary>
    /// 报告基础窗口
    /// </summary>
    public partial class JyBaseForm : ww.form.wwf.SysBaseForm
    {
        #region        自定义变量
        /// <summary>
        /// 组织 人员
        /// </summary>
        protected PersonBLL bllPerson = new PersonBLL();
        /// <summary>
        ///  组织 部门
        /// </summary>
        protected DeptBll bllDept = new DeptBll();
        /// <summary>
        /// 逻辑_仪器
        /// </summary>
        protected InstrBLL bllInstr = new InstrBLL();
        /// <summary>
        /// 患者信息 规则
        /// </summary>
        protected PatientBLL bllPatient = new PatientBLL();
        /// <summary>
        /// 公共类型逻辑  
        /// </summary>
        protected TypeBLL bllType = new TypeBLL();
        /// <summary>
        /// 报告规则
        /// </summary>
        protected jybll bllReport = new jybll();
        /// <summary>
        /// 项目规则
        /// </summary>
        protected ItemBLL bllItem = new ItemBLL();
        /// <summary>
        /// 报告 申请+样本
        /// </summary>
        protected DataTable dtReport = new DataTable();
        /// <summary>
        /// 报告 结果
        /// </summary>
        protected DataTable dtReportResult = new DataTable();
        /// <summary>
        /// 报告 图
        /// </summary>
        protected DataTable dtReportImg = new DataTable();
       
        #endregion
        #region        自定义方法

        /// <summary>
        /// 公共类别 样本状态
        /// </summary>
        /// <returns></returns>
        protected DataTable WWComTypeSampleState()
        {
            DataTable dt = null;
            try
            {
                dt = this.bllType.BllComTypeDT("样本状态", 1,"");
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            return dt;
        }
        /// <summary>
        /// 项目单位 名称
        /// </summary>
        /// <returns></returns>
        protected DataTable WWComTypefitem_unit()
        {
            DataTable dt = null;
            try
            {
                dt = this.bllType.BllComTypeDT("常用单位", 1,"");
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            return dt;
        }
        /// <summary>
        /// 病人类别
        /// </summary>
        /// <returns></returns>
        protected DataTable WWComTypeftype_id()
        {
            DataTable dt = null;
            try
            {
                dt = this.bllType.BllComTypeDT("病人类别", 1,"");
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            return dt;
        }
        /// <summary>
        /// 性别
        /// </summary>
        /// <returns></returns>
        protected DataTable WWComTypefsex()
        {
            DataTable dt = null;
            try
            {
                dt = this.bllType.BllComTypeDT("性别", 1,"");
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            return dt;
        }
        /// <summary>
        /// 年龄单位
        /// </summary>
        /// <returns></returns>
        protected DataTable WWComTypefage_unit()
        {
            DataTable dt = null;
            try
            {
                dt = this.bllType.BllComTypeDT("年龄单位", 1,"");
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            return dt;
        }
        //病人类别
        /// <summary>
        /// 项目 结果值标记
        /// </summary>
        /// <returns></returns>
        protected DataTable WWComTypefitem_badge()
        {
            DataTable dt = null;
            try
            {
                dt = this.bllType.BllComTypeDT("结果标记", 1,"");
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            return dt;
        }
        /// <summary>
        /// 样本类别
        /// </summary>
        /// <returns></returns>
        protected DataTable WWSampleType()
        {
            DataTable dt = null;
            try
            {
                dt = this.bllType.BllSamTypeDT(1);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            return dt;
        }
        /// <summary>
        /// 检验类别
        /// </summary>
        /// <returns></returns>
        protected DataTable WWCheckType()
        {
            DataTable dt = null;
            try
            {
                dt = this.bllType.BllCheckTypeDT(1);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            return dt;
        }
        /// <summary>
        /// 申请人 医生
        /// </summary>
        /// <returns></returns>
        protected DataTable WWUserApply()
        {
            DataTable dt = null;
            try
            {
                dt = this.bllPerson.BllDTAllPersonByTypeAndUse(1, "医生");
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            return dt;
        }
        /// <summary>
        /// 检验科人 检验
        /// </summary>
        /// <returns></returns>
        protected DataTable WWUserCheck()
        {
            DataTable dt = null;
            try
            {
                dt = this.bllPerson.BllDTAllPersonByTypeAndUse(1, "检验");
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            return dt;
        }
        /// <summary>
        /// 申请部门 
        /// </summary>
        /// <returns></returns>
        protected DataTable WWApplyDept()
        {
            DataTable dt = null;
            try
            {
                dt = this.bllDept.BllDeptDTuse();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            return dt;
        }
        /// <summary>
        /// 仪器列表
        /// </summary>
        /// <returns></returns>
        protected DataTable WWInstr()
        {
            DataTable dt = null;
            try
            {
                dt = this.bllInstr.BllInstrDTByUseAndGroupID(1, LoginBLL.strDeptID);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            return dt;

        }

        /// <summary>
        /// 设置 报告列表
        /// </summary>
        /// <param name="strWhere"></param>
        protected void WWSetdtReport(string strWhere)
        {
            this.dtReport = this.bllReport.GetList(strWhere);
        }
        /// <summary>
        /// 设置 报告结果
        /// </summary>
        /// <param name="strfapply_id"></param>
        protected void WWSetdtReportResult(string strfapply_id)
        {
            this.dtReportResult = this.bllReport.GetListS(" t.fjy_id='" + strfapply_id + "'");
            
        }
        /// <summary>
        /// 设置 报告图
        /// </summary>
        /// <param name="strfapply_id"></param>
        protected void WWSetdtReportImg(string strfapply_id, string strfinstr_result_id)
        {
            this.dtReportImg = this.bllReport.BllImgDT(strfapply_id, strfinstr_result_id);
        }
        /// <summary>
        /// 打开帮助
        /// </summary>
        /// <param name="strHelpID"></param>
        protected void WWOpenHelp(string strHelpID)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                HelpForm help = new HelpForm(strHelpID);
                help.Show();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        #endregion


        public JyBaseForm()
        {
            InitializeComponent();
        }

        private void ReportBaseForm_Load(object sender, EventArgs e)
        {

        }

        
    }
}