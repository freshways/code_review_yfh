﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;
using ww.lis.lisbll.sam;
namespace ww.form.lis.com
{
    public partial class HZDiseaseForm : Form
    {
        public string strid = "";
        public string strname = "";
        DiseaseBLL bll = new DiseaseBLL();
      
        public HZDiseaseForm( )
        {
            InitializeComponent();
            this.dataGridView1.AutoGenerateColumns = false;
           
        }     
          
       
        private void ApplyUserForm_Load(object sender, EventArgs e)
        {
            
            GetDT();
        }

        private void GetDT()
        {
            try
            {
               
                DataTable dt = this.bll.BllDT(" WHERE  (fcode LIKE '" + this.textBox1.Text + "%') ");
                this.bindingSource1.DataSource = dt ;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
             
            GetDT();
        }

        

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Space))//或(e.KeyCode==Keys.Enter) 
            {
                this.bindingSource1.PositionChanged -= new EventHandler(bindingSource1_PositionChanged);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
        }

        private void bindingNavigatorOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButtonNo_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.No;
            //this.Close();
        }

        private void bindingSource1_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                 
                DataRowView rowCurrent = (DataRowView)bindingSource1.Current;
                if (rowCurrent != null && rowCurrent[0].ToString()!="")
                {
                    strid = rowCurrent["fcode"].ToString();
                    strname = rowCurrent["fname"].ToString();
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

    }
}

/*
 Name	Code	Data Type	Primary	Foreign Key	Mandatory
检验_id	fjy_id	varchar(32)	TRUE	FALSE	TRUE
检验日期	fjy_date	varchar(32)	FALSE	FALSE	FALSE
仪器id	fjy_instr	varchar(32)	FALSE	FALSE	FALSE
检验样本条码号	fjy_yb_tm	varchar(32)	FALSE	FALSE	FALSE
检验样本号	fjy_yb_code	varchar(32)	FALSE	FALSE	FALSE
检验样本类型id	fjy_yb_type	varchar(32)	FALSE	FALSE	FALSE
检验费别id	fjy_sf_type	varchar(32)	FALSE	FALSE	FALSE
检验费	fjy_f	float	FALSE	FALSE	FALSE
检验收费否	fjy_f_ok	varchar(32)	FALSE	FALSE	FALSE
检验状态	fjy_zt	varchar(32)	FALSE	FALSE	FALSE
检验目的	fjy_md	varchar(200)	FALSE	FALSE	FALSE
检验临床诊断	fjy_lczd	varchar(200)	FALSE	FALSE	FALSE
患者id	fhz_id	varchar(32)	FALSE	FALSE	FALSE
患者类型_id	fhz_type_id	varchar(32)	FALSE	FALSE	FALSE
患者住院号	fhz_zyh	varchar(32)	FALSE	FALSE	FALSE
患者姓名	fhz_name	varchar(32)	FALSE	FALSE	FALSE
患者性别	fhz_sex	varchar(32)	FALSE	FALSE	FALSE
患者年龄	fhz_age	float	FALSE	FALSE	FALSE
患者年龄单位	fhz_age_unit	varchar(32)	FALSE	FALSE	FALSE
患者生日	fhz_age_date	varchar(32)	FALSE	FALSE	FALSE
患者病区	fhz_bq	varchar(32)	FALSE	FALSE	FALSE
患者房间号	fhz_room	varchar(32)	FALSE	FALSE	FALSE
患者床号	fhz_bed	varchar(32)	FALSE	FALSE	FALSE
患者民族	fhz_volk	varchar(32)	FALSE	FALSE	FALSE
患者婚姻	fhz_hy	varchar(32)	FALSE	FALSE	FALSE
患者职业	fhz_job	varchar(32)	FALSE	FALSE	FALSE
患者过敏史	fhz_gms	varchar(100)	FALSE	FALSE	FALSE
患者住址	fhz_add	varchar(200)	FALSE	FALSE	FALSE
患者电话	fhz_tel	varchar(64)	FALSE	FALSE	FALSE
申请单id	fapply_id	varchar(32)	FALSE	FALSE	FALSE
申请人/送检医师	fapply_user_id	varchar(32)	FALSE	FALSE	FALSE
申请科室_id/病人科室	fapply_dept_id	varchar(32)	FALSE	FALSE	FALSE
申请时间/送检时间	fapply_time	varchar(32)	FALSE	FALSE	FALSE
采样人_id	fsampling_user_id	varchar(32)	FALSE	FALSE	FALSE
采样时间	fsampling_time	varchar(32)	FALSE	FALSE	FALSE
检验人_id	fjy_user_id	varchar(32)	FALSE	FALSE	FALSE
审核医师_id	fchenk_user_id	varchar(32)	FALSE	FALSE	FALSE
审核时间	fcheck_time	varchar(32)	FALSE	FALSE	FALSE
报告时间	freport_time	varchar(32)	FALSE	FALSE	FALSE
打印时间	fprint_time	varchar(32)	FALSE	FALSE	FALSE
打印状态	fprint_zt	varchar(32)	FALSE	FALSE	FALSE
打印次数	fprint_count	int	FALSE	FALSE	FALSE
系统用户	fsys_user	varchar(32)	FALSE	FALSE	FALSE
系统时间	fsys_time	varchar(32)	FALSE	FALSE	FALSE
系统部门	fsys_dept	varchar(32)	FALSE	FALSE	FALSE
备注	fremark	varchar(200)	FALSE	FALSE	FALSE
 */