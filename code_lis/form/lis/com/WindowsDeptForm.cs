﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;

namespace ww.form.lis.com
{
    public partial class WindowsDeptForm : Form
    {
       
       
        DeptBll bllDept = new DeptBll();
        DataTable dtDept = new DataTable();   
      

        string strDeptID = "";//部门ＩＤ
        public WindowsDeptForm()
        {
            InitializeComponent();
            
            
        }
        private WindowsResult r;
        public WindowsDeptForm(WindowsResult r)
            : this()
        {
            this.r = r;

        }

      

        /// <summary>
        /// 列出系统部门树
        /// </summary>
        public void GetTree(string pid)
        {
            try
            {
                this.wwTreeView1.ZADataTable = dtDept;
                this.wwTreeView1.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeView1.ZAParentFieldName = "fp_id";//上级字段名称
                this.wwTreeView1.ZADisplayFieldName = "fname_show";//显示字段名称
                this.wwTreeView1.ZAKeyFieldName = "fdept_id";//主键字段名称
                this.wwTreeView1.ZATreeViewShow();//显示树        
                try
                {
                    this.wwTreeView1.SelectedNode = wwTreeView1.Nodes[0].Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }
        private void ApplyUserForm_Load(object sender, EventArgs e)
        {
            try
            {
                dtDept = this.bllDept.BllDeptDTuse();
                bindingSource1.DataSource = dtDept;
                GetTree("-1");            
              
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void wwTreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                this.strDeptID = e.Node.Name.ToString();
               // r.ChangeTextString(strDeptID);
               // this.Close();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void wwTreeView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                r.ChangeTextString(this.strDeptID);
                this.Close();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonNo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bindingNavigatorOK_Click(object sender, EventArgs e)
        {
            try
            {
                r.ChangeTextString(this.strDeptID);
                this.Close();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void wwTreeView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    r.ChangeTextString(this.strDeptID);
                    this.Close();
                }
                catch (Exception ex)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
                }
            }   
        }

    }
}