﻿namespace ww.form.lis.com
{
    partial class InstrDrvieForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label fportLabel;
            System.Windows.Forms.Label fbaud_rateLabel;
            System.Windows.Forms.Label fdata_bitLabel;
            System.Windows.Forms.Label fstop_bitLabel;
            System.Windows.Forms.Label finfo_modeLabel;
            System.Windows.Forms.Label freadbufferSizeLabel;
            System.Windows.Forms.Label fwritebufferSizeLabel;
            System.Windows.Forms.Label freadtimeoutLabel;
            System.Windows.Forms.Label fwritetimeoutLabel;
            System.Windows.Forms.Label fremarkLabel;
            System.Windows.Forms.Label fparityLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InstrDrvieForm));
            this.fportComboBox = new System.Windows.Forms.ComboBox();
            this.sAM_INSTR_DRIVEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.samDataSet = new ww.form.lis.sam.samDataSet();
            this.fbaud_rateComboBox = new System.Windows.Forms.ComboBox();
            this.fdata_bitComboBox = new System.Windows.Forms.ComboBox();
            this.fstop_bitComboBox = new System.Windows.Forms.ComboBox();
            this.finfo_modeComboBox = new System.Windows.Forms.ComboBox();
            this.freadbufferSizeComboBox = new System.Windows.Forms.ComboBox();
            this.fwritebufferSizeComboBox = new System.Windows.Forms.ComboBox();
            this.freadtimeoutComboBox = new System.Windows.Forms.ComboBox();
            this.fwritetimeoutComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.fparityComboBox = new System.Windows.Forms.ComboBox();
            this.fremarkTextBox = new System.Windows.Forms.TextBox();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorOK = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonNo = new System.Windows.Forms.ToolStripButton();
            fportLabel = new System.Windows.Forms.Label();
            fbaud_rateLabel = new System.Windows.Forms.Label();
            fdata_bitLabel = new System.Windows.Forms.Label();
            fstop_bitLabel = new System.Windows.Forms.Label();
            finfo_modeLabel = new System.Windows.Forms.Label();
            freadbufferSizeLabel = new System.Windows.Forms.Label();
            fwritebufferSizeLabel = new System.Windows.Forms.Label();
            freadtimeoutLabel = new System.Windows.Forms.Label();
            fwritetimeoutLabel = new System.Windows.Forms.Label();
            fremarkLabel = new System.Windows.Forms.Label();
            fparityLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.sAM_INSTR_DRIVEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            this.SuspendLayout();
            // 
            // fportLabel
            // 
            fportLabel.AutoSize = true;
            fportLabel.Location = new System.Drawing.Point(79, 24);
            fportLabel.Name = "fportLabel";
            fportLabel.Size = new System.Drawing.Size(35, 12);
            fportLabel.TabIndex = 0;
            fportLabel.Text = "端口:";
            // 
            // fbaud_rateLabel
            // 
            fbaud_rateLabel.AutoSize = true;
            fbaud_rateLabel.Location = new System.Drawing.Point(67, 76);
            fbaud_rateLabel.Name = "fbaud_rateLabel";
            fbaud_rateLabel.Size = new System.Drawing.Size(47, 12);
            fbaud_rateLabel.TabIndex = 2;
            fbaud_rateLabel.Text = "波特率:";
            // 
            // fdata_bitLabel
            // 
            fdata_bitLabel.AutoSize = true;
            fdata_bitLabel.Location = new System.Drawing.Point(67, 102);
            fdata_bitLabel.Name = "fdata_bitLabel";
            fdata_bitLabel.Size = new System.Drawing.Size(47, 12);
            fdata_bitLabel.TabIndex = 4;
            fdata_bitLabel.Text = "数据位:";
            // 
            // fstop_bitLabel
            // 
            fstop_bitLabel.AutoSize = true;
            fstop_bitLabel.Location = new System.Drawing.Point(67, 128);
            fstop_bitLabel.Name = "fstop_bitLabel";
            fstop_bitLabel.Size = new System.Drawing.Size(47, 12);
            fstop_bitLabel.TabIndex = 6;
            fstop_bitLabel.Text = "停止位:";
            // 
            // finfo_modeLabel
            // 
            finfo_modeLabel.AutoSize = true;
            finfo_modeLabel.Location = new System.Drawing.Point(55, 154);
            finfo_modeLabel.Name = "finfo_modeLabel";
            finfo_modeLabel.Size = new System.Drawing.Size(59, 12);
            finfo_modeLabel.TabIndex = 8;
            finfo_modeLabel.Text = "通信模式:";
            // 
            // freadbufferSizeLabel
            // 
            freadbufferSizeLabel.AutoSize = true;
            freadbufferSizeLabel.Location = new System.Drawing.Point(19, 180);
            freadbufferSizeLabel.Name = "freadbufferSizeLabel";
            freadbufferSizeLabel.Size = new System.Drawing.Size(95, 12);
            freadbufferSizeLabel.TabIndex = 10;
            freadbufferSizeLabel.Text = "输入缓冲区大小:";
            // 
            // fwritebufferSizeLabel
            // 
            fwritebufferSizeLabel.AutoSize = true;
            fwritebufferSizeLabel.Location = new System.Drawing.Point(19, 206);
            fwritebufferSizeLabel.Name = "fwritebufferSizeLabel";
            fwritebufferSizeLabel.Size = new System.Drawing.Size(95, 12);
            fwritebufferSizeLabel.TabIndex = 12;
            fwritebufferSizeLabel.Text = "输出缓冲区大小:";
            // 
            // freadtimeoutLabel
            // 
            freadtimeoutLabel.AutoSize = true;
            freadtimeoutLabel.Location = new System.Drawing.Point(67, 232);
            freadtimeoutLabel.Name = "freadtimeoutLabel";
            freadtimeoutLabel.Size = new System.Drawing.Size(47, 12);
            freadtimeoutLabel.TabIndex = 14;
            freadtimeoutLabel.Text = "读超时:";
            // 
            // fwritetimeoutLabel
            // 
            fwritetimeoutLabel.AutoSize = true;
            fwritetimeoutLabel.Location = new System.Drawing.Point(67, 258);
            fwritetimeoutLabel.Name = "fwritetimeoutLabel";
            fwritetimeoutLabel.Size = new System.Drawing.Size(47, 12);
            fwritetimeoutLabel.TabIndex = 16;
            fwritetimeoutLabel.Text = "写超时:";
            // 
            // fremarkLabel
            // 
            fremarkLabel.AutoSize = true;
            fremarkLabel.Location = new System.Drawing.Point(79, 284);
            fremarkLabel.Name = "fremarkLabel";
            fremarkLabel.Size = new System.Drawing.Size(35, 12);
            fremarkLabel.TabIndex = 18;
            fremarkLabel.Text = "备注:";
            // 
            // fparityLabel
            // 
            fparityLabel.AutoSize = true;
            fparityLabel.Location = new System.Drawing.Point(67, 50);
            fparityLabel.Name = "fparityLabel";
            fparityLabel.Size = new System.Drawing.Size(47, 12);
            fparityLabel.TabIndex = 22;
            fparityLabel.Text = "样验位:";
            // 
            // fportComboBox
            // 
            this.fportComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sAM_INSTR_DRIVEBindingSource, "fport", true));
            this.fportComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.sAM_INSTR_DRIVEBindingSource, "fport", true));
            this.fportComboBox.FormattingEnabled = true;
            this.fportComboBox.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8"});
            this.fportComboBox.Location = new System.Drawing.Point(126, 20);
            this.fportComboBox.Name = "fportComboBox";
            this.fportComboBox.Size = new System.Drawing.Size(147, 20);
            this.fportComboBox.TabIndex = 1;
            // 
            // sAM_INSTR_DRIVEBindingSource
            // 
            this.sAM_INSTR_DRIVEBindingSource.DataMember = "SAM_INSTR_DRIVE";
            this.sAM_INSTR_DRIVEBindingSource.DataSource = this.samDataSet;
            // 
            // samDataSet
            // 
            this.samDataSet.DataSetName = "samDataSet";
            this.samDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // fbaud_rateComboBox
            // 
            this.fbaud_rateComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sAM_INSTR_DRIVEBindingSource, "fbaud_rate", true));
            this.fbaud_rateComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.sAM_INSTR_DRIVEBindingSource, "fbaud_rate", true));
            this.fbaud_rateComboBox.FormattingEnabled = true;
            this.fbaud_rateComboBox.Items.AddRange(new object[] {
            "300",
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "14400",
            "28800",
            "36000",
            "115000"});
            this.fbaud_rateComboBox.Location = new System.Drawing.Point(126, 72);
            this.fbaud_rateComboBox.Name = "fbaud_rateComboBox";
            this.fbaud_rateComboBox.Size = new System.Drawing.Size(147, 20);
            this.fbaud_rateComboBox.TabIndex = 3;
            // 
            // fdata_bitComboBox
            // 
            this.fdata_bitComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sAM_INSTR_DRIVEBindingSource, "fdata_bit", true));
            this.fdata_bitComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.sAM_INSTR_DRIVEBindingSource, "fdata_bit", true));
            this.fdata_bitComboBox.FormattingEnabled = true;
            this.fdata_bitComboBox.Items.AddRange(new object[] {
            "7",
            "8",
            "9"});
            this.fdata_bitComboBox.Location = new System.Drawing.Point(126, 98);
            this.fdata_bitComboBox.Name = "fdata_bitComboBox";
            this.fdata_bitComboBox.Size = new System.Drawing.Size(147, 20);
            this.fdata_bitComboBox.TabIndex = 5;
            // 
            // fstop_bitComboBox
            // 
            this.fstop_bitComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sAM_INSTR_DRIVEBindingSource, "fstop_bit", true));
            this.fstop_bitComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.sAM_INSTR_DRIVEBindingSource, "fstop_bit", true));
            this.fstop_bitComboBox.FormattingEnabled = true;
            this.fstop_bitComboBox.Items.AddRange(new object[] {
            "None",
            "One",
            "Two",
            "OnePointFive"});
            this.fstop_bitComboBox.Location = new System.Drawing.Point(126, 124);
            this.fstop_bitComboBox.Name = "fstop_bitComboBox";
            this.fstop_bitComboBox.Size = new System.Drawing.Size(147, 20);
            this.fstop_bitComboBox.TabIndex = 7;
            // 
            // finfo_modeComboBox
            // 
            this.finfo_modeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sAM_INSTR_DRIVEBindingSource, "finfo_mode", true));
            this.finfo_modeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.sAM_INSTR_DRIVEBindingSource, "finfo_mode", true));
            this.finfo_modeComboBox.FormattingEnabled = true;
            this.finfo_modeComboBox.Items.AddRange(new object[] {
            "Text",
            "Hex"});
            this.finfo_modeComboBox.Location = new System.Drawing.Point(126, 150);
            this.finfo_modeComboBox.Name = "finfo_modeComboBox";
            this.finfo_modeComboBox.Size = new System.Drawing.Size(147, 20);
            this.finfo_modeComboBox.TabIndex = 9;
            // 
            // freadbufferSizeComboBox
            // 
            this.freadbufferSizeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sAM_INSTR_DRIVEBindingSource, "freadbufferSize", true));
            this.freadbufferSizeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.sAM_INSTR_DRIVEBindingSource, "freadbufferSize", true));
            this.freadbufferSizeComboBox.FormattingEnabled = true;
            this.freadbufferSizeComboBox.Items.AddRange(new object[] {
            "512",
            "1024",
            "2048",
            "4096",
            "8192"});
            this.freadbufferSizeComboBox.Location = new System.Drawing.Point(126, 176);
            this.freadbufferSizeComboBox.Name = "freadbufferSizeComboBox";
            this.freadbufferSizeComboBox.Size = new System.Drawing.Size(147, 20);
            this.freadbufferSizeComboBox.TabIndex = 11;
            // 
            // fwritebufferSizeComboBox
            // 
            this.fwritebufferSizeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sAM_INSTR_DRIVEBindingSource, "fwritebufferSize", true));
            this.fwritebufferSizeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.sAM_INSTR_DRIVEBindingSource, "fwritebufferSize", true));
            this.fwritebufferSizeComboBox.FormattingEnabled = true;
            this.fwritebufferSizeComboBox.Items.AddRange(new object[] {
            "512",
            "1024",
            "2048",
            "4096",
            "8192"});
            this.fwritebufferSizeComboBox.Location = new System.Drawing.Point(126, 202);
            this.fwritebufferSizeComboBox.Name = "fwritebufferSizeComboBox";
            this.fwritebufferSizeComboBox.Size = new System.Drawing.Size(147, 20);
            this.fwritebufferSizeComboBox.TabIndex = 13;
            // 
            // freadtimeoutComboBox
            // 
            this.freadtimeoutComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sAM_INSTR_DRIVEBindingSource, "freadtimeout", true));
            this.freadtimeoutComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.sAM_INSTR_DRIVEBindingSource, "freadtimeout", true));
            this.freadtimeoutComboBox.FormattingEnabled = true;
            this.freadtimeoutComboBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.freadtimeoutComboBox.Location = new System.Drawing.Point(126, 228);
            this.freadtimeoutComboBox.Name = "freadtimeoutComboBox";
            this.freadtimeoutComboBox.Size = new System.Drawing.Size(147, 20);
            this.freadtimeoutComboBox.TabIndex = 15;
            // 
            // fwritetimeoutComboBox
            // 
            this.fwritetimeoutComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sAM_INSTR_DRIVEBindingSource, "fwritetimeout", true));
            this.fwritetimeoutComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.sAM_INSTR_DRIVEBindingSource, "fwritetimeout", true));
            this.fwritetimeoutComboBox.FormattingEnabled = true;
            this.fwritetimeoutComboBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.fwritetimeoutComboBox.Location = new System.Drawing.Point(126, 254);
            this.fwritetimeoutComboBox.Name = "fwritetimeoutComboBox";
            this.fwritetimeoutComboBox.Size = new System.Drawing.Size(147, 20);
            this.fwritetimeoutComboBox.TabIndex = 17;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(fparityLabel);
            this.groupBox1.Controls.Add(this.fparityComboBox);
            this.groupBox1.Controls.Add(fremarkLabel);
            this.groupBox1.Controls.Add(this.fremarkTextBox);
            this.groupBox1.Controls.Add(fwritetimeoutLabel);
            this.groupBox1.Controls.Add(this.fwritetimeoutComboBox);
            this.groupBox1.Controls.Add(freadtimeoutLabel);
            this.groupBox1.Controls.Add(this.freadtimeoutComboBox);
            this.groupBox1.Controls.Add(fwritebufferSizeLabel);
            this.groupBox1.Controls.Add(this.fwritebufferSizeComboBox);
            this.groupBox1.Controls.Add(freadbufferSizeLabel);
            this.groupBox1.Controls.Add(this.freadbufferSizeComboBox);
            this.groupBox1.Controls.Add(finfo_modeLabel);
            this.groupBox1.Controls.Add(this.finfo_modeComboBox);
            this.groupBox1.Controls.Add(fstop_bitLabel);
            this.groupBox1.Controls.Add(this.fstop_bitComboBox);
            this.groupBox1.Controls.Add(fdata_bitLabel);
            this.groupBox1.Controls.Add(this.fdata_bitComboBox);
            this.groupBox1.Controls.Add(fbaud_rateLabel);
            this.groupBox1.Controls.Add(this.fbaud_rateComboBox);
            this.groupBox1.Controls.Add(fportLabel);
            this.groupBox1.Controls.Add(this.fportComboBox);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(342, 331);
            this.groupBox1.TabIndex = 126;
            this.groupBox1.TabStop = false;
            // 
            // fparityComboBox
            // 
            this.fparityComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sAM_INSTR_DRIVEBindingSource, "fparity", true));
            this.fparityComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.sAM_INSTR_DRIVEBindingSource, "fparity", true));
            this.fparityComboBox.FormattingEnabled = true;
            this.fparityComboBox.Items.AddRange(new object[] {
            "None",
            "Even",
            "Mark",
            "Space"});
            this.fparityComboBox.Location = new System.Drawing.Point(126, 46);
            this.fparityComboBox.Name = "fparityComboBox";
            this.fparityComboBox.Size = new System.Drawing.Size(147, 20);
            this.fparityComboBox.TabIndex = 23;
            // 
            // fremarkTextBox
            // 
            this.fremarkTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sAM_INSTR_DRIVEBindingSource, "fremark", true));
            this.fremarkTextBox.Location = new System.Drawing.Point(126, 280);
            this.fremarkTextBox.Multiline = true;
            this.fremarkTextBox.Name = "fremarkTextBox";
            this.fremarkTextBox.Size = new System.Drawing.Size(147, 48);
            this.fremarkTextBox.TabIndex = 19;
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = this.bindingNavigatorOK;
            this.bindingNavigator1.AutoSize = false;
            this.bindingNavigator1.BindingSource = this.sAM_INSTR_DRIVEBindingSource;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator1.DeleteItem = null;
            this.bindingNavigator1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bindingNavigator1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorOK,
            this.toolStripButtonNo});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 331);
            this.bindingNavigator1.MoveFirstItem = null;
            this.bindingNavigator1.MoveLastItem = null;
            this.bindingNavigator1.MoveNextItem = null;
            this.bindingNavigator1.MovePreviousItem = null;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bindingNavigator1.Size = new System.Drawing.Size(342, 35);
            this.bindingNavigator1.TabIndex = 129;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // bindingNavigatorOK
            // 
            this.bindingNavigatorOK.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorOK.Image")));
            this.bindingNavigatorOK.Name = "bindingNavigatorOK";
            this.bindingNavigatorOK.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.bindingNavigatorOK.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorOK.Size = new System.Drawing.Size(85, 32);
            this.bindingNavigatorOK.Text = "确 定(&O)  ";
            this.bindingNavigatorOK.Click += new System.EventHandler(this.bindingNavigatorOK_Click);
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 32);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 35);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 35);
            // 
            // toolStripButtonNo
            // 
            this.toolStripButtonNo.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonNo.Image")));
            this.toolStripButtonNo.Name = "toolStripButtonNo";
            this.toolStripButtonNo.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonNo.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonNo.Size = new System.Drawing.Size(85, 32);
            this.toolStripButtonNo.Text = "取 消(&C)  ";
            this.toolStripButtonNo.Click += new System.EventHandler(this.toolStripButtonNo_Click);
            // 
            // InstrDrvieForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 366);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.bindingNavigator1);
            this.Name = "InstrDrvieForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "通信参数设置";
            this.Load += new System.EventHandler(this.InstrDrvieForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sAM_INSTR_DRIVEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox fportComboBox;
        private System.Windows.Forms.ComboBox fbaud_rateComboBox;
        private System.Windows.Forms.ComboBox fdata_bitComboBox;
        private System.Windows.Forms.ComboBox fstop_bitComboBox;
        private System.Windows.Forms.ComboBox finfo_modeComboBox;
        private System.Windows.Forms.ComboBox freadbufferSizeComboBox;
        private System.Windows.Forms.ComboBox fwritebufferSizeComboBox;
        private System.Windows.Forms.ComboBox freadtimeoutComboBox;
        private System.Windows.Forms.ComboBox fwritetimeoutComboBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.BindingSource sAM_INSTR_DRIVEBindingSource;
        private ww.form.lis.sam.samDataSet samDataSet;
        private System.Windows.Forms.TextBox fremarkTextBox;
        private System.Windows.Forms.ComboBox fparityComboBox;
        protected System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorOK;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonNo;
    }
}