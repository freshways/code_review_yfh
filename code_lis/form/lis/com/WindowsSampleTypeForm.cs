﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ww.lis.lisbll.sam;
namespace ww.form.lis.com
{
    public partial class WindowsSampleTypeForm : Form
    {
        public string strid = "";
        public string strname = "";
        SampleTypeBLL bll = new SampleTypeBLL();
        DataTable dtCurrent = new DataTable();
       
        public WindowsSampleTypeForm()
        {
            InitializeComponent();
            this.dataGridView1.AutoGenerateColumns = false;
            
        }
        private WindowsResult r;
        public WindowsSampleTypeForm(WindowsResult r)
            : this()
        {
            this.r = r;

        }
       
        private void ApplyUserForm_Load(object sender, EventArgs e)
        {
            GetDT();
        }

        private void GetDT()
        {
            try
            {
                this.dtCurrent = this.bll.BllDT(" WHERE (fuse_if = 1) AND (fhelp_code LIKE '" + this.textBox1.Text + "%') ");

                this.bindingSource1.DataSource = dtCurrent;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            GetDT();
        }

        

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Space))//或(e.KeyCode==Keys.Enter) 
            {
                this.bindingSource1.PositionChanged -= new EventHandler(bindingSource1_PositionChanged);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void bindingNavigatorOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButtonNo_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.No;
            //this.Close();
        }

        private void bindingSource1_PositionChanged(object sender, EventArgs e)
        {
            
                DataRowView rowCurrent = (DataRowView)bindingSource1.Current;
                if (rowCurrent != null && rowCurrent[0].ToString() != "")
                {
                    strid = rowCurrent["fsample_type_id"].ToString();
                    strname = rowCurrent["fname"].ToString();
                }
           
        }

      
    }
}