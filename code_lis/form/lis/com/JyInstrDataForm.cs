﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;
using ww.lis.lisbll.sam;
using System.Collections;
using System.IO;

namespace ww.form.lis.com
{
    public partial class JyInstrDataForm : Form
    { /// <summary>
        /// 逻辑_仪器
        /// </summary>
    
        private JYInstrDataBll bllInstrIO = new JYInstrDataBll();
        
     
        private DataTable dtResult = new DataTable();
       public IList selItemList = null;
        ImgBLL img = new ImgBLL();
        DataTable dtIMG = new DataTable();
        DataTable dtIMG_1 = new DataTable();//返回的图列表

        public string str仪器名称 = "";
        public string str仪器id = "";
        public string str样本号 = "";
        public string str检验日期 = "";
        DataTable dt结果主表 = new DataTable();
        public int int选择结果否 = 0;
        public string str仪器结果FTaskID = "";//仪器结果主表id

        public JyInstrDataForm()
        {
            InitializeComponent();
            this.dataGridViewReport.AutoGenerateColumns = false;
            this.dataGridViewResult.AutoGenerateColumns = false;


            DataColumn D0 = new DataColumn("FImgID", typeof(System.String));
            dtIMG.Columns.Add(D0);

            DataColumn D1 = new DataColumn("结果图1", typeof(System.Byte[]));           
            dtIMG.Columns.Add(D1);

            DataColumn D2 = new DataColumn("结果图2", typeof(System.Byte[]));
            dtIMG.Columns.Add(D2);

            DataColumn D3 = new DataColumn("结果图3", typeof(System.Byte[]));
            dtIMG.Columns.Add(D3);

            DataColumn D4 = new DataColumn("结果图4", typeof(System.Byte[]));
            dtIMG.Columns.Add(D4);

            DataColumn D5 = new DataColumn("结果图5", typeof(System.Byte[]));
            dtIMG.Columns.Add(D5);

            DataColumn strimg_1 = new DataColumn("strimg_1", typeof(System.String));
            dtIMG.Columns.Add(strimg_1);

            DataColumn strimg_2 = new DataColumn("strimg_2", typeof(System.String));
            dtIMG.Columns.Add(strimg_2);

            DataColumn strimg_3 = new DataColumn("strimg_3", typeof(System.String));
            dtIMG.Columns.Add(strimg_3);

            DataColumn strimg_4 = new DataColumn("strimg_4", typeof(System.String));
            dtIMG.Columns.Add(strimg_4);

            DataColumn strimg_5 = new DataColumn("strimg_5", typeof(System.String));
            dtIMG.Columns.Add(strimg_5);
        }
       
        private void InstrDataForm_Load(object sender, EventArgs e)
        {
            try
            {
                //changed by wjz 修改日期的传入格式 ▽
                //this.fjy_dateDateTimePicker1.Text = this.str检验日期;
                //this.fjy_dateDateTimePicker2.Text = this.str检验日期;
                try
                {
                    this.fjy_dateDateTimePicker1.Value = Convert.ToDateTime(this.str检验日期);
                    this.fjy_dateDateTimePicker2.Value = Convert.ToDateTime(this.str检验日期);
                }
                catch
                {
                    this.fjy_dateDateTimePicker1.Value = DateTime.Now;
                    this.fjy_dateDateTimePicker2.Value = DateTime.Now;
                }
                //changed by wjz 修改日期的传入格式 △

                this.fsample_codeTextBox.Text = this.str样本号;
                //this.toolStripButton1.Text = "取当前样本[" + this.str样本号 + "](&A) ";
                this.Text = this.str仪器名称 + " - " + this.Text;
                WWQuery();
                if (int选择结果否 == 0)
                {
                    bll样本结果();
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void WWImg(string strid)
        {
            try
            {
                if (this.dtIMG.Rows.Count > 0)
                    this.dtIMG.Clear();
                if (this.dtIMG_1.Rows.Count > 0)
                    this.dtIMG_1.Clear();
                dtIMG_1 = this.bllInstrIO.BllImgDT(strid);              
                DataRow drimg = dtIMG.NewRow();               
                int intImgCount = dtIMG_1.Rows.Count;
                if (intImgCount > 0)
                {
                    groupBoxImg.Visible = true;
                    drimg["FImgID"] = dtIMG_1.Rows[0]["FTaskID"];
                    for (int iimg = 0; iimg < intImgCount; iimg++)
                    {
                        switch (iimg)
                        {
                            case 0:
                                img1.Visible = true;
                                img2.Visible = false;
                                img3.Visible = false;
                                img4.Visible = false;
                                img5.Visible = false;
                                drimg["结果图1"] = dtIMG_1.Rows[iimg]["FImg"];

                                l_img_1.Visible = true;
                                l_img_1.Text = dtIMG_1.Rows[iimg]["FImgNmae"].ToString(); ;
                                break;
                            case 1:
                                img2.Visible = true;
                                img3.Visible = false;
                                img4.Visible = false;
                                img5.Visible = false;
                                drimg["结果图2"] = dtIMG_1.Rows[iimg]["FImg"];

                                l_img_2.Visible = true;
                                l_img_2.Text = dtIMG_1.Rows[iimg]["FImgNmae"].ToString(); ;
                                break;
                            case 2:
                                img3.Visible = true;
                                img4.Visible = false;
                                img5.Visible = false;
                                drimg["结果图3"] = dtIMG_1.Rows[iimg]["FImg"];

                                l_img_3.Visible = true;
                                l_img_3.Text = dtIMG_1.Rows[iimg]["FImgNmae"].ToString(); ;
                                break;
                            case 3:
                                img4.Visible = true;
                                img5.Visible = false;
                                drimg["结果图4"] = dtIMG_1.Rows[iimg]["FImg"];

                                l_img_4.Visible = true;
                                l_img_4.Text = dtIMG_1.Rows[iimg]["FImgNmae"].ToString(); ;
                                break;
                            case 4:
                                img5.Visible = true;
                                drimg["结果图5"] = dtIMG_1.Rows[iimg]["FImg"];

                                l_img_5.Visible = true;
                                l_img_5.Text = dtIMG_1.Rows[iimg]["FImgNmae"].ToString(); ;
                                break;
                            default:
                                break;
                        }
                    }

                }
                else
                {
                    groupBoxImg.Visible = false;
                    img1.Visible = false;
                    img2.Visible = false;
                    img3.Visible = false;
                    img4.Visible = false;
                    img5.Visible = false;

                    l_img_1.Visible = false;
                    l_img_2.Visible = false;
                    l_img_3.Visible = false;
                    l_img_4.Visible = false;
                    l_img_5.Visible = false;
                }
                dtIMG.Rows.Add(drimg);
                lIS_REPORT_IMGBindingSource.DataSource = this.dtIMG;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
       

        private void bindingNavigatorOK_Click(object sender, EventArgs e)
        {
            bll样本结果();
            /*
            try
            {
                GelSelValueList();                
                this.Close();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }*/
        }

        private void toolStripButtonNo_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void toolStripButtonQ_Click(object sender, EventArgs e)
        {
            WWQuery();
        }
        private void WWQuery()
        {
            try
            {
                this.dt结果主表 = new DataTable();
                string str状态 = " and FTransFlag='False' ";
                if (checkBox状态.Checked)
                {
                    str状态 = " and FTransFlag='False' ";
                }
                else
                {
                    str状态 = " and FTransFlag='True' ";
                }

                if (int选择结果否 == 0)
                {
                    //str状态 = "";
                }

                string strWhere = "";
                if (fsample_codeTextBox.Text.Equals(""))
                {
                    strWhere = " where 1=1 and finstrid='" + this.str仪器id + "'  and (CONVERT(varchar(100), FDateTime, 23) >='" + fjy_dateDateTimePicker1.Value.ToString("yyyy-MM-dd") + "' and CONVERT(varchar(100), FDateTime, 23) <='" + fjy_dateDateTimePicker2.Value.ToString("yyyy-MM-dd") + "' ) " + str状态 + " order by FResultID,FDateTime desc";
                }
                else
                {
                    strWhere = " where 1=1 and finstrid='" + this.str仪器id + "' and fresultid = " + this.fsample_codeTextBox.Text + " and (CONVERT(varchar(100), FDateTime, 23) >='" + fjy_dateDateTimePicker1.Value.ToString("yyyy-MM-dd") + "' and CONVERT(varchar(100), FDateTime, 23) <='" + fjy_dateDateTimePicker2.Value.ToString("yyyy-MM-dd") + "' ) " + str状态 + " order by FResultID,FDateTime desc";
                }
                //this.richTextBox1.Clear();
                //this.richTextBox1.AppendText(strWhere);
                this.dt结果主表 = this.bllInstrIO.BllIODT(strWhere);
                this.bindingSourceIO.DataSource = this.dt结果主表; 
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
           
        }
        private void WWResult(string strid)
        {
            try
            {
                this.bindingSourceRelust.DataSource = this.bllInstrIO.BllResultDT(strid,str仪器id);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }

             
        }
        
        
       

        private void finstr_idComboBox_DropDownClosed(object sender, EventArgs e)
        {
            WWQuery();
        }

        private void bindingSourceIO_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.dtResult != null)
                    this.dtResult.Clear();
                bindingSourceRelust.DataSource = this.dtResult;

                DataRowView rowCurrent = (DataRowView)bindingSourceIO.Current;
                if (rowCurrent != null)
                {
                    this.str仪器结果FTaskID = rowCurrent["FTaskID"].ToString();
                    string strfio_id = rowCurrent["FTaskID"].ToString();
                    WWResult(strfio_id);
                    WWImg(strfio_id);
                }
                else
                {
                    this.str仪器结果FTaskID = "";
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void dataGridViewReport_DoubleClick(object sender, EventArgs e)
        {
            bll样本结果(); 
            //try
            //{
            //    if (this.dataGridViewReport.Rows.Count > 0)
            //    {
            //        this.str仪器结果FTaskID = this.dataGridViewReport.CurrentRow.Cells["FTaskID"].Value.ToString();
            //    }
            //    else
            //    {
            //        this.str仪器结果FTaskID = "";
            //    }
            //    GelSelValueList();

            //    this.Close();
            //}
            //catch (Exception ex)
            //{
            //    WWMessage.MessageShowError(ex.ToString());
            //}
        }

        private void fjy_dateDateTimePicker1_CloseUp(object sender, EventArgs e)
        {
            WWQuery();
        }

        private void fjy_dateDateTimePicker2_CloseUp(object sender, EventArgs e)
        {
            WWQuery();
        }

        private void 导入图片ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {                
                img.ImgIn(img1);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 导出图片ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                img.ImgOut(img1);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 清空图片ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                img.ImgNull(img1);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 图片浏览ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Bitmap bit = new Bitmap(img1.Image);
                ImgShowForm imgform = new ImgShowForm(bit);
                imgform.ShowDialog();
                imgform.Dispose();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void img1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                Bitmap bit = new Bitmap(img1.Image);
                ImgShowForm imgform = new ImgShowForm(bit);
                imgform.ShowDialog();
                imgform.Dispose();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void fsample_codeTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                WWQuery();
            }  
        }

        private void dataGridViewReport_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void dataGridViewResult_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            WWQuery();
        }
        private void GelSelValueList不用()
        {
            try
            {
                this.Validate();
                this.bindingSourceRelust.EndEdit();
                selItemList = new ArrayList();
                string strfitem_id = "";
                string strfitem_value = "";

                for (int i = 0; i < this.dataGridViewResult.Rows.Count; i++)
                {
                    strfitem_id = "";
                    strfitem_value = "";
                    if (this.dataGridViewResult.Rows[i].Cells["fitem_id"].Value != null)
                    {
                        strfitem_id = this.dataGridViewResult.Rows[i].Cells["fitem_id"].Value.ToString();
                    }
                    if (this.dataGridViewResult.Rows[i].Cells["fvalue"].Value != null)
                    {
                        strfitem_value = this.dataGridViewResult.Rows[i].Cells["fvalue"].Value.ToString();
                    }
                    if (strfitem_id.Equals("") || strfitem_id.Length == 0) { }
                    else
                    {
                        this.selItemList.Add(strfitem_id + ";" + strfitem_value);
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void bll样本结果()
        {
            try
            {
                //update Lis_Ins_Result set FTransFlag='True' FROM Lis_Ins_Result where FTaskID=''
                if (fsample_codeTextBox.Text.Equals("") || fsample_codeTextBox.Text.Length == 0)
                {
                    WWMessage.MessageShowWarning("当前样本号不能为空，请填写后重试。");
                }
                else
                {
                    if (this.dt结果主表.Rows.Count > 0)
                    {
                        ArrayList lisUpdateFlag = new ArrayList();
                        selItemList = new ArrayList();
                        for (int i = 0; i < this.dt结果主表.Rows.Count; i++)
                        {
                            string str结果id = this.dt结果主表.Rows[i]["FTaskID"].ToString();

                          
                                this.str仪器结果FTaskID = str结果id;
                           

                            lisUpdateFlag.Add("update Lis_Ins_Result set FTransFlag='True' FROM Lis_Ins_Result where FTaskID='" + str结果id + "'");

                            DataTable dt结果明细 = this.bllInstrIO.BllResultDT(str结果id, str仪器id);
                            //dataGridView1.DataSource = dt结果明细;
                            for (int ii = 0; ii < dt结果明细.Rows.Count; ii++)
                            {
                                string strfitem_id = "";
                                string strfitem_value = "";
                                if (dt结果明细.Rows[ii]["fitem_id"] != null)
                                {
                                    strfitem_id = dt结果明细.Rows[ii]["fitem_id"].ToString();
                                }
                                if (dt结果明细.Rows[ii]["fvalue"] != null)
                                {
                                    strfitem_value = dt结果明细.Rows[ii]["fvalue"].ToString();
                                }
                                if (strfitem_id.Equals("") || strfitem_id.Length == 0) { }
                                else
                                {
                                    this.selItemList.Add(strfitem_id + ";" + strfitem_value + ";" + dt结果明细.Rows[ii]["Fitem"].ToString()+";"+
                                        dt结果明细.Rows[ii]["fitem_name"].ToString() + ";" + dt结果明细.Rows[ii]["funit_name"].ToString()+";"+
                                        dt结果明细.Rows[ii]["fprint_num"].ToString());
                                }
                            }
                        }
                        if (lisUpdateFlag.Count > 0)
                        {
                            ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(ww.wwf.wwfbll.WWFInit.strDBConn, lisUpdateFlag);
                        }

                    }
                    else
                    {
                       // WWMessage.MessageShowWarning("样本号为：" + fsample_codeTextBox.Text + " 的报告结果为空，可能是未采集或已经取过结果。");
                    }
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;//this.Close();
                }

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            
        }
        

        private void checkBox状态_CheckedChanged(object sender, EventArgs e)
        {
            WWQuery();
        }
        
    }
}