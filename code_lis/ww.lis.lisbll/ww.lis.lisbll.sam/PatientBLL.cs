using System;
using System.Data;
using System.Text;
using ww.wwf.dao;
using ww.wwf.wwfbll;

namespace ww.lis.lisbll.sam
{
	public class PatientBLL : DAOWWF
	{
		private Log ll = new Log();

		public DataTable BllPatientDT(string strWhere)
		{
			DataTable result = null;
			if (!(strWhere == "") && strWhere != null)
			{
				strWhere = " where 1=1 " + strWhere;
				string strSql = "SELECT case when fsex='1' then '男' else '女' end fsex1,* FROM SAM_PATIENT " + strWhere;
				result = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
			}
			return result;
		}

		public DataTable BllPatientDTFromHisDBby住院号(string zyh)
		{
			DataTable result = null;
			if (!string.IsNullOrWhiteSpace(zyh))
			{
				result = WWFInit.wwfRemotingDao.GetPatientInfoBy住院号(WWFInit.strCHisDBConn, zyh).Tables[0];
			}
			return result;
		}

		public DataTable BllPatientDTFromHisDBby档案号(string dnh)
		{
			DataTable result = null;
			if (!string.IsNullOrWhiteSpace(dnh))
			{
				result = WWFInit.wwfRemotingDao.GetPatientInfoBy档案号(WWFInit.strDBConn, dnh).Tables[0];
			}
			return result;
		}

		public DataTable BllPatientDTFromHisDBby档案号New(string dnh)
		{
			DataTable result = null;
			if (!string.IsNullOrWhiteSpace(dnh))
			{
				result = WWFInit.wwfRemotingDao.GetPatientInfoBy档案号New(WWFInit.strDBConn, dnh).Tables[0];
			}
			return result;
		}

		public int BllPatientAdd(DataRow dr)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into SAM_PATIENT(");
			stringBuilder.Append("fhz_id,fhz_zyh,fsex,fname,fvolk,fhymen,fbirthday,fjob,Pfgms,fjob_org,fadd,ftel,fward_num,froom_num,fdept_id,fbed_num,fblood_abo,fblood_rh,fdiagnose,fuse_if,fhelp_code,fremark,ftime_registration,ftime");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + dr["fhz_id"] + "',");
			stringBuilder.Append("'" + dr["fhz_zyh"] + "',");
			stringBuilder.Append("'" + dr["fsex"] + "',");
			stringBuilder.Append("'" + dr["fname"] + "',");
			stringBuilder.Append("'" + dr["fvolk"] + "',");
			stringBuilder.Append("'" + dr["fhymen"] + "',");
			stringBuilder.Append("'" + dr["fbirthday"] + "',");
			stringBuilder.Append("'" + dr["fjob"] + "',");
			stringBuilder.Append("'" + dr["Pfgms"] + "',");
			stringBuilder.Append("'" + dr["fjob_org"] + "',");
			stringBuilder.Append("'" + dr["fadd"] + "',");
			stringBuilder.Append("'" + dr["ftel"] + "',");
			stringBuilder.Append("'" + dr["fward_num"] + "',");
			stringBuilder.Append("'" + dr["froom_num"] + "',");
			stringBuilder.Append("'" + dr["fdept_id"] + "',");
			stringBuilder.Append("'" + dr["fbed_num"] + "',");
			stringBuilder.Append("'" + dr["fblood_abo"] + "',");
			stringBuilder.Append("'" + dr["fblood_rh"] + "',");
			stringBuilder.Append("'" + dr["fdiagnose"] + "',");
			stringBuilder.Append(dr["fuse_if"] + ",");
			stringBuilder.Append("'" + dr["fhelp_code"] + "',");
			stringBuilder.Append("'" + dr["fremark"] + "',");
			stringBuilder.Append("'" + dr["ftime_registration"] + "',");
			stringBuilder.Append("'" + dr["ftime_registration"] + "'");
			stringBuilder.Append(")");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllPatientUpdate(DataRow dr)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update SAM_PATIENT set ");
			stringBuilder.Append("fhz_zyh='" + dr["fhz_zyh"] + "',");
			stringBuilder.Append("fsex='" + dr["fsex"] + "',");
			stringBuilder.Append("fname='" + dr["fname"] + "',");
			stringBuilder.Append("fvolk='" + dr["fvolk"] + "',");
			stringBuilder.Append("fhymen='" + dr["fhymen"] + "',");
			stringBuilder.Append("fbirthday='" + dr["fbirthday"] + "',");
			stringBuilder.Append("fjob='" + dr["fjob"] + "',");
			stringBuilder.Append("Pfgms='" + dr["Pfgms"] + "',");
			stringBuilder.Append("fjob_org='" + dr["fjob_org"] + "',");
			stringBuilder.Append("fadd='" + dr["fadd"] + "',");
			stringBuilder.Append("ftel='" + dr["ftel"] + "',");
			stringBuilder.Append("fward_num='" + dr["fward_num"] + "',");
			stringBuilder.Append("froom_num='" + dr["froom_num"] + "',");
			stringBuilder.Append("fdept_id='" + dr["fdept_id"] + "',");
			stringBuilder.Append("fbed_num='" + dr["fbed_num"] + "',");
			stringBuilder.Append("fblood_abo='" + dr["fblood_abo"] + "',");
			stringBuilder.Append("fblood_rh='" + dr["fblood_rh"] + "',");
			stringBuilder.Append("fdiagnose='" + dr["fdiagnose"] + "',");
			stringBuilder.Append("fuse_if=" + dr["fuse_if"] + ",");
			stringBuilder.Append("fhelp_code='" + dr["fhelp_code"] + "',");
			stringBuilder.Append("fremark='" + dr["fremark"] + "',");
			stringBuilder.Append("ftime_registration='" + dr["ftime_registration"] + "',");
			stringBuilder.Append("ftime='" + base.DbServerDateTim() + "'");
			stringBuilder.Append(" where fhz_id='" + dr["fhz_id"] + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllPatientDel(string id)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("delete FROM SAM_PATIENT ");
			stringBuilder.Append(" where fhz_id='" + id + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public DataTable BllDeptAndPatientDT(string strWherePatient)
		{
			DataTable dataTable = new DataTable();
			DataColumn column = new DataColumn("fid", typeof(string));
			dataTable.Columns.Add(column);
			DataColumn column2 = new DataColumn("fpid", typeof(string));
			dataTable.Columns.Add(column2);
			DataColumn column3 = new DataColumn("fname", typeof(string));
			dataTable.Columns.Add(column3);
			DataColumn column4 = new DataColumn("tag", typeof(string));
			dataTable.Columns.Add(column4);
			DataTable dataTable2 = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, "SELECT fdept_id , '['+fdept_id+']'+fname fname FROM WWF_DEPT WHERE fp_id='0' and (fuse_flag = 1) ORDER BY forder_by").Tables[0];
			for (int i = 0; i < dataTable2.Rows.Count; i++)
			{
				DataRow dataRow = dataTable.NewRow();
				string text = dataTable2.Rows[i]["fdept_id"].ToString();
				dataRow["fid"] = text;
				dataRow["fpid"] = "-1";
				dataRow["tag"] = "dept";
				dataRow["fname"] = dataTable2.Rows[i]["fname"];
				dataTable.Rows.Add(dataRow);
				DataTable dataTable3 = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, "SELECT * FROM SAM_PATIENT WHERE (fdept_id = '" + text + "')" + strWherePatient).Tables[0];
				for (int j = 0; j < dataTable3.Rows.Count; j++)
				{
					DataRow dataRow2 = dataTable.NewRow();
					dataRow2["fid"] = dataTable3.Rows[j]["fhz_id"].ToString();
					dataRow2["fpid"] = text;
					dataRow2["tag"] = "patient";
					if (dataTable3.Rows[j]["fbed_num"].ToString() == "")
					{
						dataRow2["fname"] = dataTable3.Rows[j]["fname"].ToString() + "," + dataTable3.Rows[j]["fhz_id"].ToString();
					}
					else
					{
						dataRow2["fname"] = string.Concat(new string[]
						{
							dataTable3.Rows[j]["fbed_num"].ToString(),
							"床,",
							dataTable3.Rows[j]["fname"].ToString(),
							",",
							dataTable3.Rows[j]["fhz_id"].ToString()
						});
					}
					dataTable.Rows.Add(dataRow2);
				}
			}
			return dataTable;
		}

		public string BllPatientAge(string strfbirthday)
		{
			string result = "";
			if (strfbirthday == "" || strfbirthday == null)
			{
				result = "";
			}
			else
			{
				DateTime d = DateTime.Parse(strfbirthday);
				double totalDays = (DateTime.Now - d).TotalDays;
				double num = Math.Floor(totalDays / 365.0);
				double num2 = totalDays % 365.0;
				double num3 = Math.Floor(num2 / 30.0);
				double num4 = Math.Floor(num2 % 30.0);
				if (num4 > 15.0)
				{
					num3 += 1.0;
					num4 = 0.0;
				}
				if (num3 > 6.0)
				{
					num += 1.0;
					num3 = 0.0;
				}
				if (num == 0.0 & num3 == 0.0 & num4 != 0.0)
				{
					result = "天:" + num4.ToString();
				}
				if (num == 0.0 & num3 != 0.0 & num4 <= 15.0)
				{
					result = "月:" + num3.ToString();
				}
				if (num != 0.0 & num3 <= 6.0)
				{
					result = "岁:" + num.ToString();
				}
			}
			return result;
		}

		public string BllBirthdayByAge(decimal dec年龄, string str年龄单位)
		{
			double num = Convert.ToDouble(dec年龄);
			int num2;
			if (str年龄单位.Equals("1") || str年龄单位.Equals("岁"))
			{
				num2 = 365;
			}
			else if (str年龄单位.Equals("2") || str年龄单位.Equals("月"))
			{
				num2 = 30;
			}
			else if (str年龄单位.Equals("3") || str年龄单位.Equals("天"))
			{
				num2 = 1;
			}
			else
			{
				num2 = 1;
			}
			double num3 = (double)num2 * num;
			return DateTime.Now.AddDays(-num3).ToString("yyyy-MM-dd HH:mm:ss");
		}

		public DataTable BllPatientDTByID(string fhz_id)
		{
			string strSql = "SELECT * FROM SAM_PATIENT WHERE (fhz_id = '" + fhz_id + "')";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public DataTable BllPatientDTByZYH(string fhz_zyh)
		{
			string strSql = "SELECT * FROM SAM_PATIENT WHERE (fhz_id = '" + fhz_zyh + "')";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public DataTable BllPatientDTByApplyNo(string applyNo)
		{
			string strSql = "select * from SAM_APPLY where fapply_id = '" + applyNo + "'";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}
	}
}
