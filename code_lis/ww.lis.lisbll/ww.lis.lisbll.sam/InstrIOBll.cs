using System;
using System.Collections;
using System.Data;
using System.Text;
using ww.wwf.wwfbll;

namespace ww.lis.lisbll.sam
{
	public class InstrIOBll : DAOWWF
	{
		public DataTable BllIODT(string strWhere)
		{
			DataTable result;
			if (strWhere == "" || strWhere == null)
			{
				result = null;
			}
			else
			{
				strWhere = " Where " + strWhere + " order by fsample_code";
				result = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, "SELECT * FROM SAM_INSTR_IO" + strWhere).Tables[0];
			}
			return result;
		}

		public string BllIOAdd(DataRow drRowIO, DataTable dtImg, DataTable dtResult)
		{
			IList list = new ArrayList();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into SAM_INSTR_IO(");
			stringBuilder.Append("fio_id,finstr_id,fdate,fsample_code,fsam_type_id,fjz,fqc,fstate,fcontent,ftime,fremark");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + drRowIO["fio_id"] + "',");
			stringBuilder.Append("'" + drRowIO["finstr_id"] + "',");
			stringBuilder.Append("'" + drRowIO["fdate"] + "',");
			stringBuilder.Append("'" + drRowIO["fsample_code"] + "',");
			stringBuilder.Append("'" + drRowIO["fsam_type_id"] + "',");
			stringBuilder.Append(drRowIO["fjz"] + ",");
			stringBuilder.Append(drRowIO["fqc"] + ",");
			stringBuilder.Append("'" + drRowIO["fstate"] + "',");
			stringBuilder.Append("'" + drRowIO["fcontent"] + "',");
			stringBuilder.Append("'" + drRowIO["ftime"] + "',");
			stringBuilder.Append("'" + drRowIO["fremark"] + "'");
			stringBuilder.Append(")");
			list.Add(stringBuilder.ToString());
			for (int i = 0; i < dtImg.Rows.Count; i++)
			{
				DataRow dataRow = dtImg.Rows[i];
				StringBuilder stringBuilder2 = new StringBuilder();
				stringBuilder2.Append("insert into SAM_INSTR_IO_IMG(");
				stringBuilder2.Append("fimg_id,fio_id,fimg_type,fimg,forder_by,fremark");
				stringBuilder2.Append(")");
				stringBuilder2.Append(" values (");
				stringBuilder2.Append("'" + base.DbGuid() + "',");
				stringBuilder2.Append("'" + dataRow["fio_id"] + "',");
				stringBuilder2.Append("'" + dataRow["fimg_type"] + "',");
				stringBuilder2.Append(dataRow["fimg"] + ",");
				stringBuilder2.Append("'" + dataRow["forder_by"] + "',");
				stringBuilder2.Append("'" + dataRow["fremark"] + "'");
				stringBuilder2.Append(")");
				list.Add(stringBuilder2.ToString());
			}
			for (int j = 0; j < dtResult.Rows.Count; j++)
			{
				DataRow dataRow2 = dtResult.Rows[j];
				StringBuilder stringBuilder3 = new StringBuilder();
				stringBuilder3.Append("insert into SAM_INSTR_IO_RESULT(");
				stringBuilder3.Append("fresult_id,fio_id,fitem_code,fvalue,fod_value,fcutoff_value,fstate,ftime,fremark");
				stringBuilder3.Append(")");
				stringBuilder3.Append(" values (");
				stringBuilder3.Append("'" + base.DbGuid() + "',");
				stringBuilder3.Append("'" + dataRow2["fio_id"] + "',");
				stringBuilder3.Append("'" + dataRow2["fitem_code"] + "',");
				stringBuilder3.Append("'" + dataRow2["fvalue"] + "',");
				stringBuilder3.Append("'" + dataRow2["fod_value"] + "',");
				stringBuilder3.Append("'" + dataRow2["fcutoff_value"] + "',");
				stringBuilder3.Append("'" + dataRow2["fstate"] + "',");
				stringBuilder3.Append("'" + dataRow2["ftime"] + "',");
				stringBuilder3.Append("'" + dataRow2["fremark"] + "'");
				stringBuilder3.Append(")");
				list.Add(stringBuilder3.ToString());
			}
			return WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, list);
		}

		public DataTable BllImgDT(string fio_id)
		{
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, "SELECT * FROM SAM_INSTR_IO_IMG WHERE (fio_id = '" + fio_id + "')").Tables[0];
		}

		public DataTable BllResultDT(string fio_id)
		{
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, "SELECT (SELECT fitem_code FROM SAM_ITEM t1  WHERE (t1.fitem_id = t.fitem_code)) AS fitem_code_name, (SELECT fname FROM SAM_ITEM t1 WHERE (t1.fitem_id = t.fitem_code)) AS fitem_name, * FROM SAM_INSTR_IO_RESULT t WHERE (t.fio_id = '" + fio_id + "')").Tables[0];
		}
	}
}
