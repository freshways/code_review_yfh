using System;
using System.Data;
using System.Text;
using ww.wwf.wwfbll;

namespace ww.lis.lisbll.sam
{
	public class DiseaseBLL : DAOWWF
	{
		public DataTable BllDTByCode(string strfcode, string strfname)
		{
			string strSql = string.Concat(new string[]
			{
				"SELECT * FROM SAM_DISEASE where  (fcode='",
				strfcode,
				"' or fname ='",
				strfname,
				"')"
			});
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public DataTable BllDT(string strWhere)
		{
			string strSql = "SELECT * FROM SAM_DISEASE " + strWhere + " ORDER BY fname";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public DataTable BllTypeDT(int fuse_if)
		{
			string strSql;
			if (fuse_if == 1 || fuse_if == 0)
			{
				strSql = "SELECT * FROM SAM_DISEASE_TYPE where fuse_if=" + fuse_if + " ORDER BY forder_by";
			}
			else
			{
				strSql = "SELECT * FROM SAM_DISEASE_TYPE  ORDER BY forder_by";
			}
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public DataTable BllTypeDTByPid(string fp_id)
		{
			string strSql = "SELECT * FROM SAM_DISEASE_TYPE where fp_id='" + fp_id + "' ORDER BY forder_by";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public int BllTypeAdd(DataRow dr)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into SAM_DISEASE_TYPE(");
			stringBuilder.Append("ftype_id,fp_id,fcode,fname,fname_e,fuse_if,forder_by,fhelp_code,fremark");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + dr["ftype_id"].ToString() + "',");
			stringBuilder.Append("'" + dr["fp_id"].ToString() + "',");
			stringBuilder.Append("'" + dr["fcode"].ToString() + "',");
			stringBuilder.Append("'" + dr["fname"].ToString() + "',");
			stringBuilder.Append("'" + dr["fname_e"].ToString() + "',");
			stringBuilder.Append(dr["fuse_if"].ToString() + ",");
			stringBuilder.Append("'" + dr["forder_by"].ToString() + "',");
			stringBuilder.Append("'" + dr["fhelp_code"].ToString() + "',");
			stringBuilder.Append("'" + dr["fremark"].ToString() + "'");
			stringBuilder.Append(")");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllTypeUpdate(DataRow dr)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update SAM_DISEASE_TYPE set ");
			stringBuilder.Append("fp_id='" + dr["fp_id"].ToString() + "',");
			stringBuilder.Append("fcode='" + dr["fcode"].ToString() + "',");
			stringBuilder.Append("fname='" + dr["fname"].ToString() + "',");
			stringBuilder.Append("fname_e='" + dr["fname_e"].ToString() + "',");
			stringBuilder.Append("fuse_if=" + dr["fuse_if"].ToString() + ",");
			stringBuilder.Append("forder_by='" + dr["forder_by"].ToString() + "',");
			stringBuilder.Append("fhelp_code='" + dr["fhelp_code"].ToString() + "',");
			stringBuilder.Append("fremark='" + dr["fremark"].ToString() + "'");
			stringBuilder.Append(" where ftype_id='" + dr["ftype_id"].ToString() + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllTypeDelete(string id)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("delete FROM SAM_DISEASE_TYPE ");
			stringBuilder.Append(" where ftype_id='" + id + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public DataTable BllDiseaseDTByTypeid(string ftype_id)
		{
			string strSql = "SELECT * FROM SAM_DISEASE where ftype_id='" + ftype_id + "' ORDER BY forder_by";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public int BllDiseaseAdd(DataRow dr)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into SAM_DISEASE(");
			stringBuilder.Append("fid,ftype_id,fcode,fname,fname_e,fuse_if,forder_by,fhelp_code,fremark");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + base.DbGuid() + "',");
			stringBuilder.Append("'" + dr["ftype_id"].ToString() + "',");
			stringBuilder.Append("'" + dr["fcode"].ToString() + "',");
			stringBuilder.Append("'" + dr["fname"].ToString() + "',");
			stringBuilder.Append("'" + dr["fname_e"].ToString() + "',");
			stringBuilder.Append(dr["fuse_if"].ToString() + ",");
			stringBuilder.Append("'" + dr["forder_by"].ToString() + "',");
			stringBuilder.Append("'" + dr["fhelp_code"].ToString() + "',");
			stringBuilder.Append("'" + dr["fremark"].ToString() + "'");
			stringBuilder.Append(")");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllDiseaseUpdate(DataRow dr)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update SAM_DISEASE set ");
			stringBuilder.Append("ftype_id='" + dr["ftype_id"].ToString() + "',");
			stringBuilder.Append("fcode='" + dr["fcode"].ToString() + "',");
			stringBuilder.Append("fname='" + dr["fname"].ToString() + "',");
			stringBuilder.Append("fname_e='" + dr["fname_e"].ToString() + "',");
			stringBuilder.Append("fuse_if=" + dr["fuse_if"].ToString() + ",");
			stringBuilder.Append("forder_by='" + dr["forder_by"].ToString() + "',");
			stringBuilder.Append("fhelp_code='" + dr["fhelp_code"].ToString() + "',");
			stringBuilder.Append("fremark='" + dr["fremark"].ToString() + "'");
			stringBuilder.Append(" where fid='" + dr["fid"].ToString() + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}
	}
}
