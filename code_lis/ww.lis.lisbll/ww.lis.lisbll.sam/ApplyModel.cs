using System;
using ww.wwf.wwfbll;

namespace ww.lis.lisbll.sam
{
	public class ApplyModel : DAOWWF
	{
		private string _fapply_id;

		private string _fjytype_id;

		private string _fsample_type_id;

		private int? _fjz_flag;

		private int? _fcharge_flag;

		private string _fcharge_id;

		private string _fstate;

		private decimal? _fjyf;

		private string _fapply_user_id;

		private string _fapply_dept_id;

		private string _fapply_time;

		private string _ftype_id;

		private string _fhz_id;

		private string _fhz_zyh;

		private string _fsex;

		private string _fname;

		private string _fvolk;

		private string _fhymen;

		private int? _fage;

		private string _fjob;

		private string _fgms;

		private string _fjob_org;

		private string _fadd;

		private string _ftel;

		private string _fage_unit;

		private int? _fage_year;

		private int? _fage_month;

		private int? _fage_day;

		private string _fward_num;

		private string _froom_num;

		private string _fbed_num;

		private string _fblood_abo;

		private string _fblood_rh;

		private string _fjymd;

		private string _fdiagnose;

		private decimal? _fheat;

		private decimal? _fxyld;

		private string _fcyy;

		private int? _fxhdb;

		private string _fcreate_user_id;

		private string _fcreate_time;

		private string _fupdate_user_id;

		private string _fupdate_time;

		private string _f检验项目名称;

		private string _fremark;

		private string _his申请单号;

		private string _sfzh;

		public string fapply_id
		{
			get
			{
				return this._fapply_id;
			}
			set
			{
				this._fapply_id = value;
			}
		}

		public string fjytype_id
		{
			get
			{
				return this._fjytype_id;
			}
			set
			{
				this._fjytype_id = value;
			}
		}

		public string fsample_type_id
		{
			get
			{
				return this._fsample_type_id;
			}
			set
			{
				this._fsample_type_id = value;
			}
		}

		public int? fjz_flag
		{
			get
			{
				return this._fjz_flag;
			}
			set
			{
				this._fjz_flag = value;
			}
		}

		public int? fcharge_flag
		{
			get
			{
				return this._fcharge_flag;
			}
			set
			{
				this._fcharge_flag = value;
			}
		}

		public string fcharge_id
		{
			get
			{
				return this._fcharge_id;
			}
			set
			{
				this._fcharge_id = value;
			}
		}

		public string fstate
		{
			get
			{
				return this._fstate;
			}
			set
			{
				this._fstate = value;
			}
		}

		public decimal? fjyf
		{
			get
			{
				return this._fjyf;
			}
			set
			{
				this._fjyf = value;
			}
		}

		public string fapply_user_id
		{
			get
			{
				return this._fapply_user_id;
			}
			set
			{
				this._fapply_user_id = value;
			}
		}

		public string fapply_dept_id
		{
			get
			{
				return this._fapply_dept_id;
			}
			set
			{
				this._fapply_dept_id = value;
			}
		}

		public string fapply_time
		{
			get
			{
				return this._fapply_time;
			}
			set
			{
				this._fapply_time = value;
			}
		}

		public string ftype_id
		{
			get
			{
				return this._ftype_id;
			}
			set
			{
				this._ftype_id = value;
			}
		}

		public string fhz_id
		{
			get
			{
				return this._fhz_id;
			}
			set
			{
				this._fhz_id = value;
			}
		}

		public string fhz_zyh
		{
			get
			{
				return this._fhz_zyh;
			}
			set
			{
				this._fhz_zyh = value;
			}
		}

		public string fsex
		{
			get
			{
				return this._fsex;
			}
			set
			{
				this._fsex = value;
			}
		}

		public string fname
		{
			get
			{
				return this._fname;
			}
			set
			{
				this._fname = value;
			}
		}

		public string fvolk
		{
			get
			{
				return this._fvolk;
			}
			set
			{
				this._fvolk = value;
			}
		}

		public string fhymen
		{
			get
			{
				return this._fhymen;
			}
			set
			{
				this._fhymen = value;
			}
		}

		public int? fage
		{
			get
			{
				return this._fage;
			}
			set
			{
				this._fage = value;
			}
		}

		public string fjob
		{
			get
			{
				return this._fjob;
			}
			set
			{
				this._fjob = value;
			}
		}

		public string fgms
		{
			get
			{
				return this._fgms;
			}
			set
			{
				this._fgms = value;
			}
		}

		public string fjob_org
		{
			get
			{
				return this._fjob_org;
			}
			set
			{
				this._fjob_org = value;
			}
		}

		public string fadd
		{
			get
			{
				return this._fadd;
			}
			set
			{
				this._fadd = value;
			}
		}

		public string ftel
		{
			get
			{
				return this._ftel;
			}
			set
			{
				this._ftel = value;
			}
		}

		public string fage_unit
		{
			get
			{
				return this._fage_unit;
			}
			set
			{
				this._fage_unit = value;
			}
		}

		public int? fage_year
		{
			get
			{
				return this._fage_year;
			}
			set
			{
				this._fage_year = value;
			}
		}

		public int? fage_month
		{
			get
			{
				return this._fage_month;
			}
			set
			{
				this._fage_month = value;
			}
		}

		public int? fage_day
		{
			get
			{
				return this._fage_day;
			}
			set
			{
				this._fage_day = value;
			}
		}

		public string fward_num
		{
			get
			{
				return this._fward_num;
			}
			set
			{
				this._fward_num = value;
			}
		}

		public string froom_num
		{
			get
			{
				return this._froom_num;
			}
			set
			{
				this._froom_num = value;
			}
		}

		public string fbed_num
		{
			get
			{
				return this._fbed_num;
			}
			set
			{
				this._fbed_num = value;
			}
		}

		public string fblood_abo
		{
			get
			{
				return this._fblood_abo;
			}
			set
			{
				this._fblood_abo = value;
			}
		}

		public string fblood_rh
		{
			get
			{
				return this._fblood_rh;
			}
			set
			{
				this._fblood_rh = value;
			}
		}

		public string fjymd
		{
			get
			{
				return this._fjymd;
			}
			set
			{
				this._fjymd = value;
			}
		}

		public string fdiagnose
		{
			get
			{
				return this._fdiagnose;
			}
			set
			{
				this._fdiagnose = value;
			}
		}

		public decimal? fheat
		{
			get
			{
				return this._fheat;
			}
			set
			{
				this._fheat = value;
			}
		}

		public decimal? fxyld
		{
			get
			{
				return this._fxyld;
			}
			set
			{
				this._fxyld = value;
			}
		}

		public string fcyy
		{
			get
			{
				return this._fcyy;
			}
			set
			{
				this._fcyy = value;
			}
		}

		public int? fxhdb
		{
			get
			{
				return this._fxhdb;
			}
			set
			{
				this._fxhdb = value;
			}
		}

		public string fcreate_user_id
		{
			get
			{
				return this._fcreate_user_id;
			}
			set
			{
				this._fcreate_user_id = value;
			}
		}

		public string fcreate_time
		{
			get
			{
				return this._fcreate_time;
			}
			set
			{
				this._fcreate_time = value;
			}
		}

		public string fupdate_user_id
		{
			get
			{
				return this._fupdate_user_id;
			}
			set
			{
				this._fupdate_user_id = value;
			}
		}

		public string fupdate_time
		{
			get
			{
				return this._fupdate_time;
			}
			set
			{
				this._fupdate_time = value;
			}
		}

		public string F检验项目名称
		{
			get
			{
				return this._f检验项目名称;
			}
			set
			{
				this._f检验项目名称 = value;
			}
		}

		public string fremark
		{
			get
			{
				return this._fremark;
			}
			set
			{
				this._fremark = value;
			}
		}

		public string His申请单号
		{
			get
			{
				return this._his申请单号;
			}
			set
			{
				this._his申请单号 = value;
			}
		}

		public string Sfzh
		{
			get
			{
				return this._sfzh;
			}
			set
			{
				this._sfzh = value;
			}
		}
	}
}
