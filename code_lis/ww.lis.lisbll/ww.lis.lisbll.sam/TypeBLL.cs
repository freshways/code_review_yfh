using System;
using System.Collections;
using System.Data;
using System.Text;
using ww.wwf.wwfbll;

namespace ww.lis.lisbll.sam
{
	public class TypeBLL : DAOWWF
	{
		public DataTable BllCheckTypeDT(int fuse_if)
		{
			string strSql;
			if (fuse_if == 1 || fuse_if == 0)
			{
				strSql = "SELECT * FROM sam_check_type where fuse_if=" + fuse_if + " ORDER BY forder_by";
			}
			else
			{
				strSql = "SELECT * FROM sam_check_type  ORDER BY forder_by";
			}
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public DataTable BllSamTypeDT(int fuse_if)
		{
			string strSql;
			if (fuse_if == 1 || fuse_if == 0)
			{
				strSql = "SELECT * FROM sam_sample_type where fuse_if=" + fuse_if + " ORDER BY forder_by";
			}
			else
			{
				strSql = "SELECT * FROM sam_sample_type  ORDER BY forder_by";
			}
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public DataTable BllCheckAndSamTypeDT(string fcheck_type_id)
		{
			string strSql = "SELECT t2.fcheck_type_id, t2.fsample_type_id, t2.fid, t1.fname, t1.forder_by,t1.fhelp_code FROM SAM_CHECK_SAMPLE t2,SAM_SAMPLE_TYPE t1 where t2.fsample_type_id = t1.fsample_type_id and t2.fcheck_type_id='" + fcheck_type_id + "' order by t1.forder_by";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public int BllCheckAndSamTypeDel(string fid)
		{
			string strSql = "DELETE FROM SAM_CHECK_SAMPLE WHERE (fid = '" + fid + "')";
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, strSql);
		}

		public bool BllCheckAndSamTypeExists(string fcheck_type_id, string fsample_type_id)
		{
			string sql = string.Concat(new string[]
			{
				"SELECT count(1) FROM SAM_CHECK_SAMPLE where fcheck_type_id='",
				fcheck_type_id,
				"' and fsample_type_id='",
				fsample_type_id,
				"'"
			});
			return WWFInit.wwfRemotingDao.DbRecordExists(WWFInit.strDBConn, sql);
		}

		public int BllCheckAndSamTypeAdd(string fcheck_type_id, string fsample_type_id)
		{
			string strSql = string.Concat(new string[]
			{
				"INSERT INTO SAM_CHECK_SAMPLE (fid, fcheck_type_id, fsample_type_id) VALUES ('",
				base.DbGuid(),
				"', '",
				fcheck_type_id,
				"', '",
				fsample_type_id,
				"')"
			});
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, strSql);
		}

		public DataTable BllCheckGroupDT(int fuse_if)
		{
			string strSql;
			if (fuse_if == 1 || fuse_if == 0)
			{
				strSql = "SELECT * FROM wwf_dept where fuse_flag=" + fuse_if + " ORDER BY forder_by";
			}
			else
			{
				strSql = "SELECT * FROM wwf_dept where fp_id='jyk' ORDER BY forder_by";
			}
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public DataTable BllComTypeDT(string ftype)
		{
			string strSql = "SELECT * FROM sam_type where ftype='" + ftype + "' ORDER BY forder_by";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public DataTable BllComTypeDT(string ftype, int fuse_if, string strfhelp_code)
		{
			string strSql = string.Concat(new object[]
			{
				"SELECT * FROM sam_type where (ftype='",
				ftype,
				"' and fuse_if='",
				fuse_if,
				"') and ((fhelp_code like '%",
				strfhelp_code,
				"%') or (fhelp_code IS NULL)) ORDER BY forder_by"
			});
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public DataTable BllComTypeDTByCode(string ftype, string strfcode, string strfname)
		{
			string strSql = string.Concat(new string[]
			{
				"SELECT * FROM sam_type where (ftype='",
				ftype,
				"') and (fcode='",
				strfcode,
				"' or fname ='",
				strfname,
				"')"
			});
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public string BllComTypeNameByCode(string ftype, string strfcode)
		{
			string strSql = string.Concat(new string[]
			{
				"SELECT fname FROM sam_type where (ftype='",
				ftype,
				"') and (fcode='",
				strfcode,
				"')"
			});
			return WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn, strSql);
		}

		public string BllComTypeHelpCodeUpdate(IList lisSql)
		{
			return WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, lisSql);
		}

		public int BllComTypeAdd(DataRow dr)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into sam_type(");
			stringBuilder.Append("fid,ftype,fcode,fhelp_code,fname,forder_by,fuse_if,fremark");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + dr["fid"].ToString() + "',");
			stringBuilder.Append("'" + dr["ftype"].ToString() + "',");
			stringBuilder.Append("'" + dr["fcode"].ToString() + "',");
			stringBuilder.Append("'" + dr["fhelp_code"].ToString() + "',");
			stringBuilder.Append("'" + dr["fname"].ToString() + "',");
			stringBuilder.Append("'" + dr["forder_by"].ToString() + "',");
			stringBuilder.Append(dr["fuse_if"].ToString() + ",");
			stringBuilder.Append("'" + dr["fremark"].ToString() + "'");
			stringBuilder.Append(")");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllComTypeUpdate(DataRow dr)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update sam_type set ");
			stringBuilder.Append("fhelp_code='" + dr["fhelp_code"].ToString() + "',");
			stringBuilder.Append("fname='" + dr["fname"].ToString() + "',");
			stringBuilder.Append("forder_by='" + dr["forder_by"].ToString() + "',");
			stringBuilder.Append("fuse_if=" + dr["fuse_if"].ToString() + ",");
			stringBuilder.Append("fremark='" + dr["fremark"].ToString() + "'");
			stringBuilder.Append(" where fid='" + dr["fid"].ToString() + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}
	}
}
