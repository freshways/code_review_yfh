using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using ww.wwf.wwfbll;

namespace ww.lis.lisbll.sam
{
	public class JYInstrDataBll : DAOWWF
	{
		public DataTable BllIODT(string strWhere)
		{
			string strSql = "SELECT * FROM Lis_Ins_Result " + strWhere;
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public string BllIOAdd(DataRow drRowIO, DataTable dtImg, DataTable dtResult)
		{
			IList list = new ArrayList();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into SAM_INSTR_IO(");
			stringBuilder.Append("fio_id,finstr_id,fdate,fsample_code,fsam_type_id,fjz,fqc,fstate,fcontent,ftime,fremark");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + drRowIO["fio_id"] + "',");
			stringBuilder.Append("'" + drRowIO["finstr_id"] + "',");
			stringBuilder.Append("'" + drRowIO["fdate"] + "',");
			stringBuilder.Append("'" + drRowIO["fsample_code"] + "',");
			stringBuilder.Append("'" + drRowIO["fsam_type_id"] + "',");
			stringBuilder.Append(drRowIO["fjz"] + ",");
			stringBuilder.Append(drRowIO["fqc"] + ",");
			stringBuilder.Append("'" + drRowIO["fstate"] + "',");
			stringBuilder.Append("'" + drRowIO["fcontent"] + "',");
			stringBuilder.Append("'" + drRowIO["ftime"] + "',");
			stringBuilder.Append("'" + drRowIO["fremark"] + "'");
			stringBuilder.Append(")");
			list.Add(stringBuilder.ToString());
			for (int i = 0; i < dtImg.Rows.Count; i++)
			{
				DataRow dataRow = dtImg.Rows[i];
				StringBuilder stringBuilder2 = new StringBuilder();
				stringBuilder2.Append("insert into SAM_INSTR_IO_IMG(");
				stringBuilder2.Append("fimg_id,fio_id,fimg_type,fimg,forder_by,fremark");
				stringBuilder2.Append(")");
				stringBuilder2.Append(" values (");
				stringBuilder2.Append("'" + base.DbGuid() + "',");
				stringBuilder2.Append("'" + dataRow["fio_id"] + "',");
				stringBuilder2.Append("'" + dataRow["fimg_type"] + "',");
				stringBuilder2.Append(dataRow["fimg"] + ",");
				stringBuilder2.Append("'" + dataRow["forder_by"] + "',");
				stringBuilder2.Append("'" + dataRow["fremark"] + "'");
				stringBuilder2.Append(")");
				list.Add(stringBuilder2.ToString());
			}
			for (int j = 0; j < dtResult.Rows.Count; j++)
			{
				DataRow dataRow2 = dtResult.Rows[j];
				StringBuilder stringBuilder3 = new StringBuilder();
				stringBuilder3.Append("insert into SAM_INSTR_IO_RESULT(");
				stringBuilder3.Append("fresult_id,fio_id,fitem_code,fvalue,fod_value,fcutoff_value,fstate,ftime,fremark");
				stringBuilder3.Append(")");
				stringBuilder3.Append(" values (");
				stringBuilder3.Append("'" + base.DbGuid() + "',");
				stringBuilder3.Append("'" + dataRow2["fio_id"] + "',");
				stringBuilder3.Append("'" + dataRow2["fitem_code"] + "',");
				stringBuilder3.Append("'" + dataRow2["fvalue"] + "',");
				stringBuilder3.Append("'" + dataRow2["fod_value"] + "',");
				stringBuilder3.Append("'" + dataRow2["fcutoff_value"] + "',");
				stringBuilder3.Append("'" + dataRow2["fstate"] + "',");
				stringBuilder3.Append("'" + dataRow2["ftime"] + "',");
				stringBuilder3.Append("'" + dataRow2["fremark"] + "'");
				stringBuilder3.Append(")");
				list.Add(stringBuilder3.ToString());
			}
			return WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, list);
		}

		public DataTable BllImgDT(string strTaskId)
		{
			string strSql = "SELECT * FROM Lis_Ins_Result_Img WHERE (FTaskID = '" + strTaskId + "') ORDER BY FOrder";
			return WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, strSql);
		}

		public DataTable BllResultDT(string str结果id, string str仪器id)
		{
			DataTable dataTable = new DataTable();
			string strSql = string.Concat(new string[]
			{
				"SELECT b.fitem_id as fitem_id,c.fname as fitem_name,c.funit_name,c.fprint_num,c.fref,a.*  FROM Lis_Ins_Result_Detail a,SAM_ITEM_IO b,SAM_ITEM c WHERE a.FItem = b.fcode and b.fitem_id = c.fitem_id and b.finstr_id='",
				str仪器id,
				"' and (FTaskID = '",
				str结果id,
				"')  order by convert(int,c.fprint_num)"
			});
			return WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, strSql);
		}

		public DataTable BllGetCaluatedItems(string str仪器id)
		{
			DataTable dataTable = new DataTable();
			string strSql = "SELECT b.fitem_id as fitem_id,a.fitem_code FItem, a.fname as fitem_name,a.funit_name,a.fprint_num,a.fref,a.fjx_formula FROM SAM_ITEM a, SAM_ITEM_IO b WHERE a.fitem_id = b.fitem_id and b.finstr_id='" + str仪器id + "' and a.fjx_if=1 ";
			return WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, strSql);
		}

		public string GetInsResultID(string str日期, string str仪器ID, int int样本号)
		{
			string strSql = "SELECT [FTaskID],[FInstrID],[FResultID],[FDateTime] FROM [Lis_Ins_Result]\r\n            where FDateTime >= @testDate and FDateTime < CAST(@testDate as datetime)+1 and FInstrID=@instrID and FResultID=@resultid";
			SqlParameter[] paramsList = new SqlParameter[]
			{
				new SqlParameter("@testDate", str日期),
				new SqlParameter("@instrID", str仪器ID),
				new SqlParameter("@resultid", int样本号)
			};
			DataSet dataSet = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql, paramsList);
			string result;
			if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
			{
				result = dataSet.Tables[0].Rows[0]["FTaskID"].ToString();
			}
			else
			{
				result = null;
			}
			return result;
		}
	}
}
