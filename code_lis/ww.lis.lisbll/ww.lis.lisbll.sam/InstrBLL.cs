using System;
using System.Data;
using System.Text;
using ww.wwf.wwfbll;

namespace ww.lis.lisbll.sam
{
	public class InstrBLL : DAOWWF
	{
		public DataTable BllTypeDT(int fuse_if)
		{
			string strSql;
			if (fuse_if == 1 || fuse_if == 0)
			{
				strSql = "SELECT * FROM sam_instr_type where fuse_if=" + fuse_if + " ORDER BY forder_by";
			}
			else
			{
				strSql = "SELECT * FROM sam_instr_type  ORDER BY forder_by";
			}
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public DataTable BllTypeDTByPid(string fp_id)
		{
			string strSql = "SELECT * FROM sam_instr_type where fp_id='" + fp_id + "' ORDER BY forder_by";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public int BllTypeAdd(DataRowView dr)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into sam_instr_type(");
			stringBuilder.Append("ftype_id,fp_id,fcode,fname,fname_e,fuse_if,forder_by,fhelp_code,fremark");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + dr["ftype_id"].ToString() + "',");
			stringBuilder.Append("'" + dr["fp_id"].ToString() + "',");
			stringBuilder.Append("'" + dr["fcode"].ToString() + "',");
			stringBuilder.Append("'" + dr["fname"].ToString() + "',");
			stringBuilder.Append("'" + dr["fname_e"].ToString() + "',");
			stringBuilder.Append(dr["fuse_if"].ToString() + ",");
			stringBuilder.Append("'" + dr["forder_by"].ToString() + "',");
			stringBuilder.Append("'" + dr["fhelp_code"].ToString() + "',");
			stringBuilder.Append("'" + dr["fremark"].ToString() + "'");
			stringBuilder.Append(")");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllTypeUpdate(DataRowView dr)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update sam_instr_type set ");
			stringBuilder.Append("fp_id='" + dr["fp_id"] + "',");
			stringBuilder.Append("fcode='" + dr["fcode"] + "',");
			stringBuilder.Append("fname='" + dr["fname"] + "',");
			stringBuilder.Append("fname_e='" + dr["fname_e"] + "',");
			stringBuilder.Append("fuse_if=" + dr["fuse_if"] + ",");
			stringBuilder.Append("forder_by='" + dr["forder_by"] + "',");
			stringBuilder.Append("fhelp_code='" + dr["fhelp_code"] + "',");
			stringBuilder.Append("fremark='" + dr["fremark"] + "'");
			stringBuilder.Append(" where ftype_id='" + dr["ftype_id"] + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllTypeDelete(string id)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("delete FROM sam_instr_type ");
			stringBuilder.Append(" where ftype_id='" + id + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public DataTable BllInstrDT(int fuse_if)
		{
			string strSql;
			if (fuse_if == 1 || fuse_if == 0)
			{
				strSql = "SELECT * FROM sam_instr where fuse_if=" + fuse_if + " ORDER BY forder_by";
			}
			else
			{
				strSql = "SELECT * FROM sam_instr  ORDER BY forder_by";
			}
			DataTable dataTable = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
			DataRow row = dataTable.NewRow();
			dataTable.Rows.Add(row);
			return dataTable;
		}

		public DataTable BllInstrDTByTypeID(string ftype_id)
		{
			string strSql = "SELECT * FROM sam_instr where ftype_id='" + ftype_id + "' ORDER BY forder_by";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public int BllInstrAdd(DataRowView dr)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into sam_instr(");
			stringBuilder.Append("finstr_id,ftype_id,fjygroup_id,fjytype_id,fuse_if,fname,forder_by,fremark");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + dr["finstr_id"].ToString() + "',");
			stringBuilder.Append("'" + dr["ftype_id"].ToString() + "',");
			stringBuilder.Append("'" + dr["fjygroup_id"].ToString() + "',");
			stringBuilder.Append("'" + dr["fjytype_id"].ToString() + "',");
			stringBuilder.Append(dr["fuse_if"].ToString() + ",");
			stringBuilder.Append("'" + dr["fname"].ToString() + "',");
			stringBuilder.Append("'" + dr["forder_by"].ToString() + "',");
			stringBuilder.Append("'" + dr["fremark"].ToString() + "'");
			stringBuilder.Append(")");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllInstrUpdate(DataRowView dr)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update sam_instr set ");
			stringBuilder.Append("ftype_id='" + dr["ftype_id"] + "',");
			stringBuilder.Append("fjygroup_id='" + dr["fjygroup_id"] + "',");
			stringBuilder.Append("fjytype_id='" + dr["fjytype_id"] + "',");
			stringBuilder.Append("fuse_if=" + dr["fuse_if"] + ",");
			stringBuilder.Append("fname='" + dr["fname"] + "',");
			stringBuilder.Append("forder_by='" + dr["forder_by"] + "',");
			stringBuilder.Append("fremark='" + dr["fremark"] + "'");
			stringBuilder.Append(" where finstr_id='" + dr["finstr_id"] + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllInstrDelete(string id)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("delete FROM sam_instr ");
			stringBuilder.Append(" where finstr_id='" + id + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public DataTable BllDriveDT(string finstr_id)
		{
			string strSql = "SELECT * FROM sam_instr_drive where finstr_id='" + finstr_id + "'";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public bool BllDriveOK(string finstr_id)
		{
			return WWFInit.wwfRemotingDao.DbRecordExists(WWFInit.strDBConn, "SELECT count(1) FROM sam_instr_drive where finstr_id='" + finstr_id + "'");
		}

		public int BllDriveAdd(DataRow dr)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into SAM_INSTR_DRIVE(");
			stringBuilder.Append("fdrive_id,finstr_id,fstart_bit,fend_bit,frequest_code,fresponse_code,fport,fbaud_rate,fparity,fdata_bit,fstop_bit,fstream,finfo_mode,fjz_flag,fimg_flag,fdrive,freadbufferSize,fwritebufferSize,freadtimeout,fwritetimeout,fy_mess,fry_mess,fhigh_mess,flow_mess,ferror_mess,fip,fpc_name,fremark");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + dr["fdrive_id"] + "',");
			stringBuilder.Append("'" + dr["finstr_id"] + "',");
			stringBuilder.Append("'" + dr["fstart_bit"] + "',");
			stringBuilder.Append("'" + dr["fend_bit"] + "',");
			stringBuilder.Append("'" + dr["frequest_code"] + "',");
			stringBuilder.Append("'" + dr["fresponse_code"] + "',");
			stringBuilder.Append("'" + dr["fport"] + "',");
			stringBuilder.Append("'" + dr["fbaud_rate"] + "',");
			stringBuilder.Append("'" + dr["fparity"] + "',");
			stringBuilder.Append("'" + dr["fdata_bit"] + "',");
			stringBuilder.Append("'" + dr["fstop_bit"] + "',");
			stringBuilder.Append("'" + dr["fstream"] + "',");
			stringBuilder.Append("'" + dr["finfo_mode"] + "',");
			stringBuilder.Append(dr["fjz_flag"] + ",");
			stringBuilder.Append(dr["fimg_flag"] + ",");
			stringBuilder.Append("'" + dr["fdrive"] + "',");
			stringBuilder.Append(dr["freadbufferSize"] + ",");
			stringBuilder.Append(dr["fwritebufferSize"] + ",");
			stringBuilder.Append(dr["freadtimeout"] + ",");
			stringBuilder.Append(dr["fwritetimeout"] + ",");
			stringBuilder.Append("'" + dr["fy_mess"] + "',");
			stringBuilder.Append("'" + dr["fry_mess"] + "',");
			stringBuilder.Append("'" + dr["fhigh_mess"] + "',");
			stringBuilder.Append("'" + dr["flow_mess"] + "',");
			stringBuilder.Append("'" + dr["ferror_mess"] + "',");
			stringBuilder.Append("'" + dr["fip"] + "',");
			stringBuilder.Append("'" + dr["fpc_name"] + "',");
			stringBuilder.Append("'" + dr["fremark"] + "'");
			stringBuilder.Append(")");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllDriveUpdate(DataRow dr)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update SAM_INSTR_DRIVE set ");
			stringBuilder.Append("finstr_id='" + dr["finstr_id"] + "',");
			stringBuilder.Append("fstart_bit='" + dr["fstart_bit"] + "',");
			stringBuilder.Append("fend_bit='" + dr["fend_bit"] + "',");
			stringBuilder.Append("frequest_code='" + dr["frequest_code"] + "',");
			stringBuilder.Append("fresponse_code='" + dr["fresponse_code"] + "',");
			stringBuilder.Append("fport='" + dr["fport"] + "',");
			stringBuilder.Append("fbaud_rate='" + dr["fbaud_rate"] + "',");
			stringBuilder.Append("fparity='" + dr["fparity"] + "',");
			stringBuilder.Append("fdata_bit='" + dr["fdata_bit"] + "',");
			stringBuilder.Append("fstop_bit='" + dr["fstop_bit"] + "',");
			stringBuilder.Append("fstream='" + dr["fstream"] + "',");
			stringBuilder.Append("finfo_mode='" + dr["finfo_mode"] + "',");
			stringBuilder.Append("fjz_flag=" + dr["fjz_flag"] + ",");
			stringBuilder.Append("fimg_flag=" + dr["fimg_flag"] + ",");
			stringBuilder.Append("fdrive='" + dr["fdrive"] + "',");
			stringBuilder.Append("freadbufferSize=" + dr["freadbufferSize"] + ",");
			stringBuilder.Append("fwritebufferSize=" + dr["fwritebufferSize"] + ",");
			stringBuilder.Append("freadtimeout=" + dr["freadtimeout"] + ",");
			stringBuilder.Append("fwritetimeout=" + dr["fwritetimeout"] + ",");
			stringBuilder.Append("fy_mess='" + dr["fy_mess"] + "',");
			stringBuilder.Append("fry_mess='" + dr["fry_mess"] + "',");
			stringBuilder.Append("fhigh_mess='" + dr["fhigh_mess"] + "',");
			stringBuilder.Append("flow_mess='" + dr["flow_mess"] + "',");
			stringBuilder.Append("ferror_mess='" + dr["ferror_mess"] + "',");
			stringBuilder.Append("fip='" + dr["fip"] + "',");
			stringBuilder.Append("fpc_name='" + dr["fpc_name"] + "',");
			stringBuilder.Append("fremark='" + dr["fremark"] + "'");
			stringBuilder.Append(" where fdrive_id='" + dr["fdrive_id"] + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllDriveDelete(string id)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("delete FROM sam_instr_drive ");
			stringBuilder.Append(" where fdrive_id='" + id + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public DataTable BllInstrDTByUseAndGroupID(int fuse_if, string fjygroup_id)
		{
			string strSql;
			if (fjygroup_id == "kfz")
			{
				strSql = "SELECT finstr_id,fname,fname as ShowName,fjytype_id FROM sam_instr WHERE (fuse_if = " + fuse_if + ") ORDER BY forder_by";
			}
			else
			{
				strSql = string.Concat(new object[]
				{
					"SELECT finstr_id,fname,fname as ShowName,fjytype_id FROM sam_instr WHERE (fuse_if = ",
					fuse_if,
					") AND (fjygroup_id = '",
					fjygroup_id,
					"') ORDER BY forder_by"
				});
			}
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public DataTable BllReportDT(string finstr_id)
		{
			string strSql = "SELECT * FROM SAM_REPORT WHERE (finstr_id = '" + finstr_id + "')";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public DataTable BllReportDT(string finstr_id, int fuse_if)
		{
			string strSql = string.Concat(new object[]
			{
				"SELECT * FROM SAM_REPORT WHERE (finstr_id = '",
				finstr_id,
				"') and fuse_if=",
				fuse_if
			});
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public int BllReportAdd(DataRow dr)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into SAM_REPORT(");
			stringBuilder.Append("fsetting_id,finstr_id,fuse_if,fpage_width,fpage_height,fmargin_top,fmargin_bottom,fmargin_left,fmargin_right,frows,fone_count");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + dr["fsetting_id"].ToString() + "',");
			stringBuilder.Append("'" + dr["finstr_id"].ToString() + "',");
			stringBuilder.Append(dr["fuse_if"].ToString() + ",");
			stringBuilder.Append(dr["fpage_width"].ToString() + ",");
			stringBuilder.Append(dr["fpage_height"].ToString() + ",");
			stringBuilder.Append(dr["fmargin_top"].ToString() + ",");
			stringBuilder.Append(dr["fmargin_bottom"].ToString() + ",");
			stringBuilder.Append(dr["fmargin_left"].ToString() + ",");
			stringBuilder.Append(dr["fmargin_right"].ToString() + ",");
			stringBuilder.Append(dr["frows"].ToString() + ",");
			stringBuilder.Append(dr["fone_count"].ToString() ?? "");
			stringBuilder.Append(")");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllReportUpdate(DataRow dr)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update SAM_REPORT set ");
			stringBuilder.Append("ftype_id='" + dr["ftype_id"] + "',");
			stringBuilder.Append("finstr_id='" + dr["finstr_id"] + "',");
			stringBuilder.Append("fcode='" + dr["fcode"] + "',");
			stringBuilder.Append("fname='" + dr["fname"] + "',");
			stringBuilder.Append("fprinter_name='" + dr["fprinter_name"] + "',");
			stringBuilder.Append("fpaper_name='" + dr["fpaper_name"] + "',");
			stringBuilder.Append("fpage_width=" + dr["fpage_width"] + ",");
			stringBuilder.Append("fpage_height=" + dr["fpage_height"] + ",");
			stringBuilder.Append("fmargin_top=" + dr["fmargin_top"] + ",");
			stringBuilder.Append("fmargin_bottom=" + dr["fmargin_bottom"] + ",");
			stringBuilder.Append("fmargin_left=" + dr["fmargin_left"] + ",");
			stringBuilder.Append("fmargin_right=" + dr["fmargin_right"] + ",");
			stringBuilder.Append("forientation='" + dr["forientation"] + "',");
			stringBuilder.Append("fuse_if=" + dr["fuse_if"] + ",");
			stringBuilder.Append("forder_by='" + dr["forder_by"] + "',");
			stringBuilder.Append("frows=" + dr["frows"] + ",");
			stringBuilder.Append("fone_count=" + dr["fone_count"] + ",");
			stringBuilder.Append("fy_mess='" + dr["fy_mess"] + "',");
			stringBuilder.Append("fry_mess='" + dr["fry_mess"] + "',");
			stringBuilder.Append("fhight_mess='" + dr["fhight_mess"] + "',");
			stringBuilder.Append("flow_mess='" + dr["flow_mess"] + "',");
			stringBuilder.Append("ferror_mess='" + dr["ferror_mess"] + "',");
			stringBuilder.Append("f1='" + dr["f1"] + "',");
			stringBuilder.Append("f2='" + dr["f2"] + "',");
			stringBuilder.Append("f3='" + dr["f3"] + "',");
			stringBuilder.Append("f4='" + dr["f4"] + "',");
			stringBuilder.Append("f5='" + dr["f5"] + "',");
			stringBuilder.Append("f6='" + dr["f6"] + "',");
			stringBuilder.Append("f7='" + dr["f7"] + "',");
			stringBuilder.Append("f8='" + dr["f8"] + "',");
			stringBuilder.Append("f9='" + dr["f9"] + "',");
			stringBuilder.Append("f10='" + dr["f10"] + "',");
			stringBuilder.Append("fremark='" + dr["fremark"] + "'");
			stringBuilder.Append(" where fsetting_id='" + dr["fsetting_id"] + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}
	}
}
