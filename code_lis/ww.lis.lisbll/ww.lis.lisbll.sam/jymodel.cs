using System;

namespace ww.lis.lisbll.sam
{
	public class jymodel
	{
		private string _fjy_id;

		private string _fjy_date;

		private string _fjy_instr;

		private string _fjy_yb_tm;

		private string _fjy_yb_code;

		private string _fjy_yb_type;

		private string _fjy_sf_type;

		private decimal _fjy_f;

		private string _fjy_f_ok;

		private string _fjy_zt;

		private string _fjy_md;

		private string _fjy_lczd;

		private string _fhz_id;

		private string _fhz_type_id;

		private string _fhz_zyh;

		private string _fhz_name;

		private string _fhz_sex;

		private decimal _fhz_age;

		private string _fhz_age_unit;

		private string _fhz_age_date;

		private string _fhz_bq;

		private string _fhz_room;

		private string _fhz_bed;

		private string _fhz_volk;

		private string _fhz_hy;

		private string _fhz_job;

		private string _fhz_gms;

		private string _fhz_add;

		private string _fhz_tel;

		private string _fapply_id;

		private string _fapply_user_id;

		private string _fapply_dept_id;

		private string _fapply_time;

		private string _fsampling_user_id;

		private string _fsampling_time;

		private string _fjy_user_id;

		private string _fchenk_user_id;

		private string _fcheck_time;

		private string _freport_time;

		private string _fprint_time;

		private string _fprint_zt;

		private int _fprint_count;

		private string _fsys_user;

		private string _fsys_time;

		private string _fsys_dept;

		private string _fremark;

		private string _f1;

		private string _f2;

		private string _f3;

		private string _f4;

		private string _f5;

		private string _f6;

		private string _f7;

		private string _f8;

		private string _f9;

		private string _f10;

		private string _fhz_dept;

		private string _finstr_result_id;

		private string _sfzh;

		public string S身份证号
		{
			get
			{
				return this._sfzh;
			}
			set
			{
				this._sfzh = value;
			}
		}

		public string finstr_result_id
		{
			get
			{
				return this._finstr_result_id;
			}
			set
			{
				this._finstr_result_id = value;
			}
		}

		public string fjy_检验id
		{
			get
			{
				return this._fjy_id;
			}
			set
			{
				this._fjy_id = value;
			}
		}

		public string fjy_检验日期
		{
			get
			{
				return this._fjy_date;
			}
			set
			{
				this._fjy_date = value;
			}
		}

		public string fjy_仪器ID
		{
			get
			{
				return this._fjy_instr;
			}
			set
			{
				this._fjy_instr = value;
			}
		}

		public string fjy_yb_tm
		{
			get
			{
				return this._fjy_yb_tm;
			}
			set
			{
				this._fjy_yb_tm = value;
			}
		}

		public string fjy_yb_code
		{
			get
			{
				return this._fjy_yb_code;
			}
			set
			{
				this._fjy_yb_code = value;
			}
		}

		public string fjy_yb_type
		{
			get
			{
				return this._fjy_yb_type;
			}
			set
			{
				this._fjy_yb_type = value;
			}
		}

		public string fjy_收费类型ID
		{
			get
			{
				return this._fjy_sf_type;
			}
			set
			{
				this._fjy_sf_type = value;
			}
		}

		public decimal fjy_f
		{
			get
			{
				return this._fjy_f;
			}
			set
			{
				this._fjy_f = value;
			}
		}

		public string fjy_f_ok
		{
			get
			{
				return this._fjy_f_ok;
			}
			set
			{
				this._fjy_f_ok = value;
			}
		}

		public string fjy_zt检验状态
		{
			get
			{
				return this._fjy_zt;
			}
			set
			{
				this._fjy_zt = value;
			}
		}

		public string fjy_md
		{
			get
			{
				return this._fjy_md;
			}
			set
			{
				this._fjy_md = value;
			}
		}

		public string fjy_lczd
		{
			get
			{
				return this._fjy_lczd;
			}
			set
			{
				this._fjy_lczd = value;
			}
		}

		public string fhz_id
		{
			get
			{
				return this._fhz_id;
			}
			set
			{
				this._fhz_id = value;
			}
		}

		public string fhz_患者类型id
		{
			get
			{
				return this._fhz_type_id;
			}
			set
			{
				this._fhz_type_id = value;
			}
		}

		public string fhz_住院号
		{
			get
			{
				return this._fhz_zyh;
			}
			set
			{
				this._fhz_zyh = value;
			}
		}

		public string fhz_姓名
		{
			get
			{
				return this._fhz_name;
			}
			set
			{
				this._fhz_name = value;
			}
		}

		public string fhz_性别
		{
			get
			{
				return this._fhz_sex;
			}
			set
			{
				this._fhz_sex = value;
			}
		}

		public decimal fhz_年龄
		{
			get
			{
				return this._fhz_age;
			}
			set
			{
				this._fhz_age = value;
			}
		}

		public string fhz_年龄单位
		{
			get
			{
				return this._fhz_age_unit;
			}
			set
			{
				this._fhz_age_unit = value;
			}
		}

		public string fhz_生日
		{
			get
			{
				return this._fhz_age_date;
			}
			set
			{
				this._fhz_age_date = value;
			}
		}

		public string fhz_病区
		{
			get
			{
				return this._fhz_bq;
			}
			set
			{
				this._fhz_bq = value;
			}
		}

		public string fhz_room
		{
			get
			{
				return this._fhz_room;
			}
			set
			{
				this._fhz_room = value;
			}
		}

		public string fhz_床号
		{
			get
			{
				return this._fhz_bed;
			}
			set
			{
				this._fhz_bed = value;
			}
		}

		public string fhz_volk
		{
			get
			{
				return this._fhz_volk;
			}
			set
			{
				this._fhz_volk = value;
			}
		}

		public string fhz_hy
		{
			get
			{
				return this._fhz_hy;
			}
			set
			{
				this._fhz_hy = value;
			}
		}

		public string fhz_job
		{
			get
			{
				return this._fhz_job;
			}
			set
			{
				this._fhz_job = value;
			}
		}

		public string fhz_gms
		{
			get
			{
				return this._fhz_gms;
			}
			set
			{
				this._fhz_gms = value;
			}
		}

		public string fhz_add
		{
			get
			{
				return this._fhz_add;
			}
			set
			{
				this._fhz_add = value;
			}
		}

		public string fhz_tel
		{
			get
			{
				return this._fhz_tel;
			}
			set
			{
				this._fhz_tel = value;
			}
		}

		public string fapply_申请单ID
		{
			get
			{
				return this._fapply_id;
			}
			set
			{
				this._fapply_id = value;
			}
		}

		public string fapply_user_id
		{
			get
			{
				return this._fapply_user_id;
			}
			set
			{
				this._fapply_user_id = value;
			}
		}

		public string fapply_dept_id
		{
			get
			{
				return this._fapply_dept_id;
			}
			set
			{
				this._fapply_dept_id = value;
			}
		}

		public string fapply_申请时间
		{
			get
			{
				return this._fapply_time;
			}
			set
			{
				this._fapply_time = value;
			}
		}

		public string fsampling_user_id
		{
			get
			{
				return this._fsampling_user_id;
			}
			set
			{
				this._fsampling_user_id = value;
			}
		}

		public string fsampling_采样时间
		{
			get
			{
				return this._fsampling_time;
			}
			set
			{
				this._fsampling_time = value;
			}
		}

		public string fjy_user_id
		{
			get
			{
				return this._fjy_user_id;
			}
			set
			{
				this._fjy_user_id = value;
			}
		}

		public string fchenk_审核医师ID
		{
			get
			{
				return this._fchenk_user_id;
			}
			set
			{
				this._fchenk_user_id = value;
			}
		}

		public string fcheck_审核时间
		{
			get
			{
				return this._fcheck_time;
			}
			set
			{
				this._fcheck_time = value;
			}
		}

		public string freport_报告时间
		{
			get
			{
				return this._freport_time;
			}
			set
			{
				this._freport_time = value;
			}
		}

		public string fprint_time
		{
			get
			{
				return this._fprint_time;
			}
			set
			{
				this._fprint_time = value;
			}
		}

		public string fprint_zt
		{
			get
			{
				return this._fprint_zt;
			}
			set
			{
				this._fprint_zt = value;
			}
		}

		public int fprint_count
		{
			get
			{
				return this._fprint_count;
			}
			set
			{
				this._fprint_count = value;
			}
		}

		public string fsys_用户ID
		{
			get
			{
				return this._fsys_user;
			}
			set
			{
				this._fsys_user = value;
			}
		}

		public string fsys_创建时间
		{
			get
			{
				return this._fsys_time;
			}
			set
			{
				this._fsys_time = value;
			}
		}

		public string fsys_部门ID
		{
			get
			{
				return this._fsys_dept;
			}
			set
			{
				this._fsys_dept = value;
			}
		}

		public string fremark_备注
		{
			get
			{
				return this._fremark;
			}
			set
			{
				this._fremark = value;
			}
		}

		public string f1
		{
			get
			{
				return this._f1;
			}
			set
			{
				this._f1 = value;
			}
		}

		public string f2
		{
			get
			{
				return this._f2;
			}
			set
			{
				this._f2 = value;
			}
		}

		public string f3
		{
			get
			{
				return this._f3;
			}
			set
			{
				this._f3 = value;
			}
		}

		public string f4
		{
			get
			{
				return this._f4;
			}
			set
			{
				this._f4 = value;
			}
		}

		public string f5
		{
			get
			{
				return this._f5;
			}
			set
			{
				this._f5 = value;
			}
		}

		public string f6
		{
			get
			{
				return this._f6;
			}
			set
			{
				this._f6 = value;
			}
		}

		public string f7
		{
			get
			{
				return this._f7;
			}
			set
			{
				this._f7 = value;
			}
		}

		public string f8
		{
			get
			{
				return this._f8;
			}
			set
			{
				this._f8 = value;
			}
		}

		public string f9
		{
			get
			{
				return this._f9;
			}
			set
			{
				this._f9 = value;
			}
		}

		public string f10
		{
			get
			{
				return this._f10;
			}
			set
			{
				this._f10 = value;
			}
		}

		public string fhz_dept患者科室
		{
			get
			{
				return this._fhz_dept;
			}
			set
			{
				this._fhz_dept = value;
			}
		}
	}
}
