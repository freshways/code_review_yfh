using System;
using System.Collections;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using ww.wwf.com;
using ww.wwf.wwfbll;

namespace ww.lis.lisbll.sam
{
	public class JYBLL以前的 : DAOWWF
	{
		private string table_jy = "";

		private string table_jy_result = "";

		private string table_jy_img = "";

		private UserdefinedBLL bllField = new UserdefinedBLL();

		public static string strSqlOK = "";

		private int sam_jy_fsample_code = 4;

		private NumBLL bllNum = new NumBLL();

		public JYBLL以前的()
		{
			this.table_jy = "sam_jy";
			this.table_jy_result = "sam_jy_result";
			this.table_jy_img = "sam_jy_img";
			this.sam_jy_fsample_code = this.bllNum.BllGetflengthByid("sam_jy_fsample_code");
		}

		public DataTable BllJYdt(string strWhere)
		{
			DataTable result;
			if (strWhere == "" || strWhere == null)
			{
				result = null;
			}
			else
			{
				strWhere = " Where " + strWhere + " order by fsample_code";
				result = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, "Select * FROM " + this.table_jy + " as t " + strWhere).Tables[0];
			}
			return result;
		}

		public int BllJYAdd(DataRow reportDow)
		{
			int result;
			if (this.BllJYExists(reportDow["finstr_id"].ToString(), reportDow["fjy_date"].ToString(), reportDow["fsample_code"].ToString()))
			{
				result = 0;
			}
			else
			{
				int num = 0;
				if (reportDow["fage"] == null)
				{
					num = 0;
				}
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append("insert into " + this.table_jy + "(");
				stringBuilder.Append("fjy_id,finstr_id,fjy_date,fsample_code,fprint_flag,fexamine_flag,fstate,fjygroup_id,fjytype_id,fsample_type_id,fhz_type_id,fhz_id,fsex,fname,fage,fage_unit,fage_year,fage_month,fage_day,fward_num,froom_num,fbed_num,fblood_abo,fblood_rh,fjymd,fdiagnose,fzyh,fheat,fxyld,fcyy,fxhdb,fapply_code,fapply_user_id,fapply_dept_id,fapply_time,fsampling_user_id,fsampling_time,fget_user_id,fget_user_time,fsend_user_id,fsend_user_time,fcheck_user_id,fcheck_time,fexamine_user_id,fexamine_time,fhandling_user_id,fhandling_user_time,fprint_time,fprint_count,freport_dept_id,forder_by,fcharge_flag,fjyf,fcharge_id,fcreate_user_id,fcreate_time,fupdate_user_id,fupdate_time,fremark");
				stringBuilder.Append(")");
				stringBuilder.Append(" values (");
				stringBuilder.Append("'" + reportDow["fjy_id"] + "',");
				stringBuilder.Append("'" + reportDow["finstr_id"] + "',");
				stringBuilder.Append("'" + reportDow["fjy_date"] + "',");
				stringBuilder.Append("'" + reportDow["fsample_code"] + "',");
				stringBuilder.Append(reportDow["fprint_flag"] + ",");
				stringBuilder.Append(reportDow["fexamine_flag"] + ",");
				stringBuilder.Append("'" + reportDow["fstate"] + "',");
				stringBuilder.Append("'" + reportDow["fjygroup_id"] + "',");
				stringBuilder.Append("'" + reportDow["fjytype_id"] + "',");
				stringBuilder.Append("'" + reportDow["fsample_type_id"] + "',");
				stringBuilder.Append("'" + reportDow["fhz_type_id"] + "',");
				stringBuilder.Append("'" + reportDow["fhz_id"] + "',");
				stringBuilder.Append("'" + reportDow["fsex"] + "',");
				stringBuilder.Append("'" + reportDow["fname"] + "',");
				stringBuilder.Append(num + ",");
				stringBuilder.Append("'" + reportDow["fage_unit"] + "',");
				stringBuilder.Append(reportDow["fage_year"] + ",");
				stringBuilder.Append(reportDow["fage_month"] + ",");
				stringBuilder.Append(reportDow["fage_day"] + ",");
				stringBuilder.Append("'" + reportDow["fward_num"] + "',");
				stringBuilder.Append("'" + reportDow["froom_num"] + "',");
				stringBuilder.Append("'" + reportDow["fbed_num"] + "',");
				stringBuilder.Append("'" + reportDow["fblood_abo"] + "',");
				stringBuilder.Append("'" + reportDow["fblood_rh"] + "',");
				stringBuilder.Append("'" + reportDow["fjymd"] + "',");
				stringBuilder.Append("'" + reportDow["fdiagnose"] + "',");
				stringBuilder.Append("'" + reportDow["fzyh"] + "',");
				stringBuilder.Append(reportDow["fheat"] + ",");
				stringBuilder.Append(reportDow["fxyld"] + ",");
				stringBuilder.Append("'" + reportDow["fcyy"] + "',");
				stringBuilder.Append(reportDow["fxhdb"] + ",");
				stringBuilder.Append("'" + reportDow["fapply_code"] + "',");
				stringBuilder.Append("'" + reportDow["fapply_user_id"] + "',");
				stringBuilder.Append("'" + reportDow["fapply_dept_id"] + "',");
				stringBuilder.Append("'" + reportDow["fapply_time"] + "',");
				stringBuilder.Append("'" + reportDow["fsampling_user_id"] + "',");
				stringBuilder.Append("'" + reportDow["fsampling_time"] + "',");
				stringBuilder.Append("'" + reportDow["fget_user_id"] + "',");
				stringBuilder.Append("'" + reportDow["fget_user_time"] + "',");
				stringBuilder.Append("'" + reportDow["fsend_user_id"] + "',");
				stringBuilder.Append("'" + reportDow["fsend_user_time"] + "',");
				stringBuilder.Append("'" + reportDow["fcheck_user_id"] + "',");
				stringBuilder.Append("'" + reportDow["fcheck_time"] + "',");
				stringBuilder.Append("'" + reportDow["fexamine_user_id"] + "',");
				stringBuilder.Append("'" + reportDow["fexamine_time"] + "',");
				stringBuilder.Append("'" + reportDow["fhandling_user_id"] + "',");
				stringBuilder.Append("'" + reportDow["fhandling_user_time"] + "',");
				stringBuilder.Append("'" + reportDow["fprint_time"] + "',");
				stringBuilder.Append(reportDow["fprint_count"] + ",");
				stringBuilder.Append("'" + reportDow["freport_dept_id"] + "',");
				stringBuilder.Append("'" + reportDow["forder_by"] + "',");
				stringBuilder.Append(reportDow["fcharge_flag"] + ",");
				stringBuilder.Append(reportDow["fjyf"] + ",");
				stringBuilder.Append("'" + reportDow["fcharge_id"] + "',");
				stringBuilder.Append("'" + reportDow["fcreate_user_id"] + "',");
				stringBuilder.Append("'" + reportDow["fcreate_time"] + "',");
				stringBuilder.Append("'" + reportDow["fupdate_user_id"] + "',");
				stringBuilder.Append("'" + reportDow["fupdate_time"] + "',");
				stringBuilder.Append("'" + reportDow["fremark"] + "'");
				stringBuilder.Append(")");
				result = WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
			}
			return result;
		}

		public int BllJYUpdateRow(DataRowView reportDow)
		{
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, this.BllJYUpdateSql(reportDow));
		}

		public string BllJYUpdateSql(DataRowView reportDow)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update " + this.table_jy + " set ");
			stringBuilder.Append("finstr_id='" + reportDow["finstr_id"] + "',");
			stringBuilder.Append("fjy_date='" + reportDow["fjy_date"] + "',");
			stringBuilder.Append("fsample_code='" + reportDow["fsample_code"] + "',");
			stringBuilder.Append("fprint_flag=" + reportDow["fprint_flag"] + ",");
			stringBuilder.Append("fexamine_flag=" + reportDow["fexamine_flag"] + ",");
			stringBuilder.Append("fstate='" + reportDow["fstate"] + "',");
			stringBuilder.Append("fjygroup_id='" + reportDow["fjygroup_id"] + "',");
			stringBuilder.Append("fjytype_id='" + reportDow["fjytype_id"] + "',");
			stringBuilder.Append("fsample_type_id='" + reportDow["fsample_type_id"] + "',");
			stringBuilder.Append("fhz_type_id='" + reportDow["fhz_type_id"] + "',");
			stringBuilder.Append("fhz_id='" + reportDow["fhz_id"] + "',");
			stringBuilder.Append("fsex='" + reportDow["fsex"] + "',");
			stringBuilder.Append("fname='" + reportDow["fname"] + "',");
			stringBuilder.Append("fage=" + reportDow["fage"] + ",");
			stringBuilder.Append("fage_unit='" + reportDow["fage_unit"] + "',");
			stringBuilder.Append("fage_year=" + reportDow["fage_year"] + ",");
			stringBuilder.Append("fage_month=" + reportDow["fage_month"] + ",");
			stringBuilder.Append("fage_day=" + reportDow["fage_day"] + ",");
			stringBuilder.Append("fward_num='" + reportDow["fward_num"] + "',");
			stringBuilder.Append("froom_num='" + reportDow["froom_num"] + "',");
			stringBuilder.Append("fbed_num='" + reportDow["fbed_num"] + "',");
			stringBuilder.Append("fblood_abo='" + reportDow["fblood_abo"] + "',");
			stringBuilder.Append("fblood_rh='" + reportDow["fblood_rh"] + "',");
			stringBuilder.Append("fjymd='" + reportDow["fjymd"] + "',");
			stringBuilder.Append("fdiagnose='" + reportDow["fdiagnose"] + "',");
			stringBuilder.Append("fzyh='" + reportDow["fzyh"] + "',");
			stringBuilder.Append("fheat=" + reportDow["fheat"] + ",");
			stringBuilder.Append("fxyld=" + reportDow["fxyld"] + ",");
			stringBuilder.Append("fcyy='" + reportDow["fcyy"] + "',");
			stringBuilder.Append("fxhdb=" + reportDow["fxhdb"] + ",");
			stringBuilder.Append("fapply_code='" + reportDow["fapply_code"] + "',");
			stringBuilder.Append("fapply_user_id='" + reportDow["fapply_user_id"] + "',");
			stringBuilder.Append("fapply_dept_id='" + reportDow["fapply_dept_id"] + "',");
			stringBuilder.Append("fapply_time='" + reportDow["fapply_time"] + "',");
			stringBuilder.Append("fsampling_user_id='" + reportDow["fsampling_user_id"] + "',");
			stringBuilder.Append("fsampling_time='" + reportDow["fsampling_time"] + "',");
			stringBuilder.Append("fget_user_id='" + reportDow["fget_user_id"] + "',");
			stringBuilder.Append("fget_user_time='" + reportDow["fget_user_time"] + "',");
			stringBuilder.Append("fsend_user_id='" + reportDow["fsend_user_id"] + "',");
			stringBuilder.Append("fsend_user_time='" + reportDow["fsend_user_time"] + "',");
			stringBuilder.Append("fcheck_user_id='" + reportDow["fcheck_user_id"] + "',");
			stringBuilder.Append("fcheck_time='" + reportDow["fcheck_time"] + "',");
			stringBuilder.Append("fexamine_user_id='" + reportDow["fexamine_user_id"] + "',");
			stringBuilder.Append("fexamine_time='" + reportDow["fexamine_time"] + "',");
			stringBuilder.Append("fhandling_user_id='" + reportDow["fhandling_user_id"] + "',");
			stringBuilder.Append("fhandling_user_time='" + reportDow["fhandling_user_time"] + "',");
			stringBuilder.Append("fprint_time='" + reportDow["fprint_time"] + "',");
			stringBuilder.Append("fprint_count=" + reportDow["fprint_count"] + ",");
			stringBuilder.Append("freport_dept_id='" + reportDow["freport_dept_id"] + "',");
			stringBuilder.Append("forder_by='" + reportDow["forder_by"] + "',");
			stringBuilder.Append("fcharge_flag=" + reportDow["fcharge_flag"] + ",");
			stringBuilder.Append("fjyf=" + reportDow["fjyf"] + ",");
			stringBuilder.Append("fcharge_id='" + reportDow["fcharge_id"] + "',");
			stringBuilder.Append("fcreate_user_id='" + reportDow["fcreate_user_id"] + "',");
			stringBuilder.Append("fcreate_time='" + reportDow["fcreate_time"] + "',");
			stringBuilder.Append("fupdate_user_id='" + reportDow["fupdate_user_id"] + "',");
			stringBuilder.Append("fupdate_time='" + reportDow["fupdate_time"] + "',");
			stringBuilder.Append("fremark='" + reportDow["fremark"] + "'");
			stringBuilder.Append(" where fjy_id='" + reportDow["fjy_id"] + "'");
			return stringBuilder.ToString();
		}

		public int BllJYUpdatePrintTime(string fjy_id)
		{
			string str = base.DbServerDateTim();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update " + this.table_jy + " set ");
			stringBuilder.Append("fprint_time='" + str + "',");
			stringBuilder.Append("fprint_count=fprint_count+1,");
			stringBuilder.Append("fprint_flag=1,");
			stringBuilder.Append("fupdate_user_id='" + LoginBLL.strPersonID + "',");
			stringBuilder.Append("fupdate_time='" + str + "'");
			stringBuilder.Append(" where fjy_id='" + fjy_id + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public string BllJYUpdateAndResultRow(DataRowView reportDow, DataTable dtResult)
		{
			IList list = new ArrayList();
			list.Add(this.BllJYUpdateSql(reportDow));
			for (int i = 0; i < dtResult.Rows.Count; i++)
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append("update " + this.table_jy_result + " set ");
				stringBuilder.Append("fjy_id='" + dtResult.Rows[i]["fjy_id"] + "',");
				stringBuilder.Append("fitem_id='" + dtResult.Rows[i]["fitem_id"] + "',");
				stringBuilder.Append("forder_by='" + dtResult.Rows[i]["forder_by"] + "',");
				stringBuilder.Append("fvalue='" + dtResult.Rows[i]["fvalue"] + "',");
				stringBuilder.Append("fod='" + dtResult.Rows[i]["fod"] + "',");
				stringBuilder.Append("fcutoff='" + dtResult.Rows[i]["fcutoff"] + "',");
				stringBuilder.Append("fremark='" + dtResult.Rows[i]["fremark"] + "'");
				stringBuilder.Append(" where fresult_id='" + dtResult.Rows[i]["fresult_id"] + "'");
				list.Add(stringBuilder.ToString());
			}
			return WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, list);
		}

		public int BllJYExamine(string strid, int fexamine_flag)
		{
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, string.Concat(new object[]
			{
				"Update ",
				this.table_jy,
				" set fexamine_flag=",
				fexamine_flag,
				",fstate=",
				fexamine_flag,
				" where fjy_id='",
				strid,
				"'"
			}));
		}

		public string BllGetfsampleCodeUpOrDown(string fsample_code, string upORdown)
		{
			if (!(fsample_code == "") && fsample_code != null)
			{
				this.sam_jy_fsample_code = fsample_code.Length;
			}
			string str;
			switch (this.sam_jy_fsample_code)
			{
			case 2:
				str = "0";
				break;
			case 3:
				str = "00";
				break;
			case 4:
				str = "000";
				break;
			case 5:
				str = "0000";
				break;
			case 6:
				str = "00000";
				break;
			case 7:
				str = "000000";
				break;
			case 8:
				str = "0000000";
				break;
			case 9:
				str = "00000000";
				break;
			case 10:
				str = "000000000";
				break;
			default:
				str = "";
				break;
			}
			if (fsample_code == "" || fsample_code == null || fsample_code.Length == 0)
			{
				if (upORdown == "up")
				{
					fsample_code = str + "2";
				}
				else
				{
					fsample_code = str + "0";
				}
			}
			int length = fsample_code.Length;
			int num = 1;
			if (upORdown == "up")
			{
				if (Public.IsNumber(fsample_code))
				{
					if (Convert.ToInt32(fsample_code) > 1)
					{
						num = Convert.ToInt32(fsample_code) - 1;
					}
				}
			}
			if (upORdown == "down")
			{
				if (Public.IsNumber(fsample_code))
				{
					num = Convert.ToInt32(fsample_code) + 1;
				}
			}
			int length2 = num.ToString().Length;
			string result;
			switch (length - length2)
			{
			case 1:
				result = "0" + num.ToString();
				break;
			case 2:
				result = "00" + num.ToString();
				break;
			case 3:
				result = "000" + num.ToString();
				break;
			case 4:
				result = "0000" + num.ToString();
				break;
			case 5:
				result = "00000" + num.ToString();
				break;
			case 6:
				result = "000000" + num.ToString();
				break;
			case 7:
				result = "0000000" + num.ToString();
				break;
			case 8:
				result = "00000000" + num.ToString();
				break;
			case 9:
				result = "000000000" + num.ToString();
				break;
			case 10:
				result = "0000000000" + num.ToString();
				break;
			default:
				result = num.ToString();
				break;
			}
			return result;
		}

		public bool BllJYExists(string finstr_id, string fjy_date, string fsample_code)
		{
			string sql = string.Concat(new string[]
			{
				"SELECT count(1) FROM ",
				this.table_jy,
				" WHERE (finstr_id = '",
				finstr_id,
				"') AND (fjy_date = '",
				fjy_date,
				"') AND (fsample_code = '",
				fsample_code,
				"')"
			});
			return WWFInit.wwfRemotingDao.DbRecordExists(WWFInit.strDBConn, sql);
		}

		public bool BllJYDel(string fjy_id)
		{
			string strSql = string.Concat(new string[]
			{
				"delete from ",
				this.table_jy,
				" where fjy_id='",
				fjy_id,
				"'"
			});
			string strSql2 = string.Concat(new string[]
			{
				"delete from ",
				this.table_jy_result,
				" where fjy_id='",
				fjy_id,
				"'"
			});
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryTransaction2(WWFInit.strDBConn, strSql, strSql2);
		}

		public DataTable BllResultDT(string fjy_id)
		{
			string strSql = string.Concat(new string[]
			{
				"SELECT * FROM ",
				this.table_jy_result,
				" where fjy_id = '",
				fjy_id,
				"'"
			});
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public int BllResultAdd(DataRow drResult)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into " + this.table_jy_result + "(");
			stringBuilder.Append("fresult_id,fjy_id,fitem_id,forder_by,fvalue,fod,fcutoff,fremark");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + base.DbGuid() + "',");
			stringBuilder.Append("'" + drResult["fjy_id"] + "',");
			stringBuilder.Append("'" + drResult["fitem_id"] + "',");
			stringBuilder.Append("'" + drResult["forder_by"] + "',");
			stringBuilder.Append("'" + drResult["fvalue"] + "',");
			stringBuilder.Append("'" + drResult["fod"] + "',");
			stringBuilder.Append("'" + drResult["fcutoff"] + "',");
			stringBuilder.Append("'" + drResult["fremark"] + "'");
			stringBuilder.Append(")");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public string BllResultUpdate(DataTable dtResult)
		{
			IList list = new ArrayList();
			for (int i = 0; i < dtResult.Rows.Count; i++)
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append("update " + this.table_jy_result + " set ");
				stringBuilder.Append("fjy_id='" + dtResult.Rows[i]["fjy_id"] + "',");
				stringBuilder.Append("fitem_id='" + dtResult.Rows[i]["fitem_id"] + "',");
				stringBuilder.Append("forder_by='" + dtResult.Rows[i]["forder_by"] + "',");
				stringBuilder.Append("fvalue='" + dtResult.Rows[i]["fvalue"] + "',");
				stringBuilder.Append("fod='" + dtResult.Rows[i]["fod"] + "',");
				stringBuilder.Append("fcutoff='" + dtResult.Rows[i]["fcutoff"] + "',");
				stringBuilder.Append("fremark='" + dtResult.Rows[i]["fremark"] + "'");
				stringBuilder.Append(" where fresult_id='" + dtResult.Rows[i]["fresult_id"] + "'");
				list.Add(stringBuilder.ToString());
			}
			return WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, list);
		}

		public int BllResultDel(string fresult_id)
		{
			string strSql = string.Concat(new string[]
			{
				"delete from ",
				this.table_jy_result,
				" where fresult_id='",
				fresult_id,
				"'"
			});
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, strSql);
		}

		public DataTable BllItemDT(string fitem_id)
		{
			string strSql = "SELECT (SELECT fname FROM SAM_TYPE t1 WHERE (ftype = '常用单位') AND (fcode = t.funit_name)) as funitname,* FROM SAM_ITEM t WHERE (fitem_id = '" + fitem_id + "')";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public DataTable BllItemRefDT(string fitem_id)
		{
			string strSql = "SELECT * FROM SAM_ITEM_REF WHERE (fitem_id = '" + fitem_id + "')";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public string BllItemfref(DataRow drItem, DataRowView drvReport)
		{
			string fitem_id = drItem["fitem_id"].ToString();
			string text = drItem["fname"].ToString();
			string a = drItem["fref_if_age"].ToString();
			string a2 = drItem["fref_if_sex"].ToString();
			string a3 = drItem["fref_if_sample"].ToString();
			string a4 = drItem["fref_if_method"].ToString();
			string result = drItem["fref"].ToString();
			if (!(a == "0" & a2 == "0" & a3 == "0" & a4 == "0"))
			{
				DataTable dataTable = this.BllItemRefDT(fitem_id);
				string text2 = "0";
				string text3 = "0";
				string text4 = "";
				if (drvReport["fage"] != null)
				{
					text2 = drvReport["fage"].ToString();
				}
				if (drvReport["fsex"] != null)
				{
					text3 = drvReport["fsex"].ToString();
				}
				if (drvReport["fsample_type_id"] != null)
				{
					text4 = drvReport["fsample_type_id"].ToString();
				}
				string filterExpression = "";
				if (a == "1" & a2 == "0" & a3 == "0")
				{
					filterExpression = string.Concat(new string[]
					{
						"(",
						text2,
						">=fage_low) AND (",
						text2,
						"<fage_high)"
					});
				}
				if (a == "1" & a2 == "1" & a3 == "0")
				{
					filterExpression = string.Concat(new string[]
					{
						"('",
						text3,
						"'=fsex) AND (",
						text2,
						">=fage_low) AND (",
						text2,
						"<fage_high)"
					});
				}
				if (a == "1" & a2 == "1" & a3 == "1")
				{
					filterExpression = string.Concat(new string[]
					{
						"('",
						text4,
						"'=fsample_type_id) AND ('",
						text3,
						"'=fsex) AND (",
						text2,
						">=fage_low) AND (",
						text2,
						"<fage_high)"
					});
				}
				if (a == "0" & a2 == "1" & a3 == "0")
				{
					filterExpression = "('" + text3 + "'=fsex)";
				}
				if (a == "0" & a2 == "1" & a3 == "1")
				{
					filterExpression = string.Concat(new string[]
					{
						"('",
						text4,
						"'=fsample_type_id) AND ('",
						text3,
						"'=fsex)"
					});
				}
				if (a == "0" & a2 == "0" & a3 == "1")
				{
					filterExpression = "('" + text4 + "'=fsample_type_id)";
				}
				if (a == "0" & a2 == "1" & a3 == "1")
				{
					filterExpression = string.Concat(new string[]
					{
						"('",
						text4,
						"'=fsample_type_id) AND ('",
						text3,
						"'=fsex)"
					});
				}
				DataRow[] array = dataTable.Select(filterExpression);
				if (array.Length > 0)
				{
					DataRow[] array2 = array;
					for (int i = 0; i < array2.Length; i++)
					{
						DataRow dataRow = array2[i];
						result = dataRow["fref"].ToString();
					}
				}
			}
			return result;
		}

		public int BllItemValueBJ(string fvalue, string fref)
		{
			int result = 0;
			string[] array = Regex.Split(fref, "--");
			if (Public.IsNumber(fvalue))
			{
				double num = Convert.ToDouble(fvalue);
				if (array.Length == 2)
				{
					if (Public.IsNumber(array[0].ToString()) & Public.IsNumber(array[1].ToString()))
					{
						if (num < Convert.ToDouble(array[0].ToString()))
						{
							result = 1;
						}
						if (num > Convert.ToDouble(array[1].ToString()))
						{
							result = 2;
						}
					}
				}
			}
			return result;
		}
	}
}
