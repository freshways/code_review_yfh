using System;

namespace ww.lis.lisbll.sam
{
	public class jyresultmodel
	{
		private string _fresult_id;

		private string _fjy_id;

		private string _fapply_id;

		private string _fitem_id;

		private string _fitem_code;

		private string _fitem_name;

		private string _fitem_unit;

		private string _fitem_ref;

		private string _fitem_badge;

		private string _forder_by;

		private string _fvalue;

		private string _fod;

		private string _fcutoff;

		private string _fremark;

		public string fresult_id
		{
			get
			{
				return this._fresult_id;
			}
			set
			{
				this._fresult_id = value;
			}
		}

		public string fjy_id
		{
			get
			{
				return this._fjy_id;
			}
			set
			{
				this._fjy_id = value;
			}
		}

		public string fapply_id
		{
			get
			{
				return this._fapply_id;
			}
			set
			{
				this._fapply_id = value;
			}
		}

		public string fitem_id
		{
			get
			{
				return this._fitem_id;
			}
			set
			{
				this._fitem_id = value;
			}
		}

		public string fitem_code
		{
			get
			{
				return this._fitem_code;
			}
			set
			{
				this._fitem_code = value;
			}
		}

		public string fitem_name
		{
			get
			{
				return this._fitem_name;
			}
			set
			{
				this._fitem_name = value;
			}
		}

		public string fitem_unit
		{
			get
			{
				return this._fitem_unit;
			}
			set
			{
				this._fitem_unit = value;
			}
		}

		public string fitem_ref
		{
			get
			{
				return this._fitem_ref;
			}
			set
			{
				this._fitem_ref = value;
			}
		}

		public string fitem_badge
		{
			get
			{
				return this._fitem_badge;
			}
			set
			{
				this._fitem_badge = value;
			}
		}

		public string forder_by
		{
			get
			{
				return this._forder_by;
			}
			set
			{
				this._forder_by = value;
			}
		}

		public string fvalue
		{
			get
			{
				return this._fvalue;
			}
			set
			{
				this._fvalue = value;
			}
		}

		public string fod
		{
			get
			{
				return this._fod;
			}
			set
			{
				this._fod = value;
			}
		}

		public string fcutoff
		{
			get
			{
				return this._fcutoff;
			}
			set
			{
				this._fcutoff = value;
			}
		}

		public string fremark
		{
			get
			{
				return this._fremark;
			}
			set
			{
				this._fremark = value;
			}
		}
	}
}
