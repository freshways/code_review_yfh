using System;
using System.Collections;
using System.Data;
using System.Text;
using ww.wwf.wwfbll;

namespace ww.lis.lisbll.sam
{
	public class SampleTypeBLL : DAOWWF
	{
		public DataTable BllDTByCode(string strfcode, string strfname)
		{
			string strSql = string.Concat(new string[]
			{
				"SELECT * FROM SAM_SAMPLE_TYPE where  (fsample_type_id='",
				strfcode,
				"' or fname ='",
				strfname,
				"')"
			});
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public DataTable BllDT(string strWhere)
		{
			string strSql = "SELECT * FROM sam_sample_type " + strWhere + " ORDER BY forder_by";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public string BllSampleNameByID(string strfsample_type_id)
		{
			return (string)WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(WWFInit.strDBConn, "SELECT fname FROM SAM_SAMPLE_TYPE WHERE (fsample_type_id = '" + strfsample_type_id + "')");
		}

		public string BllPersonfhelp_codeUpdate(IList lisSql)
		{
			return WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, lisSql);
		}

		public int BllAdd(DataRow drRow)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into sam_sample_type(");
			stringBuilder.Append("fsample_type_id,fp_id,fcode,fname,fname_e,fuse_if,forder_by,fremark,fhelp_code");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + drRow["fsample_type_id"] + "',");
			stringBuilder.Append("'" + drRow["fp_id"] + "',");
			stringBuilder.Append("'" + drRow["fcode"] + "',");
			stringBuilder.Append("'" + drRow["fname"] + "',");
			stringBuilder.Append("'" + drRow["fname_e"] + "',");
			stringBuilder.Append(drRow["fuse_if"] + ",");
			stringBuilder.Append("'" + drRow["forder_by"] + "',");
			stringBuilder.Append("'" + drRow["fremark"] + "',");
			stringBuilder.Append("'" + drRow["fhelp_code"] + "'");
			stringBuilder.Append(")");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllUpdate(DataRow drRow)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update sam_sample_type set ");
			stringBuilder.Append("fp_id='" + drRow["fp_id"] + "',");
			stringBuilder.Append("fcode='" + drRow["fcode"] + "',");
			stringBuilder.Append("fname='" + drRow["fname"] + "',");
			stringBuilder.Append("fname_e='" + drRow["fname_e"] + "',");
			stringBuilder.Append("fuse_if=" + drRow["fuse_if"] + ",");
			stringBuilder.Append("forder_by='" + drRow["forder_by"] + "',");
			stringBuilder.Append("fremark='" + drRow["fremark"] + "',");
			stringBuilder.Append("fhelp_code='" + drRow["fhelp_code"] + "'");
			stringBuilder.Append(" where fsample_type_id='" + drRow["fsample_type_id"] + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}
	}
}
