using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using ww.wwf.com;
using ww.wwf.wwfbll;

namespace ww.lis.lisbll.sam
{
	public class ApplyBLL : DAOWWF
	{
		private string table_SAM_APPLY = "";

		private string table_SAM_JY_RESULT = "";

		private string table_SAM_SAMPLE = "";

		public ApplyBLL()
		{
			this.table_SAM_APPLY = "SAM_APPLY";
			this.table_SAM_JY_RESULT = "SAM_JY_RESULT";
			this.table_SAM_SAMPLE = "SAM_SAMPLE";
		}

		public string BllApplyAdd(ApplyModel model, DataTable dtResult, string fitem_group)
		{
			IList list = new ArrayList();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into " + this.table_SAM_APPLY + "(");
			stringBuilder.Append("fapply_id,fjytype_id,fsample_type_id,fjz_flag,fcharge_flag,fcharge_id,fstate,fjyf,fapply_user_id,fapply_dept_id,fapply_time,ftype_id,fhz_id,fhz_zyh,fsex,fname,fvolk,fhymen,fage,fjob,fgms,fjob_org,fadd,ftel,fage_unit,fage_year,fage_month,fage_day,fward_num,froom_num,fbed_num,fblood_abo,fblood_rh,fjymd,fdiagnose,fheat,fxyld,fcyy,fxhdb,fcreate_user_id,fcreate_time,fupdate_user_id,fupdate_time,fitem_group,fremark");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + model.fapply_id + "',");
			stringBuilder.Append("'" + model.fjytype_id + "',");
			stringBuilder.Append("'" + model.fsample_type_id + "',");
			stringBuilder.Append(model.fjz_flag + ",");
			stringBuilder.Append(model.fcharge_flag + ",");
			stringBuilder.Append("'" + model.fcharge_id + "',");
			stringBuilder.Append("'" + model.fstate + "',");
			stringBuilder.Append(model.fjyf + ",");
			stringBuilder.Append("'" + model.fapply_user_id + "',");
			stringBuilder.Append("'" + model.fapply_dept_id + "',");
			stringBuilder.Append("'" + model.fapply_time + "',");
			stringBuilder.Append("'" + model.ftype_id + "',");
			stringBuilder.Append("'" + model.fhz_id + "',");
			stringBuilder.Append("'" + model.fhz_zyh + "',");
			stringBuilder.Append("'" + model.fsex + "',");
			stringBuilder.Append("'" + model.fname + "',");
			stringBuilder.Append("'" + model.fvolk + "',");
			stringBuilder.Append("'" + model.fhymen + "',");
			stringBuilder.Append(model.fage + ",");
			stringBuilder.Append("'" + model.fjob + "',");
			stringBuilder.Append("'" + model.fgms + "',");
			stringBuilder.Append("'" + model.fjob_org + "',");
			stringBuilder.Append("'" + model.fadd + "',");
			stringBuilder.Append("'" + model.ftel + "',");
			stringBuilder.Append("'" + model.fage_unit + "',");
			stringBuilder.Append(model.fage_year + ",");
			stringBuilder.Append(model.fage_month + ",");
			stringBuilder.Append(model.fage_day + ",");
			stringBuilder.Append("'" + model.fward_num + "',");
			stringBuilder.Append("'" + model.froom_num + "',");
			stringBuilder.Append("'" + model.fbed_num + "',");
			stringBuilder.Append("'" + model.fblood_abo + "',");
			stringBuilder.Append("'" + model.fblood_rh + "',");
			stringBuilder.Append("'" + model.fjymd + "',");
			stringBuilder.Append("'" + model.fdiagnose + "',");
			stringBuilder.Append(model.fheat + ",");
			stringBuilder.Append(model.fxyld + ",");
			stringBuilder.Append("'" + model.fcyy + "',");
			stringBuilder.Append(model.fxhdb + ",");
			stringBuilder.Append("'" + model.fcreate_user_id + "',");
			stringBuilder.Append("'" + model.fcreate_time + "',");
			stringBuilder.Append("'" + model.fupdate_user_id + "',");
			stringBuilder.Append("'" + model.fupdate_time + "',");
			stringBuilder.Append("'" + fitem_group + "',");
			stringBuilder.Append("'" + model.fremark + "'");
			stringBuilder.Append(")");
			list.Add(stringBuilder.ToString());
			StringBuilder stringBuilder2 = new StringBuilder();
			stringBuilder2.Append("insert into " + this.table_SAM_SAMPLE + "(");
			stringBuilder2.Append("fsample_id,fapply_id,fsample_barcode,fexamine_flag,fprint_flag,fprint_count,fread_flag,fstate");
			stringBuilder2.Append(")");
			stringBuilder2.Append(" values (");
			stringBuilder2.Append("'" + base.DbGuid() + "',");
			stringBuilder2.Append("'" + model.fapply_id + "',");
			stringBuilder2.Append("'" + model.fapply_id + "',");
			stringBuilder2.Append("0,");
			stringBuilder2.Append("0,");
			stringBuilder2.Append("0,");
			stringBuilder2.Append("0,");
			stringBuilder2.Append("'" + model.fstate + "'");
			stringBuilder2.Append(")");
			list.Add(stringBuilder2.ToString());
			for (int i = 0; i < dtResult.Rows.Count; i++)
			{
				StringBuilder stringBuilder3 = new StringBuilder();
				stringBuilder3.Append("insert into " + this.table_SAM_JY_RESULT + "(");
				stringBuilder3.Append("fresult_id,fapply_id,fitem_id,fitem_code,fitem_name,fitem_unit,forder_by");
				stringBuilder3.Append(")");
				stringBuilder3.Append(" values (");
				stringBuilder3.Append("'" + base.DbGuid() + "',");
				stringBuilder3.Append("'" + model.fapply_id + "',");
				stringBuilder3.Append("'" + dtResult.Rows[i]["fitem_id"] + "',");
				stringBuilder3.Append("'" + dtResult.Rows[i]["fitem_code"] + "',");
				stringBuilder3.Append("'" + dtResult.Rows[i]["fitem_name"] + "',");
				stringBuilder3.Append("'" + dtResult.Rows[i]["fitem_unit"] + "',");
				stringBuilder3.Append("'" + dtResult.Rows[i]["forder_by"] + "'");
				stringBuilder3.Append(")");
				list.Add(stringBuilder3.ToString());
			}
			list.Add("DELETE FROM SAM_APPLY_BCODE WHERE  (fapply_id = '" + model.fapply_id + "')");
			return WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, list);
		}

		public string BllApplyAdd_New(ApplyModel model)
		{
			string empty = string.Empty;
			IList list = new ArrayList();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into " + this.table_SAM_APPLY + "(");
			stringBuilder.Append("fapply_id,fjytype_id,fsample_type_id,fjz_flag,fcharge_flag,fcharge_id,fstate,fjyf,fapply_user_id,fapply_dept_id,\r\nfapply_time,ftype_id,fhz_id,fhz_zyh,fsex,fname,fvolk,fhymen,fage,fjob,fgms,fjob_org,fadd,ftel,fage_unit,fage_year,fage_month,fage_day,fward_num,\r\nfroom_num,fbed_num,fblood_abo,fblood_rh,fjymd,fdiagnose,fheat,fxyld,fcyy,fxhdb,fcreate_user_id,fcreate_time,fupdate_user_id,fupdate_time,fitem_group,fremark,his_recordid,sfzh");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + model.fapply_id + "',");
			stringBuilder.Append("'" + model.fjytype_id + "',");
			stringBuilder.Append("'" + model.fsample_type_id + "',");
			stringBuilder.Append(model.fjz_flag + ",");
			stringBuilder.Append(model.fcharge_flag + ",");
			stringBuilder.Append("'" + model.fcharge_id + "',");
			stringBuilder.Append("'" + model.fstate + "',");
			stringBuilder.Append(model.fjyf + ",");
			stringBuilder.Append("'" + model.fapply_user_id + "',");
			stringBuilder.Append("'" + model.fapply_dept_id + "',");
			stringBuilder.Append("'" + model.fapply_time + "',");
			stringBuilder.Append("'" + model.ftype_id + "',");
			stringBuilder.Append("'" + model.fhz_id + "',");
			stringBuilder.Append("'" + model.fhz_zyh + "',");
			stringBuilder.Append("'" + model.fsex + "',");
			stringBuilder.Append("'" + model.fname + "',");
			stringBuilder.Append("'" + model.fvolk + "',");
			stringBuilder.Append("'" + model.fhymen + "',");
			stringBuilder.Append(model.fage + ",");
			stringBuilder.Append("'" + model.fjob + "',");
			stringBuilder.Append("'" + model.fgms + "',");
			stringBuilder.Append("'" + model.fjob_org + "',");
			stringBuilder.Append("'" + model.fadd + "',");
			stringBuilder.Append("'" + model.ftel + "',");
			stringBuilder.Append("'" + model.fage_unit + "',");
			stringBuilder.Append(model.fage_year + ",");
			stringBuilder.Append(model.fage_month + ",");
			stringBuilder.Append(model.fage_day + ",");
			stringBuilder.Append("'" + model.fward_num + "',");
			stringBuilder.Append("'" + model.froom_num + "',");
			stringBuilder.Append("'" + model.fbed_num + "',");
			stringBuilder.Append("'" + model.fblood_abo + "',");
			stringBuilder.Append("'" + model.fblood_rh + "',");
			stringBuilder.Append("'" + model.fjymd + "',");
			stringBuilder.Append("'" + model.fdiagnose + "',");
			stringBuilder.Append(model.fheat + ",");
			stringBuilder.Append(model.fxyld + ",");
			stringBuilder.Append("'" + model.fcyy + "',");
			stringBuilder.Append(model.fxhdb + ",");
			stringBuilder.Append("'" + model.fcreate_user_id + "',");
			stringBuilder.Append("convert(varchar(20), getdate(),20),");
			stringBuilder.Append("'" + model.fupdate_user_id + "',");
			stringBuilder.Append("convert(varchar(20), getdate(),20),");
			stringBuilder.Append("'" + model.F检验项目名称 + "',");
			stringBuilder.Append("'" + model.fremark + "',");
			stringBuilder.Append("'" + model.His申请单号 + "'");
			stringBuilder.Append(",'" + model.Sfzh + "'");
			stringBuilder.Append(")");
			list.Add(stringBuilder.ToString());
			StringBuilder stringBuilder2 = new StringBuilder();
			stringBuilder2.Append("insert into " + this.table_SAM_SAMPLE + "(");
			stringBuilder2.Append("fsample_id,fapply_id,fsample_barcode,fexamine_flag,fprint_flag,fprint_count,fread_flag,fstate");
			stringBuilder2.Append(")");
			stringBuilder2.Append(" values (");
			stringBuilder2.Append("'" + base.DbGuid() + "',");
			stringBuilder2.Append("'" + model.fapply_id + "',");
			stringBuilder2.Append("'" + model.fapply_id + "',");
			stringBuilder2.Append("0,");
			stringBuilder2.Append("0,");
			stringBuilder2.Append("0,");
			stringBuilder2.Append("0,");
			stringBuilder2.Append("'" + model.fstate + "'");
			stringBuilder2.Append(") ");
			list.Add(stringBuilder2.ToString());
			string text = WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, list);
			if (text == "true")
			{
				string strSql = string.Concat(new string[]
				{
					" \r\n                    exec dbo.[uSp_化验申请单状态更新] '打印','",
					model.fapply_id,
					"','",
					model.His申请单号,
					"'"
				});
				WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strCHisDBConn, strSql);
			}
			return text;
		}

		public int BllApplyBCodeAdd(string strfapply_id, byte[] imgfbcode)
		{
			return WWFInit.wwfRemotingDao.DbApplyBcodeAdd(WWFInit.strDBConn, "SAM_APPLY_BCODE_ADD", strfapply_id, imgfbcode);
		}

		public DataTable BllSamplingMainDT(string strWhere)
		{
			strWhere = " where " + strWhere + " order by t1.fhz_id";
			string strSql = string.Concat(new string[]
			{
				"SELECT t1.his_recordid,t1.fapply_id,t1.fjytype_id,t1.fsample_type_id,t1.fjz_flag, t1.fcharge_flag, t1.fcharge_id, t1.fjyf, t1.fapply_user_id, t1.fapply_dept_id, \r\n            t1.fapply_time, t1.ftype_id, t1.fhz_id, t1.fhz_zyh, t1.fsex, t1.fname, t1.fvolk, t1.fhymen, t1.fage, t1.fjob, t1.fgms, t1.fjob_org, t1.fadd,t1.ftel, t1.fage_unit, \r\n            t1.fage_year, t1.fage_month, t1.fage_day, t1.fward_num, t1.froom_num, t1.fbed_num, t1.fblood_abo, t1.fblood_rh, t1.fjymd, t1.fdiagnose, t1.fheat, \r\n            t1.fxyld, t1.fcyy, t1.fxhdb, t1.fcreate_user_id, t1.fcreate_time, t1.fupdate_user_id, t1.fupdate_time, t1.fremark,t1.fitem_group,t2.fread_flag, t2.fsample_id,  \r\n            t2.fsample_barcode, t2.fsample_code, t1.fstate, t2.fexamine_flag, t2.fsampling_user_id, t2.fsampling_time, t2.fsampling_remark, t2.fsend_user_id, \r\n            t2.fsend_user_time, t2.fget_user_id, t2.fget_user_time, t2.fget_user_remark, t2.fjy_instr, t2.fjy_date, t2.fjy_group, t2.fjy_user_id, t2.fjy_time, \r\n            t2.fexamine_user_id, t2.fexamine_time, t2.fhandling_user_id, t2.fhandling_user_time, t2.fhandling_remark, t2.fprint_flag, t2.fprint_time, t2.fprint_count, \r\n            t2.frelease_time,t2.fread_user_id,t2.fremark FROM ",
				this.table_SAM_APPLY,
				" t1 left JOIN  ",
				this.table_SAM_SAMPLE,
				" t2 ON t1.fapply_id = t2.fapply_id ",
				strWhere
			});
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public DataTable BllSamplingMainDTFromHis(string strWhere, string ChisDBName)
		{
			string text = "select isnull(t2.分类名称,'') 分类名称,t1.* from " + ChisDBName + ".dbo.vw_lis_request_inp t1 left join vw_检验项目分类 t2 on t1.fitem_group = t2.HIS检查名称 where 1=1 ";
			if (!string.IsNullOrWhiteSpace(strWhere))
			{
				text = text + " and " + strWhere;
			}
			text += " and not exists (select 1 from SAM_APPLY ap where t1.fapply_id = ap.fapply_id)";
			text += " order by t1.fhz_id,t2.分类名称,t1.his_recordid  ";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, text).Tables[0];
		}

		public DataTable BllSamplingMainDTFromLis(string strWhere)
		{
			string text = "select fapply_id, fstate, ftype_id, fhz_id, fhz_zyh,his_recordid from SAM_APPLY where 1=1 ";
			text = text + " and " + strWhere;
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, text).Tables[0];
		}

		public DataTable BllSamplingResultDT(string strfapply_id)
		{
			DataTable result;
			if (string.IsNullOrWhiteSpace(strfapply_id))
			{
				result = null;
			}
			else
			{
				string strSql = string.Concat(new string[]
				{
					"SELECT * FROM ",
					this.table_SAM_JY_RESULT,
					" where fapply_id='",
					strfapply_id,
					"'"
				});
				result = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
			}
			return result;
		}

		public string BllSamplingSave(IList ilistfsample_id)
		{
			string text = "2";
			string strPersonID = LoginBLL.strPersonID;
			string text2 = base.DbServerDateTim();
			string text3 = Net.GetHostIpAddress() + "(" + Net.GetHostName() + ")";
			IList list = new ArrayList();
			for (int i = 0; i < ilistfsample_id.Count; i++)
			{
				string text4 = ilistfsample_id[i].ToString();
				string value = string.Concat(new string[]
				{
					"UPDATE ",
					this.table_SAM_SAMPLE,
					" SET fstate = '",
					text,
					"', fsampling_user_id = '",
					strPersonID,
					"', fsampling_time = '",
					text2,
					"', fsampling_remark = '",
					text3,
					"' WHERE (fsample_id = '",
					text4,
					"')"
				});
				list.Add(value);
			}
			return WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, list);
		}

		public string Get最新申请单号(string type, string yyMMdd)
		{
			string strStoredProc = "GetApplyNo";
			SqlParameter[] procParams = new SqlParameter[]
			{
				new SqlParameter("@jzlx", type),
				new SqlParameter("@yyMMdd", yyMMdd)
			};
			object obj = WWFInit.wwfRemotingDao.DbExecuteScalarByStoredProc(WWFInit.strDBConn, strStoredProc, procParams);
			string result;
			if (obj == null)
			{
				result = "";
			}
			else
			{
				result = obj.ToString();
			}
			return result;
		}

		public string Update更新申请状态(string applyID, string str状态值, string updateUserID)
		{
			IList list = new ArrayList();
			string value = string.Concat(new string[]
			{
				"update [SAM_APPLY] set [fstate] = '",
				str状态值,
				"',[fupdate_user_id] = '",
				updateUserID,
				"',\r\n[fupdate_time]=Convert(varchar(20),getdate(),20) where [fapply_id] = '",
				applyID,
				"'"
			});
			list.Add(value);
			string value2 = string.Concat(new string[]
			{
				"update SAM_SAMPLE set fstate = '",
				str状态值,
				"' where [fapply_id] = '",
				applyID,
				"'"
			});
			list.Add(value2);
			return WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, list);
		}
	}
}
