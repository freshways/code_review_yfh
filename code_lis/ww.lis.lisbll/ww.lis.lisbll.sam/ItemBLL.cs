using System;
using System.Collections;
using System.Data;
using System.Text;
using ww.wwf.wwfbll;

namespace ww.lis.lisbll.sam
{
	public class ItemBLL : DAOWWF
	{
		public DataTable BllInstrDT(int fuse_if, string fjygroup_id)
		{
			string strSql;
			if (fjygroup_id == "kfz")
			{
				strSql = "SELECT finstr_id, fname, '(' + finstr_id + ')' + fname AS ShowName,fjytype_id FROM sam_instr WHERE (fuse_if = " + fuse_if + ") ORDER BY forder_by";
			}
			else
			{
				strSql = string.Concat(new object[]
				{
					"SELECT finstr_id, fname, '(' + finstr_id + ')' + fname AS ShowName,fjytype_id FROM sam_instr WHERE (fuse_if = ",
					fuse_if,
					") AND (fjygroup_id = '",
					fjygroup_id,
					"') ORDER BY forder_by"
				});
			}
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public string BllItemHelpCodeUpdate(IList lisSql)
		{
			return WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, lisSql);
		}

		public int BllItemAdd(string strInstrID, string strCurrItemGuid, string strfjytype_id, string strfsam_type_id)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into sam_item(");
			stringBuilder.Append("fitem_id,fcheck_type_id,fsam_type_id,fitem_code,finstr_id,fcheck_method_id,fitem_type_id,fprint_type_id,fuse_if,fxs,fround,fref_high,fref_low,fref_if_age,fref_if_sex,fref_if_sample,fref_if_method,fprint_num_group,fvalue_dd,fjx_if,fvalue_sx,fvalue_xx,fjg_value_sx,fjg_value_xx,fvalue_day_num,fvalue_day_no,fcount,fprice,fprice_dd,fjz_if,fjz_no_if,fjz_tat,fjz_no_tat,fzk_if,freagent_num");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + strCurrItemGuid + "',");
			stringBuilder.Append("'" + strfjytype_id + "',");
			stringBuilder.Append("'" + strfsam_type_id + "',");
			stringBuilder.Append("'',");
			stringBuilder.Append("'" + strInstrID + "',");
			stringBuilder.Append("'1',");
			stringBuilder.Append("'4',");
			stringBuilder.Append("'1',");
			stringBuilder.Append("1,");
			stringBuilder.Append("1,");
			stringBuilder.Append("0,");
			stringBuilder.Append("0,");
			stringBuilder.Append("0,");
			stringBuilder.Append("0,");
			stringBuilder.Append("0,");
			stringBuilder.Append("0,");
			stringBuilder.Append("0,");
			stringBuilder.Append("0,");
			stringBuilder.Append("1,");
			stringBuilder.Append("0,");
			stringBuilder.Append("0,");
			stringBuilder.Append("0,");
			stringBuilder.Append("0,");
			stringBuilder.Append("0,");
			stringBuilder.Append("0,");
			stringBuilder.Append("0,");
			stringBuilder.Append("0,");
			stringBuilder.Append("0,");
			stringBuilder.Append("0,");
			stringBuilder.Append("0,");
			stringBuilder.Append("1,");
			stringBuilder.Append("0,");
			stringBuilder.Append("0,");
			stringBuilder.Append("0,");
			stringBuilder.Append("0");
			stringBuilder.Append(")");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllItemUpdate(DataRow rowCurrItem)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update sam_item set ");
			stringBuilder.Append("fitem_code='" + rowCurrItem["fitem_code"] + "',");
			stringBuilder.Append("finstr_id='" + rowCurrItem["finstr_id"] + "',");
			stringBuilder.Append("fcheck_type_id='" + rowCurrItem["fcheck_type_id"] + "',");
			stringBuilder.Append("fsam_type_id='" + rowCurrItem["fsam_type_id"] + "',");
			stringBuilder.Append("fitem_type_id='" + rowCurrItem["fitem_type_id"] + "',");
			stringBuilder.Append("fcheck_method_id='" + rowCurrItem["fcheck_method_id"] + "',");
			stringBuilder.Append("fname='" + rowCurrItem["fname"] + "',");
			stringBuilder.Append("fname_j='" + rowCurrItem["fname_j"] + "',");
			stringBuilder.Append("fhelp_code='" + rowCurrItem["fhelp_code"] + "',");
			stringBuilder.Append("fhis_item_code='" + rowCurrItem["fhis_item_code"] + "',");
			stringBuilder.Append("fxh_code='" + rowCurrItem["fxh_code"] + "',");
			stringBuilder.Append("funit_name='" + rowCurrItem["funit_name"] + "',");
			stringBuilder.Append("finstr_unit='" + rowCurrItem["finstr_unit"] + "',");
			stringBuilder.Append("fuse_if=" + rowCurrItem["fuse_if"] + ",");
			stringBuilder.Append("fxs=" + rowCurrItem["fxs"] + ",");
			stringBuilder.Append("fqc_code='" + rowCurrItem["fqc_code"] + "',");
			stringBuilder.Append("fqc_check_type='" + rowCurrItem["fqc_check_type"] + "',");
			stringBuilder.Append("fvalue_type_id='" + rowCurrItem["fvalue_type_id"] + "',");
			stringBuilder.Append("fround=" + rowCurrItem["fround"] + ",");
			stringBuilder.Append("ftime='" + rowCurrItem["ftime"] + "',");
			stringBuilder.Append("fsample_type_id='" + rowCurrItem["fsample_type_id"] + "',");
			stringBuilder.Append("fuser_id='" + rowCurrItem["fuser_id"] + "',");
			stringBuilder.Append("fname_e='" + rowCurrItem["fname_e"] + "',");
			stringBuilder.Append("fzhb_code='" + rowCurrItem["fzhb_code"] + "',");
			stringBuilder.Append("freport_code='" + rowCurrItem["freport_code"] + "',");
			stringBuilder.Append("fref_high=" + rowCurrItem["fref_high"] + ",");
			stringBuilder.Append("fref_low=" + rowCurrItem["fref_low"] + ",");
			stringBuilder.Append("fref='" + rowCurrItem["fref"] + "',");
			stringBuilder.Append("fref_if_age=" + rowCurrItem["fref_if_age"] + ",");
			stringBuilder.Append("fref_if_sex=" + rowCurrItem["fref_if_sex"] + ",");
			stringBuilder.Append("fref_if_sample=" + rowCurrItem["fref_if_sample"] + ",");
			stringBuilder.Append("fref_if_method=" + rowCurrItem["fref_if_method"] + ",");
			stringBuilder.Append("fprint_type_id='" + rowCurrItem["fprint_type_id"] + "',");
			stringBuilder.Append("fprint_num='" + rowCurrItem["fprint_num"] + "',");
			stringBuilder.Append("fprint_num_group=" + rowCurrItem["fprint_num_group"] + ",");
			stringBuilder.Append("fvalue_remark='" + rowCurrItem["fvalue_remark"] + "',");
			stringBuilder.Append("fvalue_dd=" + rowCurrItem["fvalue_dd"] + ",");
			stringBuilder.Append("fjx_if=" + rowCurrItem["fjx_if"] + ",");
			stringBuilder.Append("fjx_formula='" + rowCurrItem["fjx_formula"] + "',");
			stringBuilder.Append("fvalue_sx=" + rowCurrItem["fvalue_sx"] + ",");
			stringBuilder.Append("fvalue_xx=" + rowCurrItem["fvalue_xx"] + ",");
			stringBuilder.Append("fjg_value_sx=" + rowCurrItem["fjg_value_sx"] + ",");
			stringBuilder.Append("fjg_value_xx=" + rowCurrItem["fjg_value_xx"] + ",");
			stringBuilder.Append("fyy='" + rowCurrItem["fyy"] + "',");
			stringBuilder.Append("fvalue_day_num=" + rowCurrItem["fvalue_day_num"] + ",");
			stringBuilder.Append("fvalue_day_no=" + rowCurrItem["fvalue_day_no"] + ",");
			stringBuilder.Append("fcount=" + rowCurrItem["fcount"] + ",");
			stringBuilder.Append("fprice=" + rowCurrItem["fprice"] + ",");
			stringBuilder.Append("fprice_dd=" + rowCurrItem["fprice_dd"] + ",");
			stringBuilder.Append("fjz_if=" + rowCurrItem["fjz_if"] + ",");
			stringBuilder.Append("fjz_no_if=" + rowCurrItem["fjz_no_if"] + ",");
			stringBuilder.Append("fjz_tat=" + rowCurrItem["fjz_tat"] + ",");
			stringBuilder.Append("fjz_no_tat=" + rowCurrItem["fjz_no_tat"] + ",");
			stringBuilder.Append("fzk_if=" + rowCurrItem["fzk_if"] + ",");
			stringBuilder.Append("fgroup_id='" + rowCurrItem["fgroup_id"] + "',");
			stringBuilder.Append("fcheck_calendar='" + rowCurrItem["fcheck_calendar"] + "',");
			stringBuilder.Append("funsure='" + rowCurrItem["funsure"] + "',");
			stringBuilder.Append("fyx_ljv='" + rowCurrItem["fyx_ljv"] + "',");
			stringBuilder.Append("fyx_pdf='" + rowCurrItem["fyx_pdf"] + "',");
			stringBuilder.Append("fyx_formula='" + rowCurrItem["fyx_formula"] + "',");
			stringBuilder.Append("fperiod='" + rowCurrItem["fperiod"] + "',");
			stringBuilder.Append("freagent_id='" + rowCurrItem["freagent_id"] + "',");
			stringBuilder.Append("freagent_num=" + rowCurrItem["freagent_num"] + ",");
			stringBuilder.Append("freagent_unit_id='" + rowCurrItem["freagent_unit_id"] + "',");
			stringBuilder.Append("fhz_prepare='" + rowCurrItem["fhz_prepare"] + "',");
			stringBuilder.Append("fremark='" + rowCurrItem["fremark"] + "',");
			stringBuilder.Append("forder_by='" + rowCurrItem["forder_by"] + "'");
			stringBuilder.Append(" where fitem_id='" + rowCurrItem["fitem_id"] + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllItemUpdatefzk_if(string strfitem_id, string intfzk_if)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update sam_item set ");
			stringBuilder.Append("fzk_if=" + intfzk_if);
			stringBuilder.Append(" where fitem_id='" + strfitem_id + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllItemUpdatePrintNum(string fitem_id, string fprint_num)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update sam_item set ");
			stringBuilder.Append("fprint_num='" + fprint_num + "'");
			stringBuilder.Append(" where fitem_id='" + fitem_id + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public DataTable BllItem(int fuse_if, string finstr_id)
		{
			string strSql;
			if (fuse_if == 1 || fuse_if == 0)
			{
				strSql = string.Concat(new object[]
				{
					"SELECT * FROM sam_item where fuse_if=",
					fuse_if,
					" and finstr_id='",
					finstr_id,
					"' order by convert(int,fprint_num)"
				});
			}
			else
			{
				strSql = "SELECT * FROM sam_item where finstr_id='" + finstr_id + "' order by convert(int,fprint_num)";
			}
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public DataTable BllItemZK(int fzk_if, string finstr_id)
		{
			string strSql = string.Concat(new object[]
			{
				"SELECT * FROM sam_item where fzk_if=",
				fzk_if,
				" and finstr_id='",
				finstr_id,
				"' order by fprint_num"
			});
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public DataTable BllItem(string strWhere)
		{
			string text;
			if (strWhere == "" || strWhere == null)
			{
				text = "SELECT (SELECT fname FROM SAM_TYPE t1 WHERE (t1.ftype = '常用单位') AND (t1.fcode = t.funit_name)) as funit_name_zw,* FROM sam_item t ";
			}
			else
			{
				strWhere = " where " + strWhere;
				text = "SELECT (SELECT fname FROM SAM_TYPE t1 WHERE (t1.ftype = '常用单位') AND (t1.fcode = t.funit_name)) as funit_name_zw,* FROM sam_item t " + strWhere;
			}
			text += " order by finstr_id,convert(int,fprint_num)";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, text).Tables[0];
		}

		public DataTable BllItem(int fuse_if, string finstr_id, string fhelp_code)
		{
			string strSql = string.Concat(new object[]
			{
				"SELECT * FROM sam_item t where fuse_if=",
				fuse_if,
				" and finstr_id='",
				finstr_id,
				"' and fhelp_code like '",
				fhelp_code,
				"%' order by convert(int,fprint_num)"
			});
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public DataTable BllItem(int fuse_if, string finstr_id, string fhelp_code, string strDate, string strSampleNo)
		{
			string strSql = string.Concat(new object[]
			{
				"SELECT * FROM sam_item t where fuse_if=",
				fuse_if,
				" and finstr_id='",
				finstr_id,
				"' and fhelp_code like '",
				fhelp_code,
				"%'  and fitem_code not in (SELECT bb.FItem FROM [Lis_Ins_Result] aa left outer join [Lis_Ins_Result_Detail] bb on aa.FTaskID = bb.FTaskID  where FInstrID = '",
				finstr_id,
				"' and FResultID='",
				strSampleNo,
				"' and (FDateTime >= '",
				strDate,
				"' and FDateTime <= '",
				strDate,
				"'+' 23:59:59')) order by convert(int,fprint_num)"
			});
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public DataTable BllItemAll(int fuse_if, string fhelp_code)
		{
			string strSql = string.Concat(new object[]
			{
				"SELECT * FROM sam_item t where fuse_if=",
				fuse_if,
				" and fhelp_code like '",
				fhelp_code,
				"%' order by convert(int,fprint_num)"
			});
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public string BllItemDel(string fitem_id)
		{
			IList list = new ArrayList();
			list.Add("delete FROM sam_item where fitem_id='" + fitem_id + "'");
			list.Add("delete FROM sam_item_ref where fitem_id='" + fitem_id + "'");
			list.Add("delete FROM sam_item_io where fitem_id='" + fitem_id + "'");
			list.Add("delete FROM sam_item_value where fitem_id='" + fitem_id + "'");
			return WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, list);
		}

		public DataTable BllItemRefDT(string fitem_id)
		{
			string strSql = "SELECT * FROM sam_item_ref where fitem_id='" + fitem_id + "'";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public void BllItemRefAdd(string fitem_id)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into sam_item_ref(");
			stringBuilder.Append("fref_id,fage_high,fage_low,fref_high,fref_low,fitem_id");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + base.DbGuid() + "',");
			stringBuilder.Append("0,");
			stringBuilder.Append("0,");
			stringBuilder.Append("0,");
			stringBuilder.Append("0,");
			stringBuilder.Append("'" + fitem_id + "'");
			stringBuilder.Append(")");
			WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public void BllItemRefDel(string fref_id)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("delete FROM sam_item_ref ");
			stringBuilder.Append(" where fref_id='" + fref_id + "'");
			WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public void BllItemRefUpdate(DataRow drRow)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update sam_item_ref set ");
			stringBuilder.Append("fitem_id='" + drRow["fitem_id"] + "',");
			stringBuilder.Append("fsample_type_id='" + drRow["fsample_type_id"] + "',");
			stringBuilder.Append("fcheck_method_id='" + drRow["fcheck_method_id"] + "',");
			stringBuilder.Append(string.Concat(new object[]
			{
				"fref='",
				drRow["fref_low"],
				"--",
				drRow["fref_high"],
				"',"
			}));
			stringBuilder.Append("forder_by='" + drRow["forder_by"] + "',");
			stringBuilder.Append("fsex='" + drRow["fsex"] + "',");
			stringBuilder.Append("fage_high=" + drRow["fage_high"] + ",");
			stringBuilder.Append("fage_low=" + drRow["fage_low"] + ",");
			stringBuilder.Append("fref_high=" + drRow["fref_high"] + ",");
			stringBuilder.Append("fref_low=" + drRow["fref_low"]);
			stringBuilder.Append(" where fref_id='" + drRow["fref_id"] + "'");
			WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public DataTable BllItemIODT(string fitem_id)
		{
			string strSql = "SELECT * FROM sam_item_io where fitem_id='" + fitem_id + "'";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public void BllItemIOAdd(string fitem_id)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into sam_item_io(");
			stringBuilder.Append("fio_id,fitem_id,fcode,fuse_if");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + base.DbGuid() + "',");
			stringBuilder.Append("'" + fitem_id + "',");
			stringBuilder.Append("'',");
			stringBuilder.Append("1");
			stringBuilder.Append(")");
			WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public void BllItemIODel(string fio_id)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("delete FROM sam_item_io ");
			stringBuilder.Append(" where fio_id='" + fio_id + "'");
			WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public void BllItemIOUpdate(string fio_id, string fcode, string str仪器id)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update sam_item_io set ");
			stringBuilder.Append("finstr_id='" + str仪器id + "',");
			stringBuilder.Append("fcode='" + fcode + "'");
			stringBuilder.Append(" where fio_id='" + fio_id + "'");
			WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public DataTable BllItemValueDT(string fitem_id)
		{
			string strSql = "SELECT * FROM sam_item_value where fitem_id='" + fitem_id + "' order by forder_by";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public void BllItemValueAdd(string fitem_id)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into sam_item_value(");
			stringBuilder.Append("fvalue_id,fitem_id,fvalue,fvalue_flag,forder_by");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + base.DbGuid() + "',");
			stringBuilder.Append("'" + fitem_id + "',");
			stringBuilder.Append("'',");
			stringBuilder.Append("1,");
			stringBuilder.Append("1");
			stringBuilder.Append(")");
			WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public void BllItemValueDel(string fvalue_id)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("delete FROM sam_item_value ");
			stringBuilder.Append(" where fvalue_id='" + fvalue_id + "'");
			WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public void BllItemValueUpdate(string fvalue_id, string fvalue, string fvalue_flag, string forder_by)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update sam_item_value set ");
			stringBuilder.Append("fvalue='" + fvalue + "',");
			stringBuilder.Append("fvalue_flag='" + fvalue_flag + "',");
			stringBuilder.Append("forder_by='" + forder_by + "'");
			stringBuilder.Append(" where fvalue_id='" + fvalue_id + "'");
			WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public bool BllItemGroupRefExists(string fgroup_id, string fitem_id)
		{
			string sql = string.Concat(new string[]
			{
				"SELECT count(1) FROM SAM_ITEM_GROUP_REL where fitem_id='",
				fitem_id,
				"' and fgroup_id='",
				fgroup_id,
				"'"
			});
			return WWFInit.wwfRemotingDao.DbRecordExists(WWFInit.strDBConn, sql);
		}

		public DataTable BllItemAndGroupRefDT(string fgroup_id)
		{
			string strSql = "SELECT (SELECT fname FROM SAM_TYPE t5 WHERE (t5.ftype = '常用单位') AND (t5.fcode = t1.funit_name)) AS funit_name_zw,t2.fid, t2.fitem_id, t2.fgroup_id, t1.fitem_code, t1.fname, t1.fprint_num,t1.fuse_if FROM SAM_ITEM t1 ,SAM_ITEM_GROUP_REL t2 where t1.fitem_id = t2.fitem_id and t1.fuse_if=1 and t2.fgroup_id='" + fgroup_id + "'  order by convert(int,t1.fprint_num)";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public int BllItemGroupRefAdd(string fgroup_id, string fitem_id)
		{
			string strSql = string.Concat(new string[]
			{
				"INSERT INTO SAM_ITEM_GROUP_REL (fid, fgroup_id, fitem_id) VALUES ('",
				base.DbGuid(),
				"', '",
				fgroup_id,
				"', '",
				fitem_id,
				"')"
			});
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, strSql);
		}

		public int BllItemGroupRefDel(string fid)
		{
			string strSql = "DELETE FROM SAM_ITEM_GROUP_REL WHERE (fid = '" + fid + "')";
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, strSql);
		}

		public string BllItemGroupHelpCodeUpdate(IList lisSql)
		{
			return WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, lisSql);
		}

		public DataTable BllItemGroupDT(string strWhere)
		{
			string strSql;
			if (strWhere == "" || strWhere == null)
			{
				strSql = "SELECT * FROM SAM_ITEM_GROUP order by forder_by";
			}
			else
			{
				strWhere = " where " + strWhere;
				strSql = "SELECT * FROM SAM_ITEM_GROUP " + strWhere + " order by forder_by";
			}
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public int BllItemGroupAdd(DataRow dr)
		{
			dr["fgroup_id"] = base.DbGuid();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into SAM_ITEM_GROUP(");
			stringBuilder.Append("fgroup_id,fcode,fname,fname_e,fjz_if,fjz_no_if,fcheck_type_id,fsam_type_id,fhelp_code,fuse_if,forder_by,fremark");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + dr["fgroup_id"] + "',");
			stringBuilder.Append("'" + dr["fcode"] + "',");
			stringBuilder.Append("'" + dr["fname"] + "',");
			stringBuilder.Append("'" + dr["fname_e"] + "',");
			stringBuilder.Append(dr["fjz_if"] + ",");
			stringBuilder.Append(dr["fjz_no_if"] + ",");
			stringBuilder.Append("'" + dr["fcheck_type_id"] + "',");
			stringBuilder.Append("'" + dr["fsam_type_id"] + "',");
			stringBuilder.Append("'" + dr["fhelp_code"] + "',");
			stringBuilder.Append(dr["fuse_if"] + ",");
			stringBuilder.Append("'" + dr["forder_by"] + "',");
			stringBuilder.Append("'" + dr["fremark"] + "'");
			stringBuilder.Append(")");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public bool BllItemGroupDel(string fgroup_id)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("delete FROM SAM_ITEM_GROUP ");
			stringBuilder.Append(" where fgroup_id='" + fgroup_id + "'");
			StringBuilder stringBuilder2 = new StringBuilder();
			stringBuilder2.Append("delete FROM SAM_ITEM_GROUP_REL ");
			stringBuilder2.Append(" where fgroup_id='" + fgroup_id + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryTransaction2(WWFInit.strDBConn, stringBuilder.ToString(), stringBuilder2.ToString());
		}

		public int BllItemGroupUpdate(DataRow dr)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update SAM_ITEM_GROUP set ");
			stringBuilder.Append("fcode='" + dr["fcode"] + "',");
			stringBuilder.Append("fname='" + dr["fname"] + "',");
			stringBuilder.Append("fname_e='" + dr["fname_e"] + "',");
			stringBuilder.Append("fjz_if=" + dr["fjz_if"] + ",");
			stringBuilder.Append("fjz_no_if=" + dr["fjz_no_if"] + ",");
			stringBuilder.Append("fcheck_type_id='" + dr["fcheck_type_id"] + "',");
			stringBuilder.Append("fsam_type_id='" + dr["fsam_type_id"] + "',");
			stringBuilder.Append("fhelp_code='" + dr["fhelp_code"] + "',");
			stringBuilder.Append("fuse_if=" + dr["fuse_if"] + ",");
			stringBuilder.Append("forder_by='" + dr["forder_by"] + "',");
			stringBuilder.Append("fremark='" + dr["fremark"] + "'");
			stringBuilder.Append(" where fgroup_id='" + dr["fgroup_id"] + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public DataTable BllCheckAndSampleTypeDT()
		{
			DataTable dataTable = new DataTable();
			DataColumn column = new DataColumn("fid", typeof(string));
			dataTable.Columns.Add(column);
			DataColumn column2 = new DataColumn("fpid", typeof(string));
			dataTable.Columns.Add(column2);
			DataColumn column3 = new DataColumn("fname", typeof(string));
			dataTable.Columns.Add(column3);
			DataColumn column4 = new DataColumn("tag", typeof(string));
			dataTable.Columns.Add(column4);
			DataTable dataTable2 = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, "SELECT fcheck_type_id, fname FROM SAM_CHECK_TYPE WHERE (fuse_if = 1) ORDER BY forder_by").Tables[0];
			for (int i = 0; i < dataTable2.Rows.Count; i++)
			{
				DataRow dataRow = dataTable.NewRow();
				string text = dataTable2.Rows[i]["fcheck_type_id"].ToString();
				dataRow["fid"] = text;
				dataRow["fpid"] = "-1";
				dataRow["tag"] = "CheckType";
				dataRow["fname"] = dataTable2.Rows[i]["fname"];
				dataTable.Rows.Add(dataRow);
				DataTable dataTable3 = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, "SELECT t2.fcheck_type_id, t2.fsample_type_id, t2.fid, t1.fname, t1.forder_by,t1.fhelp_code FROM SAM_CHECK_SAMPLE t2,SAM_SAMPLE_TYPE t1 where t2.fsample_type_id = t1.fsample_type_id and t2.fcheck_type_id='" + text + "' and t1.fuse_if=1 order by t1.forder_by").Tables[0];
				for (int j = 0; j < dataTable3.Rows.Count; j++)
				{
					DataRow dataRow2 = dataTable.NewRow();
					dataRow2["fid"] = dataTable3.Rows[j]["fsample_type_id"].ToString();
					dataRow2["fpid"] = text;
					dataRow2["tag"] = "SampleType";
					dataRow2["fname"] = dataTable3.Rows[j]["fname"].ToString();
					dataTable.Rows.Add(dataRow2);
				}
			}
			return dataTable;
		}

		public DataTable BllGe收费大项()
		{
			DataTable dataTable = new DataTable();
			DataColumn column = new DataColumn("fid", typeof(string));
			dataTable.Columns.Add(column);
			DataColumn column2 = new DataColumn("fpid", typeof(string));
			dataTable.Columns.Add(column2);
			DataColumn column3 = new DataColumn("fname", typeof(string));
			dataTable.Columns.Add(column3);
			DataColumn column4 = new DataColumn("tag", typeof(string));
			dataTable.Columns.Add(column4);
			DataTable dataTable2 = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strCHisDBConn, "select * from vw_lis_收费大项 ").Tables[0];
			for (int i = 0; i < dataTable2.Rows.Count; i++)
			{
				DataRow dataRow = dataTable.NewRow();
				string value = dataTable2.Rows[i]["项目编码"].ToString();
				dataRow["fid"] = value;
				dataRow["fpid"] = "-1";
				dataRow["tag"] = "SampleType";
				dataRow["fname"] = dataTable2.Rows[i]["项目名称"];
				dataTable.Rows.Add(dataRow);
			}
			return dataTable;
		}

		public DataTable BllGet收费小项(string fhelp_code)
		{
			string text = "where 1=1 ";
			if (fhelp_code != "")
			{
				text = text + "and 拼音代码 like '" + fhelp_code + "%' ";
			}
			string strSql = "select 收费编码 ,收费名称 ,单价 ,归并编码,拼音代码 ,fuse_if,执行科室编码 from vw_lis_收费小项 " + text;
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strCHisDBConn, strSql).Tables[0];
		}

		public bool Bll检查对应项目(string s检验id)
		{
			string sql = "SELECT count(1) FROM SAM_收费项目对照 where 检验ID='" + s检验id + "' ";
			return WWFInit.wwfRemotingDao.DbRecordExists(WWFInit.strDBConn, sql);
		}

		public DataTable BllIGet对应关系(string s收费编码)
		{
			string strSql = "select id,收费编码 ,收费名称,检验ID,检验编码,检验名称,检验设备 from SAM_收费项目对照 t  where 收费编码='" + s收费编码 + "'  order by (SELECT top 1 convert(int,z.fprint_num) FROM   SAM_ITEM z WHERE (z.fitem_id = t.检验ID)) ";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public int Bll插入对应项目(string s收费编码, string s收费名称, string 单价, string s检验项目ID)
		{
			string strSql = string.Concat(new string[]
			{
				"insert into SAM_收费项目对照(收费编码 ,收费名称,单价,检验ID,检验编码,检验名称,检验设备)  select '",
				s收费编码,
				"' ,'",
				s收费名称,
				"' ,",
				单价,
				", fitem_id 检验ID,fitem_code 检验编码,fname 检验名称,finstr_id 检验设备 from SAM_ITEM where fitem_id='",
				s检验项目ID,
				"' "
			});
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, strSql);
		}

		public int Bll更新对应项目(string s收费编码, string s收费名称, string 单价, string s检验项目ID, string s检验编码, string s检验名称, string s检验设备)
		{
			string strSql = string.Concat(new string[]
			{
				"update SAM_收费项目对照 set 收费编码='",
				s收费编码,
				"',收费名称='",
				s收费名称,
				"',单价='",
				单价,
				"',检验编码='",
				s检验编码,
				"',检验名称='",
				s检验名称,
				"',检验设备='",
				s检验设备,
				"' where 检验ID ='",
				s检验项目ID,
				"' "
			});
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, strSql);
		}

		public int BllI删除对应项目(string fid)
		{
			string strSql = "update SAM_收费项目对照 set 收费编码=null,收费名称=null,单价=null where  检验ID='" + fid + "' ";
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, strSql);
		}

		public DataTable BllGet项目列表(int fuse_if, string finstr, string fhelp_code)
		{
			string text = " and  fuse_if=" + fuse_if + " ";
			if (finstr != "")
			{
				text = text + " and finstr_id='" + finstr + "' ";
			}
			if (fhelp_code != "")
			{
				text = text + " and fhelp_code like '" + fhelp_code + "%' ";
			}
			string strSql = "SELECT 0 选择,fitem_id 检验id,fname 检验项目,fitem_code 检验编码,finstr_id 设备,  fhelp_code 助记符 ,b.收费名称,b.收费编码,b.单价\r\n                                    FROM sam_item t left join SAM_收费项目对照 b on  t.fitem_id = b.检验ID  where 1=1  " + text + " order by convert(int,fprint_num)";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}
	}
}
