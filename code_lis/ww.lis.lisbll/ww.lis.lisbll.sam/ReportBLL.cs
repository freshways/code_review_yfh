using System;
using System.Collections;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using ww.wwf.com;
using ww.wwf.wwfbll;

namespace ww.lis.lisbll.sam
{
	public class ReportBLL : DAOWWF
	{
		private int sam_jy_fsample_code = 4;

		private string table_SAM_APPLY = "";

		private string table_SAM_JY_RESULT = "";

		private string table_SAM_SAMPLE = "";

		private string table_SAM_JY_IMG = "";

		private string view_LIS_REPORT = "";

		private NumBLL bllNum = new NumBLL();

		public ReportBLL()
		{
			this.table_SAM_APPLY = "SAM_APPLY";
			this.table_SAM_JY_RESULT = "SAM_JY_RESULT";
			this.table_SAM_SAMPLE = "SAM_SAMPLE";
			this.table_SAM_JY_IMG = "SAM_JY_IMG";
			this.view_LIS_REPORT = "LIS_REPORT";
		}

		public DataTable BllReportDT(string strWhere)
		{
			string strSql = "SELECT * FROM " + this.view_LIS_REPORT + strWhere;
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public DataTable BllReportResultDT(string strfapply_id)
		{
			string strSql = string.Concat(new string[]
			{
				"SELECT * FROM ",
				this.table_SAM_JY_RESULT,
				" where fapply_id='",
				strfapply_id,
				"' order by forder_by"
			});
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public int BllReportResultAdd(DataRow drResult)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into " + this.table_SAM_JY_RESULT + "(");
			stringBuilder.Append("fresult_id,fapply_id,fitem_id,fitem_code,fitem_name,fitem_unit,forder_by");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + drResult["fresult_id"] + "',");
			stringBuilder.Append("'" + drResult["fapply_id"] + "',");
			stringBuilder.Append("'" + drResult["fitem_id"] + "',");
			stringBuilder.Append("'" + drResult["fitem_code"] + "',");
			stringBuilder.Append("'" + drResult["fitem_name"] + "',");
			stringBuilder.Append("'" + drResult["fitem_unit"] + "',");
			stringBuilder.Append("'" + drResult["forder_by"] + "'");
			stringBuilder.Append(")");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public string BllReportIMGAdd(DataTable dtIMG, string strfsample_id)
		{
			IList list = new ArrayList();
			if (dtIMG != null)
			{
				if (dtIMG.Rows.Count > 0)
				{
					string strSql = string.Concat(new string[]
					{
						"delete FROM ",
						this.table_SAM_JY_IMG,
						" WHERE (fsample_id = '",
						strfsample_id,
						"')"
					});
					WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, strSql);
					for (int i = 0; i < dtIMG.Rows.Count; i++)
					{
						DataRow dataRow = dtIMG.Rows[i];
						WWFInit.wwfRemotingDao.DbReportImgAdd(WWFInit.strDBConn, this.table_SAM_JY_IMG + "_ADD", (string)dataRow["fimg_type"], (byte[])dataRow["fimg"], (string)dataRow["forder_by"], (string)dataRow["fimg_type"], strfsample_id);
					}
				}
			}
			return "true";
		}

		public int BllReportResultDel(string fresult_id)
		{
			string strSql = string.Concat(new string[]
			{
				"delete from ",
				this.table_SAM_JY_RESULT,
				" where fresult_id='",
				fresult_id,
				"'"
			});
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, strSql);
		}

		public DataTable BllReportImgDT(string strfsample_id)
		{
			string strSql = string.Concat(new string[]
			{
				"SELECT * FROM ",
				this.table_SAM_JY_IMG,
				" where fsample_id='",
				strfsample_id,
				"'"
			});
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public bool BllReportExists(string finstr_id, string fjy_date, string fsample_code)
		{
			string sql = string.Concat(new string[]
			{
				"SELECT count(1) FROM ",
				this.table_SAM_SAMPLE,
				" WHERE (fjy_instr = '",
				finstr_id,
				"') AND (fjy_date = '",
				fjy_date,
				"') AND (fsample_code = '",
				fsample_code,
				"')"
			});
			return WWFInit.wwfRemotingDao.DbRecordExists(WWFInit.strDBConn, sql);
		}

		public string BllReportAdd(DataRow dataR)
		{
			IList list = new ArrayList();
			string result;
			if (this.BllReportExists(dataR["fjy_instr"].ToString(), dataR["fjy_date"].ToString(), dataR["fsample_code"].ToString()))
			{
				result = "样本号为：" + dataR["fsample_code"].ToString() + "的记录已经存在！";
			}
			else
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append("insert into " + this.table_SAM_APPLY + "(");
				stringBuilder.Append("fapply_id,fjytype_id,fsample_type_id,fjz_flag,fcharge_flag,fcharge_id,fstate,fjyf,fapply_user_id,fapply_dept_id,fapply_time,ftype_id,fhz_id,fhz_zyh,fsex,fname,fvolk,fhymen,fage,fjob,fgms,fjob_org,fadd,ftel,fage_unit,fage_year,fage_month,fage_day,fward_num,froom_num,fbed_num,fblood_abo,fblood_rh,fjymd,fdiagnose,fheat,fxyld,fcyy,fxhdb,fcreate_user_id,fcreate_time,fupdate_user_id,fupdate_time,fitem_group,fremark");
				stringBuilder.Append(")");
				stringBuilder.Append(" values (");
				stringBuilder.Append("'" + dataR["fapply_id"] + "',");
				stringBuilder.Append("'" + dataR["fjytype_id"] + "',");
				stringBuilder.Append("'" + dataR["fsample_type_id"] + "',");
				stringBuilder.Append(dataR["fjz_flag"] + ",");
				stringBuilder.Append(dataR["fcharge_flag"] + ",");
				stringBuilder.Append("'" + dataR["fcharge_id"] + "',");
				stringBuilder.Append("'" + dataR["fstate"] + "',");
				stringBuilder.Append(dataR["fjyf"] + ",");
				stringBuilder.Append("'" + dataR["fapply_user_id"] + "',");
				stringBuilder.Append("'" + dataR["fapply_dept_id"] + "',");
				stringBuilder.Append("'" + dataR["fapply_time"] + "',");
				stringBuilder.Append("'" + dataR["ftype_id"] + "',");
				stringBuilder.Append("'" + dataR["fhz_id"] + "',");
				stringBuilder.Append("'" + dataR["fhz_zyh"] + "',");
				stringBuilder.Append("'" + dataR["fsex"] + "',");
				stringBuilder.Append("'" + dataR["fname"] + "',");
				stringBuilder.Append("'" + dataR["fvolk"] + "',");
				stringBuilder.Append("'" + dataR["fhymen"] + "',");
				stringBuilder.Append(dataR["fage"] + ",");
				stringBuilder.Append("'" + dataR["fjob"] + "',");
				stringBuilder.Append("'" + dataR["fgms"] + "',");
				stringBuilder.Append("'" + dataR["fjob_org"] + "',");
				stringBuilder.Append("'" + dataR["fadd"] + "',");
				stringBuilder.Append("'" + dataR["ftel"] + "',");
				stringBuilder.Append("'" + dataR["fage_unit"] + "',");
				stringBuilder.Append(dataR["fage_year"] + ",");
				stringBuilder.Append(dataR["fage_month"] + ",");
				stringBuilder.Append(dataR["fage_day"] + ",");
				stringBuilder.Append("'" + dataR["fward_num"] + "',");
				stringBuilder.Append("'" + dataR["froom_num"] + "',");
				stringBuilder.Append("'" + dataR["fbed_num"] + "',");
				stringBuilder.Append("'" + dataR["fblood_abo"] + "',");
				stringBuilder.Append("'" + dataR["fblood_rh"] + "',");
				stringBuilder.Append("'" + dataR["fjymd"] + "',");
				stringBuilder.Append("'" + dataR["fdiagnose"] + "',");
				stringBuilder.Append(dataR["fheat"] + ",");
				stringBuilder.Append(dataR["fxyld"] + ",");
				stringBuilder.Append("'" + dataR["fcyy"] + "',");
				stringBuilder.Append(dataR["fxhdb"] + ",");
				stringBuilder.Append("'" + dataR["fcreate_user_id"] + "',");
				stringBuilder.Append("'" + dataR["fcreate_time"] + "',");
				stringBuilder.Append("'" + dataR["fupdate_user_id"] + "',");
				stringBuilder.Append("'" + dataR["fupdate_time"] + "',");
				stringBuilder.Append("'" + dataR["fitem_group"] + "',");
				stringBuilder.Append("'" + dataR["fremark"] + "'");
				stringBuilder.Append(")");
				list.Add(stringBuilder.ToString());
				StringBuilder stringBuilder2 = new StringBuilder();
				stringBuilder2.Append("insert into " + this.table_SAM_SAMPLE + "(");
				stringBuilder2.Append("fsample_id,fapply_id,fsample_barcode,fsample_code,fstate,fexamine_flag,fsampling_user_id,fsampling_time,fsampling_remark,fsend_user_id,fsend_user_time,fget_user_id,fget_user_time,fget_user_remark,fjy_instr,fjy_date,fjy_group,fjy_user_id,fjy_time,fexamine_user_id,fexamine_time,fhandling_user_id,fhandling_user_time,fhandling_remark,fprint_flag,fprint_time,fprint_count,frelease_time,fcreate_user_id,fcreate_time,fupdate_user_id,fupdate_time,fread_flag,fread_user_id,fread_user_remark,fremark");
				stringBuilder2.Append(")");
				stringBuilder2.Append(" values (");
				stringBuilder2.Append("'" + dataR["fsample_id"] + "',");
				stringBuilder2.Append("'" + dataR["fapply_id"] + "',");
				stringBuilder2.Append("'" + dataR["fsample_barcode"] + "',");
				stringBuilder2.Append("'" + dataR["fsample_code"] + "',");
				stringBuilder2.Append("'" + dataR["fstate"] + "',");
				stringBuilder2.Append(dataR["fexamine_flag"] + ",");
				stringBuilder2.Append("'" + dataR["fsampling_user_id"] + "',");
				stringBuilder2.Append("'" + dataR["fsampling_time"] + "',");
				stringBuilder2.Append("'" + dataR["fsampling_remark"] + "',");
				stringBuilder2.Append("'" + dataR["fsend_user_id"] + "',");
				stringBuilder2.Append("'" + dataR["fsend_user_time"] + "',");
				stringBuilder2.Append("'" + dataR["fget_user_id"] + "',");
				stringBuilder2.Append("'" + dataR["fget_user_time"] + "',");
				stringBuilder2.Append("'" + dataR["fget_user_remark"] + "',");
				stringBuilder2.Append("'" + dataR["fjy_instr"] + "',");
				stringBuilder2.Append("'" + dataR["fjy_date"] + "',");
				stringBuilder2.Append("'" + dataR["fjy_group"] + "',");
				stringBuilder2.Append("'" + dataR["fjy_user_id"] + "',");
				stringBuilder2.Append("'" + dataR["fjy_time"] + "',");
				stringBuilder2.Append("'" + dataR["fexamine_user_id"] + "',");
				stringBuilder2.Append("'" + dataR["fexamine_time"] + "',");
				stringBuilder2.Append("'" + dataR["fhandling_user_id"] + "',");
				stringBuilder2.Append("'" + dataR["fhandling_user_time"] + "',");
				stringBuilder2.Append("'" + dataR["fhandling_remark"] + "',");
				stringBuilder2.Append(dataR["fprint_flag"] + ",");
				stringBuilder2.Append("'" + dataR["fprint_time"] + "',");
				stringBuilder2.Append(dataR["fprint_count"] + ",");
				stringBuilder2.Append("'" + dataR["frelease_time"] + "',");
				stringBuilder2.Append("'" + dataR["fcreate_user_id"] + "',");
				stringBuilder2.Append("'" + dataR["fcreate_time"] + "',");
				stringBuilder2.Append("'" + dataR["fupdate_user_id"] + "',");
				stringBuilder2.Append("'" + dataR["fupdate_time"] + "',");
				stringBuilder2.Append(dataR["fread_flag"] + ",");
				stringBuilder2.Append("'" + dataR["fread_user_id"] + "',");
				stringBuilder2.Append("'" + dataR["fread_user_remark"] + "',");
				stringBuilder2.Append("'" + dataR["fremark"] + "'");
				stringBuilder2.Append(")");
				list.Add(stringBuilder2.ToString());
				result = WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, list);
			}
			return result;
		}

		public string BllReportUpdate(DataRowView drReport, DataTable dtResult)
		{
			IList list = new ArrayList();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update " + this.table_SAM_APPLY + " set ");
			stringBuilder.Append("fjytype_id='" + drReport["fjytype_id"] + "',");
			stringBuilder.Append("fsample_type_id='" + drReport["fsample_type_id"] + "',");
			stringBuilder.Append("fjz_flag=" + drReport["fjz_flag"] + ",");
			stringBuilder.Append("fcharge_flag=" + drReport["fcharge_flag"] + ",");
			stringBuilder.Append("fcharge_id='" + drReport["fcharge_id"] + "',");
			stringBuilder.Append("fstate='" + drReport["fstate"] + "',");
			stringBuilder.Append("fjyf=" + drReport["fjyf"] + ",");
			stringBuilder.Append("fapply_user_id='" + drReport["fapply_user_id"] + "',");
			stringBuilder.Append("fapply_dept_id='" + drReport["fapply_dept_id"] + "',");
			stringBuilder.Append("fapply_time='" + drReport["fapply_time"] + "',");
			stringBuilder.Append("ftype_id='" + drReport["ftype_id"] + "',");
			stringBuilder.Append("fhz_id='" + drReport["fhz_id"] + "',");
			stringBuilder.Append("fhz_zyh='" + drReport["fhz_zyh"] + "',");
			stringBuilder.Append("fsex='" + drReport["fsex"] + "',");
			stringBuilder.Append("fname='" + drReport["fname"] + "',");
			stringBuilder.Append("fvolk='" + drReport["fvolk"] + "',");
			stringBuilder.Append("fhymen='" + drReport["fhymen"] + "',");
			stringBuilder.Append("fage=" + drReport["fage"] + ",");
			stringBuilder.Append("fjob='" + drReport["fjob"] + "',");
			stringBuilder.Append("fgms='" + drReport["fgms"] + "',");
			stringBuilder.Append("fjob_org='" + drReport["fjob_org"] + "',");
			stringBuilder.Append("fadd='" + drReport["fadd"] + "',");
			stringBuilder.Append("ftel='" + drReport["ftel"] + "',");
			stringBuilder.Append("fage_unit='" + drReport["fage_unit"] + "',");
			stringBuilder.Append("fage_year=" + drReport["fage_year"] + ",");
			stringBuilder.Append("fage_month=" + drReport["fage_month"] + ",");
			stringBuilder.Append("fage_day=" + drReport["fage_day"] + ",");
			stringBuilder.Append("fward_num='" + drReport["fward_num"] + "',");
			stringBuilder.Append("froom_num='" + drReport["froom_num"] + "',");
			stringBuilder.Append("fbed_num='" + drReport["fbed_num"] + "',");
			stringBuilder.Append("fblood_abo='" + drReport["fblood_abo"] + "',");
			stringBuilder.Append("fblood_rh='" + drReport["fblood_rh"] + "',");
			stringBuilder.Append("fjymd='" + drReport["fjymd"] + "',");
			stringBuilder.Append("fdiagnose='" + drReport["fdiagnose"] + "',");
			stringBuilder.Append("fheat=" + drReport["fheat"] + ",");
			stringBuilder.Append("fxyld=" + drReport["fxyld"] + ",");
			stringBuilder.Append("fcyy='" + drReport["fcyy"] + "',");
			stringBuilder.Append("fxhdb=" + drReport["fxhdb"] + ",");
			stringBuilder.Append("fcreate_user_id='" + drReport["fcreate_user_id"] + "',");
			stringBuilder.Append("fcreate_time='" + drReport["fcreate_time"] + "',");
			stringBuilder.Append("fupdate_user_id='" + drReport["fupdate_user_id"] + "',");
			stringBuilder.Append("fupdate_time='" + drReport["fupdate_time"] + "',");
			stringBuilder.Append("fitem_group='" + drReport["fitem_group"] + "',");
			stringBuilder.Append("fremark='" + drReport["fremark"] + "'");
			stringBuilder.Append(" where fapply_id='" + drReport["fapply_id"] + "' ");
			list.Add(stringBuilder.ToString());
			StringBuilder stringBuilder2 = new StringBuilder();
			stringBuilder2.Append("update " + this.table_SAM_SAMPLE + " set ");
			stringBuilder2.Append("fapply_id='" + drReport["fapply_id"] + "',");
			stringBuilder2.Append("fsample_barcode='" + drReport["fsample_barcode"] + "',");
			stringBuilder2.Append("fsample_code='" + drReport["fsample_code"] + "',");
			stringBuilder2.Append("fstate='" + drReport["fstate"] + "',");
			stringBuilder2.Append("fexamine_flag=" + drReport["fexamine_flag"] + ",");
			stringBuilder2.Append("fsampling_user_id='" + drReport["fsampling_user_id"] + "',");
			stringBuilder2.Append("fsampling_time='" + drReport["fsampling_time"] + "',");
			stringBuilder2.Append("fsampling_remark='" + drReport["fsampling_remark"] + "',");
			stringBuilder2.Append("fsend_user_id='" + drReport["fsend_user_id"] + "',");
			stringBuilder2.Append("fsend_user_time='" + drReport["fsend_user_time"] + "',");
			stringBuilder2.Append("fget_user_id='" + drReport["fget_user_id"] + "',");
			stringBuilder2.Append("fget_user_time='" + drReport["fget_user_time"] + "',");
			stringBuilder2.Append("fget_user_remark='" + drReport["fget_user_remark"] + "',");
			stringBuilder2.Append("fjy_instr='" + drReport["fjy_instr"] + "',");
			stringBuilder2.Append("fjy_date='" + drReport["fjy_date"] + "',");
			stringBuilder2.Append("fjy_group='" + drReport["fjy_group"] + "',");
			stringBuilder2.Append("fjy_user_id='" + drReport["fjy_user_id"] + "',");
			stringBuilder2.Append("fjy_time='" + drReport["fjy_time"] + "',");
			stringBuilder2.Append("fexamine_user_id='" + drReport["fexamine_user_id"] + "',");
			stringBuilder2.Append("fexamine_time='" + drReport["fexamine_time"] + "',");
			stringBuilder2.Append("fhandling_user_id='" + drReport["fhandling_user_id"] + "',");
			stringBuilder2.Append("fhandling_user_time='" + drReport["fhandling_user_time"] + "',");
			stringBuilder2.Append("fhandling_remark='" + drReport["fhandling_remark"] + "',");
			stringBuilder2.Append("fprint_flag=" + drReport["fprint_flag"] + ",");
			stringBuilder2.Append("fprint_time='" + drReport["fprint_time"] + "',");
			stringBuilder2.Append("fprint_count=" + drReport["fprint_count"] + ",");
			stringBuilder2.Append("frelease_time='" + drReport["frelease_time"] + "',");
			stringBuilder2.Append("fcreate_user_id='" + drReport["fcreate_user_id"] + "',");
			stringBuilder2.Append("fcreate_time='" + drReport["fcreate_time"] + "',");
			stringBuilder2.Append("fupdate_user_id='" + drReport["fupdate_user_id"] + "',");
			stringBuilder2.Append("fupdate_time='" + drReport["fupdate_time"] + "',");
			stringBuilder2.Append("fread_flag=" + drReport["fread_flag"] + ",");
			stringBuilder2.Append("fread_user_id='" + drReport["fread_user_id"] + "',");
			stringBuilder2.Append("fread_user_remark='" + drReport["fread_user_remark"] + "',");
			stringBuilder2.Append("fremark='" + drReport["fremark"] + "'");
			stringBuilder2.Append(" where fsample_id='" + drReport["fsample_id"] + "' ");
			list.Add(stringBuilder2.ToString());
			for (int i = 0; i < dtResult.Rows.Count; i++)
			{
				DataRow dataRow = dtResult.Rows[i];
				StringBuilder stringBuilder3 = new StringBuilder();
				stringBuilder3.Append("update " + this.table_SAM_JY_RESULT + " set ");
				stringBuilder3.Append("fapply_id='" + dataRow["fapply_id"] + "',");
				stringBuilder3.Append("fitem_id='" + dataRow["fitem_id"] + "',");
				stringBuilder3.Append("fitem_code='" + dataRow["fitem_code"] + "',");
				stringBuilder3.Append("fitem_name='" + dataRow["fitem_name"] + "',");
				stringBuilder3.Append("fitem_unit='" + dataRow["fitem_unit"] + "',");
				stringBuilder3.Append("fitem_ref='" + dataRow["fitem_ref"] + "',");
				stringBuilder3.Append("fitem_badge='" + dataRow["fitem_badge"] + "',");
				stringBuilder3.Append("forder_by='" + dataRow["forder_by"] + "',");
				stringBuilder3.Append("fvalue='" + dataRow["fvalue"] + "',");
				stringBuilder3.Append("fod='" + dataRow["fod"] + "',");
				stringBuilder3.Append("fcutoff='" + dataRow["fcutoff"] + "',");
				stringBuilder3.Append("fremark='" + dataRow["fremark"] + "'");
				stringBuilder3.Append(" where fresult_id='" + dataRow["fresult_id"] + "' ");
				list.Add(stringBuilder3.ToString());
			}
			return WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, list);
		}

		public string BllReportDel(string strfapply_id, string strfsample_id)
		{
			IList list = new ArrayList();
			string value = string.Concat(new string[]
			{
				"delete FROM ",
				this.table_SAM_APPLY,
				"  where fapply_id='",
				strfapply_id,
				"'"
			});
			string value2 = string.Concat(new string[]
			{
				"delete FROM ",
				this.table_SAM_SAMPLE,
				" where fsample_id='",
				strfsample_id,
				"'"
			});
			string value3 = string.Concat(new string[]
			{
				"delete FROM ",
				this.table_SAM_JY_RESULT,
				" where fapply_id='",
				strfapply_id,
				"'"
			});
			string value4 = string.Concat(new string[]
			{
				"delete FROM ",
				this.table_SAM_JY_IMG,
				" where fsample_id='",
				strfsample_id,
				"'"
			});
			list.Add(value);
			list.Add(value2);
			list.Add(value3);
			list.Add(value4);
			return WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, list);
		}

		public string BllGetfsampleCodeUpOrDown(string fsample_code, string upORdown)
		{
			if (!(fsample_code == "") && fsample_code != null)
			{
				this.sam_jy_fsample_code = fsample_code.Length;
			}
			string str;
			switch (this.sam_jy_fsample_code)
			{
			case 2:
				str = "0";
				break;
			case 3:
				str = "00";
				break;
			case 4:
				str = "000";
				break;
			case 5:
				str = "0000";
				break;
			case 6:
				str = "00000";
				break;
			case 7:
				str = "000000";
				break;
			case 8:
				str = "0000000";
				break;
			case 9:
				str = "00000000";
				break;
			case 10:
				str = "000000000";
				break;
			default:
				str = "";
				break;
			}
			if (fsample_code == "" || fsample_code == null || fsample_code.Length == 0)
			{
				if (upORdown == "up")
				{
					fsample_code = str + "2";
				}
				else
				{
					fsample_code = str + "0";
				}
			}
			int length = fsample_code.Length;
			int num = 1;
			if (upORdown == "up")
			{
				if (Public.IsNumber(fsample_code))
				{
					if (Convert.ToInt32(fsample_code) > 1)
					{
						num = Convert.ToInt32(fsample_code) - 1;
					}
				}
			}
			if (upORdown == "down")
			{
				if (Public.IsNumber(fsample_code))
				{
					num = Convert.ToInt32(fsample_code) + 1;
				}
			}
			int length2 = num.ToString().Length;
			string result;
			switch (length - length2)
			{
			case 1:
				result = "0" + num.ToString();
				break;
			case 2:
				result = "00" + num.ToString();
				break;
			case 3:
				result = "000" + num.ToString();
				break;
			case 4:
				result = "0000" + num.ToString();
				break;
			case 5:
				result = "00000" + num.ToString();
				break;
			case 6:
				result = "000000" + num.ToString();
				break;
			case 7:
				result = "0000000" + num.ToString();
				break;
			case 8:
				result = "00000000" + num.ToString();
				break;
			case 9:
				result = "000000000" + num.ToString();
				break;
			case 10:
				result = "0000000000" + num.ToString();
				break;
			default:
				result = num.ToString();
				break;
			}
			return result;
		}

		public int BllReportUpdatePrintTime(string fsample_id)
		{
			string str = base.DbServerDateTim();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update " + this.table_SAM_SAMPLE + " set ");
			stringBuilder.Append("fprint_time='" + str + "',");
			stringBuilder.Append("fprint_count=fprint_count+1,");
			stringBuilder.Append("fprint_flag=1,");
			stringBuilder.Append("fupdate_user_id='" + LoginBLL.strPersonID + "',");
			stringBuilder.Append("fupdate_time='" + str + "'");
			stringBuilder.Append(" where fsample_id='" + fsample_id + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public DataTable BllItemDT(string fitem_id)
		{
			string strSql = "SELECT (SELECT fname FROM SAM_TYPE t1 WHERE (ftype = '常用单位') AND (fcode = t.funit_name)) as funitname,* FROM SAM_ITEM t WHERE (fitem_id = '" + fitem_id + "')";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public DataTable BllItemRefDT(string fitem_id)
		{
			string strSql = "SELECT * FROM SAM_ITEM_REF WHERE (fitem_id = '" + fitem_id + "')";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public string BllItemfref(string strItem_id, int intfage, string strfsex, string strfsample_type_id)
		{
			DataRow dataRow = this.BllItemDT(strItem_id).Rows[0];
			string fitem_id = dataRow["fitem_id"].ToString();
			string text = dataRow["fname"].ToString();
			string a = dataRow["fref_if_age"].ToString();
			string a2 = dataRow["fref_if_sex"].ToString();
			string a3 = dataRow["fref_if_sample"].ToString();
			string a4 = dataRow["fref_if_method"].ToString();
			string result = dataRow["fref"].ToString();
			if (!(a == "0" & a2 == "0" & a3 == "0" & a4 == "0"))
			{
				DataTable dataTable = this.BllItemRefDT(fitem_id);
				string filterExpression = "";
				if (a == "1" & a2 == "0" & a3 == "0")
				{
					filterExpression = string.Concat(new object[]
					{
						"(",
						intfage,
						">=fage_low) AND (",
						intfage,
						"<fage_high)"
					});
				}
				if (a == "1" & a2 == "1" & a3 == "0")
				{
					filterExpression = string.Concat(new object[]
					{
						"('",
						strfsex,
						"'=fsex) AND (",
						intfage,
						">=fage_low) AND (",
						intfage,
						"<fage_high)"
					});
				}
				if (a == "1" & a2 == "1" & a3 == "1")
				{
					filterExpression = string.Concat(new object[]
					{
						"('",
						strfsample_type_id,
						"'=fsample_type_id) AND ('",
						strfsex,
						"'=fsex) AND (",
						intfage,
						">=fage_low) AND (",
						intfage,
						"<fage_high)"
					});
				}
				if (a == "0" & a2 == "1" & a3 == "0")
				{
					filterExpression = "('" + strfsex + "'=fsex)";
				}
				if (a == "0" & a2 == "1" & a3 == "1")
				{
					filterExpression = string.Concat(new string[]
					{
						"('",
						strfsample_type_id,
						"'=fsample_type_id) AND ('",
						strfsex,
						"'=fsex)"
					});
				}
				if (a == "0" & a2 == "0" & a3 == "1")
				{
					filterExpression = "('" + strfsample_type_id + "'=fsample_type_id)";
				}
				if (a == "0" & a2 == "1" & a3 == "1")
				{
					filterExpression = string.Concat(new string[]
					{
						"('",
						strfsample_type_id,
						"'=fsample_type_id) AND ('",
						strfsex,
						"'=fsex)"
					});
				}
				DataRow[] array = dataTable.Select(filterExpression);
				if (array.Length > 0)
				{
					DataRow[] array2 = array;
					for (int i = 0; i < array2.Length; i++)
					{
						DataRow dataRow2 = array2[i];
						result = dataRow2["fref"].ToString();
					}
				}
			}
			return result;
		}

		public int BllItemValueBJ(string fvalue, string fref)
		{
			int result = 1;
			string[] array = Regex.Split(fref, "--");
			if (Public.IsNumber(fvalue))
			{
				double num = Convert.ToDouble(fvalue);
				if (array.Length == 2)
				{
					if (Public.IsNumber(array[0].ToString()) & Public.IsNumber(array[1].ToString()))
					{
						if (num < Convert.ToDouble(array[0].ToString()))
						{
							result = 2;
						}
						if (num > Convert.ToDouble(array[1].ToString()))
						{
							result = 3;
						}
					}
				}
			}
			return result;
		}

		public string BllItemValueBJFname(int intBJ)
		{
			return WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(WWFInit.strDBConn, "SELECT fname FROM SAM_TYPE WHERE (ftype = '结果标记') AND (fcode = '" + intBJ + "')").ToString();
		}
	}
}
