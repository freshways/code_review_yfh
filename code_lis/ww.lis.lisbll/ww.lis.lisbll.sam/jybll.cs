using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using ww.wwf.com;
using ww.wwf.wwfbll;

namespace ww.lis.lisbll.sam
{
	public class jybll : DAOWWF
	{
		public string AddSql(jymodel model)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into SAM_JY(");
			stringBuilder.Append("fjy_id,fjy_date,fjy_instr,fjy_yb_tm,fjy_yb_code,fjy_yb_type,fjy_sf_type,fjy_f,fjy_f_ok,fjy_zt,fjy_md,fjy_lczd,fhz_id,fhz_type_id,fhz_zyh,fhz_name,fhz_sex,fhz_age,fhz_age_unit,fhz_age_date,fhz_bq,fhz_dept,fhz_room,fhz_bed,fhz_volk,fhz_hy,fhz_job,fhz_gms,fhz_add,fhz_tel,fapply_id,fapply_user_id,fapply_dept_id,fapply_time,fsampling_user_id,fsampling_time,fjy_user_id,fchenk_user_id,fcheck_time,freport_time,fprint_time,fprint_zt,fprint_count,fsys_user,fsys_time,fsys_dept,fremark,finstr_result_id,f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,sfzh");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + model.fjy_检验id + "',");
			stringBuilder.Append("'" + model.fjy_检验日期 + "',");
			stringBuilder.Append("'" + model.fjy_仪器ID + "',");
			stringBuilder.Append("'" + model.fjy_yb_tm + "',");
			stringBuilder.Append("'" + model.fjy_yb_code + "',");
			stringBuilder.Append("'" + model.fjy_yb_type + "',");
			stringBuilder.Append("'" + model.fjy_收费类型ID + "',");
			stringBuilder.Append(model.fjy_f + ",");
			stringBuilder.Append("'" + model.fjy_f_ok + "',");
			stringBuilder.Append("'" + model.fjy_zt检验状态 + "',");
			stringBuilder.Append("'" + model.fjy_md + "',");
			stringBuilder.Append("'" + model.fjy_lczd + "',");
			stringBuilder.Append("'" + model.fhz_id + "',");
			stringBuilder.Append("'" + model.fhz_患者类型id + "',");
			stringBuilder.Append("'" + model.fhz_住院号 + "',");
			stringBuilder.Append("'" + model.fhz_姓名 + "',");
			stringBuilder.Append("'" + model.fhz_性别 + "',");
			stringBuilder.Append(model.fhz_年龄 + ",");
			stringBuilder.Append("'" + model.fhz_年龄单位 + "',");
			stringBuilder.Append("'" + model.fhz_生日 + "',");
			stringBuilder.Append("'" + model.fhz_病区 + "',");
			stringBuilder.Append("'" + model.fhz_dept患者科室 + "',");
			stringBuilder.Append("'" + model.fhz_room + "',");
			stringBuilder.Append("'" + model.fhz_床号 + "',");
			stringBuilder.Append("'" + model.fhz_volk + "',");
			stringBuilder.Append("'" + model.fhz_hy + "',");
			stringBuilder.Append("'" + model.fhz_job + "',");
			stringBuilder.Append("'" + model.fhz_gms + "',");
			stringBuilder.Append("'" + model.fhz_add + "',");
			stringBuilder.Append("'" + model.fhz_tel + "',");
			stringBuilder.Append("'" + model.fapply_申请单ID + "',");
			stringBuilder.Append("'" + model.fapply_user_id + "',");
			stringBuilder.Append("'" + model.fapply_dept_id + "',");
			stringBuilder.Append("'" + model.fapply_申请时间 + "',");
			stringBuilder.Append("'" + model.fsampling_user_id + "',");
			stringBuilder.Append("'" + model.fsampling_采样时间 + "',");
			stringBuilder.Append("'" + model.fjy_user_id + "',");
			stringBuilder.Append("'" + model.fchenk_审核医师ID + "',");
			stringBuilder.Append("'" + model.fcheck_审核时间 + "',");
			stringBuilder.Append("'" + model.freport_报告时间 + "',");
			stringBuilder.Append("'" + model.fprint_time + "',");
			stringBuilder.Append("'" + model.fprint_zt + "',");
			stringBuilder.Append(model.fprint_count + ",");
			stringBuilder.Append("'" + model.fsys_用户ID + "',");
			stringBuilder.Append("'" + model.fsys_创建时间 + "',");
			stringBuilder.Append("'" + model.fsys_部门ID + "',");
			stringBuilder.Append("'" + model.fremark_备注 + "',");
			stringBuilder.Append("'" + model.finstr_result_id + "',");
			stringBuilder.Append("'" + model.f1 + "',");
			stringBuilder.Append("'" + model.f2 + "',");
			stringBuilder.Append("'" + model.f3 + "',");
			stringBuilder.Append("'" + model.f4 + "',");
			stringBuilder.Append("'" + model.f5 + "',");
			stringBuilder.Append("'" + model.f6 + "',");
			stringBuilder.Append("'" + model.f7 + "',");
			stringBuilder.Append("'" + model.f8 + "',");
			stringBuilder.Append("'" + model.f9 + "',");
			stringBuilder.Append("'" + model.f10 + "',");
			stringBuilder.Append("'" + model.S身份证号 + "'");
			stringBuilder.Append(")");
			return stringBuilder.ToString();
		}

		public string AddSqlPatient(jymodel model)
		{
			StringBuilder stringBuilder = new StringBuilder();
			bool flag = WWFInit.wwfRemotingDao.DbRecordExists(WWFInit.strDBConn, "SELECT  count(fhz_id) FROM  SAM_PATIENT WHERE (fhz_id = '" + model.fhz_id + "')");
			if (flag)
			{
				stringBuilder.Append(" delete from SAM_PATIENT where fhz_id='" + model.fhz_id + "'");
			}
			stringBuilder.Append("insert into SAM_PATIENT(");
			stringBuilder.Append("fhz_id,fhz_zyh,fname,fsex,ftype_id,fdept_id,fsjys_id,fbed_num,fdiagnose,fbirthday,ffb_id,fuse_if,fremark,ftime,ftime_registration,sfzh");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + model.fhz_id + "',");
			stringBuilder.Append("'" + model.fhz_住院号 + "',");
			stringBuilder.Append("'" + model.fhz_姓名 + "',");
			stringBuilder.Append("'" + model.fhz_性别 + "',");
			stringBuilder.Append("'" + model.fhz_患者类型id + "',");
			stringBuilder.Append("'" + model.fhz_dept患者科室 + "',");
			stringBuilder.Append("'" + model.fapply_user_id + "',");
			stringBuilder.Append("'" + model.fhz_床号 + "',");
			stringBuilder.Append("'" + model.fjy_lczd + "',");
			stringBuilder.Append("'" + model.fhz_生日 + "',");
			stringBuilder.Append("'" + model.fjy_收费类型ID + "',");
			stringBuilder.Append("1,");
			stringBuilder.Append("'自动保存',");
			stringBuilder.Append("'" + base.DbSysTime() + "',");
			stringBuilder.Append("'" + base.DbSysTime() + "',");
			stringBuilder.Append("'" + model.S身份证号 + "'");
			stringBuilder.Append(")");
			return stringBuilder.ToString();
		}

		public string UpdateSql(jymodel model)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update SAM_JY set ");
			stringBuilder.Append("fjy_date='" + model.fjy_检验日期 + "',");
			stringBuilder.Append("fjy_instr='" + model.fjy_仪器ID + "',");
			stringBuilder.Append("fjy_yb_tm='" + model.fjy_yb_tm + "',");
			stringBuilder.Append("fjy_yb_code='" + model.fjy_yb_code + "',");
			stringBuilder.Append("fjy_yb_type='" + model.fjy_yb_type + "',");
			stringBuilder.Append("fjy_sf_type='" + model.fjy_收费类型ID + "',");
			stringBuilder.Append("fjy_f=" + model.fjy_f + ",");
			stringBuilder.Append("fjy_f_ok='" + model.fjy_f_ok + "',");
			stringBuilder.Append("fjy_zt='" + model.fjy_zt检验状态 + "',");
			stringBuilder.Append("fjy_md='" + model.fjy_md + "',");
			stringBuilder.Append("fjy_lczd='" + model.fjy_lczd + "',");
			stringBuilder.Append("fhz_id='" + model.fhz_id + "',");
			stringBuilder.Append("fhz_type_id='" + model.fhz_患者类型id + "',");
			stringBuilder.Append("fhz_zyh='" + model.fhz_住院号 + "',");
			stringBuilder.Append("fhz_name='" + model.fhz_姓名 + "',");
			stringBuilder.Append("fhz_sex='" + model.fhz_性别 + "',");
			stringBuilder.Append("fhz_age=" + model.fhz_年龄 + ",");
			stringBuilder.Append("fhz_age_unit='" + model.fhz_年龄单位 + "',");
			stringBuilder.Append("fhz_age_date='" + model.fhz_生日 + "',");
			stringBuilder.Append("fhz_bq='" + model.fhz_病区 + "',");
			stringBuilder.Append("fhz_dept='" + model.fhz_dept患者科室 + "',");
			stringBuilder.Append("fhz_room='" + model.fhz_room + "',");
			stringBuilder.Append("fhz_bed='" + model.fhz_床号 + "',");
			stringBuilder.Append("fhz_volk='" + model.fhz_volk + "',");
			stringBuilder.Append("fhz_hy='" + model.fhz_hy + "',");
			stringBuilder.Append("fhz_job='" + model.fhz_job + "',");
			stringBuilder.Append("fhz_gms='" + model.fhz_gms + "',");
			stringBuilder.Append("fhz_add='" + model.fhz_add + "',");
			stringBuilder.Append("fhz_tel='" + model.fhz_tel + "',");
			stringBuilder.Append("fapply_id='" + model.fapply_申请单ID + "',");
			stringBuilder.Append("fapply_user_id='" + model.fapply_user_id + "',");
			stringBuilder.Append("fapply_dept_id='" + model.fapply_dept_id + "',");
			stringBuilder.Append("fapply_time='" + model.fapply_申请时间 + "',");
			stringBuilder.Append("fsampling_user_id='" + model.fsampling_user_id + "',");
			stringBuilder.Append("fsampling_time='" + model.fsampling_采样时间 + "',");
			stringBuilder.Append("fjy_user_id='" + model.fjy_user_id + "',");
			stringBuilder.Append("fchenk_user_id='" + model.fchenk_审核医师ID + "',");
			stringBuilder.Append("fcheck_time='" + model.fcheck_审核时间 + "',");
			stringBuilder.Append("freport_time='" + model.freport_报告时间 + "',");
			stringBuilder.Append("fprint_time='" + model.fprint_time + "',");
			stringBuilder.Append("fprint_zt='" + model.fprint_zt + "',");
			stringBuilder.Append("fprint_count=" + model.fprint_count + ",");
			stringBuilder.Append("fsys_user='" + model.fsys_用户ID + "',");
			stringBuilder.Append("fsys_time='" + model.fsys_创建时间 + "',");
			stringBuilder.Append("fsys_dept='" + model.fsys_部门ID + "',");
			stringBuilder.Append("fremark='" + model.fremark_备注 + "',");
			stringBuilder.Append("finstr_result_id='" + model.finstr_result_id + "',");
			stringBuilder.Append("f1='" + model.f1 + "',");
			stringBuilder.Append("f2='" + model.f2 + "',");
			stringBuilder.Append("f3='" + model.f3 + "',");
			stringBuilder.Append("f4='" + model.f4 + "',");
			stringBuilder.Append("f5='" + model.f5 + "',");
			stringBuilder.Append("f6='" + model.f6 + "',");
			stringBuilder.Append("f7='" + model.f7 + "',");
			stringBuilder.Append("f8='" + model.f8 + "',");
			stringBuilder.Append("f9='" + model.f9 + "',");
			stringBuilder.Append("f10='" + model.f10 + "',");
			stringBuilder.Append("sfzh='" + model.S身份证号 + "'");
			stringBuilder.Append(" where fjy_id='" + model.fjy_检验id + "' and  fjy_zt='未审核'");
			return stringBuilder.ToString();
		}

		public void Delete(string fjy_id)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("delete SAM_JY ");
			stringBuilder.Append(" where fjy_id='" + fjy_id + "' and fjy_zt='未审核' ");
			int num = WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
			if (num > 0)
			{
				string strSql = "DELETE FROM SAM_JY_RESULT WHERE (fjy_id = '" + fjy_id + "')";
				WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, strSql);
			}
		}

		public int UpdateZT(string fjy_id)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(string.Concat(new string[]
			{
				"update SAM_JY set fjy_zt='未审核',fchenk_user_id='',fcheck_time='',fsys_user='",
				LoginBLL.strPersonID,
				"',fsys_time='",
				DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
				"'"
			}));
			stringBuilder.Append(" where fjy_id='" + fjy_id + "'  ");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public jymodel GetModel(string fjy_id)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("select   ");
			stringBuilder.Append(" fjy_id,fjy_date,fjy_instr,fjy_yb_tm,fjy_yb_code,fjy_yb_type,fjy_sf_type,fjy_f,fjy_f_ok,fjy_zt,fjy_md,fjy_lczd,fhz_id,fhz_type_id,fhz_zyh,fhz_name,fhz_sex,fhz_age,fhz_age_unit,fhz_age_date,fhz_bq,fhz_dept,fhz_room,fhz_bed,fhz_volk,fhz_hy,fhz_job,fhz_gms,fhz_add,fhz_tel,fapply_id,fapply_user_id,fapply_dept_id,fapply_time,fsampling_user_id,fsampling_time,fjy_user_id,fchenk_user_id,fcheck_time,freport_time,fprint_time,fprint_zt,fprint_count,fsys_user,fsys_time,fsys_dept,fremark,f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,sfzh ");
			stringBuilder.Append(" from SAM_JY ");
			stringBuilder.Append(" where fjy_id='" + fjy_id + "' ");
			jymodel jymodel = new jymodel();
			DataSet dataSet = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
			jymodel result;
			if (dataSet.Tables[0].Rows.Count > 0)
			{
				jymodel.fjy_检验id = dataSet.Tables[0].Rows[0]["fjy_id"].ToString();
				jymodel.fjy_检验日期 = dataSet.Tables[0].Rows[0]["fjy_date"].ToString();
				jymodel.fjy_仪器ID = dataSet.Tables[0].Rows[0]["fjy_instr"].ToString();
				jymodel.fjy_yb_tm = dataSet.Tables[0].Rows[0]["fjy_yb_tm"].ToString();
				jymodel.fjy_yb_code = dataSet.Tables[0].Rows[0]["fjy_yb_code"].ToString();
				jymodel.fjy_yb_type = dataSet.Tables[0].Rows[0]["fjy_yb_type"].ToString();
				jymodel.fjy_收费类型ID = dataSet.Tables[0].Rows[0]["fjy_sf_type"].ToString();
				if (dataSet.Tables[0].Rows[0]["fjy_f"].ToString() != "")
				{
					jymodel.fjy_f = decimal.Parse(dataSet.Tables[0].Rows[0]["fjy_f"].ToString());
				}
				jymodel.fjy_f_ok = dataSet.Tables[0].Rows[0]["fjy_f_ok"].ToString();
				jymodel.fjy_zt检验状态 = dataSet.Tables[0].Rows[0]["fjy_zt"].ToString();
				jymodel.fjy_md = dataSet.Tables[0].Rows[0]["fjy_md"].ToString();
				jymodel.fjy_lczd = dataSet.Tables[0].Rows[0]["fjy_lczd"].ToString();
				jymodel.fhz_id = dataSet.Tables[0].Rows[0]["fhz_id"].ToString();
				jymodel.fhz_患者类型id = dataSet.Tables[0].Rows[0]["fhz_type_id"].ToString();
				jymodel.fhz_住院号 = dataSet.Tables[0].Rows[0]["fhz_zyh"].ToString();
				jymodel.fhz_姓名 = dataSet.Tables[0].Rows[0]["fhz_name"].ToString();
				jymodel.fhz_性别 = dataSet.Tables[0].Rows[0]["fhz_sex"].ToString();
				if (dataSet.Tables[0].Rows[0]["fhz_age"].ToString() != "")
				{
					jymodel.fhz_年龄 = decimal.Parse(dataSet.Tables[0].Rows[0]["fhz_age"].ToString());
				}
				jymodel.fhz_年龄单位 = dataSet.Tables[0].Rows[0]["fhz_age_unit"].ToString();
				jymodel.fhz_生日 = dataSet.Tables[0].Rows[0]["fhz_age_date"].ToString();
				jymodel.fhz_病区 = dataSet.Tables[0].Rows[0]["fhz_bq"].ToString();
				jymodel.fhz_dept患者科室 = dataSet.Tables[0].Rows[0]["fhz_dept"].ToString();
				jymodel.fhz_room = dataSet.Tables[0].Rows[0]["fhz_room"].ToString();
				jymodel.fhz_床号 = dataSet.Tables[0].Rows[0]["fhz_bed"].ToString();
				jymodel.fhz_volk = dataSet.Tables[0].Rows[0]["fhz_volk"].ToString();
				jymodel.fhz_hy = dataSet.Tables[0].Rows[0]["fhz_hy"].ToString();
				jymodel.fhz_job = dataSet.Tables[0].Rows[0]["fhz_job"].ToString();
				jymodel.fhz_gms = dataSet.Tables[0].Rows[0]["fhz_gms"].ToString();
				jymodel.fhz_add = dataSet.Tables[0].Rows[0]["fhz_add"].ToString();
				jymodel.fhz_tel = dataSet.Tables[0].Rows[0]["fhz_tel"].ToString();
				jymodel.fapply_申请单ID = dataSet.Tables[0].Rows[0]["fapply_id"].ToString();
				jymodel.fapply_user_id = dataSet.Tables[0].Rows[0]["fapply_user_id"].ToString();
				jymodel.fapply_dept_id = dataSet.Tables[0].Rows[0]["fapply_dept_id"].ToString();
				jymodel.fapply_申请时间 = dataSet.Tables[0].Rows[0]["fapply_time"].ToString();
				jymodel.fsampling_user_id = dataSet.Tables[0].Rows[0]["fsampling_user_id"].ToString();
				jymodel.fsampling_采样时间 = dataSet.Tables[0].Rows[0]["fsampling_time"].ToString();
				jymodel.fjy_user_id = dataSet.Tables[0].Rows[0]["fjy_user_id"].ToString();
				jymodel.fchenk_审核医师ID = dataSet.Tables[0].Rows[0]["fchenk_user_id"].ToString();
				jymodel.fcheck_审核时间 = dataSet.Tables[0].Rows[0]["fcheck_time"].ToString();
				jymodel.freport_报告时间 = dataSet.Tables[0].Rows[0]["freport_time"].ToString();
				jymodel.fprint_time = dataSet.Tables[0].Rows[0]["fprint_time"].ToString();
				jymodel.fprint_zt = dataSet.Tables[0].Rows[0]["fprint_zt"].ToString();
				if (dataSet.Tables[0].Rows[0]["fprint_count"].ToString() != "")
				{
					jymodel.fprint_count = int.Parse(dataSet.Tables[0].Rows[0]["fprint_count"].ToString());
				}
				jymodel.fsys_用户ID = dataSet.Tables[0].Rows[0]["fsys_user"].ToString();
				jymodel.fsys_创建时间 = dataSet.Tables[0].Rows[0]["fsys_time"].ToString();
				jymodel.fsys_部门ID = dataSet.Tables[0].Rows[0]["fsys_dept"].ToString();
				jymodel.fremark_备注 = dataSet.Tables[0].Rows[0]["fremark"].ToString();
				jymodel.f1 = dataSet.Tables[0].Rows[0]["f1"].ToString();
				jymodel.f2 = dataSet.Tables[0].Rows[0]["f2"].ToString();
				jymodel.f3 = dataSet.Tables[0].Rows[0]["f3"].ToString();
				jymodel.f4 = dataSet.Tables[0].Rows[0]["f4"].ToString();
				jymodel.f5 = dataSet.Tables[0].Rows[0]["f5"].ToString();
				jymodel.f6 = dataSet.Tables[0].Rows[0]["f6"].ToString();
				jymodel.f7 = dataSet.Tables[0].Rows[0]["f7"].ToString();
				jymodel.f8 = dataSet.Tables[0].Rows[0]["f8"].ToString();
				jymodel.f9 = dataSet.Tables[0].Rows[0]["f9"].ToString();
				jymodel.f10 = dataSet.Tables[0].Rows[0]["f10"].ToString();
				jymodel.S身份证号 = dataSet.Tables[0].Rows[0]["sfzh"].ToString();
				result = jymodel;
			}
			else
			{
				result = null;
			}
			return result;
		}

		public jymodel GetModel(string strDate, string instrID, int int样本号)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("select fjy_id,fjy_date,fjy_instr,fjy_yb_tm,fjy_yb_code,fjy_yb_type,fjy_sf_type,fjy_f,fjy_f_ok,fjy_zt,fjy_md,fjy_lczd,fhz_id,fhz_type_id,fhz_zyh,fhz_name,fhz_sex,fhz_age,fhz_age_unit,fhz_age_date,fhz_bq,fhz_dept,fhz_room,fhz_bed,fhz_volk,fhz_hy,fhz_job,fhz_gms,fhz_add,fhz_tel,fapply_id,fapply_user_id,fapply_dept_id,fapply_time,fsampling_user_id,fsampling_time,fjy_user_id,fchenk_user_id,fcheck_time,freport_time,fprint_time,fprint_zt,fprint_count,fsys_user,fsys_time,fsys_dept,fremark,f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,sfzh ");
			stringBuilder.Append(" from SAM_JY ");
			stringBuilder.Append(" where fjy_date=@testDate and fjy_instr=@instrID and fjy_yb_code=@testCode");
			SqlParameter[] paramsList = new SqlParameter[]
			{
				new SqlParameter("@testDate", strDate),
				new SqlParameter("@instrID", instrID),
				new SqlParameter("@testCode", int样本号.ToString())
			};
			DataSet dataSet = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, stringBuilder.ToString(), paramsList);
			jymodel result;
			if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
			{
				jymodel jymodel = new jymodel();
				jymodel.fjy_检验id = dataSet.Tables[0].Rows[0]["fjy_id"].ToString();
				jymodel.fjy_检验日期 = dataSet.Tables[0].Rows[0]["fjy_date"].ToString();
				jymodel.fjy_仪器ID = dataSet.Tables[0].Rows[0]["fjy_instr"].ToString();
				jymodel.fjy_yb_tm = dataSet.Tables[0].Rows[0]["fjy_yb_tm"].ToString();
				jymodel.fjy_yb_code = dataSet.Tables[0].Rows[0]["fjy_yb_code"].ToString();
				jymodel.fjy_yb_type = dataSet.Tables[0].Rows[0]["fjy_yb_type"].ToString();
				jymodel.fjy_收费类型ID = dataSet.Tables[0].Rows[0]["fjy_sf_type"].ToString();
				if (dataSet.Tables[0].Rows[0]["fjy_f"].ToString() != "")
				{
					jymodel.fjy_f = decimal.Parse(dataSet.Tables[0].Rows[0]["fjy_f"].ToString());
				}
				jymodel.fjy_f_ok = dataSet.Tables[0].Rows[0]["fjy_f_ok"].ToString();
				jymodel.fjy_zt检验状态 = dataSet.Tables[0].Rows[0]["fjy_zt"].ToString();
				jymodel.fjy_md = dataSet.Tables[0].Rows[0]["fjy_md"].ToString();
				jymodel.fjy_lczd = dataSet.Tables[0].Rows[0]["fjy_lczd"].ToString();
				jymodel.fhz_id = dataSet.Tables[0].Rows[0]["fhz_id"].ToString();
				jymodel.fhz_患者类型id = dataSet.Tables[0].Rows[0]["fhz_type_id"].ToString();
				jymodel.fhz_住院号 = dataSet.Tables[0].Rows[0]["fhz_zyh"].ToString();
				jymodel.fhz_姓名 = dataSet.Tables[0].Rows[0]["fhz_name"].ToString();
				jymodel.fhz_性别 = dataSet.Tables[0].Rows[0]["fhz_sex"].ToString();
				if (dataSet.Tables[0].Rows[0]["fhz_age"].ToString() != "")
				{
					jymodel.fhz_年龄 = decimal.Parse(dataSet.Tables[0].Rows[0]["fhz_age"].ToString());
				}
				jymodel.fhz_年龄单位 = dataSet.Tables[0].Rows[0]["fhz_age_unit"].ToString();
				jymodel.fhz_生日 = dataSet.Tables[0].Rows[0]["fhz_age_date"].ToString();
				jymodel.fhz_病区 = dataSet.Tables[0].Rows[0]["fhz_bq"].ToString();
				jymodel.fhz_dept患者科室 = dataSet.Tables[0].Rows[0]["fhz_dept"].ToString();
				jymodel.fhz_room = dataSet.Tables[0].Rows[0]["fhz_room"].ToString();
				jymodel.fhz_床号 = dataSet.Tables[0].Rows[0]["fhz_bed"].ToString();
				jymodel.fhz_volk = dataSet.Tables[0].Rows[0]["fhz_volk"].ToString();
				jymodel.fhz_hy = dataSet.Tables[0].Rows[0]["fhz_hy"].ToString();
				jymodel.fhz_job = dataSet.Tables[0].Rows[0]["fhz_job"].ToString();
				jymodel.fhz_gms = dataSet.Tables[0].Rows[0]["fhz_gms"].ToString();
				jymodel.fhz_add = dataSet.Tables[0].Rows[0]["fhz_add"].ToString();
				jymodel.fhz_tel = dataSet.Tables[0].Rows[0]["fhz_tel"].ToString();
				jymodel.fapply_申请单ID = dataSet.Tables[0].Rows[0]["fapply_id"].ToString();
				jymodel.fapply_user_id = dataSet.Tables[0].Rows[0]["fapply_user_id"].ToString();
				jymodel.fapply_dept_id = dataSet.Tables[0].Rows[0]["fapply_dept_id"].ToString();
				jymodel.fapply_申请时间 = dataSet.Tables[0].Rows[0]["fapply_time"].ToString();
				jymodel.fsampling_user_id = dataSet.Tables[0].Rows[0]["fsampling_user_id"].ToString();
				jymodel.fsampling_采样时间 = dataSet.Tables[0].Rows[0]["fsampling_time"].ToString();
				jymodel.fjy_user_id = dataSet.Tables[0].Rows[0]["fjy_user_id"].ToString();
				jymodel.fchenk_审核医师ID = dataSet.Tables[0].Rows[0]["fchenk_user_id"].ToString();
				jymodel.fcheck_审核时间 = dataSet.Tables[0].Rows[0]["fcheck_time"].ToString();
				jymodel.freport_报告时间 = dataSet.Tables[0].Rows[0]["freport_time"].ToString();
				jymodel.fprint_time = dataSet.Tables[0].Rows[0]["fprint_time"].ToString();
				jymodel.fprint_zt = dataSet.Tables[0].Rows[0]["fprint_zt"].ToString();
				if (dataSet.Tables[0].Rows[0]["fprint_count"].ToString() != "")
				{
					jymodel.fprint_count = int.Parse(dataSet.Tables[0].Rows[0]["fprint_count"].ToString());
				}
				jymodel.fsys_用户ID = dataSet.Tables[0].Rows[0]["fsys_user"].ToString();
				jymodel.fsys_创建时间 = dataSet.Tables[0].Rows[0]["fsys_time"].ToString();
				jymodel.fsys_部门ID = dataSet.Tables[0].Rows[0]["fsys_dept"].ToString();
				jymodel.fremark_备注 = dataSet.Tables[0].Rows[0]["fremark"].ToString();
				jymodel.f1 = dataSet.Tables[0].Rows[0]["f1"].ToString();
				jymodel.f2 = dataSet.Tables[0].Rows[0]["f2"].ToString();
				jymodel.f3 = dataSet.Tables[0].Rows[0]["f3"].ToString();
				jymodel.f4 = dataSet.Tables[0].Rows[0]["f4"].ToString();
				jymodel.f5 = dataSet.Tables[0].Rows[0]["f5"].ToString();
				jymodel.f6 = dataSet.Tables[0].Rows[0]["f6"].ToString();
				jymodel.f7 = dataSet.Tables[0].Rows[0]["f7"].ToString();
				jymodel.f8 = dataSet.Tables[0].Rows[0]["f8"].ToString();
				jymodel.f9 = dataSet.Tables[0].Rows[0]["f9"].ToString();
				jymodel.f10 = dataSet.Tables[0].Rows[0]["f10"].ToString();
				jymodel.S身份证号 = dataSet.Tables[0].Rows[0]["sfzh"].ToString();
				result = jymodel;
			}
			else
			{
				result = null;
			}
			return result;
		}

		public DataTable GetList(string strWhere)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("select (SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '病人类别' and z.fcode=t.fhz_type_id)) as 类型,(SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '性别' and z.fcode=t.fhz_sex)) as 性别,(SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '年龄单位' and z.fcode=t.fhz_age_unit)) as 年龄单位,(SELECT top 1 z.fname FROM WWF_DEPT z WHERE (z.fdept_id=t.fhz_dept)) as 科室,(SELECT top 1 z.fname FROM SAM_SAMPLE_TYPE z WHERE (z.fsample_type_id=t.fjy_yb_type)) as 样本,(SELECT top 1 z.fname FROM WWF_PERSON z WHERE (z.fperson_id=t.fjy_user_id)) as 检验医师,(SELECT top 1 z.fname FROM WWF_PERSON z WHERE (z.fperson_id=t.fapply_user_id)) as 申请医师,(select top 1 z.fname from SAM_DISEASE z where z.fcode = t.fjy_lczd) as 疾病名称,(select top 1 z.fname from WWF_PERSON Z WHERE (Z.fperson_id=t.fchenk_user_id)) 审核者,t.* ");
			stringBuilder.Append(" FROM SAM_JY t");
			if (strWhere.Trim() != "")
			{
				stringBuilder.Append(" where " + strWhere);
			}
			return WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public DataTable GetListALL(string strInsrt, DateTime date, string strSubWhere)
		{
			StringBuilder stringBuilder = new StringBuilder();
			string text = date.ToString("yyyy-MM-dd");
			string text2 = date.AddDays(1.0).ToString("yyyy-MM-dd");
			stringBuilder.Append(string.Concat(new string[]
			{
				"select * from \r\n(\r\nselect (SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '病人类别' and z.fcode=t.fhz_type_id)) as 类型,(SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '性别' and z.fcode=t.fhz_sex)) as 性别,(SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '年龄单位' and z.fcode=t.fhz_age_unit)) as 年龄单位,(SELECT top 1 z.fname FROM WWF_DEPT z WHERE (z.fdept_id=t.fhz_dept)) as 科室,(SELECT top 1 z.fname FROM SAM_SAMPLE_TYPE z WHERE (z.fsample_type_id=t.fjy_yb_type)) as 样本,(SELECT top 1 z.fname FROM WWF_PERSON z WHERE (z.fperson_id=t.fjy_user_id)) as 检验医师,(SELECT top 1 z.fname FROM WWF_PERSON z WHERE (z.fperson_id=t.fapply_user_id)) as 申请医师,(select top 1 z.fname from SAM_DISEASE z where z.fcode = t.fjy_lczd) as 疾病名称,t.*\r\n FROM SAM_JY t\r\n where fjy_date = '",
				text,
				"' and fjy_instr = '",
				strInsrt,
				"'\r\n union all \r\n select '' 类型,'' as 性别,'' 年龄单位,'' 科室,'' 样本,'' 检验医师,'' 申请医师,'' 疾病名称\r\n\t  ,'' [fjy_id]\r\n      ,CONVERT(varchar(20),[FDateTime],23) [fjy_date]\r\n      ,[FInstrID] [fjy_instr]\r\n      ,[FResultID] [fjy_yb_code]\r\n      ,'' [fjy_yb_tm]\r\n      ,'' [fjy_yb_type]\r\n      ,'' [fjy_sf_type]\r\n      ,'' [fjy_f]\r\n      ,'' [fjy_f_ok]\r\n      ,'未审核' [fjy_zt]\r\n      ,'' [fjy_md]\r\n      ,'' [fjy_lczd]\r\n      ,'' [fhz_id]\r\n      ,'' [fhz_type_id]\r\n      ,'' [fhz_zyh]\r\n      ,'' [fhz_name]\r\n      ,'' [fhz_sex]\r\n      ,'' [fhz_age]\r\n      ,'' [fhz_age_unit]\r\n      ,'' [fhz_age_date]\r\n      ,'' [fhz_bq]\r\n      ,'' [fhz_dept]\r\n      ,'' [fhz_room]\r\n      ,'' [fhz_bed]\r\n      ,'' [fhz_volk]\r\n      ,'' [fhz_hy]\r\n      ,'' [fhz_job]\r\n      ,'' [fhz_gms]\r\n      ,'' [fhz_add]\r\n      ,'' [fhz_tel]\r\n      ,'' [fapply_id]\r\n      ,'' [fapply_user_id]\r\n      ,'' [fapply_dept_id]\r\n      ,'' [fapply_time]\r\n      ,'' [fsampling_user_id]\r\n      ,'' [fsampling_time]\r\n      ,'' [fjy_user_id]\r\n      ,'' [fchenk_user_id]\r\n      ,'' [fcheck_time]\r\n      ,'' [freport_time]\r\n      ,'' [fprint_time]\r\n      ,'未打印' [fprint_zt]\r\n      ,0 [fprint_count]\r\n      ,'' [fsys_user]\r\n      ,'' [fsys_time]\r\n      ,'' [fsys_dept]\r\n      ,'' [fremark]\r\n      ,ftaskid [finstr_result_id]\r\n      ,'' [f1]\r\n      ,'' [f2]\r\n      ,'' [f3]\r\n      ,'' [f4]\r\n      ,'' [f5]\r\n      ,'' [f6]\r\n      ,'' [f7]\r\n      ,'' [f8]\r\n      ,'' [f9]\r\n      ,'' [f10]\r\n      ,'' [sfzh]\r\n FROM Lis_Ins_Result b\r\n  where [FDateTime] >= '",
				text,
				"' and [FDateTime] < '",
				text2,
				"'  and [FInstrID] = '",
				strInsrt,
				"'\r\n  and FTaskID not in ( select finstr_result_id FROM SAM_JY where fjy_date = '",
				text,
				"' and fjy_instr = '",
				strInsrt,
				"')\r\n\r\n) aaaa\r\nwhere 1=1 --and (ISNUMERIC(fjy_yb_code)=1 AND ROUND(fjy_yb_code,0)=fjy_yb_code) \r\n"
			}));
			if (!string.IsNullOrWhiteSpace(strSubWhere))
			{
				stringBuilder.Append(strSubWhere);
			}
			stringBuilder.Append(" Order by convert(int,fjy_yb_code)");
			return WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public DataTable GetList比较(string strWhere)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT     TOP (4) fjy_id, fjy_date, fjy_instr, fjy_yb_code  ");
			stringBuilder.Append(" FROM SAM_JY  ");
			if (strWhere.Trim() != "")
			{
				stringBuilder.Append(" where " + strWhere);
			}
			return WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public DataTable GetList比较值(string strfjy_id)
		{
			string strSql = "SELECT '' as 上一次,'' as 上二次,'' as 上三次,fitem_id AS 项目id, fitem_name AS 项目, fvalue AS 项目值 FROM    SAM_JY_RESULT WHERE     (fjy_id = '" + strfjy_id + "')  ORDER BY fjy_id, forder_by";
			return WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, strSql);
		}

		public string AddS(jyresultmodel model)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("insert into SAM_JY_RESULT(");
			stringBuilder.Append("fresult_id,fjy_id,fapply_id,fitem_id,fitem_code,fitem_name,fitem_unit,fitem_ref,fitem_badge,forder_by,fvalue,fod,fcutoff,fremark");
			stringBuilder.Append(")");
			stringBuilder.Append(" values (");
			stringBuilder.Append("'" + model.fresult_id + "',");
			stringBuilder.Append("'" + model.fjy_id + "',");
			stringBuilder.Append("'" + model.fapply_id + "',");
			stringBuilder.Append("'" + model.fitem_id + "',");
			stringBuilder.Append("'" + model.fitem_code + "',");
			stringBuilder.Append("'" + model.fitem_name + "',");
			stringBuilder.Append("'" + model.fitem_unit + "',");
			stringBuilder.Append("'" + model.fitem_ref + "',");
			stringBuilder.Append("'" + model.fitem_badge + "',");
			stringBuilder.Append("'" + model.forder_by + "',");
			stringBuilder.Append("'" + model.fvalue + "',");
			stringBuilder.Append("'" + model.fod + "',");
			stringBuilder.Append("'" + model.fcutoff + "',");
			stringBuilder.Append("'" + model.fremark + "'");
			stringBuilder.Append(")");
			return stringBuilder.ToString();
		}

		public string UpdateS(jyresultmodel model)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update SAM_JY_RESULT set ");
			stringBuilder.Append("fjy_id='" + model.fjy_id + "',");
			stringBuilder.Append("fapply_id='" + model.fapply_id + "',");
			stringBuilder.Append("fitem_id='" + model.fitem_id + "',");
			stringBuilder.Append("fitem_code='" + model.fitem_code + "',");
			stringBuilder.Append("fitem_name='" + model.fitem_name + "',");
			stringBuilder.Append("fitem_unit='" + model.fitem_unit + "',");
			stringBuilder.Append("fitem_ref='" + model.fitem_ref + "',");
			stringBuilder.Append("fitem_badge='" + model.fitem_badge + "',");
			stringBuilder.Append("forder_by='" + model.forder_by + "',");
			stringBuilder.Append("fvalue='" + model.fvalue + "',");
			stringBuilder.Append("fod='" + model.fod + "',");
			stringBuilder.Append("fcutoff='" + model.fcutoff + "',");
			stringBuilder.Append("fremark='" + model.fremark + "'");
			stringBuilder.Append(" where fresult_id='" + model.fresult_id + "' ");
			return stringBuilder.ToString();
		}

		public void DeleteS(string fresult_id)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("delete SAM_JY_RESULT ");
			stringBuilder.Append(" where fresult_id='" + fresult_id + "' ");
			WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public string DeleteByJyid(string strfjy_id)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("delete SAM_JY_RESULT ");
			stringBuilder.Append(" where fjy_id='" + strfjy_id + "' ");
			return stringBuilder.ToString();
		}

		public DataTable GetListS(string strWhere)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("select t.* ");
			stringBuilder.Append(" FROM SAM_JY_RESULT t");
			if (strWhere.Trim() != "")
			{
				stringBuilder.Append(" where " + strWhere);
			}
			stringBuilder.Append(" order by (SELECT top 1 convert(int,z.fprint_num)  FROM   SAM_ITEM z WHERE (z.fitem_id = t.fitem_id))");
			return WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public DataTable Get检验结果View(string str申请单号, string str检验日期, string str设备ID, string str样本号)
		{
			string text = "SELECT 申请单号,检验日期,检验设备,样本号,病人ID,住院号,病人姓名,年龄,身份证,检验ID,检验编码,检验名称,单位,参考值,升降,检验值,排序  ";
			string text2 = text;
			text = string.Concat(new string[]
			{
				text2,
				"  FROM VW_检验结果   WHERE 申请单号 = '",
				str申请单号,
				"' and 检验日期='",
				str检验日期,
				"' and 检验设备 ='",
				str设备ID,
				"' and 样本号='",
				str样本号,
				"'"
			});
			return WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, text);
		}

		public DataTable Get检验结果ViewBy门诊住院号(string str门诊住院号, string str检验日期, string str设备ID, string str样本号)
		{
			string text = "SELECT 申请单号,检验日期,检验设备,样本号,病人ID,住院号,病人姓名,年龄,身份证,检验ID,检验编码,检验名称,单位,参考值,升降,检验值,排序  ";
			string text2 = text;
			text = string.Concat(new string[]
			{
				text2,
				"  FROM VW_检验结果   WHERE 住院号 = '",
				str门诊住院号,
				"' and 检验日期='",
				str检验日期,
				"' and 检验设备 ='",
				str设备ID,
				"' and 样本号='",
				str样本号,
				"'"
			});
			return WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, text);
		}

		public bool IsExistInHis(string str申请单号, string str检验日期, string str设备ID, string str样本号)
		{
			string sql = string.Concat(new string[]
			{
				"SELECT count(*) FROM JY检验结果 WHERE 申请单号 = '",
				str申请单号,
				"' and 检验日期='",
				str检验日期,
				"' and 检验设备 ='",
				str设备ID,
				"' and 样本号='",
				str样本号,
				"'"
			});
			return WWFInit.wwfRemotingDao.DbRecordExists(WWFInit.strCHisDBConn, sql);
		}

		public string Bll参考值(string strItem_id, int intfage, string strfsex, string strfsample_type_id)
		{
			string result = "";
			DataTable dataTable = WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, "SELECT * FROM SAM_ITEM WHERE (fitem_id = '" + strItem_id + "')");
			if (dataTable.Rows.Count > 0)
			{
				DataRow dataRow = dataTable.Rows[0];
				result = dataRow["fref"].ToString();
				string text = dataRow["fitem_id"].ToString();
				string text2 = dataRow["fname"].ToString();
				string a = dataRow["fref_if_age"].ToString();
				string a2 = dataRow["fref_if_sex"].ToString();
				string a3 = dataRow["fref_if_sample"].ToString();
				string a4 = dataRow["fref_if_method"].ToString();
				result = dataRow["fref"].ToString();
				if (a == "1" || a2 == "0" || a3 == "0" || a4 == "0")
				{
					DataTable dataTable2 = WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, "SELECT * FROM SAM_ITEM_REF WHERE (fitem_id = '" + strItem_id + "')");
					string filterExpression = "";
					if (a == "1" & a2 == "0" & a3 == "0")
					{
						filterExpression = string.Concat(new object[]
						{
							"(",
							intfage,
							">=fage_low) AND (",
							intfage,
							"<fage_high)"
						});
					}
					if (a == "1" & a2 == "1" & a3 == "0")
					{
						filterExpression = string.Concat(new object[]
						{
							"('",
							strfsex,
							"'=fsex) AND (",
							intfage,
							">=fage_low) AND (",
							intfage,
							"<fage_high)"
						});
					}
					if (a == "1" & a2 == "1" & a3 == "1")
					{
						filterExpression = string.Concat(new object[]
						{
							"('",
							strfsample_type_id,
							"'=fsample_type_id) AND ('",
							strfsex,
							"'=fsex) AND (",
							intfage,
							">=fage_low) AND (",
							intfage,
							"<fage_high)"
						});
					}
					if (a == "0" & a2 == "1" & a3 == "0")
					{
						filterExpression = "('" + strfsex + "'=fsex)";
					}
					if (a == "0" & a2 == "1" & a3 == "1")
					{
						filterExpression = string.Concat(new string[]
						{
							"('",
							strfsample_type_id,
							"'=fsample_type_id) AND ('",
							strfsex,
							"'=fsex)"
						});
					}
					if (a == "0" & a2 == "0" & a3 == "1")
					{
						filterExpression = "('" + strfsample_type_id + "'=fsample_type_id)";
					}
					if (a == "0" & a2 == "1" & a3 == "1")
					{
						filterExpression = string.Concat(new string[]
						{
							"('",
							strfsample_type_id,
							"'=fsample_type_id) AND ('",
							strfsex,
							"'=fsex)"
						});
					}
					DataRow[] array = dataTable2.Select(filterExpression);
					if (array.Length > 0)
					{
						DataRow[] array2 = array;
						for (int i = 0; i < array2.Length; i++)
						{
							DataRow dataRow2 = array2[i];
							result = dataRow2["fref"].ToString();
						}
					}
				}
			}
			return result;
		}

		public string Bll参考值New(string strItem_id, float intfage, string strfsex, string strfsample_type_id)
		{
			string result = "";
			DataTable dataTable = WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, "SELECT fitem_id,fname,fref,fref_if_age,fref_if_sex,fref_if_sample,fref_if_method FROM SAM_ITEM WHERE (fitem_id = '" + strItem_id + "')");
			if (dataTable.Rows.Count > 0)
			{
				DataRow dataRow = dataTable.Rows[0];
				result = dataRow["fref"].ToString();
				string text = dataRow["fitem_id"].ToString();
				string text2 = dataRow["fname"].ToString();
				string a = dataRow["fref_if_age"].ToString();
				string a2 = dataRow["fref_if_sex"].ToString();
				string a3 = dataRow["fref_if_sample"].ToString();
				string a4 = dataRow["fref_if_method"].ToString();
				result = dataRow["fref"].ToString();
				if (a == "1" || a2 == "1" || a3 == "1" || a4 == "1")
				{
					DataTable dataTable2 = WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, "SELECT * FROM SAM_ITEM_REF WHERE (fitem_id = '" + strItem_id + "')");
					string filterExpression = "";
					if (a == "1" & a2 == "0" & a3 == "0")
					{
						filterExpression = string.Concat(new object[]
						{
							"(",
							intfage,
							">=fage_low) AND (",
							intfage,
							"<fage_high)"
						});
					}
					else if (a == "1" & a2 == "1" & a3 == "0")
					{
						filterExpression = string.Concat(new object[]
						{
							"('",
							strfsex,
							"'=fsex) AND (",
							intfage,
							">=fage_low) AND (",
							intfage,
							"<fage_high)"
						});
					}
					else if (a == "1" & a2 == "1" & a3 == "1")
					{
						filterExpression = string.Concat(new object[]
						{
							"('",
							strfsample_type_id,
							"'=fsample_type_id) AND ('",
							strfsex,
							"'=fsex) AND (",
							intfage,
							">=fage_low) AND (",
							intfage,
							"<fage_high)"
						});
					}
					else if (a == "0" & a2 == "1" & a3 == "0")
					{
						filterExpression = "('" + strfsex + "'=fsex)";
					}
					else if (a == "0" & a2 == "1" & a3 == "1")
					{
						filterExpression = string.Concat(new string[]
						{
							"('",
							strfsample_type_id,
							"'=fsample_type_id) AND ('",
							strfsex,
							"'=fsex)"
						});
					}
					else if (a == "0" & a2 == "0" & a3 == "1")
					{
						filterExpression = "('" + strfsample_type_id + "'=fsample_type_id)";
					}
					else if (a == "0" & a2 == "1" & a3 == "1")
					{
						filterExpression = string.Concat(new string[]
						{
							"('",
							strfsample_type_id,
							"'=fsample_type_id) AND ('",
							strfsex,
							"'=fsex)"
						});
					}
					DataRow[] array = dataTable2.Select(filterExpression);
					if (array.Length > 0)
					{
						DataRow[] array2 = array;
						for (int i = 0; i < array2.Length; i++)
						{
							DataRow dataRow2 = array2[i];
							result = dataRow2["fref"].ToString();
						}
					}
				}
			}
			return result;
		}

		public string Bll结果标记(string fvalue, string fref)
		{
			string result = "";
			string[] array = Regex.Split(fref, "--");
			if (Public.IsNumber(fvalue))
			{
				double num = Convert.ToDouble(fvalue);
				if (array.Length == 2)
				{
					if (Public.IsNumber(array[0].ToString()) & Public.IsNumber(array[1].ToString()))
					{
						if (num < Convert.ToDouble(array[0].ToString()))
						{
							result = "↓";
						}
						if (num > Convert.ToDouble(array[1].ToString()))
						{
							result = "↑";
						}
					}
				}
			}
			return result;
		}

		public DataTable BllImgDT(string strfsample_id, string strInsFTaskID)
		{
			string strSql = "SELECT  FImgID, FTaskID, FImg, FImgType, FImgNmae, FOrder, FRemark, FImgStroma, FImgModal FROM    Lis_Ins_Result_Img WHERE     (FTaskID = '" + strInsFTaskID + "') ORDER BY FOrder";
			return WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, strSql).Tables[0];
		}

		public int BllReportUpdatePrintTime(string fsample_id)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update SAM_JY set ");
			stringBuilder.Append("fprint_time='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "',");
			stringBuilder.Append("fprint_count=fprint_count+1,");
			stringBuilder.Append("fprint_zt='已打印',");
			stringBuilder.Append("fsys_user='" + LoginBLL.strPersonID + "'");
			stringBuilder.Append(" where fjy_id='" + fsample_id + "'");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public int BllReportUpdate审核状态(string fsample_id)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("update SAM_JY set ");
			stringBuilder.Append("fcheck_time='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "',");
			stringBuilder.Append("fjy_zt='已审核',");
			stringBuilder.Append("fchenk_user_id='" + LoginBLL.strPersonID + "'");
			stringBuilder.Append(" where fjy_id='" + fsample_id + "' and fjy_zt != '已审核' ");
			return WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strDBConn, stringBuilder.ToString());
		}

		public DataTable Get检验ID(string str日期, string str设备id, int intBegin样本号, int intEnd样本号, out string strErrorMsg)
		{
			DataTable result = null;
			strErrorMsg = "";
			try
			{
				List<SqlParameter> list = new List<SqlParameter>();
				string text = "SELECT [fjy_id],fjy_instr,fjy_yb_code,fjy_zt,fprint_zt,fapply_id FROM [SAM_JY] where  [fjy_date] = @testDate and [fjy_instr] = @Instr and fjy_zt='已审核' ";
				list.Add(new SqlParameter("@testDate", str日期));
				list.Add(new SqlParameter("@Instr", str设备id));
				if (intBegin样本号 >= 0)
				{
					text += " and cast([fjy_yb_code] as int) >= @BeginResultNo ";
					list.Add(new SqlParameter("@BeginResultNo", intBegin样本号));
				}
				if (intEnd样本号 >= 0)
				{
					text += " and cast([fjy_yb_code] as int) <= @EndResultNo ";
					list.Add(new SqlParameter("@EndResultNo", intEnd样本号));
				}
				text += " order by cast([fjy_yb_code] as int) ";
				DataSet dataSet = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn, text, list.ToArray());
				if (dataSet != null && dataSet.Tables.Count > 0)
				{
					result = dataSet.Tables[0];
				}
				else
				{
					strErrorMsg = "没有获取到样本数据！";
				}
			}
			catch (Exception ex)
			{
				result = null;
				strErrorMsg = ex.Message;
			}
			return result;
		}

		public void Del化验结果FromHis(string str申请单号, string str日期, string str设备ID, string str样本号)
		{
			bool flag = false;
			try
			{
				string strSql = "select top 1 参数值 from 全局参数 where 参数名称='结果回写HIS'";
				DataTable dataTable = WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, strSql);
				flag = (dataTable != null && dataTable.Rows.Count > 0 && dataTable.Rows[0]["参数值"].ToString().Equals("true"));
			}
			catch
			{
				flag = false;
			}
			bool flag2 = flag;
			if (flag2 && !string.IsNullOrWhiteSpace(str申请单号))
			{
				string strSql2 = "DELETE FROM [JY检验结果] WHERE 申请单号 = @applyid and 检验日期 =@testDate and 检验设备 = @InstrID and 样本号 =@SampleID ";
				SqlParameter[] sqlParams = new SqlParameter[]
				{
					new SqlParameter("@applyid", str申请单号),
					new SqlParameter("@testDate", str日期),
					new SqlParameter("@InstrID", str设备ID),
					new SqlParameter("@SampleID", str样本号)
				};
				WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strCHisDBConn, strSql2, sqlParams);
			}
		}

		public void WriteDataToHis(string str申请单号, string strDate, string str设备ID, string str样本号)
		{
			bool flag = false;
			try
			{
				string strSql = "select top 1 参数值 from 全局参数 where 参数名称='结果回写HIS'";
				DataTable dataTable = WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, strSql);
				flag = (dataTable != null && dataTable.Rows.Count > 0 && dataTable.Rows[0]["参数值"].ToString().Equals("true"));
			}
			catch
			{
				flag = false;
			}
			if (flag)
			{
				if (!string.IsNullOrWhiteSpace(str申请单号))
				{
					bool flag2 = this.IsExistInHis(str申请单号, strDate, str设备ID, str样本号);
					if (!flag2)
					{
						DataTable dataTable2 = this.Get检验结果View(str申请单号, strDate, str设备ID, str样本号);
						if (dataTable2 != null && dataTable2.Rows.Count > 0)
						{
							IList list = new ArrayList();
							foreach (DataRow dataRow in dataTable2.Rows)
							{
								StringBuilder stringBuilder = new StringBuilder();
								stringBuilder.Append("INSERT INTO [JY检验结果]([申请单号],[检验日期],[检验设备],[样本号],[病人ID],[住院号],[病人姓名],[年龄],[身份证],[检验ID]");
								stringBuilder.Append(",[检验编码],[检验名称],[单位],[参考值],[升降],[检验值],[排序])  VALUES( ");
								stringBuilder.Append("'" + dataRow["申请单号"].ToString() + "',");
								stringBuilder.Append("'" + dataRow["检验日期"].ToString() + "',");
								stringBuilder.Append("'" + dataRow["检验设备"].ToString() + "',");
								stringBuilder.Append("'" + dataRow["样本号"].ToString() + "',");
								stringBuilder.Append("'" + dataRow["病人ID"].ToString() + "',");
								stringBuilder.Append("'" + dataRow["住院号"].ToString() + "',");
								stringBuilder.Append("'" + dataRow["病人姓名"].ToString() + "',");
								stringBuilder.Append("'" + dataRow["年龄"].ToString() + "',");
								stringBuilder.Append("'" + dataRow["身份证"].ToString() + "',");
								stringBuilder.Append("'" + dataRow["检验ID"].ToString() + "',");
								stringBuilder.Append("'" + dataRow["检验编码"].ToString() + "',");
								stringBuilder.Append("'" + dataRow["检验名称"].ToString() + "',");
								stringBuilder.Append("'" + dataRow["单位"].ToString() + "',");
								stringBuilder.Append("'" + dataRow["参考值"].ToString() + "',");
								stringBuilder.Append("'" + dataRow["升降"].ToString() + "',");
								stringBuilder.Append("'" + dataRow["检验值"].ToString() + "',");
								stringBuilder.Append("'" + dataRow["排序"].ToString() + "')");
								list.Add(stringBuilder.ToString());
							}
							string text = WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strCHisDBConn, list);
							if (!text.Equals("true"))
							{
								WWMessage.MessageShowWarning("向His数据库回写数据失败");
							}
						}
					}
				}
			}
		}

		public void Del化验结果FromHisBy门诊住院号(string str门诊住院号, string str日期, string str设备ID, string str样本号)
		{
			bool flag = false;
			try
			{
				string strSql = "select top 1 参数值 from 全局参数 where 参数名称='结果回写HIS'";
				DataTable dataTable = WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, strSql);
				flag = (dataTable != null && dataTable.Rows.Count > 0 && dataTable.Rows[0]["参数值"].ToString().Equals("true"));
			}
			catch
			{
				flag = false;
			}
			if (flag)
			{
				string strSql2 = "DELETE FROM [JY检验结果] WHERE 住院号 = @mzzyh and 检验日期 =@testDate and 检验设备 = @InstrID and 样本号 =@SampleID ";
				SqlParameter[] sqlParams = new SqlParameter[]
				{
					new SqlParameter("@mzzyh", str门诊住院号),
					new SqlParameter("@testDate", str日期),
					new SqlParameter("@InstrID", str设备ID),
					new SqlParameter("@SampleID", str样本号)
				};
				WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(WWFInit.strCHisDBConn, strSql2, sqlParams);
			}
		}

		public void WriteDataToHisBy门诊住院号(string str门诊住院号, string strDate, string str设备ID, string str样本号)
		{
			if (!string.IsNullOrWhiteSpace(str门诊住院号))
			{
				bool flag = false;
				try
				{
					string strSql = "select top 1 参数值 from 全局参数 where 参数名称='结果回写HIS'";
					DataTable dataTable = WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(WWFInit.strDBConn, strSql);
					flag = (dataTable != null && dataTable.Rows.Count > 0 && dataTable.Rows[0]["参数值"].ToString().Equals("true"));
				}
				catch
				{
					flag = false;
				}
				if (flag)
				{
					DataTable dataTable2 = this.Get检验结果ViewBy门诊住院号(str门诊住院号, strDate, str设备ID, str样本号);
					if (dataTable2 != null && dataTable2.Rows.Count > 0)
					{
						IList list = new ArrayList();
						string value = string.Concat(new string[]
						{
							"DELETE FROM [JY检验结果] WHERE 住院号 = '",
							str门诊住院号,
							"' and 检验日期 ='",
							strDate,
							"' and 检验设备 = '",
							str设备ID,
							"' and 样本号 ='",
							str样本号,
							"'"
						});
						list.Add(value);
						foreach (DataRow dataRow in dataTable2.Rows)
						{
							StringBuilder stringBuilder = new StringBuilder();
							stringBuilder.Append("INSERT INTO [JY检验结果]([申请单号],[检验日期],[检验设备],[样本号],[病人ID],[住院号],[病人姓名],[年龄],[身份证],[检验ID]");
							stringBuilder.Append(",[检验编码],[检验名称],[单位],[参考值],[升降],[检验值],[排序])  VALUES( ");
							stringBuilder.Append("'" + dataRow["申请单号"].ToString() + "',");
							stringBuilder.Append("'" + dataRow["检验日期"].ToString() + "',");
							stringBuilder.Append("'" + dataRow["检验设备"].ToString() + "',");
							stringBuilder.Append("'" + dataRow["样本号"].ToString() + "',");
							stringBuilder.Append("'" + dataRow["病人ID"].ToString() + "',");
							stringBuilder.Append("'" + dataRow["住院号"].ToString() + "',");
							stringBuilder.Append("'" + dataRow["病人姓名"].ToString() + "',");
							stringBuilder.Append("'" + dataRow["年龄"].ToString() + "',");
							stringBuilder.Append("'" + dataRow["身份证"].ToString() + "',");
							stringBuilder.Append("'" + dataRow["检验ID"].ToString() + "',");
							stringBuilder.Append("'" + dataRow["检验编码"].ToString() + "',");
							stringBuilder.Append("'" + dataRow["检验名称"].ToString() + "',");
							stringBuilder.Append("'" + dataRow["单位"].ToString() + "',");
							stringBuilder.Append("'" + dataRow["参考值"].ToString() + "',");
							stringBuilder.Append("'" + dataRow["升降"].ToString() + "',");
							stringBuilder.Append("'" + dataRow["检验值"].ToString() + "',");
							stringBuilder.Append("'" + dataRow["排序"].ToString() + "')");
							list.Add(stringBuilder.ToString());
						}
						string text = WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strCHisDBConn, list);
						if (!text.Equals("true"))
						{
							WWMessage.MessageShowWarning("向His数据库回写数据失败");
						}
					}
				}
			}
		}
	}
}
