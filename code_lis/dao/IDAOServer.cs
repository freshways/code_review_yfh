using System;
using System.Collections;
using System.Data;
namespace ww.wwf.dao
{
    public delegate void DbClientUser_EventHandler(string strClientID);
    
    public delegate void LoadDataSet_EventHandler(string _strConn,string strSql, string strTableName);
    public delegate void LoadDataSetByStoredProc_EventHandler(string _strConn,string strStoredProcName, string strTableName, string strInParameterName, string strInParameterValue);
    public delegate void ExecuteDataSet_EventHandler(string _strConn,string strSql);
    public delegate void ExecuteDataSetByStoredProc_EventHandler(string _strConn,string strStoredProcName);
    public delegate void ExecuteDataSetPaging_EventHandler(string _strConn,string tblName, string fldName, Int32 PageSize, Int32 PageIndex, Int32 IsReCount, Int32 OrderType, string strWhere);
    

    public delegate void ExecuteNonQuery_EventHandler(string _strConn,string strSql);  
    public delegate void RecordExists_EventHandler(string _strConn,string sql);
     public delegate void ExecuteScalar_EventHandler(string _strConn,string strSql);
     public delegate void ExecuteScalarByStoredProc_EventHandler(string _strConn,string strStoredProc);
     public delegate void UpdateDataSet_EventHandler
            (string _strConn,DataSet dsObject, string strTableName, string strKey, string strStoredProcInsertCommand, string strStoredProcDeleteCommand, string strStoredProcupdateCommand, int intUpdateBehavior);
     public delegate void UpdateDataSetStoredProcInsert2_EventHandler(string _strConn,DataSet dsObject_1, string strTableName_1, string strStoredProcInsertCommand_1, DataSet dsObject_2, string strTableName_2, string strStoredProcInsertCommand_2);

     public delegate void UpdateDataSetStoredProcInsert3_EventHandler(string _strConn,DataSet dsObject_1, string strTableName_1, string strStoredProcInsertCommand_1, DataSet dsObject_2, string strTableName_2, string strStoredProcInsertCommand_2, DataSet dsObject_3, string strTableName_3, string strStoredProcInsertCommand_3);
    public delegate void UpdateDataSetStoredProcUpdate2_EventHandler(string _strConn,DataSet dsObject_1, string strTableName_1, string strStoredProcUpdateCommand_1, DataSet dsObject_2, string strTableName_2, string strStoredProcUpdateCommand_2);
    public delegate void UpdateDataSetStoredProcUpdate3_EventHandler(string _strConn,DataSet dsObject_1, string strTableName_1, string strStoredProcUpdateCommand_1, DataSet dsObject_2, string strTableName_2, string strStoredProcUpdateCommand_2, DataSet dsObject_3, string strTableName_3, string strStoredProcUpdateCommand_3);
    public delegate void ExecuteNonQueryTransaction2_EventHandler(string _strConn,string strSql1, string strSql2);
    public delegate void ExecuteNonQueryTransaction3_EventHandler(string _strConn,string strSql1, string strSql2, string strSql3);
    public delegate void ExecuteNonQueryTransaction4_EventHandler(string _strConn,string strSql1, string strSql2, string strSql3, string strSql4);
    public delegate void ExecuteReader_EventHandler(string _strConn,string strSql);
    public delegate void ExecuteReaderByStoredProc_EventHandler(string _strConn,string strStoredProc);

    public delegate void LoadDataSetPaging_EventHandler(string _strConn,string tblName, string fldName, Int32 PageSize, Int32 PageIndex, Int32 IsReCount, Int32 OrderType, string strWhere, string strLoadTableName);
    public delegate void ExecuteNonQueryTransactionSqlList_EventHandler(string _strConn,IList sqlList);//21

    public delegate void DbReportImgAdd_EventHandler(string _strConn, string strStoredProcImg, String strfimg_type, Byte[] fimg, String strforder_by, String fremark, String strfsample_id);
    public delegate void DbInstrImgAdd_EventHandler(string _strConn, string strStoredProcImg, String strfimg_type, Byte[] fimg, String strforder_by, String fremark, String strfio_id);
    public delegate void DBReport_EventHandler(IList ilt);

    /// <summary>
    /// 远程数据存取接口
    /// </summary>
    public interface IDAOServer
    {
        bool DbClientUser(string strClientID);
        
        DataSet DbLoadDataSetBySqlString(string _strConn,string strSql, string strTableName);//1
        DataSet DbLoadDataSetByStoredProc(string _strConn,string strStoredProcName, string strTableName, string strInParameterName, string strInParameterValue);//2
        DataSet DbExecuteDataSetBySqlString(string _strConn,string strSql);//3
        DataSet DbExecuteDataSetByStoredProc(string _strConn,string strStoredProcName);//4

        DataSet DbExecuteDataSetPaging(string _strConn,string tblName, string fldName, Int32 PageSize, Int32 PageIndex, Int32 IsReCount, Int32 OrderType, string strWhere);//5
        int DbExecuteNonQueryBySqlString(string _strConn,string strSql);//6
        bool DbRecordExists(string _strConn,string sql);//7
        object DbExecuteScalarBySqlString(string _strConn,string strSql);//8
        object DbExecuteScalarByStoredProc(string _strConn,string strStoredProc);//9
        int DbUpdateDataSet
            (string _strConn,DataSet dsObject, string strTableName, string strKey, string strStoredProcInsertCommand, string strStoredProcDeleteCommand, string strStoredProcupdateCommand, int intUpdateBehavior);//10
        int DbUpdateDataSetStoredProcInsert2(string _strConn,DataSet dsObject_1, string strTableName_1, string strStoredProcInsertCommand_1, DataSet dsObject_2, string strTableName_2, string strStoredProcInsertCommand_2);//11
        int DbUpdateDataSetStoredProcInsert3(string _strConn,DataSet dsObject_1, string strTableName_1, string strStoredProcInsertCommand_1, DataSet dsObject_2, string strTableName_2, string strStoredProcInsertCommand_2, DataSet dsObject_3, string strTableName_3, string strStoredProcInsertCommand_3);//12
        int DbUpdateDataSetStoredProcUpdate2(string _strConn, DataSet dsObject_1, string strTableName_1, string strStoredProcUpdateCommand_1, DataSet dsObject_2, string strTableName_2, string strStoredProcUpdateCommand_2);//13
        int DbUpdateDataSetStoredProcUpdate3(string _strConn,DataSet dsObject_1, string strTableName_1, string strStoredProcUpdateCommand_1, DataSet dsObject_2, string strTableName_2, string strStoredProcUpdateCommand_2, DataSet dsObject_3, string strTableName_3, string strStoredProcUpdateCommand_3);//14
        bool DbExecuteNonQueryTransaction2(string _strConn,string strSql1, string strSql2);//15
        bool DbExecuteNonQueryTransaction3(string _strConn,string strSql1, string strSql2, string strSql3);//16
        bool DbExecuteNonQueryTransaction4(string _strConn,string strSql1, string strSql2, string strSql3, string strSql4);//17
        IDataReader DbExecuteReaderBySqlString(string _strConn,string strSql);//18
        IDataReader DbExecuteReaderByStoredProc(string _strConn,string strStoredProc);//19
        DataSet DbLoadDataSetPaging(string _strConn,string tblName, string fldName, Int32 PageSize, Int32 PageIndex, Int32 IsReCount, Int32 OrderType, string strWhere, string strLoadTableName);//20
        string DbTransaction(string _strConn,IList sqlList);//21


        int DbReportImgAdd(string _strConn, string strStoredProcImg, String strfimg_type, Byte[] fimg, String strforder_by, String fremark, String strfsample_id);//1
        int DbInstrImgAdd(string _strConn, string strStoredProcImg, String strfimg_type, Byte[] fimg, String strforder_by, String fremark, String strfio_id);//1

        DataTable DBReport(IList ilt);
    }
}