using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
namespace ww.wwf.dao
{
    /// <summary>
    /// 日志
    /// </summary>
    public class Log
    {
        public Log()
        {
            ///
            /// TODO: 在此处添加构造函数逻辑
            ///
        }
        /// <summary>
        /// 保存日志
        /// </summary>
        /// <param name="strErrorTitle">错误标题</param>
        /// <param name="strException">异常主体</param>
        public void WWFSaveLog(string strErrorTitle, string strException)
        {
            if (strErrorTitle == "" || strErrorTitle == "")
            {
                strErrorTitle = "一般异常";
            }
            StreamWriter sw;            
            //sw = File.AppendText(Server.MapPath(null) + "\\ZASuite~Log.log");//Web
            // <!--系统日志文件名--> <add key="SysLogFileName" value="Log.log" />
   //System.Configuration.ConfigurationSettings.AppSettings["SysName"] 
            sw = File.AppendText("wwfserver.log");//Winform
            
            sw.WriteLine("时间：" + System.DateTime.Now.ToString());
            sw.WriteLine("系统：" + "wwfserver");
            sw.WriteLine("标题：" + strErrorTitle);
            sw.WriteLine("内容："+strException);           
            sw.WriteLine("");
            sw.Flush();
            sw.Close();
        }

       
    }
}
