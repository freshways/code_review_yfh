using System;
using System.Data;
using System.Collections;
using System.Text;
namespace ww.wwf.dao
{
    public class DAOServer : MarshalByRefObject, IDAOServer
    {
        /// <summary>
        /// 数据存取公共类
        /// </summary>               
        private string _HostName;
        /// <summary>
        ///  主机名
        /// </summary>
        public string strHostName
        {
            set { _HostName = value; }
            get { return _HostName; }
        }
        private string _HostIpAddress;
        /// <summary>
        ///  主机IP
        /// </summary>
        public string strHostIpAddress
        {
            set { _HostIpAddress = value; }
            get { return _HostIpAddress; }
        }
        private string _AccessPasswordClient;
        /// <summary>
        ///  客户端存取密码
        /// </summary>
        public string strAccessPasswordClient
        {
            set { _AccessPasswordClient = value; }
            get { return _AccessPasswordClient; }
        }
        private DAO dao = new DAO();
        /// <summary>
        ///  服务端存取密码
        /// </summary>
        private string strAccessPasswordServer = "F1-49-55-AA-44-F1-6D-12-0A-33-74-91-FF-0P-83-E7-tao"; 
        public static DataTable ClientDT = new DataTable();
        public static int ClientCount = 100;//授权数量
        public DAOServer()
        {
            ///
            /// TODO: 在此处添加构造函数逻辑
            ///
            
        }

        /*
         错误	1	“ww.wwf.dao.DAOServer”不会实现接口成员“ww.wwf.dao.IDAOServer.DbUpdateDataSetStoredProcUpdate2(string, System.Data.DataSet, string, string, System.Data.DataSet, string, string)”	E:\tao\网为\WW\Code\wwf\dao\DAOServer.cs	6	18	dao

         */
       // public static event DbClientUser_EventHandler DbClientUser_SendedEvent;
       
        public static event LoadDataSet_EventHandler LoadDataSet_SendedEvent;
        public static event LoadDataSetByStoredProc_EventHandler LoadDataSetByStoredProc_SendedEvent;
        public static event ExecuteDataSet_EventHandler ExecuteDataSet_SendedEvent;
        public static event ExecuteDataSetByStoredProc_EventHandler ExecuteDataSetByStoredProc_SendedEvent;
        public static event ExecuteDataSetPaging_EventHandler ExecuteDataSetPaging_SendedEvent;
        public static event ExecuteNonQuery_EventHandler ExecuteNonQuery_SendedEvent;
        public static event RecordExists_EventHandler RecordExists_SendedEvent;
        public static event ExecuteScalar_EventHandler ExecuteScalar_SendedEvent;
        public static event ExecuteScalarByStoredProc_EventHandler ExecuteScalarByStoredProc_SendedEvent;
        public static event UpdateDataSet_EventHandler UpdateDataSet_SendedEvent;
        public static event UpdateDataSetStoredProcInsert2_EventHandler UpdateDataSetStoredProcInsert2_SendedEvent;
        public static event UpdateDataSetStoredProcInsert3_EventHandler UpdateDataSetStoredProcInsert3_SendedEvent;
        public static event UpdateDataSetStoredProcUpdate2_EventHandler UpdateDataSetStoredProcUpdate2_SendedEvent;
        public static event UpdateDataSetStoredProcUpdate3_EventHandler UpdateDataSetStoredProcUpdate3_SendedEvent;
        public static event ExecuteNonQueryTransaction2_EventHandler ExecuteNonQueryTransaction2_SendedEvent;
        public static event ExecuteNonQueryTransaction3_EventHandler ExecuteNonQueryTransaction3_SendedEvent;
        public static event ExecuteNonQueryTransaction4_EventHandler ExecuteNonQueryTransaction4_SendedEvent;
        public static event ExecuteReader_EventHandler ExecuteReader_SendedEvent;
        public static event ExecuteReaderByStoredProc_EventHandler ExecuteReaderByStoredProc_SendedEvent;
        public static event LoadDataSetPaging_EventHandler LoadDataSetPaging_SendedEvent;
        public static event ExecuteNonQueryTransactionSqlList_EventHandler ExecuteNonQueryTransactionSqlList_SendedEvent;


        public static event DbReportImgAdd_EventHandler DbReportImgAdd_SendedEvent;
        public static event DbInstrImgAdd_EventHandler DbInstrImgAdd_SendedEvent;

        public static event DBReport_EventHandler DBReport_SendedEvent;
        /// <summary>
        ///取得客户端列表
        /// </summary>
        public static void DbGetClientDT()
        {
            DAO daot = new DAO();

            string strid = "";
            ww.wwf.com.HardInfo hardinfo = new ww.wwf.com.HardInfo();
            string str1 = hardinfo.GetNetCardMAC() + hardinfo.GetCpuID();
            string str2 = ww.wwf.com.Security.StrToEncrypt("SHA1", str1);
            strid = str2.Replace("-", "");

            if (daot.DbRecordExists("WW.DBConn", "SELECT count(1) FROM WWF_CLIENT where fid='" + strid + "'"))
            { }
            else
            {             

                StringBuilder strSql = new StringBuilder();
                strSql.Append("insert into WWF_CLIENT(");
                strSql.Append("fguid,fid,fpc_name,fip,fuse_flag,ftime,fname,fdept,fremark");
                strSql.Append(")");
                strSql.Append(" values (");
                strSql.Append("'" + ww.wwf.com.Public.BllGuid() + "',");
                strSql.Append("'" + strid + "',");
                strSql.Append("'" + hardinfo.GetHostName() + "',");
                strSql.Append("'" + hardinfo.GetHostIpAddress() + "',");
                strSql.Append("1,");
                strSql.Append("'" + System.DateTime.Now.ToString() + "',");
                strSql.Append("' ',");
                strSql.Append("' ',");
                strSql.Append("'wwfserver'");
                strSql.Append(")");
                daot.DbExecuteNonQueryBySqlString("WW.DBConn", strSql.ToString());
            }
            ClientDT = daot.DbExecuteDataSetBySqlString("WW.DBConn", "SELECT TOP " + ClientCount + " * FROM WWF_CLIENT WHERE (fuse_flag = 1) ORDER BY ftime").Tables[0];
        }
        /// <summary>
        /// 检验访问用户是否全法
        /// </summary>
        /// <returns></returns>
        public bool DbClientUser(string strClientID)
        {
            bool bl = false;
            
            try
            {
                DataRow[] drSelc = ClientDT.Select("fid='" + strClientID + "'");
                if (drSelc.Length > 0)
                {
                    bl = true;
                }
                else
                {
                    WWFSaveLog("未注册用户", "用户： '" + strClientID + "' 为未注册用户！不能访问本服务。");
                }
            }
            catch (Exception ex)
            {
                this.WWFSaveLog(strClientID, ex.ToString());
            }
            return bl;
        }

        /// <summary>
        /// 检验访问用户是否全法
        /// </summary>
        /// <returns></returns>
        private bool DbAccessPassword()
        {
            bool bl = false;
            if (this._AccessPasswordClient == strAccessPasswordServer)
            {
                bl = true;
            }
            else
            {
                this.WWFSaveLog("*****非法用户*****", "[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "] 请把此消息通知软件开发者(tao:tel:11111111111,email:taoyinzhou@gmail.com,QQ:249116367)");
            }
            return bl;
        }
        
        

        /// <summary>
        /// 装载可读写数据集 根据sql
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="strSql"></param>
        /// <param name="strTableName"></param>
        /// <returns></returns>
        public DataSet DbLoadDataSetBySqlString(string _strConn, string strSql, string strTableName)//1
        {
            
            DataSet ds = new DataSet();
            try
            {
                if (LoadDataSet_SendedEvent != null)
                {
                    if (DbAccessPassword())
                    {
                        LoadDataSet_SendedEvent(_strConn, strSql, strTableName);
                        ds = dao.DbLoadDataSetBySqlString(_strConn, strSql, strTableName);
                    }
                }
            }
            catch (Exception ex)
            {
                this.WWFSaveLog("装载可读写数据集 根据sql！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            return ds;
        }

        /// <summary>
        /// 装载可读写数据集 根据存过
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="strStoredProcName"></param>
        /// <param name="strTableName"></param>
        /// <param name="strInParameterName"></param>
        /// <param name="strInParameterValue"></param>
        /// <returns></returns>
        public DataSet DbLoadDataSetByStoredProc(string _strConn, string strStoredProcName, string strTableName, string strInParameterName, string strInParameterValue)//2
        {
            
            DataSet ds = new DataSet();
            try
            {
                if (LoadDataSetByStoredProc_SendedEvent != null)
                {
                    if (DbAccessPassword())
                    {
                        LoadDataSetByStoredProc_SendedEvent(_strConn, strStoredProcName, strTableName, strInParameterName, strInParameterValue);
                        ds = dao.DbLoadDataSetByStoredProc(_strConn, strStoredProcName, strTableName, strInParameterName, strInParameterValue);
                    }
                }
            }
            catch (Exception ex)
            {
                this.WWFSaveLog("装载可读写数据集 根据存过！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            return ds;
        }

        /// <summary>
        /// 装载只读数据集 根据sql
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="strSql"></param>
        /// <returns></returns>
        public DataSet DbExecuteDataSetBySqlString(string _strConn, string strSql)//3
        {
            
            DataSet ds = null;
            try
            {
                if (ExecuteDataSet_SendedEvent != null)
                {
                    if (DbAccessPassword())
                    {
                        ExecuteDataSet_SendedEvent(_strConn, strSql);
                        ds = dao.DbExecuteDataSetBySqlString(_strConn, strSql);
                    }
                }
            }
            catch (Exception ex)
            {
                this.WWFSaveLog("装载只读数据集 根据sql！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            return ds;

        }
        /// <summary>
        /// 装载只读数据集 根据存过
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="strStoredProcName"></param>
        /// <returns></returns>
        public DataSet DbExecuteDataSetByStoredProc(string _strConn, string strStoredProcName)//4
        {
            
            DataSet ds = null;
            try
            {
                if (ExecuteDataSetByStoredProc_SendedEvent != null)
                {
                    if (DbAccessPassword())
                    {
                        ExecuteDataSetByStoredProc_SendedEvent(_strConn, strStoredProcName);
                        ds = dao.DbExecuteDataSetByStoredProc(_strConn, strStoredProcName);
                    }
                }
            }
            catch (Exception ex)
            {
                this.WWFSaveLog("装载只读数据集 根据存过！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            return ds;
        }

        /// <summary>
        /// 装载只读数据集 分页
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="tblName"></param>
        /// <param name="fldName"></param>
        /// <param name="PageSize"></param>
        /// <param name="PageIndex"></param>
        /// <param name="IsReCount"></param>
        /// <param name="OrderType"></param>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet DbExecuteDataSetPaging(string _strConn, string tblName, string fldName, Int32 PageSize, Int32 PageIndex, Int32 IsReCount, Int32 OrderType, string strWhere)//5
        {
            
            DataSet ds = null;
            try
            {
                if (ExecuteDataSetPaging_SendedEvent != null)
                {
                    if (DbAccessPassword())
                    {
                        ExecuteDataSetPaging_SendedEvent(_strConn, tblName, fldName, PageSize, PageIndex, IsReCount, OrderType, strWhere);
                        ds = dao.DbExecuteDataSetPaging(_strConn, tblName, fldName, PageSize, PageIndex, IsReCount, OrderType, strWhere);
                    }
                }
            }
            catch (Exception ex)
            {
                this.WWFSaveLog("装载只读数据集 分页！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            return ds;
        }

        /// <summary>
        /// 执行sql语句
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="strSql"></param>
        /// <returns></returns>
        public int DbExecuteNonQueryBySqlString(string _strConn, string strSql)//6
        {
            
            int intR = 0;
            try
            {
                if (ExecuteNonQuery_SendedEvent != null)
                {
                    if (DbAccessPassword())
                    {
                        ExecuteNonQuery_SendedEvent(_strConn, strSql);
                        intR = dao.DbExecuteNonQueryBySqlString(_strConn, strSql);
                    }
                }
            }
            catch (Exception ex)
            {
                this.WWFSaveLog("执行sql语句！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            return intR;
        }

        /// <summary>
        /// 判断记录存在否
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="sql"></param>
        /// <returns></returns>
        public bool DbRecordExists(string _strConn, string sql)//7
        {
            
            bool boolR = false;
            try
            {

                if (RecordExists_SendedEvent != null)
                {
                    if (DbAccessPassword())
                    {
                        RecordExists_SendedEvent(_strConn, sql);
                        boolR = dao.DbRecordExists(_strConn, sql);
                    }
                }
            }
            catch (Exception ex)
            {
                this.WWFSaveLog("判断记录存在否！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            return boolR;
        }

        /// <summary>
        /// 执行sql语句返回对象
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="strSql"></param>
        /// <returns></returns>
        public object DbExecuteScalarBySqlString(string _strConn, string strSql)//8
        {
            
            object obj = null;
            try
            {
                if (ExecuteScalar_SendedEvent != null)
                {
                    if (DbAccessPassword())
                    {
                        ExecuteScalar_SendedEvent(_strConn, strSql);
                        obj = dao.DbExecuteScalarBySqlString(_strConn, strSql);
                    }
                }
            }
            catch (Exception ex)
            {
                this.WWFSaveLog("执行sql语句返回对象！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            return obj;
        }
        /// <summary>
        /// 执行存过返回对象
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="strStoredProc"></param>
        /// <returns></returns>
        public object DbExecuteScalarByStoredProc(string _strConn, string strStoredProc)//9
        {
            
            object obj = null;
            try
            {
                if (ExecuteScalarByStoredProc_SendedEvent != null)
                {
                    if (DbAccessPassword())
                    {
                        ExecuteScalarByStoredProc_SendedEvent(_strConn, strStoredProc);
                        obj = dao.DbExecuteScalarByStoredProc(_strConn, strStoredProc);
                    }
                }
            }
            catch (Exception ex)
            {
                this.WWFSaveLog("执行存过返回对象！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            return obj;
        }

        /// <summary>
        /// 批量增、删、改数据集
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="dsObject"></param>
        /// <param name="strTableName"></param>
        /// <param name="strKey"></param>
        /// <param name="strStoredProcInsertCommand"></param>
        /// <param name="strStoredProcDeleteCommand"></param>
        /// <param name="strStoredProcupdateCommand"></param>
        /// <param name="intUpdateBehavior"></param>
        /// <returns></returns>
        public int DbUpdateDataSet
            (string _strConn, DataSet dsObject, string strTableName, string strKey, string strStoredProcInsertCommand, string strStoredProcDeleteCommand, string strStoredProcupdateCommand, int intUpdateBehavior)//10
        {
            
            int intR = 0;
            try
            {
                if (UpdateDataSet_SendedEvent != null)
                {
                    if (DbAccessPassword())
                    {
                        UpdateDataSet_SendedEvent(_strConn, dsObject, strTableName, strKey, strStoredProcInsertCommand, strStoredProcDeleteCommand, strStoredProcupdateCommand, intUpdateBehavior);
                        intR = dao.DbUpdateDataSet(_strConn, dsObject, strTableName, strKey, strStoredProcInsertCommand, strStoredProcDeleteCommand, strStoredProcupdateCommand, intUpdateBehavior);
                    }
                }
               
               // abc.WWFSaveLogTest("IP地址：" + _HostIpAddress + "]");
                //this.WWFSaveLog("[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            catch (Exception ex)
            {
                this.WWFSaveLog("批量增、删、改数据集！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            return intR;
        }
        //Log abc = new Log();
        /// <summary>
        /// 批量增加数据集 事务处理2个数据集
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="dsObject_1"></param>
        /// <param name="strTableName_1"></param>
        /// <param name="strStoredProcInsertCommand_1"></param>
        /// <param name="dsObject_2"></param>
        /// <param name="strTableName_2"></param>
        /// <param name="strStoredProcInsertCommand_2"></param>
        /// <returns></returns>
        public int DbUpdateDataSetStoredProcInsert2(string _strConn, DataSet dsObject_1, string strTableName_1, string strStoredProcInsertCommand_1, DataSet dsObject_2, string strTableName_2, string strStoredProcInsertCommand_2)//11
        {
            
            int intR = 0;
            try
            {
                if (UpdateDataSetStoredProcInsert2_SendedEvent != null)
                {
                    if (DbAccessPassword())
                    {
                        UpdateDataSetStoredProcInsert2_SendedEvent(_strConn, dsObject_1, strTableName_1, strStoredProcInsertCommand_1, dsObject_2, strTableName_2, strStoredProcInsertCommand_2);
                        intR = dao.DbUpdateDataSetStoredProcInsert2(_strConn, dsObject_1, strTableName_1, strStoredProcInsertCommand_1, dsObject_2, strTableName_2, strStoredProcInsertCommand_2);
                    }
                }
            }
            catch (Exception ex)
            {
                this.WWFSaveLog("批量增加数据集 事务处理2个数据集！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            return intR;
        }

        /// <summary>
        ///  批量增加数据集 事务处理3个数据集
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="dsObject_1"></param>
        /// <param name="strTableName_1"></param>
        /// <param name="strStoredProcInsertCommand_1"></param>
        /// <param name="dsObject_2"></param>
        /// <param name="strTableName_2"></param>
        /// <param name="strStoredProcInsertCommand_2"></param>
        /// <param name="dsObject_3"></param>
        /// <param name="strTableName_3"></param>
        /// <param name="strStoredProcInsertCommand_3"></param>
        /// <returns></returns>
        public int DbUpdateDataSetStoredProcInsert3(string _strConn, DataSet dsObject_1, string strTableName_1, string strStoredProcInsertCommand_1, DataSet dsObject_2, string strTableName_2, string strStoredProcInsertCommand_2, DataSet dsObject_3, string strTableName_3, string strStoredProcInsertCommand_3)//12
        {
            
            int intR = 0;
            try
            {
                if (UpdateDataSetStoredProcInsert3_SendedEvent != null)
                {
                    if (DbAccessPassword())
                    {
                        UpdateDataSetStoredProcInsert3_SendedEvent(_strConn, dsObject_1, strTableName_1, strStoredProcInsertCommand_1, dsObject_2, strTableName_2, strStoredProcInsertCommand_2, dsObject_3, strTableName_3, strStoredProcInsertCommand_3);
                        intR = dao.DbUpdateDataSetStoredProcInsert3(_strConn, dsObject_1, strTableName_1, strStoredProcInsertCommand_1, dsObject_2, strTableName_2, strStoredProcInsertCommand_2, dsObject_3, strTableName_3, strStoredProcInsertCommand_3);
                    }
                }
            }
            catch (Exception ex)
            {
                this.WWFSaveLog(" 批量增加数据集 事务处理3个数据集！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            return intR;
        }

        /// <summary>
        ///  批量修改数据集 事务处理2个数据集
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="dsObject_1"></param>
        /// <param name="strTableName_1"></param>
        /// <param name="strStoredProcUpdateCommand_1"></param>
        /// <param name="dsObject_2"></param>
        /// <param name="strTableName_2"></param>
        /// <param name="strStoredProcUpdateCommand_2"></param>
        /// <returns></returns>
        public int DbUpdateDataSetStoredProcUpdate2(string _strConn, DataSet dsObject_1, string strTableName_1, string strStoredProcUpdateCommand_1, DataSet dsObject_2, string strTableName_2, string strStoredProcUpdateCommand_2)//13
        {
            
            int intR = 0;
            try
            {
                if (UpdateDataSetStoredProcUpdate2_SendedEvent != null)
                {
                    if (DbAccessPassword())
                    {
                        UpdateDataSetStoredProcUpdate2_SendedEvent(_strConn, dsObject_1, strTableName_1, strStoredProcUpdateCommand_1, dsObject_2, strTableName_2, strStoredProcUpdateCommand_2);
                        intR = dao.DbUpdateDataSetStoredProcUpdate2(_strConn, dsObject_1, strTableName_1, strStoredProcUpdateCommand_1, dsObject_2, strTableName_2, strStoredProcUpdateCommand_2);
                    }
                }
            }
            catch (Exception ex)
            {
                this.WWFSaveLog(" 批量修改数据集 事务处理2个数据集！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            return intR;
        }

        /// <summary>
        /// 批量修改数据集 事务处理3个数据集
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="dsObject_1"></param>
        /// <param name="strTableName_1"></param>
        /// <param name="strStoredProcUpdateCommand_1"></param>
        /// <param name="dsObject_2"></param>
        /// <param name="strTableName_2"></param>
        /// <param name="strStoredProcUpdateCommand_2"></param>
        /// <param name="dsObject_3"></param>
        /// <param name="strTableName_3"></param>
        /// <param name="strStoredProcUpdateCommand_3"></param>
        /// <returns></returns>
        public int DbUpdateDataSetStoredProcUpdate3(string _strConn, DataSet dsObject_1, string strTableName_1, string strStoredProcUpdateCommand_1, DataSet dsObject_2, string strTableName_2, string strStoredProcUpdateCommand_2, DataSet dsObject_3, string strTableName_3, string strStoredProcUpdateCommand_3)//14
        {
            
            int intR = 0;
            try
            {
                if (UpdateDataSetStoredProcUpdate3_SendedEvent != null)
                {
                    if (DbAccessPassword())
                    {
                        UpdateDataSetStoredProcUpdate3_SendedEvent(_strConn, dsObject_1, strTableName_1, strStoredProcUpdateCommand_1, dsObject_2, strTableName_2, strStoredProcUpdateCommand_2, dsObject_3, strTableName_3, strStoredProcUpdateCommand_3);
                        intR = dao.DbUpdateDataSetStoredProcUpdate3(_strConn, dsObject_1, strTableName_1, strStoredProcUpdateCommand_1, dsObject_2, strTableName_2, strStoredProcUpdateCommand_2, dsObject_3, strTableName_3, strStoredProcUpdateCommand_3);
                    }
                }
            }
            catch (Exception ex)
            {
                this.WWFSaveLog("批量修改数据集 事务处理3个数据集！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            return intR;
        }
        /// <summary>
        /// 事务执行sql语句 2个sql语句
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="strSql1"></param>
        /// <param name="strSql2"></param>
        /// <returns></returns>
        public bool DbExecuteNonQueryTransaction2(string _strConn, string strSql1, string strSql2)//15
        {
            
            bool boolR = false;
            try
            {
                if (ExecuteNonQueryTransaction2_SendedEvent != null)
                {
                    if (DbAccessPassword())
                    {
                        ExecuteNonQueryTransaction2_SendedEvent(_strConn, strSql1, strSql2);
                        boolR = dao.DbExecuteNonQueryTransaction2(_strConn, strSql1, strSql2);
                    }
                }
            }
            catch (Exception ex)
            {
                this.WWFSaveLog("事务执行sql语句 2个sql语句！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            return boolR;
        }
        /// <summary>
        /// 事务执行sql语句 3个sql语句
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="strSql1"></param>
        /// <param name="strSql2"></param>
        /// <param name="strSql3"></param>
        /// <returns></returns>
        public bool DbExecuteNonQueryTransaction3(string _strConn, string strSql1, string strSql2, string strSql3)//16
        {
            
            bool boolR = false;
            try
            {
                if (ExecuteNonQueryTransaction3_SendedEvent != null)
                {
                    if (DbAccessPassword())
                    {
                        ExecuteNonQueryTransaction3_SendedEvent(_strConn, strSql1, strSql2, strSql3);
                        boolR = dao.DbExecuteNonQueryTransaction3(_strConn, strSql1, strSql2, strSql3);
                    }
                }
            }
            catch (Exception ex)
            {
                this.WWFSaveLog("事务执行sql语句 3个sql语句！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            return boolR;
        }
        /// <summary>
        /// 事务执行sql语句 4个sql语句
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="strSql1"></param>
        /// <param name="strSql2"></param>
        /// <param name="strSql3"></param>
        /// <param name="strSql4"></param>
        /// <returns></returns>
        public bool DbExecuteNonQueryTransaction4(string _strConn, string strSql1, string strSql2, string strSql3, string strSql4)//17
        {
            
            bool boolR = false;
            try
            {
                if (ExecuteNonQueryTransaction4_SendedEvent != null)
                {
                    if (DbAccessPassword())
                    {
                        ExecuteNonQueryTransaction4_SendedEvent(_strConn, strSql1, strSql2, strSql3, strSql4);
                        boolR = dao.DbExecuteNonQueryTransaction4(_strConn, strSql1, strSql2, strSql3, strSql4);
                    }
                }
            }
            catch (Exception ex)
            {
                this.WWFSaveLog("事务执行sql语句 4个sql语句！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            return boolR;
        }

        /// <summary>
        /// 事务处理 SQL列表
        /// </summary>
        /// <param name="strConn">连接字串</param>
        /// <param name="sqlList">SQL列表</param>
        /// <returns></returns>
        public string DbTransaction(string _strConn, IList sqlList)
        {

            string result = "";
            if (sqlList.Count > 0)
            {
                try
                {
                    if (ExecuteNonQueryTransactionSqlList_SendedEvent != null)
                    {
                        if (DbAccessPassword())
                        {
                            ExecuteNonQueryTransactionSqlList_SendedEvent(_strConn, sqlList);
                            result = dao.DbTransaction(_strConn, sqlList);
                            if (result == "true")
                            { }
                            else
                            {
                                this.WWFSaveLog("ExecuteNonQueryTransactionSqlList执行事务处理SQL列表！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", result);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.WWFSaveLog("ExecuteNonQueryTransactionSqlList执行事务处理SQL列表！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", result + ex.ToString());
                }
            }
            else
            {
                result = "sql数为0";
            }
            return result;
        }
        /// <summary>
        /// IDataReader执行Sql语句 只读
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="strSql"></param>
        /// <returns></returns>
        public IDataReader DbExecuteReaderBySqlString(string _strConn, string strSql)//18
        {
            
            IDataReader idr = null;
            try
            {
                if (ExecuteReader_SendedEvent != null)
                {
                    if (DbAccessPassword())
                    {
                        ExecuteReader_SendedEvent(_strConn, strSql);
                        idr = dao.DbExecuteReaderBySqlString(_strConn, strSql);
                    }
                }
            }
            catch (Exception ex)
            {
                this.WWFSaveLog("IDataReader执行Sql语句 只读！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            return idr;
        }
        /// <summary>
        /// IDataReader执行存过 只读
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="strStoredProc"></param>
        /// <returns></returns>
        public IDataReader DbExecuteReaderByStoredProc(string _strConn, string strStoredProc)//19
        {
            
            IDataReader idr = null;
            try
            {
                if (ExecuteReaderByStoredProc_SendedEvent != null)
                {
                    if (DbAccessPassword())
                    {
                        ExecuteReaderByStoredProc_SendedEvent(_strConn, strStoredProc);
                        idr = dao.DbExecuteReaderByStoredProc(_strConn, strStoredProc);
                    }
                }
            }
            catch (Exception ex)
            {
                this.WWFSaveLog("IDataReader执行存过 只读！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            return idr;
        }
        /// <summary>
        /// 装载数据集 可读写 分页
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="tblName"></param>
        /// <param name="fldName"></param>
        /// <param name="PageSize"></param>
        /// <param name="PageIndex"></param>
        /// <param name="IsReCount"></param>
        /// <param name="OrderType"></param>
        /// <param name="strWhere"></param>
        /// <param name="strLoadTableName"></param>
        /// <returns></returns>
        public DataSet DbLoadDataSetPaging(string _strConn, string tblName, string fldName, Int32 PageSize, Int32 PageIndex, Int32 IsReCount, Int32 OrderType, string strWhere, string strLoadTableName)//20
        {
            
            DataSet ds = new DataSet();
            try
            {
                if (LoadDataSetPaging_SendedEvent != null)
                {
                    if (DbAccessPassword())
                    {
                        LoadDataSetPaging_SendedEvent(_strConn, tblName, fldName, PageSize, PageIndex, IsReCount, OrderType, strWhere, strLoadTableName);
                        ds = dao.DbLoadDataSetPaging(_strConn, tblName, fldName, PageSize, PageIndex, IsReCount, OrderType, strWhere, strLoadTableName);
                    }
                }
            }
            catch (Exception ex)
            {
                this.WWFSaveLog("IDataReader执行存过 只读！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            return ds;
        }

        /// <summary>
        /// 保存日志 异常日志
        /// </summary>
        /// <param name="strErrorTitle">错误标题</param>
        /// <param name="strException">异常主体</param>
        private void WWFSaveLog(string strErrorTitle, string strException)
        {
            
            try
            {
                Log log = new Log();
                log.WWFSaveLog(strErrorTitle, strException);
            }
            catch// (Exception ex)
            {
                //SiltMessage.ZAMessageShowError("日志存储有误!\n" + ex.ToString());
            }
        }
        
        //ExecuteNonQueryTransactionSqlList
       
        public override object InitializeLifetimeService()
        {
            return null;
        }

      /// <summary>
      /// 报告图存储
      /// </summary>
      /// <param name="_strConn"></param>
      /// <param name="strStoredProcImg"></param>
      /// <param name="strfimg_type"></param>
      /// <param name="fimg"></param>
      /// <param name="strforder_by"></param>
      /// <param name="fremark"></param>
      /// <param name="strfsample_id"></param>
      /// <returns></returns>
        public int DbReportImgAdd(string _strConn, string strStoredProcImg, String strfimg_type, Byte[] fimg, String strforder_by, String fremark, String strfsample_id)
        {

            int intR = 0;
            try
            {
                if (ExecuteNonQuery_SendedEvent != null)
                {
                    if (DbAccessPassword())
                    {
                        DbReportImgAdd_SendedEvent(_strConn, strStoredProcImg, strfimg_type, fimg, strforder_by, fremark, strfsample_id);
                        intR = dao.DbReportImgAdd(_strConn, strStoredProcImg, strfimg_type, fimg, strforder_by, fremark, strfsample_id);
                    }
                }
            }
            catch (Exception ex)
            {
                this.WWFSaveLog("执行报告图形sql语句！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            return intR;
        }

       /// <summary>
        /// 执行 仪器图存储
       /// </summary>
       /// <param name="_strConn"></param>
       /// <param name="strStoredProcImg"></param>
       /// <param name="strfimg_type"></param>
       /// <param name="fimg"></param>
       /// <param name="strforder_by"></param>
       /// <param name="fremark"></param>
       /// <param name="strfio_id"></param>
       /// <returns></returns>
        public int DbInstrImgAdd(string _strConn, string strStoredProcImg, String strfimg_type, Byte[] fimg, String strforder_by, String fremark, String strfio_id)
        {

            int intR = 0;
            try
            {
                if (ExecuteNonQuery_SendedEvent != null)
                {
                    if (DbAccessPassword())
                    {
                        DbInstrImgAdd_SendedEvent(_strConn, strStoredProcImg, strfimg_type, fimg, strforder_by, fremark, strfio_id);
                        intR = dao.DbInstrImgAdd(_strConn, strStoredProcImg, strfimg_type, fimg, strforder_by, fremark, strfio_id);
                    }
                }
            }
            catch (Exception ex)
            {
                this.WWFSaveLog("执行仪器图形sql语句！[主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            return intR;
        }




        #region 报表

        public static DataTable ReportReturnDT = new DataTable();
        public static IList ReportIlist = new ArrayList();
        public DataTable DBReport(IList ilt)
        {
            ReportReturnDT = new DataTable();
            ReportIlist = ilt;
            try
            {
                if (DBReport_SendedEvent != null)
                {
                    if (DbAccessPassword())
                    {
                        DBReport_SendedEvent(ilt);
                       // dtReport = 
                        //ww.wwf.dao.nc.hr.综合查询 a = new ww.wwf.dao.nc.hr.综合查询();
                        //a.Show();
                        GetFormByFormNameForm(ReportIlist[0].ToString()).Show();
                        this.WWFSaveLog("报表记录数为：" + DAOServer.ReportReturnDT.Rows.Count.ToString(), "");

                    }
                }
            }
            catch (Exception ex)
            {
                this.WWFSaveLog("执行DBReport [主机名：" + _HostName + " IP地址：" + _HostIpAddress + "]", ex.ToString());
            }
            return ReportReturnDT;
        }

        private System.Windows.Forms.Form GetFormByFormNameForm(string strFormName)
        {
            System.Windows.Forms.Form form = null;
            try
            {
                Type type = Type.GetType(strFormName);
                form = (System.Windows.Forms.Form)Activator.CreateInstance(type);
            }
            catch (Exception ex)
            {
                this.WWFSaveLog("报表设置有误：", ex.ToString());
            }
            return form;
        }

        #endregion

    }
}
