﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
 
namespace 句柄抓抓
{
    /// <summary>
    /// 账号：{"F":"WindowsForms10.Window.8.app.0.59efbf_r6_ad1","C":"WindowsForms10.EDIT.app.0.59efbf_r6_ad1","N":"11","M":"zh"}
    /// </summary>
    public partial class Form1 : Form
    {
        private ScanerHook listener = new ScanerHook();
        public Form1()
        {
            InitializeComponent();
            listener.ScanerEvent += Listener_ScanerEvent;
        }

        private void Listener_ScanerEvent(ScanerHook.ScanerCodes codes)
        {
            string card_no = codes.Result.Replace(';', ':').ToUpper();
            
            listener.Stop();//读卡期间暂停钩子，等待读取完毕再次打开钩子
            if (!string.IsNullOrEmpty(card_no))
            {
                ExecuteReq(card_no);
                txt_电子健康卡扫码.Text = card_no;
                //this.btn_读卡.PerformClick();
                //Clipboard.Clear(); //清空粘贴板
            }
            listener.Start();//重新启动钩子
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.treeView1.Font = new Font(this.treeView1.Font.FontFamily, 10);

            //button1.BackgroundImage = new Bitmap(@"H:\c#\抓抓标靶.png");
            btn_刷新_Click(this, e);

            frm = this.Size;
            tv = treeView1.Size;
            //启动钩子服务
            listener.Start();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            Size frm1 = this.Size;
            treeView1.Size = tv + (frm1 - frm);
            if (frm1.Width < frm.Width || frm1.Height < frm.Height)
            {
                this.Size = frm;
            }
        }

        #region WindowsAPI
        [DllImport("shell32.dll")]
        public static extern int ShellExecute(IntPtr hwnd, StringBuilder lpszOp, StringBuilder lpszFile, StringBuilder lpszParams, StringBuilder lpszDir, int FsShowCmd);
        [DllImport("user32.dll")]
        private static extern bool EnumWindows(WNDENUMPROC lpEnumFunc, int lParam);

        /// <summary>
        /// 获取窗体的句柄函数
        /// </summary>
        /// <param name="lpClassName">窗口类名</param>
        /// <param name="lpWindowName">窗口标题名</param>
        /// <returns>返回句柄</returns>
        [DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]//查找窗口
        private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [DllImport("user32.dll")]
        public static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);
        [DllImport("user32.dll")]
        private static extern int GetWindowTextW(IntPtr hWnd, [MarshalAs(UnmanagedType.LPWStr)]StringBuilder lpString, int nMaxCount);
        [DllImport("user32.dll")]
        private static extern int GetClassNameW(IntPtr hWnd, [MarshalAs(UnmanagedType.LPWStr)]StringBuilder lpString, int nMaxCount);
        private delegate bool WNDENUMPROC(IntPtr hWnd, int lParam);

        [DllImport("User32")]
        private extern static int GetWindow(int hWnd, int wCmd);

        [DllImport("User32")]
        private extern static int GetWindowLongA(int hWnd, int wIndx);

        [DllImport("user32", CharSet = CharSet.Auto)]
        private extern static int GetWindowTextLength(IntPtr hWnd);

        [DllImport("user32.dll")]
        private static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);
        //最大化用
        [DllImport("user32.dll", EntryPoint = "ShowWindow", CharSet = CharSet.Auto)]
        public static extern int ShowWindow(IntPtr hwnd, int nCmdShow);
        //置顶
        [DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);
        ////获取图标开始
        [DllImport("user32.dll", EntryPoint = "GetClassLong")]
        public static extern uint GetClassLongPtr32(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll", EntryPoint = "GetClassLongPtr")]
        public static extern IntPtr GetClassLongPtr64(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        static extern IntPtr SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        //设置文本text
        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        private static extern int SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, string lParam);
        /// <summary>
        /// 遍历子窗体
        /// </summary>
        /// <param name="hwndParent"></param>
        /// <param name="lpEnumFunc"></param>
        /// <param name="lParam"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool EnumChildWindows(IntPtr hwndParent, EnumWindowsProc lpEnumFunc, IntPtr lParam);
        /// <summary>
        /// 获取窗体文本
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="lpString"></param>
        /// <param name="nMaxCount"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern int GetWindowText(int hWnd, IntPtr lpString, int nMaxCount);

        [DllImport("User32.dll", EntryPoint = "FindWindowEx")]
        public static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpClassName, string lpWindowName);//fi
        //得到父句柄
        [DllImport("user32.dll", EntryPoint = "GetParent", SetLastError = true)]
        public static extern IntPtr GetParent(IntPtr hWnd);
        #endregion

        private const int GW_HWNDFIRST = 0;
        private const int GW_HWNDNEXT = 2;
        private const int GWL_STYLE = (-16);
        private const int WS_VISIBLE = 268435456;//所有可见窗口
        private const int WS_BORDER = 8388608;//不知道过滤什么

        #region 获取窗口句柄

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("id", typeof(int));
            dt.Columns.Add("name", typeof(string));
            dt.Columns.Add("parentid", typeof(int));
            dt.Columns.Add("calssname", typeof(string));

            //   dt.Clear();

            if (treeView1.Nodes.Count != 0) treeView1.Nodes.Clear();//删除控件中的节点
            StringBuilder sb = new StringBuilder(256);//类名
            int handle = (int)this.Handle;
            int hwCurr;
            hwCurr = GetWindow(handle, GW_HWNDFIRST);
            treeView1.ImageList = new ImageList();//增加图片列表
            //  treeView1.ImageList.ImageSize = new Size(23,20);

            while (hwCurr > 0)
            {
                int isTask = (WS_VISIBLE | WS_BORDER);//过滤什么了
                //int isTask = (WS_VISIBLE);//过滤什么了
                int lngStyle = GetWindowLongA(hwCurr, GWL_STYLE);
                bool taskWindow = ((lngStyle & isTask) == isTask);
                if (taskWindow)
                {
                    int length = GetWindowTextLength(new IntPtr(hwCurr));
                    StringBuilder sblei = new StringBuilder(256);//类名
                    StringBuilder sb2 = new StringBuilder(256);
                    GetClassNameW((IntPtr)hwCurr, sblei, sblei.Capacity);//类名
                    GetWindowText((IntPtr)hwCurr, sb2, sb2.Capacity);
                    string strTitle = sb2.ToString();
                    //       if (!string.IsNullOrEmpty(strTitle))
                    //      {
                    //  appString.Add(hwCurr.ToString() + sblei.ToString() + strTitle);
                    string a = hwCurr.ToString().PadLeft(8, '0') + "  [" + sblei.ToString() + "]  " + "\"" + strTitle + "\"";

                    list.Clear();
                    listsuoyou.Clear();
                    listClass.Clear();

                    if (GetAppIcon((IntPtr)hwCurr) != null)
                    {
                        treeView1.ImageList.Images.Add(hwCurr.ToString(), GetAppIcon((IntPtr)hwCurr));//根节点图标
                    }
                    else
                    {
                        treeView1.ImageList.Images.Add(hwCurr.ToString(), Properties.Resources.windows);
                    }//根节点图标}

                    DataRow dr = dt.NewRow();//创建与该表具有相同结构的DATAROW 
                    dr["id"] = hwCurr;
                    dr["name"] = a;
                    dr["parentid"] = 0;
                    dr["calssname"] = sblei.ToString();


                    dt.Rows.Add(dr);

                    EnumChildWindows((IntPtr)hwCurr, EnumWindowsMethod, IntPtr.Zero);

                    for (int j = 0; j < listsuoyou.Count; j++)
                    {
                        StringBuilder sbzilei = new StringBuilder(256);//类名
                        StringBuilder sbziming = new StringBuilder(256);
                        GetClassNameW((IntPtr)listsuoyou[j], sbzilei, sbzilei.Capacity);//子句柄类名
                        GetWindowText((IntPtr)listsuoyou[j], sbziming, sbziming.Capacity);

                        int rowno = dt.Select("calssname='" + sbzilei.ToString() + "'").Length;
                        string name = listsuoyou[j].ToString().PadLeft(8, '0') + " [" + (rowno + 1).ToString() + "]" + "  [" + sbzilei.ToString().PadLeft(8, '0') + "]  " + "\"" + sbziming + "\"";

                        DataRow dr1 = dt.NewRow();
                        dr1["id"] = listsuoyou[j];
                        dr1["name"] = name;
                        dr1["parentid"] = (int)GetParent((IntPtr)listsuoyou[j]);
                        dr1["calssname"] = sbzilei.ToString();

                        dt.Rows.Add(dr1);
                    }

                    //写递归的地方
                    //}
                }
                hwCurr = GetWindow(hwCurr, GW_HWNDNEXT);
            }
            CreateTreeView(treeView1, dt);
            for (int abc = 0; abc < treeView1.Nodes.Count; abc++)//循环增加图标
            {
                treeView1.Nodes[abc].ImageIndex = abc;
                treeView1.Nodes[abc].SelectedImageIndex = abc;
                FetchNode(treeView1.Nodes[abc], abc);
            }
        }

        private List<TreeNode> nodeList = new List<TreeNode>();
        private void FetchNode(TreeNode node, int abc)
        {
            nodeList.Add(node);
            node.ImageIndex = abc;
            node.SelectedImageIndex = abc;

            for (int i = 0; i < node.Nodes.Count; i++)
            {
                FetchNode(node.Nodes[i], abc);
            }
        }


        List<string> list = new List<string>();
        List<int> listsuoyou = new List<int>();
        List<string> listClass = new List<string>();

        //委托回调方法
        public delegate bool EnumWindowsProc(int hWnd, int lParam);
        private bool EnumWindowsMethod(int hWnd, int lParam)
        {
            // IntPtr lpString= Marshal.AllocHGlobal(200);
            StringBuilder sblei = new StringBuilder(256);//类名
            StringBuilder sb = new StringBuilder(256);//类名
            GetClassNameW((IntPtr)hWnd, sb, sb.Capacity);//类名
            GetWindowText((IntPtr)hWnd, sblei, 200);//标题
            //  var text = Marshal.PtrToStringAnsi(sblei);
            //  if (!string.IsNullOrWhiteSpace(text))
            list.Add(hWnd.ToString() + "  [" + sb.ToString() + "]  " + "\"" + sblei.ToString() + "\"");//所有

            listsuoyou.Add(hWnd);
            listClass.Add(sb.ToString());

            return true;
        }

        public static void CreateTreeView(TreeView tvControl, DataTable dt)
        {
            TreeView tv = tvControl;
            tv.Nodes.Clear();
            ;
            DataView dvTree = new DataView(dt);
            dvTree.RowFilter = dt.Columns[2].ColumnName + "=0";

            foreach (DataRowView drv in dvTree)
            {
                TreeNode tn = new TreeNode();
                tn.Text = drv[1].ToString();
                tn.Name = drv[0].ToString();
                tn.Tag = drv[0];
                tv.Nodes.Add(tn);
                AddNodes(tn, dt);
            }
        }

        static void AddNodes(TreeNode tn, DataTable dt)
        {
            int i = 0;
            DataView dvTree = new DataView(dt);
            dvTree.RowFilter = dt.Columns[2].ColumnName + "='" + tn.Name + "'";
            foreach (DataRowView drv in dvTree)
            {
                TreeNode ctn = new TreeNode();
                ctn.Text = drv[1].ToString();
                ctn.Name = drv[0].ToString();
                ctn.Tag = drv[0];

                tn.Nodes.Add(ctn);
                AddNodes(ctn, dt);
            }
            i = i + 1;
        } 
        #endregion

        #region 鼠标抓取操作

        private void button1_MouseUp(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;//恢复鼠标样式

            //button1.BackgroundImage = new Bitmap(@"H:\c#\抓抓标靶.png");

            timer1.Enabled = false;

            Rectangle rect12 = new Rectangle(left1, Top1, Right1 - left1, Bottom1 - Top1);

            ControlPaint.DrawReversibleFrame(rect12, Color.Red, FrameStyle.Thick);

            btn_刷新_Click(this, e);
            //  parentname = textBox1.Text.PadLeft(8, '0') + "  ["+ textBox2.Text+"]  \""+ textBox3.Text+"\"" ;
            parentname = txt_当前窗口句柄.Text.PadLeft(8, '0');
            foreach (TreeNode n in treeView1.Nodes)//递归树查找并展开
            {
                ErgodicTreeView(n);
            }
        }
        private void button1_MouseEnter(object sender, EventArgs e)
        {
            //Bitmap bp = new Bitmap(@"H:\c#\指针.png");
            //Color backColor = bp.GetPixel(0, 0);
            //bp.MakeTransparent(backColor);
            //Cursor cursor = new Cursor(bp.GetHicon());
            //this.Cursor = cursor;
            // button1.BackgroundImage = new Bitmap(@"G:\c#\抓抓标靶空白.png");
        }
        private void button1_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Arrow;//恢复鼠标样式
            //button1.BackgroundImage = new Bitmap(@"H:\c#\抓抓标靶.png");
        }

        [StructLayout(LayoutKind.Sequential)]//定义与API相兼容结构体，实际上是一种内存转换  
        public struct POINTAPI
        {
            public int X;
            public int Y;
        }

        [DllImport("user32.dll", EntryPoint = "GetCursorPos")]//获取鼠标坐标  
        public static extern int GetCursorPos(
            ref POINTAPI lpPoint
        );

        [DllImport("user32.dll", EntryPoint = "WindowFromPoint")]//指定坐标处窗体句柄  
        public static extern int WindowFromPoint(
            int xPoint,
            int yPoint
        );

        [DllImport("user32.dll", EntryPoint = "GetWindowText")]
        public static extern int GetWindowText(
            int hWnd,
            StringBuilder lpString,
            int nMaxCount
        );

        [DllImport("user32.dll", EntryPoint = "GetClassName")]
        public static extern int GetClassName(
            int hWnd,
            StringBuilder lpString,
            int nMaxCont
        );
        [DllImport("user32.dll")]
        private static extern int GetWindowRect(IntPtr hwnd, out Rect lpRect);
        [DllImport("user32.dll")]
        private static extern int GetClientRect(IntPtr hwnd, out Rect lpRect);
        public struct Rect
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;

        }

        [DllImport("user32.dll")]
        public static extern int ClientToScreen(int hWnd, out Rect lpPoint);

        [DllImport("user32")]
        public static extern int GetSystemMetrics(int nIndex);
        int left1, Top1, Right1, Bottom1;
        private void button1_MouseDown(object sender, MouseEventArgs e)//按下鼠标
        {
            //Bitmap bp = new Bitmap(@"H:\c#\指针.png");
            //Color backColor = bp.GetPixel(0, 0);
            //bp.MakeTransparent(backColor);
            //Cursor cursor = new Cursor(bp.GetHicon());
            //this.Cursor = cursor;
            //button1.BackgroundImage = new Bitmap(@"H:\c#\抓抓标靶空白.png");

            //获取初始句柄位置
            POINTAPI point = new POINTAPI();//必须用与之相兼容的结构体，类也可以  

            GetCursorPos(ref point);//获取当前鼠标坐标  

            int hwnd1 = WindowFromPoint(point.X, point.Y);//获取指定坐标处窗口的句柄 

            Rect rect = new Rect();
            GetWindowRect((IntPtr)hwnd1, out rect);//获取窗口左上角
            left1 = rect.Left;
            Top1 = rect.Top;
            Right1 = rect.Right;
            Bottom1 = rect.Bottom;

            //获取初始句柄结束
            Rectangle rect12 = new Rectangle(left1, Top1, Right1 - left1, Bottom1 - Top1);

            ControlPaint.DrawReversibleFrame(rect12, Color.Red, FrameStyle.Thick);

            timer1.Enabled = true;
        }
        int hwnd1;
        private void timer1_Tick(object sender, EventArgs e)
        {
            POINTAPI point = new POINTAPI();//必须用与之相兼容的结构体，类也可以  

            GetCursorPos(ref point);//获取当前鼠标坐标  

            int hwnd = WindowFromPoint(point.X, point.Y);//获取指定坐标处窗口的句柄 

            Rect rect = new Rect();
            GetWindowRect((IntPtr)hwnd, out rect);//获取窗口左上角
            int hwnd_x = rect.Left;
            int hwnd_y = rect.Top;

            //xiam
            Rect rect1 = new Rect();
            GetClientRect((IntPtr)hwnd, out rect1);//获取客户区窗口整体大小
            int cc = rect1.Right;
            int dd = rect1.Bottom;

            Rect rect3 = new Rect();
            ClientToScreen(hwnd, out rect3);//转换客户左上角坐标
            int abc = rect3.Left;
            int abc1 = rect3.Top;
            textBox4.Text = (point.X - abc).ToString();
            textBox5.Text = (point.Y - abc1).ToString();

            textBox6.Text = cc.ToString() + "," + dd.ToString();
            textBox7.Text = abc.ToString() + "," + abc1.ToString();

            txt_当前窗口句柄.Text = hwnd.ToString();
            StringBuilder name = new StringBuilder(256);
            GetWindowText(hwnd, name, 256);
            txt_当前窗口标题.Text = name.ToString();
            GetClassName(hwnd, name, 256);
            txt_当前窗口类名.Text = name.ToString();

            int hwndfu = (int)GetParent((IntPtr)hwnd);//父句柄
            if (hwndfu == 0)
            {
                txt_当前父窗句柄.Text = "";
                txt_当前父窗类名.Text = "";
                txt_当前父窗标题.Text = "";
            }
            else
            {
                txt_当前父窗句柄.Text = hwndfu.ToString();
                StringBuilder name1 = new StringBuilder(256);
                GetWindowText(hwndfu, name1, 256);
                txt_当前父窗标题.Text = name1.ToString();
                GetClassName(hwndfu, name1, 256);
                txt_当前父窗类名.Text = name1.ToString();

                int tophwndfu = (int)GetParent((IntPtr)hwndfu);//父句柄
                if (tophwndfu > 0)
                {
                    while (tophwndfu > 0)
                    {
                        StringBuilder topname1 = new StringBuilder(256);
                        GetClassName(tophwndfu, topname1, 256);
                        txt_顶级类名.Text = topname1.ToString();
                        GetWindowText(tophwndfu, topname1, 256);
                        txt_顶级标题.Text = topname1.ToString();
                        tophwndfu = (int)GetParent((IntPtr)tophwndfu);//父句柄
                        
                    }
                }
                else
                {
                    txt_顶级类名.Text = txt_当前父窗类名.Text;
                    txt_顶级标题.Text = txt_当前父窗标题.Text;

                }
            }

            int hwnd_r = rect.Right;
            int hwnd_b = rect.Bottom;

            //  if(point.X < left1|| point.X > Right1|| point.Y<Top1|| point.Y>Bottom1)
            if (hwnd != hwnd1)
            {
                Rectangle rect11 = new Rectangle(left1, Top1, Right1 - left1, Bottom1 - Top1);

                ControlPaint.DrawReversibleFrame(rect11, Color.Red, FrameStyle.Thick);

                Rectangle rect12 = new Rectangle(hwnd_x, hwnd_y, hwnd_r - hwnd_x, hwnd_b - hwnd_y);

                ControlPaint.DrawReversibleFrame(rect12, Color.Red, FrameStyle.Thick);

                left1 = hwnd_x;
                Top1 = hwnd_y;
                Right1 = hwnd_r;
                Bottom1 = hwnd_b;
                hwnd1 = hwnd;
            }
        } 
        #endregion


        public const int GCL_HICONSM = -34;
        public const int GCL_HICON = -14;

        public const int ICON_SMALL = 0;
        public const int ICON_BIG = 1;
        public const int ICON_SMALL2 = 2;

        public const int WM_GETICON = 0x7F;
        public static IntPtr GetClassLongPtr(IntPtr hWnd, int nIndex)
        {
            if (IntPtr.Size > 4)
                return GetClassLongPtr64(hWnd, nIndex);
            else
                return new IntPtr(GetClassLongPtr32(hWnd, nIndex));
        }
        public Bitmap GetAppIcon(IntPtr hwnd)
        {
            IntPtr iconHandle = SendMessage(hwnd, WM_GETICON, ICON_SMALL2, 0);
            if (iconHandle == IntPtr.Zero)
                iconHandle = SendMessage(hwnd, WM_GETICON, ICON_SMALL, 0);
            if (iconHandle == IntPtr.Zero)
                iconHandle = SendMessage(hwnd, WM_GETICON, ICON_BIG, 0);
            if (iconHandle == IntPtr.Zero)
                iconHandle = GetClassLongPtr(hwnd, GCL_HICON);
            if (iconHandle == IntPtr.Zero)
                iconHandle = GetClassLongPtr(hwnd, GCL_HICONSM);

            if (iconHandle == IntPtr.Zero)
                return null;

            Icon icn = Icon.FromHandle(iconHandle);
            Bitmap bitmap = icn.ToBitmap();

            //  return icn;
            return bitmap;
        }
        ///获取图片结束

        string parentname = "";

        #region 操作树结构
        //遍历所有节点，找出指定节点
        void ErgodicTreeView(TreeNode tn)
        {
            //查找到某节点时
            if (tn.Text.Substring(0, 8).Equals(parentname))
            {
                //遍历递归获取父节点，将父节点全部展开
                prenode(tn);
                //选中某节点，并加背景颜色
                treeView1.SelectedNode = tn;
                treeView1.Focus();
                //   treeView1.SelectedNode.BackColor = Color.LightSlateGray;

            }
            foreach (TreeNode n in tn.Nodes)
            {
                ErgodicTreeView(n);
            }
        }

        void prenode(TreeNode m)
        {
            if (m.Parent != null)
            {
                m.Parent.Expand();
                //当为项级节点时
                if (m.Parent.Level == 0)
                {
                    m.Parent.Expand();
                }
                //不是项级节点时
                else
                {
                    prenode(m.Parent);
                }
            }
        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            /*  TreeView treev = sender as TreeView;
              Point point = treev.PointToClient(Cursor.Position);
              TreeViewHitTestInfo info = treev.HitTest(point.X, point.Y);*/
        }

        private void treeView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)//判断你点的是不是右键
            {
                Point ClickPoint = new Point(e.X, e.Y);
                TreeNode CurrentNode = treeView1.GetNodeAt(ClickPoint);

                if (CurrentNode != null && true == CurrentNode.Checked)//判断你点的是不是一个节点
                {
                    /* switch (CurrentNode.Name)//根据不同节点显示不同的右键菜单，当然你可以让它显示一样的菜单
                     {
                         case "":
                             CurrentNode.ContextMenuStrip = contextMenuStrip1;
                             break;
                         default:
                             break;
                     }*/
                }
                if (CurrentNode == null)
                {
                    contextMenuStrip1.Visible = false;
                    return;
                }
                treeView1.SelectedNode = CurrentNode;//选中这个节点
                                                     //获取节点区域的右下角坐标值         
                contextMenuStrip1.Show(treeView1, ClickPoint);
                //在选中的节点的右下角，弹出右键菜单，并设定控制者为treeView
                jiedian = CurrentNode.Text;
            }
        }

        private void 跟踪句柄窗口ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int hwnd = int.Parse(jiedian.Substring(0, 8));
            if (IsIconic(hwndfu((IntPtr)hwnd)))
                ShowWindow(hwndfu((IntPtr)hwnd), 9);//最大化用
            SetForegroundWindow(hwndfu((IntPtr)hwnd));//显示到最前面
            for (int i = 0; i < 6; i++)
            {
                Thread.Sleep(100);

                Rect rect = new Rect();
                GetWindowRect((IntPtr)hwnd, out rect);//获取窗口左上角
                int hwnd_x = rect.Left;
                int hwnd_y = rect.Top;
                int hwnd_r = rect.Right;
                int hwnd_b = rect.Bottom;

                Rectangle rect12 = new Rectangle(hwnd_x, hwnd_y, hwnd_r - hwnd_x, hwnd_b - hwnd_y);

                ControlPaint.DrawReversibleFrame(rect12, Color.Red, FrameStyle.Thick);
            }
        }

        private void 查看句柄信息ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            POINTAPI point = new POINTAPI();//必须用与之相兼容的结构体，类也可以  

            GetCursorPos(ref point);//获取当前鼠标坐标  

            int hwnd = int.Parse(jiedian.Substring(0, 8));


            Rect rect = new Rect();
            GetWindowRect((IntPtr)hwnd, out rect);//获取窗口左上角
            int hwnd_x = rect.Left;
            int hwnd_y = rect.Top;

            //xiam
            Rect rect1 = new Rect();
            GetClientRect((IntPtr)hwnd, out rect1);//获取客户区窗口整体大小
            int cc = rect1.Right;
            int dd = rect1.Bottom;

            Rect rect3 = new Rect();
            ClientToScreen(hwnd, out rect3);//转换客户左上角坐标
            int abc = rect3.Left;
            int abc1 = rect3.Top;
            textBox4.Text = (point.X - abc).ToString();
            textBox5.Text = (point.Y - abc1).ToString();

            textBox6.Text = cc.ToString() + "," + dd.ToString();
            textBox7.Text = abc.ToString() + "," + abc1.ToString();

            txt_当前窗口句柄.Text = hwnd.ToString();
            StringBuilder name = new StringBuilder(256);
            GetWindowText(hwnd, name, 256);
            txt_当前窗口标题.Text = name.ToString();
            GetClassName(hwnd, name, 256);
            txt_当前窗口类名.Text = name.ToString();

            int hwndfu = (int)GetParent((IntPtr)hwnd);//父句柄
            if (hwndfu == 0)
            {
                txt_当前父窗句柄.Text = "";
                txt_当前父窗类名.Text = "";
                txt_当前父窗标题.Text = "";
            }
            else
            {
                txt_当前父窗句柄.Text = hwndfu.ToString();
                StringBuilder name1 = new StringBuilder(256);
                GetWindowText(hwndfu, name1, 256);
                txt_当前父窗标题.Text = name1.ToString();
                GetClassName(hwndfu, name1, 256);
                txt_当前父窗类名.Text = name1.ToString();
            }


        }
        #endregion


        #region 句柄测试
        string jiedian = "";
        private const int SW_HIDE = 0;                                                          //常量，隐藏
        private const int SW_SHOWNORMAL = 1;                                                    //常量，显示，标准状态
        private void btn_隐藏_Click(object sender, EventArgs e)
        {
            int hwnd = int.Parse(txt_当前窗口句柄.Text);
            ShowWindowAsync(hwndfu((IntPtr)hwnd), SW_HIDE);
        }

        private void btn_显示_Click(object sender, EventArgs e)
        {
            int hwnd = int.Parse(txt_当前窗口句柄.Text);
            ShowWindowAsync(hwndfu((IntPtr)hwnd), SW_SHOWNORMAL);
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int y, int Width, int Height, int flags);
        //是否最小化
        [DllImport("user32.dll")]
        public static extern bool IsIconic(IntPtr hWnd);

        private void btn_置顶窗口_Click(object sender, EventArgs e)//置顶按钮
        {
            int hwnd = int.Parse(txt_当前窗口句柄.Text);
            if (IsIconic(hwndfu((IntPtr)hwnd)))
                ShowWindow(hwndfu((IntPtr)hwnd), 9);//最大化用
            SetWindowPos(hwndfu((IntPtr)hwnd), -1, 0, 0, 0, 0, 1 | 2);
        }
        private void btn_取消置顶_Click(object sender, EventArgs e)
        {
            int hwnd = int.Parse(txt_当前窗口句柄.Text);
            if (IsIconic(hwndfu((IntPtr)hwnd))) ;
            ShowWindow(hwndfu((IntPtr)hwnd), 9);//最大化用
            SetWindowPos(hwndfu((IntPtr)hwnd), -2, 0, 0, 0, 0, 1 | 2);
        } 
        #endregion

        public IntPtr hwndfu(IntPtr hwndzi)
        {
            IntPtr hwndbian= hwndzi;

            while (GetParent(hwndbian) != IntPtr.Zero)
            {
                hwndbian = GetParent(hwndbian);
            }

            return hwndbian;
        }

        Size frm, tv;

        #region 复制文本
        private void btn_当前窗口句柄复制_Click_1(object sender, EventArgs e)
        {
            Clipboard.SetText(txt_当前窗口句柄.Text);//复制RTF数据到剪贴板
        }
        private void btn_当前窗口类名复制_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(txt_当前窗口类名.Text);
        }
        private void btn_当前窗口标题复制_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(txt_当前窗口标题.Text);
        }

        private void btn_当前父窗句柄复制_Click_1(object sender, EventArgs e)
        {
            Clipboard.SetText(txt_当前父窗句柄.Text);
        }
        private void btn_当前父窗类名复制_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(txt_当前父窗类名.Text);
        }

        private void btn_当前父窗标题复制_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(txt_当前父窗标题.Text);
        } 
        #endregion

        const int WM_SETTEXT = 0x000C;
        //const int WM_LBUTTONDOWN = 0x0201;
        //const int WM_LBUTTONUP = 0x0202;
        //const int WM_CLOSE = 0x0010;

        IntPtr FindWindowByIndex(IntPtr hWndParent, int index)
        {
            if (index == 0)
                return hWndParent;
            else
            {
                int ct = 0;
                IntPtr result = IntPtr.Zero;
                do
                {
                    result = FindWindowEx(hWndParent, result, this.txt_当前窗口类名.Text, null);
                    if (result != IntPtr.Zero)
                        ++ct;
                }
                while (ct < index && result != IntPtr.Zero);
                return result;
            }
        }


        private void btn获取顶级窗口类名_Click(object sender, EventArgs e)
        {
            int hwndfu = (int)GetParent((IntPtr)Convert.ToInt32((this.txt_当前父窗句柄.Text)));//父句柄
            while (true)
            {
                hwndfu = (int)GetParent((IntPtr)hwndfu);//父句柄
                if (hwndfu == 0)
                {
                    break;
                }
                else
                {
                    StringBuilder name1 = new StringBuilder(256);
                    GetClassName(hwndfu, name1, 256);
                    txt_顶级类名.Text = name1.ToString();
                }
            }            
        }

        private void btn_读卡_Click(object sender, EventArgs e)
        {
            ExecuteReq(txt_电子健康卡扫码.Text);
            //string message = "";
            //string url = "http://192.168.10.171:1811/ehc-portal-push/sms/send";
            //// 请求头
            //JObject request = new JObject();
            //request["appId"] = "1DNUJKJV00000100007F0000F0308269";
            //request["transType"] = "sms.send.checkresult";
            //// **********子参数**********//
            //JObject param = new JObject();
            ////三级参数
            //JObject content = new JObject();

            //content["first"] = "sms.send.checkresult.first";
            //content["keyword1"] = "赵立华";
            //content["keyword2"] = "17Y9400001629";
            //content["keyword3"] = "检查检验";
            //content["keyword4"] = "化验室";
            //content["remark"] = "sms.send.checkresult.remark";
            //param["content"] = content;//三级参数填充到二级参数

            //param["redirectUrl"] = "";
            //param["cardNo"] = "17Y9400001629";
            //param["cardType"] = "11";
            ////三级参数
            //JObject extraParams = new JObject();
            //extraParams["reportNo"] = "676f4edd805a43378f96ff06807ee728";
            //extraParams["reportType"] = "2";
            //extraParams["reportCategoryCode"] = "2";

            //param["extraParams"] = extraParams;
            ////最后汇总
            //request["param"] = param;
            //// **********子参数**********//
            //string data = JsonConvert.SerializeObject(request);

            //WebApiMethod.RequestJsonPostMethod(url, data, out message);
        }
        
        private void btn_发送文本_Click(object sender, EventArgs e)
        {
            IntPtr ParenthWnd = new IntPtr(0);
            IntPtr EdithWnd = new IntPtr(0);
            //查到窗体，得到整个窗体
            ParenthWnd = FindWindow(this.txt_顶级类名.Text, this.txt_顶级标题.Text);

            //判断这个窗体是否有效
            if (!ParenthWnd.Equals(IntPtr.Zero))
            {
                list.Clear();
                listsuoyou.Clear();
                listClass.Clear();
                EnumChildWindows(ParenthWnd, EnumWindowsMethod, IntPtr.Zero);
                int num = 0;
                for (int i = 0; i < listClass.Count; i++)
                {
                    if (listClass[i].Equals(this.txt_当前窗口类名.Text))
                    {
                        num++;
                        if (num == Convert.ToInt32(this.txt更新序号.Text))
                        {
                            EdithWnd = (IntPtr)listsuoyou[i];
                            SendMessage(EdithWnd, WM_SETTEXT, IntPtr.Zero, txt发送文本.Text);
                            //MessageBox.Show(listClass[i] + "[" + listsuoyou[i].ToString());
                            break;
                        }
                    }
                }

                //IntPtr hWndThirdButton = FindWindowByIndex(ParenthWnd, 2); // handle of third "Button" as shown in Spy++
                ////得到Form1这个子窗体的文本框，并设置其内容
                //EdithWnd = FindWindowEx(ParenthWnd, IntPtr.Zero, this.textBox2.Text, null);   //这里获取到的EdithWnd始终为0；

                //if (!EdithWnd.Equals(IntPtr.Zero))
                //{
                //    //调用SendMessage方法设置其内容
                //    SendMessage(EdithWnd, WM_SETTEXT, IntPtr.Zero, textBox3.Text);
                //    //retval++;
                //}
            }
        }

        void ExecuteReq(string cardno)
        {
            //Ylzehc.Ylzehc.ApiUrl = "http://192.168.10.171:1811/ehcService/gateway.do";   // 可按系统配置赋值
            //Ylzehc.Ylzehc.AppKey = "1DOB630TT0510100007F0000D6D2E81F";   // 可按系统配置赋值

            //// 请求头
            //JObject request = new JObject();
            //request["app_id"] = "1DOB630TT0500100007F0000CC065635";   // 可按系统配置赋值
            //request["term_id"] = "371300210001";   // 可按系统配置赋值
            //request["method"] = "ehc.ehealthcode.verify"; // *** 根据具体调用接口入参 *** 
            //request["timestamp"] = DateTime.Now.ToString("yyyyMMddHHmmss");   // *** 根据具体调用时间入参 *** 
            //request["sign_type"] = "MD5";   // 固定，支持MD5\SM3，SDK内部会自动根据该算法计算sign
            //request["version"] = "X.M.0.1";   // 固定
            //request["enc_type"] = "AES";    //  固定，支持AES\SM4，SDK内部会自动根据该算法加密

            //// 业务参数

            //// **********根据接口文档入参，当前仅为二维码验证示例**********//
            //JObject bizParam = new JObject();
            //bizParam["ehealth_code"] = cardno;//"3C7600201CD41759A266852EB9FF24A5B486290A778E31ED68EA25DEC1D370F4:1::3502A0001:";
            //bizParam["out_verify_time"] = DateTime.Now.ToString("yyyyMMddHHmmss");
            //bizParam["out_verify_no"] = System.Guid.NewGuid().ToString("N"); // 唯一编号
            //bizParam["operator_id"] = "001";
            //bizParam["operator_name"] = "测试";
            //bizParam["treatment_code"] = "010101";
            //// **********根据接口文档入参，当前仅为二维码验证示例**********//

            //request["biz_content"] = JsonConvert.SerializeObject(bizParam);

            //JObject res = Ylzehc.Ylzehc.Execute(request);
            //if (res["ret_code"].ToString().Equals("0000"))
            //{
            //    JObject user = JObject.Parse(res["biz_content"].ToString());
            //    this.txt_姓名.Text = user["user_name"].ToString();
            //    this.txt_手机号.Text = user["mobile_phone"].ToString();
            //    this.txt_身份证.Text = user["id_no"].ToString();
            //    this.txt_电子健康卡号.Text = user["card_no"].ToString();
            //    string str = user["birthday"].ToString();

            //    Set_Text(user);
            //}
            ////Console.WriteLine(JsonConvert.SerializeObject(res));
            ////Console.ReadLine();
        }

        private void Set_Text(JObject j)
        {
            //foreach (var cof in CommonUtils.ConfigInstance)
            //{
            //    IntPtr ParenthWnd = new IntPtr(0);
            //    IntPtr EdithWnd = new IntPtr(0);

            //    //查到窗体，得到整个窗体
            //    ParenthWnd = FindWindow(cof.FName, cof.FText);
            //    //判断这个窗体是否有效
            //    if (!ParenthWnd.Equals(IntPtr.Zero))
            //    {
            //        list.Clear();
            //        listsuoyou.Clear();
            //        listClass.Clear();
            //        EnumChildWindows(ParenthWnd, EnumWindowsMethod, IntPtr.Zero);
            //        //if (list.Contains("个人健康档案"))
            //        //{
            //        //    return;
            //        //}
            //        int num = 0;
            //        for (int i = 0; i < listClass.Count; i++)
            //        {
            //            if (listClass[i].Equals(cof.CName))
            //            {
            //                num++;
            //                if (num == Convert.ToInt32(cof.No))
            //                {
            //                    EdithWnd = (IntPtr)listsuoyou[i];
            //                    string sendtext = j[cof.Model].ToString();
            //                    if (cof.Model.Equals("user_sex"))
            //                    {
            //                        //判断男女
            //                        sendtext = j[cof.Model].ToString().Equals("1") ? "男" : "女";
            //                    }
            //                    else if (cof.Model.Equals("birthday"))
            //                    {
            //                        //计算年龄
            //                        DateTime now = DateTime.Now;
            //                        DateTime brh;
            //                        IFormatProvider ifp = new CultureInfo("zh-CN", true);
                                    
            //                        if (DateTime.TryParseExact(j[cof.Model].ToString(), "yyyyMMdd", ifp, DateTimeStyles.None, out brh))
            //                        {                                        
            //                            sendtext = (now.Year - brh.Year).ToString();
            //                        }
            //                        else
            //                        {
            //                            sendtext = "0";
            //                        }
            //                    }
            //                    SendMessage(EdithWnd, WM_SETTEXT, IntPtr.Zero, sendtext);
            //                    break;
            //                }
            //            }
            //        }
            //    }
            //}            
        }

        /// <summary>
        /// 重写系统事件，捕获关闭窗口消息，阻止关闭
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m)
        {
            //新增：重写捕获窗口关闭消息，下有原作者连接
            //Console.WriteLine(m.Msg);
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_CLOSE = 0xF060;
            if (m.Msg == WM_SYSCOMMAND && (int)m.WParam == SC_CLOSE)
            {
                //捕捉关闭窗体消息 http://www.cnblogs.com/sosoft/
                //用户点击关闭窗体控制按钮   注释为最小化窗体   
                //this.WindowState = FormWindowState.Minimized;

                //窗体隐藏
                this.Hide();
                return;
            }
            base.WndProc(ref m);
        }

        private void toolStripMenuItem开机启动_Click(object sender, EventArgs e)
        {
            SetSetupWindowOpenRun(Application.ExecutablePath, "句柄抓抓", "电子健康卡读卡程序");
        }
        /// <summary>
        /// 将文件放到启动文件夹中开机启动 Windows Script Host Object Model
        /// </summary>
        /// <param name="setupPath">启动程序</param>
        /// <param name="linkname">快捷方式名称</param>
        /// <param name="description">描述</param>
        public void SetSetupWindowOpenRun(string setupPath, string linkname, string description)
        {
            //string desktop = Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\" + linkname + ".lnk";
            //if (System.IO.File.Exists(desktop))
            //    System.IO.File.Delete(desktop);
            //IWshRuntimeLibrary.WshShell shell;
            //IWshRuntimeLibrary.IWshShortcut shortcut;
            //try
            //{
            //    shell = new IWshRuntimeLibrary.WshShell();
            //    shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(desktop);
            //    shortcut.TargetPath = setupPath;//程序路径
            //    shortcut.Arguments = "";//参数
            //    shortcut.Description = description;//描述
            //    shortcut.WorkingDirectory = System.IO.Path.GetDirectoryName(setupPath);//程序所在目录
            //    shortcut.IconLocation = setupPath;//图标   
            //    shortcut.WindowStyle = 1;
            //    shortcut.Save();
            //}
            //catch (Exception ex)
            //{
            //    System.Windows.Forms.MessageBox.Show(ex.Message, "友情提示");
            //}
            //finally
            //{
            //    shell = null;
            //    shortcut = null;
            //}
        }

        private void toolStripMenuItem取消开机启动_Click(object sender, EventArgs e)
        {

        }
        private void ToolStripMenuItem重新启动服务_Click(object sender, EventArgs e)
        {
            listener.Start();
        }

        private void toolStripMenuItem退出_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //Windows通知区域图标
        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
        }

    }
}
