﻿namespace 句柄抓抓
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_当前窗口句柄 = new System.Windows.Forms.TextBox();
            this.txt_当前窗口类名 = new System.Windows.Forms.TextBox();
            this.txt_当前窗口标题 = new System.Windows.Forms.TextBox();
            this.btn_当前窗口类名复制 = new System.Windows.Forms.Button();
            this.btn_当前窗口标题复制 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btn_隐藏 = new System.Windows.Forms.Button();
            this.btn_显示 = new System.Windows.Forms.Button();
            this.btn_置顶窗口 = new System.Windows.Forms.Button();
            this.btn_取消置顶 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.btn_当前父窗标题复制 = new System.Windows.Forms.Button();
            this.btn_当前父窗类名复制 = new System.Windows.Forms.Button();
            this.txt_当前父窗标题 = new System.Windows.Forms.TextBox();
            this.txt_当前父窗类名 = new System.Windows.Forms.TextBox();
            this.txt_当前父窗句柄 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.btn_刷新 = new System.Windows.Forms.Button();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.查看句柄信息ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.跟踪句柄窗口ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btn_当前窗口句柄复制 = new System.Windows.Forms.Button();
            this.btn_当前父窗句柄复制 = new System.Windows.Forms.Button();
            this.txt更新序号 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_发送文本 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_顶级类名 = new System.Windows.Forms.TextBox();
            this.btn获取顶级窗口类名 = new System.Windows.Forms.Button();
            this.txt_电子健康卡扫码 = new System.Windows.Forms.TextBox();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem开机启动 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem取消开机启动 = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem重新启动服务 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem退出 = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_读卡 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_姓名 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txt_手机号 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txt_身份证 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txt_电子健康卡号 = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txt发送文本 = new System.Windows.Forms.TextBox();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.txt_顶级标题 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.contextMenuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(67, 32);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(32, 32);
            this.button1.TabIndex = 0;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button1_MouseDown);
            this.button1.MouseEnter += new System.EventHandler(this.button1_MouseEnter);
            this.button1.MouseLeave += new System.EventHandler(this.button1_MouseLeave);
            this.button1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button1_MouseUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(114, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "<--拖动至窗口获取句柄";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "句柄";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 5;
            this.label5.Text = "类名";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 89);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 6;
            this.label6.Text = "标题";
            // 
            // txt_当前窗口句柄
            // 
            this.txt_当前窗口句柄.Location = new System.Drawing.Point(59, 28);
            this.txt_当前窗口句柄.Name = "txt_当前窗口句柄";
            this.txt_当前窗口句柄.Size = new System.Drawing.Size(139, 21);
            this.txt_当前窗口句柄.TabIndex = 7;
            // 
            // txt_当前窗口类名
            // 
            this.txt_当前窗口类名.Location = new System.Drawing.Point(59, 58);
            this.txt_当前窗口类名.Name = "txt_当前窗口类名";
            this.txt_当前窗口类名.Size = new System.Drawing.Size(139, 21);
            this.txt_当前窗口类名.TabIndex = 8;
            // 
            // txt_当前窗口标题
            // 
            this.txt_当前窗口标题.Location = new System.Drawing.Point(59, 86);
            this.txt_当前窗口标题.Name = "txt_当前窗口标题";
            this.txt_当前窗口标题.Size = new System.Drawing.Size(139, 21);
            this.txt_当前窗口标题.TabIndex = 9;
            // 
            // btn_当前窗口类名复制
            // 
            this.btn_当前窗口类名复制.Location = new System.Drawing.Point(204, 56);
            this.btn_当前窗口类名复制.Name = "btn_当前窗口类名复制";
            this.btn_当前窗口类名复制.Size = new System.Drawing.Size(51, 23);
            this.btn_当前窗口类名复制.TabIndex = 10;
            this.btn_当前窗口类名复制.Text = "复制";
            this.btn_当前窗口类名复制.UseVisualStyleBackColor = true;
            this.btn_当前窗口类名复制.Click += new System.EventHandler(this.btn_当前窗口类名复制_Click);
            // 
            // btn_当前窗口标题复制
            // 
            this.btn_当前窗口标题复制.Location = new System.Drawing.Point(204, 84);
            this.btn_当前窗口标题复制.Name = "btn_当前窗口标题复制";
            this.btn_当前窗口标题复制.Size = new System.Drawing.Size(51, 23);
            this.btn_当前窗口标题复制.TabIndex = 11;
            this.btn_当前窗口标题复制.Text = "复制";
            this.btn_当前窗口标题复制.UseVisualStyleBackColor = true;
            this.btn_当前窗口标题复制.Click += new System.EventHandler(this.btn_当前窗口标题复制_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(92, 128);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 12);
            this.label7.TabIndex = 12;
            this.label7.Text = "客户区内鼠标位置";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(48, 156);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(11, 12);
            this.label8.TabIndex = 13;
            this.label8.Text = "X";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(164, 156);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(11, 12);
            this.label9.TabIndex = 14;
            this.label9.Text = "Y";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(65, 153);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(81, 21);
            this.textBox4.TabIndex = 15;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(181, 153);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(74, 21);
            this.textBox5.TabIndex = 16;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(181, 212);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(74, 21);
            this.textBox6.TabIndex = 21;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(65, 212);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(81, 21);
            this.textBox7.TabIndex = 20;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(152, 216);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 12);
            this.label10.TabIndex = 19;
            this.label10.Text = "大小";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(18, 216);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 18;
            this.label11.Text = "左上角";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(92, 187);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 17;
            this.label12.Text = "客户区大小";
            // 
            // btn_隐藏
            // 
            this.btn_隐藏.Location = new System.Drawing.Point(33, 28);
            this.btn_隐藏.Name = "btn_隐藏";
            this.btn_隐藏.Size = new System.Drawing.Size(37, 23);
            this.btn_隐藏.TabIndex = 24;
            this.btn_隐藏.Text = "隐藏";
            this.btn_隐藏.UseVisualStyleBackColor = true;
            this.btn_隐藏.Click += new System.EventHandler(this.btn_隐藏_Click);
            // 
            // btn_显示
            // 
            this.btn_显示.Location = new System.Drawing.Point(72, 28);
            this.btn_显示.Name = "btn_显示";
            this.btn_显示.Size = new System.Drawing.Size(39, 23);
            this.btn_显示.TabIndex = 25;
            this.btn_显示.Text = "显示";
            this.btn_显示.UseVisualStyleBackColor = true;
            this.btn_显示.Click += new System.EventHandler(this.btn_显示_Click);
            // 
            // btn_置顶窗口
            // 
            this.btn_置顶窗口.Location = new System.Drawing.Point(120, 28);
            this.btn_置顶窗口.Name = "btn_置顶窗口";
            this.btn_置顶窗口.Size = new System.Drawing.Size(63, 23);
            this.btn_置顶窗口.TabIndex = 26;
            this.btn_置顶窗口.Text = "置顶窗口";
            this.btn_置顶窗口.UseVisualStyleBackColor = true;
            this.btn_置顶窗口.Click += new System.EventHandler(this.btn_置顶窗口_Click);
            // 
            // btn_取消置顶
            // 
            this.btn_取消置顶.Location = new System.Drawing.Point(187, 28);
            this.btn_取消置顶.Name = "btn_取消置顶";
            this.btn_取消置顶.Size = new System.Drawing.Size(61, 23);
            this.btn_取消置顶.TabIndex = 27;
            this.btn_取消置顶.Text = "取消置顶";
            this.btn_取消置顶.UseVisualStyleBackColor = true;
            this.btn_取消置顶.Click += new System.EventHandler(this.btn_取消置顶_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(43, 67);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(197, 12);
            this.label15.TabIndex = 28;
            this.label15.Text = "对于当前获取到的句柄进行简单测试";
            // 
            // btn_当前父窗标题复制
            // 
            this.btn_当前父窗标题复制.Location = new System.Drawing.Point(200, 71);
            this.btn_当前父窗标题复制.Name = "btn_当前父窗标题复制";
            this.btn_当前父窗标题复制.Size = new System.Drawing.Size(51, 23);
            this.btn_当前父窗标题复制.TabIndex = 38;
            this.btn_当前父窗标题复制.Text = "复制";
            this.btn_当前父窗标题复制.UseVisualStyleBackColor = true;
            this.btn_当前父窗标题复制.Click += new System.EventHandler(this.btn_当前父窗标题复制_Click);
            // 
            // btn_当前父窗类名复制
            // 
            this.btn_当前父窗类名复制.Location = new System.Drawing.Point(200, 43);
            this.btn_当前父窗类名复制.Name = "btn_当前父窗类名复制";
            this.btn_当前父窗类名复制.Size = new System.Drawing.Size(51, 23);
            this.btn_当前父窗类名复制.TabIndex = 37;
            this.btn_当前父窗类名复制.Text = "复制";
            this.btn_当前父窗类名复制.UseVisualStyleBackColor = true;
            this.btn_当前父窗类名复制.Click += new System.EventHandler(this.btn_当前父窗类名复制_Click);
            // 
            // txt_当前父窗标题
            // 
            this.txt_当前父窗标题.Location = new System.Drawing.Point(55, 73);
            this.txt_当前父窗标题.Name = "txt_当前父窗标题";
            this.txt_当前父窗标题.Size = new System.Drawing.Size(139, 21);
            this.txt_当前父窗标题.TabIndex = 36;
            // 
            // txt_当前父窗类名
            // 
            this.txt_当前父窗类名.Location = new System.Drawing.Point(55, 45);
            this.txt_当前父窗类名.Name = "txt_当前父窗类名";
            this.txt_当前父窗类名.Size = new System.Drawing.Size(139, 21);
            this.txt_当前父窗类名.TabIndex = 35;
            // 
            // txt_当前父窗句柄
            // 
            this.txt_当前父窗句柄.Location = new System.Drawing.Point(55, 17);
            this.txt_当前父窗句柄.Name = "txt_当前父窗句柄";
            this.txt_当前父窗句柄.Size = new System.Drawing.Size(139, 21);
            this.txt_当前父窗句柄.TabIndex = 34;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(20, 76);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(29, 12);
            this.label18.TabIndex = 33;
            this.label18.Text = "标题";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(20, 48);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(29, 12);
            this.label19.TabIndex = 32;
            this.label19.Text = "类名";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(20, 20);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(29, 12);
            this.label20.TabIndex = 31;
            this.label20.Text = "句柄";
            // 
            // btn_刷新
            // 
            this.btn_刷新.Location = new System.Drawing.Point(349, 51);
            this.btn_刷新.Name = "btn_刷新";
            this.btn_刷新.Size = new System.Drawing.Size(51, 23);
            this.btn_刷新.TabIndex = 41;
            this.btn_刷新.Text = "刷新";
            this.btn_刷新.UseVisualStyleBackColor = true;
            this.btn_刷新.Click += new System.EventHandler(this.btn_刷新_Click);
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(329, 80);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(627, 378);
            this.treeView1.TabIndex = 42;
            this.treeView1.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseClick);
            this.treeView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.treeView1_MouseDown);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.查看句柄信息ToolStripMenuItem,
            this.跟踪句柄窗口ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(149, 48);
            // 
            // 查看句柄信息ToolStripMenuItem
            // 
            this.查看句柄信息ToolStripMenuItem.Name = "查看句柄信息ToolStripMenuItem";
            this.查看句柄信息ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.查看句柄信息ToolStripMenuItem.Text = "查看句柄信息";
            this.查看句柄信息ToolStripMenuItem.Click += new System.EventHandler(this.查看句柄信息ToolStripMenuItem_Click);
            // 
            // 跟踪句柄窗口ToolStripMenuItem
            // 
            this.跟踪句柄窗口ToolStripMenuItem.Name = "跟踪句柄窗口ToolStripMenuItem";
            this.跟踪句柄窗口ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.跟踪句柄窗口ToolStripMenuItem.Text = "跟踪句柄窗口";
            this.跟踪句柄窗口ToolStripMenuItem.Click += new System.EventHandler(this.跟踪句柄窗口ToolStripMenuItem_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btn_当前窗口句柄复制
            // 
            this.btn_当前窗口句柄复制.Location = new System.Drawing.Point(204, 28);
            this.btn_当前窗口句柄复制.Name = "btn_当前窗口句柄复制";
            this.btn_当前窗口句柄复制.Size = new System.Drawing.Size(51, 23);
            this.btn_当前窗口句柄复制.TabIndex = 43;
            this.btn_当前窗口句柄复制.Text = "复制";
            this.btn_当前窗口句柄复制.UseVisualStyleBackColor = true;
            this.btn_当前窗口句柄复制.Click += new System.EventHandler(this.btn_当前窗口句柄复制_Click_1);
            // 
            // btn_当前父窗句柄复制
            // 
            this.btn_当前父窗句柄复制.Location = new System.Drawing.Point(200, 15);
            this.btn_当前父窗句柄复制.Name = "btn_当前父窗句柄复制";
            this.btn_当前父窗句柄复制.Size = new System.Drawing.Size(51, 23);
            this.btn_当前父窗句柄复制.TabIndex = 44;
            this.btn_当前父窗句柄复制.Text = "复制";
            this.btn_当前父窗句柄复制.UseVisualStyleBackColor = true;
            this.btn_当前父窗句柄复制.Click += new System.EventHandler(this.btn_当前父窗句柄复制_Click_1);
            // 
            // txt更新序号
            // 
            this.txt更新序号.Location = new System.Drawing.Point(190, 69);
            this.txt更新序号.Name = "txt更新序号";
            this.txt更新序号.Size = new System.Drawing.Size(34, 21);
            this.txt更新序号.TabIndex = 45;
            this.txt更新序号.Text = "1";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(159, 72);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(29, 12);
            this.label21.TabIndex = 46;
            this.label21.Text = "序号";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txt_当前窗口句柄);
            this.groupBox1.Controls.Add(this.txt_当前窗口类名);
            this.groupBox1.Controls.Add(this.btn_当前窗口句柄复制);
            this.groupBox1.Controls.Add(this.txt_当前窗口标题);
            this.groupBox1.Controls.Add(this.btn_当前窗口类名复制);
            this.groupBox1.Controls.Add(this.btn_当前窗口标题复制);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.textBox5);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.textBox7);
            this.groupBox1.Controls.Add(this.textBox6);
            this.groupBox1.Location = new System.Drawing.Point(40, 80);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(283, 245);
            this.groupBox1.TabIndex = 47;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "当前窗口";
            // 
            // btn_发送文本
            // 
            this.btn_发送文本.Location = new System.Drawing.Point(229, 67);
            this.btn_发送文本.Name = "btn_发送文本";
            this.btn_发送文本.Size = new System.Drawing.Size(75, 23);
            this.btn_发送文本.TabIndex = 47;
            this.btn_发送文本.Text = "发送文本";
            this.btn_发送文本.UseVisualStyleBackColor = true;
            this.btn_发送文本.Click += new System.EventHandler(this.btn_发送文本_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.btn_隐藏);
            this.groupBox2.Controls.Add(this.btn_显示);
            this.groupBox2.Controls.Add(this.btn_置顶窗口);
            this.groupBox2.Controls.Add(this.btn_取消置顶);
            this.groupBox2.Location = new System.Drawing.Point(40, 331);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(283, 95);
            this.groupBox2.TabIndex = 48;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "句柄测试";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.txt_当前父窗句柄);
            this.groupBox3.Controls.Add(this.txt_顶级标题);
            this.groupBox3.Controls.Add(this.btn_当前父窗句柄复制);
            this.groupBox3.Controls.Add(this.txt_顶级类名);
            this.groupBox3.Controls.Add(this.txt_当前父窗类名);
            this.groupBox3.Controls.Add(this.txt_当前父窗标题);
            this.groupBox3.Controls.Add(this.btn获取顶级窗口类名);
            this.groupBox3.Controls.Add(this.btn_当前父窗类名复制);
            this.groupBox3.Controls.Add(this.btn_当前父窗标题复制);
            this.groupBox3.Location = new System.Drawing.Point(40, 432);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(283, 165);
            this.groupBox3.TabIndex = 49;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "当前父窗";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 33;
            this.label2.Text = "顶级";
            // 
            // txt_顶级类名
            // 
            this.txt_顶级类名.Location = new System.Drawing.Point(55, 101);
            this.txt_顶级类名.Name = "txt_顶级类名";
            this.txt_顶级类名.Size = new System.Drawing.Size(139, 21);
            this.txt_顶级类名.TabIndex = 36;
            // 
            // btn获取顶级窗口类名
            // 
            this.btn获取顶级窗口类名.Location = new System.Drawing.Point(200, 99);
            this.btn获取顶级窗口类名.Name = "btn获取顶级窗口类名";
            this.btn获取顶级窗口类名.Size = new System.Drawing.Size(51, 23);
            this.btn获取顶级窗口类名.TabIndex = 38;
            this.btn获取顶级窗口类名.Text = "获取";
            this.btn获取顶级窗口类名.UseVisualStyleBackColor = true;
            this.btn获取顶级窗口类名.Click += new System.EventHandler(this.btn获取顶级窗口类名_Click);
            // 
            // txt_电子健康卡扫码
            // 
            this.txt_电子健康卡扫码.Location = new System.Drawing.Point(24, 28);
            this.txt_电子健康卡扫码.Name = "txt_电子健康卡扫码";
            this.txt_电子健康卡扫码.Size = new System.Drawing.Size(199, 21);
            this.txt_电子健康卡扫码.TabIndex = 50;
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem开机启动,
            this.toolStripMenuItem取消开机启动,
            this.ToolStripMenuItem重新启动服务,
            this.toolStripMenuItem退出});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(149, 92);
            // 
            // toolStripMenuItem开机启动
            // 
            this.toolStripMenuItem开机启动.Name = "toolStripMenuItem开机启动";
            this.toolStripMenuItem开机启动.Size = new System.Drawing.Size(148, 22);
            this.toolStripMenuItem开机启动.Text = "开机启动";
            this.toolStripMenuItem开机启动.Click += new System.EventHandler(this.toolStripMenuItem开机启动_Click);
            // 
            // toolStripMenuItem取消开机启动
            // 
            this.toolStripMenuItem取消开机启动.Name = "toolStripMenuItem取消开机启动";
            this.toolStripMenuItem取消开机启动.Size = new System.Drawing.Size(148, 22);
            this.toolStripMenuItem取消开机启动.Text = "取消开机启动";
            this.toolStripMenuItem取消开机启动.Visible = false;
            this.toolStripMenuItem取消开机启动.Click += new System.EventHandler(this.toolStripMenuItem取消开机启动_Click);
            // 
            // ToolStripMenuItem重新启动服务
            // 
            this.ToolStripMenuItem重新启动服务.Name = "ToolStripMenuItem重新启动服务";
            this.ToolStripMenuItem重新启动服务.Size = new System.Drawing.Size(148, 22);
            this.ToolStripMenuItem重新启动服务.Text = "重新启动服务";
            this.ToolStripMenuItem重新启动服务.Click += new System.EventHandler(this.ToolStripMenuItem重新启动服务_Click);
            // 
            // toolStripMenuItem退出
            // 
            this.toolStripMenuItem退出.Name = "toolStripMenuItem退出";
            this.toolStripMenuItem退出.Size = new System.Drawing.Size(148, 22);
            this.toolStripMenuItem退出.Text = "退出";
            this.toolStripMenuItem退出.Click += new System.EventHandler(this.toolStripMenuItem退出_Click);
            // 
            // btn_读卡
            // 
            this.btn_读卡.Location = new System.Drawing.Point(229, 28);
            this.btn_读卡.Name = "btn_读卡";
            this.btn_读卡.Size = new System.Drawing.Size(75, 23);
            this.btn_读卡.TabIndex = 52;
            this.btn_读卡.Text = "读卡";
            this.btn_读卡.UseVisualStyleBackColor = true;
            this.btn_读卡.Click += new System.EventHandler(this.btn_读卡_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(361, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 53;
            this.label3.Text = "姓名";
            // 
            // txt_姓名
            // 
            this.txt_姓名.Location = new System.Drawing.Point(397, 45);
            this.txt_姓名.Name = "txt_姓名";
            this.txt_姓名.Size = new System.Drawing.Size(212, 21);
            this.txt_姓名.TabIndex = 54;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(349, 72);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 53;
            this.label13.Text = "手机号";
            // 
            // txt_手机号
            // 
            this.txt_手机号.Location = new System.Drawing.Point(397, 68);
            this.txt_手机号.Name = "txt_手机号";
            this.txt_手机号.Size = new System.Drawing.Size(212, 21);
            this.txt_手机号.TabIndex = 54;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(349, 23);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 12);
            this.label14.TabIndex = 53;
            this.label14.Text = "身份证";
            // 
            // txt_身份证
            // 
            this.txt_身份证.Location = new System.Drawing.Point(397, 20);
            this.txt_身份证.Name = "txt_身份证";
            this.txt_身份证.Size = new System.Drawing.Size(212, 21);
            this.txt_身份证.TabIndex = 54;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(361, 96);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(29, 12);
            this.label16.TabIndex = 53;
            this.label16.Text = "卡号";
            // 
            // txt_电子健康卡号
            // 
            this.txt_电子健康卡号.Location = new System.Drawing.Point(397, 93);
            this.txt_电子健康卡号.Name = "txt_电子健康卡号";
            this.txt_电子健康卡号.Size = new System.Drawing.Size(212, 21);
            this.txt_电子健康卡号.TabIndex = 54;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btn_发送文本);
            this.groupBox4.Controls.Add(this.txt_电子健康卡扫码);
            this.groupBox4.Controls.Add(this.btn_读卡);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.txt_身份证);
            this.groupBox4.Controls.Add(this.txt_电子健康卡号);
            this.groupBox4.Controls.Add(this.txt发送文本);
            this.groupBox4.Controls.Add(this.txt更新序号);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.txt_手机号);
            this.groupBox4.Controls.Add(this.txt_姓名);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Location = new System.Drawing.Point(334, 464);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(621, 133);
            this.groupBox4.TabIndex = 55;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "读卡测试";
            // 
            // txt发送文本
            // 
            this.txt发送文本.Location = new System.Drawing.Point(24, 69);
            this.txt发送文本.Name = "txt发送文本";
            this.txt发送文本.Size = new System.Drawing.Size(132, 21);
            this.txt发送文本.TabIndex = 9;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip2;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "电子健康卡读卡工具";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // txt_顶级标题
            // 
            this.txt_顶级标题.Location = new System.Drawing.Point(55, 129);
            this.txt_顶级标题.Name = "txt_顶级标题";
            this.txt_顶级标题.Size = new System.Drawing.Size(139, 21);
            this.txt_顶级标题.TabIndex = 36;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(20, 132);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(29, 12);
            this.label17.TabIndex = 33;
            this.label17.Text = "标题";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(968, 604);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.btn_刷新);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "电子健康卡读卡程序";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.contextMenuStrip1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.contextMenuStrip2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_当前窗口句柄;
        private System.Windows.Forms.TextBox txt_当前窗口类名;
        private System.Windows.Forms.TextBox txt_当前窗口标题;
        private System.Windows.Forms.Button btn_当前窗口类名复制;
        private System.Windows.Forms.Button btn_当前窗口标题复制;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btn_隐藏;
        private System.Windows.Forms.Button btn_显示;
        private System.Windows.Forms.Button btn_置顶窗口;
        private System.Windows.Forms.Button btn_取消置顶;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btn_当前父窗标题复制;
        private System.Windows.Forms.Button btn_当前父窗类名复制;
        private System.Windows.Forms.TextBox txt_当前父窗标题;
        private System.Windows.Forms.TextBox txt_当前父窗类名;
        private System.Windows.Forms.TextBox txt_当前父窗句柄;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btn_刷新;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 查看句柄信息ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 跟踪句柄窗口ToolStripMenuItem;
        private System.Windows.Forms.Button btn_当前窗口句柄复制;
        private System.Windows.Forms.Button btn_当前父窗句柄复制;
        private System.Windows.Forms.TextBox txt更新序号;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn_发送文本;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_顶级类名;
        private System.Windows.Forms.Button btn获取顶级窗口类名;
        private System.Windows.Forms.TextBox txt_电子健康卡扫码;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.Button btn_读卡;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_姓名;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txt_手机号;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txt_身份证;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txt_电子健康卡号;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txt发送文本;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem开机启动;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem取消开机启动;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem退出;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem重新启动服务;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txt_顶级标题;
    }
}

