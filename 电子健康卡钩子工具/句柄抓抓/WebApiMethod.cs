﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace 句柄抓抓
{
    public class WebApiMethod
    {
        public static string RequestJsonGetMethod(string sUrl, out string sMessage)
        {
            return Request(sUrl, "GET", "", "JSON", out sMessage);
        }

        public static string RequestXmlGetMethod(string sUrl, string sMethod, out string sMessage)
        {
            return Request(sUrl, "GET", "", "XML", out sMessage);
        }

        public static string RequestJsonPostMethod(string sUrl, string sEntity, out string sMessage)
        {
            return Request(sUrl, "POST", sEntity, "JSON", out sMessage);
        }

        public static string RequestXmlPostMethod(string sUrl, string sMethod, string sEntity, out string sMessage)
        {
            return Request(sUrl, "POST", sEntity, "XML", out sMessage);
        }

        public static string Request(string sUrl, string sMethod, string sEntity, string sContentType,
            out string sMessage)
        {
            try
            {
                sMessage = "";
                using (System.Net.WebClient client = new System.Net.WebClient())
                {
                    client.Credentials = CreateAuthenticateValue(sUrl);
                    client.Headers = CreateHeader(sContentType);

                    Uri url = new Uri(sUrl);
                    byte[] bytes = Encoding.UTF8.GetBytes(sEntity);
                    byte[] buffer;
                    switch (sMethod.ToUpper())
                    {
                        case "GET":
                            buffer = client.DownloadData(url);
                            break;
                        case "POST":
                            buffer = client.UploadData(url, "POST", bytes);
                            break;
                        default:
                            buffer = client.UploadData(url, "POST", bytes);
                            break;
                    }

                    return Encoding.UTF8.GetString(buffer);
                }

            }
            catch (WebException ex)
            {
                sMessage = ex.Message;
                var rsp = ex.Response as HttpWebResponse;
                var httpStatusCode = rsp.StatusCode;
                var authenticate = rsp.Headers.Get("WWW-Authenticate");

                return "";
            }
            catch (Exception ex)
            {
                sMessage = ex.Message;
                return "";
            }
        }

        private static WebHeaderCollection CreateHeader(string sContentType)
        {
            switch (sContentType.ToUpper())
            {
                case "JSON":
                    return CreateJsonHeader();
                case "XML":
                    return CreateXmlHeader();
                default:
                    return CreateJsonHeader();
            }
        }

        private static WebHeaderCollection CreateJsonHeader()
        {
            WebHeaderCollection headers = new WebHeaderCollection();
            headers.Add("Content-Type", "application/json; charset=utf-8");
            headers.Add("Accept", "application/json");
            headers.Add("Accept-Encoding: gzip, deflate");
            return headers;
        }

        private static WebHeaderCollection CreateXmlHeader()
        {
            WebHeaderCollection headers = new WebHeaderCollection();
            headers.Add("Content-Type", "application/xml; charset=utf-8");
            headers.Add("Accept", "application/xml");
            headers.Add("Accept-Encoding: gzip, deflate");
            return headers;
        }

        private static CredentialCache CreateAuthenticateValue(string sUrl)
        {
            CredentialCache credentialCache = new CredentialCache();
            credentialCache.Add(new Uri(sUrl), "Digest", new NetworkCredential("xym", "xym"));

            return credentialCache;
        }
    }
}
