﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace 句柄抓抓
{
    /// <summary>
    /// 账号：{"FName":"WindowsForms10.Window.8.app.0.59efbf_r6_ad1","CName":"WindowsForms10.EDIT.app.0.59efbf_r6_ad1","No":"11","Model":"zh"}
    /// </summary>
    public class FConfig
    {
        public FConfig()
        {

        }

        public void SaveConfig()
        {
            var jsonStr = JsonConvert.SerializeObject(this);
            var jsonPath = AppDomain.CurrentDomain.BaseDirectory + "/FConfig.json";
            if (File.Exists(jsonPath))
            {
                //File.Delete(jsonPath);
            }
            File.WriteAllText(jsonPath, jsonStr);
        }

        /// <summary>
        /// 父级类名
        /// </summary>
        public string FName { get; set; }

        /// <summary>
        /// 父级标题
        /// </summary>
        public string FText { get; set; }

        /// <summary>
        /// 子类名
        /// </summary>
        public string CName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string No { get; set; }

        /// <summary>
        /// 填充类名
        /// </summary>
        public string Model { get; set; }

    }

    public class CommonUtils
    {

        private static List<FConfig> fConfig;
        /// <summary>
        /// 获取配置实例
        /// </summary>
        public static List<FConfig> ConfigInstance
        {
            get
            {
                if (fConfig == null)
                {
                    var sourceContent = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/FConfig.json");
                    fConfig = JsonConvert.DeserializeObject<List<FConfig>>(sourceContent);
                }
                return fConfig;
            }
            set
            {
                fConfig = value;
                //fConfig.SaveConfig();
            }
        }
    }
}
