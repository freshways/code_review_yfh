﻿namespace HeadConfig
{
    partial class frmPlugIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPlugIn));
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("功能列表");
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsb添加 = new System.Windows.Forms.ToolStripButton();
            this.tsb保存 = new System.Windows.Forms.ToolStripButton();
            this.tsb查询 = new System.Windows.Forms.ToolStripButton();
            this.tsb生成XML = new System.Windows.Forms.ToolStripButton();
            this.tsb上传 = new System.Windows.Forms.ToolStripButton();
            this.tsb删除 = new System.Windows.Forms.ToolStripButton();
            this.tsb修改 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsb停止 = new System.Windows.Forms.ToolStripButton();
            this.btnImport = new System.Windows.Forms.ToolStripButton();
            this.btnImportAll = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.dgv数据列表 = new System.Windows.Forms.DataGridView();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.rtb查询语句 = new System.Windows.Forms.RichTextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.chk批量导入 = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnFormat = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbo操作类型 = new System.Windows.Forms.ComboBox();
            this.txt功能表 = new System.Windows.Forms.TextBox();
            this.txt功能号 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.txt字段对应 = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tv节点 = new System.Windows.Forms.TreeView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.returnXml = new System.Windows.Forms.RichTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rtbXML = new System.Windows.Forms.RichTextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.TestXML = new System.Windows.Forms.RichTextBox();
            this.toolStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv数据列表)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsb添加,
            this.tsb保存,
            this.tsb查询,
            this.tsb生成XML,
            this.tsb上传,
            this.tsb删除,
            this.tsb修改,
            this.toolStripSeparator1,
            this.tsb停止,
            this.btnImport,
            this.btnImportAll,
            this.toolStripSeparator2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1014, 27);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsb添加
            // 
            this.tsb添加.Image = ((System.Drawing.Image)(resources.GetObject("tsb添加.Image")));
            this.tsb添加.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb添加.Name = "tsb添加";
            this.tsb添加.Size = new System.Drawing.Size(56, 24);
            this.tsb添加.Text = "添加";
            this.tsb添加.Click += new System.EventHandler(this.tsb添加_Click);
            // 
            // tsb保存
            // 
            this.tsb保存.Image = ((System.Drawing.Image)(resources.GetObject("tsb保存.Image")));
            this.tsb保存.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb保存.Name = "tsb保存";
            this.tsb保存.Size = new System.Drawing.Size(56, 24);
            this.tsb保存.Text = "保存";
            this.tsb保存.Click += new System.EventHandler(this.tsb保存_Click);
            // 
            // tsb查询
            // 
            this.tsb查询.Image = ((System.Drawing.Image)(resources.GetObject("tsb查询.Image")));
            this.tsb查询.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb查询.Name = "tsb查询";
            this.tsb查询.Size = new System.Drawing.Size(56, 24);
            this.tsb查询.Text = "查询";
            this.tsb查询.Click += new System.EventHandler(this.tsb查询_Click);
            // 
            // tsb生成XML
            // 
            this.tsb生成XML.Image = ((System.Drawing.Image)(resources.GetObject("tsb生成XML.Image")));
            this.tsb生成XML.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb生成XML.Name = "tsb生成XML";
            this.tsb生成XML.Size = new System.Drawing.Size(82, 24);
            this.tsb生成XML.Text = "生成XML";
            this.tsb生成XML.Click += new System.EventHandler(this.tsb生成XML_Click);
            // 
            // tsb上传
            // 
            this.tsb上传.Image = ((System.Drawing.Image)(resources.GetObject("tsb上传.Image")));
            this.tsb上传.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb上传.Name = "tsb上传";
            this.tsb上传.Size = new System.Drawing.Size(80, 24);
            this.tsb上传.Text = "公卫上传";
            this.tsb上传.Click += new System.EventHandler(this.tsb上传_Click);
            // 
            // tsb删除
            // 
            this.tsb删除.Image = ((System.Drawing.Image)(resources.GetObject("tsb删除.Image")));
            this.tsb删除.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb删除.Name = "tsb删除";
            this.tsb删除.Size = new System.Drawing.Size(56, 24);
            this.tsb删除.Text = "删除";
            this.tsb删除.Click += new System.EventHandler(this.tsb删除_Click);
            // 
            // tsb修改
            // 
            this.tsb修改.Image = ((System.Drawing.Image)(resources.GetObject("tsb修改.Image")));
            this.tsb修改.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb修改.Name = "tsb修改";
            this.tsb修改.Size = new System.Drawing.Size(56, 24);
            this.tsb修改.Text = "修改";
            this.tsb修改.Click += new System.EventHandler(this.tsb修改_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // tsb停止
            // 
            this.tsb停止.Image = ((System.Drawing.Image)(resources.GetObject("tsb停止.Image")));
            this.tsb停止.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb停止.Name = "tsb停止";
            this.tsb停止.Size = new System.Drawing.Size(80, 24);
            this.tsb停止.Text = "停止上传";
            this.tsb停止.Click += new System.EventHandler(this.tsb停止_Click);
            // 
            // btnImport
            // 
            this.btnImport.Image = ((System.Drawing.Image)(resources.GetObject("btnImport.Image")));
            this.btnImport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(100, 24);
            this.btnImport.Text = "HIS执行导入";
            this.btnImport.ToolTipText = "数据库直连导入，理论上是通用的";
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // btnImportAll
            // 
            this.btnImportAll.Image = ((System.Drawing.Image)(resources.GetObject("btnImportAll.Image")));
            this.btnImportAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnImportAll.Name = "btnImportAll";
            this.btnImportAll.Size = new System.Drawing.Size(80, 24);
            this.btnImportAll.Text = "批量导入";
            this.btnImportAll.Click += new System.EventHandler(this.btnImportAll_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 27);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1014, 643);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.splitter1);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(1006, 613);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "编辑区";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(336, 4);
            this.splitter1.Margin = new System.Windows.Forms.Padding(4);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 605);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(247)))), ((int)(((byte)(253)))));
            this.panel2.Controls.Add(this.groupBox7);
            this.panel2.Controls.Add(this.groupBox6);
            this.panel2.Controls.Add(this.groupBox5);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(336, 4);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(666, 605);
            this.panel2.TabIndex = 0;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.dgv数据列表);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox7.Location = new System.Drawing.Point(0, 359);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(666, 246);
            this.groupBox7.TabIndex = 15;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "数据列表";
            // 
            // dgv数据列表
            // 
            this.dgv数据列表.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv数据列表.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv数据列表.Location = new System.Drawing.Point(3, 22);
            this.dgv数据列表.Margin = new System.Windows.Forms.Padding(4);
            this.dgv数据列表.Name = "dgv数据列表";
            this.dgv数据列表.RowTemplate.Height = 27;
            this.dgv数据列表.Size = new System.Drawing.Size(660, 221);
            this.dgv数据列表.TabIndex = 11;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.rtb查询语句);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Location = new System.Drawing.Point(0, 150);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(666, 209);
            this.groupBox6.TabIndex = 14;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "SQL语句";
            // 
            // rtb查询语句
            // 
            this.rtb查询语句.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rtb查询语句.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtb查询语句.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtb查询语句.Location = new System.Drawing.Point(3, 22);
            this.rtb查询语句.Margin = new System.Windows.Forms.Padding(4);
            this.rtb查询语句.Name = "rtb查询语句";
            this.rtb查询语句.Size = new System.Drawing.Size(660, 184);
            this.rtb查询语句.TabIndex = 0;
            this.rtb查询语句.Text = "";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.chk批量导入);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.btnFormat);
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this.cbo操作类型);
            this.groupBox5.Controls.Add(this.txt功能表);
            this.groupBox5.Controls.Add(this.txt功能号);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox5.Location = new System.Drawing.Point(0, 0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(666, 150);
            this.groupBox5.TabIndex = 13;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "上传表";
            // 
            // chk批量导入
            // 
            this.chk批量导入.AutoSize = true;
            this.chk批量导入.Location = new System.Drawing.Point(476, 108);
            this.chk批量导入.Name = "chk批量导入";
            this.chk批量导入.Size = new System.Drawing.Size(91, 20);
            this.chk批量导入.TabIndex = 13;
            this.chk批量导入.Text = "批量导入";
            this.chk批量导入.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 33);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "功能号";
            // 
            // btnFormat
            // 
            this.btnFormat.Location = new System.Drawing.Point(476, 30);
            this.btnFormat.Name = "btnFormat";
            this.btnFormat.Size = new System.Drawing.Size(93, 32);
            this.btnFormat.TabIndex = 12;
            this.btnFormat.Text = "格式化SQL";
            this.btnFormat.UseVisualStyleBackColor = true;
            this.btnFormat.Click += new System.EventHandler(this.btnFormat_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 71);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "功能表";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 109);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "操作类型";
            // 
            // cbo操作类型
            // 
            this.cbo操作类型.FormattingEnabled = true;
            this.cbo操作类型.Items.AddRange(new object[] {
            "新增",
            "删除"});
            this.cbo操作类型.Location = new System.Drawing.Point(133, 106);
            this.cbo操作类型.Margin = new System.Windows.Forms.Padding(4);
            this.cbo操作类型.Name = "cbo操作类型";
            this.cbo操作类型.Size = new System.Drawing.Size(320, 24);
            this.cbo操作类型.TabIndex = 10;
            this.cbo操作类型.Text = "新增";
            // 
            // txt功能表
            // 
            this.txt功能表.Location = new System.Drawing.Point(133, 68);
            this.txt功能表.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt功能表.MaxLength = 20;
            this.txt功能表.Name = "txt功能表";
            this.txt功能表.Size = new System.Drawing.Size(320, 26);
            this.txt功能表.TabIndex = 8;
            // 
            // txt功能号
            // 
            this.txt功能号.Location = new System.Drawing.Point(133, 30);
            this.txt功能号.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt功能号.MaxLength = 200;
            this.txt功能号.Name = "txt功能号";
            this.txt功能号.Size = new System.Drawing.Size(320, 26);
            this.txt功能号.TabIndex = 9;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tabControl2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(4, 4);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(332, 605);
            this.panel1.TabIndex = 0;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(0, 428);
            this.tabControl2.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(332, 177);
            this.tabControl2.TabIndex = 1;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.txt字段对应);
            this.tabPage5.Location = new System.Drawing.Point(4, 26);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(324, 147);
            this.tabPage5.TabIndex = 1;
            this.tabPage5.Text = "列对应";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // txt字段对应
            // 
            this.txt字段对应.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt字段对应.Location = new System.Drawing.Point(3, 3);
            this.txt字段对应.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt字段对应.MaxLength = 20;
            this.txt字段对应.Multiline = true;
            this.txt字段对应.Name = "txt字段对应";
            this.txt字段对应.Size = new System.Drawing.Size(318, 141);
            this.txt字段对应.TabIndex = 8;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.propertyGrid1);
            this.tabPage3.Location = new System.Drawing.Point(4, 26);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage3.Size = new System.Drawing.Size(324, 147);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "参数";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid1.LineColor = System.Drawing.SystemColors.ControlDark;
            this.propertyGrid1.Location = new System.Drawing.Point(4, 4);
            this.propertyGrid1.Margin = new System.Windows.Forms.Padding(4);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(316, 139);
            this.propertyGrid1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tv节点);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(332, 428);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "功能列表";
            // 
            // tv节点
            // 
            this.tv节点.CheckBoxes = true;
            this.tv节点.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tv节点.Location = new System.Drawing.Point(4, 23);
            this.tv节点.Margin = new System.Windows.Forms.Padding(4);
            this.tv节点.Name = "tv节点";
            treeNode1.Name = "RootNode";
            treeNode1.Text = "功能列表";
            this.tv节点.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1});
            this.tv节点.Size = new System.Drawing.Size(324, 401);
            this.tv节点.TabIndex = 2;
            this.tv节点.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.tv节点_AfterCheck);
            this.tv节点.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tv节点_AfterSelect);
            this.tv节点.DoubleClick += new System.EventHandler(this.tv节点_DoubleClick);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel3);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage2.Size = new System.Drawing.Size(1006, 613);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "日志";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(247)))), ((int)(((byte)(253)))));
            this.panel3.Controls.Add(this.groupBox3);
            this.panel3.Controls.Add(this.groupBox2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(4, 4);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(998, 605);
            this.panel3.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.returnXml);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 345);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(998, 260);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "返回信息";
            // 
            // returnXml
            // 
            this.returnXml.Dock = System.Windows.Forms.DockStyle.Fill;
            this.returnXml.Location = new System.Drawing.Point(4, 23);
            this.returnXml.Margin = new System.Windows.Forms.Padding(4);
            this.returnXml.Name = "returnXml";
            this.returnXml.Size = new System.Drawing.Size(990, 233);
            this.returnXml.TabIndex = 0;
            this.returnXml.Text = "";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rtbXML);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(998, 345);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "上传信息";
            // 
            // rtbXML
            // 
            this.rtbXML.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbXML.Location = new System.Drawing.Point(4, 23);
            this.rtbXML.Margin = new System.Windows.Forms.Padding(4);
            this.rtbXML.Name = "rtbXML";
            this.rtbXML.Size = new System.Drawing.Size(990, 318);
            this.rtbXML.TabIndex = 0;
            this.rtbXML.Text = "";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox4);
            this.tabPage4.Location = new System.Drawing.Point(4, 26);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1006, 613);
            this.tabPage4.TabIndex = 2;
            this.tabPage4.Text = "xml测试";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(247)))), ((int)(((byte)(253)))));
            this.groupBox4.Controls.Add(this.button1);
            this.groupBox4.Controls.Add(this.richTextBox1);
            this.groupBox4.Controls.Add(this.TestXML);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(0, 0);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(1006, 613);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(838, 567);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 37);
            this.button1.TabIndex = 2;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(4, 339);
            this.richTextBox1.Margin = new System.Windows.Forms.Padding(4);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(993, 212);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // TestXML
            // 
            this.TestXML.Location = new System.Drawing.Point(4, 16);
            this.TestXML.Margin = new System.Windows.Forms.Padding(4);
            this.TestXML.Name = "TestXML";
            this.TestXML.Size = new System.Drawing.Size(993, 305);
            this.TestXML.TabIndex = 1;
            this.TestXML.Text = "";
            // 
            // frmPlugIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1014, 670);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("楷体", 12F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "frmPlugIn";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PlugIn-数据上传";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPlugIn_FormClosing);
            this.Load += new System.EventHandler(this.frmPlugIn_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv数据列表)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsb添加;
        private System.Windows.Forms.ToolStripButton tsb保存;
        private System.Windows.Forms.ToolStripButton tsb生成XML;
        private System.Windows.Forms.ToolStripButton tsb查询;
        private System.Windows.Forms.ToolStripButton tsb上传;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RichTextBox rtb查询语句;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TreeView tv节点;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txt功能号;
        private System.Windows.Forms.TextBox txt功能表;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgv数据列表;
        private System.Windows.Forms.ComboBox cbo操作类型;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox rtbXML;
        private System.Windows.Forms.ToolStripButton tsb删除;
        private System.Windows.Forms.ToolStripButton tsb修改;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ToolStripButton tsb停止;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RichTextBox returnXml;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RichTextBox TestXML;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.ToolStripButton btnImport;
        private System.Windows.Forms.TextBox txt字段对应;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button btnFormat;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnImportAll;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox chk批量导入;
    }
}