﻿using HeadConfig.Common;
using SharpRaven;
using SharpRaven.Data;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace HeadConfig
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);//捕获系统所产生的异常。
            //MessageBox.Show(args[0]);
            if (args.Length > 0)
            {
                string starttype = args[0].Replace("-", "").ToLower();
                if (starttype == "start" || starttype == "s")
                    Application.Run(new frmSystem("s"));
            }
            else
            {
                Application.Run(new frmSystem());
            }
        }

        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            try
            {
                var ravenClient = new RavenClient("http://cab7f574a9c04e3cba733731e70ff7ac@192.168.10.26:9000/12");
                ravenClient.Capture(new SentryEvent(e.Exception));
            }
            catch (Exception)
            {
            }
            //Msg.Error(e.Exception.Message);//处理系统异常
        }
    }
}