﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
//using Maticsoft.DBUtility;
//using ExportFile;
//using Maticsoft.Model;
using System.Windows.Forms;
//using DevComponents.DotNetBar.Controls;
//using Aspose.Cells;
using Maticsoft.Common;
using System.Collections;
using System.Data.SqlClient;
using HeadConfig.Common;
using SharpRaven;
using SharpRaven.Data;

namespace HeadConfig
{
    public class bll_frmMain
    {
        private static bool _Stop = false;

        public static void Stop( bool bl)
        {
            _Stop = bl; 
        }
        public delegate void SendToTWEventHandler(string Info); //返回结果

        public static event SendToTWEventHandler SendToMsg;

        /// <summary>
        /// 获取预览数据
        /// </summary>
        /// <returns></returns>
        public static DataTable GetYLDB(String strName)
        {
            try
            {
                DataTable dt = DbHelperSQL.Query(string.Format("select top 25 * from {0}", strName)).Tables[0];
                return dt;
            }
            catch
            {
                MessageBox.Show("数据表不存在", "提示");
                return null;
            }
        }

        public static DataTable GetYLDB(String strName,string where)
        {
            try
            {
                DataTable dt = DbHelperSQL.Query(string.Format("select * from {0} " + where, strName)).Tables[0];
                return dt;
            }
            catch
            {
                MessageBox.Show("数据表不存在", "提示");
                return null;
            }
        }

        /// <summary>
        /// 获取数据
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static Object GetSelectDB(String sql)
        {
            try
            {
                DataSet set = DbHelperSQL.Query(sql);
                if (set.Tables.Count <= 0)
                {
                    return "ok";
                }
                return set.Tables[0];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示");
                return null;
            }
        }

        //查询oracle 数据
        public static Object GetOrclSelectDB(string sql)
        {
            try
            {
                DataSet set = DbHelperOra.Query(sql);
                if (set.Tables.Count <= 0)
                {
                    return "ok";
                }
                return set.Tables[0];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示");
                return null;
            }
        }

        /// <summary>

        /// <summary>
        /// 导入数据
        /// </summary>
        public static void SetImport_ForSqlBulk(DataTable dgvImport, string strTbaleName, IBll_FrmainProgressBar ib)
        {
            try
            {
                DataTable dt = dgvImport;
                DbHelperSQL.SqlBulkCopyByDatatable(strTbaleName, dt);

                ib.ISetProgressBarValue(dt.Rows.Count);//进度条
                dgvImport = null;
                ib.ISetProgressBarValue(0);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "提示"); }
        }
        /// <summary>
        /// 导入数据
        /// </summary>
        public static void SetImport_SQL(DataTable dgvImport, string strTbaleName)
        {
            try
            {
                if (_Stop) return; //如果停止方法被调用了,那么停止一切办法就是直接返回
                DataRow rrr = dgvImport.Rows[0];
                string str111 = GetString("where", rrr);
                int i = 0;
                for (int ij = 0; ij < dgvImport.Rows.Count; )
                {
                    DataRow r = dgvImport.Rows[0];
                    StringBuilder sql = new StringBuilder();
                    #region
                    if (Servers.New_db.Count > 0 && Servers.Old_db.Count > 0)
                    {
                        if (!str111.ToString().Trim().Equals(""))
                        {
                            string str1 = GetString("where", r);
                            sql.AppendFormat("if(exists( select top 1 * from {0} where 1=1 {1} ))", strTbaleName, str1);
                            string str4 = GetString("update", r);
                            sql.AppendFormat(" update {0} set {1} where 1=1 {2}", strTbaleName, str4, str1);
                            sql.Append(" else ");
                        }
                        string str2 = GetString("insert_text", r);
                        string str3 = GetString("insert_value", r);
                        sql.AppendFormat("insert into {0}({1})values({2})", strTbaleName, str2, str3);
                    }
                    #endregion
                    #region
                    else
                    {
                        string str5 = GetString("value", r);
                        sql.AppendFormat("insert into {0} values({1})", strTbaleName, str5);
                    }
                    #endregion

                    try
                    {
                        DbHelperSQL.ExecuteSql(sql.ToString());
                    }
                    catch { }

                    //ib.ISetProgressBarValue(++i);//jin.Value += 1;
                    dgvImport.Rows.RemoveAt(0);
                }
                //ib.ISetProgressBarValue(0);//jin.Value = 0;
            }
            catch { return; }
        }

        public static void SetImport_ORCL(DataTable dgvImport, string strTbaleName,string state, System_seting setings)
        {
            try
            {
                if (_Stop) return; //如果停止方法被调用了,那么停止一切办法就是直接返回
                DataRow rrr = dgvImport.Rows[0];
                //string str111 = GetString("where", rrr);
                DataTable LogTable = new DataTable();
                LogTable = SqlHelper.ExecuteDataset(setings.SQLConn,CommandType.Text, "select * from tb_上传日志 where 1=0 ").Tables[0];
                int i = dgvImport.Rows.Count;
                for (int ij = 0; ij < dgvImport.Rows.Count; )
                {
                    DataRow r = dgvImport.Rows[0];
                    StringBuilder sql = new StringBuilder();
                    #region
                    if (Servers.New_db.Count > 0 && Servers.Old_db.Count > 0)
                    {
                        //if (!str111.ToString().Trim().Equals(""))
                        //{
                        //    string str1 = GetString("where", r);
                        //    sql.AppendFormat("if(exists( select top 1 * from {0} where 1=1 {1} ))", strTbaleName, str1);
                        //    string str4 = GetString("update", r);
                        //    sql.AppendFormat(" update {0} set {1} where 1=1 {2}", strTbaleName, str4, str1);
                        //    sql.Append(" else ");
                        //}
                        string str2 = GetString("insert_text", r);
                        string str3 = GetString("insert_value", r);
                        if (string.IsNullOrEmpty(str3)) continue;
                        sql.AppendFormat("insert into {0}({1})values({2})", strTbaleName, str2, str3);
                    }
                    #endregion
                    #region
                    else
                    {
                        string str5 = GetString("value", r);
                        sql.AppendFormat("insert into {0} values({1})", strTbaleName, str5);
                    }
                    #endregion

                    string column = GetString("Key", r);
                    try
                    {
                        int rowi = DbHelperOra.ExecuteNonQuery(setings.ORCLConn,sql.ToString());
                        if (rowi > 0)
                        {
                            DataRow dr = LogTable.NewRow();
                            dr["D001"] = column;
                            dr["功能号"] = strTbaleName;
                            dr["状态"] = "0";
                            dr["类型"] = state;
                            LogTable.Rows.Add(dr);
                            //string uploadLog = "insert into tb_上传日志(D001,功能号,状态) values('" + column + "','" + strTbaleName + "','0')";
                            //DbHelperSQL.ExecuteNonQuery(setings.SQLConn, uploadLog);                            
                        }
                    }
                    catch (Exception ex) 
                    {
                        //Msg.Warning(ij.ToString()+":"+ex.Message + "\n" + column);
                        if (SendToMsg != null)
                            SendToMsg("\n失败信息:" + ex.Message + "\n已跳过:" + column + " 剩余:" + dgvImport.Rows.Count.ToString() + "行 ");
                        dgvImport.Rows.RemoveAt(0);
                        DataRow dr = LogTable.NewRow();
                        dr["D001"] = column;
                        dr["功能号"] = strTbaleName;
                        dr["状态"] = "-1";
                        dr["类型"] = state;
                        LogTable.Rows.Add(dr);
                        //string uploadLog = "insert into tb_上传日志(D001,功能号,状态) values('" + column + "','" + strTbaleName + "','-1')";
                        //DbHelperSQL.ExecuteNonQuery(setings.SQLConn, uploadLog); 
                        var ravenClient = new RavenClient("http://cab7f574a9c04e3cba733731e70ff7ac@192.168.10.26:9000/12");
                        ravenClient.Capture(new SentryEvent(ex));
                        continue;
                    }
                    //ib.ISetProgressBarValue(++i);//jin.Value += 1;
                    
                    dgvImport.Rows.RemoveAt(0);
                }
                if (SendToMsg!=null)
                    SendToMsg("\n处理完成:" + i.ToString() + "-行 " + "|" + strTbaleName+"| ");

                try
                {
                    DbHelperSQL.SqlBulkCopyByDatatable("tb_上传日志", LogTable, setings.SQLConn);
                }
                catch (Exception ex)
                {
                    SendToMsg("\n日志写入失败:" + ex.Message);
                }
                //ib.ISetProgressBarValue(0);//jin.Value = 0;
            }
            catch { return; }
        }

        /// <summary>
        /// 获取查询字符串
        /// </summary>
        public static String GetString(string str, DataRow r)
        {
            try
            {
                String sql = "";
                StringBuilder b1 = new StringBuilder();
                switch (str)
                {
                    #region 获取查询条件
                    case "where":
                        foreach (DictionaryEntry s in Servers.New_db)
                        {
                            foreach (DictionaryEntry s0 in ((Hashtable)(s.Value)))
                            {
                                if (s0.Value.ToString().Equals("true"))
                                {
                                    foreach (DictionaryEntry s1 in ((Hashtable)Servers.Old_db[s.Key]))
                                    {
                                        b1.AppendFormat("and [" + s1.Key.ToString() + "]=" + "'{0}'", r[s0.Key.ToString()].ToString());
                                    }
                                }
                            }
                        }
                        sql = b1.ToString();
                        break;
                    #endregion
                    #region 获取插入字段
                    case "insert_text":
                        foreach (DictionaryEntry s in Servers.Old_db)
                        {
                            foreach (DictionaryEntry s0 in ((Hashtable)(s.Value)))
                            {
                                // b1.Append("[" + s0.Key + "],"); //-- 这种写法在oracle里面不支持↓
                                b1.Append( s0.Key + ",");
                            }
                        }
                        sql = (b1.ToString()).Substring(0, b1.ToString().Length - 1);
                        break;
                    #endregion
                    #region 获取插入值
                    case "insert_value":
                        foreach (DictionaryEntry s in Servers.Old_db)
                        {
                            foreach (DictionaryEntry s0 in (Hashtable)Servers.New_db[s.Key])
                            {
                                //Blob 类型处理，在sql下转换成image或tex的byte
                                if (r[s0.Key.ToString()].GetType() == typeof(byte[]))
                                {
                                    b1.Append("rawtohex('-'),");
                                }
                                else if (r[s0.Key.ToString()].GetType() == typeof(System.DateTime))
                                {
                                    b1.AppendFormat("to_date('{0}','yyyy-mm-dd hh24:mi:ss'),", r[s0.Key.ToString()].ToString().Trim());
                                }
                                else
                                    b1.AppendFormat("'{0}',", r[s0.Key.ToString()].ToString().Trim());
                            }
                        }
                        sql = (b1.ToString()).Substring(0, b1.ToString().Length - 1);
                        break;
                    #endregion
                    #region 获取插入值
                    case "value":
                        foreach (DataColumn re in r.ItemArray)
                        {
                            if (re.DataType.Name == "NUMBER")
                                b1.Append("'0',");
                            else
                                b1.AppendFormat("'{0}',", re.ToString());
                        }
                        sql = (b1.ToString()).Substring(0, b1.ToString().Length - 1);
                        break;
                    #endregion
                    #region 获取修改列值
                    case "update":
                        foreach (DictionaryEntry s in Servers.New_db)
                        {
                            foreach (DictionaryEntry s0 in ((Hashtable)(s.Value)))
                            {
                                foreach (DictionaryEntry s1 in ((Hashtable)Servers.Old_db[s.Key]))
                                {
                                    b1.AppendFormat("[" + s1.Key.ToString() + "]=" + "'{0}',", r[s0.Key.ToString()].ToString());
                                }
                            }
                        }
                        sql = (b1.ToString()).Substring(0, b1.ToString().Length - 1);
                        break;
                    #endregion
                    case "Key":
                        foreach (DictionaryEntry s in Servers.New_db)
                        {
                            foreach (DictionaryEntry s0 in ((Hashtable)(s.Value)))
                            {
                                if (s0.Value.ToString().Equals("true") || s0.Value.ToString().Equals("0"))
                                {
                                    b1.Append(r[s0.Key.ToString()].ToString());
                                    break;
                                }
                            }
                        }
                        sql = b1.ToString();
                        break;
                }
                return sql;
            }
            catch(Exception ex)
            {
                if (SendToMsg != null)
                    SendToMsg("生成脚本错误：|" + ex.Message + "| ");
                return "";
            }
        }

        /// <summary>
        /// 查询数据表
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static bool SelectTable(string p)
        {
            try
            {
                if (DbHelperSQL.Query(string.Format(" select * from sysobjects where [name] = '{0}'", p)).Tables[0].Rows.Count <= 0)
                {
                    return true;
                }
            }
            catch(Exception ex)
            { throw new Exception(ex.Message); }
            return false;
        }

        /// <summary>
        /// 创建数据表
        /// </summary>
        /// <param name="p"></param>
        public static bool CreateTable(string p, DataTable dt)
        {
            try
            {
                StringBuilder br1 = new StringBuilder();
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    br1.AppendFormat(",[{0}] nvarchar(200) ", dt.Columns[i].ColumnName.Replace("(","").Replace(")",""));
                }
                StringBuilder br = new StringBuilder();
                br.AppendFormat(" create table {0} ( isid int identity(1,1) primary key {1})", p, br1.ToString());
                DbHelperSQL.ExecuteSql(br.ToString());
                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
                       
    }

    public interface IBll_FrmainProgressBar
    {
         void ISetProgressBarValue(int ivalue);
    }
}
