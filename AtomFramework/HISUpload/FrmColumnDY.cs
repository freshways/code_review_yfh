﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
//using DevComponents.DotNetBar;
//using Maticsoft.Model;
using System.Collections;
using Maticsoft.Common;

namespace HeadConfig
{
    public partial class FrmColumnDY : Form
    {
        public FrmColumnDY()
        {
            InitializeComponent();
        }
        public ArrayList ar = new ArrayList();
        public ArrayList ar1 = new ArrayList();
        public String TbaleName = string.Empty;
        public StringBuilder sb = new StringBuilder();
        //窗体加载事件
        private void SetColumns_Load(object sender, EventArgs e)
        {
            listnew.Items.Clear();
            listold.Items.Clear();
            ListBing();
        }
        //数据绑定
        public void ListBing()
        {
            try
            {
                foreach (string s in ar1)
                {
                    listold.Items.Add(s);
                }
                listold.SelectedItem = null;
                foreach (string s in ar)
                {
                    listnew.Items.Add(s);
                }
                listnew.SelectedItem = null;
            }
            catch { }
        }
        //添加事件
        private void qu_Click(object sender, EventArgs e)
        {
            try
            {
                while (listold.SelectedItems.Count > 0)
                {
                    int j = 0;
                    for (int i = 0; i < dgvImport.Rows.Count; i++)
                    {
                        if (dgvImport.Rows[i].Cells["sjkl"].Value == null || dgvImport.Rows[i].Cells["sjkl"].Value.ToString().Trim().Equals(""))
                        {
                            j = i;
                            break;
                        }
                    }
                    if (j == dgvImport.Rows.Count - 1) dgvImport.Rows.Add();
                    dgvImport.Rows[j].Cells["sjkl"].Value = listold.SelectedItems[0].ToString();
                    listold.Items.Remove(listold.SelectedItems[0]);
                }
            }
            catch { }
        }
        //添加所有事件
        private void quall_Click(object sender, EventArgs e)
        {
            try
            {
                while (listold.Items.Count > 0)
                {
                    int j = 0;
                    for (int i = 0; i < dgvImport.Rows.Count; i++)
                    {
                        if (dgvImport.Rows[i].Cells["sjkl"].Value == null || dgvImport.Rows[i].Cells["sjkl"].Value.ToString().Trim().Equals(""))
                        {
                            j = i;
                            break;
                        }
                    }
                    if (j == dgvImport.Rows.Count - 1) dgvImport.Rows.Add();
                    dgvImport.Rows[j].Cells["sjkl"].Value = listold.Items[0].ToString();
                    listold.Items.Remove(listold.Items[0]);
                }
            }
            catch { }
        }
        //添加事件
        private void lai_Click(object sender, EventArgs e)
        {
            try
            {
                while (listnew.SelectedItems.Count > 0)
                {
                    int j = 0;
                    for (int i = 0; i < dgvImport.Rows.Count; i++)
                    {
                        if (dgvImport.Rows[i].Cells["drl"].Value == null || dgvImport.Rows[i].Cells["drl"].Value.ToString().Trim().Equals(""))
                        {
                            j = i;
                            break;
                        }
                    }
                    if (j == dgvImport.Rows.Count - 1) dgvImport.Rows.Add();
                    dgvImport.Rows[j].Cells["drl"].Value = listnew.SelectedItems[0].ToString();
                    listnew.Items.Remove(listnew.SelectedItems[0]);
                }
            }
            catch { }
        }
        //添加所有事件
        private void laiall_Click(object sender, EventArgs e)
        {
            try
            {
                while (listnew.Items.Count > 0)
                {
                    int j = 0;
                    for (int i = 0; i < dgvImport.Rows.Count; i++)
                    {
                        if (dgvImport.Rows[i].Cells["drl"].Value == null || dgvImport.Rows[i].Cells["drl"].Value.ToString().Trim().Equals(""))
                        {
                            j = i;
                            break;
                        }
                    }
                    if (j == dgvImport.Rows.Count - 1) dgvImport.Rows.Add();
                    dgvImport.Rows[j].Cells["drl"].Value = listnew.Items[0].ToString();
                    listnew.Items.Remove(listnew.Items[0]);
                }
            }
            catch { }
        }
        //保存事件
        private void buttonX1_Click(object sender, EventArgs e)
        {
            try
            {
                Servers.Old_db.Clear();
                Servers.New_db.Clear();
                foreach (DataGridViewRow r in dgvImport.Rows)
                {
                    if (r.Cells["sjkl"].Value != null && r.Cells["drl"].Value != null &&
                        !r.Cells["sjkl"].Value.ToString().Trim().Equals("") && !r.Cells["drl"].Value.ToString().Trim().Equals(""))
                    {
                        string s = "false";
                        if (r.Cells["cbo"].Value != null && r.Cells["cbo"].Value.Equals(true)) s = "true";
                        Hashtable hb1 = new Hashtable();
                        Hashtable hb2 = new Hashtable();
                        hb1.Add(r.Cells["sjkl"].Value.ToString().Trim(), s);
                        hb2.Add(r.Cells["drl"].Value.ToString().Trim(), s);
                        Servers.Old_db.Add(r.Index, hb1);
                        Servers.New_db.Add(r.Index, hb2);
                    }
                }
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            catch { }
        }
        //重置事件
        private void buttonX2_Click(object sender, EventArgs e)
        {
            try
            {
                listnew.Items.Clear();
                listold.Items.Clear();
                ListBing();
                dgvImport.Rows.Clear();
            }
            catch { }
        }
        //删除操作
        private void dgvImport_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    //删除
                    if (dgvImport.Columns[e.ColumnIndex].Name.Equals("sc"))
                    {
                        if (dgvImport.Rows.Count > 0)
                        {
                            if (dgvImport.Rows[e.RowIndex].Cells["drl"].Value != null && !dgvImport.Rows[e.RowIndex].Cells["drl"].Value.ToString().Equals("")) listnew.Items.Add(dgvImport.Rows[e.RowIndex].Cells["drl"].Value.ToString());
                            if (dgvImport.Rows[e.RowIndex].Cells["sjkl"].Value != null && !dgvImport.Rows[e.RowIndex].Cells["sjkl"].Value.ToString().Equals("")) listold.Items.Add(dgvImport.Rows[e.RowIndex].Cells["sjkl"].Value.ToString());
                            dgvImport.Rows.RemoveAt(e.RowIndex);
                        }
                    }
                }
            }
            catch { }
        }

        private void listold_DoubleClick(object sender, EventArgs e)
        {
            qu.PerformClick();
        }

        private void listnew_DoubleClick(object sender, EventArgs e)
        {
            lai.PerformClick();
        }

        private void btnToSQL_Click(object sender, EventArgs e)
        {
            StringBuilder InserSQL = new StringBuilder();
            StringBuilder UpdateSQL = new StringBuilder();
            try
            {
                StringBuilder br1 = new StringBuilder(); //写入数据库字段
                StringBuilder br2 = new StringBuilder(); //源数据列 
                StringBuilder br3 = new StringBuilder();
                foreach (DataGridViewRow r in dgvImport.Rows)
                {
                    if (r.Cells["sjkl"].Value != null && r.Cells["drl"].Value != null &&
                        !r.Cells["sjkl"].Value.ToString().Trim().Equals("") && !r.Cells["drl"].Value.ToString().Trim().Equals(""))
                    {
                        string s = "";
                        if (r.Cells["cbo"].Value != null && r.Cells["cbo"].Value.Equals(true)) s = "|true";
                        br1.AppendFormat("{0},", r.Cells["sjkl"].Value.ToString().Trim());
                        br2.AppendFormat("{0}{1},", r.Cells["drl"].Value.ToString().Trim(), s);
                        //br3.AppendFormat("a.{0} = b.{1},", r.Cells["sjkl"].Value.ToString().Trim(), r.Cells["drl"].Value.ToString().Trim());
                    }
                }
                sb.Append(br1.ToString().Substring(0, br1.Length - 1) + "&");
                sb.Append(br2.ToString().Substring(0, br2.Length - 1) + "&");
                //InserSQL.AppendFormat(" insert into {0} ({1}) \n", TbaleName, br1.ToString().Substring(0, br1.Length-1));
                //InserSQL.AppendFormat(" select {1}  \n from {0} \n\r", TbaleName, br2.ToString().Substring(0, br2.Length - 1));
                //sb.Append(InserSQL);
                //UpdateSQL.AppendFormat(" update a set {1}  \n from {0} a,{0} b ", TbaleName, br3.ToString().Substring(0, br3.Length - 1));
                //sb.Append(UpdateSQL);


                this.DialogResult = System.Windows.Forms.DialogResult.Yes;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
