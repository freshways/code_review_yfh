﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Xml.XPath;
using System.IO;
using System.Data.SqlClient;
using System.Threading;
using Maticsoft.Common;
using HeadConfig.Common;
using System.Collections;
using SharpRaven;
using SharpRaven.Data;

namespace HeadConfig
{
    public partial class frmSystem : Form
    {
        public frmSystem()
        {
            InitializeComponent();
        }

        public frmSystem(string args)
        {
            InitializeComponent();
            if (args == "s")
            {
                this.checkAuto.Checked = true;
            }

        }
       //private string _OrgCantonNo;
       //public string OrgCantonNo
       //{
       //    get { return _OrgCantonNo = txt机构编号.Text; }
       //}

       private string path=Application.StartupPath+"\\SystemSetings.xml";
        //private string path1 = Application.StartupPath + "\\system.xml";
        private State.StateType StateType;

        System_seting sys_seting = new System_seting();

        #region "方法"

        /// <summary>
        /// 保存检查-检查配置名称是否存在
        /// </summary>
        /// <param name="value">配置名称</param>
        /// <returns></returns>
        private bool Check(string value)
        {
            bool b = false;
            if (tvMain.Nodes[0].Nodes.Count > 0)
            {
                foreach (TreeNode node in tvMain.Nodes[0].Nodes)
                {
                    if (node.Text == value)
                    {
                        b = true;
                        break;
                    }
                }
            }
            return b;
        }

        private bool isNullorEmpty()
        {
            return string.IsNullOrEmpty(txt配置名称.Text.Trim()) || string.IsNullOrEmpty(txtSQLConn.Text.Trim()) ||
                string.IsNullOrEmpty(txt机构编号.Text.Trim()) || string.IsNullOrEmpty(txt接口类型.Text.Trim());

        }

        /// <summary>
        /// 保存检查-检查是否有空项
        /// </summary>
        /// <param name="b"></param>
        private void setEnable(bool b)
        {
            txtSQLConn.Enabled = b;
            txt配置名称.Enabled = b;
            txtWebService.Enabled = b;
            txt机构编号.Enabled = b;
            txt接口类型.Enabled = b;
            this.txtORCL.Enabled = b;
            if (!b)
            {
                btnSave.Visible = b;
                btnGetConn.Visible = b;
            }
        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void ClearText()
        {
            txtSQLConn.Text = "";
            txt配置名称.Text = "";
            txtWebService.Text = "";
            txt机构编号.Text = "";
            txt接口类型.Text = "";
            this.txtORCL.Text = "";
            btnSave.Visible = true;
        }

        /// <summary>
        /// 加载配置列表
        /// </summary>
        private void LoadTree()
        {
            DataSet ds = new DataSet("system");
            ds.ReadXml(path);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    tvMain.Nodes.Clear();
                    TreeNode _node = new TreeNode();
                    _node.Text = "连接配置";
                    tvMain.Nodes.Add(_node);
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        TreeNode node = new TreeNode();
                        node.Text = row["SetingName"].ToString();
                        node.Tag = row;
                        _node.Nodes.Add(node);
                    }
                    tvMain.ExpandAll();
                    //if (tvMain.Nodes.Count > 0 && tvMain.Nodes[0].Nodes.Count>1) //选中节点
                    //    tvMain.SelectedNode = tvMain.Nodes[0].Nodes[0];
                }
            }
        }

        /// <summary>
        /// 创建xml文件
        /// </summary>
        private void CreateXml(string Path)
        {
            StringBuilder xml = new StringBuilder();
            xml.Append("<?xml version='1.0' encoding='GB2312'?> ");
            xml.Append("<Configs>");
            xml.Append("</Configs>");

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml.ToString());
            doc.Save(Path);
        }

        /// <summary>
        /// 保存方法
        /// </summary>
        private void SystemAdd()
        {
            //if (!Connection)
            //{
            //    MessageBox.Show("不能正确配置")
            //}
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(path);           

                //创建一个xml属性节点
                XmlElement xml = doc.CreateElement("Setings");
                xml.SetAttribute("value", txt配置名称.Text.Trim());

                //添加xml属性子节点
                XmlElement ele = doc.CreateElement("SetingName");
                ele.InnerText = txt配置名称.Text.Trim();
                xml.AppendChild(ele);

                ele = doc.CreateElement("InterFaceType");
                ele.InnerText = this.txt接口类型.Text.Trim();
                xml.AppendChild(ele);

                ele = doc.CreateElement("RegionNo");
                ele.InnerText = this.txt机构编号.Text.Trim();
                xml.AppendChild(ele);
                
                ele = doc.CreateElement("SQLConn");
                ele.InnerText = code.EncryptStr(this.txtSQLConn.Text.Trim()); //把数据链接加密处理
                xml.AppendChild(ele);

                ele = doc.CreateElement("WebSer");
                ele.InnerText = this.txtWebService.Text.Trim();
                xml.AppendChild(ele);

                ele = doc.CreateElement("ORCLConn");
                ele.InnerText = code.EncryptStr(this.txtORCL.Text.Trim()); //数据链接都要加密处理
                xml.AppendChild(ele);

                ele = doc.CreateElement("CheckAuto");
                ele.InnerText = this.checkAuto.Checked ? "1" : "";
                xml.AppendChild(ele);

                doc.DocumentElement.AppendChild(xml);
                doc.Save(path);

                LoadTree();

                MessageBox.Show("保存成功！", "消息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "错误提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <returns></returns>
        private bool SystemEdit()
        {
            bool b = true;
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(path);
                string xPath = "//Configs//Setings";
                XmlNodeList nodelist = doc.SelectNodes(xPath);
                foreach (XmlNode node in nodelist)
                {
                    if (node.ChildNodes[0].InnerText == txt配置名称.Text)
                    {
                        node.ChildNodes[1].InnerText = this.txt接口类型.Text.Trim();
                        node.ChildNodes[2].InnerText = this.txt机构编号.Text.Trim();
                        node.ChildNodes[3].InnerText = code.EncryptStr(this.txtSQLConn.Text.Trim());
                        node.ChildNodes[4].InnerText = this.txtWebService.Text.Trim();
                        node.ChildNodes[5].InnerText = code.EncryptStr(this.txtORCL.Text.Trim());
                        if (node.SelectSingleNode("CheckAuto") != null)
                            node.SelectSingleNode("CheckAuto").InnerText = this.checkAuto.Checked ? "1" : "";
                        else
                        {
                            XmlElement _element = doc.CreateElement("CheckAuto");
                            _element.InnerText = this.checkAuto.Checked ? "1" : "";
                            //将节点插入到根节点下
                            node.AppendChild(_element);
                        }

                        break;
                    }
                }

                doc.Save(path);
                LoadTree();
                MessageBox.Show("修改成功！", "消息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnSave.Visible = false;
            }
            catch (Exception ex)
            {
                b = false;
                MessageBox.Show(ex.Message, "错误提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return b;
        }

        /// <summary>
        /// 删除
        /// </summary>
        private void SystemDelete()
        {
            try
            {
                DataSet ds = new DataSet("system");
                ds.ReadXml(path);

                int index = -1;
                foreach (TreeNode node in tvMain.Nodes[0].Nodes)
                {
                    if (node.Text.Trim() == txt配置名称.Text.Trim())
                    {
                        index = node.Index;
                        break;
                    }
                }
                if (index != -1)
                    ds.Tables[0].Rows.RemoveAt(index);

                ds.WriteXml(path);
                LoadTree();
                MessageBox.Show("删除成功！", "消息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "错误提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// 检查ip格式
        /// </summary>
        /// <returns></returns>
        private bool CheckIP()
        {
            string pat = @"^(((\d{1})|(\d{2})|(1\d{2})|(2[0-4]\d{1})|(25[0-5]))\.){3}((\d{1})|(\d{2})|(1\d{2})|(2[0-4]\d{1})|(25[0-5]))$";
            Match m=Regex.Match(txtSQLConn.Text,pat);
            return m.Success;
        }

        /// <summary>
        /// 测试数据库链接
        /// </summary>
        /// <returns></returns>
        private bool Connection()
        {
            bool b = false;
            SqlConnection con=null;
            try
            {
                //string connection = "Data Source=" + txtSQLConn.Text.Trim()  + ";Initial Catalog=" + txtWebService.Text.Trim()  + ";User ID=" + txt机构编号.Text.Trim() + ";password=" + txt接口类型.Text.Trim();
                string connection = txtSQLConn.Text.Trim();
                con = new SqlConnection(connection);
                con.Open();
                b = true;
            }
            catch (Exception ex)
            {
                b = false;
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                }
            }

            return b;
        }

        public static string GetConnectionString(string DBType)
        {
            string result = string.Empty;
            string text = DBType.Trim();
            if (text != null)
            {
                if (!(text == "SQL_SERVER"))
                {
                    if (!(text == "ORACLE"))
                    {
                        if (!(text == "ORACLE_DEVART"))
                        {
                            if (text == "ACCESS")
                            {
                                result = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\\mandala.mdb;Persist Security Info=False;";
                            }
                        }
                        else
                        {
                            result = "User ID=lchis;Password=his;Server=127.0.0.1;Direct=True;Sid=rx6600;";
                        }
                    }
                    else
                    {
                        //result = "Data Source=HIS;Persist Security Info=True;User ID=emr;Password=emr_user;Unicode=True;";
                        result = "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=127.0.0.1)(PORT=1521)) (CONNECT_DATA=(SERVICE_NAME=ORCL)));User Id=Test;Password=123";
                    }
                }
                else
                {
                    result = "server=192.168.1.1; database=HIS;uid=sa; pwd=123456;";
                }
            }
            return result;
        }

        #endregion

        #region "窗体事件"
        private void Form1_Load(object sender, EventArgs e)
        {
            #region 定时上传任务
            //初始化timer
            timer.Interval = (1000 * 120); //5分钟执行一次
            timer.Tick += Timer_Tick;
            //初始化后台上传进程
            BgWork = new BackgroundWorker();
            BgWork.WorkerSupportsCancellation = true;
            BgWork.WorkerReportsProgress = true;
            BgWork.DoWork += BgWork_DoWork;
            BgWork.RunWorkerCompleted += BgWork_RunWorkerCompleted;
            #endregion

            if (File.Exists(path))
            {
                LoadTree();
            }
            else
            {
                CreateXml(path);
            }

            if (this.checkAuto.Checked)
            {//在加载完成之后判断是否自动运行
                start();
            }
            //txt配置名称.DataBindings.Add("Text", sys_seting, "配置名称");
        }

        #endregion

        #region 关闭样式
        private void pbClose_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pic = sender as PictureBox;
            if (pic != null)
            {
                pic.BackColor = Color.FromArgb(193, 210, 238);
                pic.BorderStyle = BorderStyle.FixedSingle;
            }
        }

        private void pbClose_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = sender as PictureBox;
            if (pb != null)
            {
                pb.BackColor = this.panToolBar.BackColor;
                pb.BorderStyle = BorderStyle.None;
            }
        } 
        #endregion

        #region treeView事件

        private void tvMain_Enter(object sender, EventArgs e)
        {
            this.panToolBar.BackColor = SystemColors.ActiveCaption;
            this.lblToolBar.ForeColor = SystemColors.ActiveCaptionText;
        }

        private void tvMain_Leave(object sender, EventArgs e)
        {
            this.panToolBar.BackColor = SystemColors.InactiveCaption;
            this.panToolBar.ForeColor = SystemColors.InactiveCaptionText;
        }

        /// <summary>
        /// 配置列表选定后加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tvMain_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node != tvMain.Nodes[0])
            {
                this.tlbMain_Modify.Enabled = true;
                this.tlbMain_Delete.Enabled = true;
                setEnable(false);
                DataRow row = e.Node.Tag as DataRow;
                if (row != null)
                {
                    txt配置名称.Text = row["SetingName"].ToString();
                    sys_seting.配置名称 = row["SetingName"].ToString();

                    txt接口类型.Text = row["InterFaceType"].ToString();
                    sys_seting.接口类型 = row["InterFaceType"].ToString();

                    txt机构编号.Text = row["RegionNo"].ToString();
                    sys_seting.机构编号 = row["RegionNo"].ToString();

                    txtSQLConn.Text = code.DecryptStr(row["SQLConn"].ToString());
                    sys_seting.SQLConn = code.DecryptStr(row["SQLConn"].ToString());

                    txtWebService.Text = row["WebSer"].ToString();
                    sys_seting.WebService = row["WebSer"].ToString();

                    txtORCL.Text = code.DecryptStr(row["ORCLConn"].ToString());
                    sys_seting.ORCLConn = code.DecryptStr(row["ORCLConn"].ToString());

                    if(row.Table.Columns.Contains("CheckAuto"))
                    {
                        checkAuto.Checked = row["CheckAuto"].ToString() == "1" ? true : false;
                        sys_seting.CheckAuto = row["CheckAuto"].ToString();
                    }
                }
            }
            else
            {
                btnSave.Visible = false;
                this.tlbMain_Modify.Enabled = false;
                this.tlbMain_Delete.Enabled = false;
                setEnable(false);
            }
        }

        #endregion

        #region 按钮事件
        /// <summary>
        /// 隐藏配置列表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pbClose_Click(object sender, EventArgs e)
        {
            this.scMain.Panel1Collapsed = true;
        }

        /// <summary>
        /// 隐藏/显示配置列表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tlbMain_show_Click(object sender, EventArgs e)
        {
            this.scMain.Panel1Collapsed = !this.scMain.Panel1Collapsed;
        }


        /// <summary>
        /// 新建配置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tlbMain_New_Click(object sender, EventArgs e)
        {
            StateType = State.StateType.Add;
            this.tlbMain_Modify.Enabled = false;
            this.tlbMain_Delete.Enabled = false;
            ClearText();
            btnGetConn.Visible = true;
            setEnable(true);
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tlbMain_Modify_Click(object sender, EventArgs e)
        {
            StateType = State.StateType.Edit;
            btnSave.Visible = true;
            setEnable(true);
            txt配置名称.Enabled = false;
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            //if (!CheckIP())
            //{
            //    MessageBox.Show("服务器IP不是正确的IP地址，请检查！","提示",MessageBoxButtons.OK,MessageBoxIcon.Information );
            //    txtSQLConn.Focus();
            //    txtSQLConn.SelectAll();
            //    return;
            //}
            switch (StateType)
            {
                case State.StateType.Add:
                    if (!isNullorEmpty())
                    {
                        if (!Check(txt配置名称.Text.Trim()))
                        {
                            SystemAdd();
                        }
                        else
                            MessageBox.Show("配置文件中已存在相同配置名称！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                        MessageBox.Show("配置名称、接口类型、机构编号、数据库链接不应为空！", "提示",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case State.StateType.Edit:
                    SystemEdit();
                    break;
            }
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tlbMain_Delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("确定要删除该配置吗？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                SystemDelete();
        }

        /// <summary>
        /// 检查链接
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btntest_Click(object sender, EventArgs e)
        {
            if (!Connection())
                MessageBox.Show("该配置不能正确连接,请检查！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
                MessageBox.Show("连接成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// 获取链接字符串格式
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGetConn_Click(object sender, EventArgs e)
        {
            this.txtSQLConn.Text = GetConnectionString("SQL_SERVER");
        }

        /// <summary>
        /// 获取链接字符串格式-ORCL
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGetConn2_Click(object sender, EventArgs e)
        {
            this.txtORCL.Text = GetConnectionString("ORACLE");
        }
        #endregion

        private void btnPlugIn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txt配置名称.Text.Trim()))
            {
                MessageBox.Show("请选中节点后进行调试！");
                return;
            }
            foreach (Form frms in Application.OpenForms)
            {
                if (frms is frmPlugIn)
                {
                    frms.Activate();
                    frms.WindowState = FormWindowState.Normal;
                    return;
                }
            }
            //frmPlugIn frm = new frmPlugIn(this.txt接口类型.Text, this.txt机构编号.Text);
            frmPlugIn frm = new frmPlugIn(sys_seting);
            frm.Show();
        }

        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        /// <summary>
        /// 开启定时执行任务
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBegin_Click(object sender, EventArgs e)
        {
            start();
        }

        void start()
        {
            stopor = false;
            timer.Start();
            btnBegin.Enabled = false;
            btnEnd.Enabled = true;
            //初始化上传方法
            bll_frmMain.Stop(false);
            //注册数据上传消息返回事件
            bll_frmMain.SendToMsg += bll_frmMain_SendToMsg;
            UploadMessage_SendToMsg("\n数据传输将在["+ dateTimePicker1.Text + "]之后开始！");
        }


        /// <summary>
        /// 停止后台任务
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnd_Click(object sender, EventArgs e)
        {
            Stop();
        }

        void Stop()
        {
            stopor = true;
            timer.Stop();
            btnBegin.Enabled = true;
            btnEnd.Enabled = false;
            //声名上传方法停止上传
            bll_frmMain.Stop(true);
            //取消消息返回时间注册
            bll_frmMain.SendToMsg -= bll_frmMain_SendToMsg;

            UploadMessage_SendToMsg("\n手动停止上传！");
            if (BgWork.IsBusy)
            {
                BgWork.CancelAsync();
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (ListSys.Count <= 0)
            {
                //判断当前时间是否在工作时间段内
                string _strWorkingDayAM = dateTimePicker1.Text;//工作时间上午
                string _strWorkingDayPM = dateTimePicker2.Text;
                TimeSpan dspWorkingDayAM = DateTime.Parse(_strWorkingDayAM).TimeOfDay;
                TimeSpan dspWorkingDayPM = DateTime.Parse(_strWorkingDayPM).TimeOfDay;

                //string timeStr = "2018-6-8 23:49:00";
                DateTime t1 = DateTime.Now;//Convert.ToDateTime(timeStr);

                TimeSpan dspNow = t1.TimeOfDay;
                if (dspNow > dspWorkingDayAM && dspNow < dspWorkingDayPM)
                {
                    if (!BgWork.IsBusy && stopor == false)
                    {//如果后台任务是停止的，则重新开始
                        Thread thread = new Thread(new ThreadStart(BeginLoadTreeMain));
                        thread.Start();
                    }
                }
                //else if (ListSQL.Count <= 0)
                //{
                //    UploadMessage_SendToMsg("\n==>>任务尚未开始，未到指定时间！");
                //}
            }
            
        }

        private void BgWork_DoWork(object sender, DoWorkEventArgs e)
        {
            if (stopor) return;
            try
            {
                if (e.Argument != null)
                {
                    string BeginTime = e.Argument.ToString();
                }
                foreach (System_seting seting in ListSys)
                {
                    BeginLoadHIS(seting);//获取要上传的数据。
                    //循环队列
                    while (ListSQL.Count > 0)
                    {
                        //如果停止方法被调用了,那么停止一切办法就是直接返回
                        if (stopor)
                        {
                            ListSQL.Clear();
                            return;
                        } 
                        //验证是否把字段进行对应。
                        Servers.ToHash(ListSQL[0].s_字段对应);
                        //如果当前要上传表没有对应字段，则跳过此表
                        if (Servers.New_db.Count <= 0)
                        {
                            UploadMessage_SendToMsg("\n==========>>【错误】：【" + ListSQL[0].s_功能表 + "】字段对应失败，即将跳过！");
                            //删除当前对应错误的sql语句
                            ListSQL.RemoveAt(0);
                            continue;
                        }
                        //查询此表数据进行导入
                        DataTable dt_import = GetImportTable(ListSQL[0].s_查询语句, ListSQL[0].s_功能号, seting);
                        //如果列表不为空执行导入
                        if (dt_import != null && dt_import.Rows.Count > 0)
                        {
                            bll_frmMain.SetImport_ORCL(dt_import, ListSQL[0].s_功能号, ListSQL[0].s_操作类型, seting);
                        }
                        else
                        {
                            UploadMessage_SendToMsg("\n==========>>【警告】：【" + ListSQL[0].s_功能表 + "】未获取到数据，即将跳过！");
                            //throw new Exception("【警告】：【" + ListSQL[0].s_功能表 + "】未获取到数据，即将跳过！");
                        }
                        ListSQL.RemoveAt(0);
                        if (ListSQL.Count <= 0)
                        {
                            UploadMessage_SendToMsg("\n====>>【" + seting.配置名称 + "】上传完成！");
                            break;//stopor = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Msg.Warning(ex.Message);
            }
        }       

        /// <summary>
        /// 查询SQL返回1000行
        /// </summary>
        /// <param name="SQL"></param>
        /// <returns></returns>
        private DataTable GetImportTable(string SQL, string Function,System_seting settings)
        {
            try
            {
                DataSet ds = new DataSet();
                string newsql = "select top 1000 * from ({0}) a"; //设置每1000行处理一次
                newsql = string.Format(newsql, SQL);

                using (SqlConnection conn = SqlHelper.conn(settings.SQLConn))
                {
                    SqlCommand cmd = new SqlCommand(newsql, conn);
                    conn.Open();
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    sda.Fill(ds);
                }
                //ds  = DbHelperSQL.Query(sql);
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                UploadMessage_SendToMsg("\n==========>>【异常】：查询前1000行错误-[" + Function + "]：" + ex.Message);
                var ravenClient = new RavenClient("http://cab7f574a9c04e3cba733731e70ff7ac@192.168.10.26:9000/12");
                ravenClient.Capture(new SentryEvent(ex.Message));
                return null;
            }
        }

        private void BgWork_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (stopor)
            {
                UploadMessage_SendToMsg("\n==>>手动停止上传！");
                return; //停止上传
            }

            //执行完毕情况队列
            ListSys.Clear();
            UploadMessage_SendToMsg("\n==>>上传结束！");
        }


        List<System_seting> ListSys = new List<System_seting>();
        List<HISUploadFile> ListSQL = new List<HISUploadFile>();
        BackgroundWorker BgWork = null;
        //是否停止
        bool stopor = false;

        string pathSub = Application.StartupPath + "\\PlugInSetings{0}.xml";

        /// <summary>
        /// 开始任务后，加载所有主配置
        /// </summary>
        private void BeginLoadTreeMain()
        {
            try
            {
                UploadMessage_SendToMsg("\n==>>开始：等待任务加入队列。。。");
                foreach (TreeNode n in this.tvMain.Nodes)
                {
                    if (n.Nodes.Count > 0)
                    {
                        foreach (TreeNode ns in n.Nodes)
                        {
                            if (ns.Tag == null) continue;
                            else
                            {
                                DataRow row = ns.Tag as DataRow;
                                if (row != null && row["CheckAuto"].ToString() == "1")
                                {
                                    UploadMessage_SendToMsg("\n====>>【" + row["SetingName"].ToString() + "】加入队列！ ");

                                    sys_seting = new System_seting();

                                    sys_seting.配置名称 = row["SetingName"].ToString(); 

                                    sys_seting.接口类型 = row["InterFaceType"].ToString();

                                    sys_seting.机构编号 = row["RegionNo"].ToString();

                                    sys_seting.SQLConn = code.DecryptStr(row["SQLConn"].ToString());

                                    sys_seting.WebService = row["WebSer"].ToString();

                                    sys_seting.ORCLConn = code.DecryptStr(row["ORCLConn"].ToString());

                                    ListSys.Add(sys_seting);
                                }
                            }
                        }
                    }
                }
                
                UploadMessage_SendToMsg("\n==>>结束：共计【" + ListSys.Count.ToString() + "】条任务加入队列等待数据上传！");
                //BeginLoadHIS();
                //启动后台上传任务
                if (!BgWork.IsBusy)
                {
                    BgWork.RunWorkerAsync();
                }
            }
            catch (Exception ex)
            {
                //HeadConfig.Common.Msg.Information(ex.Message);
                UploadMessage_SendToMsg("\n ==========>>BeginLoadTreeMain:" + ex.Message);
            }
        }

        /// <summary>
        /// 获取主配置下所有的执行sql语句
        /// </summary>
        private void BeginLoadHIS(System_seting sys)
        {
            try
            {
                DataSet ds = new DataSet("system");
                ds.ReadXml(string.Format(pathSub, sys.配置名称));
                UploadMessage_SendToMsg("\n====>>【" + sys.配置名称 + "】:获取表[" + ds.Tables[0].Rows.Count.ToString() + "] ");
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        if (string.IsNullOrEmpty(row["OperateType"].ToString())) continue;
                        else
                        {
                            HISUploadFile histype = new HISUploadFile();
                            histype.s_功能表 = row["SetingName"].ToString();
                            histype.s_功能号 = row["FuncCode"].ToString();//FuncCode
                            histype.s_操作类型 = row["OperateType"].ToString();//OperateType
                            string txt查询语句 = code.DecryptStr(row["SQLConn"].ToString());//需要先解密语句
                            histype.s_查询语句 = string.Format(txt查询语句, sys.机构编号).ToString();

                            if (row.Table.Columns.Contains("SQLcolumn"))
                                histype.s_字段对应 = row["SQLcolumn"].ToString();
                            if (row.Table.Columns.Contains("CheckAllin"))
                                histype.s_批量上传 = row["CheckAllin"].ToString();
                            if (!string.IsNullOrEmpty(histype.s_查询语句) &&
                                    !string.IsNullOrEmpty(histype.s_批量上传))//如果查询语句为空则跳过
                                ListSQL.Add(histype);
                        }
                    }
                    UploadMessage_SendToMsg("\n====>>【" + sys.配置名称 + "】:待上传表[" + ListSQL.Count.ToString() + "] ");
                }
            }
            catch (Exception ex)
            {
                UploadMessage_SendToMsg("\n==========>> BeginLoadHIS:" + ex.Message);
            }
        }

        /// <summary>
        /// 返回消息填充-上传队列
        /// </summary>
        /// <param name="Info"></param>
        void UploadMessage_SendToMsg(string Info)
        {
            this.Invoke((EventHandler)(delegate { this.rtbXML.AppendText(Info + DateTime.Now.ToString()); }));
        }

        /// <summary>
        /// 返回消息填充-上传返回信息
        /// </summary>
        /// <param name="Info"></param>
        void bll_frmMain_SendToMsg(string Info)
        {
            this.Invoke((EventHandler)(delegate { this.returnXml.AppendText(Info + DateTime.Now.ToString()); }));
        }

        private void frmSystem_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ListSQL.Count > 0)
            {
                if (MessageBox.Show("还有没执行完的任务，是否关闭？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }
            }
            UploadMessage_SendToMsg("窗体正在关闭，将停止上传操作！");
            //如果程序还在运行，先进行停止
            stopor = true;
            bll_frmMain.Stop(stopor);
            if (BgWork != null && BgWork.IsBusy)
            {
                BgWork.CancelAsync();
            }
            //取消消息返回时间注册
            bll_frmMain.SendToMsg -= bll_frmMain_SendToMsg;
        }
    }
}