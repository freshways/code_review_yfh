﻿namespace HeadConfig
{
    partial class FrmColumnDY
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmColumnDY));
            this.listold = new System.Windows.Forms.ListBox();
            this.listnew = new System.Windows.Forms.ListBox();
            this.dgvImport = new System.Windows.Forms.DataGridView();
            this.cbo = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.sjkl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.drl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sc = new System.Windows.Forms.DataGridViewLinkColumn();
            this.btnToSQL = new System.Windows.Forms.Button();
            this.qu = new System.Windows.Forms.Button();
            this.quall = new System.Windows.Forms.Button();
            this.buttonX2 = new System.Windows.Forms.Button();
            this.lai = new System.Windows.Forms.Button();
            this.laiall = new System.Windows.Forms.Button();
            this.buttonX1 = new System.Windows.Forms.Button();
            this.groupControl1 = new System.Windows.Forms.GroupBox();
            this.groupControl2 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvImport)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.groupControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // listold
            // 
            this.listold.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listold.FormattingEnabled = true;
            this.listold.ItemHeight = 12;
            this.listold.Location = new System.Drawing.Point(3, 17);
            this.listold.Name = "listold";
            this.listold.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listold.Size = new System.Drawing.Size(123, 182);
            this.listold.TabIndex = 1;
            this.listold.DoubleClick += new System.EventHandler(this.listold_DoubleClick);
            // 
            // listnew
            // 
            this.listnew.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listnew.FormattingEnabled = true;
            this.listnew.ItemHeight = 12;
            this.listnew.Location = new System.Drawing.Point(3, 17);
            this.listnew.Name = "listnew";
            this.listnew.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listnew.Size = new System.Drawing.Size(123, 184);
            this.listnew.TabIndex = 0;
            this.listnew.DoubleClick += new System.EventHandler(this.listnew_DoubleClick);
            // 
            // dgvImport
            // 
            this.dgvImport.AllowUserToDeleteRows = false;
            this.dgvImport.AllowUserToOrderColumns = true;
            this.dgvImport.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dgvImport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvImport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cbo,
            this.sjkl,
            this.drl,
            this.sc});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 9F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(53)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(53)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvImport.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvImport.Dock = System.Windows.Forms.DockStyle.Right;
            this.dgvImport.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvImport.Location = new System.Drawing.Point(219, 0);
            this.dgvImport.Name = "dgvImport";
            this.dgvImport.RowTemplate.Height = 23;
            this.dgvImport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvImport.Size = new System.Drawing.Size(407, 419);
            this.dgvImport.TabIndex = 8;
            this.dgvImport.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvImport_CellContentClick);
            // 
            // cbo
            // 
            this.cbo.HeaderText = "标识";
            this.cbo.Name = "cbo";
            this.cbo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cbo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.cbo.Width = 40;
            // 
            // sjkl
            // 
            this.sjkl.HeaderText = "数据列";
            this.sjkl.Name = "sjkl";
            this.sjkl.Width = 130;
            // 
            // drl
            // 
            this.drl.HeaderText = "导入列";
            this.drl.Name = "drl";
            this.drl.Width = 130;
            // 
            // sc
            // 
            this.sc.HeaderText = "删除";
            this.sc.Name = "sc";
            this.sc.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.sc.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.sc.Text = "删除";
            this.sc.UseColumnTextForLinkValue = true;
            // 
            // btnToSQL
            // 
            this.btnToSQL.Location = new System.Drawing.Point(141, 226);
            this.btnToSQL.Name = "btnToSQL";
            this.btnToSQL.Size = new System.Drawing.Size(59, 31);
            this.btnToSQL.TabIndex = 10;
            this.btnToSQL.Text = "生成SQL";
            this.btnToSQL.Click += new System.EventHandler(this.btnToSQL_Click);
            // 
            // qu
            // 
            this.qu.Location = new System.Drawing.Point(141, 61);
            this.qu.Name = "qu";
            this.qu.Size = new System.Drawing.Size(59, 31);
            this.qu.TabIndex = 10;
            this.qu.Text = ">";
            this.qu.Click += new System.EventHandler(this.qu_Click);
            // 
            // quall
            // 
            this.quall.Location = new System.Drawing.Point(141, 108);
            this.quall.Name = "quall";
            this.quall.Size = new System.Drawing.Size(59, 31);
            this.quall.TabIndex = 10;
            this.quall.Text = ">>";
            this.quall.Click += new System.EventHandler(this.quall_Click);
            // 
            // buttonX2
            // 
            this.buttonX2.Location = new System.Drawing.Point(141, 159);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Size = new System.Drawing.Size(59, 31);
            this.buttonX2.TabIndex = 10;
            this.buttonX2.Text = "重置";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // lai
            // 
            this.lai.Location = new System.Drawing.Point(141, 278);
            this.lai.Name = "lai";
            this.lai.Size = new System.Drawing.Size(59, 31);
            this.lai.TabIndex = 10;
            this.lai.Text = ">";
            this.lai.Click += new System.EventHandler(this.lai_Click);
            // 
            // laiall
            // 
            this.laiall.Location = new System.Drawing.Point(141, 319);
            this.laiall.Name = "laiall";
            this.laiall.Size = new System.Drawing.Size(59, 31);
            this.laiall.TabIndex = 10;
            this.laiall.Text = ">>";
            this.laiall.Click += new System.EventHandler(this.laiall_Click);
            // 
            // buttonX1
            // 
            this.buttonX1.Location = new System.Drawing.Point(141, 365);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Size = new System.Drawing.Size(59, 31);
            this.buttonX1.TabIndex = 10;
            this.buttonX1.Text = "保存";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.listnew);
            this.groupControl1.Location = new System.Drawing.Point(5, 207);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(129, 204);
            this.groupControl1.TabIndex = 11;
            this.groupControl1.TabStop = false;
            this.groupControl1.Text = "导入列";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.listold);
            this.groupControl2.Location = new System.Drawing.Point(5, 0);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(129, 202);
            this.groupControl2.TabIndex = 12;
            this.groupControl2.TabStop = false;
            this.groupControl2.Text = "数据库列";
            // 
            // FrmColumnDY
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(626, 419);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.btnToSQL);
            this.Controls.Add(this.dgvImport);
            this.Controls.Add(this.buttonX1);
            this.Controls.Add(this.buttonX2);
            this.Controls.Add(this.laiall);
            this.Controls.Add(this.lai);
            this.Controls.Add(this.quall);
            this.Controls.Add(this.qu);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmColumnDY";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "选择标识列";
            this.Load += new System.EventHandler(this.SetColumns_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvImport)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listold;
        private System.Windows.Forms.ListBox listnew;
        private System.Windows.Forms.DataGridView dgvImport;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cbo;
        private System.Windows.Forms.DataGridViewTextBoxColumn sjkl;
        private System.Windows.Forms.DataGridViewTextBoxColumn drl;
        private System.Windows.Forms.DataGridViewLinkColumn sc;
        private System.Windows.Forms.Button btnToSQL;
        private System.Windows.Forms.Button qu;
        private System.Windows.Forms.Button quall;
        private System.Windows.Forms.Button buttonX2;
        private System.Windows.Forms.Button lai;
        private System.Windows.Forms.Button laiall;
        private System.Windows.Forms.Button buttonX1;
        private System.Windows.Forms.GroupBox groupControl1;
        private System.Windows.Forms.GroupBox groupControl2;
    }
}