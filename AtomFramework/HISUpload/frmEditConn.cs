﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace HeadConfig
{
    public partial class frmEditConn : Form
    {
        public frmEditConn()
        {
            InitializeComponent();
        }

        private void btntest_Click(object sender, EventArgs e)
        {
            if (!Connection())
                MessageBox.Show("该配置不能正确连接,请检查！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
                MessageBox.Show("连接成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private bool Connection()
        {
            bool b = false;
            SqlConnection con = null;
            try
            {
                string connection = "Data Source=" + txtSystemIP.Text.Trim() + ";Initial Catalog=" + txtDBName.Text.Trim() + ";User ID=" + txtUserName.Text.Trim() + ";password=" + txtPassword.Text.Trim();
                con = new SqlConnection(connection);
                con.Open();
                b = true;
            }
            catch (Exception ex)
            {
                b = false;
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                }
            }

            return b;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!CheckIP())
            {
                MessageBox.Show("服务器IP不是正确的IP地址，请检查！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSystemIP.Focus();
                txtSystemIP.SelectAll();
                return;
            }
        }

        private bool CheckIP()
        {
            string pat = @"^(((\d{1})|(\d{2})|(1\d{2})|(2[0-4]\d{1})|(25[0-5]))\.){3}((\d{1})|(\d{2})|(1\d{2})|(2[0-4]\d{1})|(25[0-5]))$";
            Match m = Regex.Match(txtSystemIP.Text, pat);
            return m.Success;
        }

    }
}
