﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HeadConfig
{
    public class Message
    {
        public enum MessageType
        {
            Add,
            Edit,
            Clear
        }
    }
    public class plugin
    {
        private string _txt功能表 = "";

        public string Txt功能表
        {
            get { return _txt功能表; }
            set { _txt功能表 = value; }
        }
        private string _txt功能号 = "";

        public string Txt功能号
        {
            get { return _txt功能号; }
            set { _txt功能号 = value; }
        }
        private string _cbo操作类型 = "";

        public string Cbo操作类型
        {
            get { return _cbo操作类型; }
            set { _cbo操作类型 = value; }
        }
        private string _rtb查询语句 = "";

        public string Rtb查询语句
        {
            get { return _rtb查询语句; }
            set { _rtb查询语句 = value; }
        }
        private string _dgv数据列表 = "";

        public string Dgv数据列表
        {
            get { return _dgv数据列表; }
            set { _dgv数据列表 = value; }
        }

        private string _rtbXML;

        public string RtbXML
        {
            get { return _rtbXML; }
            set { _rtbXML = value; }
        }

    }
}
