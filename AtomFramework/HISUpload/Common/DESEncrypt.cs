﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
namespace HeadConfig
{
	/// <summary>
	/// DES加密/解密类。
	/// </summary>
	public class DESEncrypt
	{
		public DESEncrypt()
		{			
		}

		#region ========加密======== 
 
        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="Text"></param>
        /// <returns></returns>
		public static string Encrypt(string Text) 
		{
            return Encrypt(Text, "litianping");
		}
		/// <summary> 
		/// 加密数据 
		/// </summary> 
		/// <param name="Text"></param> 
		/// <param name="sKey"></param> 
		/// <returns></returns> 
		public static string Encrypt(string Text,string sKey) 
		{ 
			DESCryptoServiceProvider des = new DESCryptoServiceProvider(); 
			byte[] inputByteArray; 
			inputByteArray=Encoding.Default.GetBytes(Text); 
			des.Key = ASCIIEncoding.ASCII.GetBytes(System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(sKey, "md5").Substring(0, 8)); 
			des.IV = ASCIIEncoding.ASCII.GetBytes(System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(sKey, "md5").Substring(0, 8)); 
			System.IO.MemoryStream ms=new System.IO.MemoryStream(); 
			CryptoStream cs=new CryptoStream(ms,des.CreateEncryptor(),CryptoStreamMode.Write); 
			cs.Write(inputByteArray,0,inputByteArray.Length); 
			cs.FlushFinalBlock(); 
			StringBuilder ret=new StringBuilder(); 
			foreach( byte b in ms.ToArray()) 
			{ 
				ret.AppendFormat("{0:X2}",b); 
			} 
			return ret.ToString(); 
		} 

		#endregion
		
		#region ========解密======== 
   
 
        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="Text"></param>
        /// <returns></returns>
		public static string Decrypt(string Text) 
		{
            return Decrypt(Text, "litianping");
		}
		/// <summary> 
		/// 解密数据 
		/// </summary> 
		/// <param name="Text"></param> 
		/// <param name="sKey"></param> 
		/// <returns></returns> 
		public static string Decrypt(string Text,string sKey) 
		{ 
			DESCryptoServiceProvider des = new DESCryptoServiceProvider(); 
			int len; 
			len=Text.Length/2; 
			byte[] inputByteArray = new byte[len]; 
			int x,i; 
			for(x=0;x<len;x++) 
			{ 
				i = Convert.ToInt32(Text.Substring(x * 2, 2), 16); 
				inputByteArray[x]=(byte)i; 
			} 
			des.Key = ASCIIEncoding.ASCII.GetBytes(System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(sKey, "md5").Substring(0, 8)); 
			des.IV = ASCIIEncoding.ASCII.GetBytes(System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(sKey, "md5").Substring(0, 8)); 
			System.IO.MemoryStream ms=new System.IO.MemoryStream(); 
			CryptoStream cs=new CryptoStream(ms,des.CreateDecryptor(),CryptoStreamMode.Write); 
			cs.Write(inputByteArray,0,inputByteArray.Length); 
			cs.FlushFinalBlock(); 
			return Encoding.Default.GetString(ms.ToArray()); 
		} 
 
		#endregion 

        #region 个人档案解密
        /// <summary>
        /// 个人姓名解密
        /// </summary>
        /// <param name="s加密字符串"></param>
        /// <returns></returns>
        public static String DES解密(string s加密字符串)
        {
            try
            {
                if (s加密字符串.Length < 16) return s加密字符串;
                String jiemi = "";
                int len = s加密字符串.Length / 16;
                for (int i = 0; i < len; i++)
                {
                    jiemi += DESEncrypt.DESDeCode(s加密字符串.Substring(i * 16, 16));
                }
                return jiemi;
            }
            catch (Exception)
            {
                return s加密字符串;
            }
        }

        /// <summary>
        /// DES解密 -java des加密字符串
        /// </summary>
        /// <param name="str">加密字符串</param>
        /// <returns></returns>
        public static string DESDeCode(string pToDecrypt)
        {
            return DESDeCode(pToDecrypt, "zhonglianjiayu");
        }

        /// <summary>
        /// 解密java des加密字符串+1
        /// </summary>
        /// <param name="str">加密字符串</param>
        ///<param name="Keys">密钥</param>
        /// <returns></returns>
        public static string DESDeCode(string pToDecrypt, string Keys)
        {
            try
            {
                DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
                provider.Mode = CipherMode.ECB;//C#默认CBC模式，解密java需要设置ECB模式
                // 密钥
                provider.Key = Encoding.ASCII.GetBytes(Keys.Substring(0, 8));
                // 偏移量
                provider.IV = Encoding.ASCII.GetBytes(Keys.Substring(0, 8));
                byte[] buffer = hexStr2ByteArr(pToDecrypt);
                //byte[] buffer = new byte[pToDecrypt.Length / 2];
                //for (int i = 0; i < (pToDecrypt.Length / 2); i++)
                //{
                //    int num2 = Convert.ToInt32(pToDecrypt.Substring(i * 2, 2), 0x10);
                //    buffer[i] = (byte)num2;
                //}
                MemoryStream stream = new MemoryStream();
                CryptoStream stream2 = new CryptoStream(stream, provider.CreateDecryptor(), CryptoStreamMode.Write);
                stream2.Write(buffer, 0, buffer.Length);
                stream2.FlushFinalBlock();
                stream.Close();
                return Encoding.GetEncoding("GB2312").GetString(stream.ToArray());
            }
            catch (Exception) { return ""; }
        }

        /// <summary>
        ///将表示16进制值的字符串转换为byte数组 和public static String byteArr2HexStr(byte[] arrB)  * 互为可逆的转换过程 
        /// </summary>
        /// <param name="strIn"></param>
        /// <returns></returns>
        public static byte[] hexStr2ByteArr(String strIn)
        {
            // 两个字符表示一个字节，所以字节数组长度是字符串长度除以2   
            int iLen = strIn.Length / 2;
            byte[] arrOut = new byte[iLen];
            for (int i = 0; i < iLen; i++)
            {
                arrOut[i] = (byte)Convert.ToInt32(strIn.Substring(i * 2, 2), 16);
            }
            return arrOut;
        } 
        #endregion

	}
}
