﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HeadConfig
{
    public class State
    {
        public enum StateType
        {
            Add,
            Edit,
            Delete
        }
    }

    public class System_seting
    {
        private string _配置名称 = "";
        /// <summary>
        /// 配置名称（唯一性）
        /// </summary>
        public string 配置名称
        {
            set { _配置名称 = value; }
            get { return _配置名称; }
        }

        private string _接口类型 = "";
        /// <summary>
        /// 接口类型
        /// </summary>
        public string 接口类型
        {
            set { _接口类型 = value; }
            get { return _接口类型; }
        }

        private string _机构编号 = "";
        /// <summary>
        /// 机构编号
        /// </summary>
        public string 机构编号
        {
            set { _机构编号 = value; }
            get { return _机构编号; }
        }

        private string _SQLConn = "";
        /// <summary>
        /// SQL数据库链接
        /// </summary>
        public string SQLConn
        {
            set { _SQLConn = value; }
            get { return _SQLConn; }
        }

        private string _WebService = "";
        /// <summary>
        /// WebService
        /// </summary>
        public string WebService
        {
            set { _WebService = value; }
            get { return _WebService; }
        }

        private string _ORCLConn = "";
        /// <summary>
        /// oracle 数据库链接
        /// </summary>
        public string ORCLConn
        {
            set { _ORCLConn = value; }
            get { return _ORCLConn; }
        }

        public string CheckAuto { get; set; }

    }

    /// <summary>
    /// 公卫上传
    /// </summary>
    public class UploadFile
    {
        public string s_个人档案编号 { get; set; }

        public string  s_xml { get; set; }

        public string  s_returnxml { get; set; }

        public string  s_state { get; set; }

        public string s_功能号 { get; set; }
    }

    /// <summary>
    /// HIS上传
    /// </summary>
    public class HISUploadFile
    {
        public string s_功能表 { get; set; }

        public string s_功能号 { get; set; }

        public string s_操作类型 { get; set; }

        public string s_查询语句 { get; set; }

        public string s_字段对应 { get; set; }

        public string s_批量上传 { get; set; }
    }

    class system
    {
        private string _systemname = "";
        /// <summary>
        /// 配置名称（唯一性）
        /// </summary>
        public string SystemName
        {
            set { _systemname = value; }
            get { return _systemname; }
        }

        private string _systemIP;
        /// <summary>
        /// 服务器IP
        /// </summary>
        public string SystemIp
        {
            set { _systemIP = value; }
            get { return _systemIP; }
        }

        private string _DBName = "";
        /// <summary>
        /// 数据库名称
        /// </summary>
        public string DBName
        {
            set { _DBName = value; }
            get { return _DBName; }
        }

        private string _UserName;
        /// <summary>
        /// 登录用户
        /// </summary>
        public string UserName
        {
            set { _UserName = value; }
            get { return _UserName; }
        }

        private string _Password;
        /// <summary>
        /// 登录密码
        /// </summary>
        public string PassWord
        {
            get { return _Password; }
            set { _Password = value; }
        }
    }
}
