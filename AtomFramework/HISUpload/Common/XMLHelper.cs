﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
//using System.Linq;
using System.Text;
using System.Xml;

namespace HeadConfig
{
    public class XMLHelper
    {

        #region 公卫
        /// <summary>
        /// 拼接公卫xml
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ConvertDataTableToEHRXml(DataRow dr, string FuncCode, string OrgCantonNo, string OperateType)
        {
            StringBuilder strXml = new StringBuilder();
            strXml.AppendLine("<?xml version='1.0' encoding='GB2312'?> ");
            strXml.AppendLine("<Params> ");
            //<!-- InterfaceMode：接口模式 --> 
            strXml.AppendLine("<InterfaceMode>IWS_JKDA</InterfaceMode>  ");
            //<!-- FuncCode：功能号 --> 
            strXml.AppendLine("<FuncCode>" + FuncCode + "</FuncCode> ");
            //<!-- OrgCantonNo：机构行政编号 -->
            strXml.AppendLine("<OrgCantonNo>" + OrgCantonNo + "</OrgCantonNo> ");
            //<!-- OperateType：操作类型（1：新增、修改 2：删除） -->   
            strXml.AppendLine("<OperateType>" + OperateType + "</OperateType>");
            //<!-- DataList：数据列表 -->  
            strXml.AppendLine("<DataList>");

            Int32 ItemNo = 0;
            foreach (var s in dr.ItemArray)
            {
                ++ItemNo;
                string sItemNo = "D" + string.Format("{0:000}", ItemNo);
                strXml.AppendLine("<" + sItemNo + ">" + s.ToString().Trim() + "</" + sItemNo + ">");
                //if (!string.IsNullOrEmpty(strName))
                //{
                //    if (sItemNo == "D002")//公共卫生基本档案表、新生儿访视随访记录表、冠心病患者信息表
                //    // if (sItemNo == "D003")//妇女病普查管理、产后访视记录表
                //    //if (sItemNo == "D004")//健康体检表、糖尿病病例管理卡 、高血压病例管理卡、产后42天检查记录表
                //    {
                //        strXml.AppendLine("<" + sItemNo + ">" + strName + "</" + sItemNo + ">");
                //    }
                //    else
                //    {
                //        strXml.AppendLine("<" + sItemNo + ">" + s.ToString().Trim() + "</" + sItemNo + ">");
                //    }

                //}
                //else
                //{
                //    strXml.AppendLine("<" + sItemNo + ">" + s.ToString().Trim() + "</" + sItemNo + ">");
                //}

            }
            strXml.AppendLine("</DataList>");
            strXml.AppendLine("</Params>");

            return strXml.ToString();
        }

        /// <summary>
        /// 解析xml返回字段
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public DataTable ConvertXmlToEHRData(string xml)
        {
            DataSet ds = ConvertXmlToDataTable(xml);
            if (ds == null || ds.Tables.Count <= 0)
                return null;
            DataTable dt = ds.Tables[0];
            return dt;
        }

        #endregion

        #region 电子病历转换
        /// <summary>
        /// 电子病历xml格式转换
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public string ConvertDataTableToEmrXml(DataTable dt)
        {
            StringBuilder strXml = new StringBuilder();
            strXml.AppendLine("<?xml version='1.0' encoding='utf-8' ?>");
            strXml.AppendLine("<GROUP>");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                strXml.AppendLine("<ITEM ");
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    strXml.AppendLine(dt.Columns[j].ColumnName + "='" + dt.Rows[i][j] + "' ");
                }
                strXml.AppendLine("/>");
            }
            strXml.AppendLine("</GROUP>");

            return strXml.ToString();
        }

        /// <summary>
        /// 解析xml返回字段
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public DataTable ConvertXmlToEmrData(string xml)
        {
            DataSet ds = ConvertXmlToDataTable(xml);
            if (ds == null || ds.Tables.Count <= 0)
                return null;
            DataTable dt = ds.Tables[0];
            return dt;
            //if (dt == null || dt.Rows.Count <= 0)
            //    return "";
            //return dt.Rows[0][0].ToString();
        }

        public string Requestxml(string xml)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(xml);
            return xmldoc.SelectSingleNode("/emrtextdoc/body").InnerXml;
        }

        #endregion

        #region xmlconvert
        /// <summary>
        /// 标准对称
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public string ConvertDataTableToXml(DataTable dt)
        {
            StringBuilder strXml = new StringBuilder();
            strXml.AppendLine("<GROUP>");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                strXml.AppendLine("<ITEM>");
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    strXml.AppendLine("<" + dt.Columns[j].ColumnName + ">" + dt.Rows[i][j] + "</" + dt.Columns[j].ColumnName + ">");
                }
                strXml.AppendLine("</ITEM>");
            }
            strXml.AppendLine("</GROUP>");

            return strXml.ToString();
        }

        private DataSet ConvertXmlToDataTable(string xmlData)
        {
            StringReader stream = null;
            XmlTextReader reader = null;
            try
            {
                DataSet xmlDS = new DataSet();
                stream = new StringReader(xmlData);
                reader = new XmlTextReader(stream);
                xmlDS.ReadXml(reader);

                return xmlDS;
            }
            catch (Exception ex)
            {
                string strTest = ex.Message;
                return null;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
        }
        #endregion

        /// <summary>  
        /// 读取xml中的指定节点的值
        /// </summary>  
        public static string ReadXmlNode(string XML)
        {
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.LoadXml(XML);
                //读取Activity节点下的数据。SelectSingleNode匹配第一个Activity节点
                XmlNode root = xmlDoc.SelectSingleNode("//OutResult");//当节点Workflow带有属性是，使用SelectSingleNode无法读取        
                if (root != null)
                {
                    string ActivityId = (root.SelectSingleNode("ErrCode")).InnerText;
                    return ActivityId;
                }
                else
                {
                    Console.WriteLine("the node  is not existed");
                    //Console.Read();
                    return "the node  is not existed";
                }
            }
            catch (Exception e)
            {
                //显示错误信息
                Console.WriteLine(e.Message);
                return e.Message;
            }
        }

        //public void GetXml()
        //{
        //    using (SqlConnection conn = new SqlConnection("Server=192.168.10.121;DataBase=AtomEHR.YSDB;Uid=yggsuser;Pwd=yggsuser;"))
        //    {
        //        conn.Open();
        //        SqlCommand command = new SqlCommand(rtb查询语句.Text, conn);
        //        command.CommandType = CommandType.Text;
        //        DataSet ds = new DataSet("Params");
        //        //DATASET将成为XML文件中的根节点名称，否则系统将其命名为NewDataSet      
        //        SqlDataAdapter sda = new SqlDataAdapter();
        //        sda.SelectCommand = command;
        //        sda.Fill(ds, "DataList");
        //        //DATATABLE为所生成XML文件中的子节点名称，否则系统将其命名为Table。   
        //        ds.WriteXml("SqlXml.xml");
        //    }
        //} 

    }
}      
