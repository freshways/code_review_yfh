﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace HeadConfig.Common
{
    public class Msg
    {
        public static void Asterisk(string message)
        {
            MessageBox.Show(message, "消息", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        public static void Error(string message)
        {
            MessageBox.Show(message, "消息", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void Exclamation(string message)
        {
            MessageBox.Show(message, "消息", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        public static void Hand(string message)
        {
            MessageBox.Show(message, "消息", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }

        public static void Information(string message)
        {
            MessageBox.Show(message, "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void None(string message)
        {
            MessageBox.Show(message, "消息", MessageBoxButtons.OK, MessageBoxIcon.None);
        }

        public static void Question(string message)
        {
            MessageBox.Show(message, "消息", MessageBoxButtons.OK, MessageBoxIcon.Question);
        }

        public static void Stop(string message)
        {
            MessageBox.Show(message, "消息", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }

        public static void Warning(string message)
        {
            MessageBox.Show(message, "消息", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
    }
}
