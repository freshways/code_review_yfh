﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Sql;
using System.Data;
using System.Collections;
using System.Windows.Forms;

namespace Maticsoft.Common
{
    public class Servers
    {
        //存储数据库
        public static string GlobalSql {get;set;}

        private static Hashtable old_db = new Hashtable();

        public static Hashtable Old_db
        {
            get { return Servers.old_db; }
            set { Servers.old_db = value; }
        }

        private static Hashtable new_db = new Hashtable();

        public static Hashtable New_db
        {
            get { return Servers.new_db; }
            set { Servers.new_db = value; }
        }

        /// <summary>
        /// 临时写的-字段对应组合哈希表
        /// </summary>
        /// <param name="ss"></param>
        public static void ToHash(string ss)
        {
            try
            {
                Servers.Old_db.Clear();
                Servers.New_db.Clear();
                string[] newss = ss.Split('&');
                if (newss.Length < 2) return;
                int keyint = 0;
                string[] sjkl = newss[0].Split(',');
                string[] drl = newss[1].Split(',');
                foreach (string c in sjkl)
                {
                    keyint++;
                    if (!string.IsNullOrEmpty(c))
                    {
                        string s = "false";
                        Hashtable hb1 = new Hashtable();
                        hb1.Add(c, s);
                        Servers.Old_db.Add(keyint, hb1);
                    }
                }
                keyint = 0;
                foreach (string c in drl)
                {
                    keyint++;
                    if (!string.IsNullOrEmpty(c))
                    {
                        string s = "false";
                        Hashtable hb2 = new Hashtable();
                        if (c.Contains("|") && c.Split('|').Length > 1)
                        {
                            if (c.Split('|')[1].Equals("true") || c.Split('|')[1].Equals("0")) s = "true";

                            hb2.Add(c.Split('|')[0].ToString(), s);
                        }
                        else
                            hb2.Add(c, s);
                        Servers.New_db.Add(keyint, hb2);
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// 获取服务器
        /// </summary>
        /// <returns></returns>
        public static DataTable GetServers()
        {
            try
            {
                SqlDataSourceEnumerator instance = SqlDataSourceEnumerator.Instance;
                DataTable dt = instance.GetDataSources();
                while(dt.Rows.Count <=0 )
                {
                    if(MessageBox.Show("网路繁忙是否重试","提示",MessageBoxButtons.YesNo).Equals(DialogResult.No)) break;
                    dt = instance.GetDataSources();
                }
                return dt;
            }
            catch { return null; }
        }

    }
}
 