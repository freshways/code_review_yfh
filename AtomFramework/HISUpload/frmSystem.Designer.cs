﻿namespace HeadConfig
{
    partial class frmSystem
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("(尚无连接配置...)");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSystem));
            this.scMain = new System.Windows.Forms.SplitContainer();
            this.tvMain = new System.Windows.Forms.TreeView();
            this.ilEnterpriseExplore = new System.Windows.Forms.ImageList(this.components);
            this.panToolBar = new System.Windows.Forms.Panel();
            this.pbClose = new System.Windows.Forms.PictureBox();
            this.lblToolBar = new System.Windows.Forms.Label();
            this.panelMain = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.checkAuto = new System.Windows.Forms.CheckBox();
            this.btnEnd = new System.Windows.Forms.Button();
            this.btnBegin = new System.Windows.Forms.Button();
            this.btnPlugIn = new System.Windows.Forms.Button();
            this.btnGetConn2 = new System.Windows.Forms.Button();
            this.btnGetConn = new System.Windows.Forms.Button();
            this.btntest = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txt配置名称 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txt接口类型 = new System.Windows.Forms.TextBox();
            this.txtORCL = new System.Windows.Forms.TextBox();
            this.txtSQLConn = new System.Windows.Forms.TextBox();
            this.txt机构编号 = new System.Windows.Forms.TextBox();
            this.txtWebService = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.returnXml = new System.Windows.Forms.RichTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rtbXML = new System.Windows.Forms.RichTextBox();
            this.tlbMain = new System.Windows.Forms.ToolStrip();
            this.tlbMain_show = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tlbMain_New = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tlbMain_Modify = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tlbMain_Delete = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.scMain)).BeginInit();
            this.scMain.Panel1.SuspendLayout();
            this.scMain.Panel2.SuspendLayout();
            this.scMain.SuspendLayout();
            this.panToolBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbClose)).BeginInit();
            this.panelMain.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tlbMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // scMain
            // 
            this.scMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scMain.Location = new System.Drawing.Point(0, 0);
            this.scMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.scMain.Name = "scMain";
            // 
            // scMain.Panel1
            // 
            this.scMain.Panel1.Controls.Add(this.tvMain);
            this.scMain.Panel1.Controls.Add(this.panToolBar);
            // 
            // scMain.Panel2
            // 
            this.scMain.Panel2.Controls.Add(this.panelMain);
            this.scMain.Panel2.Controls.Add(this.tlbMain);
            this.scMain.Size = new System.Drawing.Size(969, 662);
            this.scMain.SplitterDistance = 302;
            this.scMain.SplitterWidth = 6;
            this.scMain.TabIndex = 0;
            // 
            // tvMain
            // 
            this.tvMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvMain.HideSelection = false;
            this.tvMain.ImageIndex = 0;
            this.tvMain.ImageList = this.ilEnterpriseExplore;
            this.tvMain.Location = new System.Drawing.Point(0, 30);
            this.tvMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tvMain.Name = "tvMain";
            treeNode1.Name = "RootNode";
            treeNode1.Text = "(尚无连接配置...)";
            this.tvMain.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1});
            this.tvMain.SelectedImageIndex = 1;
            this.tvMain.Size = new System.Drawing.Size(302, 632);
            this.tvMain.TabIndex = 2;
            this.tvMain.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvMain_AfterSelect);
            this.tvMain.Enter += new System.EventHandler(this.tvMain_Enter);
            this.tvMain.Leave += new System.EventHandler(this.tvMain_Leave);
            // 
            // ilEnterpriseExplore
            // 
            this.ilEnterpriseExplore.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilEnterpriseExplore.ImageStream")));
            this.ilEnterpriseExplore.TransparentColor = System.Drawing.Color.Transparent;
            this.ilEnterpriseExplore.Images.SetKeyName(0, "服务器.ico");
            this.ilEnterpriseExplore.Images.SetKeyName(1, "组织结构.ico");
            this.ilEnterpriseExplore.Images.SetKeyName(2, "软件系统.ico");
            this.ilEnterpriseExplore.Images.SetKeyName(3, "角色.ico");
            this.ilEnterpriseExplore.Images.SetKeyName(4, "部门.ico");
            this.ilEnterpriseExplore.Images.SetKeyName(5, "岗位.ico");
            this.ilEnterpriseExplore.Images.SetKeyName(6, "模块.ico");
            this.ilEnterpriseExplore.Images.SetKeyName(7, "功能点.ico");
            this.ilEnterpriseExplore.Images.SetKeyName(8, "16close.gif");
            this.ilEnterpriseExplore.Images.SetKeyName(9, "用户.ico");
            // 
            // panToolBar
            // 
            this.panToolBar.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panToolBar.Controls.Add(this.pbClose);
            this.panToolBar.Controls.Add(this.lblToolBar);
            this.panToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.panToolBar.Location = new System.Drawing.Point(0, 0);
            this.panToolBar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panToolBar.Name = "panToolBar";
            this.panToolBar.Size = new System.Drawing.Size(302, 30);
            this.panToolBar.TabIndex = 1;
            // 
            // pbClose
            // 
            this.pbClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.pbClose.ErrorImage = global::HeadConfig.Properties.Resources.close;
            this.pbClose.Image = global::HeadConfig.Properties.Resources.close;
            this.pbClose.Location = new System.Drawing.Point(278, 0);
            this.pbClose.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbClose.Name = "pbClose";
            this.pbClose.Size = new System.Drawing.Size(24, 30);
            this.pbClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbClose.TabIndex = 1;
            this.pbClose.TabStop = false;
            this.pbClose.Click += new System.EventHandler(this.pbClose_Click);
            this.pbClose.MouseEnter += new System.EventHandler(this.pbClose_MouseEnter);
            this.pbClose.MouseLeave += new System.EventHandler(this.pbClose_MouseLeave);
            // 
            // lblToolBar
            // 
            this.lblToolBar.AutoSize = true;
            this.lblToolBar.Location = new System.Drawing.Point(4, 7);
            this.lblToolBar.Margin = new System.Windows.Forms.Padding(0);
            this.lblToolBar.Name = "lblToolBar";
            this.lblToolBar.Size = new System.Drawing.Size(71, 15);
            this.lblToolBar.TabIndex = 0;
            this.lblToolBar.Text = "连接配置";
            // 
            // panelMain
            // 
            this.panelMain.Controls.Add(this.tabControl1);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(0, 25);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(661, 637);
            this.panelMain.TabIndex = 3;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(661, 637);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(653, 609);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "配置信息";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.dateTimePicker2);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Controls.Add(this.checkAuto);
            this.panel1.Controls.Add(this.btnEnd);
            this.panel1.Controls.Add(this.btnBegin);
            this.panel1.Controls.Add(this.btnPlugIn);
            this.panel1.Controls.Add(this.btnGetConn2);
            this.panel1.Controls.Add(this.btnGetConn);
            this.panel1.Controls.Add(this.btntest);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.txt配置名称);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txt接口类型);
            this.panel1.Controls.Add(this.txtORCL);
            this.panel1.Controls.Add(this.txtSQLConn);
            this.panel1.Controls.Add(this.txt机构编号);
            this.panel1.Controls.Add(this.txtWebService);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(647, 603);
            this.panel1.TabIndex = 2;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(277, 552);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(67, 24);
            this.textBox1.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(196, 557);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 15);
            this.label9.TabIndex = 16;
            this.label9.Text = "间隔：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(367, 556);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 15);
            this.label8.TabIndex = 16;
            this.label8.Text = "分钟";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(257, 529);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(15, 15);
            this.label7.TabIndex = 16;
            this.label7.Text = "~";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.CustomFormat = "";
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePicker2.Location = new System.Drawing.Point(277, 522);
            this.dateTimePicker2.MaxDate = new System.DateTime(2030, 12, 31, 0, 0, 0, 0);
            this.dateTimePicker2.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.ShowCheckBox = true;
            this.dateTimePicker2.ShowUpDown = true;
            this.dateTimePicker2.Size = new System.Drawing.Size(130, 24);
            this.dateTimePicker2.TabIndex = 15;
            this.dateTimePicker2.Value = new System.DateTime(2018, 10, 12, 1, 26, 0, 0);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePicker1.Location = new System.Drawing.Point(121, 522);
            this.dateTimePicker1.MaxDate = new System.DateTime(2030, 12, 31, 0, 0, 0, 0);
            this.dateTimePicker1.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.ShowCheckBox = true;
            this.dateTimePicker1.ShowUpDown = true;
            this.dateTimePicker1.Size = new System.Drawing.Size(130, 24);
            this.dateTimePicker1.TabIndex = 15;
            this.dateTimePicker1.Value = new System.DateTime(2018, 10, 12, 1, 13, 0, 0);
            // 
            // checkAuto
            // 
            this.checkAuto.AutoSize = true;
            this.checkAuto.Location = new System.Drawing.Point(437, 525);
            this.checkAuto.Name = "checkAuto";
            this.checkAuto.Size = new System.Drawing.Size(90, 19);
            this.checkAuto.TabIndex = 14;
            this.checkAuto.Text = "自动上传";
            this.checkAuto.UseVisualStyleBackColor = true;
            // 
            // btnEnd
            // 
            this.btnEnd.Location = new System.Drawing.Point(451, 470);
            this.btnEnd.Name = "btnEnd";
            this.btnEnd.Size = new System.Drawing.Size(75, 23);
            this.btnEnd.TabIndex = 13;
            this.btnEnd.Text = "停止";
            this.btnEnd.UseVisualStyleBackColor = true;
            this.btnEnd.Click += new System.EventHandler(this.btnEnd_Click);
            // 
            // btnBegin
            // 
            this.btnBegin.Location = new System.Drawing.Point(451, 430);
            this.btnBegin.Name = "btnBegin";
            this.btnBegin.Size = new System.Drawing.Size(75, 23);
            this.btnBegin.TabIndex = 13;
            this.btnBegin.Text = "开始";
            this.btnBegin.UseVisualStyleBackColor = true;
            this.btnBegin.Click += new System.EventHandler(this.btnBegin_Click);
            // 
            // btnPlugIn
            // 
            this.btnPlugIn.Location = new System.Drawing.Point(166, 430);
            this.btnPlugIn.Name = "btnPlugIn";
            this.btnPlugIn.Size = new System.Drawing.Size(232, 63);
            this.btnPlugIn.TabIndex = 12;
            this.btnPlugIn.Text = "PlugIn调试工具";
            this.btnPlugIn.UseVisualStyleBackColor = true;
            this.btnPlugIn.Click += new System.EventHandler(this.btnPlugIn_Click);
            // 
            // btnGetConn2
            // 
            this.btnGetConn2.Location = new System.Drawing.Point(534, 341);
            this.btnGetConn2.Name = "btnGetConn2";
            this.btnGetConn2.Size = new System.Drawing.Size(28, 25);
            this.btnGetConn2.TabIndex = 11;
            this.btnGetConn2.Text = "+";
            this.btnGetConn2.UseVisualStyleBackColor = true;
            this.btnGetConn2.Click += new System.EventHandler(this.btnGetConn2_Click);
            // 
            // btnGetConn
            // 
            this.btnGetConn.Location = new System.Drawing.Point(534, 189);
            this.btnGetConn.Name = "btnGetConn";
            this.btnGetConn.Size = new System.Drawing.Size(28, 25);
            this.btnGetConn.TabIndex = 11;
            this.btnGetConn.Text = "+";
            this.btnGetConn.UseVisualStyleBackColor = true;
            this.btnGetConn.Click += new System.EventHandler(this.btnGetConn_Click);
            // 
            // btntest
            // 
            this.btntest.Location = new System.Drawing.Point(416, 110);
            this.btntest.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btntest.Name = "btntest";
            this.btntest.Size = new System.Drawing.Size(111, 28);
            this.btntest.TabIndex = 10;
            this.btntest.Text = "连接测试";
            this.btntest.UseVisualStyleBackColor = true;
            this.btntest.Click += new System.EventHandler(this.btntest_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(415, 44);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(112, 28);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txt配置名称
            // 
            this.txt配置名称.Location = new System.Drawing.Point(166, 46);
            this.txt配置名称.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txt配置名称.MaxLength = 20;
            this.txt配置名称.Name = "txt配置名称";
            this.txt配置名称.Size = new System.Drawing.Size(241, 24);
            this.txt配置名称.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(59, 49);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "配置名称";
            // 
            // txt接口类型
            // 
            this.txt接口类型.Location = new System.Drawing.Point(166, 80);
            this.txt接口类型.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txt接口类型.MaxLength = 20;
            this.txt接口类型.Name = "txt接口类型";
            this.txt接口类型.Size = new System.Drawing.Size(241, 24);
            this.txt接口类型.TabIndex = 5;
            // 
            // txtORCL
            // 
            this.txtORCL.Location = new System.Drawing.Point(165, 338);
            this.txtORCL.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtORCL.MaxLength = 5000;
            this.txtORCL.Multiline = true;
            this.txtORCL.Name = "txtORCL";
            this.txtORCL.Size = new System.Drawing.Size(361, 75);
            this.txtORCL.TabIndex = 2;
            // 
            // txtSQLConn
            // 
            this.txtSQLConn.Location = new System.Drawing.Point(165, 151);
            this.txtSQLConn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSQLConn.MaxLength = 5000;
            this.txtSQLConn.Multiline = true;
            this.txtSQLConn.Name = "txtSQLConn";
            this.txtSQLConn.Size = new System.Drawing.Size(361, 82);
            this.txtSQLConn.TabIndex = 2;
            // 
            // txt机构编号
            // 
            this.txt机构编号.Location = new System.Drawing.Point(166, 114);
            this.txt机构编号.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txt机构编号.MaxLength = 20;
            this.txt机构编号.Name = "txt机构编号";
            this.txt机构编号.Size = new System.Drawing.Size(241, 24);
            this.txt机构编号.TabIndex = 4;
            // 
            // txtWebService
            // 
            this.txtWebService.Location = new System.Drawing.Point(165, 245);
            this.txtWebService.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtWebService.MaxLength = 3000;
            this.txtWebService.Multiline = true;
            this.txtWebService.Name = "txtWebService";
            this.txtWebService.Size = new System.Drawing.Size(361, 82);
            this.txtWebService.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(43, 268);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Web服务地址";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(59, 83);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "接口类型";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(59, 346);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 15);
            this.label6.TabIndex = 0;
            this.label6.Text = "ORCL链接";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(59, 117);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "机构编号";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 194);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "数据库链接";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(653, 609);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "日志";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.returnXml);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 348);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(647, 258);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "返回信息";
            // 
            // returnXml
            // 
            this.returnXml.Dock = System.Windows.Forms.DockStyle.Fill;
            this.returnXml.Location = new System.Drawing.Point(4, 21);
            this.returnXml.Margin = new System.Windows.Forms.Padding(4);
            this.returnXml.Name = "returnXml";
            this.returnXml.Size = new System.Drawing.Size(639, 233);
            this.returnXml.TabIndex = 0;
            this.returnXml.Text = "";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rtbXML);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(647, 345);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "上传信息";
            // 
            // rtbXML
            // 
            this.rtbXML.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbXML.Location = new System.Drawing.Point(4, 21);
            this.rtbXML.Margin = new System.Windows.Forms.Padding(4);
            this.rtbXML.Name = "rtbXML";
            this.rtbXML.Size = new System.Drawing.Size(639, 320);
            this.rtbXML.TabIndex = 0;
            this.rtbXML.Text = "";
            // 
            // tlbMain
            // 
            this.tlbMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(247)))), ((int)(((byte)(253)))));
            this.tlbMain.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.tlbMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tlbMain_show,
            this.toolStripSeparator2,
            this.tlbMain_New,
            this.toolStripSeparator3,
            this.tlbMain_Modify,
            this.toolStripSeparator1,
            this.tlbMain_Delete});
            this.tlbMain.Location = new System.Drawing.Point(0, 0);
            this.tlbMain.Name = "tlbMain";
            this.tlbMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.tlbMain.Size = new System.Drawing.Size(661, 25);
            this.tlbMain.TabIndex = 1;
            this.tlbMain.Text = "toolStrip1";
            // 
            // tlbMain_show
            // 
            this.tlbMain_show.Checked = true;
            this.tlbMain_show.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tlbMain_show.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tlbMain_show.Name = "tlbMain_show";
            this.tlbMain_show.Size = new System.Drawing.Size(60, 22);
            this.tlbMain_show.Text = "连接配置";
            this.tlbMain_show.ToolTipText = "是否显示左侧组织结构";
            this.tlbMain_show.Click += new System.EventHandler(this.tlbMain_show_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tlbMain_New
            // 
            this.tlbMain_New.Checked = true;
            this.tlbMain_New.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tlbMain_New.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tlbMain_New.Name = "tlbMain_New";
            this.tlbMain_New.Size = new System.Drawing.Size(48, 22);
            this.tlbMain_New.Text = "新配置";
            this.tlbMain_New.ToolTipText = "添加一个新配置";
            this.tlbMain_New.Click += new System.EventHandler(this.tlbMain_New_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // tlbMain_Modify
            // 
            this.tlbMain_Modify.Checked = true;
            this.tlbMain_Modify.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tlbMain_Modify.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tlbMain_Modify.Name = "tlbMain_Modify";
            this.tlbMain_Modify.Size = new System.Drawing.Size(36, 22);
            this.tlbMain_Modify.Text = "修改";
            this.tlbMain_Modify.ToolTipText = "修改连接配置";
            this.tlbMain_Modify.Click += new System.EventHandler(this.tlbMain_Modify_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tlbMain_Delete
            // 
            this.tlbMain_Delete.Checked = true;
            this.tlbMain_Delete.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tlbMain_Delete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tlbMain_Delete.Name = "tlbMain_Delete";
            this.tlbMain_Delete.Size = new System.Drawing.Size(36, 22);
            this.tlbMain_Delete.Text = "删除";
            this.tlbMain_Delete.ToolTipText = "删除指定配置";
            this.tlbMain_Delete.Click += new System.EventHandler(this.tlbMain_Delete_Click);
            // 
            // frmSystem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(247)))), ((int)(((byte)(253)))));
            this.ClientSize = new System.Drawing.Size(969, 662);
            this.Controls.Add(this.scMain);
            this.Font = new System.Drawing.Font("楷体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "frmSystem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "数据库系统配置";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSystem_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.scMain.Panel1.ResumeLayout(false);
            this.scMain.Panel2.ResumeLayout(false);
            this.scMain.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scMain)).EndInit();
            this.scMain.ResumeLayout(false);
            this.panToolBar.ResumeLayout(false);
            this.panToolBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbClose)).EndInit();
            this.panelMain.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tlbMain.ResumeLayout(false);
            this.tlbMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer scMain;
        private System.Windows.Forms.Panel panToolBar;
        private System.Windows.Forms.PictureBox pbClose;
        private System.Windows.Forms.Label lblToolBar;
        private System.Windows.Forms.TreeView tvMain;
        private System.Windows.Forms.ImageList ilEnterpriseExplore;
        private System.Windows.Forms.ToolStrip tlbMain;
        private System.Windows.Forms.ToolStripButton tlbMain_New;
        private System.Windows.Forms.ToolStripButton tlbMain_Delete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tlbMain_show;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tlbMain_Modify;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txt接口类型;
        private System.Windows.Forms.TextBox txtSQLConn;
        private System.Windows.Forms.TextBox txt机构编号;
        private System.Windows.Forms.TextBox txtWebService;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt配置名称;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btntest;
        private System.Windows.Forms.Button btnGetConn;
        private System.Windows.Forms.Button btnPlugIn;
        private System.Windows.Forms.TextBox txtORCL;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnGetConn2;
        private System.Windows.Forms.Button btnBegin;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox rtbXML;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RichTextBox returnXml;
        private System.Windows.Forms.Button btnEnd;
        private System.Windows.Forms.CheckBox checkAuto;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}

