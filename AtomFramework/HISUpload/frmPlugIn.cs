﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml;
using System.IO;
//using AtomEHR.公共卫生.util;
using HeadConfig.Common;
using Maticsoft.Common;
using System.Collections;

namespace HeadConfig
{
    public partial class frmPlugIn : Form
    {
        private string OrgCantonNo; //机构编号
        private string strName;//姓名
        private string FuncCode;
        private string OperateType;
        private System_seting settings = new System_seting();
        public frmPlugIn()
        {
            InitializeComponent();
        }

        public frmPlugIn(System_seting set)
        {
            InitializeComponent();
            settings = set;
            this.OrgCantonNo = set.机构编号;
            path = Application.StartupPath + "\\PlugInSetings" + set.配置名称 + ".xml";
            SqlHelper.connString = set.SQLConn;
            if (!string.IsNullOrEmpty(set.ORCLConn))
            { 
                
            }
        }

        public frmPlugIn(string txt接口类型, string txt机构编号)
        {
            InitializeComponent();
            this.OrgCantonNo = txt机构编号;
        }
        private string path = Application.StartupPath + "\\PlugInSetings.xml";
        private State.StateType StateType;

        /// <summary>
        /// 初始化控件（清空控件内容）
        /// </summary>
        private void ClearText()
        {
            txt功能号.Text = "";
            txt功能表.Text = "";
            cbo操作类型.Text = "";
            rtb查询语句.Text = "";
            txt字段对应.Text = "";
        }

        /// <summary>
        /// 检查保存
        /// </summary>
        /// <param name="b"></param>
        private void setEnable(bool b)
        {
           txt功能号.Enabled = b;
           txt功能表.Enabled = b;
           cbo操作类型.Enabled = b;
           rtb查询语句.Enabled = b;
            if (!b)
            {
                tsb保存.Visible = b;
            }
        }
        
        /// <summary>
        /// 检查功能表是否存在
        /// </summary>
        /// <param name="value"></param>
        private bool Check(string value)
        {
            bool b = false;
            if (tv节点.Nodes[0].Nodes.Count >0)
            {
                foreach (TreeNode node in tv节点.Nodes[0].Nodes)
                {
                    if (node.Text == value)
                    {
                        b = true;
                        break;
                    }
                }
            }
            return b;
        }

        private void CreateXml(string path)
        {
            StringBuilder xml = new StringBuilder();
            xml.Append("<?xml version='1.0' encoding='GB2312'?> ");
            xml.Append("<Configs>");
            xml.Append("</Configs>");

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml.ToString());
            doc.Save(path);
        }

        /// <summary>
        /// 判断是否为空
        /// </summary>
        /// <returns></returns>
        private bool isNullorEmpty()
        {
            return string.IsNullOrEmpty(txt功能表.Text.Trim()) || string.IsNullOrEmpty(txt功能号.Text.Trim())
                || string.IsNullOrEmpty(cbo操作类型.Text.Trim()) || string.IsNullOrEmpty(rtb查询语句.Text.Trim());
        }

        #region treeView事件

        /// <summary>
        /// 配置列表选定后加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tv节点_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node != tv节点.Nodes[0])
            {
                DataRow row = e.Node.Tag as DataRow;
                if (row != null)
                {
                    txt功能表.Text = row["SetingName"].ToString();
                    txt功能号.Text = row["FuncCode"].ToString();//FuncCode
                    cbo操作类型.Text = row["OperateType"].ToString();//OperateType
                    rtb查询语句.Text = code.DecryptStr(row["SQLConn"].ToString());
                    if (row.Table.Columns.Contains("SQLcolumn"))
                    {
                        txt字段对应.Text = row["SQLcolumn"].ToString();
                    }
                    if (row.Table.Columns.Contains("CheckAllin"))
                    {
                        chk批量导入.Checked = row["CheckAllin"].ToString() == "1" ? true : false;
                        //sys_seting.CheckAuto = row["CheckAllin"].ToString();
                    }

                }
            } 
        }

        private void tv节点_DoubleClick(object sender, EventArgs e)
        {
            if (tv节点.SelectedNode.Nodes != null)
            {
                if (!string.IsNullOrEmpty(rtb查询语句.Text))//&& dgv数据列表.DataSource==null
                    tsb查询.PerformClick();
            }
        }

        #endregion

        #region 窗体Load事件
        /// <summary>
        /// 更新窗体页面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmPlugIn_Load(object sender, EventArgs e)
        {
            bll_frmMain.SendToMsg += bll_frmMain_SendToMsg; //消息返回时间注册
            //this.propertyGrid1.SelectedObject = treeView1;
            if (File.Exists(path))
            {
                LoadTree();
            }
            else
            {
                CreateXml(path);
            }
        }

        /// <summary>
        /// 加载功能列表
        /// </summary>
        private void LoadTree()
        {
            DataSet ds = new DataSet("system");
            ds.ReadXml(path);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    tv节点.Nodes.Clear();
                    TreeNode _node = new TreeNode
                    {
                        Text = "功能列表"
                    };
                    tv节点.Nodes.Add(_node);
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        TreeNode node = new TreeNode
                        {
                            Text = row["SetingName"].ToString(),
                            Tag = row,
                            Checked = (ds.Tables[0].Columns.Contains("CheckAllin") && row["CheckAllin"].ToString() == "1") ? true : false
                        };
                        //if (ds.Tables[0].Columns.Contains("CheckAllin"))
                        //{
                        //    node.Checked = (ds.Tables[0].Columns.Contains("CheckAllin") && row["CheckAllin"].ToString() == "1") ? true : false;
                        //}
                        _node.Nodes.Add(node);
                    }
                    tv节点.ExpandAll();
                }
            }
        }
        #endregion


        #region 窗体Closed的事件
        /// <summary>
        /// 窗体关闭时需要处理load方法加载的事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmPlugIn_FormClosing(object sender, FormClosingEventArgs e)
        {            
            if (ListSQL.Count > 0)
            {
                if (MessageBox.Show("还有没执行完的任务，是否关闭？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }
            }
            UploadMessage_SendToMsg("窗体正在关闭，将停止上传操作！");
            //如果程序还在运行，先进行停止
            stopor = true;
            bll_frmMain.Stop(stopor);
            if (BgWork!=null && BgWork.IsBusy)
            {
                BgWork.CancelAsync();
            }
            //取消消息返回时间注册
            bll_frmMain.SendToMsg -= bll_frmMain_SendToMsg;
        } 
        #endregion

        #region  按钮事件
        /// <summary>
        /// 修改按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsb添加_Click(object sender, EventArgs e)
        {
            StateType= State.StateType.Add;
            ClearText();//清空所有控件中的内容
            setEnable(true);
        }
        /// <summary>
        /// 保存按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsb保存_Click(object sender, EventArgs e)
        {
            switch (StateType)
            {
                case State.StateType.Add:
                    if (!isNullorEmpty())
                    {
                        if (!Check(txt功能表.Text.Trim()))
                        {
                            PlugInAdd();
                        }
                        else
                        {
                            MessageBox.Show("已存在相同功能表", "提示信息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        MessageBox.Show("功能号、功能表、操作类型、查询语句、数据列表不能为空", "提示信息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case State.StateType.Edit:
                    PlugInEdit();//修改
                    break;
            }
        }
        /// <summary>
        /// 删除按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsb删除_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("确定要删除该配置吗？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                PlugInDelete();
        }
        /// <summary>
        /// 修改按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsb修改_Click(object sender, EventArgs e)
        {
            if (this.tv节点.SelectedNode!=null && this.tv节点.SelectedNode.Text == "功能列表" && this.tv节点.SelectedNode.Checked)
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(path);
                string strPath = "//Configs//Setings";
                XmlNodeList xnl = doc.SelectNodes(strPath);
                foreach (XmlNode node in xnl)
                {
                    if (node.SelectSingleNode("CheckAllin") != null)
                    {
                        node.SelectSingleNode("CheckAllin").InnerText = "1";
                        //bool aa = this.tv节点.Nodes.Find(node.SelectSingleNode("SetingName").InnerText, true)[0].Checked;
                    }
                    else
                    {
                        XmlElement _element = doc.CreateElement("CheckAllin");
                        _element.InnerText = "1";
                        //将节点插入到根节点下
                        node.AppendChild(_element);
                    }
                }
                doc.Save(path);
                LoadTree();
                MessageBox.Show("修改成功！", "消息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            StateType = State.StateType.Edit;
            //rtb查询语句.Text = "";
            tsb保存.Visible = true;
            setEnable(true);
            rtb查询语句.Enabled = true;
            txt功能号.Enabled = true;
            txt功能表.Enabled = false;
        }
        /// <summary>
        /// 查询按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsb查询_Click(object sender, EventArgs e)
        {
            try
            {
                dgv数据列表.DataSource = null;
                string newSQL = string.Format(rtb查询语句.Text, settings.机构编号).ToString();
                if (string.IsNullOrEmpty(newSQL)) return;
                dgv数据列表.DataSource = GetImportTable(newSQL,txt功能号.Text).DefaultView;

                //DataSet ds = new DataSet();
                //string sql = "select top 1000 * from ({0}) a"; //设置每1000行处理一次
                //sql = string.Format(sql, newSQL);
                //if (sql == "")
                //{
                //    rtb查询语句.Text = "";
                //}
                //else
                //{
                //    using (SqlConnection conn = SqlHelper.conn(settings.SQLConn))
                //    {
                //        SqlCommand cmd = new SqlCommand(sql, conn);
                //        conn.Open();
                //        SqlDataAdapter sda = new SqlDataAdapter(cmd);
                //        sda.Fill(ds);
                //    }
                //    dgv数据列表.DataSource = ds.Tables[0].DefaultView;
                //}
            }
            catch (Exception ex)
            {
                Msg.Warning(ex.Message);
            }
            finally
            {
            }
        }
        /// <summary>
        /// 生成XML语句
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsb生成XML_Click(object sender, EventArgs e)
        {
            SelectCodeAndType(out FuncCode, out OperateType);
            DataTable dt = new DataTable();//SqlHelper.ExecuteDataset(SqlHelper.connString, CommandType.Text, rtb查询语句.Text).Tables[0];
            if (dgv数据列表.DataSource != null)
                dt = ((DataView)dgv数据列表.DataSource).ToTable();
            if (dt != null && dt.Rows.Count > 0)
            {
                string str = XMLHelper.ConvertDataTableToEHRXml(dt.Rows[0], FuncCode, OrgCantonNo, OperateType);
                TestXML.Text = str;
            }
        }

        //初始化web服务
        WebReference.InterService ser = new WebReference.InterService();
        //开启线程单独执行
        BackgroundWorker BgWork = null;
        //公卫上传列表，不为空一直传
        List<UploadFile> upXml = new List<UploadFile>();
        //是否停止
        bool stopor = false;
        bool uploadall = false; //是否批量上传，批量上传的时候不再循环单个
        //HIS上传列表
        List<HISUploadFile> ListSQL = new List<HISUploadFile>();

        //上传完成列表-指没有查到数据的sql
        //List<HISUploadFile> ListENDSQL = new List<HISUploadFile>();

        /// <summary>
        /// 上传数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsb上传_Click(object sender, EventArgs e)
        {
            stopor = false; //是否停止
            BgWork = new BackgroundWorker();
            BgWork.DoWork += BgWork_DoWork;
            BgWork.RunWorkerCompleted += BgWork_RunWorkerCompleted;

            //处理查询数据生成xml
            DelegateDoXmlWork();
            tsb上传.Enabled = false;
        }

        void DelegateDoXmlWork()
        {
            string FuncCode = this.txt功能号.Text.Trim();
            string OperateType = "";
            if (this.cbo操作类型.Text.Trim() == "新增")
                OperateType = "1";
            else
                OperateType = "2";

            string sql = "{0}";//"select top 1000 * from ({0}) a"; //设置每1000行处理一次
            sql = string.Format(sql, rtb查询语句.Text);
            //string InsertSql = " insert into tb_上传日志(个人档案编号,功能号)  select top 1000 D001,{1} from ({0}) a ";
            //InsertSql = string.Format(InsertSql, rtb查询语句.Text, txt功能号.Text.Trim());
            DataTable dt = SqlHelper.ExecuteDataset(settings.SQLConn, CommandType.Text, sql).Tables[0];
            //int rows = 0;
            if (dt != null && dt.Rows.Count > 0) //如果查询出来数据不为空，则把查询结果插入上传日志
            {
                if (stopor) return;
                //rows = SqlHelper.ExecuteNonQuery(SqlHelper.connString, CommandType.Text, InsertSql);
                //UploadMessage_SendToMsg("\n" + DateTime.Now.ToString() + ": 影响：" + rows.ToString() + "行 \t");
            }
            else
            {
                stopor = true;
                UploadMessage_SendToMsg("\n停止上传！==>>暂无可用数据！");
            }
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                try
                {
                    UploadFile upl = new UploadFile();//初始化上传类
                    upl.s_个人档案编号 = dt.Rows[i]["D001"].ToString();
                    upl.s_功能号 = this.txt功能号.Text.Trim();
                    
                    upl.s_xml = XMLHelper.ConvertDataTableToEHRXml(dt.Rows[i], FuncCode, OrgCantonNo, OperateType);
                    
                    upXml.Add(upl);
                }
                catch (Exception ex)
                {
                    UploadMessage_SendToMsg("\n初始化上传错误！===>>" + ex.Message);
                    continue;
                }
            }
            //处理完后开始调用后台任务，接口服务
            if (!BgWork.IsBusy)
            {
                BgWork.RunWorkerAsync();
            }
            //MessageBox.Show("处理1000条完毕！");
            UploadMessage_SendToMsg("\n开始处理！==>>不要关闭程序，后台任务正在处理。");
        }

        //后台处理要上传的数据
        void BgWork_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                while (upXml!=null && upXml.Count > 0) //&& !stopor
                {
                    try
                    {
                        string msg = ser.ExecuteAction("key", "IDataTransfersA", upXml[0].s_xml);
                        
                        this.Invoke((EventHandler)delegate
                        {//输出返回
                            if (XMLHelper.ReadXmlNode(msg) != "0")
                            {
                                this.richTextBox1.AppendText(DateTime.Now.ToString() + ": \n 个人档案编号：" + upXml[0].s_个人档案编号 + "\n 返回" + msg + "\n");
                                returnXml.AppendText(DateTime.Now.ToString() + ": " + upXml.Count.ToString() + "\n 个人档案编号：" + upXml[0].s_个人档案编号 + "\n 返回：" + msg + "\n");
                                this.TestXML.AppendText(upXml[0].s_xml + "\n");
                            }
                            else
                            {
                                string InsertSql = " insert into tb_上传日志(个人档案编号,功能号)  values('{0}','{1}') ";
                                //ListInsertSql.Add(string.Format(InsertSql, upXml[0].s_个人档案编号, upXml[0].s_功能号));
                                sb.Append(string.Format(InsertSql, upXml[0].s_个人档案编号, upXml[0].s_功能号));
                            }
                        });
                        upXml.RemoveAt(0);
                    }
                    catch (Exception ex)
                    {
                        upXml.RemoveAt(0);
                        UploadMessage_SendToMsg("\n上传异常：" + ex.Message);
                        continue;
                    }
                }
                try
                {
                    if (!string.IsNullOrEmpty(sb.ToString()))
                    {
                        //循环完毕插入成功的记录
                        int rows = SqlHelper.ExecuteNonQuery(settings.SQLConn, CommandType.Text, sb.ToString());
                        UploadMessage_SendToMsg("\n成功：" + rows.ToString() + "行 \t");
                        sb = new StringBuilder();
                    }
                }
                catch (Exception ex)
                {
                    UploadMessage_SendToMsg("\n插入记录失败"+ex.Message);
                }
            }
            catch
            {
                //upXml = new List<string>(); //初始
            }
            //throw new NotImplementedException();
        }

        //后台完成传输后继续上传
        void BgWork_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (stopor)
            {
                UploadMessage_SendToMsg("\n手动停止上传！");
                //打开上传开关按钮
                tsb上传.Enabled = true;
                return;
            }
            //returnXml.Text = "";
            DelegateDoXmlWork();
            //throw new NotImplementedException();
        }
        
        private void tsb停止_Click(object sender, EventArgs e)
        {
            stopor = true;
            uploadall = false;
            bll_frmMain.Stop(stopor);
            //if (BgWork.IsBusy)
            //{
            //    BgWork.CancelAsync();
            //}
        }

        #endregion
        
        #region  增删改方法
        /// <summary>
        /// 修改方法
        /// </summary>
        /// <returns></returns>
        private bool PlugInEdit()
        {
            bool b = true;
            XmlDocument doc = new XmlDocument();
            doc.Load(path);
            string strPath = "//Configs//Setings";
            XmlNodeList xnl = doc.SelectNodes(strPath);
            foreach (XmlNode node in xnl)
            {
                if (node.ChildNodes[0].InnerText == txt功能表.Text)
                {
                    node.ChildNodes[1].InnerText = this.txt功能号.Text.Trim();
                    node.ChildNodes[2].InnerText = this.cbo操作类型.Text.Trim();
                    node.ChildNodes[3].InnerText = code.EncryptStr(this.rtb查询语句.Text.Trim());
                    if (node.SelectSingleNode("SQLcolumn") != null)
                    {
                        node.SelectSingleNode("SQLcolumn").InnerText = this.txt字段对应.Text.Trim();
                    }
                    else
                    {
                        XmlElement _element = doc.CreateElement("SQLcolumn");
                        _element.InnerText = this.txt字段对应.Text.Trim();
                        //将节点插入到根节点下
                        node.AppendChild(_element);
                    }
                    if (node.SelectSingleNode("CheckAllin") != null)
                    {
                        node.SelectSingleNode("CheckAllin").InnerText = this.chk批量导入.Checked ? "1" : "";
                    }
                    else
                    {
                        XmlElement _element = doc.CreateElement("CheckAllin");
                        _element.InnerText = this.chk批量导入.Checked ? "1" : "";
                        //将节点插入到根节点下
                        node.AppendChild(_element);
                    }
                    break;
                }
            }
            doc.Save(path);
            LoadTree();
            MessageBox.Show("修改成功！", "消息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return b;
        }
        /// <summary>
        /// 添加并保存数据方法
        /// </summary>
        private void PlugInAdd()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(path);
                XmlElement xml = doc.CreateElement("Setings");
                xml.SetAttribute("value", txt功能表.Text.Trim());

                XmlElement ele = doc.CreateElement("SetingName");
                ele.InnerText = txt功能表.Text.Trim();
                xml.AppendChild(ele);

                ele = doc.CreateElement("FuncCode");
                ele.InnerText = this.txt功能号.Text.Trim();
                xml.AppendChild(ele);

                ele = doc.CreateElement("OperateType");
                ele.InnerText = this.cbo操作类型.Text.Trim();
                xml.AppendChild(ele);

                ele = doc.CreateElement("SQLConn");
                ele.InnerText = code.EncryptStr(this.rtb查询语句.Text.Trim()); //把数据链接加密处理
                xml.AppendChild(ele);

                ele = doc.CreateElement("SQLcolumn");
                ele.InnerText = this.txt字段对应.Text.Trim();
                xml.AppendChild(ele);

                ele = doc.CreateElement("CheckAllin");
                ele.InnerText = this.chk批量导入.Checked ? "1" : "";
                xml.AppendChild(ele);

                doc.DocumentElement.AppendChild(xml);
                doc.Save(path);

                LoadTree();

                MessageBox.Show("保存成功！", "消息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "错误提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        /// <summary>
        /// 删除方法
        /// </summary>
        private void PlugInDelete()
        {
            try
            {
                DataSet ds = new DataSet("system");
                ds.ReadXml(path);

                int index = -1;
                foreach (TreeNode node in tv节点.Nodes[0].Nodes)
                {
                    if (node.Text.Trim() == txt功能表.Text.Trim())
                    {
                        index = node.Index;
                        break;
                    }
                }
                if (index != -1)
                    ds.Tables[0].Rows.RemoveAt(index);

                ds.WriteXml(path);
                LoadTree();
                MessageBox.Show("删除成功！", "消息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "错误提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region 方法封装
        /// <summary>
        /// 姓名解密判断
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="i"></param>
        private void DecryptName(DataTable dt, int i)
        {
            if (dt.Columns.Contains("姓名"))//公共卫生基本档案表、糖尿病病例管理卡、高血压病例管理卡、冠心病患者信息表
            {
                strName = DESEncrypt.DES解密(dt.Rows[i]["姓名"].ToString());
            }
            else if (dt.Columns.Contains("本人姓名"))//健康体检表、妇女病普查管理、产后访视记录表、产后42天检查记录表
            {
                strName = DESEncrypt.DES解密(dt.Rows[i]["本人姓名"].ToString());
            }
            else if (dt.Columns.Contains("新生儿姓名"))//新生儿访视随访记录表
            {
                strName = DESEncrypt.DES解密(dt.Rows[i]["新生儿姓名"].ToString());
            }
        }
        /// <summary>
        /// 查询功能号和操作状态
        /// </summary>
        /// <param name="FuncCode"></param>
        /// <param name="OperateType"></param>
        private void SelectCodeAndType(out string FuncCode, out string OperateType)
        {
            tabControl1.SelectedIndex = 1;//跳转到tabPage2页面
            FuncCode = this.txt功能号.Text.Trim();
            OperateType = this.cbo操作类型.Text.Trim();
            if (OperateType == "新增")
            {
                OperateType = "1";
            }
            else if (OperateType == "删除")
            {
                OperateType = "2";
            }
        }

        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            //if (TestXML.Text != "")
            //{
            //    Adapter.WebServiceClient wc = new Adapter.WebServiceClient();
            //    wc.WebServiceInit(settings.WebService);
            //    richTextBox1.AppendText(wc.GetOneMethod("ExecuteAction"));
            //}
        }

        #region HIS/电子病历部分
        //开启线程单独执行
        BackgroundWorker BgHISWork =null;
        //上传列表，不为空一直传
        //List<UploadFile> upXml = new List<UploadFile>();

        //开启线程后台传输
        void BgHISWork_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                //ListENDSQL = e.Argument as List<HISUploadFile>;
                while (ListSQL.Count > 0)
                {
                    if (stopor) { ListSQL.Clear(); return; } //如果停止方法被调用了,那么停止一切办法就是直接返回
                    ToHash(ListSQL[0].s_字段对应);
                    if (Servers.New_db.Count <= 0) continue;
                    DataTable dt_import = GetImportTable(ListSQL[0].s_查询语句, ListSQL[0].s_功能表);
                    //如果列表不为空一直执行导入
                    if (dt_import != null && dt_import.Rows.Count > 0 )
                        //&& ListENDSQL.Find(s=>s.s_功能号.Equals(ListSQL[0].s_功能号))!=null
                    {
                        //this.Invoke((EventHandler)(delegate { dgv数据列表.DataSource = null; }));
                        //正常导入
                        Import(dt_import, ListSQL[0].s_功能号, ListSQL[0].s_操作类型);

                        UploadMessage_SendToMsg(string.Format("\n====>>【{0}】导入完成，即将移除队列！", ListSQL[0].s_功能表));
                    }
                    else
                    {
                        //ListENDSQL.Remove(ListSQL[0]);
                    }
                    ListSQL.RemoveAt(0);
                    //if (ListENDSQL.Count <= 0) stopor = true;//如果上传列表中的数据都已经查询不到，则停止上传
                    if (ListSQL.Count <= 0) return;//stopor = true;

                }
            }
            catch(Exception ex)
            {
                Msg.Warning(ex.Message);
            }
        }

        //后台完成传输后继续上传
        void BgHISWork_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (stopor)
            {
                if (stopor) UploadMessage_SendToMsg("\n==>>手动停止上传！");
                return; //停止上传
            }
            else if (uploadall) //批量上传
            {
                UploadMessage_SendToMsg("\n==>>任务队列执行完毕等待下一轮执行。。。。");
                btnImportAll.PerformClick();
            }

            if (!BgHISWork.IsBusy && !uploadall)
            {
                HISUploadFile histype = new HISUploadFile();
                histype.s_查询语句 = string.Format(rtb查询语句.Text, settings.机构编号).ToString();
                histype.s_功能号 = txt功能号.Text.Trim();
                histype.s_功能表 = txt功能表.Text.Trim();
                histype.s_字段对应 = txt字段对应.Text.Trim();
                if (!string.IsNullOrEmpty(histype.s_查询语句))//如果查询语句为空则跳过
                {
                    ListSQL.Add(histype);
                    //UploadMessage_SendToMsg(string.Format("\n==>>【{0}】添加到队列！", histype.s_功能表));
                }
                //string newSQL = string.Format(rtb查询语句.Text, settings.机构编号).ToString();
                BgHISWork.RunWorkerAsync(ListSQL);
                
            }           
        }

        /// <summary>
        /// 执行导入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnImport_Click(object sender, EventArgs e)
        {
            stopor = false;
            bll_frmMain.Stop(stopor);//是否停止               
            BgHISWork = new BackgroundWorker();
            BgHISWork.DoWork += BgHISWork_DoWork;
            BgHISWork.RunWorkerCompleted += BgHISWork_RunWorkerCompleted;

            if (!string.IsNullOrEmpty(txt功能号.Text) && !string.IsNullOrEmpty(rtb查询语句.Text))
            {
                try
                {
                    ToHash(txt字段对应.Text);
                    //DataTable dt_import = ((DataView)dgv数据列表.DataSource).Table;
                    if (Servers.New_db.Count <= 0)
                        if (MessageBox.Show("没有选择标识列,这样数据只会插入\n是否选择标识列", "提示", MessageBoxButtons.YesNo).Equals(DialogResult.Yes))
                        {
                            //选择标识列
                            if (!Check_Column()) return;
                        }
                        else
                        {
                            return;
                        }
                    if (!BgHISWork.IsBusy)
                    {
                        //string[] newSQL= {string.Format(rtb查询语句.Text, settings.机构编号).ToString(),txt功能号.Text};
                        HISUploadFile histype = new HISUploadFile();
                        histype.s_查询语句 = string.Format(rtb查询语句.Text, settings.机构编号).ToString();
                        histype.s_功能号 = txt功能号.Text.Trim();
                        histype.s_功能表 = txt功能表.Text.Trim();
                        histype.s_字段对应 = txt字段对应.Text.Trim();
                        histype.s_操作类型 = cbo操作类型.Text.Trim();
                        if (!string.IsNullOrEmpty(histype.s_查询语句))//如果查询语句为空则跳过
                        {
                            for (int i = 0; i < 10; i++)
                                ListSQL.Add(histype);
                        }
                        BgHISWork.RunWorkerAsync(ListSQL);
                    }
                    dgv数据列表.DataSource = null;
                    //d.Caption = "";
                }
                catch { }
            }
        }
        
        /// <summary>
        /// 批量导入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnImportAll_Click(object sender, EventArgs e)
        {
            if (ListSQL.Count > 0)
            {
                if (MessageBox.Show("还有没执行完的任务，是否继续批量导入？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.No)
                    return;
            }
            UploadMessage_SendToMsg("\n==>>开始批量导入！");
            foreach (TreeNode n in this.tv节点.Nodes)
            {
                if (n.Nodes.Count > 0)
                {
                    foreach (TreeNode ns in n.Nodes)
                    {
                        if (ns.Tag == null) continue;
                        else
                        {
                            DataRow row = ns.Tag as DataRow;
                            if (row != null )
                            {
                                //string[] newSQL = { string.Format(txt查询语句, settings.机构编号).ToString(), txt功能号 };
                                HISUploadFile histype = new HISUploadFile();
                                histype.s_功能表 = row["SetingName"].ToString();
                                histype.s_功能号 = row["FuncCode"].ToString();//FuncCode
                                histype.s_操作类型 = row["OperateType"].ToString();//OperateType
                                string txt查询语句 = code.DecryptStr(row["SQLConn"].ToString());//需要先解密语句
                                histype.s_查询语句 = string.Format(txt查询语句, settings.机构编号).ToString();
                                if (row.Table.Columns.Contains("SQLcolumn"))
                                    histype.s_字段对应 = row["SQLcolumn"].ToString();
                                if(row.Table.Columns.Contains("CheckAllin"))
                                    histype.s_批量上传 = row["CheckAllin"].ToString();
                                if (!string.IsNullOrEmpty(histype.s_查询语句) &&
                                    !string.IsNullOrEmpty(histype.s_操作类型) &&
                                    !string.IsNullOrEmpty(histype.s_批量上传))//如果查询语句为空则跳过
                                {
                                    ListSQL.Add(histype);
                                    UploadMessage_SendToMsg(string.Format("\n====>>【{0}】添加到队列！", histype.s_功能表));
                                }
                            }

                        }
                    }
                    UploadMessage_SendToMsg("\n==>>队列生成完成共计：【"+ ListSQL.Count.ToString()+"】！");
                    if (ListSQL.Count > 0)
                    {
                        //循环完成后执行后台导入任务
                        stopor = false;
                        uploadall = true; //此处为了不单独某一个功能循环导入
                        bll_frmMain.Stop(stopor);//是否停止  
                        if (BgHISWork == null)
                        {
                            BgHISWork = new BackgroundWorker();
                            BgHISWork.DoWork += BgHISWork_DoWork;
                            BgHISWork.RunWorkerCompleted += BgHISWork_RunWorkerCompleted;
                        }

                        if (!BgHISWork.IsBusy)
                        {
                            BgHISWork.RunWorkerAsync(ListSQL);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 临时写的-字段对应组合哈希表
        /// </summary>
        /// <param name="ss"></param>
        public void ToHash(string ss)
        {
            try
            {
                Servers.Old_db.Clear();
                Servers.New_db.Clear();
                string[] newss = ss.Split('&');
                if (newss.Length < 2) return;
                int keyint = 0;
                string[] sjkl = newss[0].Split(',');
                string[] drl = newss[1].Split(',');
                foreach (string c in sjkl)
                {
                    keyint++;
                    if (!string.IsNullOrEmpty(c))
                    {
                        string s = "false";
                        Hashtable hb1 = new Hashtable();
                        hb1.Add(c, s);
                        Servers.Old_db.Add(keyint, hb1);
                    }
                }
                keyint = 0;
                foreach (string c in drl)
                {
                    keyint++;
                    if (!string.IsNullOrEmpty(c))
                    {
                        string s = "false";
                        Hashtable hb2 = new Hashtable();
                        if (c.Contains("|") && c.Split('|').Length > 1)
                        {
                            if (c.Split('|')[1].Equals("true") || c.Split('|')[1].Equals("0")) s = "true";

                            hb2.Add(c.Split('|')[0].ToString(), s);
                        }
                        else
                            hb2.Add(c, s);
                        Servers.New_db.Add(keyint, hb2);
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// 显示列标识
        /// </summary>
        public bool Check_Column()
        {
            try
            {
                Servers.New_db.Clear();
                Servers.Old_db.Clear();
                //添加标识列
                FrmColumnDY a = new FrmColumnDY();
                ArrayList b = new ArrayList();
                DataTable dt1 = GetYLDB(txt功能号.Text.Trim());
                DataTable dt_import = ((DataView)dgv数据列表.DataSource).Table;
                ArrayList bb = new ArrayList();
                foreach (DataColumn c in dt_import.Columns)
                {
                    b.Add(c.ColumnName);
                }
                foreach (DataColumn f in dt1.Columns)
                {
                    bb.Add(f.ColumnName);
                }
                a.ar = b;
                a.ar1 = bb;
                a.TbaleName = txt功能号.Text.Trim();
                DialogResult dlresult = a.ShowDialog();
                if (dlresult == DialogResult.OK) return true;
                else if (dlresult == DialogResult.Yes)
                {
                    if (a.sb.Length > 0)
                        txt字段对应.Text = a.sb.ToString();
                }

                return false;
            }
            catch { return false; }
        }

        /// <summary>
        /// 导入数据
        /// </summary>
        public void Import(DataTable dt_import,string Types,string state)
        {
            try
            {
                //DataTable dt_import = ((DataView)dgv数据列表.DataSource).Table;
                if (dt_import == null || dt_import.Rows.Count <= 0) return;
                //jin.Maximum = dgvImport.Rows.Count;
                bll_frmMain.SetImport_ORCL(dt_import, Types, state, settings);

            }
            catch { }
        }

        /// <summary>
        /// 查询SQL返回1000行
        /// </summary>
        /// <param name="SQL"></param>
        /// <returns></returns>
        private DataTable GetImportTable(string SQL,string Function)
        {
            try
            {
                DataSet ds = new DataSet();
                string newsql = "select top 1000 * from ({0}\n) a"; //设置每1000行处理一次
                newsql = string.Format(newsql, SQL);

                using (SqlConnection conn = SqlHelper.conn(settings.SQLConn))
                {
                    SqlCommand cmd = new SqlCommand(newsql, conn);
                    conn.Open();
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    sda.Fill(ds);
                }
                //ds  = DbHelperSQL.Query(sql);
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                UploadMessage_SendToMsg("\n上传查询错误-" + Function + "：" + ex.Message);
                return null;
            }
        }
        /// <summary>
        /// 返回消息填充
        /// </summary>
        /// <param name="Info"></param>
        void bll_frmMain_SendToMsg(string Info)
        {
            if (this == null) return;
            this.Invoke((EventHandler)(delegate 
            {
                returnXml.AppendText("\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") +  Info);
            }));
        }

        /// <summary>
        /// 返回消息填充
        /// </summary>
        /// <param name="Info"></param>
        void UploadMessage_SendToMsg(string Info)
        {
            if (this == null) return;
            this.Invoke((EventHandler)(delegate 
            {
                rtbXML.AppendText("\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + Info);
            }));
        }
        

        /// <summary>
        /// 获取oracle数据库功能表
        /// </summary>
        /// <param name="strName"></param>
        /// <returns></returns>
        public DataTable GetYLDB(String strName)
        {
            try
            {
                DataTable dt = DbHelperOra.Query(settings.ORCLConn,string.Format("select * from {0} where rownum<1 ", strName)).Tables[0];
                return dt;
            }
            catch(Exception ex)
            {
                UploadMessage_SendToMsg("\n错误信息：\n" + ex.Message);
                MessageBox.Show("数据表不存在", "提示");
                return null;
            }
        }
        
        #endregion

        private void btnFormat_Click(object sender, EventArgs e)
        {
            rtb查询语句.Text = string.Format(rtb查询语句.Text, settings.机构编号).ToString();
        }

        private void tv节点_AfterCheck(object sender, TreeViewEventArgs e)
        {
            //只处理鼠标点击引起的状态变化
            if (e.Action == TreeViewAction.ByMouse)
            {
                //更新子节点状态
                UpdateChildNodes(e.Node);
            }
        }

        private void UpdateChildNodes(TreeNode node)
        {
            foreach (TreeNode child in node.Nodes)
            {
                child.Checked = node.Checked;
                UpdateChildNodes(child);
            }
        }
    }
}
