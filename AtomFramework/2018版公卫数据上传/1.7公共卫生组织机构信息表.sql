/* 1.7 公共卫生组织机构信息表（18版新增表） - EHR_ORG_BASE */

select 机构名称
	   ,机构编号
	   ,'3713' as 地市
	   ,'371323' as 区县
	   ,'0' as 乡镇
	   ,'未知' as 详细地址
	   ,联系电话
	   ,'0' 服务人口数
	   ,case when 机构级别='2' then '2' else '3' end 类别
	   ,上级机构 as 父级机构编码
	   ,case when isnull(状态,'')='1' then '0' else '1' end 是否有效
	   ,convert(varchar,创建时间,112) as 创建时间
	   ,convert(varchar,创建时间,112) as 更新日期
	   ,'19000101' as 作废日期
	   ,'INSERT' as 交换类型
	   ,GETDATE() as 数据上传时间戳
from  tb_机构信息
where len(机构编号)>=6
  and 创建时间>=DATEADD(day,-1,getdate())
  and 创建时间<getdate()
