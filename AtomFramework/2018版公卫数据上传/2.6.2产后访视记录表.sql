/* 2.6.2产后访视记录表（C-11标） - EHR_POSTPARTUM_VISIT */

select 
	LEFT(a.个人档案编号+CONVERT(varchar(10),a.ID),36) as ID
	,RIGHT(a.个人档案编号,17) as HEALTH_RECORD_CODE
	,LEFT(b.姓名,50) as NAME
	,case when ISNUMERIC(a.体温)<1 then '0'
		  when a.体温<0.1 or a.体温>999.9 then '0'
		  else CONVERT(numeric(4,1),a.体温) end TEMPERATURE
	,'0' as BREAST_RED_SWOLLEN_CODE
	,'' as BREAST_ABNORMAL_DESCRIBE
	,case when a.子宫='1' then '0' else '1' end PALACE_MARK
	,LEFT(a.子宫异常,100) as PALACE_REMARKS
	,case when a.恶露='1' then '0' else '1' end LOCHIA_MARK
	,LEFT(a.恶露异常,100) as LOCHIA_REMARKS	
	,case when a.伤口='1' then '1' else '4' end WOUND_HEAL_CODE
	,'' as WOUND_HEAL_DESCRIBE
	,case when a.转诊='2' then '1' else '0' end REFERRAL_MARK
	,LEFT(a.机构科室,70) as CHANGE_INTO_HOSPITAL_NAME
	,LEFT(a.机构科室,70) as CHANGE_INTO_DEPARTMENT_NAME
	,LEFT(a.转诊原因,100) as REFERRAL_REASON
	,LEFT(a.随访医生签名,50) as FOLLOW_DOCTOR_NAME
	,LEFT(REPLACE(a.下次随访日期,'-',''),8) as NEXT_FOLLOW_DATE
	,LEFT(REPLACE(a.随访日期,'-',''),8) as FOLLOW_DATE
	,case when ISNUMERIC(a.血压1)=0 then '0'
		  when LEN(a.血压1)>3		then '0'
		  else a.血压1 end SHRINK_PRESSURE
	,case when ISNUMERIC(a.血压2)=0 then '0'
		  when LEN(a.血压2)>3		then '0'
		  else a.血压2 end DIASTOLE_PRESSURE
	,case when a.分类='1' then '1' else '0' end MATERNAL_HEALTH_MARK
	,LEFT(a.分类异常,100) as MATERNAL_HEALTH_REMARKS
	,case a.指导 when '1' then '1'
				 when '2' then '2'
				 when '3' then '3'
				 when '4' then '9'
				 when '5' then '10'
				 when '6' then '99'
				 else '' end MATERNAL_HEALTH_TYPE_CODE
	,'' as MATERNAL_HEALTH_TYPE_DESCRIBE
	,'' as POSTPARTUM_VISIT_CODE
	,LEFT(a.一般健康情况,50) as HEALTH_REMARKS
	,LEFT(a.一般心理状况,50) as PSYCHOLOGY_REMARKS
	,case when a.乳房='1' then '1' else '9' end LEFT_BREAST_RESULT_CODE
	,'' as LEFT_BREAST_RESULT_NAME
	,case when a.乳房='1' then '1' else '9' end RIGHT_BREAST_RESULT_CODE
	,'' as RIGHT_BREAST_RESULT_NAME
	,b.身份证号 as ID_NUMBER
	,'' as OTHER_ABNORMAL_CONDITION
	,'' as MULTIPLE_HEALTH_ABNORMAL
	,'1' as MULTIPLE_HEALTH_STATE
	,'' as HEALTH_ABNORMAL_DESCRIBE
	,'' as VISIT_FAIL_REASON
	,convert(datetime,case when ISDATE(a.分娩日期)=0 then '19000101' else a.分娩日期 end) as DELIVER_DATE
	,convert(datetime,case when ISDATE(a.出院日期)=0 then '19000101' else a.出院日期 end) as LEAVE_HOSPITAL_DATE
	,'0' as ENTRING_WAY
	,'0' as CANCLE_CODE
	,'' as CANCLE_NAME
	,a.产次 as BIRTH_TIMES
	,'' as OTHER_HEALTH_GUIDANCE_DESCRIBE
	,'' as FAMILY_SIGN
	,LEFT((select 机构名称 from tb_机构信息 c where c.机构编号=a.所属机构),70) as ORG_NAME
	,RIGHT(a.所属机构,10) as ORG_CODE
	,LEFT(b.区,6) as DISTRICT_CODE
	,'INSERT' as EXCHANGE_TYPE
	,getdate() as SYSTEM_TIME
from tb_孕妇_产后访视情况 a left join tb_健康档案 b on a.个人档案编号=b.个人档案编号
where 1=1 
	 and a.创建时间>=DATEADD(day,-1,getdate())
	 and a.创建时间<getdate()

--and LEFT(a.个人档案编号+CONVERT(varchar(10),a.ID),36) not in
--(select D001 from tb_上传日志 where 功能号='EHR_POSTPARTUM_VISIT')
