/* 2.7.1老年人随访服务 - EHR_OLD_PEOPLE_HEALTH_MANAGE */

select
	LEFT(a.个人档案编号+CONVERT(varchar(10),a.ID),36) as ID
	,RIGHT(z.个人档案编号,17) as HEALTH_RECORD_CODE
	,LEFT(z.姓名,50) as NAME
	,z.性别 as SEX_CODE
	,'1' 身份证件类别代码
	,case when DATALENGTH(z.身份证号)>18 then '' else z.身份证号 end as 身份证件号码
	,z.省 as PROVINCE
	,z.市 as CITY
	,z.区 as COUNTY
	,z.街道 as TOWN
	,z.居委会 as VILLAGE
	,RIGHT(z.居住地址,70) as HOUSE_NUM
	,'276400' as POSTALCODE
	,'' as TEL_TYPE_NAME
	,LEFT(z.本人电话,20) as TEL_NO
	,LEFT(z.联系人姓名,50) as LINKMAN_NAME
	,'' as LINKMAN_TEL_TYPE_NAME
	,LEFT(z.联系人电话,20) as LINKMAN_TEL_NO
	,(select 地区名称 from tb_地区档案 where 地区编码=z.居委会) as NEIGHBORHOOD_NAME
	,'' as NEIGHBORHOOD_LINKMAN_NAME
	,'' as NEIGHBORHOOD_LINKMAN_TEL_TYPE
	,'' as NEIGHBORHOOD_LINKMAN_TEL_NO
	,'' as DUTY_DOCTOR_CODE
	,a.FIELD2 as DUTY_DOCTOR_NAME
	,'' as DUTY_DOCTOR_NAME_TEL_TYPE
	,'未知' as DUTY_DOCTOR_NAME_TEL_NO
	,LEFT(REPLACE(a.体检日期,'-',''),8) as FOLLOWUP_DATE
	,'' as FOLLOWUP_WAY_CODE
	,LEFT(a.症状,5) as SYMPTOM_CODE
	,LEFT(dbo.fn_GetP_DESC(replace(a.症状,' ',''),'zz_zhengzhuang'),25) as SYMPTOM_NAME
	,case when a.身高>=40.0 and a.身高<=240.0
		  then CONVERT(numeric(5,1),a.身高) else 0 end as HEIGHT
	,case when a.体重>=25.0 and a.体重<=300.0
		  then CONVERT(numeric(5,1),a.体重) else 0 end as BODY_WEIGHT
	,case when dbo.fn_isNum(a.血压右侧1,1,'0')>=30 and dbo.fn_isNum(a.血压右侧1,1,'0')<=350
		  then CONVERT(numeric,a.血压右侧1) else 0 end as SBP
	,case when dbo.fn_isNum(a.血压右侧2,1,'0')>=20 and dbo.fn_isNum(a.血压右侧2,1,'0')<=350
		  then CONVERT(numeric,a.血压右侧2) else 0 end as DBP
	,case when CONVERT(numeric,dbo.fn_isNum(a.空腹血糖,2,'99'))>=0 
		  and  CONVERT(numeric,dbo.fn_isNum(a.空腹血糖,2,'99'))<=50 
		  then CONVERT(numeric(4,1),a.空腹血糖) else 0 end as FBG_VALUE
	,case when isnull(a.老年人状况评估,'')='' then '0' else a.老年人状况评估 end HEALTH_STATUS_CODE
	,case when isnull(a.老年人自理评估,'')='' then '0' else a.老年人自理评估 end SELF_CARE_ABILITY_CODE
	,case when isnull(a.老年人认知,'')='' then '0' else a.老年人认知 end COGNITIVE_FUNCTION_CODE
	,case when ISNUMERIC(a.老年人认知分)=0 then '0' else CONVERT(decimal,a.老年人认知分) end COGNITIVE_FUNCTION_SCORE
	,a.老年人情感 as EMOTIONAL_STATE_CODE
	,case when ISNUMERIC(a.老年人情感分)=0 then '0' else CONVERT(decimal,a.老年人情感分) end DEPRESS_SCORE
	,LEFT(a.FIELD2,50) as CHECK_PERSON_NAME
	,LEFT(REPLACE(a.体检日期,'-',''),8) as CHECK_DATE
	,a.吸烟状况 as SMOKING_STATUS_CODE
	,case when ISNUMERIC(a.开始吸烟年龄)=0 then '0' else CONVERT(decimal,a.开始吸烟年龄) end FIRST_SMOKE_AGE
	,CONVERT(numeric,dbo.fn_isNum(a.日吸烟量,1,'0')) as DAY_SMOKE_AMT
	,0 as DAY_SMOKE_TAR_AMT
	,case when ISNUMERIC(a.戒烟年龄)=0 then '0' else CONVERT(decimal,a.戒烟年龄) end QUIT_SMOKE_AGE
	,case when ISNUMERIC(a.开始饮酒年龄)=0 then '0' else CONVERT(decimal,a.开始饮酒年龄) end FIRST_DRINK_AGE
	,a.饮酒频率 as DRINK_FREQ_CODE
	,case when a.饮酒种类='99' then '9' else LEFT(a.饮酒种类,1) end DRINK_TYPE_CODE
	,case when ISNUMERIC(a.日饮酒量)=0 then '0' else CONVERT(decimal,a.日饮酒量) end DAY_DRINK_AMT
	,0 as TAR_DAY_DRINK_AMT
	,a.近一年内是否曾醉酒 as DRUNK_MARK
	,a.是否戒酒 as QUIT_DRINK_MARK
	,case when ISNUMERIC(a.戒酒年龄)=0 then '0' else CONVERT(decimal,a.戒酒年龄) end QUIT_DRINK_AGE
	,0 as WEEK_EXERCISE_COUNT
	,0 as WEEK_EXERCISE_TAR_COUNT
	,0 as EXERCISE_TIME
	,0 as EXERCISE_TAR_TIME
	,0 as CON_MON_EXERCISE_TIME
	,'' as EXERCISE_WAY
	,'' as SALT_INTAKE_GRADE_CODE
	,'' as SALT_INTAKE_TAR_GRADE_CODE
	,LEFT(a.饮食习惯,1) as FOOD_HABITS_CODE
	,'' as REASONABLE_DIET_CODE
	,'' as PSYCHOLOGY_STATUS_CODE
	,'' as PSYCHOLOGY_ADJUST_CODE
	,'' as DOCTOR_ADVICE_CODE
	,'' as PSYCHOLOGY_GUIDE_MARK
	,'' as PSYCHOLOGY_GUIDE_DETAIL
	,'' as CHINESE_MED_TYPE_CODE
	,(select top 1 y.药物名称 from tb_健康体检_用药情况表 y where y.个人档案编号=a.个人档案编号 and y.创建时间=a.创建时间) as DRUG_NAME
	,(select top 1 y.用法 from tb_健康体检_用药情况表 y where y.个人档案编号=a.个人档案编号 and y.创建时间=a.创建时间) as USING_DRUG_FREQ
	,'' as DOSE_UNIT
	,0 as DOSE_SINGLE
	,0 as DOSE_TOTAL
	,'' as DRUG_WAY_CODE
	,'' as DRUG_COMPLIANCE_CODE
	,LEFT(REPLACE(b.入院日期,'-',''),8) as ADMISSION_DATE
	,LEFT(b.原因,100) as ADMISSION_REASON
	,LEFT(REPLACE(b.出院日期,'-',''),8) as DISCHARGE_DATE
	,LEFT(b.医疗机构名称,70) as PRE_HOS_ORG_NAME
	,LEFT(b.病案号,18) as PRE_CASE_NUM
	,LEFT(REPLACE(c.出院日期,'-',''),8) as CANCEL_BED_DATE
	,LEFT(REPLACE(c.入院日期,'-',''),8) as BUILD_BED_DATE
	,LEFT(c.原因,100) as BUILD_BED_REASON
	,LEFT(c.医疗机构名称,70) as PRE_FAMILY_HOS_ORG_NAME
	,0 as OXYGEN_TIME
	,'' as VACCINE_CODE
	,'' as VACCINE_BATCH_NUMBER
	,'' as VACCINATE_DATE
	,'' as VACCINATE_ORG_NAME
	,'' as FOLLOWUP_CYCLE_ADVISE_CODE
	,'' as NEXT_FOLLOWUP_DATE
	,'' as FOLLOWUP_DOCTOR_CODE
	,LEFT(a.FIELD2,50) as FOLLOWUP_DOCTOR_NAME
	,(select 机构名称 from tb_机构信息 where 机构编号=a.所属机构) as ORG_NAME
	,a.所属机构 as ORG_CODE
	,'' as CITIZEN_ID
	,'' as EFFECTIVE_TIME
	,'' as STATUS
	,convert(datetime,case when ISDATE(a.创建时间)=0 then '19000101' else a.创建时间 end) AS CREATED_AT
	,z.区 as DISTRICT_CODE
	,'INSERT' as EXCHANGE_TYPE
	,'' as DEI
	,'' as NODE_CODE
	,GETDATE() as SYSTEM_TIME
from tb_健康体检 a
left join tb_健康体检_住院史 b on a.个人档案编号=b.个人档案编号 and a.创建时间=b.创建日期 and b.类型='1'
left join tb_健康体检_住院史 c on a.个人档案编号=c.个人档案编号 and a.创建时间=b.创建日期 and c.类型='2'
left join tb_健康档案 z on a.个人档案编号=z.个人档案编号
where z.出生日期 <= convert(varchar(10),dateadd(yy,-65,getdate()),120)
	and a.创建时间>=DATEADD(day,-1,getdate())
	and a.创建时间<getdate()


--and not exists(select 1 from tb_上传日志 b where b.D001=LEFT(a.个人档案编号+CONVERT(varchar(10),a.ID),36)
--and b.功能号='EHR_OLD_PEOPLE_HEALTH_MANAGE')

