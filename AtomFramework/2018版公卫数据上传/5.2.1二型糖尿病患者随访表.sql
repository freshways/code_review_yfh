/* 5.2.1二型糖尿病患者随访表（C-16） - EHR_TOW_DIABETES_FOLLOW */

SELECT a.个人档案编号+convert(varchar,a.ID)  AS ID
      ,RIGHT(a.个人档案编号,17) AS HELTH_ARCHIVES_CODE
      ,a.ID AS DIABETES_RECORD_CODE  
      ,b.姓名 AS NAME
      ,b.性别编码 AS GENDER 
	  ,b.性别 GENDERN
      ,'1' AS ID_TYPE
	  ,'居民身份证' AS ID_TYPE_N
      ,b.身份证号 AS ID_CODE
      ,'山东省' AS PERMANENT_ADDRESS_PROVINCE
      ,b.市 AS PERMANENT_ADDRESS_CITY
      ,b.区 AS PERMANENT_ADDRESS_AREA
      ,b.街道 AS PERMANENT_ADDRESS_VILLAGE
      ,b.居委会 AS PERMANENT_ADDRESS_STREET
      ,'' AS PERMANENT_ADDRESS_NUM
      ,'276400' AS PERMANENT_ADDRESS_POSTCODE 
      ,'山东省' AS LIVE_ADDRESS_PROVINCE
      ,b.市 AS LIVE_ADDRESS_CITY
      ,b.区 AS LIVE_ADDRESS_AREA
      ,b.街道 AS LIVE_ADDRESS_VILLAGE
      ,b.居委会 AS LIVE_ADDRESS_STREET
      ,'' AS LIVE_ADDRESS_NUM
      ,'276400' AS LIVE_ADDRESS_POSTCODE
      ,'1' AS HER_PHONE_TYPE 
      ,联系电话 AS HER_PHONE_NUM 
      ,联系人姓名 AS HER_CONTACTS_NAME  
      ,'' AS HER_CONTACTS_TYPE  
      ,联系人电话 AS HER_CONTACTS_NUM
      ,b.居委会 AS COM_NAME  
      ,'' AS COM_CONTACTS_NAME  
      ,'' AS HER_COM_CONTACTS_PHONE_TYPE   
      ,'' AS HER_COM_CONTACTS_PHONE_NUM 
      ,随访医生 AS RESPON_DOCTOR_NAME 
      ,'' AS RESPON_DOCTOR_PHONE_TYPE  
      ,'' AS RESPON_DOCTOR_PHONE_NUM  
      ,SUBSTRING(目前症状,1,1) AS SYMPTOM_CODE
      ,isnull((SELECT P_DESC FROM tb_常用字典 b WHERE b.P_CODE=SUBSTRING(a.目前症状,1,1) AND b.P_FUN_DESC='糖尿病目前症状' ),'其他') AS SYMPTOM_NAME 
      ,收缩压 as SYSTOLIC_PRESSURE
      ,舒张压 as DIASTOLIC_PRESSURE
      ,'0' AS TEMPERATURE 
      ,CAST(身高 AS DECIMAL(5,1)) AS HEIGHT  
      ,CAST(体重 AS DECIMAL(6,2)) as WEIGHT
      ,CAST(体重2 AS DECIMAL(6,2)) AS TARGET_WEIGHT  
      ,CAST(BMI AS DECIMAL(5,2)) AS BODY_INDEX
      ,CAST(BMI2 AS DECIMAL(5,2)) AS TARGET_BODY_INDEX
      ,足背动脉搏动 AS FOOT_ARTERY_SIGN
      ,体征其他 AS OTHER_POSITIVE_SIGNS 
      ,case when 吸烟数量>0 then '3' else '1' end SMOKE_STATUS_CODE
	  ,case when 吸烟数量>0 then '吸烟' else '从不吸烟' end SMOKE_STATUS_NAME
      ,'0' AS SMOKE_AGE  
      ,吸烟数量 AS SMOKE_START_DAY_NUM
      ,吸烟数量2 AS TARGET_SMOKE_DAY_NUM
      ,'0' AS QUIT_SMOKING_AGE
      ,'0' AS WINE_DRINK_AGE
      ,case when 饮酒数量>0 then '2' else '1' end DRINK_FREQUENCY_CODE
	  ,case when 饮酒数量>0 then '偶尔' else '从不' end DRINK_FREQUENCY_NAME
      ,'9' AS DRINK_KIND_CODE
	  ,'其他' AS DRINK_KIND_NAME
      ,CAST(饮酒数量 AS DECIMAL(5,0)) AS DRINK_MILLILITER
      ,CAST(饮酒数量2 AS DECIMAL(5,0)) AS TARGET_DRINK_MILLILITER
      ,'1' AS DRUNK_SIGN
	  ,'' as QUIT_DRINK_SIGN
      ,'0' AS QUIT_DRINK_AGE  
      ,运动频率 AS EXERCISE_AMOUNT
      ,运动频率2  AS TARGET_EXERCISE_AMOUNT 
      ,CAST(运动持续时间 AS DECIMAL(5,0)) AS EXERCISE_TIMES
      ,CAST(运动持续时间2 AS DECIMAL(5,0)) AS TARGET_EXERCISE_TIMES
      ,'0' AS EXERCISE_CONTINUED_TIME
      ,'未知' AS EXERCISE_MODE
      ,'' AS SALT_LEVEL_CODE
      ,'' AS TARGET_SALT_LEVEL_CODE    
      ,'0' AS STAPLE_FOOD
      ,'0' AS TARGET_STAPLE_FOOD
      ,'' AS DIET_CODE
	  ,'' as DIET_NAME
      ,'' AS DIET_RESULT_CODE
      ,'' AS MENTALITY_CODE
	  ,'' as MENTALITY_NAME
      ,心理调整 AS MENTALITY_ADJUST_CODE
      ,遵医行为 AS COMPLY_DOCTOR_CODE
      ,CAST(空腹血糖 AS DECIMAL(4,1)) AS FASTING_BLOOD_GLUCOSE
      ,'0' AS BLOOD_GLUCOSE 
      ,CAST(isnull(糖化血红蛋白,0) AS DECIMAL(4,1)) AS SUGAR_HEMOGLOBIN
      ,'' AS AUXILIARY_INSPECT_PROJECT
      ,'' AS AUXILIARY_INSPECT_RESULT
      ,'' AS INSPECT_NAME
      ,REPLACE(发生时间,'-','') AS INSPECT_DATE   
      ,服药依从性 AS DOSE_CODE
      ,药物副作用 AS ADVERSE_REACTION_MARK
      ,副作用详述 AS ADVERSE_REACTION_DESCRIBE
      ,低血糖反应 AS HYPOGLYCEMIC_CODE
      ,'' AS DRUE_TYPE_CODE
	  ,'' as DRUE_TYPE_NAME
      ,'' AS DRUG_INSULIN_TYPE
      ,'0' AS DRUG_INSULIN_TIMES
      ,'0' AS DRUG_INSULIN_AMOUNT
      ,'' AS INHOSPATIL_DATE
      ,'' AS INHOSPATIL_RESON
      ,'' AS OUTHOSPATIL_DATE
      ,'' AS FORME_INHOSPATIL_NAME
      ,'' AS FORME_RECORD_CODE
      ,'' AS HOME_BED_DATE
      ,'' AS HOEM_BED_RESON
      ,'' AS HOME_BED_EVACUATE_DATE
      ,'' AS FORME_HOME_BED_NAME
      ,'' AS FORME_BED_RECORD_CODE 
      ,'0' AS OXYGEN_TIMES
      ,转诊原因 as TRANSFER_RESON
      ,转诊科别 AS TRANSFER_HOSPATIL_NAME
      ,转诊科别 AS TRANSFER_DEPARTMENT_NAME
      ,'' AS CYCLE_CODE
	  ,'' as CYCLE_NAME
      ,'' AS MARITAL_STATUS
      ,'' AS PROFESSION_TYPE_CODE
	  ,'' as PROFESSION_TYPE_NAME
      ,REPLACE(b.出生日期,'-','') AS BIRTH_DATE 
      ,'' AS MANAGEMENT_GROUP
      ,'' AS DISASE_SOURCE
      ,REPLACE(CONVERT(VARCHAR(10),a.创建时间,112),'-','') AS SET_CARD_DATE
	  ,'0' AS TOTAL_CHOLESTEROL 
      ,'0' AS TRIGLYCERIDE 
      ,'0' AS LOW_DENSITY_LIPOPROTEIN
      ,'0' AS HIG_DENSITY_LIPOPROTEIN
      ,'' AS URINE_MICRO_ALBUMIN 
      ,'' AS STOP_SUPERVISE_DATE 
      ,'' AS STOP_RESON
      ,'' AS DEFULT_RESON
      ,'' AS OTHER_SYMPTOM
      ,'' AS OTHER_DIABETES_TYPE
	  ,REPLACE(CONVERT(VARCHAR(10),a.创建时间,112),'-','') AS CREAT_TIME
      ,'0' AS CARD_STATUS
      ,'' AS DIABETES_TYPE
      ,'' AS DISEASE_TYPE_CODE
      ,'' AS PATHOLOGICAL_TYPE
      ,'0' AS ACTUAL_AGE
      ,'' AS COMPANY_NAME
      ,'' AS OTHER_DISEASE_TYPE
      ,'0' AS CLINICAL_DIAGNOSIS_YEAR
      ,'0' AS IGR_DIAGNOSIS_YEAR
      ,'' AS EDUCATION_DEGREE_CODE
      ,'' AS MARITAL_TYPE_CODE
      ,'' AS MEDICAL_TYPE_CODE
      ,'0' AS REGISTER_SUGAR 
      ,'0' AS RANDAM_SUGAR
      ,'' AS DIABETES_KIN_CODE
	  ,'' AS PATIENT_RELATION_CODE
      ,'' AS DIABETES_COMPLICATION 
      ,'' AS DIABETES_COMPLICATION_TYPE
      ,'' AS DIABETES_COMPLICATION_NAME
      ,'' AS DIABETES_COMPLICATION_TIME 
      ,'' AS DEATH_MESSAGE
      ,'' AS EXERCISE_CODE
	  ,'' as EXERCISE_NAME
	  ,'' as OTHER_SYM_DESC
	  ,'' as DRINK_WINE_MARK
	  ,'' as SUGAR_HEMOGLOBIN_MON
	  ,'' as SUGAR_HEMOGLOBIN_DAY
	  ,'' as SMOKE_MARK
	  ,'' as ZY_HEALTH_GUIDANCE
	  ,下一步管理措施 as NEXT_STEP
	  ,'常规随访' as NEXT_STEP_NAME
      ,REPLACE(下次随访时间,'-','') AS NEXT_VISIT_DATE	   
      ,REPLACE(CONVERT(VARCHAR(10),发生时间,112),'-','') AS THIS_FOLLOW_DTAE    
	  ,随访方式 as THIS_FOLLOW_CODE
	  ,本次随访分类 AS EVALUATION_RESULTS_CODE   
	  ,(select P_DESC from tb_常用字典 where P_FUN_CODE ='sffl' and P_CODE=a.本次随访分类) AS EVALUATION_RESULTS_NAME
      ,随访医生 AS VISIT_DOCTOR_NAME
	  ,'' as RESIDENT_NAME
	  ,'INSERT' as EXCHANGE_TYPE
	  ,GETDATE() as SYSTEM_TIME
      ,b.创建机构 AS ORG_CODE
	  ,left(a.个人档案编号,6) as DISTRICT_CODE  
	  ,ISNULL((SELECT 机构名称 FROM tb_机构信息 c WHERE c.机构编号=b.创建机构),'未知') AS ORG_NAME 
  FROM tb_MXB糖尿病随访表 a
  left join View_个人信息表 b on a.个人档案编号 = b.个人档案编号
  WHERE 1=1
  AND b.身份证号<>'' 
  and (isnull(收缩压,'')<>'' and isnull(舒张压,'')<>'')
  and a.创建时间>=DATEADD(day,-1,getdate())
  and a.创建时间<getdate()

  --and  not exists(
  --select 1 from tb_上传日志 rz where (a.个人档案编号+convert(varchar,a.ID)) = rz.D001 and 功能号='EHR_TOW_DIABETES_FOLLOW')
