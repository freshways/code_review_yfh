/* 家族史(辅C-1标) - EHR_PIR_FAMILY */

select  (a.个人档案编号+convert(varchar,a.ID)) as 疾病史内码
	  ,b.个人档案编号+b.ID as 个人基本信息内码
	  ,RIGHT(a.[个人档案编号],17) AS 个人档案编号
	  ,case when isnull(a.家族关系,'')='' then '99' else replace(substring(a.家族关系,1,2),',','') end as 患者与本人关系
	  ,case when isnull(a.家族病史,'')='' then '99' else replace(substring(a.家族病史,1,2),',','') end as 既往患病种类代码
	  ,(select P_DESC from tb_常用字典 where P_FUN_CODE='jzsxin' and P_CODE=replace(substring(a.家族病史,1,2),',','')) as 疾病名称
	  ,convert(date,case when isdate(b.创建时间)<>1 then '19000101' else b.创建时间 end) 创建时间
	  ,left(a.个人档案编号,6) as 所属行政区划
	  ,'INSERT' as 交换类型
	  ,getdate() as 数据上传时间戳
	  ,所属机构
	  ,所属机构名称
from tb_健康档案_家族病史 a
left join View_个人信息表 b on a.个人档案编号=b.个人档案编号
where 1=1
and a.家族病史!='' and b.个人档案编号!=''
and b.创建时间>=DATEADD(day,-1,getdate())
  and b.创建时间<getdate()
