/* 10.1糖尿病病例管理卡 - EHR_DM_CARD */

SELECT a.ID AS ID
      ,a.管理卡编号 as CARD_NUM
      ,RIGHT(a.个人档案编号,17) AS HEALTH_RECORD_CODE
      ,b.姓名 as NAME
      ,b.性别 as SEX
      ,'1' AS ID_NO_CD
      ,b.身份证号 AS ID_NO
      ,b.婚姻状况 as MATRIAL_STATUS
      ,b.职业 AS OCCUPATION_CODE
	  ,'' as OCCUPATION_NAME
      ,REPLACE(b.出生日期,'-','') AS BORN_DATE
      ,b.本人电话 AS LINK_TEL
      ,a.身高 as HEIGHT
      ,a.体重 as BODY_WEIGHT
      ,a.腰围 as WAISTLINE
      ,a.管理组别 as GROUP_TYPE
	  ,'' as GROUP_NAME
      ,case when a.病例来源='99' then '4' else a.病例来源 end CASE_SOURCE
	  ,'' as CASE_SOURCE_NAME
      ,b.居住地址 AS ADDRESS
      ,a.糖尿病类型 as DM_TYPE
	  ,'' as DM_NAME
      ,a.家族史 AS DISEASE_TYPE
	  ,'' as DISEASE_TYPE_NAME
      ,SUBSTRING(a.目前症状,1,1) AS SYMPTOM_CODE
	  ,'' as SYMPTOM_NAME
      ,a.目前症状其他 AS OTHER_SYMPTOM
      ,'' AS OTHER_DM_TYPE
      ,a.胰岛素用量 as INS_DRUG_DOSE
      ,a.吸烟情况 AS DAILY_SMOKING
      ,a.饮酒情况 AS WEEK_DRINK_COUNT
      ,'' AS QUANTITY_EVERYTIME 
      ,a.锻炼频率 AS WEEK_EXERCISE_COUNT
      ,'0' AS EXERCISE_TIME 
      ,a.生活自理能力 as SELF_CARE_ABILITY
	  ,'' as SELF_CARE_ABILITY_NAME
      ,a.BMI AS BMI
      ,a.收缩压 as SBP
      ,a.舒张压 as DBP
      ,CAST(a.空腹血糖 AS INT) AS FBG_VALUE
      ,CAST(a.餐后血糖 AS INT) AS PBG2H_VALUE
      ,CAST(a.总胆固醇 AS INT) AS CHOL_VALUE
      ,CAST(a.甘油三脂 AS INT) AS TG_VALUE
      ,CAST(a.低密度蛋白 AS INT) AS LDL_VALUE
      ,CAST(a.高密度蛋白 AS INT) AS HDL_VALUE
      ,a.尿微量蛋白 AS IS_MAU
      ,CAST(a.糖化血红蛋白 AS INT) AS HBA1C_VALUE
      ,a.终止管理日期 as END_MANAGE_DATE
      ,a.终止理由 as END_REASON
      ,'' AS LOST_REASON
	  ,case when ISDATE(a.确诊时间)=0 then '19000101' else REPLACE(CONVERT(varchar(10),convert(date,a.确诊时间),120),'-','') end MEDICAL_DATE
      ,a.确诊单位 AS MEDICAL_ORG
      ,a.创建人 AS DOCTOR_CODE
      ,(SELECT UserName FROM tb_MyUser d WHERE a.创建人=d.用户编码 ) AS DOCTOR_NAME
      ,REPLACE(SUBSTRING(a.创建时间,1,10 ),'-','') AS CARD_DATE 
      ,a.创建机构 AS ORG_CODE
      ,(SELECT 机构名称 FROM tb_机构信息 c WHERE a.创建机构=c.机构编号) AS ORG_NAME
      ,convert(datetime,case when isdate(a.创建时间)<>'1' then '1999-01-01' else a.创建时间 end) AS CREATED_AT 
      ,'1' as STATUS
	  ,case when ISDATE(a.发生时间)=0 then '19000101' else REPLACE(CONVERT(varchar(10),convert(date,a.发生时间),120),'-','') end EFFECTIVE_TIME
      ,a.个人档案编号 as CITIZEN_ID
      ,RIGHT(a.[个人档案编号],17) AS DISTRICT_CODE
      ,'INSERT' as EXCHANGE_TYPE
      ,null as DEI
      ,null as NODE_CODE
      ,getdate() as SYSTEM_TIME
  FROM tb_MXB糖尿病管理卡 a
  LEFT JOIN tb_健康档案 b ON b.个人档案编号=a.个人档案编号
  WHERE a.管理卡编号<>''
  AND a.目前症状<>'' and b.身份证号<>''
  and a.创建时间>=convert(varchar,DATEADD(day,-1,getdate()),120)
  and a.创建时间<convert(varchar,getdate(),120)

  --and  not exists(
  --select 1 from tb_上传日志 rz where a.ID = rz.D001 and 功能号='EHR_DM_CARD' )