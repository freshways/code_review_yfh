/* 9.1高血压病例管理卡 - EHR_HBP_CARD */

SELECT a.个人档案编号+convert(varchar,a.ID) AS ID
      ,case when isnull(a.管理卡编号,'')='' then a.ID else a.管理卡编号  end CARD_NUM
      ,RIGHT(a.个人档案编号,17) AS HEALTH_RECORD_CODE
      ,b.姓名 as NAME
      ,b.性别 as SEX
      ,'1' AS ID_NO_CD
      ,b.身份证号 AS ID_NO
      ,REPLACE(b.出生日期,'-','') AS BORN_DATE
      ,b.本人电话 AS LINK_TEL
      ,b.婚姻状况 AS MATRIAL_STATUS
      ,b.职业 AS OCCUPATION_CODE 
      ,a.管理组别 as GROUP_TYPE
      ,a.病例来源 as CASE_SOURCE
      ,b.居住地址 AS ADDRESS         
      ,a.家族史 AS DISEASE_TYPE
      ,a.目前症状 AS SYMPTOM_CODE
      ,a.目前症状其他 AS OTHER_SYMPTOM
      ,'' AS BRAIN_DISEASE_TYPE
      ,'' AS HEART_DISEASE_TYPE
      ,'' AS KIDNEY_DISEASE_TYPE
      ,'' AS EYE_DISEASE_TYPE
      ,'' AS TAKE_MEDICINE_STATUS     
      ,isnull(a.吸烟情况,0) AS DAILY_SMOKING
      ,isnull(a.饮酒情况,0) AS WEEK_DRINK_COUNT
      ,'0' AS QUANTITY_EVERYTIME 
      ,isnull(a.运动频率,0) AS WEEK_EXERCISE_COUNT
      ,'0' AS EXERCISE_TIME 
      ,a.生活自理能力 as SELF_CARE_ABILITY
      ,isnull(a.身高,0) AS HEIGHT
      ,isnull(a.体重,0) AS BODY_WEIGHT
      ,isnull(a.腰围,0) AS WAISTLINE
      ,isnull(a.BMI,0) AS BMI
      ,isnull(a.总胆固醇,0) AS CHOL_VALUE
      ,isnull(a.甘油三脂,0) AS TG_VALUE
      ,isnull(a.收缩压,0) AS SBP
      ,isnull(a.舒张压,0) AS DBP
      ,isnull(a.空腹血糖,0) AS FBG_VALUE
      ,isnull(a.低密度蛋白,0) AS LDL_VALUE
      ,isnull(a.高密度蛋白,0) AS HDL_VALUE
      ,'' AS ECG
      ,a.终止管理日期 AS END_MANAGE_DATE
      ,a.终止理由 AS END_REASON
      ,'' AS LOST_REASON
      ,a.创建人 AS DOCTOR_CODE
      ,(SELECT UserName FROM tb_MyUser c WHERE a.创建人=c.用户编码) AS DOCTOR_NAME
      ,REPLACE(SUBSTRING(a.创建时间,1,10),'-','') AS CARD_DATE
      ,a.创建机构 AS ORG_CODE
      ,(SELECT 机构名称 FROM tb_机构信息 d WHERE a.创建机构=d.机构编号) AS ORG_NAME 
      ,convert(datetime,case when isdate(a.创建时间)<>'1' then '1999-01-01' else a.创建时间 end) as CREATED_AT
      ,null STATUS
      ,REPLACE(SUBSTRING(a.创建时间,1,10),'-','') AS EFFECTIVE_TIME
      ,'' as CITIZEN_ID
	  ,left(a.个人档案编号,6) as DISTRICT_CODE
      ,'INSERT' as EXCHANGE_TYPE
      ,null as DEI
      ,null AS NODE_CODE
	  ,GETDATE() as SYSTEM_TIME    
  FROM tb_MXB高血压管理卡 a
  LEFT JOIN tb_健康档案 b ON b.个人档案编号=a.个人档案编号
  WHERE 1=1
  AND b.身份证号<>''
  and a.创建时间>=convert(varchar,DATEADD(day,-1,getdate()),120)
  and a.创建时间<convert(varchar,getdate(),120)

  --and  not exists(
  --select 1 from tb_上传日志 rz where (a.个人档案编号+convert(varchar,a.ID)) = rz.D001 and 功能号='EHR_HBP_CARD')
