/* 11.2脑卒中病人管理 - EHR_STROKE_PATIENT_MANAGE */

select
	a.个人档案编号+convert(varchar,a.ID) AS ID
	,RIGHT(a.个人档案编号,17) as HEALTH_RECORD_CODE
	,LEFT(z.姓名,50) as NAME
	,LEFT(z.性别,1) as SEX 
	,LEFT(REPLACE(z.出生日期,'-',''),8) as BORN_DATE
	,LEFT(z.民族,2) as NATION
	,'汉' as NATION_NAME
	,'1' AS ID_TYPE_CODE
	,'身份证' as ID_TYPE_NAME
	,case when DATALENGTH(z.身份证号)>18 then '' else z.身份证号 end ID_NUM
	,case a.职业 when '99' then 'J10' else 'J05' end OCCUPATION_CODE
	,case a.职业 when '99' then '无职业' else '农、林、牧、渔、水利业生产人员' end OCCUPATION_NAME
	,(select 地区名称 from tb_地区档案 where 地区编码=z.省) as HOUSEHOLD_PROVINCE
	,(select 地区名称 from tb_地区档案 where 地区编码=z.市) as HOUSEHOLD_CITY
	,(select 地区名称 from tb_地区档案 where 地区编码=z.区) as HOUSEHOLD_AREA
	,(select 地区名称 from tb_地区档案 where 地区编码=z.街道) as HOUSEHOLD_TOWN
	,(select 地区名称 from tb_地区档案 where 地区编码=z.居委会) as HOUSEHOLD_VILLAGE
	,RIGHT(z.居住地址,70) as HOUSEHOLD_HOUSE_NO
	,(select 地区名称 from tb_地区档案 where 地区编码=z.省) as PROVINCE_NAME
	,(select 地区名称 from tb_地区档案 where 地区编码=z.市) as CITY_NAME
	,(select 地区名称 from tb_地区档案 where 地区编码=z.区) as AREA_NAME
	,(select 地区名称 from tb_地区档案 where 地区编码=z.街道) as TOWN_NAME
	,(select 地区名称 from tb_地区档案 where 地区编码=z.居委会) as VILLAGE_NAME
	,RIGHT(z.居住地址,70) as HOUSE_NO
	,LEFT(z.本人电话,20) as TELEPHONE
	,LEFT(a.联系电话,20) as CONTACT_TELE
	,'' as RELATION_WITH
	,LEFT(z.婚姻状况,2) as MARITAL_STATUS_CODE
	,'' as MARITAL_STATUS_NAME
	,case  when z.文化程度 in('99','100') then '90' else LEFT(z.文化程度,2) end as EDUCATION_CODE
	,substring((select P_DESC from tb_常用字典 where P_FUN_DESC ='文化程度' and P_CODE=a.[文化程度]),1,20) as EDUCATION_NAME
	,case LEFT(z.医疗费支付类型,1) when '1' then '7' when '2' then '6' when '3' then '1'
								   when '4' then '2' when '5' then '3' when '6' then '8'
								   when '7' then '5' when '8' then '4' else '99' end INSURANCE_TYPE_CODE
	,substring(case when ISNULL(a.医疗费支付类型,'')='' then '不祥' else
	  STUFF((select ','+P_DESC from [tb_常用字典] where P_FUN_CODE='ylfzflx' 
	   and CHARINDEX(','+P_CODE+',',','+a.医疗费支付类型+',')>0 for xml path('')),1,1,'') end,1,25) AS INSURANCE_TYPE_NAME
	,LEFT(z.医疗保险号,10) as MEDICAL_CARD_NUM
	,LEFT(b.诊断医院,70) as VISIT_ORG_NAME
	,0 as FIR_SYSTOLIC_PRESSURE
	,0 as FIR_DIASTOLIC_PRESSURE
	,case when ISDATE(b.发病时间)=0 then '19000101' else REPLACE(CONVERT(varchar,CONVERT(date,b.发病时间)),'-','') end as FIR_ONSET_DATE
	,'未知' as ONSET_TIME
	,'未知' as VISIT_TIME
	,'未知' as CONFIRM_TIME
	,case LEFT(b.脑卒中分类,1) when '2' then '3' when '3' then '2' 
							   when '7' then '4' else '' end as STROKE_TYPE_CODE
	,isnull((select P_DESC from tb_常用字典 where P_FUN_CODE='nzzfl'and P_CODE=a.脑卒中分类),'未分类脑卒中') as STROKE_TYPE_NAME
	,'' as FLAG_TIA_ATTACK
	,'未知' as SIGN
	,LEFT(a.随访医生建议,200) as TREAT_IDEA
	,'' as DUTY_DR_CODE
	,LEFT(a.随访医生,50) as DUTY_DR_NAME
	,'5' as DRUG_SITUATION_CODE
	,'不详' as DRUG_SITUATION_NAME
	,'9' as NODRUG_REASON_CODE
	,'其他' as NODRUG_REASON_NAME
	,'0' as THE_SYSTOLIC_PRESSURE
	,'0' as THE_DIASTOLIC_PRESSURE
	,'3' as HYPERTENSION_CODE
	,'3' as DIABETES_HISTORY_CODE
	,'3' as HYPERLIPIDEMIA_CODE
	,'3' as CAROTID_STENOSIS_CODE
	,'3' as TIA_HISTORY_CODE
	,'3' as STROKE_HISTORY_CODE
	,'3' as CHD_HISTORY_CODE
	,'3' as OTHER_HEARTD_CODE
	,'' as OTHER_HEARTD_NAMECODE
	,'未知' as OTHER_HEARTD_NAME
	,'' as SMOKE_HISTORY_CODE
	,'' as OVERWEIGHT_HISTORY_CODE
	,'' as DRINK_HISTORY_CODE
	,'' as DRINK_TYPE_CODE
	,'' as DRINK_TYPE_NAME
	,0 as DAILY_ALCOHOL_AMOUT
	,'' as FAMILY_STROKE_CODE
	,'' as OTHER_RISK_CODE
	,'' as OTHER_RISK_NAME
	,'' as STROKE_COMPLICATION_CODE
	,'未知' as STROKE_COMPLICATION_NAME
	,'' as STROKE_RECURSYM_CODE
	,'未知' as STROKE_RECURSYM_NAME
	,'' as STROKE_FIRSYS_CODE
	,'未知' as STROKE_FIRSYS_NAME
	,a.创建人 as FOLLOW_STAFF_CODE
	,(select UserName from tb_MyUser where 用户编码=a.创建人) as FOLLOW_STAFF_NAME
	,(select 机构名称 from tb_机构信息 where 机构编号=a.所属机构) as FOLLOW_ORG_NAME
	,LEFT(REPLACE(a.发生时间,'-',''),8) as FOLLOW_DATE
	,'' as LOST_REASON_CODE
	,'' as CARE_STAFF_CODE
	,'' as CDPF_REG_CODE
	,'' as ASSESS_ORDER
	,'' as ASSESS_TABLE_NAME
	,'' as ASSESS_POSITION_CODE
	,'未知' as ASSESS_POSITION_NAME
	,'' as ASSESS_LEVEL
	,'未知' as ASSESS_LEVEL_NAME
	,'' as ASSESS_SCORE
	,'' as ASSESS_DATE
	,'' as REHABILITATION_CODE
	,'未知' as REHABILITATION_NAME
	,'' as PRETREAT_ORG_NAME
	,'' as REHABILITATION_DATE
	,'' as RECURE_REQUIRE_CODE
	,'未知' as RECURE_REQUIRE_NAME
	,'' as RECURE_PLACE_CODE
	,'未知' as RECURE_PLACE_NAME
	,'' as REQUIRE_PROJECT_NAME
	,'未知' as TREAT_PROJECT_NAME
	,'' as TREAT_FREQUENCY_CODE
	,'未知' as TREAT_FREQUENCY_NAME
	,'' as NOTREAT_REASON_CODE
	,'未知' as NOTREAT_REASON_NAME
	,'' as ACCEPTANCE_CODE
	,'未知' as ACCEPTANCE_NAME
	,'' as UNACCEPT_REASON
	,'' as INSTRUCTION
	,'未知' as TREAT_ORG_NAME
	,'' as RECURE_DR_CODE
	,'未知' as RECURE_DR_NAME
	,'' as TREAT_EFFECT_CODE
	,'未知' as TREAT_EFFECT_NAME
	,'' as LIMB_RECOVER_CODE
	,'未知' as LIMB_RECOVER_NAME
	,'' as LANGUAGE_RECOVER_CODE
	,'未知' as LANGUAGE_RECOVER_NAME
	,'' as COGNITION_RECOVER_CODE
	,'未知' as COGNITION_RECOVER_NAME
	,'' as SELFCARE_RECOVER_CODE
	,'未知' as SELFCARE_RECOVER_NAME
	,(select 机构名称 from tb_机构信息 where 机构编号=a.创建机构) as REPORT_ORG_NAME
	,a.创建人 as REPORT_DR_CODE
	,(select UserName from tb_MyUser where 用户编码=a.创建人) as REPORT_DR_NAME
	,LEFT(REPLACE(a.创建时间,'-',''),8) as REPORT_DATE
	,'' as SELF_RECURE_TIME
	,'' as RECURE_EXERCISE_PROJECT
	,'' as RECURE_EXERCISE_MINUTE
	,'' as SELF_FEEL_CODE
	,'' as SELF_FEEL_NAME
	,'' as DR_RECOGNITION_CODE
	,'' as DR_RECOGNITION_NAME
	,LEFT(a.随访医生建议,400) as DR_ADVISE
	,'' as DR_CODE
	,LEFT(a.随访医生,50) as DR_NAME
	,'' as TRANSIN_CANT_CODE
	,'' as TRANSOUT_CANT_CODE
	,'' as TRANS_REASON_CODE
	,'未知' as TRANS_REASON_NAME
	,'' as STROKE_RECUR_TIME
	,'' as STROKE_PLACE_CODE
	,'未知' as STROKE_PLACE_NAME
	,LEFT(a.转诊科别,70) as TRANSIN_ORG_NAME
	,'' as TRANSOUT_ORG_NAME
	,LEFT(REPLACE(b.终止管理日期,'-',''),8) as STOP_MANAGE_DATE
	,case b.终止理由 when '3' then '4' when '99' then '3'
					 else LEFT(b.终止理由,1) end as STOP_REASON_CODE
	,'' as STOP_REASON_NAME
	,'' as DEATH_TIME
	,'' as DEATH_PLACE_CODE
	,'' as DEATH_DIRECT_CODE
	,'' as DEATH_DIRECT_NAME
	,a.所属机构 as ORG_CODE
	,(select 机构名称 from tb_机构信息 where 机构编号=a.所属机构) as ORG_NAME
	,a.个人档案编号 as CITIZEN_ID
	,'' as EFFECTIVE_TIME
	,'' as STATUS
	,convert(datetime,case when ISDATE(a.创建时间)=0 then '19000101' else a.创建时间 end) AS CREATED_AT
	,LEFT(z.区,6) as DISTRICT_CODE
	,'INSERT' as EXCHANGE_TYPE
	,'' as DEI
	,'未知' as NODE_CODE
	,GETDATE() as SYSTEM_TIME
	,case when DATALENGTH(z.身份证号)>18 then '' else z.身份证号 end AS CZRK_ID
from tb_MXB脑卒中随访表 a
left join tb_MXB脑卒中管理卡 b on a.个人档案编号=b.个人档案编号
left join tb_健康档案 z on a.个人档案编号=z.个人档案编号
where 1=1
	and a.创建时间>=DATEADD(day,-1,getdate())
	and a.创建时间<getdate()

--not exists
--(select 1 from tb_上传日志 b where (a.个人档案编号+convert(varchar,a.ID)) = b.D001 and 功能号='EHR_STROKE_PATIENT_MANAGE')