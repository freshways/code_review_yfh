/* 2.6.1产后42天检查记录表（C-12标） - EHR_POSTPARTUM_INSPECT */

select
LEFT(a.个人档案编号+CONVERT(varchar(10),a.ID),36) as ID
,'' as CHPE_CARDID_GB
,RIGHT(a.个人档案编号,17) as HEALTH_RECORD_CODE
,LEFT(b.姓名,50) as NAME_GB
,LEFT(REPLACE(a.随访日期,'-',''),8) as VISIT_DATE_GB
,LEFT(a.一般健康情况,100) as GENERAL_HEALTH_STATUS
,LEFT(a.一般心理状况,100) as GENERAL_MIND_STATUS
,case when ISNUMERIC(a.血压1)=0 then '0'
	  when LEN(a.血压1)>3		then '0'
	  else a.血压1 end as 收缩压
,case when ISNUMERIC(a.血压2)=0 then '0'
	  when LEN(a.血压2)>3		then '0'
	  else a.血压2 end as 舒张压
,case when a.乳房='1' then '1' else '9' end as 左侧乳腺检查结果代码
,case when a.乳房='1' then '1' else '9' end as 右侧乳腺检查结果代码
,case when a.子宫='1' then '0' else '1' end as 宫体异常标志
,LEFT(a.子宫异常,100) as 宫体异常描述
,case when a.恶露='1' then '0' else '1' end as 恶露异常标志
,LEFT(a.恶露异常,100) as 恶露状况
,case when a.伤口='1' then '1' else '4' end as 伤口愈合状况代码
,case when a.分类='1' then '1' else '0' end as 产妇恢复标志
,LEFT(a.分类异常,100) as 产妇健康状况评估描述
,case a.指导 when '1' then '2'	--值域与文档字段长度自我矛盾
			 when '2' then '0'
			 when '3' then '0'
			 when '4' then '3'
			 when '99' then '0'
			 else '0' end as 孕产妇健康指导类别代码
,'1' as 结案标志
,'0' as 转诊标志
,LEFT(a.转诊原因,100) as 转诊原因
,LEFT(a.机构科室,70) as 转入医疗机构名称
,'' as 转出机构科室名称
,LEFT(a.随访医生签名,50) as 访视医师姓名
,case when a.乳房='1' then '1' else '9' end as 乳房检查结果
,case when a.子宫='1' then '0' else '1' end as 子宫异常标志
,LEFT(a.子宫异常,100) as 子宫异常描述
,LEFT(a.伤口异常,100) as 伤口愈合状况异常描述
,case b.证件类型 when '1' then '1'
				 when '2' then '3'
				 when '3' then '4'
				 else '0' end as 身份证件类别代码
,case when DATALENGTH(b.身份证号)>18 then '' else b.身份证号 end as 身份证件号码
,LEFT((select 机构名称 from tb_机构信息 c where c.机构编号=a.所属机构),70) as 组织机构名称
,RIGHT(a.所属机构,10) as 组织机构代码
,'' as 特殊情况记录
,'' as 心律
,0.0 as 体重
,case when ISNULL(a.乳房,'')!='' then '1' else '2' end as 乳房检查标志
,LEFT(a.乳房异常,100) as 乳房检查异常结果描述
,0 as 乳汁量
,LEFT(a.其他,300) as 其他妇科检查
,0.0 as 红细胞计数值
,0 as 血红蛋白值
,'' as 血小板计数值
,'' as 白细胞分类结果
,0.0 as 白细胞计数值
,0.0 as 尿糖定量检测
,0.0 as 尿液酸碱度
,0.0 as 尿比重
,0.0 as 尿蛋白定量检测值
,0.0 as 产后天数
,'0' as 孕产妇健康评估异常标志
,'' as 产妇结论
,'' as 婴儿结论
,'' as 转出机构科室代码
,LEFT(REPLACE(a.随访日期,'-',''),8) as 预约产后检查日期
,'' as 下次随访日期
,LEFT(b.区,6) as 所属行政区划
,LEFT(a.机构科室,70) as 转入科室名称
from tb_孕妇_产后42天检查记录 a left join tb_健康档案 b on a.个人档案编号=b.个人档案编号
where LEFT(a.个人档案编号+CONVERT(varchar(10),a.ID),36) not in
(select D001 from tb_上传日志 where 功能号='EHR_POSTPARTUM_INSPECT')