
/*既往疾病史(辅C-1标) - EHR_PIR_DISEASE*/
SELECT  a.个人档案编号+convert(varchar,a.[ID]) as ID
	  ,a.[ID] as 个人基本信息内码
      ,RIGHT(a.[个人档案编号],17) AS 个人档案编号
	  ,case when ISNULL(d.疾病名称,'')='' then '13' else replace(substring(d.疾病名称,1,2),',','') end as 疾病类别代码
      ,(select P_DESC from tb_常用字典 where P_FUN_CODE='jb_gren' and P_CODE=case when ISNULL(d.疾病名称,'')='' then '13' else replace(substring(d.疾病名称,1,2),',','') end) as 疾病名称
	  , replace(d.日期,'-','') as 确诊时间
	  ,convert(date,case when ISDATE(a.创建时间)=0 then '19000101' else a.创建时间 end) AS 创建时间
	  ,left(a.个人档案编号,6) as 所属行政区划
      ,'INSERT' as 交换类型 
	  ,getdate() as 数据上传时间戳
      ,a.创建机构 AS 建档机构代码
	  ,ISNULL((SELECT 机构名称 FROM tb_机构信息 b WHERE a.创建机构=b.机构编号),'不祥') AS 建档机构名称 
  FROM tb_健康档案 a 
  left join tb_健康档案_健康状态 c on a.个人档案编号=c.个人档案编号  
  left join tb_健康档案_既往病史 d on a.个人档案编号=d.个人档案编号
  left join tb_健康档案_家族病史 e on a.个人档案编号=e.个人档案编号
  WHERE 1=1
  and d.疾病类型='疾病'
  and a.创建时间>=DATEADD(day,-1,getdate())
  and a.创建时间<getdate()