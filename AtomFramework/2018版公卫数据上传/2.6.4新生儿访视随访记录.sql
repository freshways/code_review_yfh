/* 2.6.4新生儿访视随访记录（C-13 标） - EHR_NABV */

SELECT a.个人档案编号+convert(varchar,a.[ID]) as ID
      ,RIGHT(a.[个人档案编号],17) AS HEALTH_RECORD_CODE
	,b.姓名 as NAME
	,a.ID as RECORD_NO
	,b.性别 as GENDER_CD
	,case when ISDATE(b.出生日期)=0 then '19000101' else convert(varchar,convert(date,b.出生日期),112) end BIRTH_DATE_TS
	,b.证件类型 as ID_NO_CD
	,'居民身份证' as ID_NO_NM
	,b.身份证号 as ID_NO 
	,case when ISDATE(c.父亲出生日期)=0 then '19000101' else convert(varchar,convert(datetime,c.父亲出生日期),112) end FATHER_BIRTH_DATE
	,substring(replace(a.父亲联系电话,'.',''),1,15) as TEL_FATHER
	,c.父亲姓名 as NAME_FATHER
	,case when isnull(a.父亲身份证号,'')='' then '未知' else a.父亲身份证号 end FA_ID_NUM
	,'' as OCCUPATION_FATHER_STYPE
	,case when isnull(a.父亲职业,'')='' then '99' when len(a.父亲职业)>2 then 99 when ISNUMERIC(a.父亲职业)=0 then 99 else a.父亲职业 end OCCUPATION_FATHER_NAME	
	,case when isdate(c.母亲出生日期)=0 then '19000101' else convert(varchar,convert(datetime,c.母亲出生日期),112) end  MOTHER_BIRTH_DATE
	,c.母亲联系电话 as TEL_MOTHER
	,c.母亲姓名 as NAME_MOTHER
	,c.母亲身份证号 as MO_ID_NUM
	,'' as OCCUPATION_MOTHER_TYPE
	,c.母亲工作单位 as OCCUPATION_MOTHER_NAME
	,case when ISNUMERIC(a.出生孕周)=0 then 40 else substring(replace(a.出生孕周,'.',''),1,2) end BIRTH_PREG_WEEKS
	,case when ISNULL(a.妊娠期患病疾病情况,'')='' then '9' else replace(substring(a.妊娠期患病疾病情况,1,1),',','') end PREGNANCY_ILLNESS_CODE
	,(SELECT TOP 1 [P_DESC] FROM [tb_常用字典] where P_FUN_CODE = 'rcqhbqk' and P_CODE =replace(a.妊娠期患病疾病情况,',','')) as PREGNANCY_ILLNESS_NAME
	,a.助产机构名称 as DE_ORG_NAME
	,case when ISNULL(a.出生情况,'')='' then '9' when substring(replace(a.出生情况,',',''),1,2)='99' then '99' else substring(replace(a.出生情况,',',''),1,1) end DE_WAY_CD
	,case when substring(replace(a.新生儿窒息,',',''),1,1)=1 then 1 else 0 end SUFFOCATION
	,case when ISNUMERIC(a.Apgar评分)=0 then 1 else SUBSTRING(a.Apgar评分,1,1) end APGAR_VALUE	
	,case when a.是否有畸型=1 then '1' else '0' end  NEO_ABNORMAL_MARK
	,a.是否有畸型其他 as NEO_ABNORMAL_DESCR
	,case when isnumeric(a.新生儿听力筛查)=0 then '1' when a.新生儿听力筛查=1 then '1' else '2' end LEFT_AUDI_TEST_RESULT
	,case when REPLACE(SUBSTRING(a.新生儿疾病筛查,1,2),',','')=5 then 9 else REPLACE(SUBSTRING(a.新生儿疾病筛查,1,1),',','')end  NEO_DS_RESULT
	,a.新生儿疾病筛查其他 as NEO_DS_DESC
	,case when ISNUMERIC(a.新生儿出生体重)=0 then 3 when len(a.新生儿出生体重)-LEN(replace(a.新生儿出生体重,'.',''))=0 and LEN(a.新生儿出生体重)>1 then 3 else convert(int,round(a.新生儿出生体重,0)) end BIRTH_BODY_WEIGHT
	,case when ISNUMERIC(a.目前体重)=0 then 4.5 when len(a.目前体重)-LEN(replace(a.目前体重,'.',''))=0 and LEN(a.目前体重)>2 then 4.5 else convert(decimal(5,2),round(a.目前体重,2)) end  WEIGHT
	,case when ISNUMERIC(a.出生身长)=0 then 50 when len(a.出生身长)-LEN(replace(a.出生身长,'.',''))=0 and LEN(a.出生身长)>2 then 50 else convert(decimal(4,1),round(a.出生身长,1)) end BIRTH_BODY_LENGTH
	,case when ISNUMERIC(a.喂养方式)=0 then 1 else substring(a.喂养方式,1,1) end  FEED_WAY_CD
	,'母乳喂养' as FEED_WAY_NM
	,case when ISNUMERIC(a.吃奶量)=0 then 0 else convert(int,round(a.吃奶量,0)) end NEO_MILK_AMOUNT
	,case when ISNUMERIC(a.吃奶次数)=0 then 0 else CONVERT(dec,a.吃奶次数) end NEO_MILK_TIMES
	,case when ISNUMERIC(a.呕吐)=0 and a.呕吐=1 then 1 else 0 end  SPEW_MARK
	,case when ISNUMERIC(a.大便)=0 then 1 else a.大便 end  NEO_STOOL_RECORD
	,'' as NEO_STOOL_RECORD_DESC
	,case when ISNUMERIC(a.大便次数)=0 then 0 else convert(int,round(substring(a.大便次数,1,2),0)) end NEO_STOOL_TIMES
	,case when isnumeric(a.体温)=0 then '0' else convert(dec,a.体温) end NEO_BODY_TEMP
	,case when ISNUMERIC(a.呼吸频率)=0 then 40 when ISNULL(a.呼吸频率,'')='' then 40 WHEN LEN(a.呼吸频率)>3 then 40 else convert(int,round(a.呼吸频率,0)) end BREATHE_FREQ
	,case when ISNUMERIC(a.脉率)=0 then 134 when CHARINDEX ('.', 脉率)>0 then 134 when isnull(a.脉率,'')='' then 134 when LEN(a.脉率)>3 then 134 else convert(int,round(a.脉率,0)) end PR
	,case when ISNUMERIC(replace(substring(a.面色,1,1),',',''))=0 then '9' else replace(substring(a.面色,1,1),',','') end COMPLEXION_TEST_RESULT
	,'' as COMPLEXION_TEST_DESC
	,a.面色其他 as OTHER_COMPLEXION
	,case when (ISNUMERIC(a.黄疸部位)=0 or a.黄疸部位='99') then '9' else a.黄疸部位 end JAUN_PART
	,'其他' as JAUN_PART_NAME
	,case when isnumeric(a.前囟1)=0 then '0' else CONVERT(dec,前囟1) end  BREGAM_TRANS_DIA
	,case when isnumeric(a.前囟2)=0 then '0' else CONVERT(dec,前囟2) end  BREGAM_VERT_DIA
	,case when a.前囟状况=4 or a.前囟状况=0 then '9' else a.前囟状况 end  BREGAM_TENSION
	,'其他' as BREGAM_TENSION_NAME
	,case when a.前囟状况=4 or a.前囟状况=0 then '9' else a.前囟状况 end  BREGAM
	,'其他' as BREGAM_DESC
	,substring(a.前囟状况其他,1,20) as OTHER_BREGAM
	,case when isnumeric(a.眼外观)=1 and a.眼外观=1 then '1' else '0' end  EYE_TF
	,a.眼外观其他 as EYE_ASPECT_TEST_RESULT
	,case when(a.四肢活动度)=1 then '1' else '0' end  LIMBS_TF
	,a.四肢活动度其他 as LIMBS_TEST_RESULT
	,case when(a.耳外观)=1 then '1' else '0' end  EAR_TF
	,a.耳外观其他 as EAR_TEST_RESULT
	,case when a.颈部包块=1 then '1' else '0' end  NECK_MASS_TF
	,a.颈部包块其他 as NECK_MASS_TEST_RESULT
	,case when(a.鼻)=1 then '1' else '0' end  NOSE_TF
	,a.鼻其他 as NOSE_TEST_RESULT 
	,case when ISNULL(a.皮肤,'')='' then '99' else replace(substring(a.皮肤,1,2),',','') end  SKIN_TEST_RESULT
	,a.皮肤其他 as OTHER_SKIN_RESULT
	,case when(a.口腔)=1 then '1' else '0' end  ORAL_CAVITY_TF
	,a.口腔其他 as ORAL_CAVITY_TEST_RESULT
	,case when(a.肛门)=1 then '1' else '0' end  ANUS_TF
	,a.肛门其他 as ANUS_TEST_RESULT
	,case when(a.心肺)=1 then '1' else '0' end  NEO_HEART_AUSC_TF
	,a.心肺其他 as NEO_HEART_AUSC_RESULT 
	,case when(a.心肺)=1 then '1' else '0' end  NEO_LUNG_AUSC_TF 
	,a.心肺其他 as NEO_LUNG_AUSC_RESULT
	,case when(a.腹部)=1 then '1' else '0' end  BELLY_TF
	,a.腹部其他 as BELLY_TEST_RESULT
	,case when(a.外生殖器官)=1 then '1' else '0' end  EXTERNALIA_TF
	,a.外生殖器官其他 as EXTERNALIA_TEST_RESULT
	,case when(a.脊柱)=1 then '1' else '0' end  BACKBONE_TF
	,a.脊柱其他 as BACKBONE_TEST_RESULT		
	,case when a.脐带=4 or a.脐带=0 then '9' when isnull(a.脐带,'')='' then '9' else substring(a.脐带,1,1) end  UMBILICAL_TEST_RESULT
	,a.脐带其他 as UMBILICAL_TEST_DESC
	,case when a.转诊=1 then '1' else '0' end  TRANSFER_TF
	,a.转诊机构及科室 as TRANSFER_ORG
	,isnull(a.转诊机构及科室,'未知') as TRANSFER_DEPT
	,a.转诊原因 as TRANSFER_REASON
	,case when ISDATE(a.发生时间)=0 then '19000101' else convert(varchar,convert(date,a.发生时间),112) end  FOLLOWUP_DATE
	,case when ISDATE(a.下次随访日期)=0 then '19000101' else convert(varchar,convert(datetime,a.下次随访日期),112) end NEXT_FOLLOWUP_DATE
	,a.下次随访地点 as MOVE_INTO_DATE
	,a.随访医生签名 as FOLLOWUP_EMPLOY_CODE
	,case when isnull(a.随访医生签名,'')='' then '''''' else a.随访医生签名 end  FOLLOWUP_EMPLOY
	,case when replace(substring(a.指导,1,2),',','')=99 then '9' when ISNULL(replace(substring(a.指导,1,2),',',''),'')='' then '9' else replace(substring(a.指导,1,2),',','') end  TR_OPINION_CD
	,(select c.机构名称 from tb_机构信息 c where c.机构编号 =a.创建机构) as ORG_NAME
	,a.创建机构 as ORG_CODE
	,left(a.个人档案编号,6) as DISTRICT_CODE
	,'INSERT' as EXCHANGE_TYPE
	,getdate() as SYSTEM_TIME	
  FROM tb_儿童新生儿访视记录 a
  inner join tb_健康档案 b on b.个人档案编号=a.个人档案编号
  left join tb_儿童基本信息 c on c.个人档案编号=a.个人档案编号
  where 1=1
	 and a.创建时间>=DATEADD(day,-1,getdate())
	 and a.创建时间<getdate()

--    and not exists(
--select 1 from tb_上传日志 rz where (a.个人档案编号+convert(varchar,a.[ID])) = rz.D001 and 功能号='EHR_NABV')
