/*健康体检-主要用药情况（辅C-3标） - EHR_PE_DRUG_USE*/

SELECT a.个人档案编号+convert(varchar,a.ID) as ID
	  ,RIGHT(a.个人档案编号,17) as EXAM_ID
	  ,'1' as CHINESE_MEDICINE_CODE
      ,a.药物名称 as DRUG_NAME
      ,a.用法 as USAGE
      ,replace(a.用量,'''','') as DOSAGE
      ,a.用药时间 as DRUG_USE_TIME
      ,case when isnull(a.服药依从性,'')='' then '3' else a.服药依从性 end DRUG_COMPLIANCE_CODE
	  ,(select P_DESC from tb_常用字典 where P_FUN_CODE='fyycx-mb' and P_CODE=a.服药依从性) AS DRUG_COMPLIANCE
	  ,left(a.个人档案编号,6) as DISTRICT_CODE
      , convert(datetime,case when ISDATE(a.创建时间)=0 then '19000101' else a.创建时间 end) AS CREATED_AT
	  ,a.ID as SN
	  ,'INSERT' as EXCHANGE_TYPE
	  ,replace(substring(a.用法,1,10),',','') as USING_DRUG_FREQ
	  ,case when len(a.用量)>3 then replace(SUBSTRING(a.用量,1,3),',','') else a.用量 end DOSE_UNIT
	  ,'0.0' as DOSE_SINGLE
      ,'未知' as VACCINE_BATCH_NUM
	  ,'1' as DRUG_WAY_CD
	  ,'口服' as DRUG_WAY
	  ,getdate() as SYSTEM_TIME
	  ,ISNULL((SELECT 机构名称 FROM tb_机构信息 c WHERE c.机构编号=b.创建机构),'未知') AS ORG_NAME 
      ,b.创建机构 AS ORG_CODE
  FROM tb_健康体检_用药情况表 a
  left join tb_健康体检 b on b.个人档案编号=a.个人档案编号 and a.创建时间 = b.创建时间
  where 1=1
  and isnull(a.药物名称,'')<>''
  and a.创建时间>=DATEADD(day,-1,getdate())
  and a.创建时间<getdate()