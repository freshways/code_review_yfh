/* 9.2高血压近期药物治疗情况 - EHR_HBP_CARD_DRUG */

SELECT Y_ID AS ID
      ,a.管理卡编号 AS CARD_ID
      ,ID AS SN  
      ,药物名称 AS DRUG_NAME
      ,用法 AS USAGE
	  ,left(a.个人档案编号,6) as DISTRICT_CODE  
      ,convert(datetime,case when isdate(b.创建时间)<>'1' then '1999-01-01' else b.创建时间 end) as CREATED_AT
      ,'INSERT' as EXCHANGE_TYPE
	  ,GETDATE() as SYSTEM_TIME
  FROM tb_MXB高血压管理卡_用药情况 b
  inner join tb_MXB高血压管理卡 a on a.个人档案编号 = b.个人档案编号
  where ISNULL(a.管理卡编号,'')<>''
  and a.创建时间>=convert(varchar,DATEADD(day,-1,getdate()),120)
  and a.创建时间<convert(varchar,getdate(),120)
  --and  not exists(
  --select 1 from tb_上传日志 rz where Y_ID = rz.D001 and rz.功能号='EHR_HBP_CARD_DRUG' )

