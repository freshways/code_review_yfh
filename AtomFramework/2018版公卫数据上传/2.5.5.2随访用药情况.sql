/* 2.5.5.2随访用药情况（C-18 辅标） - EHR_MENTAL_MEDICATION */

SELECT  a.个人档案编号+convert(varchar,c.C_JRID)+convert(varchar,c.ID) as ID
		,a.个人档案编号+convert(varchar,a.[ID]) as VISIT_ID
		,'1' as DRUE_TYPE_CODE
      ,replace(c.[药物名称],'<','[') as DRUG_NAME
	  ,replace(substring(c.用法,1,10),',','') as USING_DRUG_FREQ
	  ,0.0 as DOSE_TOTAL
      ,0.0 as DOSE_SINGLE
	  ,'mg' as DOSE_UNIT
	  ,'9' as DRUG_WAY_CD
	  ,'其他用药途径' as DRUGS_USE_METHOD_NAME
	  ,convert(datetime,case when isdate(a.创建时间)=0 then '19000101' else a.创建时间 end) as CREATED_AT
	  ,left(b.区,6) as DISTRICT_CODE
	  ,'INSERT' as EXCHANGE_TYPE
	  ,GETDATE() as SYSTEM_TIME
	  ,a.所属机构 as ORG_CODE
	  ,LEFT((select 机构名称 from tb_机构信息 c where c.机构编号=a.所属机构),70) as ORG_NAME
  FROM [dbo].[tb_精神病_用药情况] c
 left join  tb_精神病随访记录 a on c.C_JRID=a.ID
 left join tb_健康档案 b on b.个人档案编号 = a.个人档案编号
  WHERE 1=1
	and ISNULL (c.C_JRID,'')!=''
	and isnull (a.个人档案编号,'')!=''
	and a.创建时间>=DATEADD(day,-1,getdate())
	and a.创建时间<getdate()

--     and not exists(
--select 1 from tb_上传日志 rz where (a.个人档案编号+convert(varchar,c.C_JRID)+convert(varchar,c.ID)) = rz.D001 and 功能号='EHR_MENTAL_MEDICATION')