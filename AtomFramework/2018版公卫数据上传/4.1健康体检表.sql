/* 4.1��������(C-3) - EHR_HEALTH_PHYSICAL */

SELECT a.���˵������+convert(varchar,a.ID) as ID
      ,(SELECT �������� FROM tb_������Ϣ b WHERE a.��������=b.�������) AS ORG_NAME
      ,a.�������� AS ORG_CODE  
	  ,left(a.���˵������,6) as DISTRICT_CODE  
	  ,'INSERT' as EXCHANGE_TYPE
	  ,GETDATE() as SYSTEM_TIME
	  ,RIGHT(a.���˵������,17) AS HEALTH_ARCHIVES_CODE
      ,a.ID AS HEALTH_TABLE_NUM
      ,b.���� AS NAME
      ,FIELD2 AS DOCTOR_NAME  
      ,case when ISDATE(a.�������)=0 then '19000101' else REPLACE(CONVERT(varchar(10),convert(date,�������),120),'-','') end PHYSICAL_DATE
      ,SUBSTRING(֢״,1,1) AS SYMPTOM_CODE
	  ,֢״���� as SYMPTOM_NAME
      --,(select P_DESC from tb_�����ֵ� where P_FUN_CODE='zz_zhengzhuang' and P_CODE=a.֢״) AS ֢״���� 
      ,case when ISNUMERIC(a.����)=0 then '0' when CHARINDEX('.',a.����)>4 then '0' else CONVERT(decimal(4,1),a.����) end TEMPERATURE
      ,case when ISNUMERIC(a.����)=0 then '0' else CONVERT(int,a.����) end PULSE_RATE
      ,case when ISNUMERIC(a.����)=0 then '0' else CONVERT(int,a.����) end RESPIRATORY_RATE 
      ,case when isnumeric(a.Ѫѹ���1)=0 then '0' when charindex('.',a.Ѫѹ���1)>0 then '0' else CONVERT(int,a.Ѫѹ���1) end LEFT_SHRINK_PRESSURE
      ,case when isnumeric(a.Ѫѹ���2)=0 then '0' when charindex('.',a.Ѫѹ���2)>0 then '0' else CONVERT(int,round(a.Ѫѹ���2,0)) end LEFT_DIASTOLE_PRESSURE           
      ,case when isnumeric(a.Ѫѹ�Ҳ�1)=0 then '0' when charindex('.',a.Ѫѹ�Ҳ�1)>0 then '0' else CONVERT(int,a.Ѫѹ�Ҳ�1) end RIGHT_SHRINK_PRESSURE
      ,case when isnumeric(a.Ѫѹ�Ҳ�2)=0 then '0' when charindex('.',a.Ѫѹ�Ҳ�2)>0 then '0' else CONVERT(int,a.Ѫѹ�Ҳ�2) end RIGHT_DIASTOLE_PRESSURE
      ,case when isnumeric(a.����)=0 then '0' when CHARINDEX('.',a.����)>5 then '0' else CONVERT(decimal(5,1),a.����) end as HEIGHT
      ,case when isnumeric(a.����)=0 then '0' when CHARINDEX('.',a.����)>5 then '0' else CONVERT(decimal(5,1),a.����) end as WEIGTH
      ,case when isnumeric(a.��Χ)=0 then '0' when CHARINDEX('.',a.��Χ)>5 then '0' else CONVERT(decimal(5,1),a.��Χ) end as WAIST
      ,case when isnumeric(a.����ָ��)=0 then '0' 
		when charindex('.',a.����ָ��)>3 then '0' 
		when charindex('.',a.����ָ��)=0 then '0'
		when CHARINDEX('-',a.����ָ��)>0 then '0'
		when a.����ָ��='.' then '0'
		else CONVERT(decimal(5,2),a.����ָ��) end BODY_MASS_INDEX
      ,case when ISNUMERIC(a.������״������)=0 then '3' else substring(a.������״������,1,1) end OLD_HEALTH_CODE
	  ,'����' as OLD_HEALTH
      ,case when ISNUMERIC(a.��������������)=0 then '1' else substring(a.��������������,1,1) end OLD_THEMSELVES_CODE
	  ,'������' as OLD_THEMSELVES
      ,case when ISNUMERIC(a.��������֪)=0 then '1' else substring(a.��������֪,1,3) end OLD_COGNITION_CODE
	  ,'��ɸ����' as OLD_COGNITION
      ,case when ISNUMERIC(a.��������֪��)=0 then '0' when charindex('.',a.��������֪��)>0 then '0'  else CONVERT(int,a.��������֪��) end OLD_COGNITION_SCORE
	  ,'��ɸ����' as OLD_EMOTION
      ,case when ISNUMERIC(a.���������)=0 then '1' else substring(a.���������,1,1) end OLD_EMOTION_CODE
      ,case when ISNUMERIC(a.��������з�)=0 then '0' when charindex('.',a.��������з�)>0 then '0' else CONVERT(int,a.��������з�) end OLD_DEPRESSED_SCORE
      ,case when ISNUMERIC(a.����Ƶ��)=0 then '4' else substring(a.����Ƶ��,1,2) end STEEL_FREQUENCY_CODE
	  ,'���˶�' as STEEL_FREQUENCY
      ,case when ISNUMERIC(a.ÿ�ζ���ʱ��)=0 then '0' when charindex('.',a.ÿ�ζ���ʱ��)>0 then '0' else CONVERT(int,a.ÿ�ζ���ʱ��) end MOTION_WHEN_LONG
      ,case when ISNUMERIC(a.��ֶ���ʱ��)=0 then '0' else CONVERT(int,round(convert(float,a.��ֶ���ʱ��)*12,0)) end STEEL_MONTH
      ,������ʽ AS STEEL_TYPE_REMARKS
      ,case when ISNULL(a.��ʳϰ��,'')='' then '1' else substring(��ʳϰ��,1,1) end DITE_HABIT_CODE
	  ,'���ؾ���' as DITE_HABIT
      ,case when ISNULL(a.����״��,'')='' then '1' else substring(a.����״��,1,1) end SMOKE_STATUS_CODE
	  ,case when ISNULL(a.����״��,'')='' then '�Ӳ�����' else '����' end SMOKE_STATUS
      ,case when ISNULL(a.��������,'')='' then '0' when len(��������)>3 then '0' else �������� end SMOKE_COUNT_DAY
      ,case when ISNULL(a.��ʼ��������,'')='' then '0' when len(��ʼ��������)>2 then '0' else ��ʼ�������� end BEGIN_SMOKE_AGE
      ,case when ISNULL(a.��������,'')='' then '0' when len(��������)>2 then '0' else �������� end QUIT_SMOKE_AGE
      ,case when ISNULL(a.����Ƶ��,'')='' then '1' else ����Ƶ�� end as DRINK_FREQUENCY
	  ,case when ISNULL(a.����Ƶ��,'')='' then '�Ӳ�' else 'ż��' end as DRINK_FREQUENCY_NAME
      ,case when ISNULL(a.��������,'')='' then '0' when len(��������)>3 then '0' else �������� end DRINK_AMOUNT
      ,case when ISNULL(a.�Ƿ���,'')='' then '1' else a.�Ƿ��� end QUIT_DRINK_MARK
      ,case when ISNULL(a.�������,'')='' then '0' when len(�������)>2 then '0' else a.������� end QUIT_DRINK_AGE
      ,case when ISNULL(a.��ʼ��������,'')='' then '0' when len(��ʼ��������)>2 then '0' else a.��ʼ�������� end START_DRINK_AGE
      ,case when isnull(a.��һ�����Ƿ�������,'')='' then '2' when a.��һ�����Ƿ�������='��' then '2' when a.��һ�����Ƿ�������='��' then '1' else SUBSTRING(a.��һ�����Ƿ�������,1,1) end DRUNK_MARK
      ,case when isnull(a.��������,'')='' then '9' else replace(SUBSTRING(��������,1,2),',','') end DRINK_KIND_CODE
	  ,'����' as DRINK_KIND
      ,����ְҵ�� AS OCCUPATION_MARK
      ,'' AS HARM_OCCUPATION_REMARKS
      ,'0' AS HARM_OCCUPATION_TIME
      ,'9' AS HARM_OCCUPATION_CODE
      ,'����' AS HARM_OCCUPATION_NAME
      ,'1' AS OCCUPATION_PROJECT_MARK
      ,�ڴ� AS LIPS_TEST_CODE
	  ,case when �ڴ�='1' then '����' else '����' end  LIPS_TEST_NAME
      ,case when substring(����,1,1)='1' then '1' else '2' end DENTITION_CODE
	  ,case when substring(����,1,1)='1' then '����' else 'ȱ��' end DENTITION
      ,����ȱ�� AS DENTITION_REMARKS
      ,case when ISNUMERIC(a.�ʲ�)=0 then '1' else a.�ʲ� end PHARYNX_TEST_CODE
	  ,case when ISNUMERIC(a.�ʲ�)=0 then '�޳�Ѫ' else '��Ѫ' end PHARYNX_TEST
      ,cast(case when isnumeric(��������)=0 then '0' when CHARINDEX('.',��������)>3 then '0' else �������� end as dec(3,1)) AS EYE_LEFT_VALUE
      ,cast(case when isnumeric(��������)=0 then '0' when CHARINDEX('.',��������)>3 then '0' else �������� end as dec(3,1)) AS EYE_RIGHT_VALUE
      ,case when isnumeric(a.���۽���)=0 then '0' when CHARINDEX('.',a.���۽���)>3 then '0' else CONVERT(decimal(3,1),a.���۽���) end AS EYE_LEFT_CORRECT_VALUE
      ,case when isnumeric(a.���۽���)=0 then '0' when CHARINDEX('.',a.���۽���)>3 then '0' else CONVERT(decimal(3,1),a.���۽���) end AS EYE_RIGHT_CORRECT_VALUE
      ,case when substring(����,1,1)='1' then '1' else '2' end HEARING_TEST_CODE
	  ,case when substring(����,1,1)='1' then '����' else '��������޷�����' end HEARING_TEST
      ,case when substring(�˶�����,1,1)='1' then '1' else '2' end MOTION_STATE_CODE
	  ,case when substring(�˶�����,1,1)='1' then '��˳�����' else '�޷�������������κ�һ������' end MOTION_STATE
      ,case when substring(a.�۵��쳣,1,1)='1' then '1' else '2' end EYE_ABNORMAL_MARK
      ,a.�۵� AS EYE_ABNORMAL_REMARKS
      ,case when isnull(a.Ƥ��,'')='' then '1' else replace(SUBSTRING(a.Ƥ��,1,2),',','') end SKIN_TEST_CODE
	  ,case when isnull(a.Ƥ��,'')='' then 'δ���쳣' else '����' end SKIN_TEST
      ,case when isnull(a.��Ĥ,'')='' then '1' else SUBSTRING(a.��Ĥ,1,1) end SCLERA_TEST_CODE
	  ,case when isnull(a.��Ĥ,'')='' then '����' else '����' end SCLERA_TEST
      ,case when isnull(a.�ܰͽ�,'')='' then '1' else SUBSTRING(a.�ܰͽ�,1,1) end GLAND_TEST_CODE
	  ,case when isnull(a.�ܰͽ�,'')='' then 'δ����' else '����' end GLAND_TEST
      ,case when isnull(a.Ͱ״��,'')='' then '0' else SUBSTRING(a.Ͱ״��,1,1) end BARREL_CHEST_MARK
      ,case when isnull(a.������,'')='' then '1' else SUBSTRING(a.������,1,1) end LUNGS_BREATH_MARK 
      ,'' AS LUNGS_BREATH_REMARKS
      ,case when substring(a.����,1,1)='1' then '1' else '2' end LUNGS_RALE_MARK
      ,'' AS LUNGS_RALE_REMARKS
      ,cast(case when isnumeric(����)=0 then '0' when len(����)>3 then '0' else ���� end as dec(3,0)) AS HEART_RATE
      ,case when isnumeric(a.����)=0 then '1' else convert(int,substring(a.����,1,1)) end HEART_RATE_CODE
	  ,case when a.����='1' then '������' else '���ɲ���' end HEART_RATE_NAME
      ,case when substring(a.����,1,1)='1' then '1' else '2' end HEART_NOISE_MARK
      ,'' AS HEART_NOISE_RMARKS
      ,case when substring(ѹʹ,1,1)='1' then '1' else '2' end ABDOMEN_TENDERNESS_MARK
      ,'' AS ABDOMEN_TENDERNESS_REMARKS
      ,case when SUBSTRING(����,1,1)='1' then '1' else '2' end ABDOMEN_PIECE_MARK
      ,'' AS ABDOMEN_PIECE_REMARKS
      ,case when substring(a.�δ�,1,1)='1' then '1' else '2' end LIVER_BIG_MARK
      ,'' AS LIVER_BIG_REMARKS
      ,case when substring(a.Ƣ��,1,1)='1' then '1' else '2' end SPLEEN_BIG_MARK
      ,'' AS SPLEEN_BIG_REMARKS
      ,case when substring(a.����,1,1)='1' then '1' else '2' end ABDOMEN_MOVE_MARK
      ,'' AS ABDOMEN_MOVE_REMARKS
      ,case when isnumeric(a.��֫ˮ��)=0 then '1' else convert(int,substring(a.��֫ˮ��,1,1)) end LEGS_EDEMA_CODE
	  ,'' as LEGS_EDEMA
      ,'1' AS FOOT_DORSUM_CODE 
	  ,'' as FOOT_DORSUM
      ,'1' AS ANUS_TEST_CODE
	  ,'' as ANUS_TEST
      ,���� AS BREAST_TEST_CODE
	  ,�������� as BREAST_TEST
      ,'2' AS VULVA_MARK
      ,�����쳣 AS VULVA_REMARKS
      ,'2' AS VAGINA_MARK
      ,�����쳣 AS VAGINA_REMARKS
      ,'2' AS CERVICAL_MARK
      ,�����쳣 AS CERVICAL_REMARKS
      ,'2' AS PALACE_MARK
      ,�����쳣 AS PALACE_REMARKS
      ,'2' AS REVIEW_MAK
      ,�����쳣 AS REVIEW_REMARKS
      ,�������� AS OTHER_TEST_RESULT  
      ,cast(case when isnumeric(Ѫ�쵰��)=0 then '0' when len(a.Ѫ�쵰��)>3 then '0' when CHARINDEX('.',Ѫ�쵰��)=0 and LEN(Ѫ�쵰��)>3 then '0' else Ѫ�쵰�� end as dec(3,0)) AS HEMOGLOBIN_VALUE
      ,cast(case when isnumeric(��ϸ��)=0 then '0' when CHARINDEX('.',a.��ϸ��)>4 then '0' when CHARINDEX('.',��ϸ��)=0 and LEN(��ϸ��)>3 then '0' else ��ϸ�� end as dec(4,1)) AS WHITE_BLOOD_VALUE
      ,cast(case when isnumeric(ѪС��)=0 then '0' when  LEN(a.ѪС��)>3 then '0' when CHARINDEX('.',ѪС��)=0 and LEN(ѪС��)>3 then '0' else ѪС�� end as dec(3,0)) AS PLATELET_VALUE
      ,'1' AS URINE_PROTEIN_CODE
      ,'0' AS URINE_PROTEIN_RESULT 
	  ,'0' as URINE_PROTEIN_VALUE
      ,'0' AS NT_CODE
	  ,'0' as NT_NAME
      ,'0' AS NT_VALUE 
      ,'0' AS URINE_CODE
	  ,'0' as URINE_NAME
      ,'0' AS BLD_CODE
	  ,'0' as BLD_NAME
      ,'0' AS URINE_SPECIFIC_GRAVITY
      ,'0' AS URINE_ACID_BASE
      ,'0' AS URINE_PROTEIN
      ,cast(case when isnumeric(�ո�Ѫ��)=0 then '0' when CHARINDEX('.',�ո�Ѫ��)>4 then '0' when CHARINDEX('.',�ո�Ѫ��)=0 and LEN(�ո�Ѫ��)>3 then '0' else �ո�Ѫ�� end as dec(4,1)) as BLOOD_SUGAR_FASTING
      ,'0' AS BLOOD_SUGAR_2HOUR
      ,'2' AS ELECTROCARDIOGRAM_MARK
      ,'0' AS ELECTROCARDIOGRAM_REMARKS 
      ,'2' AS FECAL_OCCULT_BLOOD
      ,'0' AS SUGAR_HEMOGLOBIN_VALUE
      ,'1' AS HEPATITIS_CODE
      ,'1' AS HEPATITIS_E_TEST_CODE 
      ,'1' AS HEPATITIS_ANTIBODY_CODE 
      ,'1' AS HEPATITIS_E_ANTIBODY_CODE 
      ,'1' AS HEPATITIS_CORE_CODE
      ,cast(case when isnumeric(Ѫ��ȱ�ת��ø)=0 then '0' when CHARINDEX('.',Ѫ��ȱ�ת��ø)=0 then '0' when CHARINDEX('.',Ѫ��ȱ�ת��ø)>4 then '0' when CHARINDEX('-',Ѫ��ȱ�ת��ø)>0 then '0' else Ѫ��ȱ�ת��ø end as dec(3,0)) AS SERUM_ALANINE_VALUE
      ,cast(case when isnumeric(Ѫ��Ȳ�ת��ø)=0 then '0' when CHARINDEX('.',Ѫ��Ȳ�ת��ø)=0 then '0' when CHARINDEX('.',Ѫ��Ȳ�ת��ø)>4 then '0' when CHARINDEX('-',Ѫ��Ȳ�ת��ø)>0 then '0' else Ѫ��Ȳ�ת��ø end as dec(3,0)) AS SERUM_ASPARTATE_VALUE
      ,cast(case when isnumeric(�׵���)=0 then '0' when CHARINDEX('.',�׵���)>4 then '0' when CHARINDEX('-',�׵���)>0 then '0' when CHARINDEX('.',�׵���)=0 then '0' else �׵��� end as dec(3,0)) AS PROTEIN_CONCENTRATION
	  ,cast(case when isnumeric(�ܵ�����)=0 then '0' when CHARINDEX('.',�ܵ�����)>4 then '0' when CHARINDEX('.',�ܵ�����)=0 and len(�ܵ�����)>3 then '0' when CHARINDEX('-',�ܵ�����)>0 then '0' else �ܵ����� end as dec(4,1)) AS TOTAL_BILIRUBIN_VALUE
      ,cast(case when isnumeric(��ϵ�����)=0 then '0' when CHARINDEX('.',��ϵ�����)>4 then '0' when CHARINDEX('.',��ϵ�����)=0 and len(��ϵ�����)>4 then '0' when CHARINDEX('-',��ϵ�����)>0 then '0' else ��ϵ����� end as dec(5,1)) AS CONJUGATED_BILIRUBIN
      ,cast(case when isnumeric(Ѫ�弡��)=0 then '0' when CHARINDEX('.',Ѫ�弡��)>4 then '0' when CHARINDEX('.',Ѫ�弡��)=0 and len(Ѫ�弡��)>4 then '0' when CHARINDEX('-',Ѫ�弡��)>0 then '0' else Ѫ�弡�� end as dec(5,1)) AS BLOOD_CREATININE_TEST
      ,cast(case when isnumeric(Ѫ���ص�)=0 then '0' when CHARINDEX('.',Ѫ���ص�)>4 then '0' when CHARINDEX('.',Ѫ���ص�)=0 and len(Ѫ���ص�)>3 then '0' when CHARINDEX('-',Ѫ���ص�)>0 then '0' else Ѫ���ص� end as dec(4,1)) AS BLOOD_UREA_NITROGEN
      ,cast(case when isnumeric(Ѫ��Ũ��)=0 then '0' when CHARINDEX('.',Ѫ��Ũ��)>4 then '0' when CHARINDEX('.',Ѫ��Ũ��)=0 and len(Ѫ��Ũ��)>3 then '0' when CHARINDEX('-',Ѫ��Ũ��)>0 then '0' else Ѫ��Ũ�� end as dec(4,1)) AS BLOOD_POTASSIUM
      ,cast(case when isnumeric(Ѫ��Ũ��)=0 then '0' when CHARINDEX('.',Ѫ��Ũ��)>6 then '0' when CHARINDEX('.',Ѫ��Ũ��)=0 and len(Ѫ��Ũ��)>5 then '0' when CHARINDEX('-',Ѫ��Ũ��)>0 then '0' else Ѫ��Ũ�� end as dec(6,2)) AS BLOOD_SODIUM 
      ,cast(case when isnumeric(�ܵ��̴�)=0 then '0' when CHARINDEX('.',�ܵ��̴�)>4 then '0' when CHARINDEX('.',�ܵ��̴�)=0 and len(�ܵ��̴�)>3 then '0' when CHARINDEX('-',�ܵ��̴�)>0 then '0' else �ܵ��̴� end as dec(5,2)) AS TOTAL_CHOLESTEROL 
      ,cast(case when isnumeric(��������)=0 then '0' when CHARINDEX('.',��������)>3 then '0' when CHARINDEX('.',��������)=0 and len(��������)>2 then '0' when CHARINDEX('-',��������)>0 then '0' else �������� end as dec(3,1)) AS TRIGLYCERIDE
      ,cast(case when isnumeric(Ѫ����ܶ�֬���׵��̴�)=0 then '0' when CHARINDEX('.',Ѫ����ܶ�֬���׵��̴�)>4 then '0' when CHARINDEX('.',Ѫ����ܶ�֬���׵��̴�)=0 and len(Ѫ����ܶ�֬���׵��̴�)>3 then '0' when CHARINDEX('-',Ѫ����ܶ�֬���׵��̴�)>0 then '0' else Ѫ����ܶ�֬���׵��̴� end as dec(5,2)) AS SERUM_CHOLESTEROL_LOW
      ,cast(case when isnumeric(Ѫ����ܶ�֬���׵��̴�)=0 then '0' when CHARINDEX('.',Ѫ����ܶ�֬���׵��̴�)>4 then '0' when CHARINDEX('.',Ѫ����ܶ�֬���׵��̴�)=0 and len(Ѫ����ܶ�֬���׵��̴�)>3 then '0' when CHARINDEX('-',Ѫ����ܶ�֬���׵��̴�)>0 then '0' else Ѫ����ܶ�֬���׵��̴� end as dec(5,2)) AS SERUM_CHOLESTEROL_HEIGHT
      ,'0' AS CANCER_EMBRYO_ANTIGEN 
      ,case when substring(a.�ز�X��Ƭ,1,1)='1' then '1' else '2' end CHEST_X_RAY_MARK
      ,�ز�X��Ƭ�쳣 AS CHEST_X_RAY_REMARKS
      ,case when substring(a.B��,1,1)='1' then '1' else '2' end B_MODE_MARK                 
      ,B������ AS B_MODE_REMARKS
      ,case when substring(a.����ͿƬ,1,1)='1' then '1' else '2' end CERVICAL_SMEAR_MARK
      ,����ͿƬ�쳣 AS CERVICAL_SMEAR_REMARKS
      ,����������� AS AUXILIARY_TEST
      ,case when ƽ����='1' then '1' else '99' end ZY_CONSTITUTION_TYPE_CODE
	  ,'' as ZY_CONSTITUTION_TYPE_NAME
      ,'' AS ZY_CONSTITUTION_RESULT_CODE
	  ,'' as ZY_CONSTITUTION_RESULT
      ,'' AS HEALTH_PROBLEM_CODE
	  ,'' as HEALTH_PROBLEM
	  , isnull((select top 1 case when isnull(��ҩ������,'')='' then '1' else ��ҩ������ end from tb_�������_��ҩ����� c where a.���˵������=c.���˵������ and DATEDIFF(day,a.����ʱ�� ,c.����ʱ��)=0),'3') as MEDICINE_COMPLIANCE_CODE
	  ,'' as MEDICINE_COMPLIANCE
      ,�������� AS HEALTH_ABNORMAL_MARK
      ,���������쳣1 AS HEALTH_ABNORMAL_REMARKS
	  ,���������쳣2 AS HEALTH_ABNORMAL_REMARKS2
	  ,���������쳣3 AS HEALTH_ABNORMAL_REMARKS3
	  ,���������쳣4 AS HEALTH_ABNORMAL_REMARKS4
      ,'' AS HEALTH_GUIDANCE_CODE
	  ,����ָ�� as HEALTH_GUIDANCE
      ,'' AS DANGER_CODE
	  ,'' as DANGER_NAME
      ,cast(case when isnumeric(SUBSTRING(Σ�����ؿ�������,1,2))=0 then '0' else SUBSTRING(Σ�����ؿ�������,1,2) end as dec(6,2)) AS TARGET_WEIGTH 
      ,'1' AS PROPOSAL_VACCINE_CODE
	  ,Σ�����ؿ������� as PROPOSAL_VACCINE
	  ,'' as SYMPTOMATIC_DOCTOR
	  ,'' as SKIN_OT_DESCRIPTION
	  ,'' as SCLERAL_OT_DESCRIPTION
	  ,'' as LYMPHGLAND_OT_DESCRIPTION
	  ,'' as COMMONLY_DOC_NAME
	  ,'' as OT_OCCUPATION_HAZARDS
	  ,'' as OCCUP_PROTEC_NAME
	  ,'' as URETHRA_OT_DESCRIPTION
	  ,'' as HOMOCYSTEINE
	  ,'' as ECG_RESULTS_CODE
	  ,'' as ECG_RESULTS
	  ,'' as ECG_RESULTS_OTHER
	  ,'' as B_MODE_RESULTS_OTFLAG
	  ,'' as B_MODE_RESULTS_OT
	  ,'1' as CERE_DISE_CODE
	  ,'δ����' as CERE_DISE_NAME
	  ,'' as CERE_DISE_OTHER
	  ,'1' as KIDNEY_DISE_CODE
	  ,'δ����' as KIDNEY_DISE_NAME
	  ,'' as KIDNEY_DISE_OT
	  ,'1' as CARDIO_DISE_CODE
	  ,'δ����' as CARDIO_DISE_NAME
	  ,'' as CARDIO_DISE_OT
	  ,'1' as EYES_DISE_CODE
	  ,'δ����' as EYES_DISE_NAME
	  ,'' as EYES_DISE_OT
	  ,'1' as NERVOUS_OTDISE_CODE
	  ,'δ����' as NERVOUS_OTDISE_NAME
	  ,'' as NERVOUS_OTDISE_OT
	  ,'1' as OTHER_SYS_DISE_CODE
	  ,'δ����' as OTHER_SYS_DISE_NAME
	  ,'' as OTHER_SYS_DISE_OT
	  ,'' as HEALTH_PRO_DOC_NAME
	  ,'' as INOC_DOC_NAME
	  ,'' as HEALTH_EVA_DOC_NAME
	  ,Σ�����ؿ������� as DANGERS_HOLD_OT
	  ,֢״���� as OT_SYM_DESC
	  ,'' as ILLNESS_DESC
	  ,'' as PHYSICAL_SUMMARY
	  ,'' as HEALTH_DOC_NAME
	  ,'' as ANUS_DRE_OT
	  ,'' as BREAST_EXAM_RESULTS_OT
	  ,'' as BLOOD_TYPE_CODE
	  ,'' as BLOOD_TYPE
	  ,'' as BLOOD_ROUTINE_EXAM_OT
	  ,'' as SIGN
	  ,'' as FAMILY_SIGN
	  ,'' as FEEDBACKER_SIGN
	  ,'' as FEEDBACK_TIME
	  ,'' as LIFESTYLE_DOC_SIGN
	  ,'' as VISCERA_DOC_SIGN
	  ,'' as EYES_DOC_SIGN
	  ,'' as EXAMBODY_DOC_SIGN
	  ,'' as DRE_DOC_SIGN
	  ,'' as BREAST_DOC_SIGN
	  ,'' as GYNAECOLOGY_DOC_SIGN
	  ,'' as GYNAECOLOGY_OTDOC_SIGN
	  ,'' as ASSIST_EXAM_DOC_SIGN1
	  ,'' as ASSIST_EXAM_DOC_SIGN2
	  ,'' as CARDIOGRAPH_DOC_SIGN
	  ,'' as CHEST_X_RAY_DOC_SIGN
	  ,'' as ABDO_B_ULTR_DOC_SIGN
	  ,'' as ABDO_B_ULTR_OTDOC_SIGN
	  ,'' as CERVICAL_SMEAR_DOC_SIGN
	  ,'' as ASSIST_EXAM_OTDOC_SIGN
	  ,'' as HIPLINE
	  ,'' as CATARACT_MARK
	  ,'' as COLOR_VISION_CODE
	  ,'' as COLOR_VISION
	  ,'' as DRUM_EXAM_RESULTS
	  ,'' as DRUM_EXAM_RESUL
	  ,'' as EAR_EXAM_EXTR
	  ,'' as NASAL_SEPTUM_EXAM_RESUL_CODE
	  ,'' as NASAL_SEPTUM_EXAM_RESUL
	  ,'' as NASAL_MUCOSA_EXAM_RESUL_CODE
	  ,'' as NASAL_MUCOSA_EXAM_RESUL
	  ,'' as NASAL_EXAM_EXTR
	  ,'' as TONSIL_EXAM_RESULTS_CODE
	  ,'' as TONSIL_EXAM_RESULTS
	  ,'' as LARYNX_EXAM_RESULTS_CODE
	  ,'' as LARYNX_EXAM_RESULTS
	  ,'' as LARYNX_EXAM_EXTR
	  ,'' as CARIES_COUNT
	  ,'' as HYPODONTIA_COUNT
	  ,'' as DENTURE_COUNT
	  ,'' as DENTAL_PER_EXAM_RESULT_CODE
	  ,'' as DENTAL_PER_EXAM_RESULT
	  ,'' as NONN_EXAM_EXTR
	  ,'' as THYROID_EXAM_RESULTS_CODE
	  ,'' as THYROID_EXAM_RESULTS
	  ,'' as SPINE_EXAM_RESULTS_CODE
	  ,'' as SPINE_EXAM_RESULTS
	  ,'' as LIMB_EXAM_RESULTS_CODE
	  ,'' as LIMB_EXAM_RESULTS
	  ,'' as PROSTATE_EXAM_RESULTS_CODE
	  ,'' as PROSTATE_EXAM_RESULTS
	  ,'' as EX_GENITALIA_EXTR_MARK
	  ,'' as SURG_EXAM_EXTR
	  ,'' as CHEST_EXAM_RESULTS_CODE
	  ,'' as CHEST_EXAM_RESULTS
	  ,'' as KPERCU_PAIN_EXAM_RESULT_CODE
	  ,'' as KPERCU_PAIN_EXAM_RESULT
	  ,'' as VITAL_EXAM_EXTR
	  ,'' as HEART_EXAM_EXTR
	  ,'' as PHYREFLEX_EXTR_MARK
	  ,'' as PATHREFLEX_EXTR_MARK_CODE
	  ,'' as PATHREFLEX_EXTR_MARK
	  ,'' as NERVOUS_EXAM_EXTR
	  ,'' as GYNAECOLOGY_EXAM_EXTR
	  ,'' as LYMPHOCYTE_COUNT
	  ,'' as GLOBULIN
	  ,'' as LIVER_OT_EXAM_RESULTS
	  ,'' as SERUM_URIC_ACID
	  ,'' as RENAL_OT_EXAM_RESULTS
	  ,'' as OTBLOODFAT_EXAM_RESULTS_CODE
	  ,'' as OTBLOODFAT_EXAM_RESULTS
	  ,'' as HAAB_EXAM_RESULTS_CODE
	  ,'' as HAAB_EXAM_RESULTS
	  ,'' as HCVANT_EXAM_RESULTS_CODE
	  ,'' as HCVANT_EXAM_RESULTS
	  ,'' as HEPA_OTLABOR_RESULTS
	  ,'' as AFP
	  ,'' as OTHER_ABOR
	  ,'' as B_MODE_PANCR_EXTR_MARK
	  ,'' as B_MODE_KIDNE_EXTR_MARK
	  ,'' as B_MODE_SPLEE_EXTR_MARK
	  ,'' as IOCDMDC_PORTFOLIO_CODE
	  ,'' as IOCDMDC_PORTFOLIO
	  ,'' as OTHER_DOC_PRO
	  ,'' as PHYSICAL_ORG_CODE
	  ,'' as ZY_CON_PH
	  ,'' as ZY_CON_QX
	  ,'' as ZY_CON_YANGX
	  ,'' as ZY_CON_YINX
	  ,'' as ZY_CON_TS
	  ,'' as ZY_CON_SR
	  ,'' as ZY_CON_XY
	  ,'' as ZY_CON_QY
	  ,'' as ZY_CON_TB
	  ,'' as HEART_DISE_CODE
	  ,'' as HEART_DISE
	  ,'' as HEART_DISE_OTHER
	  ,'' as BLOOD_VESSELL_DISE_CODE
	  ,'' as BLOOD_VESSELL_DISE
	  ,'' as BLOOD_VESSELL_DISE_OT
	  ,'' as NERVOUS_DISE_CODE
	  ,'' as NERVOUS_DISE
	  ,'' as NERVOUS_DISE_OT
	  ,'' as PHYSICAL_ORG_NAME
	  ,'' as BREAST
	  ,'' as ASSIST_EXAM
	  ,'' as HYPODONTIA_RIGHTDOWN
	  ,'' as HYPODONTIA_RIGHTTOP
	  ,'' as HYPODONTIA_LEFTDOWN
	  ,'' as HYPODONTIA_LEFTTOP
	  ,'' as CARIES_RIGHTDOWN
	  ,'' as CARIES_RIGHTTOP
	  ,'' as CARIES_LEFTDOWN
	  ,'' as CARIES_LEFTTOP
	  ,'' as DENTURE_RIGHTDOWN
	  ,'' as DENTURE_RIGHTTOP
	  ,'' as DENTURE_LEFTDOWN
	  ,'' as DENTURE_LEFTTOP
	  ,'' as DUST
	  ,'' as RADIOGEN
	  ,'' as PHYSICAL_FACTOR
	  ,'' as CHEMICAL_SUBSTANCES
	  ,'' as FOLLOW_UP_DATE
	  ,'' as ZY_HEALTH_CODE
	  ,'' as ZY_HEALTH_NAME
	  ,'' as NOW_ORG_CODE
	  ,'' as SYSTOLIC_PRESSURE
	  ,'' as DIASTOLIC_PRESSURE
	  ,'' as EYES_EXAM_EXTR
	  ,'' as OTHER_SYS_DISEYON
	  ,'' as OTHER_SYS_DISE_OTNAME             
  FROM tb_������� a
  inner join tb_�������� b on a.���˵������=b.���˵������
  where 1=1 and ����״̬='1'
  AND b.����֤��<>''
  and isnull(a.FIELD2,'')<>''
  and a.����ʱ��>=convert(varchar,DATEADD(day,-1,getdate()),120)
  and a.����ʱ��<convert(varchar,getdate(),120)

  --and not exists(
  --select 1 from tb_�ϴ���־ rz where (a.���˵������+convert(varchar,a.[ID])) = rz.D001 and ���ܺ�='EHR_HEALTH_PHYSICAL')
