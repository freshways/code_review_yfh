/* 2.8.1儿童健康体检记录表（C-14 标） - EHR_CHILDREN_PHYSICAL */

SELECT top 1000 a.个人档案编号+convert(varchar,a.ID)+'01' as ID
      ,RIGHT(a.个人档案编号,17) AS HEALTH_RECORD_CODE
	  ,'未知' as CHILDREN_PHYSICAL_CODE
	  ,b.姓名 as NAME
	  ,r.性别 as SEX
	  ,convert(datetime,case when ISDATE(b.出生日期)=0 then '19000101' else b.出生日期 end) as BIRTH_DATE
	  ,b.母亲姓名 as MOTHER_NAME
	  ,substring(b.母亲联系电话,1,20) as MOTHER_TEL
	  ,b.父亲姓名 as FATHER_NAME
	  ,substring(b.父亲联系电话,1,20) as FATHER_TEL
	  ,(SELECT top 1 地区名称 FROM tb_地区档案 where 地区编码= b.省) as ADDRESS_NOW_PROVINCE
	  ,(SELECT top 1 地区名称 FROM tb_地区档案 where 地区编码= b.市) as ADDRESS_NOW_CITY
	  ,(SELECT top 1 地区名称 FROM tb_地区档案 where 地区编码=b.区) as ADDRESS_NOW_COUNTY
	  ,(SELECT top 1 地区名称 FROM tb_地区档案 where 地区编码= b.街道) as ADDRESS_NOW_COUNTRY
	  ,(SELECT top 1 地区名称 FROM tb_地区档案 where 地区编码= b.居委会) as ADDRESS_NOW_VILLAGE
	  ,'' as ADDRESS_NOW_DOORPLATE
	  ,case when isnumeric(a.身长)=0 then '0' when CHARINDEX('.',a.身长)>5 then '0' when CHARINDEX('.',a.身长)=0 then '0' else CONVERT(decimal(5,1),a.身长) end as LENGTH
	  ,case when isnumeric(a.身长)=0 then '0' when CHARINDEX('.',a.身长)>5 then '0' when CHARINDEX('.',a.身长)=0 then '0' else CONVERT(decimal(5,1),a.身长) end as HEIGHT
	  ,case when ISNULL(a.体重选项,'')='' then '1' else replace(substring(a.体重选项,1,2),',','') end AGE_HEIGHT_CODE
	  ,isnull((SELECT P_DESC FROM tb_常用字典 WHERE P_CODE=a.体重选项 AND P_FUN_CODE='hw_item'),'上') as AGE_HEIGHT_NAME
	  ,case when isnumeric(a.体重)=0 then '0' when CHARINDEX('.',a.体重)>6 then '0'  when CHARINDEX('.',a.体重)=0 then '0' else CONVERT(decimal(6,2),a.体重) end as WEIGTH
	  ,case when ISNULL(a.体重选项,'')='' then '1' else replace(substring(a.体重选项,1,2),',','') end AGE_WEIGHT_CODE
	  ,isnull((SELECT P_DESC FROM tb_常用字典 WHERE P_CODE=a.体重选项 AND P_FUN_CODE='hw_item'),'上') as AGE_WEIGHT_NAME
	  ,case when ISNULL(a.体重选项,'')='' then '1' else replace(substring(a.体重选项,1,2),',','') end HEIGHT_WEIGHT_CODE
	  ,isnull((SELECT P_DESC FROM tb_常用字典 WHERE P_CODE=a.体重选项 AND P_FUN_CODE='hw_item'),'上') as HEIGHT_WEIGHT_NAME
	  ,case when ISNULL(a.发育评估,'')='' then '0' else replace(substring(a.发育评估,1,2),',','') end PHYSIQUE_CODE
	  ,'未知' as PHYSIQUE_NAME
	  ,case when isnumeric(a.头围)=0 then '0' when CHARINDEX('.',a.头围)>4 then '0' when CHARINDEX('.',a.头围)=0 then '0' else CONVERT(decimal(4,1),a.头围) end HEAD_CIRCUMFERENCE
	  ,case when ISNULL(a.面色,'')='' then '1' else substring(a.面色,1,1) end COMPLEXION_CODE
	  ,'' as COMPLEXION_DESC
	  ,case when substring(a.皮肤,1,1)=1 or substring(a.皮肤,1,1)=2 then '1' else '0' end SKIN_TEST_RESULT
	  ,case when substring(a.前囟,1,1)=1 then '1' else '0' end FRONT_HALOGEN_CLOSURE
	  ,case when substring(a.颈部包块,1,1)=1 then '1' else '0' end NECK_MASS
	  ,case when substring(a.眼部,1,1)=1 then '1' else '0' end as EYE_TEST_MARK
	  ,cast(case when isnumeric(c.视力)=0 then '0' when CHARINDEX('.',c.视力)>3 then '0' else c.视力 end as dec(3,1)) AS EYE_LEFT_VALUE
      ,cast(case when isnumeric(c.视力)=0 then '0' when CHARINDEX('.',c.视力)>3 then '0' else c.视力 end as dec(3,1)) AS EYE_RIGHT_VALUE
      ,case when isnumeric(c.视力)=0 then '0' when CHARINDEX('.',c.视力)>3 then '0' else CONVERT(decimal(3,1),c.视力) end AS EYE_LEFT_CORRECT_VALUE
      ,case when isnumeric(c.视力)=0 then '0' when CHARINDEX('.',c.视力)>3 then '0' else CONVERT(decimal(3,1),c.视力) end AS EYE_RIGHT_CORRECT_VALUE
	  ,case when substring(a.耳部,1,1)=1 then '1' else '0' end as EAR_APPEARANCE_MARK
	  ,case when substring(q.听力,1,1)=1 then '1' else '0' end as HEARING_CODE
	  ,'通过' as HEARING_NAME
	  ,case when substring(a.口腔,1,1)=1 then '1' else '0' end as ORAL_CAVITY_MARK
	  ,case when ISNUMERIC(c.牙齿数目)=0 then 0 else convert(decimal(2,0),c.牙齿数目) end TEETHING
	  ,case when ISNUMERIC(c.龋齿数)=0 then 0 else convert(decimal(2,0),c.龋齿数) end CARIES_NUMBER
	  ,case when substring(a.肺炎,1,1)=1 then '1' else '0' end LUNGS_MARK
	  ,case when substring(a.心肺,1,1)=1 then '1' else '0' end HEART_MARK
	  ,case when substring(a.腹部,1,1)=1 then '1' else '0' end ABDOMEN_MARK
	  ,case when substring(a.脐部,1,1)=1 then '1' else '0' end UMBILICAL_CORD_MARK
	  ,case when substring(a.脐部,1,1)=1 then '1' else '0' end UMBILICAL_HERNIA_MARK
	  ,case when substring(a.四肢,1,1)=1 then '1' else '0' end LIMB_MARK
	  ,case when substring(f.步态,1,1)=1 then '1' else '0' end GAIT_MARK
	  ,case when a.佝偻病体征=1 then '1' else '0' end  RICKETS_SYMPTOM_CODE
	  ,'未知' as RICKETS_SYMPTOM_NAME
	  ,a.佝偻病体征 as RICKETS_TZ_CODE
	  ,'其他' as RICKETS_TZ_NAME
	  ,'0' as ANUS_MARK
	  ,case when a.生殖器=1 then 1 else 0 end EXTERNAL_GENITAL_ORGANS_MARK
	  ,case when isnumeric(a.血红蛋白值)=0 then '0' when CHARINDEX('.',a.血红蛋白值)>3 then '0' else CONVERT(decimal(3,0),a.血红蛋白值) end HEMOGLOBIN_VALUE
	  ,case when isnumeric(a.户外活动)=0 then '0' when CHARINDEX('.',a.户外活动)>4 then '0' else CONVERT(decimal(4,1),a.户外活动) end OUTDOORS_TIME
	  ,'' as VITAMIN_D_NAME
	  ,case when isnumeric(a.服用维生素)=0 then '0' when len(a.服用维生素)>5 then '0' else CONVERT(decimal(5,0),a.服用维生素) end VITAMIN_D_DOSE
	  ,case when substring(a.发育评估,1,1)=1 then '1' else '0' end CHILDREN_DEVELOPMENT_MARK
	  ,case when isnumeric(substring(a.两次随访间患病情况,1,1))=1 and substring(a.两次随访间患病情况,1,1)=1 then '1' else '0' end FOLLOW_ILL_MARK
	  ,case when substring(a.肺炎,1,1)!=0 then a.肺炎 else '0' end FOLLOW_PNEUMONIA_TIMES
	  ,case when substring(a.腹泻,1,1)!=0 then a.腹泻 else '0' end FOLLOW_DIARRHEA_TIMES
	  ,case when substring(a.外伤,1,1)!=0 then a.外伤 else '0' end FOLLOW_ACCIDENT_TIMES
	  ,case when isnull(a.患病其他,'')='' then '未知' else a.患病其他 end FOLLOW_OTHER_ILL
	  ,case when substring(a.转诊状况,1,1)=1 then '1' else '0' end REFERRAL_MARK
	  ,a.转诊原因 as REFERRAL_REASON
	  ,a.转诊机构 as CHANGE_INTO_HOSPITAL_NAME
	  ,case when ISNULL(a.指导,'')='' then 9 else substring(a.指导,1,1) end CHILDREN_HEALTH_CODE
	  ,case when ISNULL(a.指导,'')='' then '其他' else '科学喂养' end CHILDREN_HEALTH_NAME
	  ,case when isdate(a.发生时间)=0 then '19000101' else REPLACE(CONVERT(VARCHAR(10),a.发生时间,120),'-','') end FOLLOW_DATE
	  ,a.随访医生 as FOLLOW_DOCTOR_NAME
	  ,(select top 1 机构名称 from tb_机构信息 where 机构编号=a.创建机构)  AS ORG_NAME
	  ,a.创建机构 AS ORG_CODE
	  ,left(a.个人档案编号,6) as DISTRICT_CODE
	  ,GETDATE() as SYSTEM_TIME
	  ,case when isnull(b.身份证号,'')='' then '未知' else b.身份证号 end IDCARDNUM
	  ,0 as BREGAM_TRANS_DIA
	  ,0 as BREGAM_VERT_DIA
	  ,'1' as LEFT_AUDI_TEST_RESULT
	  ,'听见' as LEFT_AUDI_TEST_DESC
	  ,'1' as ANUS_TEST_RESULT
	  ,'未见异常' as ANUS_TEST_DESC
	  ,i.脐带 as UMBILICAL_TEST_RESULT
	  ,(SELECT P_DESC FROM tb_常用字典 WHERE P_CODE=i.脐带 AND P_FUN_CODE='jd_jidai') as UMBILICAL_TEST_DESC
	  ,'' as OTHER_TEST_RESULT
	  ,convert(datetime,case when ISDATE(a.下次随访日期)=0 then '19000101' else a.下次随访日期 end) as NEXT_FOLLOWUP_DATE
	  ,'' as TAKE_DRUG
	  ,'饮食调养指导,起居调摄指导,摩腹捏脊' as ZY_HEALTH
	  ,'' as LOSE_VISIT_CAUSE
	  ,'1' as CHEST_EXAM_MARK
	  ,'0' as INPUT_TYPE
	  ,'' as GUIDANCE_OTHER
	  ,'未知' as BMI
	  ,'0' as BMI_RESULT
	  ,'' as BMI_DESC
	  ,'INSERT' as EXCHANGE_TYPE
from tb_儿童_健康检查_满月 a 
left join tb_儿童新生儿访视记录 i on a.个人档案编号 = i.个人档案编号
left join tb_儿童基本信息 b on  a.个人档案编号=b.个人档案编号
left join tb_儿童_健康检查_6岁 c on c.个人档案编号 = a.个人档案编号
left join tb_儿童_健康检查_5岁 d on d.个人档案编号 = a.个人档案编号
left join tb_儿童_健康检查_4岁 e on e.个人档案编号 = a.个人档案编号
left join tb_儿童_健康检查_30月 f on f.个人档案编号 = a.个人档案编号
left join tb_儿童_健康检查_24月 g on g.个人档案编号 = a.个人档案编号
left join tb_儿童_健康检查_18月 h on h.个人档案编号 = a.个人档案编号
left join tb_儿童_健康检查_12月 o on o.个人档案编号 = a.个人档案编号
left join tb_儿童_健康检查_8月 p on p.个人档案编号 = a.个人档案编号
left join tb_儿童_健康检查_6月 q on q.个人档案编号 = a.个人档案编号
left join tb_健康档案 r on r.个人档案编号 = a.个人档案编号
where 1=1
	 and a.创建时间>=DATEADD(day,-1,getdate())
	 and a.创建时间<getdate()
	 --   and not exists(
  --select 1 from tb_上传日志 rz where (a.个人档案编号+convert(varchar,a.[ID])+'01') = rz.D001 and 功能号='EHR_CHILDREN_PHYSICAL')

