/* 5.1.1高血压患者随访服务记录表(C-18) - EHR_HYPERTENSION_FOLLOW */

SELECT a.个人档案编号+convert(varchar,a.ID)  AS ID
      ,RIGHT(b.个人档案编号,17) AS HELTH_ARCHIVES_CODE
      ,a.ID AS HYPERTENSION_FORM_CODE 
      ,b.姓名 AS HER_NAME
      ,b.性别编码 AS HER_GENDER
      ,REPLACE(b.出生日期,'-','') AS BIRTH_DATE 
      ,'1' AS HER_ID_TYPE 
      ,b.身份证号 AS HER_ID_CODE
      ,'山东省' AS PERMANENT_ADDRESS_PROVINCE
      ,b.市 AS PERMANENT_ADDRESS_CITY
      ,b.区 AS PERMANENT_ADDRESS_AREA
      ,b.街道 AS PERMANENT_ADDRESS_VILLAGE
      ,b.居委会 AS PERMANENT_ADDRESS_STREET
      ,'未知' AS PERMANENT_ADDRESS_NUM
      ,'276400' AS PERMANENT_ADDRESS_POSTCODE 
      ,b.市 AS LIVE_ADDRESS_CITY
      ,b.区 AS LIVE_ADDRESS_AREA
      ,b.街道 AS LIVE_ADDRESS_VILLAGE
      ,b.居委会 AS LIVE_ADDRESS_STREET
      ,'未知' AS LIVE_ADDRESS_NUM
      ,'未知' AS LIVE_ADDRESS_POSTCODE
      ,'未知' AS HER_PHONE_TYPE 
      ,联系电话 AS HER_PHONE_NUM 
      ,b.联系人姓名 AS HER_CONTACTS_NAME  
      ,'未知' AS HER_CONTACTS_TYPE  
      ,b.联系人电话 AS HER_CONTACTS_NUM
      ,b.居委会 AS COM_NAME  
      ,'未知' AS COM_CONTACTS_NAME  
      ,'未知' AS HER_COM_CONTACTS_PHONE_TYPE   
      ,'未知' AS HER_COM_CONTACTS_PHONE_NUM 
      ,replace(目前症状,',','#') AS SYMPTOM_CODE
	  ,STUFF((select '#'+P_DESC from [tb_常用字典] where P_FUN_CODE='gxbmqzz' 
	   and CHARINDEX(','+P_CODE+',',','+目前症状+',')>0 for xml path('')),1,1,'') AS SYMPTOM_NAME
      ,收缩压 as SYSTOLIC_PRESSURE
      ,舒张压 as DIASTOLIC_PRESSURE
      ,空腹血糖 AS FASTING_BLOOD_GLUCOSE 
      ,'0' AS SERUM_POTASSIUM
      ,'0' AS SERUM_SODIUM
      ,体重 as WEIGHT
      ,体重2 AS TARGET_WEIGHT
      ,身高 as HEIGHT
      ,BMI AS BODY_INDEX
      ,BMI2 AS TARGET_BODY_INDEX
      ,isnull(心率,'0') AS  HEART_RATE
      ,体征其他 AS OTHER_POSITIVE_SIGNS
      ,case when 吸烟数量>0 then '3' else '1' end SMOKE_STATUS_CODE
      ,'0' AS SMOKE_AGE  
      ,吸烟数量 AS SMOKE_START_DAY_NUM  
      ,吸烟数量2 AS TARGET_SMOKE_DAY_NUM
      ,'0' AS QUIT_SMOKING_AGE
      ,'0' AS WINE_DRINK_AGE 
      ,case when 饮酒数量>0 then '2' else '1' end DRINK_FREQUENCY_CODE
	  ,case when 饮酒数量>0 then '偶尔' else '从不' end DRINK_FREQUENCY_NAME
      ,'9' AS DRINK_KIND_CODE
	  ,'其他' AS DRINK_KIND_NAM
      ,CAST(饮酒数量 AS DECIMAL(5,0)) AS DRINK_MILLILITER    
      ,CAST(饮酒数量2 AS DECIMAL(5,0)) AS TARGET_DRINK_MILLILITER
      ,'1' AS DRUNK_SIGN
      ,'0' AS QUIT_DRINK_AGE
      ,运动频率 AS EXERCISE_AMOUNT  
      ,运动频率2 AS TARGET_EXERCISE_AMOUNT
      ,CAST(运动持续时间 AS DECIMAL(5,0)) AS EXERCISE_TIMES 
      ,CAST(运动持续时间2 AS DECIMAL(5,0)) AS TARGET_EXERCISE_TIMES 
      ,'0' AS EXERCISE_CONTINUED_TIME
      ,'未知' AS EXERCISE_MODE
      ,摄盐情况 AS SALT_LEVEL_CODE
      ,摄盐情况2 AS TARGET_SALT_LEVEL_CODE
      ,'1' AS DIET_CODE
	  ,'荤素均衡' as DIET_NAME
      ,'1' AS DIET_RESULT_CODE
      ,'9' AS MENTALITY_CODE
	  ,'其他' AS MENTALITY_NAME
      ,心理调整 AS MENTALITY_ADJUST_CODE
      ,遵医行为 AS COMPLY_DOCTOR_CODE
      ,辅助检查 AS AUXILIARY_INSPECT_PROJECT
      ,辅助检查 AS AUXILIARY_INSPECT_RESULT
	  ,'' AS INSPECT_NAME
      ,REPLACE(发生时间,'-','') AS INSPECT_DATE
      ,服药依从性 AS DOSE_CODE
      ,药物副作用 AS ADVERSE_REACTION_MARK
      ,药物副作用 AS ADVERSE_REACTION_DESCRIBE
      ,'' AS INHOSPATIL_DATE
      ,'' AS INHOSPATIL_RESON 
      ,'' AS OUTHOSPATIL_DATE
      ,'' AS FORME_INHOSPATIL_NAME
      ,'' AS FORME_RECORD_CODE
      ,'' AS HOME_BED_DATE
      ,'' AS HOEM_BED_RESON
      ,'' AS HOME_BED_EVACUATE_DATE 
      ,'' AS FORME_HOME_BED_NAME
      ,'' AS FORME_BED_RECORD_CODE
      ,'0' AS OXYGEN_TIMES
      ,转诊原因 as TRANSFER_RESON
      ,转诊科别 AS TRANSFER_HOSPATIL_NAME
      ,转诊科别 AS TRANSFER_DEPARTMENT_NAME
      ,'' AS CYCLE_CODE
      ,b.职业 AS VOCATION_TYPE_CODE
      ,'0' AS WAIST
      ,'0' AS TOTAL_CHOLESTEROL
      ,'0' AS TRIGLYCERIDE
      ,'0' AS LOW_DENSITY_LIPOPROTEIN
      ,'0' AS HIG_DENSITY_LIPOPROTEIN
      ,'' AS ELECTROCARDIOGRAM
      ,null AS STOP_SUPERVISE_DATE
      ,'' AS STOP_RESON
      ,'' AS DEFULT_RESON
      ,'0' AS CEREBRAL_VASCULAR_TYPE
      ,'0' AS HEART_DISEASE_TYPE
      ,'0' AS ACTUAL_AGE
      ,'0' AS PULSE
      ,'0' AS FAMILY_SMOKING_SIGN
      ,'0' AS NEXT_ADVISE_EXERCISE_TYPE_CODE
      ,'0' AS NEXT_ADVISE_EXERCISE_CODE
      ,'0' AS NEXT_ADVISE_EXERCISE_AMOUNT
      ,'0' AS NEXT_ADVISE_EXERCISE_TIMES
      ,'0' AS NEXT_ADVISE_STAPLE_FOOD
      ,'0' AS NEXT_ADVISE_SMOKE
      ,'0' AS NEXT_ADVISE_DRINK
      ,'1' AS NEXT_ADVISE_SALT
      ,'0' AS BODY_MASS_INDEX
      ,'0' AS HYPERTENSION_TYPE_CODE
      ,'' AS HYPERTENSION_TYPE_NAME 
      ,'' AS PRESSURE_GRADING_NAME                                       
      ,'' AS PRESSURE_GRADING_CODE
      ,a.医疗费支付类型 AS PAYMENT_METHOD_CODE
      ,a.医疗费支付类型 AS PAYMENT_METHOD_TYPE
      ,'' AS HISTORY_HYPERTENSION_CODE
      ,'' AS HYPERTENSION_COMPLICATION_CODE
      ,'' AS LIFE_ABILITY_CODE
      ,'0' AS HYPERTENSION_NO_DRUG
      ,'' AS MAJOR_HEALTH_PROBLEMS
      ,'' AS MAJOR_HEALTH_PROBLEMS_CODE
      ,'' AS DETECTION_WAY_CODE
      ,'' AS DETECTION_WAY
	  ,目前症状其他 as OTHER_SYM_DESC
	  ,case when 饮酒数量>0 then '1' else '0' end DRINK_WINE_MARK
	  ,'1' as PRESSURE_MARK
	  ,case when 吸烟数量>0 then '1' else '0' end SMOKE_MARK
	  ,'' as ZY_HEALTH_GUIDANCE
	  ,下一步管理措施 as NEXT_STEP
	  ,substring(居民签名,1,255) as RESIDENT_NAME
	  ,'' as PHOTO_URL
      ,'未知' AS RESPON_DOCTOR_PHONE_TYPE  
      ,'未知' AS RESPON_DOCTOR_PHONE_NUM  
	  ,随访医生 AS DOCTOR_NAME
      ,REPLACE(发生时间,'-','') AS THIS_FOLLOW_DTAE
      ,随访方式 AS THIS_FOLLOW_CODE	  
      ,本次随访分类 AS EVALUATION_RESULTS_CODE   
	  ,(select P_DESC from tb_常用字典 where P_FUN_CODE ='sffl' and P_CODE=a.本次随访分类) AS EVALUATION_RESULTS
      ,REPLACE(下次随访时间,'-','') AS NEXT_VISIT_DATE
      ,随访医生 AS VISIT_DOCTOR_NAME
	  ,ISNULL((SELECT 机构名称 FROM tb_机构信息 c WHERE c.机构编号=b.创建机构),'未知') AS ORG_NAME 
      ,b.创建机构 AS ORG_CODE
	  ,left(a.个人档案编号,6) as DISTRICT_CODE  
	  ,GETDATE() as SYSTEM_TIME
	  ,'INSERT' as EXCHANGE_TYPE
  FROM tb_MXB高血压随访表 a
  left join View_个人信息表 b on a.个人档案编号 = b.个人档案编号
  WHERE 1=1
  AND b.身份证号<>'' 
  and (isnull(收缩压,'')<>'' and isnull(舒张压,'')<>'')
  and a.创建时间>=DATEADD(day,-1,getdate())
  and a.创建时间<getdate()


  --and  not exists(
  --select 1 from tb_上传日志 rz where (a.个人档案编号+convert(varchar,a.ID)) = rz.D001 and 功能号='EHR_HYPERTENSION_FOLLOW')
