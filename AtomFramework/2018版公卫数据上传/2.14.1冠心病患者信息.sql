/* 2.14.1冠心病患者信息 - EHR_CORONARYHEARTDISEASEINFO */

SELECT
	a.个人档案编号+convert(varchar,a.[ID]) AS ID
	,RIGHT(a.个人档案编号,17) as HEALTH_RECORD_CODE
	,LEFT(c.姓名,50) as NAME
	,case when DATALENGTH(c.身份证号)>18 then '' else c.身份证号 end AS CZRK_ID
	,case when DATALENGTH(c.身份证号)>18 then '' else c.身份证号 end AS ID_NUM
	,'1' AS OTHER_CARD_CODE
	,'' AS OTHER_CARD_NO
	,LEFT(a.冠心病类型,1) AS DIAGNOSISOFCHD
	,'1' AS DIAGNOSTICBASIS
	,case when ISDATE(a.确诊日期)=0 then '' else REPLACE(CONVERT(varchar,CONVERT(date,a.确诊日期)),'-','') end ACCIDENT_DATE   
	,case when ISDATE(a.确诊日期)=0 then '' else REPLACE(CONVERT(varchar,CONVERT(date,a.确诊日期)),'-','') end CONFIRMED_DATE
	,'1' AS IS_FRIST_ATTACK
	,convert(datetime,case when ISDATE(a.创建时间)=0 then '19000101' else a.创建时间 end) AS DATAACQUISITIONDATE
	,a.[创建机构] AS DATAACQUISITION_ORG_CODE
	,(SELECT 机构名称 FROM tb_机构信息 b WHERE a.创建机构=b.机构编号)  AS DATAACQUISITION_ORG_NAME
	,RIGHT(a.创建人,18) AS DATAACQUISITION_PEO_CODE
	,(SELECT 机构名称 FROM tb_机构信息 b WHERE a.创建机构=b.机构编号)  AS ORG_NAME
	,a.[创建机构] AS ORG_CODE
	,LEFT(c.区,6) as DISTRICT_CODE
	,GETDATE() as SYSTEM_TIME
	,'INSERT' as EXCHANGE_TYPE
FROM [tb_MXB冠心病管理卡] a
LEFT JOIN tb_健康档案 c ON c.个人档案编号=a.个人档案编号
WHERE  1=1 
	and a.创建时间>=DATEADD(day,-19,getdate())
	and a.创建时间<getdate()

--and not exists
--(select 1 from tb_上传日志 b where (a.个人档案编号+convert(varchar,a.ID)) = b.D001 and 功能号='EHR_CORONARYHEARTDISEASEINFO')