/* 5.2.2糖尿病随访用药记录（C-16 辅标） - EHR_DCV_PHARMACY */

SELECT Y_ID AS ID
      ,b.个人档案编号+convert(varchar,b.ID) AS VISIT_ID 
      ,药物名称 as DRUG_NAME
	  ,'0' as S_FLAG
      ,'未知' AS USING_DRUG_FREQ
      ,stuff(a.用法,patindex('%[^0-9]%',a.用法),LEN(a.用法),'0') AS DOSE_TOTAL
      ,'0' AS DOSE_SINGLE
      ,'未知' AS DOSE_UNIT
      ,'1' AS DRUG_WAY_CD  
      ,convert(datetime,case when isdate(b.创建时间)='0' then '1999-01-01' else b.创建时间 end) AS CREATED_AT
      ,left(a.个人档案编号,6) as DISTRICT_CODE 
      ,'INSERT' as EXCHANGE_TYPE
	  ,GETDATE() as SYSTEM_TIME
	  ,ISNULL((SELECT 机构名称 FROM tb_机构信息 c WHERE b.创建机构=c.机构编号),'不祥') AS ORG_NAME 
      ,b.创建机构 AS ORG_CODE	  
	  ,RIGHT(a.[个人档案编号],17) AS HEALTH_RECORD_CODE
  FROM tb_MXB糖尿病随访表_用药情况 a
  inner join tb_MXB糖尿病随访表 b on a.个人档案编号 = b.个人档案编号 and a.创建时间 = b.创建时间
  WHERE 药物名称<>'' 
  AND b.身份证号<>'' 
  and (isnull(收缩压,'')<>'' and isnull(舒张压,'')<>'')
  and a.创建时间>=DATEADD(day,-1,getdate())
  and a.创建时间<getdate()


  --and  not exists(
  --select 1 from tb_上传日志 rz where a.Y_ID = rz.D001 and 功能号='EHR_DCV_PHARMACY')
