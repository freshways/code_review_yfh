/* 2.5.4.1脑卒中用药情况(C-37辅标) - EHR_STROKE_MEDICATION */

select
	a.个人档案编号+convert(varchar,a.Y_ID) AS ID
	,b.个人档案编号+convert(varchar,b.ID) AS VISIT_ID
	,LEFT(a.药物名称,50) as DRUG_NAME
	,case when DATALENGTH(a.用法)>20 then LEFT(a.用法,10) else LEFT(a.用法,20) end USING_DRUG_FREQ
	,0 as DOSE_TOTAL
	,0 as DOSE_SINGLE
	,'未知' as DOSE_UNIT
	,'' as DRUG_WAY_CD
	,'' as DRUG_WAY_NM
	,LEFT(a.个人档案编号,6) as DISTRICT_CODE
	,'' as CHINESE_MED_TYPE_CODE
	,'' as CHINESE_MED_TYPE_NAME
	,GETDATE() as SYSTEM_TIME
	,b.[创建机构] AS ORG_CODE
	,(SELECT 机构名称 FROM tb_机构信息 c WHERE b.创建机构=c.机构编号)  AS ORG_NAME
	,'INSERT' as EXCHANGE_TYPE
from tb_MXB脑卒中随访表_用药情况 a
left join tb_MXB脑卒中随访表 b on a.个人档案编号=b.个人档案编号 and a.创建时间=b.创建时间
WHERE  1=1
	and isnull(a.药物名称,'')<>''
	and a.创建时间>=convert(varchar,DATEADD(day,-10,getdate()),120)
	and a.创建时间<convert(varchar,getdate(),120)

--not exists
--(select 1 from tb_上传日志 b where (a.个人档案编号+convert(varchar,a.Y_ID)) = b.D001 and 功能号='EHR_STROKE_MEDICATION')
