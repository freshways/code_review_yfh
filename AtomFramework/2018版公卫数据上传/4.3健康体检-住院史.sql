/*健康体检-住院史（辅C-3标） - EHR_PE_HOSPITALIZATIONS */

SELECT a.个人档案编号+convert(varchar,a.ID) as ID
      ,RIGHT(a.个人档案编号,17) as EXAM_ID
      ,case when ISDATE(a.入院日期)=0 then '19000101' else REPLACE(CONVERT(varchar(10),convert(date,a.入院日期),120),'-','') end  as ADMISSION_DATE
      ,case when ISDATE(a.出院日期)=0 then '19000101' else REPLACE(CONVERT(varchar(10),convert(date,a.出院日期),120),'-','') end  as DISCHARGE_DATE
      ,a.原因 as ADMISSION_REASON
      ,a.医疗机构名称 as ORG_NAME 
	  ,'未知' as ORG_CODE
	  ,'未知' as DEPT_CODE
	  ,'未知' as DEPT_NAME
      ,a.病案号 as CASE_NUM
	  ,a.ID as SN
	  ,left(a.个人档案编号,6) as DISTRICT_CODE
      ,convert(datetime,case when ISDATE(a.创建日期)=0 then '19000101' else a.创建日期 end) AS CREATED_AT
	  ,'INSERT' as EXCHANGE_TYPE
	  ,GETDATE() as SYSTEM_TIME
  FROM tb_健康体检_住院史 a 
  left join tb_健康体检 b on b.个人档案编号=a.个人档案编号
  where 1=1
  and 类型 =1
  and b.创建时间>=DATEADD(day,-1,getdate())
  and b.创建时间<getdate()

  --and not exists(
  --select 1 from tb_上传日志 rz where (a.个人档案编号+convert(varchar,a.[ID])) = rz.D001 and 功能号='EHR_PE_HOSPITALIZATIONS')
