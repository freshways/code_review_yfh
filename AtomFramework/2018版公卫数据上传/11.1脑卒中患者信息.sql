/* 11.1脑卒中患者信息 - EHR_CEREBRAL_APOPLEXY_INFO */

SELECT a.个人档案编号+convert(varchar,a.[ID]) AS ID
	,RIGHT(a.个人档案编号,17) as HEALTH_RECORD_CODE
	,LEFT(c.姓名,50) as NAME
	,case when DATALENGTH(c.身份证号)>18 then '' else c.身份证号 end AS PERSONAL_ID
	,case when DATALENGTH(c.身份证号)>18 then '' else c.身份证号 end AS CARD_ID
	,'1' AS OTHER_CARD_TYPE
	,'' AS OTHER_CARD_ID
	,isnull(LEFT(a.脑卒中分类,1),'7') AS CEREBRAL_APOPLEXY_DIAGNOSIS
	,isnull((select P_DESC from tb_常用字典 where P_FUN_CODE='nzzfl'and P_CODE=a.脑卒中分类),'未分类脑卒中') as CEREBRAL_APO_DIAGNOSIS_NAME
	,'1' AS DIAGNOSIS_BASIS
	,'临床症状' as DIAGNOSIS_BASIS_NAME
	,isnull(LEFT(a.脑卒中分类,1),'7') AS DIAGNOSIS_CLASSIFICATION
	,isnull((select P_DESC from tb_常用字典 where P_FUN_CODE='nzzfl'and P_CODE=a.脑卒中分类),'未分类脑卒中') as DIAGNOSIS_CLASS_NAME
	,case when ISDATE(a.发病时间)=0 then '19000101' else REPLACE(CONVERT(varchar,CONVERT(date,a.发病时间)),'-','') end AS MORBIDITY_DATE   
	,case when ISDATE(a.发病时间)=0 then '19000101' else REPLACE(CONVERT(varchar,CONVERT(date,a.发病时间)),'-','') end AS DIAGNOSIS_DATE
	,'1' AS FIRST_MORBIDITY
	,LEFT(REPLACE(a.创建时间,'-',''),8) AS COLLECT_DATE
	,a.[创建机构] AS COLLECT_ORG_CODE
	,(SELECT 机构名称 FROM tb_机构信息 b WHERE a.创建机构=b.机构编号)  AS COLLECT_ORG_NAME
	,RIGHT(a.创建人,18) AS COLLECT_PERSONNEL_CODE
	,(SELECT 机构名称 FROM tb_机构信息 b WHERE a.创建机构=b.机构编号)  AS ORG_NAME
	,a.[创建机构] AS ORG_CODE
	,LEFT(c.区,6) as DISTRICT_CODE
	,GETDATE() as SYSTEM_TIME
	,'INSERT' as EXCHANGE_TYPE
FROM [tb_MXB脑卒中管理卡] a
LEFT JOIN tb_健康档案 c ON c.个人档案编号=a.个人档案编号
WHERE 1=1 
  AND c.身份证号<>''
  and a.创建时间>=convert(varchar,DATEADD(day,-10,getdate()),120)
  and a.创建时间<convert(varchar,getdate(),120)

--and not exists
--(select 1 from tb_上传日志 b where (a.个人档案编号+convert(varchar,a.ID)) = b.D001 and 功能号='EHR_CEREBRAL_APOPLEXY_INFO')
