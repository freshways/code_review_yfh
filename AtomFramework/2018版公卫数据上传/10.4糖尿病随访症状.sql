/* 10.4糖尿病随访症状 - EHR_DCV_SYMPTOM */

SELECT a.个人档案编号+convert(varchar,a.ID)  AS ID
      ,a.ID  AS DCV_ID
      ,substring(目前症状,1,1) AS PE_SYMPTOM_CD
      ,(SELECT P_DESC FROM dbo.tb_常用字典  WHERE P_FUN_CODE='tnbmqzz' AND substring(P_CODE,1,1) =substring(目前症状,1,1))SYMPTOM_NAME
      ,convert(datetime,case when isdate(a.创建时间)<>'1' then '1999-01-01' else a.创建时间 end) AS CREATED_AT
	  ,left(a.个人档案编号,6) as DISTRICT_CODE 
      ,'INSERT' as EXCHANGE_TYPE
	  ,GETDATE() as SYSTEM_TIME
	  ,ISNULL((SELECT 机构名称 FROM tb_机构信息 c WHERE c.机构编号=b.创建机构),'未知') AS ORG_NAME	  
	  ,b.创建机构 AS ORG_CODE 
      ,RIGHT(a.个人档案编号,17) AS HEALTH_RECORD_CODE
  FROM tb_MXB糖尿病随访表 a
  left join View_个人信息表 b on a.个人档案编号 = b.个人档案编号
  WHERE 1=1
  AND b.身份证号<>''
  and a.创建时间>=DATEADD(day,-1,getdate())
  and a.创建时间<getdate()

  --and  not exists(
  --select 1 from tb_上传日志 rz where (a.个人档案编号+convert(varchar,a.ID)) = rz.D001 and 功能号='EHR_DCV_SYMPTOM')
