/* 2.13.1重性精神疾病患者管理表 - EHR_SEVER_PSYCHOSIS_MANAGE */

SELECT a.个人档案编号+convert(varchar,d.ID) as ID
      ,RIGHT(a.[个人档案编号],17) AS HELTH_ARCHIVES_CODE
	  ,a.ID as MENTAL_MESSAGE_CODE
	  ,d.ID as MENTAL_RECORD_FORM_CODE
	  ,'' as MEDICAL_RECORD_NUM
	  ,e.姓名 as NAME
	  ,substring(e.性别,1,2) as GENDER_CODE
	  ,e.身份证号 as ID_CODE
	  ,'1' AS ID_TYPE
      ,a.监护人姓名 as CUSTODIAN_NAME
      ,case a.与患者关系 when '2' then '10' when '3' then '20' when '4' then '40'  when '5' then '50' when '6' then '60' when '7' then '70' when '13' then '60'  when '8' then '38'  when '9' then '28' else '80' end as CUSTODIAN_RELATION
	  ,'' as CUSTODIAN_RELATION_NAME
	  ,(SELECT top 1 [地区名称] FROM [dbo].[tb_地区档案] where 地区编码= e.省) as PERMANENT_ADDRESS_PROVINCE
	  ,(SELECT top 1 [地区名称] FROM [dbo].[tb_地区档案] where 地区编码= e.市) as PERMANENT_ADDRESS_CITY
	  ,(SELECT top 1 [地区名称] FROM [dbo].[tb_地区档案] where 地区编码=e.区) as PERMANENT_ADDRESS_AREA
	  ,(SELECT top 1 [地区名称] FROM [dbo].[tb_地区档案] where 地区编码= e.街道) as PERMANENT_ADDRESS_VILLAGE
	  ,(SELECT top 1 [地区名称] FROM [dbo].[tb_地区档案] where 地区编码= e.居委会) as PERMANENT_ADDRESS_STREET
	  ,'' as PERMANENT_ADDRESS_NUM
	  ,'276200' as PERMANENT_ADDRESS_POSTCODE
	  ,(SELECT top 1 [地区名称] FROM [dbo].[tb_地区档案] where 地区编码= e.省) as LIVE_ADDRESS_PROVINCE
	  ,(SELECT top 1 [地区名称] FROM [dbo].[tb_地区档案] where 地区编码= e.市) as LIVE_ADDRESS_CITY
	  ,(SELECT top 1 [地区名称] FROM [dbo].[tb_地区档案] where 地区编码=e.区) as LIVE_ADDRESS_AREA
	  ,(SELECT top 1 [地区名称] FROM [dbo].[tb_地区档案] where 地区编码= e.街道) as LIVE_ADDRESS_VILLAGE
	  ,(SELECT top 1 [地区名称] FROM [dbo].[tb_地区档案] where 地区编码= e.居委会) as LIVE_ADDRESS_STREET
	  ,'' as LIVE_ADDRESS_NUM
	  ,'276200' as LIVE_ADDRESS_POSTCODE
	  ,(SELECT top 1 [地区名称] FROM [dbo].[tb_地区档案] where 地区编码= e.省) as CUSTODIAN_ADDRESS_PROVINCE
	  ,(SELECT top 1 [地区名称] FROM [dbo].[tb_地区档案] where 地区编码= e.市) as CUSTODIAN_ADDRESS_CITY
	  ,(SELECT top 1 [地区名称] FROM [dbo].[tb_地区档案] where 地区编码=e.区) as CUSTODIAN_ADDRESS_AREA
	  ,'' as CUSTODIAN_ADDRESS_VILLAGE
	  ,'' as CUSTODIAN_ADDRESS_STREET
	  ,a.监护人住址 as CUSTODIAN_ADDRESS_NUM
	  ,'276200' as CUSTODIAN_ADDRESS_POSTCODE
	  ,case when datalength(a.联系电话)>20 then '0' else a.联系电话 end  as HER_PHONE_NUM
	  ,'' as HER_PHONE_TYPE
	  ,case when datalength(a.监护人电话)>20 then '0' else a.监护人电话 end as CUSTODIAN_PHONE_NUM
	  ,'' as CUSTODIAN_PHONE_TYPE
	  ,(select top 1 精神专科医师姓名 from tb_精神病随访记录 where 个人档案编号=e.个人档案编号) as RESPON_DOCTOR_NAME
	  ,'' as RESPON_DOCTOR_PHONE_TYPE
	  ,(select top 1 case when datalength(精神专科医师电话)>20 then '0' else 精神专科医师电话 end from tb_精神病随访记录 where 个人档案编号=e.个人档案编号) as RESPON_DOCTOR_PHONE_NUM
	  ,(SELECT top 1 [地区名称] FROM [dbo].[tb_地区档案] where 地区编码= e.居委会) as COM_NAME
	  ,'' as COM_CONTACTS_NAME
	  ,'' as HER_COM_CONTACTS_PHONE_TYPE
	  ,'' as HER_COM_CONTACTS_PHONE_NUM
	  ,case a.知情同意 when '1' then '1' else '0' end as INFORMED_AGREE_SIGN
	  ,case when ISDATE(a.签字时间)=0 then '19000101' else replace(CONVERT(varchar(10),convert(datetime,a.签字时间),120),'-','') end as INFORMED_AGREE_DATE
	  ,a.签字 as INFORMED_AGREE_NAME
	  ,case when ISNULL(REPLACE(a.初次发病时间,'-',''),'')='' then 0 
			 when isnumeric(substring(replace(e.出生日期,'-',''),1,4))=0 then 0
			 when convert(int,substring(replace(a.初次发病时间,'-',''),1,4)) - convert(int,substring(replace(e.出生日期,'-',''),1,4))<=0 then 0
			 when LEN(convert(int,substring(replace(a.初次发病时间,'-',''),1,4)) - convert(int,substring(replace(e.出生日期,'-',''),1,4)))>=3 then 0
             when  isnull(replace(e.出生日期,'-',''),'')='' then 0
	         else convert(int,substring(replace(a.初次发病时间,'-',''),1,4)) - convert(int,substring(replace(e.出生日期,'-',''),1,4)) end as FIRST_MORBIDITY_DATE
	  ,'0' as CASE_MANAGE_SIGN
	  ,'0' as CASE_MANAGE_CODE
	  ,'0' as CASE_MANAGE_JUDGE_CODE
	  ,case when isnull((select top 1 d.目前症状 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号),'')='' 
	  then '99' when isnumeric((select top 1 d.目前症状 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号))='0' 
	  then '99' 
	  else  convert(int,SUBSTRING((select top 1 d.目前症状 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号),1,1)) end as MENTAL_DISEASE_CODE
	  ,'其他' as MENTAL_DISEASE_NAME
	  ,case when isnull((select top 1 f.家族病史 from tb_健康档案_家族病史 f where f.个人档案编号=e.个人档案编号),'')='' then '2' else '1' end as 家族史标志
	  ,case a.门诊状况 when '1' then '1' when '2' then '2' when '3' then '3' else '1' end as 既往门诊治疗情况代码
	  ,'' as 抗精神病药物治疗标志
	  ,case when isdate(a.首次抗精神病药治疗时间)=0 then '19000101' else replace(replace(a.首次抗精神病药治疗时间,'-',''),'/','') end as 首次抗精神病药物治疗日期
	  ,case when ISNULL(a.住院次数,'')='' then '0' when ISNUMERIC(a.住院次数)='0' then '0' when DATALENGTH(a.住院次数)>2 then '0' else convert(int,a.住院次数) end as 既往精神专科住院次数
	  ,'9' as 重症精神疾病名称代码
	  ,a.确诊医院 as 确诊医疗机构名称
	  ,case when ISDATE(a.确诊日期)=0 then '19000101' else replace(convert(varchar(10),convert(date,a.确诊日期,120)),'-','') end as 确诊日期
	  ,case when ISNUMERIC( a.既往主要症状)='0' then '99' else substring(a.既往主要症状,1,1) end as 既往精神类疾病诊断名称
	  ,'1' as 既往治疗效果类别代码表
	  ,'0' as 患病对家庭社会的影响次数  
	  ,case substring(a.对家庭和社会的影响,1,1) when '1' then '2' when '2' then '3' when '3' then '4' when '4' then '5' when '5' then '6' when '6' then '1' when '7' then '9' else '9' end as 患病对家庭社会的影响类别代码
	  ,case a.经济状况 when '1' then '1' when '2' then '2' when '3' then '3' else '3' end as 经济状况代码
	  ,case a.关锁情况 when '1' then '1' when '2' then '2' when '3' then '3' else '3' end as 关锁情况代码
	  --,substring(a.医疗支付类型,1,4) as 医疗补助来源
	  ,substring(a.医疗支付类型,1,4) as 医疗补助来源代码
	  ,(SELECT UserName FROM [dbo].[tb_MyUser] where 用户编码=a.创建人) as 填报人姓名
	  ,case when ISDATE(a.创建时间)=0 then '19000101' else convert(varchar(15),cast(a.创建时间 as datetime),112) end as 填报日期
	  ,a.医生意见 as 专科医生意见
	  ,case when isdate(substring(replace((select top 1 d.随访日期 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号),'-',''),1,10))=0 then '19000101' else substring(replace((select top 1 d.随访日期 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号),'-',''),1,10) end as 本次随访日期
	  ,(select top 1 危险性 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号) as 危险性分级代码
	  ,(select top 1 自知力 from tb_精神病随访记录 where 个人档案编号=e.个人档案编号) as 自知力评价结果代码
	  ,case (select top 1 睡眠情况 from tb_精神病随访记录 where 个人档案编号=e.个人档案编号) when '1' then '1' when '2' then '2' when '3' then '3' else '3' end as 睡眠情况代码
	  ,case when isnull((select top 1 d.学习能力 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号),'')='' 
	  then 3 else substring((select top 1 d.学习能力 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号),1,1) end as 社会功能情况分类代码
	  ,case (select top 1 d.影响 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号) when '1' then '1' when '2' then '2' else '1' end as 社会功能情况评价代码
	  ,(select top 1 生产劳动及工作 from tb_精神病随访记录 where 个人档案编号=e.个人档案编号) as 生活和劳动能力评价结果代码
	  ,(select top 1 死亡原因_躯体疾病 from tb_精神病随访记录 where 个人档案编号=e.个人档案编号) as 躯体疾病名称
	  ,case (select top 1 住院情况 from tb_精神病随访记录 where 个人档案编号=e.个人档案编号) when '1' then '1' when '2' then '2' when '3' then '3' else '1' end as 住院情况代码
	  ,case when isnull((select top 1 d.末次出院时间 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号),'')='' then '19000101'
	   when(select top 1 d.末次出院时间 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号) not like '%年%' then '19000101'
	   when isdate((select top 1 d.末次出院时间 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号))=0 then '19000101'
	   else substring(replace(replace(replace((select top 1 d.末次出院时间 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号),'月',''),'年',''),'-',''),1,10) end as 末次出院日期
	  ,case(select top 1 d.实验室检查 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号) when '1' then '1' else '0' end as 辅助检查标志
	  ,'' as 辅助检查项目
	  ,'' as 辅助检查结果
	  ,'' as 检查人员姓名
	  ,'19900101' as 检查日期
	  ,'' as 应急医疗处置标志
	  ,'' as 应急医疗处置缘由代码
	  ,'0' as 应急医疗缘由次数
	  ,'' as 应急医疗处置措施代码 
	  ,'0' as 应急医疗处置措施次数
	  ,'' as 应急医疗处置诊断情况代码
	  ,'0' as 应急医疗处置性质代码 
	  ,'0' as 应急医疗处置对象来源代码
	 ,case (select top 1 d.服药依从性 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号) when '1'then '1' when '2' then '2' when '3' then '3' else '1' end as 服药依从性代码
	 ,case (select top 1 d.药物不良反应有无 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号) when '1' then '1' else '0' end as 药物不良反应标志
	 ,(select top 1 d.药物不良反应 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号) as 服药不良反应描述
	 ,case (select top 1 d.是否转诊 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号) when '1' then '1' else '0' end as 转诊标志
	 ,(select top 1 d.转诊原因 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号) as 转诊原因
	 ,(select top 1 d.转诊机构 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号) as 转入医疗机构名称
	 ,(select top 1 d.转诊科室 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号) as 转入医疗机构科室
	 ,'1' as 中药类别代码
	 ,'' as 药物名称
	 ,'' as 药物使用频率
	 ,'9' as 药物使用途径代码
	 ,'0' as 药物使用总剂量
	 ,'0' as 药物使用次剂量
	 ,'' as 药物使用剂量单位
	 ,'0' as 重性精神疾病患者干预类型代码
	  ,left(e.区,6) as 行政区划代码
	 ,(select top 1 机构名称 from tb_机构信息 where 机构编号=e.所属机构) as 登记机构名称
	 ,e.创建机构 as 登记机构代码
	 ,case (select top 1 d.饮食情况 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号) when '1' then '1' when '2' then '2' when '3' then '3' else '9' end as 饮食情况代码
	 ,(select top 1 d.此次随访分类 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号) as 访到标志
	 ,'' as 评价结果代码
	,case when isdate((select top 1 d.下次随访日期 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号))=0 then '19000101' else replace(CONVERT(varchar(10),(select top 1 d.下次随访日期 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号),120),'-','') end as 下次随访日期
	 ,(select top 1 d.随访医生 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号) as 随访医生签名
	 ,case (substring((select top 1 d.康复措施 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号),1,1)) when '1' then '1' when '2' then '2' when '3' then '3' when '4' then '4' else '9' end as 精神康复措施代码
	 ,(select top 1 d.康复措施其他 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号) as 康复措施指导
	,(convert(int,CONVERT(varchar(4),GETDATE(),120))) - convert(int,case when ISNULL(e.出生日期,'')='' then 0 else left(e.出生日期,4) end) as 实际年龄
	 , case when ISDATE(e.出生日期)=0 then '19000101' else replace(substring(convert(varchar(10),convert(date,e.出生日期)),1,10),'-','') end as 出生日期
	 ,a.医疗支付类型 as 医疗费用支付方式代码
	 ,(select top 1 d.转诊机构 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号) as 接诊机构
	 ,(select top 1 d.转诊科室 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号) as 接诊科室名称
	 ,(select top 1 机构名称 from tb_机构信息 where 机构编号 in (select top 1 创建机构 from tb_精神病随访记录 d where d.个人档案编号=e.个人档案编号)) as 填报单位名称
	 ,'19900101' as 终止管理日期
	 ,'' as 终止管理原因
	 ,'' as 诊断科室代码
	 ,'' as 诊断医师姓名
	 ,'' as 其他辅助检查结果
	 , case when ISDATE(a.检查日期)=0 then '19000101' else replace(substring(convert(varchar(10),convert(date,a.检查日期)),1,10),'-','') end as 指导日期
  FROM [dbo].[tb_精神疾病信息补充表] a
   left join [tb_健康档案] e on e.个人档案编号=a.个人档案编号
   left join [tb_精神病随访记录] d on a.个人档案编号=d.个人档案编号 and substring(d.随访日期,1,4)=substring(a.检查日期,1,4)
   WHERE 1=1
   and ISNULL(e.身份证号,'')!=''
   and ISNULL(e.个人档案编号,'')!=''
   and d.ID!=''
     and not exists(
select 1 from tb_上传日志 rz where (a.个人档案编号+convert(varchar,d.[ID])) = rz.D001 and 功能号='EHR_SEVER_PSYCHOSIS_MANAGE')