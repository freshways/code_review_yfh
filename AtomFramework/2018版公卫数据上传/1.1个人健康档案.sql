/* 公共卫生个人基本档案(C-1) - EHR_PIR */

SELECT  a.个人档案编号+convert(varchar,a.[ID]) as ID  
	  ,left(a.个人档案编号,6) as DISTRICT_CODE
      ,'INSERT' as EXCHANGE_TYPE
	  ,GETDATE() as SYSTEM_TIME
      ,RIGHT(a.[个人档案编号],17) AS HEALTH_RECORD_CODE
      ,a.姓名 as NAME
      ,case when ISnumeric(a.性别)=0 then '1' else substring(a.[性别],1,1) end GENDER_CD
      ,(SELECT P_DESC FROM tb_常用字典 b WHERE b.P_CODE=a.性别 AND b.P_FUN_DESC='性别') AS GENDER_NAME 
      ,case when ISDATE(a.[出生日期])=0 then '19000101' else REPLACE(CONVERT(varchar(10),convert(date,a.[出生日期]),120),'-','') end BIRTH_DATE
	  ,case when ISDATE(a.[出生日期])=0 then '0' else DATEDIFF(year,a.出生日期,getdate()) end AGE
      ,case when ISNULL(a.[证件类型],'')='' then '1' else substring(a.证件类型,1,3) end AS ORTHER_ID_NO_CD
	  ,'' as ORTHER_ID_NO_NAME
	  ,'' as ORTHER_ID_NO    
      ,case when isnull(a.身份证号,'')='' then '未知' else substring(a.身份证号,1,18) end ID_NO
      ,a.[工作单位] AS WOKING_UNIT_NAME
      ,SUBSTRING(a.[本人电话],1,20) as TEL
      ,case when isnull(a.[联系人姓名],'')='' then a.姓名 else a.[联系人姓名] end LINKMAN
      ,case when isnull(a.[联系人电话],'')='' then a.本人电话 else SUBSTRING(a.[联系人电话],1,15) end  as LINKMAN_TEL
      ,a.档案类别 as AD_REGISTER_MARK
	  ,case a.档案类别 when '1' then '城市' when '2' then '乡村' end AD_REGISTER_NAME
      ,a.[居住地址] AS ADDR
      ,'' AS REG_ADDR 
      ,substring(a.[民族],1,2) as NATION_CD
	  ,(select P_DESC from tb_常用字典 where P_FUN_DESC ='民族'and P_CODE=a.民族) as NATION
	  ,case when isnull(c.血型,'')='' then '5' else substring(c.血型,1,1) end ABO_CD
	  ,isnull((select P_DESC from tb_常用字典 where P_FUN_DESC ='血型' and P_CODE=c.血型),'不详') as ABO_NAME
      ,substring(c.RH,1,1) AS RH_CD
	  ,(select P_DESC from tb_常用字典 where P_FUN_DESC ='rh' and P_CODE=c.RH) as RH_NAME
      ,substring(a.[文化程度],1,2) AS ED_BG_CD
	  ,substring((select P_DESC from tb_常用字典 where P_FUN_DESC ='文化程度' and P_CODE=a.[文化程度]),1,20) as ED_BG_NAME
      ,case when isnull(a.[职业],'')='' then '4' else substring(a.[职业],1,20) end OCCU_TYPE_CD
	  ,isnull((select P_DESC from tb_常用字典 where P_FUN_DESC ='职业' and P_CODE=a.职业),'农林牧渔水利业生产人员') as OCCU_TYPE_NAME
	  ,case when ISNULL(a.[婚姻状况],'')='' then '90' else substring(a.[婚姻状况],1,2) end MARITAL_ST_CD
	  ,(select P_DESC from tb_常用字典 where P_FUN_DESC ='婚姻状况' and P_CODE=a.[婚姻状况]) as MARITAL_ST_NAME
	  ,case when ISNULL(a.[医疗费支付类型],'')='' then '99' 
			when SUBSTRING(a.[医疗费支付类型],1,1)='9' then '99'
			else SUBSTRING(a.[医疗费支付类型],1,1) end  AS MEDICARE_CD  
      ,substring(case when ISNULL(a.医疗费支付类型,'')='' then '不祥' else
	  STUFF((select ','+P_DESC from [tb_常用字典] where P_FUN_CODE='ylfzflx' 
	   and CHARINDEX(','+P_CODE+',',','+a.医疗费支付类型+',')>0 for xml path('')),1,1,'') end,1,25) AS MEDICARE_NAME
      ,case when ISNULL(c.药物过敏史,'')='' then '1' 
			when SUBSTRING(c.药物过敏史,1,1)='99' then '7'
		    else SUBSTRING(c.药物过敏史,1,1) end ALLERGY_DRUG
	  ,case when ISNULL(c.药物过敏史,'')='' then '无' 
			when SUBSTRING(c.药物过敏史,1,1)='99' then '其他'
		    else SUBSTRING(c.药物过敏史,1,1) end ALLERGY_NAME
      ,case when d.疾病类型='疾病' then SUBSTRING(d.疾病名称,1,1) else '1'end IS_DISEASE 
      ,case when d.疾病类型='外伤' then '1' else '2' end IS_TRAUMA
      ,case when d.疾病类型='手术' then '1' else '2' end IS_OPERATION
      ,case when d.疾病类型='输血' then '1' else '2' end IS_TRANSFUSION      
	  ,case when ISNULL(e.家族病史,'')!='' then '1' else '2' end AS IS_FAMILY_DISEASE
      ,substring(c.遗传病史有无,1,1) AS IS_GENETIC
	  ,case when ISNULL(c.过敏史有无,'')='' then 2 else substring(c.过敏史有无,1,1) end IS_ALLERGY
	  ,substring(c.遗传病史,1,1) AS GENETIC_NAME
	  ,substring(c.遗传病史,1,1) as GENETIC_DE
	  ,SUBSTRING(c.残疾情况,1,1) AS DISABILITY_CD
      ,STUFF((select ','+P_DESC from [tb_常用字典] where P_FUN_CODE='cjjk' 
	   and CHARINDEX(','+P_CODE+',',','+c.残疾情况+',')>0 for xml path('')),1,1,'') AS DISABILITY_NAME
      ,case when isnull(a.厨房排气设施,'')='' then '2' else '1' end  KITCHEN_EXHAUST_FLAG
	  ,case when isnull(a.厨房排气设施,'')='' then '1' else SUBSTRING(a.[厨房排气设施],1,1) end  KITCHEN_EXHAUST_FAC_CD
	  ,STUFF((select ','+P_DESC from [tb_常用字典] where P_FUN_CODE='cf_pqsb' 
	   and CHARINDEX(','+P_CODE+',',','+a.[厨房排气设施]+',')>0 for xml path('')),1,1,'') AS KITCHEN_EXHAUST_FAC_NM
      ,case when isnull(a.[燃料类型],'')='' then '1' else SUBSTRING(a.[燃料类型],1,1) end FUEL_TYPE_CD
	  ,STUFF((select ','+P_DESC from [tb_常用字典] where P_FUN_CODE='ranliao_lx' 
	   and CHARINDEX(','+P_CODE+',',','+a.[燃料类型]+',')>0 for xml path('')),1,1,'') AS FUEL_TYPE_NM
      ,case when isnull(a.饮水,'')='' then '1' else SUBSTRING(a.饮水,1,1) end WATER_TYPE_CD
	  ,STUFF((select ','+P_DESC from [tb_常用字典] where P_FUN_CODE='yinshui' 
	   and CHARINDEX(','+P_CODE+',',','+a.饮水+',')>0 for xml path('')),1,1,'') AS WATER_TYPE_NM
      ,case when isnull(a.[厕所],'')='' then '1' else SUBSTRING(a.[厕所],1,1) end TOILET_TYPE_CD
	  ,STUFF((select ','+P_DESC from [tb_常用字典] where P_FUN_CODE='cesuo' 
	   and CHARINDEX(','+P_CODE+',',','+a.[厕所]+',')>0 for xml path('')),1,1,'') AS TOILET_TYPE_NM
      ,case when isnull(a.禽畜栏,'')='' then '1' else SUBSTRING(a.禽畜栏,1,1) end LIVESTOCK_TYPE_CD
	  ,STUFF((select ','+P_DESC from [tb_常用字典] where P_FUN_CODE='qinxulan' 
	   and CHARINDEX(','+P_CODE+',',','+a.禽畜栏+',')>0 for xml path('')),1,1,'') AS LIVESTOCK_TYPE_NM
      ,'0' AS PERSONAL_STATUS_CODE  
      ,'未知' AS PERSONAL_STATUS_NAME
      ,'0' AS COMMUNITY_CODE
      ,'未知' AS COMMUNITY_NAME
      ,(SELECT [地区名称] FROM tb_地区档案 b WHERE b.地区编码=a.[街道]) AS COUNTY_NAME
      ,(SELECT [地区名称] FROM tb_地区档案 b WHERE b.地区编码=a.[居委会]) AS VILLAGE_NAME 
      ,ISNULL(a.[创建人],'未知') AS FILING_MAN_CODE
      ,case when ISNULL((SELECT UserName FROM tb_MyUser b WHERE a.创建人=b.用户编码),'')='' then '未知'
			else (SELECT UserName FROM tb_MyUser b WHERE a.创建人=b.用户编码) end FILING_MAN
	  ,case when ISNULL((SELECT Tel FROM tb_MyUser b WHERE a.创建人=b.用户编码),'')='' then '未知'
			else (SELECT b.Tel FROM tb_MyUser b WHERE a.创建人=b.用户编码) end  FILING_PHONE
	  ,convert(varchar(8),convert(date,case when ISDATE(a.创建时间)=0 then '19000101' else a.创建时间 end),112) as BUILD_DATE
	  ,convert(varchar,convert(date,case when ISDATE(a.创建时间)=0 then '19000101' else a.修改时间 end),112) as UP_DATE
	  ,'19000101' DE_DATE
	  ,case when ISNULL((SELECT UserName FROM tb_MyUser b WHERE a.修改人=b.用户编码),'')='' then '未知'
			else (SELECT UserName FROM tb_MyUser b WHERE a.创建人=b.用户编码) end UP_OPERNAME
      ,'0' AS DOCTOR_CODE
      ,case when ISNULL((SELECT UserName FROM tb_MyUser b WHERE a.创建人=b.用户编码),'')='' then '未知'
			else (SELECT UserName FROM tb_MyUser b WHERE a.创建人=b.用户编码) end DOCTOR_NAME
	  ,'未知' as DOCTOR_PHONE
      ,ISNULL((SELECT 机构名称 FROM tb_机构信息 b WHERE a.创建机构=b.机构编号),'未知') AS ORG_NAME 
      ,a.创建机构 AS ORG_CODE
	  ,ISNULL((SELECT 机构名称 FROM tb_机构信息 b WHERE a.所属机构=b.机构编号),'未知') AS S_ORG_NAME 
      ,a.所属机构 AS S_ORG_CODE
	  ,substring(c.暴露史,1,1) AS ERF_EXPOSURE_CODE
	  ,a.新农合号 as RURAL_COOPERATIVES_CD
	  ,a.职工医疗保险卡号 as PROVINCE_INSURANCE_CARD
	  ,a.居民医疗保险卡号 as MEDICAL_INSURANCE_CARD
	  ,a.贫困救助卡号 as POVERTY_RELIEF_CARD
	  ,a.医疗费用支付类型其他 as OTHER_PAYMENT
	  ,c.过敏史有无 as  DRUG_ALLERGY_FLAG
	  ,c.过敏史其他 as DRUG_ALLERGY_OTHER
	  ,case when c.暴露史='1' then '1' else '0' end OCCUPATION_FLAG
	  ,'0' as OCCUPATION_CLASS
	  ,'未知' as OCCUPATION_CLASS_NAME
	  ,a.户主姓名 as HOUSEHOLDER_NAME
	  ,a.户主身份证号 as ID_CARD_NUM
	  ,a.家庭人口数 as FAMILY_MEMBER_NUM
	  ,a.家庭结构 as FAMILY_STRUCTURE
	  ,a.居住情况 as LIVING_CONDITION_CODE
	  ,ISNULL((SELECT P_DESC FROM tb_常用字典 b WHERE P_FUN_CODE='jzqk' and a.居住情况=b.P_CODE),'未知') AS LIVING_CONDITION
	  ,case when isnull(a.本人或家属签字,'')<>'' then a.姓名 end SIGN
	  ,'' as FAMILY_MEMBERS_SIGN
	  ,convert(varchar,convert(date,case when ISDATE(a.签字时间)=0 then '19000101' else a.签字时间 end),112) as SIGN_TIME
	  ,case when isnull(a.怀孕情况,'未孕')='未孕' then '0' else '1' end MATERNAL_FLAG
	  ,'19000101' MATERNAL_TIME_FLAG
	  ,'未知' as CHILD_FLAG
	  ,'未知' as AGED_FLAG
	  ,'未知' HYPERTENSION_FLAG
	  ,'未知' DIABETES_FLAG
	  ,'未知' MENTAL_DISEASE_FLAG
	  ,'未知' CORONARY_DISEASE_FLAG
	  ,'未知' STROKE_FLAG
	  ,'未知' DEFORMITY_FLAG
	  ,'未知' PUL_TUBERCULOSIS_FLAG
	  ,a.是否贫困人口 as DISTRESS_FLAG
	  ,'0' HIGH_RISK_MATERNAL_FLAG
	  ,'0' HIGH_RISK_CHILD_FLAG
	  ,'0' ASTHMA_FLAG
	  ,case when a.与户主关系='1' then '1' else '0' end HOUSEHOLDER_FLAG
	  ,'276400' as POSTRALCODE
	  ,'' HOME_PHONE_NUM
	  ,a.户主姓名 REGISTERED_HOLDER_NAME
	  ,a.与户主关系 as RELATIONSHIP_HOUSEHOLDER
	  ,'未知' BIRTH_CERTIFICATE
	  ,'未知' PASSPORT_NUM
	  ,'未知' VACCINATE_CARD_NUM
	  ,'未知' F_GUARDIAN_NAME
	  ,'未知' F_GUARDIAN_RELATION
	  ,'未知' F_GUARDIAN_PHONE
	  ,'未知' S_GUARDIAN_NAME
	  ,'未知' S_GUARDIAN_RELATION
	  ,'未知' S_GUARDIAN_PHONE
	  ,'未知' FATHER_NAME
	  ,'未知' FATHER_ID_CARD_NUM
	  ,'未知' MOTHER_NAME
	  ,'未知' MOTHER_ID_CARD_NUM
	  ,'未知' FATHER_OCCUPATION
	  ,'未知' MOTHER_OCCUPATION
	  ,'未知' MOTHER_WORK_UNIT
	  ,'未知' FATHER_WORK_UNIT
	  ,'未知' SPOUSE_OCCUPATION
	  ,'未知' SPOUSE_EDUCATION_CODE
	  ,'未知' SPOUSE_EDUCATION_NAME
	  ,'19000101' SPOUSE_BIRTHDAY
	  ,'未知' SPOUSE_NAME
	  ,'未知' SPOUSE_ID_CARD_NUM
	  ,'未知' SPOUSE_REGISTER_ADDRESS
	  ,'未知' SPOUSE_PHONE
	  ,'未知' WORK_UNIT_SCHOOL_PHONE
	  ,a.档案位置 as ARCHIVES_PLACE
	  ,'未知' VILLAGE_TEAM
	  ,'未知' VILLAGE_SERVICE_TYPE
	  ,a.完整度 as ARCHIVES_COMPLETENESS
	  ,'0' CANCEL_FLAG
	  ,'' REHABILITATE_ADDRESS
	  ,'' REHA_ADDRESS_POSTRALCODE
	  ,'0' ADDRESS
	  ,'9' FAMILY_INCOME
	  ,a.档案位置 as ARCHIVES_PLACE_THIS
	  ,'1' as ARCHIVES_TYPE
	  ,'' as HEALTH_INCIDENT_NAME
	  ,'' as HEALTH_INCIDENT_DATE
	  ,'' as HEALTH_INCIDENT_ADDRESS
	  ,'156' as NATIONALITY
	  ,'1' as PERMANENT_FLAG
	  ,'' as E_MAIL
	  ,'' as REGISTER_PROVINCE
	  ,'' as REGISTER_CITY
	  ,'' as REGISTER_COUNTY
	  ,'' as REGISTER_TOWN
	  ,'' as REGISTER_VILLAGE
	  ,'' as REGISTER_HOUSE_NUM
	  ,'' as POSTRALCODE_ADDRESS
	  ,(SELECT top 1 地区名称 FROM dbo.tb_地区档案 WHERE 地区编码 = 省) as PRESENT_ADDRESS_PROVINCE
	  ,(SELECT top 1 地区名称 FROM dbo.tb_地区档案 WHERE 地区编码 = 市) as PRESENT_ADDRESS_CITY
	  ,(SELECT top 1 地区名称 FROM dbo.tb_地区档案 WHERE 地区编码 = 区) as PRESENT_ADDRESS_COUNTY
	  ,(SELECT top 1 地区名称 FROM dbo.tb_地区档案 WHERE 地区编码 = 街道) as PRESENT_ADDRESS_TOWN
	  ,(SELECT top 1 地区名称 FROM dbo.tb_地区档案 WHERE 地区编码 = 居委会) as PRESENT_ADDRESS_VILLAGE
	  ,'' as PRESENT_ADDRESS_HOUSE_NUM
	  ,'276400' as POSTRALCODE_NOW
	  ,'其他' as INSURANCE_CLASS
	  ,'99' as INSURANCE_CODE
	  ,'0' as DELETE_FLAG
      ,'未知' as CITIZEN_ID
	  ,'未知' as EFFECTIVE_TIME
	  ,'未知' as STATUS
	  ,convert(datetime,case when ISDATE(a.创建时间)=0 then '19000101' else a.创建时间 end) AS CREATED_AT
	  ,'未知' as DEI
	  ,'未知' as NODE_CODE 
  FROM [tb_健康档案] a 
  left join tb_健康档案_健康状态 c on a.个人档案编号=c.个人档案编号  
  left join tb_健康档案_既往病史 d on a.个人档案编号=d.个人档案编号
  left join tb_健康档案_家族病史 e on a.个人档案编号=e.个人档案编号
  WHERE 1=1
  and isnull(a.[联系人电话],'')<>''
  and a.创建时间>=DATEADD(day,-1,getdate())
  and a.创建时间<getdate()


  --and not exists(
  --select 1 from tb_上传日志 rz where (a.个人档案编号+convert(varchar,a.[ID])) = rz.D001 and 功能号='EHR_PIR')

  --select * from tb_健康档案_健康状态 where ISNULL(药物过敏史,'')<>''
  --and 个人档案编号='371323160010071401'

