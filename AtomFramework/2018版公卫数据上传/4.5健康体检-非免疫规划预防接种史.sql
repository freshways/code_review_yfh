/* 健康体检-非免疫规划预防接种史（辅C-3标） - EHR_PE_INOCULATION */


SELECT  a.个人档案编号+convert(varchar,a.ID) as ID
      ,RIGHT(a.个人档案编号,17) as EXAM_ID 
	  ,'99' as VACCINE_CODE
	  ,'未知' as VACCINE_BATCH_NUMBER
	  ,a.接种名称 as VACCINE_NAME
	  ,case when isdate(a.接种日期)=0 then '19000101' else replace(convert(varchar(10),convert(date,a.接种日期,120)),'-','') end INOCULATE_DATE
	  ,'未知' as ORG_CODE
      ,isnull(a.接种机构,'未知') as ORG_NAME
      ,left(a.个人档案编号,6) as DISTRICT_CODE
      ,convert(datetime,case when isdate(a.创建日期)=0 then '19000101' else a.创建日期 end) as CREATED_AT
	  ,a.ID as SN
	  ,'INSERT' as EXCHANGE_TYPE
	  ,GETDATE() as SYSTEM_TIME
  FROM tb_健康体检_非免疫规划预防接种史 a 
  left join tb_健康体检 b on b.个人档案编号=a.个人档案编号 and a.创建日期 = b.创建时间
  where 1=1
  and a.接种名称<>''
  and b.创建时间>=DATEADD(day,-10,getdate())
  and b.创建时间<getdate()


  --and not exists(
  --select 1 from tb_上传日志 rz where (a.个人档案编号+convert(varchar,a.[ID])) = rz.D001 and 功能号='EHR_PE_INOCULATION')
