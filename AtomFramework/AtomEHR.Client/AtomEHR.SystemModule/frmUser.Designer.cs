namespace AtomEHR.SystemModule
{
    partial class frmUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUser));
            this.gcSummary = new DevExpress.XtraGrid.GridControl();
            this.gvSummary = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col用户编码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col机构 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEdit机构 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colTel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDomainName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNovellAccount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastLoginTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastLogoutTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsLocked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFlagAdmin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoginCounter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colsfz = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtPassword2 = new DevExpress.XtraEditors.TextEdit();
            this.txtPassword1 = new DevExpress.XtraEditors.TextEdit();
            this.labPassword1 = new System.Windows.Forms.Label();
            this.labPassword2 = new System.Windows.Forms.Label();
            this.chkIsLocked = new DevExpress.XtraEditors.CheckEdit();
            this.txtEmail = new DevExpress.XtraEditors.TextEdit();
            this.txtTel = new DevExpress.XtraEditors.TextEdit();
            this.txtUserName = new DevExpress.XtraEditors.TextEdit();
            this.txtAccount = new DevExpress.XtraEditors.TextEdit();
            this.labEmail = new System.Windows.Forms.Label();
            this.labAccount = new System.Windows.Forms.Label();
            this.labTel = new System.Windows.Forms.Label();
            this.labUserName = new System.Windows.Forms.Label();
            this.gcDetailEditor = new DevExpress.XtraEditors.GroupControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.lbMyGroups = new DevExpress.XtraEditors.ListBoxControl();
            this.ucDataSets = new AtomEHR.Library.UserControls.ucCheckedListBoxBinding();
            this.txtDomainName = new DevExpress.XtraEditors.MemoEdit();
            this.txtNovellAccount = new DevExpress.XtraEditors.MemoEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtLoginCounter = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtLastLogoutTime = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gcPassword = new DevExpress.XtraEditors.GroupControl();
            this.label4 = new System.Windows.Forms.Label();
            this.chkIsAdmin = new DevExpress.XtraEditors.CheckEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.txt用户ID = new DevExpress.XtraEditors.TextEdit();
            this.txt机构名称 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证 = new DevExpress.XtraEditors.TextEdit();
            this.txt机构编码 = new DevExpress.XtraEditors.ButtonEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.txt_姓名 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton对照 = new DevExpress.XtraEditors.SimpleButton();
            this.btnEmpty = new DevExpress.XtraEditors.SimpleButton();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txt_手机 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.txt_用户标识 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.txt机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.txt_是否 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            this.tpDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit机构)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsLocked.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetailEditor)).BeginInit();
            this.gcDetailEditor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbMyGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDomainName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNovellAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLoginCounter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastLogoutTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcPassword)).BeginInit();
            this.gcPassword.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsAdmin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt用户ID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt机构名称.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt机构编码.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_手机.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_用户标识.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_是否.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gcSummary);
            this.tpSummary.Controls.Add(this.panelControl3);
            this.tpSummary.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tpSummary.Size = new System.Drawing.Size(896, 442);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 28);
            this.pnlSummary.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlSummary.Size = new System.Drawing.Size(902, 471);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tcBusiness.Size = new System.Drawing.Size(902, 471);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Controls.Add(this.gcDetailEditor);
            this.tpDetail.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tpDetail.Size = new System.Drawing.Size(896, 442);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcNavigator.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gcNavigator.Size = new System.Drawing.Size(902, 28);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(724, 3);
            this.controlNavigatorSummary.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            // 
            // txtFocusForSave
            // 
            this.txtFocusForSave.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(527, 3);
            // 
            // gcSummary
            // 
            this.gcSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSummary.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcSummary.Location = new System.Drawing.Point(0, 54);
            this.gcSummary.MainView = this.gvSummary;
            this.gcSummary.Name = "gcSummary";
            this.gcSummary.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.LookUpEdit机构});
            this.gcSummary.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.gcSummary.Size = new System.Drawing.Size(896, 388);
            this.gcSummary.TabIndex = 7;
            this.gcSummary.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSummary});
            // 
            // gvSummary
            // 
            this.gvSummary.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col用户编码,
            this.colAccount,
            this.colUserName,
            this.col机构,
            this.colTel,
            this.colEmail,
            this.colDomainName,
            this.colNovellAccount,
            this.colLastLoginTime,
            this.colLastLogoutTime,
            this.colIsLocked,
            this.colCreateTime,
            this.colFlagAdmin,
            this.colLoginCounter,
            this.colsfz});
            this.gvSummary.GridControl = this.gcSummary;
            this.gvSummary.Name = "gvSummary";
            this.gvSummary.OptionsView.ColumnAutoWidth = false;
            this.gvSummary.OptionsView.ShowFooter = true;
            this.gvSummary.OptionsView.ShowGroupPanel = false;
            // 
            // col用户编码
            // 
            this.col用户编码.AppearanceHeader.Options.UseTextOptions = true;
            this.col用户编码.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col用户编码.Caption = "用户标识";
            this.col用户编码.FieldName = "用户编码";
            this.col用户编码.Name = "col用户编码";
            this.col用户编码.Visible = true;
            this.col用户编码.VisibleIndex = 0;
            // 
            // colAccount
            // 
            this.colAccount.AppearanceCell.Options.UseTextOptions = true;
            this.colAccount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAccount.AppearanceHeader.Options.UseTextOptions = true;
            this.colAccount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAccount.Caption = "登录名";
            this.colAccount.FieldName = "Account";
            this.colAccount.Name = "colAccount";
            this.colAccount.Visible = true;
            this.colAccount.VisibleIndex = 1;
            // 
            // colUserName
            // 
            this.colUserName.AppearanceHeader.Options.UseTextOptions = true;
            this.colUserName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUserName.Caption = "姓名";
            this.colUserName.FieldName = "UserName";
            this.colUserName.Name = "colUserName";
            this.colUserName.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "UserName", "共计:{0}")});
            this.colUserName.Visible = true;
            this.colUserName.VisibleIndex = 2;
            // 
            // col机构
            // 
            this.col机构.Caption = "所属机构";
            this.col机构.ColumnEdit = this.LookUpEdit机构;
            this.col机构.FieldName = "所属机构";
            this.col机构.Name = "col机构";
            this.col机构.Visible = true;
            this.col机构.VisibleIndex = 3;
            this.col机构.Width = 111;
            // 
            // LookUpEdit机构
            // 
            this.LookUpEdit机构.AutoHeight = false;
            this.LookUpEdit机构.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit机构.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("机构编号", "机构编号", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("机构名称", "机构名称")});
            this.LookUpEdit机构.Name = "LookUpEdit机构";
            this.LookUpEdit机构.NullText = "";
            // 
            // colTel
            // 
            this.colTel.AppearanceHeader.Options.UseTextOptions = true;
            this.colTel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTel.Caption = "电话";
            this.colTel.FieldName = "Tel";
            this.colTel.Name = "colTel";
            this.colTel.Visible = true;
            this.colTel.VisibleIndex = 5;
            this.colTel.Width = 120;
            // 
            // colEmail
            // 
            this.colEmail.AppearanceHeader.Options.UseTextOptions = true;
            this.colEmail.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEmail.Caption = "邮件";
            this.colEmail.FieldName = "Email";
            this.colEmail.Name = "colEmail";
            this.colEmail.Visible = true;
            this.colEmail.VisibleIndex = 6;
            this.colEmail.Width = 148;
            // 
            // colDomainName
            // 
            this.colDomainName.Caption = "域用户";
            this.colDomainName.FieldName = "DomainName";
            this.colDomainName.Name = "colDomainName";
            this.colDomainName.Visible = true;
            this.colDomainName.VisibleIndex = 7;
            this.colDomainName.Width = 107;
            // 
            // colNovellAccount
            // 
            this.colNovellAccount.Caption = "Novell帐户";
            this.colNovellAccount.FieldName = "NovellAccount";
            this.colNovellAccount.Name = "colNovellAccount";
            this.colNovellAccount.Visible = true;
            this.colNovellAccount.VisibleIndex = 4;
            this.colNovellAccount.Width = 120;
            // 
            // colLastLoginTime
            // 
            this.colLastLoginTime.AppearanceCell.Options.UseTextOptions = true;
            this.colLastLoginTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLastLoginTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colLastLoginTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLastLoginTime.Caption = "最后登录时间";
            this.colLastLoginTime.FieldName = "LastLoginTime";
            this.colLastLoginTime.Name = "colLastLoginTime";
            this.colLastLoginTime.Visible = true;
            this.colLastLoginTime.VisibleIndex = 8;
            this.colLastLoginTime.Width = 100;
            // 
            // colLastLogoutTime
            // 
            this.colLastLogoutTime.Caption = "最后登出时间";
            this.colLastLogoutTime.FieldName = "LastLogoutTime";
            this.colLastLogoutTime.Name = "colLastLogoutTime";
            this.colLastLogoutTime.Visible = true;
            this.colLastLogoutTime.VisibleIndex = 9;
            this.colLastLogoutTime.Width = 96;
            // 
            // colIsLocked
            // 
            this.colIsLocked.AppearanceCell.Options.UseTextOptions = true;
            this.colIsLocked.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIsLocked.AppearanceHeader.Options.UseTextOptions = true;
            this.colIsLocked.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIsLocked.Caption = "锁定";
            this.colIsLocked.FieldName = "IsLocked";
            this.colIsLocked.Name = "colIsLocked";
            this.colIsLocked.Visible = true;
            this.colIsLocked.VisibleIndex = 10;
            this.colIsLocked.Width = 44;
            // 
            // colCreateTime
            // 
            this.colCreateTime.AppearanceCell.Options.UseTextOptions = true;
            this.colCreateTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCreateTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colCreateTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCreateTime.Caption = "创建时间";
            this.colCreateTime.FieldName = "CreateTime";
            this.colCreateTime.Name = "colCreateTime";
            this.colCreateTime.Visible = true;
            this.colCreateTime.VisibleIndex = 11;
            this.colCreateTime.Width = 100;
            // 
            // colFlagAdmin
            // 
            this.colFlagAdmin.Caption = "管理员";
            this.colFlagAdmin.FieldName = "FlagAdmin";
            this.colFlagAdmin.Name = "colFlagAdmin";
            this.colFlagAdmin.Visible = true;
            this.colFlagAdmin.VisibleIndex = 12;
            // 
            // colLoginCounter
            // 
            this.colLoginCounter.Caption = "登录次数";
            this.colLoginCounter.FieldName = "LoginCounter";
            this.colLoginCounter.Name = "colLoginCounter";
            this.colLoginCounter.Visible = true;
            this.colLoginCounter.VisibleIndex = 13;
            // 
            // colsfz
            // 
            this.colsfz.Caption = "身份证";
            this.colsfz.FieldName = "身份证";
            this.colsfz.Name = "colsfz";
            this.colsfz.Visible = true;
            this.colsfz.VisibleIndex = 14;
            // 
            // txtPassword2
            // 
            this.txtPassword2.Location = new System.Drawing.Point(59, 57);
            this.txtPassword2.Name = "txtPassword2";
            this.txtPassword2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtPassword2.Properties.Appearance.Options.UseBackColor = true;
            this.txtPassword2.Properties.PasswordChar = '*';
            this.txtPassword2.Size = new System.Drawing.Size(117, 20);
            this.txtPassword2.TabIndex = 1;
            // 
            // txtPassword1
            // 
            this.txtPassword1.Location = new System.Drawing.Point(59, 30);
            this.txtPassword1.Name = "txtPassword1";
            this.txtPassword1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtPassword1.Properties.Appearance.Options.UseBackColor = true;
            this.txtPassword1.Properties.PasswordChar = '*';
            this.txtPassword1.Size = new System.Drawing.Size(117, 20);
            this.txtPassword1.TabIndex = 0;
            // 
            // labPassword1
            // 
            this.labPassword1.AutoSize = true;
            this.labPassword1.Location = new System.Drawing.Point(9, 32);
            this.labPassword1.Name = "labPassword1";
            this.labPassword1.Size = new System.Drawing.Size(47, 14);
            this.labPassword1.TabIndex = 108;
            this.labPassword1.Text = "第一次:";
            // 
            // labPassword2
            // 
            this.labPassword2.AutoSize = true;
            this.labPassword2.Location = new System.Drawing.Point(9, 59);
            this.labPassword2.Name = "labPassword2";
            this.labPassword2.Size = new System.Drawing.Size(47, 14);
            this.labPassword2.TabIndex = 109;
            this.labPassword2.Text = "第二次:";
            // 
            // chkIsLocked
            // 
            this.chkIsLocked.Location = new System.Drawing.Point(274, 60);
            this.chkIsLocked.Name = "chkIsLocked";
            this.chkIsLocked.Properties.Caption = "锁定用户";
            this.chkIsLocked.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.chkIsLocked.Properties.ValueChecked = "Y";
            this.chkIsLocked.Properties.ValueGrayed = "\"\"";
            this.chkIsLocked.Properties.ValueUnchecked = "N";
            this.chkIsLocked.Size = new System.Drawing.Size(97, 19);
            this.chkIsLocked.TabIndex = 12;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(93, 229);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(278, 20);
            this.txtEmail.TabIndex = 5;
            // 
            // txtTel
            // 
            this.txtTel.Location = new System.Drawing.Point(93, 203);
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(278, 20);
            this.txtTel.TabIndex = 4;
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(93, 85);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(155, 20);
            this.txtUserName.TabIndex = 93;
            // 
            // txtAccount
            // 
            this.txtAccount.Location = new System.Drawing.Point(93, 59);
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Size = new System.Drawing.Size(155, 20);
            this.txtAccount.TabIndex = 92;
            // 
            // labEmail
            // 
            this.labEmail.AutoSize = true;
            this.labEmail.Location = new System.Drawing.Point(22, 232);
            this.labEmail.Name = "labEmail";
            this.labEmail.Size = new System.Drawing.Size(59, 14);
            this.labEmail.TabIndex = 105;
            this.labEmail.Text = "电子邮件:";
            // 
            // labAccount
            // 
            this.labAccount.AutoSize = true;
            this.labAccount.Location = new System.Drawing.Point(22, 61);
            this.labAccount.Name = "labAccount";
            this.labAccount.Size = new System.Drawing.Size(59, 14);
            this.labAccount.TabIndex = 103;
            this.labAccount.Text = "登录帐号:";
            // 
            // labTel
            // 
            this.labTel.AutoSize = true;
            this.labTel.Location = new System.Drawing.Point(22, 205);
            this.labTel.Name = "labTel";
            this.labTel.Size = new System.Drawing.Size(35, 14);
            this.labTel.TabIndex = 104;
            this.labTel.Text = "电话:";
            // 
            // labUserName
            // 
            this.labUserName.AutoSize = true;
            this.labUserName.Location = new System.Drawing.Point(22, 88);
            this.labUserName.Name = "labUserName";
            this.labUserName.Size = new System.Drawing.Size(35, 14);
            this.labUserName.TabIndex = 102;
            this.labUserName.Text = "姓名:";
            // 
            // gcDetailEditor
            // 
            this.gcDetailEditor.Controls.Add(this.labelControl3);
            this.gcDetailEditor.Controls.Add(this.labelControl6);
            this.gcDetailEditor.Controls.Add(this.groupControl1);
            this.gcDetailEditor.Controls.Add(this.ucDataSets);
            this.gcDetailEditor.Controls.Add(this.txtDomainName);
            this.gcDetailEditor.Controls.Add(this.txtNovellAccount);
            this.gcDetailEditor.Controls.Add(this.label10);
            this.gcDetailEditor.Controls.Add(this.label9);
            this.gcDetailEditor.Controls.Add(this.label8);
            this.gcDetailEditor.Controls.Add(this.label7);
            this.gcDetailEditor.Controls.Add(this.label6);
            this.gcDetailEditor.Controls.Add(this.txtLoginCounter);
            this.gcDetailEditor.Controls.Add(this.labelControl2);
            this.gcDetailEditor.Controls.Add(this.txtLastLogoutTime);
            this.gcDetailEditor.Controls.Add(this.labelControl1);
            this.gcDetailEditor.Controls.Add(this.gcPassword);
            this.gcDetailEditor.Controls.Add(this.label4);
            this.gcDetailEditor.Controls.Add(this.txtAccount);
            this.gcDetailEditor.Controls.Add(this.labUserName);
            this.gcDetailEditor.Controls.Add(this.chkIsAdmin);
            this.gcDetailEditor.Controls.Add(this.chkIsLocked);
            this.gcDetailEditor.Controls.Add(this.labTel);
            this.gcDetailEditor.Controls.Add(this.labAccount);
            this.gcDetailEditor.Controls.Add(this.label1);
            this.gcDetailEditor.Controls.Add(this.labEmail);
            this.gcDetailEditor.Controls.Add(this.txt用户ID);
            this.gcDetailEditor.Controls.Add(this.txt机构名称);
            this.gcDetailEditor.Controls.Add(this.txtUserName);
            this.gcDetailEditor.Controls.Add(this.txt身份证);
            this.gcDetailEditor.Controls.Add(this.txtTel);
            this.gcDetailEditor.Controls.Add(this.txtEmail);
            this.gcDetailEditor.Controls.Add(this.txt机构编码);
            this.gcDetailEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcDetailEditor.Location = new System.Drawing.Point(0, 0);
            this.gcDetailEditor.Name = "gcDetailEditor";
            this.gcDetailEditor.Size = new System.Drawing.Size(896, 442);
            this.gcDetailEditor.TabIndex = 119;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(25, 36);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(48, 14);
            this.labelControl3.TabIndex = 386;
            this.labelControl3.Text = "用户ID：";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(25, 363);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(52, 14);
            this.labelControl6.TabIndex = 386;
            this.labelControl6.Text = "所属机构:";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.lbMyGroups);
            this.groupControl1.Location = new System.Drawing.Point(394, 36);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(252, 213);
            this.groupControl1.TabIndex = 142;
            this.groupControl1.Text = "我所属的组列表";
            // 
            // lbMyGroups
            // 
            this.lbMyGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbMyGroups.Location = new System.Drawing.Point(2, 22);
            this.lbMyGroups.Name = "lbMyGroups";
            this.lbMyGroups.Size = new System.Drawing.Size(248, 189);
            this.lbMyGroups.TabIndex = 0;
            // 
            // ucDataSets
            // 
            this.ucDataSets.EditValue = "";
            this.ucDataSets.Location = new System.Drawing.Point(93, 255);
            this.ucDataSets.Margin = new System.Windows.Forms.Padding(0);
            this.ucDataSets.Name = "ucDataSets";
            this.ucDataSets.Size = new System.Drawing.Size(278, 98);
            this.ucDataSets.TabIndex = 141;
            // 
            // txtDomainName
            // 
            this.txtDomainName.Location = new System.Drawing.Point(93, 157);
            this.txtDomainName.Name = "txtDomainName";
            this.txtDomainName.Size = new System.Drawing.Size(278, 40);
            this.txtDomainName.TabIndex = 140;
            this.txtDomainName.UseOptimizedRendering = true;
            // 
            // txtNovellAccount
            // 
            this.txtNovellAccount.Location = new System.Drawing.Point(93, 111);
            this.txtNovellAccount.Name = "txtNovellAccount";
            this.txtNovellAccount.Size = new System.Drawing.Size(278, 40);
            this.txtNovellAccount.TabIndex = 139;
            this.txtNovellAccount.UseOptimizedRendering = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(374, 161);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(14, 14);
            this.label10.TabIndex = 137;
            this.label10.Text = "*";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(374, 114);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(14, 14);
            this.label9.TabIndex = 136;
            this.label9.Text = "*";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(251, 63);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(14, 14);
            this.label8.TabIndex = 135;
            this.label8.Text = "*";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 161);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 14);
            this.label7.TabIndex = 134;
            this.label7.Text = "域帐号:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 122);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 14);
            this.label6.TabIndex = 132;
            this.label6.Text = "Novell帐号:";
            // 
            // txtLoginCounter
            // 
            this.txtLoginCounter.Location = new System.Drawing.Point(304, 387);
            this.txtLoginCounter.Name = "txtLoginCounter";
            this.txtLoginCounter.Size = new System.Drawing.Size(68, 20);
            this.txtLoginCounter.TabIndex = 130;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(246, 390);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(52, 14);
            this.labelControl2.TabIndex = 129;
            this.labelControl2.Text = "登录次数:";
            // 
            // txtLastLogoutTime
            // 
            this.txtLastLogoutTime.Location = new System.Drawing.Point(93, 387);
            this.txtLastLogoutTime.Name = "txtLastLogoutTime";
            this.txtLastLogoutTime.Size = new System.Drawing.Size(130, 20);
            this.txtLastLogoutTime.TabIndex = 128;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(25, 390);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(52, 14);
            this.labelControl1.TabIndex = 127;
            this.labelControl1.Text = "上次登出:";
            // 
            // gcPassword
            // 
            this.gcPassword.Controls.Add(this.txtPassword2);
            this.gcPassword.Controls.Add(this.labPassword2);
            this.gcPassword.Controls.Add(this.labPassword1);
            this.gcPassword.Controls.Add(this.txtPassword1);
            this.gcPassword.Location = new System.Drawing.Point(396, 255);
            this.gcPassword.Name = "gcPassword";
            this.gcPassword.Size = new System.Drawing.Size(248, 98);
            this.gcPassword.TabIndex = 124;
            this.gcPassword.Text = "输入密码";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(251, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 14);
            this.label4.TabIndex = 1;
            this.label4.Text = "*";
            // 
            // chkIsAdmin
            // 
            this.chkIsAdmin.Location = new System.Drawing.Point(275, 86);
            this.chkIsAdmin.Name = "chkIsAdmin";
            this.chkIsAdmin.Properties.Caption = "跨机构修改";
            this.chkIsAdmin.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.chkIsAdmin.Properties.ValueChecked = "Y";
            this.chkIsAdmin.Properties.ValueGrayed = "\"\"";
            this.chkIsAdmin.Properties.ValueUnchecked = "N";
            this.chkIsAdmin.Size = new System.Drawing.Size(97, 19);
            this.chkIsAdmin.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 414);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 14);
            this.label1.TabIndex = 105;
            this.label1.Text = "身份证号:";
            // 
            // txt用户ID
            // 
            this.txt用户ID.Location = new System.Drawing.Point(93, 33);
            this.txt用户ID.Name = "txt用户ID";
            this.txt用户ID.Size = new System.Drawing.Size(155, 20);
            this.txt用户ID.TabIndex = 93;
            // 
            // txt机构名称
            // 
            this.txt机构名称.Location = new System.Drawing.Point(229, 360);
            this.txt机构名称.Name = "txt机构名称";
            this.txt机构名称.Size = new System.Drawing.Size(142, 20);
            this.txt机构名称.TabIndex = 93;
            // 
            // txt身份证
            // 
            this.txt身份证.Location = new System.Drawing.Point(93, 411);
            this.txt身份证.Name = "txt身份证";
            this.txt身份证.Properties.NullText = "请填写正确身份证信息!";
            this.txt身份证.Size = new System.Drawing.Size(278, 20);
            this.txt身份证.TabIndex = 5;
            this.txt身份证.ToolTip = "身份证是信息化互联互通唯一的凭证!请填写正确!";
            this.txt身份证.ToolTipTitle = "提示";
            // 
            // txt机构编码
            // 
            this.txt机构编码.Location = new System.Drawing.Point(93, 360);
            this.txt机构编码.Name = "txt机构编码";
            this.txt机构编码.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txt机构编码.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txt机构编码.Size = new System.Drawing.Size(130, 20);
            this.txt机构编码.TabIndex = 387;
            this.txt机构编码.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txt机构_ButtonClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(80, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "*";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.pictureEdit1);
            this.panelControl3.Controls.Add(this.txt_姓名);
            this.panelControl3.Controls.Add(this.labelControl22);
            this.panelControl3.Controls.Add(this.labelControl25);
            this.panelControl3.Controls.Add(this.simpleButton对照);
            this.panelControl3.Controls.Add(this.btnEmpty);
            this.panelControl3.Controls.Add(this.btnQuery);
            this.panelControl3.Controls.Add(this.labelControl4);
            this.panelControl3.Controls.Add(this.txt_手机);
            this.panelControl3.Controls.Add(this.labelControl26);
            this.panelControl3.Controls.Add(this.txt_用户标识);
            this.panelControl3.Controls.Add(this.labelControl27);
            this.panelControl3.Controls.Add(this.txt机构);
            this.panelControl3.Controls.Add(this.txt_是否);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(896, 54);
            this.panelControl3.TabIndex = 389;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(9, 3);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Size = new System.Drawing.Size(48, 48);
            this.pictureEdit1.TabIndex = 30;
            // 
            // txt_姓名
            // 
            this.txt_姓名.Location = new System.Drawing.Point(504, 5);
            this.txt_姓名.Name = "txt_姓名";
            this.txt_姓名.Size = new System.Drawing.Size(114, 20);
            this.txt_姓名.TabIndex = 27;
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(462, 8);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(36, 14);
            this.labelControl22.TabIndex = 28;
            this.labelControl22.Text = "姓名：";
            // 
            // labelControl25
            // 
            this.labelControl25.Location = new System.Drawing.Point(286, 31);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(60, 14);
            this.labelControl25.TabIndex = 25;
            this.labelControl25.Text = "是否有效：";
            // 
            // simpleButton对照
            // 
            this.simpleButton对照.Location = new System.Drawing.Point(806, 6);
            this.simpleButton对照.Name = "simpleButton对照";
            this.simpleButton对照.Size = new System.Drawing.Size(76, 41);
            this.simpleButton对照.TabIndex = 21;
            this.simpleButton对照.Text = "用户对照";
            this.simpleButton对照.Click += new System.EventHandler(this.simpleButton对照_Click);
            // 
            // btnEmpty
            // 
            this.btnEmpty.Location = new System.Drawing.Point(720, 6);
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.Size = new System.Drawing.Size(76, 41);
            this.btnEmpty.TabIndex = 21;
            this.btnEmpty.Text = "清空(&E)";
            this.btnEmpty.Click += new System.EventHandler(this.btnEmpty_Click);
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(636, 6);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(73, 42);
            this.btnQuery.TabIndex = 20;
            this.btnQuery.Text = "查询(&S)";
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(99, 32);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(36, 14);
            this.labelControl4.TabIndex = 24;
            this.labelControl4.Text = "手机：";
            // 
            // txt_手机
            // 
            this.txt_手机.Location = new System.Drawing.Point(142, 29);
            this.txt_手机.Name = "txt_手机";
            this.txt_手机.Size = new System.Drawing.Size(138, 20);
            this.txt_手机.TabIndex = 15;
            // 
            // labelControl26
            // 
            this.labelControl26.Location = new System.Drawing.Point(286, 7);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(60, 14);
            this.labelControl26.TabIndex = 24;
            this.labelControl26.Text = "用户标识：";
            // 
            // txt_用户标识
            // 
            this.txt_用户标识.Location = new System.Drawing.Point(352, 5);
            this.txt_用户标识.Name = "txt_用户标识";
            this.txt_用户标识.Size = new System.Drawing.Size(100, 20);
            this.txt_用户标识.TabIndex = 15;
            // 
            // labelControl27
            // 
            this.labelControl27.Location = new System.Drawing.Point(99, 7);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(36, 14);
            this.labelControl27.TabIndex = 16;
            this.labelControl27.Text = "机构：";
            // 
            // txt机构
            // 
            this.txt机构.EditValue = "txt机构";
            this.txt机构.Location = new System.Drawing.Point(142, 5);
            this.txt机构.Name = "txt机构";
            this.txt机构.Properties.AutoExpandAllNodes = false;
            this.txt机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt机构.Properties.NullText = "";
            this.txt机构.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.txt机构.Size = new System.Drawing.Size(138, 20);
            this.txt机构.TabIndex = 14;
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(0, 0);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.RootValue = 1;
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // txt_是否
            // 
            this.txt_是否.Location = new System.Drawing.Point(352, 29);
            this.txt_是否.Name = "txt_是否";
            this.txt_是否.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt_是否.Properties.Items.AddRange(new object[] {
            "是",
            "否"});
            this.txt_是否.Properties.PopupSizeable = true;
            this.txt_是否.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txt_是否.Size = new System.Drawing.Size(100, 20);
            this.txt_是否.TabIndex = 29;
            // 
            // frmUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.ClientSize = new System.Drawing.Size(902, 499);
            this.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Name = "frmUser";
            this.Text = "用户管理";
            this.Load += new System.EventHandler(this.frmUser_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            this.tpDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit机构)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsLocked.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetailEditor)).EndInit();
            this.gcDetailEditor.ResumeLayout(false);
            this.gcDetailEditor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lbMyGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDomainName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNovellAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLoginCounter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastLogoutTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcPassword)).EndInit();
            this.gcPassword.ResumeLayout(false);
            this.gcPassword.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsAdmin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt用户ID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt机构名称.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt机构编码.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_手机.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_用户标识.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_是否.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcSummary;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSummary;
        private DevExpress.XtraGrid.Columns.GridColumn colUserName;
        private DevExpress.XtraGrid.Columns.GridColumn colAccount;
        private DevExpress.XtraGrid.Columns.GridColumn colTel;
        private DevExpress.XtraGrid.Columns.GridColumn colEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colLastLoginTime;
        private DevExpress.XtraGrid.Columns.GridColumn colIsLocked;
        private DevExpress.XtraGrid.Columns.GridColumn colCreateTime;
        private DevExpress.XtraEditors.TextEdit txtPassword2;
        private DevExpress.XtraEditors.TextEdit txtPassword1;
        private System.Windows.Forms.Label labPassword1;
        private System.Windows.Forms.Label labPassword2;
        private DevExpress.XtraEditors.CheckEdit chkIsLocked;
        private DevExpress.XtraEditors.TextEdit txtEmail;
        private DevExpress.XtraEditors.TextEdit txtTel;
        private DevExpress.XtraEditors.TextEdit txtUserName;
        private DevExpress.XtraEditors.TextEdit txtAccount;
        private System.Windows.Forms.Label labEmail;
        private System.Windows.Forms.Label labAccount;
        private System.Windows.Forms.Label labTel;
        private System.Windows.Forms.Label labUserName;
        private DevExpress.XtraEditors.GroupControl gcDetailEditor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.GroupControl gcPassword;
        private DevExpress.XtraGrid.Columns.GridColumn colFlagAdmin;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtLastLogoutTime;
        private DevExpress.XtraEditors.TextEdit txtLoginCounter;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraGrid.Columns.GridColumn colLastLogoutTime;
        private DevExpress.XtraGrid.Columns.GridColumn colLoginCounter;
        private DevExpress.XtraGrid.Columns.GridColumn colNovellAccount;
        private DevExpress.XtraGrid.Columns.GridColumn colDomainName;
        private DevExpress.XtraEditors.MemoEdit txtDomainName;
        private DevExpress.XtraEditors.MemoEdit txtNovellAccount;
        private AtomEHR.Library.UserControls.ucCheckedListBoxBinding ucDataSets;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.ListBoxControl lbMyGroups;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.ButtonEdit txt机构编码;
        private DevExpress.XtraEditors.TextEdit txt机构名称;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txt用户ID;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.TextEdit txt_姓名;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.SimpleButton btnEmpty;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.TextEdit txt_用户标识;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txt_手机;
        private DevExpress.XtraGrid.Columns.GridColumn col用户编码;
        private DevExpress.XtraEditors.TreeListLookUpEdit txt机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraEditors.ComboBoxEdit txt_是否;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn col机构;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEdit机构;
        private DevExpress.XtraEditors.SimpleButton simpleButton对照;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit txt身份证;
        private DevExpress.XtraGrid.Columns.GridColumn colsfz;
        private DevExpress.XtraEditors.CheckEdit chkIsAdmin;
    }
}
