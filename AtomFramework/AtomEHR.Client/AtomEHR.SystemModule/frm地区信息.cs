﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//引用自定义
using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.Models;
using AtomEHR.Interfaces;

namespace AtomEHR.SystemModule
{
    public partial class frm地区信息 :   AtomEHR.Library.frmBaseDataDictionary
    {
        public frm地区信息()
        {
            InitializeComponent();
        }

        private bll地区档案 _BllCustomer; //业务逻辑层对象引用

        private void frm地区信息_Load(object sender, EventArgs e)
        {
            this.InitializeForm();//自定义初始化操作  
        }

        #region override function
        protected override void InitializeForm()
        {
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.
            _ActiveEditor = txt地区名称;
            //_KeyEditor = txt地区编码;
            _DetailGroupControl = gcDetailEditor;
            _BLL = new bll地区档案(); //业务逻辑实例
            _BllCustomer = _BLL as bll地区档案; //本窗体引用

            base.InitializeForm();

            //绑定相关缓存数据
            
            DataBinder.BindingLookupEditDataSource(txt上级, 
                _BLL.ConvertRowsToTable(DataDictCache.Cache.t地区信息.Select("地区编码='" + Loginer.CurrentUser.单位代码 + "' or 上级编码='" + Loginer.CurrentUser.单位代码 + "' ")),
                    tb_地区档案.地区名称, tb_地区档案.地区编码);
            DataBinder.BindingLookupEditDataSource(txt创建人, DataDictCache.Cache.User, TUser.UserName, TUser.用户编码);

            ShowSummary(); //下载显示数据.
            BindingSummaryNavigator(controlNavigatorSummary, gcSummary); //Summary导航条.
            ShowSummaryPage(true); //一切初始化完毕後显示SummaryPage      
        }

        public override void DoSave(IButtonInfo sender)
        {
            UpdateLastControl(); //更新最后一个输入控件的数据

            if (!ValidatingData()) return; //检查输入完整性
            //新增时获取新的机构编号
            if (UpdateType.Add == _UpdateType)
            {
                _BLL.DataBinder.Rows[0][tb_地区档案.地区编码] = this.txt地区编码.Text.Trim().ToString();//_BllCustomer.GetNew机构编号(txt上级.EditValue.ToString()); //新增用户时获取新的编号                   
            }
            _BLL.DataBinder.Rows[0][tb_地区档案.是否城市] = this.txt是否城市.Checked ? "是" : "否";
            base.DoSave(sender);
            this.txt地区编码.Text = _BLL.DataBinder.Rows[0][tb_地区档案.地区编码].ToString();
        }

        // 检查主表数据是否完整或合法
        protected override bool ValidatingData()
        {
            if (txt地区编码.Text == string.Empty)
            {
                Msg.Warning("地区编码不能为空!");
                txt地区编码.Focus();
                return false;
            }

            if (txt地区名称.Text == string.Empty)
            {
                Msg.Warning("地区名称不能为空!");
                txt地区名称.Focus();
                return false;
            }

            if (_UpdateType == UpdateType.Add)
            {
                if (_BLL.CheckNoExists(txt地区编码.Text))
                {
                    Msg.Warning("地区编码已存在!");
                    txt地区编码.Focus();
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 绑定输入框
        /// </summary>
        /// <param name="summary"></param>
        protected override void DoBindingSummaryEditor(DataTable summary)
        {
            try
            {
                if (summary == null) return;
                DataBinder.BindingTextEdit(txt地区编码, summary, tb_地区档案.地区编码);
                DataBinder.BindingTextEdit(txt地区名称, summary, tb_地区档案.地区名称);
                DataBinder.BindingTextEdit(txt详细名称, summary, tb_地区档案.详细地区名称);
                DataBinder.BindingTextEdit(txt地区人口, summary, tb_地区档案.地区人口);
                DataBinder.BindingTextEdit(txt上级, summary, tb_地区档案.上级编码);
                DataBinder.BindingTextEdit(txt备注, summary, tb_地区档案.备注);
                DataBinder.BindingTextEdit(txt创建人, summary, tb_地区档案.创建人);
                DataBinder.BindingTextEdit(txt创建日期, summary, tb_地区档案.创建时间);

                if (summary.Rows[0][tb_地区档案.是否城市].Equals("是"))
                    this.txt是否城市.Checked = true;
                else
                    this.txt是否城市.Checked = true;
            }
            catch (Exception ex)
            { Msg.ShowException(ex); }
        }

        protected override void ButtonStateChanged(UpdateType currentState)// 按钮状态改变时触发的事件
        {
            //设置页面的控件
            this.SetDetailEditorsAccessable(gcDetailEditor, this.DataChanged);

            if (currentState == Common.UpdateType.Add)
            {
                _BLL.DataBinder.Rows[0][tb_地区档案.地区编码] = "*手动输入*";
                _BLL.DataBinder.Rows[0][tb_地区档案.上级编码] = Loginer.CurrentUser.所属机构;
                _BLL.DataBinder.Rows[0][tb_地区档案.创建人] = Loginer.CurrentUser.用户编码;
                _BLL.DataBinder.Rows[0][tb_地区档案.创建时间] = DateTime.Today;
            }
            else if (currentState == Common.UpdateType.Modify)
            {
                //_BLL.DataBinder.Rows[0][tb_机构信息._修改人] = Loginer.CurrentUser.Account;
                //_BLL.DataBinder.Rows[0][tb_机构信息._修改时间] = DateTime.Today;

                txt修改人.Text = Loginer.CurrentUser.AccountName;
                txt修改时间.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
            //this.txt地区编码.Properties.ReadOnly = true;
            //管理员能指定上机机构
            this.txt上级.Enabled = Loginer.CurrentUser.IsAdmin();
            txt创建人.Enabled = false;
            txt创建日期.Enabled = false;
            txt修改人.Enabled = false;
            txt修改时间.Enabled = false;
            //this.SetEditorEnable(txt机构, false, true);
        }
        #endregion

        private void btnEmpty_Click(object sender, EventArgs e)
        {
            base.ClearContainerEditorText(panelControl3);     
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            //搜索数据            
            this._BllCustomer.SearchBy(txt_CustomerFrom.Text, txt_CustomerTo.Text, txt_Name.Text, ConvertEx.ToString(txt_Attr.EditValue), true);
            this.DoBindingSummaryGrid(_BLL.SummaryTable); //绑定主表的Grid
            this.ShowSummaryPage(true); //显示Summary页面. 
        }
    }
}
