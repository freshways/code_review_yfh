﻿using AtomEHR.Business;
using AtomEHR.Business.BLL_Business;
using AtomEHR.Business.Security;
using AtomEHR.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;

namespace AtomEHR.SystemModule
{
    public partial class frmLogQuery : frmBase
    {
        public frmLogQuery()
        {
            InitializeComponent();
        }

        private void frmLogQuery_Load(object sender, EventArgs e)
        {
            InitView();
        }

        private void InitView()
        {
            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                treeListLookUpEdit机构.Properties.ValueMember = "机构编号";
                treeListLookUpEdit机构.Properties.DisplayMember = "机构名称";

                treeListLookUpEdit机构.Properties.TreeList.KeyFieldName = "机构编号";
                treeListLookUpEdit机构.Properties.TreeList.ParentFieldName = "上级机构";
                treeListLookUpEdit机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                treeListLookUpEdit机构.Properties.DataSource = dt所属机构;
                //treeListLookUpEdit机
                //treeListLookUpEdit机构.Properties.TreeList.CollapseAll();

                if (Loginer.CurrentUser.所属机构.Length >= 12)
                    treeListLookUpEdit机构.Properties.AutoExpandAllNodes = true;
                else { treeListLookUpEdit机构.Properties.AutoExpandAllNodes = false; }

                treeListLookUpEdit机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                treeListLookUpEdit机构.Text = Loginer.CurrentUser.所属机构;
            }

            try
            {
                DataTable dt录入人 = new bllUser().GetUserNameAndIDBy机构用户(treeListLookUpEdit机构.EditValue.ToString());
                cbo录入人.Properties.DisplayMember = "UserName";
                cbo录入人.Properties.ValueMember = "用户编码";
                cbo录入人.Properties.DataSource = dt录入人;
            }
            catch (Exception ex)
            {
                cbo录入人.Text = "请选择...";
            }
        }

        private void btn查询_Click(object sender, EventArgs e)
        {
            if (!Funchk()) return;

            bllCom com = new bllCom();
            string _strWhere = string.Empty;
            string pgrid = this.treeListLookUpEdit机构.EditValue.ToString();
            if (this.checkEdit1.Checked)//包含下属机构
            {
                if (pgrid.Length == 12)
                {
                    _strWhere += " and ([LogUser]=  '" + pgrid + "' or substring([LogUser],1,7)+'1'+substring([LogUser],9,7) like '" + pgrid + "%')";
                }
                else
                {
                    _strWhere += " and [LogUser] like '" + pgrid + "%'";
                }
            }
            else
            {
                _strWhere += " and [LogUser] ='" + pgrid + "'";
            }

            if (!string.IsNullOrEmpty(this.txt操作对象.Text.Trim()))
            {
                _strWhere += " and [KeyFieldName] LIKE '" + this.txt操作对象.Text.Trim() + "%'  ";
            }

            if (cbo录入人.EditValue != null && cbo录入人.EditValue.ToString().Trim() != "")
            {
                _strWhere += " and [LogUser] =  '" + cbo录入人.EditValue.ToString() + "' ";
            }
            if (this.dte结束时间.Text.Trim() != "")
            {
                _strWhere += " and [LogDate] <=  '" + dte结束时间.Text.Trim() + " ' ";
            }
            if (this.dte开始时间.Text.Trim() != "")
            {
                _strWhere += " and [LogDate] >=  '" + dte开始时间.Text.Trim() + " ' ";
            }
            DataSet ds = com.GetLogData(_strWhere);
            this.gridControl1.DataSource = ds.Tables[0];
            this.gridView1.BestFitColumns();

        }

        private bool Funchk()
        {
            if (this.dte结束时间.Text.Trim() != "" && this.dte开始时间.Text.Trim() != "")
            {
                if (this.dte开始时间.DateTime > this.dte结束时间.DateTime)
                {
                    Msg.ShowError("开始时间不能大于结束时间！");
                    return false;
                }
            }
            return true;
        }
    }
}
