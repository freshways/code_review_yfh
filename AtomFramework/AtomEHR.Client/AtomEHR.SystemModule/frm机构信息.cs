﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.Models;
using AtomEHR.Interfaces;

namespace AtomEHR.SystemModule
{
    public partial class frm机构信息 : AtomEHR.Library.frmBaseDataDictionary
    {
        private bll机构信息 _BllCustomer; //业务逻辑层对象引用

        public frm机构信息()
        {
            InitializeComponent();
        }

        private void frm机构信息_Load(object sender, EventArgs e)
        {
            this.InitializeForm();//自定义初始化操作  
        }
        protected override void InitializeForm()
        {
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.
            _ActiveEditor = txt机构名称;
            _KeyEditor = txt机构编号;
            _DetailGroupControl = gcDetailEditor;
            _BLL = new bll机构信息(); //业务逻辑实例
            _BllCustomer = _BLL as bll机构信息; //本窗体引用

            base.InitializeForm();

            //绑定相关缓存数据
            DataBinder.BindingLookupEditDataSource(repositoryItemLookUpEdit1, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='jgjb'")),
                "P_DESC", "P_CODE");
            DataBinder.BindingLookupEditDataSource(txt机构级别, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='jgjb'")),
                "P_DESC", "P_CODE");
            DataBinder.BindingLookupEditDataSource(txt_Attr, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='jgjb'")),
                "P_DESC", "P_CODE");
            //DataBinder.BindingLookupEditDataSource(txt机构级别, DataDictCache.Cache.t机构级别, "NativeName", "DataCode");
            DataBinder.BindingLookupEditDataSource(txt上级, _BLL.ConvertRowsToTable(DataDictCache.Cache.t机构信息.Select("机构级别 <>'1' ")),
                    tb_机构信息._机构名称, tb_机构信息._机构编号);
            DataBinder.BindingLookupEditDataSource(txt创建人, DataDictCache.Cache.User, TUser.UserName, TUser.用户编码);
            //DataBinder.BindingLookupEditDataSource(txt_Attr, DataDictCache.Cache.t机构级别, "NativeName", "DataCode");

            ShowSummary(); //下载显示数据.
            BindingSummaryNavigator(controlNavigatorSummary, gcSummary); //Summary导航条.
            ShowSummaryPage(true); //一切初始化完毕後显示SummaryPage      
        }

        public override void DoSave(IButtonInfo sender)
        {
            UpdateLastControl(); //更新最后一个输入控件的数据

            if (!ValidatingData()) return; //检查输入完整性
            //新增时获取新的机构编号
            if (UpdateType.Add == _UpdateType)
            {
                _BLL.DataBinder.Rows[0][tb_机构信息._机构编号] = _BllCustomer.GetNew机构编号(txt上级.EditValue.ToString()); //新增用户时获取新的编号                   
            }
            base.DoSave(sender);
            this.txt机构编号.Text = _BLL.DataBinder.Rows[0][tb_机构信息._机构编号].ToString();           
        }
        
        // 检查主表数据是否完整或合法
        protected override bool ValidatingData()
        {
            if (txt机构级别.Text == string.Empty)
            {
                Msg.Warning("机构级别不能为空!");
                txt机构级别.Focus();
                return false;
            }

            if (txt机构名称.Text == string.Empty)
            {
                Msg.Warning("机构名称不能为空!");
                txt机构名称.Focus();
                return false;
            }

            if (_UpdateType == UpdateType.Add)
            {
                if (_BLL.CheckNoExists(txt机构编号.Text))
                {
                    Msg.Warning("机构编号已存在!");
                    txt机构编号.Focus();
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 绑定输入框
        /// </summary>
        /// <param name="summary"></param>
        protected override void DoBindingSummaryEditor(DataTable summary)
        {
            try
            {
                if (summary == null) return;
                DataBinder.BindingTextEdit(txt机构编号, summary, tb_机构信息._机构编号);
                DataBinder.BindingTextEdit(txt机构名称, summary, tb_机构信息._机构名称);
                DataBinder.BindingTextEdit(txt机构级别, summary, tb_机构信息._机构级别);
                DataBinder.BindingTextEdit(txt负责人, summary, tb_机构信息._负责人);
                DataBinder.BindingTextEdit(txt联系人, summary, tb_机构信息._联系人);
                DataBinder.BindingTextEdit(txt联系电话, summary, tb_机构信息._联系电话);
                DataBinder.BindingTextEdit(txt上级, summary, tb_机构信息._上级机构);
                DataBinder.BindingTextEdit(txt备注, summary, tb_机构信息._P_RGID_MARKER);
                DataBinder.BindingTextEdit(txt创建人, summary, tb_机构信息._创建人);
                DataBinder.BindingTextEdit(txt创建日期, summary, tb_机构信息._创建时间);
            }
            catch (Exception ex)
            { Msg.ShowException(ex); }
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            //搜索数据            
            this._BllCustomer.SearchBy(txt_CustomerFrom.Text, txt_CustomerTo.Text, txt_Name.Text, ConvertEx.ToString(txt_Attr.EditValue), true);
            this.DoBindingSummaryGrid(_BLL.SummaryTable); //绑定主表的Grid
            this.ShowSummaryPage(true); //显示Summary页面. 
        }

        private void btnEmpty_Click(object sender, EventArgs e)
        {
            base.ClearContainerEditorText(panelControl3);     
        }

        protected override void ButtonStateChanged(UpdateType currentState)// 按钮状态改变时触发的事件
        {
            //设置页面的控件
            this.SetDetailEditorsAccessable(gcDetailEditor, this.DataChanged);

            if (currentState == Common.UpdateType.Add)
            {
                _BLL.DataBinder.Rows[0][tb_机构信息._机构编号] = "*自动生成*";
                _BLL.DataBinder.Rows[0][tb_机构信息._上级机构] = Loginer.CurrentUser.所属机构;
                _BLL.DataBinder.Rows[0][tb_机构信息._创建人] = Loginer.CurrentUser.用户编码;
                _BLL.DataBinder.Rows[0][tb_机构信息._创建时间] = DateTime.Today;
            }
            else if (currentState == Common.UpdateType.Modify)
            {
                //_BLL.DataBinder.Rows[0][tb_机构信息._修改人] = Loginer.CurrentUser.Account;
                //_BLL.DataBinder.Rows[0][tb_机构信息._修改时间] = DateTime.Today;
            }
            this.txt机构编号.Properties.ReadOnly = true;
            //管理员能锁定上机机构
            this.txt上级.Enabled = Loginer.CurrentUser.IsAdmin();
            txt创建人.Enabled = false;
            txt创建日期.Enabled = false;
            //this.SetEditorEnable(txt机构, false, true);
        }

    }
}
