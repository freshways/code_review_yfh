﻿namespace AtomEHR.SystemModule
{
    partial class frm医生信息
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm医生信息));
            this.gcSummary = new DevExpress.XtraGrid.GridControl();
            this.gvSummary = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEdit机构 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gcDetailEditor = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton上传 = new DevExpress.XtraEditors.SimpleButton();
            this.txt是否城市 = new DevExpress.XtraEditors.CheckEdit();
            this.lab创建日期 = new DevExpress.XtraEditors.LabelControl();
            this.txt创建日期 = new DevExpress.XtraEditors.DateEdit();
            this.txt创建人 = new DevExpress.XtraEditors.LookUpEdit();
            this.lab创建人 = new DevExpress.XtraEditors.LabelControl();
            this.txt职务 = new DevExpress.XtraEditors.TextEdit();
            this.txt医生编号 = new DevExpress.XtraEditors.TextEdit();
            this.lab机构级别 = new DevExpress.XtraEditors.LabelControl();
            this.lab负责人 = new DevExpress.XtraEditors.LabelControl();
            this.lab机构编号 = new DevExpress.XtraEditors.LabelControl();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt科室 = new DevExpress.XtraEditors.TextEdit();
            this.lab备注 = new DevExpress.XtraEditors.LabelControl();
            this.lab联系电话 = new DevExpress.XtraEditors.LabelControl();
            this.lab联系人 = new DevExpress.XtraEditors.LabelControl();
            this.txt医生名称 = new DevExpress.XtraEditors.TextEdit();
            this.lab机构名称 = new DevExpress.XtraEditors.LabelControl();
            this.lab上级 = new DevExpress.XtraEditors.LabelControl();
            this.txt所属机构 = new DevExpress.XtraEditors.LookUpEdit();
            this.pictureEdit手签 = new DevExpress.XtraEditors.PictureEdit();
            this.txt查体类型 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.txt_医生姓名 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.btnEmpty = new DevExpress.XtraEditors.SimpleButton();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.txt_科室 = new DevExpress.XtraEditors.TextEdit();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.txt_查体模块 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt_机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            this.tpDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit机构)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetailEditor)).BeginInit();
            this.gcDetailEditor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt是否城市.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职务.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt科室.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生名称.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit手签.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt查体类型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_医生姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_科室.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_查体模块.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gcSummary);
            this.tpSummary.Controls.Add(this.panelControl3);
            this.tpSummary.Size = new System.Drawing.Size(889, 356);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Size = new System.Drawing.Size(895, 385);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Size = new System.Drawing.Size(895, 385);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Controls.Add(this.gcDetailEditor);
            this.tpDetail.Size = new System.Drawing.Size(889, 356);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(895, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(717, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(520, 2);
            // 
            // gcSummary
            // 
            this.gcSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSummary.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcSummary.Location = new System.Drawing.Point(0, 54);
            this.gcSummary.MainView = this.gvSummary;
            this.gcSummary.Name = "gcSummary";
            this.gcSummary.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit1,
            this.LookUpEdit机构});
            this.gcSummary.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.gcSummary.Size = new System.Drawing.Size(889, 302);
            this.gcSummary.TabIndex = 12;
            this.gcSummary.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSummary});
            // 
            // gvSummary
            // 
            this.gvSummary.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8});
            this.gvSummary.GridControl = this.gcSummary;
            this.gvSummary.Name = "gvSummary";
            this.gvSummary.OptionsView.ColumnAutoWidth = false;
            this.gvSummary.OptionsView.ShowFooter = true;
            this.gvSummary.OptionsView.ShowGroupPanel = false;
            this.gvSummary.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gvSummary_CustomUnboundColumnData);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "医生编码";
            this.gridColumn1.FieldName = "b编码";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "机构编号", "共计:{0} 条")});
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 135;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "医生姓名";
            this.gridColumn2.FieldName = "x医生姓名";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 155;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "职务";
            this.gridColumn3.FieldName = "z职务";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 145;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "科室";
            this.gridColumn4.FieldName = "k科室";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "身份证号";
            this.gridColumn5.FieldName = "s身份证号";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "查体模块";
            this.gridColumn6.FieldName = "c查体模块";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            this.gridColumn6.Width = 100;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "手签";
            this.gridColumn7.ColumnEdit = this.repositoryItemPictureEdit1;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn7.Width = 100;
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.CustomHeight = 40;
            this.repositoryItemPictureEdit1.InitialImage = global::AtomEHR.SystemModule.Properties.Resources._16_CustomerQuery;
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            this.repositoryItemPictureEdit1.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "所属机构";
            this.gridColumn8.ColumnEdit = this.LookUpEdit机构;
            this.gridColumn8.FieldName = "s所属机构";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 6;
            // 
            // LookUpEdit机构
            // 
            this.LookUpEdit机构.AutoHeight = false;
            this.LookUpEdit机构.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit机构.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("机构编码", "编码"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("机构名称", "名称")});
            this.LookUpEdit机构.Name = "LookUpEdit机构";
            // 
            // gcDetailEditor
            // 
            this.gcDetailEditor.Controls.Add(this.simpleButton上传);
            this.gcDetailEditor.Controls.Add(this.txt是否城市);
            this.gcDetailEditor.Controls.Add(this.lab创建日期);
            this.gcDetailEditor.Controls.Add(this.txt创建日期);
            this.gcDetailEditor.Controls.Add(this.txt创建人);
            this.gcDetailEditor.Controls.Add(this.lab创建人);
            this.gcDetailEditor.Controls.Add(this.txt职务);
            this.gcDetailEditor.Controls.Add(this.txt医生编号);
            this.gcDetailEditor.Controls.Add(this.lab机构级别);
            this.gcDetailEditor.Controls.Add(this.lab负责人);
            this.gcDetailEditor.Controls.Add(this.lab机构编号);
            this.gcDetailEditor.Controls.Add(this.txt身份证号);
            this.gcDetailEditor.Controls.Add(this.txt科室);
            this.gcDetailEditor.Controls.Add(this.lab备注);
            this.gcDetailEditor.Controls.Add(this.lab联系电话);
            this.gcDetailEditor.Controls.Add(this.lab联系人);
            this.gcDetailEditor.Controls.Add(this.txt医生名称);
            this.gcDetailEditor.Controls.Add(this.lab机构名称);
            this.gcDetailEditor.Controls.Add(this.lab上级);
            this.gcDetailEditor.Controls.Add(this.txt所属机构);
            this.gcDetailEditor.Controls.Add(this.pictureEdit手签);
            this.gcDetailEditor.Controls.Add(this.txt查体类型);
            this.gcDetailEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcDetailEditor.Location = new System.Drawing.Point(0, 0);
            this.gcDetailEditor.Name = "gcDetailEditor";
            this.gcDetailEditor.Size = new System.Drawing.Size(889, 356);
            this.gcDetailEditor.TabIndex = 15;
            this.gcDetailEditor.Text = "资料数据维护";
            // 
            // simpleButton上传
            // 
            this.simpleButton上传.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton上传.Image")));
            this.simpleButton上传.Location = new System.Drawing.Point(377, 174);
            this.simpleButton上传.Name = "simpleButton上传";
            this.simpleButton上传.Size = new System.Drawing.Size(63, 23);
            this.simpleButton上传.TabIndex = 263;
            this.simpleButton上传.Text = "上传";
            this.simpleButton上传.Click += new System.EventHandler(this.simpleButton上传_Click);
            // 
            // txt是否城市
            // 
            this.txt是否城市.Location = new System.Drawing.Point(375, 36);
            this.txt是否城市.Name = "txt是否城市";
            this.txt是否城市.Properties.Caption = "是否停用";
            this.txt是否城市.Size = new System.Drawing.Size(129, 19);
            this.txt是否城市.TabIndex = 262;
            // 
            // lab创建日期
            // 
            this.lab创建日期.Location = new System.Drawing.Point(38, 263);
            this.lab创建日期.Name = "lab创建日期";
            this.lab创建日期.Size = new System.Drawing.Size(60, 14);
            this.lab创建日期.TabIndex = 261;
            this.lab创建日期.Text = "创建日期：";
            // 
            // txt创建日期
            // 
            this.txt创建日期.EditValue = new System.DateTime(2010, 10, 29, 18, 35, 50, 843);
            this.txt创建日期.Location = new System.Drawing.Point(111, 260);
            this.txt创建日期.Name = "txt创建日期";
            this.txt创建日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt创建日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txt创建日期.Size = new System.Drawing.Size(212, 20);
            this.txt创建日期.TabIndex = 259;
            // 
            // txt创建人
            // 
            this.txt创建人.Location = new System.Drawing.Point(111, 232);
            this.txt创建人.Name = "txt创建人";
            this.txt创建人.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt创建人.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("用户编码", 80, "用户编码"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("UserName", 100, "用户名")});
            this.txt创建人.Properties.NullText = "";
            this.txt创建人.Properties.PopupWidth = 180;
            this.txt创建人.Size = new System.Drawing.Size(212, 20);
            this.txt创建人.TabIndex = 258;
            // 
            // lab创建人
            // 
            this.lab创建人.Location = new System.Drawing.Point(50, 235);
            this.lab创建人.Name = "lab创建人";
            this.lab创建人.Size = new System.Drawing.Size(48, 14);
            this.lab创建人.TabIndex = 260;
            this.lab创建人.Text = "创建人：";
            // 
            // txt职务
            // 
            this.txt职务.Location = new System.Drawing.Point(111, 119);
            this.txt职务.Name = "txt职务";
            this.txt职务.Size = new System.Drawing.Size(212, 20);
            this.txt职务.TabIndex = 2;
            // 
            // txt医生编号
            // 
            this.txt医生编号.EditValue = "*自动生成*";
            this.txt医生编号.Location = new System.Drawing.Point(111, 63);
            this.txt医生编号.Name = "txt医生编号";
            this.txt医生编号.Properties.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.txt医生编号.Properties.Appearance.Options.UseForeColor = true;
            this.txt医生编号.Properties.ReadOnly = true;
            this.txt医生编号.Size = new System.Drawing.Size(212, 20);
            this.txt医生编号.TabIndex = 2;
            // 
            // lab机构级别
            // 
            this.lab机构级别.Location = new System.Drawing.Point(38, 38);
            this.lab机构级别.Name = "lab机构级别";
            this.lab机构级别.Size = new System.Drawing.Size(60, 14);
            this.lab机构级别.TabIndex = 3;
            this.lab机构级别.Text = "查体类型：";
            // 
            // lab负责人
            // 
            this.lab负责人.Location = new System.Drawing.Point(62, 125);
            this.lab负责人.Name = "lab负责人";
            this.lab负责人.Size = new System.Drawing.Size(36, 14);
            this.lab负责人.TabIndex = 3;
            this.lab负责人.Text = "职务：";
            // 
            // lab机构编号
            // 
            this.lab机构编号.Location = new System.Drawing.Point(38, 66);
            this.lab机构编号.Name = "lab机构编号";
            this.lab机构编号.Size = new System.Drawing.Size(60, 14);
            this.lab机构编号.TabIndex = 3;
            this.lab机构编号.Text = "医生编号：";
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(111, 175);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Size = new System.Drawing.Size(212, 20);
            this.txt身份证号.TabIndex = 4;
            // 
            // txt科室
            // 
            this.txt科室.Location = new System.Drawing.Point(111, 147);
            this.txt科室.Name = "txt科室";
            this.txt科室.Size = new System.Drawing.Size(212, 20);
            this.txt科室.TabIndex = 4;
            // 
            // lab备注
            // 
            this.lab备注.Location = new System.Drawing.Point(377, 94);
            this.lab备注.Name = "lab备注";
            this.lab备注.Size = new System.Drawing.Size(36, 14);
            this.lab备注.TabIndex = 11;
            this.lab备注.Text = "手签：";
            // 
            // lab联系电话
            // 
            this.lab联系电话.Location = new System.Drawing.Point(38, 178);
            this.lab联系电话.Name = "lab联系电话";
            this.lab联系电话.Size = new System.Drawing.Size(60, 14);
            this.lab联系电话.TabIndex = 5;
            this.lab联系电话.Text = "身份证号：";
            // 
            // lab联系人
            // 
            this.lab联系人.Location = new System.Drawing.Point(62, 150);
            this.lab联系人.Name = "lab联系人";
            this.lab联系人.Size = new System.Drawing.Size(36, 14);
            this.lab联系人.TabIndex = 5;
            this.lab联系人.Text = "科室：";
            // 
            // txt医生名称
            // 
            this.txt医生名称.Location = new System.Drawing.Point(111, 91);
            this.txt医生名称.Name = "txt医生名称";
            this.txt医生名称.Size = new System.Drawing.Size(212, 20);
            this.txt医生名称.TabIndex = 4;
            // 
            // lab机构名称
            // 
            this.lab机构名称.Location = new System.Drawing.Point(38, 94);
            this.lab机构名称.Name = "lab机构名称";
            this.lab机构名称.Size = new System.Drawing.Size(67, 14);
            this.lab机构名称.TabIndex = 5;
            this.lab机构名称.Text = "医生名称：*";
            // 
            // lab上级
            // 
            this.lab上级.Location = new System.Drawing.Point(38, 206);
            this.lab上级.Name = "lab上级";
            this.lab上级.Size = new System.Drawing.Size(60, 14);
            this.lab上级.TabIndex = 9;
            this.lab上级.Text = "所属机构：";
            // 
            // txt所属机构
            // 
            this.txt所属机构.Location = new System.Drawing.Point(111, 203);
            this.txt所属机构.Name = "txt所属机构";
            this.txt所属机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt所属机构.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("机构编号", 10, "编码"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("机构名称", "名称")});
            this.txt所属机构.Properties.NullText = "";
            this.txt所属机构.Size = new System.Drawing.Size(212, 20);
            this.txt所属机构.TabIndex = 8;
            // 
            // pictureEdit手签
            // 
            this.pictureEdit手签.Location = new System.Drawing.Point(377, 119);
            this.pictureEdit手签.Name = "pictureEdit手签";
            this.pictureEdit手签.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit手签.Size = new System.Drawing.Size(175, 45);
            this.pictureEdit手签.TabIndex = 10;
            this.pictureEdit手签.TabStop = true;
            // 
            // txt查体类型
            // 
            this.txt查体类型.Location = new System.Drawing.Point(111, 35);
            this.txt查体类型.Name = "txt查体类型";
            this.txt查体类型.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt查体类型.Properties.Items.AddRange(new object[] {
            "症状",
            "身高体重",
            "血压",
            "生活方式",
            "脏器功能",
            "一般体格检查",
            "辅助检查",
            "心电图",
            "胸部射线",
            "B超",
            "主要现存问题",
            "主要用药情况",
            "非免疫规划预防接种",
            "健康评价",
            "健康指导",
            "结果反馈"});
            this.txt查体类型.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txt查体类型.Size = new System.Drawing.Size(212, 20);
            this.txt查体类型.TabIndex = 2;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.txt_医生姓名);
            this.panelControl3.Controls.Add(this.labelControl22);
            this.panelControl3.Controls.Add(this.labelControl25);
            this.panelControl3.Controls.Add(this.btnEmpty);
            this.panelControl3.Controls.Add(this.btnQuery);
            this.panelControl3.Controls.Add(this.labelControl26);
            this.panelControl3.Controls.Add(this.txt_科室);
            this.panelControl3.Controls.Add(this.pictureBox3);
            this.panelControl3.Controls.Add(this.labelControl27);
            this.panelControl3.Controls.Add(this.txt_查体模块);
            this.panelControl3.Controls.Add(this.txt_机构);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(889, 54);
            this.panelControl3.TabIndex = 13;
            // 
            // txt_医生姓名
            // 
            this.txt_医生姓名.Location = new System.Drawing.Point(383, 4);
            this.txt_医生姓名.Name = "txt_医生姓名";
            this.txt_医生姓名.Size = new System.Drawing.Size(122, 20);
            this.txt_医生姓名.TabIndex = 27;
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(317, 8);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(60, 14);
            this.labelControl22.TabIndex = 28;
            this.labelControl22.Text = "医生姓名：";
            // 
            // labelControl25
            // 
            this.labelControl25.Location = new System.Drawing.Point(317, 30);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(60, 14);
            this.labelControl25.TabIndex = 25;
            this.labelControl25.Text = "查体模块：";
            // 
            // btnEmpty
            // 
            this.btnEmpty.Location = new System.Drawing.Point(614, 5);
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.Size = new System.Drawing.Size(76, 41);
            this.btnEmpty.TabIndex = 21;
            this.btnEmpty.Text = "清空(&E)";
            this.btnEmpty.Click += new System.EventHandler(this.btnEmpty_Click);
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(530, 5);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(73, 42);
            this.btnQuery.TabIndex = 20;
            this.btnQuery.Text = "查询(&S)";
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // labelControl26
            // 
            this.labelControl26.Location = new System.Drawing.Point(100, 30);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(36, 14);
            this.labelControl26.TabIndex = 24;
            this.labelControl26.Text = "科室：";
            // 
            // txt_科室
            // 
            this.txt_科室.Location = new System.Drawing.Point(142, 28);
            this.txt_科室.Name = "txt_科室";
            this.txt_科室.Size = new System.Drawing.Size(151, 20);
            this.txt_科室.TabIndex = 15;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new System.Drawing.Point(5, 2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(50, 50);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 22;
            this.pictureBox3.TabStop = false;
            // 
            // labelControl27
            // 
            this.labelControl27.Location = new System.Drawing.Point(100, 7);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(36, 14);
            this.labelControl27.TabIndex = 16;
            this.labelControl27.Text = "机构：";
            // 
            // txt_查体模块
            // 
            this.txt_查体模块.Location = new System.Drawing.Point(383, 28);
            this.txt_查体模块.Name = "txt_查体模块";
            this.txt_查体模块.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt_查体模块.Properties.Items.AddRange(new object[] {
            "症状",
            "身高体重",
            "血压",
            "生活方式",
            "脏器功能",
            "一般体格检查",
            "辅助检查",
            "心电图",
            "胸部射线",
            "B超",
            "主要现存问题",
            "主要用药情况",
            "非免疫规划预防接种",
            "健康评价",
            "健康指导",
            "结果反馈"});
            this.txt_查体模块.Properties.PopupSizeable = true;
            this.txt_查体模块.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txt_查体模块.Size = new System.Drawing.Size(122, 20);
            this.txt_查体模块.TabIndex = 29;
            // 
            // txt_机构
            // 
            this.txt_机构.Location = new System.Drawing.Point(142, 4);
            this.txt_机构.Name = "txt_机构";
            this.txt_机构.Properties.AutoExpandAllNodes = false;
            this.txt_机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt_机构.Properties.NullText = "";
            this.txt_机构.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.txt_机构.Size = new System.Drawing.Size(151, 20);
            this.txt_机构.TabIndex = 14;
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(0, 0);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // frm医生信息
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.ClientSize = new System.Drawing.Size(895, 411);
            this.Name = "frm医生信息";
            this.Text = "医生信息";
            this.Load += new System.EventHandler(this.frm医生信息_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            this.tpDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit机构)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetailEditor)).EndInit();
            this.gcDetailEditor.ResumeLayout(false);
            this.gcDetailEditor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt是否城市.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职务.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt科室.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生名称.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit手签.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt查体类型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_医生姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_科室.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_查体模块.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcSummary;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSummary;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.GroupControl gcDetailEditor;
        private DevExpress.XtraEditors.TextEdit txt职务;
        private DevExpress.XtraEditors.TextEdit txt医生编号;
        private DevExpress.XtraEditors.LabelControl lab机构级别;
        private DevExpress.XtraEditors.LabelControl lab负责人;
        private DevExpress.XtraEditors.LabelControl lab机构编号;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt科室;
        private DevExpress.XtraEditors.LabelControl lab备注;
        private DevExpress.XtraEditors.LabelControl lab联系电话;
        private DevExpress.XtraEditors.LabelControl lab联系人;
        private DevExpress.XtraEditors.TextEdit txt医生名称;
        private DevExpress.XtraEditors.LabelControl lab机构名称;
        private DevExpress.XtraEditors.LabelControl lab上级;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.TextEdit txt_医生姓名;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.SimpleButton btnEmpty;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.TextEdit txt_科室;
        private System.Windows.Forms.PictureBox pictureBox3;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl lab创建日期;
        private DevExpress.XtraEditors.DateEdit txt创建日期;
        private DevExpress.XtraEditors.LookUpEdit txt创建人;
        private DevExpress.XtraEditors.LabelControl lab创建人;
        private DevExpress.XtraEditors.LookUpEdit txt所属机构;
        private DevExpress.XtraEditors.CheckEdit txt是否城市;
        private DevExpress.XtraEditors.SimpleButton simpleButton上传;
        private DevExpress.XtraEditors.PictureEdit pictureEdit手签;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraEditors.ComboBoxEdit txt查体类型;
        private DevExpress.XtraEditors.ComboBoxEdit txt_查体模块;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEdit机构;
        private DevExpress.XtraEditors.TreeListLookUpEdit txt_机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
    }
}
