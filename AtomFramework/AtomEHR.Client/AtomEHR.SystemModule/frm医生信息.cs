﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.Models;
using AtomEHR.Interfaces;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.IO.Compression;

namespace AtomEHR.SystemModule
{
    public partial class frm医生信息 : AtomEHR.Library.frmBaseDataDictionary
    {
        private bll医生信息 _BllCustomer; //业务逻辑层对象引用

        public frm医生信息()
        {
            InitializeComponent();
        }

        private void frm医生信息_Load(object sender, EventArgs e)
        {
            this.InitializeForm();//自定义初始化操作  
            Init机构();
        }
        protected override void InitializeForm()
        {
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.
            _ActiveEditor = txt医生名称;
            _KeyEditor = txt医生编号;
            _DetailGroupControl = gcDetailEditor;
            _BLL = new bll医生信息(); //业务逻辑实例
            _BllCustomer = _BLL as bll医生信息; //本窗体引用

            base.InitializeForm();

            //绑定相关缓存数据

            //DataBinder.BindingLookupEditDataSource(LookUpEdit机构, DataDictCache.Cache.t机构信息, tb_机构信息._机构名称, tb_机构信息._机构编号);
            DataBinder.BindingLookupEditDataSource(txt所属机构, _BLL.ConvertRowsToTable(DataDictCache.Cache.t机构信息.Select("上级机构 ='" + Loginer.CurrentUser.所属机构 + "' ")),
                    tb_机构信息._机构名称, tb_机构信息._机构编号);
            DataBinder.BindingLookupEditDataSource(txt创建人, DataDictCache.Cache.User, TUser.UserName, TUser.用户编码);

            ShowSummary(); //下载显示数据.
            BindingSummaryNavigator(controlNavigatorSummary, gcSummary); //Summary导航条.
            ShowSummaryPage(true); //一切初始化完毕後显示SummaryPage      
        }

        private void Init机构()
        {
            LookUpEdit机构.ValueMember = "机构编号";
            LookUpEdit机构.DisplayMember = "机构名称";
            LookUpEdit机构.DataSource = DataDictCache.Cache.t机构信息;
            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                txt_机构.Properties.ValueMember = "机构编号";
                txt_机构.Properties.DisplayMember = "机构名称";

                txt_机构.Properties.TreeList.KeyFieldName = "机构编号";
                txt_机构.Properties.TreeList.ParentFieldName = "上级机构";
                txt_机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                txt_机构.Properties.DataSource = dt所属机构;
                //txt机构.Properties.TreeList.ExpandAll();
                //txt机构.Properties.TreeList.CollapseAll();

                txt_机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                txt_机构.Text = Loginer.CurrentUser.所属机构;
            }
        }

        public override void DoSave(IButtonInfo sender)
        {
            UpdateLastControl(); //更新最后一个输入控件的数据

            if (!ValidatingData()) return; //检查输入完整性
            //新增时获取新的机构编号
            if (UpdateType.Add == _UpdateType)
            {
                _BLL.DataBinder.Rows[0][tb_医生信息.b编码] = _BllCustomer.GetNew医生编码(txt所属机构.EditValue.ToString()); //新增用户时获取新的编号                   
            }

            base.DoSave(sender);
            //this.txt机构编号.Text = _BLL.DataBinder.Rows[0][tb_医生信息._机构编号].ToString();           
        }
        
        // 检查主表数据是否完整或合法
        protected override bool ValidatingData()
        {
            if (txt查体类型.Text == string.Empty)
            {
                Msg.Warning("查体类型不能为空!");
                txt查体类型.Focus();
                return false;
            }

            if (txt医生名称.Text == string.Empty)
            {
                Msg.Warning("医生名称不能为空!");
                txt医生名称.Focus();
                return false;
            }

            if (_UpdateType == UpdateType.Add)
            {
                if (_BLL.CheckNoExists(txt医生编号.Text))
                {
                    Msg.Warning("医生编号已存在!");
                    txt医生编号.Focus();
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 绑定输入框
        /// </summary>
        /// <param name="summary"></param>
        protected override void DoBindingSummaryEditor(DataTable summary)
        {
            try
            {
                if (summary == null) return;
                DataBinder.BindingTextEdit(txt医生编号, summary, tb_医生信息.b编码);
                DataBinder.BindingTextEdit(txt医生名称, summary, tb_医生信息.x医生姓名);
                DataBinder.BindingTextEdit(txt查体类型, summary, tb_医生信息.c查体模块);
                DataBinder.BindingTextEdit(txt职务, summary, tb_医生信息.z职务);
                DataBinder.BindingTextEdit(txt科室, summary, tb_医生信息.k科室);
                DataBinder.BindingTextEdit(txt身份证号, summary, tb_医生信息.s身份证号);
                DataBinder.BindingTextEdit(txt所属机构, summary, tb_医生信息.s所属机构);
                if(_BLL.DataBinder.Rows[0][tb_医生信息.s手签]!=DBNull.Value)
                    DataBinder.BindingImageEdit(pictureEdit手签, summary, tb_医生信息.s手签);
                DataBinder.BindingTextEdit(txt创建人, summary, tb_医生信息.c创建人);
                DataBinder.BindingTextEdit(txt创建日期, summary, tb_医生信息.c创建时间);
            }
            catch (Exception ex)
            { Msg.ShowException(ex); }
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            //搜索数据            
            this._BllCustomer.SearchBy(txt_机构.EditValue.ToString(), txt_科室.Text, txt_医生姓名.Text, txt_查体模块.Text, true);
            this.DoBindingSummaryGrid(_BLL.SummaryTable); //绑定主表的Grid
            this.ShowSummaryPage(true); //显示Summary页面. 
        }

        private void btnEmpty_Click(object sender, EventArgs e)
        {
            base.ClearContainerEditorText(panelControl3);     
        }

        protected override void ButtonStateChanged(UpdateType currentState)// 按钮状态改变时触发的事件
        {
            //设置页面的控件
            this.SetDetailEditorsAccessable(gcDetailEditor, this.DataChanged);

            if (currentState == Common.UpdateType.Add)
            {
                _BLL.DataBinder.Rows[0][tb_医生信息.b编码] = "*自动生成*";
                _BLL.DataBinder.Rows[0][tb_医生信息.s所属机构] = Loginer.CurrentUser.所属机构;
                _BLL.DataBinder.Rows[0][tb_医生信息.c创建人] = Loginer.CurrentUser.用户编码;
                _BLL.DataBinder.Rows[0][tb_医生信息.c创建时间] = DateTime.Today;
                this.pictureEdit手签.Image = (Image)null;
            }
            else if (currentState == Common.UpdateType.Modify)
            {
                this.txt医生编号.Properties.ReadOnly = true;
                //_BLL.DataBinder.Rows[0][tb_医生信息._修改人] = Loginer.CurrentUser.Account;
                //_BLL.DataBinder.Rows[0][tb_医生信息._修改时间] = DateTime.Today;
            }
            //管理员能锁定上机机构
            this.txt所属机构.Enabled = Loginer.CurrentUser.IsAdmin();
            txt创建人.Enabled = false;
            txt创建日期.Enabled = false;
            //this.SetEditorEnable(txt机构, false, true);
        }

        private void simpleButton上传_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "选择图片";
            openFileDialog.Filter = "jpg文件(*.jpg)|*.jpg";
            DialogResult dialogResult = openFileDialog.ShowDialog(this);
            if (dialogResult == DialogResult.OK)
            {
                this.pictureEdit手签.Image = Image.FromFile(openFileDialog.FileName);

                _BLL.DataBinder.Rows[0][tb_医生信息.s手签] = CompressionObject(this.pictureEdit手签.Image);
                //HIS.COMM.Class医生.ysImageSaveToDb(HIS.COMM.Class医生.PhotoToArray(openFileDialog.FileName), _s用户编码);
                //XtraMessageBox.Show("图片上传成功", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        public static byte[] CompressionObject(object DataOriginal)
        {
            if (DataOriginal == null) return null;
            BinaryFormatter bFormatter = new BinaryFormatter();
            MemoryStream mStream = new MemoryStream();
            bFormatter.Serialize(mStream, DataOriginal);
            byte[] bytes = mStream.ToArray();
            MemoryStream oStream = new MemoryStream();
            DeflateStream zipStream = new DeflateStream(oStream, CompressionMode.Compress);
            zipStream.Write(bytes, 0, bytes.Length);
            zipStream.Flush();
            zipStream.Close();
            return oStream.ToArray();
        }

        private void repositoryItemPictureEdit1_FormatEditValue(object sender, DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs e)
        {
            try
            {
                if (e.Value != null && e.Value != DBNull.Value)
                {
                    System.Drawing.Image img = (Image)ZipTools.DecompressionObject((byte[])e.Value);
                    e.Value = img;
                }
                else
                    e.Value = (Image)null;
            }
            catch
            {
                e.Value = (Image)null;
            }
        }

        private void gvSummary_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "s手签" && e.IsGetData)
            {
                Console.WriteLine("gvSummary_CustomUnboundColumnData");
                //try
                //{
                //    if (e.Value != null && e.Value != DBNull.Value)
                //    {
                //        System.Drawing.Image img = (Image)ZipTools.DecompressionObject((byte[])e.Value);
                //        e.Value = img;
                //    }
                //    else
                //        e.Value = (Image)null;
                //}
                //catch
                //{
                //    e.Value = (Image)null;
                //}
            }

        }

    }
}
