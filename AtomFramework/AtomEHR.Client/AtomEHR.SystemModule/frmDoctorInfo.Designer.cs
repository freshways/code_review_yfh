namespace AtomEHR.SystemModule
{
    partial class frmDoctorInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDoctorInfo));
            this.gcSummary = new DevExpress.XtraGrid.GridControl();
            this.gvSummary = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col医生编码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col医生姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col性别 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col出生日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col职务 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col科室 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col身份证号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col查体模块 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col是否停用 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col所属机构 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEdit机构 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.col创建时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col创建人 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEdit创建人 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.chk是否停用 = new DevExpress.XtraEditors.CheckEdit();
            this.txt职务 = new DevExpress.XtraEditors.TextEdit();
            this.txt医生姓名 = new DevExpress.XtraEditors.TextEdit();
            this.labAccount = new System.Windows.Forms.Label();
            this.gcDetailEditor = new DevExpress.XtraEditors.GroupControl();
            this.lue所属机构 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit_SmallImg = new DevExpress.XtraEditors.PictureEdit();
            this.sBut_SelectImg = new DevExpress.XtraEditors.SimpleButton();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureEdit_BigImg = new DevExpress.XtraEditors.PictureEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbe查体模块 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txt科室 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txt医生编码 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证 = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.comboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.txt_姓名 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.btnEmpty = new DevExpress.XtraEditors.SimpleButton();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.txt_用户标识 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.lue机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.txt_是否 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            this.tpDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit机构)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit创建人)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk是否停用.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职务.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetailEditor)).BeginInit();
            this.gcDetailEditor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lue所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit_SmallImg.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit_BigImg.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbe查体模块.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt科室.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生编码.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_用户标识.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lue机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_是否.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gcSummary);
            this.tpSummary.Controls.Add(this.panelControl3);
            this.tpSummary.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tpSummary.Size = new System.Drawing.Size(896, 442);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 28);
            this.pnlSummary.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlSummary.Size = new System.Drawing.Size(902, 471);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tcBusiness.Size = new System.Drawing.Size(902, 471);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Controls.Add(this.gcDetailEditor);
            this.tpDetail.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tpDetail.Size = new System.Drawing.Size(896, 442);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcNavigator.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gcNavigator.Size = new System.Drawing.Size(902, 28);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(724, 3);
            this.controlNavigatorSummary.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            // 
            // txtFocusForSave
            // 
            this.txtFocusForSave.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(527, 3);
            // 
            // gcSummary
            // 
            this.gcSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSummary.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcSummary.Location = new System.Drawing.Point(0, 54);
            this.gcSummary.MainView = this.gvSummary;
            this.gcSummary.Name = "gcSummary";
            this.gcSummary.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.LookUpEdit机构,
            this.LookUpEdit创建人});
            this.gcSummary.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.gcSummary.Size = new System.Drawing.Size(896, 388);
            this.gcSummary.TabIndex = 7;
            this.gcSummary.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSummary});
            // 
            // gvSummary
            // 
            this.gvSummary.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col医生编码,
            this.col医生姓名,
            this.col性别,
            this.col出生日期,
            this.col职务,
            this.col科室,
            this.col身份证号,
            this.col查体模块,
            this.col是否停用,
            this.col所属机构,
            this.col创建时间,
            this.col创建人});
            this.gvSummary.GridControl = this.gcSummary;
            this.gvSummary.Name = "gvSummary";
            this.gvSummary.OptionsView.ColumnAutoWidth = false;
            this.gvSummary.OptionsView.ShowFooter = true;
            this.gvSummary.OptionsView.ShowGroupPanel = false;
            // 
            // col医生编码
            // 
            this.col医生编码.AppearanceHeader.Options.UseTextOptions = true;
            this.col医生编码.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col医生编码.Caption = "医生编码";
            this.col医生编码.FieldName = "b编码";
            this.col医生编码.Name = "col医生编码";
            this.col医生编码.Visible = true;
            this.col医生编码.VisibleIndex = 0;
            this.col医生编码.Width = 82;
            // 
            // col医生姓名
            // 
            this.col医生姓名.AppearanceHeader.Options.UseTextOptions = true;
            this.col医生姓名.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col医生姓名.Caption = "医生姓名";
            this.col医生姓名.FieldName = "x医生姓名";
            this.col医生姓名.Name = "col医生姓名";
            this.col医生姓名.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "UserName", "共计:{0}")});
            this.col医生姓名.Visible = true;
            this.col医生姓名.VisibleIndex = 1;
            this.col医生姓名.Width = 68;
            // 
            // col性别
            // 
            this.col性别.AppearanceHeader.Options.UseTextOptions = true;
            this.col性别.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col性别.Caption = "性别";
            this.col性别.FieldName = "x性别";
            this.col性别.Name = "col性别";
            this.col性别.Visible = true;
            this.col性别.VisibleIndex = 4;
            this.col性别.Width = 44;
            // 
            // col出生日期
            // 
            this.col出生日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col出生日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col出生日期.Caption = "出生日期";
            this.col出生日期.FieldName = "c出生日期";
            this.col出生日期.Name = "col出生日期";
            this.col出生日期.Visible = true;
            this.col出生日期.VisibleIndex = 5;
            this.col出生日期.Width = 86;
            // 
            // col职务
            // 
            this.col职务.Caption = "职务";
            this.col职务.FieldName = "z职务";
            this.col职务.Name = "col职务";
            this.col职务.Visible = true;
            this.col职务.VisibleIndex = 6;
            this.col职务.Width = 59;
            // 
            // col科室
            // 
            this.col科室.Caption = "科室";
            this.col科室.FieldName = "k科室";
            this.col科室.Name = "col科室";
            this.col科室.Visible = true;
            this.col科室.VisibleIndex = 3;
            this.col科室.Width = 61;
            // 
            // col身份证号
            // 
            this.col身份证号.AppearanceCell.Options.UseTextOptions = true;
            this.col身份证号.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col身份证号.AppearanceHeader.Options.UseTextOptions = true;
            this.col身份证号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col身份证号.Caption = "身份证号";
            this.col身份证号.FieldName = "s身份证号";
            this.col身份证号.Name = "col身份证号";
            this.col身份证号.Visible = true;
            this.col身份证号.VisibleIndex = 7;
            this.col身份证号.Width = 100;
            // 
            // col查体模块
            // 
            this.col查体模块.Caption = "查体模块";
            this.col查体模块.FieldName = "c查体模块";
            this.col查体模块.Name = "col查体模块";
            this.col查体模块.Visible = true;
            this.col查体模块.VisibleIndex = 8;
            this.col查体模块.Width = 60;
            // 
            // col是否停用
            // 
            this.col是否停用.AppearanceCell.Options.UseTextOptions = true;
            this.col是否停用.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col是否停用.AppearanceHeader.Options.UseTextOptions = true;
            this.col是否停用.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col是否停用.Caption = "是否停用";
            this.col是否停用.FieldName = "s是否停用";
            this.col是否停用.Name = "col是否停用";
            this.col是否停用.Visible = true;
            this.col是否停用.VisibleIndex = 9;
            this.col是否停用.Width = 60;
            // 
            // col所属机构
            // 
            this.col所属机构.Caption = "所属机构";
            this.col所属机构.ColumnEdit = this.LookUpEdit机构;
            this.col所属机构.FieldName = "s所属机构";
            this.col所属机构.Name = "col所属机构";
            this.col所属机构.Visible = true;
            this.col所属机构.VisibleIndex = 2;
            this.col所属机构.Width = 87;
            // 
            // LookUpEdit机构
            // 
            this.LookUpEdit机构.AutoHeight = false;
            this.LookUpEdit机构.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit机构.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("机构编号", "机构编号", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("机构名称", "机构名称")});
            this.LookUpEdit机构.Name = "LookUpEdit机构";
            this.LookUpEdit机构.NullText = "";
            // 
            // col创建时间
            // 
            this.col创建时间.AppearanceCell.Options.UseTextOptions = true;
            this.col创建时间.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col创建时间.AppearanceHeader.Options.UseTextOptions = true;
            this.col创建时间.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col创建时间.Caption = "创建时间";
            this.col创建时间.FieldName = "c创建时间";
            this.col创建时间.Name = "col创建时间";
            this.col创建时间.Visible = true;
            this.col创建时间.VisibleIndex = 10;
            this.col创建时间.Width = 100;
            // 
            // col创建人
            // 
            this.col创建人.Caption = "创建人";
            this.col创建人.ColumnEdit = this.LookUpEdit创建人;
            this.col创建人.FieldName = "c创建人";
            this.col创建人.Name = "col创建人";
            this.col创建人.Visible = true;
            this.col创建人.VisibleIndex = 11;
            // 
            // LookUpEdit创建人
            // 
            this.LookUpEdit创建人.AutoHeight = false;
            this.LookUpEdit创建人.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit创建人.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("用户编码", "用户编码"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("UserName", "UserName")});
            this.LookUpEdit创建人.Name = "LookUpEdit创建人";
            this.LookUpEdit创建人.NullText = "";
            // 
            // chk是否停用
            // 
            this.chk是否停用.EditValue = "N";
            this.chk是否停用.Location = new System.Drawing.Point(274, 34);
            this.chk是否停用.Name = "chk是否停用";
            this.chk是否停用.Properties.Caption = "是否停用";
            this.chk是否停用.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.chk是否停用.Properties.ValueChecked = "Y";
            this.chk是否停用.Properties.ValueGrayed = "\"\"";
            this.chk是否停用.Properties.ValueUnchecked = "N";
            this.chk是否停用.Size = new System.Drawing.Size(97, 19);
            this.chk是否停用.TabIndex = 12;
            // 
            // txt职务
            // 
            this.txt职务.Location = new System.Drawing.Point(93, 111);
            this.txt职务.Name = "txt职务";
            this.txt职务.Size = new System.Drawing.Size(155, 20);
            this.txt职务.TabIndex = 5;
            // 
            // txt医生姓名
            // 
            this.txt医生姓名.Location = new System.Drawing.Point(93, 59);
            this.txt医生姓名.Name = "txt医生姓名";
            this.txt医生姓名.Size = new System.Drawing.Size(155, 20);
            this.txt医生姓名.TabIndex = 92;
            // 
            // labAccount
            // 
            this.labAccount.AutoSize = true;
            this.labAccount.Location = new System.Drawing.Point(22, 61);
            this.labAccount.Name = "labAccount";
            this.labAccount.Size = new System.Drawing.Size(59, 14);
            this.labAccount.TabIndex = 103;
            this.labAccount.Text = "医生姓名:";
            // 
            // gcDetailEditor
            // 
            this.gcDetailEditor.Controls.Add(this.lue所属机构);
            this.gcDetailEditor.Controls.Add(this.labelControl1);
            this.gcDetailEditor.Controls.Add(this.pictureEdit_SmallImg);
            this.gcDetailEditor.Controls.Add(this.sBut_SelectImg);
            this.gcDetailEditor.Controls.Add(this.label9);
            this.gcDetailEditor.Controls.Add(this.pictureEdit_BigImg);
            this.gcDetailEditor.Controls.Add(this.label6);
            this.gcDetailEditor.Controls.Add(this.label2);
            this.gcDetailEditor.Controls.Add(this.cbe查体模块);
            this.gcDetailEditor.Controls.Add(this.label7);
            this.gcDetailEditor.Controls.Add(this.label5);
            this.gcDetailEditor.Controls.Add(this.label4);
            this.gcDetailEditor.Controls.Add(this.txt科室);
            this.gcDetailEditor.Controls.Add(this.labelControl3);
            this.gcDetailEditor.Controls.Add(this.labelControl6);
            this.gcDetailEditor.Controls.Add(this.label8);
            this.gcDetailEditor.Controls.Add(this.txt医生姓名);
            this.gcDetailEditor.Controls.Add(this.chk是否停用);
            this.gcDetailEditor.Controls.Add(this.labAccount);
            this.gcDetailEditor.Controls.Add(this.label1);
            this.gcDetailEditor.Controls.Add(this.txt医生编码);
            this.gcDetailEditor.Controls.Add(this.txt身份证);
            this.gcDetailEditor.Controls.Add(this.txt职务);
            this.gcDetailEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcDetailEditor.Location = new System.Drawing.Point(0, 0);
            this.gcDetailEditor.Name = "gcDetailEditor";
            this.gcDetailEditor.Size = new System.Drawing.Size(896, 442);
            this.gcDetailEditor.TabIndex = 119;
            // 
            // lue所属机构
            // 
            this.lue所属机构.Location = new System.Drawing.Point(93, 189);
            this.lue所属机构.Name = "lue所属机构";
            this.lue所属机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lue所属机构.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("机构编码", "编码"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("机构名称", "名称")});
            this.lue所属机构.Properties.NullText = "";
            this.lue所属机构.Size = new System.Drawing.Size(155, 20);
            this.lue所属机构.TabIndex = 408;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(25, 234);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(373, 56);
            this.labelControl1.TabIndex = 407;
            this.labelControl1.Text = "说明:选择医生手签图片的后需要进行剪裁,以适应显示框大小,选择图片\r\n点击确定后会自动进入Cutter(剪切)窗体,在图片上合适的地方按住鼠标\r\n左键进行截图,松" +
    "开鼠标左键结束截图,鼠标左键双击截取的图完成截图\r\n并返回。";
            // 
            // pictureEdit_SmallImg
            // 
            this.pictureEdit_SmallImg.Location = new System.Drawing.Point(345, 147);
            this.pictureEdit_SmallImg.Name = "pictureEdit_SmallImg";
            this.pictureEdit_SmallImg.Properties.NullText = "选择图片";
            this.pictureEdit_SmallImg.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit_SmallImg.Size = new System.Drawing.Size(69, 25);
            this.pictureEdit_SmallImg.TabIndex = 405;
            // 
            // sBut_SelectImg
            // 
            this.sBut_SelectImg.Location = new System.Drawing.Point(345, 178);
            this.sBut_SelectImg.Name = "sBut_SelectImg";
            this.sBut_SelectImg.Size = new System.Drawing.Size(69, 23);
            this.sBut_SelectImg.TabIndex = 404;
            this.sBut_SelectImg.Text = "选择图片";
            this.sBut_SelectImg.Click += new System.EventHandler(this.sBut_SelectImg_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(274, 65);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 14);
            this.label9.TabIndex = 403;
            this.label9.Text = "选择医生手签:";
            // 
            // pictureEdit_BigImg
            // 
            this.pictureEdit_BigImg.Location = new System.Drawing.Point(276, 91);
            this.pictureEdit_BigImg.Name = "pictureEdit_BigImg";
            this.pictureEdit_BigImg.Properties.NullText = "选择图片";
            this.pictureEdit_BigImg.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit_BigImg.Size = new System.Drawing.Size(138, 50);
            this.pictureEdit_BigImg.TabIndex = 402;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(254, 91);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 14);
            this.label6.TabIndex = 400;
            this.label6.Text = "*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(254, 195);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 14);
            this.label2.TabIndex = 399;
            this.label2.Text = "*";
            // 
            // cbe查体模块
            // 
            this.cbe查体模块.Location = new System.Drawing.Point(93, 163);
            this.cbe查体模块.Name = "cbe查体模块";
            this.cbe查体模块.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbe查体模块.Properties.Items.AddRange(new object[] {
            "症状",
            "身高体重",
            "血压",
            "生活方式",
            "脏器功能",
            "一般体格检查",
            "辅助检查",
            "心电图",
            "胸部射线",
            "B超",
            "主要现存问题",
            "主要用药情况",
            "非免疫规划预防接种",
            "健康评价",
            "健康指导",
            "结果反馈"});
            this.cbe查体模块.Size = new System.Drawing.Size(155, 20);
            this.cbe查体模块.TabIndex = 398;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 166);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 14);
            this.label7.TabIndex = 397;
            this.label7.Text = "查体模块:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(46, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 14);
            this.label5.TabIndex = 395;
            this.label5.Text = "科室:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(46, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 14);
            this.label4.TabIndex = 394;
            this.label4.Text = "职务:";
            // 
            // txt科室
            // 
            this.txt科室.Location = new System.Drawing.Point(93, 137);
            this.txt科室.Name = "txt科室";
            this.txt科室.Size = new System.Drawing.Size(155, 20);
            this.txt科室.TabIndex = 391;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(25, 36);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 14);
            this.labelControl3.TabIndex = 386;
            this.labelControl3.Text = "医生编码：";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(29, 192);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(52, 14);
            this.labelControl6.TabIndex = 386;
            this.labelControl6.Text = "所属机构:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(254, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(14, 14);
            this.label8.TabIndex = 135;
            this.label8.Text = "*";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 14);
            this.label1.TabIndex = 105;
            this.label1.Text = "身份证号:";
            // 
            // txt医生编码
            // 
            this.txt医生编码.EditValue = "*自动生成*";
            this.txt医生编码.Location = new System.Drawing.Point(93, 33);
            this.txt医生编码.Name = "txt医生编码";
            this.txt医生编码.Properties.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.txt医生编码.Properties.Appearance.Options.UseForeColor = true;
            this.txt医生编码.Properties.Appearance.Options.UseTextOptions = true;
            this.txt医生编码.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txt医生编码.Properties.ReadOnly = true;
            this.txt医生编码.Size = new System.Drawing.Size(155, 20);
            this.txt医生编码.TabIndex = 93;
            // 
            // txt身份证
            // 
            this.txt身份证.EditValue = "请填写正确身份证信息!";
            this.txt身份证.Location = new System.Drawing.Point(93, 85);
            this.txt身份证.Name = "txt身份证";
            this.txt身份证.Properties.NullText = "请填写正确身份证信息!";
            this.txt身份证.Size = new System.Drawing.Size(155, 20);
            this.txt身份证.TabIndex = 5;
            this.txt身份证.ToolTip = "身份证是信息化互联互通唯一的凭证!请填写正确!";
            this.txt身份证.ToolTipTitle = "提示";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(80, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "*";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.comboBoxEdit1);
            this.panelControl3.Controls.Add(this.pictureEdit1);
            this.panelControl3.Controls.Add(this.txt_姓名);
            this.panelControl3.Controls.Add(this.labelControl22);
            this.panelControl3.Controls.Add(this.labelControl25);
            this.panelControl3.Controls.Add(this.btnEmpty);
            this.panelControl3.Controls.Add(this.btnQuery);
            this.panelControl3.Controls.Add(this.labelControl4);
            this.panelControl3.Controls.Add(this.labelControl26);
            this.panelControl3.Controls.Add(this.txt_用户标识);
            this.panelControl3.Controls.Add(this.labelControl27);
            this.panelControl3.Controls.Add(this.lue机构);
            this.panelControl3.Controls.Add(this.txt_是否);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(896, 54);
            this.panelControl3.TabIndex = 389;
            // 
            // comboBoxEdit1
            // 
            this.comboBoxEdit1.Location = new System.Drawing.Point(142, 29);
            this.comboBoxEdit1.Name = "comboBoxEdit1";
            this.comboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit1.Properties.Items.AddRange(new object[] {
            "症状",
            "身高体重",
            "血压",
            "生活方式",
            "脏器功能",
            "一般体格检查",
            "辅助检查",
            "心电图",
            "胸部射线",
            "B超",
            "主要现存问题",
            "主要用药情况",
            "非免疫规划预防接种",
            "健康评价",
            "健康指导",
            "结果反馈"});
            this.comboBoxEdit1.Size = new System.Drawing.Size(138, 20);
            this.comboBoxEdit1.TabIndex = 31;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(9, 3);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Size = new System.Drawing.Size(48, 48);
            this.pictureEdit1.TabIndex = 30;
            // 
            // txt_姓名
            // 
            this.txt_姓名.Location = new System.Drawing.Point(504, 5);
            this.txt_姓名.Name = "txt_姓名";
            this.txt_姓名.Size = new System.Drawing.Size(114, 20);
            this.txt_姓名.TabIndex = 27;
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(462, 8);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(36, 14);
            this.labelControl22.TabIndex = 28;
            this.labelControl22.Text = "姓名：";
            // 
            // labelControl25
            // 
            this.labelControl25.Location = new System.Drawing.Point(286, 31);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(60, 14);
            this.labelControl25.TabIndex = 25;
            this.labelControl25.Text = "是否停用：";
            // 
            // btnEmpty
            // 
            this.btnEmpty.Location = new System.Drawing.Point(813, 6);
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.Size = new System.Drawing.Size(76, 41);
            this.btnEmpty.TabIndex = 21;
            this.btnEmpty.Text = "清空(&E)";
            this.btnEmpty.Click += new System.EventHandler(this.btnEmpty_Click);
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(734, 6);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(73, 42);
            this.btnQuery.TabIndex = 20;
            this.btnQuery.Text = "查询(&S)";
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(76, 32);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 14);
            this.labelControl4.TabIndex = 24;
            this.labelControl4.Text = "查体模块：";
            // 
            // labelControl26
            // 
            this.labelControl26.Location = new System.Drawing.Point(310, 7);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(36, 14);
            this.labelControl26.TabIndex = 24;
            this.labelControl26.Text = "编码：";
            // 
            // txt_用户标识
            // 
            this.txt_用户标识.Location = new System.Drawing.Point(352, 5);
            this.txt_用户标识.Name = "txt_用户标识";
            this.txt_用户标识.Size = new System.Drawing.Size(100, 20);
            this.txt_用户标识.TabIndex = 15;
            // 
            // labelControl27
            // 
            this.labelControl27.Location = new System.Drawing.Point(99, 7);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(36, 14);
            this.labelControl27.TabIndex = 16;
            this.labelControl27.Text = "机构：";
            // 
            // lue机构
            // 
            this.lue机构.EditValue = "txt机构";
            this.lue机构.Location = new System.Drawing.Point(142, 5);
            this.lue机构.Name = "lue机构";
            this.lue机构.Properties.AutoExpandAllNodes = false;
            this.lue机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lue机构.Properties.NullText = "";
            this.lue机构.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.lue机构.Size = new System.Drawing.Size(138, 20);
            this.lue机构.TabIndex = 14;
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(0, 0);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.RootValue = 1;
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // txt_是否
            // 
            this.txt_是否.Location = new System.Drawing.Point(352, 29);
            this.txt_是否.Name = "txt_是否";
            this.txt_是否.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt_是否.Properties.Items.AddRange(new object[] {
            "是",
            "否"});
            this.txt_是否.Properties.PopupSizeable = true;
            this.txt_是否.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txt_是否.Size = new System.Drawing.Size(100, 20);
            this.txt_是否.TabIndex = 29;
            // 
            // frmDoctorInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.ClientSize = new System.Drawing.Size(902, 499);
            this.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Name = "frmDoctorInfo";
            this.Text = "医生信息";
            this.Load += new System.EventHandler(this.frmDoctorInfo_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            this.tpDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit机构)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit创建人)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk是否停用.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职务.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetailEditor)).EndInit();
            this.gcDetailEditor.ResumeLayout(false);
            this.gcDetailEditor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lue所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit_SmallImg.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit_BigImg.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbe查体模块.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt科室.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生编码.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_用户标识.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lue机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_是否.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcSummary;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSummary;
        private DevExpress.XtraGrid.Columns.GridColumn col医生姓名;
        private DevExpress.XtraGrid.Columns.GridColumn col性别;
        private DevExpress.XtraGrid.Columns.GridColumn col出生日期;
        private DevExpress.XtraGrid.Columns.GridColumn col身份证号;
        private DevExpress.XtraGrid.Columns.GridColumn col是否停用;
        private DevExpress.XtraGrid.Columns.GridColumn col创建时间;
        private DevExpress.XtraEditors.CheckEdit chk是否停用;
        private DevExpress.XtraEditors.TextEdit txt职务;
        private DevExpress.XtraEditors.TextEdit txt医生姓名;
        private System.Windows.Forms.Label labAccount;
        private DevExpress.XtraEditors.GroupControl gcDetailEditor;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraGrid.Columns.GridColumn col创建人;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraGrid.Columns.GridColumn col查体模块;
        private DevExpress.XtraGrid.Columns.GridColumn col科室;
        private DevExpress.XtraGrid.Columns.GridColumn col职务;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txt医生编码;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.TextEdit txt_姓名;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.SimpleButton btnEmpty;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.TextEdit txt_用户标识;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraGrid.Columns.GridColumn col医生编码;
        private DevExpress.XtraEditors.TreeListLookUpEdit lue机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraEditors.ComboBoxEdit txt_是否;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn col所属机构;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEdit机构;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit txt身份证;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.TextEdit txt科室;
        private DevExpress.XtraEditors.ComboBoxEdit cbe查体模块;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.PictureEdit pictureEdit_BigImg;
        private DevExpress.XtraEditors.SimpleButton sBut_SelectImg;
        private DevExpress.XtraEditors.PictureEdit pictureEdit_SmallImg;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LookUpEdit lue所属机构;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEdit创建人;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit1;
    }
}
