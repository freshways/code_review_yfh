﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Business.Security;
using AtomEHR.Common;

namespace AtomEHR.SystemModule
{
    public partial class frm用户对照 : frmBase
    {
       private bllUser _DataProxy = new bllUser();//用户业务类

       string ehrID = "", ehr姓名 = "", ehr拼音代码 = "", ehr所属机构 = "";

        public frm用户对照()
        {
            InitializeComponent();
        }

        public frm用户对照(string _ehrID,string _ehr姓名,string _ehr拼音代码,string _ehr所属机构)
        {
            InitializeComponent();
            ehrID = _ehrID;
            ehr姓名 = _ehr姓名;
            ehr拼音代码 = _ehr拼音代码;
            ehr所属机构 = _ehr所属机构;
        }

        private void frm用户对照_Load(object sender, EventArgs e)
        {
            Binding用户();
            Binding对照信息();
        }
        #region PrivateFunction
        void Binding用户()
        {
            try
            {
                string 机构 = "";
                if(ehr所属机构.Length>=12)
                    机构 += "1" + ehr所属机构.Substring(10,2);
                DataTable dt检查部位 = _DataProxy.InterFaceUserList(机构);
                lookUpEdit用户编码.Properties.DataSource = dt检查部位;
                lookUpEdit用户编码.Properties.PopulateViewColumns();
                lookUpEdit用户编码.Properties.DisplayMember = "名称";
                lookUpEdit用户编码.Properties.ValueMember = "编码";
                lookUpEdit用户编码.Properties.View.Columns["名称"].Width = 100;
                //lookUpEdit用户编码.Properties.View.Columns["机构"].Visible = true;
                lookUpEdit用户编码.Properties.PopupFormSize = new System.Drawing.Size(500, 400);
            }
            catch (Exception ex)
            {
                MessageBox.Show("不能获取用户信息！请联系管理员！" + ex.Message);
            }
        }

        void Binding对照信息()
        {
            //string sql = "select * from InterFace_用户对照 where 对应用户id='" + ehrID + "' ";
            //执行sql
            DataTable dt = _DataProxy.InterFaceUserMapingList(ehrID);
            //绑定数据
            this.gridControl1.DataSource = dt;
        } 
        #endregion

        #region ButtonClick
        private void simpleButton保存_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.comboBoxEdit系统.Text.ToString().Trim()))
            {
                Msg.ShowInformation("请选择要对应的系统"); return;
            }
            if (string.IsNullOrEmpty(this.lookUpEdit用户编码.EditValue.ToString()))
            {
                Msg.ShowInformation("请选择要对应的用户"); return;
            }
            string sql = "insert into InterFace_用户对照(用户id,用户姓名,对应系统,对应用户id,对应用户姓名,拼音代码,所属机构) values('" +
                this.lookUpEdit用户编码.EditValue + "','" + this.lookUpEdit用户编码.Text + "','" + comboBoxEdit系统.Text + "','" + ehrID + "','" +
                ehr姓名 + "','" + ehr拼音代码 + "','" + ehr所属机构 + "')";
            if (_DataProxy.InterFaceUserExSql(sql))
            {
                Msg.ShowInformation("添加成功");
                Binding对照信息();
            }
        }

        private void simpleButton刷新_Click(object sender, EventArgs e)
        {
            Binding对照信息();
        }

        private void simpleButton删除_Click(object sender, EventArgs e)
        {
            DataRow summary = gridView1.GetDataRow(gridView1.FocusedRowHandle);
            if (summary == null)
            {
                Msg.ShowInformation("请选择用户再进行删除！"); return;
            }
            string sql = "delete from InterFace_用户对照 where ID='" + summary["ID"].ToString() + "' ";
           
                if (_DataProxy.InterFaceUserExSql(sql))
            {
                Msg.ShowInformation("删除成功");
                Binding对照信息();
            }
        }

        private void simpleButton取消_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        #endregion
    }
}
