﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.Models;
using AtomEHR.Interfaces;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.IO.Compression;

namespace AtomEHR.SystemModule
{
    public partial class frmB超报告模板 : AtomEHR.Library.frmBaseDataDictionary
    {
        private bllB超报告模板 _BllCustomer; //业务逻辑层对象引用

        public frmB超报告模板()
        {
            InitializeComponent();
        }

        private void frm医生信息_Load(object sender, EventArgs e)
        {
            this.InitializeForm();//自定义初始化操作  
        }
        protected override void InitializeForm()
        {
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.
            _ActiveEditor = txt超声提示;
            _KeyEditor = txt模板编号;
            _DetailGroupControl = gcDetailEditor;
            _BLL = new bllB超报告模板(); //业务逻辑实例
            _BllCustomer = _BLL as bllB超报告模板; //本窗体引用

            base.InitializeForm();

            //绑定相关缓存数据

            DataBinder.BindingLookupEditDataSource(repositoryItemLookUpEdit1, DataDictCache.Cache.t机构信息,
                tb_机构信息._机构名称, tb_机构信息._机构编号);
            DataBinder.BindingLookupEditDataSource(txt所属机构, _BLL.ConvertRowsToTable(DataDictCache.Cache.t机构信息.Select("上级机构 ='" + Loginer.CurrentUser.所属机构 + "' ")),
                    tb_机构信息._机构名称, tb_机构信息._机构编号);
            DataBinder.BindingLookupEditDataSource(txt创建人, DataDictCache.Cache.User, TUser.UserName, TUser.用户编码);

            ShowSummary(); //下载显示数据.
            BindingSummaryNavigator(controlNavigatorSummary, gcSummary); //Summary导航条.
            ShowSummaryPage(true); //一切初始化完毕後显示SummaryPage      
        }

        public override void DoSave(IButtonInfo sender)
        {
            UpdateLastControl(); //更新最后一个输入控件的数据

            if (!ValidatingData()) return; //检查输入完整性
            //新增时获取新的机构编号
            if (UpdateType.Add == _UpdateType)
            {
                //新增用户时获取新的编号                   
            }
            base.DoSave(sender);          
        }
        
        // 检查主表数据是否完整或合法
        protected override bool ValidatingData()
        {
            if (txt检查部位.Text == string.Empty)
            {
                Msg.Warning("检查部位不能为空!");
                txt检查部位.Focus();
                return false;
            }

            if (txt超声提示.Text == string.Empty)
            {
                Msg.Warning("超声提示不能为空!");
                txt超声提示.Focus();
                return false;
            }

            if (txt超声所见.Text == string.Empty)
            {
                Msg.Warning("超声所见不能为空!");
                txt超声所见.Focus();
                return false;
            }

            //if (_UpdateType == UpdateType.Add)
            //{
            //    if (_BLL.CheckNoExists(txt模板编号.Text))
            //    {
            //        Msg.Warning("模板编号已存在!");
            //        txt模板编号.Focus();
            //        return false;
            //    }
            //}
            return true;
        }

        /// <summary>
        /// 绑定输入框
        /// </summary>
        /// <param name="summary"></param>
        protected override void DoBindingSummaryEditor(DataTable summary)
        {
            try
            {
                if (summary == null) return;
                //DataBinder.BindingTextEdit(txt模板编号, summary, tb_B超报告模板.ID);
                DataBinder.BindingTextEdit(txt检查部位, summary, tb_B超报告模板.检查部位);
                DataBinder.BindingTextEdit(txt超声提示, summary, tb_B超报告模板.超声提示);
                DataBinder.BindingTextEdit(txt超声所见, summary, tb_B超报告模板.超声所见);
                DataBinder.BindingTextEdit(txt备注, summary, tb_B超报告模板.备注);
                DataBinder.BindingTextEdit(txt所属机构, summary, tb_B超报告模板.所属机构);
                DataBinder.BindingTextEdit(txt创建人, summary, tb_B超报告模板.创建人);
                DataBinder.BindingTextEdit(txt创建日期, summary, tb_B超报告模板.创建日期);
            }
            catch (Exception ex)
            { Msg.ShowException(ex); }
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            //搜索数据            
            //this._BllCustomer.SearchBy(txt_CustomerFrom.Text, txt_CustomerTo.Text, txt_Name.Text, ConvertEx.ToString(txt_Attr.EditValue), true);
            this.DoBindingSummaryGrid(_BLL.SummaryTable); //绑定主表的Grid
            this.ShowSummaryPage(true); //显示Summary页面. 
        }

        private void btnEmpty_Click(object sender, EventArgs e)
        {
            base.ClearContainerEditorText(panelControl3);     
        }

        protected override void ButtonStateChanged(UpdateType currentState)// 按钮状态改变时触发的事件
        {
            //设置页面的控件
            this.SetDetailEditorsAccessable(gcDetailEditor, this.DataChanged);

            if (currentState == Common.UpdateType.Add)
            {
                //_BLL.DataBinder.Rows[0][tb_B超报告模板.ID] = "*自动生成*";
                _BLL.DataBinder.Rows[0][tb_B超报告模板.所属机构] = Loginer.CurrentUser.所属机构;
                _BLL.DataBinder.Rows[0][tb_B超报告模板.创建人] = Loginer.CurrentUser.用户编码;
                _BLL.DataBinder.Rows[0][tb_B超报告模板.创建日期] = DateTime.Today;
            }
            else if (currentState == Common.UpdateType.Modify)
            {
                this.txt模板编号.Properties.ReadOnly = true;
                //_BLL.DataBinder.Rows[0][tb_B超报告模板._修改人] = Loginer.CurrentUser.Account;
                //_BLL.DataBinder.Rows[0][tb_B超报告模板._修改时间] = DateTime.Today;
            }
            //管理员能锁定上机机构
            this.txt所属机构.Enabled = Loginer.CurrentUser.IsAdmin();
            txt创建人.Enabled = false;
            txt创建日期.Enabled = false;
        }       

    }
}
