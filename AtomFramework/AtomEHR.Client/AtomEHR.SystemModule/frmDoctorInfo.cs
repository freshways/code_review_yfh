using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using AtomEHR.Library;
using AtomEHR.Business;
using AtomEHR.Interfaces;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.Core;
using AtomEHR.Business.Security;
using AtomEHR.ReportsFastReport;
using AtomEHR.ReportsDevExpress;
using AtomEHR.Business.BLL_Base;
using System.IO;

namespace AtomEHR.SystemModule
{
    public partial class frmDoctorInfo : AtomEHR.Library.frmBaseDataDictionary
    {
        private bll医生信息 _BllCustomer; //业务逻辑层对象引用
        private AnalysisIDCard Analysis;
        public frmDoctorInfo()
        {
            InitializeComponent();
        }

        private void frmDoctorInfo_Load(object sender, EventArgs e)
        {
            this.InitializeForm();//自定义初始化操作  
        }

        protected override void InitializeForm()
        {
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.
            _ActiveEditor = txt医生姓名;
            _KeyEditor = txt医生编码;
            _DetailGroupControl = gcDetailEditor;
            _BLL = new bll医生信息(); //业务逻辑实例
            _BllCustomer = _BLL as bll医生信息; //本窗体引用

            base.InitializeForm();

            //绑定相关缓存数据

            DataBinder.BindingLookupEditDataSource(LookUpEdit机构, _BLL.ConvertRowsToTable(DataDictCache.Cache.t机构信息.Select("1=1")),
            tb_机构信息._机构名称, tb_机构信息._机构编号);
            DataBinder.BindingLookupEditDataSource(LookUpEdit创建人, _BLL.ConvertRowsToTable(DataDictCache.Cache.User.Select("1=1")),
            TUser.UserName, TUser.用户编码);
            DataBinder.BindingLookupEditDataSource(lue所属机构, _BLL.ConvertRowsToTable(DataDictCache.Cache.t机构信息.Select("1=1")),
            tb_机构信息._机构名称, tb_机构信息._机构编号);

            ShowSummary(); //下载显示数据.
            BindingSummaryNavigator(controlNavigatorSummary, gcSummary); //Summary导航条.
            ShowSummaryPage(true); //一切初始化完毕後显示SummaryPage      
        }

        public override void DoSave(IButtonInfo sender)
        {
            UpdateLastControl(); //更新最后一个输入控件的数据

            if (!ValidatingData()) return; //检查输入完整性
            //新增时获取新的机构编号
            if (UpdateType.Add == _UpdateType)
            {
                _BLL.DataBinder.Rows[0][tb_医生信息.b编码] = _BllCustomer.GetNew医生编码(lue所属机构.EditValue.ToString()); //新增用户时获取新的编号                   
            }

            if (Analysis.info.是否有效)
            {
                _BLL.DataBinder.Rows[0][tb_医生信息.x性别] = Analysis.info.性别;
                _BLL.DataBinder.Rows[0][tb_医生信息.c出生日期] = Analysis.info.出生日期.ToString("yyyy-MM-dd");
            }

            if (pictureEdit_BigImg.Image != null)
            {
                _BLL.DataBinder.Rows[0][tb_医生信息.s手签] = ZipTools.CompressionObject(pictureEdit_BigImg.Image);
            }

            base.DoSave(sender);
            //this.txt机构编号.Text = _BLL.DataBinder.Rows[0][tb_医生信息._机构编号].ToString();           
        }


        // 检查主表数据是否完整或合法
        protected override bool ValidatingData()
        {
            if (cbe查体模块.Text == string.Empty)
            {
                Msg.Warning("查体类型不能为空!");
                cbe查体模块.Focus();
                return false;
            }

            if (txt医生姓名.Text == string.Empty)
            {
                Msg.Warning("医生名称不能为空!");
                txt医生姓名.Focus();
                return false;
            }

            if (txt身份证.Text == string.Empty)
            {
                Msg.Warning("身份证号不能为空!");
                txt身份证.Focus();
                return false;
            }
            else
            {
                Analysis = new AnalysisIDCard(txt身份证.Text);
                if (!Analysis.info.是否有效)
                {
                    Msg.Warning(Analysis.info.错误信息);
                    txt身份证.Focus();
                    return false;
                }
            }

            if (_UpdateType == UpdateType.Add)
            {
                if (_BLL.CheckNoExists(txt医生编码.Text))
                {
                    Msg.Warning("医生编号已存在!");
                    txt医生编码.Focus();
                    return false;
                }
            }

            if (true)
            {
            }
            return true;
        }

        /// <summary>
        /// 绑定输入框
        /// </summary>
        /// <param name="summary"></param>
        protected override void DoBindingSummaryEditor(DataTable summary)
        {
            try
            {
                if (summary == null) return;
                DataBinder.BindingTextEdit(txt医生编码, summary, tb_医生信息.b编码);
                DataBinder.BindingTextEdit(txt医生姓名, summary, tb_医生信息.x医生姓名);
                DataBinder.BindingTextEdit(cbe查体模块, summary, tb_医生信息.c查体模块);
                DataBinder.BindingTextEdit(txt职务, summary, tb_医生信息.z职务);
                DataBinder.BindingTextEdit(txt科室, summary, tb_医生信息.k科室);
                DataBinder.BindingTextEdit(txt身份证, summary, tb_医生信息.s身份证号);
                DataBinder.BindingTextEdit(lue所属机构, summary, tb_医生信息.s所属机构);
                if (_BLL.DataBinder.Rows[0][tb_医生信息.s是否停用] == DBNull.Value)
                {
                    _BLL.DataBinder.Rows[0][tb_医生信息.s是否停用] = false;
                }
                DataBinder.BindingCheckEdit(chk是否停用, summary, tb_医生信息.s是否停用);
                if (_BLL.DataBinder.Rows[0][tb_医生信息.s手签] != DBNull.Value)
                {
                    DataBinder.BindingImageEdit(pictureEdit_BigImg, summary, tb_医生信息.s手签);
                    DataBinder.BindingImageEdit(pictureEdit_SmallImg, summary, tb_医生信息.s手签);
                }

            }
            catch (Exception ex)
            { Msg.ShowException(ex); }
        }

        protected override void ButtonStateChanged(UpdateType currentState)// 按钮状态改变时触发的事件
        {
            //设置页面的控件
            this.SetDetailEditorsAccessable(gcDetailEditor, this.DataChanged);

            if (currentState == Common.UpdateType.Add)
            {
                _BLL.DataBinder.Rows[0][tb_医生信息.b编码] = "*自动生成*";
                _BLL.DataBinder.Rows[0][tb_医生信息.s所属机构] = Loginer.CurrentUser.所属机构;
                _BLL.DataBinder.Rows[0][tb_医生信息.c创建人] = Loginer.CurrentUser.用户编码;
                _BLL.DataBinder.Rows[0][tb_医生信息.c创建时间] = DateTime.Today;
                this.pictureEdit_BigImg.Image = (Image)null;
                this.pictureEdit_SmallImg.Image = (Image)null;
            }
            else if (currentState == Common.UpdateType.Modify)
            {
                this.txt医生编码.Properties.ReadOnly = true;
            }
            //管理员能锁定上机机构
            this.lue所属机构.Enabled = Loginer.CurrentUser.IsAdmin();

            //this.SetEditorEnable(txt机构, false, true);
        }

        private void sBut_SelectImg_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "选择图片";
            openFileDialog.Filter = "jpg文件(*.jpg)|*.jpg";
            DialogResult dialogResult = openFileDialog.ShowDialog(this);
            if (dialogResult == DialogResult.OK)
            {
                Bitmap prototypeBmp = new Bitmap(openFileDialog.FileName);
                Cutter cutter = new Cutter(prototypeBmp, fixedWH.fixedWidth, 400);
                if (cutter.ShowDialog() == DialogResult.OK)
                {
                    Image img = new Bitmap(cutter.img);
                    pictureEdit_BigImg.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
                    pictureEdit_SmallImg.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
                    pictureEdit_BigImg.Image = img;
                    pictureEdit_SmallImg.Image = img;
                }
            }
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            //搜索数据            
            //this._BllCustomer.SearchBy(txt_CustomerFrom.Text, txt_CustomerTo.Text, txt_Name.Text, ConvertEx.ToString(txt_Attr.EditValue), true);
            this.DoBindingSummaryGrid(_BLL.SummaryTable); //绑定主表的Grid
            this.ShowSummaryPage(true); //显示Summary页面. 
        }

        private void btnEmpty_Click(object sender, EventArgs e)
        {
            base.ClearContainerEditorText(panelControl3);
        }


    }
}

