///*************************************************************************/
///*
///* 文件名    ：frmSystemMain.cs                   
///* 程序说明  : 系统管理模块主窗体
///* 原创作者  ：ATOM 
///* Copyright ©  2015 GGBond
///*
///**************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using System.Reflection;
using AtomEHR.Interfaces;
using AtomEHR.Common;
using DevExpress.XtraEditors;

namespace AtomEHR.SystemModule
{
    /// <summary>
    /// 系统管理模块主窗体
    /// </summary>
    public partial class frmSystemMain : frmModuleBase
    {
        public frmSystemMain()
        {
            InitializeComponent();

            _ModuleID = ModuleID.SystemManage; //设置模块编号
            _ModuleName = ModuleNames.SystemManage;//设置模块名称
            menuStrip1.Text = ModuleNames.SystemManage; //与AssemblyModuleEntry.ModuleName定义相同

            this.MainMenuStrip = this.menuStrip1;

            SetMenuTag();
        }

        #region Init初始化
        public override MenuStrip GetModuleMenu()
        {
            return this.menuStrip1;
        }

        private void SetMenuTag()
        {
            menuSystemManager.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.SystemManage, AuthorityCategory.NONE);
            menuItemUserMgr.Tag = new MenuItemTag(MenuType.DataForm, (int)ModuleID.SystemManage, AuthorityCategory.MASTER_ACTION);
            menuItemAuth.Tag = new MenuItemTag(MenuType.DataForm, (int)ModuleID.SystemManage, AuthorityCategory.MASTER_ACTION);
            menuItemSetup.Tag = new MenuItemTag(MenuType.Dialog, (int)ModuleID.SystemManage, AuthorityCategory.NONE);
            menuItemSetup.Tag = new MenuItemTag(MenuType.Dialog, (int)ModuleID.SystemManage, AuthorityCategory.NONE);
            menuCompanyInfo.Tag = new MenuItemTag(MenuType.DataForm, (int)ModuleID.SystemManage, AuthorityCategory.MASTER_ACTION);
            menuCustomMenuAuth.Tag = new MenuItemTag(MenuType.DataForm, (int)ModuleID.SystemManage, AuthorityCategory.DATA_ACTION_VALUE + ButtonAuthority.EX_01);
            menuLog.Tag = new MenuItemTag(MenuType.Dialog, (int)ModuleID.SystemManage, AuthorityCategory.NONE);
            menu日志查询.Tag = new MenuItemTag(MenuType.Dialog, (int)ModuleID.SystemManage, AuthorityCategory.NONE);
            menuItemBackup.Tag = new MenuItemTag(MenuType.Dialog, (int)ModuleID.SystemManage, AuthorityCategory.NONE);
            menuItem机构信息.Tag = new MenuItemTag(MenuType.DataForm, (int)ModuleID.SystemManage, AuthorityCategory.MASTER_ACTION);
            menuItem地区信息.Tag = new MenuItemTag(MenuType.DataForm, (int)ModuleID.SystemManage, AuthorityCategory.MASTER_ACTION);
            menuItem医生档案.Tag = new MenuItemTag(MenuType.DataForm, (int)ModuleID.SystemManage, AuthorityCategory.MASTER_ACTION);
            menuItemb超报告模板.Tag = new MenuItemTag(MenuType.DataForm, (int)ModuleID.SystemManage, AuthorityCategory.MASTER_ACTION);
        }

        public override void SetSecurity(object securityInfo)
        {
            base.SetSecurity(securityInfo);

            if (securityInfo is ToolStrip)
            {
                btnUser.Enabled = menuItemUserMgr.Enabled;
                btnAuth.Enabled = menuItemAuth.Enabled;
                btnSetup.Enabled = menuItemSetup.Enabled;
                btnLog.Enabled = menuLog.Enabled;
                btnLogQuery.Enabled = menu日志查询.Enabled;
                btnCustomMenuAuth.Enabled = menuCustomMenuAuth.Enabled;
                btn机构信息.Enabled = menuItem机构信息.Enabled;
                if (Loginer.CurrentUser.IsAdmin())
                { btnBackup.Enabled = Loginer.CurrentUser.IsAdmin(); }
            }
        }
        #endregion

        #region menuClick
        //用户设置
        private void menuItemUserMgr_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frmUser), menuItemUserMgr);
        }
        //权限设置
        private void menuItemAuth_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frmGroup), menuItemAuth);
        }
        //公司设置
        private void menuCompanyInfo_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frmCompanyInfo), menuCompanyInfo);
        }
        //菜单设置
        private void menuCustomMenuAuth_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frmMenuAuth), menuCustomMenuAuth);
        }
        //系统设置
        private void menuItemSetup_Click(object sender, EventArgs e)
        {
            new frmSystemOptions().ShowDialog();
        }
        //日志设置
        private void menuLog_Click(object sender, EventArgs e)
        {
            frmLogConfig.Execute("");
        }
        //备份设置
        private void menuItemBackup_Click(object sender, EventArgs e)
        {
            new frmBackupDB().ShowDialog();
        }

        private void menuItem机构信息_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm机构信息), menuItem机构信息);
        }

        private void menuItem地区信息_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm地区信息), menuItem地区信息);
        }

        private void menu日志查询_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frmLogQuery), menu日志查询);
        }

        private void menuItem医生档案_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm医生信息), menuItem医生档案);
        }

        private void menuItemb超报告模板_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frmB超报告模板), menuItemb超报告模板);
        }

        #endregion


    }
}

