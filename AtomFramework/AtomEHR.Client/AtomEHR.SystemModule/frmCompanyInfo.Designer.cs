﻿namespace AtomEHR.SystemModule
{
    partial class frmCompanyInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtEnglishName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtNativeName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtCompanyCode = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.gcDetailEditor = new DevExpress.XtraEditors.GroupControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtFax = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtTel = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtAddress = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtReportHead = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtProgramName = new DevExpress.XtraEditors.TextEdit();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            this.tpDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEnglishName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNativeName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetailEditor)).BeginInit();
            this.gcDetailEditor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportHead.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProgramName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.tpSummary.Size = new System.Drawing.Size(1066, 732);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 33);
            this.pnlSummary.Size = new System.Drawing.Size(1072, 765);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Size = new System.Drawing.Size(1072, 765);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Controls.Add(this.gcDetailEditor);
            this.tpDetail.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tpDetail.Size = new System.Drawing.Size(1066, 732);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            // 
            // txtFocusForSave
            // 
            this.txtFocusForSave.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            // 
            // txtEnglishName
            // 
            this.txtEnglishName.Location = new System.Drawing.Point(143, 161);
            this.txtEnglishName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtEnglishName.Name = "txtEnglishName";
            this.txtEnglishName.Size = new System.Drawing.Size(518, 24);
            this.txtEnglishName.TabIndex = 21;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(42, 165);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(102, 18);
            this.labelControl2.TabIndex = 20;
            this.labelControl2.Text = "单位名称(英)：";
            // 
            // txtNativeName
            // 
            this.txtNativeName.Location = new System.Drawing.Point(143, 126);
            this.txtNativeName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNativeName.Name = "txtNativeName";
            this.txtNativeName.Size = new System.Drawing.Size(518, 24);
            this.txtNativeName.TabIndex = 19;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(42, 130);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(102, 18);
            this.labelControl1.TabIndex = 18;
            this.labelControl1.Text = "单位名称(中)：";
            // 
            // txtCompanyCode
            // 
            this.txtCompanyCode.Location = new System.Drawing.Point(143, 76);
            this.txtCompanyCode.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCompanyCode.Name = "txtCompanyCode";
            this.txtCompanyCode.Properties.MaxLength = 12;
            this.txtCompanyCode.Size = new System.Drawing.Size(149, 24);
            this.txtCompanyCode.TabIndex = 25;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(67, 80);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(75, 18);
            this.labelControl4.TabIndex = 24;
            this.labelControl4.Text = "单位编号：";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.Gray;
            this.labelControl5.Location = new System.Drawing.Point(299, 80);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(53, 18);
            this.labelControl5.TabIndex = 26;
            this.labelControl5.Text = "3个字符";
            // 
            // gcDetailEditor
            // 
            this.gcDetailEditor.Controls.Add(this.labelControl9);
            this.gcDetailEditor.Controls.Add(this.txtFax);
            this.gcDetailEditor.Controls.Add(this.labelControl8);
            this.gcDetailEditor.Controls.Add(this.txtTel);
            this.gcDetailEditor.Controls.Add(this.labelControl7);
            this.gcDetailEditor.Controls.Add(this.txtAddress);
            this.gcDetailEditor.Controls.Add(this.labelControl6);
            this.gcDetailEditor.Controls.Add(this.txtReportHead);
            this.gcDetailEditor.Controls.Add(this.labelControl3);
            this.gcDetailEditor.Controls.Add(this.txtProgramName);
            this.gcDetailEditor.Controls.Add(this.txtEnglishName);
            this.gcDetailEditor.Controls.Add(this.labelControl5);
            this.gcDetailEditor.Controls.Add(this.labelControl1);
            this.gcDetailEditor.Controls.Add(this.txtCompanyCode);
            this.gcDetailEditor.Controls.Add(this.txtNativeName);
            this.gcDetailEditor.Controls.Add(this.labelControl4);
            this.gcDetailEditor.Controls.Add(this.labelControl2);
            this.gcDetailEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcDetailEditor.Location = new System.Drawing.Point(0, 0);
            this.gcDetailEditor.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcDetailEditor.Name = "gcDetailEditor";
            this.gcDetailEditor.Size = new System.Drawing.Size(1066, 732);
            this.gcDetailEditor.TabIndex = 27;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(95, 338);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(45, 18);
            this.labelControl9.TabIndex = 35;
            this.labelControl9.Text = "传真：";
            // 
            // txtFax
            // 
            this.txtFax.Location = new System.Drawing.Point(143, 334);
            this.txtFax.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(229, 24);
            this.txtFax.TabIndex = 36;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(95, 303);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(45, 18);
            this.labelControl8.TabIndex = 33;
            this.labelControl8.Text = "电话：";
            // 
            // txtTel
            // 
            this.txtTel.Location = new System.Drawing.Point(143, 300);
            this.txtTel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(229, 24);
            this.txtTel.TabIndex = 34;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(67, 390);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(75, 18);
            this.labelControl7.TabIndex = 31;
            this.labelControl7.Text = "单位地址：";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(143, 386);
            this.txtAddress.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(518, 24);
            this.txtAddress.TabIndex = 32;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(67, 251);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(75, 18);
            this.labelControl6.TabIndex = 29;
            this.labelControl6.Text = "报表标题：";
            // 
            // txtReportHead
            // 
            this.txtReportHead.Location = new System.Drawing.Point(143, 247);
            this.txtReportHead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtReportHead.Name = "txtReportHead";
            this.txtReportHead.Size = new System.Drawing.Size(518, 24);
            this.txtReportHead.TabIndex = 30;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(42, 199);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(102, 18);
            this.labelControl3.TabIndex = 27;
            this.labelControl3.Text = "程序名称(中)：";
            // 
            // txtProgramName
            // 
            this.txtProgramName.Location = new System.Drawing.Point(143, 195);
            this.txtProgramName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtProgramName.Name = "txtProgramName";
            this.txtProgramName.Size = new System.Drawing.Size(518, 24);
            this.txtProgramName.TabIndex = 28;
            // 
            // frmCompanyInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.ClientSize = new System.Drawing.Size(1072, 798);
            this.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.Name = "frmCompanyInfo";
            this.Text = "单位资料设置";
            this.Load += new System.EventHandler(this.frmCompanyInfo_Load);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            this.tpDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEnglishName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNativeName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetailEditor)).EndInit();
            this.gcDetailEditor.ResumeLayout(false);
            this.gcDetailEditor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportHead.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProgramName.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txtEnglishName;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtNativeName;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtCompanyCode;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.GroupControl gcDetailEditor;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtProgramName;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtReportHead;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txtFax;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtTel;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtAddress;
    }
}
