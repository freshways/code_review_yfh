﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AtomEHR.Common;
using AtomEHR.Server.DataAccess;

namespace AtomEHR.Business
{
    /*==========================================
 *   程序说明: 查询慢性病人群数据
 *   作者姓名: yufh
 *   创建日期: 2017-03-16 16:45:54
 *   最后修改: 
 *==========================================*/

    public class bll慢性病统计分析
    {
        private dal慢性病统计分析 _MyBridge;
          public bll慢性病统计分析()
        {
            _MyBridge = new dal慢性病统计分析(Loginer.CurrentUser);
        }

          public DataTable GetMain(string age)
          {
              return _MyBridge.GetMain(age);
          }

          public DataTable Get高血压人群分析(string types,string age)
          {
              return _MyBridge.Get高血压人群分析(types, age);
          }

          public DataTable Get糖尿病人群分析(string types, string age)
          {
              return _MyBridge.Get糖尿病人群分析(types, age);
          }

          public DataTable Get冠心病人群分析(string types, string age)
          {
              return _MyBridge.Get冠心病人群分析(types, age);
          }

          public DataTable Get脑卒中人群分析(string types, string age)
          {
              return _MyBridge.Get脑卒中人群分析(types, age);
          }

          public DataTable GetDataTable(String SQL)
          {
              return _MyBridge.GetDataTable(SQL);
          }
    }
}
