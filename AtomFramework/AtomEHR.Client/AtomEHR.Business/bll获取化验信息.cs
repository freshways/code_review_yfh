﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AtomEHR.Common;
using AtomEHR.Server.DataAccess;
using AtomEHR.Server.DataAccess.DAL_System;

namespace AtomEHR.Business
{
    public class bll获取化验信息
    {
        private static string ServerIP = "";
        private static string DataBase = "";
        private static string UserID = "";
        private static string PassWord = "";

        private static string sConn = "Server={0};Database={1};User ID={2};Password={3};Connection TimeOut=180;";


        //Begin WXF 2018-11-06 | 14:26
        //@@UpdateValue 
        public struct Bean_所属医院
        {
            public string str_机构编号;
            public string str_机构名称;
        }
        //End


        public static DataTable Get化验信息(string s身份证, string s化验日期)
        {
            if (ServerIP == "")
            { GetInterfaceconn(); sConn = string.Format(sConn, ServerIP, DataBase, UserID, PassWord).ToString(); }
            //暂时以姚店子化验数据，后期做成配置项，在加载的时候判断是哪个医院
            //sConn = "Server=192.168.10.57;Database=LIS2719;User ID=yggsuser;Password=yggsuser;Connection TimeOut=180;";
            SqlConnection conn = new SqlConnection(sConn);
            DataTable dt = Interface_获取化验信息.Get化验室检查信息(conn, s身份证, s化验日期);
            return dt;
        }

        public static DataTable Get化验信息列表(string s身份证)
        {
            if (ServerIP == "")
            { GetInterfaceconn(); sConn = string.Format(sConn, ServerIP, DataBase, UserID, PassWord).ToString(); }
            SqlConnection conn = new SqlConnection(sConn);
            DataTable dt = Interface_获取化验信息.Get化验室检查信息列表(conn, s身份证);
            return dt;
        }

        //add by wjz 20170409 调整读取化验结果的功能 ▽
        public static DataTable Get化验信息列表(string s身份证, string s档案号)
        {
            if (ServerIP == "")
            { GetInterfaceconn(); sConn = string.Format(sConn, ServerIP, DataBase, UserID, PassWord).ToString(); }
            SqlConnection conn = new SqlConnection(sConn);
            DataTable dt = Interface_获取化验信息.Get化验室检查信息列表(conn, s身份证, s档案号);
            return dt;
        }
        //add by wjz 20170409 调整读取化验结果的功能 △

        /// <summary>
        /// 2017-06-01 21:49:48 yufh 添加
        /// </summary>
        /// <param name="项目"></param>
        /// <param name="条码"></param>
        /// <param name="体检日期"></param>
        /// <returns></returns>
        public static DataTable getLIs数据(string 项目, string 条码, string 体检日期)
        {
            if (ServerIP == "")
            { GetInterfaceconn(); sConn = string.Format(sConn, ServerIP, DataBase, UserID, PassWord).ToString(); }
            SqlConnection conn = new SqlConnection(sConn);

            DataTable dt = Interface_获取化验信息.GetLIs数据(conn, 项目, 条码, 体检日期);
            return dt;
        }

        private static void GetInterfaceconn()
        {
            try
            {
                DataTable dt = new dalCommon(Loginer.CurrentUser).GetInterfaceConn(Loginer.CurrentUser.所属机构);
                if (dt != null && dt.Rows.Count > 0)
                {
                    ServerIP = dt.Rows[0]["ServerIP"].ToString();
                    DataBase = dt.Rows[0]["DBName"].ToString();
                    UserID = dt.Rows[0]["DBUserName"].ToString();
                    PassWord = dt.Rows[0]["DBUserPassword"].ToString();
                }
                else { throw new Exception("没有配置化验室接口连接！无法获取化验结果！"); }
            }
            catch (Exception ex)
            {
                throw new Exception("没有配置化验室接口连接！无法获取化验结果！" + ex.Message);
                //sConn = "Server=192.168.10.57;Database=LIS2719;User ID=yggsuser;Password=yggsuser;Connection TimeOut=180;";
            }
        }

        //Begin WXF 2018-11-06 | 14:21
        //获取所属医院信息
        public static Bean_所属医院 Get_所属医院()
        {
            Bean_所属医院 be;
            try
            {
                DataTable dt = new dalCommon(Loginer.CurrentUser).GetInterfaceConn(Loginer.CurrentUser.所属机构);
                if (dt != null && dt.Rows.Count > 0)
                {
                    be.str_机构编号 = dt.Rows[0]["RGID"].ToString();
                    be.str_机构名称 = dt.Rows[0]["DataSetName"].ToString();
                }
                else
                {
                    throw new Exception("没有配置化验室接口连接！无法获取化验结果！");
                }
            }
            catch (Exception ex)
            {

                throw new Exception("没有配置化验室接口连接！无法获取化验结果！" + ex.Message);
            }

            return be;
        }
        //End


        //Begin WXF 2018-11-06 | 15:08
        //获取血常规
        public static DataTable Get_血常规(string str_idNum, string str_examineDate)
        {
            if (ServerIP == "")
            { GetInterfaceconn(); sConn = string.Format(sConn, ServerIP, DataBase, UserID, PassWord).ToString(); }
            SqlConnection conn = new SqlConnection(sConn);

            DataTable dt = Interface_获取化验信息.Get_血常规(conn, str_idNum, str_examineDate);

            return dt;
        }

        public static DataTable Get_血生化(string str_idNum, string str_examineDate)
        {
            if (ServerIP == "")
            { GetInterfaceconn(); sConn = string.Format(sConn, ServerIP, DataBase, UserID, PassWord).ToString(); }
            SqlConnection conn = new SqlConnection(sConn);

            DataTable dt = Interface_获取化验信息.Get_血生化(conn, str_idNum, str_examineDate);

            return dt;
        }

        //End

    }
}
