﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AtomEHR.Common;
using AtomEHR.Server.DataAccess;

namespace AtomEHR.Business
{
    /*==========================================
 *   程序说明: 常用统计分析扩展类，以后添加的查询统计都放在这个类下面
 *   作者姓名: yufh
 *   创建日期: 2018-09-27 10:25:54
 *   最后修改: 
 *==========================================*/

    public class bll常用统计分析
    {
        private dal常用统计分析 _MyBridge;
        public bll常用统计分析()
        {
            _MyBridge = new dal常用统计分析(Loginer.CurrentUser);
        }

        public DataSet Get重复统计(string rgid, string begindate, string enddate,string ctype)
        {
            return _MyBridge.Get重复统计(rgid, begindate, enddate, ctype);
        }

        public DataSet Get疾病人群统计分析(string rgid, string begindate, string enddate,string _完整度,string _Type)
        {
            return _MyBridge.Get疾病人群统计分析(rgid, begindate, enddate);
        }

        public DataTable GetDataTable(String SQL)
        {
            return _MyBridge.GetDataTable(SQL);
        }
    }
}
