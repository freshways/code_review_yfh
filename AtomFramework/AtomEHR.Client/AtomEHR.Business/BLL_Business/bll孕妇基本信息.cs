﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using AtomEHR.Models;
using AtomEHR.Bridge;
using AtomEHR.Common;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Server.DataAccess;

/*==========================================
 *   程序说明: 孕妇基本信息的业务逻辑层
 *   作者姓名: ATOM
 *   创建日期: 2015/09/30 04:57:52
 *   最后修改: 2015/09/30 04:57:52
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Business
{
    /// <summary>
    /// bll孕妇基本信息
    /// </summary>
    public class bll孕妇基本信息 : bllBaseBusiness
    {
        /// <summary>
        /// 构造器
        /// </summary>
        public bll孕妇基本信息()
        {
            _KeyFieldName = tb_孕妇基本信息.__KeyName; //主键字段
            _SummaryTableName = tb_孕妇基本信息.__TableName;//表名
        }

        /// <summary>
        ///根据单据号码取业务数据
        /// </summary>
        public override DataSet GetBusinessByKey(string keyValue, bool resetCurrent)
        {
            DataSet ds = new dal孕妇基本信息(Loginer.CurrentUser).GetBusinessByKey(keyValue);
            this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }

        /// <summary>
        ///删除单据
        /// </summary>
        public override bool Delete(string keyValue)
        {
            return new dal孕妇基本信息(Loginer.CurrentUser).Delete(keyValue);
        }

        /// <summary>
        ///检查单号是否存在
        /// </summary>
        public bool CheckNoExists(string keyValue)
        {
            return new dal孕妇基本信息(Loginer.CurrentUser).CheckNoExists(keyValue);
        }

        /// <summary>
        ///保存数据
        /// </summary>
        public override SaveResult Save(DataSet saveData)
        {
            return new dal孕妇基本信息(Loginer.CurrentUser).Update(saveData); //交给数据层处理
        }

        /// <summary>
        ///审核单据
        /// </summary>
        public override void ApprovalBusiness(DataRow summaryRow)
        {
            summaryRow[BusinessCommonFields.AppDate] = DateTime.Now;
            summaryRow[BusinessCommonFields.AppUser] = Loginer.CurrentUser.Account;
            summaryRow[BusinessCommonFields.FlagApp] = "Y";
            string key = ConvertEx.ToString(summaryRow[tb_孕妇基本信息.__KeyName]);
            new dal孕妇基本信息(Loginer.CurrentUser).ApprovalBusiness(key, "Y", Loginer.CurrentUser.Account, DateTime.Now);
        }

        /// <summary>
        ///新增一张业务单据
        /// </summary>
        public override void NewBusiness()
        {
            DataTable summaryTable = _CurrentBusiness.Tables[tb_孕妇基本信息.__TableName];
            DataRow row = summaryTable.Rows.Add();
            //根据需要自行取消注释
            //row[tb_孕妇基本信息.__KeyName] = "*自动生成*";
            //row[tb_孕妇基本信息.创建人] = Loginer.CurrentUser.Account;
            //row[tb_孕妇基本信息.创建时间] = DateTime.Now;
            //row[tb_孕妇基本信息.修改人] = Loginer.CurrentUser.Account;
            //row[tb_孕妇基本信息.修改时间] = DateTime.Now;
        }

        /// <summary>
        ///创建用于保存的临时数据
        /// </summary>
        public override DataSet CreateSaveData(DataSet currentBusiness, UpdateType currentType)
        {
            this.UpdateSummaryRowState(this.DataBindRow, currentType);

            //创建用于保存的临时数据,里面包含主表数据
            DataSet save = this.DoCreateTempData(currentBusiness, tb_孕妇基本信息.__TableName);
            DataTable summary = save.Tables[0];
            //根据需要自行取消注释
            //summary.Rows[0][BusinessCommonFields.s修改人] = Loginer.CurrentUser.Account;
            //summary.Rows[0][BusinessCommonFields.s修改时间] = DateTime.Now;

            this.UpdateSummaryRowState(currentBusiness.Tables[tb_健康档案.__TableName].Rows[0], currentType); //个人档案
            DataTable detail = currentBusiness.Tables[tb_健康档案.__TableName].Copy();
            this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
            save.Tables.Add(detail); //加入个人档案数据 

            return save;
        }

        /// <summary>
        ///查询数据
        /// </summary>
        public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
        {
            DataTable dt = new dal孕妇基本信息(Loginer.CurrentUser).GetSummaryByParam(docNoFrom, docNoTo, docDateFrom, docDateTo);
            //this.SetNumericDefaultValue(ds); //设置预设值
            //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return dt;
        }

        /// <summary>
        ///获取报表数据
        /// </summary>
        public DataSet GetReportDataBYLRSJ(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string type)
        {
            DataSet ds = new dal孕妇基本信息(Loginer.CurrentUser).GetReportDataBYLRSJ(DocNo机构, DocNo完整度, DateFrom, DateTo, type);
            this.SetNumericDefaultValue(ds); //设置预设值
            return ds;
        }
        public DataSet GetReportDataBYDCSJ(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string type)
        {
            DataSet ds = new dal孕妇基本信息(Loginer.CurrentUser).GetReportDataBYDCSJ(DocNo机构, DocNo完整度, DateFrom, DateTo, type);
            this.SetNumericDefaultValue(ds); //设置预设值
            return ds;
        }
        #region Business Log

        /// <summary>
        /// 写入日志
        /// </summary>
        public override void WriteLog()
        {
            string key = this.DataBinder.Rows[0][tb_孕妇基本信息.__KeyName].ToString();//取单据号码
            DataSet dsOriginal = this.GetBusinessByKey(key, false); //取保存前的原始数据, 用于保存日志时匹配数据.
            DataSet dsTemplate = this.CreateSaveData(this.CurrentBusiness, UpdateType.Modify); //创建用于保存的临时数据
            this.WriteLog(dsOriginal, dsTemplate);//保存日志      
        }

        /// <summary>
        /// 写入日志
        /// </summary>
        /// <param name="original">原始数据</param>
        /// <param name="changes">修改后的数据</param>
        public override void WriteLog(DataTable original, DataTable changes) { }

        /// <summary>
        /// 写入日志
        /// </summary>
        /// <param name="original">原始数据</param>
        /// <param name="changes">修改后的数据</param>
        public override void WriteLog(DataSet original, DataSet changes)
        {  //单独处理,即使错误,不向外抛出异常
            //if (_SaveVersionLog == false) return; //应用策略
            try
            {
                string logID = Guid.NewGuid().ToString().Replace("-", ""); //本次日志ID
                //SystemLog.WriteLog(logID, original.Tables[0], changes.Tables[0], tb_孕妇基本信息.__TableName, tb_孕妇基本信息.__KeyName, true); //主表
                //明细表的修改日志,系统不支持自动生成,请手工调整代码
                //SystemLog.WriteLog(logID, original.Tables[1], changes.Tables[1], tb_孕妇基本信息.__TableName, tb_孕妇基本信息.__KeyName, false);
            }
            catch
            {
                Msg.Warning("写入日志发生错误！");
            }
        }

        #endregion

        #region 新增的方法 by wjz 20151022
        public DataSet GetBusinessByGrdabh(string keyValue)
        {
            DataSet ds = new dal孕妇基本信息(Loginer.CurrentUser).GetBusinessByGrdabh(keyValue);
            return ds;
        }

        public void UpdateInfoByGrdabh(string grdabh, string sfflag, string xcsfsj)
        {
            new dal孕妇基本信息(Loginer.CurrentUser).UpdateInfoByGrdabh(grdabh, sfflag, xcsfsj);
        }
        #endregion


        /// <summary>
        /// 统计查询孕产妇详细信息
        /// </summary>
        /// <param name="prgid">所属机构</param>
        /// <param name="sDate">开始时间</param>
        /// <param name="eDate">结束时间</param>
        /// <param name="p4"></param>
        /// <param name="p5"></param>
        /// <returns></returns>
        public DataSet GetYunChanFuInfo(string prgid, string sDate, string eDate, string p4, string p5)
        {
            DataSet ds = new dal孕妇基本信息(Loginer.CurrentUser).GetYunChanFuInfo(prgid, sDate, eDate, p4, p5);
            return ds;
        }

        public DataSet GetYunChanFuInfoNew(string prgid, string fmBDate1, string fmEDate2, string chsfDate1, string chsfDate2, string name, string sfzh, string dah, string yzbegin, string yzend)
        {
            DataSet ds = new dal孕妇基本信息(Loginer.CurrentUser).GetYunChanFuInfoNew(prgid, fmBDate1, fmEDate2, chsfDate1, chsfDate2, name, sfzh, dah, yzbegin, yzend);
            return ds;
        }

        public DataSet GetYunFuInfo(string prgid, string sDate, string eDate, string SearchBy, string selectType)
        {
            DataSet ds = new dal孕妇基本信息(Loginer.CurrentUser).GetYunFuInfo(prgid, sDate, eDate, SearchBy, selectType);
            return ds;
        }

    }
}
