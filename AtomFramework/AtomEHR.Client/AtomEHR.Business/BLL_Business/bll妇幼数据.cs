﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using AtomEHR.Models;
using AtomEHR.Bridge;
using AtomEHR.Common;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Server.DataAccess;

/*==========================================
 *   程序说明: 妇幼数据的业务逻辑层
 *   作者姓名: ATOM
 *   创建日期: 2015/09/21 11:45:01
 *   最后修改: 2015/09/21 11:45:01
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Business
{
    /// <summary>
    /// bll妇幼数据
    /// </summary>
    public class bll妇幼数据 : bllBaseBusiness
    {
         /// <summary>
         /// 构造器
         /// </summary>
         public bll妇幼数据()
         {
             _KeyFieldName = tb_妇幼数据.__KeyName; //主键字段
             _SummaryTableName = tb_妇幼数据.__TableName;//表名
         }

          /// <summary>
          ///根据单据号码取业务数据
          /// </summary>
          public override DataSet GetBusinessByKey(string keyValue, bool resetCurrent)
          {
              DataSet ds = new dal妇幼数据(Loginer.CurrentUser).GetBusinessByKey(keyValue); 
              this.SetNumericDefaultValue(ds); //设置预设值
              if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return ds;
           }

          /// <summary>
          ///删除单据
          /// </summary>
          public override bool Delete(string keyValue)
          {
              return new dal妇幼数据(Loginer.CurrentUser).Delete(keyValue);
          }

          /// <summary>
          /// 删除单据
          /// </summary>
          /// <param name="biaodannum">表单号</param>
          /// <param name="csrq">出生日期</param>
          /// <param name="lrr">录入人</param>
          /// <param name="lrsj">录入时间</param>
          /// <param name="name">姓名</param>
          /// <param name="sfzh">身份证号码</param>
          /// <param name="sjly">暂时不知含义</param>
          /// <param name="jcrgid">机构代码</param>
          /// <returns></returns>
          public bool Delete(string biaodannum, string csrq, string lrr, string lrsj,
                    string name, string sfzh, string sjly, string jcrgid)
          {
              return new dal妇幼数据(Loginer.CurrentUser).Delete(biaodannum, csrq, lrr, lrsj, name, sfzh, sjly, jcrgid);
          }

          /// <summary>
          ///检查单号是否存在
          /// </summary>
          public bool CheckNoExists(string keyValue)
          {
              return new dal妇幼数据(Loginer.CurrentUser).CheckNoExists(keyValue);
          }

          /// <summary>
          ///保存数据
          /// </summary>
          public override SaveResult Save(DataSet saveData)
          {
              return new dal妇幼数据(Loginer.CurrentUser).Update(saveData); //交给数据层处理
          }

          /// <summary>
          ///审核单据
          /// </summary>
          public override void ApprovalBusiness(DataRow summaryRow)
          {
               summaryRow[BusinessCommonFields.AppDate] = DateTime.Now;
               summaryRow[BusinessCommonFields.AppUser] = Loginer.CurrentUser.Account;
               summaryRow[BusinessCommonFields.FlagApp] = "Y";
               string key = ConvertEx.ToString(summaryRow[tb_妇幼数据.__KeyName]);
               new dal妇幼数据(Loginer.CurrentUser).ApprovalBusiness(key, "Y", Loginer.CurrentUser.Account, DateTime.Now);
          }

          /// <summary>
          ///新增一张业务单据
          /// </summary>
          public override void NewBusiness()
          {
              DataTable summaryTable = _CurrentBusiness.Tables[tb_妇幼数据.__TableName];
              DataRow row = summaryTable.Rows.Add();     
              //根据需要自行取消注释
              //row[tb_妇幼数据.__KeyName] = "*自动生成*";
              //row[tb_妇幼数据.创建人] = Loginer.CurrentUser.Account;
              //row[tb_妇幼数据.创建时间] = DateTime.Now;
              //row[tb_妇幼数据.修改人] = Loginer.CurrentUser.Account;
              //row[tb_妇幼数据.修改时间] = DateTime.Now;
           }

          /// <summary>
          ///创建用于保存的临时数据
          /// </summary>
          public override DataSet CreateSaveData(DataSet currentBusiness, UpdateType currentType)
          {
              this.UpdateSummaryRowState(this.DataBindRow, currentType);

              //创建用于保存的临时数据,里面包含主表数据
              DataSet save = this.DoCreateTempData(currentBusiness, tb_妇幼数据.__TableName);
              DataTable summary = save.Tables[0];
              //根据需要自行取消注释
              //summary.Rows[0][BusinessCommonFields.s修改人] = Loginer.CurrentUser.Account;
              //summary.Rows[0][BusinessCommonFields.s修改时间] = DateTime.Now;

              //this.UpdateSummaryRowState(currentBusiness.Tables[tb_妇幼数据s.__TableName].Rows[0], currentType); //子表
              //DataTable detail = currentBusiness.Tables[tb_妇幼数据s.__TableName].Copy();
              //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
              //save.Tables.Add(detail); //加入明细数据 

              return save; 
          }

          /// <summary>
          ///查询数据
          /// </summary>
          public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
          {
              DataTable dt = new dal妇幼数据(Loginer.CurrentUser).GetSummaryByParam(docNoFrom, docNoTo, docDateFrom, docDateTo);
              //this.SetNumericDefaultValue(ds); //设置预设值
              //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return dt;
          }

          /// <summary>
          ///获取报表数据
          /// </summary>
          public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
          {
              return null;
          }


        /// <summary>
          /// 需要先插入儿童基本信息，然后在调用此方法,注意使用默认值的几个参数
          /// </summary>
          /// <param name="卡号"></param>
          /// <param name="姓名"></param>
          /// <param name="性别"></param>
          /// <param name="出生日期"></param>
          /// <param name="身份证号"></param>
          /// <param name="身份">使用默认值“儿童”</param>
          /// <param name="yuncc"></param>
          /// <param name="YUNCC"></param>
          /// <param name="表单号">表单号使用默认值“e0”</param>
          /// <param name="母亲姓名"></param>
          /// <param name="母亲身份证号"></param>
          /// <param name="JCRGID"></param>
          /// <param name="JCRGNAME"></param>
          /// <param name="FCYYRGID"></param>
          /// <param name="录入人"></param>
          /// <param name="录入时间"></param>
          /// <param name="SJTQFSQK">使用默认值“0”</param>
          /// <param name="SJLY">使用默认值“zhonglian”</param>
          public bool InsertData(string 卡号, string 姓名, string 性别, string 出生日期, string 身份证号, string 母亲姓名,
                                 string 母亲身份证号, string JCRGID, string JCRGNAME, string FCYYRGID, string 录入人, string 录入时间
              // ,string 身份,string YUNCC, string 表单号,string SJTQFSQK, string SJLY
                                )
          {
              return new dal妇幼数据(Loginer.CurrentUser).InsertData(卡号, 姓名, 性别, 出生日期, 身份证号,
                                 母亲姓名, 母亲身份证号, JCRGID, JCRGNAME, FCYYRGID, 录入人, 录入时间
                                 //,身份, YUNCC, 表单号, SJTQFSQK, SJLY
                                 );
          }

          #region Business Log

          /// <summary>
          /// 写入日志
          /// </summary>
          public override void WriteLog()
          {
              string key = this.DataBinder.Rows[0][tb_妇幼数据.__KeyName].ToString();//取单据号码
              DataSet dsOriginal = this.GetBusinessByKey(key, false); //取保存前的原始数据, 用于保存日志时匹配数据.
              DataSet dsTemplate = this.CreateSaveData(this.CurrentBusiness, UpdateType.Modify); //创建用于保存的临时数据
              this.WriteLog(dsOriginal, dsTemplate);//保存日志      
          }

          /// <summary>
          /// 写入日志
          /// </summary>
          /// <param name="original">原始数据</param>
          /// <param name="changes">修改后的数据</param>
          public override void WriteLog(DataTable original, DataTable changes) { }

          /// <summary>
          /// 写入日志
          /// </summary>
          /// <param name="original">原始数据</param>
          /// <param name="changes">修改后的数据</param>
          public override void WriteLog(DataSet original, DataSet changes)
          {  //单独处理,即使错误,不向外抛出异常
              //if (_SaveVersionLog == false) return; //应用策略
              try
              {
                  string logID = Guid.NewGuid().ToString().Replace("-", ""); //本次日志ID
                  //SystemLog.WriteLog(logID, original.Tables[0], changes.Tables[0], tb_妇幼数据.__TableName, tb_妇幼数据.__KeyName, true); //主表
                  //明细表的修改日志,系统不支持自动生成,请手工调整代码
                  //SystemLog.WriteLog(logID, original.Tables[1], changes.Tables[1], tb_妇幼数据.__TableName, tb_妇幼数据.__KeyName, false);
              }
              catch
              {
                  Msg.Warning("写入日志发生错误！");
              }
          }

          #endregion
     }
}
