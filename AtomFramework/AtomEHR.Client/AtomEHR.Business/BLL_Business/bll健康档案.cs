﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using AtomEHR.Models;
using AtomEHR.Bridge;
using AtomEHR.Common;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Server.DataAccess;
using AtomEHR.Interfaces.Interfaces_Bridge;
using AtomEHR.Business.BLL_Business;
using AtomEHR.Bridge.CommonBridge;

/*==========================================
 *   程序说明: 健康档案的业务逻辑层
 *   作者姓名: ATOM
 *   创建日期: 2015-07-30 04:20:42
 *   最后修改: 2015-07-30 04:20:42
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Business
{
    /// <summary>
    /// bll健康档案
    /// </summary>
    public class bll健康档案 : bllBaseBusiness
    {
        private IBridge_CommonData _MyBridge; //桥接/策略接口
        /// <summary>
        /// 构造器
        /// </summary>
        public bll健康档案()
        {
            _KeyFieldName = tb_健康档案.__KeyName; //主键字段
            _SummaryTableName = tb_健康档案.__TableName;//表名
            _MyBridge = this.CreateBridge();
        }

        /// <summary>
        /// 创建桥接通信通道
        /// </summary>
        /// <returns></returns>
        private IBridge_CommonData CreateBridge()
        {
            return new WebService_CommonData();
        }

        /// <summary>
        ///根据单据号码取个人健康档案信息业务数据
        /// </summary>
        public override DataSet GetBusinessByKey(string keyValue, bool resetCurrent)
        {
            DataSet ds = new dal健康档案(Loginer.CurrentUser).GetBusinessByKey(keyValue);
            this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }
        public DataSet GetBusinessByFamilNo(string familNo, bool resetCurrent)
        {
            DataSet ds = new dal健康档案(Loginer.CurrentUser).GetBusinessByFamilyNO(familNo);
            this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }
        /// <summary>
        /// 获取一张业务单据的数据
        /// 获取原始的个人档案信息
        /// </summary>
        /// <param name="docNo">单据号码</param>
        /// <returns></returns>
        public System.Data.DataSet GetRowInfoByKey(string grdah)
        {
            DataSet ds = new dal健康档案(Loginer.CurrentUser).GetRowInfoByKey(grdah);
            //this.SetNumericDefaultValue(ds); //设置预设值
            //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }
        public override bool Delete(string keyValue)
        {
            return new dal健康档案(Loginer.CurrentUser).Delete(keyValue);
        }
        /// <summary>
        /// 根据单据号码取业务数据
        /// </summary>
        /// <param name="familNo">个人档案编号</param>
        /// <param name="familyNo">家庭档案编号</param>
        /// <param name="resetCurrent"></param>
        /// <returns></returns>
        public DataSet GetAllUserByKey(string familNo, string docNo, bool resetCurrent)
        {
            DataSet ds = new dal健康档案(Loginer.CurrentUser).GetAllUserByKey(familNo, docNo);
            //this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }
        /// <summary>
        ///  UC个人基本信息 显示 页面用，不需要生成  家庭档案编号
        /// </summary>
        /// <param name="docNo"></param>
        /// <returns></returns>
        public System.Data.DataSet GetOneUserByKeyForShow(string docNo, bool resetCurrent)
        {
            DataSet ds = new dal健康档案(Loginer.CurrentUser).GetOneUserByKeyForShow(docNo);
            //this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }
        /// <summary>
        ///  UC个人基本信息  修改  页面用，不需要生成  家庭档案编号
        /// </summary>
        /// <param name="docNo"></param>
        /// <returns></returns>
        public System.Data.DataSet GetOneUserCodeByKey(string docNo, bool resetCurrent)
        {
            DataSet ds = new dal健康档案(Loginer.CurrentUser).GetOneUserCodeByKey(docNo);
            this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }
        /// <summary>
        ///  UC个人基本信息页面用，不需要生成  家庭档案编号
        /// </summary>
        /// <param name="docNo"></param>
        /// <returns></returns>
        public System.Data.DataSet GetNewUserByKey(string docNo, bool resetCurrent)
        {
            DataSet ds = new dal健康档案(Loginer.CurrentUser).GetNewUserByKey(docNo);
            //this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }
        /// <summary>
        /// 根据机构、街道和居委会查询总人数
        /// </summary>
        /// <param name="rgid"></param>
        /// <param name="jd"></param>
        /// <param name="jwh"></param>
        /// <returns></returns>
        public int SelectCountForJg(string rgid, string jd, string jwh,string creUser)
        {
            return new dal健康档案(Loginer.CurrentUser).SelectCountForJg(rgid, jd, jwh, creUser);
        }
        /// <summary>
        ///删除单据
        /// </summary>
        public SaveResult DeleteByDocNO(string keyValue)
        {
            return new dal健康档案(Loginer.CurrentUser).DeleteByDocNo(keyValue);
        }

        /// <summary>
        ///检查单号是否存在
        /// </summary>
        public bool CheckNoExists(string keyValue)
        {
            return new dal健康档案(Loginer.CurrentUser).CheckNoExists(keyValue);
        }

        /// <summary>
        ///保存数据
        /// </summary>
        public override SaveResult Save(DataSet saveData)
        {
            return new dal健康档案(Loginer.CurrentUser).Update(saveData); //交给数据层处理
        }
        public SaveResult SaveOneData(DataSet saveData)
        {
            return new dal健康档案(Loginer.CurrentUser).UpdateOneData(saveData); //交给数据层处理
        }
        /// <summary>
        ///审核单据
        /// </summary>
        public override void ApprovalBusiness(DataRow summaryRow)
        {
            summaryRow[BusinessCommonFields.AppDate] = DateTime.Now;
            summaryRow[BusinessCommonFields.AppUser] = Loginer.CurrentUser.Account;
            summaryRow[BusinessCommonFields.FlagApp] = "Y";
            string key = ConvertEx.ToString(summaryRow[tb_健康档案.__KeyName]);
            new dal健康档案(Loginer.CurrentUser).ApprovalBusiness(key, "Y", Loginer.CurrentUser.Account, DateTime.Now);
        }

        /// <summary>
        ///新增户主档案
        /// </summary>
        public override void NewBusiness()
        {
            //tb_家庭档案
            DataTable summaryTable = _CurrentBusiness.Tables[tb_家庭档案.__TableName];
            DataRow row = summaryTable.Rows.Add();
            //根据需要自行取消注释
            row[tb_家庭档案.__KeyName] = "";

            //tb_健康档案
            DataTable detailTable = _CurrentBusiness.Tables[tb_健康档案.__TableName];
            DataRow detailrow = detailTable.Rows.Add();
            //根据需要自行取消注释
            detailrow[tb_健康档案.__KeyName] = "";

            //tb_健康档案_个人健康特征
            DataTable detailTable2 = _CurrentBusiness.Tables[tb_健康档案_个人健康特征.__TableName];
            DataRow detailrow2 = detailTable2.Rows.Add();
            //根据需要自行取消注释
            detailrow2[tb_健康档案_个人健康特征.__KeyName] = "*自动生成*";

            //tb_健康档案_健康状态
            DataTable detailTable3 = _CurrentBusiness.Tables[tb_健康档案_健康状态.__TableName];
            DataRow detailrow3 = detailTable3.Rows.Add();

        }
        /// <summary>
        ///新增家庭成员档案
        /// </summary>
        public void NewUser(String s家庭档案编号)
        {
            DataTable summaryTable = _CurrentBusiness.Tables[tb_健康档案.__TableName];
            DataRow row = summaryTable.Rows.Add();
            //根据需要自行取消注释
            row[tb_健康档案.__KeyName] = "*自动生成*";
            row[tb_健康档案.家庭档案编号] = s家庭档案编号;
            //row[tb_健康档案.所属机构] = Loginer.CurrentUser.所属机构;
            //row[tb_健康档案.创建机构] = Loginer.CurrentUser.所属机构;
            //row[tb_健康档案.创建人] = Loginer.CurrentUser.Account;
            //row[tb_健康档案.创建时间] = DateTime.Now;
            //row[tb_健康档案.修改人] = Loginer.CurrentUser.Account;
            //row[tb_健康档案.修改时间] = DateTime.Now;

            DataTable detailTable = _CurrentBusiness.Tables[tb_健康档案_个人健康特征.__TableName];
            DataRow detailrow = detailTable.Rows.Add();
            //根据需要自行取消注释
            detailrow[tb_健康档案_个人健康特征.__KeyName] = "*自动更新*";
            detailrow[tb_健康档案_个人健康特征.家庭档案编号] = s家庭档案编号;

            DataTable detailTable2 = _CurrentBusiness.Tables[tb_健康档案_健康状态.__TableName];
            DataRow detailrow2 = detailTable2.Rows.Add();
            //DataTable detailTable3 = _CurrentBusiness.Tables[tb_儿童基本信息.__TableName];
            //DataRow detailrow3 = detailTable3.Rows.Add();
            //DataTable detailTable2 = _CurrentBusiness.Tables[tb_健康档案_健康状态.__TableName];
            //DataRow detailrow2 = detailTable2.Rows.Add();
            //DataTable detailTable2 = _CurrentBusiness.Tables[tb_健康档案_健康状态.__TableName];
            //DataRow detailrow2 = detailTable2.Rows.Add();
            //DataTable detailTable2 = _CurrentBusiness.Tables[tb_健康档案_健康状态.__TableName];
            //DataRow detailrow2 = detailTable2.Rows.Add();
            //detailrow2[tb_健康档案_健康状态.创建人] = Loginer.CurrentUser.Account;
            //detailrow2[tb_健康档案_健康状态.创建时间] = DateTime.Now;
            //detailrow2[tb_健康档案_健康状态.修改人] = Loginer.CurrentUser.Account;
            //根据需要自行取消注释
            //detailrow2[tb_健康档案_健康状态.__KeyName] = "*自动更新*";
            //detailrow2[tb_健康档案_健康状态.家庭档案编号] = s家庭档案编号;

        }
        #region datatable去重
        /// datatable去重  
        /// </summary>  
        /// <param name="dtSource">需要去重的datatable</param>  
        /// <param name="columnNames">依据哪些列去重</param>  
        /// <returns></returns>  
        public static DataTable GetDistinctTable(DataTable dtSource, params string[] columnNames)
        {
            DataTable distinctTable = dtSource.Clone();
            try
            {
                if (dtSource != null && dtSource.Rows.Count > 0)
                {
                    DataView dv = new DataView(dtSource);
                    distinctTable = dv.ToTable(true, columnNames);
                }
            }
            catch
            {
                return dtSource;
            }
            return distinctTable;
        }

        /// <summary>  
        /// datatable去重  
        /// </summary>  
        /// <param name="dtSource">需要去重的datatable</param>  
        /// <returns></returns>  
        public static DataTable GetDistinctTable(DataTable dtSource)
        {
            DataTable distinctTable = null;
            try
            {
                if (dtSource != null && dtSource.Rows.Count > 0)
                {
                    string[] columnNames = GetTableColumnName(dtSource);
                    DataView dv = new DataView(dtSource);
                    distinctTable = dv.ToTable(true, columnNames);
                }
            }
            catch
            {
                return dtSource;
            }
            return distinctTable;
        }

        #endregion

        #region 获取表中所有列名
        public static string[] GetTableColumnName(DataTable dt)
        {
            string cols = string.Empty;
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                cols += (dt.Columns[i].ColumnName + ",");
            }
            cols = cols.TrimEnd(',');
            return cols.Split(',');
        }
        #endregion  
        /// <summary>
        ///创建用于保存的临时数据
        /// </summary>
        public override DataSet CreateSaveData(DataSet currentBusiness, UpdateType currentType)
        {

            this.UpdateSummaryRowState(this.DataBindRow, currentType);

            //创建用于保存的临时数据,里面包含主表数据
            DataSet save = this.DoCreateTempData(currentBusiness, tb_家庭档案.__TableName);
            DataTable summary = save.Tables[0];
            //根据需要自行取消注释
            //summary.Rows[0][BusinessCommonFields.s修改人] = Loginer.CurrentUser.Account;
            //summary.Rows[0][BusinessCommonFields.s修改时间] = DateTime.Now;

            if (currentType == UpdateType.Add || currentType == UpdateType.Modify)
            {

                // this.UpdateSummaryRowState(currentBusiness.Tables[tb_健康档案.__TableName].Rows[0], currentType); //子表
                DataTable detail = currentBusiness.Tables[tb_健康档案.__TableName].Copy();
                //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                //detail.Rows[0][tb_健康档案.修改人] = Loginer.CurrentUser.Account;
                //detail.Rows[0][tb_健康档案.修改时间] = DateTime.Now;
                save.Tables.Add(detail); //加入明细数据 

                //this.UpdateSummaryRowState(currentBusiness.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0], currentType); //子表
                DataTable detail2 = currentBusiness.Tables[tb_健康档案_个人健康特征.__TableName].Copy();
                //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                save.Tables.Add(detail2); //加入明细数据 

                if (currentBusiness.Tables[tb_健康档案_健康状态.__TableName].Rows.Count >= 1)
                {
                    // this.UpdateSummaryRowState(currentBusiness.Tables[tb_健康档案_健康状态.__TableName].Rows[0], currentType); //子表
                    DataTable detail3 = currentBusiness.Tables[tb_健康档案_健康状态.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail3); //加入明细数据 
                }

                if (currentBusiness.Tables[tb_健康档案_既往病史.__TableName].Rows.Count >= 1)
                {
                    //this.UpdateSummaryRowState(currentBusiness.Tables[tb_健康档案_既往病史.__TableName].Rows[0], currentType); //子表
                    DataTable detail4 = currentBusiness.Tables[tb_健康档案_既往病史.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail4); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_健康档案_家族病史.__TableName].Rows.Count >= 1)
                {
                    //this.UpdateSummaryRowState(currentBusiness.Tables[tb_健康档案_家族病史.__TableName].Rows[0], currentType); //子表
                    DataTable detail4 = currentBusiness.Tables[tb_健康档案_家族病史.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail4); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_儿童基本信息.__TableName].Rows.Count >= 1)
                {
                    DataTable detail4 = currentBusiness.Tables[tb_儿童基本信息.__TableName].Copy();
                    save.Tables.Add(detail4); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_老年人基本信息.__TableName].Rows.Count >= 1)
                {
                    DataTable detail4 = currentBusiness.Tables[tb_老年人基本信息.__TableName].Copy();
                    save.Tables.Add(detail4); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_妇女基本信息.__TableName].Rows.Count >= 1)
                {
                    DataTable detail4 = currentBusiness.Tables[tb_妇女基本信息.__TableName].Copy();
                    save.Tables.Add(detail4); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_孕妇基本信息.__TableName].Rows.Count >= 1)
                {
                    DataTable detail4 = currentBusiness.Tables[tb_孕妇基本信息.__TableName].Copy();
                    save.Tables.Add(GetDistinctTable(detail4)); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_MXB慢病基础信息表.__TableName].Rows.Count >= 1)
                {
                    //this.UpdateSummaryRowState(currentBusiness.Tables[tb_MXB慢病基础信息表.__TableName].Rows[0], currentType); //子表
                    DataTable detail4 = currentBusiness.Tables[tb_MXB慢病基础信息表.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail4); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_MXB高血压管理卡.__TableName].Rows.Count >= 1)
                {
                    //this.UpdateSummaryRowState(currentBusiness.Tables[tb_MXB高血压管理卡.__TableName].Rows[0], currentType); //子表
                    DataTable detail4 = currentBusiness.Tables[tb_MXB高血压管理卡.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail4); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_MXB糖尿病管理卡.__TableName].Rows.Count >= 1)
                {
                    //this.UpdateSummaryRowState(currentBusiness.Tables[tb_MXB糖尿病管理卡.__TableName].Rows[0], currentType); //子表
                    DataTable detail4 = currentBusiness.Tables[tb_MXB糖尿病管理卡.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail4); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_MXB脑卒中管理卡.__TableName].Rows.Count >= 1)
                {
                    //this.UpdateSummaryRowState(currentBusiness.Tables[tb_MXB糖尿病管理卡.__TableName].Rows[0], currentType); //子表
                    DataTable detail4 = currentBusiness.Tables[tb_MXB脑卒中管理卡.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail4); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_MXB冠心病管理卡.__TableName].Rows.Count >= 1)
                {
                    //this.UpdateSummaryRowState(currentBusiness.Tables[tb_MXB糖尿病管理卡.__TableName].Rows[0], currentType); //子表
                    DataTable detail4 = currentBusiness.Tables[tb_MXB冠心病管理卡.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail4); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_精神疾病信息补充表.__TableName].Rows.Count >= 1)
                {
                    //this.UpdateSummaryRowState(currentBusiness.Tables[tb_MXB糖尿病管理卡.__TableName].Rows[0], currentType); //子表
                    DataTable detail4 = currentBusiness.Tables[tb_精神疾病信息补充表.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail4); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_残疾人_基本信息.__TableName].Rows.Count >= 1)
                {
                    //this.UpdateSummaryRowState(currentBusiness.Tables[tb_MXB糖尿病管理卡.__TableName].Rows[0], currentType); //子表
                    DataTable detail4 = currentBusiness.Tables[tb_残疾人_基本信息.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail4); //加入明细数据 
                }
            }
            return save;
        }
        /// <summary>
        ///创建用于保存的临时数据
        /// </summary>
        public DataSet CreateOnlyOneData(DataSet currentBusiness, UpdateType currentType)
        {

            this.UpdateSummaryRowState(this.DataBindRow, currentType);

            //创建用于保存的临时数据,里面包含主表数据
            DataSet save = this.DoCreateTempData(currentBusiness, tb_健康档案.__TableName);
            DataTable summary = save.Tables[0];
            ////根据需要自行取消注释
            //summary.Rows[0][BusinessCommonFields.s修改人] = Loginer.CurrentUser.Account;
            //summary.Rows[0][BusinessCommonFields.s修改时间] = DateTime.Now;
            if (currentType == UpdateType.Add || currentType == UpdateType.Modify || currentType == UpdateType.AddPeople)
            {
                //this.UpdateSummaryRowState(currentBusiness.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0], currentType); //子表
                DataTable detail2 = currentBusiness.Tables[tb_健康档案_个人健康特征.__TableName].Copy();
                //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                save.Tables.Add(detail2); //加入明细数据 

                if (currentBusiness.Tables[tb_健康档案_健康状态.__TableName].Rows.Count >= 1)
                {
                    //this.UpdateSummaryRowState(currentBusiness.Tables[tb_健康档案_健康状态.__TableName].Rows[0], currentType); //子表
                    DataTable detail3 = currentBusiness.Tables[tb_健康档案_健康状态.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail3); //加入明细数据 
                }
                if (currentBusiness.Tables.Contains(tb_家庭档案.__TableName) && currentBusiness.Tables[tb_家庭档案.__TableName].Rows.Count >= 1)
                {
                    //this.UpdateSummaryRowState(currentBusiness.Tables[tb_健康档案_健康状态.__TableName].Rows[0], currentType); //子表
                    DataTable detail4 = currentBusiness.Tables[tb_家庭档案.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail4); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_健康档案_既往病史.__TableName].Rows.Count >= 1)
                {
                    //this.UpdateSummaryRowState(currentBusiness.Tables[tb_健康档案_既往病史.__TableName].Rows[0], currentType); //子表
                    DataTable detail5 = currentBusiness.Tables[tb_健康档案_既往病史.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail5); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_健康档案_家族病史.__TableName].Rows.Count >= 1)
                {
                    //this.UpdateSummaryRowState(currentBusiness.Tables[tb_健康档案_家族病史.__TableName].Rows[0], currentType); //子表
                    DataTable detail6 = currentBusiness.Tables[tb_健康档案_家族病史.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail6); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_儿童基本信息.__TableName].Rows.Count >= 1)
                {
                    //this.UpdateSummaryRowState(currentBusiness.Tables[tb_儿童基本信息.__TableName].Rows[0], currentType); //子表
                    DataTable detail7 = currentBusiness.Tables[tb_儿童基本信息.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail7); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_老年人基本信息.__TableName].Rows.Count >= 1)
                {
                    //this.UpdateSummaryRowState(currentBusiness.Tables[tb_老年人基本信息.__TableName].Rows[0], currentType); //子表
                    DataTable detail8 = currentBusiness.Tables[tb_老年人基本信息.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail8); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_妇女基本信息.__TableName].Rows.Count >= 1)
                {
                    //this.UpdateSummaryRowState(currentBusiness.Tables[tb_老年人基本信息.__TableName].Rows[0], currentType); //子表
                    DataTable detail9 = currentBusiness.Tables[tb_妇女基本信息.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail9); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_孕妇基本信息.__TableName].Rows.Count >= 1)
                {
                    //this.UpdateSummaryRowState(currentBusiness.Tables[tb_老年人基本信息.__TableName].Rows[0], currentType); //子表
                    DataTable detail10 = currentBusiness.Tables[tb_孕妇基本信息.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail10); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_MXB慢病基础信息表.__TableName].Rows.Count >= 1)
                {
                    //this.UpdateSummaryRowState(currentBusiness.Tables[tb_MXB慢病基础信息表.__TableName].Rows[0], currentType); //子表
                    DataTable detail11 = currentBusiness.Tables[tb_MXB慢病基础信息表.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail11); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_MXB高血压管理卡.__TableName].Rows.Count >= 1)
                {
                    //this.UpdateSummaryRowState(currentBusiness.Tables[tb_MXB高血压管理卡.__TableName].Rows[0], currentType); //子表
                    DataTable detail12 = currentBusiness.Tables[tb_MXB高血压管理卡.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail12); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_MXB糖尿病管理卡.__TableName].Rows.Count >= 1)
                {
                    // this.UpdateSummaryRowState(currentBusiness.Tables[tb_MXB糖尿病管理卡.__TableName].Rows[0], currentType); //子表
                    DataTable detail13 = currentBusiness.Tables[tb_MXB糖尿病管理卡.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail13); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_MXB脑卒中管理卡.__TableName].Rows.Count >= 1)
                {
                    // this.UpdateSummaryRowState(currentBusiness.Tables[tb_MXB糖尿病管理卡.__TableName].Rows[0], currentType); //子表
                    DataTable detail14 = currentBusiness.Tables[tb_MXB脑卒中管理卡.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail14); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_MXB冠心病管理卡.__TableName].Rows.Count >= 1)
                {
                    // this.UpdateSummaryRowState(currentBusiness.Tables[tb_MXB糖尿病管理卡.__TableName].Rows[0], currentType); //子表
                    DataTable detail15 = currentBusiness.Tables[tb_MXB冠心病管理卡.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail15); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_残疾人_基本信息.__TableName].Rows.Count >= 1)
                {
                    // this.UpdateSummaryRowState(currentBusiness.Tables[tb_MXB糖尿病管理卡.__TableName].Rows[0], currentType); //子表
                    DataTable detail16 = currentBusiness.Tables[tb_残疾人_基本信息.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail16); //加入明细数据 
                }
                if (currentBusiness.Tables[tb_精神疾病信息补充表.__TableName].Rows.Count >= 1)
                {
                    // this.UpdateSummaryRowState(currentBusiness.Tables[tb_MXB糖尿病管理卡.__TableName].Rows[0], currentType); //子表
                    DataTable detail17 = currentBusiness.Tables[tb_精神疾病信息补充表.__TableName].Copy();
                    //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                    save.Tables.Add(detail17); //加入明细数据 
                }
            }
            return save;
        }
        /// <summary>
        ///查询数据
        /// </summary>
        public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
        {
            DataTable dt = new dal健康档案(Loginer.CurrentUser).GetSummaryByParam(docNoFrom, docNoTo, docDateFrom, docDateTo);
            //this.SetNumericDefaultValue(ds); //设置预设值
            //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return dt;
        }

        /// <summary>
        ///查询数据
        /// </summary>
        public DataTable GetSummaryByParam(string strWhere)
        {
            DataTable dt = new dal健康档案(Loginer.CurrentUser).GetSummaryByParam(strWhere);
            //this.SetNumericDefaultValue(ds); //设置预设值
            //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return dt;
        }

        /// <summary>
        ///查询数据家庭档案
        /// </summary>
        public DataTable Get家庭档案(string strWhere)
        {
            DataTable dt = new dal健康档案(Loginer.CurrentUser).Get家庭档案(strWhere);
            //this.SetNumericDefaultValue(ds); //设置预设值
            //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return dt;
        }

        /// <summary>
        ///获取报表数据
        /// </summary>
        public DataSet GetReportDataBYCJSJ(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string type)
        {
            DataSet ds = new dal健康档案(Loginer.CurrentUser).GetReportDataBYCJSJ(DocNo机构, DocNo完整度, DateFrom, DateTo, type);
            //this.SetNumericDefaultValue(ds); //设置预设值
            return ds;
        }
        public DataSet GetReportDataBYDCSJ(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string type)
        {
            DataSet ds = new dal健康档案(Loginer.CurrentUser).GetReportDataBYDCSJ(DocNo机构, DocNo完整度, DateFrom, DateTo, type);
            //this.SetNumericDefaultValue(ds); //设置预设值
            return ds;
        }
        #region Business Log

        /// <summary>
        /// 写入日志
        /// </summary>
        public override void WriteLog()
        {
            //TODO:  填写日志，需要重新开发
            //if (this.DataBinder.Rows.Count==1)
            //{

            //}
            //string key = this.DataBinder.Rows[0][tb_家庭档案.__KeyName].ToString();//取单据号码
            //DataSet dsOriginal = this.GetBusinessByKey(key, false); //取保存前的原始数据, 用于保存日志时匹配数据.
            //DataSet dsTemplate = this.CreateSaveData(this.CurrentBusiness, UpdateType.Modify); //创建用于保存的临时数据
            //this.WriteLog(dsOriginal, dsTemplate);//保存日志      
        }

        /// <summary>
        /// 写入日志
        /// </summary>
        /// <param name="original">原始数据</param>
        /// <param name="changes">修改后的数据</param>
        public override void WriteLog(DataTable original, DataTable changes)
        {

            //单独处理,即使错误,不向外抛出异常
            //if (_SaveVersionLog == false) return; //应用策略
            try
            {
                string logID = Guid.NewGuid().ToString().Replace("-", ""); //本次日志ID

                IBridge_EditLogHistory SystemLog = bllBusinessLog.CreateEditLogHistoryBridge();//初始化日志方法

                SystemLog.WriteLog(logID, original, changes, tb_健康档案.__TableName, tb_健康档案.__KeyName, true); //主表
                //明细表的修改日志,系统不支持自动生成,请手工调整代码
                //SystemLog.WriteLog(logID, original.Tables[1], changes.Tables[1], tb_健康档案.__TableName, tb_健康档案.__KeyName, false);
            }
            catch
            {
                Msg.Warning("写入日志发生错误！");
            }

        }

        /// <summary>
        /// 写入日志
        /// </summary>
        /// <param name="original">原始数据</param>
        /// <param name="changes">修改后的数据</param>
        public override void WriteLog(DataSet original, DataSet changes)
        {  //单独处理,即使错误,不向外抛出异常
            //if (_SaveVersionLog == false) return; //应用策略
            try
            {
                string logID = Guid.NewGuid().ToString().Replace("-", ""); //本次日志ID
                //SystemLog.WriteLog(logID, original.Tables[0], changes.Tables[0], tb_健康档案.__TableName, tb_健康档案.__KeyName, true); //主表
                //明细表的修改日志,系统不支持自动生成,请手工调整代码
                //SystemLog.WriteLog(logID, original.Tables[1], changes.Tables[1], tb_健康档案.__TableName, tb_健康档案.__KeyName, false);
            }
            catch
            {
                Msg.Warning("写入日志发生错误！");
            }
        }

        #endregion

        public DataTable GetBusinessByID(string id)
        {
            return new dal健康档案(Loginer.CurrentUser).GetBusinessByID(id);
        }

        public string GetYHZGX(string docNo)
        {
            return new dal健康档案(Loginer.CurrentUser).GetYHZGX(docNo);
        }

        //个人基本信息
        public DataSet GetInfoByXinxi(string _docNo, bool resetCurrent)
        {
            DataSet ds = new dal健康档案(Loginer.CurrentUser).GetInfoByXinxi(_docNo);
            //this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }
		
		public string GetAddressByDAH(string dah)
        {
            return new dal健康档案(Loginer.CurrentUser).GetAddressByDAH(dah);
        }
        		
        //档案管理率获取
        public DataSet GetReportDataOfGLL(string DocNo机构, string DocNo完整度, string type)
        {
            DataSet ds = new dal健康档案(Loginer.CurrentUser).GetReportDataOfGLL(DocNo机构, DocNo完整度, type);
            //this.SetNumericDefaultValue(ds); //设置预设值
            return ds;
        }

        /// <summary>
        /// 获取签约医生
        /// </summary>
        /// <param name="IDCrad"></param>
        /// <returns></returns>
        public string GetQYYS(string IDCrad)
        {
            try
            {

                return _MyBridge.GetQYYS(IDCrad);
            }
            catch (Exception)
            {
                return "";
            }
        }

        public string GetQYYSName(string IDCrad)
        {
            try
            {
                return _MyBridge.GetQYYSName(IDCrad);
            }
            catch (Exception)
            {
                return "";
            }
        }

        //Begin WXF @@2018-11-01 | 17:08
        //新增既往史疾病查询,用于档案分类判断 
        public System.Data.DataSet Get_基本信息And既往史疾病And健康状态(string grdah)
        {
            DataSet ds = new dal健康档案(Loginer.CurrentUser).Get_基本信息And既往史疾病And健康状态(grdah);
            //this.SetNumericDefaultValue(ds); //设置预设值
            //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }
        //End

        public DataSet Get_健康档案封面(string str_recordNum)
        {
            DataSet ds = new dal健康档案(Loginer.CurrentUser).Get_健康档案封面(str_recordNum);

            return ds;
        }

        public bool Is孕产妇(string dah)
        {
            return new dal健康档案(Loginer.CurrentUser).Is孕产妇(dah);
        }

        /// <summary>
        /// 肺结核、高血压、糖尿病、冠心病、脑卒中、精神疾病
        /// </summary>
        /// <param name="dah"></param>
        /// <returns></returns>
        public DataSet Get重点人群档案号ByDah(List<string> dahlist)
        {
            if (dahlist.Count == 0)
            {
                return null;
            }
            //健康档案_既往病史
            return new dal健康档案(Loginer.CurrentUser).Get重点人群档案号ByDah(dahlist);
        }
    }
}
