﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using AtomEHR.Bridge;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Business.BLL_Business;
using AtomEHR.Common;
using AtomEHR.Models;
using AtomEHR.Server.DataAccess;
using AtomEHR.Interfaces.Interfaces_Bridge;

/*==========================================
 *   程序说明: 精神病随访记录的业务逻辑层
 *   作者姓名: ATOM
 *   创建日期: 2015/10/31 02:42:31
 *   最后修改: 2015/10/31 02:42:31
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Business
{
    /// <summary>
    /// bll精神病随访记录
    /// </summary>
    public class bll精神病随访记录 : bllBaseBusiness
    {
         /// <summary>
         /// 构造器
         /// </summary>
         public bll精神病随访记录()
         {
             _KeyFieldName = tb_精神病随访记录.__KeyName; //主键字段
             _SummaryTableName = tb_精神病随访记录.__TableName;//表名
         }

          /// <summary>
          ///根据单据号码取业务数据
          /// </summary>
          public override DataSet GetBusinessByKey(string keyValue, bool resetCurrent)
          {
               DataSet ds = new dal精神病随访记录(Loginer.CurrentUser).GetBusinessByKey(keyValue); 
              //this.SetNumericDefaultValue(ds); //设置预设值
              if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return ds;
           }

          //public override DataSet GetAllBusinessByKey(string keyValue, bool resetCurrent)
          //{
          //    DataSet ds = new dal精神病随访记录(Loginer.CurrentUser).GetAllBusinessByKey(keyValue);
          //    //this.SetNumericDefaultValue(ds); //设置预设值
          //    if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
          //    return ds;
          //}


          /// <summary>
          ///删除单据
          /// </summary>
          public override bool Delete(string keyValue)
          {
              return new dal精神病随访记录(Loginer.CurrentUser).Delete(keyValue);
          }

          /// <summary>
          ///检查单号是否存在
          /// </summary>
          public bool CheckNoExists(string keyValue)
          {
              return new dal精神病随访记录(Loginer.CurrentUser).CheckNoExists(keyValue);
          }

          /// <summary>
          ///保存数据
          /// </summary>
          public override SaveResult Save(DataSet saveData)
          {
              return new dal精神病随访记录(Loginer.CurrentUser).Update(saveData); //交给数据层处理
          }

          /// <summary>
          ///审核单据
          /// </summary>
          public override void ApprovalBusiness(DataRow summaryRow)
          {
               summaryRow[BusinessCommonFields.AppDate] = DateTime.Now;
               summaryRow[BusinessCommonFields.AppUser] = Loginer.CurrentUser.Account;
               summaryRow[BusinessCommonFields.FlagApp] = "Y";
               string key = ConvertEx.ToString(summaryRow[tb_精神病随访记录.__KeyName]);
               new dal精神病随访记录(Loginer.CurrentUser).ApprovalBusiness(key, "Y", Loginer.CurrentUser.Account, DateTime.Now);
          }

          /// <summary>
          ///新增一张业务单据
          /// </summary>
          public override void NewBusiness()
          {
              DataTable summaryTable = _CurrentBusiness.Tables[tb_精神病随访记录.__TableName];
              DataRow row = summaryTable.Rows.Add();     
              //根据需要自行取消注释
              //row[tb_精神病随访记录.__KeyName] = "*自动生成*";
              //row[tb_精神病随访记录.创建人] = Loginer.CurrentUser.Account;
              //row[tb_精神病随访记录.创建时间] = DateTime.Now;
              //row[tb_精神病随访记录.修改人] = Loginer.CurrentUser.Account;
              //row[tb_精神病随访记录.修改时间] = DateTime.Now;
           }

          /// <summary>
          ///创建用于保存的临时数据
          /// </summary>
          public override DataSet CreateSaveData(DataSet currentBusiness, UpdateType currentType)
          {
              this.UpdateSummaryRowState(this.DataBindRow, currentType);

              //创建用于保存的临时数据,里面包含主表数据
              DataSet save = this.DoCreateTempData(currentBusiness, tb_精神病随访记录.__TableName);
              DataTable summary = save.Tables[0];
              //根据需要自行取消注释
              //summary.Rows[0][BusinessCommonFields.s修改人] = Loginer.CurrentUser.Account;
              //summary.Rows[0][BusinessCommonFields.s修改时间] = DateTime.Now;

              //DataTable detail = currentBusiness.Tables[tb_精神病随访记录s.__TableName].Copy();
              //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
              //save.Tables.Add(detail); //加入明细数据 

              return save; 
          }

          /// <summary>
          ///查询数据
          /// </summary>
          public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
          {
              DataTable dt = new dal精神病随访记录(Loginer.CurrentUser).GetSummaryByParam(docNoFrom, docNoTo, docDateFrom, docDateTo);
              //this.SetNumericDefaultValue(ds); //设置预设值
              //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return dt;
          }

          /// <summary>
          ///获取报表数据
          /// </summary>
          public DataSet GetReportDataByLRSJ(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string type)
          {
              DataSet ds = new dal精神病随访记录(Loginer.CurrentUser).GetReportDataByLRSJ(DocNo机构, DocNo完整度, DateFrom, DateTo, type);
              this.SetNumericDefaultValue(ds); //设置预设值
              return ds;
          }
          public DataSet GetReportDataByDCSJ(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string type)
          {
              DataSet ds = new dal精神病随访记录(Loginer.CurrentUser).GetReportDataByDCSJ(DocNo机构, DocNo完整度, DateFrom, DateTo, type);
              this.SetNumericDefaultValue(ds); //设置预设值
              return ds;
          }
          #region Business Log

          /// <summary>
          /// 写入日志
          /// </summary>
          public override void WriteLog()
          {
              string key = this.DataBinder.Rows[0][tb_精神病随访记录.__KeyName].ToString();//取单据号码
              DataSet dsOriginal = this.GetBusinessByKey(key, false); //取保存前的原始数据, 用于保存日志时匹配数据.
              DataSet dsTemplate = this.CreateSaveData(this.CurrentBusiness, UpdateType.Modify); //创建用于保存的临时数据
              this.WriteLog(dsOriginal, dsTemplate);//保存日志      
          }

          /// <summary>
          /// 写入日志
          /// </summary>
          /// <param name="original">原始数据</param>
          /// <param name="changes">修改后的数据</param>
          public override void WriteLog(DataTable original, DataTable changes) { }

          /// <summary>
          /// 写入日志
          /// </summary>
          /// <param name="original">原始数据</param>
          /// <param name="changes">修改后的数据</param>
          public override void WriteLog(DataSet original, DataSet changes)
          {  //单独处理,即使错误,不向外抛出异常
              //if (_SaveVersionLog == false) return; //应用策略
              try
              {
                  string logID = Guid.NewGuid().ToString().Replace("-", ""); //本次日志ID

                  IBridge_EditLogHistory SystemLog = bllBusinessLog.CreateEditLogHistoryBridge();//初始化日志方法

                  SystemLog.WriteLog(logID, original.Tables[0], changes.Tables[0], tb_精神病随访记录.__TableName, tb_精神病随访记录.__KeyName, true); //主表
                  //明细表的修改日志,系统不支持自动生成,请手工调整代码
                  //SystemLog.WriteLog(logID, original.Tables[1], changes.Tables[1], tb_精神病随访记录.__TableName, tb_精神病随访记录.__KeyName, false);
              }
              catch
              {
                  Msg.Warning("写入日志发生错误！");
              }
          }

          #endregion

        #region 新增的方法
          /// <summary>
          ///根据单据号码取业务数据
          /// </summary>
          public DataSet GetBusinessByGrdabhAndID(string grdabh, string id, bool resetCurrent)
          {
              DataSet ds = new dal精神病随访记录(Loginer.CurrentUser).GetBusinessByGrdabhAndID(grdabh, id);
              //this.SetNumericDefaultValue(ds); //设置预设值
              if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return ds;
          }

        //返回的DataSet中只提供ID, 个人档案编号, 随访日期三个字段
          public DataSet Get随访概要(string grdabh)
          {
              DataSet ds = new dal精神病随访记录(Loginer.CurrentUser).Get随访概要(grdabh);
              return ds;
          }

        /// <summary>
        /// 只获取一条随访记录的数据
        /// </summary>
        /// <returns></returns>
          public DataSet Get随访单一(string grdabh, string id)
          {
              DataSet ds = new dal精神病随访记录(Loginer.CurrentUser).Get随访单一(grdabh, id);
              return ds;
          }

          public DataSet Get基本概要(string grdabh)
          {
              DataSet ds = new dal精神病随访记录(Loginer.CurrentUser).Get基本概要(grdabh);
              return ds;
          }

          public bool DeleteByID(string id)
          {
              return new dal精神病随访记录(Loginer.CurrentUser).DeleteByID(id);
          }

          public string GetIdByGrdabhAndUpdateUserTime(string grdabh, string updateuser, string updatetime)
          {
              return new dal精神病随访记录(Loginer.CurrentUser).GetIdByGrdabhAndUpdateUserTime(grdabh, updateuser, updatetime);
          }

          public int GetVisitCount(string grdabh, string year)
          {
              return new dal精神病随访记录(Loginer.CurrentUser).GetVisitCount(grdabh, year);
          }

        public DataSet GetBusinessByID(string keyValue)
          {
              DataSet ds = new dal精神病随访记录(Loginer.CurrentUser).GetBusinessByKey(keyValue); 
              //this.SetNumericDefaultValue(ds); //设置预设值
              //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return ds;
           }

        public DataSet GetInfoByGrdabhAndYearOrderByVisitDate(string grdabh, string year, bool resetCurrent)
        {
            DataSet ds = new dal精神病随访记录(Loginer.CurrentUser).GetInfoByGrdabhAndYearOrderByVisitDate(grdabh,year); 
              //this.SetNumericDefaultValue(ds); //设置预设值
              if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return ds;
        }

        public DataSet GetInfoByGrdabhAndNotYearOrderbyNextDate(string grdabh, string year)
        {
            DataSet ds = new dal精神病随访记录(Loginer.CurrentUser).GetInfoByGrdabhAndNotYearOrderbyNextDate(grdabh,year); 
              //this.SetNumericDefaultValue(ds); //设置预设值
              //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return ds;
        }

        public void Update信息补充表And个人体征(string xxbGrdabh, string xxbXcsfsj, string tzGrdabh, string tz)
        {
            new dal精神病随访记录(Loginer.CurrentUser).Update信息补充表And个人体征(xxbGrdabh, xxbXcsfsj, tzGrdabh, tz);
        }

        public void DeleteMDinfoByVisitID(string sfid)
        {
            new dal精神病_用药情况(Loginer.CurrentUser).DeleteBySFID(sfid);
        }

        public void UpdateSFCSByID(string id, string sfcs)
        {
            new dal精神病随访记录(Loginer.CurrentUser).UpdateSFCSByID(id,sfcs);
        }

        public void deletsfbyid(string id)
        {
            dal精神病随访记录 dal = new dal精神病随访记录(Loginer.CurrentUser);
            Hashtable tcjrJsjbxxbcb = new Hashtable();
            Hashtable grtzb = new Hashtable();
            string serverDateTime = this.ServiceDateTime;

            DateTime createTime;
            DateTime date;
            try
            {
                //根据ID 获取随访记录
                DataSet dsSelectedByID = dal.GetBusinessByID(id);
                string strSFCS = dsSelectedByID.Tables[0].Rows[0][tb_精神病随访记录.SFCS].ToString();
                //TcjrJingshen tcjrJingshen = this.GetBusinessByKey(id);
                //String 随访次数 = tcjrJingshen.getSfcs();//取得随访次数

                //设置随访日期年
                //tcjrJingshen.setHappentime(tcjrJingshen.getHappentime().substring(0, 4));
                string year = dsSelectedByID.Tables[0].Rows[0][tb_精神病随访记录.随访日期].ToString().Substring(0, 4);
                string grdabh = dsSelectedByID.Tables[0].Rows[0][tb_精神病随访记录.个人档案编号].ToString();

                //查询同一年的随访记录
                DataSet listsf = dal.GetInfoByGrdabhAndYearOrderByVisitDate(grdabh, year);
                //List listsf = this.tcjrJingshenMapper.根据个人档案号、随访时间=thisyear查询 orderby随访时间(tcjrJingshen);
                if (listsf != null && listsf.Tables.Count > 0 && listsf.Tables[0].Rows.Count > 1)
                //if (listsf.size() > 1) 
                {
                    //TcjrJingshen tingli;
                    if (listsf.Tables[0].Rows[0][tb_精神病随访记录.ID].ToString() == id)
                    //if (((TcjrJingshen)listsf.get(0)).getId().longValue() == Long.parseLong(id)) 
                    { 														//查询出的同一年随访记录的第一条是否为要删除的记录

                        createTime = DateTime.Now;
                        date = DateTime.Now;
                        //TcjrJsjbxxbcb tcjrJsjbxxbcb = new TcjrJsjbxxbcb();
                        try
                        {
                            //同一年的随访记录中的第二条记录的下次随访日期
                            //createTime = Long.valueOf(sdf.parse(((TcjrJingshen)listsf.get(1)).getcXcsfrq()).getTime());
                            createTime = Convert.ToDateTime(listsf.Tables[0].Rows[1][tb_精神病随访记录.下次随访日期].ToString());
                            date = Convert.ToDateTime(serverDateTime);
                            //date = Long.valueOf(sdf.parse(服务器时间).getTime());
                        }
                        catch //(ParseException e) 
                        {
                            //e.printStackTrace();
                        }
                        //List listallsf;
                        DataSet listallsf;
                        //if (createTime.longValue() > date.longValue()) //同一年的随访记录中的第二条记录的下次随访日期 > serverDateTime 
                        if (createTime > date)
                        {
                            //tcjrJsjbxxbcb.setdGrdabh(((TcjrJingshen)listsf.get(1)).getdGrdabh());
                            //tcjrJsjbxxbcb.setjXcsfsj(((TcjrJingshen)listsf.get(1)).getcXcsfrq());

                            tcjrJsjbxxbcb[tb_精神疾病信息补充表.个人档案编号] = listsf.Tables[0].Rows[1][tb_精神病随访记录.个人档案编号].ToString();
                            tcjrJsjbxxbcb[tb_精神疾病信息补充表.下次随访时间] = listsf.Tables[0].Rows[1][tb_精神病随访记录.下次随访日期].ToString();

                            //String qdqxz = ((TcjrJingshen)listsf.get(1)).getQdqxz();
                            //String wzd = ((TcjrJingshen)listsf.get(1)).getWzd();
                            string qdqxz = listsf.Tables[0].Rows[1][tb_精神病随访记录.缺项].ToString();
                            string wzd = listsf.Tables[0].Rows[1][tb_精神病随访记录.完整度].ToString();


                            //查询与被删除记录的随访日期不在同一年的随访记录
                            //listallsf = this.tcjrJingshenMapper.根据个人档案、随访日期!=thisyear号号查询Orderby下次随访日期(tcjrJingshen);
                            listallsf = dal.GetInfoByGrdabhAndNotYearOrderbyNextDate(grdabh, year);
                            //if ((listallsf != null) && (listallsf.size() > 0)) 
                            if (listallsf != null && listallsf.Tables.Count > 0 && listallsf.Tables[0].Rows.Count > 0)
                            {
                                foreach (DataRow tingli in listallsf.Tables[0].Rows)
                                {
                                    //for (TcjrJingshen tingli : listallsf) {
                                    //tcjrJsjbxxbcb.setdGrdabh(tingli.getdGrdabh());
                                    tcjrJsjbxxbcb[tb_精神疾病信息补充表.个人档案编号] = tingli[tb_精神病随访记录.个人档案编号].ToString();
                                    try
                                    {
                                        if ((tingli[tb_精神病随访记录.ID].ToString() != id)
                                            && (Convert.ToDateTime(tingli[tb_精神病随访记录.下次随访日期].ToString()) > Convert.ToDateTime(tcjrJsjbxxbcb[tb_精神疾病信息补充表.下次随访时间])))
                                        {
                                            //if ((tingli.getId().longValue() != Long.parseLong(id)) 
                                            //&& (sdf.parse(tingli.getcXcsfrq()).getTime() > sdf.parse(tcjrJsjbxxbcb.getjXcsfsj()).getTime())) {
                                            //tcjrJsjbxxbcb.setjXcsfsj(tingli.getcXcsfrq());
                                            tcjrJsjbxxbcb[tb_精神疾病信息补充表.下次随访时间] = tingli[tb_精神病随访记录.下次随访日期].ToString();
                                            //qdqxz = tingli.getQdqxz();
                                            //wzd = tingli.getWzd();
                                            qdqxz = tingli[tb_精神病随访记录.缺项].ToString();
                                            wzd = tingli[tb_精神病随访记录.完整度].ToString();
                                        }
                                    }
                                    catch //(NumberFormatException e) 
                                    {
                                        //e.printStackTrace();
                                    }
                                    //catch (ParseException e) 
                                    //{
                                    //    e.printStackTrace();
                                    //}
                                }
                            }

                            //将第二条随访记录的
                            //tz.settCjrJingshen(qdqxz + "," + wzd);
                            grtzb[tb_健康档案_个人健康特征.精神疾病随访表] = qdqxz + "," + wzd;
                            //tzExampleC.andDGrdabhEqualTo(((TcjrJingshen)listsf.get(1)).getdGrdabh());
                            grtzb[tb_健康档案_个人健康特征.个人档案编号] = listallsf.Tables[0].Rows[1][tb_精神病随访记录.个人档案编号].ToString();
                        }
                        else
                        {
                            //List listallsf = this.tcjrJingshenMapper.根据个人档案、随访日期!=thisyear号号查询Orderby下次随访日期(tcjrJingshen);
                            listallsf = dal.GetInfoByGrdabhAndNotYearOrderbyNextDate(grdabh, year);
                            //if ((listallsf != null) && (listallsf.size() > 0)) 
                            if (listallsf != null && listallsf.Tables.Count > 0 && listallsf.Tables[0].Rows.Count > 0)
                            {
                                //tcjrJsjbxxbcb.setjXcsfsj("1900-01-01");
                                tcjrJsjbxxbcb[tb_精神疾病信息补充表.下次随访时间] = "1900-01-01";
                                //for (listallsf = listallsf.iterator(); listallsf.hasNext(); ) 
                                for (int index = 0; index < listallsf.Tables[0].Rows.Count; index++)
                                {
                                    //tingli = (TcjrJingshen)listallsf.next();
                                    DataRow tingli = listallsf.Tables[0].Rows[index];
                                    //tcjrJsjbxxbcb.setdGrdabh(tingli.getdGrdabh());
                                    tcjrJsjbxxbcb[tb_精神疾病信息补充表.个人档案编号] = tingli[tb_精神病随访记录.个人档案编号].ToString();
                                    try
                                    {
                                        //if ((tingli.getId().longValue() != Long.parseLong(id)) 
                                        //&& (sdf.parse(tingli.getcXcsfrq()).getTime() > sdf.parse(tcjrJsjbxxbcb.getjXcsfsj()).getTime())) 
                                        if (tingli[tb_精神病随访记录.ID].ToString() != id
                                            && Convert.ToDateTime(tingli[tb_精神病随访记录.下次随访日期]) > Convert.ToDateTime(tcjrJsjbxxbcb[tb_精神疾病信息补充表.下次随访时间].ToString()))
                                        {
                                            //tcjrJsjbxxbcb.setjXcsfsj(tingli.getcXcsfrq());
                                            tcjrJsjbxxbcb[tb_精神疾病信息补充表.下次随访时间] = tingli[tb_精神病随访记录.下次随访日期].ToString();
                                            //createTime = Long.valueOf(sdf.parse(tingli.getcXcsfrq()).getTime());
                                            createTime = Convert.ToDateTime(tingli[tb_精神病随访记录.下次随访日期].ToString());
                                            //date = Long.valueOf(sdf.parse(serverDateTime).getTime());
                                            date = Convert.ToDateTime(serverDateTime);

                                            //if (createTime.longValue() > date.longValue()) {
                                            if (createTime > date)
                                            {
                                                //String qdqxz = tingli.getQdqxz();
                                                //String wzd = tingli.getWzd();
                                                string qdqxz = tingli[tb_精神病随访记录.缺项].ToString();
                                                string wzd = tingli[tb_精神病随访记录.完整度].ToString();

                                                //tz.settCjrJingshen(qdqxz + "," + wzd);
                                                grtzb[tb_健康档案_个人健康特征.精神疾病随访表] = qdqxz + "," + wzd;
                                                //tzExampleC.andDGrdabhEqualTo(tingli.getdGrdabh());
                                                grtzb[tb_健康档案_个人健康特征.个人档案编号] = tingli[tb_精神病随访记录.个人档案编号].ToString();
                                            }
                                            else
                                            {
                                                //tz.settCjrJingshen("未建");
                                                grtzb[tb_健康档案_个人健康特征.精神疾病随访表] = "未建";
                                                //tzExampleC.andDGrdabhEqualTo(tingli.getdGrdabh());
                                                grtzb[tb_健康档案_个人健康特征.个人档案编号] = tingli[tb_精神病随访记录.个人档案编号].ToString();
                                            }
                                            //tzExampleC.andDGrdabhEqualTo(tingli.getdGrdabh());
                                            grtzb[tb_健康档案_个人健康特征.个人档案编号] = tingli[tb_精神病随访记录.个人档案编号].ToString();
                                        }
                                    }
                                    catch //(NumberFormatException e) 
                                    {
                                        //e.printStackTrace();
                                    }
                                    //catch (ParseException e) 
                                    //{
                                    //    e.printStackTrace();
                                    //}
                                }
                            }
                            else
                            {
                                //tz.settCjrJingshen("未建");
                                grtzb[tb_健康档案_个人健康特征.精神疾病随访表] = "未建";

                                //tzExampleC.andDGrdabhEqualTo(((TcjrJingshen)listsf.get(1)).getdGrdabh());
                                grtzb[tb_健康档案_个人健康特征.个人档案编号] = listsf.Tables[0].Rows[1][tb_精神病随访记录.个人档案编号].ToString();

                                //tcjrJsjbxxbcb.setdGrdabh(((TcjrJingshen)listsf.get(1)).getdGrdabh());
                                //tcjrJsjbxxbcb.setjXcsfsj(((TcjrJingshen)listsf.get(1)).getcXcsfrq());
                                tcjrJsjbxxbcb[tb_精神疾病信息补充表.个人档案编号] = listsf.Tables[0].Rows[1][tb_精神病随访记录.个人档案编号].ToString();
                                tcjrJsjbxxbcb[tb_精神疾病信息补充表.下次随访时间] = listsf.Tables[0].Rows[1][tb_精神病随访记录.下次随访日期].ToString();
                            }
                        }

                        //this.tdaJkdaGrjktzMapper.updateByExampleSelective(tz, tzExample);
                        //this.tcjrJsjbxxbcbMapper.updateByPrimaryKeySelective(tcjrJsjbxxbcb);
                        //更新
                        //throw  new Exception("未实现");
                        //dal.Update信息补充表And个人体征(tcjrJsjbxxbcb[tb_精神病随访记录.个人档案编号].ToString(), tcjrJsjbxxbcb[tb_精神病随访记录.下次随访日期].ToString(),
                        //                                 grtzb[tb_健康档案_个人健康特征.个人档案编号].ToString(), grtzb[tb_健康档案_个人健康特征.精神疾病随访表].ToString());
                        string bcbgrdabh = tcjrJsjbxxbcb[tb_精神疾病信息补充表.个人档案编号].ToString();
                        string xcsfsj = tcjrJsjbxxbcb[tb_精神疾病信息补充表.下次随访时间].ToString();
                        string tzgrdabh = grtzb[tb_健康档案_个人健康特征.个人档案编号].ToString();
                        string tzjsjb = grtzb[tb_健康档案_个人健康特征.精神疾病随访表].ToString();
                        dal.Update信息补充表And个人体征(bcbgrdabh, xcsfsj, tzgrdabh, tzjsjb);
                    }
                    else
                    {
                        //TcjrJsjbxxbcb tcjrJsjbxxbcb = new TcjrJsjbxxbcb();
                        //tcjrJsjbxxbcb.setjXcsfsj("1900-01-01");
                        tcjrJsjbxxbcb[tb_精神疾病信息补充表.下次随访时间] = "1900-01-01";
                        //for (TcjrJingshen tingli : listsf) 
                        foreach (DataRow tingli in listsf.Tables[0].Rows)
                        {
                            //if ((tingli.getSfcs() != null) && (!"".equals(tingli.getSfcs())) && (Integer.parseInt(tingli.getSfcs()) > Integer.parseInt(随访次数))) {
                            if (!(string.IsNullOrWhiteSpace(tingli[tb_精神病随访记录.SFCS].ToString())) && !(string.IsNullOrWhiteSpace(strSFCS))
                             && Convert.ToInt32(tingli[tb_精神病随访记录.SFCS].ToString()) > Convert.ToInt32(strSFCS))
                            {
                                //TcjrJingshen tinglinew = new TcjrJingshen();
                                //tinglinew.setdGrdabh(tingli.getdGrdabh());
                                //int sfcsnew = Integer.parseInt(tingli.getSfcs()) - 1;
                                int sfcsnew = Convert.ToInt32(tingli[tb_精神病随访记录.SFCS].ToString()) - 1;
                                //tinglinew.setSfcs(sfcsnew);
                                //this.tcjrJingshenMapper.updateByPrimaryKeySelective(tinglinew);
                                dal.UpdateSFCSByID(tingli[tb_精神病随访记录.ID].ToString(), sfcsnew.ToString());
                            }
                            //tcjrJsjbxxbcb.setdGrdabh(tingli.getdGrdabh());
                            tcjrJsjbxxbcb[tb_精神疾病信息补充表.个人档案编号] = tingli[tb_精神病随访记录.个人档案编号].ToString();
                            try
                            {
                                //if ((tingli.getId().longValue() != Long.parseLong(id)) && (sdf.parse(tingli.getcXcsfrq()).getTime() > sdf.parse(tcjrJsjbxxbcb.getjXcsfsj()).getTime())) 
                                if (tingli[tb_精神病随访记录.ID].ToString() != id
                                    && Convert.ToDateTime(tingli[tb_精神病随访记录.下次随访日期].ToString()) > Convert.ToDateTime(tcjrJsjbxxbcb[tb_精神疾病信息补充表.下次随访时间].ToString()))
                                {
                                    //tcjrJsjbxxbcb.setjXcsfsj(tingli.getcXcsfrq());
                                    tcjrJsjbxxbcb[tb_精神疾病信息补充表.下次随访时间] = tingli[tb_精神病随访记录.下次随访日期].ToString();
                                    //createTime = Long.valueOf(sdf.parse(tingli.getcXcsfrq()).getTime());
                                    //date = Long.valueOf(sdf.parse(serverDateTime).getTime());
                                    createTime = Convert.ToDateTime(tingli[tb_精神病随访记录.下次随访日期].ToString());
                                    date = Convert.ToDateTime(serverDateTime);
                                    //if (createTime.longValue() > date.longValue()) 
                                    if (createTime > date)
                                    {
                                        //String qdqxz = tingli.getQdqxz();
                                        //String wzd = tingli.getWzd();
                                        string qdqxz = tingli[tb_精神病随访记录.缺项].ToString();
                                        string wzd = tingli[tb_精神病随访记录.完整度].ToString();

                                        //tz.settCjrJingshen(qdqxz + "," + wzd);
                                        //tzExampleC.andDGrdabhEqualTo(tingli.getdGrdabh());
                                        grtzb[tb_健康档案_个人健康特征.精神疾病随访表] = qdqxz + "," + wzd;
                                        grtzb[tb_健康档案_个人健康特征.个人档案编号] = tingli[tb_精神病随访记录.个人档案编号].ToString();
                                    }
                                    else
                                    {
                                        //tz.settCjrJingshen("未建");
                                        //tzExampleC.andDGrdabhEqualTo(tingli.getdGrdabh());
                                        grtzb[tb_健康档案_个人健康特征.精神疾病随访表] = "未建";
                                        grtzb[tb_健康档案_个人健康特征.个人档案编号] = tingli[tb_精神病随访记录.个人档案编号].ToString();
                                    }
                                    //tzExampleC.andDGrdabhEqualTo(tingli.getdGrdabh());
                                    grtzb[tb_健康档案_个人健康特征.个人档案编号] = tingli[tb_精神病随访记录.个人档案编号].ToString();
                                }
                            }
                            catch //(NumberFormatException e) 
                            {
                                //e.printStackTrace();
                            }
                            //catch (ParseException e) 
                            //{
                            //    e.printStackTrace();
                            //}
                        }
                        //List listallsf = this.tcjrJingshenMapper.根据个人档案、随访日期!=thisyear号号查询Orderby下次随访日期(tcjrJingshen);
                        DataSet listallsf = dal.GetInfoByGrdabhAndNotYearOrderbyNextDate(grdabh, year);
                        //if ((listallsf != null) && (listallsf.size() > 0)) 
                        if (listallsf != null && listallsf.Tables.Count > 0 && listallsf.Tables[0].Rows.Count > 0)
                        {
                            //for (TcjrJingshen tingli : listallsf) {
                            foreach (DataRow tingli in listallsf.Tables[0].Rows)
                            {
                                //tcjrJsjbxxbcb.setdGrdabh(tingli.getdGrdabh());
                                tcjrJsjbxxbcb[tb_精神疾病信息补充表.个人档案编号] = tingli[tb_精神病随访记录.个人档案编号].ToString();

                                try
                                {
                                    //if ((tingli.getId().longValue() != Long.parseLong(id)) 
                                    //&& (sdf.parse(tingli.getcXcsfrq()).getTime() > sdf.parse(tcjrJsjbxxbcb.getjXcsfsj()).getTime())) {
                                    if (tingli[tb_精神病随访记录.ID].ToString() != id
                                        && Convert.ToDateTime(tingli[tb_精神病随访记录.下次随访日期].ToString()) > Convert.ToDateTime(tcjrJsjbxxbcb[tb_精神疾病信息补充表.下次随访时间].ToString()))
                                    {
                                        //tcjrJsjbxxbcb.setjXcsfsj(tingli.getcXcsfrq());
                                        tcjrJsjbxxbcb[tb_精神疾病信息补充表.下次随访时间] = tingli[tb_精神病随访记录.下次随访日期].ToString();
                                        //createTime = Long.valueOf(sdf.parse(tingli.getcXcsfrq()).getTime());
                                        //date = Long.valueOf(sdf.parse(serverDateTime).getTime());
                                        createTime = Convert.ToDateTime(tingli[tb_精神病随访记录.下次随访日期].ToString());
                                        date = Convert.ToDateTime(serverDateTime);

                                        //if (createTime.longValue() > date.longValue()) 
                                        if (createTime > date)
                                        {
                                            //String qdqxz = tingli.getQdqxz();
                                            //String wzd = tingli.getWzd();
                                            string qdqxz = tingli[tb_精神病随访记录.缺项].ToString();
                                            string wzd = tingli[tb_精神病随访记录.完整度].ToString();

                                            //tz.settCjrJingshen(qdqxz + "," + wzd);
                                            //tzExampleC.andDGrdabhEqualTo(tingli.getdGrdabh());
                                            grtzb[tb_健康档案_个人健康特征.精神疾病随访表] = qdqxz + "," + wzd;
                                            grtzb[tb_健康档案_个人健康特征.个人档案编号] = tingli[tb_精神病随访记录.个人档案编号].ToString();
                                        }
                                        else
                                        {
                                            //tz.settCjrJingshen("未建");
                                            //tzExampleC.andDGrdabhEqualTo(tingli.getdGrdabh());
                                            grtzb[tb_健康档案_个人健康特征.精神疾病随访表] = "未建";
                                            grtzb[tb_健康档案_个人健康特征.个人档案编号] = tingli[tb_精神病随访记录.个人档案编号].ToString();
                                        }
                                        //tzExampleC.andDGrdabhEqualTo(tingli.getdGrdabh());
                                        grtzb[tb_健康档案_个人健康特征.个人档案编号] = tingli[tb_精神病随访记录.个人档案编号].ToString();
                                    }
                                }
                                catch //(NumberFormatException e) 
                                {
                                    //e.printStackTrace();
                                }
                                //catch (ParseException e) 
                                //{
                                //    e.printStackTrace();
                                //}
                            }
                        }
                        //this.tdaJkdaGrjktzMapper.updateByExampleSelective(tz, tzExample);
                        //this.tcjrJsjbxxbcbMapper.updateByPrimaryKeySelective(tcjrJsjbxxbcb);
                        string bcbgrdabh = tcjrJsjbxxbcb[tb_精神疾病信息补充表.个人档案编号].ToString();
                        string xcsfsj = tcjrJsjbxxbcb[tb_精神疾病信息补充表.下次随访时间].ToString();
                        string tzgrdabh = grtzb[tb_健康档案_个人健康特征.个人档案编号].ToString();
                        string tzjsjb = grtzb[tb_健康档案_个人健康特征.精神疾病随访表].ToString();
                        dal.Update信息补充表And个人体征(bcbgrdabh, xcsfsj, tzgrdabh, tzjsjb);
                    }
                    //this.tcjrJingshenMapper.deleteByPrimaryKey(Long.valueOf(Long.parseLong(id)));
                    dal.DeleteByID(id);
                    //this.tcjryaopinMapper.deleteBycJrid(Long.valueOf(Long.parseLong(id)));

                    this.DeleteMDinfoByVisitID(id);
                }
                else //同一年的随访记录数只有一条的时候
                {
                    //this.tcjrJingshenMapper.deleteByPrimaryKey(Long.valueOf(Long.parseLong(id)));
                    dal.DeleteByID(id);
                    //this.tcjryaopinMapper.deleteBycJrid(Long.valueOf(Long.parseLong(id)));
                    this.DeleteMDinfoByVisitID(id);
                    //TcjrJsjbxxbcb tcjrJsjbxxbcb = new TcjrJsjbxxbcb();
                    //tcjrJsjbxxbcb.setdGrdabh(tcjrJingshen.getdGrdabh());

                    tcjrJsjbxxbcb[tb_精神疾病信息补充表.个人档案编号] = dsSelectedByID.Tables[0].Rows[0][0].ToString();
                    //List listallsf = this.tcjrJingshenMapper.根据个人档案、随访日期!=thisyear号号查询Orderby下次随访日期(tcjrJingshen);
                    DataSet listallsf = dal.GetInfoByGrdabhAndNotYearOrderbyNextDate(grdabh, year);
                    //if ((listallsf != null) && (listallsf.size() > 0)) 
                    if (listallsf != null && listallsf.Tables.Count > 0 && listallsf.Tables[0].Rows.Count > 0)
                    {
                        //tcjrJsjbxxbcb.setjXcsfsj(((TcjrJingshen)listallsf.get(0)).getcXcsfrq());
                        tcjrJsjbxxbcb[tb_精神疾病信息补充表.下次随访时间] = listallsf.Tables[0].Rows[0][tb_精神病随访记录.下次随访日期].ToString();
                        //if (Tools.str2Date(tcjrJsjbxxbcb.getjXcsfsj() + " 00:00:00").getTime() >= Tools.str2Date(serverDateTime).getTime())
                        if (Convert.ToDateTime(tcjrJsjbxxbcb[tb_精神疾病信息补充表.下次随访时间].ToString()) > Convert.ToDateTime(serverDateTime))
                        {
                            //tz.settCjrJingshen(((TcjrJingshen)listallsf.get(0)).getQdqxz() + "," + ((TcjrJingshen)listallsf.get(0)).getWzd());
                            grtzb[tb_健康档案_个人健康特征.精神疾病随访表] = listallsf.Tables[0].Rows[0][tb_精神病随访记录.缺项].ToString() + "," +
                                                                                listallsf.Tables[0].Rows[0][tb_精神病随访记录.完整度].ToString();
                        }
                        else
                        {
                            ///tz.settCjrJingshen("未建");
                            grtzb[tb_健康档案_个人健康特征.精神疾病随访表] = "未建";
                        }
                    }
                    else
                    {
                        //tcjrJsjbxxbcb.setjXcsfsj("");
                        tcjrJsjbxxbcb[tb_精神疾病信息补充表.下次随访时间] = "";
                        //tz.settCjrJingshen("未建");
                        grtzb[tb_健康档案_个人健康特征.精神疾病随访表] = "未建";
                    }

                    //tzExampleC.andDGrdabhEqualTo(((TcjrJingshen)listsf.get(0)).getdGrdabh());
                    grtzb[tb_健康档案_个人健康特征.个人档案编号] = dsSelectedByID.Tables[0].Rows[0][tb_精神病随访记录.个人档案编号].ToString();
                    //this.tdaJkdaGrjktzMapper.updateByExampleSelective(tz, tzExample);
                    //this.tcjrJsjbxxbcbMapper.updateByPrimaryKeySelective(tcjrJsjbxxbcb);

                    //dal.Update信息补充表And个人体征(tcjrJsjbxxbcb[tb_精神疾病信息补充表.个人档案编号].ToString(), tcjrJsjbxxbcb[tb_精神疾病信息补充表.下次随访时间].ToString(),
                    //                                grtzb[tb_健康档案_个人健康特征.个人档案编号].ToString(), grtzb[tb_健康档案_个人健康特征.精神疾病随访表].ToString());
                    string bcbgrdabh = tcjrJsjbxxbcb[tb_精神疾病信息补充表.个人档案编号].ToString();
                    string xcsfsj = tcjrJsjbxxbcb[tb_精神疾病信息补充表.下次随访时间].ToString();
                    string tzgrdabh = grtzb[tb_健康档案_个人健康特征.个人档案编号].ToString();
                    string tzjsjb = grtzb[tb_健康档案_个人健康特征.精神疾病随访表].ToString();
                    dal.Update信息补充表And个人体征(bcbgrdabh, xcsfsj, tzgrdabh, tzjsjb);
                }
                //}//////////////////循环内的实现

            }
            catch
            {

            }
        }
        #endregion

        public DataSet GetInfoBySuiFang(string docNo, string year,bool resetCurrent)
        {
            DataSet ds = new dal精神病随访记录(Loginer.CurrentUser).GetInfoBySuiFang(docNo, year);
            //this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }

        //Begin WXF 2018-11-01 | 19:29
        //根据档案号返回所有发生时间 
        public DataTable Get_发生时间(string grdah)
        {
            DataTable dt = new dal精神病随访记录(Loginer.CurrentUser).Get_发生时间(grdah);
            return dt;
        }
        //End
    }
}
