﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using AtomEHR.Models;
using AtomEHR.Bridge;
using AtomEHR.Common;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Server.DataAccess;

/*==========================================
 *   程序说明: 老年人随访的业务逻辑层
 *   作者姓名: ATOM
 *   创建日期: 2015/09/22 08:54:43
 *   最后修改: 2015/09/22 08:54:43
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Business
{
    /// <summary>
    /// bll老年人随访
    /// </summary>
    public class bll老年人随访 : bllBaseBusiness
    {
        /// <summary>
        /// 构造器
        /// </summary>
        public bll老年人随访()
        {
            _KeyFieldName = tb_老年人随访.__KeyName; //主键字段
            _SummaryTableName = tb_老年人随访.__TableName;//表名
        }

        /// <summary>
        ///根据单据号码取业务数据
        /// </summary>
        public override DataSet GetBusinessByKey(string keyValue, bool resetCurrent)
        {
            DataSet ds = new dal老年人随访(Loginer.CurrentUser).GetBusinessByKey(keyValue);
            this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }
        /// <summary>
        ///根据单据号码取业务数据
        /// </summary>
        public DataSet GetOneDataByKey(string docNo, string id, bool resetCurrent)
        {
            DataSet ds = new dal老年人随访(Loginer.CurrentUser).GetOneDataByKey(docNo, id);
            this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }
        /// <summary>
        /// 获取全部业务单据的数据
        /// </summary>
        /// <param name="docNo">个人档案编号</param>
        ///  <param name="id">id主键</param>
        /// <returns></returns>
        public System.Data.DataSet GetAllDataByKey(string docNo)
        {
            return new dal老年人随访(Loginer.CurrentUser).GetAllDataByKey(docNo);
        }
        /// <summary>
        ///删除单据
        /// </summary>
        public override bool Delete(string keyValue)
        {
            return new dal老年人随访(Loginer.CurrentUser).Delete(keyValue);
        }

        /// <summary>
        ///检查单号是否存在
        /// </summary>
        public bool CheckNoExists(string keyValue)
        {
            return new dal老年人随访(Loginer.CurrentUser).CheckNoExists(keyValue);
        }
        /// <summary>
        /// 判断是否存在 此随访日期 的随访
        /// </summary>
        /// <param name="docNo"></param>
        /// <param name="happenTime"></param>
        /// <returns></returns>
        public bool isExists(string docNo, string happenTime)
        {
            return new dal老年人随访(Loginer.CurrentUser).isExists(docNo, happenTime);
        }
        /// <summary>
        /// 获取本年度随访次数
        /// </summary>
        /// <param name="docNo">个人档案编号</param>
        /// <param name="happenTime">随访时间（年）</param>
        /// <returns></returns>
        public int getbndsfcs(string docNo, string happenTime)
        {
            return new dal老年人随访(Loginer.CurrentUser).getbndsfcs(docNo, happenTime);
        }
        /// <summary>
        ///保存数据
        /// </summary>
        public override SaveResult Save(DataSet saveData)
        {
            return new dal老年人随访(Loginer.CurrentUser).Update(saveData); //交给数据层处理
        }

        /// <summary>
        ///审核单据
        /// </summary>
        public override void ApprovalBusiness(DataRow summaryRow)
        {
            summaryRow[BusinessCommonFields.AppDate] = DateTime.Now;
            summaryRow[BusinessCommonFields.AppUser] = Loginer.CurrentUser.Account;
            summaryRow[BusinessCommonFields.FlagApp] = "Y";
            string key = ConvertEx.ToString(summaryRow[tb_老年人随访.__KeyName]);
            new dal老年人随访(Loginer.CurrentUser).ApprovalBusiness(key, "Y", Loginer.CurrentUser.Account, DateTime.Now);
        }

        /// <summary>
        ///新增一张业务单据
        /// </summary>
        public override void NewBusiness()
        {
            DataTable summaryTable = _CurrentBusiness.Tables[tb_老年人随访.__TableName];
            DataRow row = summaryTable.Rows.Add();
            //根据需要自行取消注释
            //row[tb_老年人随访.__KeyName] = "*自动生成*";
            //row[tb_老年人随访.创建人] = Loginer.CurrentUser.Account;
            //row[tb_老年人随访.创建时间] = DateTime.Now;
            //row[tb_老年人随访.修改人] = Loginer.CurrentUser.Account;
            //row[tb_老年人随访.修改时间] = DateTime.Now;
        }

        /// <summary>
        ///创建用于保存的临时数据
        /// </summary>
        public override DataSet CreateSaveData(DataSet currentBusiness, UpdateType currentType)
        {
            this.UpdateSummaryRowState(this.DataBindRow, currentType);

            //创建用于保存的临时数据,里面包含主表数据
            DataSet save = this.DoCreateTempData(currentBusiness, tb_老年人基本信息.__TableName);
            DataTable summary = save.Tables[0];

            //根据需要自行取消注释
            //summary.Rows[0][BusinessCommonFields.s修改人] = Loginer.CurrentUser.Account;
            //summary.Rows[0][BusinessCommonFields.s修改时间] = DateTime.Now;

            //this.UpdateSummaryRowState(currentBusiness.Tables[tb_老年人随访s.__TableName].Rows[0], currentType); //子表
            if (currentBusiness.Tables.Contains(tb_老年人随访.__TableName))
            {
                DataTable detail = currentBusiness.Tables[tb_老年人随访.__TableName].Copy();
                save.Tables.Add(detail); //加入明细数据 
            }
            if (currentBusiness.Tables.Contains(tb_健康档案_个人健康特征.__TableName))
            {
                DataTable detail = currentBusiness.Tables[tb_健康档案_个人健康特征.__TableName].Copy();
                save.Tables.Add(detail); //加入明细数据 
            }

            //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
            //save.Tables.Add(detail); //加入明细数据 

            return save;
        }

        /// <summary>
        ///查询数据
        /// </summary>
        public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
        {
            DataTable dt = new dal老年人随访(Loginer.CurrentUser).GetSummaryByParam(docNoFrom, docNoTo, docDateFrom, docDateTo);
            //this.SetNumericDefaultValue(ds); //设置预设值
            //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return dt;
        }

        /// <summary>
        ///获取报表数据
        /// </summary>
        public DataSet GetReportDataByLRSJ(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string type)
        {
            DataSet ds = new dal老年人随访(Loginer.CurrentUser).GetReportDataByLRSJ(DocNo机构, DocNo完整度, DateFrom, DateTo, type);
            this.SetNumericDefaultValue(ds); //设置预设值
            return ds;
        }
        public DataSet GetReportDataByDCSJ(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string type)
        {
            DataSet ds = new dal老年人随访(Loginer.CurrentUser).GetReportDataByDCSJ(DocNo机构, DocNo完整度, DateFrom, DateTo, type);
            this.SetNumericDefaultValue(ds); //设置预设值
            return ds;
        }
        #region Business Log

        /// <summary>
        /// 写入日志
        /// </summary>
        public override void WriteLog()
        {
            string key = this.DataBinder.Rows[0][tb_老年人随访.__KeyName].ToString();//取单据号码
            DataSet dsOriginal = this.GetBusinessByKey(key, false); //取保存前的原始数据, 用于保存日志时匹配数据.
            DataSet dsTemplate = this.CreateSaveData(this.CurrentBusiness, UpdateType.Modify); //创建用于保存的临时数据
            this.WriteLog(dsOriginal, dsTemplate);//保存日志      
        }

        /// <summary>
        /// 写入日志
        /// </summary>
        /// <param name="original">原始数据</param>
        /// <param name="changes">修改后的数据</param>
        public override void WriteLog(DataTable original, DataTable changes) { }

        /// <summary>
        /// 写入日志
        /// </summary>
        /// <param name="original">原始数据</param>
        /// <param name="changes">修改后的数据</param>
        public override void WriteLog(DataSet original, DataSet changes)
        {  //单独处理,即使错误,不向外抛出异常
            //if (_SaveVersionLog == false) return; //应用策略
            try
            {
                string logID = Guid.NewGuid().ToString().Replace("-", ""); //本次日志ID
                //SystemLog.WriteLog(logID, original.Tables[0], changes.Tables[0], tb_老年人随访.__TableName, tb_老年人随访.__KeyName, true); //主表
                //明细表的修改日志,系统不支持自动生成,请手工调整代码
                //SystemLog.WriteLog(logID, original.Tables[1], changes.Tables[1], tb_老年人随访.__TableName, tb_老年人随访.__KeyName, false);
            }
            catch
            {
                Msg.Warning("写入日志发生错误！");
            }
        }

        #endregion

        public SaveResult fun删除随访(string _id, string _docNo, string happenYear)
        {
            return new dal老年人随访(Loginer.CurrentUser).fun删除随访(_id, _docNo, happenYear);
        }
        /// <summary>
        /// 这是老年人随访的业务数据引用
        /// </summary>
        /// <param name="_docNo"></param>
        /// <param name="resetCurrent"></param>
        /// <returns></returns>
        public DataSet GetInfoByFangshi(string _docNo, string _date, bool resetCurrent)
        {
         
            DataSet ds = new dal老年人随访(Loginer.CurrentUser).GetInfoByFangshi(_docNo,_date);
            //this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }

        /// <summary>
        /// 老年人管理率
        /// </summary>
        /// <param name="DocNo机构"></param>
        /// <param name="DocNo完整度"></param>
        /// <param name="Type"></param>
        /// <returns></returns>
        public DataSet GetReportDataOfGLL(string DocNo机构, string DocNo完整度, string Type)
        {
            DataSet ds = new dal老年人随访(Loginer.CurrentUser).GetReportDataOfGLL(DocNo机构, DocNo完整度,Type);
            this.SetNumericDefaultValue(ds); //设置预设值
            return ds;
        }

        //Begin WXF 2018-11-01 | 19:29
        //根据档案号返回所有发生时间 
        public DataTable Get_发生时间(string grdah)
        {
            DataTable dt = new dal老年人随访(Loginer.CurrentUser).Get_发生时间(grdah);
            return dt;
        }
        //End

        //Begin WXF 2018-11-26 | 16:24
        //获取指定年份的评估数据(一年一条) 
        public DataTable Get_yearData(string recordNum, string year)
        {
            return new dal老年人随访(Loginer.CurrentUser).Get_yearData(recordNum,year);
        }
        //End
    }
}
