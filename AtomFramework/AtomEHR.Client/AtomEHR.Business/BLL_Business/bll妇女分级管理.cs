﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using AtomEHR.Models;
using AtomEHR.Bridge;
using AtomEHR.Common;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Server.DataAccess;
using AtomEHR.Interfaces;

/*==========================================
 *   程序说明: 妇女分级管理的业务逻辑层
 *   作者姓名: ATOM
 *   创建日期: 2018年8月22日
 *   最后修改: 2018年8月22日
 *   
 *   注: 
 *   版权所有 Copyright © . 2018
 *==========================================*/

namespace AtomEHR.Business
{
    /// <summary>
    /// bll妇女保健检查
    /// </summary>
    public class bll妇女分级管理 : bllBaseBusiness,IFuzzySearchSupportable
    {
        /// <summary>
        /// 构造器
        /// </summary>
        public bll妇女分级管理()
        {
            _KeyFieldName = tb_妇女分级管理.__KeyName; //主键字段
            _SummaryTableName = tb_妇女分级管理.__TableName;//表名
        }

        /// <summary>
        ///根据单据号码取业务数据
        /// </summary>
        public override DataSet GetBusinessByKey(string keyValue, bool resetCurrent)
        {
            DataSet ds = new dal妇女分级管理(Loginer.CurrentUser).GetBusinessByKey(keyValue);
            this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }

        /// <summary>
        ///删除单据
        /// </summary>
        public override bool Delete(string keyValue)
        {
            return new dal妇女分级管理(Loginer.CurrentUser).Delete(keyValue);
        }

        /// <summary>
        ///检查单号是否存在
        /// </summary>
        public bool CheckNoExists(string keyValue)
        {
            return new dal妇女分级管理(Loginer.CurrentUser).CheckNoExists(keyValue);
        }
        public bool CheckNoExistsByDate(string docNo, string date)
        {
            return new dal妇女分级管理(Loginer.CurrentUser).CheckNoExistsByDate(docNo,date);
        }

        /// <summary>
        ///保存数据
        /// </summary>
        public override SaveResult Save(DataSet saveData)
        {
            return new dal妇女分级管理(Loginer.CurrentUser).Update(saveData); //交给数据层处理
        }

        /// <summary>
        ///审核单据
        /// </summary>
        public override void ApprovalBusiness(DataRow summaryRow)
        {
            summaryRow[BusinessCommonFields.AppDate] = DateTime.Now;
            summaryRow[BusinessCommonFields.AppUser] = Loginer.CurrentUser.Account;
            summaryRow[BusinessCommonFields.FlagApp] = "Y";
            string key = ConvertEx.ToString(summaryRow[tb_妇女分级管理.__KeyName]);
            new dal妇女分级管理(Loginer.CurrentUser).ApprovalBusiness(key, "Y", Loginer.CurrentUser.Account, DateTime.Now);
        }

        /// <summary>
        ///新增一张业务单据
        /// </summary>
        public override void NewBusiness()
        {
            DataTable summaryTable = _CurrentBusiness.Tables[tb_妇女分级管理.__TableName];
            DataRow row = summaryTable.Rows.Add();
            //根据需要自行取消注释
            //row[tb_妇女分级管理.__KeyName] = "*自动生成*";
            row[tb_妇女分级管理.创建机构] = Loginer.CurrentUser.所属机构;
            row[tb_妇女分级管理.所属机构] = Loginer.CurrentUser.所属机构;
            row[tb_妇女分级管理.创建人] = Loginer.CurrentUser.用户编码;
            row[tb_妇女分级管理.创建时间] = base.ServiceDateTime;
            row[tb_妇女分级管理.修改人] = Loginer.CurrentUser.用户编码;
            row[tb_妇女分级管理.修改时间] = base.ServiceDateTime;
        }

        /// <summary>
        ///创建用于保存的临时数据
        /// </summary>
        public override DataSet CreateSaveData(DataSet currentBusiness, UpdateType currentType)
        {
            this.UpdateSummaryRowState(this.DataBindRow, currentType);

            //创建用于保存的临时数据,里面包含主表数据
            DataSet save = this.DoCreateTempData(currentBusiness, tb_妇女分级管理.__TableName);
            DataTable summary = save.Tables[0];
            //根据需要自行取消注释
            //summary.Rows[0][BusinessCommonFields.s修改人] = Loginer.CurrentUser.Account;
            //summary.Rows[0][BusinessCommonFields.s修改时间] = DateTime.Now;

            //this.UpdateSummaryRowState(currentBusiness.Tables[tb_妇女分级管理s.__TableName].Rows[0], currentType); //子表
            //DataTable detail = currentBusiness.Tables[tb_妇女分级管理s.__TableName].Copy();
            //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
            //save.Tables.Add(detail); //加入明细数据 

            return save;
        }

        /// <summary>
        ///查询数据
        /// </summary>
        public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
        {
            DataTable dt = new dal妇女分级管理(Loginer.CurrentUser).GetSummaryByParam(docNoFrom, docNoTo, docDateFrom, docDateTo);
            //this.SetNumericDefaultValue(ds); //设置预设值
            //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return dt;
        }

        /// <summary>
        ///获取报表数据
        /// </summary>
        public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
        {
            return null;
        }

        #region Business Log

        /// <summary>
        /// 写入日志
        /// </summary>
        public override void WriteLog()
        {
            string key = this.DataBinder.Rows[0][tb_妇女分级管理.__KeyName].ToString();//取单据号码
            DataSet dsOriginal = this.GetBusinessByKey(key, false); //取保存前的原始数据, 用于保存日志时匹配数据.
            DataSet dsTemplate = this.CreateSaveData(this.CurrentBusiness, UpdateType.Modify); //创建用于保存的临时数据
            this.WriteLog(dsOriginal, dsTemplate);//保存日志      
        }

        /// <summary>
        /// 写入日志
        /// </summary>
        /// <param name="original">原始数据</param>
        /// <param name="changes">修改后的数据</param>
        public override void WriteLog(DataTable original, DataTable changes) { }

        /// <summary>
        /// 写入日志
        /// </summary>
        /// <param name="original">原始数据</param>
        /// <param name="changes">修改后的数据</param>
        public override void WriteLog(DataSet original, DataSet changes)
        {  //单独处理,即使错误,不向外抛出异常
            //if (_SaveVersionLog == false) return; //应用策略
            try
            {
                string logID = Guid.NewGuid().ToString().Replace("-", ""); //本次日志ID
                //SystemLog.WriteLog(logID, original.Tables[0], changes.Tables[0], tb_妇女分级管理.__TableName, tb_妇女分级管理.__KeyName, true); //主表
                //明细表的修改日志,系统不支持自动生成,请手工调整代码
                //SystemLog.WriteLog(logID, original.Tables[1], changes.Tables[1], tb_妇女分级管理.__TableName, tb_妇女分级管理.__KeyName, false);
            }
            catch
            {
                Msg.Warning("写入日志发生错误！");
            }
        }

        #endregion

        public DataSet GetAllDataByKey(string _docNo)
        {
            return new dal妇女分级管理(Loginer.CurrentUser).GetAllDataByKey(_docNo);
        }

        public DataSet GetOneDataByKey(string _docNo, string _id, bool resetCurrent)
        {
            DataSet ds = new dal妇女分级管理(Loginer.CurrentUser).GetOneDataByKey(_docNo, _id);
            this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }

        /// <summary>
        /// 根据id删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteByID(string id)
        {
            return new dal妇女分级管理(Loginer.CurrentUser).DeleteByID(id);
        }

        #region 实现筛选接口
        public string FuzzySearchName
        {
            get { return "搜索个人档案"; }
        }

        /// <summary>
        /// 通用筛选器
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public DataTable FuzzySearch(string content)
        {
            return new dal妇女分级管理(Loginer.CurrentUser).F模糊查询(content);
        } 
        #endregion
    }
}
