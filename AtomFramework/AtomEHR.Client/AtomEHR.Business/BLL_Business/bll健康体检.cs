﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using AtomEHR.Models;
using AtomEHR.Bridge;
using AtomEHR.Common;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Server.DataAccess;

/*==========================================
 *   程序说明: 健康体检的业务逻辑层
 *   作者姓名: ATOM
 *   创建日期: 2015/08/20 08:49:00
 *   最后修改: 2015/08/20 08:49:00
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Business
{
    /// <summary>
    /// bll健康体检
    /// </summary>
    public class bll健康体检 : bllBaseBusiness
    {
        /// <summary>
        /// 构造器
        /// </summary>
        public bll健康体检()
        {
            _KeyFieldName = tb_健康体检.__KeyName; //主键字段
            _SummaryTableName = tb_健康体检.__TableName;//表名
        }

        /// <summary>
        ///根据单据号码取业务数据
        /// </summary>
        public override DataSet GetBusinessByKey(string keyValue, bool resetCurrent)
        {
            DataSet ds = new dal健康体检(Loginer.CurrentUser).GetBusinessByKey(keyValue);
            //this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }
        public DataSet GetUserValueByKey(string docNo, string date, bool resetCurrent)
        {
            DataSet ds = new dal健康体检(Loginer.CurrentUser).GetUserValueByKey(docNo, date, resetCurrent);
            // this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }
        /// <summary>
        ///删除单据
        /// </summary>
        public bool DeleteByDocnoAndDate(string docNo, string date ,string _ID)
        {
            return new dal健康体检(Loginer.CurrentUser).DeleteBYDocNoAndDate(docNo, date, _ID);
        }

        /// <summary>
        ///检查单号是否存在
        /// </summary>
        public bool CheckNoExists(string keyValue)
        {
            return new dal健康体检(Loginer.CurrentUser).CheckNoExists(keyValue);
        }

        /// <summary>
        ///保存数据
        /// </summary>
        public override SaveResult Save(DataSet saveData)
        {
            return new dal健康体检(Loginer.CurrentUser).Update(saveData); //交给数据层处理
        }

        /// <summary>
        ///审核单据
        /// </summary>
        public override void ApprovalBusiness(DataRow summaryRow)
        {
            summaryRow[BusinessCommonFields.AppDate] = DateTime.Now;
            summaryRow[BusinessCommonFields.AppUser] = Loginer.CurrentUser.Account;
            summaryRow[BusinessCommonFields.FlagApp] = "Y";
            string key = ConvertEx.ToString(summaryRow[tb_健康体检.__KeyName]);
            new dal健康体检(Loginer.CurrentUser).ApprovalBusiness(key, "Y", Loginer.CurrentUser.Account, DateTime.Now);
        }

        /// <summary>
        ///新增一张业务单据
        /// </summary>
        public override void NewBusiness()
        {
            DataTable table健康体检 = _CurrentBusiness.Tables[tb_健康体检.__TableName];
            DataRow row = table健康体检.Rows.Add();
            //根据需要自行取消注释
            //row[tb_健康体检.__KeyName] = "*自动生成*";
            //row[tb_健康体检.创建人] = Loginer.CurrentUser.Account;
            //row[tb_健康体检.创建时间] = DateTime.Now;
            //row[tb_健康体检.修改人] = Loginer.CurrentUser.Account;
            //row[tb_健康体检.修改时间] = DateTime.Now;

            //tb_健康档案_个人健康特征 表已经存在相关信息，只需更新即可
            //DataTable table个人健康特征 = _CurrentBusiness.Tables[tb_健康档案_个人健康特征.__TableName];
            //DataRow row2 = table个人健康特征.Rows.Add();
            //根据需要自行取消注释
            //row[tb_健康体检.__KeyName] = "*自动生成*";
            //row2[tb_健康档案_个人健康特征.创建人] = Loginer.CurrentUser.Account;
            //row2[tb_健康档案_个人健康特征.创建时间] = DateTime.Now;
            //row2[tb_健康档案_个人健康特征.修改人] = Loginer.CurrentUser.Account;
            //row2[tb_健康档案_个人健康特征.修改时间] = DateTime.Now;
        }

        /// <summary>
        ///创建用于保存的临时数据
        /// </summary>
        public override DataSet CreateSaveData(DataSet currentBusiness, UpdateType currentType)
        {
            this.UpdateSummaryRowState(this.DataBindRow, currentType);

            //创建用于保存的临时数据,里面包含主表数据
            DataSet save = this.DoCreateTempData(currentBusiness, tb_健康体检.__TableName);
            DataTable summary = save.Tables[0];
            //根据需要自行取消注释
            if (currentType == UpdateType.Add)
            {
                //summary.Rows[0][BusinessCommonFields.s创建人] = Loginer.CurrentUser.Account;
                //summary.Rows[0][BusinessCommonFields.s创建时间] = DateTime.Now;
                //summary.Rows[0][tb_健康体检.创建人] = Loginer.CurrentUser.Account;
                //summary.Rows[0][tb_健康体检.创建时间] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                //summary.Rows[0][tb_健康体检.创建机构] = Loginer.CurrentUser.所属机构;
            }
            //summary.Rows[0][BusinessCommonFields.s修改人] = Loginer.CurrentUser.Account;
            //summary.Rows[0][BusinessCommonFields.s修改时间] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            this.UpdateSummaryRowState(currentBusiness.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0], UpdateType.Modify); //子表
            DataTable detail = currentBusiness.Tables[tb_健康档案_个人健康特征.__TableName].Copy();
            save.Tables.Add(detail); //加入明细数据 
            if (currentBusiness.Tables["住院史"].Rows.Count >= 1)
            {
                //this.UpdateSummaryRowState(currentBusiness.Tables["住院史"].Rows[0], currentType); //子表
                DataTable detail4 = currentBusiness.Tables["住院史"].Copy();
                //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                save.Tables.Add(detail4); //加入明细数据 
            }
            if (currentBusiness.Tables["家庭病床史"].Rows.Count >= 1)
            {
                //this.UpdateSummaryRowState(currentBusiness.Tables["家庭病床史"].Rows[0], currentType); //子表
                DataTable detail4 = currentBusiness.Tables["家庭病床史"].Copy();
                //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                save.Tables.Add(detail4); //加入明细数据 
            }
            if (currentBusiness.Tables[tb_健康体检_用药情况表.__TableName].Rows.Count >= 1)
            {
                //this.UpdateSummaryRowState(currentBusiness.Tables[tb_健康体检_用药情况表.__TableName].Rows[0], currentType); //子表
                DataTable detail4 = currentBusiness.Tables[tb_健康体检_用药情况表.__TableName].Copy();
                //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                save.Tables.Add(detail4); //加入明细数据 
            }
            if (currentBusiness.Tables[tb_健康体检_非免疫规划预防接种史.__TableName].Rows.Count >= 1)
            {
                //this.UpdateSummaryRowState(currentBusiness.Tables[tb_健康体检_非免疫规划预防接种史.__TableName].Rows[0], currentType); //子表
                DataTable detail4 = currentBusiness.Tables[tb_健康体检_非免疫规划预防接种史.__TableName].Copy();
                //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                save.Tables.Add(detail4); //加入明细数据 
            }
            if (currentBusiness.Tables[tb_MXB高血压管理卡.__TableName].Rows.Count >= 1)
            {
                //this.UpdateSummaryRowState(currentBusiness.Tables[tb_健康体检_非免疫规划预防接种史.__TableName].Rows[0], currentType); //子表
                DataTable detail4 = currentBusiness.Tables[tb_MXB高血压管理卡.__TableName].Copy();
                //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                save.Tables.Add(detail4); //加入明细数据 
            } 
            if (currentBusiness.Tables[tb_MXB慢病基础信息表.__TableName].Rows.Count >= 1)
            {
                //this.UpdateSummaryRowState(currentBusiness.Tables[tb_健康体检_非免疫规划预防接种史.__TableName].Rows[0], currentType); //子表
                DataTable detail4 = currentBusiness.Tables[tb_MXB慢病基础信息表.__TableName].Copy();
                //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                save.Tables.Add(detail4); //加入明细数据 
            }
            if (currentBusiness.Tables[tb_MXB糖尿病管理卡.__TableName].Rows.Count >= 1)
            {
                //this.UpdateSummaryRowState(currentBusiness.Tables[tb_健康体检_非免疫规划预防接种史.__TableName].Rows[0], currentType); //子表
                DataTable detail4 = currentBusiness.Tables[tb_MXB糖尿病管理卡.__TableName].Copy();
                //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                save.Tables.Add(detail4); //加入明细数据 
            }
            if (currentBusiness.Tables[tb_健康档案_既往病史.__TableName].Rows.Count >= 1)
            {
                //this.UpdateSummaryRowState(currentBusiness.Tables[tb_健康体检_非免疫规划预防接种史.__TableName].Rows[0], currentType); //子表
                DataTable detail4 = currentBusiness.Tables[tb_健康档案_既往病史.__TableName].Copy();
                //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                save.Tables.Add(detail4); //加入明细数据 
            }
            if (currentBusiness.Tables[tb_健康档案_健康状态.__TableName].Rows.Count >= 1)
            {
                //this.UpdateSummaryRowState(currentBusiness.Tables[tb_健康体检_tb_健康档案_健康状态.__TableName].Rows[0], currentType); //子表
                DataTable detail4 = currentBusiness.Tables[tb_健康档案_健康状态.__TableName].Copy();
                //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
                save.Tables.Add(detail4); //加入明细数据 
            }
            return save;
        }

        /// <summary>
        ///查询数据
        /// </summary>
        public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
        {
            DataTable dt = new dal健康体检(Loginer.CurrentUser).GetSummaryByParam(docNoFrom, docNoTo, docDateFrom, docDateTo);
            //this.SetNumericDefaultValue(ds); //设置预设值
            //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return dt;
        }
        public DataTable GetSummaryByParam(string strWhere)
        {
            DataTable dt = new dal健康体检(Loginer.CurrentUser).GetSummaryByParam(strWhere);
            //this.SetNumericDefaultValue(ds); //设置预设值
            //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return dt;
        }
        /// <summary>
        ///获取报表数据
        /// </summary>
        public DataSet GetReportDataByLRSJ(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string type)
        {
            DataSet ds = new dal健康体检(Loginer.CurrentUser).GetReportDataByLRSJ(DocNo机构, DocNo完整度, DateFrom, DateTo, type);
            //this.SetNumericDefaultValue(ds); //设置预设值
            return ds;
        }
        public DataSet GetReportDataByDCSJ(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string type)
        {
            DataSet ds = new dal健康体检(Loginer.CurrentUser).GetReportDataByDCSJ(DocNo机构, DocNo完整度, DateFrom, DateTo, type);
            //this.SetNumericDefaultValue(ds); //设置预设值
            return ds;
        }
        
        #region Business Log

        /// <summary>
        /// 写入日志
        /// </summary>
        public override void WriteLog()
        {
            string key = this.DataBinder.Rows[0][tb_健康体检.__KeyName].ToString();//取单据号码
            DataSet dsOriginal = this.GetBusinessByKey(key, false); //取保存前的原始数据, 用于保存日志时匹配数据.
            DataSet dsTemplate = this.CreateSaveData(this.CurrentBusiness, UpdateType.Modify); //创建用于保存的临时数据
            this.WriteLog(dsOriginal, dsTemplate);//保存日志      
        }

        /// <summary>
        /// 写入日志
        /// </summary>
        /// <param name="original">原始数据</param>
        /// <param name="changes">修改后的数据</param>
        public override void WriteLog(DataTable original, DataTable changes) { }

        /// <summary>
        /// 写入日志
        /// </summary>
        /// <param name="original">原始数据</param>
        /// <param name="changes">修改后的数据</param>
        public override void WriteLog(DataSet original, DataSet changes)
        {  //单独处理,即使错误,不向外抛出异常
            //if (_SaveVersionLog == false) return; //应用策略
            try
            {
                string logID = Guid.NewGuid().ToString().Replace("-", ""); //本次日志ID
                //SystemLog.WriteLog(logID, original.Tables[0], changes.Tables[0], tb_健康体检.__TableName, tb_健康体检.__KeyName, true); //主表
                //明细表的修改日志,系统不支持自动生成,请手工调整代码
                //SystemLog.WriteLog(logID, original.Tables[1], changes.Tables[1], tb_健康体检.__TableName, tb_健康体检.__KeyName, false);
            }
            catch
            {
                Msg.Warning("写入日志发生错误！");
            }
        }

        #endregion

        public DataSet GetOneDataByKey(string docNo, bool resetCurrent)
        {
            DataSet ds = new dal健康体检(Loginer.CurrentUser).GetOneDataByKey(docNo);
            //this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }

        public override bool Delete(string keyValue)
        {
            return true;
        }

        public DataSet GetReportDataDetail(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string type, string searchType, string SearchBy)
        {
            DataSet ds = new dal健康体检(Loginer.CurrentUser).GetReportDataDetail(DocNo机构, DocNo完整度, DateFrom, DateTo, type,searchType, SearchBy);
            //this.SetNumericDefaultValue(ds); //设置预设值
            return ds;
        }

        /// <summary>
        /// 打印体检报表所用的方法
        /// </summary>
        /// <param name="docNo">个人档案编号</param>
        /// <param name="date">体检日期</param>
        /// <returns></returns>
        public DataSet GetReportDataByKey(string docNo, string date)
        {
            DataSet ds = new dal健康体检(Loginer.CurrentUser).GetReportDataByKey(docNo, date);
            // this.SetNumericDefaultValue(ds); //设置预设值
            return ds;
        }

        /// <summary>
        /// 获取复核情况 2017年10月25日 yufh 添加，同时添加手机接口
        /// </summary>
        /// <param name="DocNo机构"></param>
        /// <returns></returns>
        public DataSet GetReportDataByFH(string DocNo机构, string DateFrom, string DateTo)
        {
            DataSet ds = new dal健康体检(Loginer.CurrentUser).GetReportDataByFH(DocNo机构, DateFrom, DateTo);
            // this.SetNumericDefaultValue(ds); //设置预设值
            return ds;
        }

        //Begin WXF 2018-11-01 | 19:29
        //根据档案号返回所有发生时间 
        public DataTable Get_发生时间(string grdah)
        {
            DataTable dt = new dal健康体检(Loginer.CurrentUser).Get_发生时间(grdah);
            return dt;
        }
        //End


        //Begin WXF 2018-11-04 | 02:25
        //根据档案号与体检日期获取一条数据
        public DataRow Get_One体检(string grdah, string str_CreateDT)
        {
            DataRow dr = new dal健康体检(Loginer.CurrentUser).Get_One体检(grdah, str_CreateDT);
            return dr;
        }
        //End

        public DataTable Get_图像采集(string grdah, string str_date)
        {
            DataTable dt = new dal健康体检(Loginer.CurrentUser).Get_图像采集(grdah, str_date);
            return dt;
        }

        public DataTable Get_B超报告(string grdah, string str_date)
        {
            DataTable dt = new dal健康体检(Loginer.CurrentUser).Get_B超报告(grdah, str_date);
            return dt;
        }
											 
    }
}
