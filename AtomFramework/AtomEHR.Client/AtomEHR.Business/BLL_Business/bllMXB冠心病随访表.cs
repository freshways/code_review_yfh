﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using AtomEHR.Bridge;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Business.BLL_Business;
using AtomEHR.Common;
using AtomEHR.Models;
using AtomEHR.Server.DataAccess;
using AtomEHR.Interfaces.Interfaces_Bridge;

/*==========================================
 *   程序说明: MXB冠心病随访表的业务逻辑层
 *   作者姓名: ATOM
 *   创建日期: 2015-12-02 04:22:14
 *   最后修改: 2015-12-02 04:22:14
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Business
{
    /// <summary>
    /// bllMXB冠心病随访表
    /// </summary>
    public class bllMXB冠心病随访表 : bllBaseBusiness
    {
         /// <summary>
         /// 构造器
         /// </summary>
         public bllMXB冠心病随访表()
         {
             _KeyFieldName = tb_MXB冠心病随访表.__KeyName; //主键字段
             _SummaryTableName = tb_MXB冠心病随访表.__TableName;//表名
         }

          /// <summary>
          ///根据单据号码取业务数据
          /// </summary>
          public override DataSet GetBusinessByKey(string keyValue, bool resetCurrent)
          {
              DataSet ds = new dalMXB冠心病随访表(Loginer.CurrentUser).GetBusinessByKey(keyValue); 
              //this.SetNumericDefaultValue(ds); //设置预设值
              if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return ds;
           }

          /// <summary>
          /// 获取一个修改的数据源
          /// </summary>
          public DataSet GetBusinessByKeyEdit(string keyValue, bool resetCurrent)
          {
              DataSet ds = new dalMXB冠心病随访表(Loginer.CurrentUser).GetBusinessByID(keyValue);
              //this.SetNumericDefaultValue(ds); //设置预设值
              if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return ds;
          }

          /// <summary>
          ///删除单据
          /// </summary>
          public override bool Delete(string keyValue)
          {
              return new dalMXB冠心病随访表(Loginer.CurrentUser).Delete(keyValue);
          }

          /// <summary>
          ///检查单号是否存在
          /// </summary>
          public bool CheckNoExists(string keyValue)
          {
              return new dalMXB冠心病随访表(Loginer.CurrentUser).CheckNoExists(keyValue);
          }

          /// <summary>
          ///保存数据
          /// </summary>
          public override SaveResult Save(DataSet saveData)
          {
              return new dalMXB冠心病随访表(Loginer.CurrentUser).Update(saveData); //交给数据层处理
          }

          /// <summary>
          /// 更新个人健康特征-完整度
          /// </summary>
          /// <param name="s个人档案编号"></param>
          public void Set个人健康特征(string s个人档案编号)
          {
              new dalMXB冠心病随访表(Loginer.CurrentUser).Update个人健康特征(s个人档案编号);
          }
        
          /// <summary>
          ///新增一张业务单据
          /// </summary>
          public override void NewBusiness()
          {
              DataTable summaryTable = _CurrentBusiness.Tables[tb_MXB冠心病随访表.__TableName];
              DataRow row = summaryTable.Rows.Add();     
              //根据需要自行取消注释
              //row[tb_MXB冠心病随访表.__KeyName] = "*自动生成*";
              row[tb_MXB冠心病随访表.创建人] = Loginer.CurrentUser.用户编码;
              row[tb_MXB冠心病随访表.创建时间] = base.ServiceDateTime;
              row[tb_MXB冠心病随访表.修改人] = Loginer.CurrentUser.用户编码;
              row[tb_MXB冠心病随访表.修改时间] = base.ServiceDateTime;
              row[tb_MXB冠心病随访表.所属机构] = Loginer.CurrentUser.所属机构;
              row[tb_MXB冠心病随访表.创建机构] = Loginer.CurrentUser.所属机构;
              //扩展-默认值
              row[tb_MXB冠心病随访表.随访方式] = "1"; //门诊
              row[tb_MXB冠心病随访表.目前症状] = "0"; //无
              row[tb_MXB冠心病随访表.药物副作用] = "1"; //无
              row[tb_MXB冠心病随访表.降压药] = "2"; //无
              row[tb_MXB冠心病随访表.摄盐情况2] = "1"; //轻
              row[tb_MXB冠心病随访表.运动频率2] = "5"; //5次
              row[tb_MXB冠心病随访表.运动持续时间2] = "30"; //30分钟
              row[tb_MXB冠心病随访表.心理调整] = "1"; //良好
              row[tb_MXB冠心病随访表.随访医生] = Loginer.CurrentUser.AccountName; //默认随访医生为当前登陆人员
              #region 慢性病随访表设置默认值
              row[tb_MXB冠心病随访表.吸烟数量] = "0";
              row[tb_MXB冠心病随访表.吸烟数量2] = "0";
              row[tb_MXB冠心病随访表.饮酒数量] = "0";
              row[tb_MXB冠心病随访表.饮酒数量2] = "0";
              row[tb_MXB冠心病随访表.摄盐情况] = "1"; //轻
              row[tb_MXB冠心病随访表.遵医行为] = "1";//良好
              row[tb_MXB冠心病随访表.辅助检查] = "正常";//正常
              row[tb_MXB冠心病随访表.非药物治疗措施] = "1,4,6";
              #endregion
           }

          /// <summary>
          ///创建用于保存的临时数据
          /// </summary>
          public override DataSet CreateSaveData(DataSet currentBusiness, UpdateType currentType)
          {
              this.UpdateSummaryRowState(this.DataBindRow, currentType);

              //创建用于保存的临时数据,里面包含主表数据
              DataSet save = this.DoCreateTempData(currentBusiness, tb_MXB冠心病随访表.__TableName);
              DataTable summary = save.Tables[0];
              //根据需要自行取消注释
              summary.Rows[0][BusinessCommonFields.s修改人] = Loginer.CurrentUser.用户编码;
              summary.Rows[0][BusinessCommonFields.s修改时间] = base.ServiceDateTime;

              DataTable detail = currentBusiness.Tables[tb_MXB冠心病随访表_用药情况.__TableName].Copy();
              //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
              save.Tables.Add(detail); //加入明细数据 

              return save; 
          }

          /// <summary>
          ///查询数据
          /// </summary>
          public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
          {
              DataTable dt = new dalMXB冠心病随访表(Loginer.CurrentUser).GetSummaryByParam(docNoFrom, docNoTo, docDateFrom, docDateTo);
              //this.SetNumericDefaultValue(ds); //设置预设值
              //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return dt;
          }

          /// <summary>
          ///获取报表数据
          /// </summary>
          public DataSet GetReportData(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string type, string DateType)
          {
              DataSet ds = new dalMXB冠心病随访表(Loginer.CurrentUser).GetReportData(DocNo机构, DocNo完整度, DateFrom, DateTo, type,  DateType);
              this.SetNumericDefaultValue(ds); //设置预设值
              return ds;
          }

          #region Business Log

          /// <summary>
          /// 写入日志
          /// </summary>
          public override void WriteLog()
          {
              string key = this.DataBinder.Rows[0][tb_MXB冠心病随访表.__KeyName].ToString();//取单据号码
              DataSet dsOriginal = this.GetBusinessByKey(key, false); //取保存前的原始数据, 用于保存日志时匹配数据.
              DataSet dsTemplate = this.CreateSaveData(this.CurrentBusiness, UpdateType.Modify); //创建用于保存的临时数据
              this.WriteLog(dsOriginal, dsTemplate);//保存日志      
          }

          /// <summary>
          /// 写入日志
          /// </summary>
          /// <param name="original">原始数据</param>
          /// <param name="changes">修改后的数据</param>
          public override void WriteLog(DataTable original, DataTable changes) { }

          /// <summary>
          /// 写入日志
          /// </summary>
          /// <param name="original">原始数据</param>
          /// <param name="changes">修改后的数据</param>
          public override void WriteLog(DataSet original, DataSet changes)
          {  //单独处理,即使错误,不向外抛出异常
              //if (_SaveVersionLog == false) return; //应用策略
              try
              {
                  string logID = Guid.NewGuid().ToString().Replace("-", ""); //本次日志ID

                  IBridge_EditLogHistory SystemLog = bllBusinessLog.CreateEditLogHistoryBridge();//初始化日志方法

                  SystemLog.WriteLog(logID, original.Tables[0], changes.Tables[0], tb_MXB冠心病随访表.__TableName, tb_MXB冠心病随访表.__KeyName, true); //主表
                  //明细表的修改日志,系统不支持自动生成,请手工调整代码
                  //SystemLog.WriteLog(logID, original.Tables[1], changes.Tables[1], tb_MXB冠心病随访表.__TableName, tb_MXB冠心病随访表.__KeyName, false);
              }
              catch
              {
                  Msg.Warning("写入日志发生错误！");
              }
          }

          #endregion

        /// <summary>
        /// 冠心病用药情况
        /// </summary>
        /// <param name="docNo"></param>
        /// <param name="year"></param>
        /// <param name="resetCurrent"></param>
        /// <returns></returns>
          public DataSet GetInfoByYY(string docNo, string year, bool resetCurrent)
          {
              DataSet ds = new dalMXB冠心病随访表(Loginer.CurrentUser).GetInfoByYY(docNo, year);
              //this.SetNumericDefaultValue(ds); //设置预设值
              if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return ds;
          }
        /// <summary>
        /// 冠心病随访记录
        /// </summary>
        /// <param name="docNo"></param>
        /// <param name="dates"></param>
        /// <param name="resetCurrent"></param>
        /// <returns></returns>
          public DataSet GetInfoByGXB(string docNo, List<string> dates, bool resetCurrent)
          {
              DataSet ds = new dalMXB冠心病随访表(Loginer.CurrentUser).GetInfoByGXB(docNo, dates);
              //this.SetNumericDefaultValue(ds); //设置预设值
              if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return ds;
          }
        /// <summary>
        /// 获取冠心病随访日期
        /// </summary>
        /// <param name="docNo"></param>
        /// <param name="year"></param>
        /// <param name="resetCurrent"></param>
        /// <returns></returns>
          public DataSet GetInfoByDate(string docNo, string year, bool resetCurrent)
          {
              DataSet ds = new dalMXB冠心病随访表(Loginer.CurrentUser).GetInfoByDate(docNo, year);
              //this.SetNumericDefaultValue(ds); //设置预设值
              if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return ds;
          }

        //高血压患者
          public bool Check高血压随访添加(string s个人档案编号, string s随访日期)
          {
              return new dalMXB冠心病随访表(Loginer.CurrentUser).Check添加高血压随访(s个人档案编号, s随访日期);
          }

          //糖尿病患者
          public bool Check糖尿病随访添加(string s个人档案编号, string s随访日期)
          {
              return new dalMXB冠心病随访表(Loginer.CurrentUser).Check添加糖尿病随访(s个人档案编号, s随访日期);
          }

          //脑卒中患者
          public bool Check脑卒中随访添加(string s个人档案编号, string s随访日期)
          {
              return new dalMXB冠心病随访表(Loginer.CurrentUser).Check添加脑卒中随访(s个人档案编号, s随访日期);
          }

          //Begin WXF 2018-11-02 | 11:12
          //根据档案号返回所有发生时间
          public DataTable Get_发生时间(string grdah)
          {
              DataTable dt = new dalMXB冠心病随访表(Loginer.CurrentUser).Get_发生时间(grdah);
              return dt;
          }
        //End
         
    }
}
