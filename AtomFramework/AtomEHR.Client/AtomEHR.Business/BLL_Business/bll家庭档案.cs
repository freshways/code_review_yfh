﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using AtomEHR.Models;
using AtomEHR.Bridge;
using AtomEHR.Common;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Server.DataAccess;

/*==========================================
 *   程序说明: 家庭档案的业务逻辑层
 *   作者姓名: ATOM
 *   创建日期: 2015/08/20 11:07:10
 *   最后修改: 2015/08/20 11:07:10
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Business
{
    /// <summary>
    /// bll家庭档案
    /// </summary>
    public class bll家庭档案 : bllBaseBusiness
    {
        /// <summary>
        /// 构造器
        /// </summary>
        public bll家庭档案()
        {
            _KeyFieldName = tb_家庭档案.__KeyName; //主键字段
            _SummaryTableName = tb_家庭档案.__TableName;//表名
        }

        /// <summary>
        ///根据单据号码取业务数据
        /// </summary>
        public override DataSet GetBusinessByKey(string keyValue, bool resetCurrent)
        {
            DataSet ds = new dal家庭档案(Loginer.CurrentUser).GetBusinessByKey(keyValue);
            this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }


        /// <summary>
        ///删除单据
        /// </summary>
        public override bool Delete(string keyValue)
        {
            return new dal家庭档案(Loginer.CurrentUser).Delete(keyValue);
        }

        /// <summary>
        ///检查单号是否存在
        /// </summary>
        public bool CheckNoExists(string keyValue)
        {
            return new dal家庭档案(Loginer.CurrentUser).CheckNoExists(keyValue);
        }

        /// <summary>
        ///保存数据
        /// </summary>
        public override SaveResult Save(DataSet saveData)
        {
            return new dal家庭档案(Loginer.CurrentUser).Update(saveData); //交给数据层处理
        }

        /// <summary>
        ///审核单据
        /// </summary>
        public override void ApprovalBusiness(DataRow summaryRow)
        {
            summaryRow[BusinessCommonFields.AppDate] = DateTime.Now;
            summaryRow[BusinessCommonFields.AppUser] = Loginer.CurrentUser.Account;
            summaryRow[BusinessCommonFields.FlagApp] = "Y";
            string key = ConvertEx.ToString(summaryRow[tb_家庭档案.__KeyName]);
            new dal家庭档案(Loginer.CurrentUser).ApprovalBusiness(key, "Y", Loginer.CurrentUser.Account, DateTime.Now);
        }

        /// <summary>
        ///新增一张业务单据
        /// </summary>
        public override void NewBusiness()
        {
            DataTable summaryTable = _CurrentBusiness.Tables[tb_家庭档案.__TableName];
            DataRow row = summaryTable.Rows.Add();
            //根据需要自行取消注释
            row[tb_家庭档案.__KeyName] = "*自动生成*";
            row[tb_家庭档案.创建人] = Loginer.CurrentUser.Account;
            row[tb_家庭档案.创建时间] = DateTime.Now;
            row[tb_家庭档案.修改人] = Loginer.CurrentUser.Account;
            row[tb_家庭档案.修改时间] = DateTime.Now;
        }

        /// <summary>
        ///创建用于保存的临时数据
        /// </summary>
        public override DataSet CreateSaveData(DataSet currentBusiness, UpdateType currentType)
        {
            this.UpdateSummaryRowState(this.DataBindRow, currentType);

            //创建用于保存的临时数据,里面包含主表数据
            DataSet save = this.DoCreateTempData(currentBusiness, tb_家庭档案.__TableName);
            DataTable summary = save.Tables[0];
            //根据需要自行取消注释
            //summary.Rows[0][BusinessCommonFields.s修改人] = Loginer.CurrentUser.Account;
            //summary.Rows[0][BusinessCommonFields.s修改时间] = DateTime.Now;

            //this.UpdateSummaryRowState(currentBusiness.Tables[tb_家庭档案s.__TableName].Rows[0], currentType); //子表
            DataTable detail = currentBusiness.Tables[tb_健康档案.__TableName].Copy();
            //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
            save.Tables.Add(detail); //加入明细数据 

            DataTable detail2 = currentBusiness.Tables[tb_健康档案_个人健康特征.__TableName].Copy();
            //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
            save.Tables.Add(detail2); //加入明细数据 

            return save;
        }
        /// <summary>
        /// 注销用户 更新与户主关系和家庭档案编号字段为''即可
        /// </summary>
        /// <param name="docNo"></param>
        /// <returns></returns>
        public bool fun注销(string 个人档案编号, string 家庭档案编号, string 与户主关系, int 家庭成员数量)
        {
            return new dal家庭档案(Loginer.CurrentUser).fun注销(个人档案编号, 家庭档案编号, 与户主关系, 家庭成员数量);
        }
        /// <summary>
        /// 家庭成员转出功能
        /// </summary>
        /// <param name="yhzgx">与户主关系</param>
        /// <param name="jtdabh">转入的家庭档案编号</param>
        /// <param name="yjtdabh">原家庭档案编号</param>
        /// <param name="grdabh">个人档案编号</param>
        /// <returns></returns>
        public bool fun转出(string yhzgx, string jtdabh, string yjtdabh, string grdabh)
        {
            return new dal家庭档案(Loginer.CurrentUser).fun转出(yhzgx, jtdabh, yjtdabh, grdabh);
        }
        /// <summary>
        ///查询数据
        /// </summary>
        public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
        {
            DataTable dt = new dal家庭档案(Loginer.CurrentUser).GetSummaryByParam(docNoFrom, docNoTo, docDateFrom, docDateTo);
            //this.SetNumericDefaultValue(ds); //设置预设值
            //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return dt;
        }

        /// <summary>
        ///获取报表数据
        /// </summary>
        public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
        {
            return null;
        }

        #region Business Log

        /// <summary>
        /// 写入日志
        /// </summary>
        public override void WriteLog()
        {
            string key = this.DataBinder.Rows[0][tb_家庭档案.__KeyName].ToString();//取单据号码
            DataSet dsOriginal = this.GetBusinessByKey(key, false); //取保存前的原始数据, 用于保存日志时匹配数据.
            DataSet dsTemplate = this.CreateSaveData(this.CurrentBusiness, UpdateType.Modify); //创建用于保存的临时数据
            this.WriteLog(dsOriginal, dsTemplate);//保存日志      
        }

        /// <summary>
        /// 写入日志
        /// </summary>
        /// <param name="original">原始数据</param>
        /// <param name="changes">修改后的数据</param>
        public override void WriteLog(DataTable original, DataTable changes) { }

        /// <summary>
        /// 写入日志
        /// </summary>
        /// <param name="original">原始数据</param>
        /// <param name="changes">修改后的数据</param>
        public override void WriteLog(DataSet original, DataSet changes)
        {  //单独处理,即使错误,不向外抛出异常
            //if (_SaveVersionLog == false) return; //应用策略
            try
            {
                string logID = Guid.NewGuid().ToString().Replace("-", ""); //本次日志ID
                //SystemLog.WriteLog(logID, original.Tables[0], changes.Tables[0], tb_家庭档案.__TableName, tb_家庭档案.__KeyName, true); //主表
                //明细表的修改日志,系统不支持自动生成,请手工调整代码
                //SystemLog.WriteLog(logID, original.Tables[1], changes.Tables[1], tb_家庭档案.__TableName, tb_家庭档案.__KeyName, false);
            }
            catch
            {
                Msg.Warning("写入日志发生错误！");
            }
        }

        #endregion
        /// <summary>
        /// 家庭转出 页面查询方法
        /// </summary>
        /// <param name="flag">查询条件cbo</param>
        /// <param name="txt">查询条件txt</param>
        /// <param name="rgid">所属机构</param>
        /// <param name="_jtdabh">家庭档案编号</param>
        /// <returns></returns>
        public DataTable selectRkxzlByHzxx(string flag, string txt, string rgid, string _jtdabh)
        {
            return new dal家庭档案(Loginer.CurrentUser).selectRkxzlByHzxx(flag, txt, rgid, _jtdabh);
        }
        /// <summary>
        /// 方法重载
        /// </summary>
        /// <param name="where"></param>
        /// <param name="_jtdabh"></param>
        /// <returns></returns>
        public DataTable selectRkxzlByHzxx(string where, string _jtdabh)
        {
            return new dal家庭档案(Loginer.CurrentUser).selectRkxzlByHzxx(where, _jtdabh);
        }
        /// <summary>
        /// 查询一个家庭中的成员个数
        /// </summary>
        /// <param name="jtdabh">家庭档案编号</param>
        /// <returns></returns>
        public int countHomeCy(string jtdabh)
        {
            return new dal家庭档案(Loginer.CurrentUser).countHomeCy(jtdabh);
        }
        /// <summary>
        /// 转入家庭成员  页面   查询按钮
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataTable selectFreeJtcy(string strWhere)
        {
            return new dal家庭档案(Loginer.CurrentUser).selectFreeJtcy(strWhere);
        }
        /// <summary>
        /// 转入家庭成员  页面“加入家庭” 按钮
        /// </summary>
        /// <param name="grdabh"></param>
        /// <param name="_jtdabh"></param>
        /// <param name="yhzgx"></param>
        /// <returns></returns>
        public bool addJtcy(string grdabh, string _jtdabh, string yhzgx)
        {
            return new dal家庭档案(Loginer.CurrentUser).addJtcy(grdabh, _jtdabh, yhzgx);
        }

        public DataSet GetBusinessByFamilNo(string familNo, bool resetCurrent)
        {
            DataSet ds = new dal家庭档案(Loginer.CurrentUser).GetBusinessByFamilyNO(familNo);
            this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }
        /// <summary>
        /// 获取家庭档案信息
        /// </summary>
        /// <param name="_docNo"></param>
        /// <param name="resetCurrent"></param>
        /// <returns></returns>
        public DataSet GetInfoByFamily(string _docNo, bool resetCurrent)
        {
            DataSet ds = new dal家庭档案(Loginer.CurrentUser).GetInfoByFamily(_docNo);
            //this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }
    }
}
