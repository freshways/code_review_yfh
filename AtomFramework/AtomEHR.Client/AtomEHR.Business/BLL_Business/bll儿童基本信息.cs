﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using AtomEHR.Models;
using AtomEHR.Bridge;
using AtomEHR.Common;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Server.DataAccess;

/*==========================================
 *   程序说明: 儿童基本信息的业务逻辑层
 *   作者姓名: ATOM
 *   创建日期: 2015/09/02 01:27:07
 *   最后修改: 2015/09/02 01:27:07
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Business
{
    /// <summary>
    /// bll儿童基本信息
    /// </summary>
    public class bll儿童基本信息 : bllBaseBusiness
    {
         /// <summary>
         /// 构造器
         /// </summary>
         public bll儿童基本信息()
         {
             _KeyFieldName = tb_儿童基本信息.__KeyName; //主键字段
             _SummaryTableName = tb_儿童基本信息.__TableName;//表名
         }

          /// <summary>
          ///根据单据号码取业务数据
          /// </summary>
          public override DataSet GetBusinessByKey(string keyValue, bool resetCurrent)
          {
              DataSet ds = new dal儿童基本信息(Loginer.CurrentUser).GetBusinessByKey(keyValue); 
              this.SetNumericDefaultValue(ds); //设置预设值
              if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return ds;
           }

        ///根据ID获取数据
          public DataSet GetBusinessByID(string keyValue)
          {
              DataSet ds = new dal儿童基本信息(Loginer.CurrentUser).GetBusinessByID(keyValue);
              //this.SetNumericDefaultValue(ds); //设置预设值
              //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return ds;
          }

          public void CreateNew儿童基本信息()
          {
              DataTable summaryTable = _CurrentBusiness.Tables[tb_儿童基本信息.__TableName];
              DataRow row = summaryTable.Rows.Add();
              //row[tb_儿童基本信息.ID] = "";
              //row[tb_儿童基本信息.家庭档案编号] = st家庭档案编号;
              row[tb_儿童基本信息.所属机构] = Loginer.CurrentUser.所属机构;
              row[tb_儿童基本信息.创建机构] = Loginer.CurrentUser.所属机构;
              row[tb_儿童基本信息.创建人] = Loginer.CurrentUser.Account;
              //row[tb_儿童基本信息.创建日期] = strServerDate;
              row[tb_儿童基本信息.修改人] = Loginer.CurrentUser.Account;
              //row[tb_儿童基本信息.修改日期] = strServerDate;
          }

           /// <summary>
          ///根据单据号码取业务数据
          /// </summary>
          public DataSet Get儿童基本信息ByKey(string keyValue)
          {
              DataSet ds = new dal儿童基本信息(Loginer.CurrentUser).Get儿童基本信息ByKey(keyValue); 
              this.SetNumericDefaultValue(ds); //设置预设值
              return ds;
           }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="grdabh"></param>
        /// <returns></returns>
          public System.Data.DataSet Get儿童基本信息ByGrdabh(string grdabh)
          {
              DataSet ds = new dal儿童基本信息(Loginer.CurrentUser).Get儿童基本信息ByGrdabh(grdabh);
              //this.SetNumericDefaultValue(ds); //设置预设值
              return ds;
          }

        /// <summary>
        /// 从儿童信息业务表中删除所有的儿童信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="grdabh"></param>
        /// <param name="strBirth"></param>
        /// <param name="strRet"></param>
        /// <returns>-1：删除异常；0：删除失败；1：删除成功</returns>
          public int Delete(string id, string grdabh, string strBirth, out string strRet)
          {
              if(new bllCom().GetAgeByBirthDay(strBirth)>6)//年龄大于6岁
              {
                  try
                  {
                      DeleteSubInfo(id, grdabh);

                      //删除儿童基本信息
                      Delete(grdabh);

                      //返回删除成功
                      strRet = "档案号为" + grdabh + "的档案删除成功！";
                      return 1;
                  }
                  catch(Exception ex)
                  {
                      strRet = ex.Message;
                      return -1;
                  }
              }
              else
              {
                  //不允许删除
                  strRet= "档案号为" + grdabh + "的人为儿童，不允许删除！";
                  return 0;
              }
          }

        /// <summary>
        /// 根据个人档案编号删除所有的儿童信息，调用之前需要先判断年龄>6岁
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
          public bool DeleteByGrdabh(string id, string grdabh)
          {
              try
              {
                  DeleteSubInfo(id, grdabh);

                  //删除儿童基本信息
                  Delete(grdabh);

                  //返回删除成功
                  return true;
               
              }
              catch (Exception ex)
              {
                  return false;
              }
          }

        /// <summary>
        ///删除儿童基本信息之外的儿童信息
        /// </summary>
          private void DeleteSubInfo(string id, string grdabh)
          {
              //根据个人档案编号进行删除
              //删除新生儿家庭访视记录
              //删除儿童健康检查记录表(满月)
              //删除儿童健康检查记录表(3月龄)
              //删除儿童健康检查记录表(6月龄)
              //删除儿童健康检查记录表(8月龄)
              //删除儿童健康检查记录表(12月龄)
              //删除儿童健康检查记录表(18月龄)
              //删除儿童健康检查记录表(24月龄)
              //删除儿童健康检查记录表(30月龄)
              //删除儿童健康检查记录表(3岁)
              //删除儿童健康检查记录表(4岁)
              //删除儿童健康检查记录表(5岁)
              //删除儿童健康检查记录表(6岁)
              //删除儿童入托检查表(4岁)

              //删除tfydata数据
              DataSet ds = GetBusinessByID(id);
              if(ds!=null && ds.Tables.Count>0 && ds.Tables[0].Rows.Count > 0)
              {
                  string biaodanhao = "e0";
                  string csrq = ds.Tables[0].Rows[0][tb_儿童基本信息.出生日期].ToString();
                  string lrr = ds.Tables[0].Rows[0]["UserName"].ToString();
                  string lrsj = ds.Tables[0].Rows[0][tb_儿童基本信息.创建日期].ToString();
                  string xm = DESEncrypt.DES解密(ds.Tables[0].Rows[0][tb_儿童基本信息.姓名].ToString());
                  string sfzh = ds.Tables[0].Rows[0][tb_儿童基本信息.身份证号].ToString();
                  string sjly = "zhonglian";
                  string jcrgid = ds.Tables[0].Rows[0][tb_儿童基本信息.所属机构].ToString();

                  new bll妇幼数据().Delete(biaodanhao, csrq, lrr, lrsj, xm, sfzh, sjly, jcrgid);
              }


          }

          /// <summary>
          ///只从儿童信息表中删除儿童信息
          /// </summary>
          public override bool Delete(string keyValue)
          {
              return new dal儿童基本信息(Loginer.CurrentUser).Delete(keyValue);
          }

          /// <summary>
          ///检查单号是否存在
          /// </summary>
          public bool CheckNoExists(string keyValue)
          {
              return new dal儿童基本信息(Loginer.CurrentUser).CheckNoExists(keyValue);
          }

          /// <summary>
          ///保存数据
          /// </summary>
          public override SaveResult Save(DataSet saveData)
          {
              return new dal儿童基本信息(Loginer.CurrentUser).Update(saveData); //交给数据层处理
          }

          /// <summary>
          ///审核单据
          /// </summary>
          public override void ApprovalBusiness(DataRow summaryRow)
          {
               summaryRow[BusinessCommonFields.AppDate] = DateTime.Now;
               summaryRow[BusinessCommonFields.AppUser] = Loginer.CurrentUser.Account;
               summaryRow[BusinessCommonFields.FlagApp] = "Y";
               string key = ConvertEx.ToString(summaryRow[tb_儿童基本信息.__KeyName]);
               new dal儿童基本信息(Loginer.CurrentUser).ApprovalBusiness(key, "Y", Loginer.CurrentUser.Account, DateTime.Now);
          }

          /// <summary>
          ///新增一张业务单据
          /// </summary>
          public override void NewBusiness()
          {
              DataTable summaryTable = _CurrentBusiness.Tables[tb_儿童基本信息.__TableName];
              DataRow row = summaryTable.Rows.Add();     
              //根据需要自行取消注释
              //row[tb_儿童基本信息.__KeyName] = "*自动生成*";
              //row[tb_儿童基本信息.创建人] = Loginer.CurrentUser.Account;
              //row[tb_儿童基本信息.创建时间] = DateTime.Now;
              //row[tb_儿童基本信息.修改人] = Loginer.CurrentUser.Account;
              //row[tb_儿童基本信息.修改时间] = DateTime.Now;
           }

          /// <summary>
          ///创建用于保存的临时数据
          /// </summary>
          public override DataSet CreateSaveData(DataSet currentBusiness, UpdateType currentType)
          {
              this.UpdateSummaryRowState(this.DataBindRow, currentType);

              //创建用于保存的临时数据,里面包含主表数据
              DataSet save = this.DoCreateTempData(currentBusiness, tb_儿童基本信息.__TableName);
              DataTable summary = save.Tables[0];
              //根据需要自行取消注释
              //summary.Rows[0][BusinessCommonFields.s修改人] = Loginer.CurrentUser.Account;
              //summary.Rows[0][BusinessCommonFields.s修改时间] = DateTime.Now;

              //this.UpdateSummaryRowState(currentBusiness.Tables[tb_儿童基本信息s.__TableName].Rows[0], currentType); //子表
              //DataTable detail = currentBusiness.Tables[tb_儿童基本信息s.__TableName].Copy();
              //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
              //save.Tables.Add(detail); //加入明细数据 

              return save; 
          }

          /// <summary>
          ///查询数据
          /// </summary>
          public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
          {
              DataTable dt = new dal儿童基本信息(Loginer.CurrentUser).GetSummaryByParam(docNoFrom, docNoTo, docDateFrom, docDateTo);
              //this.SetNumericDefaultValue(ds); //设置预设值
              //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return dt;
          }

          /// <summary>
          ///查询数据
          /// </summary>
          public DataTable GetSummaryByParam(string str机构编码, bool b含下属机构, string str姓名, string str性别,
                                             string str出生日期开始, string str出生日期截止, string str档案状态, string str录入人,
                                             string str录入时间Begin, string str录入时间End, string str档案编号, string str身份证号,
                                             string str是否合格,
                                             string str镇, string str村, string str村地址)
          {
              DataTable dt = new dal儿童基本信息(Loginer.CurrentUser).GetSummaryByParam(str机构编码, b含下属机构, str姓名, str性别,
                                             str出生日期开始, str出生日期截止, str档案状态, str录入人,
                                             str录入时间Begin, str录入时间End, str档案编号, str身份证号,
                                             str是否合格,
                                             str镇, str村, str村地址);
              //this.SetNumericDefaultValue(ds); //设置预设值
              //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return dt;
          }

          /// <summary>
          ///获取报表数据
          /// </summary>
          public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
          {
              return null;
          }

          #region Business Log

          /// <summary>
          /// 写入日志
          /// </summary>
          public override void WriteLog()
          {
              string key = this.DataBinder.Rows[0][tb_儿童基本信息.__KeyName].ToString();//取单据号码
              DataSet dsOriginal = this.GetBusinessByKey(key, false); //取保存前的原始数据, 用于保存日志时匹配数据.
              DataSet dsTemplate = this.CreateSaveData(this.CurrentBusiness, UpdateType.Modify); //创建用于保存的临时数据
              this.WriteLog(dsOriginal, dsTemplate);//保存日志      
          }

          /// <summary>
          /// 写入日志
          /// </summary>
          /// <param name="original">原始数据</param>
          /// <param name="changes">修改后的数据</param>
          public override void WriteLog(DataTable original, DataTable changes) { }

          /// <summary>
          /// 写入日志
          /// </summary>
          /// <param name="original">原始数据</param>
          /// <param name="changes">修改后的数据</param>
          public override void WriteLog(DataSet original, DataSet changes)
          {  //单独处理,即使错误,不向外抛出异常
              //if (_SaveVersionLog == false) return; //应用策略
              try
              {
                  string logID = Guid.NewGuid().ToString().Replace("-", ""); //本次日志ID
                  //SystemLog.WriteLog(logID, original.Tables[0], changes.Tables[0], tb_儿童基本信息.__TableName, tb_儿童基本信息.__KeyName, true); //主表
                  //明细表的修改日志,系统不支持自动生成,请手工调整代码
                  //SystemLog.WriteLog(logID, original.Tables[1], changes.Tables[1], tb_儿童基本信息.__TableName, tb_儿童基本信息.__KeyName, false);
              }
              catch
              {
                  Msg.Warning("写入日志发生错误！");
              }
          }

          #endregion

        #region 新增的方法
          /// <summary>
          ///获取报表数据
          /// </summary>
          public DataSet GetReportData(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string type)
          {
              DataSet ds = new dal儿童基本信息(Loginer.CurrentUser).GetReportData(DocNo机构, DocNo完整度, DateFrom, DateTo, type);
              this.SetNumericDefaultValue(ds); //设置预设值
              return ds;
          }
        #endregion

        /// <summary>
        /// 统计0-6岁儿童健康检查详细信息
        /// </summary>
        /// <param name="prgid">所属机构</param>
        /// <param name="sDate">开始时间</param>
        /// <param name="eDate">结束时间</param>
        /// <param name="p4"></param>
        /// <param name="p5"></param>
        /// <returns></returns>
          public DataSet GetChildInfo(string prgid, string sDate, string eDate, string p4, string p5)
          {
              DataSet ds = new dal儿童基本信息(Loginer.CurrentUser).GetChildInfo(prgid, sDate, eDate, p4, p5);
              return ds;
          }

          public DataSet GetChildInfoNew(string prgid, string sDate, string eDate, string name, string sfzh, string dah)
          {
              DataSet ds = new dal儿童基本信息(Loginer.CurrentUser).GetChildInfoNew(prgid, sDate, eDate, name, sfzh, dah);
              return ds;
          }

          public DataSet GetChildGLL(string rgid, string sDate, string eDate, string SearchBy, string selectType)
          {
              DataSet ds = new dal儿童基本信息(Loginer.CurrentUser).GetChildGLL(rgid, sDate, eDate, SearchBy, selectType);
              return ds;
          }

          public DataSet GetInfoByErTong(string _docNo, bool resetCurrent)
          {
              //throw new NotImplementedException();
              DataSet ds = new dal儿童基本信息(Loginer.CurrentUser).GetInfoByFangshi(_docNo);
              //this.SetNumericDefaultValue(ds); //设置预设值
              if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return ds;
          }

        public DataTable GetBaseData(string sex)
          {
              return new dal儿童基本信息(Loginer.CurrentUser).GetBaseData(sex);
          }

        public DataTable GetGrownData(string dah)
        {
            return new dal儿童基本信息(Loginer.CurrentUser).GetGrownData(dah);
        }

        public DataSet Get妇保院工作量(string name, string sfzh, string dah, string createUserNo, string sfrq1, string sfrq2, string csrq1, string csrq2)
        {
            DataSet ds = new dal儿童基本信息(Loginer.CurrentUser).Get妇保院工作量(name, sfzh, dah, createUserNo, sfrq1, sfrq2, csrq1, csrq2);
            return ds;
        }
    }
}
