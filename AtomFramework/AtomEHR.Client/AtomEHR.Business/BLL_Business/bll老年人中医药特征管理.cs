﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using AtomEHR.Models;
using AtomEHR.Bridge;
using AtomEHR.Common;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Server.DataAccess;

/*==========================================
 *   程序说明: 老年人中医药特征管理的业务逻辑层
 *   作者姓名: ATOM
 *   创建日期: 2015/09/23 08:00:16
 *   最后修改: 2015/09/23 08:00:16
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Business
{
    /// <summary>
    /// bll老年人中医药特征管理
    /// </summary>
    public class bll老年人中医药特征管理 : bllBaseBusiness
    {
        /// <summary>
        /// 构造器
        /// </summary>
        public bll老年人中医药特征管理()
        {
            _KeyFieldName = tb_老年人中医药特征管理.__KeyName; //主键字段
            _SummaryTableName = tb_老年人中医药特征管理.__TableName;//表名
        }

        /// <summary>
        ///根据单据号码取业务数据
        /// </summary>
        public override DataSet GetBusinessByKey(string keyValue, bool resetCurrent)
        {
            DataSet ds = new dal老年人中医药特征管理(Loginer.CurrentUser).GetBusinessByKey(keyValue);
            // this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }

        public DataSet GetOneDataByKey(string _docNo, string _id, bool resetCurrent)
        {
            DataSet ds = new dal老年人中医药特征管理(Loginer.CurrentUser).GetOneDataByKeys(_docNo, _id);
            //this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }

        public DataSet GetAllDataByKey(string _docNo)
        {
            DataSet ds = new dal老年人中医药特征管理(Loginer.CurrentUser).GetAllDataByKey(_docNo);
            //this.SetNumericDefaultValue(ds); //设置预设值
            //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }
        public DataSet GetAllDataValueByKey(string _docNo)
        {
            DataSet ds = new dal老年人中医药特征管理(Loginer.CurrentUser).GetAllDataValueByKey(_docNo);
            //this.SetNumericDefaultValue(ds); //设置预设值
            //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }
        /// <summary>
        ///删除单据
        /// </summary>
        public override bool Delete(string keyValue)
        {
            return new dal老年人中医药特征管理(Loginer.CurrentUser).Delete(keyValue);
        }

        /// <summary>
        ///检查单号是否存在
        /// </summary>
        public bool CheckNoExists(string keyValue)
        {
            return new dal老年人中医药特征管理(Loginer.CurrentUser).CheckNoExists(keyValue);
        }

        /// <summary>
        ///保存数据
        /// </summary>
        public override SaveResult Save(DataSet saveData)
        {
            return new dal老年人中医药特征管理(Loginer.CurrentUser).Update(saveData); //交给数据层处理
        }

        /// <summary>
        ///审核单据
        /// </summary>
        public override void ApprovalBusiness(DataRow summaryRow)
        {
            summaryRow[BusinessCommonFields.AppDate] = DateTime.Now;
            summaryRow[BusinessCommonFields.AppUser] = Loginer.CurrentUser.Account;
            summaryRow[BusinessCommonFields.FlagApp] = "Y";
            string key = ConvertEx.ToString(summaryRow[tb_老年人中医药特征管理.__KeyName]);
            new dal老年人中医药特征管理(Loginer.CurrentUser).ApprovalBusiness(key, "Y", Loginer.CurrentUser.Account, DateTime.Now);
        }

        /// <summary>
        ///新增一张业务单据
        /// </summary>
        public override void NewBusiness()
        {
            DataTable summaryTable = _CurrentBusiness.Tables[tb_老年人中医药特征管理.__TableName];
            DataRow row = summaryTable.Rows.Add();
            //根据需要自行取消注释
            //row[tb_老年人中医药特征管理.__KeyName] = "*自动生成*";
            //row[tb_老年人中医药特征管理.创建人] = Loginer.CurrentUser.Account;
            //row[tb_老年人中医药特征管理.创建时间] = DateTime.Now;
            //row[tb_老年人中医药特征管理.修改人] = Loginer.CurrentUser.Account;
            //row[tb_老年人中医药特征管理.修改时间] = DateTime.Now;
        }

        /// <summary>
        ///创建用于保存的临时数据
        /// </summary>
        public override DataSet CreateSaveData(DataSet currentBusiness, UpdateType currentType)
        {
            this.UpdateSummaryRowState(this.DataBindRow, currentType);

            //创建用于保存的临时数据,里面包含主表数据
            DataSet save = this.DoCreateTempData(currentBusiness, tb_老年人中医药特征管理.__TableName);
            DataTable summary = save.Tables[0];
            //根据需要自行取消注释
            //summary.Rows[0][BusinessCommonFields.s修改人] = Loginer.CurrentUser.Account;
            //summary.Rows[0][BusinessCommonFields.s修改时间] = DateTime.Now;

            //this.UpdateSummaryRowState(currentBusiness.Tables[tb_老年人中医药特征管理s.__TableName].Rows[0], currentType); //子表
            //DataTable detail = currentBusiness.Tables[tb_老年人中医药特征管理s.__TableName].Copy();
            //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
            //save.Tables.Add(detail); //加入明细数据 

            return save;
        }

        /// <summary>
        ///查询数据
        /// </summary>
        public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
        {
            DataTable dt = new dal老年人中医药特征管理(Loginer.CurrentUser).GetSummaryByParam(docNoFrom, docNoTo, docDateFrom, docDateTo);
            //this.SetNumericDefaultValue(ds); //设置预设值
            //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return dt;
        }

        /// <summary>
        ///获取报表数据
        /// </summary>
        public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
        {
            return null;
        }

        #region Business Log

        /// <summary>
        /// 写入日志
        /// </summary>
        public override void WriteLog()
        {
            string key = this.DataBinder.Rows[0][tb_老年人中医药特征管理.__KeyName].ToString();//取单据号码
            DataSet dsOriginal = this.GetBusinessByKey(key, false); //取保存前的原始数据, 用于保存日志时匹配数据.
            DataSet dsTemplate = this.CreateSaveData(this.CurrentBusiness, UpdateType.Modify); //创建用于保存的临时数据
            this.WriteLog(dsOriginal, dsTemplate);//保存日志      
        }

        /// <summary>
        /// 写入日志
        /// </summary>
        /// <param name="original">原始数据</param>
        /// <param name="changes">修改后的数据</param>
        public override void WriteLog(DataTable original, DataTable changes) { }

        /// <summary>
        /// 写入日志
        /// </summary>
        /// <param name="original">原始数据</param>
        /// <param name="changes">修改后的数据</param>
        public override void WriteLog(DataSet original, DataSet changes)
        {  //单独处理,即使错误,不向外抛出异常
            //if (_SaveVersionLog == false) return; //应用策略
            try
            {
                string logID = Guid.NewGuid().ToString().Replace("-", ""); //本次日志ID
                //SystemLog.WriteLog(logID, original.Tables[0], changes.Tables[0], tb_老年人中医药特征管理.__TableName, tb_老年人中医药特征管理.__KeyName, true); //主表
                //明细表的修改日志,系统不支持自动生成,请手工调整代码
                //SystemLog.WriteLog(logID, original.Tables[1], changes.Tables[1], tb_老年人中医药特征管理.__TableName, tb_老年人中医药特征管理.__KeyName, false);
            }
            catch
            {
                Msg.Warning("写入日志发生错误！");
            }
        }

        #endregion



        /// <summary>
        /// 根据条件查询数据
        /// </summary>
        /// <param name="dict"></param>
        /// <returns></returns>
        public DataTable GetDataByPrarm(Dictionary<string, string> dict)
        {
            return new dal老年人中医药特征管理(Loginer.CurrentUser).GetDataByPrarm(dict);

        }

        public bool update体检信息(string year, string sql)
        {
            return new dal老年人中医药特征管理(Loginer.CurrentUser).update体检信息(year, sql);
        }

        public DataSet GetInfoByOldZYY(string docNo, string _date, bool resetCurrent)
        {
            DataSet ds = new dal老年人中医药特征管理(Loginer.CurrentUser).GetInfoByOldZYY(docNo, _date);
            //this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }

        //Begin WXF 2018-11-01 | 19:29
        //根据档案号返回所有发生时间 
        public DataTable Get_发生时间(string grdah)
        {
            DataTable dt = new dal老年人中医药特征管理(Loginer.CurrentUser).Get_发生时间(grdah);
            return dt;
        }
        //End


        //Begin WXF 2018-11-07 | 17:43
        //根据档案号与体检日期获取一条数据 
        public DataRow Get_One中医药管理(string grdah, string str_CreateDT)
        {
            DataRow dr = new dal老年人中医药特征管理(Loginer.CurrentUser).Get_One中医药管理(grdah, str_CreateDT);
            return dr;
        }
        //End

    }
}
