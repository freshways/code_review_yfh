﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using AtomEHR.Bridge;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Business.BLL_Business;
using AtomEHR.Common;
using AtomEHR.Models;
using AtomEHR.Server.DataAccess;
using AtomEHR.Interfaces.Interfaces_Bridge;

/*==========================================
 *   程序说明: 个案随访表的业务逻辑层
 *   作者姓名: ATOM
 *   创建日期: 2017-09-28 04:54:09
 *   最后修改: 2017-09-28 04:54:09
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Business
{
    /// <summary>
    /// bll个案随访表
    /// </summary>
    public class bll个案随访表 : bllBaseBusiness
    {
        /// <summary>
        /// 构造器
        /// </summary>
        public bll个案随访表()
        {
            _KeyFieldName = tb_个案随访表.__KeyName; //主键字段
            _SummaryTableName = tb_个案随访表.__TableName;//表名
        }

        /// <summary>
        ///根据单据号码取业务数据
        /// </summary>
        public override DataSet GetBusinessByKey(string keyValue, bool resetCurrent)
        {
            DataSet ds = new dal个案随访表(Loginer.CurrentUser).GetBusinessByKey(keyValue);
            //this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }

        /// <summary>
        /// 获取一个修改的数据源
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="resetCurrent"></param>
        /// <returns></returns>
        public DataSet GetBusinessByKeyEdit(string keyValue, bool resetCurrent)
        {
            DataSet ds = new dal个案随访表(Loginer.CurrentUser).GetBusinessByID(keyValue);
            this.SetNumericDefaultValue(ds); //设置预设值
            if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return ds;
        }

        /// <summary>
        ///删除单据
        /// </summary>
        public override bool Delete(string keyValue)
        {
            return new dal个案随访表(Loginer.CurrentUser).Delete(keyValue);
        }

        /// <summary>
        ///检查单号是否存在
        /// </summary>
        public bool CheckNoExists(string keyValue)
        {
            return new dal个案随访表(Loginer.CurrentUser).CheckNoExists(keyValue);
        }

        /// <summary>
        ///保存数据
        /// </summary>
        public override SaveResult Save(DataSet saveData)
        {
            return new dal个案随访表(Loginer.CurrentUser).Update(saveData); //交给数据层处理
        }

        /// <summary>
        ///审核单据
        /// </summary>
        public override void ApprovalBusiness(DataRow summaryRow)
        {
            summaryRow[BusinessCommonFields.AppDate] = DateTime.Now;
            summaryRow[BusinessCommonFields.AppUser] = Loginer.CurrentUser.Account;
            summaryRow[BusinessCommonFields.FlagApp] = "Y";
            string key = ConvertEx.ToString(summaryRow[tb_个案随访表.__KeyName]);
            new dal个案随访表(Loginer.CurrentUser).ApprovalBusiness(key, "Y", Loginer.CurrentUser.Account, DateTime.Now);
        }

        /// <summary>
        ///新增一张业务单据
        /// </summary>
        public override void NewBusiness()
        {
            DataTable summaryTable = _CurrentBusiness.Tables[tb_个案随访表.__TableName];
            DataRow row = summaryTable.Rows.Add();
            //根据需要自行取消注释
            //row[tb_个案随访表.__KeyName] = "*自动生成*";
            row[tb_个案随访表.创建人] = Loginer.CurrentUser.用户编码;
            row[tb_个案随访表.创建时间] = base.ServiceDateTime;
            row[tb_个案随访表.修改人] = Loginer.CurrentUser.用户编码;
            row[tb_个案随访表.修改时间] = base.ServiceDateTime;
            row[tb_个案随访表.创建机构] = Loginer.CurrentUser.所属机构;
            row[tb_个案随访表.所属机构] = Loginer.CurrentUser.所属机构;
            //设置默认项
            row[tb_个案随访表.是否羁押] ="2";
            row[tb_个案随访表.是否已死亡] = "2";
            row[tb_个案随访表.是否为母婴传播病例] = "2";
            row[tb_个案随访表.是否有配偶] = "2";
            row[tb_个案随访表.是否有固定性伴] = "2";
            row[tb_个案随访表.是否咳嗽咳痰] = "2";
            row[tb_个案随访表.是否咳痰带血] = "2";
            row[tb_个案随访表.是否反复发热] = "2";
            row[tb_个案随访表.是否夜间出汗] = "2";
            row[tb_个案随访表.是否异常体重下降] = "2";
            row[tb_个案随访表.是否容易疲劳] = "2";
            row[tb_个案随访表.是否淋巴结肿大] = "2";
            row[tb_个案随访表.是否接受过结核病检查] = "2";
            row[tb_个案随访表.是否接受免费艾滋病抗病毒治疗] = "2";

        }

        /// <summary>
        ///创建用于保存的临时数据
        /// </summary>
        public override DataSet CreateSaveData(DataSet currentBusiness, UpdateType currentType)
        {
            this.UpdateSummaryRowState(this.DataBindRow, currentType);

            //创建用于保存的临时数据,里面包含主表数据
            DataSet save = this.DoCreateTempData(currentBusiness, tb_个案随访表.__TableName);
            DataTable summary = save.Tables[0];
            //根据需要自行取消注释
            summary.Rows[0][BusinessCommonFields.s修改人] = Loginer.CurrentUser.用户编码;
            summary.Rows[0][BusinessCommonFields.s修改时间] = base.ServiceDateTime;

            //DataTable detail = currentBusiness.Tables[tb_个案随访表s.__TableName].Copy();
            //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
            //save.Tables.Add(detail); //加入明细数据 

            return save;
        }

        /// <summary>
        ///查询数据
        /// </summary>
        public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
        {
            DataTable dt = new dal个案随访表(Loginer.CurrentUser).GetSummaryByParam(docNoFrom, docNoTo, docDateFrom, docDateTo);
            //this.SetNumericDefaultValue(ds); //设置预设值
            //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
            return dt;
        }

        /// <summary>
        ///获取报表数据
        /// </summary>
        public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
        {
            return null;
        }

        #region Business Log

        /// <summary>
        /// 写入日志
        /// </summary>
        public override void WriteLog()
        {
            string key = this.DataBinder.Rows[0][tb_个案随访表.__KeyName].ToString();//取单据号码
            DataSet dsOriginal = this.GetBusinessByKey(key, false); //取保存前的原始数据, 用于保存日志时匹配数据.
            DataSet dsTemplate = this.CreateSaveData(this.CurrentBusiness, UpdateType.Modify); //创建用于保存的临时数据
            this.WriteLog(dsOriginal, dsTemplate);//保存日志      
        }

        /// <summary>
        /// 写入日志
        /// </summary>
        /// <param name="original">原始数据</param>
        /// <param name="changes">修改后的数据</param>
        public override void WriteLog(DataTable original, DataTable changes) { }

        /// <summary>
        /// 写入日志
        /// </summary>
        /// <param name="original">原始数据</param>
        /// <param name="changes">修改后的数据</param>
        public override void WriteLog(DataSet original, DataSet changes)
        {  //单独处理,即使错误,不向外抛出异常
            //if (_SaveVersionLog == false) return; //应用策略
            try
            {
                string logID = Guid.NewGuid().ToString().Replace("-", ""); //本次日志ID

                IBridge_EditLogHistory SystemLog = bllBusinessLog.CreateEditLogHistoryBridge();//初始化日志方法

                SystemLog.WriteLog(logID, original.Tables[0], changes.Tables[0], tb_个案随访表.__TableName, tb_个案随访表.__KeyName, true); //主表
                //明细表的修改日志,系统不支持自动生成,请手工调整代码
                //SystemLog.WriteLog(logID, original.Tables[1], changes.Tables[1], tb_个案随访表.__TableName, tb_个案随访表.__KeyName, false);
            }
            catch
            {
                Msg.Warning("写入日志发生错误！");
            }
        }

        #endregion


    }
}
