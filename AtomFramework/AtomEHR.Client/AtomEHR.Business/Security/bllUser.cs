﻿
///*************************************************************************/
///*
///* 文件名    ：bllUser.cs        
///
///* 程序说明  : 用户管理的业务逻辑层
///               
///* 原创作者  ：ATOM 
///* Copyright ©  2015 GGBond
///*
///**************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.Interfaces;
using AtomEHR.Business.BLL_Base;
//using AtomEHR.WebServiceReference;
using System.ServiceModel;
using AtomEHR.Core;
using AtomEHR.Bridge;
using AtomEHR.Bridge.SystemModule;
using AtomEHR.Server.DataAccess;
using AtomEHR.Server.DataAccess.DAL_System;

namespace AtomEHR.Business.Security
{
    /// <summary>
    /// 用户管理的业务逻辑层
    /// </summary>
    public class bllUser : bllBaseDataDict, IFuzzySearchSupportable
    {
        private IBridge_User _MyBridge;

        public bllUser()
        {
            _KeyFieldName = TUser.__KeyName; //主键字段
            _SummaryTableName = TUser.__TableName;//表名
            _WriteDataLog = false;//是否保存日志                    
            _DataDictBridge = BridgeFactory.CreateDataDictBridge(typeof(TUser));
            _MyBridge = this.CreateBridge();
        }

        /// <summary>
        /// 创建实现了桥接接口的通信通道
        /// </summary>
        /// <returns></returns>
        private IBridge_User CreateBridge()
        {
            if (BridgeFactory.BridgeType == BridgeType.ADODirect)
                return new ADODirect_User().GetInstance();

            if (BridgeFactory.BridgeType == BridgeType.WebService)
                return new WebService_User();

            return null;
        }

        /// <summary>
        /// 获取所有用户数据
        /// </summary>
        /// <returns></returns>
        public DataTable GetUsers()
        {
            _SummaryTable = new dalUser(Loginer.CurrentUser).GetUsers();

            SetDefault(_SummaryTable);
            return _SummaryTable;
        }

        /// <summary>
        /// 根据机构ID获取所有用户数据
        /// </summary>
        /// <returns></returns>
        public DataTable GetUsersRegion(string RGID,string strWhere="")
        {
            _SummaryTable = new dalUser(Loginer.CurrentUser).GetUsersRegion(RGID,strWhere);

            SetDefault(_SummaryTable);
            return _SummaryTable;
        }

        protected override void SetDefault(DataTable data)
        {
            data.Columns[TUser.IsLocked].DefaultValue = 0;
            data.Columns[TUser.CreateTime].DefaultValue = DateTime.Now;
        }

        public override bool Delete(string s_用户编码)
        {
            return _MyBridge.DeleteUser(s_用户编码);
        }

        /// <summary>
        /// 获取指定用户数据
        /// </summary>
        /// <param name="account">用户帐号</param>
        /// <returns></returns>
        public DataTable GetUser(string account)
        {
            return _MyBridge.GetUser(account);
        }

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="data">用户数据</param>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool Update(DataTable data, UpdateType type)
        {
            DataSet ds = this.CreateDataset(data, type);
            return _MyBridge.Update(ds);
        }        

        public override bool CheckNoExists(string account)
        {
            return _MyBridge.ExistsUser(account);
        }

        /// <summary>
        /// 修改用户密码
        /// </summary>
        /// <param name="account">用户帐号</param>
        /// <param name="pwd">新密码</param>
        /// <returns></returns>
        public bool ModifyPassword(string account, string pwd)
        {
            return _MyBridge.ModifyPassword(account, pwd);
        }

        /// <summary>
        /// 跟据Novell帐号获取用户登录帐号
        /// </summary>
        /// <param name="novellAccount">Novell帐号</param>
        /// <returns></returns>
        public DataTable GetUserByNovellID(string novellAccount)
        {
            return _MyBridge.GetUserByNovellID(novellAccount);
        }

        public void Validate(LoginUser user)
        {
            if (CheckNoExists(user.Account))
                throw new Exception("用户已经存在!");
        }

        public static void ValidateLogin(string userID, string password)
        {
            if (userID.Trim() == "")
                throw new Exception("用户编号不正确或不能为空!");

            if (password.Trim() == "")
                throw new Exception("密码不正确或不能为空!");
        }

        /// <summary>
        /// 获取用户基本信息
        /// </summary>
        /// <param name="account"></param>
        /// <param name="DBName"></param>
        /// <returns></returns>
        public DataTable GetUserDirect(string account, string DBName)
        {
            return _MyBridge.GetUserDirect(account, DBName);
        }

        public bool ModifyPasswordDirect(string account, string pwd, string DBName)
        {
            return _MyBridge.ModifyPwdDirect(account, pwd, DBName);
        }

        public bool Modify身份证Direct(string account, string sfz)
        {
            return new dalUser(Loginer.CurrentUser).ModifySFZDirect(account, sfz);
        }

        public DataTable GetUserGroups(string account)
        {
            return _MyBridge.GetUserGroups(account);
        }

        public DataTable Login(LoginUser loginUser, char LoginUserType)
        {
            return _MyBridge.Login(loginUser, LoginUserType);
        }

        public DataTable GetUserAuthorities(Loginer user)
        {
            return _MyBridge.GetUserAuthorities(user);
        }

        public void Logout(string account)
        {
            _MyBridge.Logout(account);
        }

        public DataSet GetUserReportData(DateTime createDateFrom, DateTime createDateTo)
        {
            return _MyBridge.GetUserReportData(createDateFrom, createDateTo);
        }

        //取得与当前用户所在机构相同的所有用户列表
        public DataTable GetUserNameAndIDBy机构用户(string rgid)
        {
            return new dalUser(Loginer.CurrentUser).GetUserNameAndIDBy机构用户(rgid);
        }

        #region IFuzzySearchSupportable Members

        public string FuzzySearchName
        {
            get { return "搜索机构资料"; }
        }

        public DataTable FuzzySearch(string content)
        {
            return new dal机构信息(Loginer.CurrentUser).F模糊查询(content);
        }

        #endregion

        /// <summary>
        /// 根据机构获取最大的用户编码
        /// </summary>
        /// <param name="RegionID">机构ID</param>
        /// <returns></returns>
        public string GetNew用户编号(string RegionID)
        {
            return new dalUser(Loginer.CurrentUser).GetNew用户编号(RegionID);
        }

        #region InterFace接口类
        /// <summary>
        /// 执行添加和删除用户对照sql语句
        /// </summary>
        /// <param name="SQL"></param>
        /// <returns></returns>
        public bool InterFaceUserExSql(string SQL)
        {
            return new dalUser(Loginer.CurrentUser).InterFaceUserExSql(SQL);
        }

        /// <summary>
        /// 返回用户对照列表,根据用户编码获取所有对应记录
        /// </summary>
        /// <param name="ehrUserID"></param>
        /// <returns></returns>
        public DataTable InterFaceUserMapingList(string ehrUserID)
        {
            return new dalUser(Loginer.CurrentUser).InterFaceGetUserMapingList(ehrUserID);
        }

        /// <summary>
        /// 获取外围用户-暂时是一体化
        /// </summary>
        /// <param name="ehrRegionID"></param>
        /// <returns></returns>
        public DataTable InterFaceUserList(string ehrRegionID)
        {
            return new dalUser(Loginer.CurrentUser).InterFaceGetUserList(ehrRegionID);
        }
        #endregion

        public DataTable GetGWUserListByRGID(string rgid, string name)
        {
            return new dalUser(Loginer.CurrentUser).GetGWUserListByRGID(rgid, name);
        }

        //通知通告用 wxf 2018年9月29日 19:45:22
        public DataTable Get_卫生院管理员成员()
        {
            return new dalUser(Loginer.CurrentUser).Get_卫生院管理员成员();
        }

        public DataTable Get_用户名称(string UserCode)
        {
            return new dalUser(Loginer.CurrentUser).Get_用户名称(UserCode);
        }

        //public DataTable GetwxUser(string jgbh)
        //{
        //    return new dalUser(Loginer.CurrentUser).GetwxUser(jgbh);
        //}
    }
}
