﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.Common;
using AtomEHR.Models;
using AtomEHR.Bridge;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Server.DataAccess;
using AtomEHR.Interfaces;

/*==========================================
 *   程序说明: B超报告模板的业务逻辑层
 *   作者姓名: ATOM
 *   创建日期: 2019-04-12 12:02:36
 *   最后修改: 2019-04-12 12:02:36
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Business
{
    public class bllB超报告模板 : bllBaseDataDict, IFuzzySearchSupportable
    {
        private dalB超报告模板 _MyBridge;
         public bllB超报告模板()
         {
             _KeyFieldName = tb_B超报告模板.__KeyName; //主键字段
             _SummaryTableName = tb_B超报告模板.__TableName;//表名
             //_WriteDataLog = true;//是否保存日志
             //方式一：由ORM自动查询DAL类
             _DataDictBridge = BridgeFactory.CreateDataDictBridge(typeof(tb_B超报告模板));
             _MyBridge = new dalB超报告模板(Loginer.CurrentUser);
         }

         /// <summary>
         /// 重写绑定方法，返回null是为了默认不加载；
         /// 如果需要加载数据，还需再dal下重新此方法，对数据库进行查询
         /// </summary>
         /// <param name="resetCurrent"></param>
         /// <returns></returns>
         public override DataTable GetSummaryData(bool resetCurrent)
         {
             DataTable data = _MyBridge.GetSummaryData();
             this.SetDefault(data);

             if (resetCurrent) _SummaryTable = data;
             return data;
         }
        
         #region 实现筛选接口
         public string FuzzySearchName
         {
             get { return "搜索B超模板"; }
         }

         /// <summary>
         /// 通用筛选器
         /// </summary>
         /// <param name="content"></param>
         /// <returns></returns>
         public DataTable FuzzySearch(string content)
         {
             return new dalB超报告模板(Loginer.CurrentUser).F模糊查询(content);
         }
         #endregion
     }
}
