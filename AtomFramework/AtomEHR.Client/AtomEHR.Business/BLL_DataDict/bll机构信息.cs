﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.Models;
using AtomEHR.Bridge;
using AtomEHR.Common;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Server.DataAccess;

/*==========================================
 *   程序说明: 机构信息的业务逻辑层
 *   作者姓名: ATOM
 *   创建日期: 2015-07-15 11:19:55
 *   最后修改: 2015-07-15 11:19:55
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Business
{
    public class bll机构信息 : bllBaseDataDict
    {
        private dal机构信息 _MyBridge;
        public bll机构信息()
        {
            _KeyFieldName = tb_机构信息.__KeyName; //主键字段
            _SummaryTableName = tb_机构信息.__TableName;//表名
            //_WriteDataLog = true;//是否保存日志
            //方式一：由ORM自动查询DAL类
            _DataDictBridge = BridgeFactory.CreateDataDictBridge(typeof(tb_机构信息));
            _MyBridge = new dal机构信息(Loginer.CurrentUser);
        }

        /// <summary>
        /// 重写绑定方法，返回null是为了默认不加载；
        /// 如果需要加载数据，还需再dal下重新此方法，对数据库进行查询
        /// </summary>
        /// <param name="resetCurrent"></param>
        /// <returns></returns>
        public override DataTable GetSummaryData(bool resetCurrent)
        {
            DataTable data = _MyBridge.GetSummaryData();
            this.SetDefault(data);

            if (resetCurrent) _SummaryTable = data;
            return data;
        }

        /// <summary>
        /// 根据机构获取最大的用户编码
        /// </summary>
        /// <param name="RegionID">机构ID</param>
        /// <returns></returns>
        public string GetNew机构编号(string RegionID)
        {
            return _MyBridge.GetNew机构编号(RegionID);
        }

        /// <summary>
        /// 自定义查询方法
        /// </summary>
        public DataTable SearchBy(string CustomerFrom, string CustomerTo, string Name, string Attribute, bool resetCurrent)
        {
            DataTable data = _MyBridge.SearchBy(CustomerFrom, CustomerTo, Name, Attribute);
            if (resetCurrent) _SummaryTable = data;
            this.SetDefault(_SummaryTable);
            return data;
        }

        /// <summary>
        /// 模糊查询
        /// </summary>
        /// <param name="attributeCodes">编号</param>
        /// <param name="content">名称</param>
        /// <returns></returns>
        public DataTable F模糊查询(string attributeCodes)
        {
            return _MyBridge.F模糊查询(attributeCodes);
        }

        public DataTable Get机构树()
        {
            return _MyBridge.Get机构树();
        }
        public DataTable Get机构树(string rgid)
        {
            return _MyBridge.Get机构树(rgid);
        }

        /// <summary>
        /// 根据机构ID获取其子机构信息
        /// </summary>
        /// <param name="rgid"></param>
        /// <returns></returns>
        public DataTable Get机构(string rgid)
        {
            return _MyBridge.Get机构(rgid);
        }

        public string Get上级机构(string rgid)
        {
            return _MyBridge.Get上级机构(rgid);
        }

        public string Get所在乡镇编码(string rgid)
        {
            return _MyBridge.Get所在乡镇编码(rgid);
        }

        //通知通告用 wxf 2018年9月29日 19:45:53
        public DataTable Get_卫生院名称(string rgid)
        {
            return _MyBridge.Get_卫生院名称(rgid);
        }

        public DataTable Get_机构名称(string rgid)
        {
            return _MyBridge.Get_机构名称(rgid);
        }
    }
}
