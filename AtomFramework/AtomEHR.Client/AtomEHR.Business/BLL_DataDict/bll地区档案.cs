﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using AtomEHR.Models;
using AtomEHR.Bridge;
using AtomEHR.Common;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Server.DataAccess;

/*==========================================
 *   程序说明: 地区档案的业务逻辑层
 *   作者姓名: ATOM
 *   创建日期: 2015/09/21 08:21:42
 *   最后修改: 2015/09/21 08:21:42
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Business
{
    /// <summary>
    /// bll地区档案
    /// </summary>
    public class bll地区档案 : bllBaseDataDict
    {
        private dal地区档案 _MyBridge;
         /// <summary>
         /// 构造器
         /// </summary>
         public bll地区档案()
         {
             _KeyFieldName = tb_地区档案.__KeyName; //主键字段
             _SummaryTableName = tb_地区档案.__TableName;//表名
             //_WriteDataLog = true;//是否保存日志
             //方式一：由ORM自动查询DAL类
             _DataDictBridge = BridgeFactory.CreateDataDictBridge(typeof(tb_地区档案));
             _MyBridge = new dal地区档案(Loginer.CurrentUser);
         }

         /// <summary>
         /// 重写绑定方法，返回null是为了默认不加载；
         /// 如果需要加载数据，还需再dal下重新此方法，对数据库进行查询
         /// </summary>
         /// <param name="resetCurrent"></param>
         /// <returns></returns>
         public override DataTable GetSummaryData(bool resetCurrent)
         {
             DataTable data = _MyBridge.GetSummaryData();
             this.SetDefault(data);

             if (resetCurrent) _SummaryTable = data;
             return data;
         }
         /// <summary>
         /// 根据地区获取最大的用户编码
         /// </summary>
         /// <param name="RegionID">地区ID</param>
         /// <returns></returns>
         public string GetNew地区编码(string RegionID)
         {
             return _MyBridge.GetNew地区编码(RegionID);
         }

          /// <summary>
          /// 自定义查询方法
          /// </summary>
          public DataTable SearchBy(string CustomerFrom, string CustomerTo, string Name,
             string Attribute, bool resetCurrent)
          {
              DataTable data = _MyBridge.SearchBy(CustomerFrom, CustomerTo, Name, Attribute);
              if (resetCurrent) _SummaryTable = data;
              this.SetDefault(_SummaryTable);
              return data;
          }          
        
          public string Get名称By地区ID(string 地区ID)
          {
              return new dal地区档案(Loginer.CurrentUser).Get名称By地区ID(地区ID); //交给数据层处理
          }
     }
}
