﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.Bridge;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Business.BLL_Business;
using AtomEHR.Common;
using AtomEHR.Models;
using AtomEHR.Server.DataAccess;
using AtomEHR.Interfaces.Interfaces_Bridge;

/*==========================================
 *   程序说明: IDCard_AreaCode_New的业务逻辑层
 *   作者姓名: ATOM
 *   创建日期: 2018-12-06 05:39:54
 *   最后修改: 2018-12-06 05:39:54
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Business
{
    public class bllIDCard_AreaCode_New : bllBaseDataDict
    {
        public bllIDCard_AreaCode_New()
        {
            _KeyFieldName = tb_IDCard_AreaCode_New.__KeyName; //主键字段
            _SummaryTableName = tb_IDCard_AreaCode_New.__TableName;//表名
            _WriteDataLog = true;//是否保存日志
            //方式一：由ORM自动查询DAL类
            _DataDictBridge = BridgeFactory.CreateDataDictBridge(typeof(tb_IDCard_AreaCode_New));
        }

        //Begin WXF 2018-12-06 | 19:41
        //@@UpdateValue 
        /// <summary>
        /// 根据地区编码获取地区详细行政区划信息
        /// </summary>
        /// <param name="AreaCode"></param>
        /// <returns></returns>
        public DataTable GetAreaInfo(string AreaCode)
        {
            return new dalIDCard_AreaCode_New(Loginer.CurrentUser).GetAreaInfo(AreaCode);
        }
        //End
    }
}
