﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.Bridge;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Business.BLL_Business;
using AtomEHR.Common;
using AtomEHR.Models;
using AtomEHR.Server.DataAccess;
using AtomEHR.Interfaces.Interfaces_Bridge;
using System.Drawing;
using AtomEHR.Interfaces;

/*==========================================
 *   程序说明: 医生信息的业务逻辑层
 *   作者姓名: ATOM
 *   创建日期: 2018-12-04 10:48:58
 *   最后修改: 2018-12-04 10:48:58
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Business
{
    public class bll医生信息 : bllBaseDataDict, IFuzzySearchSupportable
    {
        private dal医生信息 _MyBridge;
        public bll医生信息()
        {
            _KeyFieldName = tb_医生信息.__KeyName; //主键字段
            _SummaryTableName = tb_医生信息.__TableName;//表名
            _WriteDataLog = true;//是否保存日志
            //方式一：由ORM自动查询DAL类
            _DataDictBridge = BridgeFactory.CreateDataDictBridge(typeof(tb_医生信息));
            _MyBridge = new dal医生信息(Loginer.CurrentUser);
        }

        /// <summary>
        /// 重写绑定方法，返回null是为了默认不加载；
        /// 如果需要加载数据，还需再dal下重新此方法，对数据库进行查询
        /// </summary>
        /// <param name="resetCurrent"></param>
        /// <returns></returns>
        public override DataTable GetSummaryData(bool resetCurrent)
        {
            DataTable data = _MyBridge.GetSummaryData();
            this.SetDefault(data);

            if (resetCurrent) _SummaryTable = data;
            return data;
        }

        /// <summary>
        /// 根据地区获取最大的编码
        /// </summary>
        /// <param name="RegionID">地区ID</param>
        /// <returns></returns>
        public string GetNew医生编码(string RegionID)
        {
            return _MyBridge.GetNew医生编码(RegionID);
        }

        /// <summary>
        /// 自定义查询方法
        /// </summary>
        public DataTable SearchBy(string _RGID, string _科室, string _医生姓名, string _查体模块, bool resetCurrent)
        {
            DataTable data = _MyBridge.SearchBy(_RGID, _科室, _医生姓名, _查体模块);
            if (resetCurrent) _SummaryTable = data;
            this.SetDefault(_SummaryTable);
            return data;
        }

        public DataTable Get手签By机构(string RGIF)
        {
            return new dal医生信息(Loginer.CurrentUser).Get手签By机构(RGIF); //交给数据层处理
        }

        //Begin WXF 2018-12-24 | 17:26
        //医师签字绑定用  
        public DataTable LookUp医师签字(string Org)
        {
            return new dal医生信息(Loginer.CurrentUser).LookUp医师签字(Org);
        }

        /// <summary>
        /// 根据医生编码获取医师手签图片(Bitmap)
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        //public Bitmap GetBmp手签(string code)
        //{
        //    if (string.IsNullOrEmpty(code)) return (Bitmap)null;

        //    byte[] bt_手签 = new dal医生信息(Loginer.CurrentUser).GetBmp手签(code);
        //    if (bt_手签 == null) return (Bitmap)null;

        //    return (Bitmap)ZipTools.DecompressionObject(bt_手签);
        //}

        //End


        //Begin WXF 2019-01-22 | 14:29
        //IFuzzySearchSupportable接口实现 
        string IFuzzySearchSupportable.FuzzySearchName
        {
            get { return "搜索医生信息"; }
        }

        DataTable IFuzzySearchSupportable.FuzzySearch(string content)
        {
            return new dal医生信息(Loginer.CurrentUser).F模糊查询(content);
        }
        //End
    }
}
