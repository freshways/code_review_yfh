﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.Bridge;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Business.BLL_Business;
using AtomEHR.Common;
using AtomEHR.Models;
using AtomEHR.Server.DataAccess;
using AtomEHR.Interfaces.Interfaces_Bridge;

/*==========================================
 *   程序说明: 常用字典的业务逻辑层
 *   作者姓名: ATOM
 *   创建日期: 2018-11-07 06:20:29
 *   最后修改: 2018-11-07 06:20:29
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Business
{
    public class bll常用字典 : bllBaseDataDict
    {
         public bll常用字典()
         {
             _KeyFieldName = tb_常用字典.__KeyName; //主键字段
             _SummaryTableName = tb_常用字典.__TableName;//表名
             _WriteDataLog = true;//是否保存日志
             //方式一：由ORM自动查询DAL类
             _DataDictBridge = BridgeFactory.CreateDataDictBridge(typeof(tb_常用字典));  
         }

         //Begin WXF 2018-11-07 | 16:29
         //翻译常用字典

         /// <summary>
         /// 翻译遗传字典值,拼接为字符串后返回
         /// P_FUN_CODE 字典键值
         /// P_CODEs 字典代码串
         /// separator 字典代码串分隔符
         /// </summary>
         /// <param name="P_FUN_CODE"></param>
         /// <param name="P_CODEs"></param>
         /// <param name="separator"></param>
         /// <returns></returns>
         public string DicMap(string P_FUN_CODE, string P_CODEs, char separator)
         {
             List<string> result = new List<string>();
             string[] strs_P_CODE = null;
             if (!string.IsNullOrWhiteSpace(P_CODEs))
             {
                 strs_P_CODE = P_CODEs.Split(separator);
             }

             if (strs_P_CODE != null)
             {
                 foreach (string item in strs_P_CODE)
                 {
                     result.Add(new dal常用字典(Loginer.CurrentUser).DicMap(P_FUN_CODE, item));
                 }
             }


             return string.Join(Convert.ToString(separator), result.ToArray());
         }

         /// <summary>
         /// 翻译单个值
         /// </summary>
         /// <param name="P_FUN_CODE"></param>
         /// <param name="P_CODE"></param>
         /// <returns></returns>
         public string DicMap(string P_FUN_CODE, string P_CODE)
         {
             if (string.IsNullOrEmpty(P_CODE)) return "";
             return new dal常用字典(Loginer.CurrentUser).DicMap(P_FUN_CODE, P_CODE);
         }
        //End
     }
}
