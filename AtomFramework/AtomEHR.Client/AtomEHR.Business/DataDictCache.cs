﻿///*************************************************************************/
///*
///* 文件名    ：DataDictCache.cs      
///
///* 程序说明  : 数据字典缓存数据
///               
///* 原创作者  ：ATOM 
///* Copyright ©  2015 GGBond
///*
///**************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.Interfaces;
using AtomEHR.Bridge;
using AtomEHR.Server.DataAccess;


namespace AtomEHR.Business
{
    /*
     数据字典缓存数据
     */
    public class DataDictCache
    {

        private DataDictCache() { } /*私有构造器,不允许外部创建实例*/

        #region 缓存数据唯一实例

        private static DataDictCache _Cache = null;

        /// <summary>
        /// 缓存数据唯一实例
        /// </summary>
        public static DataDictCache Cache
        {
            get
            {
                if (_Cache == null)
                {
                    _Cache = new DataDictCache();
                    _Cache.DownloadBaseCacheData();//下载基本数据                    
                }
                return _Cache;
            }
        }
        #endregion

        #region 外部使用的静态方法

        /// <summary>
        /// 刷新缓存数据
        /// </summary>
        public static void RefreshCache()
        {
            DataDictCache.Cache.DownloadBaseCacheData();
        }

        /// <summary>
        /// 刷新单个数据字典
        /// </summary>
        /// <param name="tableName">字典表名</param>
        public static void RefreshCache(string tableName)
        {
            DataTable cache = DataDictCache.Cache.GetCacheTableData(tableName);

            if (cache != null) //有客户窗体引用缓存数据时才更新
            {
                IBridge_DataDict bridge = BridgeFactory.CreateDataDictBridge(tableName, "");
                DataTable data = bridge.GetDataDictByTableName(tableName);
                cache.Rows.Clear();
                foreach (DataRow row in data.Rows) cache.ImportRow(row);
                cache.AcceptChanges();
            }
        }

        #endregion

        #region 2.数据表缓存数据. 局域变易及属性定义

        private DataSet _AllDataDicts = null;

        private DataTable _BusinessTables = null;
        public DataTable BusinessTables { get { return _BusinessTables; } }

        //private DataTable _StockType = null;
        //public DataTable StockType { get { return _StockType; } }

        //private DataTable _Currency = null;
        //public DataTable Currency { get { return _Currency; } }

        //private DataTable _PayType = null;
        //public DataTable PayType { get { return _PayType; } }

        private DataTable _User = null; //用户表
        public DataTable User { get { return _User; } }

        //private DataTable _Person = null; //营销员
        //public DataTable Person { get { return _Person; } }

        //private DataTable _Storage = null; //仓库
        //public DataTable Storage { get { return _Storage; } }

        //private DataTable _Unit = null;
        //public DataTable Unit { get { return _Unit; } }

        private DataTable _DepartmentData = null;
        public DataTable DepartmentData { get { return _DepartmentData; } }

        private DataTable _CustomerAttributes = null;
        public DataTable CustomerAttributes { get { return _CustomerAttributes; } }

        private DataTable _体检医师 = null;
        public DataTable 体检医师 { get { return _体检医师; } }

        private DataTable _CommonDataDictType = null;
        public DataTable CommonDataDictType { get { return _CommonDataDictType; } }

        //private DataTable _Location = null;
        //public DataTable Location { get { return _Location; } }

        private DataTable _t机构级别 = null;
        public DataTable t机构级别 { get { return _t机构级别; } }

        private DataTable _t机构信息 = null;
        public DataTable t机构信息 { get { return _t机构信息; } }

        private DataTable _t地区信息 = null;
        public DataTable t地区信息 { get { return _t地区信息; } }

        private DataTable _t与户主关系 = null;
        public DataTable t与户主关系 { get { return _t与户主关系; } }

        private DataTable _t性别 = null;
        public DataTable t性别 { get { return _t性别; } }

        private DataTable _t证件类型 = null;
        public DataTable t证件类型 { get { return _t证件类型; } }

        private DataTable _t常住类型 = null;
        public DataTable t常住类型 { get { return _t常住类型; } }

        private DataTable _t民族 = null;
        public DataTable t民族 { get { return _t民族; } }

        private DataTable _t血型 = null;
        public DataTable t血型 { get { return _t血型; } }

        private DataTable _tRH = null;
        public DataTable tRH { get { return _tRH; } }

        private DataTable _t职业 = null;
        public DataTable t职业 { get { return _t职业; } }

        private DataTable _t文化程度 = null;
        public DataTable t文化程度 { get { return _t文化程度; } }

        private DataTable _t劳动强度 = null;
        public DataTable t劳动强度 { get { return _t劳动强度; } }

        private DataTable _t婚姻状况 = null;
        public DataTable t婚姻状况 { get { return _t婚姻状况; } }

        private DataTable _t档案类别 = null;
        public DataTable t档案类别 { get { return _t档案类别; } }

        private DataTable _t有无 = null;
        public DataTable t有无 { get { return _t有无; } }

        private DataTable _t家族史成员 = null;
        public DataTable t家族史成员 { get { return _t家族史成员; } }

        private DataTable _t住房类型 = null;
        public DataTable t住房类型 { get { return _t住房类型; } }

        private DataTable _t厕所类型 = null;
        public DataTable t厕所类型 { get { return _t厕所类型; } }

        private DataTable _t是否 = null;
        public DataTable t是否 { get { return _t是否; } }

        private DataTable _t是否戒酒 = null;
        public DataTable t是否戒酒 { get { return _t是否戒酒; } }

        private DataTable _t服药依从性 = null;
        public DataTable t服药依从性 { get { return _t服药依从性; } }

        private DataTable _t参数列表 = null;
        public DataTable t参数列表 { get { return _t参数列表; } }


        private DataTable _t考核项 = null;
        public DataTable t考核项 { get { return _t考核项; } }

        private DataTable _t常用字典 = null;
        public DataTable t常用字典 { get { return _t常用字典; } }

        private DataTable _t与户主关系_户主 = null;
        public DataTable t与户主关系_户主 { get { return _t与户主关系_户主; } }

        private DataTable _t与户主关系_非户主 = null;
        public DataTable t与户主关系_非户主 { get { return _t与户主关系_非户主; } }

        private DataTable _t档案状态 = null;
        public DataTable t档案状态 { get { return _t档案状态; } }

        private DataTable _t有无异常 = null;
        public DataTable t有无异常 { get { return _t有无异常; } }

        private DataTable _t阴道分泌物 = null;
        public DataTable t阴道分泌物 { get { return _t阴道分泌物; } }

        private DataTable _t阴道清洁度 = null;
        public DataTable t阴道清洁度 { get { return _t阴道清洁度; } }

        private DataTable _t阴性阳性 = null;
        public DataTable t阴性阳性 { get { return _t阴性阳性; } }

        private DataTable _t恢复状态 = null;
        public DataTable t恢复状态 { get { return _t恢复状态; } }

        private DataTable _t处理类型 = null;
        public DataTable t处理类型 { get { return _t处理类型; } }

        private DataTable _t产后42天指导 = null;
        public DataTable t产后42天指导 { get { return _t产后42天指导; } }

        private DataTable _t知情同意 = null;
        public DataTable t知情同意 { get { return _t知情同意; } }

        private DataTable _t体检项目 = null;
        public DataTable t体检项目 { get { return _t体检项目; } }

        private DataTable _t体检建议 = null;
        public DataTable t体检建议 { get { return _t体检建议; } }

        private DataTable _t多重残疾 = null;
        public DataTable t多重残疾 { get { return _t多重残疾; } }

        private DataTable _t康复服务情况 = null;
        public DataTable t康复服务情况 { get { return _t康复服务情况; } }

        private DataTable _t康复目标 = null;
        public DataTable t康复目标 { get { return _t康复目标; } }

        private DataTable _t主要残疾 = null;
        public DataTable t主要残疾 { get { return _t主要残疾; } }

        private DataTable _t训练场地 = null;
        public DataTable t训练场地 { get { return _t训练场地; } }

        private DataTable _t训练效果 = null;
        public DataTable t训练效果 { get { return _t训练效果; } }

        private DataTable _t遵医行为= null;
        public DataTable t遵医行为 { get { return _t遵医行为; } }

        private DataTable _t此次随访分类 = null;
        public DataTable t此次随访分类 { get { return _t此次随访分类; } }

        private DataTable _t精神病专科医生意见 = null;
        public DataTable t精神病专科医生意见 { get { return _t精神病专科医生意见; } }

        private DataTable _t精神病诊断 = null;
        public DataTable t精神病诊断 { get { return _t精神病诊断; } }

        private DataTable _t是否体检 = null;
        public DataTable t是否体检 { get { return _t是否体检; } }

        private DataTable _t是否随访 = null;
        public DataTable t是否随访 { get { return _t是否随访; } }

        private DataTable _t建册情况 = null;
        public DataTable t建册情况 { get { return _t建册情况; } }

        private DataTable _t是否到位 = null;
        public DataTable t是否到位 { get { return _t是否到位; } }

        #endregion


        public void DownloadBaseCacheData()
        {
            IBridge_DataDict bridge = BridgeFactory.CreateDataDictBridge("");

            //下载小字典表数据
            _AllDataDicts = bridge.DownloadDicts();

            //跟据存储过程返回数据表的顺序取
            _BusinessTables = _AllDataDicts.Tables[1];
            _User = _AllDataDicts.Tables[2];
            _CustomerAttributes = _AllDataDicts.Tables[3];
            _体检医师 = _AllDataDicts.Tables[4];
            _CommonDataDictType = _AllDataDicts.Tables[5];
            _DepartmentData = _AllDataDicts.Tables[6];
            _t机构级别 = _AllDataDicts.Tables[7];
            _t机构信息 = _AllDataDicts.Tables[8];
            _t地区信息 = _AllDataDicts.Tables[9];

            _t与户主关系 = _AllDataDicts.Tables[10];
            //档案状态   11
            _t性别 = _AllDataDicts.Tables[12];
            _t证件类型 = _AllDataDicts.Tables[13];
            _t常住类型 = _AllDataDicts.Tables[14];
            _t民族 = _AllDataDicts.Tables[15];
            _t血型 = _AllDataDicts.Tables[16];
            _tRH = _AllDataDicts.Tables[17];
            _t职业 = _AllDataDicts.Tables[18];
            _t文化程度 = _AllDataDicts.Tables[19];
            _t劳动强度 = _AllDataDicts.Tables[20];
            _t婚姻状况 = _AllDataDicts.Tables[21];
            _t档案类别 = _AllDataDicts.Tables[22];
            _t有无 = _AllDataDicts.Tables[23];
            _t家族史成员 = _AllDataDicts.Tables[24];
            _t住房类型 = _AllDataDicts.Tables[25];
            _t厕所类型 = _AllDataDicts.Tables[26];
            _t是否 = _AllDataDicts.Tables[27];
            _t是否戒酒 = _AllDataDicts.Tables[28];
            _t服药依从性 = _AllDataDicts.Tables[29];
            _t考核项 = _AllDataDicts.Tables[30];
            _t常用字典 = _AllDataDicts.Tables[31];
            _t与户主关系_户主 = _AllDataDicts.Tables[32];
            _t与户主关系_非户主 = _AllDataDicts.Tables[33];
            _t档案状态 = _AllDataDicts.Tables[34];
            _t有无异常 = _AllDataDicts.Tables[35];
            _t阴道分泌物 = _AllDataDicts.Tables[36];
            _t阴道清洁度 = _AllDataDicts.Tables[37];
            _t阴性阳性 = _AllDataDicts.Tables[38];
            _t恢复状态 = _AllDataDicts.Tables[39];
            _t处理类型 = _AllDataDicts.Tables[40];
            _t产后42天指导 = _AllDataDicts.Tables[41];
            _t知情同意 = _AllDataDicts.Tables[42];
            _t体检项目 = _AllDataDicts.Tables[43];
            _t体检建议 = _AllDataDicts.Tables[44];
            _t多重残疾 = _AllDataDicts.Tables[45];
            _t康复服务情况 = _AllDataDicts.Tables[46];
            _t康复目标 = _AllDataDicts.Tables[47];
            _t主要残疾 = _AllDataDicts.Tables[48];
            _t训练场地 = _AllDataDicts.Tables[49];
            _t训练效果= _AllDataDicts.Tables[50];
            _t遵医行为 = _AllDataDicts.Tables[51];
            _t此次随访分类 = _AllDataDicts.Tables[52];
            _t精神病专科医生意见 = _AllDataDicts.Tables[53];
            _t精神病诊断 = _AllDataDicts.Tables[54];
            _t是否体检 = _AllDataDicts.Tables[55];
            _t是否随访 = _AllDataDicts.Tables[56];
            _t建册情况 = _AllDataDicts.Tables[57];
            _t是否到位 = _AllDataDicts.Tables[58];

            //_tRH = _AllDataDicts.Tables[17];  
            //调用数据表名
            _AllDataDicts.Tables[1].TableName = sys_BusinessTables.__TableName;
            _AllDataDicts.Tables[2].TableName = TUser.__TableName;
            _AllDataDicts.Tables[3].TableName = tb_CustomerAttribute.__TableName;
            //_AllDataDicts.Tables[4].TableName = "#Bank"; //tb_CommDataDictType表的银行类别的记录
            _AllDataDicts.Tables[4].TableName = "#体检医师"; 
            _AllDataDicts.Tables[5].TableName = tb_CommDataDictType.__TableName;
            _AllDataDicts.Tables[6].TableName = "#Dept"; //tb_CommDataDictType表的部门类别的记录
            _AllDataDicts.Tables[7].TableName = "#t机构级别";
            _AllDataDicts.Tables[8].TableName = tb_机构信息.__TableName;
            _AllDataDicts.Tables[9].TableName = "#地区信息";
            _AllDataDicts.Tables[10].TableName = "与户主关系";
            _AllDataDicts.Tables[12].TableName = "性别";
            _AllDataDicts.Tables[13].TableName = "证件类型";
            _AllDataDicts.Tables[14].TableName = "常住类型";
            _AllDataDicts.Tables[15].TableName = "民族";
            _AllDataDicts.Tables[16].TableName = "血型";
            _AllDataDicts.Tables[17].TableName = "RH";
            _AllDataDicts.Tables[18].TableName = "职业";
            _AllDataDicts.Tables[19].TableName = "文化程度";
            _AllDataDicts.Tables[20].TableName = "劳动强度";
            _AllDataDicts.Tables[21].TableName = "婚姻状况";
            _AllDataDicts.Tables[22].TableName = "档案类别";
            _AllDataDicts.Tables[23].TableName = "有无";
            _AllDataDicts.Tables[24].TableName = "家族史成员";
            _AllDataDicts.Tables[25].TableName = "住房类型";
            _AllDataDicts.Tables[26].TableName = "厕所类型";
            _AllDataDicts.Tables[27].TableName = "是否";
            _AllDataDicts.Tables[28].TableName = "是否戒酒";
            _AllDataDicts.Tables[29].TableName = "体检服药依从性";
            _AllDataDicts.Tables[30].TableName = "#考核项";
            _AllDataDicts.Tables[31].TableName = "#常用字典";
            _AllDataDicts.Tables[32].TableName = "#与户主关系_户主";
            _AllDataDicts.Tables[33].TableName = "#与户主关系_非户主";
            _AllDataDicts.Tables[34].TableName = "#档案状态";
            _AllDataDicts.Tables[35].TableName = "#有无异常";
            _AllDataDicts.Tables[36].TableName = "#阴道分泌物";
            _AllDataDicts.Tables[37].TableName = "#阴道清洁度";
            _AllDataDicts.Tables[38].TableName = "#阴性阳性";
            _AllDataDicts.Tables[39].TableName = "#恢复状态";
            _AllDataDicts.Tables[40].TableName = "#处理类型";
            _AllDataDicts.Tables[41].TableName = "#产后42天指导";
            _AllDataDicts.Tables[42].TableName = "#知情同意";
            _AllDataDicts.Tables[43].TableName = "#体检项目";
            _AllDataDicts.Tables[44].TableName = "#体检建议";
            _AllDataDicts.Tables[45].TableName = "#多重残疾";
            _AllDataDicts.Tables[46].TableName = "#康复服务情况";
            _AllDataDicts.Tables[47].TableName = "#康复目标";
            _AllDataDicts.Tables[48].TableName = "#主要残疾";
            _AllDataDicts.Tables[49].TableName = "#训练场地";
            _AllDataDicts.Tables[50].TableName = "#训练效果";
            _AllDataDicts.Tables[51].TableName = "#遵医行为";
            _AllDataDicts.Tables[52].TableName = "#此次随访分类";
            _AllDataDicts.Tables[53].TableName = "#精神病专科医生意见";
            _AllDataDicts.Tables[54].TableName = "#精神病诊断";
            _AllDataDicts.Tables[55].TableName = "#是否体检";
            _AllDataDicts.Tables[56].TableName = "#是否随访";
            _AllDataDicts.Tables[57].TableName = "#建册情况";
            _AllDataDicts.Tables[58].TableName = "#是否到位";
            try
            {
                _t参数列表 = new dal参数列表(Loginer.CurrentUser).Get参数列表("", "");
            }
            catch
            {
                _t参数列表 = null;
            }

        }

        /// <summary>
        /// 跟据表名取数据表实例
        /// </summary>
        /// <param name="tableName">字典表名</param>
        /// <returns></returns>
        private DataTable GetCacheTableData(string tableName)
        {
            foreach (DataTable dt in _AllDataDicts.Tables)
            {
                if (dt.TableName.ToUpper() == tableName.ToUpper()) return dt;
            }

            DataTable cache = null;
            //if (tableName == tb_CommDataDictType.__TableName) cache = _CommonDataDictType;            
            return cache;
        }

        /// <summary>
        ///删除字典数据某一行数据
        /// </summary>
        /// <param name="tableName">字典表名</param>
        /// <param name="keyField">主键</param>
        /// <param name="key">主键值</param>
        public void DeleteCacheRow(string tableName, string keyField, string key)
        {
            DataTable cach = this.GetCacheTableData(tableName);
            if (cach != null)
            {
                DataRow[] rows = cach.Select(keyField + "='" + key + "'");
                if (rows.Length > 0)
                    cach.Rows.Remove(rows[0]);
                cach.AcceptChanges();
            }
        }

        #region 系统开关参数
        //2019年1月24日 重新整理
        /// <summary>
        /// true 表示可以修改 false 表示不可以修改
        /// </summary>
        /// <returns></returns>
        public bool IsAllow跨机构修改()
        {
            if (_t参数列表 == null)
            {
                return false;
            }
            else
            {
                DataRow[] drs = _t参数列表.Select(tb_参数列表.别名 + "='fnkjgxg'");
                if (drs.Length > 0)
                {
                    return drs[0][tb_参数列表.键值].ToString().Equals("1");
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// true 表示可以修改 false 表示不可以修改
        /// </summary>
        /// <returns></returns>
        public bool IsAllow往年体检显示()
        {
            if (_t参数列表 == null)
            {
                return false;
            }
            else
            {
                DataRow[] drs = _t参数列表.Select(tb_参数列表.别名 + "='tjxs'");
                if (drs.Length > 0)
                {
                    return drs[0][tb_参数列表.键值].ToString().Equals("1");
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// true 表示可以修改 false 表示不可以修改
        /// </summary>
        /// <returns></returns>
        public int IsAllow延时加载数据()
        {
            try
            {
                if (_t参数列表 == null)
                {
                    return 0;
                }
                else
                {
                    DataRow[] drs = _t参数列表.Select(tb_参数列表.别名 + "='ysjz' and " + tb_参数列表.P_RGID + "='" + Loginer.CurrentUser.所属机构 + "' ");
                    if (drs.Length > 0)
                    {
                        return Convert.ToInt32(drs[0][tb_参数列表.键值]);
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch
            {
                return 0;
            }
        } 

        #endregion

    }

    /// <summary>
    /// 数据字典类型
    /// </summary>
    public enum enumBase
    {
        /// <summary>
        /// 体检-症状
        /// </summary>
        zz_zhengzhuang,
        /// <summary>
        /// 锻炼频率
        /// </summary>
        dlpl,
        /// <summary>
        /// 饮食习惯
        /// </summary>
        ysxg,
        /// <summary>
        /// 吸烟情况
        /// </summary>
        glkxyqk,
        /// <summary>
        /// 口唇
        /// </summary>
        kc_houchun,
        /// <summary>
        /// 齿列
        /// </summary>
        cl_chilei,
        /// <summary>
        /// 咽部
        /// </summary>
        yb_yanbu,
        /// <summary>
        /// 听力
        /// </summary>
        tingli,
        /// <summary>
        /// 运动功能
        /// </summary>
        yundong,
        /// <summary>
        /// 下肢水肿
        /// </summary>
        xzsz,
        /// <summary>
        /// 足背动脉搏动
        /// </summary>
        zbdmbd,
        /// <summary>
        /// 肛门指诊
        /// </summary>
        gmzz,
        /// <summary>
        /// 乳腺
        /// </summary>
        rx_ruxian,
        /// <summary>
        /// 神经系统疾病
        /// </summary>
        jktj_sjjb,
        /// <summary>
        /// 其他系统疾病
        /// </summary>
        jktj_qtjb,
        /// <summary>
        /// 体检有无异常
        /// </summary>
        tjywyc,
        /// <summary>
        /// 皮肤
        /// </summary>
        pifu_pf,
        /// <summary>
        /// 巩膜
        /// </summary>
        gm_gongmo,
        /// <summary>
        /// 淋巴结
        /// </summary>
        lbj2,
        /// <summary>
        /// 罗音
        /// </summary>
        luoyin,
        /// <summary>
        /// 心脏心律
        /// </summary>
        xzxl,
        /// <summary>
        /// 正常异常
        /// </summary>
        zcyc,
        /// <summary>
        /// 有无异常
        /// </summary>
        ywyc,
        /// <summary>
        /// 脑血管疾病
        /// </summary>
        nxgjb,
        /// <summary>
        /// 肾脏疾病
        /// </summary>
        szjb,
        /// <summary>
        /// 心脏疾病
        /// </summary>
        xzjb,
        /// <summary>
        /// 眼部疾病
        /// </summary>
        ybjb,
        /// <summary>
        /// 未发现有
        /// </summary>
        wfxy,
        /// <summary>
        /// 健康指导
        /// </summary>
        jkzd,
        /// <summary>
        /// 危险因素指导
        /// </summary>
        wxyszd,
        /// <summary>
        /// 是否
        /// </summary>
        sf_shifou,
        /// <summary>
        /// 无有
        /// </summary>
        wy_wuyou,
        /// <summary>
        /// 有无
        /// </summary>
        yw_youwu,
        /// <summary>
        /// 婚姻状况
        /// </summary>
        hyzk,
        /// <summary>
        /// 档案类别
        /// </summary>
        dalb,
        /// <summary>
        /// 职业
        /// </summary>
        zy_zhiye
    }
}
