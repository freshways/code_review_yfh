﻿///*************************************************************************/
///*
///* 文件名    ：bllBase.cs                                      
///* 程序说明  : 业务逻辑层基类
///* 原创作者  ：ATOM 
///* 
///* Copyright ©  2015 GGBond
///**************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.Common;
using AtomEHR.Server.DataAccess.DAL_Base;

namespace AtomEHR.Business.BLL_Base
{
    /// <summary>
    /// 业务逻辑层基类
    /// </summary>
    public class bllBase
    {
        #region Base
        /// <summary>
        /// 创建原始数据用于更新. 由DataTable转换为DataSet
        /// </summary>
        protected DataSet CreateDataset(DataTable data, UpdateType updateType)
        {
            DataSet ds = new DataSet();
            data.AcceptChanges(); //保存缓存数据
            foreach (DataRow row in data.Rows)
                this.SetRowState(row, updateType); //设置记录状态
            ds.Tables.Add(data.Copy()); //加到新的DataSet
            return ds;
        }

        /// <summary>
        /// 更新记录状态
        /// </summary>
        /// <param name="dataRow">记录</param>
        /// <param name="updateType">操作类型</param>
        private void SetRowState(DataRow dataRow, UpdateType updateType)
        {
            if (dataRow.RowState != DataRowState.Unchanged) return;
            if (updateType == UpdateType.Add)
                dataRow.SetAdded();
            if (updateType == UpdateType.Modify)
                dataRow.SetModified();
        }

        /// <summary>
        /// 数字类型设置预设值0
        /// </summary>
        /// <param name="ds"></param>
        protected void SetNumericDefaultValue(DataSet ds)
        {
            foreach (DataTable t in ds.Tables)
                this.SetNumericDefaultValue(t);
        }

        /// <summary>
        /// 数字类型设置预设值0
        /// </summary>
        /// <param name="dt"></param>
        protected void SetNumericDefaultValue(DataTable dt)
        {
            string names = ",int,int16,int32,int64,decimal,float,double,";
            foreach (DataColumn col in dt.Columns)
                if (names.IndexOf(col.DataType.Name.ToLower()) > 0)
                    col.DefaultValue = 0.00;

        }
        #endregion

        /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <param name="tableName">表名/视图</param>
        /// <param name="Fields">查询字段,可默认*或指定查询字段</param>
        /// <param name="strWhere">查询条件</param>
        /// <param name="SortExpression">排序字段</param>
        /// <param name="pageSize">页/大小</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="count">返回记录总数</param>
        /// <param name="SortDire">排序类型</param>
        /// <returns></returns>
        public DataSet Get分页数据(String tableName, String Fields, String strWhere, String SortExpression, Int32 pageSize, Int32 pageIndex, out Int32 count, String SortDire)
        {
            return new dalBase(Loginer.CurrentUser).Get分页数据(tableName, Fields, strWhere, SortExpression, pageSize, pageIndex, out count, SortDire);
        }

        /// <summary>
        /// 查询分页数据,2015-12-25 10:59:01 Yfuh添加
        /// </summary>
        /// <param name="tableName">表名/视图</param>
        /// <param name="Fields">查询字段,可默认*或指定查询字段</param>
        /// <param name="strWhere">查询条件</param>
        /// <param name="SortExpression">排序字段</param>
        /// <param name="pageSize">页/大小</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="count">返回记录总数</param>
        /// <param name="SortDire">排序类型</param>
        /// <returns></returns>
        public DataSet Get分页数据New(String tableName, String Fields, String strWhere, String SortExpression, Int32 pageSize, Int32 pageIndex, out Int32 count, String SortDire)
        {
            return new dalBase(Loginer.CurrentUser).Get分页数据New(tableName, Fields, strWhere, SortExpression, pageSize, pageIndex, out count, SortDire);
        }

        /// <summary>
        /// wxf 2018年10月13日 16:29:24
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="Fields"></param>
        /// <param name="strWhere"></param>
        /// <param name="SortExpression"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="count"></param>
        /// <param name="SortDire"></param>
        /// <returns></returns>
        public DataSet Get分页数据New_New(String tableName, String Fields, String strWhere, String SortExpression, Int32 pageSize, Int32 pageIndex, out Int32 count, String SortDire)
        {
            return new dalBase(Loginer.CurrentUser).Get分页数据New_New(tableName, Fields, strWhere, SortExpression, pageSize, pageIndex, out count, SortDire);
        }

        /// <summary>
        /// 查询全部数据
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="Fields"></param>
        /// <param name="strWhere"></param>
        /// <param name="SortExpression"></param>
        /// <param name="SortDire"></param>
        /// <returns></returns>
        public DataTable Get全部数据(String tableName, String Fields, String strWhere, String SortExpression, String SortDire)
        {
            return new dalBase(Loginer.CurrentUser).Get全部数据(tableName, Fields, strWhere, SortExpression, SortDire);
        }
        #region 扩展 - 2015-10-22 13:24:57 yufh 移动，对缓存的操作不仅对业务数据，字典也能用到
        /// <summary>
        /// 行转Table 2015-09-17 09:54:23
        /// </summary>
        /// <param name="drs">Table.Select 返回的dr[]</param>
        /// <returns></returns>
        public virtual DataTable ConvertRowsToTable(DataRow[] drs)
        {
            DataTable newdt = null;
            if (drs.Length > 0)
            {
                newdt = drs[0].Table.Clone();
                foreach (DataRow dr in drs)
                    newdt.Rows.Add(dr.ItemArray);
            }
            return newdt;
        }

        /// <summary>
        /// 行转Table
        /// </summary>
        /// <param name="drs">DataRow</param>
        /// <returns></returns>
        public virtual DataTable ConvertRowsToTable(DataRow dr)
        {
            DataTable newdt = dr.Table.Clone();
            if (dr != null && dr.ItemArray.Length > 0)
            {
                newdt.Rows.Add(dr.ItemArray);
            }
            return newdt;
        }

        /// <summary>
        /// 获取字典中的名称
        /// 2015-09-16 21:19:17 yufh 添加
        /// </summary>
        /// <param name="_类型"></param>
        /// <param name="_编码"></param>
        /// <returns></returns>
        public virtual string ReturnDis字典显示(string _类型, string _编码)
        {
            if (DataDictCache.Cache.t常用字典 != null)
            {
                DataRow[] dr = DataDictCache.Cache.t常用字典.Select("(P_FUN_CODE='" + _类型 + "' or P_FUN_DESC = '" + _类型 + "') and P_CODE='" + _编码 + "' ");

                if (dr.Length > 0)
                {
                    return dr[0]["P_DESC"].ToString();
                }
            }
            return _编码;
        }

        /// <summary>
        /// 获取用户名称
        /// 2015-10-22 13:45:42 yufh 添加
        /// </summary>
        /// <param name="_编码"></param>
        /// <returns></returns>
        public virtual string Return用户名称(string _编码)
        {
            if (DataDictCache.Cache.User != null)
            {
                DataRow[] dr = DataDictCache.Cache.User.Select("用户编码='" + _编码 + "' ");
                if (dr.Length > 0) { return dr[0]["UserName"].ToString(); }
            }
            return _编码;
        }

        /// <summary>
        /// 获取机构名称
        /// 2015-10-22 13:45:42 yufh 添加
        /// </summary>
        /// <param name="_编码"></param>
        /// <returns></returns>
        public virtual string Return机构名称(string _编码)
        {
            if (DataDictCache.Cache.t机构信息 != null)
            {
                DataRow[] dr = DataDictCache.Cache.t机构信息.Select("机构编号='" + _编码 + "' ");
                if (dr.Length > 0) { return dr[0]["机构名称"].ToString(); }
            }
            return _编码;
        }

        /// <summary>
        /// 获取机构名称
        /// 2015-10-22 13:45:42 yufh 添加
        /// </summary>
        /// <param name="_编码"></param>
        /// <returns></returns>
        public virtual string Return地区名称(string _编码)
        {
            if (DataDictCache.Cache.t地区信息 != null)
            {
                DataRow[] dr = DataDictCache.Cache.t地区信息.Select("地区编码='" + _编码 + "' ");
                if (dr.Length > 0) { return dr[0]["地区名称"].ToString(); }
            }
            return _编码;
        }

        #endregion
    }
}
