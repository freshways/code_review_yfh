﻿using AtomEHR.Common;
using AtomEHR.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace AtomEHR.Business
{
    public class AnalysisIDCard
    {

        //Begin WXF 2018-12-06 | 20:14
        //妥协的方法...... 

        //End

        public IDCardAnalysis.AnalysisInfo info;

		/// <summary>
		/// 解析身份证号信息
		/// </summary>
		/// <param name="IDCard"></param>
		/// <returns></returns>
        public AnalysisIDCard(string IDCard)
        {
            info = IDCardAnalysis.IDCardInfo(IDCard);

            if (info.是否有效)
            {
                // 解析行政区划编码

                bllIDCard_AreaCode_New bllNewCode = new bllIDCard_AreaCode_New();
                bllIDCard_AreaCode_Old bllOldCode = new bllIDCard_AreaCode_Old();

                DataTable dtNewCode = bllNewCode.GetAreaInfo(info.区);
                DataTable dtOldCode = bllOldCode.GetAreaInfo(info.区);

                if (dtNewCode.Rows.Count > 0)
                {
                    info.省 = dtNewCode.Rows[0][tb_IDCard_AreaCode_New.上上级地区].ToString();
                    info.市 = dtNewCode.Rows[0][tb_IDCard_AreaCode_New.上级地区].ToString();
                    info.区 = dtNewCode.Rows[0][tb_IDCard_AreaCode_New.地区].ToString();
                }
                else if (dtOldCode.Rows.Count > 0)
                {
                    info.省 = dtOldCode.Rows[0][tb_IDCard_AreaCode_Old.上上级地区].ToString();
                    info.市 = dtOldCode.Rows[0][tb_IDCard_AreaCode_Old.上级地区].ToString();
                    info.区 = dtOldCode.Rows[0][tb_IDCard_AreaCode_Old.地区].ToString();
                }
                else
                {
                    info.是否有效 = false;
                    info.错误信息 = "行政区划编码错误,错误:" + info.区;
                    info.位数 = default(int);
                    info.二代身份证号 = default(string);
                    info.省 = default(string);
                    info.市 = default(string);
                    info.区 = default(string);
                    info.出生日期 = default(DateTime);
                    info.性别 = default(string);
                }
            }
        }
    }
}
