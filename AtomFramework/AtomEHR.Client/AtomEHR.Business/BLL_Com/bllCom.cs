﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using AtomEHR.Models;
using AtomEHR.Bridge;
using AtomEHR.Common;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Server.DataAccess;

namespace AtomEHR.Business
{
    public class bllCom
    {

        /// <summary>
        /// 从服务器中获取系统时间
        /// </summary>
        /// <returns></returns>
        public string GetDateTimeFromDBServer()
        {
            //DataSet ds = new dalCom(Loginer.CurrentUser).GetBusinessByKey(keyValue);
            return new dalCom(Loginer.CurrentUser).GetDateTimeFromDBServer();
        }

        //计算年龄
        //日期的格式必须限定为yyyy-MM-dd HH:mm:ss
        public int GetAgeByBirthDay(string strBirthday)
        {
            return new dalCom(Loginer.CurrentUser).GetAgeByBirthDay(strBirthday);
        }

        //计算年龄
        //日期的格式必须限定为yyyy-MM-dd HH:mm:ss
        public int GetAgeByBirthDay(string strBirthday, string beginDate)
        {
            return new dalCom(Loginer.CurrentUser).GetAgeByBirthDay(strBirthday, beginDate);
        }

        public DataSet Get工作提醒(string sdate, string edate, string strWhere)
        {
            return new dalCom(Loginer.CurrentUser).Get工作提醒(sdate, edate, strWhere);
        }

        public DataSet Get工作提醒详细(string sdate, string edate, string strWhere, string type)
        {
            return new dalCom(Loginer.CurrentUser).Get工作提醒详细(sdate, edate, strWhere, type);
        }

        public DataTable Get姓名(string tableName,string where ="")
        {
            return new dalCom(Loginer.CurrentUser).Get姓名(tableName, where);
        }

        public SaveResult Get解密(DataTable table, string tableName)
        {
            return new dalCom(Loginer.CurrentUser).Get解密(table, tableName);
        }

        public DataSet GetLogData(string strWhere)
        {
            return new dalCom(Loginer.CurrentUser).GetLogData(strWhere);
        }

        public DataSet Get管理率By机构ID(string jgid)
        {
            return new dalCom(Loginer.CurrentUser).Get管理率By机构ID(jgid);
        }

        public DataSet Get动态管理率(string RGID, string begindate, string enddate)
        { 
            return new dalCom(Loginer.CurrentUser).Get动态管理率(RGID, begindate, enddate); 
        }

        public DataSet Get档案汇总统计(string RGID, string begindate, string enddate)
        {
            return new dalCom(Loginer.CurrentUser).Get档案汇总统计(RGID, begindate, enddate);
        }
		
        //public string Get_APP_Version()
        //{
        //    return new dalCom(Loginer.CurrentUser).Get_APP_Version();
        //}
    }
}
