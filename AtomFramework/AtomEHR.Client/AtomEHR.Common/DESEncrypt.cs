﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace AtomEHR.Common
{
    public class DESEncrypt
    {
        #region 健康档案加密/解密

        public const string THE_KEY = "zhonglianjiayu";
        public const string IV_KEY = "291a4100b5a51b49";

        #region 加密
        /// <summary>
        /// DES加密New
        /// </summary>
        /// <param name="pToEncrypt">要加密的字符串</param>
        /// <returns></returns>
        public static string DESEnCode(string pToEncrypt)
        {
            return DESEnCode(pToEncrypt, THE_KEY);
        }

        /// <summary>
        /// 加密算法
        /// </summary>
        /// <param name="pToEncrypt">要加密的字符串</param>
        /// <param name="Keys">密钥</param>
        /// <returns></returns>
        public static string DESEnCode(string pToEncrypt, string Keys)
        {
            try
            {
                DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
                provider.Mode = CipherMode.ECB;//C#默认CBC模式，解密java需要设置ECB模式
                byte[] inputByteArray = Encoding.GetEncoding("GB2312").GetBytes(pToEncrypt);

                //建立加密对象的密钥和偏移量    
                //原文使用ASCIIEncoding.ASCII方法的GetBytes方法    
                //使得输入密码必须输入英文文本    
                provider.Key = Encoding.ASCII.GetBytes(Keys.Substring(0, 8));
                provider.IV = Encoding.ASCII.GetBytes(Keys.Substring(0, 8));
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, provider.CreateEncryptor(), CryptoStreamMode.Write);

                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                return byteArr2HexStr(ms.ToArray());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 将byte数组转换为表示16进制值的字符串 和public static byte[]    hexStr2ByteArr(String strIn) 互为可逆的转换过程 
        /// </summary>
        /// <param name="arrB"></param>
        /// <returns></returns>
        public static String byteArr2HexStr(byte[] arrB)
        {
            int iLen = arrB.Length;
            // 每个byte用两个字符才能表示，所以字符串的长度是数组长度的两倍   
            StringBuilder sb = new StringBuilder(iLen * 2);
            for (int i = 0; i < iLen; i++)
            {
                int intTmp = arrB[i];
                // 把负数转换为正数   
                while (intTmp < 0)
                {
                    intTmp = intTmp + 256;
                }
                // 小于0F的数需要在前面补0   
                if (intTmp < 16)
                {
                    sb.Append("0");
                }
                sb.Append(Convert.ToString(intTmp, 16));
            }
            return sb.ToString();
        }
        #endregion

        #region 解密

        /// <summary>
        /// DES解密 -java des加密字符串
        /// </summary>
        /// <param name="str">加密字符串</param>
        /// <returns></returns>
        public static string DESDeCode(string pToDecrypt)
        {
            return DESDeCode(pToDecrypt, THE_KEY);
        }

        /// <summary>
        /// 解密java des加密字符串+1
        /// </summary>
        /// <param name="str">加密字符串</param>
        ///<param name="Keys">密钥</param>
        /// <returns></returns>
        public static string DESDeCode(string pToDecrypt, string Keys)
        {
            try
            {
                DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
                provider.Mode = CipherMode.ECB;//C#默认CBC模式，解密java需要设置ECB模式
                // 密钥
                provider.Key = Encoding.ASCII.GetBytes(Keys.Substring(0, 8));
                // 偏移量
                provider.IV = Encoding.ASCII.GetBytes(Keys.Substring(0, 8));
                byte[] buffer = hexStr2ByteArr(pToDecrypt);
                //byte[] buffer = new byte[pToDecrypt.Length / 2];
                //for (int i = 0; i < (pToDecrypt.Length / 2); i++)
                //{
                //    int num2 = Convert.ToInt32(pToDecrypt.Substring(i * 2, 2), 0x10);
                //    buffer[i] = (byte)num2;
                //}
                MemoryStream stream = new MemoryStream();
                CryptoStream stream2 = new CryptoStream(stream, provider.CreateDecryptor(), CryptoStreamMode.Write);
                stream2.Write(buffer, 0, buffer.Length);
                stream2.FlushFinalBlock();
                stream.Close();
                return Encoding.GetEncoding("GB2312").GetString(stream.ToArray());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                //return ""; 
            }
        }

        /// <summary>
        ///将表示16进制值的字符串转换为byte数组 和public static String byteArr2HexStr(byte[] arrB)  * 互为可逆的转换过程 
        /// </summary>
        /// <param name="strIn"></param>
        /// <returns></returns>
        public static byte[] hexStr2ByteArr(String strIn)
        {
            // 两个字符表示一个字节，所以字节数组长度是字符串长度除以2   
            int iLen = strIn.Length / 2;
            byte[] arrOut = new byte[iLen];
            for (int i = 0; i < iLen; i++)
            {
                arrOut[i] = (byte)Convert.ToInt32(strIn.Substring(i * 2, 2), 16);
            }
            return arrOut;
        }
        #endregion

        #endregion


        /// <summary>
        /// 个人姓名加密
        /// </summary>
        /// <param name="s原始字符串"></param>
        /// <returns></returns>
        public static String DES加密(string s原始字符串)
        {
            return s原始字符串;
            try
            {
                String jiemi = "";
                int len = s原始字符串.Length;
                for (int i = 0; i < len; i++)
                {
                    jiemi += DESEncrypt.DESEnCode(s原始字符串.Substring(i, 1));
                }
                return jiemi;
            }
            catch (Exception)
            {
                return s原始字符串;
            }

            //if (s原始字符串.Length > 3)
            //    return DESEncrypt.DESEnCode(s原始字符串);
            //else
            //{
            //    String jiemi = "";
            //    int len = s原始字符串.Length;
            //    for (int i = 0; i < len; i++)
            //    {
            //        jiemi += DESEncrypt.DESEnCode(s原始字符串.Substring(i, 1));
            //    }
            //    return jiemi;
            //}
        }

        /// <summary>
        /// 个人姓名解密
        /// </summary>
        /// <param name="s加密字符串"></param>
        /// <returns></returns>
        public static String DES解密(string s加密字符串)
        {
            try
            {
                if (s加密字符串.Length < 16) return s加密字符串;
                String jiemi = "";
                int len = s加密字符串.Length / 16;
                for (int i = 0; i < len; i++)
                {
                    jiemi += DESEncrypt.DESDeCode(s加密字符串.Substring(i * 16, 16));
                }
                return jiemi;
            }
            catch (Exception)
            {
                return s加密字符串;
            }
        }

        /// <summary>
        /// 获取汉字首字母（可包含多个汉字）
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public static string GetChineseSpell(string strText)
        {
            int len = strText.Length;
            string myStr = "";
            for (int i = 0; i < len; i++)
            {
                myStr += getSpell(strText.Substring(i, 1));
            }
            return myStr;
        }

        public static string getSpell(string cnChar)
        {
            byte[] arrCN = Encoding.Default.GetBytes(cnChar);
            if (arrCN.Length > 1)
            {
                int area = (short)arrCN[0];
                int pos = (short)arrCN[1];
                int code = (area << 8) + pos;
                int[] areacode = { 45217, 45253, 45761, 46318, 46826, 47010, 47297, 47614, 48119, 48119, 49062, 49324, 49896, 50371, 50614, 50622, 50906, 51387, 51446, 52218, 52698, 52698, 52698, 52980, 53689, 54481 };
                for (int i = 0; i < 26; i++)
                {
                    int max = 55290;
                    if (i != 25) max = areacode[i + 1];
                    if (areacode[i] <= code && code < max)
                    {
                        return Encoding.Default.GetString(new byte[] { (byte)(65 + i) });
                    }
                }
                return "*";
            }
            else
                return cnChar;
        }

        /// <summary>
        /// 获取第一个汉字的首字母，只能输入汉字
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static string GetPYChar(string c)
        {
            byte[] array = new byte[2];
            array = System.Text.Encoding.Default.GetBytes(c);
            int i = (short)(array[0] - '\0') * 256 + ((short)(array[1] - '\0'));
            if (i < 0xB0A1) return "*";
            if (i < 0xB0C5) return "A";
            if (i < 0xB2C1) return "B";
            if (i < 0xB4EE) return "C";
            if (i < 0xB6EA) return "D";
            if (i < 0xB7A2) return "E";
            if (i < 0xB8C1) return "F";
            if (i < 0xB9FE) return "G";
            if (i < 0xBBF7) return "H";
            if (i < 0xBFA6) return "J";
            if (i < 0xC0AC) return "K";
            if (i < 0xC2E8) return "L";
            if (i < 0xC4C3) return "M";
            if (i < 0xC5B6) return "N";
            if (i < 0xC5BE) return "O";
            if (i < 0xC6DA) return "P";
            if (i < 0xC8BB) return "Q";
            if (i < 0xC8F6) return "R";
            if (i < 0xCBFA) return "S";
            if (i < 0xCDDA) return "T";
            if (i < 0xCEF4) return "W";
            if (i < 0xD1B9) return "X";
            if (i < 0xD4D1) return "Y";
            if (i < 0xD7FA) return "Z";
            return "*";
        }
    }
}
