﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentFTP;
using System.IO;

namespace AtomEHR.Common
{
    //结构:有文件类型文件名称俩元素
    public struct NameAndType
    {
        public string fileType;
        public string fileName;
    }
    /// <summary>
    /// FTP操作类封装,实现简单的Select,Upload,Download,Delete
    /// </summary>
    public class FtpTools
    {
        private static string _ServerIP;
        private static string _FtpUser;
        private static string _FtpPwd;

        /// <summary>
        /// 设置FTP地址,登录名,密码
        /// </summary>
        /// <param name="ServerIP"></param>
        /// <param name="FtpUser"></param>
        /// <param name="FtpPwd"></param>
        public static void FtpLogon(string ServerIP, string FtpUser, string FtpPwd)
        {
            _ServerIP = ServerIP;
            _FtpUser = FtpUser;
            _FtpPwd = FtpPwd;
        }
        /// <summary>
        /// 创建FTP连接
        /// </summary>
        /// <returns></returns>
        private static FtpClient ftpConn()
        {
            FtpTrace.EnableTracing = false;

            FtpClient fc = new FtpClient(_ServerIP, _FtpUser, _FtpPwd);
            fc.Connect();

            return fc;
        }

        /// <summary>
        /// 展示文件
        /// </summary>
        /// <returns></returns>
        public static List<NameAndType> Select()
        {
            List<NameAndType> list_NT = new List<NameAndType>();

            foreach (FtpListItem item in ftpConn().GetListing())    //GetListing获取文件/目录集合
            {
                NameAndType nt = new NameAndType();
                switch (item.Type)  //文件类别
                {
                    case FtpFileSystemObjectType.File:          //基本文件类别
                        nt.fileType = "File";
                        nt.fileName = item.Name;
                        list_NT.Add(nt);
                        break;
                    case FtpFileSystemObjectType.Directory:     //文件夹/目录类别
                        nt.fileType = "Directory";
                        nt.fileName = item.Name;
                        list_NT.Add(nt);
                        break;
                    case FtpFileSystemObjectType.Link:          //链接类别
                        nt.fileType = "Link";
                        nt.fileName = item.Name;
                        list_NT.Add(nt);
                        break;
                    default:
                        break;
                }
            }

            ftpConn().Disconnect();
            return list_NT;
        }

        /// <summary>
        /// 展示指定文件
        /// </summary>
        /// <param name="list_filePath"></param>
        /// <returns></returns>
        public static List<NameAndType> Select(List<string> list_filePath)
        {
            List<NameAndType> list_NT = new List<NameAndType>();

            foreach (string filePath in list_filePath)    //GetListing获取文件/目录集合
            {
                FtpListItem item = ftpConn().GetListing(filePath)[0];
                NameAndType nt = new NameAndType();
                switch (item.Type)  //文件类别
                {
                    case FtpFileSystemObjectType.File:          //基本文件类别
                        nt.fileType = "File";
                        nt.fileName = item.Name;
                        list_NT.Add(nt);
                        break;
                    case FtpFileSystemObjectType.Directory:     //文件夹/目录类别
                        nt.fileType = "Directory";
                        nt.fileName = item.Name;
                        list_NT.Add(nt);
                        break;
                    case FtpFileSystemObjectType.Link:          //链接类别
                        nt.fileType = "Link";
                        nt.fileName = item.Name;
                        list_NT.Add(nt);
                        break;
                    default:
                        break;
                }
            }

            ftpConn().Disconnect();
            return list_NT;
        }

        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="Path_Host"></param>
        /// <param name="Path_Server"></param>
        /// <returns></returns>
        public static int Upload(string Path_Host, string Path_Server)
        {

            if (ftpConn().FileExists(Path_Server)) //检测文件是否存在于服务器
            {
                ftpConn().Disconnect();
                return 2;
            }
            else
            {
                ftpConn().UploadFile(Path_Host, Path_Server);   //UploadFile()上传单个文件,UploadFiles()可以批量上传

                ftpConn().Disconnect();
                return 1;
            }
        }

        /// <summary>
        /// 下载文件
        /// </summary>
        /// <param name="Path_Host"></param>
        /// <param name="Path_Server"></param>
        /// <returns></returns>
        public static int Download(string Path_Host, string Path_Server)
        {
            string str_savePath = Path_Host + "/" + Path.GetFileName(Path_Server);

            if (ftpConn().FileExists(Path_Server))
            {
                ftpConn().DownloadFile(str_savePath, Path_Server);     //DownloadFile()下载单个文件,DownloadFiles()批量下载

                ftpConn().Disconnect();
                return 1;
            }
            else
            {
                ftpConn().Disconnect();
                return 2;
            }
        }

        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="fileType"></param>
        /// <param name="Path_Server"></param>
        /// <returns></returns>
        public static int Delete(string fileType, string Path_Server)
        {
            string str_Path = string.Empty;

            if ("Directory".Equals(fileType))
            {

                if (ftpConn().DirectoryExists(Path_Server))
                {
                    ftpConn().DeleteDirectory(Path_Server);    //DeleteDirectory()删除目录(会递归删除目录下的所有子目录及其文件)

                    ftpConn().Disconnect();
                    return 1;
                }
                else
                {
                    ftpConn().Disconnect();
                    return 2;
                }

            }
            else
            {
                if (ftpConn().FileExists(Path_Server))
                {
                    ftpConn().DeleteFile(Path_Server);     //DeleteFile()删除单个文件

                    ftpConn().Disconnect();
                    return 1;
                }
                else
                {
                    ftpConn().Disconnect();
                    return 2;
                }
            }
        }
    }
    //wxf 2018年9月30日 18:11:49
}
