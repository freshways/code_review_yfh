﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AtomEHR.Common
{
    public static class CardIDHelper
    {

        //15位身份证号转18位身份证号
        public static bool ChangeSFZH(string oldsfzh, out string newsfzh)
        {
            bool bret = false;
            string temp = "";
            newsfzh = "";

            try
            {
                if (Regex.IsMatch(oldsfzh, @"^\d{15}$"))
                {
                    temp = oldsfzh.Substring(0, 6) + "19" + oldsfzh.Substring(6);

                    int[] i系数 = { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 };
                    int[] iSFZH = new int[17];
                    for (int index = 0; index < 17; index++)
                    {
                        iSFZH[index] = temp[index] - 48;
                    }

                    //将17位数字分别和系数相乘，然后结果相加
                    int sum = 0;
                    for (int index = 0; index < 17; index++)
                    {
                        sum += iSFZH[index] * i系数[index];
                    }

                    //用加出来和除以11，取余数
                    int mod = sum % 11;
                    char[] c检验码 = { '1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2' };

                    newsfzh = temp + c检验码[mod];

                    bret = true;
                }
            }
            catch (Exception ex)
            {

            }
            return bret;
        }

        public static bool Check(string sfzh)
        {
            bool bret = false;
            if(string.IsNullOrWhiteSpace(sfzh))
            {
            }
            else if(sfzh.Length == 15)
            {
                bret = Check15(sfzh);
            }
            else if(sfzh.Length==18)
            {
                bret = Check18(sfzh);
            }
            else
            {
                
            }
            return bret;
        }

        public static bool Check15(string sfzh)
        {
            bool bret = false;
            string temp = "";

            if (string.IsNullOrWhiteSpace(sfzh) || sfzh.Length != 15)
            {
                return bret;
            }

            ///不符合基本校验
            if (!Regex.IsMatch(sfzh, @"^\d{15}$"))
            {
                return bret;
            }

            try
            {
                string birth = "19" + sfzh.Substring(6, 2) + "-" + sfzh.Substring(8, 2) + "-" + sfzh.Substring(10, 2);
                DateTime dateBirth = Convert.ToDateTime(birth); 
                bret = true;
            }
            catch
            {
                return bret;
            }

            return bret;
        }

        //检查18位身份证号是否正确
        public static bool Check18(string sfzh)
        {
            bool bret = false;
            string temp = "";

            if (string.IsNullOrWhiteSpace(sfzh) || sfzh.Length != 18)
            {
                return bret;
            }

            ///不符合基本校验
            if (!Regex.IsMatch(sfzh, @"^(\d{18})|(\d{17}(\d|X|x))$"))
            {
                return bret;
            }

            try
            {
                string birth = sfzh.Substring(6, 4) + "-" + sfzh.Substring(10, 2) + "-" + sfzh.Substring(12, 2);
                DateTime dateBirth = Convert.ToDateTime(birth);
            }
            catch
            {
                return bret;
            }

                try
                {
                    ///^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/
                    //if (Regex.IsMatch(sfzh, @"^(\d{15})|(\d{18})|(\d{17}(\d|X|x))$"))
                    //if (Regex.IsMatch(sfzh, @"^(\d{18})|(\d{17}(\d|X|x))$"))
                    //{
                    temp = sfzh.Substring(0, 17);

                    int[] i系数 = { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 };
                    int[] iSFZH = new int[17];
                    for (int index = 0; index < 17; index++)
                    {
                        iSFZH[index] = temp[index] - 48;
                    }

                    //将17位数字分别和系数相乘，然后结果相加
                    int sum = 0;
                    for (int index = 0; index < 17; index++)
                    {
                        sum += iSFZH[index] * i系数[index];
                    }

                    //用加出来和除以11，取余数
                    int mod = sum % 11;
                    char[] c检验码 = { '1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2' };

                    if (c检验码[mod] == sfzh[17] || (sfzh[17] == 'x' && c检验码[mod] == 'X'))
                    {
                        bret = true;
                    }
                }
                catch
                {

                }
            
            return bret;
        }

        public static bool SimpleCheck(string sfzh)
        {
            if (Regex.IsMatch(sfzh, @"^(\d{15})|(\d{18})|(\d{17}(\d|X|x))$"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string GetBirthday(string sfzh)
        {
            if (Regex.IsMatch(sfzh, @"^(\d{15})|(\d{18})|(\d{17}(\d|X|x))$"))
            {
                if (sfzh.Length == 15)
                {
                    return "19" + sfzh.Substring(6, 2) + "-" + sfzh.Substring(8, 2) + "-" + sfzh.Substring(10, 2);
                }
                else //if 长度==18
                {
                    return sfzh.Substring(6, 4) + "-" + sfzh.Substring(10, 2) + "-" + sfzh.Substring(12, 2);
                }
            }
            else
            {
                return "";
            }
        }

        public static string GetSex(string sfzh)
        {
            string SerialNo = "";
            if (Regex.IsMatch(sfzh, @"^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$"))
            {
                if (sfzh.Length == 15)
                {
                    SerialNo = sfzh.Substring(12,3);
                }
                else //if 长度==18
                {
                    SerialNo = sfzh.Substring(14,3);
                }

                try
                {
                    int iserialno = Convert.ToInt32(SerialNo);
                    if(iserialno %2==0)
                    {
                        return "2";//偶数，表示女
                    }
                    else
                    {
                        return "1"; //奇数表示男
                    }
                }
                catch
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        public static string GetAge(string sfzh, DateTime nowdate)
        {
            int age;
            string ret="";
            string strbirth = GetBirthday(sfzh);

            DateTime deBirth;
            try
            {
                deBirth = Convert.ToDateTime(strbirth);
            }
            catch
            {
                string[] arrtemp = strbirth.Split('-');
                if (arrtemp.Length == 3)
                {
                    //
                    deBirth = new DateTime(Convert.ToInt32(arrtemp[0]), Convert.ToInt32(arrtemp[1]), 1);
                }
                else
                {
                    deBirth = DateTime.Now;
                }
            }
            

            age = nowdate.Year - deBirth.Year;
            if(age ==0)
            { 
                age = nowdate.Month-deBirth.Month;
                if(age == 0)
                {
                    age = nowdate.Day - deBirth.Day;
                    if(age> 0)
                    {
                        ret = age.ToString() + "天";
                    }
                }
                else if (age > 0)
                {
                    ret = age.ToString() + "月";
                }
            }
            else if (age > 0)
            {
                ret = age.ToString() + "岁";
            }

            return ret;
        }
    }
}
