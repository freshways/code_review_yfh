﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AtomEHR.Common
{
    //Begin WXF 2018-12-05 | 11:52
    //解析身份证号 
    //
    //End
    public class IDCardAnalysis
    {
        /// <summary>
        /// 解析结果
        /// </summary>
        public struct AnalysisInfo
        {
            public bool 是否有效; //= false;
            public int 位数; //= default(int);
            public string 二代身份证号; //= default(string);
            public string 省; //= default(string);
            public string 市; //= default(string);
            public string 区; //= default(string);
            public DateTime 出生日期; //= default(DateTime);
            public string 性别; //= default(string);
            public string 错误信息; //= default(string);
        }

        static AnalysisInfo info = new AnalysisInfo();

        //初始化解析结果
        static void initializationAnalysisInfo()
        {
            info.是否有效 = false;
            info.位数 = default(int);
            info.二代身份证号 = default(string);
            info.省 = default(string);
            info.市 = default(string);
            info.区 = default(string);
            info.出生日期 = default(DateTime);
            info.性别 = default(string);
            info.错误信息 = default(string);
        }

        /// <summary>
        /// 解析身份证号信息
        /// </summary>
        /// <param name="IDCard"></param>
        /// <returns></returns>
        public static AnalysisInfo IDCardInfo(string IDCard)
        {
            initializationAnalysisInfo();

            string strError = IDCardCheck(IDCard);
            if (strError == default(string))
            {
                info.是否有效 = true;
                info.位数 = IDCard.Length;
                info.二代身份证号 = IDCard.Length == 18 ? IDCard : Convert15To18(IDCard);
                info.省 = IDCard.Substring(0, 2) + "0000";
                info.市 = IDCard.Substring(0, 4) + "00";
                info.区 = IDCard.Substring(0, 6);
                info.出生日期 = DateTime.Parse(AnalysisBirthday(IDCard));
                info.性别 = AnalysisAreaSex(IDCard);
            }
            else
            {
                info.错误信息 = strError;
            }

            return info;
        }

        /// <summary>
        /// 检测身份证号错误异常
        /// </summary>
        /// <param name="IDCard"></param>
        /// <returns></returns>
        static string IDCardCheck(string IDCard)
        {
            if (string.IsNullOrWhiteSpace(IDCard))
            {
                return "身份证号不能为空或空格或NULL";
            }
            if (IDCard.Length != 18 && IDCard.Length != 15)
            {
                return "身份证号位数不是15/18位,错误:" + IDCard.Length;
            }
            if (!Regex.IsMatch(IDCard.Substring(0, 1), @"[1-9]"))
            {
                return "身份证第一位不能是1-9之外的内容,错误:" + IDCard.Substring(0, 1);
            }
            if (!Regex.IsMatch(IDCard.Substring(1, 5), @"[\d]{5}"))
            {
                return "行政区划编码只能是纯数字,错误:" + IDCard.Substring(0, 6);
            }
            DateTime Birthday = default(DateTime);
            string strBirthday = AnalysisBirthday(IDCard);
            if (!DateTime.TryParse(strBirthday, out Birthday))
            {
                return "出生日期异常,错误:" + strBirthday;
            }
            if (IDCard.Length == 18)
            {
                if (!Regex.IsMatch(IDCard, @"[\d]{17}[\dXx]"))
                {
                    return "第二代身份证号必须由17位纯数字加X或x组成,错误:" + IDCard;
                }
                string strCheckCode = IDCordCheckCode(IDCard);
                string strActualCode = IDCard.Substring(17, 1).ToUpper();
                if (!strCheckCode.Equals(strActualCode))
                {
                    return "此身份证号无法通过校验位,校验值:" + strCheckCode + " 实际值:" + strActualCode;
                }
            }
            if (IDCard.Length == 15)
            {
                if (!Regex.IsMatch(IDCard, @"[\d]{15}"))
                {
                    return "第一代身份证号必须由15位纯数字组成,错误:" + IDCard;
                }
            }

            return default(string);
        }

        /// <summary>
        /// 从身份证号中解析出出生日期字符串
        /// </summary>
        /// <param name="IDCard"></param>
        /// <returns></returns>
        static string AnalysisBirthday(string IDCard)
        {
            if (IDCard.Length == 18)
            {
                return IDCard.Substring(6, 4) + "-" + IDCard.Substring(10, 2) + "-" + IDCard.Substring(12, 2);
            }
            else if (IDCard.Length == 15)
            {
                return IDCard.Substring(6, 2) + "-19" + IDCard.Substring(8, 2) + "-" + IDCard.Substring(10, 2);
            }
            else
            {
                return default(string);
            }
        }

        /// <summary>
        /// 从身份证号中解析出省,市,区
        /// </summary>
        /// <param name="IDCard"></param>
        /// <returns></returns>
        static Dictionary<string, string> AnalysisArea(string IDCard)
        {
            Dictionary<string, string> DicArea = new Dictionary<string, string>();
            DicArea.Add("省", IDCard.Substring(0, 2) + "0000");
            DicArea.Add("市", IDCard.Substring(0, 4) + "00");
            DicArea.Add("区", IDCard.Substring(0, 6));

            //这个方法原本是要根据解析的地区码去数据库查询地区名称的
            //但是后来发现AtomEHR.Common这个命名空间没法引用BLL层的命名空间
            //所以此方法暂时不用,地区解析放到BLL层的AnalysisIDCard类中,之后通过调用AnalysisIDCard来解析身份证

            return DicArea;
        }

        /// <summary>
        /// 从身份证号中解析出性别
        /// </summary>
        /// <param name="IDCord"></param>
        /// <returns></returns>
        static string AnalysisAreaSex(string IDCard)
        {
            int iSex = default(int);

            if (IDCard.Length == 18)
            {
                iSex = int.Parse(IDCard.Substring(16, 1));
            }
            if (IDCard.Length == 15)
            {
                iSex = int.Parse(IDCard.Substring(14, 1));
            }

            //true为奇数(男)false为偶数(女) 0为false(偶数)
            return Convert.ToBoolean(iSex % 2) ? "男" : "女";
        }

        /// <summary>
        /// 计算二代身份证校验位
        /// </summary>
        /// <param name="IDCard"></param>
        /// <returns></returns>
        static string IDCordCheckCode(string IDCard)
        {
            // 取身份证号前17位
            string strSeventeenCode = IDCard.Length == 17 ? IDCard : IDCard.Substring(0, 17);
            // 身份证号前17位转为int数组
            int[] isSeventeenCode = Array.ConvertAll<char, int>(strSeventeenCode.ToCharArray(), s => Convert.ToInt32(s.ToString()));

            // 校验系数
            string strCoefficient = "7,9,10,5,8,4,2,1,6,3,7,9,10,5,8,4,2";
            // 校验系数int数组
            int[] isCoefficient = Array.ConvertAll<string, int>(strCoefficient.Split(','), s => int.Parse(s));
            // 对照值
            string strMantissa = "1,0,X,9,8,7,6,5,4,3,2";
            // 对照值数组
            string[] strsMantissa = strMantissa.Split(',');

            // 1、将前面的身份证号码17位数分别乘以不同的系数。从第一位到第十七位的系数分别为：7－9－10－5－8－4－2－1－6－3－7－9－10－5－8－4－2。
            // 2、将这17位数字和系数相乘的结果相加。
            // 3、用加出来和除以11，看余数是多少？
            // 4、余数只可能有0－1－2－3－4－5－6－7－8－9－10这11个数字。其分别对应的最后一位身份证的号码为1－0－X －9－8－7－6－5－4－3－2。
            int iSum = 0;
            for (int i = 0; i < isSeventeenCode.Length; i++)
            {
                iSum += isSeventeenCode[i] * isCoefficient[i];
            }

            int iRemainder = iSum % 11;
            if (iRemainder <= strsMantissa.Length)
            {
                return strsMantissa[iRemainder];
            }
            else
            {
                return default(string);
            }
        }

        /// <summary>
        /// 15位身份证号转18位身份证号
        /// </summary>
        /// <returns></returns>
        static string Convert15To18(string IDCard)
        {
            if (IDCard.Length == 15)
            {
                string newIDCord = default(string);

                newIDCord = IDCard.Substring(0, 6) + "19" + IDCard.Substring(6, 9);
                newIDCord += IDCordCheckCode(newIDCord);

                return newIDCord;
            }
            else
            {
                return default(string);
            }
        }
    }
}
