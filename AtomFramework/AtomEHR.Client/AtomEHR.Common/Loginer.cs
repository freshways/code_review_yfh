///*************************************************************************/
///*
///* 文件名    ：Loginer.cs                                  
///* 程序说明  : 用户登录信息
///* 原创作者  ：ATOM 
///* 
///* Copyright ©  2015 GGBond
///**************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace AtomEHR.Common
{
    /// <summary>
    /// 系统支持３种登录授权策略
    /// </summary>
    public enum LoginAuthType
    {
        /// <summary>
        /// 系统内部用户授权
        /// </summary>
        LocalSystemAuth = 1,

        /// <summary>
        /// Novell网用户授权
        /// </summary>
        NovellUserAuth = 2,

        /// <summary>
        /// Windows域用户授权
        /// </summary>
        WindowsDomainAuth = 3
    }

    /// <summary>
    /// 当前登录的用户信息
    /// </summary>
    [Serializable]
    public class Loginer
    {
        private string _Account = "";
        private string _AccountName = "";
        private string _Email = "";
        private DateTime _LoginTime;
        private LoginAuthType _LoginAuthType = LoginAuthType.LocalSystemAuth;
        private string _FlagAdmin = "N";
        private string _DataSetID;
        private string _DataSetName;
        private string _DBName;
        private string _版本;
        private string _单位代码; //2016-12-29 17:13:28 yufh 添加：记录当前所属县区代码
        private string _程序名称;//2017-05-16 09:51:21 yufh 添加：记录当前县区的程序名称

        /// <summary>
        /// 当前卫生局的代码
        /// </summary>
        public string 单位代码
        {
            get { return _单位代码; }
            set { _单位代码 = value; }
        }

        /// <summary>
        /// 当前卫生局的程序名称
        /// </summary>
        public string 程序名称
        {
            get { return _程序名称; }
            set { _程序名称 = value; }
        }

        private string _所属机构;

        public string 所属机构
        {
            get { return _所属机构; }
            set { _所属机构 = value; }
        }

        //add by wjz 20150902 添加用户ID、用户编码的获取 ▽
        //用户的主键值
        private string _isid = "";

        public string Isid
        {
            get { return _isid; }
            set { _isid = value; }
        }

        //用户编号
        private string _用户编码 = "";

        public string 用户编码
        {
            get { return _用户编码; }
            set { _用户编码 = value; }
        }

        private string _所属机构名称="";
        public string 所属机构名称
        {
            get { return _所属机构名称; }
            set { _所属机构名称 = value; }
        }
        //add by wjz 20150902 添加用户ID、用户编码的获取 △

        public string 版本 { get { return _版本; } set { _版本 = value; } }

        public string  身份证 { get; set; } //2017-02-14 09:41:08 yufh 添加 记录用户身份证，李主任要求

        /// <summary>
        /// 用户帐号，登录帐号
        /// </summary>
        public string Account { get { return _Account; } set { _Account = value; } }

        /// <summary>
        /// 用户名
        /// </summary>
        public string AccountName { get { return _AccountName; } set { _AccountName = value; } }

        /// <summary>
        /// 用户的邮件地址
        /// </summary>
        public string Email { get { return _Email; } set { _Email = value; } }

        /// <summary>
        /// 登录时间
        /// </summary>
        public DateTime LoginTime { get { return _LoginTime; } set { _LoginTime = value; } }

        /// <summary>
        /// 登录授权策略
        /// </summary>
        public LoginAuthType LoginAuthType
        {
            get { return _LoginAuthType; }
            set { _LoginAuthType = value; }
        }

        /// <summary>
        /// ADMIN标记
        /// </summary>
        public string FlagAdmin { get { return _FlagAdmin; } set { _FlagAdmin = value; } }

        /// <summary>
        /// 帐套编号
        /// </summary>
        public string DataSetID { get { return _DataSetID; } set { _DataSetID = value; } }

        /// <summary>
        /// 帐套名称
        /// </summary>
        public string DataSetName { get { return _DataSetName; } set { _DataSetName = value; } }

        /// <summary>
        /// 数据库名
        /// </summary>
        public string DBName { get { return _DBName; } set { _DBName = value; } }

        /// <summary>
        /// 是否ADMIN，所有权限
        /// </summary>
        /// <returns></returns>
        public bool IsAdmin()
        {
            return _FlagAdmin == "Y";
        }

        /// <summary>
        /// 给妇幼 和有需要特殊权限跨机构修改档案
        /// </summary>
        /// <returns></returns>
        public bool IsSubAdmin()
        {
            return _FlagAdmin == "F";
        }

        private static Loginer _User = null;

        /// <summary>
        /// 当前登录的用户
        /// </summary>
        public static Loginer CurrentUser
        {
            get
            {
                if (_User == null) _User = new Loginer(); //空对象
                return _User;
            }
            set
            {
                _User = value;
            }
        }
    }

    /// <summary>
    /// 用于登录及修改密码的实体类
    /// </summary>
    [Serializable]
    public class LoginUser
    {
        private string _Account;
        private string _Password;
        private string _DataSetID;
        private string _DataSetDBName;
        private string _Version;
        private string _IP;

        public LoginUser() { }

        public LoginUser(string account, string password, string dataSetID, string dataSetDBName,string Version,string IP)
        {
            _Account = account;
            _Password = password;
            _DataSetID = dataSetID;
            _DataSetDBName = dataSetID;//dataSetDBName; 2015-12-23 09:33:49 yufh，为了减少替换把数据库名统一换成id，根据id获取连接
            _Version = Version; //2016-01-07 14:39:44 yufh 添加
            _IP = IP; //2019年4月18日 yufh 添加
        }

        /// <summary>
        /// 用户帐号，登录帐号
        /// </summary>
        public string Account { get { return _Account; } set { _Account = value; } }

        /// <summary>
        /// 登录密码
        /// </summary>
        public string Password { get { return _Password; } set { _Password = value; } }

        /// <summary>
        /// 当前登录的帐套
        /// </summary>
        public string DataSetID { get { return _DataSetID; } set { _DataSetID = value; } }

        /// <summary>
        /// 帐套的数据库名
        /// </summary>
        public string DataSetDBName { get { return _DataSetDBName; } set { _DataSetDBName = value; } }

        /// <summary>
        /// 当前版本
        /// </summary>
        public string Version { get { return _Version; } set { _Version = value; } }

        /// <summary>
        /// 登陆IP
        /// </summary>
        public string IP { get { return _IP; } set { _IP = value; } }

    }

}

