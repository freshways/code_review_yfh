﻿using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.公共卫生.Module.个人健康;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生
{
    public partial class frm工作提醒 : frmBase
    {

        public string _type = string.Empty;
        public string _dateType = string.Empty;
        private DataSet ds = null;
        string strWhere = string.Empty;
        AtomEHR.Business.BLL_Business.bllUnknow _Bll = new Business.BLL_Business.bllUnknow();
        public frm工作提醒()
        {
            InitializeComponent();
        }
        public frm工作提醒(string type, string dateType)
        {
            InitializeComponent();
            _type = type;
            _dateType = dateType;
        }

        private void link档案编号_Click(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.HyperLinkEdit link = (DevExpress.XtraEditors.HyperLinkEdit)sender;
            if (link == null) return;
            DevExpress.XtraGrid.GridControl gc = (DevExpress.XtraGrid.GridControl)link.Parent;
            if (gc == null) return;

            DataRow row = ((DevExpress.XtraGrid.Views.Grid.GridView)gc.MainView).GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            ////_BLL.GetBusinessByKey(个人档案编号, true);//下载一个空业务单据            
            ////_BLL.NewBusiness(); //增加一条主表记录
            frm个人健康 frm = new frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, null);
            frm.Show();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            strWhere = string.Empty;

            if (!string.IsNullOrEmpty(textEdit1.Text.Trim()) && "请选择".Equals(comboBoxEdit个人信息.Text.Trim()))
            {
                XtraMessageBox.Show("请选择查询条件类型");
                return;
            }
            if (this.comboBoxEdit个人信息.Text.Trim() != "请选择")
            {
                string index = this.comboBoxEdit个人信息.Text.Trim();
                string text = textEdit1.Text.Trim();
                if (index == "姓名")
                {
                    strWhere += " and b.姓名 = '" + util.DESEncrypt.DES加密(text) + "' ";
                }
                else if (index == "档案号")//档案号   
                {
                    strWhere += " and b.个人档案编号 = '" + text + "' ";
                }
                else if (index == "身份证")//身份证
                {
                    strWhere += " and b.身份证号 = '" + text + "' ";
                }
            }

            if (this.comboBoxEdit镇.Text.Trim() != "" && this.comboBoxEdit镇.Text.Trim() != "请选择")
            {
                strWhere += " and b.街道 = '" + util.ControlsHelper.GetComboxKey(this.comboBoxEdit镇) + "' ";
            }
            if (this.comboBoxEdit村.Text.Trim() != "" && this.comboBoxEdit村.Text.Trim() != "请选择")
            {
                strWhere += " and b.居委会= '" + util.ControlsHelper.GetComboxKey(this.comboBoxEdit村) + "' ";
            }
            string sdate = this.ucDtpToDtp1.Dte1.Text.Trim();
            string edate = this.ucDtpToDtp1.Dte2.Text.Trim();
            ds = new Business.bllCom().Get工作提醒详细(sdate, edate, strWhere, _type);
            BindData();
        }

        private void frm工作提醒_Load(object sender, EventArgs e)
        {
            frmGridCustomize.RegisterGrid(gv儿童);
            frmGridCustomize.RegisterGrid(gv高血压);
            frmGridCustomize.RegisterGrid(gv冠心病);
            frmGridCustomize.RegisterGrid(gv健康体检);
            frmGridCustomize.RegisterGrid(gv精神病);
            frmGridCustomize.RegisterGrid(gv老年人);
            frmGridCustomize.RegisterGrid(gv脑卒中);
            frmGridCustomize.RegisterGrid(gv糖尿病);
            frmGridCustomize.RegisterGrid(gv孕产妇);
            //为“镇”绑定信息
            DataView dv镇 = new DataView(DataDictCache.Cache.t地区信息);
            dv镇.RowFilter = "上级编码='" + Loginer.CurrentUser.单位代码 + "' ";
            //DataBinder.BindingLookupEditDataSource(lookUpEdit镇, dv市.ToTable(), "地区名称", "地区编码");
            util.ControlsHelper.BindComboxData(dv镇.ToTable(), comboBoxEdit镇, "地区编码", "地区名称");



            this.ucDtpToDtp1.Dte1.Width = 90;
            this.ucDtpToDtp1.Dte2.Width = 90;
            this.tab.Dock = DockStyle.Fill;
            BindDataForGrid();
        }
        //public DateTime GetWeekLastDaySun(DateTime datetime)
        //{
        //    //星期天为最后一天  
        //    int weeknow = Convert.ToInt32(datetime.DayOfWeek);
        //    weeknow = (weeknow == 0 ? 7 : weeknow);
        //    int daydiff = (7 - weeknow);

        //    //本周最后一天  
        //    string LastDay = datetime.AddDays(daydiff).ToString("yyyy-MM-dd");
        //    return Convert.ToDateTime(LastDay);
        //}
        private void BindDataForGrid()
        {
            if (!string.IsNullOrEmpty(_dateType))
            {
                DateTime dt = DateTime.Now;
                switch (_dateType)
                {
                    case "本季度随访":
                        //this.ucDtpToDtp1.Dte1.Text = DateTime.Now.AddMonths(0 - (DateTime.Now.Month - 1) % 3).ToString("yyyy-MM-01");
                        //this.ucDtpToDtp1.Dte2.Text = DateTime.Now.ToString("yyyy-MM-dd");
                        //wxf 2018年8月29日 16:31:05 修改本季度随访默认日期范围为季度初到季度末的日期范围
                        DateTime startQuarter = dt.AddMonths(0 - (dt.Month - 1) % 3).AddDays(1 - dt.Day);  //本季度初
                        DateTime endQuarter = startQuarter.AddMonths(3).AddDays(-1);  //本季度末
                        this.ucDtpToDtp1.Dte1.Text = startQuarter.ToString("yyyy-MM-dd");
                        this.ucDtpToDtp1.Dte2.Text = endQuarter.ToString("yyyy-MM-dd");
                        break;
                    case "今日随访":
                        this.ucDtpToDtp1.Dte1.Text = DateTime.Now.ToString("yyyy-MM-dd");
                        this.ucDtpToDtp1.Dte2.Text = DateTime.Now.ToString("yyyy-MM-dd");
                        break;
                    case "明日随访":
                        this.ucDtpToDtp1.Dte1.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                        this.ucDtpToDtp1.Dte2.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                        break;
                    case "本周随访":
                        //this.ucDtpToDtp1.Dte1.Text = DateTime.Now.ToString("yyyy-MM-dd");
                        //this.ucDtpToDtp1.Dte2.Text = GetWeekLastDaySun(DateTime.Now).ToString("yyyy-MM-dd");
                        //wxf 2018年8月29日 16:39:27 修改本周随访默认日期范围为周一到周日对应的日期范围
                        DateTime startWeek = dt.AddDays(1 - Convert.ToInt32(dt.DayOfWeek.ToString("d")));  //本周周一
                        DateTime endWeek = startWeek.AddDays(6);  //本周周日
                        this.ucDtpToDtp1.Dte1.Text = startWeek.ToString("yyyy-MM-dd");
                        this.ucDtpToDtp1.Dte2.Text = endWeek.ToString("yyyy-MM-dd");
                        break;
                    case "未体检":
                        this.ucDtpToDtp1.Dte1.Text = DateTime.Now.Year + "01-01";
                        this.ucDtpToDtp1.Dte2.Text = DateTime.Now.ToString("yyyy-MM-dd");
                        break;
                    case "已体检":
                        this.ucDtpToDtp1.Dte1.Text = DateTime.Now.Year + "01-01";
                        this.ucDtpToDtp1.Dte2.Text = DateTime.Now.ToString("yyyy-MM-dd");
                        break;
                    default:
                        break;
                }

                if (!string.IsNullOrEmpty(_type))
                {
                    switch (_type)
                    {
                        case "儿童":
                            string oldName = this.tab.SelectedTabPage.Name;
                            this.tab.SelectedTabPage = tab儿童;
                            //如果默认Page就是tab儿童则手动加载数据 wxf 2018年8月30日 18:30:57
                            if (oldName.Equals("tab儿童"))
                            {
                                BindData();
                            }
                            break;
                        case "孕产妇":
                            this.tab.SelectedTabPage = tab孕产妇;
                            break;
                        case "老年人":
                            this.tab.SelectedTabPage = tab老年人;
                            break;
                        case "高血压":
                            this.tab.SelectedTabPage = tab高血压;
                            break;
                        case "糖尿病":
                            this.tab.SelectedTabPage = tab糖尿病;
                            break;
                        case "精神病":
                            this.tab.SelectedTabPage = tab精神病;
                            break;
                        case "脑卒中":
                            this.tab.SelectedTabPage = tab脑卒中;
                            break;
                        case "冠心病":
                            this.tab.SelectedTabPage = tab冠心病;
                            break;
                        default:
                            break;
                    }
                    // BindData();
                }
            }
        }

        private void BindData()
        {
            string sdate = this.ucDtpToDtp1.Dte1.Text.ToString();
            string edate = this.ucDtpToDtp1.Dte2.Text.ToString();
            AtomEHR.Business.bllCom bllCom = new Business.bllCom();
            ds = bllCom.Get工作提醒详细(sdate, edate, strWhere, _type);
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                DataTable table = ds.Tables[i];
                for (int j = 0; j < table.Rows.Count; j++)
                {
                    table.Rows[j]["姓名"] = util.DESEncrypt.DES解密(table.Rows[j]["姓名"].ToString());
                    table.Rows[j]["性别"] = _Bll.ReturnDis字典显示("xb_xingbie", table.Rows[j]["性别"].ToString());
                    table.Rows[j]["居住地址"] = _Bll.Return地区名称(table.Rows[j]["街道"].ToString()) + _Bll.Return地区名称(table.Rows[j]["居委会"].ToString()) + table.Rows[j]["居住地址"].ToString();
                }
            }
            if (!string.IsNullOrEmpty(_type))
            {
                switch (_type)
                {
                    case "儿童":
                        this.gc儿童.DataSource = ds.Tables[0];
                        this.gv儿童.BestFitColumns();
                        break;
                    case "孕产妇":
                        this.gc孕产妇.DataSource = ds.Tables[0];
                        this.gv孕产妇.BestFitColumns();
                        break;
                    case "老年人":
                        this.gc老年人.DataSource = ds.Tables[0];
                        this.gv老年人.BestFitColumns();
                        break;
                    case "高血压":
                        this.gc高血压.DataSource = ds.Tables[0];
                        this.gv高血压.BestFitColumns();
                        break;
                    case "糖尿病":
                        this.gc糖尿病.DataSource = ds.Tables[0];
                        this.gv糖尿病.BestFitColumns();
                        break;
                    case "精神病":
                        this.gc精神病.DataSource = ds.Tables[0];
                        this.gv精神病.BestFitColumns();
                        break;
                    case "脑卒中":
                        this.gc脑卒中.DataSource = ds.Tables[0];
                        this.gv脑卒中.BestFitColumns();
                        break;
                    case "冠心病":
                        this.gc冠心病.DataSource = ds.Tables[0];
                        this.gv冠心病.BestFitColumns();
                        break;
                    default:
                        break;
                }
            }
        }

        private void tab_TabIndexChanged(object sender, EventArgs e)
        {

        }

        private void tab_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            DevExpress.XtraTab.XtraTabPage selectedTab = this.tab.SelectedTabPage;
            _type = selectedTab.Tag != null ? selectedTab.Tag.ToString() : "";
            BindData();
        }

        private void ShowGrid(string tabName)
        {
            if (ds == null || ds.Tables.Count == 0) return;

            switch (tabName)
            {
                case "tab儿童":
                    this.tab.SelectedTabPage = tab儿童;
                    this.tab儿童.Controls.Add(gc儿童);
                    this.gc儿童.DataSource = ds.Tables[0];
                    this.gv儿童.BestFitColumns();
                    break;
                case "tab孕产妇":
                    this.tab.SelectedTabPage = tab孕产妇;
                    this.tab孕产妇.Controls.Add(gc儿童);
                    this.gc儿童.DataSource = ds.Tables[1];
                    this.gv儿童.BestFitColumns();
                    break;
                case "tab老年人":
                    this.tab.SelectedTabPage = tab老年人;
                    this.gc儿童.DataSource = ds.Tables[2];
                    this.gv儿童.BestFitColumns();
                    break;
                case "tab高血压":
                    this.tab.SelectedTabPage = tab高血压;
                    this.gc儿童.DataSource = ds.Tables[3];
                    this.gv儿童.BestFitColumns();
                    break;
                case "tab糖尿病":
                    this.tab.SelectedTabPage = tab糖尿病;
                    this.gc儿童.DataSource = ds.Tables[4];
                    this.gv儿童.BestFitColumns();
                    break;
                case "tab精神病":
                    this.tab.SelectedTabPage = tab精神病;
                    this.gc儿童.DataSource = ds.Tables[5];
                    this.gv儿童.BestFitColumns();
                    break;
                default:
                    break;
            }
        }

        private void comboBoxEdit镇_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv村 = new DataView(DataDictCache.Cache.t地区信息);
                dv村.RowFilter = "上级编码='" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'";
                //DataBinder.BindingLookupEditDataSource(lookUpEdit村, dv县.ToTable(), "地区名称", "地区编码");
                util.ControlsHelper.BindComboxData(dv村.ToTable(), comboBoxEdit村, "地区编码", "地区名称");
                util.ControlsHelper.SetComboxNullData("", comboBoxEdit村, "请选择居/村委会");
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }

    }
}
