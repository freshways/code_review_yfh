﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Models;
using AtomEHR.Business;
using DevExpress.XtraEditors.Controls;
using AtomEHR.Business.Security;
using System.IO;

namespace AtomEHR.公共卫生
{
    public partial class frm通知通告管理 : AtomEHR.Library.frmBaseBusinessForm
    {
        private string strWhere = string.Empty;
        private string str_GUID = string.Empty;
        private string str_RootPath = string.Empty;
        private string str_RootUser = string.Empty;
        private string str_RootOrg = string.Empty;
        private Loginer logInfo = Loginer.CurrentUser;
        private DataTable dt_通知通告;
        private DataSet ds_通知通告;
        private DataSet ds_通知通告_附件;
        private bll通知通告表 bll_通知 = new bll通知通告表();
        private bll通知通告表_附件 bll_附件 = new bll通知通告表_附件();
        private bllFtpUserConfig bll_Ftp = new bllFtpUserConfig();
        private UpdateType ut = UpdateType.None;
        private List<string> list_filePath = new List<string>();
        private List<string> list_Delete = new List<string>();

        public frm通知通告管理()
        {
            InitializeComponent();
            ShowConfig();
            FtpInfo();
        }

        private void sBut_查询_Click(object sender, EventArgs e)
        {
            strWhere = string.Empty;
            //if (!string.IsNullOrEmpty(DateEdit_创建时间查询1.Text) && !string.IsNullOrEmpty(DateEdit_创建时间查询2.Text))
            //{
            //    DateTime dt1 = Convert.ToDateTime(DateEdit_创建时间查询1.Text);
            //    DateTime dt2 = Convert.ToDateTime(DateEdit_创建时间查询2.Text);
            //    if (DateTime.Compare(dt1, dt2) > 0) MessageBox.Show("起始查询日期不能晚于终止查询日期!");
            //    return;
            //}
            //strWhere += @" and (a.createOrg='" + LookUp_创建机构查询.EditValue + "' or a.createOrg='371328') and (a.creator='" + LookUp_创建人查询.EditValue + "' or a.creator='3713280000')";
            //if (!string.IsNullOrEmpty(DateEdit_创建时间查询1.Text))
            //{
            //    strWhere += @" and a.createTime >='" + DateEdit_创建时间查询1.Text + "'";
            //}
            //if (!string.IsNullOrEmpty(DateEdit_创建时间查询2.Text))
            //{
            //    strWhere += @" and a.createTime <='" + DateEdit_创建时间查询2.Text + "'";
            //}
            //if (!string.IsNullOrEmpty(ComboBoxEdit_置顶.Text) && !"请选择".Equals(ComboBoxEdit_置顶.Text))
            //{
            //    if (ComboBoxEdit_置顶.Text.Equals("置顶"))
            //    {
            //        strWhere = @" and a.setTop='1'";
            //    }
            //    else
            //    {
            //        strWhere = @" and a.setTop='0'";
            //    }
            //}
            //if (!string.IsNullOrWhiteSpace(textEdit_标题查询.Text))
            //{
            //    strWhere += @" and a.headline like '%" + textEdit_标题查询.Text + "%'";
            //}

            //dt_通知通告 = bll_通知.MyGetSummaryByParam(strWhere);

            if (!string.IsNullOrEmpty(DateEdit_创建时间查询1.Text) && !string.IsNullOrEmpty(DateEdit_创建时间查询2.Text))
            {
                DateTime dt1 = Convert.ToDateTime(DateEdit_创建时间查询1.Text);
                DateTime dt2 = Convert.ToDateTime(DateEdit_创建时间查询2.Text);
                if (DateTime.Compare(dt1, dt2) > 0) MessageBox.Show("起始查询日期不能晚于终止查询日期!");
                return;
            }
            strWhere += @" and (创建机构_Code='" + LookUp_创建机构查询.EditValue + "' or 创建机构_Code='371328') and (创建人_Code='" + LookUp_创建人查询.EditValue + "' or 创建人_Code='3713280000')";
            if (!string.IsNullOrEmpty(DateEdit_创建时间查询1.Text))
            {
                strWhere += @" and 创建时间 >='" + DateEdit_创建时间查询1.Text + "'";
            }
            if (!string.IsNullOrEmpty(DateEdit_创建时间查询2.Text))
            {
                strWhere += @" and 创建时间 <='" + DateEdit_创建时间查询2.Text + "'";
            }
            if (!string.IsNullOrEmpty(ComboBoxEdit_置顶.Text) && !"请选择".Equals(ComboBoxEdit_置顶.Text))
            {
                if (ComboBoxEdit_置顶.Text.Equals("置顶"))
                {
                    strWhere = @" and 是否置顶_Code='1'";
                }
                else
                {
                    strWhere = @" and a.是否置顶_Code='0'";
                }
            }
            if (!string.IsNullOrWhiteSpace(textEdit_标题查询.Text))
            {
                strWhere += @" and 标题 like '%" + textEdit_标题查询.Text + "%'";
            }

            Binding();
        }

        private void pagerControl_分页_OnPageChanged(object sender, EventArgs e)
        {
            Binding();
        }

        private void Binding()
        {
            dataGridControl_查询.View = "View_通知通告";
            dataGridControl_查询.StrWhere = strWhere;
            dt_通知通告 = pagerControl_分页.GetQueryResultNew("View_通知通告", "*", strWhere, "创建时间", "desc").Tables[0];

            if (dt_通知通告.Rows.Count == 0)
            {
                dataGridControl_查询.DataSource = null;
                MessageBox.Show("没有符合条件的数据!");
            }
            else
            {
                gridColumn_创建时间.FieldName = "创建时间";
                gridColumn_标题.FieldName = "标题";
                gridColumn_创建人.FieldName = "创建人";
                gridColumn_创建机构.FieldName = "创建机构";
                gridColumn_是否置顶.FieldName = "是否置顶";
                gridColumn_GUID.FieldName = "GUID";
                //单独设置列宽自适应
                //gridColumn_创建时间.BestFit();
                //gridColumn_标题.BestFit();
                //gridColumn_创建人.BestFit();
                //gridColumn_创建机构.BestFit();
                //gridColumn_是否置顶.BestFit();
                dataGridControl_查询.DataSource = dt_通知通告;

                pagerControl_分页.DrawControl();
                //所有列列宽自适应
                gridView_查询.BestFitColumns();
            }
        }

        private void sBut_新建公告_Click(object sender, EventArgs e)
        {
            list_filePath.Clear();
            listView_附件编辑.Items.Clear();
            textEdit_标题编辑.Text = string.Empty;
            memoEdit_内容编辑.Text = string.Empty;
            checkEdit_是否置顶编辑.Checked = false;

            ut = UpdateType.Add;
            str_GUID = "";
            Page_编辑.PageVisible = true;
            Page_编辑.Show();
        }

        private void sBut_下载附件_Click(object sender, EventArgs e)
        {
            string str_Message = string.Empty;
            string str_Message_Y = string.Empty;
            string str_Message_N = string.Empty;
            string str_Mrssage_I = string.Empty;    //ignore(忽略)

            ListView.SelectedListViewItemCollection slvc = listView_附件详情.SelectedItems;
            if (slvc.Count > 0)
            {
                if (MessageBox.Show("是否下载选中的" + slvc.Count + "个文件?", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    FolderBrowserDialog fbd = new FolderBrowserDialog();

                    if (fbd.ShowDialog() == DialogResult.OK)
                    {
                        string str_savePath = fbd.SelectedPath + "/";

                        foreach (ListViewItem item in slvc)
                        {
                            if (File.Exists(str_savePath + item.Text))
                            {
                                str_Mrssage_I += "\n" + item.Text;
                            }
                            else
                            {
                                string str_serverPath = bll_附件.GetFileInfo(item.Name).Rows[0][tb_通知通告表_附件.filePath].ToString();
                                if (FtpTools.Download(str_savePath, str_serverPath) == 1)
                                {
                                    str_Message_Y += "\n" + item.Text;
                                }
                                else
                                {
                                    str_Message_N += "\n" + item.Text;
                                }
                            }
                        }
                    }
                }

            }
            else
            {
                MessageBox.Show("请先选择要下载的文件(可多选)!", "提示");
                return;
            }

            if (!string.IsNullOrEmpty(str_Message_Y))
            {
                str_Message += "\n" + "*=====文件:=====*" + str_Message_Y + "\n" + "*=====下载成功!=====*";
            }
            if (!string.IsNullOrEmpty(str_Message_N))
            {
                str_Message += "\n" + "*=====要下载的文件:=====*" + str_Message_N + "\n" + "=====不存在于FTP服务器!=====";
            }
            if (!string.IsNullOrEmpty(str_Mrssage_I))
            {
                str_Message += "\n" + "*=====保存路径下已存在以下同名文件:=====*" + str_Mrssage_I + "\n" + "*=====这些文件将被跳过!=====*";
            }

            if (!string.IsNullOrEmpty(str_Message))
            {
                MessageBox.Show(str_Message, "下载提示");
            }
            else
            {
                MessageBox.Show("下载已取消!", "提示");
            }
        }

        private void sBut_修改_Click(object sender, EventArgs e)
        {
            list_filePath.Clear();
            listView_附件编辑.Items.Clear();

            textEdit_标题编辑.Text = textEdit_标题详情.Text;
            memoEdit_内容编辑.Text = memoEdit_内容详情.Text;
            checkEdit_是否置顶编辑.Checked = checkEdit_是否置顶详情.Checked;

            List<NameAndType> list_NT = new List<NameAndType>();
            foreach (ListViewItem item in listView_附件详情.Items)
            {
                NameAndType nt = new NameAndType();
                nt.fileName = item.Text;
                nt.fileType = item.Name;
                list_NT.Add(nt);
            }
            ListView_Show(listView_附件编辑, list_NT);

            ut = UpdateType.Modify;
            Page_详情.PageVisible = false;
            Page_编辑.PageVisible = true;
            Page_编辑.Show();
        }

        private void sBut_删除_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("是否删除此公告?\n将同时删除对应附件!", "警告!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                DataTable dt_附件 = bll_附件.GetFileInfos(str_GUID);
                List<string> list_FileGUID = new List<string>();
                foreach (DataRow item in dt_附件.Rows)
                {
                    list_FileGUID.Add(item[tb_通知通告表_附件.__KeyName].ToString());
                }

                File_Delete(list_FileGUID);

                if (bll_通知.Delete(str_GUID))
                {


                    MessageBox.Show("删除成功!");
                    Page_查询.Show();
                    Page_详情.PageVisible = false;
                }
                else
                {
                    MessageBox.Show("删除失败!!");
                }
            }
        }

        private void sBut_保存_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textEdit_标题编辑.Text) && string.IsNullOrWhiteSpace(memoEdit_内容编辑.Text))
            {
                MessageBox.Show("标题或内容不可为空!");
                return;
            }

            string NewGUID = string.Empty;
            if (ut == UpdateType.Add)
            {
                bll_通知.GetBusinessByKey(NewGUID, true);
                bll_通知.NewBusiness();
                ds_通知通告 = bll_通知.CurrentBusiness;
                NewGUID = Guid.NewGuid().ToString("N");
            }
            else
            {
                ds_通知通告 = bll_通知.GetBusinessByKey(str_GUID, false);
                NewGUID = ds_通知通告.Tables[tb_通知通告表.__TableName].Rows[0][tb_通知通告表.GUID].ToString();
            }




            if (ut == UpdateType.Add)
            {
                ds_通知通告.Tables[tb_通知通告表.__TableName].Rows[0][tb_通知通告表.GUID] = NewGUID;
                ds_通知通告.Tables[tb_通知通告表.__TableName].Rows[0][tb_通知通告表.createOrg] = logInfo.所属机构;
                ds_通知通告.Tables[tb_通知通告表.__TableName].Rows[0][tb_通知通告表.createTime] = bll_通知.ServiceDateTime;
                ds_通知通告.Tables[tb_通知通告表.__TableName].Rows[0][tb_通知通告表.creator] = logInfo.用户编码;
            }
            ds_通知通告.Tables[tb_通知通告表.__TableName].Rows[0][tb_通知通告表.headline] = textEdit_标题编辑.Text;
            ds_通知通告.Tables[tb_通知通告表.__TableName].Rows[0][tb_通知通告表.value] = memoEdit_内容编辑.Text;

            DataTable dt_置顶 = bll_通知.Get_置顶(str_RootOrg);
            if (checkEdit_是否置顶编辑.Checked == true && dt_置顶.Rows.Count > 0)
            {
                if (MessageBox.Show("本机构已存在置顶的公告,是否替换?", "提示!", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    bll_通知.Set_不置顶(dt_置顶.Rows[0][tb_通知通告表.__KeyName].ToString());
                    ds_通知通告.Tables[tb_通知通告表.__TableName].Rows[0][tb_通知通告表.setTop] = true;
                }
                else
                {
                    checkEdit_是否置顶编辑.Checked = false;
                    ds_通知通告.Tables[tb_通知通告表.__TableName].Rows[0][tb_通知通告表.setTop] = false;
                }
            }
            else
            {
                ds_通知通告.Tables[tb_通知通告表.__TableName].Rows[0][tb_通知通告表.setTop] = checkEdit_是否置顶编辑.Checked;
            }

            ds_通知通告.Tables[tb_通知通告表.__TableName].Rows[0][tb_通知通告表.updateOrg] = logInfo.所属机构;
            ds_通知通告.Tables[tb_通知通告表.__TableName].Rows[0][tb_通知通告表.updateTime] = bll_通知.ServiceDateTime;
            ds_通知通告.Tables[tb_通知通告表.__TableName].Rows[0][tb_通知通告表.modifier] = logInfo.用户编码;

            if (list_filePath.Count > 0)
            {
                File_Add(NewGUID, list_filePath);
            }
            if (list_Delete.Count > 0)
            {
                File_Delete(list_Delete);
            }

            bll_通知.Save(ds_通知通告);

            str_GUID = NewGUID;

            Page_编辑.PageVisible = false;

            Page_详情_Show(NewGUID);
        }

        private void sBut_选择附件_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = true;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                string[] strs_filePath;

                if (listView_附件编辑.Items.Count == 0)
                {
                    strs_filePath = ofd.FileNames;
                }
                else
                {
                    string[] strs_Compare = ofd.FileNames.Intersect(list_filePath.ToArray()).ToArray();
                    if (strs_Compare.Length == 0)
                    {
                        strs_filePath = ofd.FileNames;
                    }
                    else
                    {
                        MessageBox.Show("文件:\n" + string.Join("\n", strs_Compare) + "已存在于附件列表!请重新选择!");
                        return;
                    }
                }

                NameAndType nt = new NameAndType();
                List<NameAndType> list_NT = new List<NameAndType>();

                foreach (string item in strs_filePath)
                {
                    nt.fileName = Path.GetFileName(item);
                    nt.fileType = "";
                    list_NT.Add(nt);
                    list_filePath.Add(item);
                }

                ListView_Show(listView_附件编辑, list_NT);
            }
        }

        private void sBut_删除附件_Click(object sender, EventArgs e)
        {
            ListView.SelectedListViewItemCollection slvc = listView_附件编辑.SelectedItems;
            if (slvc.Count > 0)
            {
                string str_Temp = string.Empty;
                foreach (ListViewItem item in slvc)
                {
                    list_Delete.Add(item.Name);
                    listView_附件编辑.Items.Remove(item);
                    foreach (string filePath in list_filePath)
                    {
                        if (Path.GetFileName(filePath).Equals(item.Text))
                        {
                            str_Temp = filePath;
                        }
                    }
                    list_filePath.Remove(str_Temp);
                }
            }
        }

        private void frm通知通告管理_Load(object sender, EventArgs e)
        {
            if (logInfo.Account.Equals("admin"))
            {
                LookUp_创建机构查询.Properties.ValueMember = "机构编号";                                    //values
                LookUp_创建机构查询.Properties.DisplayMember = "机构名称";                                  //text(显示的内容)
                LookUp_创建机构查询.Properties.DataSource = _bll机构信息.Get_卫生院名称(logInfo.所属机构);     //绑定数据
                LookUp_创建机构查询.Properties.Columns.Add(new LookUpColumnInfo("机构名称"));               //只显示机构名称列
                LookUp_创建机构查询.EditValue = logInfo.所属机构;                                           //默认显示

                LookUp_创建人查询.Properties.ValueMember = "用户编码";
                LookUp_创建人查询.Properties.DisplayMember = "用户名称";
                LookUp_创建人查询.Properties.DataSource = _bllUser.Get_卫生院管理员成员();
                LookUp_创建人查询.Properties.Columns.Add(new LookUpColumnInfo("用户名称"));
                LookUp_创建人查询.EditValue = logInfo.用户编码;
            }
            else
            {
                DataTable dt_用户信息 = new DataTable();

                dt_用户信息.Columns.Add("机构编号");
                dt_用户信息.Columns.Add("机构名称");
                dt_用户信息.Columns.Add("用户编码");
                dt_用户信息.Columns.Add("用户名称");
                dt_用户信息.Rows.Add(str_RootOrg, _bll机构信息.Get_机构名称(str_RootOrg).Rows[0][tb_机构信息._机构名称].ToString(), str_RootUser, _bllUser.GetwxUser(str_RootUser).Rows[0][TUser.UserName].ToString());


                LookUp_创建机构查询.Properties.ValueMember = "机构编号";                                    //values
                LookUp_创建机构查询.Properties.DisplayMember = "机构名称";                                  //text(显示的内容)
                LookUp_创建机构查询.Properties.DataSource = dt_用户信息;     //绑定数据
                LookUp_创建机构查询.Properties.Columns.Add(new LookUpColumnInfo("机构名称"));               //只显示机构名称列
                LookUp_创建机构查询.EditValue = str_RootOrg;                                           //默认显示
                LookUp_创建机构查询.Enabled = false;

                LookUp_创建人查询.Properties.ValueMember = "用户编码";
                LookUp_创建人查询.Properties.DisplayMember = "用户名称";
                LookUp_创建人查询.Properties.DataSource = dt_用户信息;
                LookUp_创建人查询.Properties.Columns.Add(new LookUpColumnInfo("用户名称"));
                LookUp_创建人查询.EditValue = str_RootUser;
                LookUp_创建人查询.Enabled = false;
            }

            util.ControlsHelper.BindComboxData(_BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='sfzd' ")), ComboBoxEdit_置顶, "P_CODE", "P_DESC");
        }

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            DataRowView drv = (DataRowView)gridView_查询.GetFocusedRow(); //获取点击行的内容

            str_GUID = drv["GUID"].ToString();

            if (bll_通知.GetBusinessByKey(str_GUID,false).Tables[tb_通知通告表.__TableName].Rows[0][tb_通知通告表.createOrg].ToString().Equals(str_RootOrg))
            {
                sBut_修改.Visible = true;
                sBut_删除.Visible = true;
            }
            else
            {
                sBut_修改.Visible = false;
                sBut_删除.Visible = false;
            }

            Page_详情_Show(str_GUID);

            Page_详情.PageVisible = true;
            Page_详情.Show();

        }

        /// <summary>
        /// 附件添加
        /// </summary>
        /// <param name="Map_GUID"></param>
        /// <param name="filePath"></param>
        private void File_Add(string Map_GUID, List<string> filePath)
        {
            ds_通知通告_附件 = bll_附件.GetBusinessByKey("", false);

            for (int i = 0; i < filePath.Count; i++)
            {
                string str_filePath = filePath[i].ToLower();
                FileInfo fi = new FileInfo(str_filePath);
                FtpTools.Upload(fi.FullName, str_RootPath + fi.Name);
                string NewGUID = Guid.NewGuid().ToString("N");

                ds_通知通告_附件.Tables[tb_通知通告表_附件.__TableName].Rows.Add();
                ds_通知通告_附件.Tables[tb_通知通告表_附件.__TableName].Rows[i][tb_通知通告表_附件.__KeyName] = NewGUID;
                ds_通知通告_附件.Tables[tb_通知通告表_附件.__TableName].Rows[i][tb_通知通告表_附件.Map_GUID] = Map_GUID;
                ds_通知通告_附件.Tables[tb_通知通告表_附件.__TableName].Rows[i][tb_通知通告表_附件.fileName] = fi.Name;
                ds_通知通告_附件.Tables[tb_通知通告表_附件.__TableName].Rows[i][tb_通知通告表_附件.fileType] = fi.Extension;
                ds_通知通告_附件.Tables[tb_通知通告表_附件.__TableName].Rows[i][tb_通知通告表_附件.fileSize] = Convert_Filesize(fi.Length);
                ds_通知通告_附件.Tables[tb_通知通告表_附件.__TableName].Rows[i][tb_通知通告表_附件.filePath] = str_RootPath + fi.Name;
                ds_通知通告_附件.Tables[tb_通知通告表_附件.__TableName].Rows[i][tb_通知通告表_附件.createTime] = bll_通知.ServiceDateTime;
            }

            bll_附件.Save(ds_通知通告_附件);
        }

        /// <summary>
        /// 根据附件的GUID删除文件
        /// </summary>
        /// <param name="FileGUID"></param>
        private void File_Delete(List<string> FileGUID)
        {
            foreach (string item in FileGUID)
            {
                DataTable dt_附件 = bll_附件.GetFileInfo(item);
                FtpTools.Delete("File", dt_附件.Rows[0][tb_通知通告表_附件.filePath].ToString());
                bll_附件.Delete(dt_附件.Rows[0][tb_通知通告表_附件.__KeyName].ToString());
            }
            FileGUID.Clear();

        }

        /// <summary>
        /// 根据文件名称与文件类别返回可用图标名称
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="fileType"></param>
        /// <returns></returns>
        private string Select_ICO(string fileName, string fileType)
        {
            string str_ICO = string.Empty;

            switch (fileType)
            {
                case "File":
                    switch (Path.GetExtension(fileName).ToLower())
                    {
                        case ".xls":
                        case ".xlsx":
                            str_ICO = "Excel";
                            break;
                        case ".doc":
                        case ".docx":
                            str_ICO = "Word";
                            break;
                        case ".ppt":
                        case ".pptx":
                            str_ICO = "PowerPoint";
                            break;
                        case ".jpg":
                        case ".jpeg":
                            str_ICO = "JPG";
                            break;
                        case ".png":
                            str_ICO = "PNG";
                            break;
                        case ".pdf":
                            str_ICO = "PDF";
                            break;
                        case ".txt":
                            str_ICO = "TXT";
                            break;
                        case ".zip":
                            str_ICO = "ZIP";
                            break;
                        case ".rar":
                            str_ICO = "RAR";
                            break;
                        case ".exe":
                            str_ICO = "EXE";
                            break;
                        default:
                            str_ICO = "File";
                            break;
                    }
                    break;
                case "Directory":
                    str_ICO = "Folder";
                    break;
                case "Link":
                    break;
                default:
                    str_ICO = "Unknown";
                    break;
            }

            return str_ICO;
        }

        /// <summary>
        /// 在指定的ListView上显示指定的文件集
        /// </summary>
        /// <param name="lv"></param>
        /// <param name="list_NT"></param>
        private void ListView_Show(ListView lv, List<NameAndType> list_NT)
        {
            lv.BeginUpdate();
            foreach (NameAndType nt in list_NT)
            {
                ListViewItem lvi = new ListViewItem();
                //lvi.ImageKey = Select_ICO(nt.fileName, nt.fileType);
                lvi.ImageKey = Select_ICO(nt.fileName, "File");
                lvi.Name = nt.fileType;
                lvi.Text = nt.fileName;
                lv.Items.Add(lvi);
            }
            lv.EndUpdate();
        }

        /// <summary>
        /// 详情页展示
        /// </summary>
        /// <param name="GUID"></param>
        private void Page_详情_Show(string GUID)
        {
            list_filePath.Clear();
            listView_附件编辑.Items.Clear();

            textEdit_标题详情.Text = string.Empty;
            memoEdit_内容详情.Text = string.Empty;
            textEdit_创建时间.Text = string.Empty;
            textEdit_修改时间.Text = string.Empty;
            textEdit_创建机构.Text = string.Empty;
            textEdit修改机构.Text = string.Empty;
            textEdit_创建人.Text = string.Empty;
            textEdit_修改人.Text = string.Empty;
            checkEdit_是否置顶详情.Checked = false;

            DataRow dr_通知通告 = bll_通知.Get_详情(GUID).Rows[0];

            textEdit_标题详情.Text = dr_通知通告["标题"].ToString();
            memoEdit_内容详情.Text = dr_通知通告["内容"].ToString();
            textEdit_创建时间.Text = dr_通知通告["创建时间"].ToString();
            textEdit_修改时间.Text = dr_通知通告["修改时间"].ToString();
            textEdit_创建机构.Text = dr_通知通告["创建机构"].ToString();
            textEdit修改机构.Text = dr_通知通告["修改机构"].ToString();
            textEdit_创建人.Text = dr_通知通告["创建人"].ToString();
            textEdit_修改人.Text = dr_通知通告["修改人"].ToString();
            switch (dr_通知通告["是否置顶"].ToString())
            {
                case "置顶":
                    checkEdit_是否置顶详情.Checked = true;
                    break;
                case "不置顶":
                    checkEdit_是否置顶详情.Checked = false;
                    break;
                default:
                    break;
            }

            if (listView_附件详情.Items.Count > 0)
            {
                listView_附件详情.Items.Clear();
            }

            DataTable dt_附件 = bll_附件.GetFileInfos(GUID);
            List<NameAndType> list_NameAndID = new List<NameAndType>();
            foreach (DataRow item in dt_附件.Rows)
            {
                NameAndType nt = new NameAndType();
                nt.fileName = item[tb_通知通告表_附件.fileName].ToString();
                nt.fileType = item[tb_通知通告表_附件.__KeyName].ToString();
                list_NameAndID.Add(nt);
                list_filePath.Add(item[tb_通知通告表_附件.filePath].ToString());
            }
            ListView_Show(listView_附件详情, list_NameAndID);

            Page_详情.PageVisible = true;
            Page_详情.Show();
        }

        /// <summary>
        /// 传入一个byte单位的文件大小(long类型),转为Byte,KB,MB,GB单位(string类型)
        /// </summary>
        /// <param name="fileSize"></param>
        /// <returns></returns>
        private string Convert_Filesize(long fileSize)
        {
            string str_fileSize = string.Empty;

            if (fileSize >= 1024 && fileSize < 1024 * 1024)
            {
                str_fileSize = Math.Round((double)(fileSize / 1024), 2, MidpointRounding.AwayFromZero).ToString() + "KB";
            }
            else if (fileSize >= 1024 * 1024 && fileSize < 1024 * 1024 * 1024)
            {
                str_fileSize = Math.Round((double)(fileSize / 1024 / 1024), 2, MidpointRounding.AwayFromZero).ToString() + "MB";
            }
            else if (fileSize >= 1024 * 1024 * 1024)
            {
                str_fileSize = Math.Round((double)(fileSize / 1024 / 1024 / 1024), 2, MidpointRounding.AwayFromZero).ToString() + "GB";
            }
            else
            {
                str_fileSize = fileSize.ToString() + "Byte";
            }

            return str_fileSize;
        }

        /// <summary>
        /// 设置FTP使用信息
        /// </summary>
        private void FtpInfo()
        {
            string FtpIP;
            string FtpUser;
            string FtpPwd;
            DataRow dr_Ftp;

            if (bll_Ftp.OrgToFtp(logInfo.所属机构).Tables[tb_FtpUserConfig.__TableName].Rows.Count > 0)
            {
                dr_Ftp = bll_Ftp.OrgToFtp(logInfo.所属机构).Tables[tb_FtpUserConfig.__TableName].Rows[0];
            }
            else
            {
                string str_Orgcode = _bll机构信息.Get上级机构(logInfo.所属机构);
                if (bll_Ftp.OrgToFtp(str_Orgcode).Tables[tb_FtpUserConfig.__TableName].Rows.Count > 0)
                {
                    dr_Ftp = bll_Ftp.OrgToFtp(str_Orgcode).Tables[tb_FtpUserConfig.__TableName].Rows[0];
                }
                else
                {
                    MessageBox.Show("只有卫生院及其下属机构的账户可以使用此功能!", "警告!");
                    return;
                }
            }

            FtpIP = dr_Ftp[tb_FtpUserConfig.FtpIP].ToString();
            FtpUser = dr_Ftp[tb_FtpUserConfig.FtpUser].ToString();
            FtpPwd = dr_Ftp[tb_FtpUserConfig.FtpPwd].ToString();
            FtpPwd = CEncoder.Decode(FtpPwd);

            str_RootPath = dr_Ftp[tb_FtpUserConfig.OrgRoot].ToString();
            str_RootUser = dr_Ftp[tb_FtpUserConfig.UserCode].ToString();
            str_RootOrg = dr_Ftp[tb_FtpUserConfig.OrgCode].ToString();

            FtpTools.FtpLogon(FtpIP, FtpUser, FtpPwd);
        }

        private void ShowConfig()
        {
            if (bll_Ftp.UserToFtp(logInfo.用户编码).Tables[tb_FtpUserConfig.__TableName].Rows.Count > 0)
            {
                sBut_新建公告.Visible = true;
                sBut_修改.Visible = true;
                sBut_删除.Visible = true;
            }
        }
    }
}
