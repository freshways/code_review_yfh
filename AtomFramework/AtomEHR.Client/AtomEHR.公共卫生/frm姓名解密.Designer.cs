﻿namespace AtomEHR.公共卫生
{
    partial class frm姓名解密
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.个人档案编号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit表名 = new DevExpress.XtraEditors.TextEdit();
            this.treeListLookUpEdit机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.checkEdit21 = new DevExpress.XtraEditors.CheckEdit();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit表名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.checkEdit21);
            this.tpSummary.Controls.Add(this.treeListLookUpEdit机构);
            this.tpSummary.Controls.Add(this.simpleButton1);
            this.tpSummary.Controls.Add(this.gridControl1);
            this.tpSummary.Controls.Add(this.textEdit表名);
            this.tpSummary.Controls.Add(this.simpleButton2);
            this.tpSummary.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.tpSummary.Size = new System.Drawing.Size(838, 413);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlSummary.Size = new System.Drawing.Size(844, 419);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tcBusiness.Size = new System.Drawing.Size(844, 419);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.tpDetail.Size = new System.Drawing.Size(815, 451);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gcNavigator.Size = new System.Drawing.Size(844, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(666, 2);
            this.controlNavigatorSummary.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            // 
            // txtFocusForSave
            // 
            this.txtFocusForSave.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(469, 2);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(93, 13);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(88, 26);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "查询";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.gridControl1.Location = new System.Drawing.Point(93, 85);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(643, 321);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.个人档案编号,
            this.姓名,
            this.gridColumn1});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // 个人档案编号
            // 
            this.个人档案编号.Caption = "个人档案编号";
            this.个人档案编号.FieldName = "个人档案编号";
            this.个人档案编号.Name = "个人档案编号";
            this.个人档案编号.Visible = true;
            this.个人档案编号.VisibleIndex = 0;
            // 
            // 姓名
            // 
            this.姓名.Caption = "加密姓名";
            this.姓名.FieldName = "加密姓名";
            this.姓名.Name = "姓名";
            this.姓名.Visible = true;
            this.姓名.VisibleIndex = 1;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "解密姓名";
            this.gridColumn1.FieldName = "解密姓名";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 2;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(490, 15);
            this.simpleButton2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(88, 26);
            this.simpleButton2.TabIndex = 2;
            this.simpleButton2.Text = "解密";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // textEdit表名
            // 
            this.textEdit表名.Location = new System.Drawing.Point(206, 16);
            this.textEdit表名.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textEdit表名.Name = "textEdit表名";
            this.textEdit表名.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.textEdit表名.Properties.Appearance.Options.UseFont = true;
            this.textEdit表名.Size = new System.Drawing.Size(232, 26);
            this.textEdit表名.TabIndex = 3;
            // 
            // treeListLookUpEdit机构
            // 
            this.treeListLookUpEdit机构.Location = new System.Drawing.Point(206, 48);
            this.treeListLookUpEdit机构.Name = "treeListLookUpEdit机构";
            this.treeListLookUpEdit机构.Properties.AutoExpandAllNodes = false;
            this.treeListLookUpEdit机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEdit机构.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.treeListLookUpEdit机构.Size = new System.Drawing.Size(134, 20);
            this.treeListLookUpEdit机构.TabIndex = 35;
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(219, 106);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(519, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // checkEdit21
            // 
            this.checkEdit21.EditValue = true;
            this.checkEdit21.Location = new System.Drawing.Point(346, 49);
            this.checkEdit21.Name = "checkEdit21";
            this.checkEdit21.Properties.Caption = "含下属机构";
            this.checkEdit21.Size = new System.Drawing.Size(92, 19);
            this.checkEdit21.TabIndex = 36;
            // 
            // frm姓名解密
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 445);
            this.Margin = new System.Windows.Forms.Padding(4, 9, 4, 9);
            this.Name = "frm姓名解密";
            this.Text = "frm姓名解密";
            this.Load += new System.EventHandler(this.frm姓名解密_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit表名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn 个人档案编号;
        private DevExpress.XtraGrid.Columns.GridColumn 姓名;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.TextEdit textEdit表名;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEdit机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraEditors.CheckEdit checkEdit21;
    }
}