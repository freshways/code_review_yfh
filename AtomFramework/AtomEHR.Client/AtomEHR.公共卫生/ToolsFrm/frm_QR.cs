﻿using AtomEHR.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.ToolsFrm
{
    public partial class QR_Show : frmBase
    {
        Bitmap _bmp;
        public QR_Show(Bitmap bmp, string value)
        {
            InitializeComponent();
            this._bmp = bmp;
            showQR(bmp, value);
        }

        private void showQR(Bitmap bmp, string value)
        {
            pictureEdit_OR.Image = bmp;

            hyperLinkEdit_Value.Text = value;
        }

        private void sBut_download_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            //设置保存文件对话框的标题
            sfd.Title = "保存二维码到本地";
            //保存对话框是否记忆上次打开的目录 
            sfd.RestoreDirectory = true;
            //设置文件类型 
            sfd.Filter = "JPG文件|*.jpg|PNG文件|*.png|BMP文件|*.bmp";

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                _bmp.Save(sfd.FileName.ToString());
                MessageBox.Show("保存成功!");
            }

        }
    }
}
