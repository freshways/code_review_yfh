﻿namespace AtomEHR.公共卫生.ToolsFrm
{
    partial class QR_Show
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureEdit_OR = new DevExpress.XtraEditors.PictureEdit();
            this.hyperLinkEdit_Value = new DevExpress.XtraEditors.HyperLinkEdit();
            this.sBut_download = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit_OR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hyperLinkEdit_Value.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureEdit_OR
            // 
            this.pictureEdit_OR.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureEdit_OR.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit_OR.Name = "pictureEdit_OR";
            this.pictureEdit_OR.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit_OR.Size = new System.Drawing.Size(384, 384);
            this.pictureEdit_OR.TabIndex = 0;
            // 
            // hyperLinkEdit_Value
            // 
            this.hyperLinkEdit_Value.Dock = System.Windows.Forms.DockStyle.Top;
            this.hyperLinkEdit_Value.Location = new System.Drawing.Point(0, 384);
            this.hyperLinkEdit_Value.Name = "hyperLinkEdit_Value";
            this.hyperLinkEdit_Value.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hyperLinkEdit_Value.Properties.Appearance.Options.UseFont = true;
            this.hyperLinkEdit_Value.Size = new System.Drawing.Size(384, 24);
            this.hyperLinkEdit_Value.TabIndex = 2;
            // 
            // sBut_download
            // 
            this.sBut_download.Location = new System.Drawing.Point(272, 419);
            this.sBut_download.Name = "sBut_download";
            this.sBut_download.Size = new System.Drawing.Size(100, 30);
            this.sBut_download.TabIndex = 3;
            this.sBut_download.Text = "保存二维码";
            this.sBut_download.Click += new System.EventHandler(this.sBut_download_Click);
            // 
            // QR_Show
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 461);
            this.Controls.Add(this.sBut_download);
            this.Controls.Add(this.hyperLinkEdit_Value);
            this.Controls.Add(this.pictureEdit_OR);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "QR_Show";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "QR&Link";
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit_OR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hyperLinkEdit_Value.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit pictureEdit_OR;
        private DevExpress.XtraEditors.HyperLinkEdit hyperLinkEdit_Value;
        private DevExpress.XtraEditors.SimpleButton sBut_download;
    }
}