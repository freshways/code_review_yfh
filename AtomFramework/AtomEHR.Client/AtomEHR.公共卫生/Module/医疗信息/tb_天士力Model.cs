﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtomEHR.公共卫生.Module.医疗信息
{
    class tb_天士力Model
    {
        
    }
    public class RootObject
    {
        /// <summary>
        /// 
        /// </summary>
        public string event_data { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string event_type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string owner_info { get; set; }
    }

    public class Event_data
    {
        #region 公用/SOS
        /// <summary>
        /// 5769bdeaa5c24543b6047bc41cf2cd1a
        /// </summary>
        public string token { get; set; }
        /// <summary>
        /// Time
        /// </summary>
        public string time { get; set; }
        #endregion

        #region 定位
        /// <summary>
        /// 经度
        /// </summary>
        public string longitude { get; set; }
        /// <summary>
        /// 纬度
        /// </summary>
        public string latitude { get; set; }
        /// <summary>
        /// wifi
        /// </summary>
        public string type { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string desc { get; set; }
        /// <summary>
        /// Accuracy
        /// </summary>
        public string accuracy { get; set; }
        #endregion

        #region 血压部分
        /// <summary>
        /// High
        /// </summary>
        public string high { get; set; }
        /// <summary>
        /// Low
        /// </summary>
        public string low { get; set; }
        /// <summary>
        /// Rate
        /// </summary>
        public string rate { get; set; }
        /// <summary>
        /// FromTime
        /// </summary>
        private string _fromTime;
        public string fromTime 
        { 
            get
            {
                System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1)); // 当地时区
                DateTime dt = startTime.AddMilliseconds(Convert.ToDouble(_fromTime));
                return dt.ToString("yyyy-MM-dd HH:mm:ss");
            } 
            set
            {
                _fromTime = value;
            } 
        }
        /// <summary>
        /// ToTime
        /// </summary>
        private string _toTime;
        public string toTime 
        { 
            get
            {
                System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1)); // 当地时区
                DateTime dt = startTime.AddMilliseconds(Convert.ToDouble(_toTime));
                return dt.ToString("yyyy-MM-dd HH:mm:ss");
            } 
            set
            {
                _toTime = value;
            } 
        }
        #endregion
        /// <summary>
        /// 心率
        /// </summary>
        public string count { get; set; }
    }

    public class Owner_info
    {
        /// <summary>
        /// Active
        /// </summary>
        public string active { get; set; }
        /// <summary>
        /// ActiveTime
        /// </summary>
        public string activeTime { get; set; }
        /// <summary>
        /// Binded
        /// </summary>
        public string binded { get; set; }
        /// <summary>
        /// BindedUserId
        /// </summary>
        public string bindedUserId { get; set; }
        /// <summary>
        /// 1969-6-20
        /// </summary>
        public string birthday { get; set; }
        /// <summary>
        /// [{"headImage":"","isSos":"1","nickName":"自己","phoneNumber":"18715008554","userId":0}]
        /// </summary>
        public string contacts { get; set; }
        /// <summary>
        /// CreateDateTime
        /// </summary>
        public string createDateTime { get; set; }
        /// <summary>
        /// Deleted
        /// </summary>
        public string deleted { get; set; }
        /// <summary>
        /// 普通版
        /// </summary>
        public string deviceVersion { get; set; }
        /// <summary>
        /// Height
        /// </summary>
        public string height { get; set; }
        /// <summary>
        /// Id
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// 891817121907033
        /// </summary>
        public string imei { get; set; }
        /// <summary>
        /// LastBindedTime
        /// </summary>
        public string lastBindedTime { get; set; }
        /// <summary>
        /// LocState
        /// </summary>
        public string locState { get; set; }
        /// <summary>
        /// 老张
        /// </summary>
        public string nickName { get; set; }
        /// <summary>
        /// Online
        /// </summary>
        public string online { get; set; }
        /// <summary>
        /// 15055151370
        /// </summary>
        public string phoneNumber { get; set; }
        /// <summary>
        /// PowerOn
        /// </summary>
        public string powerOn { get; set; }
        /// <summary>
        /// tsl-A017S
        /// </summary>
        public string productionBatchName { get; set; }
        /// <summary>
        /// ProductionTime
        /// </summary>
        public string productionTime { get; set; }
        /// <summary>
        /// A017S_V4.0_0113
        /// </summary>
        public string roomVersion { get; set; }
        /// <summary>
        /// 1
        /// </summary>
        public string sex { get; set; }
        /// <summary>
        /// Status
        /// </summary>
        public string status { get; set; }
        /// <summary>
        /// 5769bdeaa5c24543b6047bc41cf2cd1a
        /// </summary>
        public string token { get; set; }
        /// <summary>
        /// UpdateDateTime
        /// </summary>
        public string updateDateTime { get; set; }
        /// <summary>
        /// Version
        /// </summary>
        public string version { get; set; }
        /// <summary>
        /// Weight
        /// </summary>
        public string weight { get; set; }
    }

    public class TB_Main
    {
        /// <summary>
        /// 
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string 姓名 { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        private string _性别;
        public string 性别 { get { return _性别 == "1" ? "男" : "女"; } set { _性别 = value; } }
        /// <summary>
        /// 
        /// </summary>
        public string 生日 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string 手表号码 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string 体重 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string 身高 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string 地址 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string 类型 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string 类型名称 { get; set; }
    }

    /// <summary>
    /// 定义统计返回json格式数据
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class JsonResult<T>
    {
        public int Errno { get; set; }
        public string Errmsg { get; set; }
        public List<T> Data { get; set; }
    }
}
