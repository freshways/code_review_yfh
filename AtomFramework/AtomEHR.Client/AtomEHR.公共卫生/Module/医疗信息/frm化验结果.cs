﻿using AtomEHR.Common;
using AtomEHR.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Models;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using AtomEHR.公共卫生.Module.个人健康;
using AtomEHR.Business.Security;
using TableXtraReport;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;


namespace AtomEHR.公共卫生.Module.医疗信息
{
    public partial class frm化验结果 : AtomEHR.Library.frmBaseBusinessForm
    {
        private string _strWhere;
        BackgroundWorker back = new BackgroundWorker();
        //bll家庭档案 _bll家庭档案 = new bll家庭档案();
        #region Constructor
        public frm化验结果()
        {
            InitializeComponent();
        }

        #endregion

        #region Handler Events
        private void frm化验结果_Load(object sender, EventArgs e)
        {
            InitView();//初始化页面操作
            InitializeForm();
            //tpDetail.Hide();
            tpDetail.PageVisible = false;
            this.gc个人健康档案.UseCheckBox = true;
            this.pagerControl1.OnPageChanged += pagerControl1_OnPageChanged;
            back.DoWork += back_DoWork;
            back.RunWorkerCompleted += back_RunWorkerCompleted;
            //back.WorkerReportsProgress = true;
            this.pagerControl1.Height = 35;
        }
        void back_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            DataSet ds = e.Result as DataSet;
            this.gc个人健康档案.DataSource = ds.Tables[0];
            this.pagerControl1.DrawControl();
            this.gv个人健康档案.BestFitColumns();
            this.btnQuery.Enabled = true;
        }
        void back_DoWork(object sender, DoWorkEventArgs e)
        {

            _strWhere = string.Empty;
            this.pagerControl1.InitControl();
            string pgrid = this.treeListLookUpEdit机构.EditValue.ToString();
            if (this.checkEdit21.Checked)//包含下属机构
            {
                if (pgrid.Length == 12)
                {
                    _strWhere += " and ([所属机构]=  '" + pgrid + "' or substring([所属机构],1,7)+'1'+substring([所属机构],9,7) like '" + pgrid + "%')";
                }
                else
                {
                    _strWhere += " and [所属机构] like '" + pgrid + "%'";
                }
            }
            else
            {
                _strWhere += " and [所属机构] ='" + pgrid + "'";
            }
            if (!string.IsNullOrEmpty(this.txt姓名.Text.Trim()))
            {
                _strWhere += " and (姓名 like'" + this.txt姓名.Text.Trim() + "%') ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(cbo性别)))
            {
                _strWhere += " and 性别编码 ='" + util.ControlsHelper.GetComboxKey(cbo性别) + "'  ";
            }
            if (!string.IsNullOrEmpty(this.txt档案号.Text.Trim()))
            {
                _strWhere += " and 个人档案编号 LIKE '%" + this.txt档案号.Text.Trim() + "%'  ";
            }
            if (!string.IsNullOrEmpty(this.txt身份证号.Text.Trim()))
            {
                _strWhere += " and 身份证号 LIKE '%" + this.txt身份证号.Text.Trim() + "%'  ";
            }
            if (!string.IsNullOrEmpty(this.dte出生时间1.Text.Trim()))
            {
                _strWhere += " and 出生日期 >= '" + this.dte出生时间1.Text.Trim() + "'  ";
            }
            if (!string.IsNullOrEmpty(this.dte出生时间2.Text.Trim()))
            {
                _strWhere += " and 出生日期 <= '" + this.dte出生时间2.Text.Trim() + "'  ";
            }
            if (!string.IsNullOrEmpty(this.dte录入时间1.Text.Trim()))
            {
                _strWhere += " and 创建时间 >= '" + this.dte录入时间1.Text.Trim() + " 00:00:00'  ";
            }
            if (!string.IsNullOrEmpty(this.dte录入时间2.Text.Trim()))
            {
                _strWhere += " and 创建时间 <= '" + this.dte录入时间2.Text.Trim() + " 23:59:59'  ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(cbo档案类别)))
            {
                _strWhere += " and 档案类别 ='" + util.ControlsHelper.GetComboxKey(cbo档案类别) + "'  ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(cbo档案状态)))
            {
                _strWhere += " and 档案状态 ='" + util.ControlsHelper.GetComboxKey(cbo档案状态) + "'  ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(cbo合格档案)))
            {
                if (util.ControlsHelper.GetComboxKey(cbo合格档案) == "2")//不合格档案
                {
                    _strWhere += " and 完整度 <> 100 ";
                }
                if (util.ControlsHelper.GetComboxKey(cbo合格档案) == "1")//合格档案
                {
                    _strWhere += " and 完整度 = 100 ";
                }
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(comboBoxEdit镇)))
            {
                _strWhere += " and 街道编码 = '" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'  ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(comboBoxEdit村)))
            {
                _strWhere += " and 居委会编码 = '" + util.ControlsHelper.GetComboxKey(comboBoxEdit村) + "'  ";
            }
            if (!string.IsNullOrEmpty(this.textEdit地址.Text.Trim()))
            {
                _strWhere += " and 居住地址 LIKE '%" + this.textEdit地址.Text.Trim() + "%'  ";
            }
            if (!string.IsNullOrEmpty(this.txt本人电话.Text.Trim()))
            {
                _strWhere += " and 本人电话 LIKE '%" + this.txt本人电话.Text.Trim() + "%'  ";
            }

            this.gc个人健康档案.View = "View_个人信息表";
            this.gc个人健康档案.StrWhere = _strWhere;


            DataSet ds = null;
            ds = this.pagerControl1.GetQueryResultNew("View_个人信息表", "*", _strWhere, "创建时间", "DESC");

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                ds.Tables[0].Rows[i][tb_健康档案.姓名] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i][tb_健康档案.姓名].ToString());
                ds.Tables[0].Rows[i][tb_健康档案.档案状态] = _BLL.ReturnDis字典显示("rkx_dazt", ds.Tables[0].Rows[i][tb_健康档案.档案状态].ToString());
                ds.Tables[0].Rows[i][tb_健康档案.所属机构] = _BLL.Return机构名称(ds.Tables[0].Rows[i][tb_健康档案.所属机构].ToString());
                ds.Tables[0].Rows[i][tb_健康档案.创建机构] = _BLL.Return机构名称(ds.Tables[0].Rows[i][tb_健康档案.创建机构].ToString());
                ds.Tables[0].Rows[i][tb_健康档案.创建人] = _BLL.Return用户名称(ds.Tables[0].Rows[i][tb_健康档案.创建人].ToString());
                ds.Tables[0].Rows[i][tb_健康档案.居住地址] = ds.Tables[0].Rows[i][tb_健康档案.区].ToString() + ds.Tables[0].Rows[i][tb_健康档案.街道] + ds.Tables[0].Rows[i][tb_健康档案.居委会] + ds.Tables[0].Rows[i][tb_健康档案.居住地址];
            }
            e.Result = ds;
            //this.gc个人健康档案.DataSource = ds.Tables[0];
            //this.pagerControl1.DrawControl();
            //BindData();
        }
        void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            BindData();
        }
        
        private void btn查询_Click(object sender, EventArgs e)
        {
            if (funCheck())
            {
                try
                {
                    this.btnQuery.Enabled = false;
                    back.RunWorkerAsync();
                }
                catch (Exception ex)
                {
                    Msg.ShowInformation(ex.Message);
                }
            }
        }
        /// <summary>
        /// 绑定数据
        /// </summary>
        private void BindData()
        {
            this.gc个人健康档案.View = "View_个人信息表";
            this.gc个人健康档案.StrWhere = _strWhere;

            DataSet ds = this.pagerControl1.GetQueryResultNew("View_个人信息表", "*", _strWhere, "创建时间", "DESC");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                ds.Tables[0].Rows[i][tb_健康档案.姓名] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i][tb_健康档案.姓名].ToString());
                ds.Tables[0].Rows[i][tb_健康档案.档案状态] = _BLL.ReturnDis字典显示("rkx_dazt", ds.Tables[0].Rows[i][tb_健康档案.档案状态].ToString());
                ds.Tables[0].Rows[i][tb_健康档案.所属机构] = _BLL.Return机构名称(ds.Tables[0].Rows[i][tb_健康档案.所属机构].ToString());
                ds.Tables[0].Rows[i][tb_健康档案.创建机构] = _BLL.Return机构名称(ds.Tables[0].Rows[i][tb_健康档案.创建机构].ToString());
                ds.Tables[0].Rows[i][tb_健康档案.创建人] = _BLL.Return用户名称(ds.Tables[0].Rows[i][tb_健康档案.创建人].ToString());
                ds.Tables[0].Rows[i][tb_健康档案.居住地址] = ds.Tables[0].Rows[i][tb_健康档案.市].ToString() + ds.Tables[0].Rows[i][tb_健康档案.区] + ds.Tables[0].Rows[i][tb_健康档案.街道] + ds.Tables[0].Rows[i][tb_健康档案.居委会] + ds.Tables[0].Rows[i][tb_健康档案.居住地址];
            }
            this.gc个人健康档案.DataSource = ds.Tables[0];
            this.pagerControl1.DrawControl();
            this.gv个人健康档案.BestFitColumns();//列自适应宽度         
        }
        
        private void comboBoxEdit镇_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv村 = new DataView(DataDictCache.Cache.t地区信息);
                dv村.RowFilter = "上级编码='" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'";
                //DataBinder.BindingLookupEditDataSource(lookUpEdit村, dv县.ToTable(), "地区名称", "地区编码");
                util.ControlsHelper.BindComboxData(dv村.ToTable(), comboBoxEdit村, "地区编码", "地区名称");
                util.ControlsHelper.SetComboxNullData("", comboBoxEdit村, "请选择居/村委会");
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }
        private bool funCheck()
        {
            if (!string.IsNullOrEmpty(this.dte出生时间1.Text.Trim()) && !string.IsNullOrEmpty(this.dte出生时间2.Text.Trim()) && this.dte出生时间1.DateTime > this.dte出生时间2.DateTime)
            {
                Msg.ShowError("出生日期 结束时间不能小于开始时间，请重新选择！");
                this.dte出生时间2.Text = "";
                this.dte出生时间2.Focus();
                return false;
            }

            if (!string.IsNullOrEmpty(this.dte录入时间1.Text.Trim()) && !string.IsNullOrEmpty(this.dte录入时间2.Text.Trim()) && this.dte录入时间1.DateTime > this.dte录入时间2.DateTime)
            {
                Msg.ShowError("录入时间 结束时间不能小于开始时间，请重新选择！");
                this.dte录入时间2.Text = "";
                this.dte录入时间2.Focus();
                return false;
            }
            return true;
        }

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv个人健康档案.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            //_BLL.GetBusinessByKey(个人档案编号, true);//下载一个空业务单据            
            //_BLL.NewBusiness(); //增加一条主表记录
            frm个人健康 frm = new frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, null);
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //BindData();
            }
        }

        #endregion

        #region Private Methods
        private void InitView()
        {
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, cbo性别, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案类别, cbo档案类别, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案状态, cbo档案状态, "P_CODE", "P_DESC");
            cbo档案状态.SelectedIndex = 1;
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t是否, cbo合格档案, "P_CODE", "P_DESC");
            
            //为“镇”绑定信息
            DataView dv镇 = new DataView(DataDictCache.Cache.t地区信息);
            dv镇.RowFilter = "上级编码=371323";
            //DataBinder.BindingLookupEditDataSource(lookUpEdit镇, dv市.ToTable(), "地区名称", "地区编码");
            util.ControlsHelper.BindComboxData(dv镇.ToTable(), comboBoxEdit镇, "地区编码", "地区名称");

            //为“所属机构"绑定信息
            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                treeListLookUpEdit机构.Properties.ValueMember = "机构编号";
                treeListLookUpEdit机构.Properties.DisplayMember = "机构名称";

                treeListLookUpEdit机构.Properties.TreeList.KeyFieldName = "机构编号";
                treeListLookUpEdit机构.Properties.TreeList.ParentFieldName = "上级机构";
                treeListLookUpEdit机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                treeListLookUpEdit机构.Properties.DataSource = dt所属机构;
                //treeListLookUpEdit机
                //treeListLookUpEdit机构.Properties.TreeList.CollapseAll();

                treeListLookUpEdit机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                treeListLookUpEdit机构.Text = Loginer.CurrentUser.所属机构;
            }
            if (Loginer.CurrentUser.所属机构.Length >= 12)
                treeListLookUpEdit机构.Properties.AutoExpandAllNodes = true;
            else { treeListLookUpEdit机构.Properties.AutoExpandAllNodes = false; }

        }

        #endregion

        #region Override Methods
        protected override void InitializeForm()
        {
            _BLL = new bll健康档案();// 业务逻辑层实例
            _SummaryView = new DevGridView(gv个人健康档案);//给成员变量赋值.每个业务窗体必需给这个变量赋值.
            _ActiveEditor = txt编码;
            _DetailGroupControl = gcDetailEditor;

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gv个人健康档案);
            //DevStyle.SetGridControlLayout(gc个人健康档案, false);//表格设置   
            //DevStyle.SetSummaryGridViewLayout(gv个人健康档案);
            //gridColumn2.OptionsColumn.AllowEdit = false;
            //_BLL.GetBusinessByKey("-", true);//加载一个空的业务对象.
            //BindingSummarySearchPanel(btnQuery, btnEmpty, panelControl1);

        }

        protected override void ButtonStateChanged(UpdateType currentState)
        {
            base.ButtonStateChanged(currentState);
            txt编码.Enabled = UpdateType.Add == currentState;
        }

        /// <summary>
        /// 绑定输入框
        /// </summary>
        /// <param name="summary"></param>
        protected override void DoBindingSummaryEditor(DataTable summary)
        {
            try
            {
                if (summary == null) return;
                //DataBinder.BindingTextEdit(txt编码, summary, tb_Test._编码);
                //DataBinder.BindingTextEdit(txt名称, summary, tb_Test._名称);
            }
            catch (Exception ex)
            { Msg.ShowException(ex); }
        }


        public override void DoAdd(Interfaces.IButtonInfo sender)
        {
            //base.DoAdd(sender);
            //this._UpdateType = UpdateType.Add; //标记新增记录
            //_BLL.GetBusinessByKey("-", true);//下载一个空业务单据            
            //_BLL.NewBusiness(); //增加一条主表记录
            frm个人基本信息表 frm = new frm个人基本信息表();
            //frm个人基本信息表 frm = new frm个人基本信息表();
            if (frm.ShowDialog() == DialogResult.Yes)
            {
                frm.Close();
                btn查询_Click(null, null);
                Msg.ShowInformation("保存成功!");
            }

        }

        public override void DoDelete(Interfaces.IButtonInfo sender)
        {
            base.DoDelete(sender);
        }

        /// <summary>
        /// 重写保存
        /// </summary>
        /// <param name="sender">自定义按钮接口</param>
        public override void DoSave(Interfaces.IButtonInfo sender)// 保存数据
        {
            this.UpdateLastControl();//更新最后一个输入框的数据


            if (_UpdateType == UpdateType.Modify) _BLL.WriteLog(); //注意:只有修改状态下保存修改日志

            DataSet dsTemplate = _BLL.CreateSaveData(_BLL.CurrentBusiness, _UpdateType); //创建用于保存的临时数据

            SaveResult result = _BLL.Save(dsTemplate);//调用业务逻辑保存数据方法

            if (result.Success) //保存成功, 不需要重新加载数据，更新当前的缓存数据就行．
            {
                //if (_UpdateType == UpdateType.Add) _BLL.DataBindRow[tb_健康档案.__KeyName] = result.DocNo; //更新单据号码
                //if (_UpdateType == UpdateType.Modify) _BLL.NotifyUser();//修改后通知创建人

                //this.UpdateSummaryRow(_BLL.DataBindRow);//刷新表格内当前记录的缓存数据.
                //this.DoBindingSummaryEditor(_BLL.DataBinder); //重新显示数据
                //if (_UpdateType == UpdateType.Add) gv个人健康档案.MoveLast(); //如是新增单据,移动到取后一条记录.
                // base.DoSave(sender); //调用基类的方法. 此行代码应放较后位置.       
                DoSearchSummary();
                btn查询_Click(null, null);
                Msg.ShowInformation("保存成功!");
            }
            else
                Msg.Warning("保存失败!");
        }
        #endregion

        private void repositoryItemHyperLinkEdit2_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv个人健康档案.GetFocusedDataRow();
            if (row == null) 
            {
                return;
            }
            string str身份证号 = row["身份证号"] as string;
            string str姓名 = row["姓名"].ToString();

            if (string.IsNullOrWhiteSpace(str身份证号))
            {
                MessageBox.Show("身份证号是空的，无法查询此人的化验结果。","提示");
                return;
            }

            try
            {
                interfaceZljl.FrmLisResult frm = new interfaceZljl.FrmLisResult(str姓名, str身份证号);
                frm.ShowDialog();
            }
            catch
            {
                MessageBox.Show("显示化验结果时出现未知错误，请联系工程师排查原因。");
            }
        }
    }
}
