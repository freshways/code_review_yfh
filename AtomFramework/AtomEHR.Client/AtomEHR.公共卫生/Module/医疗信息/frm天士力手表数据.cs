﻿using AtomEHR.Common;
using AtomEHR.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Models;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using AtomEHR.公共卫生.Module.个人健康;
using AtomEHR.Business.Security;
using TableXtraReport;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Newtonsoft.Json;
using System.Threading.Tasks;


namespace AtomEHR.公共卫生.Module.医疗信息
{
    public partial class frm天士力手表数据 : AtomEHR.Library.frmBaseBusinessForm
    {
        BackgroundWorker back = new BackgroundWorker();
        System.Threading.Tasks.Task Task = new System.Threading.Tasks.Task(() => { Msg.AskQuestion(""); });
        //bll家庭档案 _bll家庭档案 = new bll家庭档案();
        #region Constructor
        public frm天士力手表数据()
        {
            InitializeComponent();
        }

        #endregion

        #region Handler Events
        private void frm天士力手表数据_Load(object sender, EventArgs e)
        {
            InitView();//初始化页面操作
            InitializeForm();
            //tpDetail.Hide();
            //tpDetail.PageVisible = false;
            this.gc个人健康档案.UseCheckBox = true;
            this.pagerControl1.OnPageChanged += pagerControl1_OnPageChanged;
            back.DoWork += back_DoWork;
            back.RunWorkerCompleted += back_RunWorkerCompleted;
            //back.WorkerReportsProgress = true;
            this.pagerControl1.Height = 35;
        }
        void back_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.btnQuery.Enabled = true;

            this.gc个人健康档案.DataSource = e.Result;
            //this.pagerControl1.DrawControl();
            this.gv个人健康档案.BestFitColumns();
        }
        void back_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                // 服务器IP
                string host = "192.168.10.118";//"localhost";
                // 服务器端口
                string port = "83";//"34164";
                // 获取用户信息接口
                string sUrl = string.Format("http://{0}:{1}/WebApi/api/QZAI/Get_List", host, port);
                //string sUrl = string.Format("http://{0}:{1}/api/QZAI/Get_List", host, port);
                string sMessage = "";
                string TypeNO = "";
                switch (txt档案号.Text)
                {
                    case "定位":
                        TypeNO = "3000";
                        break;
                    case "SOS":
                        TypeNO = "3001";
                        break;
                    case "血压":
                        TypeNO = "3002";
                        break;
                    case "心率":
                        TypeNO = "3003";
                        break;
                    default:
                        break;
                }
                string sEntity = "{\"PhoneNo\":\"" + txt本人电话.Text + "\",\"Name\":\"" + txt姓名.Text + "\",\"TypeNo\":\"" + TypeNO + "\"}";

                string sReq = WebApiMethod.RequestJsonPostMethod(sUrl, sEntity, out sMessage);
                JsonResult<TB_Main> rb = JsonConvert.DeserializeObject<JsonResult<TB_Main>>(sReq);
                //this.richTextBox1.AppendText(rb.Data[0].Text);

                //gc个人健康档案.DataSource = rb.Data;
                e.Result = rb.Data;
            }
            catch (Exception ex)
            {
                Msg.ShowInformation(ex.Message);
            }
        }


        void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            BindData();
        }
                

        private void btn查询_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnQuery.Enabled = false;
                if (!back.IsBusy)
                    back.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                Msg.ShowInformation(ex.Message);
                this.btnQuery.Enabled = true;
            }            

        }
        /// <summary>
        /// 绑定数据
        /// </summary>
        private void BindData()
        {
            //this.gc个人健康档案.View = "View_个人信息表";
            //this.gc个人健康档案.StrWhere = _strWhere;

            //DataSet ds = this.pagerControl1.GetQueryResultNew("View_个人信息表", "*", _strWhere, "创建时间", "DESC");
            //for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            //{
            //    ds.Tables[0].Rows[i][tb_健康档案.姓名] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i][tb_健康档案.姓名].ToString());
            //    ds.Tables[0].Rows[i][tb_健康档案.档案状态] = _BLL.ReturnDis字典显示("rkx_dazt", ds.Tables[0].Rows[i][tb_健康档案.档案状态].ToString());
            //    ds.Tables[0].Rows[i][tb_健康档案.所属机构] = _BLL.Return机构名称(ds.Tables[0].Rows[i][tb_健康档案.所属机构].ToString());
            //    ds.Tables[0].Rows[i][tb_健康档案.创建机构] = _BLL.Return机构名称(ds.Tables[0].Rows[i][tb_健康档案.创建机构].ToString());
            //    ds.Tables[0].Rows[i][tb_健康档案.创建人] = _BLL.Return用户名称(ds.Tables[0].Rows[i][tb_健康档案.创建人].ToString());
            //    ds.Tables[0].Rows[i][tb_健康档案.居住地址] = ds.Tables[0].Rows[i][tb_健康档案.市].ToString() + ds.Tables[0].Rows[i][tb_健康档案.区] + ds.Tables[0].Rows[i][tb_健康档案.街道] + ds.Tables[0].Rows[i][tb_健康档案.居委会] + ds.Tables[0].Rows[i][tb_健康档案.居住地址];
            //}
            //this.gc个人健康档案.DataSource = ds.Tables[0];
            //this.pagerControl1.DrawControl();
            //this.gv个人健康档案.BestFitColumns();//列自适应宽度         
        }
        

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv个人健康档案.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            //_BLL.GetBusinessByKey(个人档案编号, true);//下载一个空业务单据            
            //_BLL.NewBusiness(); //增加一条主表记录
            frm个人健康 frm = new frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, null);
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //BindData();
            }
        }

        #endregion
        #region Override Methods
        protected override void InitializeForm()
        {
            _BLL = new bll健康档案();// 业务逻辑层实例
            _SummaryView = new DevGridView(gv个人健康档案);//给成员变量赋值.每个业务窗体必需给这个变量赋值.
            _SummaryView.BindingDoubleClick(gv个人健康档案_DoubleClick); //注册双击事件
            _ActiveEditor = txt编码;
            _DetailGroupControl = gcDetailEditor;
            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gv个人健康档案);
            //DevStyle.SetGridControlLayout(gc个人健康档案, false);//表格设置   
            //DevStyle.SetSummaryGridViewLayout(gv个人健康档案);
            //gridColumn2.OptionsColumn.AllowEdit = false;
            //_BLL.GetBusinessByKey("-", true);//加载一个空的业务对象.
            //BindingSummarySearchPanel(btnQuery, btnEmpty, panelControl1);

        }
        #endregion

        #region Private Methods
        private void InitView()
        {
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, cbo性别, "P_CODE", "P_DESC");
            
        }

        #endregion


        private void gv个人健康档案_DoubleClick(object sender, EventArgs e)
        {
            TB_Main row = (TB_Main)this.gv个人健康档案.GetFocusedRow();
            this.richTextBox1.AppendText(row.Text);

            RootObject rb = JsonConvert.DeserializeObject<RootObject>(row.Text);
            this.txt编码.Text = rb.event_data;
            this.txt类型.Text = rb.event_type;
            this.txtInfo.Text = rb.owner_info;
            tcBusiness.SelectedTabPage = tpDetail;

            try
            {
                switch (rb.event_type)
                {
                    case "3002":
                        {
                            List<Event_data> data = JsonConvert.DeserializeObject<List<Event_data>>(rb.event_data);

                            txt收缩压.Text = data[0].high;
                            txt舒张压.Text = data[0].low;
                            txt心率.Text = data[0].rate;
                            txt时间.Text = data[0].fromTime;
                            this.dataGridControl1.DataSource = data;
                            this.gridView1.BestFitColumns();
                        }
                        break;
                    default:
                        {
                            Event_data data = JsonConvert.DeserializeObject<Event_data>(rb.event_data);
                            txt地址.Text = data.desc;
                            txt经度.Text = data.longitude;
                            txt维度.Text = data.latitude;
                        }
                        break;
                }
                if (rb.event_type == "3002")
                {
                    
                }
                
                
                Owner_info info = JsonConvert.DeserializeObject<Owner_info>(rb.owner_info);
                txt昵称.Text = info.nickName;
                txt手机号.Text = info.phoneNumber;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        
    }
}
