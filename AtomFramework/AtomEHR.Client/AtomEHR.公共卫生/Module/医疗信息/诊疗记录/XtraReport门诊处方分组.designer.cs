﻿namespace AtomEHR.公共卫生.Module.医疗信息.诊疗记录
{
    partial class XtraReport门诊处方分组
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraReport门诊处方分组));
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.formattingRule2 = new DevExpress.XtraReports.UI.FormattingRule();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel日期 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel临床诊断 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel年龄 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel性别 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel科室 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelCfid = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrLabel医生 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel总金额 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox医生签名 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.GroupFooter2 = new DevExpress.XtraReports.UI.GroupFooterBand();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel4,
            this.xrLabel3,
            this.xrLabel11,
            this.xrLabel12,
            this.xrLabel14});
            this.Detail.HeightF = 46F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "频次")});
            this.xrLabel10.FormattingRules.Add(this.formattingRule1);
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(260.3258F, 22.99999F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(86.34085F, 23F);
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "xrLabel10";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // formattingRule1
            // 
            this.formattingRule1.Condition = "[财务分类] == \'卫生材料\'";
            // 
            // 
            // 
            this.formattingRule1.Formatting.ForeColor = System.Drawing.Color.White;
            this.formattingRule1.Name = "formattingRule1";
            // 
            // xrLabel9
            // 
            this.xrLabel9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "用法")});
            this.xrLabel9.FormattingRules.Add(this.formattingRule1);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(171.8334F, 22.99999F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(88.37497F, 23F);
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "xrLabel9";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel8
            // 
            this.xrLabel8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "剂量单位")});
            this.xrLabel8.FormattingRules.Add(this.formattingRule1);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(128.0833F, 22.99999F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(43.75F, 23F);
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "xrLabel8";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "剂量数量")});
            this.xrLabel7.FormattingRules.Add(this.formattingRule1);
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(78.95834F, 22.99999F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(49.125F, 23F);
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "xrLabel7";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel6
            // 
            this.xrLabel6.CanGrow = false;
            this.xrLabel6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "数量")});
            this.xrLabel6.FormattingRules.Add(this.formattingRule1);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(301.9925F, 0F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(44.67422F, 23F);
            this.xrLabel6.Text = "xrLabel6";
            // 
            // xrLabel5
            // 
            this.xrLabel5.CanGrow = false;
            this.xrLabel5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "单位")});
            this.xrLabel5.FormattingRules.Add(this.formattingRule1);
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(260.3258F, 0F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(30.20834F, 23F);
            this.xrLabel5.Text = "xrLabel5";
            // 
            // xrLabel4
            // 
            this.xrLabel4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "规格")});
            this.xrLabel4.FormattingRules.Add(this.formattingRule1);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(190.5341F, 0F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(58.2159F, 23F);
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "xrLabel4";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel4.WordWrap = false;
            // 
            // xrLabel3
            // 
            this.xrLabel3.CanGrow = false;
            this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "项目名称")});
            this.xrLabel3.FormattingRules.Add(this.formattingRule1);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(14.16664F, 0F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(176.3674F, 23F);
            this.xrLabel3.Text = "xrLabel3";
            this.xrLabel3.WordWrap = false;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel11.FormattingRules.Add(this.formattingRule1);
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(248.75F, 0F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(11.45834F, 23F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "/";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel12.FormattingRules.Add(this.formattingRule1);
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(290.5341F, 0F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(11.45834F, 23F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "×";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel14
            // 
            this.xrLabel14.FormattingRules.Add(this.formattingRule2);
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(15.41675F, 22.99999F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(63.54161F, 23F);
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "用法";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // formattingRule2
            // 
            this.formattingRule2.Condition = "[财务分类] == \'卫生材料\'  Or (([剂量单位]  ==  NULL) And ([剂量数量] == NULL )  And ([用法] ==NULL)" +
    " And( [频次] == NULL))";
            // 
            // 
            // 
            this.formattingRule2.Formatting.ForeColor = System.Drawing.Color.White;
            this.formattingRule2.Name = "formattingRule2";
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 9F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(15.41675F, 71.7083F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(339.5833F, 23F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "门 诊 处 方 笺";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(15.41675F, 173.75F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(40.625F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "R";
            this.xrLabel1.Visible = false;
            // 
            // xrLine4
            // 
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(13.33342F, 171.75F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(341.6667F, 2.000015F);
            // 
            // xrLabel日期
            // 
            this.xrLabel日期.LocationFloat = new DevExpress.Utils.PointFloat(250.0001F, 94.70828F);
            this.xrLabel日期.Name = "xrLabel日期";
            this.xrLabel日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel日期.SizeF = new System.Drawing.SizeF(104.9999F, 23F);
            this.xrLabel日期.StylePriority.UseTextAlignment = false;
            this.xrLabel日期.Text = "日期:";
            this.xrLabel日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrLabel日期.WordWrap = false;
            // 
            // xrLine2
            // 
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(13.33342F, 144.3332F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(341.6667F, 2F);
            // 
            // xrLabel临床诊断
            // 
            this.xrLabel临床诊断.LocationFloat = new DevExpress.Utils.PointFloat(15.41675F, 148.75F);
            this.xrLabel临床诊断.Name = "xrLabel临床诊断";
            this.xrLabel临床诊断.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel临床诊断.SizeF = new System.Drawing.SizeF(339.5833F, 23F);
            this.xrLabel临床诊断.StylePriority.UseTextAlignment = false;
            this.xrLabel临床诊断.Text = "诊断:";
            this.xrLabel临床诊断.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrLabel临床诊断.WordWrap = false;
            // 
            // xrLabel年龄
            // 
            this.xrLabel年龄.LocationFloat = new DevExpress.Utils.PointFloat(190.5341F, 121.3333F);
            this.xrLabel年龄.Name = "xrLabel年龄";
            this.xrLabel年龄.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel年龄.SizeF = new System.Drawing.SizeF(93.74997F, 23F);
            this.xrLabel年龄.StylePriority.UseTextAlignment = false;
            this.xrLabel年龄.Text = "年龄:";
            this.xrLabel年龄.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel性别
            // 
            this.xrLabel性别.LocationFloat = new DevExpress.Utils.PointFloat(284.2841F, 121.3333F);
            this.xrLabel性别.Name = "xrLabel性别";
            this.xrLabel性别.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel性别.SizeF = new System.Drawing.SizeF(70.71591F, 23.00002F);
            this.xrLabel性别.StylePriority.UseTextAlignment = false;
            this.xrLabel性别.Text = "性别:";
            this.xrLabel性别.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel姓名
            // 
            this.xrLabel姓名.LocationFloat = new DevExpress.Utils.PointFloat(15.41675F, 121.3333F);
            this.xrLabel姓名.Name = "xrLabel姓名";
            this.xrLabel姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel姓名.SizeF = new System.Drawing.SizeF(175.1173F, 23F);
            this.xrLabel姓名.StylePriority.UseTextAlignment = false;
            this.xrLabel姓名.Text = "患者姓名:";
            this.xrLabel姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrLabel姓名.WordWrap = false;
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(13.33342F, 117.7917F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(342.7083F, 2.000008F);
            // 
            // xrLabel科室
            // 
            this.xrLabel科室.LocationFloat = new DevExpress.Utils.PointFloat(15.41675F, 94.70828F);
            this.xrLabel科室.Name = "xrLabel科室";
            this.xrLabel科室.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel科室.SizeF = new System.Drawing.SizeF(113.9167F, 23F);
            this.xrLabel科室.StylePriority.UseTextAlignment = false;
            this.xrLabel科室.Text = "科室:";
            this.xrLabel科室.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrLabel科室.WordWrap = false;
            // 
            // xrLabelCfid
            // 
            this.xrLabelCfid.LocationFloat = new DevExpress.Utils.PointFloat(129.3334F, 94.70828F);
            this.xrLabelCfid.Name = "xrLabelCfid";
            this.xrLabelCfid.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelCfid.SizeF = new System.Drawing.SizeF(120.6666F, 23F);
            this.xrLabelCfid.StylePriority.UseTextAlignment = false;
            this.xrLabelCfid.Text = "门诊号:";
            this.xrLabelCfid.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrLabelCfid.WordWrap = false;
            // 
            // xrLabelTitle
            // 
            this.xrLabelTitle.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabelTitle.LocationFloat = new DevExpress.Utils.PointFloat(15.41675F, 48.41665F);
            this.xrLabelTitle.Name = "xrLabelTitle";
            this.xrLabelTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelTitle.SizeF = new System.Drawing.SizeF(339.5833F, 23F);
            this.xrLabelTitle.StylePriority.UseFont = false;
            this.xrLabelTitle.StylePriority.UseTextAlignment = false;
            this.xrLabelTitle.Text = "处方便签";
            this.xrLabelTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 10F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.HeightF = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrLabel医生
            // 
            this.xrLabel医生.LocationFloat = new DevExpress.Utils.PointFloat(46.37508F, 9.999974F);
            this.xrLabel医生.Name = "xrLabel医生";
            this.xrLabel医生.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel医生.SizeF = new System.Drawing.SizeF(128.5834F, 23F);
            this.xrLabel医生.Text = "医生:";
            this.xrLabel医生.WordWrap = false;
            // 
            // xrLabel总金额
            // 
            this.xrLabel总金额.LocationFloat = new DevExpress.Utils.PointFloat(228.9583F, 10.00001F);
            this.xrLabel总金额.Name = "xrLabel总金额";
            this.xrLabel总金额.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel总金额.SizeF = new System.Drawing.SizeF(55.32576F, 23F);
            this.xrLabel总金额.Text = "总金额:";
            this.xrLabel总金额.WordWrap = false;
            // 
            // xrPictureBox医生签名
            // 
            this.xrPictureBox医生签名.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox医生签名.Image")));
            this.xrPictureBox医生签名.LocationFloat = new DevExpress.Utils.PointFloat(202.1098F, 10.04168F);
            this.xrPictureBox医生签名.Name = "xrPictureBox医生签名";
            this.xrPictureBox医生签名.SizeF = new System.Drawing.SizeF(25.92432F, 22.95834F);
            this.xrPictureBox医生签名.Visible = false;
            // 
            // xrLine3
            // 
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(8.125051F, 1.041698F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(341.6667F, 2F);
            // 
            // xrLabel15
            // 
            this.xrLabel15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "金额")});
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(284.2841F, 9.999974F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(64.58334F, 23F);
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Page;
            this.xrLabel15.Summary = xrSummary1;
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.Name = "xrControlStyle1";
            // 
            // ReportFooter
            // 
            this.ReportFooter.HeightF = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLine5
            // 
            this.xrLine5.LineDirection = DevExpress.XtraReports.UI.LineDirection.Slant;
            this.xrLine5.LineWidth = 2;
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(132.2499F, 0F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.SizeF = new System.Drawing.SizeF(39.58335F, 45.5F);
            this.xrLine5.Visible = false;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 36F);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(190.5341F, 0F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(37.50002F, 45.5F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UsePadding = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "/";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel2.Visible = false;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel15,
            this.xrLabel医生,
            this.xrPictureBox医生签名,
            this.xrLabel总金额,
            this.xrLine3});
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 64.24999F;
            this.GroupFooter1.Level = 1;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.PrintAtBottom = true;
            this.GroupFooter1.RepeatEveryPage = true;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabelTitle,
            this.xrLabel1,
            this.xrLine4,
            this.xrLabel日期,
            this.xrLine2,
            this.xrLabel临床诊断,
            this.xrLabel年龄,
            this.xrLabel性别,
            this.xrLabel姓名,
            this.xrLine1,
            this.xrLabel科室,
            this.xrLabelCfid,
            this.xrLabel13});
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("组别", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.HeightF = 196.75F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand;
            this.GroupHeader1.RepeatEveryPage = true;
            // 
            // GroupFooter2
            // 
            this.GroupFooter2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine5,
            this.xrLabel2});
            this.GroupFooter2.HeightF = 54.16667F;
            this.GroupFooter2.Name = "GroupFooter2";
            this.GroupFooter2.RepeatEveryPage = true;
            // 
            // XtraReport门诊处方分组
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageFooter,
            this.ReportFooter,
            this.GroupFooter1,
            this.GroupHeader1,
            this.GroupFooter2});
            this.DataMember = "ys门诊处方";
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1,
            this.formattingRule2});
            this.Margins = new System.Drawing.Printing.Margins(26, 21, 9, 10);
            this.PageHeight = 827;
            this.PageWidth = 445;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1});
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel临床诊断;
        private DevExpress.XtraReports.UI.XRLabel xrLabel年龄;
        private DevExpress.XtraReports.UI.XRLabel xrLabel性别;
        private DevExpress.XtraReports.UI.XRLabel xrLabel姓名;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel科室;
        private DevExpress.XtraReports.UI.XRLabel xrLabelCfid;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTitle;
        private DevExpress.XtraReports.UI.XRLabel xrLabel总金额;
        private DevExpress.XtraReports.UI.XRLabel xrLabel医生;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel日期;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox医生签名;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        //private Db.DataSetYsgzz dataSetYsgzz1;
        //private Db.DataSetYsgzzTableAdapters.YS医嘱频次TableAdapter yS医嘱频次TableAdapter;
        //private Db.DataSetYsgzzTableAdapters.ys门诊处方TableAdapter ys门诊处方TableAdapter1;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter2;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule2;
    }
}
