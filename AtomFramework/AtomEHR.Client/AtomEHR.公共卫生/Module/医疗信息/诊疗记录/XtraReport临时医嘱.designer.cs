﻿namespace AtomEHR.公共卫生.Module.医疗信息.诊疗记录
{
    partial class XtraReport临时医嘱
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable医嘱内容 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell开嘱日期 = new DevExpress.XtraReports.UI.XRTableCell();
            this.formattingRule续打不显示医嘱 = new DevExpress.XtraReports.UI.FormattingRule();
            this.formattingRule续打显示内容无边框 = new DevExpress.XtraReports.UI.FormattingRule();
            this.xrTableCell开嘱时间 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell医嘱内容 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell剂量数量 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell组别符号 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell用法 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell频次 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell医师 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell执行护士 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell执行时间 = new DevExpress.XtraReports.UI.XRTableCell();
            this.formattingRule续打执行时间 = new DevExpress.XtraReports.UI.FormattingRule();
            this.xrTableCell真正执行时间 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.formattingRule标题和页码 = new DevExpress.XtraReports.UI.FormattingRule();
            this.xrLabel病历号 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel床号 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel病室 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel科别 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel医院名称 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.formattingRule表头 = new DevExpress.XtraReports.UI.FormattingRule();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.para续打 = new DevExpress.XtraReports.Parameters.Parameter();
            this.s续打时间点 = new DevExpress.XtraReports.Parameters.Parameter();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable医嘱内容)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable医嘱内容});
            this.Detail.HeightF = 36F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable医嘱内容
            // 
            this.xrTable医嘱内容.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable医嘱内容.LocationFloat = new DevExpress.Utils.PointFloat(20F, 0F);
            this.xrTable医嘱内容.Name = "xrTable医嘱内容";
            this.xrTable医嘱内容.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable医嘱内容.SizeF = new System.Drawing.SizeF(780.6248F, 36F);
            this.xrTable医嘱内容.StylePriority.UseBorders = false;
            this.xrTable医嘱内容.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable医嘱内容_BeforePrint);
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell开嘱日期,
            this.xrTableCell开嘱时间,
            this.xrTableCell医嘱内容,
            this.xrTableCell剂量数量,
            this.xrTableCell组别符号,
            this.xrTableCell用法,
            this.xrTableCell频次,
            this.xrTableCell医师,
            this.xrTableCell执行护士,
            this.xrTableCell执行时间,
            this.xrTableCell真正执行时间});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell开嘱日期
            // 
            this.xrTableCell开嘱日期.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell开嘱日期.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "开嘱日期")});
            this.xrTableCell开嘱日期.FormattingRules.Add(this.formattingRule续打不显示医嘱);
            this.xrTableCell开嘱日期.FormattingRules.Add(this.formattingRule续打显示内容无边框);
            this.xrTableCell开嘱日期.Name = "xrTableCell开嘱日期";
            this.xrTableCell开嘱日期.StylePriority.UseBorders = false;
            this.xrTableCell开嘱日期.StylePriority.UseTextAlignment = false;
            this.xrTableCell开嘱日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell开嘱日期.Weight = 0.27661287886307384D;
            this.xrTableCell开嘱日期.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCell开嘱日期_BeforePrint);
            // 
            // formattingRule续打不显示医嘱
            // 
            this.formattingRule续打不显示医嘱.Condition = "[真正开嘱时间] < [Parameters.s续打时间点] And [Parameters.para续打] == True";
            // 
            // 
            // 
            this.formattingRule续打不显示医嘱.Formatting.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.formattingRule续打不显示医嘱.Formatting.ForeColor = System.Drawing.Color.White;
            this.formattingRule续打不显示医嘱.Formatting.Visible = DevExpress.Utils.DefaultBoolean.True;
            this.formattingRule续打不显示医嘱.Name = "formattingRule续打不显示医嘱";
            // 
            // formattingRule续打显示内容无边框
            // 
            this.formattingRule续打显示内容无边框.Condition = "[Parameters.para续打] == True";
            // 
            // 
            // 
            this.formattingRule续打显示内容无边框.Formatting.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.formattingRule续打显示内容无边框.Name = "formattingRule续打显示内容无边框";
            // 
            // xrTableCell开嘱时间
            // 
            this.xrTableCell开嘱时间.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell开嘱时间.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "开嘱时间")});
            this.xrTableCell开嘱时间.FormattingRules.Add(this.formattingRule续打不显示医嘱);
            this.xrTableCell开嘱时间.FormattingRules.Add(this.formattingRule续打显示内容无边框);
            this.xrTableCell开嘱时间.Name = "xrTableCell开嘱时间";
            this.xrTableCell开嘱时间.StylePriority.UseBorders = false;
            this.xrTableCell开嘱时间.StylePriority.UseTextAlignment = false;
            this.xrTableCell开嘱时间.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell开嘱时间.Weight = 0.2410282683415336D;
            this.xrTableCell开嘱时间.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCell开嘱时间_BeforePrint);
            // 
            // xrTableCell医嘱内容
            // 
            this.xrTableCell医嘱内容.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell医嘱内容.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "医嘱内容")});
            this.xrTableCell医嘱内容.FormattingRules.Add(this.formattingRule续打不显示医嘱);
            this.xrTableCell医嘱内容.FormattingRules.Add(this.formattingRule续打显示内容无边框);
            this.xrTableCell医嘱内容.Name = "xrTableCell医嘱内容";
            this.xrTableCell医嘱内容.StylePriority.UseBorders = false;
            this.xrTableCell医嘱内容.StylePriority.UseTextAlignment = false;
            this.xrTableCell医嘱内容.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell医嘱内容.Weight = 0.84666652559166167D;
            // 
            // xrTableCell剂量数量
            // 
            this.xrTableCell剂量数量.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell剂量数量.BorderWidth = 1F;
            this.xrTableCell剂量数量.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "剂量数量单位")});
            this.xrTableCell剂量数量.FormattingRules.Add(this.formattingRule续打不显示医嘱);
            this.xrTableCell剂量数量.FormattingRules.Add(this.formattingRule续打显示内容无边框);
            this.xrTableCell剂量数量.Name = "xrTableCell剂量数量";
            this.xrTableCell剂量数量.StylePriority.UseBorders = false;
            this.xrTableCell剂量数量.StylePriority.UseBorderWidth = false;
            this.xrTableCell剂量数量.StylePriority.UseTextAlignment = false;
            this.xrTableCell剂量数量.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell剂量数量.Weight = 0.15647126423494046D;
            this.xrTableCell剂量数量.WordWrap = false;
            // 
            // xrTableCell组别符号
            // 
            this.xrTableCell组别符号.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell组别符号.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "组别符号")});
            this.xrTableCell组别符号.FormattingRules.Add(this.formattingRule续打不显示医嘱);
            this.xrTableCell组别符号.FormattingRules.Add(this.formattingRule续打显示内容无边框);
            this.xrTableCell组别符号.Name = "xrTableCell组别符号";
            this.xrTableCell组别符号.StylePriority.UseBorders = false;
            this.xrTableCell组别符号.StylePriority.UseTextAlignment = false;
            this.xrTableCell组别符号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell组别符号.Weight = 0.07445909655435072D;
            this.xrTableCell组别符号.WordWrap = false;
            // 
            // xrTableCell用法
            // 
            this.xrTableCell用法.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell用法.BorderWidth = 1F;
            this.xrTableCell用法.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "用法名称")});
            this.xrTableCell用法.FormattingRules.Add(this.formattingRule续打不显示医嘱);
            this.xrTableCell用法.FormattingRules.Add(this.formattingRule续打显示内容无边框);
            this.xrTableCell用法.Name = "xrTableCell用法";
            this.xrTableCell用法.StylePriority.UseBorders = false;
            this.xrTableCell用法.StylePriority.UseBorderWidth = false;
            this.xrTableCell用法.StylePriority.UseTextAlignment = false;
            this.xrTableCell用法.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell用法.Weight = 0.20208773356620152D;
            this.xrTableCell用法.WordWrap = false;
            this.xrTableCell用法.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCell用法_BeforePrint);
            // 
            // xrTableCell频次
            // 
            this.xrTableCell频次.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell频次.BorderWidth = 1F;
            this.xrTableCell频次.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "频次名称")});
            this.xrTableCell频次.FormattingRules.Add(this.formattingRule续打不显示医嘱);
            this.xrTableCell频次.FormattingRules.Add(this.formattingRule续打显示内容无边框);
            this.xrTableCell频次.Name = "xrTableCell频次";
            this.xrTableCell频次.StylePriority.UseBorders = false;
            this.xrTableCell频次.StylePriority.UseBorderWidth = false;
            this.xrTableCell频次.StylePriority.UseTextAlignment = false;
            this.xrTableCell频次.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell频次.Weight = 0.20982606719459424D;
            this.xrTableCell频次.WordWrap = false;
            this.xrTableCell频次.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCell用法_BeforePrint);
            // 
            // xrTableCell医师
            // 
            this.xrTableCell医师.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell医师.FormattingRules.Add(this.formattingRule续打不显示医嘱);
            this.xrTableCell医师.FormattingRules.Add(this.formattingRule续打显示内容无边框);
            this.xrTableCell医师.Name = "xrTableCell医师";
            this.xrTableCell医师.StylePriority.UseBorders = false;
            this.xrTableCell医师.StylePriority.UseTextAlignment = false;
            this.xrTableCell医师.Text = "..";
            this.xrTableCell医师.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell医师.Weight = 0.24863167538495171D;
            this.xrTableCell医师.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCell医师_BeforePrint);
            // 
            // xrTableCell执行护士
            // 
            this.xrTableCell执行护士.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell执行护士.ForeColor = System.Drawing.Color.Black;
            this.xrTableCell执行护士.FormattingRules.Add(this.formattingRule续打不显示医嘱);
            this.xrTableCell执行护士.FormattingRules.Add(this.formattingRule续打显示内容无边框);
            this.xrTableCell执行护士.Name = "xrTableCell执行护士";
            this.xrTableCell执行护士.StylePriority.UseBorders = false;
            this.xrTableCell执行护士.StylePriority.UseForeColor = false;
            this.xrTableCell执行护士.StylePriority.UseTextAlignment = false;
            this.xrTableCell执行护士.Text = "..";
            this.xrTableCell执行护士.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell执行护士.Weight = 0.26233110327479958D;
            this.xrTableCell执行护士.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCell执行护士_BeforePrint);
            // 
            // xrTableCell执行时间
            // 
            this.xrTableCell执行时间.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell执行时间.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "执行时间")});
            this.xrTableCell执行时间.FormattingRules.Add(this.formattingRule续打显示内容无边框);
            this.xrTableCell执行时间.FormattingRules.Add(this.formattingRule续打执行时间);
            this.xrTableCell执行时间.Name = "xrTableCell执行时间";
            this.xrTableCell执行时间.StylePriority.UseBorders = false;
            this.xrTableCell执行时间.StylePriority.UseTextAlignment = false;
            this.xrTableCell执行时间.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell执行时间.Weight = 0.29659144814565108D;
            this.xrTableCell执行时间.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCell执行时间_BeforePrint);
            // 
            // formattingRule续打执行时间
            // 
            this.formattingRule续打执行时间.Condition = "[真正执行时间] < [Parameters.s续打时间点] And [Parameters.para续打] == True";
            // 
            // 
            // 
            this.formattingRule续打执行时间.Formatting.ForeColor = System.Drawing.Color.White;
            this.formattingRule续打执行时间.Name = "formattingRule续打执行时间";
            // 
            // xrTableCell真正执行时间
            // 
            this.xrTableCell真正执行时间.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "真正执行时间")});
            this.xrTableCell真正执行时间.Name = "xrTableCell真正执行时间";
            this.xrTableCell真正执行时间.Visible = false;
            this.xrTableCell真正执行时间.Weight = 0.058898347762074449D;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 5F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel8.FormattingRules.Add(this.formattingRule标题和页码);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(90.67981F, 105.0833F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(618.75F, 22.99999F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "临   时   医   嘱   单";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // formattingRule标题和页码
            // 
            this.formattingRule标题和页码.Condition = "[Parameters.para续打] == True";
            // 
            // 
            // 
            this.formattingRule标题和页码.Formatting.ForeColor = System.Drawing.Color.White;
            this.formattingRule标题和页码.Name = "formattingRule标题和页码";
            // 
            // xrLabel病历号
            // 
            this.xrLabel病历号.FormattingRules.Add(this.formattingRule标题和页码);
            this.xrLabel病历号.LocationFloat = new DevExpress.Utils.PointFloat(641.5129F, 133.3333F);
            this.xrLabel病历号.Name = "xrLabel病历号";
            this.xrLabel病历号.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel病历号.SizeF = new System.Drawing.SizeF(143.0835F, 22.99998F);
            this.xrLabel病历号.StylePriority.UseTextAlignment = false;
            this.xrLabel病历号.Text = "病历号：";
            this.xrLabel病历号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel床号
            // 
            this.xrLabel床号.FormattingRules.Add(this.formattingRule标题和页码);
            this.xrLabel床号.LocationFloat = new DevExpress.Utils.PointFloat(470.7217F, 133.3333F);
            this.xrLabel床号.Name = "xrLabel床号";
            this.xrLabel床号.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel床号.SizeF = new System.Drawing.SizeF(141.6247F, 22.99998F);
            this.xrLabel床号.StylePriority.UseTextAlignment = false;
            this.xrLabel床号.Text = "床号：";
            this.xrLabel床号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel病室
            // 
            this.xrLabel病室.FormattingRules.Add(this.formattingRule标题和页码);
            this.xrLabel病室.LocationFloat = new DevExpress.Utils.PointFloat(307.7632F, 133.3333F);
            this.xrLabel病室.Name = "xrLabel病室";
            this.xrLabel病室.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel病室.SizeF = new System.Drawing.SizeF(152.0833F, 22.99998F);
            this.xrLabel病室.StylePriority.UseTextAlignment = false;
            this.xrLabel病室.Text = "病室：";
            this.xrLabel病室.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel科别
            // 
            this.xrLabel科别.FormattingRules.Add(this.formattingRule标题和页码);
            this.xrLabel科别.LocationFloat = new DevExpress.Utils.PointFloat(160.5905F, 133.3333F);
            this.xrLabel科别.Name = "xrLabel科别";
            this.xrLabel科别.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel科别.SizeF = new System.Drawing.SizeF(135.7143F, 22.99998F);
            this.xrLabel科别.StylePriority.UseTextAlignment = false;
            this.xrLabel科别.Text = "科别：";
            this.xrLabel科别.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel姓名
            // 
            this.xrLabel姓名.FormattingRules.Add(this.formattingRule标题和页码);
            this.xrLabel姓名.LocationFloat = new DevExpress.Utils.PointFloat(22.97147F, 131.3334F);
            this.xrLabel姓名.Name = "xrLabel姓名";
            this.xrLabel姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel姓名.SizeF = new System.Drawing.SizeF(113.7083F, 23.00002F);
            this.xrLabel姓名.StylePriority.UseTextAlignment = false;
            this.xrLabel姓名.Text = "姓名：";
            this.xrLabel姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel医院名称
            // 
            this.xrLabel医院名称.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrLabel医院名称.FormattingRules.Add(this.formattingRule标题和页码);
            this.xrLabel医院名称.LocationFloat = new DevExpress.Utils.PointFloat(56.26314F, 77.08327F);
            this.xrLabel医院名称.Name = "xrLabel医院名称";
            this.xrLabel医院名称.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel医院名称.SizeF = new System.Drawing.SizeF(693.75F, 23F);
            this.xrLabel医院名称.StylePriority.UseFont = false;
            this.xrLabel医院名称.StylePriority.UseTextAlignment = false;
            this.xrLabel医院名称.Text = "XX医院";
            this.xrLabel医院名称.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTable2
            // 
            this.xrTable2.FormattingRules.Add(this.formattingRule标题和页码);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(20F, 161.3333F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(764.6248F, 40F);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell11,
            this.xrTableCell15,
            this.xrTableCell3,
            this.xrTableCell16});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.FormattingRules.Add(this.formattingRule表头);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "日期";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 0.33595909766752924D;
            // 
            // formattingRule表头
            // 
            this.formattingRule表头.Condition = "[Parameters.para续打] == True";
            // 
            // 
            // 
            this.formattingRule表头.Formatting.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.formattingRule表头.Formatting.ForeColor = System.Drawing.Color.White;
            this.formattingRule表头.Name = "formattingRule表头";
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell2.FormattingRules.Add(this.formattingRule表头);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "时间";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell2.Weight = 0.29274034002676463D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell11.FormattingRules.Add(this.formattingRule表头);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.Text = "医嘱内容";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell11.Weight = 1.8153869810829222D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.FormattingRules.Add(this.formattingRule表头);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "医师签名";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell15.Weight = 0.29566813439086381D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell3.FormattingRules.Add(this.formattingRule表头);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "执行护士";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 0.31861226498281187D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell16.FormattingRules.Add(this.formattingRule表头);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.Text = "执行时间";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell16.Weight = 0.36022483609650069D;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 16F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.HeightF = 43F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Format = "第 {0} 页";
            this.xrPageInfo1.FormattingRules.Add(this.formattingRule标题和页码);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(374.2917F, 10.00001F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.Number;
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(56.25003F, 23F);
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // para续打
            // 
            this.para续打.Description = "是否续打:";
            this.para续打.Name = "para续打";
            this.para续打.Type = typeof(bool);
            this.para续打.ValueInfo = "False";
            this.para续打.Visible = false;
            // 
            // s续打时间点
            // 
            this.s续打时间点.Description = "续打时间点";
            this.s续打时间点.Name = "s续打时间点";
            this.s续打时间点.Visible = false;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel医院名称,
            this.xrTable2,
            this.xrLabel姓名,
            this.xrLabel科别,
            this.xrLabel病室,
            this.xrLabel床号,
            this.xrLabel病历号,
            this.xrLabel8});
            this.PageHeader.HeightF = 202F;
            this.PageHeader.Name = "PageHeader";
            // 
            // XtraReport临时医嘱
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageFooter,
            this.PageHeader});
            this.DataMember = "dt临时医嘱YsImage";
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule标题和页码,
            this.formattingRule表头,
            this.formattingRule续打不显示医嘱,
            this.formattingRule续打显示内容无边框,
            this.formattingRule续打执行时间});
            this.Margins = new System.Drawing.Printing.Margins(6, 5, 5, 16);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.para续打,
            this.s续打时间点});
            this.Version = "13.2";
            this.FillEmptySpace += new DevExpress.XtraReports.UI.BandEventHandler(this.XtraReport临时医嘱_FillEmptySpace);
            this.ParametersRequestSubmit += new System.EventHandler<DevExpress.XtraReports.Parameters.ParametersRequestEventArgs>(this.XtraReport临时医嘱_ParametersRequestSubmit);
            this.AfterPrint += new System.EventHandler(this.XtraReport临时医嘱_AfterPrint);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable医嘱内容)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel病历号;
        private DevExpress.XtraReports.UI.XRLabel xrLabel床号;
        private DevExpress.XtraReports.UI.XRLabel xrLabel病室;
        private DevExpress.XtraReports.UI.XRLabel xrLabel科别;
        private DevExpress.XtraReports.UI.XRLabel xrLabel姓名;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        //private Db.DataSetYsgzz emr1;
        //private emr emr1;
        //private Db.DataSetYsgzzTableAdapters.dt长期医嘱TableAdapter dt长期医嘱TableAdapter;
        //private emrTableAdapters.dt长期医嘱TableAdapter dt长期医嘱TableAdapter;
        //private Db.DataSetYsgzzTableAdapters.dt长期医嘱YsImageTableAdapter dt长期医嘱YsImageTableAdapter1;
        //private emrTableAdapters.dt长期医嘱YsImageTableAdapter dt长期医嘱YsImageTableAdapter1;
        private DevExpress.XtraReports.UI.XRTable xrTable医嘱内容;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell开嘱日期;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell医嘱内容;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell医师;
        private DevExpress.XtraReports.UI.XRLabel xrLabel医院名称;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell执行护士;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell开嘱时间;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell剂量数量;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell组别符号;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell用法;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell频次;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule标题和页码;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule表头;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule续打不显示医嘱;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule续打显示内容无边框;
        //private Db.DataSetYsgzzTableAdapters.dt临时医嘱YsImageTableAdapter dt临时医嘱YsImageTableAdapter1;
        //private emrTableAdapters.dt临时医嘱YsImageTableAdapter dt临时医嘱YsImageTableAdapter1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell执行时间;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell真正执行时间;
        public DevExpress.XtraReports.Parameters.Parameter s续打时间点;
        public DevExpress.XtraReports.Parameters.Parameter para续打;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule续打执行时间;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
    }
}
