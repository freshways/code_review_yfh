﻿namespace AtomEHR.公共卫生.Module.医疗信息.诊疗记录
{
    partial class XtraReport长期医嘱
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable医嘱内容 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell开嘱日期 = new DevExpress.XtraReports.UI.XRTableCell();
            this.formattingRule续打不显示医嘱内容 = new DevExpress.XtraReports.UI.FormattingRule();
            this.formattingRule续打不显示医嘱边框 = new DevExpress.XtraReports.UI.FormattingRule();
            this.xrTableCell开嘱时间 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell医嘱内容 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell剂量数量 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell组别符号 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell用法 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell频次 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell医师 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell护士 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell停嘱日期 = new DevExpress.XtraReports.UI.XRTableCell();
            this.formattingRule续打不显示停嘱日期时间 = new DevExpress.XtraReports.UI.FormattingRule();
            this.xrTableCell停嘱时间 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell医师签名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell真正停嘱时间 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.formattingRule续打不显示标题页码边框 = new DevExpress.XtraReports.UI.FormattingRule();
            this.xrLabel病历号 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel床号 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel病室 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel科别 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel医院名称 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.para续打 = new DevExpress.XtraReports.Parameters.Parameter();
            this.s续打时间点 = new DevExpress.XtraReports.Parameters.Parameter();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable医嘱内容)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable医嘱内容});
            this.Detail.HeightF = 36F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable医嘱内容
            // 
            this.xrTable医嘱内容.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable医嘱内容.LocationFloat = new DevExpress.Utils.PointFloat(28.375F, 0F);
            this.xrTable医嘱内容.Name = "xrTable医嘱内容";
            this.xrTable医嘱内容.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable医嘱内容.SizeF = new System.Drawing.SizeF(758.8464F, 36F);
            this.xrTable医嘱内容.StylePriority.UseBorders = false;
            this.xrTable医嘱内容.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable医嘱内容_BeforePrint);
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell开嘱日期,
            this.xrTableCell开嘱时间,
            this.xrTableCell医嘱内容,
            this.xrTableCell剂量数量,
            this.xrTableCell组别符号,
            this.xrTableCell用法,
            this.xrTableCell频次,
            this.xrTableCell医师,
            this.xrTableCell护士,
            this.xrTableCell停嘱日期,
            this.xrTableCell停嘱时间,
            this.xrTableCell医师签名,
            this.xrTableCell真正停嘱时间});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell开嘱日期
            // 
            this.xrTableCell开嘱日期.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell开嘱日期.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt长期医嘱YsImage.开嘱日期")});
            this.xrTableCell开嘱日期.FormattingRules.Add(this.formattingRule续打不显示医嘱内容);
            this.xrTableCell开嘱日期.FormattingRules.Add(this.formattingRule续打不显示医嘱边框);
            this.xrTableCell开嘱日期.Name = "xrTableCell开嘱日期";
            this.xrTableCell开嘱日期.StylePriority.UseBorders = false;
            this.xrTableCell开嘱日期.StylePriority.UseTextAlignment = false;
            this.xrTableCell开嘱日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell开嘱日期.Weight = 0.27056526192453262D;
            this.xrTableCell开嘱日期.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCell开嘱日期_BeforePrint);
            // 
            // formattingRule续打不显示医嘱内容
            // 
            this.formattingRule续打不显示医嘱内容.Condition = " [Parameters.para续打] == True  And [真正开嘱时间]< [Parameters.s续打时间点]";
            // 
            // 
            // 
            this.formattingRule续打不显示医嘱内容.Formatting.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.formattingRule续打不显示医嘱内容.Formatting.ForeColor = System.Drawing.Color.White;
            this.formattingRule续打不显示医嘱内容.Formatting.Visible = DevExpress.Utils.DefaultBoolean.True;
            this.formattingRule续打不显示医嘱内容.Name = "formattingRule续打不显示医嘱内容";
            // 
            // formattingRule续打不显示医嘱边框
            // 
            this.formattingRule续打不显示医嘱边框.Condition = "[Parameters.para续打] == True";
            // 
            // 
            // 
            this.formattingRule续打不显示医嘱边框.Formatting.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.formattingRule续打不显示医嘱边框.Name = "formattingRule续打不显示医嘱边框";
            // 
            // xrTableCell开嘱时间
            // 
            this.xrTableCell开嘱时间.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell开嘱时间.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt长期医嘱YsImage.开嘱时间")});
            this.xrTableCell开嘱时间.FormattingRules.Add(this.formattingRule续打不显示医嘱内容);
            this.xrTableCell开嘱时间.FormattingRules.Add(this.formattingRule续打不显示医嘱边框);
            this.xrTableCell开嘱时间.Name = "xrTableCell开嘱时间";
            this.xrTableCell开嘱时间.StylePriority.UseBorders = false;
            this.xrTableCell开嘱时间.StylePriority.UseTextAlignment = false;
            this.xrTableCell开嘱时间.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell开嘱时间.Weight = 0.19001786210122118D;
            this.xrTableCell开嘱时间.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCell开嘱时间_BeforePrint);
            // 
            // xrTableCell医嘱内容
            // 
            this.xrTableCell医嘱内容.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell医嘱内容.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt长期医嘱YsImage.医嘱内容")});
            this.xrTableCell医嘱内容.FormattingRules.Add(this.formattingRule续打不显示医嘱内容);
            this.xrTableCell医嘱内容.FormattingRules.Add(this.formattingRule续打不显示医嘱边框);
            this.xrTableCell医嘱内容.Name = "xrTableCell医嘱内容";
            this.xrTableCell医嘱内容.StylePriority.UseBorders = false;
            this.xrTableCell医嘱内容.StylePriority.UseTextAlignment = false;
            this.xrTableCell医嘱内容.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell医嘱内容.Weight = 0.62579722331135912D;
            // 
            // xrTableCell剂量数量
            // 
            this.xrTableCell剂量数量.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell剂量数量.BorderWidth = 1F;
            this.xrTableCell剂量数量.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt长期医嘱YsImage.剂量数量单位")});
            this.xrTableCell剂量数量.FormattingRules.Add(this.formattingRule续打不显示医嘱内容);
            this.xrTableCell剂量数量.FormattingRules.Add(this.formattingRule续打不显示医嘱边框);
            this.xrTableCell剂量数量.Name = "xrTableCell剂量数量";
            this.xrTableCell剂量数量.StylePriority.UseBorders = false;
            this.xrTableCell剂量数量.StylePriority.UseBorderWidth = false;
            this.xrTableCell剂量数量.StylePriority.UseTextAlignment = false;
            this.xrTableCell剂量数量.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell剂量数量.Weight = 0.15500635231537557D;
            this.xrTableCell剂量数量.WordWrap = false;
            // 
            // xrTableCell组别符号
            // 
            this.xrTableCell组别符号.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell组别符号.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt长期医嘱YsImage.组别符号")});
            this.xrTableCell组别符号.FormattingRules.Add(this.formattingRule续打不显示医嘱内容);
            this.xrTableCell组别符号.FormattingRules.Add(this.formattingRule续打不显示医嘱边框);
            this.xrTableCell组别符号.Name = "xrTableCell组别符号";
            this.xrTableCell组别符号.StylePriority.UseBorders = false;
            this.xrTableCell组别符号.StylePriority.UseTextAlignment = false;
            this.xrTableCell组别符号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell组别符号.Weight = 0.04011743280808D;
            this.xrTableCell组别符号.WordWrap = false;
            // 
            // xrTableCell用法
            // 
            this.xrTableCell用法.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell用法.BorderWidth = 1F;
            this.xrTableCell用法.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt长期医嘱YsImage.用法名称")});
            this.xrTableCell用法.FormattingRules.Add(this.formattingRule续打不显示医嘱内容);
            this.xrTableCell用法.FormattingRules.Add(this.formattingRule续打不显示医嘱边框);
            this.xrTableCell用法.Name = "xrTableCell用法";
            this.xrTableCell用法.StylePriority.UseBorders = false;
            this.xrTableCell用法.StylePriority.UseBorderWidth = false;
            this.xrTableCell用法.StylePriority.UseTextAlignment = false;
            this.xrTableCell用法.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell用法.Weight = 0.14724640122752991D;
            this.xrTableCell用法.WordWrap = false;
            this.xrTableCell用法.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCell用法_BeforePrint);
            // 
            // xrTableCell频次
            // 
            this.xrTableCell频次.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell频次.BorderWidth = 1F;
            this.xrTableCell频次.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt长期医嘱YsImage.频次名称")});
            this.xrTableCell频次.FormattingRules.Add(this.formattingRule续打不显示医嘱内容);
            this.xrTableCell频次.FormattingRules.Add(this.formattingRule续打不显示医嘱边框);
            this.xrTableCell频次.Name = "xrTableCell频次";
            this.xrTableCell频次.StylePriority.UseBorders = false;
            this.xrTableCell频次.StylePriority.UseBorderWidth = false;
            this.xrTableCell频次.StylePriority.UseTextAlignment = false;
            this.xrTableCell频次.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell频次.Weight = 0.11124929232933933D;
            this.xrTableCell频次.WordWrap = false;
            this.xrTableCell频次.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCell频次_BeforePrint);
            // 
            // xrTableCell医师
            // 
            this.xrTableCell医师.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell医师.FormattingRules.Add(this.formattingRule续打不显示医嘱内容);
            this.xrTableCell医师.FormattingRules.Add(this.formattingRule续打不显示医嘱边框);
            this.xrTableCell医师.Name = "xrTableCell医师";
            this.xrTableCell医师.StylePriority.UseBorders = false;
            this.xrTableCell医师.StylePriority.UseTextAlignment = false;
            this.xrTableCell医师.Text = "..";
            this.xrTableCell医师.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell医师.Weight = 0.21391928678991806D;
            this.xrTableCell医师.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCell医师_BeforePrint);
            // 
            // xrTableCell护士
            // 
            this.xrTableCell护士.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell护士.FormattingRules.Add(this.formattingRule续打不显示医嘱内容);
            this.xrTableCell护士.FormattingRules.Add(this.formattingRule续打不显示医嘱边框);
            this.xrTableCell护士.Name = "xrTableCell护士";
            this.xrTableCell护士.StylePriority.UseBorders = false;
            this.xrTableCell护士.StylePriority.UseTextAlignment = false;
            this.xrTableCell护士.Text = "..";
            this.xrTableCell护士.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell护士.Weight = 0.18053046214066465D;
            this.xrTableCell护士.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCell护士_BeforePrint);
            // 
            // xrTableCell停嘱日期
            // 
            this.xrTableCell停嘱日期.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell停嘱日期.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt长期医嘱YsImage.停嘱日期")});
            this.xrTableCell停嘱日期.FormattingRules.Add(this.formattingRule续打不显示停嘱日期时间);
            this.xrTableCell停嘱日期.FormattingRules.Add(this.formattingRule续打不显示医嘱边框);
            this.xrTableCell停嘱日期.Name = "xrTableCell停嘱日期";
            this.xrTableCell停嘱日期.StylePriority.UseBorders = false;
            this.xrTableCell停嘱日期.StylePriority.UseTextAlignment = false;
            this.xrTableCell停嘱日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell停嘱日期.Weight = 0.19236436984195213D;
            this.xrTableCell停嘱日期.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCell停嘱日期_BeforePrint);
            // 
            // formattingRule续打不显示停嘱日期时间
            // 
            this.formattingRule续打不显示停嘱日期时间.Condition = "[Parameters.para续打] == True And [真正停嘱时间]  <  [Parameters.s续打时间点]";
            // 
            // 
            // 
            this.formattingRule续打不显示停嘱日期时间.Formatting.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.formattingRule续打不显示停嘱日期时间.Formatting.ForeColor = System.Drawing.Color.White;
            this.formattingRule续打不显示停嘱日期时间.Formatting.Visible = DevExpress.Utils.DefaultBoolean.True;
            this.formattingRule续打不显示停嘱日期时间.Name = "formattingRule续打不显示停嘱日期时间";
            // 
            // xrTableCell停嘱时间
            // 
            this.xrTableCell停嘱时间.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell停嘱时间.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt长期医嘱YsImage.停嘱时间")});
            this.xrTableCell停嘱时间.FormattingRules.Add(this.formattingRule续打不显示停嘱日期时间);
            this.xrTableCell停嘱时间.FormattingRules.Add(this.formattingRule续打不显示医嘱边框);
            this.xrTableCell停嘱时间.Name = "xrTableCell停嘱时间";
            this.xrTableCell停嘱时间.StylePriority.UseBorders = false;
            this.xrTableCell停嘱时间.StylePriority.UseTextAlignment = false;
            this.xrTableCell停嘱时间.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell停嘱时间.Weight = 0.17702658240010108D;
            this.xrTableCell停嘱时间.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCell停嘱时间_BeforePrint);
            // 
            // xrTableCell医师签名
            // 
            this.xrTableCell医师签名.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell医师签名.FormattingRules.Add(this.formattingRule续打不显示医嘱内容);
            this.xrTableCell医师签名.FormattingRules.Add(this.formattingRule续打不显示医嘱边框);
            this.xrTableCell医师签名.Name = "xrTableCell医师签名";
            this.xrTableCell医师签名.StylePriority.UseBorders = false;
            this.xrTableCell医师签名.Weight = 0.23927538143480795D;
            // 
            // xrTableCell真正停嘱时间
            // 
            this.xrTableCell真正停嘱时间.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell真正停嘱时间.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt长期医嘱YsImage.真正停嘱时间")});
            this.xrTableCell真正停嘱时间.ForeColor = System.Drawing.Color.Transparent;
            this.xrTableCell真正停嘱时间.FormattingRules.Add(this.formattingRule续打不显示医嘱内容);
            this.xrTableCell真正停嘱时间.FormattingRules.Add(this.formattingRule续打不显示医嘱边框);
            this.xrTableCell真正停嘱时间.Name = "xrTableCell真正停嘱时间";
            this.xrTableCell真正停嘱时间.StylePriority.UseBorders = false;
            this.xrTableCell真正停嘱时间.StylePriority.UseForeColor = false;
            this.xrTableCell真正停嘱时间.Weight = 0.25031900235855892D;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 5F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel8.FormattingRules.Add(this.formattingRule续打不显示标题页码边框);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(95.08331F, 77.95833F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(618.75F, 22.99999F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "长   期   医   嘱   单";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // formattingRule续打不显示标题页码边框
            // 
            this.formattingRule续打不显示标题页码边框.Condition = "[Parameters.para续打] == True";
            // 
            // 
            // 
            this.formattingRule续打不显示标题页码边框.Formatting.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.formattingRule续打不显示标题页码边框.Formatting.ForeColor = System.Drawing.Color.White;
            this.formattingRule续打不显示标题页码边框.Name = "formattingRule续打不显示标题页码边框";
            // 
            // xrLabel病历号
            // 
            this.xrLabel病历号.FormattingRules.Add(this.formattingRule续打不显示标题页码边框);
            this.xrLabel病历号.LocationFloat = new DevExpress.Utils.PointFloat(641.715F, 106.2083F);
            this.xrLabel病历号.Name = "xrLabel病历号";
            this.xrLabel病历号.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel病历号.SizeF = new System.Drawing.SizeF(144.2849F, 22.99998F);
            this.xrLabel病历号.StylePriority.UseTextAlignment = false;
            this.xrLabel病历号.Text = "病历号：";
            this.xrLabel病历号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel床号
            // 
            this.xrLabel床号.FormattingRules.Add(this.formattingRule续打不显示标题页码边框);
            this.xrLabel床号.LocationFloat = new DevExpress.Utils.PointFloat(472.1252F, 106.2083F);
            this.xrLabel床号.Name = "xrLabel床号";
            this.xrLabel床号.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel床号.SizeF = new System.Drawing.SizeF(141.6247F, 22.99998F);
            this.xrLabel床号.StylePriority.UseTextAlignment = false;
            this.xrLabel床号.Text = "床号：";
            this.xrLabel床号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel病室
            // 
            this.xrLabel病室.FormattingRules.Add(this.formattingRule续打不显示标题页码边框);
            this.xrLabel病室.LocationFloat = new DevExpress.Utils.PointFloat(309.1667F, 106.2083F);
            this.xrLabel病室.Name = "xrLabel病室";
            this.xrLabel病室.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel病室.SizeF = new System.Drawing.SizeF(152.0833F, 22.99998F);
            this.xrLabel病室.StylePriority.UseTextAlignment = false;
            this.xrLabel病室.Text = "病室：";
            this.xrLabel病室.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel科别
            // 
            this.xrLabel科别.FormattingRules.Add(this.formattingRule续打不显示标题页码边框);
            this.xrLabel科别.LocationFloat = new DevExpress.Utils.PointFloat(171.6667F, 106.2083F);
            this.xrLabel科别.Name = "xrLabel科别";
            this.xrLabel科别.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel科别.SizeF = new System.Drawing.SizeF(126.0416F, 22.99998F);
            this.xrLabel科别.StylePriority.UseTextAlignment = false;
            this.xrLabel科别.Text = "科别：";
            this.xrLabel科别.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel姓名
            // 
            this.xrLabel姓名.FormattingRules.Add(this.formattingRule续打不显示标题页码边框);
            this.xrLabel姓名.LocationFloat = new DevExpress.Utils.PointFloat(28.37499F, 104.2083F);
            this.xrLabel姓名.Name = "xrLabel姓名";
            this.xrLabel姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel姓名.SizeF = new System.Drawing.SizeF(110.7083F, 23.00002F);
            this.xrLabel姓名.StylePriority.UseTextAlignment = false;
            this.xrLabel姓名.Text = "姓名：";
            this.xrLabel姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel医院名称
            // 
            this.xrLabel医院名称.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrLabel医院名称.FormattingRules.Add(this.formattingRule续打不显示标题页码边框);
            this.xrLabel医院名称.LocationFloat = new DevExpress.Utils.PointFloat(60.66661F, 49.95832F);
            this.xrLabel医院名称.Name = "xrLabel医院名称";
            this.xrLabel医院名称.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel医院名称.SizeF = new System.Drawing.SizeF(693.75F, 22.99999F);
            this.xrLabel医院名称.StylePriority.UseFont = false;
            this.xrLabel医院名称.StylePriority.UseTextAlignment = false;
            this.xrLabel医院名称.Text = "XX医院";
            this.xrLabel医院名称.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTable3
            // 
            this.xrTable3.FormattingRules.Add(this.formattingRule续打不显示标题页码边框);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(28.37499F, 129.2083F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(758.5F, 35F);
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell12,
            this.xrTableCell13});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell12.FormattingRules.Add(this.formattingRule续打不显示标题页码边框);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.StylePriority.UseBorders = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "起    始";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell12.Weight = 2.3494778596383088D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell13.FormattingRules.Add(this.formattingRule续打不显示标题页码边框);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseBorders = false;
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "停   止";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell13.Weight = 1.0417285276619133D;
            // 
            // xrTable2
            // 
            this.xrTable2.FormattingRules.Add(this.formattingRule续打不显示标题页码边框);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(28.37499F, 164.2083F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(758.8464F, 35F);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell11,
            this.xrTableCell15,
            this.xrTableCell9,
            this.xrTableCell14,
            this.xrTableCell3,
            this.xrTableCell10,
            this.xrTableCell16});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.FormattingRules.Add(this.formattingRule续打不显示标题页码边框);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "日期";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 0.32720351432064804D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell2.FormattingRules.Add(this.formattingRule续打不显示标题页码边框);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "时间";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell2.Weight = 0.23219609614966591D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell11.FormattingRules.Add(this.formattingRule续打不显示标题页码边框);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.Text = "医嘱内容";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell11.Weight = 1.3157599923692998D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.FormattingRules.Add(this.formattingRule续打不显示标题页码边框);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "医师";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell15.Weight = 0.25505706059417504D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell9.FormattingRules.Add(this.formattingRule续打不显示标题页码边框);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "护士";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell9.Weight = 0.21926156170499667D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell14.FormattingRules.Add(this.formattingRule续打不显示标题页码边框);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "日期";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell14.Weight = 0.23363684186636952D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell3.FormattingRules.Add(this.formattingRule续打不显示标题页码边框);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "时间";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 0.21500592864217064D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell10.FormattingRules.Add(this.formattingRule续打不显示标题页码边框);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.Text = "医师签名";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell10.Weight = 0.290611034001039D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell16.FormattingRules.Add(this.formattingRule续打不显示标题页码边框);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.Text = "护士签名";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell16.Weight = 0.30402400419005D;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 16F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.HeightF = 43F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Format = "第 {0} 页";
            this.xrPageInfo1.FormattingRules.Add(this.formattingRule续打不显示标题页码边框);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(365.602F, 10.00001F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.Number;
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(64.93964F, 23F);
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // para续打
            // 
            this.para续打.Description = "是否续打:";
            this.para续打.Name = "para续打";
            this.para续打.Type = typeof(bool);
            this.para续打.ValueInfo = "False";
            this.para续打.Visible = false;
            // 
            // s续打时间点
            // 
            this.s续打时间点.Description = "续打时间点";
            this.s续打时间点.Name = "s续打时间点";
            this.s续打时间点.Visible = false;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel科别,
            this.xrTable3,
            this.xrLabel医院名称,
            this.xrLabel姓名,
            this.xrTable2,
            this.xrLabel病室,
            this.xrLabel床号,
            this.xrLabel病历号,
            this.xrLabel8});
            this.PageHeader.HeightF = 200F;
            this.PageHeader.Name = "PageHeader";
            // 
            // XtraReport长期医嘱
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageFooter,
            this.PageHeader});
            this.DataMember = "dt长期医嘱YsImage";
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule续打不显示标题页码边框,
            this.formattingRule续打不显示医嘱内容,
            this.formattingRule续打不显示停嘱日期时间,
            this.formattingRule续打不显示医嘱边框});
            this.Margins = new System.Drawing.Printing.Margins(6, 5, 5, 16);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.para续打,
            this.s续打时间点});
            this.Version = "13.2";
            this.FillEmptySpace += new DevExpress.XtraReports.UI.BandEventHandler(this.XtraReport长期医嘱_FillEmptySpace);
            this.ParametersRequestSubmit += new System.EventHandler<DevExpress.XtraReports.Parameters.ParametersRequestEventArgs>(this.XtraReport长期医嘱_ParametersRequestSubmit);
            this.AfterPrint += new System.EventHandler(this.XtraReport长期医嘱_AfterPrint);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable医嘱内容)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel病历号;
        private DevExpress.XtraReports.UI.XRLabel xrLabel床号;
        private DevExpress.XtraReports.UI.XRLabel xrLabel病室;
        private DevExpress.XtraReports.UI.XRLabel xrLabel科别;
        private DevExpress.XtraReports.UI.XRLabel xrLabel姓名;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        //private Db.DataSetYsgzz emr1;
        //private Db.DataSetYsgzzTableAdapters.dt长期医嘱TableAdapter dt长期医嘱TableAdapter;
        //private Db.DataSetYsgzzTableAdapters.dt长期医嘱YsImageTableAdapter dt长期医嘱YsImageTableAdapter1;
        private DevExpress.XtraReports.UI.XRTable xrTable医嘱内容;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell开嘱日期;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell医嘱内容;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell真正停嘱时间;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell医师;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell护士;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell停嘱日期;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell医师签名;
        private DevExpress.XtraReports.UI.XRLabel xrLabel医院名称;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell停嘱时间;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell开嘱时间;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell剂量数量;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell组别符号;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell用法;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell频次;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule续打不显示标题页码边框;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule续打不显示医嘱内容;
        public DevExpress.XtraReports.Parameters.Parameter para续打;
        public DevExpress.XtraReports.Parameters.Parameter s续打时间点;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule续打不显示停嘱日期时间;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule续打不显示医嘱边框;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
    }
}
