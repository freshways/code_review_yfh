﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

namespace AtomEHR.公共卫生.Module.医疗信息.诊疗记录
{
    public partial class Frm住院信息 : Form
    {
        private DataTable dt临时医嘱 = null;
        private DataTable dt长期医嘱 = null;
        private DataTable dt住院病人信息 = null;
        private string str医院名称 = null;
        private string str病历信息 = null;
        public Frm住院信息()
        {
            InitializeComponent();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="yyid">医院编号</param>
        /// <param name="zyid">住院id</param>
        public Frm住院信息(string yyid, string zyid)
        {
            InitializeComponent();
            if (Check(yyid, zyid))
            {
                GetAllInfos(yyid, zyid);

                Show长期医嘱();
                Show临时医嘱();
                Show病历信息();
            }
        }

        private void GetAllInfos(string yyid, string zyid)
        {
            dt临时医嘱 = Get临时医嘱(yyid, zyid);
            dt长期医嘱 = Get长期医嘱(yyid, zyid);
            dt住院病人信息 = Get住院病人信息(yyid, zyid);
            str医院名称 = Get医院名称(yyid);
            str病历信息 = Get病历信息(yyid, zyid);
        }

        private void Show临时医嘱()
        {
            if (str医院名称!=null && dt临时医嘱!=null && dt住院病人信息!= null)
            {
                //DetailReport.XtraReport临时医嘱 rep = new DetailReport.XtraReport临时医嘱(str医院名称, dt住院病人信息, dt临时医嘱);
                XtraReport临时医嘱 rep = new XtraReport临时医嘱(str医院名称, dt住院病人信息, dt临时医嘱);
                ucTabPage2.documentViewer1.DocumentSource = rep;
                rep.CreateDocument();
            }
            else
            {
                ucTabPage2.Visible = false;
            }
        }

        private void Show长期医嘱()
        {
            if (str医院名称 != null && dt长期医嘱 != null && dt住院病人信息 != null)
            {
                //DetailReport.XtraReport长期医嘱 rep = new DetailReport.XtraReport长期医嘱(str医院名称, dt住院病人信息, dt长期医嘱);
                XtraReport长期医嘱 rep = new XtraReport长期医嘱(str医院名称, dt住院病人信息, dt长期医嘱);
                ucTabPage1.documentViewer1.DocumentSource = rep;
                rep.CreateDocument();
            }
            else
            {
                ucTabPage1.Visible = false;
            }
        }

        private void Show病历信息()
        {
            if(str病历信息 != null)
            {
                //label3.Text = str病历信息;
                webBrowser1.DocumentText = str病历信息;
            }
        }

        //获取临时医嘱，并将临时医嘱存入Session["zhy临时医嘱"] 类型DataTable
        private DataTable Get临时医嘱(string yyid, string zyid)
        {
            DataTable dt = null;
            try
            {
                string constr = GetHisCon();
                string sql = "";
                #region 临时医嘱sql语句--包含历史医嘱
//                sql = "use chis" + yyid + @";
//select * 
// from
// (
//SELECT   aa.id,aa.组别, aa.开嘱时间 kzsj,
//		 CONVERT(VARCHAR(100), aa.开嘱时间, 23) 开嘱日期,
//         left(CONVERT(VARCHAR(100), aa.开嘱时间, 108), 5) 开嘱时间,
//         aa.[项目名称] AS 医嘱内容,
//         isnull(cast(剂量数量 as varchar(20)),'') + isnull(aa.[剂量单位], '') 剂量数量单位,
//         isnull(aa.[组别符号], ISNULL(组别,'')) 组别符号,
//         yf.用法名称,
//         pc.频次名称,
//         bb.用户名 AS 开嘱医生,
//         cc.用户名 AS 执行开嘱护士,
//         left(CONVERT(VARCHAR(100), aa.执行时间, 108), 5) AS 执行时间,aa.执行时间 真正执行时间,
//aa.开嘱时间 真正开嘱时间,
//aa.开嘱核对者编码
//FROM     YS住院医嘱 AS aa
//         LEFT OUTER JOIN pubUser AS bb ON aa.开嘱医生编码 = bb.用户编码
//         LEFT OUTER JOIN pubUser AS cc ON aa.开嘱执行者编码 = cc.用户编码
//         LEFT OUTER JOIN pubUser AS dd ON aa.停嘱医生编码 = dd.用户编码
//         LEFT OUTER JOIN pubUser AS ee ON aa.停嘱执行者编码 = ee.用户编码
//         LEFT OUTER JOIN ZY病人信息 AS zy ON aa.ZYID = zy.ZYID
//         left outer join [YS医嘱频次] as pc on aa.[频次名称] = pc.[频次名称]
//         left outer join [YS医嘱用法] as yf on aa.[用法名称] = yf.[用法名称]
//WHERE    (zy.ZYID = @zyid) AND (aa.医嘱类型 = '临时医嘱')
//
//union all 
//
//SELECT   aa.id,aa.组别, aa.开嘱时间 kzsj,
//		 CONVERT(VARCHAR(100), aa.开嘱时间, 23) 开嘱日期,
//         left(CONVERT(VARCHAR(100), aa.开嘱时间, 108), 5) 开嘱时间,
//         aa.[项目名称] AS 医嘱内容,
//         isnull(cast(剂量数量 as varchar(20)),'') + isnull(aa.[剂量单位], '') 剂量数量单位,
//         isnull(aa.[组别符号], ISNULL(组别,'')) 组别符号,
//         yf.用法名称,
//         pc.频次名称,
//         bb.用户名 AS 开嘱医生,
//         cc.用户名 AS 执行开嘱护士,
//         left(CONVERT(VARCHAR(100), aa.执行时间, 108), 5) AS 执行时间,aa.执行时间 真正执行时间,
//aa.开嘱时间 真正开嘱时间,
//aa.开嘱核对者编码
//FROM     YS住院医嘱历史 AS aa
//         LEFT OUTER JOIN pubUser AS bb ON aa.开嘱医生编码 = bb.用户编码
//         LEFT OUTER JOIN pubUser AS cc ON aa.开嘱执行者编码 = cc.用户编码
//         LEFT OUTER JOIN pubUser AS dd ON aa.停嘱医生编码 = dd.用户编码
//         LEFT OUTER JOIN pubUser AS ee ON aa.停嘱执行者编码 = ee.用户编码
//         LEFT OUTER JOIN ZY病人信息 AS zy ON aa.ZYID = zy.ZYID
//         left outer join [YS医嘱频次] as pc on aa.[频次名称] = pc.[频次名称]
//         left outer join [YS医嘱用法] as yf on aa.[用法名称] = yf.[用法名称]
//WHERE    (zy.ZYID = @zyid) AND (aa.医嘱类型 = '临时医嘱')
//) aaa
//order by aaa.组别 asc,aaa.kzsj asc, aaa.id asc
//";
                #endregion

                #region 修改后的临时医嘱查询sql语句
                sql = sql = "use chis" + yyid + @";
SELECT aa.id,
      CONVERT(VARCHAR(100), aa.开嘱时间, 23) 开嘱日期,
       left(CONVERT(VARCHAR(100), aa.开嘱时间, 108), 5) 开嘱时间,
       aa.医嘱内涵 AS 医嘱内容,
      isnull([dbo].[ClearZero](剂量数量), '') + isnull(aa.[剂量单位], '') 剂量数量单位,
       isnull(aa.[组别符号], '') 组别符号,
       yf.[用法简写] 用法名称,
       pc.[频次简写] 频次名称,
       right(CONVERT(VARCHAR(100), aa.停嘱时间, 23), 5) 停嘱日期,
       left(CONVERT(VARCHAR(100), aa.停嘱时间, 108), 5) 停嘱时间,
       bb.手签照片 AS 开嘱医生,
       cc.手签照片 AS 执行开嘱护士,
       CONVERT(varchar(10), aa.执行时间, 120) + ',' + CONVERT(varchar(5), aa.执行时间, 114) AS 执行时间,
       aa.[已打印标志],aa.停嘱时间 真正停嘱时间,aa.开嘱时间 真正开嘱时间,aa.执行时间 真正执行时间
FROM   
(SELECT ID, ZYID, [医嘱类型], [组别], [项目类型],
       [项目编码], [项目名称], [剂量数量], [剂量单位], [医嘱数量],
       [频次名称], [用法名称], [开嘱医生编码], [开嘱时间], [开嘱执行者编码],
       [开嘱核对者编码], [执行时间], [停嘱医生编码], [停嘱执行者编码], [停嘱核对者编码],
       [停嘱时间], [单日总量], [频次数量], createTime, [临床路径ID],
       [组别符号], [库存数量], [药房规格], [药房单位], [药房编码],
       [更新标志], [单次数量], [财务分类], [页号],
       [行号], [已打印标志], [子嘱ID], [医嘱内涵], [医嘱备注],
       [停嘱原因], [作废原因], [医嘱ID]
FROM   [YS住院医嘱]
union all
SELECT ID, ZYID, [医嘱类型], [组别], [项目类型],
       [项目编码], [项目名称], [剂量数量], [剂量单位], [医嘱数量],
       [频次名称], [用法名称], [开嘱医生编码], [开嘱时间], [开嘱执行者编码],
       [开嘱核对者编码], [执行时间], [停嘱医生编码], [停嘱执行者编码], [停嘱核对者编码],
       [停嘱时间], [单日总量], [频次数量], createTime, [临床路径ID],
       [组别符号], [库存数量], [药房规格], [药房单位], [药房编码],
       [更新标志], [单次数量], [财务分类], [页号],
       [行号], [已打印标志], [子嘱ID], [医嘱内涵], [医嘱备注],
       [停嘱原因], [作废原因], [在院医嘱ID]
FROM   [YS住院医嘱历史] )
AS aa
       LEFT OUTER JOIN pubUser AS bb ON aa.开嘱医生编码 = bb.用户编码
       LEFT OUTER JOIN pubUser AS cc ON aa.开嘱执行者编码 = cc.用户编码
       LEFT OUTER JOIN pubUser AS dd ON aa.停嘱医生编码 = dd.用户编码
       LEFT OUTER JOIN pubUser AS ee ON aa.停嘱执行者编码 = ee.用户编码
       left outer join [YS医嘱频次] as pc on aa.[频次名称] = pc.[频次名称]
       left outer join [YS医嘱用法] as yf on aa.[用法名称] = yf.[用法名称]
WHERE  (aa.zyid = @zyid) AND (aa.医嘱类型 = '临时医嘱')
order by aa.组别 asc,aa.开嘱时间 asc, aa.id asc
";
                #endregion

                SqlParameter[] sqlparam = new SqlParameter[1];
                sqlparam[0] = new SqlParameter("@zyid", zyid);
                dt = tykClass.Db.SqlHelper.ExecuteDataset(constr, CommandType.Text, sql, sqlparam).Tables[0];
            }
            catch
            {
                dt = null;
            }
            return dt;
        }

        //获取长期医嘱，并将长期医嘱存入Session["zhy长期医嘱"] 类型DataTable
        private DataTable Get长期医嘱(string yyid, string zyid)
        {
            DataTable dt = null;
            try
            {
                string constr = GetHisCon();
                string sql = "";
                #region 查询长期医嘱的sql语句---包含历史医嘱
//                sql = "use chis" + yyid + @";
//select * 
//from
//(
//   SELECT aa.id,aa.组别,aa.开嘱时间 kzsj,
//      CONVERT(VARCHAR(100), aa.开嘱时间, 23) 开嘱日期,
//       left(CONVERT(VARCHAR(100), aa.开嘱时间, 108), 5) 开嘱时间,
//       aa.[项目名称] AS 医嘱内容,
//      isnull(cast(剂量数量 as varchar(20)), '') + isnull(aa.[剂量单位], '') 剂量数量单位,
//       isnull(aa.[组别符号], ISNULL(组别,'')) 组别符号,
//       yf.用法名称,
//       pc.频次名称,
//       right(CONVERT(VARCHAR(100), aa.停嘱时间, 23), 5) 停嘱日期,
//       left(CONVERT(VARCHAR(100), aa.停嘱时间, 108), 5) 停嘱时间,
//       bb.用户名 AS 开嘱医生,
//       cc.用户名 AS 执行开嘱护士,
//       CONVERT(varchar(10), aa.执行时间, 120) + ',' + CONVERT(varchar(5), aa.开嘱时间, 114) AS 执行时间,
//       aa.停嘱时间 真正停嘱时间,aa.开嘱时间 真正开嘱时间
//FROM   YS住院医嘱历史 aa
//       LEFT OUTER JOIN pubUser AS bb ON aa.开嘱医生编码 = bb.用户编码
//       LEFT OUTER JOIN pubUser AS cc ON aa.开嘱执行者编码 = cc.用户编码
//       LEFT OUTER JOIN pubUser AS dd ON aa.停嘱医生编码 = dd.用户编码
//       LEFT OUTER JOIN pubUser AS ee ON aa.停嘱执行者编码 = ee.用户编码
//       LEFT OUTER JOIN ZY病人信息 AS zy ON aa.ZYID = zy.ZYID
//       left outer join [YS医嘱频次] as pc on aa.[频次名称] = pc.[频次名称]
//       left outer join [YS医嘱用法] as yf on aa.[用法名称] = yf.[用法名称]
//WHERE  (zy.ZYID = @zyid) AND (aa.医嘱类型 = '长期医嘱')
//union all
//SELECT aa.id,aa.组别,aa.开嘱时间 kzsj,
//      CONVERT(VARCHAR(100), aa.开嘱时间, 23) 开嘱日期,
//       left(CONVERT(VARCHAR(100), aa.开嘱时间, 108), 5) 开嘱时间,
//       aa.[项目名称] AS 医嘱内容,
//      isnull(cast(剂量数量 as varchar(20)), '') + isnull(aa.[剂量单位], '') 剂量数量单位,
//       isnull(aa.[组别符号], ISNULL(组别,'')) 组别符号,
//       yf.用法名称,
//       pc.频次名称,
//       right(CONVERT(VARCHAR(100), aa.停嘱时间, 23), 5) 停嘱日期,
//       left(CONVERT(VARCHAR(100), aa.停嘱时间, 108), 5) 停嘱时间,
//       bb.用户名 AS 开嘱医生,
//       cc.用户名 AS 执行开嘱护士,
//       CONVERT(varchar(10), aa.执行时间, 120) + ',' + CONVERT(varchar(5), aa.开嘱时间, 114) AS 执行时间,
//       aa.停嘱时间 真正停嘱时间,aa.开嘱时间 真正开嘱时间
//FROM   YS住院医嘱 AS aa
//       LEFT OUTER JOIN pubUser AS bb ON aa.开嘱医生编码 = bb.用户编码
//       LEFT OUTER JOIN pubUser AS cc ON aa.开嘱执行者编码 = cc.用户编码
//       LEFT OUTER JOIN pubUser AS dd ON aa.停嘱医生编码 = dd.用户编码
//       LEFT OUTER JOIN pubUser AS ee ON aa.停嘱执行者编码 = ee.用户编码
//       LEFT OUTER JOIN ZY病人信息 AS zy ON aa.ZYID = zy.ZYID
//       left outer join [YS医嘱频次] as pc on aa.[频次名称] = pc.[频次名称]
//       left outer join [YS医嘱用法] as yf on aa.[用法名称] = yf.[用法名称]
//WHERE  (zy.ZYID = @zyid) AND (aa.医嘱类型 = '长期医嘱')
//) aaa
//order by aaa.组别 asc,aaa.kzsj asc, aaa.id asc";
                #endregion

                #region 修改后的长期医嘱
                sql = "use chis" + yyid + @";
SELECT aa.id,
      CONVERT(VARCHAR(100), aa.开嘱时间, 23) 开嘱日期,
       left(CONVERT(VARCHAR(100), aa.开嘱时间, 108), 5) 开嘱时间,
       aa.医嘱内涵 AS 医嘱内容,
      isnull([dbo].[ClearZero](剂量数量), '') + isnull(aa.[剂量单位], '') 剂量数量单位,
       isnull(aa.[组别符号], '') 组别符号,
       yf.[用法简写] 用法名称,
       pc.[频次简写] 频次名称,
       right(CONVERT(VARCHAR(100), aa.停嘱时间, 23), 5) 停嘱日期,
       left(CONVERT(VARCHAR(100), aa.停嘱时间, 108), 5) 停嘱时间,
       bb.手签照片 AS 开嘱医生,
       cc.手签照片 AS 执行开嘱护士,
       CONVERT(varchar(10), aa.执行时间, 120) + ',' + CONVERT(varchar(5), aa.执行时间, 114) AS 执行时间,
       aa.[已打印标志],aa.停嘱时间 真正停嘱时间,aa.开嘱时间 真正开嘱时间
FROM   
(SELECT ID, ZYID, [医嘱类型], [组别], [项目类型],
       [项目编码], [项目名称], [剂量数量], [剂量单位], [医嘱数量],
       [频次名称], [用法名称], [开嘱医生编码], [开嘱时间], [开嘱执行者编码],
       [开嘱核对者编码], [执行时间], [停嘱医生编码], [停嘱执行者编码], [停嘱核对者编码],
       [停嘱时间], [单日总量], [频次数量], createTime, [临床路径ID],
       [组别符号], [库存数量], [药房规格], [药房单位], [药房编码],
       [更新标志], [单次数量], [财务分类], [页号],
       [行号], [已打印标志], [子嘱ID], [医嘱内涵], [医嘱备注],
       [停嘱原因], [作废原因], [医嘱ID]
FROM   [YS住院医嘱]
union all
SELECT ID, ZYID, [医嘱类型], [组别], [项目类型],
       [项目编码], [项目名称], [剂量数量], [剂量单位], [医嘱数量],
       [频次名称], [用法名称], [开嘱医生编码], [开嘱时间], [开嘱执行者编码],
       [开嘱核对者编码], [执行时间], [停嘱医生编码], [停嘱执行者编码], [停嘱核对者编码],
       [停嘱时间], [单日总量], [频次数量], createTime, [临床路径ID],
       [组别符号], [库存数量], [药房规格], [药房单位], [药房编码],
       [更新标志], [单次数量], [财务分类], [页号],
       [行号], [已打印标志], [子嘱ID], [医嘱内涵], [医嘱备注],
       [停嘱原因], [作废原因], [在院医嘱ID]
FROM   [YS住院医嘱历史] )
AS aa
       LEFT OUTER JOIN pubUser AS bb ON aa.开嘱医生编码 = bb.用户编码
       LEFT OUTER JOIN pubUser AS cc ON aa.开嘱执行者编码 = cc.用户编码
       LEFT OUTER JOIN pubUser AS dd ON aa.停嘱医生编码 = dd.用户编码
       LEFT OUTER JOIN pubUser AS ee ON aa.停嘱执行者编码 = ee.用户编码
       left outer join [YS医嘱频次] as pc on aa.[频次名称] = pc.[频次名称]
       left outer join [YS医嘱用法] as yf on aa.[用法名称] = yf.[用法名称]
WHERE  (aa.zyid = @zyid) AND (aa.医嘱类型 = '长期医嘱')
order by aa.组别 asc,aa.开嘱时间 asc, aa.id asc
";
                #endregion

                SqlParameter[] sqlparam = new SqlParameter[1];
                sqlparam[0] = new SqlParameter("@zyid", zyid);
                dt = tykClass.Db.SqlHelper.ExecuteDataset(constr, CommandType.Text, sql, sqlparam).Tables[0];
            }
            catch
            {
                dt = null;
            }
            return dt;
        }

        //获取住院病人信息，并存入Session["zhy病人信息"] DataTable类型
        private DataTable Get住院病人信息(string yyid, string zyid)
        {
            DataTable dt = null;
            try
            {
                string connstr = GetHisCon();
                string sql = "USE CHIS" + yyid + @";
SELECT top 1 [ZYID],[住院号码],[病人姓名],[病区],[病床],bb.科室名称 科室
FROM [ZY病人信息] aa left outer join GY科室设置 bb ON aa.科室 = bb.科室编码
where ZYID = '" + zyid + "'";
                dt = tykClass.Db.SqlHelper.ExecuteDataset(connstr, CommandType.Text, sql).Tables[0];
            }
            catch
            {
                dt = null;
            }
            return dt;
        }

        //获取医院名称,并存入Session["zhy医院名称"] string类型
        private string Get医院名称(string yyid)
        {
            string str医院名称 = "";
            if (yyid.Equals("2709") || yyid.Equals("109"))
            {
                str医院名称 = "沂水县第二人民医院";
            }
            else
            {
                try
                {
                    string connectionStr = GetHisCon();
                    string sql = "select deptid, deptname from chis2701.dbo.中心院列表 where deptid = @yyid";
                    SqlParameter[] sqlparams = new SqlParameter[1];
                    sqlparams[0] = new SqlParameter("@yyid", yyid);
                    str医院名称 = tykClass.Db.SqlHelper.ExecuteDataset(connectionStr, CommandType.Text, sql, sqlparams).Tables[0].Rows[0]["deptname"].ToString();
                }
                catch (Exception ex)
                {
                    str医院名称 = "医院名称";
                }
            }
            return str医院名称;
        }

        private string Get病历信息(string yyid, string zyid)
        {
            string text = "";
            string connectionStr = GetHisCon();

            string sql = "select 主题,病历内容,aa.ID  from  CHIS" + yyid + ".dbo.emr病历索引 aa left join  CHIS" + yyid + @".dbo.ZY病人信息 bb on aa.病案号 = bb.住院号码 
 where bb.ZYID = '" + zyid + "' order by 创建时间 ";

            DataTable dt = tykClass.Db.SqlHelper.ExecuteDataset(connectionStr, CommandType.Text, sql).Tables[0];

            #region 获取病历的Xml格式的数据
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                text += "<b>" + dt.Rows[i]["主题"].ToString() + "</b><br/>";

                XmlDocument xml = new XmlDocument();
                xml.LoadXml(dt.Rows[i]["病历内容"].ToString());
                //text += xml.Requestxml(dt.Rows[i]["病历内容"].ToString());//xml.ConvertXmlToEmrData() + "\n";
                text += xml.SelectSingleNode("/emrtextdoc/body").InnerXml;

            }
            #endregion

            #region 病历显示处理及入院记录的图片显示处理
            //for (int i = 0; i < dt.Rows.Count; i++)
            //{
            //    //获取病历主题
            //    string temp主题 = dt.Rows[i]["主题"].ToString();
            //    text += "<b>" + temp主题 + "</b><br/>";

            //    //获取病历内容
            //    string bingli_xml = dt.Rows[i]["病历内容"].ToString();

            //    //由于病历内容是以XML形式组织的，所以通过XmlDocument对象来操作病历内容
            //    XmlDocument xmlimg = new XmlDocument();
            //    xmlimg.LoadXml(bingli_xml);
            //    XmlNode imgnode = xmlimg.SelectSingleNode("/emrtextdoc/body/img");

            //    //在病历是入院记录，且包含图片的情况下
            //    if (bingli_xml.Contains("</img>") && temp主题.Equals("入院记录"))
            //    {
            //        imgnode.InnerText = "";

            //        XmlDocument imgnodedoc = imgnode.OwnerDocument;

            //        //修改img的高度，高度数据既存
            //        XmlAttribute imgheight = imgnodedoc.CreateAttribute("height");
            //        imgheight.Value = "41";
            //        imgnode.Attributes.Append(imgheight);

            //        //修改img的宽度，宽度数据既存
            //        XmlAttribute imgwidth = imgnodedoc.CreateAttribute("width");
            //        imgwidth.Value = "98";
            //        imgnode.Attributes.Append(imgwidth);

            //        //为img新增src属性，showlittleimage.aspx页面的处理中从Cookie中获取图片
            //        XmlAttribute newAttr = imgnodedoc.CreateAttribute("src");
            //        newAttr.Value = "../xnh/showlittleimage.aspx?ID=" + dt.Rows[i]["ID"].ToString() + "&yyid=" + yyid;
            //        imgnode.Attributes.Append(newAttr);
            //    }
            //    text += xmlimg.SelectSingleNode("/emrtextdoc/body").InnerXml;
            //}
            #endregion

            //当数据库中没有记录的情况下
            if (string.IsNullOrWhiteSpace(text))
            {
                text = "没有找到病历记录.";
            }
            return text;
        }

        //检查医院编号、住院id是否全部为数字，如果是返回true,否则为false
        private bool Check(string yyid, string zyid)
        {
            bool ret = false;

            if (zyid != null && yyid != null)
            {
                if ((Regex.IsMatch(zyid, @"^\d+$")) && (Regex.IsMatch(yyid, @"^\d+$")))
                {
                    ret = true;
                }
            }
            return ret;
        }

        private string GetHisCon()
        {
            string conStr = interfaceZljl.Properties.Settings.Default.CHISConnectionString;
            return conStr;
        }
    }
}
