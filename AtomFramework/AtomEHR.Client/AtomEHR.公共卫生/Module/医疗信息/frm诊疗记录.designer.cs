﻿using AtomEHR.Library.UserControls;
namespace AtomEHR.公共卫生.Module.医疗信息
{
    partial class frm诊疗记录
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm诊疗记录));
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.dte录入时间2 = new DevExpress.XtraEditors.DateEdit();
            this.dte录入时间1 = new DevExpress.XtraEditors.DateEdit();
            this.dte出生时间2 = new DevExpress.XtraEditors.DateEdit();
            this.dte出生时间1 = new DevExpress.XtraEditors.DateEdit();
            this.txt本人电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit地址 = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEdit村 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit镇 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.checkEdit21 = new DevExpress.XtraEditors.CheckEdit();
            this.treeListLookUpEdit机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt档案号 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.cbo档案类别 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo档案状态 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo合格档案 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo性别 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.gc个人健康档案 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv个人健康档案 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col个人档案号码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.col姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col性别 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col出生日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col身份证号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col居住地址 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col本人电话 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col所属机构 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col创建时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gcDetailEditor = new DevExpress.XtraEditors.GroupControl();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.txt编码 = new DevExpress.XtraEditors.TextEdit();
            this.pagerControl1 = new TActionProject.PagerControl();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            this.tpDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt本人电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案类别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo合格档案.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc个人健康档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv个人健康档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetailEditor)).BeginInit();
            this.gcDetailEditor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt编码.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gc个人健康档案);
            this.tpSummary.Controls.Add(this.pagerControl1);
            this.tpSummary.Controls.Add(this.groupControl1);
            this.tpSummary.Size = new System.Drawing.Size(917, 529);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Size = new System.Drawing.Size(923, 535);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Size = new System.Drawing.Size(923, 535);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Controls.Add(this.gcDetailEditor);
            this.tpDetail.Size = new System.Drawing.Size(932, 589);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(923, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(745, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(548, 2);
            // 
            // panelControl5
            // 
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(0, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(923, 561);
            this.panelControl5.TabIndex = 3;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.layoutControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.ShowCaption = false;
            this.groupControl1.Size = new System.Drawing.Size(917, 83);
            this.groupControl1.TabIndex = 2;
            this.groupControl1.Text = "信息查询";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnQuery);
            this.layoutControl1.Controls.Add(this.dte录入时间2);
            this.layoutControl1.Controls.Add(this.dte录入时间1);
            this.layoutControl1.Controls.Add(this.dte出生时间2);
            this.layoutControl1.Controls.Add(this.dte出生时间1);
            this.layoutControl1.Controls.Add(this.txt本人电话);
            this.layoutControl1.Controls.Add(this.textEdit地址);
            this.layoutControl1.Controls.Add(this.comboBoxEdit村);
            this.layoutControl1.Controls.Add(this.comboBoxEdit镇);
            this.layoutControl1.Controls.Add(this.checkEdit21);
            this.layoutControl1.Controls.Add(this.treeListLookUpEdit机构);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt档案号);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.cbo档案类别);
            this.layoutControl1.Controls.Add(this.cbo档案状态);
            this.layoutControl1.Controls.Add(this.cbo合格档案);
            this.layoutControl1.Controls.Add(this.cbo性别);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(913, 79);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnQuery
            // 
            this.btnQuery.Image = ((System.Drawing.Image)(resources.GetObject("btnQuery.Image")));
            this.btnQuery.Location = new System.Drawing.Point(756, 52);
            this.btnQuery.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(68, 22);
            this.btnQuery.StyleController = this.layoutControl1;
            this.btnQuery.TabIndex = 5;
            this.btnQuery.Text = "查询";
            this.btnQuery.Click += new System.EventHandler(this.btn查询_Click);
            // 
            // dte录入时间2
            // 
            this.dte录入时间2.EditValue = null;
            this.dte录入时间2.Location = new System.Drawing.Point(182, 52);
            this.dte录入时间2.Margin = new System.Windows.Forms.Padding(0);
            this.dte录入时间2.Name = "dte录入时间2";
            this.dte录入时间2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte录入时间2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte录入时间2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte录入时间2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte录入时间2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte录入时间2.Size = new System.Drawing.Size(103, 20);
            this.dte录入时间2.StyleController = this.layoutControl1;
            this.dte录入时间2.TabIndex = 5;
            // 
            // dte录入时间1
            // 
            this.dte录入时间1.EditValue = null;
            this.dte录入时间1.Location = new System.Drawing.Point(69, 52);
            this.dte录入时间1.Margin = new System.Windows.Forms.Padding(0);
            this.dte录入时间1.Name = "dte录入时间1";
            this.dte录入时间1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte录入时间1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte录入时间1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte录入时间1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte录入时间1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte录入时间1.Size = new System.Drawing.Size(94, 20);
            this.dte录入时间1.StyleController = this.layoutControl1;
            this.dte录入时间1.TabIndex = 3;
            // 
            // dte出生时间2
            // 
            this.dte出生时间2.EditValue = null;
            this.dte出生时间2.Location = new System.Drawing.Point(182, 28);
            this.dte出生时间2.Margin = new System.Windows.Forms.Padding(0);
            this.dte出生时间2.Name = "dte出生时间2";
            this.dte出生时间2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出生时间2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出生时间2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte出生时间2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte出生时间2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte出生时间2.Size = new System.Drawing.Size(103, 20);
            this.dte出生时间2.StyleController = this.layoutControl1;
            this.dte出生时间2.TabIndex = 2;
            // 
            // dte出生时间1
            // 
            this.dte出生时间1.EditValue = null;
            this.dte出生时间1.Location = new System.Drawing.Point(69, 28);
            this.dte出生时间1.Margin = new System.Windows.Forms.Padding(0);
            this.dte出生时间1.Name = "dte出生时间1";
            this.dte出生时间1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出生时间1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出生时间1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte出生时间1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte出生时间1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte出生时间1.Size = new System.Drawing.Size(94, 20);
            this.dte出生时间1.StyleController = this.layoutControl1;
            this.dte出生时间1.TabIndex = 0;
            // 
            // txt本人电话
            // 
            this.txt本人电话.Location = new System.Drawing.Point(632, 4);
            this.txt本人电话.Name = "txt本人电话";
            this.txt本人电话.Size = new System.Drawing.Size(66, 20);
            this.txt本人电话.StyleController = this.layoutControl1;
            this.txt本人电话.TabIndex = 39;
            // 
            // textEdit地址
            // 
            this.textEdit地址.Location = new System.Drawing.Point(567, 52);
            this.textEdit地址.Name = "textEdit地址";
            this.textEdit地址.Size = new System.Drawing.Size(131, 20);
            this.textEdit地址.StyleController = this.layoutControl1;
            this.textEdit地址.TabIndex = 38;
            // 
            // comboBoxEdit村
            // 
            this.comboBoxEdit村.Location = new System.Drawing.Point(430, 52);
            this.comboBoxEdit村.Name = "comboBoxEdit村";
            this.comboBoxEdit村.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit村.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit村.Size = new System.Drawing.Size(133, 20);
            this.comboBoxEdit村.StyleController = this.layoutControl1;
            this.comboBoxEdit村.TabIndex = 37;
            // 
            // comboBoxEdit镇
            // 
            this.comboBoxEdit镇.Location = new System.Drawing.Point(354, 52);
            this.comboBoxEdit镇.Name = "comboBoxEdit镇";
            this.comboBoxEdit镇.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit镇.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit镇.Size = new System.Drawing.Size(72, 20);
            this.comboBoxEdit镇.StyleController = this.layoutControl1;
            this.comboBoxEdit镇.TabIndex = 36;
            this.comboBoxEdit镇.EditValueChanged += new System.EventHandler(this.comboBoxEdit镇_EditValueChanged);
            // 
            // checkEdit21
            // 
            this.checkEdit21.EditValue = true;
            this.checkEdit21.Location = new System.Drawing.Point(167, 4);
            this.checkEdit21.Name = "checkEdit21";
            this.checkEdit21.Properties.Caption = "含下属机构";
            this.checkEdit21.Size = new System.Drawing.Size(118, 19);
            this.checkEdit21.StyleController = this.layoutControl1;
            this.checkEdit21.TabIndex = 35;
            // 
            // treeListLookUpEdit机构
            // 
            this.treeListLookUpEdit机构.Location = new System.Drawing.Point(69, 4);
            this.treeListLookUpEdit机构.Name = "treeListLookUpEdit机构";
            this.treeListLookUpEdit机构.Properties.AutoExpandAllNodes = false;
            this.treeListLookUpEdit机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEdit机构.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.treeListLookUpEdit机构.Size = new System.Drawing.Size(94, 20);
            this.treeListLookUpEdit机构.StyleController = this.layoutControl1;
            this.treeListLookUpEdit机构.TabIndex = 34;
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(0, 0);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(767, 4);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Size = new System.Drawing.Size(142, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 8;
            // 
            // txt档案号
            // 
            this.txt档案号.Location = new System.Drawing.Point(767, 28);
            this.txt档案号.Name = "txt档案号";
            this.txt档案号.Size = new System.Drawing.Size(142, 20);
            this.txt档案号.StyleController = this.layoutControl1;
            this.txt档案号.TabIndex = 7;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(354, 4);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(72, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 5;
            // 
            // cbo档案类别
            // 
            this.cbo档案类别.Location = new System.Drawing.Point(354, 28);
            this.cbo档案类别.Name = "cbo档案类别";
            this.cbo档案类别.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo档案类别.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo档案类别.Size = new System.Drawing.Size(72, 20);
            this.cbo档案类别.StyleController = this.layoutControl1;
            this.cbo档案类别.TabIndex = 10;
            // 
            // cbo档案状态
            // 
            this.cbo档案状态.Location = new System.Drawing.Point(495, 28);
            this.cbo档案状态.Name = "cbo档案状态";
            this.cbo档案状态.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo档案状态.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo档案状态.Size = new System.Drawing.Size(68, 20);
            this.cbo档案状态.StyleController = this.layoutControl1;
            this.cbo档案状态.TabIndex = 11;
            // 
            // cbo合格档案
            // 
            this.cbo合格档案.Location = new System.Drawing.Point(632, 28);
            this.cbo合格档案.Name = "cbo合格档案";
            this.cbo合格档案.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo合格档案.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo合格档案.Size = new System.Drawing.Size(66, 20);
            this.cbo合格档案.StyleController = this.layoutControl1;
            this.cbo合格档案.TabIndex = 12;
            // 
            // cbo性别
            // 
            this.cbo性别.Location = new System.Drawing.Point(495, 4);
            this.cbo性别.Name = "cbo性别";
            this.cbo性别.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo性别.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo性别.Size = new System.Drawing.Size(68, 20);
            this.cbo性别.StyleController = this.layoutControl1;
            this.cbo性别.TabIndex = 6;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem32,
            this.layoutControlItem1,
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.layoutControlItem35,
            this.layoutControlItem5,
            this.layoutControlItem37,
            this.layoutControlItem38,
            this.layoutControlItem6,
            this.layoutControlItem39,
            this.emptySpaceItem1,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem4,
            this.emptySpaceItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(913, 79);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem7.Control = this.cbo档案类别;
            this.layoutControlItem7.CustomizationFormText = "档案类别：";
            this.layoutControlItem7.Location = new System.Drawing.Point(285, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(141, 24);
            this.layoutControlItem7.Text = "档案类别：";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem8.Control = this.cbo档案状态;
            this.layoutControlItem8.CustomizationFormText = "档案状态：";
            this.layoutControlItem8.Location = new System.Drawing.Point(426, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(137, 24);
            this.layoutControlItem8.Text = "档案状态：";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem9.Control = this.cbo合格档案;
            this.layoutControlItem9.CustomizationFormText = "合格档案：";
            this.layoutControlItem9.Location = new System.Drawing.Point(563, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(135, 24);
            this.layoutControlItem9.Text = "合格档案：";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem2.Control = this.txt姓名;
            this.layoutControlItem2.CustomizationFormText = "姓名：";
            this.layoutControlItem2.Location = new System.Drawing.Point(285, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(141, 24);
            this.layoutControlItem2.Text = "姓名：";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem3.Control = this.cbo性别;
            this.layoutControlItem3.CustomizationFormText = "性别：";
            this.layoutControlItem3.Location = new System.Drawing.Point(426, 0);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(119, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(137, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "性别：";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.treeListLookUpEdit机构;
            this.layoutControlItem32.CustomizationFormText = "所属机构：";
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem32.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem32.MinSize = new System.Drawing.Size(117, 24);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(163, 24);
            this.layoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem32.Text = "所属机构：";
            this.layoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem32.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.checkEdit21;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(163, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(122, 24);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.comboBoxEdit镇;
            this.layoutControlItem33.CustomizationFormText = "居住地址：";
            this.layoutControlItem33.Location = new System.Drawing.Point(285, 48);
            this.layoutControlItem33.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem33.MinSize = new System.Drawing.Size(117, 24);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(141, 27);
            this.layoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem33.Text = "居住地址：";
            this.layoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem33.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem33.TextToControlDistance = 5;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.comboBoxEdit村;
            this.layoutControlItem34.CustomizationFormText = "layoutControlItem34";
            this.layoutControlItem34.Location = new System.Drawing.Point(426, 48);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(137, 27);
            this.layoutControlItem34.Text = "layoutControlItem34";
            this.layoutControlItem34.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem34.TextToControlDistance = 0;
            this.layoutControlItem34.TextVisible = false;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.textEdit地址;
            this.layoutControlItem35.CustomizationFormText = "layoutControlItem35";
            this.layoutControlItem35.Location = new System.Drawing.Point(563, 48);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(135, 27);
            this.layoutControlItem35.Text = "layoutControlItem35";
            this.layoutControlItem35.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem35.TextToControlDistance = 0;
            this.layoutControlItem35.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem5.Control = this.txt身份证号;
            this.layoutControlItem5.CustomizationFormText = "身份证号：";
            this.layoutControlItem5.Location = new System.Drawing.Point(698, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(119, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(211, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "身份证号：";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.dte出生时间1;
            this.layoutControlItem37.CustomizationFormText = "出生日期：";
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(163, 24);
            this.layoutControlItem37.Text = "出生日期：";
            this.layoutControlItem37.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem37.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem37.TextToControlDistance = 5;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.dte出生时间2;
            this.layoutControlItem38.CustomizationFormText = "~";
            this.layoutControlItem38.Location = new System.Drawing.Point(163, 24);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(122, 24);
            this.layoutControlItem38.Text = "~";
            this.layoutControlItem38.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem38.TextSize = new System.Drawing.Size(10, 20);
            this.layoutControlItem38.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.dte录入时间1;
            this.layoutControlItem6.CustomizationFormText = "录入时间：";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(163, 27);
            this.layoutControlItem6.Text = "录入时间：";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.Control = this.dte录入时间2;
            this.layoutControlItem39.CustomizationFormText = "~";
            this.layoutControlItem39.Location = new System.Drawing.Point(163, 48);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(122, 27);
            this.layoutControlItem39.Text = "~";
            this.layoutControlItem39.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem39.TextSize = new System.Drawing.Size(10, 20);
            this.layoutControlItem39.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(698, 48);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(54, 27);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.btnQuery;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(752, 48);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(72, 27);
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.txt本人电话;
            this.layoutControlItem11.CustomizationFormText = "本人电话：";
            this.layoutControlItem11.Location = new System.Drawing.Point(563, 0);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(119, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(135, 24);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "本人电话：";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem4.Control = this.txt档案号;
            this.layoutControlItem4.CustomizationFormText = "档案号：";
            this.layoutControlItem4.Location = new System.Drawing.Point(698, 24);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(119, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(211, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "档案号：";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(824, 48);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(85, 27);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // gc个人健康档案
            // 
            this.gc个人健康档案.AllowBandedGridColumnSort = false;
            this.gc个人健康档案.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc个人健康档案.IsBestFitColumns = true;
            this.gc个人健康档案.Location = new System.Drawing.Point(0, 83);
            this.gc个人健康档案.MainView = this.gv个人健康档案;
            this.gc个人健康档案.Name = "gc个人健康档案";
            this.gc个人健康档案.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemHyperLinkEdit2});
            this.gc个人健康档案.ShowContextMenu = true;
            this.gc个人健康档案.Size = new System.Drawing.Size(917, 381);
            this.gc个人健康档案.StrWhere = "";
            this.gc个人健康档案.TabIndex = 4;
            this.gc个人健康档案.UseCheckBox = true;
            this.gc个人健康档案.View = "";
            this.gc个人健康档案.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv个人健康档案});
            // 
            // gv个人健康档案
            // 
            this.gv个人健康档案.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gv个人健康档案.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gv个人健康档案.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gv个人健康档案.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.DetailTip.Options.UseFont = true;
            this.gv个人健康档案.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.Empty.Options.UseFont = true;
            this.gv个人健康档案.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.EvenRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FilterPanel.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FixedLine.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FocusedCell.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FocusedRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gv个人健康档案.Appearance.FooterPanel.Options.UseFont = true;
            this.gv个人健康档案.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.GroupButton.Options.UseFont = true;
            this.gv个人健康档案.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.GroupFooter.Options.UseFont = true;
            this.gv个人健康档案.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.GroupPanel.Options.UseFont = true;
            this.gv个人健康档案.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.GroupRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.HeaderPanel.Options.UseFont = true;
            this.gv个人健康档案.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.HorzLine.Options.UseFont = true;
            this.gv个人健康档案.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.OddRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.Preview.Options.UseFont = true;
            this.gv个人健康档案.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.Row.Options.UseFont = true;
            this.gv个人健康档案.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.RowSeparator.Options.UseFont = true;
            this.gv个人健康档案.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.SelectedRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.TopNewRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.VertLine.Options.UseFont = true;
            this.gv个人健康档案.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.ViewCaption.Options.UseFont = true;
            this.gv个人健康档案.ColumnPanelRowHeight = 30;
            this.gv个人健康档案.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col个人档案号码,
            this.col姓名,
            this.col性别,
            this.col出生日期,
            this.col身份证号,
            this.col居住地址,
            this.col本人电话,
            this.col所属机构,
            this.col创建时间,
            this.gridColumn1});
            this.gv个人健康档案.GridControl = this.gc个人健康档案;
            this.gv个人健康档案.GroupPanelText = "DragColumn";
            this.gv个人健康档案.Name = "gv个人健康档案";
            this.gv个人健康档案.OptionsView.ColumnAutoWidth = false;
            this.gv个人健康档案.OptionsView.EnableAppearanceEvenRow = true;
            this.gv个人健康档案.OptionsView.EnableAppearanceOddRow = true;
            this.gv个人健康档案.OptionsView.ShowGroupPanel = false;
            // 
            // col个人档案号码
            // 
            this.col个人档案号码.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.col个人档案号码.AppearanceCell.Options.UseBackColor = true;
            this.col个人档案号码.AppearanceCell.Options.UseTextOptions = true;
            this.col个人档案号码.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col个人档案号码.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col个人档案号码.AppearanceHeader.Options.UseFont = true;
            this.col个人档案号码.AppearanceHeader.Options.UseTextOptions = true;
            this.col个人档案号码.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col个人档案号码.Caption = "档案号";
            this.col个人档案号码.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.col个人档案号码.FieldName = "个人档案编号";
            this.col个人档案号码.Name = "col个人档案号码";
            this.col个人档案号码.OptionsColumn.ReadOnly = true;
            this.col个人档案号码.Visible = true;
            this.col个人档案号码.VisibleIndex = 0;
            this.col个人档案号码.Width = 127;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit1_Click);
            // 
            // col姓名
            // 
            this.col姓名.AppearanceCell.Options.UseTextOptions = true;
            this.col姓名.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col姓名.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col姓名.AppearanceHeader.Options.UseFont = true;
            this.col姓名.AppearanceHeader.Options.UseTextOptions = true;
            this.col姓名.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col姓名.Caption = "姓名";
            this.col姓名.FieldName = "姓名";
            this.col姓名.Name = "col姓名";
            this.col姓名.OptionsColumn.ReadOnly = true;
            this.col姓名.Visible = true;
            this.col姓名.VisibleIndex = 1;
            // 
            // col性别
            // 
            this.col性别.AppearanceCell.Options.UseTextOptions = true;
            this.col性别.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col性别.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col性别.AppearanceHeader.Options.UseFont = true;
            this.col性别.AppearanceHeader.Options.UseTextOptions = true;
            this.col性别.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col性别.Caption = "性别";
            this.col性别.FieldName = "性别";
            this.col性别.Name = "col性别";
            this.col性别.OptionsColumn.ReadOnly = true;
            this.col性别.Visible = true;
            this.col性别.VisibleIndex = 2;
            // 
            // col出生日期
            // 
            this.col出生日期.AppearanceCell.Options.UseTextOptions = true;
            this.col出生日期.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col出生日期.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col出生日期.AppearanceHeader.Options.UseFont = true;
            this.col出生日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col出生日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col出生日期.Caption = "出生日期";
            this.col出生日期.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.col出生日期.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.col出生日期.FieldName = "出生日期";
            this.col出生日期.Name = "col出生日期";
            this.col出生日期.OptionsColumn.ReadOnly = true;
            this.col出生日期.Visible = true;
            this.col出生日期.VisibleIndex = 3;
            // 
            // col身份证号
            // 
            this.col身份证号.AppearanceCell.Options.UseTextOptions = true;
            this.col身份证号.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col身份证号.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col身份证号.AppearanceHeader.Options.UseFont = true;
            this.col身份证号.AppearanceHeader.Options.UseTextOptions = true;
            this.col身份证号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col身份证号.Caption = "身份证号";
            this.col身份证号.FieldName = "身份证号";
            this.col身份证号.Name = "col身份证号";
            this.col身份证号.OptionsColumn.ReadOnly = true;
            this.col身份证号.Visible = true;
            this.col身份证号.VisibleIndex = 4;
            // 
            // col居住地址
            // 
            this.col居住地址.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col居住地址.AppearanceHeader.Options.UseFont = true;
            this.col居住地址.AppearanceHeader.Options.UseTextOptions = true;
            this.col居住地址.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col居住地址.Caption = "居住地址";
            this.col居住地址.FieldName = "居住地址";
            this.col居住地址.Name = "col居住地址";
            this.col居住地址.OptionsColumn.ReadOnly = true;
            this.col居住地址.Visible = true;
            this.col居住地址.VisibleIndex = 5;
            this.col居住地址.Width = 163;
            // 
            // col本人电话
            // 
            this.col本人电话.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col本人电话.AppearanceHeader.Options.UseFont = true;
            this.col本人电话.AppearanceHeader.Options.UseTextOptions = true;
            this.col本人电话.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col本人电话.Caption = "本人电话";
            this.col本人电话.FieldName = "本人电话";
            this.col本人电话.Name = "col本人电话";
            this.col本人电话.OptionsColumn.ReadOnly = true;
            this.col本人电话.Visible = true;
            this.col本人电话.VisibleIndex = 6;
            // 
            // col所属机构
            // 
            this.col所属机构.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col所属机构.AppearanceHeader.Options.UseFont = true;
            this.col所属机构.AppearanceHeader.Options.UseTextOptions = true;
            this.col所属机构.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col所属机构.Caption = "当前所属机构";
            this.col所属机构.FieldName = "所属机构";
            this.col所属机构.Name = "col所属机构";
            this.col所属机构.OptionsColumn.ReadOnly = true;
            this.col所属机构.Visible = true;
            this.col所属机构.VisibleIndex = 7;
            this.col所属机构.Width = 93;
            // 
            // col创建时间
            // 
            this.col创建时间.AppearanceCell.Options.UseTextOptions = true;
            this.col创建时间.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col创建时间.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col创建时间.AppearanceHeader.Options.UseFont = true;
            this.col创建时间.AppearanceHeader.Options.UseTextOptions = true;
            this.col创建时间.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col创建时间.Caption = "录入时间";
            this.col创建时间.DisplayFormat.FormatString = "d";
            this.col创建时间.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.col创建时间.FieldName = "创建时间";
            this.col创建时间.Name = "col创建时间";
            this.col创建时间.OptionsColumn.ReadOnly = true;
            this.col创建时间.Visible = true;
            this.col创建时间.VisibleIndex = 8;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.Caption = "查看诊疗记录";
            this.gridColumn1.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            //this.gridColumn1.DisplayFormat.FormatString = "查看诊疗记录...";
            this.gridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn1.OptionsFilter.AllowFilter = false;
            this.gridColumn1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 9;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            //this.repositoryItemHyperLinkEdit2.Caption = "查看诊疗记录...";
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.NullText = "查看诊疗记录...";
            this.repositoryItemHyperLinkEdit2.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit2_Click);
            // 
            // gcDetailEditor
            // 
            this.gcDetailEditor.Controls.Add(this.textEdit12);
            this.gcDetailEditor.Controls.Add(this.textEdit11);
            this.gcDetailEditor.Controls.Add(this.txt编码);
            this.gcDetailEditor.Location = new System.Drawing.Point(30, 60);
            this.gcDetailEditor.Name = "gcDetailEditor";
            this.gcDetailEditor.Size = new System.Drawing.Size(587, 274);
            this.gcDetailEditor.TabIndex = 0;
            this.gcDetailEditor.Text = "groupControl2";
            // 
            // textEdit12
            // 
            this.textEdit12.Location = new System.Drawing.Point(109, 155);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Size = new System.Drawing.Size(254, 20);
            this.textEdit12.TabIndex = 2;
            // 
            // textEdit11
            // 
            this.textEdit11.Location = new System.Drawing.Point(109, 114);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Size = new System.Drawing.Size(254, 20);
            this.textEdit11.TabIndex = 1;
            // 
            // txt编码
            // 
            this.txt编码.Location = new System.Drawing.Point(109, 74);
            this.txt编码.Name = "txt编码";
            this.txt编码.Size = new System.Drawing.Size(254, 20);
            this.txt编码.TabIndex = 0;
            // 
            // pagerControl1
            // 
            this.pagerControl1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pagerControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pagerControl1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pagerControl1.JumpText = "跳转";
            this.pagerControl1.Location = new System.Drawing.Point(0, 464);
            this.pagerControl1.MaximumSize = new System.Drawing.Size(0, 1211);
            this.pagerControl1.Name = "pagerControl1";
            this.pagerControl1.PageIndex = 1;
            this.pagerControl1.PageSize = 10;
            this.pagerControl1.paramters = null;
            this.pagerControl1.RecordCount = 0;
            this.pagerControl1.Size = new System.Drawing.Size(917, 65);
            this.pagerControl1.SqlQuery = null;
            this.pagerControl1.TabIndex = 5;
            // 
            // frm诊疗记录
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 561);
            this.Controls.Add(this.panelControl5);
            this.Name = "frm诊疗记录";
            this.Text = "诊疗记录";
            this.Load += new System.EventHandler(this.frm诊疗记录_Load);
            this.Controls.SetChildIndex(this.panelControl5, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.Controls.SetChildIndex(this.gcNavigator, 0);
            this.Controls.SetChildIndex(this.pnlSummary, 0);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            this.tpDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt本人电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案类别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo合格档案.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc个人健康档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv个人健康档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetailEditor)).EndInit();
            this.gcDetailEditor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt编码.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DataGridControl gc个人健康档案;
        private DevExpress.XtraGrid.Views.Grid.GridView gv个人健康档案;
        private DevExpress.XtraGrid.Columns.GridColumn col个人档案号码;
        private DevExpress.XtraGrid.Columns.GridColumn col姓名;
        private DevExpress.XtraGrid.Columns.GridColumn col性别;
        private DevExpress.XtraGrid.Columns.GridColumn col出生日期;
        private DevExpress.XtraGrid.Columns.GridColumn col身份证号;
        private DevExpress.XtraGrid.Columns.GridColumn col居住地址;
        private DevExpress.XtraGrid.Columns.GridColumn col本人电话;
        private DevExpress.XtraGrid.Columns.GridColumn col所属机构;
        private DevExpress.XtraGrid.Columns.GridColumn col创建时间;
        private DevExpress.XtraEditors.GroupControl gcDetailEditor;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.TextEdit txt编码;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit textEdit地址;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit村;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit镇;
        private DevExpress.XtraEditors.CheckEdit checkEdit21;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEdit机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraEditors.DateEdit dte录入时间1;
        private DevExpress.XtraEditors.DateEdit dte录入时间2;
        private DevExpress.XtraEditors.DateEdit dte出生时间1;
        private DevExpress.XtraEditors.DateEdit dte出生时间2;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt档案号;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.ComboBoxEdit cbo档案类别;
        private DevExpress.XtraEditors.ComboBoxEdit cbo档案状态;
        private DevExpress.XtraEditors.ComboBoxEdit cbo合格档案;
        private DevExpress.XtraEditors.ComboBoxEdit cbo性别;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private TActionProject.PagerControl pagerControl1;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraEditors.TextEdit txt本人电话;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;

    }
}