﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Common;
using DevExpress.XtraEditors;
using AtomEHR.公共卫生.util;
using AtomEHR.公共卫生.Module.个人健康.家庭成员健康信息;
using System.Text.RegularExpressions;
using DevExpress.XtraLayout;
using DevExpress.XtraGrid.Views.Grid;

namespace AtomEHR.公共卫生.Module
{
    public partial class frm个人基本信息表 : frmBase
    {
        #region Fields
        DataSet _ds健康档案;
        bll健康档案 _Bll = new bll健康档案();
        UpdateType _updateType = UpdateType.Add;
        string _serverDateTime;
        #endregion

        #region Constructor

        public frm个人基本信息表()
        {
            InitializeComponent();
            _serverDateTime = _Bll.ServiceDateTime;
            _Bll.GetBusinessByKey("-", true);
            _Bll.NewBusiness();
            _ds健康档案 = _Bll.CurrentBusiness;
            DoBindingSummaryEditor(_ds健康档案);
        }

        public frm个人基本信息表(DataSet docNo)
        {
            //InitializeComponent();
            //_ds健康档案 = docNo;
            ////_bll个人健康特征.GetBusinessByKey("-", true);
            //DoBindingSummaryEditor(_ds健康档案);
        }
        #endregion

        #region Handler Events
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (f保存检查())
                {
                    bool tnb = false;  //糖尿病
                    bool nzz = false; //脑卒中
                    bool gxb = false; //冠心病
                    bool gxy = false; //高血压
                    bool jsb = false; //精神病
                    bool mz = false;//慢阻肺
                    bool zl = false;//肿瘤

                    //赋值家庭档案中的街道字段
                    string _jiedao = this.cbo居住地址_村委会.EditValue == null ? "" : Convert.ToString(this.cbo居住地址_村委会.EditValue);
                    #region tb_家庭档案

                    _ds健康档案.Tables[tb_家庭档案.__TableName].Rows[0][tb_家庭档案.街道] = _jiedao;
                    _ds健康档案.Tables[tb_家庭档案.__TableName].Rows[0][tb_家庭档案.所属机构] = Loginer.CurrentUser.所属机构;
                    _ds健康档案.Tables[tb_家庭档案.__TableName].Rows[0][tb_家庭档案.创建机构] = Loginer.CurrentUser.所属机构;
                    _ds健康档案.Tables[tb_家庭档案.__TableName].Rows[0][tb_家庭档案.创建人] = Loginer.CurrentUser.用户编码;
                    _ds健康档案.Tables[tb_家庭档案.__TableName].Rows[0][tb_家庭档案.创建时间] = _serverDateTime;
                    _ds健康档案.Tables[tb_家庭档案.__TableName].Rows[0][tb_家庭档案.修改人] = Loginer.CurrentUser.用户编码;
                    _ds健康档案.Tables[tb_家庭档案.__TableName].Rows[0][tb_家庭档案.修改时间] = _serverDateTime;

                    #endregion

                    #region tb_健康档案 表保存

                    //赋值
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.与户主关系] = ControlsHelper.GetComboxKey(cbo与户主关系);
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.档案状态] = Get档案状态();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(),false);
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.拼音简码] = util.DESEncrypt.GetChineseSpell(this.txt姓名.Text.Trim());
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.性别] = ControlsHelper.GetComboxKey(this.cbo性别);
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.证件类型] = ControlsHelper.GetComboxKey(this.cbo证件类型);//.SelectedIndex;
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.身份证号] = this.txt证件编号.Text.Trim();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.出生日期] = string.IsNullOrEmpty(this.dte出生日期.Text) ? "" : this.dte出生日期.DateTime.ToString("yyyy-MM-dd");
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.本人电话] = this.txt本人电话.Text.Trim();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.工作单位] = this.txt工作单位.Text.Trim();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.联系人电话] = this.txt联系人电话.Text.Trim();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.联系人姓名] = this.txt联系人姓名.Text.Trim();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.常住类型] = ControlsHelper.GetComboxKey(this.cbo常住类型);//.SelectedIndex;
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.劳动强度] = ControlsHelper.GetComboxKey(this.cbo劳动程度);//.SelectedIndex;
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.省] = "37";
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.市] = this.cbo居住地址_市.EditValue;
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.区] = this.cbo居住地址_县.EditValue;
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.街道] = this.cbo居住地址_街道.EditValue;
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.居委会] = this.cbo居住地址_村委会.EditValue;
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.民族] = ControlsHelper.GetComboxKey(this.cbo民族);//.SelectedIndex;
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.职业] = ControlsHelper.GetComboxKey(this.cbo职业);//.SelectedIndex;
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.档案位置] = this.txt档案位置.Text.Trim();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.文化程度] = ControlsHelper.GetComboxKey(this.cbo文化程度);//.SelectedIndex;
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.婚姻状况] = ControlsHelper.GetComboxKey(this.cbo婚姻状况);//.SelectedIndex;
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.医疗费支付类型] = Get医疗费用支付方式();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.医疗费用支付类型其他] = this.txt医疗费用支付方式_其他.Text.Trim();
                    //_ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.医疗保险号] = this.txt医疗保险号.Text.Trim();
                    //_ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.新农合号] = this.txt新农合号.Text.Trim();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.居住地址] = this.txt居住地址_详细地址.Text.Trim();
                    //_ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.所属片区] = this.cbo所属片区.Text.Trim();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.所属片区] = this.textEdit所属片区.Text.Trim();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.档案类别] = ControlsHelper.GetComboxKey(this.cbo档案类别);//.SelectedIndex;

                    #region 新版本添加
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.医疗保险号] = this.textEdit职工医疗保险卡号.Text.Trim();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.新农合号] = this.textEdit居民医疗保险卡号.Text.Trim();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.贫困救助卡号] = this.textEdit贫困救助卡号.Text.Trim();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.本人或家属签字] = this.textEdit本人或家属签字.Text.Trim();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.签字时间] = this.dateEdit签字时间.Text.Trim();
                    #endregion

                    //孕产情况  如果孕产情况显示，则表示其为孕妇，需要进行保存孕产情况
                    string 孕产情况 = string.Empty;
                    bool isYunFu = false;
                    if (this.layout孕产情况.Visibility == DevExpress.XtraLayout.Utils.LayoutVisibility.Always)
                    {
                        if (this.chk孕产1.Checked)
                            孕产情况 = this.chk孕产1.Text;
                        else if (this.chk孕产2.Checked)
                        {
                            孕产情况 = this.chk孕产2.Text;
                            isYunFu = true;
                        }
                        else if (this.chk孕产3.Checked)
                        {
                            孕产情况 = this.chk孕产3.Text;
                            isYunFu = true;
                        }
                        else if (this.chk孕产4.Checked)
                        {
                            孕产情况 = this.chk孕产4.Text;
                            isYunFu = true;
                        }
                    }
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.怀孕情况] = 孕产情况;//.SelectedIndex;
                    if (!string.IsNullOrEmpty(孕产情况) && 孕产情况 != "未孕")
                    {
                        if (this.cbo孕次.Text.Trim() == "" || this.cbo产次.Text.Trim() == "")
                        {
                            Msg.Warning("请选择孕次及产次！");
                            this.cbo孕次.Focus();
                            return;
                        }
                    }
                    if (!this.chk孕产1.Checked)
                    {
                        _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.孕次] = this.cbo孕次.Text.Trim();//.SelectedIndex;
                        _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.产次] = this.cbo产次.Text.Trim();//.SelectedIndex;
                    }
                    int i = Get缺项();//缺项
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.缺项] = i;//.SelectedIndex;
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.完整度] = Get完整度(i);//.SelectedIndex;

                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.厨房排气设施] = Get厨房排气设施();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.燃料类型] = Get燃料类型();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.饮水] = Get饮水();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.厕所] = Get厕所();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.禽畜栏] = Get禽畜栏();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.调查时间] = string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建时间] = _serverDateTime;
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建人] = Loginer.CurrentUser.用户编码;
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.修改人] = Loginer.CurrentUser.用户编码;
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.修改时间] = _serverDateTime;
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建机构] = Loginer.CurrentUser.所属机构;
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.所属机构] = Loginer.CurrentUser.所属机构;
                    #endregion

                    #region tb_健康档案_健康状态 表保存
                    string ywjws = "";//有无既往史
                    if (this.radio疾病.EditValue == "1" || this.radio手术.EditValue == "1" || this.radio输血.EditValue == "1" || this.radio外伤.EditValue == "1")
                    {
                        ywjws = "1";
                    }
                    _ds健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.暴露史] = ControlsHelper.GetComboxKey(this.cbo暴露史);//.SelectedIndex;
                    _ds健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.暴露史毒物] = this.txt暴露史_毒物.Text.Trim();
                    _ds健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.暴露史化学品] = this.txt暴露史_化学品.Text.Trim();
                    _ds健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.暴露史射线] = this.txt暴露史_射线.Text.Trim();
                    _ds健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.遗传病史有无] = ControlsHelper.GetComboxKey(this.cbo遗传病史);//.SelectedIndex;
                    _ds健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.遗传病史] = this.txt遗传病史_疾病名称.Text.Trim();
                    _ds健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.有无残疾] = ControlsHelper.GetComboxKey(cbo残疾情况);
                    _ds健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.有无疾病] = this.radio疾病.EditValue;
                    _ds健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.有无既往史] = ywjws;
                    _ds健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.残疾情况] = Get残疾情况();
                    _ds健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.残疾其他] = this.txt残疾情况_其他残疾.Text.Trim();
                    _ds健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.过敏史有无] = ControlsHelper.GetComboxKey(this.cbo过敏史);//.SelectedIndex;
                    _ds健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.药物过敏史] = Get过敏史();
                    _ds健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.过敏史其他] = this.txt过敏史_其他.Text.Trim();
                    _ds健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.血型] = ControlsHelper.GetComboxKey(this.cbo血型);//.SelectedIndex;
                    _ds健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.RH] = ControlsHelper.GetComboxKey(this.cboRH阴性);//.SelectedIndex;
                    _ds健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.调查时间] = this.dte调查时间.Text;
                    _ds健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.创建时间] = _serverDateTime;
                    _ds健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.创建人] = Loginer.CurrentUser.用户编码;
                    _ds健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.修改人] = Loginer.CurrentUser.用户编码;
                    #endregion

                    int Age = GetAge(this.dte出生日期.Text.Trim());

                    #region 6岁以下儿童需要保存  儿童基本信息表
                    if (Age > 6)//6岁以上儿童不需要创建儿童信息表
                    {
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童基本信息] = "";
                    }
                    else
                    {
                        DataRow row儿童基本信息 = _ds健康档案.Tables[tb_儿童基本信息.__TableName].Rows.Add();
                        row儿童基本信息[tb_儿童基本信息.个人档案编号] = "";
                        row儿童基本信息[tb_儿童基本信息.卡号] = "";
                        row儿童基本信息[tb_儿童基本信息.家庭档案编号] = "";
                        row儿童基本信息[tb_儿童基本信息.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(),false);
                        row儿童基本信息[tb_儿童基本信息.拼音简码] = util.DESEncrypt.GetChineseSpell(this.txt姓名.Text.Trim());
                        row儿童基本信息[tb_儿童基本信息.身份证号] = this.txt证件编号.Text.Trim();
                        row儿童基本信息[tb_儿童基本信息.工作单位] = this.txt工作单位.Text.Trim();
                        row儿童基本信息[tb_儿童基本信息.联系电话] = this.txt联系人电话.Text.Trim();
                        row儿童基本信息[tb_儿童基本信息.省] = "37";
                        row儿童基本信息[tb_儿童基本信息.市] = this.cbo居住地址_市.EditValue;
                        row儿童基本信息[tb_儿童基本信息.区] = this.cbo居住地址_县.EditValue;
                        row儿童基本信息[tb_儿童基本信息.街道] = this.cbo居住地址_街道.EditValue;
                        row儿童基本信息[tb_儿童基本信息.居委会] = this.cbo居住地址_村委会.EditValue;
                        row儿童基本信息[tb_儿童基本信息.居住地址] = this.txt居住地址_详细地址.Text.Trim();
                        row儿童基本信息[tb_儿童基本信息.医疗保险号] = this.txt医疗保险号.Text.Trim();
                        row儿童基本信息[tb_儿童基本信息.所属片区] = this.textEdit所属片区.Text.Trim();
                        row儿童基本信息[tb_儿童基本信息.居住状况] = ControlsHelper.GetComboxKey(this.cbo常住类型);//.SelectedIndex;
                        row儿童基本信息[tb_儿童基本信息.性别] = ControlsHelper.GetComboxKey(this.cbo性别);
                        row儿童基本信息[tb_儿童基本信息.出生日期] = string.IsNullOrEmpty(this.dte出生日期.Text) ? "" : this.dte出生日期.DateTime.ToString("yyyy-MM-dd");
                        row儿童基本信息[tb_儿童基本信息.民族] = ControlsHelper.GetComboxKey(this.cbo民族);
                        row儿童基本信息[tb_儿童基本信息.文化程度] = ControlsHelper.GetComboxKey(this.cbo文化程度);
                        row儿童基本信息[tb_儿童基本信息.职业] = ControlsHelper.GetComboxKey(this.cbo职业);
                        row儿童基本信息[tb_儿童基本信息.婚姻状况] = ControlsHelper.GetComboxKey(this.cbo婚姻状况);
                        row儿童基本信息[tb_儿童基本信息.医疗费支付类型] = GetFlowLayoutResult(flow医疗费用支付方式);
                        row儿童基本信息[tb_儿童基本信息.新农合号] = this.txt新农合号.Text.Trim();
                        row儿童基本信息[tb_儿童基本信息.所属机构] = Loginer.CurrentUser.所属机构;
                        row儿童基本信息[tb_儿童基本信息.HAPPENTIME] = string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
                        row儿童基本信息[tb_儿童基本信息.创建日期] = this.dte录入时间.Text;
                        row儿童基本信息[tb_儿童基本信息.修改日期] = this.dte最近更新时间.Text;
                        row儿童基本信息[tb_儿童基本信息.创建人] = Loginer.CurrentUser.用户编码;
                        row儿童基本信息[tb_儿童基本信息.修改人] = Loginer.CurrentUser.用户编码;
                        row儿童基本信息[tb_儿童基本信息.创建机构] = Loginer.CurrentUser.所属机构;
                        row儿童基本信息[tb_儿童基本信息.档案状态] = this.radio档案状态.EditValue;
                        row儿童基本信息[tb_儿童基本信息.与户主关系] = ControlsHelper.GetComboxKey(this.cbo与户主关系);
                        row儿童基本信息[tb_儿童基本信息.缺项] = "14";
                        row儿童基本信息[tb_儿童基本信息.完整度] = "7";
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童基本信息] = string.Format("{0},{1}", "14", "7");
                    }
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.新生儿随访记录] = "";
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表满月] = "";
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表3月] = "";
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表6月] = "";
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表8月] = "";
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表12月] = "";
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表18月] = "";
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表2岁] = "";
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表2岁半] = "";
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表3岁] = "";
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表4岁] = "";
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表5岁] = "";
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表6岁] = "";
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童入托信息表] = "";
                    #endregion

                    #region 老年人信息保存
                    if (Age >= 65)
                    {
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.老年人随访] = "未建";
                        if (_ds健康档案.Tables[tb_老年人基本信息.__TableName].Rows.Count == 0)
                        {

                            DataRow row老年人 = _ds健康档案.Tables[tb_老年人基本信息.__TableName].Rows.Add();
                            row老年人[tb_老年人基本信息.个人档案编号] = "";
                            //row老年人[tb_老年人基本信息.卡号] = "";
                            row老年人[tb_老年人基本信息.家庭档案编号] = "";
                            row老年人[tb_老年人基本信息.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(),false);
                            row老年人[tb_老年人基本信息.拼音简码] = util.DESEncrypt.GetChineseSpell(this.txt姓名.Text.Trim());
                            row老年人[tb_老年人基本信息.身份证号] = this.txt证件编号.Text.Trim();
                            row老年人[tb_老年人基本信息.工作单位] = this.txt工作单位.Text.Trim();
                            row老年人[tb_老年人基本信息.联系电话] = this.txt联系人电话.Text.Trim();
                            row老年人[tb_老年人基本信息.省] = "37";
                            row老年人[tb_老年人基本信息.市] = this.cbo居住地址_市.EditValue;
                            row老年人[tb_老年人基本信息.区] = this.cbo居住地址_县.EditValue;
                            row老年人[tb_老年人基本信息.街道] = this.cbo居住地址_街道.EditValue;
                            row老年人[tb_老年人基本信息.居委会] = this.cbo居住地址_村委会.EditValue;
                            row老年人[tb_老年人基本信息.居住地址] = this.txt居住地址_详细地址.Text.Trim();
                            row老年人[tb_老年人基本信息.新农合号] = this.txt医疗保险号.Text.Trim();
                            row老年人[tb_老年人基本信息.所属片区] = this.textEdit所属片区.Text.Trim();
                            row老年人[tb_老年人基本信息.常住类型] = ControlsHelper.GetComboxKey(this.cbo常住类型);//.SelectedIndex;
                            row老年人[tb_老年人基本信息.性别] = ControlsHelper.GetComboxKey(this.cbo性别);
                            row老年人[tb_老年人基本信息.出生日期] = string.IsNullOrEmpty(this.dte出生日期.Text) ? "" : this.dte出生日期.DateTime.ToString("yyyy-MM-dd");
                            row老年人[tb_老年人基本信息.民族] = ControlsHelper.GetComboxKey(this.cbo民族);
                            row老年人[tb_老年人基本信息.文化程度] = ControlsHelper.GetComboxKey(this.cbo文化程度);
                            row老年人[tb_老年人基本信息.职业] = ControlsHelper.GetComboxKey(this.cbo职业);
                            row老年人[tb_老年人基本信息.婚姻状况] = ControlsHelper.GetComboxKey(this.cbo婚姻状况);
                            row老年人[tb_老年人基本信息.医疗费支付类型] = GetFlowLayoutResult(flow医疗费用支付方式);
                            row老年人[tb_老年人基本信息.新农合号] = this.txt新农合号.Text.Trim();
                            row老年人[tb_老年人基本信息.所属机构] = Loginer.CurrentUser.所属机构;
                            row老年人[tb_老年人基本信息.发生时间] = string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
                            row老年人[tb_老年人基本信息.创建时间] = this.dte录入时间.Text;
                            row老年人[tb_老年人基本信息.修改时间] = this.dte最近更新时间.Text;
                            row老年人[tb_老年人基本信息.创建人] = Loginer.CurrentUser.用户编码;
                            row老年人[tb_老年人基本信息.修改人] = Loginer.CurrentUser.用户编码;
                            row老年人[tb_老年人基本信息.创建机构] = Loginer.CurrentUser.所属机构;
                            row老年人[tb_老年人基本信息.档案状态] = this.radio档案状态.EditValue;
                            row老年人[tb_老年人基本信息.与户主关系] = ControlsHelper.GetComboxKey(this.cbo与户主关系);
                        }
                    }
                    #endregion

                    #region 妇女
                    if (Age >= 15 && ControlsHelper.GetComboxKey(cbo性别) == "2")
                    {
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.妇女保健检查] = "未建";
                        if (_ds健康档案.Tables[tb_妇女基本信息.__TableName].Rows.Count == 0)
                        {

                            DataRow row妇女 = _ds健康档案.Tables[tb_妇女基本信息.__TableName].Rows.Add();
                            row妇女[tb_妇女基本信息.个人档案编号] = "";
                            //row老年人[tb_老年人基本信息.卡号] = "";
                            row妇女[tb_妇女基本信息.家庭档案编号] = "";
                            row妇女[tb_妇女基本信息.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(),false);
                            row妇女[tb_妇女基本信息.身份证号] = this.txt证件编号.Text.Trim();
                            row妇女[tb_妇女基本信息.工作单位] = this.txt工作单位.Text.Trim();
                            row妇女[tb_妇女基本信息.联系电话] = this.txt联系人电话.Text.Trim();
                            row妇女[tb_妇女基本信息.省] = "37";
                            row妇女[tb_妇女基本信息.市] = this.cbo居住地址_市.EditValue;
                            row妇女[tb_妇女基本信息.区] = this.cbo居住地址_县.EditValue;
                            row妇女[tb_妇女基本信息.街道] = this.cbo居住地址_街道.EditValue;
                            row妇女[tb_妇女基本信息.居委会] = this.cbo居住地址_村委会.EditValue;
                            row妇女[tb_妇女基本信息.居住地址] = this.txt居住地址_详细地址.Text.Trim();
                            row妇女[tb_妇女基本信息.新农合号] = this.txt医疗保险号.Text.Trim();
                            row妇女[tb_妇女基本信息.所属片区] = this.textEdit所属片区.Text.Trim();
                            row妇女[tb_妇女基本信息.常住类型] = ControlsHelper.GetComboxKey(this.cbo常住类型);//.SelectedIndex;
                            row妇女[tb_妇女基本信息.性别] = ControlsHelper.GetComboxKey(this.cbo性别);
                            row妇女[tb_妇女基本信息.出生日期] = string.IsNullOrEmpty(this.dte出生日期.Text) ? "" : this.dte出生日期.DateTime.ToString("yyyy-MM-dd");
                            row妇女[tb_妇女基本信息.民族] = ControlsHelper.GetComboxKey(this.cbo民族);
                            row妇女[tb_妇女基本信息.文化程度] = ControlsHelper.GetComboxKey(this.cbo文化程度);
                            row妇女[tb_妇女基本信息.职业] = ControlsHelper.GetComboxKey(this.cbo职业);
                            row妇女[tb_妇女基本信息.婚姻状况] = ControlsHelper.GetComboxKey(this.cbo婚姻状况);
                            row妇女[tb_妇女基本信息.医疗费用支付方式] = GetFlowLayoutResult(flow医疗费用支付方式);
                            row妇女[tb_妇女基本信息.医疗保险号] = this.txt医疗保险号.Text.Trim();
                            row妇女[tb_妇女基本信息.新农合号] = this.txt新农合号.Text.Trim();
                            row妇女[tb_妇女基本信息.所属机构] = Loginer.CurrentUser.所属机构;
                            row妇女[tb_妇女基本信息.建档时间] = string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
                            row妇女[tb_妇女基本信息.创建时间] = this.dte录入时间.Text;
                            row妇女[tb_妇女基本信息.修改时间] = this.dte最近更新时间.Text;
                            row妇女[tb_妇女基本信息.创建人] = Loginer.CurrentUser.用户编码;
                            row妇女[tb_妇女基本信息.修改人] = Loginer.CurrentUser.用户编码;
                            row妇女[tb_妇女基本信息.创建机构] = Loginer.CurrentUser.所属机构;
                            row妇女[tb_妇女基本信息.档案状态] = this.radio档案状态.EditValue;
                            row妇女[tb_妇女基本信息.与户主关系] = ControlsHelper.GetComboxKey(this.cbo与户主关系);
                        }
                    }
                    #endregion

                    #region 孕妇基本信息表保存
                    if (isYunFu)//是孕妇
                    {
                        if (_ds健康档案.Tables[tb_孕妇基本信息.__TableName].Rows.Count == 0)//不存在妇女信息
                        {
                            _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.孕产妇基本信息表] = "未建";
                            DataRow row孕妇 = _ds健康档案.Tables[tb_孕妇基本信息.__TableName].Rows.Add();
                            row孕妇[tb_孕妇基本信息.个人档案编号] = "";
                            //row老年人[tb_老年人基本信息.卡号] = "";
                            row孕妇[tb_孕妇基本信息.家庭档案编号] = "";
                            row孕妇[tb_孕妇基本信息.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(),false);
                            row孕妇[tb_孕妇基本信息.身份证号] = this.txt证件编号.Text.Trim();
                            row孕妇[tb_孕妇基本信息.工作单位] = this.txt工作单位.Text.Trim();
                            row孕妇[tb_孕妇基本信息.联系电话] = this.txt联系人电话.Text.Trim();
                            row孕妇[tb_孕妇基本信息.省] = "37";
                            row孕妇[tb_孕妇基本信息.市] = this.cbo居住地址_市.EditValue;
                            row孕妇[tb_孕妇基本信息.区] = this.cbo居住地址_县.EditValue;
                            row孕妇[tb_孕妇基本信息.街道] = this.cbo居住地址_街道.EditValue;
                            row孕妇[tb_孕妇基本信息.居委会] = this.cbo居住地址_村委会.EditValue;
                            row孕妇[tb_孕妇基本信息.居住地址] = this.txt居住地址_详细地址.Text.Trim();
                            row孕妇[tb_孕妇基本信息.新农合号] = this.txt医疗保险号.Text.Trim();
                            row孕妇[tb_孕妇基本信息.所属片区] = this.textEdit所属片区.Text.Trim();
                            row孕妇[tb_孕妇基本信息.性别] = ControlsHelper.GetComboxKey(this.cbo性别);
                            row孕妇[tb_孕妇基本信息.出生日期] = string.IsNullOrEmpty(this.dte出生日期.Text) ? "" : this.dte出生日期.DateTime.ToString("yyyy-MM-dd");
                            row孕妇[tb_孕妇基本信息.民族] = ControlsHelper.GetComboxKey(this.cbo民族);
                            row孕妇[tb_孕妇基本信息.文化程度] = ControlsHelper.GetComboxKey(this.cbo文化程度);
                            row孕妇[tb_孕妇基本信息.职业] = ControlsHelper.GetComboxKey(this.cbo职业);
                            row孕妇[tb_孕妇基本信息.婚姻状况] = ControlsHelper.GetComboxKey(this.cbo婚姻状况);
                            row孕妇[tb_孕妇基本信息.医疗费用支付方式] = GetFlowLayoutResult(flow医疗费用支付方式);
                            row孕妇[tb_孕妇基本信息.医疗保险号] = this.txt医疗保险号.Text.Trim();
                            row孕妇[tb_孕妇基本信息.新农合号] = this.txt新农合号.Text.Trim();
                            row孕妇[tb_孕妇基本信息.所属机构] = Loginer.CurrentUser.所属机构;
                            row孕妇[tb_孕妇基本信息.建档时间] = string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
                            row孕妇[tb_孕妇基本信息.创建时间] = this.dte录入时间.Text;
                            row孕妇[tb_孕妇基本信息.修改时间] = this.dte最近更新时间.Text;
                            row孕妇[tb_孕妇基本信息.创建人] = Loginer.CurrentUser.用户编码;
                            row孕妇[tb_孕妇基本信息.修改人] = Loginer.CurrentUser.用户编码;
                            row孕妇[tb_孕妇基本信息.创建机构] = Loginer.CurrentUser.所属机构;
                            row孕妇[tb_孕妇基本信息.档案状态] = this.radio档案状态.EditValue;
                            row孕妇[tb_孕妇基本信息.与户主关系] = ControlsHelper.GetComboxKey(this.cbo与户主关系);
                            row孕妇[tb_孕妇基本信息.缺项] = "12";
                            row孕妇[tb_孕妇基本信息.完整度] = "0";
                            row孕妇[tb_孕妇基本信息.下次随访时间] = "1900-01-01";
                            row孕妇[tb_孕妇基本信息.孕次] = this.cbo孕次.EditValue.ToString();
                            row孕妇[tb_孕妇基本信息.产次] = this.cbo产次.EditValue.ToString();
                        }
                        else
                        {
                            DataRow row孕妇 = _ds健康档案.Tables[tb_孕妇基本信息.__TableName].Rows[0];
                            row孕妇[tb_孕妇基本信息.个人档案编号] = "";
                            //row老年人[tb_老年人基本信息.卡号] = "";
                            row孕妇[tb_孕妇基本信息.家庭档案编号] = "";
                            row孕妇[tb_孕妇基本信息.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(),false);
                            row孕妇[tb_孕妇基本信息.身份证号] = this.txt证件编号.Text.Trim();
                            row孕妇[tb_孕妇基本信息.工作单位] = this.txt工作单位.Text.Trim();
                            row孕妇[tb_孕妇基本信息.联系电话] = this.txt联系人电话.Text.Trim();
                            row孕妇[tb_孕妇基本信息.省] = "37";
                            row孕妇[tb_孕妇基本信息.市] = this.cbo居住地址_市.EditValue;
                            row孕妇[tb_孕妇基本信息.区] = this.cbo居住地址_县.EditValue;
                            row孕妇[tb_孕妇基本信息.街道] = this.cbo居住地址_街道.EditValue;
                            row孕妇[tb_孕妇基本信息.居委会] = this.cbo居住地址_村委会.EditValue;
                            row孕妇[tb_孕妇基本信息.居住地址] = this.txt居住地址_详细地址.Text.Trim();
                            row孕妇[tb_孕妇基本信息.新农合号] = this.txt医疗保险号.Text.Trim();
                            row孕妇[tb_孕妇基本信息.所属片区] = this.textEdit所属片区.Text.Trim();
                            row孕妇[tb_孕妇基本信息.性别] = ControlsHelper.GetComboxKey(this.cbo性别);
                            row孕妇[tb_孕妇基本信息.出生日期] = string.IsNullOrEmpty(this.dte出生日期.Text) ? "" : this.dte出生日期.DateTime.ToString("yyyy-MM-dd");
                            row孕妇[tb_孕妇基本信息.民族] = ControlsHelper.GetComboxKey(this.cbo民族);
                            row孕妇[tb_孕妇基本信息.文化程度] = ControlsHelper.GetComboxKey(this.cbo文化程度);
                            row孕妇[tb_孕妇基本信息.职业] = ControlsHelper.GetComboxKey(this.cbo职业);
                            row孕妇[tb_孕妇基本信息.婚姻状况] = ControlsHelper.GetComboxKey(this.cbo婚姻状况);
                            row孕妇[tb_孕妇基本信息.医疗费用支付方式] = GetFlowLayoutResult(flow医疗费用支付方式);
                            row孕妇[tb_孕妇基本信息.医疗保险号] = this.txt医疗保险号.Text.Trim();
                            row孕妇[tb_孕妇基本信息.新农合号] = this.txt新农合号.Text.Trim();
                            row孕妇[tb_孕妇基本信息.所属机构] = Loginer.CurrentUser.所属机构;
                            row孕妇[tb_孕妇基本信息.修改时间] = this.dte最近更新时间.Text;
                            row孕妇[tb_孕妇基本信息.修改人] = Loginer.CurrentUser.用户编码;
                            row孕妇[tb_孕妇基本信息.档案状态] = this.radio档案状态.EditValue;
                            row孕妇[tb_孕妇基本信息.与户主关系] = ControlsHelper.GetComboxKey(this.cbo与户主关系);
                            row孕妇[tb_孕妇基本信息.孕次] = this.cbo孕次.EditValue.ToString();
                            row孕妇[tb_孕妇基本信息.产次] = this.cbo产次.EditValue.ToString();

                        }
                    }
                    #endregion

                    #region 疾病  高血压 糖尿病 脑卒中 冠心病  精神病 等
                    if (ywjws == "1")//存在疾病
                    {
                        DataRow[] rows = _ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Select(" 疾病类型 = '疾病'");
                        for (int j = 0; j < rows.Length; j++)
                        {
                            string jibingmingcheng = rows[j]["疾病名称"].ToString();
                            string[] a = jibingmingcheng.Split(',');
                            for (int k = 0; k < a.Length; k++)
                            {
                                #region 高血压

                                if (a[k].Trim() == "2")//高血压
                                {
                                    gxy = true;
                                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.高血压管理卡] = string.Format("{0},{1}", "21", "0");
                                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.高血压随访表] = "未建";
                                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否高血压] = "1";

                                }
                                #endregion
                                #region 糖尿病

                                if (a[k].Trim() == "3")//糖尿病
                                {
                                    tnb = true;
                                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.糖尿病管理卡] = string.Format("{0},{1}", "26", "0");
                                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.糖尿病随访表] = "未建";
                                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否糖尿病] = "1";

                                }
                                #endregion
                                #region 精神病

                                if (a[k].Trim() == "8")//精神病
                                {
                                    jsb = true;
                                }
                                #endregion
                                #region 脑卒中

                                if ((a[k].Trim() == "7"))//脑卒中
                                {
                                    nzz = true;
                                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.脑卒中管理卡] = string.Format("{0},{1}", "26", "0");
                                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.脑卒中随访表] = "未建";
                                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否脑卒中] = "1";
                                }
                                #endregion
                                #region 冠心病

                                if ((a[k].Trim() == "4"))//冠心病
                                {
                                    gxb = true;
                                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.冠心病管理卡] = string.Format("{0},{1}", "17", "0");
                                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.冠心病随访表] = "未建";
                                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否冠心病] = "1";
                                }
                                #endregion
                                #region 肿瘤

                                if ((a[k].Trim() == "6"))//肿瘤
                                {
                                    zl = true;
                                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否肿瘤] = "1";
                                }
                                #endregion
                                #region 慢阻肺

                                if ((a[k].Trim() == "5"))//慢阻肺
                                {
                                    mz = true;
                                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否慢阻肺] = "1";
                                }
                                #endregion
                            }
                        }
                    }
                    #region tb_MXB慢病基础信息表

                    if ((gxy) || (tnb) || (nzz) || (gxb))
                    {
                        if (_ds健康档案.Tables[tb_MXB慢病基础信息表.__TableName].Rows.Count == 0)
                        {
                            DataRow row = _ds健康档案.Tables[tb_MXB慢病基础信息表.__TableName].Rows.Add();
                            //row[tb_MXB慢病基础信息表.户主关系] = ControlsHelper.GetComboxKey(cbo与户主关系);
                            row[tb_MXB慢病基础信息表.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(),false);
                            row[tb_MXB慢病基础信息表.拼音简码] = util.DESEncrypt.GetChineseSpell(this.txt姓名.Text.Trim());
                            row[tb_MXB慢病基础信息表.性别] = ControlsHelper.GetComboxKey(this.cbo性别);
                            row[tb_MXB慢病基础信息表.身份证号] = this.txt证件编号.Text.Trim();
                            row[tb_MXB慢病基础信息表.出生日期] = string.IsNullOrEmpty(this.dte出生日期.Text) ? "" : this.dte出生日期.DateTime.ToString("yyyy-MM-dd");
                            row[tb_MXB慢病基础信息表.民族] = ControlsHelper.GetComboxKey(this.cbo民族);//.SelectedIndex;
                            row[tb_MXB慢病基础信息表.文化程度] = ControlsHelper.GetComboxKey(this.cbo文化程度);//.SelectedIndex;
                            row[tb_MXB慢病基础信息表.常住类型] = ControlsHelper.GetComboxKey(this.cbo常住类型);//.SelectedIndex;
                            row[tb_MXB慢病基础信息表.工作单位] = this.txt工作单位.Text.Trim();
                            row[tb_MXB慢病基础信息表.婚姻状况] = ControlsHelper.GetComboxKey(this.cbo婚姻状况);//.SelectedIndex;
                            row[tb_MXB慢病基础信息表.职业] = ControlsHelper.GetComboxKey(this.cbo职业);//.SelectedIndex;
                            row[tb_MXB慢病基础信息表.联系人电话] = this.txt联系人电话.Text.Trim();
                            row[tb_MXB慢病基础信息表.省] = "37";
                            row[tb_MXB慢病基础信息表.市] = this.cbo居住地址_市.EditValue;
                            row[tb_MXB慢病基础信息表.区] = this.cbo居住地址_县.EditValue;
                            row[tb_MXB慢病基础信息表.街道] = this.cbo居住地址_街道.EditValue;
                            row[tb_MXB慢病基础信息表.居委会] = this.cbo居住地址_村委会.EditValue;
                            row[tb_MXB慢病基础信息表.所属片区] = this.textEdit所属片区.Text.Trim();
                            row[tb_MXB慢病基础信息表.居住地址] = this.txt居住地址_详细地址.Text.Trim();
                            row[tb_MXB慢病基础信息表.医疗费用支付方式] = Get医疗费用支付方式();
                            //row[tb_MXB慢病基础信息表.医疗费用支付类型其他] = this.txt医疗费用支付方式_其他.Text.Trim();
                            row[tb_MXB慢病基础信息表.医疗保险号] = this.txt医疗保险号.Text.Trim();
                            row[tb_MXB慢病基础信息表.新农合号] = this.txt新农合号.Text.Trim();
                            row[tb_MXB慢病基础信息表.建档机构] = Loginer.CurrentUser.所属机构;
                            row[tb_MXB慢病基础信息表.建档时间] = this.dte录入时间.Text.Trim();
                            row[tb_MXB慢病基础信息表.建档人] = Loginer.CurrentUser.用户编码;
                            row[tb_MXB慢病基础信息表.创建机构] = Loginer.CurrentUser.所属机构;
                            row[tb_MXB慢病基础信息表.创建时间] = this.dte录入时间.Text.Trim();
                            row[tb_MXB慢病基础信息表.创建人] = Loginer.CurrentUser.用户编码;
                            row[tb_MXB慢病基础信息表.更新时间] = this.dte录入时间.Text.Trim();
                            row[tb_MXB慢病基础信息表.更新人] = Loginer.CurrentUser.用户编码;
                            row[tb_MXB慢病基础信息表.HAPPENTIME] = this.dte录入时间.Text.Trim();
                            row[tb_MXB慢病基础信息表.所属机构] = Loginer.CurrentUser.所属机构;
                            row[tb_MXB慢病基础信息表.高血压随访时间] = "1900-01-01";
                            row[tb_MXB慢病基础信息表.糖尿病随访时间] = "1900-01-01";
                            row[tb_MXB慢病基础信息表.脑卒中随访时间] = "1900-01-01";
                            row[tb_MXB慢病基础信息表.冠心病随访时间] = "1900-01-01";
                            row[tb_MXB慢病基础信息表.家庭档案编号] = "";
                            if (gxy)
                            {
                                row[tb_MXB慢病基础信息表.高血压随访时间] = "";
                            }
                            if (tnb)
                            {
                                row[tb_MXB慢病基础信息表.糖尿病随访时间] = "";
                            }
                            if (nzz)
                            {
                                row[tb_MXB慢病基础信息表.脑卒中随访时间] = "";
                            }
                            if (gxb)
                            {
                                row[tb_MXB慢病基础信息表.冠心病随访时间] = "";
                            }
                        }
                    }
                    #endregion
                    #region 保存高血压管理卡表

                    if (gxy) //保存高血压管理卡表
                    {
                        if (_ds健康档案.Tables[tb_MXB高血压管理卡.__TableName].Rows.Count == 0)
                        {
                            DataRow row高血压 = _ds健康档案.Tables[tb_MXB高血压管理卡.__TableName].Rows.Add();
                            row高血压[tb_MXB高血压管理卡.管理卡编号] = "";
                            row高血压[tb_MXB高血压管理卡.创建机构] = Loginer.CurrentUser.所属机构;
                            row高血压[tb_MXB高血压管理卡.发生时间] = string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
                            row高血压[tb_MXB高血压管理卡.创建时间] = this.dte录入时间.Text.Trim();
                            row高血压[tb_MXB高血压管理卡.创建人] = Loginer.CurrentUser.用户编码;
                            row高血压[tb_MXB高血压管理卡.所属机构] = Loginer.CurrentUser.所属机构;
                            row高血压[tb_MXB高血压管理卡.修改人] = Loginer.CurrentUser.用户编码;
                            row高血压[tb_MXB高血压管理卡.修改时间] = this.dte录入时间.Text.Trim();
                            row高血压[tb_MXB高血压管理卡.缺项] = "21";
                            row高血压[tb_MXB高血压管理卡.完整度] = "0";
                        }
                    }
                    #endregion
                    #region 保存糖尿病管理卡表

                    if (tnb) //保存糖尿病管理卡表
                    {
                        if (_ds健康档案.Tables[tb_MXB糖尿病管理卡.__TableName].Rows.Count == 0)
                        {
                            DataRow row糖尿病 = _ds健康档案.Tables[tb_MXB糖尿病管理卡.__TableName].Rows.Add();
                            row糖尿病[tb_MXB糖尿病管理卡.管理卡编号] = "";
                            row糖尿病[tb_MXB糖尿病管理卡.创建机构] = Loginer.CurrentUser.所属机构;
                            row糖尿病[tb_MXB糖尿病管理卡.发生时间] = string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
                            row糖尿病[tb_MXB糖尿病管理卡.创建时间] = this.dte录入时间.Text.Trim();
                            row糖尿病[tb_MXB糖尿病管理卡.创建人] = Loginer.CurrentUser.用户编码;
                            row糖尿病[tb_MXB糖尿病管理卡.所属机构] = Loginer.CurrentUser.所属机构;
                            row糖尿病[tb_MXB糖尿病管理卡.修改人] = Loginer.CurrentUser.用户编码;
                            row糖尿病[tb_MXB糖尿病管理卡.修改时间] = this.dte录入时间.Text.Trim();
                            row糖尿病[tb_MXB糖尿病管理卡.缺项] = "26";
                            row糖尿病[tb_MXB糖尿病管理卡.完整度] = "0";

                        }
                    }
                    #endregion
                    #region 保存脑卒中管理卡表

                    if (nzz) //保存脑卒中管理卡表
                    {
                        if (_ds健康档案.Tables[tb_MXB脑卒中管理卡.__TableName].Rows.Count == 0)
                        {
                            DataRow row脑卒中 = _ds健康档案.Tables[tb_MXB脑卒中管理卡.__TableName].Rows.Add();
                            row脑卒中[tb_MXB脑卒中管理卡.管理卡编号] = "";
                            row脑卒中[tb_MXB脑卒中管理卡.个人档案编号] = "";
                            row脑卒中[tb_MXB脑卒中管理卡.创建机构] = Loginer.CurrentUser.所属机构;
                            row脑卒中[tb_MXB脑卒中管理卡.发生时间] = string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
                            row脑卒中[tb_MXB脑卒中管理卡.创建时间] = this.dte录入时间.Text.Trim();
                            row脑卒中[tb_MXB脑卒中管理卡.创建人] = Loginer.CurrentUser.用户编码;
                            row脑卒中[tb_MXB脑卒中管理卡.所属机构] = Loginer.CurrentUser.所属机构;
                            row脑卒中[tb_MXB脑卒中管理卡.修改人] = Loginer.CurrentUser.用户编码;
                            row脑卒中[tb_MXB脑卒中管理卡.修改时间] = this.dte录入时间.Text.Trim();
                            row脑卒中[tb_MXB脑卒中管理卡.缺项] = "17";
                            row脑卒中[tb_MXB脑卒中管理卡.完整度] = "0";

                        }
                    }
                    #endregion
                    #region 保存冠心病管理卡表
                    if (gxb) //保存冠心病管理卡表
                    {
                        if (_ds健康档案.Tables[tb_MXB冠心病管理卡.__TableName].Rows.Count == 0)
                        {

                            DataRow row冠心病 = _ds健康档案.Tables[tb_MXB冠心病管理卡.__TableName].Rows.Add();

                            row冠心病[tb_MXB冠心病管理卡.管理卡编号] = "";
                            row冠心病[tb_MXB冠心病管理卡.个人档案编号] = "";
                            row冠心病[tb_MXB冠心病管理卡.创建机构] = Loginer.CurrentUser.所属机构;
                            row冠心病[tb_MXB冠心病管理卡.发生时间] = string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
                            row冠心病[tb_MXB冠心病管理卡.创建时间] = this.dte录入时间.Text.Trim();
                            row冠心病[tb_MXB冠心病管理卡.创建人] = Loginer.CurrentUser.用户编码;
                            row冠心病[tb_MXB冠心病管理卡.所属机构] = Loginer.CurrentUser.所属机构;
                            row冠心病[tb_MXB冠心病管理卡.修改人] = Loginer.CurrentUser.用户编码;
                            row冠心病[tb_MXB冠心病管理卡.修改时间] = this.dte录入时间.Text.Trim();
                            row冠心病[tb_MXB冠心病管理卡.缺项] = "17";
                            row冠心病[tb_MXB冠心病管理卡.完整度] = "0";

                        }
                    }
                    #endregion
                    #region tb_精神疾病信息补充表
                    if (jsb)//  tb_精神疾病信息补充表
                    {
                        if (_ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows.Count == 0)
                        {
                            DataRow row精神病 = _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows.Add();
                            //TcjrJsjbxxbcb jsbinfo = new TcjrJsjbxxbcb();
                            //jsbinfo.setdGrdabh(tdaJkdaRkxzl.getdGrdabh());
                            //jsbinfo.setdGrdabh17(tdaJkdaRkxzl.getdGrdabh17());
                            //jsbinfo.setdGrdabhshow(tdaJkdaRkxzl.getdGrdabhshow());
                            row精神病[tb_精神疾病信息补充表.个人档案编号] = "";
                            row精神病[tb_精神疾病信息补充表.家庭档案编号] = "";
                            row精神病[tb_精神疾病信息补充表.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(),false);
                            row精神病[tb_精神疾病信息补充表.与患者关系] = ControlsHelper.GetComboxKey(this.cbo与户主关系);
                            row精神病[tb_精神疾病信息补充表.性别] = ControlsHelper.GetComboxKey(this.cbo性别);
                            row精神病[tb_精神疾病信息补充表.身份证号] = this.txt证件编号.Text.Trim();
                            row精神病[tb_精神疾病信息补充表.出生日期] = this.dte出生日期.DateTime.ToString("yyyy-MM-dd");
                            row精神病[tb_精神疾病信息补充表.民族] = ControlsHelper.GetComboxKey(this.cbo民族);
                            row精神病[tb_精神疾病信息补充表.文化程度] = ControlsHelper.GetComboxKey(this.cbo文化程度);
                            row精神病[tb_精神疾病信息补充表.常住类型] = ControlsHelper.GetComboxKey(this.cbo常住类型);
                            row精神病[tb_精神疾病信息补充表.工作地址] = this.txt工作单位.Text.Trim();
                            row精神病[tb_精神疾病信息补充表.婚姻状况] = ControlsHelper.GetComboxKey(this.cbo婚姻状况);
                            row精神病[tb_精神疾病信息补充表.职业] = ControlsHelper.GetComboxKey(this.cbo职业);
                            row精神病[tb_精神疾病信息补充表.联系电话] = this.txt本人电话.Text.Trim();
                            row精神病[tb_精神疾病信息补充表.省] = "37";
                            row精神病[tb_精神疾病信息补充表.市] = this.cbo居住地址_市.EditValue;
                            row精神病[tb_精神疾病信息补充表.区] = this.cbo居住地址_县.EditValue;
                            row精神病[tb_精神疾病信息补充表.街道] = this.cbo居住地址_街道.EditValue;
                            row精神病[tb_精神疾病信息补充表.居委会] = this.cbo居住地址_村委会.EditValue;
                            row精神病[tb_精神疾病信息补充表.所属片区] = this.textEdit所属片区.Text.Trim();
                            row精神病[tb_精神疾病信息补充表.居住地址] = this.txt居住地址_详细地址.Text.Trim();
                            row精神病[tb_精神疾病信息补充表.医疗支付类型] = Get医疗费用支付方式();
                            row精神病[tb_精神疾病信息补充表.新农合号] = this.txt新农合号.Text.Trim();
                            row精神病[tb_精神疾病信息补充表.创建机构] = Loginer.CurrentUser.所属机构;
                            row精神病[tb_精神疾病信息补充表.创建时间] = this.dte录入时间.Text.Trim();
                            row精神病[tb_精神疾病信息补充表.创建人] = Loginer.CurrentUser.用户编码;
                            row精神病[tb_精神疾病信息补充表.修改时间] = this.dte录入时间.Text.Trim();
                            row精神病[tb_精神疾病信息补充表.修改人] = Loginer.CurrentUser.用户编码;
                            row精神病[tb_精神疾病信息补充表.检查日期] = this.dte录入时间.Text.Trim();
                            row精神病[tb_精神疾病信息补充表.所属机构] = Loginer.CurrentUser.所属机构;
                            row精神病[tb_精神疾病信息补充表.档案状态] = this.radio档案状态.EditValue.ToString();
                            row精神病[tb_精神疾病信息补充表.完整度] = "11";
                            row精神病[tb_精神疾病信息补充表.缺项] = "16";
                            //jsbinfo.setdJtdabh(tdaJkdaRkxzl.getdJtdabh());

                            _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.精神疾病信息补充表] = string.Format("{0},{1}", "16", "11");
                            _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.精神疾病随访表] = "未建";
                        }
                    }

                    #endregion
                    #region 残疾人基本信息表
                    if (this.cbo残疾情况.Text == "有")
                    {
                        if (_ds健康档案.Tables[tb_残疾人_基本信息.__TableName].Rows.Count == 0)
                        {
                            DataRow row残疾人 = _ds健康档案.Tables[tb_残疾人_基本信息.__TableName].Rows.Add();
                            //TcjrJsjbxxbcb jsbinfo = new TcjrJsjbxxbcb();
                            //jsbinfo.setdGrdabh(tdaJkdaRkxzl.getdGrdabh());
                            //jsbinfo.setdGrdabh17(tdaJkdaRkxzl.getdGrdabh17());
                            //jsbinfo.setdGrdabhshow(tdaJkdaRkxzl.getdGrdabhshow());
                            row残疾人[tb_残疾人_基本信息.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(),false);
                            row残疾人[tb_残疾人_基本信息.与户主关系] = ControlsHelper.GetComboxKey(this.cbo与户主关系);
                            row残疾人[tb_残疾人_基本信息.性别] = ControlsHelper.GetComboxKey(this.cbo性别);
                            row残疾人[tb_残疾人_基本信息.身份证号] = this.txt证件编号.Text.Trim();
                            row残疾人[tb_残疾人_基本信息.出生日期] = this.dte出生日期.DateTime.ToString("yyyy-MM-dd");
                            row残疾人[tb_残疾人_基本信息.民族] = ControlsHelper.GetComboxKey(this.cbo民族);
                            row残疾人[tb_残疾人_基本信息.文化程度] = ControlsHelper.GetComboxKey(this.cbo文化程度);
                            row残疾人[tb_残疾人_基本信息.常住类型] = ControlsHelper.GetComboxKey(this.cbo常住类型);
                            row残疾人[tb_残疾人_基本信息.工作地址] = this.txt工作单位.Text.Trim();
                            row残疾人[tb_残疾人_基本信息.婚姻状况] = ControlsHelper.GetComboxKey(this.cbo婚姻状况);
                            row残疾人[tb_残疾人_基本信息.职业] = ControlsHelper.GetComboxKey(this.cbo职业);
                            row残疾人[tb_残疾人_基本信息.联系电话] = this.txt本人电话.Text.Trim();
                            row残疾人[tb_残疾人_基本信息.省] = "37";
                            row残疾人[tb_残疾人_基本信息.市] = this.cbo居住地址_市.EditValue;
                            row残疾人[tb_残疾人_基本信息.区] = this.cbo居住地址_县.EditValue;
                            row残疾人[tb_残疾人_基本信息.街道] = this.cbo居住地址_街道.EditValue;
                            row残疾人[tb_残疾人_基本信息.居委会] = this.cbo居住地址_村委会.EditValue;
                            row残疾人[tb_残疾人_基本信息.所属片区] = this.textEdit所属片区.Text.Trim();
                            row残疾人[tb_残疾人_基本信息.居住地址] = this.txt居住地址_详细地址.Text.Trim();
                            row残疾人[tb_残疾人_基本信息.医疗费用支付类型] = Get医疗费用支付方式();
                            row残疾人[tb_残疾人_基本信息.医疗保险号] = this.txt医疗保险号.Text.Trim();
                            row残疾人[tb_残疾人_基本信息.新农合号] = this.txt新农合号.Text.Trim();
                            row残疾人[tb_残疾人_基本信息.创建机构] = Loginer.CurrentUser.所属机构;
                            row残疾人[tb_残疾人_基本信息.创建时间] = this.dte录入时间.Text.Trim();
                            row残疾人[tb_残疾人_基本信息.创建人] = Loginer.CurrentUser.用户编码;
                            row残疾人[tb_残疾人_基本信息.修改时间] = this.dte录入时间.Text.Trim();
                            row残疾人[tb_残疾人_基本信息.修改人] = Loginer.CurrentUser.用户编码;
                            row残疾人[tb_残疾人_基本信息.发生时间] = this.dte调查时间.Text.Trim();
                            row残疾人[tb_残疾人_基本信息.所属机构] = Loginer.CurrentUser.所属机构;
                            row残疾人[tb_残疾人_基本信息.档案状态] = this.radio档案状态.EditValue.ToString();
                            if (this.chk残疾情况_听力残.Checked)
                            {
                                _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.听力言语残疾随访表] = "未建";
                                row残疾人[tb_残疾人_基本信息.听力残下次随访时间] = "";
                            }
                            if (this.chk残疾情况_言语残.Checked)
                            {
                                _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.听力言语残疾随访表] = "未建";
                                row残疾人[tb_残疾人_基本信息.听力残下次随访时间] = "";
                            }
                            if (this.chk残疾情况_肢体残.Checked)
                            {
                                _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.肢体残疾随访表] = "未建";
                                row残疾人[tb_残疾人_基本信息.肢体残下次随访时间] = "";
                            }
                            if (this.chk残疾情况_智力残.Checked)
                            {
                                _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.智力残疾随访表] = "未建";
                                row残疾人[tb_残疾人_基本信息.智力残下次随访时间] = "";
                            }
                            if (this.chk残疾情况_视力残.Checked)
                            {
                                _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.视力残疾随访表] = "未建";
                                row残疾人[tb_残疾人_基本信息.视力残下次随访时间] = "";
                            }
                            if (this.chk残疾情况_视力残.Checked)
                            {
                                _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.视力残疾随访表] = "未建";
                                row残疾人[tb_残疾人_基本信息.视力残下次随访时间] = "";
                            }
                            //row残疾人[tb_CJR_残疾人基本信息.完整度] = "11";
                            //row残疾人[tb_CJR_残疾人基本信息.缺项] = "16";
                            //jsbinfo.setdJtdabh(tdaJkdaRkxzl.getdJtdabh());

                            _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.精神疾病信息补充表] = string.Format("{0},{1}", "16", "11");
                            _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.精神疾病随访表] = "未建";
                        }
                    }
                    #endregion
                    #endregion

                    #region tb_健康档案_个人健康特征

                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.个人基本信息表] = string.Format("{0},{1}", i, Get完整度(i));
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.家庭档案] = string.Format("{0},{1}", "6", "0");
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.与户主关系] = ControlsHelper.GetComboxKey(cbo与户主关系);
                    #endregion

                    if (_ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows.Count > 0)
                    {
                        for (int k = 0; k < _ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows.Count; k++)
                        {
                            if (_ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows[k][tb_健康档案_既往病史.日期] != null &&
                                !string.IsNullOrEmpty(_ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows[k][tb_健康档案_既往病史.日期].ToString()))
                            {
                                DateTime date;
                                if (DateTime.TryParse(_ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows[k][tb_健康档案_既往病史.日期].ToString(), out date))
                                {
                                    if (_ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows[k][tb_健康档案_既往病史.疾病类型].ToString() == "疾病")
                                    {
                                        _ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows[k][tb_健康档案_既往病史.日期] = date.ToString("yyyy-MM");
                                    }
                                    else
                                    {
                                        _ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows[k][tb_健康档案_既往病史.日期] = date.ToString("yyyy-MM-dd");
                                    }
                                }
                            }

                        }
                    }

                    #region  新版本添加
                    //家庭情况保存
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.户主姓名] = this.textEdit户主姓名.Text.Trim();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.户主身份证号] = this.textEdit户主身份证号.Text.Trim();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.家庭人口数] = this.textEdit家庭人口数.Text.Trim();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.家庭结构] = this.textEdit家庭结构.Text.Trim();
                    if (Age >= 65)
                    {
                        _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.居住情况] = Get居住情况();
                    }
                    else
                    {
                        _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.居住情况] = "";
                    }
                    #endregion

                    _Bll.WriteLog(); //注意:只有修改状态下保存修改日志
                    DataSet dsTemplate = _Bll.CreateSaveData(_ds健康档案, _updateType); //创建用于保存的临时数据
                    SaveResult result = _Bll.Save(dsTemplate);//调用业务逻辑保存数据方法
                    if (result.Success)
                    {
                        this.DialogResult = System.Windows.Forms.DialogResult.Yes;
                    }
                }
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }

        private object Get居住情况()
        {
            return GetFlowLayoutResult(flow居住情况);
        }
        private void chk医疗费用支付方式_其他_CheckedChanged(object sender, EventArgs e)
        {
            this.txt医疗费用支付方式_其他.Enabled = this.chk医疗费用支付方式_其他.Checked;
        }
        private void chk过敏史_其他_CheckedChanged(object sender, EventArgs e)
        {
            this.txt过敏史_其他.Enabled = this.chk过敏史_其他.Checked;
        }
        private void chk残疾情况_其他残疾_CheckedChanged(object sender, EventArgs e)
        {
            this.txt残疾情况_其他残疾.Enabled = this.chk残疾情况_其他残疾.Checked;
        }
        private void cbo暴露史_SelectedIndexChanged(object sender, EventArgs e)
        {
            string text = this.cbo暴露史.Text.Trim();
            if (text == "有")
            {
                this.txt暴露史_毒物.Enabled = true;
                this.txt暴露史_化学品.Enabled = true;
                this.txt暴露史_射线.Enabled = true;
            }
            else
            {
                this.txt暴露史_毒物.Enabled = false;
                this.txt暴露史_化学品.Enabled = false;
                this.txt暴露史_射线.Enabled = false;
            }
        }
        private void cbo家族史_SelectedIndexChanged(object sender, EventArgs e)
        {
            string text = this.cbo家族史.Text.Trim();
            if (text == "有")
            {
                this.chk家族史_恶性肿瘤.Enabled = true;
                this.chk家族史_肝炎.Enabled = true;
                this.chk家族史_高血压.Enabled = true;
                this.chk家族史_冠心病.Enabled = true;
                this.chk家族史_结核病.Enabled = true;
                this.chk家族史_慢性阻塞性肺疾病.Enabled = true;
                this.chk家族史_脑卒中.Enabled = true;
                this.chk家族史_其他.Enabled = true;
                this.chk家族史_糖尿病.Enabled = true;
                this.chk家族史_先天畸形.Enabled = true;
                this.chk家族史_重性精神疾病.Enabled = true;
                this.cbo家族史关系.Enabled = true;

                this.btn家族史_添加.Enabled = true;
            }
            else
            {
                this.chk家族史_恶性肿瘤.Enabled = false;
                this.chk家族史_肝炎.Enabled = false;
                this.chk家族史_高血压.Enabled = false;
                this.chk家族史_冠心病.Enabled = false;
                this.chk家族史_结核病.Enabled = false;
                this.chk家族史_慢性阻塞性肺疾病.Enabled = false;
                this.chk家族史_脑卒中.Enabled = false;
                this.chk家族史_其他.Enabled = false;
                this.chk家族史_糖尿病.Enabled = false;
                this.chk家族史_先天畸形.Enabled = false;
                this.chk家族史_重性精神疾病.Enabled = false;
                this.txt家族史_其他.Enabled = false;
                this.cbo家族史关系.Enabled = false;
                this.btn家族史_添加.Enabled = false;

            }
        }
        private void cbo遗传病史_SelectedIndexChanged(object sender, EventArgs e)
        {
            string text = this.cbo遗传病史.Text.Trim();
            if (text == "有")
            {
                this.txt遗传病史_疾病名称.Enabled = true;
            }
            else
            {
                this.txt遗传病史_疾病名称.Enabled = false;
            }
        }
        private void cbo残疾情况_SelectedIndexChanged(object sender, EventArgs e)
        {
            string text = this.cbo残疾情况.Text.Trim();
            if (text == "有")
            {
                this.chk残疾情况_精神残.Enabled = true;
                this.chk残疾情况_其他残疾.Enabled = true;
                this.chk残疾情况_视力残.Enabled = true;
                this.chk残疾情况_听力残.Enabled = true;
                this.chk残疾情况_言语残.Enabled = true;
                this.chk残疾情况_肢体残.Enabled = true;
                this.chk残疾情况_智力残.Enabled = true;
            }
            else
            {
                this.chk残疾情况_精神残.Enabled = false;
                this.chk残疾情况_其他残疾.Enabled = false;
                this.chk残疾情况_视力残.Enabled = false;
                this.chk残疾情况_听力残.Enabled = false;
                this.chk残疾情况_言语残.Enabled = false;
                this.chk残疾情况_肢体残.Enabled = false;
                this.chk残疾情况_智力残.Enabled = false;
                this.txt残疾情况_其他残疾.Enabled = false;
            }
        }
        private void chk厨房排风设施_无_CheckedChanged(object sender, EventArgs e)
        {
            this.chk厨房排风设施_换气扇.Enabled = this.chk厨房排风设施_油烟机.Enabled = this.chk厨房排风设施_烟囱.Enabled = !chk厨房排风设施_无.Checked;
            if (chk厨房排风设施_无.Checked)
                this.chk厨房排风设施_换气扇.Checked = this.chk厨房排风设施_油烟机.Checked = this.chk厨房排风设施_烟囱.Checked = false;

        }
        private void chk疾病_恶性肿瘤_CheckedChanged(object sender, EventArgs e)
        {
            this.txt疾病_恶性肿瘤.Enabled = this.chk疾病_恶性肿瘤.Checked;
        }
        private void chk家族史_其他_CheckedChanged(object sender, EventArgs e)
        {
            this.txt家族史_其他.Enabled = this.chk家族史_其他.Checked;
        }
        private void chk疾病_职业病_CheckedChanged(object sender, EventArgs e)
        {
            this.txt疾病_职业病.Enabled = this.chk疾病_职业病.Checked;
        }
        private void chk疾病_其他_CheckedChanged(object sender, EventArgs e)
        {
            this.txt疾病_其他.Enabled = this.chk疾病_其他.Checked;

        }
        private void cbo过敏史_SelectedIndexChanged(object sender, EventArgs e)
        {
            string text = this.cbo过敏史.Text.Trim();
            if (text == "有")
            {
                this.chk过敏史_磺胺.Enabled = true;
                this.chk过敏史_链霉素.Enabled = true;
                this.chk过敏史_青霉素.Enabled = true;
                this.chk过敏史_其他.Enabled = true;
                this.chk过敏史_不详.Enabled = true;
                this.txt过敏史_其他.Enabled = true;
            }
            else
            {
                this.chk过敏史_磺胺.Enabled = false;
                this.chk过敏史_链霉素.Enabled = false;
                this.chk过敏史_青霉素.Enabled = false;
                this.chk过敏史_其他.Enabled = false;
                this.chk过敏史_不详.Enabled = false;
                this.txt过敏史_其他.Enabled = false;
            }
        }
        private void frm个人基本信息表_Load(object sender, EventArgs e)
        {
            Disable滚动();

            cbo居住地址_市.EditValueChanged += cbo居住地址_市_EditValueChanged;
            cbo居住地址_县.EditValueChanged += cbo居住地址_县_EditValueChanged;
            cbo居住地址_街道.EditValueChanged += cbo居住地址_街道_EditValueChanged;


            DataBinder.BindingLookupEditDataSource(cbo居住地址_村委会, DataDictCache.Cache.t地区信息, "地区名称", "地区编码");

            DataBinder.BindingLookupEditDataSource(cbo居住地址_街道, DataDictCache.Cache.t地区信息, "地区名称", "地区编码");

            DataBinder.BindingLookupEditDataSource(cbo居住地址_县, DataDictCache.Cache.t地区信息, "地区名称", "地区编码");

            //绑定相关缓存数据
            DataView dv市 = new DataView(DataDictCache.Cache.t地区信息);
            dv市.RowFilter = "上级编码=37";
            DataBinder.BindingLookupEditDataSource(cbo居住地址_市, dv市.ToTable(), "地区名称", "地区编码");

            cbo居住地址_市.EditValue = "3713";

            cbo居住地址_县.EditValue = Loginer.CurrentUser.单位代码;
            this.cbo与户主关系.SelectedIndex = 0;

            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, cbo性别);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.tRH, cboRH阴性);
            AtomEHR.公共卫生.util.ControlsHelper.SetComboxData("3", cboRH阴性);//默认不详
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t常住类型, cbo常住类型);
            AtomEHR.公共卫生.util.ControlsHelper.SetComboxData("1", cbo常住类型);//默认本地户籍常住
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案类别, cbo档案类别);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t婚姻状况, cbo婚姻状况);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t劳动强度, cbo劳动程度);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t与户主关系_户主, cbo与户主关系);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t民族, cbo民族);
            AtomEHR.公共卫生.util.ControlsHelper.SetComboxData("1", cbo民族);//默认汉族
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t文化程度, cbo文化程度);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t血型, cbo血型);
            cbo血型.Properties.Items.RemoveAt(4);
            AtomEHR.公共卫生.util.ControlsHelper.SetComboxData("5", cbo血型);//默认不详
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t证件类型, cbo证件类型);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t职业, cbo职业);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t有无, cbo过敏史);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t有无, cbo暴露史);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t有无, cbo遗传病史);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t有无, cbo残疾情况);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t家族史成员, cbo家族史关系);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t家族史成员, rpcbo家族关系);
            AtomEHR.公共卫生.util.ControlsHelper.SetComboxData("2", cbo过敏史);
            AtomEHR.公共卫生.util.ControlsHelper.SetComboxData("2", cbo暴露史);
            AtomEHR.公共卫生.util.ControlsHelper.SetComboxData("2", cbo遗传病史);
            AtomEHR.公共卫生.util.ControlsHelper.SetComboxData("2", cbo残疾情况);
            this.cbo与户主关系.SelectedIndex = 0;


            this.dte录入时间.Text = _Bll.ServiceDateTime;
            this.dte最近更新时间.Text = _Bll.ServiceDateTime;
            this.txt录入人.Text = Loginer.CurrentUser.AccountName;
            this.txt最近更新人.Text = Loginer.CurrentUser.AccountName;
            this.txt当前所属机构.Text = Loginer.CurrentUser.所属机构名称;

            this.radio疾病.EditValue = this.radio手术.EditValue = this.radio输血.EditValue = this.radio外伤.EditValue = "2";
        }
        private void radio疾病_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio疾病.EditValue.ToString();

            this.chk疾病_高血压.Enabled = this.chk疾病_恶性肿瘤.Enabled = this.chk疾病_肝炎.Enabled = this.chk疾病_肝炎.Enabled = this.chk疾病_冠心病.Enabled = this.chk疾病_结核病.Enabled = this.chk疾病_慢性阻塞性肺病.Enabled = this.chk疾病_脑卒中.Enabled = this.chk疾病_其他.Enabled = this.chk疾病_其他法定传染病.Enabled = this.chk疾病_糖尿病.Enabled = this.chk疾病_职业病.Enabled = this.chk疾病_重性精神疾病.Enabled = this.chk疾病_恶性肿瘤.Enabled = this.dte疾病_确诊时间.Enabled = this.btn疾病_添加.Enabled = index == "1" ? true : false;
        }
        private void radio手术_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio手术.EditValue.ToString();
            this.txt手术名称.Enabled = this.dte手术时间.Enabled = this.btn手术_添加.Enabled = index == "1" ? true : false;
        }
        private void radio外伤_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio外伤.EditValue.ToString();
            this.txt外伤名称.Enabled = this.dte外伤时间.Enabled = this.btn外伤_添加.Enabled = index == "1" ? true : false;
        }
        private void radio输血_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio输血.EditValue.ToString();
            this.txt输血原因.Enabled = this.dte输血时间.Enabled = this.btn输血_添加.Enabled = index == "1" ? true : false;
        }
        private void btn疾病_添加_Click(object sender, EventArgs e)
        {
            Set既往史_疾病();

        }
        private void btn手术_添加_Click(object sender, EventArgs e)
        {
            Set既往史_手术();
        }
        private void btn外伤_添加_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(dte外伤时间.Text.Trim()))
            {
                Msg.ShowInformation("请填写 确认时间！");
                this.dte外伤时间.Focus();
                return;
            }
            DataRow dr = _ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows.Add();
            dr[tb_健康档案_既往病史.疾病名称] = this.txt外伤名称.Text.Trim();
            dr[tb_健康档案_既往病史.D_JBBM] = this.txt外伤名称.Text.Trim();

            dr[tb_健康档案_既往病史.日期] = this.dte外伤时间.DateTime.ToString("yyyy-MM-dd");
            dr[tb_健康档案_既往病史.疾病类型] = "外伤";

            this.txt外伤名称.Text = string.Empty;
            this.dte外伤时间.Text = string.Empty;
            this.gc既往史.RefreshDataSource();
        }
        private void btn输血_添加_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(dte输血时间.Text.Trim()))
            {
                Msg.ShowInformation("请填写 确认时间！");
                this.dte输血时间.Focus();
                return;
            }
            DataRow dr = _ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows.Add();
            dr[tb_健康档案_既往病史.疾病名称] = this.txt输血原因.Text.Trim();
            dr[tb_健康档案_既往病史.D_JBBM] = this.txt输血原因.Text.Trim();

            dr[tb_健康档案_既往病史.日期] = this.dte输血时间.DateTime.ToString("yyyy-MM-dd");
            dr[tb_健康档案_既往病史.疾病类型] = "输血";

            this.txt输血原因.Text = string.Empty;
            this.dte输血时间.Text = string.Empty;
            this.gc既往史.RefreshDataSource();
        }

        #region 地区联动
        void cbo居住地址_街道_EditValueChanged(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            try
            {
                DataView dv县 = new DataView(DataDictCache.Cache.t地区信息);
                dv县.RowFilter = "上级编码=" + cbo居住地址_街道.EditValue;
                DataBinder.BindingLookupEditDataSource(cbo居住地址_村委会, dv县.ToTable(), "地区名称", "地区编码");
            }
            catch
            {
            }

        }

        void cbo居住地址_县_EditValueChanged(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            try
            {
                DataView dv县 = new DataView(DataDictCache.Cache.t地区信息);
                dv县.RowFilter = "上级编码=" + cbo居住地址_县.EditValue;
                DataBinder.BindingLookupEditDataSource(cbo居住地址_街道, dv县.ToTable(), "地区名称", "地区编码");
            }
            catch
            {
            }

        }

        void cbo居住地址_市_EditValueChanged(object sender, EventArgs e)
        {
            //throw new NotImplementedException();                             
            try
            {
                DataView dv县 = new DataView(DataDictCache.Cache.t地区信息);
                dv县.RowFilter = "上级编码=" + cbo居住地址_市.EditValue;
                DataBinder.BindingLookupEditDataSource(cbo居住地址_县, dv县.ToTable(), "地区名称", "地区编码");
            }
            catch
            {
            }

        }

        #endregion
        private void btn家族史_添加_Click(object sender, EventArgs e)
        {
            Set家族病史();
        }

        private void menuItem删除选中项_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.GridControl control = (DevExpress.XtraGrid.GridControl)this.contextMenuStrip1.SourceControl;
            if (control == null) return;
            DevExpress.XtraGrid.Views.Grid.GridView view = control.MainView as DevExpress.XtraGrid.Views.Grid.GridView;
            if (view == null) return;
            view.DeleteSelectedRows();
        }
        private void txt证件编号_Leave(object sender, EventArgs e)
        {
            string id = this.txt证件编号.Text.Trim();
            if (string.IsNullOrEmpty(id)) return;
            if ((!Regex.IsMatch(txt证件编号.Text, @"^(^\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$", RegexOptions.IgnoreCase)))
            {
                Msg.Warning("请输入正确的身份证号码！");
                this.txt证件编号.Text = "";
                this.txt证件编号.Focus();
                return;

            }
            if (_Bll.CheckNoExists(id))
            {
                DataTable dt = _Bll.GetBusinessByID(id);
                Msg.ShowInformation("此身份证号已经注册，不能再次注册！" + Environment.NewLine +
                    "居住地址为：" + _Bll.Return地区名称(dt.Rows[0][tb_健康档案.区].ToString()) + _Bll.Return地区名称(dt.Rows[0][tb_健康档案.街道].ToString()) + _Bll.Return地区名称(dt.Rows[0][tb_健康档案.居委会].ToString()) + dt.Rows[0][tb_健康档案.居住地址].ToString());
                //Msg.ShowInformation("此身份证号已经注册，不能再次注册！");
                this.txt证件编号.Text = "";
                this.txt证件编号.Focus();
                return;
            }
            else
            {
                if (id.Length == 18)
                {
                    string sf = id.Substring(6, 8);
                    string ddd = sf.Substring(0, 4) + "-" + sf.Substring(4, 2) + "-" + sf.Substring(6, 2);
                    this.dte出生日期.Text = ddd;

                    string xingbieindex = id.Substring(16, 1);
                    int index = 0;
                    if (Int32.TryParse(xingbieindex, out index))
                    {
                        if (index % 2 == 1)//奇数，代表男
                        {
                            util.ControlsHelper.SetComboxData("1", cbo性别);
                        }
                        else if (index % 2 == 0)//偶数，代表女
                        {
                            util.ControlsHelper.SetComboxData("2", cbo性别);
                        }
                    }
                }
                return;
            }
        }
        private void btn重复档案检测_Click(object sender, EventArgs e)
        {
            string sql = string.Empty;
            if (string.IsNullOrEmpty(this.txt姓名.Text.Trim()))
            {
                Msg.Warning("姓名不能为空！");
                this.txt姓名.Focus();
                return;
            }
            else
            {
                sql += " AND 姓名 = '" + this.txt姓名.Text.Trim() + "'";
            }
            if (string.IsNullOrEmpty(this.cbo性别.Text.Trim()) || this.cbo性别.Text.Trim() == "请选择")
            {
                Msg.Warning("性别不能为空！");
                this.cbo性别.Focus();
                return;
            }
            else
            {
                sql += " AND 性别 = '" + ControlsHelper.GetComboxKey(this.cbo性别) + "'";

            }
            if (string.IsNullOrEmpty(this.dte出生日期.Text.Trim()))
            {
                Msg.Warning("出生日期不能为空！");
                this.dte出生日期.Focus();
                return;
            }
            else
            {
                sql += " AND 出生日期 = '" + this.dte出生日期.DateTime.ToString("yyyy-MM-dd") + "'";
            }

            DataTable dt重复档案 = _Bll.GetSummaryByParam(sql);
            if (dt重复档案.Rows.Count == 0)
            {
                Msg.ShowInformation("没有检测到重复档案！");
            }
            else
            {
                frm重复档案检测 frm = new frm重复档案检测(dt重复档案);
                DialogResult result = frm.ShowDialog();
            }
        }
        private void dte出生日期_EditValueChanged(object sender, EventArgs e)
        {
            Show孕产情况();
        }

        private void chk孕产1_CheckedChanged(object sender, EventArgs e)
        {
            yccpd(chk孕产1, "0");
        }
        private void chk孕产2_CheckedChanged(object sender, EventArgs e)
        {
            yccpd(chk孕产2, "1");
        }
        private void chk孕产3_CheckedChanged(object sender, EventArgs e)
        {
            yccpd(chk孕产3, "2");
        }
        private void chk孕产4_CheckedChanged(object sender, EventArgs e)
        {
            yccpd(chk孕产4, "2");
        }

        private void cbo性别_SelectedIndexChanged(object sender, EventArgs e)
        {
            Show孕产情况();
        }


        #endregion

        #region Private Methods
        /// <summary>
        /// 绑定主表控件内容
        /// </summary>
        /// <param name="dataSource"></param>
        void DoBindingSummaryEditor(DataSet dataSource)
        {

            if (dataSource == null) return;
            this.gc既往史.DataSource = dataSource.Tables[tb_健康档案_既往病史.__TableName];
            this.gc家族史.DataSource = dataSource.Tables[tb_健康档案_家族病史.__TableName];
            //DataBinder.BindingTextEdit(cbo与户主关系, dataSource.Tables[tb_健康档案.__TableName], tb_健康档案.户主关系);
            ////DataBinder.BindingTextEdit(txt姓名, dataSource.Tables[tb_健康档案.__TableName], tb_健康档案.姓名);
            //DataBinder.BindingTextEdit(cbo居住地址_市, dataSource.Tables[tb_健康档案.__TableName], tb_健康档案.市);
            //DataBinder.BindingTextEdit(cbo居住地址_县, dataSource.Tables[tb_健康档案.__TableName], tb_健康档案.区);
            //DataBinder.BindingTextEdit(cbo居住地址_街道, dataSource.Tables[tb_健康档案.__TableName], tb_健康档案.街道);
            //DataBinder.BindingTextEdit(cbo居住地址_村委会, dataSource.Tables[tb_健康档案.__TableName], tb_健康档案.居委会);
        }
        private bool f保存检查()
        {
            if (string.IsNullOrEmpty(this.cbo与户主关系.Text.Trim()))
            {
                Msg.Warning("与户主关系不能为空！");
                this.cbo与户主关系.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(this.radio档案状态.Text.Trim()))
            {
                Msg.Warning("档案状态不能为空！");
                this.radio档案状态.Focus();
                return false;
            }
            if (txt姓名.Text == "")
            {
                Msg.Warning("姓名不能为空！");
                this.txt姓名.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(this.cbo性别.Text.Trim()))
            {
                Msg.Warning("性别不能为空！");
                this.cbo性别.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(this.dte出生日期.Text.Trim()))
            {
                Msg.Warning("出生日期不能为空！");
                this.dte出生日期.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(this.cbo民族.Text.Trim()))
            {
                Msg.Warning("民族不能为空！");
                this.cbo民族.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(cbo居住地址_街道.Text.Trim()))
            {
                Msg.Warning("街道/乡镇不能为空！");
                cbo居住地址_街道.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(cbo居住地址_村委会.Text.Trim()))
            {
                Msg.Warning("村委会不能为空！");
                cbo居住地址_村委会.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txt居住地址_详细地址.Text.Trim()))
            {
                Msg.Warning("详细地址不能为空！");
                txt居住地址_详细地址.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(cbo档案类别.Text.Trim()))
            {
                Msg.Warning("档案类别不能为空！");
                cbo档案类别.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(dte调查时间.Text.Trim()))
            {
                Msg.Warning("调查时间不能为空！");
                dte调查时间.Focus();
                return false;
            }
            if (this.cbo残疾情况.Text == "有")//存在残疾情况，至少需要选择一个残疾信息
            {
                string cjxx = GetFlowLayoutResult(this.flow残疾情况);
                if (string.IsNullOrEmpty(cjxx))
                {
                    Msg.Warning("有无残疾必须选择一个！");
                    this.cbo残疾情况.Focus();
                    return false;
                }
            }
            if (this.radio疾病.EditValue.ToString() == "1") //存在疾病情况
            {
                if (_ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Select("疾病类型='疾病'").Length <= 0)
                {
                    Msg.Warning("你选择了存在疾病，请至少选择一个疾病！");
                    this.radio疾病.Focus();
                    return false;
                }
            }
            if (this.cbo家族史.Text == "有") //存在疾病情况
            {
                if (_ds健康档案.Tables[tb_健康档案_家族病史.__TableName].Rows.Count <= 0)
                {
                    Msg.Warning("你选择了存在家族史，请至少选择一个疾病！");
                    this.cbo家族史.Focus();
                    return false;
                }
            }

            if (!string.IsNullOrEmpty(this.txt本人电话.Text.Trim()) && (this.txt本人电话.Text.Trim().Length != 7 && this.txt本人电话.Text.Trim().Length != 11))
            {
                Msg.Warning("请填写7位或者11位电话号码！");
                this.txt本人电话.Focus();
                return false;
            }
            if (!string.IsNullOrEmpty(this.txt联系人电话.Text.Trim()) && (this.txt联系人电话.Text.Trim().Length != 7 && this.txt联系人电话.Text.Trim().Length != 11))
            {
                Msg.Warning("请填写7位或者11位电话号码！");
                this.txt联系人电话.Focus();
                return false;
            }

            return true;
        }
        private string GetFlowLayoutResult(FlowLayoutPanel flow)
        {
            if (flow == null || flow.Controls.Count == 0) return "";
            string result = "";
            for (int i = 0; i < flow.Controls.Count; i++)
            {
                if (flow.Controls[i].GetType() == typeof(CheckEdit))
                {
                    CheckEdit chk = (CheckEdit)flow.Controls[i];
                    if (chk == null) continue;
                    if (chk.Checked)
                    {
                        result += chk.Tag + ",";
                    }
                }
            }
            result = string.IsNullOrEmpty(result) ? result : result.Remove(result.Length - 1);
            return result;
        }
        #region 获取生活环境数据

        private object Get禽畜栏()
        {
            return GetFlowLayoutResult(flow禽畜栏);
        }



        private object Get厕所()
        {
            return GetFlowLayoutResult(flow厕所);
        }

        private object Get饮水()
        {
            return GetFlowLayoutResult(flow饮水);
        }

        private object Get燃料类型()
        {
            return GetFlowLayoutResult(flow燃料类型);
        }

        private object Get厨房排气设施()
        {
            return GetFlowLayoutResult(flow厨房排风设施);
        }
        #endregion
        private void Set既往史_疾病()
        {
            if (string.IsNullOrEmpty(dte疾病_确诊时间.Text.Trim()))
            {
                Msg.ShowInformation("请填写 确认时间！");
                this.dte疾病_确诊时间.Focus();
                return;
            }
            /*
                    10001855	jb_gren	疾病	13	NULL	其他	NULL	1
                    10001856	jb_gren	疾病	2		高血压		1
                    10001857	jb_gren	疾病	3		糖尿病		1
                    10001858	jb_gren	疾病	4		冠心病		1
                    10001859	jb_gren	疾病	5		慢性阻塞性肺疾病		1
                    10001860	jb_gren	疾病	6		恶性肿瘤		1
                    10001861	jb_gren	疾病	7		脑卒中		1
                    10001862	jb_gren	疾病	8		重性精神疾病		1
                    10001863	jb_gren	疾病	9		结核病		1
                    10001864	jb_gren	疾病	10		肝炎		1
                    10001865	jb_gren	疾病	11		其他法定传染病		1
                    10001866	jb_gren	疾病	12		职业病		1
             */
            string 疾病名称 = string.Empty;
            string 疾病代码 = string.Empty;
            for (int i = 0; i < flow疾病.Controls.Count; i++)
            {
                if (flow疾病.Controls[i].GetType() == typeof(CheckEdit))//判断是否是复选框
                {
                    CheckEdit chk = (CheckEdit)flow疾病.Controls[i];
                    if (chk == null) continue;
                    if (chk.Tag == null) continue;
                    if (chk.Checked)
                    {
                        疾病名称 += chk.Tag + ",";
                        疾病代码 += chk.Text + ",";
                    }
                }
            }
            if (!string.IsNullOrEmpty(疾病名称))
            {
                疾病名称 = 疾病名称.Remove(疾病名称.Length - 1);
                疾病代码 = 疾病代码.Remove(疾病代码.Length - 1);
            }

            DataRow dr = _ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows.Add();
            dr[tb_健康档案_既往病史.疾病名称] = 疾病名称;
            dr[tb_健康档案_既往病史.D_JBBM] = 疾病代码;
            dr[tb_健康档案_既往病史.恶性肿瘤] = this.chk疾病_恶性肿瘤.Checked ? this.txt疾病_恶性肿瘤.Text.Trim() : "";
            dr[tb_健康档案_既往病史.职业病其他] = this.chk疾病_职业病.Checked ? this.txt疾病_职业病.Text.Trim() : "";
            dr[tb_健康档案_既往病史.疾病其他] = this.chk疾病_其他.Checked ? this.txt疾病_其他.Text.Trim() : "";
            dr[tb_健康档案_既往病史.日期] = this.dte疾病_确诊时间.DateTime.ToString("yyyy-MM");
            dr[tb_健康档案_既往病史.疾病类型] = "疾病";
            //清空所有的数据
            for (int i = 0; i < flow疾病.Controls.Count; i++)
            {
                if (flow疾病.Controls[i].GetType() == typeof(CheckEdit))//判断是否是复选框
                {
                    CheckEdit chk = (CheckEdit)flow疾病.Controls[i];
                    chk.Checked = false;
                }
                if (flow疾病.Controls[i].GetType() == typeof(TextEdit))//判断是否是复选框
                {
                    TextEdit txt = (TextEdit)flow疾病.Controls[i];
                    txt.Text = string.Empty;
                }
                if (flow疾病.Controls[i].GetType() == typeof(DateEdit))//判断是否是复选框
                {
                    DateEdit dte = (DateEdit)flow疾病.Controls[i];
                    dte.Text = string.Empty;
                }
            }

            this.gc既往史.RefreshDataSource();
        }
        private void Set家族病史()
        {
            /*
             ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
                    10002050	jzsxin	家族史	1		高血压		1
                    10002051	jzsxin	家族史	2		糖尿病		1
                    10002052	jzsxin	家族史	3		冠心病		1
                    10002053	jzsxin	家族史	4		慢性阻塞性肺疾病		1
                    10002054	jzsxin	家族史	5		恶性肿瘤		1
                    10002055	jzsxin	家族史	6		脑卒中		1
                    10002056	jzsxin	家族史	7		重性精神疾病		1
                    10002057	jzsxin	家族史	8		结核病		1
                    10002058	jzsxin	家族史	9		肝炎		1
                    10002059	jzsxin	家族史	10		先天畸形		1
                    10002060	jzsxin	家族史	100		其他		1 
             * 
             * 
             P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC
                    jzscy	        家族史成员	1		（外）祖父
                    jzscy	        家族史成员	2		（外）祖母
                    jzscy	        家族史成员	3		父亲
                    jzscy	        家族史成员	4		母亲
                    jzscy	        家族史成员	5		兄弟姐妹
             */
            string 家族病史 = string.Empty;
            string 疾病代码 = string.Empty;
            string 家族关系 = string.Empty;
            for (int i = 0; i < flow家族史.Controls.Count; i++)
            {
                if (flow家族史.Controls[i].GetType() == typeof(CheckEdit))//判断是否是复选框
                {
                    CheckEdit chk = (CheckEdit)flow家族史.Controls[i];
                    if (chk == null) continue;
                    if (chk.Tag == null) continue;
                    if (chk.Checked)
                    {
                        家族病史 += chk.Tag + ",";
                        疾病代码 += chk.Text + ",";
                    }
                }
            }
            if (!string.IsNullOrEmpty(家族病史))
            {
                家族病史 = 家族病史.Remove(家族病史.Length - 1);
                疾病代码 = 疾病代码.Remove(疾病代码.Length - 1);
            }

            DataRow dr = _ds健康档案.Tables[tb_健康档案_家族病史.__TableName].Rows.Add();
            dr[tb_健康档案_家族病史.家族病史] = 家族病史;
            dr[tb_健康档案_家族病史.D_JBBM] = 疾病代码;
            dr[tb_健康档案_家族病史.家族关系] = ControlsHelper.GetComboxKey(cbo家族史关系);
            dr[tb_健康档案_家族病史.其他疾病] = this.txt家族史_其他.Text.Trim();
            dr[tb_健康档案_家族病史.家族关系名称] = this.cbo家族史关系.Text.Trim();
            //清空所有的数据
            for (int i = 0; i < flow家族史.Controls.Count; i++)
            {
                if (flow家族史.Controls[i].GetType() == typeof(CheckEdit))//判断是否是复选框
                {
                    CheckEdit chk = (CheckEdit)flow家族史.Controls[i];
                    chk.Checked = false;
                }
                if (flow家族史.Controls[i].GetType() == typeof(TextEdit))//判断是否是复选框
                {
                    TextEdit txt = (TextEdit)flow家族史.Controls[i];
                    txt.Text = string.Empty;
                }
                if (flow疾病.Controls[i].GetType() == typeof(DateEdit))//判断是否是复选框
                {
                    DateEdit dte = (DateEdit)flow家族史.Controls[i];
                    dte.Text = string.Empty;
                }
            }

            this.gc家族史.RefreshDataSource();
        }
        private void Set既往史_手术()
        {
            if (string.IsNullOrEmpty(dte手术时间.Text.Trim()))
            {
                Msg.ShowInformation("请填写 确认时间！");
                this.dte手术时间.Focus();
                return;
            }
            DataRow dr = _ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows.Add();
            dr[tb_健康档案_既往病史.疾病名称] = this.txt手术名称.Text.Trim();
            dr[tb_健康档案_既往病史.D_JBBM] = this.txt手术名称.Text.Trim();
            dr[tb_健康档案_既往病史.日期] = this.dte手术时间.DateTime.ToString("yyyy-MM-dd");
            dr[tb_健康档案_既往病史.疾病类型] = "手术";

            this.txt手术名称.Text = string.Empty;
            this.dte手术时间.Text = string.Empty;

            this.gc既往史.RefreshDataSource();
        }
        private object Get过敏史()
        {
            return GetFlowLayoutResult(flow过敏史);
        }
        private object Get残疾情况()
        {
            return GetFlowLayoutResult(flow残疾情况);
        }
        private object Get医疗费用支付方式()
        {
            return GetFlowLayoutResult(flow医疗费用支付方式);
        }
        private object Get档案状态()
        {
            return this.radio档案状态.EditValue;
        }
        private int Get完整度(int i)
        {
            int k = Convert.ToInt32((20 - i) * 100 / 20.0);
            return k;
        }
        private int Get缺项()
        {
            int i = 0;
            if (this.txt证件编号.Text.Trim() == "") i++;
            if (this.txt工作单位.Text.Trim() == "") i++;
            if (this.txt本人电话.Text.Trim() == "") i++;
            if (this.txt联系人姓名.Text.Trim() == "") i++;
            if (this.txt联系人电话.Text.Trim() == "") i++;
            if (this.cbo常住类型.Text.Trim() == "") i++;
            if (this.cbo民族.Text.Trim() == "") i++;
            if (this.cbo血型.Text.Trim() == "") i++;
            //if (this.cboRH阴性.Text.Trim() == "") i++;
            if (this.cbo文化程度.Text.Trim() == "") i++;
            if (this.cbo职业.Text.Trim() == "") i++;
            if (Get医疗费用支付方式().ToString() == "") i++;
            if (this.txt居住地址_详细地址.Text.Trim() == "") i++;
            if (this.cbo婚姻状况.Text.Trim() == "") i++;
            //if (Get过敏史().ToString() == "") i++;
            //if (this.txt工作单位.Text.Trim() == "") i++;
            //if (this.txt工作单位.Text.Trim() == "") i++;
            //if (this.txt工作单位.Text.Trim() == "") i++;

            return i;
        }
        private int GetAge(String strBirthday)
        {
            int returnAge = 0;
            if (string.IsNullOrEmpty(strBirthday))
                return returnAge;

            String[] strBirthdayArr = strBirthday.Split('-');
            int birthYear = Int32.Parse(strBirthdayArr[0]);
            int birthMonth = Int32.Parse(strBirthdayArr[1]);
            int birthDay = Int32.Parse(strBirthdayArr[2]);

            String date = _Bll.ServiceDateTime;
            int nowYear = Int32.Parse(date.Substring(0, 4));
            int nowMonth = Int32.Parse(date.Substring(5, 2));
            int nowDay = Int32.Parse(date.Substring(8, 2));
            if (nowYear == birthYear)
            {
                returnAge = 0;
            }
            else
            {
                int ageDiff = nowYear - birthYear;
                if (ageDiff > 0)
                {
                    if (nowMonth == birthMonth)
                    {
                        int dayDiff = nowDay - birthDay;
                        if (dayDiff < 0)
                            returnAge = ageDiff - 1;
                        else
                            returnAge = ageDiff;
                    }
                    else
                    {
                        int monthDiff = nowMonth - birthMonth;
                        if (monthDiff < 0)
                            returnAge = ageDiff - 1;
                        else
                            returnAge = ageDiff;
                    }
                }
                else
                {
                    returnAge = -1;
                }
            }
            return returnAge;
        }
        public static long getIntervalDay(String sd, String ed)
        {
            return (Convert.ToDateTime(ed).Ticks - Convert.ToDateTime(sd).Ticks) / 86400000L;
        }
        private void yccpd(CheckEdit pram, string pd)
        {
            string fyc = this.cbo孕次.Text.Trim();
            string fcc = this.cbo产次.Text.Trim();
            if (pd == "1" && pram.Checked)
            {
                cbo孕次.Enabled = true;
                cbo产次.Enabled = true;
            }
            else if (pd == "2" && pram.Checked)
            {
                if (fcc == "0" || fcc == "")
                {
                    cbo产次.SelectedIndex = 1;
                }
                cbo孕次.Enabled = true;
                cbo产次.Enabled = true;
            }
            else
            {
                cbo孕次.Enabled = false;
                cbo产次.Enabled = false;
            }
        }
        private void Show孕产情况()
        {
            //修改出生日期，会控制  怀孕情况 的隐藏和显示
            if (this.cbo性别.Text.Trim() == "女")
            {
                int age = GetAge(this.dte出生日期.Text);
                if (age >= 15)
                {
                    this.layout孕产情况.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                }
                else
                {
                    this.layout孕产情况.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                }
            }
            else
            {
                this.layout孕产情况.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;

            }
        }

        private void Disable滚动()
        {
            for (int i = 0; i < this.Controls.Count; i++)
            {
                if (Controls[i] is LayoutControl)
                {
                    LayoutControl layoutControl = (LayoutControl)Controls[i];
                    for (int j = 0; j < layoutControl.Controls.Count; j++)
                    {
                        if (layoutControl.Controls[j] is ComboBoxEdit)
                        {
                            ComboBoxEdit cbo = (ComboBoxEdit)layoutControl.Controls[j];
                            cbo.Properties.AllowMouseWheel = false;
                        }
                        if (layoutControl.Controls[j] is RadioGroup)
                        {
                            RadioGroup radio = (RadioGroup)layoutControl.Controls[j];
                            radio.Properties.AllowMouseWheel = false;
                        }
                        if (layoutControl.Controls[j] is FlowLayoutPanel)
                        {
                            FlowLayoutPanel flow = (FlowLayoutPanel)layoutControl.Controls[j];
                            for (int k = 0; k < flow.Controls.Count; k++)
                            {
                                if (flow.Controls[k] is ComboBoxEdit)
                                {
                                    ComboBoxEdit cbo = (ComboBoxEdit)flow.Controls[k];
                                    cbo.Properties.AllowMouseWheel = false;
                                }
                            }
                        }
                    }
                }
                if (Controls[i] is FlowLayoutPanel)
                {
                    FlowLayoutPanel flow = (FlowLayoutPanel)Controls[i];
                    for (int j = 0; j < flow.Controls.Count; j++)
                    {
                        if (flow.Controls[j] is ComboBoxEdit)
                        {
                            ComboBoxEdit cbo = (ComboBoxEdit)flow.Controls[j];
                            cbo.Properties.AllowMouseWheel = false;
                        }
                    }
                }

                if (Controls[i] is ComboBoxEdit)
                {
                    ComboBoxEdit cbo = (ComboBoxEdit)Controls[i];
                    cbo.Properties.AllowMouseWheel = false;
                }
            }
        }
        #endregion

        private void cbo居住地址_村委会_EditValueChanged(object sender, EventArgs e)
        {
            string value = this.cbo居住地址_村委会.Text.Trim();
            this.txt居住地址_详细地址.Text = value;
        }

        private void cbo性别_EditValueChanged(object sender, EventArgs e)
        {
            Show孕产情况();
        }

        private void gv既往史_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if (e.Column != col日期) return;
            GridView gv = sender as GridView;
            string fieldName = gv.GetRowCellValue(e.RowHandle, gv.Columns["疾病类型"]).ToString();
            switch (fieldName)
            {
                case "疾病":
                    e.RepositoryItem = repositoryItemDateEdit2;
                    break;
                default:
                    e.RepositoryItem = repositoryItemDateEdit1;
                    break;

            }
        }

        /// <summary>
        /// 快捷键 设置
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref   System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
                return true;
            }
            return base.ProcessCmdKey(ref   msg, keyData);
        }

        private void chk医疗费用支付方式_城镇职工基本医疗保险_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chk医疗费用支付方式_城镇职工基本医疗保险.Checked)
            {
                this.textEdit职工医疗保险卡号.Enabled = true;
            }
            else
            {
                this.textEdit职工医疗保险卡号.Enabled = false;
                this.textEdit职工医疗保险卡号.Text = "";
            }
        }

        private void chk医疗费用支付方式_城镇居民基本医疗保险_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chk医疗费用支付方式_城镇居民基本医疗保险.Checked)
            {
                this.textEdit居民医疗保险卡号.Enabled = true;
            }
            else
            {
                this.textEdit居民医疗保险卡号.Enabled = false;
                this.textEdit居民医疗保险卡号.Text = "";
            }
        }

        private void chk医疗费用支付方式_贫困救助_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chk医疗费用支付方式_贫困救助.Checked)
            {
                this.textEdit贫困救助卡号.Enabled = true;
            }
            else
            {
                this.textEdit贫困救助卡号.Enabled = false;
                this.textEdit贫困救助卡号.Text = "";
            }
        }

    }
}
