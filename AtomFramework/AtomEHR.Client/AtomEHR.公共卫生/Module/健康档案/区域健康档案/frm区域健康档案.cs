﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Business.Security;

namespace AtomEHR.公共卫生.Module.健康档案.辖区健康档案
{
    public partial class frm区域健康档案 : AtomEHR.Library.frmBaseBusinessForm
    {
        public frm区域健康档案()
        {
            InitializeComponent();
        }

        private void frm区域健康档案_Load(object sender, EventArgs e)
        {
            InitializeForm();
        }

        protected override void InitializeForm()
        {
            _BLL = new bll健康档案();// 业务逻辑层实例
            _SummaryView = new DevGridView(gv个人健康档案);//给成员变量赋值.每个业务窗体必需给这个变量赋值.
            _ActiveEditor = txt身份证号;
            //_DetailGroupControl = groupControl1;

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gv个人健康档案);

            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, cbo性别, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案类别, cbo档案类别, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案状态, cbo档案状态, "P_CODE", "P_DESC");
            cbo档案状态.SelectedIndex = 1;
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t是否, cbo合格档案, "P_CODE", "P_DESC");
            //为“镇”绑定信息
            DataView dv镇 = new DataView(DataDictCache.Cache.t地区信息);
            dv镇.RowFilter = "上级编码=371323";
            util.ControlsHelper.BindComboxData(dv镇.ToTable(), comboBoxEdit镇, "地区编码", "地区名称");
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txt身份证号.Text))
            {
                Msg.Warning("为了信息安全，请输入身份证号查询；如果为儿童可先输入父母身份证查询其父母信息后，通过家庭查找儿童！");
                return;
            }
            pagerControl1.InitControl();
            DoSearchSummary();
        }

        protected override bool DoSearchSummary()
        {
            string _strWhere = "";
            if (!string.IsNullOrEmpty(this.txt姓名.Text.Trim()))
            {
                _strWhere += " and (姓名 like'" + this.txt姓名.Text.Trim() + "%') ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(cbo性别)))
            {
                _strWhere += " and 性别编码 ='" + util.ControlsHelper.GetComboxKey(cbo性别) + "'  ";
            }
            if (!string.IsNullOrEmpty(this.txt档案号.Text.Trim()))
            {
                _strWhere += " and 个人档案编号 = '" + this.txt档案号.Text.Trim() + "'  ";
            }
            if (!string.IsNullOrEmpty(this.txt身份证号.Text.Trim()))
            {
                _strWhere += " and 身份证号 = '" + this.txt身份证号.Text.Trim() + "'  ";
            }
            if (!string.IsNullOrEmpty(this.dte出生时间1.Text.Trim()))
            {
                _strWhere += " and 出生日期 >= '" + this.dte出生时间1.Text.Trim() + "'  ";
            }
            if (!string.IsNullOrEmpty(this.dte出生时间2.Text.Trim()))
            {
                _strWhere += " and 出生日期 <= '" + this.dte出生时间2.Text.Trim() + "'  ";
            }
            if (!string.IsNullOrEmpty(this.dte录入时间1.Text.Trim()))
            {
                _strWhere += " and 创建时间 >= '" + this.dte录入时间1.Text.Trim() + " 00:00:00'  ";
            }
            if (!string.IsNullOrEmpty(this.dte录入时间2.Text.Trim()))
            {
                _strWhere += " and 创建时间 <= '" + this.dte录入时间2.Text.Trim() + " 23:59:59'  ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(cbo档案类别)))
            {
                _strWhere += " and 档案类别 ='" + util.ControlsHelper.GetComboxKey(cbo档案类别) + "'  ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(cbo档案状态)))
            {
                _strWhere += " and 档案状态 ='" + util.ControlsHelper.GetComboxKey(cbo档案状态) + "'  ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(cbo合格档案)))
            {
                if (util.ControlsHelper.GetComboxKey(cbo合格档案) == "2")//不合格档案
                {
                    _strWhere += " and 完整度 <> 100 ";
                }
                if (util.ControlsHelper.GetComboxKey(cbo合格档案) == "1")//合格档案
                {
                    _strWhere += " and 完整度 = 100 ";
                }
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(comboBoxEdit镇)))
            {
                _strWhere += " and 街道编码 = '" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'  ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(comboBoxEdit村)))
            {
                _strWhere += " and 居委会编码 = '" + util.ControlsHelper.GetComboxKey(comboBoxEdit村) + "'  ";
            }
            if (!string.IsNullOrEmpty(this.textEdit地址.Text.Trim()))
            {
                _strWhere += " and 居住地址 LIKE '%" + this.textEdit地址.Text.Trim() + "%'  ";
            }
            DataTable dt = null;
            if (this.cbo排序方式.Text.Trim() != "查询排序方式")
            {
                if (this.cbo排序方式.Text.Trim() == "档案创建时间")
                    dt = this.pagerControl1.GetQueryResult("View_个人信息表", "*", _strWhere, "创建时间", "DESC").Tables[0];
                if (this.cbo排序方式.Text.Trim() == "个人档案编号")
                    dt = this.pagerControl1.GetQueryResult("View_个人信息表", "*", _strWhere, "个人档案编号", "DESC").Tables[0];
                if (this.cbo排序方式.Text.Trim() == "家庭档案编号")
                    dt = this.pagerControl1.GetQueryResultNew("View_个人信息表", "*", _strWhere, "家庭档案编号", "DESC").Tables[0];
            }
            else
            {
                dt = this.pagerControl1.GetQueryResultNew("View_个人信息表", "*", _strWhere, "创建时间", "DESC").Tables[0];
            }
            this.pagerControl1.DrawControl();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i][tb_健康档案.姓名] = util.DESEncrypt.DES解密(dt.Rows[i][tb_健康档案.姓名].ToString());
                dt.Rows[i][tb_健康档案.档案状态] = _BLL.ReturnDis字典显示("rkx_dazt", dt.Rows[i][tb_健康档案.档案状态].ToString());
                dt.Rows[i][tb_健康档案.所属机构] = _BLL.Return机构名称(dt.Rows[i][tb_健康档案.所属机构].ToString());
                dt.Rows[i][tb_健康档案.创建机构] = _BLL.Return机构名称(dt.Rows[i][tb_健康档案.创建机构].ToString());
                dt.Rows[i][tb_健康档案.创建人] = _BLL.Return用户名称(dt.Rows[i][tb_健康档案.创建人].ToString());
                dt.Rows[i][tb_健康档案.居住地址] = dt.Rows[i][tb_健康档案.区].ToString() + dt.Rows[i][tb_健康档案.街道] + dt.Rows[i][tb_健康档案.居委会] + dt.Rows[i][tb_健康档案.居住地址];
            }
            DoBindingSummaryGrid(dt); //绑定主表的Grid
            ShowSummaryPage(true); //显示Summary页面.   

            return dt != null && dt.Rows.Count > 0;
        }

        private void link档案号_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv个人健康档案.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            string date = row["创建时间"] as string;
            Module.个人健康.frm个人健康 frm = new Module.个人健康.frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, date);
            frm.Show();
        }

    }
}
