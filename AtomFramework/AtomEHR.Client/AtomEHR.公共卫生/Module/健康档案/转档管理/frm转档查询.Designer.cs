﻿using AtomEHR.Library.UserControls;
namespace AtomEHR.公共卫生.Module.健康档案.转档管理
{
    partial class frm转档查询
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.cbo机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn已转入档案 = new DevExpress.XtraEditors.SimpleButton();
            this.btn已转出档案 = new DevExpress.XtraEditors.SimpleButton();
            this.btn待转入档案 = new DevExpress.XtraEditors.SimpleButton();
            this.btn待转出档案 = new DevExpress.XtraEditors.SimpleButton();
            this.btn驳回申请档案 = new DevExpress.XtraEditors.SimpleButton();
            this.gc已转出档案 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv已转出档案 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col个人档案号码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.link个人档案编号 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.col姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col性别 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col身份证号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col所属机构 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col转入机构 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col转档原因 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col所属机构审核人 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col转出时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col转档人 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.tab驳回申请档案 = new DevExpress.XtraTab.XtraTabPage();
            this.gc驳回申请档案 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv驳回申请档案 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pager驳回申请档案 = new TActionProject.PagerControl();
            this.tab已转出档案 = new DevExpress.XtraTab.XtraTabPage();
            this.pager已转出档案 = new TActionProject.PagerControl();
            this.tab待转入档案 = new DevExpress.XtraTab.XtraTabPage();
            this.gc待转入档案 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv待转入档案 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col操作 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn撤销 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.pager待转入档案 = new TActionProject.PagerControl();
            this.tab已转入档案 = new DevExpress.XtraTab.XtraTabPage();
            this.gc已转入档案 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv已转入档案 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pager已转入档案 = new TActionProject.PagerControl();
            this.tab待转出档案 = new DevExpress.XtraTab.XtraTabPage();
            this.gc待转出档案 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv待转出档案 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn同意 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn驳回 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.pager待转出档案 = new TActionProject.PagerControl();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc已转出档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv已转出档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link个人档案编号)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.tab驳回申请档案.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc驳回申请档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv驳回申请档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit5)).BeginInit();
            this.tab已转出档案.SuspendLayout();
            this.tab待转入档案.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc待转入档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv待转入档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn撤销)).BeginInit();
            this.tab已转入档案.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc已转入档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv已转入档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            this.tab待转出档案.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc待转出档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv待转出档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn同意)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn驳回)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.groupControl2);
            this.tpSummary.Controls.Add(this.flowLayoutPanel1);
            this.tpSummary.Controls.Add(this.groupControl1);
            this.tpSummary.Size = new System.Drawing.Size(1108, 530);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 29);
            this.pnlSummary.Size = new System.Drawing.Size(1114, 536);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Size = new System.Drawing.Size(1114, 536);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(932, 589);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Location = new System.Drawing.Point(0, 3);
            this.gcNavigator.Size = new System.Drawing.Size(1114, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(936, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(739, 2);
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1114, 3);
            this.panelControl2.TabIndex = 2;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.gridControl1);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(0, 3);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1114, 562);
            this.panelControl3.TabIndex = 3;
            // 
            // gridControl1
            // 
            this.gridControl1.AllowBandedGridColumnSort = false;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.IsBestFitColumns = true;
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.ShowContextMenu = false;
            this.gridControl1.Size = new System.Drawing.Size(1110, 558);
            this.gridControl1.StrWhere = "";
            this.gridControl1.TabIndex = 1;
            this.gridControl1.UseCheckBox = false;
            this.gridControl1.View = "";
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gridView1.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gridView1.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.DetailTip.Options.UseFont = true;
            this.gridView1.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.Empty.Options.UseFont = true;
            this.gridView1.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.EvenRow.Options.UseFont = true;
            this.gridView1.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gridView1.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.FilterPanel.Options.UseFont = true;
            this.gridView1.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.FixedLine.Options.UseFont = true;
            this.gridView1.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.FocusedCell.Options.UseFont = true;
            this.gridView1.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.GroupButton.Options.UseFont = true;
            this.gridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView1.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gridView1.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.HorzLine.Options.UseFont = true;
            this.gridView1.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.OddRow.Options.UseFont = true;
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.RowSeparator.Options.UseFont = true;
            this.gridView1.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.SelectedRow.Options.UseFont = true;
            this.gridView1.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.TopNewRow.Options.UseFont = true;
            this.gridView1.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.VertLine.Options.UseFont = true;
            this.gridView1.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.ViewCaption.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn6,
            this.gridColumn8,
            this.gridColumn4,
            this.gridColumn7,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn5});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupPanelText = "DragColumn";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn1.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "档案号";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn2.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "姓名";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn3.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "性别";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn6.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn6.AppearanceHeader.Options.UseFont = true;
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "身份证号";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 3;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn8.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn8.AppearanceHeader.Options.UseFont = true;
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.Caption = "当前所属机构";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 4;
            this.gridColumn8.Width = 112;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn4.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "转出机构";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 5;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn7.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn7.AppearanceHeader.Options.UseFont = true;
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.Caption = "转档原因";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 6;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn9.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn9.AppearanceHeader.Options.UseFont = true;
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.Caption = "所属机构审核人";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 7;
            this.gridColumn9.Width = 150;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn10.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn10.AppearanceHeader.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.Caption = "转入时间";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 8;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn5.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn5.AppearanceHeader.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "转档人";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 9;
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.layoutControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1108, 53);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "信息查询 \r\n";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.checkEdit1);
            this.layoutControl1.Controls.Add(this.cbo机构);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 22);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1104, 29);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(304, 4);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "包含下属机构";
            this.checkEdit1.Size = new System.Drawing.Size(796, 19);
            this.checkEdit1.StyleController = this.layoutControl1;
            this.checkEdit1.TabIndex = 5;
            // 
            // cbo机构
            // 
            this.cbo机构.Location = new System.Drawing.Point(109, 4);
            this.cbo机构.Name = "cbo机构";
            this.cbo机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo机构.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.cbo机构.Size = new System.Drawing.Size(191, 20);
            this.cbo机构.StyleController = this.layoutControl1;
            this.cbo机构.TabIndex = 4;
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(0, 0);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1104, 29);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.cbo机构;
            this.layoutControlItem1.CustomizationFormText = "机 构：";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(300, 25);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "机 构：";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.checkEdit1;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(300, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(800, 25);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn已转入档案);
            this.flowLayoutPanel1.Controls.Add(this.btn已转出档案);
            this.flowLayoutPanel1.Controls.Add(this.btn待转入档案);
            this.flowLayoutPanel1.Controls.Add(this.btn待转出档案);
            this.flowLayoutPanel1.Controls.Add(this.btn驳回申请档案);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 53);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1108, 40);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // btn已转入档案
            // 
            this.btn已转入档案.Location = new System.Drawing.Point(3, 3);
            this.btn已转入档案.Name = "btn已转入档案";
            this.btn已转入档案.Size = new System.Drawing.Size(93, 34);
            this.btn已转入档案.TabIndex = 0;
            this.btn已转入档案.Text = "已转入档案";
            this.btn已转入档案.Click += new System.EventHandler(this.btn已转入档案_Click);
            // 
            // btn已转出档案
            // 
            this.btn已转出档案.Location = new System.Drawing.Point(102, 3);
            this.btn已转出档案.Name = "btn已转出档案";
            this.btn已转出档案.Size = new System.Drawing.Size(93, 34);
            this.btn已转出档案.TabIndex = 2;
            this.btn已转出档案.Text = "已转出档案";
            this.btn已转出档案.Click += new System.EventHandler(this.btn已转出档案_Click);
            // 
            // btn待转入档案
            // 
            this.btn待转入档案.Location = new System.Drawing.Point(201, 3);
            this.btn待转入档案.Name = "btn待转入档案";
            this.btn待转入档案.Size = new System.Drawing.Size(93, 34);
            this.btn待转入档案.TabIndex = 3;
            this.btn待转入档案.Text = "待转入档案";
            this.btn待转入档案.Click += new System.EventHandler(this.btn待转入档案_Click);
            // 
            // btn待转出档案
            // 
            this.btn待转出档案.Location = new System.Drawing.Point(300, 3);
            this.btn待转出档案.Name = "btn待转出档案";
            this.btn待转出档案.Size = new System.Drawing.Size(93, 34);
            this.btn待转出档案.TabIndex = 4;
            this.btn待转出档案.Text = "待转出档案";
            this.btn待转出档案.Click += new System.EventHandler(this.btn待转出档案_Click);
            // 
            // btn驳回申请档案
            // 
            this.btn驳回申请档案.Location = new System.Drawing.Point(399, 3);
            this.btn驳回申请档案.Name = "btn驳回申请档案";
            this.btn驳回申请档案.Size = new System.Drawing.Size(93, 34);
            this.btn驳回申请档案.TabIndex = 5;
            this.btn驳回申请档案.Text = "驳回申请档案";
            this.btn驳回申请档案.Click += new System.EventHandler(this.btn驳回申请档案_Click);
            // 
            // gc已转出档案
            // 
            this.gc已转出档案.AllowBandedGridColumnSort = false;
            this.gc已转出档案.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc已转出档案.IsBestFitColumns = true;
            this.gc已转出档案.Location = new System.Drawing.Point(0, 0);
            this.gc已转出档案.MainView = this.gv已转出档案;
            this.gc已转出档案.Name = "gc已转出档案";
            this.gc已转出档案.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.link个人档案编号});
            this.gc已转出档案.ShowContextMenu = false;
            this.gc已转出档案.Size = new System.Drawing.Size(1098, 354);
            this.gc已转出档案.StrWhere = "";
            this.gc已转出档案.TabIndex = 5;
            this.gc已转出档案.UseCheckBox = false;
            this.gc已转出档案.View = "";
            this.gc已转出档案.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv已转出档案});
            // 
            // gv已转出档案
            // 
            this.gv已转出档案.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gv已转出档案.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gv已转出档案.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gv已转出档案.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.DetailTip.Options.UseFont = true;
            this.gv已转出档案.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.Empty.Options.UseFont = true;
            this.gv已转出档案.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.EvenRow.Options.UseFont = true;
            this.gv已转出档案.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gv已转出档案.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.FilterPanel.Options.UseFont = true;
            this.gv已转出档案.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.FixedLine.Options.UseFont = true;
            this.gv已转出档案.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.FocusedCell.Options.UseFont = true;
            this.gv已转出档案.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.FocusedRow.Options.UseFont = true;
            this.gv已转出档案.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gv已转出档案.Appearance.FooterPanel.Options.UseFont = true;
            this.gv已转出档案.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.GroupButton.Options.UseFont = true;
            this.gv已转出档案.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.GroupFooter.Options.UseFont = true;
            this.gv已转出档案.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.GroupPanel.Options.UseFont = true;
            this.gv已转出档案.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.GroupRow.Options.UseFont = true;
            this.gv已转出档案.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.HeaderPanel.Options.UseFont = true;
            this.gv已转出档案.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gv已转出档案.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.HorzLine.Options.UseFont = true;
            this.gv已转出档案.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.OddRow.Options.UseFont = true;
            this.gv已转出档案.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.Preview.Options.UseFont = true;
            this.gv已转出档案.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.Row.Options.UseFont = true;
            this.gv已转出档案.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.RowSeparator.Options.UseFont = true;
            this.gv已转出档案.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.SelectedRow.Options.UseFont = true;
            this.gv已转出档案.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.TopNewRow.Options.UseFont = true;
            this.gv已转出档案.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.VertLine.Options.UseFont = true;
            this.gv已转出档案.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转出档案.Appearance.ViewCaption.Options.UseFont = true;
            this.gv已转出档案.ColumnPanelRowHeight = 30;
            this.gv已转出档案.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col个人档案号码,
            this.col姓名,
            this.col性别,
            this.col身份证号,
            this.col所属机构,
            this.col转入机构,
            this.col转档原因,
            this.col所属机构审核人,
            this.col转出时间,
            this.col转档人});
            this.gv已转出档案.GridControl = this.gc已转出档案;
            this.gv已转出档案.GroupPanelText = "DragColumn";
            this.gv已转出档案.Name = "gv已转出档案";
            this.gv已转出档案.OptionsView.ColumnAutoWidth = false;
            this.gv已转出档案.OptionsView.EnableAppearanceEvenRow = true;
            this.gv已转出档案.OptionsView.EnableAppearanceOddRow = true;
            this.gv已转出档案.OptionsView.ShowGroupPanel = false;
            // 
            // col个人档案号码
            // 
            this.col个人档案号码.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.col个人档案号码.AppearanceCell.Options.UseBackColor = true;
            this.col个人档案号码.AppearanceCell.Options.UseTextOptions = true;
            this.col个人档案号码.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col个人档案号码.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col个人档案号码.AppearanceHeader.Options.UseFont = true;
            this.col个人档案号码.AppearanceHeader.Options.UseTextOptions = true;
            this.col个人档案号码.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col个人档案号码.Caption = "档案号";
            this.col个人档案号码.ColumnEdit = this.link个人档案编号;
            this.col个人档案号码.FieldName = "个人档案编号";
            this.col个人档案号码.Name = "col个人档案号码";
            this.col个人档案号码.OptionsColumn.ReadOnly = true;
            this.col个人档案号码.Visible = true;
            this.col个人档案号码.VisibleIndex = 0;
            this.col个人档案号码.Width = 127;
            // 
            // link个人档案编号
            // 
            this.link个人档案编号.AutoHeight = false;
            this.link个人档案编号.Name = "link个人档案编号";
            this.link个人档案编号.Click += new System.EventHandler(this.link个人档案编号_Click);
            // 
            // col姓名
            // 
            this.col姓名.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.col姓名.AppearanceCell.Options.UseBackColor = true;
            this.col姓名.AppearanceCell.Options.UseTextOptions = true;
            this.col姓名.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col姓名.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col姓名.AppearanceHeader.Options.UseFont = true;
            this.col姓名.AppearanceHeader.Options.UseTextOptions = true;
            this.col姓名.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col姓名.Caption = "姓名";
            this.col姓名.FieldName = "姓名";
            this.col姓名.Name = "col姓名";
            this.col姓名.OptionsColumn.AllowEdit = false;
            this.col姓名.OptionsColumn.ReadOnly = true;
            this.col姓名.Visible = true;
            this.col姓名.VisibleIndex = 1;
            // 
            // col性别
            // 
            this.col性别.AppearanceCell.Options.UseTextOptions = true;
            this.col性别.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col性别.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col性别.AppearanceHeader.Options.UseFont = true;
            this.col性别.AppearanceHeader.Options.UseTextOptions = true;
            this.col性别.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col性别.Caption = "性别";
            this.col性别.FieldName = "性别";
            this.col性别.Name = "col性别";
            this.col性别.OptionsColumn.AllowEdit = false;
            this.col性别.OptionsColumn.ReadOnly = true;
            this.col性别.Visible = true;
            this.col性别.VisibleIndex = 2;
            // 
            // col身份证号
            // 
            this.col身份证号.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.col身份证号.AppearanceCell.Options.UseBackColor = true;
            this.col身份证号.AppearanceCell.Options.UseTextOptions = true;
            this.col身份证号.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col身份证号.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col身份证号.AppearanceHeader.Options.UseFont = true;
            this.col身份证号.AppearanceHeader.Options.UseTextOptions = true;
            this.col身份证号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col身份证号.Caption = "身份证号";
            this.col身份证号.FieldName = "身份证号";
            this.col身份证号.Name = "col身份证号";
            this.col身份证号.OptionsColumn.AllowEdit = false;
            this.col身份证号.OptionsColumn.ReadOnly = true;
            this.col身份证号.Visible = true;
            this.col身份证号.VisibleIndex = 3;
            // 
            // col所属机构
            // 
            this.col所属机构.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.col所属机构.AppearanceCell.Options.UseBackColor = true;
            this.col所属机构.AppearanceCell.Options.UseTextOptions = true;
            this.col所属机构.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col所属机构.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col所属机构.AppearanceHeader.Options.UseFont = true;
            this.col所属机构.AppearanceHeader.Options.UseTextOptions = true;
            this.col所属机构.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col所属机构.Caption = "当前所属机构";
            this.col所属机构.FieldName = "所属机构";
            this.col所属机构.Name = "col所属机构";
            this.col所属机构.OptionsColumn.AllowEdit = false;
            this.col所属机构.OptionsColumn.ReadOnly = true;
            this.col所属机构.Visible = true;
            this.col所属机构.VisibleIndex = 4;
            this.col所属机构.Width = 93;
            // 
            // col转入机构
            // 
            this.col转入机构.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col转入机构.AppearanceHeader.Options.UseFont = true;
            this.col转入机构.AppearanceHeader.Options.UseTextOptions = true;
            this.col转入机构.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col转入机构.Caption = "转入机构";
            this.col转入机构.FieldName = "转入机构";
            this.col转入机构.Name = "col转入机构";
            this.col转入机构.OptionsColumn.ReadOnly = true;
            this.col转入机构.Visible = true;
            this.col转入机构.VisibleIndex = 5;
            // 
            // col转档原因
            // 
            this.col转档原因.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col转档原因.AppearanceHeader.Options.UseFont = true;
            this.col转档原因.AppearanceHeader.Options.UseTextOptions = true;
            this.col转档原因.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col转档原因.Caption = "转档原因";
            this.col转档原因.FieldName = "备注";
            this.col转档原因.Name = "col转档原因";
            this.col转档原因.OptionsColumn.ReadOnly = true;
            this.col转档原因.Visible = true;
            this.col转档原因.VisibleIndex = 6;
            // 
            // col所属机构审核人
            // 
            this.col所属机构审核人.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col所属机构审核人.AppearanceHeader.Options.UseFont = true;
            this.col所属机构审核人.AppearanceHeader.Options.UseTextOptions = true;
            this.col所属机构审核人.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col所属机构审核人.Caption = "所属机构审核人";
            this.col所属机构审核人.FieldName = "所属机构审核人";
            this.col所属机构审核人.Name = "col所属机构审核人";
            this.col所属机构审核人.OptionsColumn.ReadOnly = true;
            this.col所属机构审核人.Visible = true;
            this.col所属机构审核人.VisibleIndex = 7;
            this.col所属机构审核人.Width = 100;
            // 
            // col转出时间
            // 
            this.col转出时间.AppearanceCell.Options.UseTextOptions = true;
            this.col转出时间.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col转出时间.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col转出时间.AppearanceHeader.Options.UseFont = true;
            this.col转出时间.AppearanceHeader.Options.UseTextOptions = true;
            this.col转出时间.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col转出时间.Caption = "转出时间";
            this.col转出时间.DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.col转出时间.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.col转出时间.FieldName = "HAPPENTIME";
            this.col转出时间.Name = "col转出时间";
            this.col转出时间.OptionsColumn.AllowEdit = false;
            this.col转出时间.OptionsColumn.ReadOnly = true;
            this.col转出时间.Visible = true;
            this.col转出时间.VisibleIndex = 8;
            // 
            // col转档人
            // 
            this.col转档人.AppearanceCell.Options.UseTextOptions = true;
            this.col转档人.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col转档人.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col转档人.AppearanceHeader.Options.UseFont = true;
            this.col转档人.AppearanceHeader.Options.UseTextOptions = true;
            this.col转档人.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col转档人.Caption = "转档人";
            this.col转档人.FieldName = "所属机构审核人";
            this.col转档人.Name = "col转档人";
            this.col转档人.OptionsColumn.AllowEdit = false;
            this.col转档人.OptionsColumn.ReadOnly = true;
            this.col转档人.Visible = true;
            this.col转档人.VisibleIndex = 9;
            // 
            // groupControl2
            // 
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.xtraTabControl1);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 93);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1108, 437);
            this.groupControl2.TabIndex = 6;
            this.groupControl2.Text = "查询结果列表";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(2, 22);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.tab驳回申请档案;
            this.xtraTabControl1.Size = new System.Drawing.Size(1104, 413);
            this.xtraTabControl1.TabIndex = 10;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tab已转出档案,
            this.tab待转入档案,
            this.tab已转入档案,
            this.tab待转出档案,
            this.tab驳回申请档案});
            // 
            // tab驳回申请档案
            // 
            this.tab驳回申请档案.Controls.Add(this.gc驳回申请档案);
            this.tab驳回申请档案.Controls.Add(this.pager驳回申请档案);
            this.tab驳回申请档案.Name = "tab驳回申请档案";
            this.tab驳回申请档案.Size = new System.Drawing.Size(1098, 384);
            this.tab驳回申请档案.Text = "驳回申请档案";
            // 
            // gc驳回申请档案
            // 
            this.gc驳回申请档案.AllowBandedGridColumnSort = false;
            this.gc驳回申请档案.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc驳回申请档案.IsBestFitColumns = true;
            this.gc驳回申请档案.Location = new System.Drawing.Point(0, 0);
            this.gc驳回申请档案.MainView = this.gv驳回申请档案;
            this.gc驳回申请档案.Name = "gc驳回申请档案";
            this.gc驳回申请档案.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit5});
            this.gc驳回申请档案.ShowContextMenu = false;
            this.gc驳回申请档案.Size = new System.Drawing.Size(1098, 354);
            this.gc驳回申请档案.StrWhere = "";
            this.gc驳回申请档案.TabIndex = 9;
            this.gc驳回申请档案.UseCheckBox = false;
            this.gc驳回申请档案.View = "";
            this.gc驳回申请档案.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv驳回申请档案});
            // 
            // gv驳回申请档案
            // 
            this.gv驳回申请档案.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.DetailTip.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.Empty.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.EvenRow.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.FilterPanel.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.FixedLine.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.FocusedCell.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.FocusedRow.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gv驳回申请档案.Appearance.FooterPanel.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.GroupButton.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.GroupFooter.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.GroupPanel.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.GroupRow.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.HeaderPanel.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.HorzLine.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.OddRow.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.Preview.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.Row.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.RowSeparator.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.SelectedRow.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.TopNewRow.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.VertLine.Options.UseFont = true;
            this.gv驳回申请档案.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gv驳回申请档案.Appearance.ViewCaption.Options.UseFont = true;
            this.gv驳回申请档案.ColumnPanelRowHeight = 30;
            this.gv驳回申请档案.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn42,
            this.gridColumn43,
            this.gridColumn44,
            this.gridColumn45,
            this.gridColumn46,
            this.gridColumn47,
            this.gridColumn48,
            this.gridColumn50,
            this.gridColumn51,
            this.gridColumn53,
            this.gridColumn49});
            this.gv驳回申请档案.GridControl = this.gc驳回申请档案;
            this.gv驳回申请档案.GroupPanelText = "DragColumn";
            this.gv驳回申请档案.Name = "gv驳回申请档案";
            this.gv驳回申请档案.OptionsView.ColumnAutoWidth = false;
            this.gv驳回申请档案.OptionsView.EnableAppearanceEvenRow = true;
            this.gv驳回申请档案.OptionsView.EnableAppearanceOddRow = true;
            this.gv驳回申请档案.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn42
            // 
            this.gridColumn42.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn42.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn42.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn42.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn42.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn42.AppearanceHeader.Options.UseFont = true;
            this.gridColumn42.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn42.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn42.Caption = "档案号";
            this.gridColumn42.ColumnEdit = this.repositoryItemHyperLinkEdit5;
            this.gridColumn42.FieldName = "个人档案编号";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.ReadOnly = true;
            this.gridColumn42.Visible = true;
            this.gridColumn42.VisibleIndex = 0;
            this.gridColumn42.Width = 127;
            // 
            // repositoryItemHyperLinkEdit5
            // 
            this.repositoryItemHyperLinkEdit5.AutoHeight = false;
            this.repositoryItemHyperLinkEdit5.Name = "repositoryItemHyperLinkEdit5";
            this.repositoryItemHyperLinkEdit5.Click += new System.EventHandler(this.link个人档案编号_Click);
            // 
            // gridColumn43
            // 
            this.gridColumn43.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn43.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn43.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn43.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn43.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn43.AppearanceHeader.Options.UseFont = true;
            this.gridColumn43.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn43.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn43.Caption = "姓名";
            this.gridColumn43.FieldName = "姓名";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.OptionsColumn.AllowEdit = false;
            this.gridColumn43.OptionsColumn.ReadOnly = true;
            this.gridColumn43.Visible = true;
            this.gridColumn43.VisibleIndex = 1;
            // 
            // gridColumn44
            // 
            this.gridColumn44.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn44.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn44.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn44.AppearanceHeader.Options.UseFont = true;
            this.gridColumn44.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn44.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn44.Caption = "性别";
            this.gridColumn44.FieldName = "性别";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.OptionsColumn.AllowEdit = false;
            this.gridColumn44.OptionsColumn.ReadOnly = true;
            this.gridColumn44.Visible = true;
            this.gridColumn44.VisibleIndex = 2;
            // 
            // gridColumn45
            // 
            this.gridColumn45.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn45.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn45.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn45.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn45.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn45.AppearanceHeader.Options.UseFont = true;
            this.gridColumn45.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn45.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn45.Caption = "身份证号";
            this.gridColumn45.FieldName = "身份证号";
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.OptionsColumn.AllowEdit = false;
            this.gridColumn45.OptionsColumn.ReadOnly = true;
            this.gridColumn45.Visible = true;
            this.gridColumn45.VisibleIndex = 3;
            // 
            // gridColumn46
            // 
            this.gridColumn46.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn46.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn46.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn46.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn46.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn46.AppearanceHeader.Options.UseFont = true;
            this.gridColumn46.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn46.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn46.Caption = "当前所属机构";
            this.gridColumn46.FieldName = "所属机构";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.AllowEdit = false;
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            this.gridColumn46.Visible = true;
            this.gridColumn46.VisibleIndex = 4;
            this.gridColumn46.Width = 93;
            // 
            // gridColumn47
            // 
            this.gridColumn47.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn47.AppearanceHeader.Options.UseFont = true;
            this.gridColumn47.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn47.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn47.Caption = "转出机构";
            this.gridColumn47.FieldName = "转出机构";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.OptionsColumn.ReadOnly = true;
            this.gridColumn47.Visible = true;
            this.gridColumn47.VisibleIndex = 5;
            // 
            // gridColumn48
            // 
            this.gridColumn48.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn48.AppearanceHeader.Options.UseFont = true;
            this.gridColumn48.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn48.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn48.Caption = "申请原因";
            this.gridColumn48.FieldName = "备注";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.ReadOnly = true;
            this.gridColumn48.Visible = true;
            this.gridColumn48.VisibleIndex = 6;
            // 
            // gridColumn50
            // 
            this.gridColumn50.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn50.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn50.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn50.AppearanceHeader.Options.UseFont = true;
            this.gridColumn50.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn50.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn50.Caption = "申请时间";
            this.gridColumn50.DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.gridColumn50.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn50.FieldName = "HAPPENTIME";
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.OptionsColumn.AllowEdit = false;
            this.gridColumn50.OptionsColumn.ReadOnly = true;
            this.gridColumn50.Visible = true;
            this.gridColumn50.VisibleIndex = 8;
            // 
            // gridColumn51
            // 
            this.gridColumn51.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn51.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn51.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn51.AppearanceHeader.Options.UseFont = true;
            this.gridColumn51.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn51.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn51.Caption = "所属机构审核人";
            this.gridColumn51.FieldName = "所属机构审核人";
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.OptionsColumn.AllowEdit = false;
            this.gridColumn51.OptionsColumn.ReadOnly = true;
            this.gridColumn51.Visible = true;
            this.gridColumn51.VisibleIndex = 10;
            // 
            // gridColumn53
            // 
            this.gridColumn53.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn53.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn53.Caption = "卫生局审核人";
            this.gridColumn53.FieldName = "卫生局审核人";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.OptionsColumn.ReadOnly = true;
            this.gridColumn53.Visible = true;
            this.gridColumn53.VisibleIndex = 9;
            // 
            // gridColumn49
            // 
            this.gridColumn49.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn49.AppearanceHeader.Options.UseFont = true;
            this.gridColumn49.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn49.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn49.Caption = "拒绝原因";
            this.gridColumn49.FieldName = "拒绝原因";
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.OptionsColumn.ReadOnly = true;
            this.gridColumn49.Visible = true;
            this.gridColumn49.VisibleIndex = 7;
            this.gridColumn49.Width = 100;
            // 
            // pager驳回申请档案
            // 
            this.pager驳回申请档案.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pager驳回申请档案.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pager驳回申请档案.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pager驳回申请档案.JumpText = "跳转";
            this.pager驳回申请档案.Location = new System.Drawing.Point(0, 354);
            this.pager驳回申请档案.Name = "pager驳回申请档案";
            this.pager驳回申请档案.PageIndex = 1;
            this.pager驳回申请档案.PageSize = 15;
            this.pager驳回申请档案.paramters = null;
            this.pager驳回申请档案.RecordCount = 0;
            this.pager驳回申请档案.Size = new System.Drawing.Size(1098, 30);
            this.pager驳回申请档案.SqlQuery = null;
            this.pager驳回申请档案.TabIndex = 10;
            this.pager驳回申请档案.OnPageChanged += new System.EventHandler(this.pager驳回申请档案_OnPageChanged);
            // 
            // tab已转出档案
            // 
            this.tab已转出档案.Controls.Add(this.gc已转出档案);
            this.tab已转出档案.Controls.Add(this.pager已转出档案);
            this.tab已转出档案.Name = "tab已转出档案";
            this.tab已转出档案.Size = new System.Drawing.Size(1098, 384);
            this.tab已转出档案.Text = "已转出档案";
            // 
            // pager已转出档案
            // 
            this.pager已转出档案.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pager已转出档案.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pager已转出档案.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pager已转出档案.JumpText = "跳转";
            this.pager已转出档案.Location = new System.Drawing.Point(0, 354);
            this.pager已转出档案.Name = "pager已转出档案";
            this.pager已转出档案.PageIndex = 1;
            this.pager已转出档案.PageSize = 15;
            this.pager已转出档案.paramters = null;
            this.pager已转出档案.RecordCount = 0;
            this.pager已转出档案.Size = new System.Drawing.Size(1098, 30);
            this.pager已转出档案.SqlQuery = null;
            this.pager已转出档案.TabIndex = 6;
            this.pager已转出档案.OnPageChanged += new System.EventHandler(this.pager已转出档案_OnPageChanged);
            // 
            // tab待转入档案
            // 
            this.tab待转入档案.Controls.Add(this.gc待转入档案);
            this.tab待转入档案.Controls.Add(this.pager待转入档案);
            this.tab待转入档案.Name = "tab待转入档案";
            this.tab待转入档案.Size = new System.Drawing.Size(1098, 384);
            this.tab待转入档案.Text = "待转入档案";
            // 
            // gc待转入档案
            // 
            this.gc待转入档案.AllowBandedGridColumnSort = false;
            this.gc待转入档案.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc待转入档案.IsBestFitColumns = true;
            this.gc待转入档案.Location = new System.Drawing.Point(0, 0);
            this.gc待转入档案.MainView = this.gv待转入档案;
            this.gc待转入档案.Name = "gc待转入档案";
            this.gc待转入档案.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit3,
            this.btn撤销});
            this.gc待转入档案.ShowContextMenu = false;
            this.gc待转入档案.Size = new System.Drawing.Size(1098, 354);
            this.gc待转入档案.StrWhere = "";
            this.gc待转入档案.TabIndex = 7;
            this.gc待转入档案.UseCheckBox = false;
            this.gc待转入档案.View = "";
            this.gc待转入档案.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv待转入档案});
            // 
            // gv待转入档案
            // 
            this.gv待转入档案.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gv待转入档案.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gv待转入档案.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gv待转入档案.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.DetailTip.Options.UseFont = true;
            this.gv待转入档案.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.Empty.Options.UseFont = true;
            this.gv待转入档案.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.EvenRow.Options.UseFont = true;
            this.gv待转入档案.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gv待转入档案.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.FilterPanel.Options.UseFont = true;
            this.gv待转入档案.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.FixedLine.Options.UseFont = true;
            this.gv待转入档案.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.FocusedCell.Options.UseFont = true;
            this.gv待转入档案.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.FocusedRow.Options.UseFont = true;
            this.gv待转入档案.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gv待转入档案.Appearance.FooterPanel.Options.UseFont = true;
            this.gv待转入档案.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.GroupButton.Options.UseFont = true;
            this.gv待转入档案.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.GroupFooter.Options.UseFont = true;
            this.gv待转入档案.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.GroupPanel.Options.UseFont = true;
            this.gv待转入档案.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.GroupRow.Options.UseFont = true;
            this.gv待转入档案.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.HeaderPanel.Options.UseFont = true;
            this.gv待转入档案.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gv待转入档案.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.HorzLine.Options.UseFont = true;
            this.gv待转入档案.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.OddRow.Options.UseFont = true;
            this.gv待转入档案.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.Preview.Options.UseFont = true;
            this.gv待转入档案.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.Row.Options.UseFont = true;
            this.gv待转入档案.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.RowSeparator.Options.UseFont = true;
            this.gv待转入档案.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.SelectedRow.Options.UseFont = true;
            this.gv待转入档案.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.TopNewRow.Options.UseFont = true;
            this.gv待转入档案.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.VertLine.Options.UseFont = true;
            this.gv待转入档案.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转入档案.Appearance.ViewCaption.Options.UseFont = true;
            this.gv待转入档案.ColumnPanelRowHeight = 30;
            this.gv待转入档案.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn28,
            this.col操作});
            this.gv待转入档案.GridControl = this.gc待转入档案;
            this.gv待转入档案.GroupPanelText = "DragColumn";
            this.gv待转入档案.Name = "gv待转入档案";
            this.gv待转入档案.OptionsView.ColumnAutoWidth = false;
            this.gv待转入档案.OptionsView.EnableAppearanceEvenRow = true;
            this.gv待转入档案.OptionsView.EnableAppearanceOddRow = true;
            this.gv待转入档案.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn21
            // 
            this.gridColumn21.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn21.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn21.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn21.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn21.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn21.AppearanceHeader.Options.UseFont = true;
            this.gridColumn21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn21.Caption = "档案号";
            this.gridColumn21.ColumnEdit = this.repositoryItemHyperLinkEdit3;
            this.gridColumn21.FieldName = "个人档案编号";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 0;
            this.gridColumn21.Width = 127;
            // 
            // repositoryItemHyperLinkEdit3
            // 
            this.repositoryItemHyperLinkEdit3.AutoHeight = false;
            this.repositoryItemHyperLinkEdit3.Name = "repositoryItemHyperLinkEdit3";
            this.repositoryItemHyperLinkEdit3.Click += new System.EventHandler(this.link个人档案编号_Click);
            // 
            // gridColumn22
            // 
            this.gridColumn22.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn22.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn22.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn22.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn22.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn22.AppearanceHeader.Options.UseFont = true;
            this.gridColumn22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn22.Caption = "姓名";
            this.gridColumn22.FieldName = "姓名";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 1;
            // 
            // gridColumn23
            // 
            this.gridColumn23.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn23.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn23.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn23.AppearanceHeader.Options.UseFont = true;
            this.gridColumn23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn23.Caption = "性别";
            this.gridColumn23.FieldName = "性别";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 2;
            // 
            // gridColumn24
            // 
            this.gridColumn24.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn24.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn24.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn24.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn24.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn24.AppearanceHeader.Options.UseFont = true;
            this.gridColumn24.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn24.Caption = "身份证号";
            this.gridColumn24.FieldName = "身份证号";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 3;
            // 
            // gridColumn25
            // 
            this.gridColumn25.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn25.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn25.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn25.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn25.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn25.AppearanceHeader.Options.UseFont = true;
            this.gridColumn25.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn25.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn25.Caption = "当前所属机构";
            this.gridColumn25.FieldName = "所属机构";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 4;
            this.gridColumn25.Width = 93;
            // 
            // gridColumn26
            // 
            this.gridColumn26.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn26.AppearanceHeader.Options.UseFont = true;
            this.gridColumn26.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn26.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn26.Caption = "待转出机构 ";
            this.gridColumn26.FieldName = "转出机构";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 5;
            // 
            // gridColumn27
            // 
            this.gridColumn27.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn27.AppearanceHeader.Options.UseFont = true;
            this.gridColumn27.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn27.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn27.Caption = "转档原因";
            this.gridColumn27.FieldName = "备注";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 6;
            // 
            // gridColumn29
            // 
            this.gridColumn29.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn29.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn29.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn29.AppearanceHeader.Options.UseFont = true;
            this.gridColumn29.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn29.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn29.Caption = "申请时间";
            this.gridColumn29.DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.gridColumn29.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn29.FieldName = "HAPPENTIME";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 8;
            // 
            // gridColumn30
            // 
            this.gridColumn30.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn30.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn30.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn30.AppearanceHeader.Options.UseFont = true;
            this.gridColumn30.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn30.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn30.Caption = "申请人";
            this.gridColumn30.FieldName = "创建人";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 9;
            // 
            // gridColumn28
            // 
            this.gridColumn28.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn28.AppearanceHeader.Options.UseFont = true;
            this.gridColumn28.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn28.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn28.Caption = "状 态";
            this.gridColumn28.FieldName = "类型";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 7;
            this.gridColumn28.Width = 100;
            // 
            // col操作
            // 
            this.col操作.AppearanceHeader.Options.UseTextOptions = true;
            this.col操作.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col操作.Caption = "操 作";
            this.col操作.ColumnEdit = this.btn撤销;
            this.col操作.Name = "col操作";
            this.col操作.Visible = true;
            this.col操作.VisibleIndex = 10;
            // 
            // btn撤销
            // 
            this.btn撤销.AutoHeight = false;
            this.btn撤销.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "撤销", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.btn撤销.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btn撤销.Name = "btn撤销";
            this.btn撤销.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btn撤销.Click += new System.EventHandler(this.btn撤销_Click);
            // 
            // pager待转入档案
            // 
            this.pager待转入档案.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pager待转入档案.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pager待转入档案.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pager待转入档案.JumpText = "跳转";
            this.pager待转入档案.Location = new System.Drawing.Point(0, 354);
            this.pager待转入档案.Name = "pager待转入档案";
            this.pager待转入档案.PageIndex = 1;
            this.pager待转入档案.PageSize = 15;
            this.pager待转入档案.paramters = null;
            this.pager待转入档案.RecordCount = 0;
            this.pager待转入档案.Size = new System.Drawing.Size(1098, 30);
            this.pager待转入档案.SqlQuery = null;
            this.pager待转入档案.TabIndex = 8;
            this.pager待转入档案.OnPageChanged += new System.EventHandler(this.pager待转入档案_OnPageChanged);
            // 
            // tab已转入档案
            // 
            this.tab已转入档案.Controls.Add(this.gc已转入档案);
            this.tab已转入档案.Controls.Add(this.pager已转入档案);
            this.tab已转入档案.Name = "tab已转入档案";
            this.tab已转入档案.Size = new System.Drawing.Size(1098, 384);
            this.tab已转入档案.Text = "已转入档案";
            // 
            // gc已转入档案
            // 
            this.gc已转入档案.AllowBandedGridColumnSort = false;
            this.gc已转入档案.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc已转入档案.IsBestFitColumns = true;
            this.gc已转入档案.Location = new System.Drawing.Point(0, 0);
            this.gc已转入档案.MainView = this.gv已转入档案;
            this.gc已转入档案.Name = "gc已转入档案";
            this.gc已转入档案.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit2});
            this.gc已转入档案.ShowContextMenu = false;
            this.gc已转入档案.Size = new System.Drawing.Size(1098, 354);
            this.gc已转入档案.StrWhere = "";
            this.gc已转入档案.TabIndex = 6;
            this.gc已转入档案.UseCheckBox = false;
            this.gc已转入档案.View = "";
            this.gc已转入档案.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv已转入档案});
            // 
            // gv已转入档案
            // 
            this.gv已转入档案.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gv已转入档案.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gv已转入档案.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gv已转入档案.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.DetailTip.Options.UseFont = true;
            this.gv已转入档案.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.Empty.Options.UseFont = true;
            this.gv已转入档案.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.EvenRow.Options.UseFont = true;
            this.gv已转入档案.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gv已转入档案.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.FilterPanel.Options.UseFont = true;
            this.gv已转入档案.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.FixedLine.Options.UseFont = true;
            this.gv已转入档案.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.FocusedCell.Options.UseFont = true;
            this.gv已转入档案.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.FocusedRow.Options.UseFont = true;
            this.gv已转入档案.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gv已转入档案.Appearance.FooterPanel.Options.UseFont = true;
            this.gv已转入档案.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.GroupButton.Options.UseFont = true;
            this.gv已转入档案.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.GroupFooter.Options.UseFont = true;
            this.gv已转入档案.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.GroupPanel.Options.UseFont = true;
            this.gv已转入档案.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.GroupRow.Options.UseFont = true;
            this.gv已转入档案.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.HeaderPanel.Options.UseFont = true;
            this.gv已转入档案.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gv已转入档案.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.HorzLine.Options.UseFont = true;
            this.gv已转入档案.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.OddRow.Options.UseFont = true;
            this.gv已转入档案.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.Preview.Options.UseFont = true;
            this.gv已转入档案.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.Row.Options.UseFont = true;
            this.gv已转入档案.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.RowSeparator.Options.UseFont = true;
            this.gv已转入档案.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.SelectedRow.Options.UseFont = true;
            this.gv已转入档案.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.TopNewRow.Options.UseFont = true;
            this.gv已转入档案.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.VertLine.Options.UseFont = true;
            this.gv已转入档案.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gv已转入档案.Appearance.ViewCaption.Options.UseFont = true;
            this.gv已转入档案.ColumnPanelRowHeight = 30;
            this.gv已转入档案.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20});
            this.gv已转入档案.GridControl = this.gc已转入档案;
            this.gv已转入档案.GroupPanelText = "DragColumn";
            this.gv已转入档案.Name = "gv已转入档案";
            this.gv已转入档案.OptionsView.ColumnAutoWidth = false;
            this.gv已转入档案.OptionsView.EnableAppearanceEvenRow = true;
            this.gv已转入档案.OptionsView.EnableAppearanceOddRow = true;
            this.gv已转入档案.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn11.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn11.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn11.AppearanceHeader.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "档案号";
            this.gridColumn11.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.gridColumn11.FieldName = "个人档案编号";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 0;
            this.gridColumn11.Width = 127;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.Click += new System.EventHandler(this.link个人档案编号_Click);
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn12.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn12.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn12.AppearanceHeader.Options.UseFont = true;
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.Caption = "姓名";
            this.gridColumn12.FieldName = "姓名";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 1;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn13.AppearanceHeader.Options.UseFont = true;
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.Caption = "性别";
            this.gridColumn13.FieldName = "性别";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 2;
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn14.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn14.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn14.AppearanceHeader.Options.UseFont = true;
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.Caption = "身份证号";
            this.gridColumn14.FieldName = "身份证号";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 3;
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn15.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn15.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn15.AppearanceHeader.Options.UseFont = true;
            this.gridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.Caption = "当前所属机构";
            this.gridColumn15.FieldName = "所属机构";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 4;
            this.gridColumn15.Width = 93;
            // 
            // gridColumn16
            // 
            this.gridColumn16.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn16.AppearanceHeader.Options.UseFont = true;
            this.gridColumn16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn16.Caption = "转出机构";
            this.gridColumn16.FieldName = "转出机构";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 5;
            // 
            // gridColumn17
            // 
            this.gridColumn17.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn17.AppearanceHeader.Options.UseFont = true;
            this.gridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.Caption = "转档原因";
            this.gridColumn17.FieldName = "备注";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 6;
            // 
            // gridColumn18
            // 
            this.gridColumn18.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn18.AppearanceHeader.Options.UseFont = true;
            this.gridColumn18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn18.Caption = "所属机构审核人";
            this.gridColumn18.FieldName = "所属机构审核人";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 7;
            this.gridColumn18.Width = 100;
            // 
            // gridColumn19
            // 
            this.gridColumn19.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn19.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn19.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn19.AppearanceHeader.Options.UseFont = true;
            this.gridColumn19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn19.Caption = "转入时间";
            this.gridColumn19.DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.gridColumn19.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn19.FieldName = "HAPPENTIME";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 8;
            // 
            // gridColumn20
            // 
            this.gridColumn20.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn20.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn20.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn20.AppearanceHeader.Options.UseFont = true;
            this.gridColumn20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn20.Caption = "转档人";
            this.gridColumn20.FieldName = "所属机构审核人";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 9;
            // 
            // pager已转入档案
            // 
            this.pager已转入档案.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pager已转入档案.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pager已转入档案.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pager已转入档案.JumpText = "跳转";
            this.pager已转入档案.Location = new System.Drawing.Point(0, 354);
            this.pager已转入档案.Name = "pager已转入档案";
            this.pager已转入档案.PageIndex = 1;
            this.pager已转入档案.PageSize = 15;
            this.pager已转入档案.paramters = null;
            this.pager已转入档案.RecordCount = 0;
            this.pager已转入档案.Size = new System.Drawing.Size(1098, 30);
            this.pager已转入档案.SqlQuery = null;
            this.pager已转入档案.TabIndex = 7;
            this.pager已转入档案.OnPageChanged += new System.EventHandler(this.pager已转入档案_OnPageChanged);
            // 
            // tab待转出档案
            // 
            this.tab待转出档案.Controls.Add(this.gc待转出档案);
            this.tab待转出档案.Controls.Add(this.pager待转出档案);
            this.tab待转出档案.Name = "tab待转出档案";
            this.tab待转出档案.Size = new System.Drawing.Size(1098, 384);
            this.tab待转出档案.Text = "待转出档案";
            // 
            // gc待转出档案
            // 
            this.gc待转出档案.AllowBandedGridColumnSort = true;
            this.gc待转出档案.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc待转出档案.IsBestFitColumns = true;
            this.gc待转出档案.Location = new System.Drawing.Point(0, 0);
            this.gc待转出档案.MainView = this.gv待转出档案;
            this.gc待转出档案.Name = "gc待转出档案";
            this.gc待转出档案.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit4,
            this.btn驳回,
            this.btn同意});
            this.gc待转出档案.ShowContextMenu = false;
            this.gc待转出档案.Size = new System.Drawing.Size(1098, 354);
            this.gc待转出档案.StrWhere = "";
            this.gc待转出档案.TabIndex = 8;
            this.gc待转出档案.UseCheckBox = false;
            this.gc待转出档案.View = "";
            this.gc待转出档案.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv待转出档案});
            // 
            // gv待转出档案
            // 
            this.gv待转出档案.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gv待转出档案.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gv待转出档案.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gv待转出档案.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.DetailTip.Options.UseFont = true;
            this.gv待转出档案.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.Empty.Options.UseFont = true;
            this.gv待转出档案.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.EvenRow.Options.UseFont = true;
            this.gv待转出档案.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gv待转出档案.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.FilterPanel.Options.UseFont = true;
            this.gv待转出档案.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.FixedLine.Options.UseFont = true;
            this.gv待转出档案.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.FocusedCell.Options.UseFont = true;
            this.gv待转出档案.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.FocusedRow.Options.UseFont = true;
            this.gv待转出档案.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gv待转出档案.Appearance.FooterPanel.Options.UseFont = true;
            this.gv待转出档案.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.GroupButton.Options.UseFont = true;
            this.gv待转出档案.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.GroupFooter.Options.UseFont = true;
            this.gv待转出档案.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.GroupPanel.Options.UseFont = true;
            this.gv待转出档案.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.GroupRow.Options.UseFont = true;
            this.gv待转出档案.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.HeaderPanel.Options.UseFont = true;
            this.gv待转出档案.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gv待转出档案.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.HorzLine.Options.UseFont = true;
            this.gv待转出档案.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.OddRow.Options.UseFont = true;
            this.gv待转出档案.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.Preview.Options.UseFont = true;
            this.gv待转出档案.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.Row.Options.UseFont = true;
            this.gv待转出档案.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.RowSeparator.Options.UseFont = true;
            this.gv待转出档案.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.SelectedRow.Options.UseFont = true;
            this.gv待转出档案.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.TopNewRow.Options.UseFont = true;
            this.gv待转出档案.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.VertLine.Options.UseFont = true;
            this.gv待转出档案.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gv待转出档案.Appearance.ViewCaption.Options.UseFont = true;
            this.gv待转出档案.ColumnPanelRowHeight = 30;
            this.gv待转出档案.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn52,
            this.gridColumn41});
            this.gv待转出档案.GridControl = this.gc待转出档案;
            this.gv待转出档案.GroupPanelText = "DragColumn";
            this.gv待转出档案.Name = "gv待转出档案";
            this.gv待转出档案.OptionsView.ColumnAutoWidth = false;
            this.gv待转出档案.OptionsView.EnableAppearanceEvenRow = true;
            this.gv待转出档案.OptionsView.EnableAppearanceOddRow = true;
            this.gv待转出档案.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn31
            // 
            this.gridColumn31.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn31.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn31.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn31.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn31.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn31.AppearanceHeader.Options.UseFont = true;
            this.gridColumn31.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn31.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn31.Caption = "档案号";
            this.gridColumn31.ColumnEdit = this.repositoryItemHyperLinkEdit4;
            this.gridColumn31.FieldName = "个人档案编号";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 0;
            this.gridColumn31.Width = 127;
            // 
            // repositoryItemHyperLinkEdit4
            // 
            this.repositoryItemHyperLinkEdit4.AutoHeight = false;
            this.repositoryItemHyperLinkEdit4.Name = "repositoryItemHyperLinkEdit4";
            this.repositoryItemHyperLinkEdit4.Click += new System.EventHandler(this.link个人档案编号_Click);
            // 
            // gridColumn32
            // 
            this.gridColumn32.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn32.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn32.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn32.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn32.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn32.AppearanceHeader.Options.UseFont = true;
            this.gridColumn32.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn32.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn32.Caption = "姓名";
            this.gridColumn32.FieldName = "姓名";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 1;
            // 
            // gridColumn33
            // 
            this.gridColumn33.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn33.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn33.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn33.AppearanceHeader.Options.UseFont = true;
            this.gridColumn33.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn33.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn33.Caption = "性别";
            this.gridColumn33.FieldName = "性别";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 2;
            // 
            // gridColumn34
            // 
            this.gridColumn34.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn34.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn34.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn34.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn34.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn34.AppearanceHeader.Options.UseFont = true;
            this.gridColumn34.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn34.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn34.Caption = "身份证号";
            this.gridColumn34.FieldName = "身份证号";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 3;
            // 
            // gridColumn35
            // 
            this.gridColumn35.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn35.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn35.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn35.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn35.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn35.AppearanceHeader.Options.UseFont = true;
            this.gridColumn35.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn35.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn35.Caption = "当前所属机构";
            this.gridColumn35.FieldName = "所属机构";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 4;
            this.gridColumn35.Width = 93;
            // 
            // gridColumn36
            // 
            this.gridColumn36.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn36.AppearanceHeader.Options.UseFont = true;
            this.gridColumn36.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn36.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn36.Caption = "待转入机构 ";
            this.gridColumn36.FieldName = "转入机构";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.Visible = true;
            this.gridColumn36.VisibleIndex = 5;
            this.gridColumn36.Width = 92;
            // 
            // gridColumn37
            // 
            this.gridColumn37.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn37.AppearanceHeader.Options.UseFont = true;
            this.gridColumn37.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn37.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn37.Caption = "转档原因";
            this.gridColumn37.FieldName = "备注";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 6;
            // 
            // gridColumn38
            // 
            this.gridColumn38.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn38.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn38.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn38.AppearanceHeader.Options.UseFont = true;
            this.gridColumn38.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn38.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn38.Caption = "申请时间";
            this.gridColumn38.DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.gridColumn38.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn38.FieldName = "HAPPENTIME";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 7;
            // 
            // gridColumn39
            // 
            this.gridColumn39.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn39.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn39.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn39.AppearanceHeader.Options.UseFont = true;
            this.gridColumn39.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn39.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn39.Caption = "申请人";
            this.gridColumn39.FieldName = "创建人";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.Visible = true;
            this.gridColumn39.VisibleIndex = 8;
            // 
            // gridColumn40
            // 
            this.gridColumn40.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn40.AppearanceHeader.Options.UseFont = true;
            this.gridColumn40.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn40.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn40.Caption = "状 态";
            this.gridColumn40.FieldName = "类型";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.Visible = true;
            this.gridColumn40.VisibleIndex = 9;
            this.gridColumn40.Width = 100;
            // 
            // gridColumn52
            // 
            this.gridColumn52.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn52.AppearanceHeader.Options.UseFont = true;
            this.gridColumn52.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn52.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn52.Caption = "同 意";
            this.gridColumn52.ColumnEdit = this.btn同意;
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.Visible = true;
            this.gridColumn52.VisibleIndex = 10;
            // 
            // btn同意
            // 
            this.btn同意.Appearance.ForeColor = System.Drawing.Color.White;
            this.btn同意.Appearance.Options.UseForeColor = true;
            this.btn同意.AutoHeight = false;
            this.btn同意.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "同意", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.btn同意.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btn同意.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.btn同意.Name = "btn同意";
            this.btn同意.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btn同意.Click += new System.EventHandler(this.btn同意_Click);
            // 
            // gridColumn41
            // 
            this.gridColumn41.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn41.AppearanceHeader.Options.UseFont = true;
            this.gridColumn41.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn41.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn41.Caption = "驳 回";
            this.gridColumn41.ColumnEdit = this.btn驳回;
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.Visible = true;
            this.gridColumn41.VisibleIndex = 11;
            // 
            // btn驳回
            // 
            this.btn驳回.Appearance.ForeColor = System.Drawing.Color.White;
            this.btn驳回.Appearance.Options.UseForeColor = true;
            this.btn驳回.AutoHeight = false;
            this.btn驳回.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "驳回", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", null, null, true)});
            this.btn驳回.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btn驳回.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.btn驳回.Name = "btn驳回";
            this.btn驳回.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btn驳回.Click += new System.EventHandler(this.btn驳回_Click);
            // 
            // pager待转出档案
            // 
            this.pager待转出档案.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pager待转出档案.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pager待转出档案.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pager待转出档案.JumpText = "跳转";
            this.pager待转出档案.Location = new System.Drawing.Point(0, 354);
            this.pager待转出档案.Name = "pager待转出档案";
            this.pager待转出档案.PageIndex = 1;
            this.pager待转出档案.PageSize = 15;
            this.pager待转出档案.paramters = null;
            this.pager待转出档案.RecordCount = 0;
            this.pager待转出档案.Size = new System.Drawing.Size(1098, 30);
            this.pager待转出档案.SqlQuery = null;
            this.pager待转出档案.TabIndex = 9;
            this.pager待转出档案.OnPageChanged += new System.EventHandler(this.pager待转出档案_OnPageChanged);
            // 
            // frm转档查询
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1114, 565);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl2);
            this.Name = "frm转档查询";
            this.Text = "转档查询";
            this.Load += new System.EventHandler(this.frm转档查询_Load);
            this.Controls.SetChildIndex(this.panelControl2, 0);
            this.Controls.SetChildIndex(this.panelControl3, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.Controls.SetChildIndex(this.gcNavigator, 0);
            this.Controls.SetChildIndex(this.pnlSummary, 0);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc已转出档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv已转出档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link个人档案编号)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.tab驳回申请档案.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc驳回申请档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv驳回申请档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit5)).EndInit();
            this.tab已转出档案.ResumeLayout(false);
            this.tab待转入档案.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc待转入档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv待转入档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn撤销)).EndInit();
            this.tab已转入档案.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc已转入档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv已转入档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            this.tab待转出档案.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc待转出档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv待转出档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn同意)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn驳回)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DataGridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.TreeListLookUpEdit cbo机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn已转入档案;
        private DevExpress.XtraEditors.SimpleButton btn已转出档案;
        private DevExpress.XtraEditors.SimpleButton btn待转入档案;
        private DevExpress.XtraEditors.SimpleButton btn待转出档案;
        private DevExpress.XtraEditors.SimpleButton btn驳回申请档案;
        private DataGridControl gc已转出档案;
        private DevExpress.XtraGrid.Views.Grid.GridView gv已转出档案;
        private DevExpress.XtraGrid.Columns.GridColumn col个人档案号码;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link个人档案编号;
        private DevExpress.XtraGrid.Columns.GridColumn col姓名;
        private DevExpress.XtraGrid.Columns.GridColumn col性别;
        private DevExpress.XtraGrid.Columns.GridColumn col身份证号;
        private DevExpress.XtraGrid.Columns.GridColumn col所属机构;
        private DevExpress.XtraGrid.Columns.GridColumn col转出时间;
        private DevExpress.XtraGrid.Columns.GridColumn col转档人;
        private DevExpress.XtraGrid.Columns.GridColumn col转入机构;
        private DevExpress.XtraGrid.Columns.GridColumn col转档原因;
        private DevExpress.XtraGrid.Columns.GridColumn col所属机构审核人;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DataGridControl gc已转入档案;
        private DevExpress.XtraGrid.Views.Grid.GridView gv已转入档案;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DataGridControl gc待转入档案;
        private DevExpress.XtraGrid.Views.Grid.GridView gv待转入档案;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn col操作;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btn撤销;
        private DataGridControl gc待转出档案;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btn驳回;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit4;
        private DataGridControl gc驳回申请档案;
        private DevExpress.XtraGrid.Views.Grid.GridView gv驳回申请档案;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btn同意;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage tab驳回申请档案;
        private DevExpress.XtraTab.XtraTabPage tab已转出档案;
        private DevExpress.XtraTab.XtraTabPage tab待转入档案;
        private DevExpress.XtraTab.XtraTabPage tab已转入档案;
        private DevExpress.XtraTab.XtraTabPage tab待转出档案;
        private TActionProject.PagerControl pager已转出档案;
        private TActionProject.PagerControl pager驳回申请档案;
        private TActionProject.PagerControl pager待转入档案;
        private TActionProject.PagerControl pager已转入档案;
        private TActionProject.PagerControl pager待转出档案;
        private DevExpress.XtraGrid.Views.Grid.GridView gv待转出档案;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;

    }
}