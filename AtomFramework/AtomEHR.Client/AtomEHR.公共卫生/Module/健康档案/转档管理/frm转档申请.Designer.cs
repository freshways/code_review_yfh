﻿using AtomEHR.Library.UserControls;
namespace AtomEHR.公共卫生.Module.健康档案.转档管理
{
    partial class frm转档申请
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm转档申请));
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btn区域档案查询 = new DevExpress.XtraEditors.SimpleButton();
            this.cbo村委会 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo街道 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt档案号 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gc个人健康档案 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv个人健康档案 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col个人档案号码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.col姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col性别 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col出生日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col身份证号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col居住地址 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col本人电话 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col联系人电话 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col所属机构 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col创建人 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col创建时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col档案状态 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn档案转入申请 = new DevExpress.XtraEditors.SimpleButton();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo村委会.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo街道.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc个人健康档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv个人健康档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.groupControl1);
            this.tpSummary.Controls.Add(this.panelControl1);
            this.tpSummary.Size = new System.Drawing.Size(1108, 530);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 29);
            this.pnlSummary.Size = new System.Drawing.Size(1114, 536);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Size = new System.Drawing.Size(1114, 536);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(932, 589);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Location = new System.Drawing.Point(0, 3);
            this.gcNavigator.Size = new System.Drawing.Size(1114, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(936, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(739, 2);
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1114, 3);
            this.panelControl2.TabIndex = 2;
            // 
            // panelControl3
            // 
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(0, 3);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1114, 562);
            this.panelControl3.TabIndex = 3;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.layoutControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1108, 81);
            this.panelControl1.TabIndex = 2;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btn区域档案查询);
            this.layoutControl1.Controls.Add(this.cbo村委会);
            this.layoutControl1.Controls.Add(this.cbo街道);
            this.layoutControl1.Controls.Add(this.txt档案号);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1104, 77);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btn区域档案查询
            // 
            this.btn区域档案查询.Image = ((System.Drawing.Image)(resources.GetObject("btn区域档案查询.Image")));
            this.btn区域档案查询.Location = new System.Drawing.Point(606, 52);
            this.btn区域档案查询.Name = "btn区域档案查询";
            this.btn区域档案查询.Size = new System.Drawing.Size(136, 22);
            this.btn区域档案查询.StyleController = this.layoutControl1;
            this.btn区域档案查询.TabIndex = 9;
            this.btn区域档案查询.Text = "区域档案查询";
            this.btn区域档案查询.Click += new System.EventHandler(this.btn区域档案查询_Click);
            // 
            // cbo村委会
            // 
            this.cbo村委会.Location = new System.Drawing.Point(411, 52);
            this.cbo村委会.Name = "cbo村委会";
            this.cbo村委会.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo村委会.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo村委会.Size = new System.Drawing.Size(191, 20);
            this.cbo村委会.StyleController = this.layoutControl1;
            this.cbo村委会.TabIndex = 8;
            // 
            // cbo街道
            // 
            this.cbo街道.Location = new System.Drawing.Point(111, 52);
            this.cbo街道.Name = "cbo街道";
            this.cbo街道.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo街道.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo街道.Size = new System.Drawing.Size(191, 20);
            this.cbo街道.StyleController = this.layoutControl1;
            this.cbo街道.TabIndex = 7;
            this.cbo街道.EditValueChanged += new System.EventHandler(this.cbo街道_EditValueChanged);
            // 
            // txt档案号
            // 
            this.txt档案号.Location = new System.Drawing.Point(711, 28);
            this.txt档案号.Name = "txt档案号";
            this.txt档案号.Size = new System.Drawing.Size(191, 20);
            this.txt档案号.StyleController = this.layoutControl1;
            this.txt档案号.TabIndex = 6;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(411, 28);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Size = new System.Drawing.Size(191, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 5;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(111, 28);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(191, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.emptySpaceItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1087, 80);
            this.layoutControlGroup1.Text = "信息查询 \r\n";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.txt姓名;
            this.layoutControlItem1.CustomizationFormText = "姓名：";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(300, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "姓名：";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.txt身份证号;
            this.layoutControlItem2.CustomizationFormText = "身份证号：";
            this.layoutControlItem2.Location = new System.Drawing.Point(300, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(300, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "身份证号：";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.txt档案号;
            this.layoutControlItem3.CustomizationFormText = "档案号：";
            this.layoutControlItem3.Location = new System.Drawing.Point(600, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(300, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "档案号：";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(900, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(179, 50);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.cbo街道;
            this.layoutControlItem4.CustomizationFormText = "街道/乡镇:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(300, 26);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "街道/乡镇:";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.cbo村委会;
            this.layoutControlItem5.CustomizationFormText = "居/村委会：";
            this.layoutControlItem5.Location = new System.Drawing.Point(300, 24);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(300, 26);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "居/村委会：";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btn区域档案查询;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(600, 24);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(140, 26);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(140, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(300, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.gc个人健康档案);
            this.groupControl1.Controls.Add(this.btn档案转入申请);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 81);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1108, 449);
            this.groupControl1.TabIndex = 6;
            this.groupControl1.Text = "查询结果列表";
            // 
            // gc个人健康档案
            // 
            this.gc个人健康档案.AllowBandedGridColumnSort = false;
            this.gc个人健康档案.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc个人健康档案.IsBestFitColumns = true;
            this.gc个人健康档案.Location = new System.Drawing.Point(2, 24);
            this.gc个人健康档案.MainView = this.gv个人健康档案;
            this.gc个人健康档案.Name = "gc个人健康档案";
            this.gc个人健康档案.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.gc个人健康档案.ShowContextMenu = false;
            this.gc个人健康档案.Size = new System.Drawing.Size(1104, 423);
            this.gc个人健康档案.TabIndex = 7;
            this.gc个人健康档案.UseCheckBox = true;
            this.gc个人健康档案.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv个人健康档案});
            // 
            // gv个人健康档案
            // 
            this.gv个人健康档案.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gv个人健康档案.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gv个人健康档案.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gv个人健康档案.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.DetailTip.Options.UseFont = true;
            this.gv个人健康档案.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.Empty.Options.UseFont = true;
            this.gv个人健康档案.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.EvenRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FilterPanel.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FixedLine.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FocusedCell.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FocusedRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gv个人健康档案.Appearance.FooterPanel.Options.UseFont = true;
            this.gv个人健康档案.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.GroupButton.Options.UseFont = true;
            this.gv个人健康档案.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.GroupFooter.Options.UseFont = true;
            this.gv个人健康档案.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.GroupPanel.Options.UseFont = true;
            this.gv个人健康档案.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.GroupRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.HeaderPanel.Options.UseFont = true;
            this.gv个人健康档案.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.HorzLine.Options.UseFont = true;
            this.gv个人健康档案.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.OddRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.Preview.Options.UseFont = true;
            this.gv个人健康档案.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.Row.Options.UseFont = true;
            this.gv个人健康档案.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.RowSeparator.Options.UseFont = true;
            this.gv个人健康档案.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.SelectedRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.TopNewRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.VertLine.Options.UseFont = true;
            this.gv个人健康档案.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.ViewCaption.Options.UseFont = true;
            this.gv个人健康档案.ColumnPanelRowHeight = 30;
            this.gv个人健康档案.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col个人档案号码,
            this.col姓名,
            this.col性别,
            this.col出生日期,
            this.col身份证号,
            this.col居住地址,
            this.col本人电话,
            this.col联系人电话,
            this.col所属机构,
            this.col创建人,
            this.col创建时间,
            this.col档案状态});
            this.gv个人健康档案.GridControl = this.gc个人健康档案;
            this.gv个人健康档案.GroupPanelText = "DragColumn";
            this.gv个人健康档案.Name = "gv个人健康档案";
            this.gv个人健康档案.OptionsView.ColumnAutoWidth = false;
            this.gv个人健康档案.OptionsView.EnableAppearanceEvenRow = true;
            this.gv个人健康档案.OptionsView.EnableAppearanceOddRow = true;
            this.gv个人健康档案.OptionsView.ShowGroupPanel = false;
            // 
            // col个人档案号码
            // 
            this.col个人档案号码.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.col个人档案号码.AppearanceCell.Options.UseBackColor = true;
            this.col个人档案号码.AppearanceCell.Options.UseTextOptions = true;
            this.col个人档案号码.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col个人档案号码.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col个人档案号码.AppearanceHeader.Options.UseFont = true;
            this.col个人档案号码.AppearanceHeader.Options.UseTextOptions = true;
            this.col个人档案号码.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col个人档案号码.Caption = "档案号";
            this.col个人档案号码.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.col个人档案号码.FieldName = "个人档案编号";
            this.col个人档案号码.Name = "col个人档案号码";
            this.col个人档案号码.OptionsColumn.ReadOnly = true;
            this.col个人档案号码.Visible = true;
            this.col个人档案号码.VisibleIndex = 0;
            this.col个人档案号码.Width = 127;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit1_Click);
            // 
            // col姓名
            // 
            this.col姓名.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.col姓名.AppearanceCell.Options.UseBackColor = true;
            this.col姓名.AppearanceCell.Options.UseTextOptions = true;
            this.col姓名.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col姓名.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col姓名.AppearanceHeader.Options.UseFont = true;
            this.col姓名.AppearanceHeader.Options.UseTextOptions = true;
            this.col姓名.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col姓名.Caption = "姓名";
            this.col姓名.FieldName = "姓名";
            this.col姓名.Name = "col姓名";
            this.col姓名.OptionsColumn.AllowEdit = false;
            this.col姓名.OptionsColumn.ReadOnly = true;
            this.col姓名.Visible = true;
            this.col姓名.VisibleIndex = 1;
            // 
            // col性别
            // 
            this.col性别.AppearanceCell.Options.UseTextOptions = true;
            this.col性别.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col性别.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col性别.AppearanceHeader.Options.UseFont = true;
            this.col性别.AppearanceHeader.Options.UseTextOptions = true;
            this.col性别.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col性别.Caption = "性别";
            this.col性别.FieldName = "性别";
            this.col性别.Name = "col性别";
            this.col性别.OptionsColumn.AllowEdit = false;
            this.col性别.OptionsColumn.ReadOnly = true;
            this.col性别.Visible = true;
            this.col性别.VisibleIndex = 2;
            // 
            // col出生日期
            // 
            this.col出生日期.AppearanceCell.Options.UseTextOptions = true;
            this.col出生日期.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col出生日期.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col出生日期.AppearanceHeader.Options.UseFont = true;
            this.col出生日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col出生日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col出生日期.Caption = "出生日期";
            this.col出生日期.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.col出生日期.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.col出生日期.FieldName = "出生日期";
            this.col出生日期.Name = "col出生日期";
            this.col出生日期.OptionsColumn.AllowEdit = false;
            this.col出生日期.OptionsColumn.ReadOnly = true;
            this.col出生日期.Visible = true;
            this.col出生日期.VisibleIndex = 3;
            // 
            // col身份证号
            // 
            this.col身份证号.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.col身份证号.AppearanceCell.Options.UseBackColor = true;
            this.col身份证号.AppearanceCell.Options.UseTextOptions = true;
            this.col身份证号.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col身份证号.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col身份证号.AppearanceHeader.Options.UseFont = true;
            this.col身份证号.AppearanceHeader.Options.UseTextOptions = true;
            this.col身份证号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col身份证号.Caption = "身份证号";
            this.col身份证号.FieldName = "身份证号";
            this.col身份证号.Name = "col身份证号";
            this.col身份证号.OptionsColumn.AllowEdit = false;
            this.col身份证号.OptionsColumn.ReadOnly = true;
            this.col身份证号.Visible = true;
            this.col身份证号.VisibleIndex = 4;
            // 
            // col居住地址
            // 
            this.col居住地址.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col居住地址.AppearanceHeader.Options.UseFont = true;
            this.col居住地址.AppearanceHeader.Options.UseTextOptions = true;
            this.col居住地址.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col居住地址.Caption = "居住地址";
            this.col居住地址.FieldName = "居住地址";
            this.col居住地址.Name = "col居住地址";
            this.col居住地址.OptionsColumn.AllowEdit = false;
            this.col居住地址.OptionsColumn.ReadOnly = true;
            this.col居住地址.Visible = true;
            this.col居住地址.VisibleIndex = 5;
            this.col居住地址.Width = 163;
            // 
            // col本人电话
            // 
            this.col本人电话.AppearanceCell.Options.UseTextOptions = true;
            this.col本人电话.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col本人电话.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col本人电话.AppearanceHeader.Options.UseFont = true;
            this.col本人电话.AppearanceHeader.Options.UseTextOptions = true;
            this.col本人电话.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col本人电话.Caption = "本人电话";
            this.col本人电话.FieldName = "本人电话";
            this.col本人电话.Name = "col本人电话";
            this.col本人电话.OptionsColumn.AllowEdit = false;
            this.col本人电话.OptionsColumn.ReadOnly = true;
            this.col本人电话.Visible = true;
            this.col本人电话.VisibleIndex = 6;
            // 
            // col联系人电话
            // 
            this.col联系人电话.AppearanceCell.Options.UseTextOptions = true;
            this.col联系人电话.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col联系人电话.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col联系人电话.AppearanceHeader.Options.UseFont = true;
            this.col联系人电话.AppearanceHeader.Options.UseTextOptions = true;
            this.col联系人电话.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col联系人电话.Caption = "联系人电话";
            this.col联系人电话.FieldName = "联系人电话";
            this.col联系人电话.Name = "col联系人电话";
            this.col联系人电话.OptionsColumn.AllowEdit = false;
            this.col联系人电话.OptionsColumn.ReadOnly = true;
            this.col联系人电话.Visible = true;
            this.col联系人电话.VisibleIndex = 7;
            // 
            // col所属机构
            // 
            this.col所属机构.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.col所属机构.AppearanceCell.Options.UseBackColor = true;
            this.col所属机构.AppearanceCell.Options.UseTextOptions = true;
            this.col所属机构.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col所属机构.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col所属机构.AppearanceHeader.Options.UseFont = true;
            this.col所属机构.AppearanceHeader.Options.UseTextOptions = true;
            this.col所属机构.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col所属机构.Caption = "当前所属机构";
            this.col所属机构.FieldName = "所属机构";
            this.col所属机构.Name = "col所属机构";
            this.col所属机构.OptionsColumn.AllowEdit = false;
            this.col所属机构.OptionsColumn.ReadOnly = true;
            this.col所属机构.Visible = true;
            this.col所属机构.VisibleIndex = 8;
            this.col所属机构.Width = 93;
            // 
            // col创建人
            // 
            this.col创建人.AppearanceCell.Options.UseTextOptions = true;
            this.col创建人.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col创建人.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col创建人.AppearanceHeader.Options.UseFont = true;
            this.col创建人.AppearanceHeader.Options.UseTextOptions = true;
            this.col创建人.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col创建人.Caption = "录入人";
            this.col创建人.FieldName = "创建人";
            this.col创建人.Name = "col创建人";
            this.col创建人.OptionsColumn.AllowEdit = false;
            this.col创建人.OptionsColumn.ReadOnly = true;
            this.col创建人.Visible = true;
            this.col创建人.VisibleIndex = 9;
            // 
            // col创建时间
            // 
            this.col创建时间.AppearanceCell.Options.UseTextOptions = true;
            this.col创建时间.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col创建时间.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col创建时间.AppearanceHeader.Options.UseFont = true;
            this.col创建时间.AppearanceHeader.Options.UseTextOptions = true;
            this.col创建时间.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col创建时间.Caption = "录入时间";
            this.col创建时间.DisplayFormat.FormatString = "d";
            this.col创建时间.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.col创建时间.FieldName = "创建时间";
            this.col创建时间.Name = "col创建时间";
            this.col创建时间.OptionsColumn.AllowEdit = false;
            this.col创建时间.OptionsColumn.ReadOnly = true;
            this.col创建时间.Visible = true;
            this.col创建时间.VisibleIndex = 10;
            // 
            // col档案状态
            // 
            this.col档案状态.AppearanceCell.Options.UseTextOptions = true;
            this.col档案状态.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col档案状态.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col档案状态.AppearanceHeader.Options.UseFont = true;
            this.col档案状态.AppearanceHeader.Options.UseTextOptions = true;
            this.col档案状态.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col档案状态.Caption = "档案状态";
            this.col档案状态.FieldName = "档案状态";
            this.col档案状态.Name = "col档案状态";
            this.col档案状态.OptionsColumn.AllowEdit = false;
            this.col档案状态.OptionsColumn.ReadOnly = true;
            this.col档案状态.Visible = true;
            this.col档案状态.VisibleIndex = 11;
            // 
            // btn档案转入申请
            // 
            this.btn档案转入申请.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.btn档案转入申请.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn档案转入申请.Appearance.Options.UseFont = true;
            this.btn档案转入申请.Appearance.Options.UseForeColor = true;
            this.btn档案转入申请.Location = new System.Drawing.Point(214, -1);
            this.btn档案转入申请.Name = "btn档案转入申请";
            this.btn档案转入申请.Size = new System.Drawing.Size(106, 23);
            this.btn档案转入申请.TabIndex = 6;
            this.btn档案转入申请.Text = "档案转入申请";
            this.btn档案转入申请.Click += new System.EventHandler(this.btn档案转入申请_Click);
            // 
            // frm转档申请
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1114, 565);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl2);
            this.Name = "frm转档申请";
            this.Text = "转档申请";
            this.Load += new System.EventHandler(this.frm转档申请_Load);
            this.Controls.SetChildIndex(this.panelControl2, 0);
            this.Controls.SetChildIndex(this.panelControl3, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.Controls.SetChildIndex(this.gcNavigator, 0);
            this.Controls.SetChildIndex(this.pnlSummary, 0);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbo村委会.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo街道.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc个人健康档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv个人健康档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txt档案号;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.SimpleButton btn区域档案查询;
        private DevExpress.XtraEditors.ComboBoxEdit cbo村委会;
        private DevExpress.XtraEditors.ComboBoxEdit cbo街道;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btn档案转入申请;
        private DataGridControl gc个人健康档案;
        private DevExpress.XtraGrid.Views.Grid.GridView gv个人健康档案;
        private DevExpress.XtraGrid.Columns.GridColumn col个人档案号码;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn col姓名;
        private DevExpress.XtraGrid.Columns.GridColumn col性别;
        private DevExpress.XtraGrid.Columns.GridColumn col出生日期;
        private DevExpress.XtraGrid.Columns.GridColumn col身份证号;
        private DevExpress.XtraGrid.Columns.GridColumn col居住地址;
        private DevExpress.XtraGrid.Columns.GridColumn col本人电话;
        private DevExpress.XtraGrid.Columns.GridColumn col联系人电话;
        private DevExpress.XtraGrid.Columns.GridColumn col所属机构;
        private DevExpress.XtraGrid.Columns.GridColumn col创建人;
        private DevExpress.XtraGrid.Columns.GridColumn col创建时间;
        private DevExpress.XtraGrid.Columns.GridColumn col档案状态;

    }
}