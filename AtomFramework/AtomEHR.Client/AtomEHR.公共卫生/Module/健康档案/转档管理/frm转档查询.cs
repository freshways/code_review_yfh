﻿using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.Models;
using AtomEHR.公共卫生.Module.个人健康;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.健康档案.转档管理
{
    public partial class frm转档查询 : frmBaseBusinessForm
    {

        AtomEHR.Business.bll转档申请 _Bll = new Business.bll转档申请();
        string _serverDate;
        DataTable _dt;
        DataSet _ds;

        public frm转档查询()
        {
            InitializeComponent();
            _serverDate = _Bll.ServiceDateTime;
        }
        /// <summary>
        /// 带转入档案  撤销操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn撤销_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv待转入档案.GetFocusedDataRow();
            if (row == null) return;
            string id = row["ID"].ToString();
            if (!string.IsNullOrEmpty(id))
            {
                if (_Bll.Delete(id))
                {
                    Msg.ShowInformation("撤销成功！");
                    this.btn待转入档案_Click(null, null);
                }
            }
        }
        /// <summary>
        /// 带转出档案  驳回操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn驳回_Click(object sender, EventArgs e)
        {
            DataTable dt = this.gc待转出档案.DataSource as DataTable;
            DataRow row = this.gv待转出档案.GetFocusedDataRow();
            if (row == null) return;
            frm驳回原因 frm = new frm驳回原因();
            frm.ShowDialog();
            if (frm.DialogResult != System.Windows.Forms.DialogResult.OK) return;

            string jjyy = string.Empty;
            if (frm.DialogResult == System.Windows.Forms.DialogResult.OK)
                jjyy = frm._remark;
            else
                return;
            string id = row["ID"].ToString();
            string type = string.Empty;
            string wsjshr = string.Empty;
            string ssjgshr = string.Empty;
            string pRgid = Loginer.CurrentUser.所属机构;
            if (pRgid.Length <= 6)
            {
                type = "4";
                wsjshr = Loginer.CurrentUser.用户编码;
            }
            else
            {
                type = "2";
                ssjgshr = Loginer.CurrentUser.用户编码;
            }
            DataSet ds = _Bll.GetBusinessByKey(id, true);
            if (ds.Tables[Models.tb_转档申请.__TableName].Rows.Count == 1)
            {
                ds.Tables[tb_转档申请.__TableName].Rows[0][tb_转档申请.拒绝原因] = jjyy;
                ds.Tables[tb_转档申请.__TableName].Rows[0][tb_转档申请.类型] = type;
                ds.Tables[tb_转档申请.__TableName].Rows[0][tb_转档申请.所属机构审核人] = ssjgshr;
                ds.Tables[tb_转档申请.__TableName].Rows[0][tb_转档申请.卫生局审核人] = wsjshr;
            }
            SaveResult result = _Bll.Save(ds);
            if (result.Success)
            {
                Msg.ShowInformation("驳回成功！");
                btn待转出档案_Click(null, null);
            }
            else
            {
                //Msg.ShowInformation("操作失败，请联系管理员"+result.e);
            }
        }

        private void btn已转入档案_Click(object sender, EventArgs e)
        {
            #region 源代码

            //serrchConvert('alreadyRollin');
            //ID	别名	名称	键值	备注	P_RGID
            //8	sfsh	转档卫生局是否审核	2	1审核，2不审核，只识别1/2	NULL
            //Convert --》TzdZdsqController ---->  serrchConvert
            /*
<select id="selectByMaplistPage" resultMap="TzdZdsqResultMap"
		parameterType="com.jiayu.convert.model.TzdZdsq">
		select
		<include refid="zdAndRkx" />
		from DB2ADMIN.T_ZD_ZDSQ zd inner join DB2ADMIN.T_DA_JKDA_RKXZL
		rkx on  D_GRDABH = rkx.D_GRDABH
		<where>
			<if
				test="status=='alreadyRollin'||status=='waitRollin'||status=='reject'">
				<if test="contains == 'on'">
					<if test="dqjg.length() &lt; 7">
						and  P_ZRPRGID like
						CONCAT(#{dqjg,jdbcType=VARCHAR},'%')
					</if>
					<if test="dqjg.length() == 12">
						and ( P_ZRPRGID = #{dqjg,jdbcType=VARCHAR} or(
						length( P_ZRPRGID)=15 and
						(substr( P_ZRPRGID,1,7)||'1'||substr( P_ZRPRGID,9,7)
						like CONCAT(#{dqjg},'%'))))
					</if>
					<if test="dqjg.length() == 15">
						and  P_ZRPRGID = #{dqjg,jdbcType=VARCHAR}
					</if>
				</if>
				<if test="contains == null">
					and  P_ZRPRGID = #{dqjg,jdbcType=VARCHAR}
				</if>
				<if test="status=='alreadyRollin'">and  TYPE='3'</if>
				<if test="status=='waitRollin'">
					and ( TYPE='0' or  TYPE='1')
				</if>
				<if test="status=='reject'">
					and ( TYPE='2' or  TYPE='4')
				</if>
			</if>
			<if
				test="status=='alreadyRollout'||status=='waitRollout'">
				<if test="contains == 'on'">
					<if test="dqjg.length() &lt; 7">
						and  P_ZCPRGID like
						CONCAT(#{dqjg,jdbcType=VARCHAR},'%')
					</if>
					<if test="dqjg.length() == 12">
						and ( P_ZCPRGID = #{dqjg,jdbcType=VARCHAR} or(
						length( P_ZCPRGID)=15 and
						(substr( P_ZCPRGID,1,7)||'1'||substr( P_ZCPRGID,9,7)
						like CONCAT(#{dqjg},'%'))))
					</if>
					<if test="dqjg.length() == 15">
						and  P_ZCPRGID = #{dqjg,jdbcType=VARCHAR}
					</if>
				</if>
				<if test="contains == null">
					and  P_ZCPRGID = #{dqjg,jdbcType=VARCHAR}
				</if>
				<if test="status=='alreadyRollout'">and  TYPE='3'</if>
				<if test="status=='waitRollout'">
					and ( TYPE='0' or  TYPE='1')
				</if>
			</if>
		</where>
		order by  HAPPENTIME desc
	</select>
             */
            #endregion
            //Dictionary<string, string> _param = new Dictionary<string, string>();
            //_param.Add("status", "alreadyRollin");
            //_param.Add("包含下属机构", this.checkEdit1.Checked ? "1" : "0");
            //_param.Add("机构", this.cbo机构.EditValue.ToString());
            this.pager已转入档案.InitControl();
            string _strWhere = string.Empty;
            string pgrid = this.cbo机构.EditValue.ToString();
            if (this.checkEdit1.Checked)//包含下属机构
            {
                if (pgrid.Length < 7)
                {
                    _strWhere += " AND  转入机构 like '" + pgrid + "%' ";
                }
                if (pgrid.Length == 12)
                {
                    _strWhere += " AND ( 转入机构 = '" + pgrid + "'  OR (len( 转入机构)=15 and  substring( 转入机构,1,7)+'1'+substring( 转入机构,9,7)  like '" + pgrid + "%'))";
                }
                if (pgrid.Length == 15)
                {
                    _strWhere += " AND  转入机构 = '" + pgrid + "' ";
                }
            }
            else
            {
                _strWhere += " AND 转入机构 = '" + pgrid + "' ";
            }
            _strWhere += " AND 类型 = '3' ";
            this.gc已转入档案.StrWhere = _strWhere;
            this.gc已转入档案.View = "vw_转档";
            DataSet ds = this.pager已转入档案.GetQueryResultNew("vw_转档", "*", _strWhere, "HAPPENTIME", "DESC");

            this.xtraTabControl1.SelectedTabPage = tab已转入档案;
            if (ds.Tables[0].Rows.Count >= 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    ds.Tables[0].Rows[i]["性别"] = _Bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    ds.Tables[0].Rows[i]["转出机构"] = _BLL.Return机构名称(ds.Tables[0].Rows[i]["转出机构"].ToString());
                    ds.Tables[0].Rows[i]["所属机构"] = _BLL.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                    ds.Tables[0].Rows[i]["创建人"] = _BLL.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                    ds.Tables[0].Rows[i]["所属机构审核人"] = _BLL.Return用户名称(ds.Tables[0].Rows[i]["所属机构审核人"].ToString());
                }
            }
            this.pager已转入档案.DrawControl();
            this.gc已转入档案.DataSource = ds.Tables[0];
            this.gv已转入档案.BestFitColumns();
        }
        private void pager已转入档案_OnPageChanged(object sender, EventArgs e)
        {
            DataSet ds = this.pager已转入档案.GetQueryResultNew("vw_转档", "*", this.gc已转入档案.StrWhere, "HAPPENTIME", "DESC");

            this.xtraTabControl1.SelectedTabPage = tab已转入档案;
            if (ds.Tables[0].Rows.Count >= 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    ds.Tables[0].Rows[i]["性别"] = _Bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    ds.Tables[0].Rows[i]["转出机构"] = _BLL.Return机构名称(ds.Tables[0].Rows[i]["转出机构"].ToString());
                    ds.Tables[0].Rows[i]["所属机构"] = _BLL.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                    ds.Tables[0].Rows[i]["创建人"] = _BLL.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                    ds.Tables[0].Rows[i]["所属机构审核人"] = _BLL.Return用户名称(ds.Tables[0].Rows[i]["所属机构审核人"].ToString());
                }
            }
            this.gc已转入档案.DataSource = ds.Tables[0];
            this.gv已转入档案.BestFitColumns();
        }
        private void btn已转出档案_Click(object sender, EventArgs e)
        {
            //Dictionary<string, string> _param = new Dictionary<string, string>();
            //_param.Add("status", "alreadyRollout");
            string _strWhere = string.Empty;
            string pgrid = this.cbo机构.EditValue.ToString();
            this.pager已转出档案.InitControl();
            if (this.checkEdit1.Checked)//包含下属机构
            {
                if (pgrid.Length < 7)
                {
                    _strWhere += " AND  转出机构 like '" + pgrid + "%' ";
                }
                if (pgrid.Length == 12)
                {
                    _strWhere += " AND (转出机构 = '" + pgrid + "'  OR (len(转出机构)=15 and substring(转出机构,1,7)+'1'+substring(转出机构,9,7)  like '" + pgrid + "%'))";
                }
                if (pgrid.Length == 15)
                {
                    _strWhere += " AND 转出机构 = '" + pgrid + "' ";
                }
            }
            else
            {
                _strWhere += " AND 转出机构 = '" + pgrid + "' ";
            }
            _strWhere += " AND 类型 = '3' ";
            this.gc已转出档案.StrWhere = _strWhere;
            this.gc已转出档案.View = "vw_转档";
            DataSet ds = this.pager已转出档案.GetQueryResultNew("vw_转档", "*", _strWhere, "HAPPENTIME", "DESC");
            if (ds.Tables[0].Rows.Count >= 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    ds.Tables[0].Rows[i]["性别"] = _Bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    ds.Tables[0].Rows[i]["转出机构"] = _Bll.Return机构名称(ds.Tables[0].Rows[i]["转出机构"].ToString());
                    ds.Tables[0].Rows[i]["转入机构"] = _Bll.Return机构名称(ds.Tables[0].Rows[i]["转入机构"].ToString());
                    ds.Tables[0].Rows[i]["所属机构"] = _Bll.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                    ds.Tables[0].Rows[i]["所属机构审核人"] = _Bll.Return用户名称(ds.Tables[0].Rows[i]["所属机构审核人"].ToString());
                    ds.Tables[0].Rows[i]["创建人"] = _Bll.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                }
            }
            this.pager已转出档案.DrawControl();
            this.xtraTabControl1.SelectedTabPage = tab已转出档案;
            this.gc已转出档案.DataSource = ds.Tables[0];
            this.gv已转出档案.BestFitColumns();
        }
        private void pager已转出档案_OnPageChanged(object sender, EventArgs e)
        {
            DataSet ds = this.pager已转出档案.GetQueryResultNew("vw_转档", "*", this.gc已转出档案.StrWhere, "HAPPENTIME", "DESC");
            if (ds.Tables[0].Rows.Count >= 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    ds.Tables[0].Rows[i]["性别"] = _Bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    ds.Tables[0].Rows[i]["转出机构"] = _Bll.Return机构名称(ds.Tables[0].Rows[i]["转出机构"].ToString());
                    ds.Tables[0].Rows[i]["转入机构"] = _Bll.Return机构名称(ds.Tables[0].Rows[i]["转入机构"].ToString());
                    ds.Tables[0].Rows[i]["所属机构"] = _Bll.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                    ds.Tables[0].Rows[i]["所属机构审核人"] = _Bll.Return用户名称(ds.Tables[0].Rows[i]["所属机构审核人"].ToString());
                    ds.Tables[0].Rows[i]["创建人"] = _Bll.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                }
            }
            this.xtraTabControl1.SelectedTabPage = tab已转出档案;
            this.gc已转出档案.DataSource = ds.Tables[0];
            this.gv已转出档案.BestFitColumns();
        }
        private void btn待转入档案_Click(object sender, EventArgs e)
        {
            //Dictionary<string, string> _param = new Dictionary<string, string>();
            //_param.Add("status", "waitRollin");
            //_param.Add("包含下属机构", this.checkEdit1.Checked ? "1" : "0");
            string _strWhere = string.Empty;
            string pgrid = this.cbo机构.EditValue.ToString();
            this.pager待转入档案.InitControl();
            if (this.checkEdit1.Checked)//包含下属机构
            {
                if (pgrid.Length < 7)
                {
                    _strWhere += " AND  转入机构 like '" + pgrid + "%' ";
                }
                if (pgrid.Length == 12)
                {
                    _strWhere += " AND ( 转入机构 = '" + pgrid + "'  OR (len( 转入机构)=15 and  substring( 转入机构,1,7)+'1'+substring( 转入机构,9,7)  like '" + pgrid + "%'))";
                }
                if (pgrid.Length == 15)
                {
                    _strWhere += " AND  转入机构 = '" + pgrid + "' ";
                }
            }
            else
            {
                _strWhere += " AND  转入机构 = '" + pgrid + "' ";
            }
            _strWhere += " AND (类型 = '0' or  类型 = '1' )";
            gc待转入档案.StrWhere = _strWhere;
            this.gc待转入档案.View = "vw_转档";
            DataSet ds = this.pager待转入档案.GetQueryResultNew("vw_转档", "*", _strWhere, "HAPPENTIME", "DESC");

            if (ds.Tables[0].Rows.Count >= 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    ds.Tables[0].Rows[i]["性别"] = _Bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    ds.Tables[0].Rows[i]["转出机构"] = _Bll.Return机构名称(ds.Tables[0].Rows[i]["转出机构"].ToString());
                    ds.Tables[0].Rows[i]["所属机构"] = _Bll.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                    ds.Tables[0].Rows[i]["创建人"] = _Bll.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                    ds.Tables[0].Rows[i]["类型"] = "待所属机构审核";
                }
            }
            this.pager待转入档案.DrawControl();
            this.xtraTabControl1.SelectedTabPage = tab待转入档案;
            this.gc待转入档案.DataSource = ds.Tables[0];
            this.gv待转入档案.BestFitColumns();
        }
        private void pager待转入档案_OnPageChanged(object sender, EventArgs e)
        {
            DataSet ds = this.pager待转入档案.GetQueryResultNew("vw_转档", "*", gc待转入档案.StrWhere, "HAPPENTIME", "DESC");

            if (ds.Tables[0].Rows.Count >= 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    ds.Tables[0].Rows[i]["性别"] = _Bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    ds.Tables[0].Rows[i]["转出机构"] = _Bll.Return机构名称(ds.Tables[0].Rows[i]["转出机构"].ToString());
                    ds.Tables[0].Rows[i]["所属机构"] = _Bll.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                    ds.Tables[0].Rows[i]["创建人"] = _Bll.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                    ds.Tables[0].Rows[i]["类型"] = "待所属机构审核";
                }
            }
            this.xtraTabControl1.SelectedTabPage = tab待转入档案;
            this.gc待转入档案.DataSource = ds.Tables[0];
            this.gv待转入档案.BestFitColumns();
        }
        private void btn待转出档案_Click(object sender, EventArgs e)
        {
            //Dictionary<string, string> _param = new Dictionary<string, string>();
            //_param.Add("status", "waitRollout");
            //_param.Add("包含下属机构", this.checkEdit1.Checked ? "1" : "0");
            //_param.Add("机构", this.cbo机构.EditValue.ToString());
            string _strWhere = string.Empty;
            string pgrid = this.cbo机构.EditValue.ToString();
            this.pager待转出档案.InitControl();
            if (this.checkEdit1.Checked)//包含下属机构
            {
                if (pgrid.Length < 7)
                {
                    _strWhere += " AND  转出机构 like '" + pgrid + "%' ";
                }
                if (pgrid.Length == 12)
                {
                    _strWhere += " AND ( 转出机构 = '" + pgrid + "'  OR (len( 转出机构)=15 and substring( 转出机构,1,7)+'1'+substring( 转出机构,9,7)  like '" + pgrid + "%'))";
                }
                if (pgrid.Length == 15)
                {
                    _strWhere += " AND  转出机构 = '" + pgrid + "' ";
                }
            }
            else
            {
                _strWhere += " AND  转出机构 = '" + pgrid + "' ";
            }
            _strWhere += " AND (类型 = '0' or  类型 = '1' )";
            this.gc待转出档案.View = "vw_转档";
            gc待转出档案.StrWhere = _strWhere;
            DataSet ds = this.pager待转出档案.GetQueryResultNew("vw_转档", "*", _strWhere, "HAPPENTIME", "DESC");

            if (ds.Tables[0].Rows.Count >= 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    ds.Tables[0].Rows[i]["性别"] = _Bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    ds.Tables[0].Rows[i]["转入机构"] = _Bll.Return机构名称(ds.Tables[0].Rows[i]["转入机构"].ToString());
                    ds.Tables[0].Rows[i]["所属机构"] = _Bll.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                    ds.Tables[0].Rows[i]["创建人"] = _Bll.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                    ds.Tables[0].Rows[i]["类型"] = "待所属机构审核";
                }
            }
            this.pager待转出档案.DrawControl();
            this.xtraTabControl1.SelectedTabPage = tab待转出档案;
            this.gc待转出档案.DataSource = ds.Tables[0];
            this.gv待转出档案.BestFitColumns();

        }
        private void pager待转出档案_OnPageChanged(object sender, EventArgs e)
        {
            DataSet ds = this.pager待转出档案.GetQueryResultNew("vw_转档", "*", gc待转出档案.StrWhere, "HAPPENTIME", "DESC");

            if (ds.Tables[0].Rows.Count >= 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    ds.Tables[0].Rows[i]["性别"] = _Bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    ds.Tables[0].Rows[i]["转入机构"] = _Bll.Return机构名称(ds.Tables[0].Rows[i]["转入机构"].ToString());
                    ds.Tables[0].Rows[i]["所属机构"] = _Bll.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                    ds.Tables[0].Rows[i]["创建人"] = _Bll.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                    ds.Tables[0].Rows[i]["类型"] = "待所属机构审核";
                }
            }
            this.xtraTabControl1.SelectedTabPage = tab待转出档案;
            this.gc待转出档案.DataSource = ds.Tables[0];
            this.gv待转出档案.BestFitColumns();

        }
        private void btn驳回申请档案_Click(object sender, EventArgs e)
        {
            string _strWhere = string.Empty;
            string pgrid = this.cbo机构.EditValue.ToString();
            this.pager驳回申请档案.InitControl();
            if (this.checkEdit1.Checked)//包含下属机构
            {
                if (pgrid.Length < 7)
                {
                    _strWhere += " AND  转入机构 like '" + pgrid + "%' ";
                }
                if (pgrid.Length == 12)
                {
                    _strWhere += " AND ( 转入机构 = '" + pgrid + "'  OR (len( 转入机构)=15 and  substring( 转入机构,1,7)+'1'+substring( 转入机构,9,7)  like '" + pgrid + "%'))";
                }
                if (pgrid.Length == 15)
                {
                    _strWhere += " AND  转入机构 = '" + pgrid + "' ";
                }
            }
            else
            {
                _strWhere += " AND  转入机构 = '" + pgrid + "' ";
            }
            _strWhere += " AND (类型 = '2' or  类型 = '4' )";
            this.gc驳回申请档案.View = "vw_转档";
            this.gc驳回申请档案.StrWhere = _strWhere;
            DataSet ds = this.pager驳回申请档案.GetQueryResultNew("vw_转档", "*", _strWhere, "HAPPENTIME", "DESC");

            if (ds.Tables[0].Rows.Count >= 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    ds.Tables[0].Rows[i]["性别"] = _Bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    ds.Tables[0].Rows[i]["转出机构"] = _Bll.Return机构名称(ds.Tables[0].Rows[i]["转出机构"].ToString());
                    ds.Tables[0].Rows[i]["所属机构"] = _Bll.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                    ds.Tables[0].Rows[i]["创建人"] = _Bll.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                    ds.Tables[0].Rows[i]["所属机构审核人"] = _Bll.Return用户名称(ds.Tables[0].Rows[i]["所属机构审核人"].ToString());
                    ds.Tables[0].Rows[i]["卫生局审核人"] = _Bll.Return用户名称(ds.Tables[0].Rows[i]["卫生局审核人"].ToString());
                    ds.Tables[0].Rows[i]["类型"] = "待所属机构审核";
                }
            }
            this.pager驳回申请档案.DrawControl();
            this.xtraTabControl1.SelectedTabPage = tab驳回申请档案;
            this.gc驳回申请档案.DataSource = ds.Tables[0];
            this.gv驳回申请档案.BestFitColumns();
        }
        private void pager驳回申请档案_OnPageChanged(object sender, EventArgs e)
        {
            DataSet ds = this.pager驳回申请档案.GetQueryResultNew("vw_转档", "*", this.gc驳回申请档案.StrWhere, "HAPPENTIME", "DESC");

            if (ds.Tables[0].Rows.Count >= 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    ds.Tables[0].Rows[i]["性别"] = _Bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    ds.Tables[0].Rows[i]["转出机构"] = _Bll.Return机构名称(ds.Tables[0].Rows[i]["转出机构"].ToString());
                    ds.Tables[0].Rows[i]["所属机构"] = _Bll.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                    ds.Tables[0].Rows[i]["创建人"] = _Bll.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                    ds.Tables[0].Rows[i]["所属机构审核人"] = _Bll.Return用户名称(ds.Tables[0].Rows[i]["所属机构审核人"].ToString());
                    ds.Tables[0].Rows[i]["卫生局审核人"] = _Bll.Return用户名称(ds.Tables[0].Rows[i]["卫生局审核人"].ToString());
                    ds.Tables[0].Rows[i]["类型"] = "待所属机构审核";
                }
            }
            this.xtraTabControl1.SelectedTabPage = tab驳回申请档案;
            this.gc驳回申请档案.DataSource = ds.Tables[0];
            this.gv驳回申请档案.BestFitColumns();
        }

        /// <summary>
        /// 待转入档案  同意操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn同意_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv待转出档案.GetFocusedDataRow();
            if (row == null) return;
            string id = row["ID"].ToString();
            string jtdabh = row["家庭档案编号"].ToString();
            string docNO = row["个人档案编号"].ToString();
            string yhzgx = new bll健康档案().GetYHZGX(docNO);
            int jtcysl = 0;//家庭成员数量
            string dqjg = Loginer.CurrentUser.所属机构;
            if (!string.IsNullOrEmpty(jtdabh))
            {
                bll家庭档案 _bll家庭档案 = new bll家庭档案();
                jtcysl = _bll家庭档案.countHomeCy(jtdabh);//获取家庭成员数量
                if (dqjg.Length > 7)
                {
                    if (!string.IsNullOrEmpty(yhzgx) && jtcysl > 1)
                    {
                        if (Msg.AskQuestion("此居民家庭中含有其他成员！【确定】将把整个家庭的档案转出，【取消】将把该居民从家庭中注销并转出(该居民若为户主除外)。"))
                        {
                            SaveResult result = _Bll.updateConvertTopassed(id, yhzgx, jtdabh, "2");
                            if (result.Success)
                            {
                                Msg.ShowInformation("转档成功！");
                                //btn待转出档案_Click(null, null);
                                this.gv待转出档案.DeleteRow(this.gv待转出档案.FocusedRowHandle);
                            }
                        }
                        else
                        {
                            if (yhzgx == "1")//为户主，不能进行转档
                            {
                                Msg.ShowInformation("此居民是【户主】,且家庭中有其他成员，请先到家庭基本信息表中修改其【户主关系】为‘非户主’后再对其进行转档!");
                                return;
                            }
                            else
                            {
                                SaveResult result = _Bll.updateConvertTopassed(id, yhzgx, jtdabh, "1");
                                if (result.Success)
                                {
                                    Msg.ShowInformation("转档成功！");
                                    //btn待转出档案_Click(null, null);
                                    this.gv待转出档案.DeleteRow(this.gv待转出档案.FocusedRowHandle);
                                }
                            }
                        }
                    }
                    else
                    {
                        SaveResult result = _Bll.updateConvertTopassed(id, "", "", "1");
                        if (result.Success)
                        {
                            Msg.ShowInformation("转档成功！");
                            //btn待转出档案_Click(null, null);
                            this.gv待转出档案.DeleteRow(this.gv待转出档案.FocusedRowHandle);
                        }
                    }
                }
                else
                {
                    SaveResult result = _Bll.updateConvertTopassed(id, "", jtdabh, "");
                    if (result.Success)
                    {
                        Msg.ShowInformation("转档成功！");
                        //btn待转出档案_Click(null, null);
                        this.gv待转出档案.DeleteRow(this.gv待转出档案.FocusedRowHandle);
                    }
                }
            }
            else
            {
                SaveResult result = _Bll.updateConvertTopassed(id, "", jtdabh, "1");
                if (result.Success)
                {
                    //serrchConvert('waitRollout');
                    Msg.ShowInformation("转档成功！");
                    //btn待转出档案_Click(null, null);
                    this.gv待转出档案.DeleteRow(this.gv待转出档案.FocusedRowHandle);
                }
            }
        }
        private void frm转档查询_Load(object sender, EventArgs e)
        {
            InitializeForm();
            //为“所属机构"绑定信息
            try
            {
                DataTable dt所属机构 = _bll机构信息.Get机构树();
                cbo机构.Properties.ValueMember = "机构编号";
                cbo机构.Properties.DisplayMember = "机构名称";

                cbo机构.Properties.TreeList.KeyFieldName = "机构编号";
                cbo机构.Properties.TreeList.ParentFieldName = "上级机构";
                cbo机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                cbo机构.Properties.DataSource = dt所属机构;
                //treeListLookUpEdit机构.Properties.TreeList.ExpandAll();
                //treeListLookUpEdit机构.Properties.TreeList.CollapseAll();

                cbo机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                cbo机构.Text = Loginer.CurrentUser.所属机构;
            }
        }
        private void repositoryItemHyperLinkEdit3_Click(object sender, EventArgs e)
        {

        }
        protected override void InitializeForm()
        {
            base.InitializeForm();
            frmGridCustomize.RegisterGrid(gv已转入档案);
            frmGridCustomize.RegisterGrid(gv待转入档案);
        }
        private void link个人档案编号_Click(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.HyperLinkEdit edit = (DevExpress.XtraEditors.HyperLinkEdit)sender;
            if (edit == null) return;
            AtomEHR.Library.UserControls.DataGridControl gc = edit.Parent as AtomEHR.Library.UserControls.DataGridControl;
            if (gc == null) return;

            DevExpress.XtraGrid.Views.Grid.GridView view = gc.Views[0] as DevExpress.XtraGrid.Views.Grid.GridView;
            if (view == null) return;
            DataRow focusRow = view.GetFocusedDataRow();
            if (focusRow == null) return;
            string 个人档案编号 = focusRow["个人档案编号"] as string;
            string 家庭档案编号 = focusRow["家庭档案编号1"] as string;
            string date = focusRow["创建时间"] as string;
            //_BLL.GetBusinessByKey(个人档案编号, true);//下载一个空业务单据            
            //_BLL.NewBusiness(); //增加一条主表记录
            frm个人健康 frm = new frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, date);
            frm.Show();
        }

    }
}
