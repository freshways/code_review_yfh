﻿using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.公共卫生.Module.个人健康;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.健康档案.转档管理
{
    public partial class frm转档申请 : frmBaseBusinessForm
    {
        bll健康档案 _bll = new bll健康档案();
        string _serverDateTime;
        DataTable dt;
        public frm转档申请()
        {
            InitializeComponent();
            _serverDateTime = _bll.ServiceDateTime;
        }

        private void frm转档申请_Load(object sender, EventArgs e)
        {
            //为“镇”绑定信息
            DataView dv镇 = new DataView(DataDictCache.Cache.t地区信息);
            dv镇.RowFilter = "上级编码=371323";
            //DataBinder.BindingLookupEditDataSource(lookUpEdit镇, dv市.ToTable(), "地区名称", "地区编码");
            util.ControlsHelper.BindComboxData(dv镇.ToTable(), cbo街道, "地区编码", "地区名称");

        }

        private void cbo街道_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv村 = new DataView(DataDictCache.Cache.t地区信息);
                dv村.RowFilter = "上级编码='" + util.ControlsHelper.GetComboxKey(cbo街道) + "'";
                //DataBinder.BindingLookupEditDataSource(lookUpEdit村, dv县.ToTable(), "地区名称", "地区编码");
                util.ControlsHelper.BindComboxData(dv村.ToTable(), cbo村委会, "地区编码", "地区名称");
                util.ControlsHelper.SetComboxNullData("", cbo村委会, "请选择居/村委会");
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }

        private void btn区域档案查询_Click(object sender, EventArgs e)
        {
            string xm = this.txt姓名.Text.Trim();
            string sfzh = this.txt身份证号.Text.Trim();

            if (string.IsNullOrEmpty(xm) || string.IsNullOrEmpty(sfzh))
            {
                Msg.Warning("为了居民信息安全，姓名和身份证号必须填写！请重新输入！");
                return;
            }
            else
            {
                string sql = string.Empty;
                sql += " AND (姓名 = '" + xm + "') ";
                sql += " AND 身份证号 = '" + sfzh + "' ";
                string jiedao = util.ControlsHelper.GetComboxKey(cbo街道);
                if (!string.IsNullOrEmpty(jiedao))
                {
                    sql += " AND 街道 = '" + jiedao + "' ";
                }
                string cun = util.ControlsHelper.GetComboxKey(cbo村委会);
                if (!string.IsNullOrEmpty(cun))
                {
                    sql += " AND 居委会 = '" + cun + "' ";
                }

                dt = _bll.GetSummaryByParam(sql);
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dt.Rows[i]["姓名"] = util.DESEncrypt.DES解密(dt.Rows[i]["姓名"].ToString());
                    }
                }
                this.gc个人健康档案.DataSource = dt;
                this.gv个人健康档案.BestFitColumns();
            }
        }

        private void btn档案转入申请_Click(object sender, EventArgs e)
        {
            if (this.gv个人健康档案.SelectedRowsCount > 1 || this.gv个人健康档案.SelectedRowsCount == 0)
            {
                Msg.Warning("每次至少且只能选择一个居民进行转档申请操作，请重新选择您要转档的居民！");
                return;
            }
            else
            {
                int[] dr = this.gv个人健康档案.GetSelectedRows();
                string grdabh = this.gv个人健康档案.GetRowCellValue(dr[0], "个人档案编号").ToString();
                DataSet ds = new bll健康档案().GetOneUserCodeByKey(grdabh, false);
                DataTable dt个人健康档案 = ds.Tables[Models.tb_健康档案.__TableName];
                string 本机构编码 = Loginer.CurrentUser.所属机构;
                string pRgid = dt个人健康档案.Rows[0][Models.tb_健康档案.所属机构].ToString();
                if (pRgid == 本机构编码)
                {
                    Msg.ShowInformation("此居民属于本机构,无需进行转档,请重新选择！");
                    return;
                }
                else
                {
                    frm转档信息录入 frm = new frm转档信息录入(dt个人健康档案.Rows[0]);
                    DialogResult result = frm.ShowDialog();
                    if (result == System.Windows.Forms.DialogResult.OK)//发出转档申请成功
                    {

                    }
                }
            }
        }

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv个人健康档案.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            //_BLL.GetBusinessByKey(个人档案编号, true);//下载一个空业务单据            
            //_BLL.NewBusiness(); //增加一条主表记录
            frm个人健康 frm = new frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, null);
             frm.Show();
        }
    }
}
