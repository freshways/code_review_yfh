﻿using AtomEHR.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.健康档案.转档管理
{
    public partial class frm驳回原因 : Form
    {
        public string _remark = "";
        public frm驳回原因()
        {
            InitializeComponent();
        }

        private void btn确定_Click(object sender, EventArgs e)
        {
            if (this.textEdit1.Text.Trim() == "")
            {
                Msg.Warning("驳回原因是必填项，请输入驳回原因！");
                return;
            }
            _remark = this.textEdit1.Text.Trim();
            this.Close();
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btn取消_Click(object sender, EventArgs e)
        {
            this.Close();
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
