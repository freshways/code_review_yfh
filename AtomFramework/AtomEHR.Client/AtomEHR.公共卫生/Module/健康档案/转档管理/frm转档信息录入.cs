﻿using AtomEHR.Business;
using AtomEHR.Business.Security;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.健康档案.转档管理
{
    public partial class frm转档信息录入 : frmBase
    {
        string _grdabh;
        DataRow _dr;
        bll转档申请 _Bll = new bll转档申请();
        DataSet _ds;
        string _serverDate;
        bll机构信息 _bll机构信息 = new bll机构信息();
        bllUser _bllUser = new bllUser();
        public frm转档信息录入()
        {
            InitializeComponent();
        }

        public frm转档信息录入(DataRow dr)
        {
            InitializeComponent();
            _dr = dr;
            _ds = _Bll.GetBusinessByKey("-", true);
            _serverDate = _Bll.ServiceDateTime;
            BindData(dr);
        }

        private void BindData(DataRow dr)
        {
            this.txt档案号.Text = dr[tb_健康档案.个人档案编号].ToString();
            this.txt姓名.Text = util.DESEncrypt.DES解密(dr[tb_健康档案.姓名].ToString());
            this.txt性别.Text = _Bll.ReturnDis字典显示("xb_xingbie", dr[tb_健康档案.性别].ToString());
            this.txt身份证号.Text = dr[tb_健康档案.身份证号].ToString();
            this.txt居住地址.Text = _Bll.Return地区名称(dr[tb_健康档案.省].ToString()) + _Bll.Return地区名称(dr[tb_健康档案.市].ToString()) + _Bll.Return地区名称(dr[tb_健康档案.区].ToString()) + _Bll.Return地区名称(dr[tb_健康档案.街道].ToString()) + _Bll.Return地区名称(dr[tb_健康档案.居委会].ToString()) + dr[tb_健康档案.居住地址].ToString();
            _grdabh = dr[tb_健康档案.个人档案编号].ToString();
        }

        private void frm转档信息录入_Load(object sender, EventArgs e)
        {
            //为“镇”绑定信息
            DataView dv镇 = new DataView(DataDictCache.Cache.t地区信息);
            dv镇.RowFilter = "上级编码=371323";
            //DataBinder.BindingLookupEditDataSource(lookUpEdit镇, dv市.ToTable(), "地区名称", "地区编码");
            util.ControlsHelper.BindComboxData(dv镇.ToTable(), cbo镇, "地区编码", "地区名称");
        }

        private void cbo镇_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv村 = new DataView(DataDictCache.Cache.t地区信息);
                dv村.RowFilter = "上级编码='" + util.ControlsHelper.GetComboxKey(cbo镇) + "'";
                //DataBinder.BindingLookupEditDataSource(lookUpEdit村, dv县.ToTable(), "地区名称", "地区编码");
                util.ControlsHelper.BindComboxData(dv村.ToTable(), cbo村委会, "地区编码", "地区名称");
                util.ControlsHelper.SetComboxNullData("", cbo村委会, "请选择居/村委会");
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }

        private void btn发出申请_Click(object sender, EventArgs e)
        {
            string jd = util.ControlsHelper.GetComboxKey(this.cbo镇);
            if (string.IsNullOrEmpty(jd))
            {
                Msg.Warning("转入街道是必填项，请确认！");
                this.cbo镇.Focus();
                return;
            }
            string jwh = util.ControlsHelper.GetComboxKey(this.cbo村委会);
            if (string.IsNullOrEmpty(jwh))
            {
                Msg.Warning("转入居委会是必填项，请确认！");
                this.cbo村委会.Focus();
                return;
            }
            string xxdz = this.txt详细地址.Text.Trim();
            if (string.IsNullOrEmpty(xxdz))
            {
                Msg.Warning("转入详细地址是必填项，请确认！");
                this.txt详细地址.Focus();
                return;
            }
            string remark = this.txt申请原因.Text.Trim();
            if (string.IsNullOrEmpty(remark))
            {
                Msg.Warning("转入 申请原因 是必填项，请确认！");
                this.txt申请原因.Focus();
                return;
            }

            DataTable dt = _Bll.GetZDXX(_grdabh);//先查询一下是否处于转档状态
            if (dt == null || dt.Rows.Count <= 0)
            {
                string yhzgx = new bll健康档案().GetYHZGX(_grdabh);
                //if (Msg.AskQuestion("此人在家庭中为户主，如果"))
                //{
                    
                //}
                DataRow newRow = _ds.Tables[tb_转档申请.__TableName].Rows.Add();
                newRow[tb_转档申请.个人档案编号] = _grdabh;
                newRow[tb_转档申请.转入机构] = Loginer.CurrentUser.所属机构;
                newRow[tb_转档申请.转出机构] = _dr[tb_健康档案.所属机构];
                newRow[tb_转档申请.转入居委会] = jwh;
                newRow[tb_转档申请.创建人] = Loginer.CurrentUser.用户编码;
                newRow[tb_转档申请.HAPPENTIME] = _serverDate;
                newRow[tb_转档申请.类型] = "0";
                newRow[tb_转档申请.家庭档案编号] = _dr[tb_健康档案.家庭档案编号];
                newRow[tb_转档申请.转入详细地址] = xxdz;
                newRow[tb_转档申请.转出居委会] = _dr[tb_健康档案.居委会];
                newRow[tb_转档申请.转出详细地址] = _dr[tb_健康档案.居住地址];
                newRow[tb_转档申请.备注] = remark;
                //newRow[tb_转档申请.转档类型] = (yhzgx == "1" ? "0" : "1");//转档类型赋值：户主则为全家转档；否则为个人转档

            }
            //else
            //{
            //    Msg.ShowInformation("此人已经被申请转档");
            //}
            //TODO: 日志
            //_Bll.WriteLog();
            SaveResult result = _Bll.Save(_ds);
            if (result.Success)
            {
                this.Close();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else
            {
                Msg.Warning("发出转档申请失败，请联系管理员");
            }
        }
    }
}
