﻿using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.公共卫生.Module.个人健康;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.健康档案.转档管理
{
    public partial class frm批量转档 : frmBaseBusinessForm
    {
        bll转档申请 _bll = new bll转档申请();
        string _serverDateTime;
        DataTable dt;
        public frm批量转档()
        {
            InitializeComponent();
            _serverDateTime = _bll.ServiceDateTime;
        }

        private void frm批量转档_Load(object sender, EventArgs e)
        {
            //为“镇”绑定信息
            DataView dv镇 = new DataView(DataDictCache.Cache.t地区信息);
            dv镇.RowFilter = "上级编码=371323";
            //DataBinder.BindingLookupEditDataSource(lookUpEdit镇, dv市.ToTable(), "地区名称", "地区编码");
            util.ControlsHelper.BindComboxData(dv镇.ToTable(), cbo转出街道, "地区编码", "地区名称");
            util.ControlsHelper.BindComboxData(dv镇.ToTable(), cbo转入街道, "地区编码", "地区名称");

            if (Loginer.CurrentUser.所属机构.Length >= 12)
            {
                cbo转出机构.Properties.AutoExpandAllNodes = true;
            }
            else
            {
                cbo转出机构.Properties.AutoExpandAllNodes = false;
            }


            cbo转入机构.Properties.AutoExpandAllNodes = false;
            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                DataTable dt所属机构2 = bll机构.Get机构树("371323");

                cbo转入机构.Properties.ValueMember = "机构编号";
                cbo转入机构.Properties.DisplayMember = "机构名称";

                cbo转入机构.Properties.TreeList.KeyFieldName = "机构编号";
                cbo转入机构.Properties.TreeList.ParentFieldName = "上级机构";
                cbo转入机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                cbo转入机构.Properties.DataSource = dt所属机构2;
                cbo转入机构.EditValue = "371323";
                cbo转入机构.Properties.AutoExpandAllNodes = false;
                cbo转入机构.Properties.TreeList.CollapseAll();
                cbo转出机构.EditValue = Loginer.CurrentUser.所属机构;


                cbo转出机构.Properties.ValueMember = "机构编号";
                cbo转出机构.Properties.DisplayMember = "机构名称";

                cbo转出机构.Properties.TreeList.KeyFieldName = "机构编号";
                cbo转出机构.Properties.TreeList.ParentFieldName = "上级机构";
                cbo转出机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                cbo转出机构.Properties.DataSource = dt所属机构;
                //treeListLookUpEdit机
                //treeListLookUpEdit机构.Properties.TreeList.CollapseAll();
                if (!Loginer.CurrentUser.IsAdmin())
                    this.chk限定修改人.Enabled = false;
            }
            catch
            {
                cbo转出机构.Text = Loginer.CurrentUser.所属机构;
                cbo转入机构.Text = Loginer.CurrentUser.所属机构;
            }


        }

        private void cbo转出街道_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv村 = new DataView(DataDictCache.Cache.t地区信息);
                dv村.RowFilter = "上级编码='" + util.ControlsHelper.GetComboxKey(cbo转出街道) + "'";
                //DataBinder.BindingLookupEditDataSource(lookUpEdit村, dv县.ToTable(), "地区名称", "地区编码");
                util.ControlsHelper.BindComboxData(dv村.ToTable(), cbo转出村委会, "地区编码", "地区名称");
                util.ControlsHelper.SetComboxNullData("", cbo转出村委会, "请选择居/村委会");
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }
        private void cbo转入街道_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv村 = new DataView(DataDictCache.Cache.t地区信息);
                dv村.RowFilter = "上级编码='" + util.ControlsHelper.GetComboxKey(cbo转入街道) + "'";
                //DataBinder.BindingLookupEditDataSource(lookUpEdit村, dv县.ToTable(), "地区名称", "地区编码");
                util.ControlsHelper.BindComboxData(dv村.ToTable(), cbo转入村委会, "地区编码", "地区名称");
                util.ControlsHelper.SetComboxNullData("", cbo转入村委会, "请选择居/村委会");
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }
        private void btn批量转档_Click(object sender, EventArgs e)
        {
            string zcjd = util.ControlsHelper.GetComboxKey(this.cbo转出街道);
            string zcjwh = util.ControlsHelper.GetComboxKey(this.cbo转出村委会);
            string zrjd = util.ControlsHelper.GetComboxKey(this.cbo转入街道);
            string zrjwh = util.ControlsHelper.GetComboxKey(this.cbo转入村委会);
            string jzdz = this.textEdit地址.Text.ToString().Trim();

            string zcrgid = this.cbo转出机构.EditValue.ToString();
            string zrrgid = this.cbo转入机构.EditValue.ToString();
            if (!string.IsNullOrEmpty(zcrgid) && !string.IsNullOrEmpty(zrrgid))//选择了转入转出机构
            {
                #region 批量转档

                if (zcrgid.Length <= 6 || zrrgid.Length <= 6)
                {
                    Msg.Warning("转出、转入机构必须为服务中心或服务站！");
                    return;
                }
                if (zcrgid == zrrgid)
                {
                    Msg.Warning("转出、转入机构相同不能执行批量转档！");
                    return;
                }


                string zcjgname = this.cbo转出机构.Text.Trim();
                string zrjgname = this.cbo转入机构.Text.Trim();

                string zdxx = "机构“" + zcjgname + "”";
                string zcjdname = this.cbo转出街道.Text.Trim();
                string zcjwhname = this.cbo转出村委会.Text.Trim();
                string zrjdname = this.cbo转入街道.Text.Trim();
                string zrjwhname = this.cbo转入村委会.Text.Trim();
                if (zcjdname != "")
                {
                    zdxx = zdxx + "下面的”" + zcjdname + "“";
                }
                if (zcjwhname != "")
                {
                    zdxx = zdxx + "下面的”" + zcjwhname + "“";
                }
                zdxx = zdxx + "的档案批量导入到机构”" + zrjgname + "“内";
                if (zrjdname != "")
                {
                    zdxx = zdxx + "下面的”" + zrjdname + "“";
                }
                if (zrjwhname != "")
                {
                    zdxx = zdxx + "下面的”" + zrjwhname + "“";
                }
                zdxx = zdxx + "内";
                int countDa = new bll健康档案().SelectCountForJg(zcrgid, zcjd, zcjwh, chk限定修改人.Checked ? zrrgid : "");
                if (countDa != 0 && Msg.AskQuestion(zdxx + "！确定么？共【" + countDa.ToString()+"】条档案！"))
                {
                    String fkxx = "4";
                    SaveResult result = _bll.ConvertForRgid(zcrgid, zcjd, zcjwh, zrrgid, zrjd, zrjwh, chk限定修改人.Checked ? "1" : "", jzdz);
                    if (result.Success)
                    {
                        Msg.ShowInformation("批量转档成功，成功转入档案  " + countDa + "  条！");
                    }
                }
                else
                {
                    Msg.ShowInformation("未查询到需要转档的记录，请确认！");
                }

                #endregion
            }
            else
            {
                if (string.IsNullOrEmpty(zcrgid))
                {
                    Msg.Warning("转出机构是必选项，请选择转出机构！");
                    this.cbo转出机构.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(zcjd))
                {
                    Msg.Warning("转出街道是必选项，请选择转出街道！");
                    this.cbo转出街道.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(zcjwh))
                {
                    Msg.Warning("转出居委会是必选项，请选择转出居委会！");
                    this.cbo转出村委会.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(zrrgid))
                {
                    Msg.Warning("转入机构是必选项，请选择转入机构！");
                    this.cbo转出机构.Focus();
                    return;
                }
            }
        }
    }
}
