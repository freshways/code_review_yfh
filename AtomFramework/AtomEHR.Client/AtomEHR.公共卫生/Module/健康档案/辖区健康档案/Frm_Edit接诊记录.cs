﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;


namespace AtomEHR.公共卫生.Module.健康档案.辖区健康档案
{
    public partial class Frm_Edit接诊记录 : DevExpress.XtraEditors.XtraForm
    {
        #region 变量
        AtomEHR.Business.bll接诊记录 bll = new Business.bll接诊记录();
        DataTable dt个人档案 = null;
        private String _s档案号 = "";
        bool i是否打印 = false;
        DataRow _dr个人档案信息 = null;
        string _创建日期 = "";
        #endregion

        #region 构造函数
        public Frm_Edit接诊记录(DataRow dr)
        {
            InitializeComponent();
            _dr个人档案信息 = dr;
            //默认绑定
            textEdit档案号.Text = dr["个人档案编号"].ToString();
            this.textEdit姓名.Text = util.DESEncrypt.DES解密(dr["姓名"].ToString());
            this.textEdit性别.Text = dr["性别"].ToString();
            this.textEdit身份证号.Text = dr["身份证号"].ToString();
            this.textEdit出生日期.Text = dr["出生日期"].ToString();
            this.textEdit联系电话.Text = dr["本人电话"].ToString();
            this.textEdit居住地址.Text = dr["居住地址"].ToString();


            this.dateEdit接诊时间.Text = dr["接诊时间"].ToString();// dataSource[tb_接诊记录.接诊时间].ToString();
            this.textEdit接诊医生.Text = dr["接诊医生"].ToString();// dataSource[tb_接诊记录.接诊医生].ToString();
            this.textEdit主观资料.Text = dr["主观资料"].ToString();// dataSource[tb_接诊记录.主观资料].ToString();
            this.textEdit评估.Text = dr["评估"].ToString();// dataSource[tb_接诊记录.评估].ToString();
            this.textEdit客观资料.Text = dr["客观资料"].ToString();// dataSource[tb_接诊记录.客观资料].ToString();
            this.textEdit处置计划.Text = dr["处置计划"].ToString();// dataSource[tb_接诊记录.处置计划].ToString();
            //非编辑项            
            this.textEdit所属机构.Text = bll.Return机构名称(dr["所属机构"].ToString());// BLL.Return机构名称(dataSource[tb_接诊记录.所属机构].ToString());
            this.textEdit创建机构.Text = bll.Return机构名称(dr["创建机构"].ToString());// BLL.Return机构名称(dataSource[tb_接诊记录.创建机构].ToString());
            this.textEdit创建人.Text = bll.Return用户名称(dr["创建人"].ToString());// BLL.Return用户名称(dataSource[tb_接诊记录.创建人].ToString());
            this.textEdit创建时间.Text = dr["创建时间"].ToString();// dataSource[tb_接诊记录.创建时间].ToString();
            this.textEdit修改人.Text = bll.Return用户名称(dr["修改人"].ToString());// BLL.Return用户名称(dataSource[tb_接诊记录.修改人].ToString());
            this.textEdit更新时间.Text = dr["修改时间"].ToString();// dataSource[tb_接诊记录.修改时间].ToString();
        }
        #endregion

        #region Load

        public Frm_Edit接诊记录(string s_Grdnh)
        {
            InitializeComponent();
            this._s档案号 = s_Grdnh;
        }


        /// <summary>
        /// 页面加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Frm_Edit接诊记录_Load(object sender, EventArgs e)
        {
            dateEdit接诊时间.Text = System.DateTime.Now.ToString("yyyy-MM-dd");
            //this.textEdit接诊医生.Text = s操作人姓名;//.Focus();
            //EditBind();
        }

        #endregion

        #region 菜单事件
        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton打印_Click(object sender, EventArgs e)
        {
            //if (!i是否打印)
            //{
            //    MessageBox.Show("请先完善病人信息！", "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    return;
            //}
            Print();
        }

        #endregion

        #region 方法

        //private DataTable dt个人档案;
        //private void EditBind()
        //{
        //    try
        //    {
        //        if (_s档案号 == "") return;

        //        dt个人档案 = new DataTable();
        //        string sql = "SELECT ID ,档案号码 ,姓名 ,case 性别 when '1' then '男' else '女' end 性别,出生日期 ," +
        //            "身份证号 ,居住地址 ,本人电话,联系人电话 ,所属机构 ,创建时间 ,修改时间 ,创建人 ,修改人 ,创建机构  " +
        //            "FROM YSDB.dbo.T_健康档案  where 1=1  and (档案号码='" + this._s档案号 + "')";

        //        dt个人档案 = SqlHelper.ExecuteDataset(, CommandType.Text, sql).Tables ;
        //        if (dt个人档案 != null && dt个人档案.Rows.Count > 0)
        //        {
        //            this.textEdit档案号.Text = dt个人档案.Rows ["个人档案编号"].ToString();
        //            this.textEdit姓名.Text = Common.DES解密(dt个人档案.Rows ["姓名"].ToString());
        //            this.textEdit性别.Text = dt个人档案.Rows ["性别"].ToString();
        //            this.textEdit身份证号.Text = dt个人档案.Rows ["身份证号"].ToString();
        //            this.textEdit出生日期.Text = dt个人档案.Rows ["出生日期"].ToString();
        //            this.textEdit联系电话.Text = dt个人档案.Rows ["本人电话"].ToString();
        //            this.textEdit居住地址.Text = dt个人档案.Rows ["居住地址"].ToString();
        //            //dt个人档案.Rows ["所属机构"].ToString()
        //            this.textEdit所属机构.Text = Common.RetrunColunm("select 机构编号,机构名称 from dbo.tb_机构信息 where 机构编号='" + _s机构号 + "'; ", "机构名称");
        //            //dt个人档案.Rows ["创建机构"].ToString()
        //            this.textEdit创建机构.Text = Common.RetrunColunm("select 机构编号,机构名称 from dbo.tb_机构信息 where 机构编号='" + _s机构号 + "'; ", "机构名称");
        //            //绑定已有的接诊信息
        //            Bind接诊信息(dt个人档案.Rows ["个人档案编号"].ToString());
        //            Common.Log("查看病人接诊信息！操作人：" + s操作人);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show("打开病人信息失败！异常信息：" + e.Message, "异常消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //                  }
        //}

        private void Print()
        {
            try
            {
                int age = System.DateTime.Now.Year - Convert.ToDateTime(textEdit出生日期.Text).Year;
                List<string> list = new List<string>();
                list.Add(this.dateEdit接诊时间.Text.ToString());
                list.Add(this.textEdit接诊医生.Text.ToString());
                list.Add(this.textEdit主观资料.Text.ToString());
                list.Add(this.textEdit客观资料.Text.ToString());
                list.Add(this.textEdit评估.Text.ToString());
                list.Add(this.textEdit处置计划.Text.ToString());
                XtraReport接诊记录单 xrpt = new XtraReport接诊记录单(_dr个人档案信息, textEdit姓名.Text, textEdit性别.Text, age.ToString(), textEdit居住地址.Text);
                xrpt.ShowPreviewDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("打印失败！异常信息:\n" + ex.Message, "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        #endregion

        private void Frm_Edit接诊记录_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                SendKeys.Send("{Tab}");
            }
        }
    }
}
