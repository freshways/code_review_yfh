﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Interfaces;
using AtomEHR.公共卫生.Module.个人健康;

namespace AtomEHR.公共卫生.Module.健康档案.辖区健康档案
{
    public partial class frm接诊记录 : AtomEHR.Library.frmBaseBusinessForm
    {
        public frm接诊记录()
        {
            InitializeComponent();
        }

        private void frm接诊记录_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
        }

        #region Init初始化
        protected override void InitializeForm()
        {
            _BLL = new bll接诊记录();// 业务逻辑层实例
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gvSummary);
            BindingSummarySearchPanel(btnQuery, btnEmpty, gcFindGroup);

            //绑定相关缓存数据
            //DataBinder.BindingLookupEditDataSource(txt机构级别, DataDictCache.Cache.t机构级别, "NativeName", "DataCode");
            DataView dv县 = new DataView(DataDictCache.Cache.t地区信息);
            dv县.RowFilter = "上级编码=371323";
            DataBinder.BindingLookupEditDataSource(txt镇, dv县.ToTable(), "地区名称", "地区编码");
            txt镇.EditValueChanged += txt镇_EditValueChanged;
            DataBinder.BindingLookupEditDataSource(txt性别, DataDictCache.Cache.t性别, "P_DESC", "P_CODE");
            DataBinder.BindingLookupEditDataSource(txt档案状态, DataDictCache.Cache.t档案状态, "P_DESC", "P_CODE");
            txt档案状态.ItemIndex = 0;
            DataBinder.BindingLookupEditDataSource(txt合格档案, DataDictCache.Cache.t是否, "P_DESC", "P_CODE");
            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                txt机构.Properties.ValueMember = "机构编号";
                txt机构.Properties.DisplayMember = "机构名称";

                txt机构.Properties.TreeList.KeyFieldName = "机构编号";
                txt机构.Properties.TreeList.ParentFieldName = "上级机构";
                txt机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                txt机构.Properties.DataSource = dt所属机构;
                if (Loginer.CurrentUser.所属机构.Length >= 12)
                    txt机构.Properties.AutoExpandAllNodes = true;

                txt机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                txt机构.Text = Loginer.CurrentUser.所属机构;
            }

            _BLL.GetBusinessByKey("-", true);//加载一个空的业务对象.

            ShowSummaryPage(true); //一切初始化完毕后显示SummaryPage        
            this.pagerControl1.OnPageChanged += pagerControl1_OnPageChanged;
        }

        private void txt镇_EditValueChanged(object sender, EventArgs e)
        {
            DataView dv村 = new DataView(DataDictCache.Cache.t地区信息);
            dv村.RowFilter = "上级编码=" + txt镇.EditValue;
            DataBinder.BindingLookupEditDataSource(txt村, dv村.ToTable(), "地区名称", "地区编码");
        }

        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            DoSearchSummary();
        }
        #endregion

        private void link档案号_Click(object sender, EventArgs e)
        {
            DataRow row = this.gvSummary.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            string date = row["创建时间"] as string;
            //_BLL.GetBusinessByKey(个人档案编号, true);//下载一个空业务单据            
            //_BLL.NewBusiness(); //增加一条主表记录
            frm个人健康 frm = new frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, date);
            frm.ShowDialog();
        }

        /// <summary>
        /// 绑定列表数据
        /// </summary>
        /// <returns></returns>
        protected override bool DoSearchSummary()
        {
            //DataTable dt = (_BLL as bll接诊记录).GetSummaryByParam("and 1=1");
            string _strWhere = "";
            if (txt机构.Text != "" && txt机构.EditValue != null)
            {
                string pgrid = txt机构.EditValue.ToString();
                if (ck包含下属机构.Checked)
                {
                    if (pgrid.Length == 12)
                        _strWhere += " and ([所属机构]=  '" + pgrid + "' or substring([所属机构],1,7)+'1'+substring([所属机构],9,7) like '" + pgrid + "%')";
                    else
                        _strWhere += " and [所属机构] like '" + pgrid + "%'";
                }
                else
                    _strWhere += " and 所属机构='" + pgrid + "'";
            }
            if (txt姓名.Text != "")
                _strWhere += " and (姓名 like'" + txt姓名.Text + "%') ";
            if (txt性别.EditValue != null && txt性别.Text != "")
                _strWhere += " and 性别='" + txt性别.Text.ToString() + "'";
            if (txt档案编号.Text != "")
                _strWhere += " and 个人档案编号='" + txt档案编号.Text.ToString() + "'";
            if (txt出生日期1.Text != "")
                _strWhere += " and 接诊时间>='" + txt出生日期1.Text.ToString() + "'";
            if (txt出生日期2.Text != "")
                _strWhere += " and 接诊时间<='" + txt出生日期2.Text.ToString() + "'";
            if (txt档案状态.EditValue != null && txt档案状态.Text != "")
                _strWhere += " and 档案状态='" + txt档案状态.EditValue.ToString() + "'";
            if (txt身份证.Text != "")
                _strWhere += " and 身份证号='" + txt身份证.Text.ToString() + "'";
            if (txt录入时间1.Text != "")
                _strWhere += " and 创建时间>='" + txt录入时间1.Text.ToString() + "'";
            if (txt录入时间2.Text != "")
                _strWhere += " and 创建时间<='" + txt录入时间2.Text.ToString() + " 23:59:59'";
            if (txt合格档案.EditValue != null && txt合格档案.Text != "")
            {
                if (txt合格档案.EditValue.ToString() == "1")
                    _strWhere += " and 完整度=100";
                else
                    _strWhere += " and 完整度<>100";
            }
            if (txt镇.EditValue != null && txt镇.Text != "")
                _strWhere += " and 街道编码='" + txt镇.EditValue.ToString() + "'";
            if (txt村.EditValue != null && txt村.Text != "")
                _strWhere += " and 居委会编码='" + txt村.EditValue.ToString() + "'";
            if (txt地址.Text != "")
                _strWhere += " and 居住地址='" + txt地址.Text.ToString() + "'";

            DataTable dt = this.pagerControl1.GetQueryResult("vw_接诊记录", "*", _strWhere, "ID", "desc").Tables[0];
            this.pagerControl1.DrawControl();

            foreach (DataRow dr in dt.Rows)
                dr[tb_健康档案.姓名] = util.DESEncrypt.DES解密(dr[tb_健康档案.姓名].ToString());

            DoBindingSummaryGrid(dt); //绑定主表的Grid
            ShowSummaryPage(true); //显示Summary页面. 

            this.gcSummary.View = "vw_接诊记录";
            this.gcSummary.StrWhere = _strWhere;

            return dt != null && dt.Rows.Count > 0;
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            this.pagerControl1.InitControl();
        }

        private void btn打印_Click(object sender, EventArgs e)
        {
            if (this.gvSummary.SelectedRowsCount != 1)
            {
                Msg.ShowInformation("每次只能打印一个人的接诊记录表，请确认！");
                return;
            }

            if (gvSummary.GetSelectedRows() != null && gvSummary.GetSelectedRows().Length > 0)
            {
                string s档案号 = this.gvSummary.GetRowCellValue(this.gvSummary.GetSelectedRows()[0], "个人档案编号").ToString();
                //string sDate = this.gvSummary.GetRowCellValue(this.gvSummary.GetSelectedRows()[0], "").ToString();
                DataRow row = this.gvSummary.GetFocusedDataRow();

                Frm_Edit接诊记录 frm = new Frm_Edit接诊记录(row);
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("没有人员信息！是否查询?", "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

    }
}
