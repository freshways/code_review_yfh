﻿namespace AtomEHR.公共卫生.Module.健康档案.辖区健康档案
{
    partial class report体检
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel尿胴体 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel尿潜血 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel尿常规其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel空腹血糖1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel空腹血糖2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel心电图 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel78 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel尿微量白蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel大便潜血 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel糖化血红蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel乙肝 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血清谷草转氨酶 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel宫体异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel附件异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel外阴 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel阴道 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel宫颈 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel宫体 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel附件 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel查体其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血红蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel白细胞 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血小板 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血常规其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel尿蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel尿糖 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel压痛 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel包块 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel肝大 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel脾大 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel移动性浊音有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel移动性浊音 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel下肢水肿 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel足背动脉 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel肛门指诊 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel肛门指诊其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel乳腺1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel乳腺2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel乳腺4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel乳腺3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel乳腺其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel外阴异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel阴道异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel宫颈异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel心率 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel罗音其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel心律 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel杂音 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel杂音有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel压痛有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel包块有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel肝大有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel脾大有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel眼底异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel皮肤 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel皮肤异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel巩膜 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel巩膜异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel淋巴结 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel桶状胸 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel呼吸音 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel淋巴结其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel呼吸音异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel龋齿3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel龋齿4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel义齿1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel义齿2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel义齿3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel义齿4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel左眼 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel右眼 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel左眼矫正 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel右眼矫正 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel听力 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel咽部 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel运动功能 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel眼底 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel缺齿3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel缺齿4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel龋齿1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel龋齿2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel缺齿2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel缺齿1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel齿列 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel口腔 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrLabel辅助检查其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血清高密度脂蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血清低密度脂蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel甘油三酯 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel总胆固醇 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血钠浓度 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血尿素氮 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血钾浓度 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血清肌酐 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel结合胆红素 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel总胆红素 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel白蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血清谷丙转氨酶 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel气郁质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血瘀质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel湿热质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel痰湿质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel阴虚质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel阳虚质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel气虚质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel平和质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel宫颈涂片 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelB超 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelX线片 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel特禀质 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.BorderWidth = 0F;
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel特禀质,
            this.xrLabel19,
            this.xrLabel18,
            this.xrLabel17,
            this.xrLabel16,
            this.xrLabel15,
            this.xrLabel14,
            this.xrLabel13,
            this.xrLabel1,
            this.xrLabelX线片,
            this.xrLabelB超,
            this.xrLabel宫颈涂片,
            this.xrLabel平和质,
            this.xrLabel气虚质,
            this.xrLabel阳虚质,
            this.xrLabel阴虚质,
            this.xrLabel痰湿质,
            this.xrLabel湿热质,
            this.xrLabel血瘀质,
            this.xrLabel气郁质,
            this.xrLabel血清谷丙转氨酶,
            this.xrLabel白蛋白,
            this.xrLabel总胆红素,
            this.xrLabel结合胆红素,
            this.xrLabel血清肌酐,
            this.xrLabel血钾浓度,
            this.xrLabel血尿素氮,
            this.xrLabel血钠浓度,
            this.xrLabel总胆固醇,
            this.xrLabel甘油三酯,
            this.xrLabel血清低密度脂蛋白,
            this.xrLabel血清高密度脂蛋白,
            this.xrLabel4,
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel辅助检查其他,
            this.xrLabel尿胴体,
            this.xrLabel尿潜血,
            this.xrLabel尿常规其他,
            this.xrLabel空腹血糖1,
            this.xrLabel空腹血糖2,
            this.xrLabel心电图,
            this.xrLabel78,
            this.xrLabel尿微量白蛋白,
            this.xrLabel大便潜血,
            this.xrLabel糖化血红蛋白,
            this.xrLabel乙肝,
            this.xrLabel血清谷草转氨酶,
            this.xrLabel宫体异常,
            this.xrLabel附件异常,
            this.xrLabel外阴,
            this.xrLabel阴道,
            this.xrLabel宫颈,
            this.xrLabel宫体,
            this.xrLabel附件,
            this.xrLabel查体其他,
            this.xrLabel血红蛋白,
            this.xrLabel白细胞,
            this.xrLabel血小板,
            this.xrLabel血常规其他,
            this.xrLabel尿蛋白,
            this.xrLabel尿糖,
            this.xrLabel压痛,
            this.xrLabel包块,
            this.xrLabel肝大,
            this.xrLabel脾大,
            this.xrLabel移动性浊音有,
            this.xrLabel移动性浊音,
            this.xrLabel下肢水肿,
            this.xrLabel足背动脉,
            this.xrLabel肛门指诊,
            this.xrLabel肛门指诊其他,
            this.xrLabel乳腺1,
            this.xrLabel乳腺2,
            this.xrLabel乳腺4,
            this.xrLabel乳腺3,
            this.xrLabel乳腺其他,
            this.xrLabel外阴异常,
            this.xrLabel阴道异常,
            this.xrLabel宫颈异常,
            this.xrLabel心率,
            this.xrLabel39,
            this.xrLabel罗音其他,
            this.xrLabel心律,
            this.xrLabel杂音,
            this.xrLabel杂音有,
            this.xrLabel压痛有,
            this.xrLabel包块有,
            this.xrLabel肝大有,
            this.xrLabel脾大有,
            this.xrLabel眼底异常,
            this.xrLabel皮肤,
            this.xrLabel皮肤异常,
            this.xrLabel巩膜,
            this.xrLabel巩膜异常,
            this.xrLabel淋巴结,
            this.xrLabel桶状胸,
            this.xrLabel呼吸音,
            this.xrLabel淋巴结其他,
            this.xrLabel呼吸音异常,
            this.xrLabel龋齿3,
            this.xrLabel龋齿4,
            this.xrLabel义齿1,
            this.xrLabel义齿2,
            this.xrLabel义齿3,
            this.xrLabel义齿4,
            this.xrLabel左眼,
            this.xrLabel右眼,
            this.xrLabel左眼矫正,
            this.xrLabel右眼矫正,
            this.xrLabel听力,
            this.xrLabel咽部,
            this.xrLabel运动功能,
            this.xrLabel眼底,
            this.xrLabel缺齿3,
            this.xrLabel缺齿4,
            this.xrLabel龋齿1,
            this.xrLabel龋齿2,
            this.xrLabel缺齿2,
            this.xrLabel缺齿1,
            this.xrLabel齿列,
            this.xrLabel口腔,
            this.xrPictureBox1});
            this.Detail.HeightF = 1402F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseBorderWidth = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel尿胴体
            // 
            this.xrLabel尿胴体.LocationFloat = new DevExpress.Utils.PointFloat(554.1666F, 860.4999F);
            this.xrLabel尿胴体.Name = "xrLabel尿胴体";
            this.xrLabel尿胴体.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel尿胴体.SizeF = new System.Drawing.SizeF(67.29187F, 13.625F);
            this.xrLabel尿胴体.Text = "尿胴体";
            // 
            // xrLabel尿潜血
            // 
            this.xrLabel尿潜血.LocationFloat = new DevExpress.Utils.PointFloat(677.0833F, 860.4999F);
            this.xrLabel尿潜血.Name = "xrLabel尿潜血";
            this.xrLabel尿潜血.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel尿潜血.SizeF = new System.Drawing.SizeF(64.5835F, 13.625F);
            this.xrLabel尿潜血.Text = "尿潜血";
            // 
            // xrLabel尿常规其他
            // 
            this.xrLabel尿常规其他.LocationFloat = new DevExpress.Utils.PointFloat(311.4583F, 884.4579F);
            this.xrLabel尿常规其他.Name = "xrLabel尿常规其他";
            this.xrLabel尿常规其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel尿常规其他.SizeF = new System.Drawing.SizeF(224.9999F, 13.625F);
            this.xrLabel尿常规其他.Text = "尿常规其他";
            // 
            // xrLabel空腹血糖1
            // 
            this.xrLabel空腹血糖1.LocationFloat = new DevExpress.Utils.PointFloat(279.7914F, 903.125F);
            this.xrLabel空腹血糖1.Name = "xrLabel空腹血糖1";
            this.xrLabel空腹血糖1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel空腹血糖1.SizeF = new System.Drawing.SizeF(87.5F, 13.625F);
            this.xrLabel空腹血糖1.Text = "空腹血糖1";
            // 
            // xrLabel空腹血糖2
            // 
            this.xrLabel空腹血糖2.LocationFloat = new DevExpress.Utils.PointFloat(501.4583F, 903.125F);
            this.xrLabel空腹血糖2.Name = "xrLabel空腹血糖2";
            this.xrLabel空腹血糖2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel空腹血糖2.SizeF = new System.Drawing.SizeF(90.62476F, 13.625F);
            this.xrLabel空腹血糖2.Text = "空腹血糖2";
            // 
            // xrLabel心电图
            // 
            this.xrLabel心电图.LocationFloat = new DevExpress.Utils.PointFloat(746.5845F, 933.2449F);
            this.xrLabel心电图.Name = "xrLabel心电图";
            this.xrLabel心电图.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel心电图.SizeF = new System.Drawing.SizeF(16.66663F, 13.625F);
            this.xrLabel心电图.Text = "1";
            // 
            // xrLabel78
            // 
            this.xrLabel78.LocationFloat = new DevExpress.Utils.PointFloat(377.7082F, 931.25F);
            this.xrLabel78.Name = "xrLabel78";
            this.xrLabel78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel78.SizeF = new System.Drawing.SizeF(227.0834F, 13.625F);
            this.xrLabel78.Text = "心电图异常";
            // 
            // xrLabel尿微量白蛋白
            // 
            this.xrLabel尿微量白蛋白.LocationFloat = new DevExpress.Utils.PointFloat(1263.542F, 123.9583F);
            this.xrLabel尿微量白蛋白.Name = "xrLabel尿微量白蛋白";
            this.xrLabel尿微量白蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel尿微量白蛋白.SizeF = new System.Drawing.SizeF(111.4584F, 13.62501F);
            this.xrLabel尿微量白蛋白.Text = "尿微量白蛋白";
            // 
            // xrLabel大便潜血
            // 
            this.xrLabel大便潜血.LocationFloat = new DevExpress.Utils.PointFloat(1565.625F, 160F);
            this.xrLabel大便潜血.Name = "xrLabel大便潜血";
            this.xrLabel大便潜血.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel大便潜血.SizeF = new System.Drawing.SizeF(16.66663F, 13.625F);
            this.xrLabel大便潜血.Text = "1";
            // 
            // xrLabel糖化血红蛋白
            // 
            this.xrLabel糖化血红蛋白.LocationFloat = new DevExpress.Utils.PointFloat(1103.125F, 180.83F);
            this.xrLabel糖化血红蛋白.Name = "xrLabel糖化血红蛋白";
            this.xrLabel糖化血红蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel糖化血红蛋白.SizeF = new System.Drawing.SizeF(84.375F, 13.62498F);
            this.xrLabel糖化血红蛋白.Text = "1";
            this.xrLabel糖化血红蛋白.WordWrap = false;
            // 
            // xrLabel乙肝
            // 
            this.xrLabel乙肝.LocationFloat = new DevExpress.Utils.PointFloat(1565.625F, 210.21F);
            this.xrLabel乙肝.Name = "xrLabel乙肝";
            this.xrLabel乙肝.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel乙肝.SizeF = new System.Drawing.SizeF(16.66663F, 13.625F);
            this.xrLabel乙肝.Text = "1";
            // 
            // xrLabel血清谷草转氨酶
            // 
            this.xrLabel血清谷草转氨酶.LocationFloat = new DevExpress.Utils.PointFloat(1475F, 233F);
            this.xrLabel血清谷草转氨酶.Name = "xrLabel血清谷草转氨酶";
            this.xrLabel血清谷草转氨酶.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血清谷草转氨酶.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel血清谷草转氨酶.Text = "2";
            // 
            // xrLabel宫体异常
            // 
            this.xrLabel宫体异常.LocationFloat = new DevExpress.Utils.PointFloat(417.7084F, 730F);
            this.xrLabel宫体异常.Name = "xrLabel宫体异常";
            this.xrLabel宫体异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel宫体异常.SizeF = new System.Drawing.SizeF(186.4584F, 13.625F);
            this.xrLabel宫体异常.Text = "宫体异常";
            // 
            // xrLabel附件异常
            // 
            this.xrLabel附件异常.LocationFloat = new DevExpress.Utils.PointFloat(417.7084F, 755F);
            this.xrLabel附件异常.Name = "xrLabel附件异常";
            this.xrLabel附件异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel附件异常.SizeF = new System.Drawing.SizeF(186.4584F, 13.625F);
            this.xrLabel附件异常.Text = "附件异常";
            // 
            // xrLabel外阴
            // 
            this.xrLabel外阴.LocationFloat = new DevExpress.Utils.PointFloat(745.9597F, 660F);
            this.xrLabel外阴.Name = "xrLabel外阴";
            this.xrLabel外阴.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel外阴.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel外阴.Text = "1";
            // 
            // xrLabel阴道
            // 
            this.xrLabel阴道.LocationFloat = new DevExpress.Utils.PointFloat(745.9597F, 683F);
            this.xrLabel阴道.Name = "xrLabel阴道";
            this.xrLabel阴道.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel阴道.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel阴道.Text = "1";
            // 
            // xrLabel宫颈
            // 
            this.xrLabel宫颈.LocationFloat = new DevExpress.Utils.PointFloat(745.9597F, 708F);
            this.xrLabel宫颈.Name = "xrLabel宫颈";
            this.xrLabel宫颈.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel宫颈.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel宫颈.Text = "1";
            // 
            // xrLabel宫体
            // 
            this.xrLabel宫体.LocationFloat = new DevExpress.Utils.PointFloat(745.9597F, 730F);
            this.xrLabel宫体.Name = "xrLabel宫体";
            this.xrLabel宫体.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel宫体.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel宫体.Text = "1";
            // 
            // xrLabel附件
            // 
            this.xrLabel附件.LocationFloat = new DevExpress.Utils.PointFloat(745.9597F, 755F);
            this.xrLabel附件.Name = "xrLabel附件";
            this.xrLabel附件.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel附件.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel附件.Text = "1";
            // 
            // xrLabel查体其他
            // 
            this.xrLabel查体其他.LocationFloat = new DevExpress.Utils.PointFloat(279.1666F, 775F);
            this.xrLabel查体其他.Name = "xrLabel查体其他";
            this.xrLabel查体其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel查体其他.SizeF = new System.Drawing.SizeF(370.8333F, 31.33331F);
            this.xrLabel查体其他.Text = "查体其他";
            // 
            // xrLabel血红蛋白
            // 
            this.xrLabel血红蛋白.LocationFloat = new DevExpress.Utils.PointFloat(339.5833F, 823.9583F);
            this.xrLabel血红蛋白.Name = "xrLabel血红蛋白";
            this.xrLabel血红蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血红蛋白.SizeF = new System.Drawing.SizeF(64.58334F, 13.625F);
            this.xrLabel血红蛋白.Text = "血红蛋白";
            // 
            // xrLabel白细胞
            // 
            this.xrLabel白细胞.LocationFloat = new DevExpress.Utils.PointFloat(501.4583F, 823.9583F);
            this.xrLabel白细胞.Name = "xrLabel白细胞";
            this.xrLabel白细胞.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel白细胞.SizeF = new System.Drawing.SizeF(75.00009F, 13.625F);
            this.xrLabel白细胞.Text = "白细胞";
            // 
            // xrLabel血小板
            // 
            this.xrLabel血小板.LocationFloat = new DevExpress.Utils.PointFloat(661.4583F, 823.9583F);
            this.xrLabel血小板.Name = "xrLabel血小板";
            this.xrLabel血小板.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血小板.SizeF = new System.Drawing.SizeF(75.20837F, 13.625F);
            this.xrLabel血小板.Text = "血小板";
            // 
            // xrLabel血常规其他
            // 
            this.xrLabel血常规其他.LocationFloat = new DevExpress.Utils.PointFloat(306.2499F, 840F);
            this.xrLabel血常规其他.Name = "xrLabel血常规其他";
            this.xrLabel血常规其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血常规其他.SizeF = new System.Drawing.SizeF(246.8751F, 13.625F);
            this.xrLabel血常规其他.Text = "血常规其他";
            // 
            // xrLabel尿蛋白
            // 
            this.xrLabel尿蛋白.LocationFloat = new DevExpress.Utils.PointFloat(329.1666F, 860.4999F);
            this.xrLabel尿蛋白.Name = "xrLabel尿蛋白";
            this.xrLabel尿蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel尿蛋白.SizeF = new System.Drawing.SizeF(55.2084F, 13.625F);
            this.xrLabel尿蛋白.Text = "尿蛋白";
            // 
            // xrLabel尿糖
            // 
            this.xrLabel尿糖.LocationFloat = new DevExpress.Utils.PointFloat(431.2499F, 860.4999F);
            this.xrLabel尿糖.Name = "xrLabel尿糖";
            this.xrLabel尿糖.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel尿糖.SizeF = new System.Drawing.SizeF(76.04166F, 13.625F);
            this.xrLabel尿糖.Text = "尿糖";
            // 
            // xrLabel压痛
            // 
            this.xrLabel压痛.LocationFloat = new DevExpress.Utils.PointFloat(745.9597F, 482.7917F);
            this.xrLabel压痛.Name = "xrLabel压痛";
            this.xrLabel压痛.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel压痛.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel压痛.Text = "1";
            // 
            // xrLabel包块
            // 
            this.xrLabel包块.LocationFloat = new DevExpress.Utils.PointFloat(745.9597F, 500F);
            this.xrLabel包块.Name = "xrLabel包块";
            this.xrLabel包块.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel包块.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel包块.Text = "1";
            // 
            // xrLabel肝大
            // 
            this.xrLabel肝大.LocationFloat = new DevExpress.Utils.PointFloat(745.9595F, 520F);
            this.xrLabel肝大.Name = "xrLabel肝大";
            this.xrLabel肝大.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel肝大.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel肝大.Text = "1";
            // 
            // xrLabel脾大
            // 
            this.xrLabel脾大.LocationFloat = new DevExpress.Utils.PointFloat(745.9595F, 540F);
            this.xrLabel脾大.Name = "xrLabel脾大";
            this.xrLabel脾大.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel脾大.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel脾大.Text = "1";
            // 
            // xrLabel移动性浊音有
            // 
            this.xrLabel移动性浊音有.LocationFloat = new DevExpress.Utils.PointFloat(440F, 560F);
            this.xrLabel移动性浊音有.Name = "xrLabel移动性浊音有";
            this.xrLabel移动性浊音有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel移动性浊音有.SizeF = new System.Drawing.SizeF(96.45847F, 13.625F);
            this.xrLabel移动性浊音有.Text = "移动性浊音";
            // 
            // xrLabel移动性浊音
            // 
            this.xrLabel移动性浊音.LocationFloat = new DevExpress.Utils.PointFloat(745.9597F, 560F);
            this.xrLabel移动性浊音.Name = "xrLabel移动性浊音";
            this.xrLabel移动性浊音.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel移动性浊音.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel移动性浊音.Text = "1";
            // 
            // xrLabel下肢水肿
            // 
            this.xrLabel下肢水肿.LocationFloat = new DevExpress.Utils.PointFloat(745.9598F, 580F);
            this.xrLabel下肢水肿.Name = "xrLabel下肢水肿";
            this.xrLabel下肢水肿.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel下肢水肿.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel下肢水肿.Text = "1";
            // 
            // xrLabel足背动脉
            // 
            this.xrLabel足背动脉.LocationFloat = new DevExpress.Utils.PointFloat(745.9597F, 600F);
            this.xrLabel足背动脉.Name = "xrLabel足背动脉";
            this.xrLabel足背动脉.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel足背动脉.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel足背动脉.Text = "1";
            // 
            // xrLabel肛门指诊
            // 
            this.xrLabel肛门指诊.LocationFloat = new DevExpress.Utils.PointFloat(745.9597F, 620F);
            this.xrLabel肛门指诊.Name = "xrLabel肛门指诊";
            this.xrLabel肛门指诊.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel肛门指诊.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel肛门指诊.Text = "1";
            // 
            // xrLabel肛门指诊其他
            // 
            this.xrLabel肛门指诊其他.LocationFloat = new DevExpress.Utils.PointFloat(661.4583F, 620F);
            this.xrLabel肛门指诊其他.Name = "xrLabel肛门指诊其他";
            this.xrLabel肛门指诊其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel肛门指诊其他.SizeF = new System.Drawing.SizeF(75.20837F, 13.625F);
            this.xrLabel肛门指诊其他.Text = "肛门指诊";
            // 
            // xrLabel乳腺1
            // 
            this.xrLabel乳腺1.LocationFloat = new DevExpress.Utils.PointFloat(675F, 644.7917F);
            this.xrLabel乳腺1.Name = "xrLabel乳腺1";
            this.xrLabel乳腺1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel乳腺1.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel乳腺1.Text = "1";
            // 
            // xrLabel乳腺2
            // 
            this.xrLabel乳腺2.LocationFloat = new DevExpress.Utils.PointFloat(698.125F, 644.7917F);
            this.xrLabel乳腺2.Name = "xrLabel乳腺2";
            this.xrLabel乳腺2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel乳腺2.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel乳腺2.Text = "1";
            // 
            // xrLabel乳腺4
            // 
            this.xrLabel乳腺4.LocationFloat = new DevExpress.Utils.PointFloat(749.0847F, 644.7917F);
            this.xrLabel乳腺4.Name = "xrLabel乳腺4";
            this.xrLabel乳腺4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel乳腺4.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel乳腺4.Text = "1";
            // 
            // xrLabel乳腺3
            // 
            this.xrLabel乳腺3.LocationFloat = new DevExpress.Utils.PointFloat(723.125F, 644.7917F);
            this.xrLabel乳腺3.Name = "xrLabel乳腺3";
            this.xrLabel乳腺3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel乳腺3.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel乳腺3.Text = "1";
            // 
            // xrLabel乳腺其他
            // 
            this.xrLabel乳腺其他.LocationFloat = new DevExpress.Utils.PointFloat(635.4167F, 644.7917F);
            this.xrLabel乳腺其他.Name = "xrLabel乳腺其他";
            this.xrLabel乳腺其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel乳腺其他.SizeF = new System.Drawing.SizeF(39.58331F, 13.625F);
            this.xrLabel乳腺其他.Text = "乳腺其他";
            this.xrLabel乳腺其他.WordWrap = false;
            // 
            // xrLabel外阴异常
            // 
            this.xrLabel外阴异常.LocationFloat = new DevExpress.Utils.PointFloat(417.7084F, 660F);
            this.xrLabel外阴异常.Name = "xrLabel外阴异常";
            this.xrLabel外阴异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel外阴异常.SizeF = new System.Drawing.SizeF(186.4584F, 13.625F);
            this.xrLabel外阴异常.Text = "外阴异常";
            // 
            // xrLabel阴道异常
            // 
            this.xrLabel阴道异常.LocationFloat = new DevExpress.Utils.PointFloat(417.7083F, 683F);
            this.xrLabel阴道异常.Name = "xrLabel阴道异常";
            this.xrLabel阴道异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel阴道异常.SizeF = new System.Drawing.SizeF(186.4584F, 13.625F);
            this.xrLabel阴道异常.Text = "阴道异常";
            // 
            // xrLabel宫颈异常
            // 
            this.xrLabel宫颈异常.LocationFloat = new DevExpress.Utils.PointFloat(417.7084F, 708F);
            this.xrLabel宫颈异常.Name = "xrLabel宫颈异常";
            this.xrLabel宫颈异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel宫颈异常.SizeF = new System.Drawing.SizeF(186.4584F, 13.625F);
            this.xrLabel宫颈异常.Text = "宫颈异常";
            // 
            // xrLabel心率
            // 
            this.xrLabel心率.LocationFloat = new DevExpress.Utils.PointFloat(339.5833F, 440F);
            this.xrLabel心率.Name = "xrLabel心率";
            this.xrLabel心率.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel心率.SizeF = new System.Drawing.SizeF(44.79172F, 13.625F);
            this.xrLabel心率.Text = "心率";
            // 
            // xrLabel39
            // 
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(748.9583F, 420F);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel39.Text = "1";
            // 
            // xrLabel罗音其他
            // 
            this.xrLabel罗音其他.LocationFloat = new DevExpress.Utils.PointFloat(580.2083F, 420F);
            this.xrLabel罗音其他.Name = "xrLabel罗音其他";
            this.xrLabel罗音其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel罗音其他.SizeF = new System.Drawing.SizeF(57.29175F, 13.625F);
            this.xrLabel罗音其他.Text = "罗音";
            // 
            // xrLabel心律
            // 
            this.xrLabel心律.LocationFloat = new DevExpress.Utils.PointFloat(745.96F, 440F);
            this.xrLabel心律.Name = "xrLabel心律";
            this.xrLabel心律.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel心律.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel心律.Text = "1";
            // 
            // xrLabel杂音
            // 
            this.xrLabel杂音.LocationFloat = new DevExpress.Utils.PointFloat(745.96F, 458.75F);
            this.xrLabel杂音.Name = "xrLabel杂音";
            this.xrLabel杂音.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel杂音.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel杂音.Text = "1";
            // 
            // xrLabel杂音有
            // 
            this.xrLabel杂音有.LocationFloat = new DevExpress.Utils.PointFloat(417.7083F, 458.75F);
            this.xrLabel杂音有.Name = "xrLabel杂音有";
            this.xrLabel杂音有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel杂音有.SizeF = new System.Drawing.SizeF(78.12503F, 13.625F);
            this.xrLabel杂音有.Text = "杂音有";
            // 
            // xrLabel压痛有
            // 
            this.xrLabel压痛有.LocationFloat = new DevExpress.Utils.PointFloat(388.5414F, 482.7917F);
            this.xrLabel压痛有.Name = "xrLabel压痛有";
            this.xrLabel压痛有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel压痛有.SizeF = new System.Drawing.SizeF(87.5F, 13.625F);
            this.xrLabel压痛有.Text = "压痛";
            // 
            // xrLabel包块有
            // 
            this.xrLabel包块有.LocationFloat = new DevExpress.Utils.PointFloat(388.5414F, 500F);
            this.xrLabel包块有.Name = "xrLabel包块有";
            this.xrLabel包块有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel包块有.SizeF = new System.Drawing.SizeF(87.5F, 13.625F);
            this.xrLabel包块有.Text = "包块";
            // 
            // xrLabel肝大有
            // 
            this.xrLabel肝大有.LocationFloat = new DevExpress.Utils.PointFloat(388.5414F, 520F);
            this.xrLabel肝大有.Name = "xrLabel肝大有";
            this.xrLabel肝大有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel肝大有.SizeF = new System.Drawing.SizeF(87.49997F, 13.625F);
            this.xrLabel肝大有.Text = "肝大";
            // 
            // xrLabel脾大有
            // 
            this.xrLabel脾大有.LocationFloat = new DevExpress.Utils.PointFloat(388.5413F, 540F);
            this.xrLabel脾大有.Name = "xrLabel脾大有";
            this.xrLabel脾大有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel脾大有.SizeF = new System.Drawing.SizeF(87.5F, 13.625F);
            this.xrLabel脾大有.Text = "脾大";
            // 
            // xrLabel眼底异常
            // 
            this.xrLabel眼底异常.LocationFloat = new DevExpress.Utils.PointFloat(380F, 260.5F);
            this.xrLabel眼底异常.Name = "xrLabel眼底异常";
            this.xrLabel眼底异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel眼底异常.SizeF = new System.Drawing.SizeF(176.0417F, 13.625F);
            this.xrLabel眼底异常.Text = "1";
            // 
            // xrLabel皮肤
            // 
            this.xrLabel皮肤.LocationFloat = new DevExpress.Utils.PointFloat(748.9583F, 290F);
            this.xrLabel皮肤.Name = "xrLabel皮肤";
            this.xrLabel皮肤.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel皮肤.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel皮肤.Text = "1";
            // 
            // xrLabel皮肤异常
            // 
            this.xrLabel皮肤异常.LocationFloat = new DevExpress.Utils.PointFloat(680F, 290F);
            this.xrLabel皮肤异常.Name = "xrLabel皮肤异常";
            this.xrLabel皮肤异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel皮肤异常.SizeF = new System.Drawing.SizeF(50F, 13.625F);
            this.xrLabel皮肤异常.Text = "皮肤";
            // 
            // xrLabel巩膜
            // 
            this.xrLabel巩膜.LocationFloat = new DevExpress.Utils.PointFloat(748.9581F, 320F);
            this.xrLabel巩膜.Name = "xrLabel巩膜";
            this.xrLabel巩膜.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel巩膜.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel巩膜.Text = "1";
            // 
            // xrLabel巩膜异常
            // 
            this.xrLabel巩膜异常.LocationFloat = new DevExpress.Utils.PointFloat(490F, 320F);
            this.xrLabel巩膜异常.Name = "xrLabel巩膜异常";
            this.xrLabel巩膜异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel巩膜异常.SizeF = new System.Drawing.SizeF(86.45831F, 13.625F);
            this.xrLabel巩膜异常.Text = "巩膜";
            // 
            // xrLabel淋巴结
            // 
            this.xrLabel淋巴结.LocationFloat = new DevExpress.Utils.PointFloat(748.9583F, 340F);
            this.xrLabel淋巴结.Name = "xrLabel淋巴结";
            this.xrLabel淋巴结.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel淋巴结.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel淋巴结.Text = "1";
            // 
            // xrLabel桶状胸
            // 
            this.xrLabel桶状胸.LocationFloat = new DevExpress.Utils.PointFloat(748.9583F, 370F);
            this.xrLabel桶状胸.Name = "xrLabel桶状胸";
            this.xrLabel桶状胸.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel桶状胸.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel桶状胸.Text = "1";
            // 
            // xrLabel呼吸音
            // 
            this.xrLabel呼吸音.LocationFloat = new DevExpress.Utils.PointFloat(748.9583F, 400F);
            this.xrLabel呼吸音.Name = "xrLabel呼吸音";
            this.xrLabel呼吸音.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel呼吸音.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel呼吸音.Text = "1";
            // 
            // xrLabel淋巴结其他
            // 
            this.xrLabel淋巴结其他.LocationFloat = new DevExpress.Utils.PointFloat(560F, 340F);
            this.xrLabel淋巴结其他.Name = "xrLabel淋巴结其他";
            this.xrLabel淋巴结其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel淋巴结其他.SizeF = new System.Drawing.SizeF(61.45837F, 13.62497F);
            this.xrLabel淋巴结其他.Text = "1";
            // 
            // xrLabel呼吸音异常
            // 
            this.xrLabel呼吸音异常.LocationFloat = new DevExpress.Utils.PointFloat(444.7917F, 400F);
            this.xrLabel呼吸音异常.Name = "xrLabel呼吸音异常";
            this.xrLabel呼吸音异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel呼吸音异常.SizeF = new System.Drawing.SizeF(91.66669F, 13.625F);
            this.xrLabel呼吸音异常.Text = "呼吸音";
            // 
            // xrLabel龋齿3
            // 
            this.xrLabel龋齿3.LocationFloat = new DevExpress.Utils.PointFloat(519.7917F, 147.08F);
            this.xrLabel龋齿3.Name = "xrLabel龋齿3";
            this.xrLabel龋齿3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel龋齿3.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel龋齿3.Text = "1";
            // 
            // xrLabel龋齿4
            // 
            this.xrLabel龋齿4.LocationFloat = new DevExpress.Utils.PointFloat(536.4584F, 147.08F);
            this.xrLabel龋齿4.Name = "xrLabel龋齿4";
            this.xrLabel龋齿4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel龋齿4.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel龋齿4.Text = "1";
            // 
            // xrLabel义齿1
            // 
            this.xrLabel义齿1.LocationFloat = new DevExpress.Utils.PointFloat(671.875F, 136.4583F);
            this.xrLabel义齿1.Name = "xrLabel义齿1";
            this.xrLabel义齿1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel义齿1.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel义齿1.Text = "1";
            // 
            // xrLabel义齿2
            // 
            this.xrLabel义齿2.LocationFloat = new DevExpress.Utils.PointFloat(688.5417F, 136.4583F);
            this.xrLabel义齿2.Name = "xrLabel义齿2";
            this.xrLabel义齿2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel义齿2.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel义齿2.Text = "1";
            // 
            // xrLabel义齿3
            // 
            this.xrLabel义齿3.LocationFloat = new DevExpress.Utils.PointFloat(671.8751F, 147.9167F);
            this.xrLabel义齿3.Name = "xrLabel义齿3";
            this.xrLabel义齿3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel义齿3.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel义齿3.Text = "1";
            // 
            // xrLabel义齿4
            // 
            this.xrLabel义齿4.LocationFloat = new DevExpress.Utils.PointFloat(688.5417F, 147.9167F);
            this.xrLabel义齿4.Name = "xrLabel义齿4";
            this.xrLabel义齿4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel义齿4.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel义齿4.Text = "1";
            // 
            // xrLabel左眼
            // 
            this.xrLabel左眼.LocationFloat = new DevExpress.Utils.PointFloat(317.7083F, 180.83F);
            this.xrLabel左眼.Name = "xrLabel左眼";
            this.xrLabel左眼.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel左眼.SizeF = new System.Drawing.SizeF(38.54169F, 13.62498F);
            this.xrLabel左眼.StylePriority.UsePadding = false;
            this.xrLabel左眼.Text = "1";
            // 
            // xrLabel右眼
            // 
            this.xrLabel右眼.LocationFloat = new DevExpress.Utils.PointFloat(398.9583F, 180.83F);
            this.xrLabel右眼.Name = "xrLabel右眼";
            this.xrLabel右眼.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel右眼.SizeF = new System.Drawing.SizeF(48.95834F, 13.62498F);
            this.xrLabel右眼.Text = "1";
            // 
            // xrLabel左眼矫正
            // 
            this.xrLabel左眼矫正.LocationFloat = new DevExpress.Utils.PointFloat(580.2083F, 180.83F);
            this.xrLabel左眼矫正.Name = "xrLabel左眼矫正";
            this.xrLabel左眼矫正.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel左眼矫正.SizeF = new System.Drawing.SizeF(57.29169F, 13.62498F);
            this.xrLabel左眼矫正.Text = "1";
            // 
            // xrLabel右眼矫正
            // 
            this.xrLabel右眼矫正.LocationFloat = new DevExpress.Utils.PointFloat(688.5417F, 180.83F);
            this.xrLabel右眼矫正.Name = "xrLabel右眼矫正";
            this.xrLabel右眼矫正.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel右眼矫正.SizeF = new System.Drawing.SizeF(53.12506F, 13.62498F);
            this.xrLabel右眼矫正.Text = "1";
            // 
            // xrLabel听力
            // 
            this.xrLabel听力.LocationFloat = new DevExpress.Utils.PointFloat(748.9583F, 210.21F);
            this.xrLabel听力.Name = "xrLabel听力";
            this.xrLabel听力.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel听力.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel听力.Text = "1";
            // 
            // xrLabel咽部
            // 
            this.xrLabel咽部.LocationFloat = new DevExpress.Utils.PointFloat(748.9582F, 163.7083F);
            this.xrLabel咽部.Name = "xrLabel咽部";
            this.xrLabel咽部.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel咽部.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel咽部.Text = "1";
            // 
            // xrLabel运动功能
            // 
            this.xrLabel运动功能.LocationFloat = new DevExpress.Utils.PointFloat(748.9583F, 230.25F);
            this.xrLabel运动功能.Name = "xrLabel运动功能";
            this.xrLabel运动功能.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel运动功能.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel运动功能.Text = "1";
            // 
            // xrLabel眼底
            // 
            this.xrLabel眼底.LocationFloat = new DevExpress.Utils.PointFloat(748.9583F, 260.5F);
            this.xrLabel眼底.Name = "xrLabel眼底";
            this.xrLabel眼底.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel眼底.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel眼底.Text = "1";
            // 
            // xrLabel缺齿3
            // 
            this.xrLabel缺齿3.LocationFloat = new DevExpress.Utils.PointFloat(417.7084F, 147.08F);
            this.xrLabel缺齿3.Name = "xrLabel缺齿3";
            this.xrLabel缺齿3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel缺齿3.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel缺齿3.StylePriority.UsePadding = false;
            this.xrLabel缺齿3.Text = "1";
            // 
            // xrLabel缺齿4
            // 
            this.xrLabel缺齿4.LocationFloat = new DevExpress.Utils.PointFloat(444.7917F, 147.08F);
            this.xrLabel缺齿4.Name = "xrLabel缺齿4";
            this.xrLabel缺齿4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel缺齿4.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel缺齿4.Text = "1";
            // 
            // xrLabel龋齿1
            // 
            this.xrLabel龋齿1.LocationFloat = new DevExpress.Utils.PointFloat(519.7917F, 136.4583F);
            this.xrLabel龋齿1.Name = "xrLabel龋齿1";
            this.xrLabel龋齿1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel龋齿1.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel龋齿1.Text = "1";
            // 
            // xrLabel龋齿2
            // 
            this.xrLabel龋齿2.LocationFloat = new DevExpress.Utils.PointFloat(536.4584F, 136.4583F);
            this.xrLabel龋齿2.Name = "xrLabel龋齿2";
            this.xrLabel龋齿2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel龋齿2.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel龋齿2.Text = "1";
            // 
            // xrLabel缺齿2
            // 
            this.xrLabel缺齿2.LocationFloat = new DevExpress.Utils.PointFloat(444.7917F, 136.4583F);
            this.xrLabel缺齿2.Name = "xrLabel缺齿2";
            this.xrLabel缺齿2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel缺齿2.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel缺齿2.Text = "1";
            // 
            // xrLabel缺齿1
            // 
            this.xrLabel缺齿1.LocationFloat = new DevExpress.Utils.PointFloat(417.7083F, 136.4583F);
            this.xrLabel缺齿1.Name = "xrLabel缺齿1";
            this.xrLabel缺齿1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel缺齿1.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel缺齿1.Text = "1";
            // 
            // xrLabel齿列
            // 
            this.xrLabel齿列.LocationFloat = new DevExpress.Utils.PointFloat(748.9583F, 143.92F);
            this.xrLabel齿列.Name = "xrLabel齿列";
            this.xrLabel齿列.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel齿列.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel齿列.Text = "1";
            // 
            // xrLabel口腔
            // 
            this.xrLabel口腔.LocationFloat = new DevExpress.Utils.PointFloat(748.9583F, 123.9583F);
            this.xrLabel口腔.Name = "xrLabel口腔";
            this.xrLabel口腔.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel口腔.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel口腔.Text = "1";
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.ImageUrl = "C:\\Users\\wangm\\Desktop\\001\\001.JPG";
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(1654F, 1169F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.BottomMargin.BorderWidth = 0F;
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.StylePriority.UseBorders = false;
            this.BottomMargin.StylePriority.UseBorderWidth = false;
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel辅助检查其他
            // 
            this.xrLabel辅助检查其他.LocationFloat = new DevExpress.Utils.PointFloat(1103.125F, 482.7917F);
            this.xrLabel辅助检查其他.Name = "xrLabel辅助检查其他";
            this.xrLabel辅助检查其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel辅助检查其他.SizeF = new System.Drawing.SizeF(361.4584F, 33.625F);
            this.xrLabel辅助检查其他.Text = "辅助检查其他";
            // 
            // xrLabel2
            // 
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(1215.625F, 450F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel2.Text = "1";
            // 
            // xrLabel3
            // 
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(1215.625F, 425.5834F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel3.Text = "1";
            // 
            // xrLabel4
            // 
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(1215.625F, 405F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel4.Text = "1";
            // 
            // xrLabel血清高密度脂蛋白
            // 
            this.xrLabel血清高密度脂蛋白.LocationFloat = new DevExpress.Utils.PointFloat(1283.333F, 390F);
            this.xrLabel血清高密度脂蛋白.Name = "xrLabel血清高密度脂蛋白";
            this.xrLabel血清高密度脂蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血清高密度脂蛋白.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel血清高密度脂蛋白.Text = "1";
            // 
            // xrLabel血清低密度脂蛋白
            // 
            this.xrLabel血清低密度脂蛋白.LocationFloat = new DevExpress.Utils.PointFloat(1283.333F, 370F);
            this.xrLabel血清低密度脂蛋白.Name = "xrLabel血清低密度脂蛋白";
            this.xrLabel血清低密度脂蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血清低密度脂蛋白.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel血清低密度脂蛋白.Text = "2";
            // 
            // xrLabel甘油三酯
            // 
            this.xrLabel甘油三酯.LocationFloat = new DevExpress.Utils.PointFloat(1360.417F, 345F);
            this.xrLabel甘油三酯.Name = "xrLabel甘油三酯";
            this.xrLabel甘油三酯.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel甘油三酯.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel甘油三酯.Text = "1";
            // 
            // xrLabel总胆固醇
            // 
            this.xrLabel总胆固醇.LocationFloat = new DevExpress.Utils.PointFloat(1156.25F, 345F);
            this.xrLabel总胆固醇.Name = "xrLabel总胆固醇";
            this.xrLabel总胆固醇.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel总胆固醇.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel总胆固醇.Text = "1";
            // 
            // xrLabel血钠浓度
            // 
            this.xrLabel血钠浓度.LocationFloat = new DevExpress.Utils.PointFloat(1393.75F, 325F);
            this.xrLabel血钠浓度.Name = "xrLabel血钠浓度";
            this.xrLabel血钠浓度.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血钠浓度.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel血钠浓度.Text = "1";
            // 
            // xrLabel血尿素氮
            // 
            this.xrLabel血尿素氮.LocationFloat = new DevExpress.Utils.PointFloat(1393.75F, 300F);
            this.xrLabel血尿素氮.Name = "xrLabel血尿素氮";
            this.xrLabel血尿素氮.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血尿素氮.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel血尿素氮.Text = "1";
            // 
            // xrLabel血钾浓度
            // 
            this.xrLabel血钾浓度.LocationFloat = new DevExpress.Utils.PointFloat(1171.875F, 325F);
            this.xrLabel血钾浓度.Name = "xrLabel血钾浓度";
            this.xrLabel血钾浓度.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血钾浓度.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel血钾浓度.Text = "2";
            // 
            // xrLabel血清肌酐
            // 
            this.xrLabel血清肌酐.LocationFloat = new DevExpress.Utils.PointFloat(1171.875F, 300F);
            this.xrLabel血清肌酐.Name = "xrLabel血清肌酐";
            this.xrLabel血清肌酐.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血清肌酐.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel血清肌酐.Text = "1";
            // 
            // xrLabel结合胆红素
            // 
            this.xrLabel结合胆红素.LocationFloat = new DevExpress.Utils.PointFloat(1200F, 282F);
            this.xrLabel结合胆红素.Name = "xrLabel结合胆红素";
            this.xrLabel结合胆红素.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel结合胆红素.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel结合胆红素.Text = "4";
            // 
            // xrLabel总胆红素
            // 
            this.xrLabel总胆红素.LocationFloat = new DevExpress.Utils.PointFloat(1445.833F, 260.4999F);
            this.xrLabel总胆红素.Name = "xrLabel总胆红素";
            this.xrLabel总胆红素.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel总胆红素.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel总胆红素.Text = "1";
            // 
            // xrLabel白蛋白
            // 
            this.xrLabel白蛋白.LocationFloat = new DevExpress.Utils.PointFloat(1200F, 260.5F);
            this.xrLabel白蛋白.Name = "xrLabel白蛋白";
            this.xrLabel白蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel白蛋白.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel白蛋白.Text = "3";
            // 
            // xrLabel血清谷丙转氨酶
            // 
            this.xrLabel血清谷丙转氨酶.LocationFloat = new DevExpress.Utils.PointFloat(1215.625F, 233F);
            this.xrLabel血清谷丙转氨酶.Name = "xrLabel血清谷丙转氨酶";
            this.xrLabel血清谷丙转氨酶.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血清谷丙转氨酶.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel血清谷丙转氨酶.Text = "1";
            // 
            // xrLabel气郁质
            // 
            this.xrLabel气郁质.LocationFloat = new DevExpress.Utils.PointFloat(1565.625F, 660F);
            this.xrLabel气郁质.Name = "xrLabel气郁质";
            this.xrLabel气郁质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel气郁质.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel气郁质.Text = "8";
            // 
            // xrLabel血瘀质
            // 
            this.xrLabel血瘀质.LocationFloat = new DevExpress.Utils.PointFloat(1565.625F, 640F);
            this.xrLabel血瘀质.Name = "xrLabel血瘀质";
            this.xrLabel血瘀质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血瘀质.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel血瘀质.Text = "7";
            // 
            // xrLabel湿热质
            // 
            this.xrLabel湿热质.LocationFloat = new DevExpress.Utils.PointFloat(1565.625F, 620F);
            this.xrLabel湿热质.Name = "xrLabel湿热质";
            this.xrLabel湿热质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel湿热质.SizeF = new System.Drawing.SizeF(16.66687F, 13.625F);
            this.xrLabel湿热质.Text = "6";
            // 
            // xrLabel痰湿质
            // 
            this.xrLabel痰湿质.LocationFloat = new DevExpress.Utils.PointFloat(1565.625F, 600F);
            this.xrLabel痰湿质.Name = "xrLabel痰湿质";
            this.xrLabel痰湿质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel痰湿质.SizeF = new System.Drawing.SizeF(16.6665F, 13.625F);
            this.xrLabel痰湿质.Text = "5";
            // 
            // xrLabel阴虚质
            // 
            this.xrLabel阴虚质.LocationFloat = new DevExpress.Utils.PointFloat(1565.625F, 575F);
            this.xrLabel阴虚质.Name = "xrLabel阴虚质";
            this.xrLabel阴虚质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel阴虚质.SizeF = new System.Drawing.SizeF(16.66626F, 13.625F);
            this.xrLabel阴虚质.Text = "4";
            // 
            // xrLabel阳虚质
            // 
            this.xrLabel阳虚质.LocationFloat = new DevExpress.Utils.PointFloat(1565.625F, 555F);
            this.xrLabel阳虚质.Name = "xrLabel阳虚质";
            this.xrLabel阳虚质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel阳虚质.SizeF = new System.Drawing.SizeF(16.66638F, 13.625F);
            this.xrLabel阳虚质.Text = "3";
            // 
            // xrLabel气虚质
            // 
            this.xrLabel气虚质.LocationFloat = new DevExpress.Utils.PointFloat(1565.625F, 535F);
            this.xrLabel气虚质.Name = "xrLabel气虚质";
            this.xrLabel气虚质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel气虚质.SizeF = new System.Drawing.SizeF(16.66663F, 13.625F);
            this.xrLabel气虚质.Text = "2";
            // 
            // xrLabel平和质
            // 
            this.xrLabel平和质.LocationFloat = new DevExpress.Utils.PointFloat(1565.625F, 510F);
            this.xrLabel平和质.Name = "xrLabel平和质";
            this.xrLabel平和质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel平和质.SizeF = new System.Drawing.SizeF(16.6665F, 13.625F);
            this.xrLabel平和质.Text = "1";
            // 
            // xrLabel宫颈涂片
            // 
            this.xrLabel宫颈涂片.LocationFloat = new DevExpress.Utils.PointFloat(1565.625F, 450F);
            this.xrLabel宫颈涂片.Name = "xrLabel宫颈涂片";
            this.xrLabel宫颈涂片.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel宫颈涂片.SizeF = new System.Drawing.SizeF(16.66663F, 13.625F);
            this.xrLabel宫颈涂片.Text = "1";
            // 
            // xrLabelB超
            // 
            this.xrLabelB超.LocationFloat = new DevExpress.Utils.PointFloat(1565.625F, 425.5834F);
            this.xrLabelB超.Name = "xrLabelB超";
            this.xrLabelB超.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelB超.SizeF = new System.Drawing.SizeF(16.66663F, 13.625F);
            this.xrLabelB超.Text = "1";
            // 
            // xrLabelX线片
            // 
            this.xrLabelX线片.LocationFloat = new DevExpress.Utils.PointFloat(1565.625F, 405F);
            this.xrLabelX线片.Name = "xrLabelX线片";
            this.xrLabelX线片.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelX线片.SizeF = new System.Drawing.SizeF(16.66663F, 13.625F);
            this.xrLabelX线片.Text = "1";
            // 
            // xrLabel1
            // 
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(800F, 700F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel1.Text = "1";
            // 
            // xrLabel13
            // 
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(800F, 700F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel13.Text = "1";
            // 
            // xrLabel14
            // 
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(800F, 700F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel14.Text = "1";
            // 
            // xrLabel15
            // 
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(800F, 700F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel15.Text = "1";
            // 
            // xrLabel16
            // 
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(800F, 700F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel16.Text = "1";
            // 
            // xrLabel17
            // 
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(800F, 700F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel17.Text = "1";
            // 
            // xrLabel18
            // 
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(800F, 700F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel18.Text = "1";
            // 
            // xrLabel19
            // 
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(800F, 700F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel19.Text = "1";
            // 
            // xrLabel特禀质
            // 
            this.xrLabel特禀质.LocationFloat = new DevExpress.Utils.PointFloat(1565.625F, 680F);
            this.xrLabel特禀质.Name = "xrLabel特禀质";
            this.xrLabel特禀质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel特禀质.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel特禀质.Text = "9";
            // 
            // report体检
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.BorderWidth = 0F;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
            this.PageHeight = 1169;
            this.PageWidth = 1654;
            this.PaperKind = System.Drawing.Printing.PaperKind.A3;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel口腔;
        private DevExpress.XtraReports.UI.XRLabel xrLabel压痛;
        private DevExpress.XtraReports.UI.XRLabel xrLabel包块;
        private DevExpress.XtraReports.UI.XRLabel xrLabel肝大;
        private DevExpress.XtraReports.UI.XRLabel xrLabel脾大;
        private DevExpress.XtraReports.UI.XRLabel xrLabel移动性浊音有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel移动性浊音;
        private DevExpress.XtraReports.UI.XRLabel xrLabel下肢水肿;
        private DevExpress.XtraReports.UI.XRLabel xrLabel足背动脉;
        private DevExpress.XtraReports.UI.XRLabel xrLabel肛门指诊;
        private DevExpress.XtraReports.UI.XRLabel xrLabel肛门指诊其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel乳腺1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel乳腺2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel乳腺4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel乳腺3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel乳腺其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel外阴异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel阴道异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel宫颈异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel心率;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel xrLabel罗音其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel心律;
        private DevExpress.XtraReports.UI.XRLabel xrLabel杂音;
        private DevExpress.XtraReports.UI.XRLabel xrLabel杂音有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel压痛有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel包块有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel肝大有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel脾大有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel眼底异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel皮肤;
        private DevExpress.XtraReports.UI.XRLabel xrLabel皮肤异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel巩膜;
        private DevExpress.XtraReports.UI.XRLabel xrLabel巩膜异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel淋巴结;
        private DevExpress.XtraReports.UI.XRLabel xrLabel桶状胸;
        private DevExpress.XtraReports.UI.XRLabel xrLabel呼吸音;
        private DevExpress.XtraReports.UI.XRLabel xrLabel淋巴结其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel呼吸音异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel龋齿3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel龋齿4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel义齿1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel义齿2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel义齿3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel义齿4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel左眼;
        private DevExpress.XtraReports.UI.XRLabel xrLabel右眼;
        private DevExpress.XtraReports.UI.XRLabel xrLabel左眼矫正;
        private DevExpress.XtraReports.UI.XRLabel xrLabel右眼矫正;
        private DevExpress.XtraReports.UI.XRLabel xrLabel听力;
        private DevExpress.XtraReports.UI.XRLabel xrLabel咽部;
        private DevExpress.XtraReports.UI.XRLabel xrLabel运动功能;
        private DevExpress.XtraReports.UI.XRLabel xrLabel眼底;
        private DevExpress.XtraReports.UI.XRLabel xrLabel缺齿3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel缺齿4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel龋齿1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel龋齿2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel缺齿2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel缺齿1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel齿列;
        private DevExpress.XtraReports.UI.XRLabel xrLabel尿胴体;
        private DevExpress.XtraReports.UI.XRLabel xrLabel尿潜血;
        private DevExpress.XtraReports.UI.XRLabel xrLabel尿常规其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel空腹血糖1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel空腹血糖2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel心电图;
        private DevExpress.XtraReports.UI.XRLabel xrLabel78;
        private DevExpress.XtraReports.UI.XRLabel xrLabel尿微量白蛋白;
        private DevExpress.XtraReports.UI.XRLabel xrLabel大便潜血;
        private DevExpress.XtraReports.UI.XRLabel xrLabel糖化血红蛋白;
        private DevExpress.XtraReports.UI.XRLabel xrLabel乙肝;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血清谷草转氨酶;
        private DevExpress.XtraReports.UI.XRLabel xrLabel宫体异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel附件异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel外阴;
        private DevExpress.XtraReports.UI.XRLabel xrLabel阴道;
        private DevExpress.XtraReports.UI.XRLabel xrLabel宫颈;
        private DevExpress.XtraReports.UI.XRLabel xrLabel宫体;
        private DevExpress.XtraReports.UI.XRLabel xrLabel附件;
        private DevExpress.XtraReports.UI.XRLabel xrLabel查体其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血红蛋白;
        private DevExpress.XtraReports.UI.XRLabel xrLabel白细胞;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血小板;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血常规其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel尿蛋白;
        private DevExpress.XtraReports.UI.XRLabel xrLabel尿糖;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血清谷丙转氨酶;
        private DevExpress.XtraReports.UI.XRLabel xrLabel白蛋白;
        private DevExpress.XtraReports.UI.XRLabel xrLabel总胆红素;
        private DevExpress.XtraReports.UI.XRLabel xrLabel结合胆红素;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血清肌酐;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血钾浓度;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血尿素氮;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血钠浓度;
        private DevExpress.XtraReports.UI.XRLabel xrLabel总胆固醇;
        private DevExpress.XtraReports.UI.XRLabel xrLabel甘油三酯;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血清低密度脂蛋白;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血清高密度脂蛋白;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel辅助检查其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabelX线片;
        private DevExpress.XtraReports.UI.XRLabel xrLabelB超;
        private DevExpress.XtraReports.UI.XRLabel xrLabel宫颈涂片;
        private DevExpress.XtraReports.UI.XRLabel xrLabel平和质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel气虚质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel阳虚质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel阴虚质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel痰湿质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel湿热质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血瘀质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel气郁质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel特禀质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
    }
}
