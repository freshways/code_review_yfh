﻿using AtomEHR.Common;
using AtomEHR.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Models;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using AtomEHR.公共卫生.Module.个人健康;
using AtomEHR.Business.Security;
using TableXtraReport;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using AtomEHR.Interfaces;
using AtomEHR.公共卫生.ReportFrm;
using AtomEHR.公共卫生.Module.个人健康.家庭成员健康信息;


namespace AtomEHR.公共卫生.Module.健康档案.辖区健康档案
{
    public partial class frm个人健康档案 : AtomEHR.Library.frmBaseBusinessForm
    {
        private int panelSize;
        private string _strWhere;
        BackgroundWorker back = new BackgroundWorker();
        //bll家庭档案 _bll家庭档案 = new bll家庭档案();
        #region Constructor
        public frm个人健康档案()
        {
            InitializeComponent();
        }

        #endregion

        #region Handler Events
        private void frm个人健康档案_Load(object sender, EventArgs e)
        {
            InitView();//初始化页面操作
            this.InitializeForm();
            panelSize = this.panelControl1.Height;
            //tpDetail.Hide();
            tpDetail.PageVisible = false;
            this.gc个人健康档案.UseCheckBox = true;
            this.pagerControl1.OnPageChanged += pagerControl1_OnPageChanged;
            back.DoWork += back_DoWork;
            back.RunWorkerCompleted += back_RunWorkerCompleted;
            //back.WorkerReportsProgress = true;
            this.pagerControl1.Height = 35;
        }
        void back_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            DataSet ds = e.Result as DataSet;
            this.gc个人健康档案.DataSource = ds.Tables[0];
            this.pagerControl1.DrawControl();
            this.gv个人健康档案.BestFitColumns();

            //add by 颜色标注重点人 by wjz ▽
            SetColorImageWidth();
            //add by 颜色标注重点人 by wjz △

            this.btnQuery.Enabled = true;
        }
        void back_DoWork(object sender, DoWorkEventArgs e)
        {

            _strWhere = string.Empty;
            this.pagerControl1.InitControl();
            string pgrid = this.treeListLookUpEdit机构.EditValue.ToString();
            if (this.checkEdit21.Checked)//包含下属机构
            {
                if (pgrid.Length == 12)
                {
                    _strWhere += " and ([所属机构]=  '" + pgrid + "' or substring([所属机构],1,7)+'1'+substring([所属机构],9,7) like '" + pgrid + "%')";
                }
                else
                {
                    _strWhere += " and [所属机构] like '" + pgrid + "%'";
                }
            }
            else
            {
                _strWhere += " and [所属机构] ='" + pgrid + "'";
            }
            if (!string.IsNullOrEmpty(this.txt姓名.Text.Trim()))
            {
                _strWhere += " and (姓名 LIKE '" + this.txt姓名.Text.Trim() + "%') ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(cbo性别)))
            {
                _strWhere += " and 性别编码 ='" + util.ControlsHelper.GetComboxKey(cbo性别) + "'  ";
            }
            if (!string.IsNullOrEmpty(this.txt档案号.Text.Trim()))
            {
                _strWhere += " and 个人档案编号 LIKE '%" + this.txt档案号.Text.Trim() + "%'  ";
            }
            if (!string.IsNullOrEmpty(this.txt身份证号.Text.Trim()))
            {
                _strWhere += " and 身份证号 LIKE '%" + this.txt身份证号.Text.Trim() + "%'  ";
            }
            if (!string.IsNullOrEmpty(this.dte出生时间1.Text.Trim()))
            {
                _strWhere += " and 出生日期 >= '" + this.dte出生时间1.Text.Trim() + "'  ";
            }
            if (!string.IsNullOrEmpty(this.dte出生时间2.Text.Trim()))
            {
                _strWhere += " and 出生日期 <= '" + this.dte出生时间2.Text.Trim() + "'  ";
            }
            if (!string.IsNullOrEmpty(this.dte录入时间1.Text.Trim()))
            {
                _strWhere += " and 创建时间 >= '" + this.dte录入时间1.Text.Trim() + " 00:00:00'  ";
            }
            if (!string.IsNullOrEmpty(this.dte录入时间2.Text.Trim()))
            {
                _strWhere += " and 创建时间 <= '" + this.dte录入时间2.Text.Trim() + " 23:59:59'  ";
            }
            if (!string.IsNullOrEmpty(this.dte调查时间1.Text.Trim()))
            {
                _strWhere += " and 调查时间 >= '" + this.dte调查时间1.Text.Trim() + " 00:00:00'  ";
            }
            if (!string.IsNullOrEmpty(this.dte调查时间2.Text.Trim()))
            {
                _strWhere += " and 调查时间 <= '" + this.dte调查时间2.Text.Trim() + " 23:59:59'  ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(cbo档案类别)))
            {
                _strWhere += " and 档案类别 ='" + util.ControlsHelper.GetComboxKey(cbo档案类别) + "'  ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(cbo档案状态)))
            {
                _strWhere += " and 档案状态 ='" + util.ControlsHelper.GetComboxKey(cbo档案状态) + "'  ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(cbo合格档案)))
            {
                if (util.ControlsHelper.GetComboxKey(cbo合格档案) == "2")//不合格档案
                {
                    _strWhere += " and 完整度 <> 100 ";
                }
                if (util.ControlsHelper.GetComboxKey(cbo合格档案) == "1")//合格档案
                {
                    _strWhere += " and 完整度 = 100 ";
                }
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(comboBoxEdit镇)))
            {
                _strWhere += " and 街道编码 = '" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'  ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(comboBoxEdit村)))
            {
                _strWhere += " and 居委会编码 = '" + util.ControlsHelper.GetComboxKey(comboBoxEdit村) + "'  ";
            }
            if (!string.IsNullOrEmpty(this.textEdit地址.Text.Trim()))
            {
                _strWhere += " and 居住地址 LIKE '%" + this.textEdit地址.Text.Trim() + "%'  ";
            }
            if (!string.IsNullOrEmpty(this.txt本人电话.Text.Trim()))
            {
                _strWhere += " and 本人电话 LIKE '%" + this.txt本人电话.Text.Trim() + "%'  ";
            }
            if (!string.IsNullOrEmpty(this.txt联系人电话.Text.Trim()))
            {
                _strWhere += " and 联系人电话 LIKE '%" + this.txt联系人电话.Text.Trim() + "%'  ";
            }
            if (this.cbo缺项调查.Text.Trim() != "缺项调查")
            {
                if (this.cbo缺项调查.Text.Trim() == "身份证号")
                {
                    _strWhere += " and (身份证号 is null  or 身份证号 = '') ";
                }
                if (this.cbo缺项调查.Text.Trim() == "本人电话")
                {
                    _strWhere += " and (本人电话 is null or 本人电话='') ";
                }
                if (this.cbo缺项调查.Text.Trim() == "血型")
                {
                    _strWhere += " and isnull(血型,'') in('' ,'5' ,'6' ,'7') ";
                }
                if (this.cbo缺项调查.Text.Trim() == "居住情况")
                {
                    _strWhere += " and isnull(居住情况,'')='' ";
                }
            }
            if (!string.IsNullOrEmpty(this.txt工作单位.Text.Trim()))
            {
                _strWhere += " and 工作单位 LIKE '%" + this.txt工作单位.Text.Trim() + "%'  ";
            }
            //扩展查询
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(cbo职业)))
            {
                _strWhere += " and 职业 = '" + util.ControlsHelper.GetComboxKey(cbo职业) + "'  ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(cbo常住类型)))
            {
                _strWhere += " and 常住类型 = '" + util.ControlsHelper.GetComboxKey(cbo常住类型) + "'  ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(cbo民族)))
            {
                _strWhere += " and 民族 = '" + util.ControlsHelper.GetComboxKey(cbo民族) + "'  ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(cbo婚姻状况)))
            {
                _strWhere += " and 婚姻状况 = '" + util.ControlsHelper.GetComboxKey(cbo婚姻状况) + "'  ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(cbo文化程度)))
            {
                _strWhere += " and 文化程度 = '" + util.ControlsHelper.GetComboxKey(cbo文化程度) + "'  ";
            }
            if (!string.IsNullOrEmpty(this.txt医疗保险号.Text.Trim()))
            {
                _strWhere += " and 医疗保险号 LIKE '%" + this.txt医疗保险号.Text.Trim() + "%'  ";
            }
            if (!string.IsNullOrEmpty(this.txt新农合号.Text.Trim()))
            {
                _strWhere += " and 新农合号 LIKE '%" + this.txt新农合号.Text.Trim() + "%'  ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(cbo血型)))  //tb_健康档案_健康状态
            {
                if (string.IsNullOrEmpty(cbo血型.Text))//查询血型空的，2018年11月7日 yfh
                {
                    _strWhere += " and isnull(血型,'') = ''  ";
                }
                else
                {
                    _strWhere += " and 血型 = '" + util.ControlsHelper.GetComboxKey(cbo血型) + "'  ";
                }
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(cboRH))) //tb_健康档案_健康状态
            {
                _strWhere += " and RH = '" + util.ControlsHelper.GetComboxKey(cboRH) + "'  ";
            }
            if (!string.IsNullOrEmpty(this.dte最近更新时间1.Text.Trim()))
            {
                _strWhere += " and 修改时间 >= '" + this.dte最近更新时间1.Text.Trim() + " 00:00:00'  ";
            }
            if (!string.IsNullOrEmpty(this.dte最近更新时间2.Text.Trim()))
            {
                _strWhere += " and 修改时间 <= '" + this.dte最近更新时间2.Text.Trim() + " 23:59:59'  ";
            }
            if (cbo录入人.EditValue != null && cbo录入人.EditValue.ToString().Trim() != "")
            {
                _strWhere += " and 创建人 =  '" + cbo录入人.EditValue.ToString() + " ' ";
            }
            if (cbo最近修改人.EditValue != null && cbo最近修改人.EditValue.ToString().Trim() != "")
            {
                _strWhere += " and 修改人 =  '" + cbo最近修改人.EditValue.ToString() + " ' ";
            }

            if (cbo最近修改人.EditValue != null && cbo最近修改人.EditValue.ToString().Trim() != "")
            {
                _strWhere += " and 修改人 =  '" + cbo最近修改人.EditValue.ToString() + " ' ";
            }
            if (this.chk户主.Checked)
            {
                _strWhere += " and 与户主关系 =  '1' ";
            }
	        if (chk普通人群.Checked)
			{
				//排除儿童0-6岁 65岁及以上老年人 及出生日期为空的
				_strWhere += " and (出生日期 < '" + DateTime.Now.AddYears(-7).ToString("yyyy-MM-dd") + "' and  substring(出生日期,0,5) > '" + DateTime.Now.AddYears(-65).ToString("yyyy-MM-dd") + "' or 出生日期 is null) ";

				//孕产妇
				_strWhere += " and isnull(孕产妇基本信息表,'')='' ";

				//高血压
				_strWhere += "and  not exists  ( select 1 from tb_MXB高血压管理卡 where View_个人信息表.个人档案编号=tb_MXB高血压管理卡.个人档案编号 and isnull(tb_MXB高血压管理卡.终止管理,'')<>'1' )";

				//2型糖尿病
				_strWhere += "and  not exists  ( select 1 from tb_MXB糖尿病管理卡 where View_个人信息表.个人档案编号=tb_MXB糖尿病管理卡.个人档案编号 and isnull(tb_MXB糖尿病管理卡.终止管理,'')<>'1' ) ";

				//冠心病
				_strWhere += "and  not exists  ( select 1 from tb_MXB冠心病管理卡 where View_个人信息表.个人档案编号=tb_MXB冠心病管理卡.个人档案编号 and isnull(tb_MXB冠心病管理卡.终止管理,'')<>'1' ) ";

				//脑卒中
				_strWhere += "and  not exists  ( select 1 from tb_MXB脑卒中管理卡 where View_个人信息表.个人档案编号=tb_MXB脑卒中管理卡.个人档案编号 and isnull(tb_MXB脑卒中管理卡.终止管理,'')<>'1') ";

				//chk重症精神病
				_strWhere += " and not exists ( select distinct 个人档案编号 from tb_精神疾病信息补充表 a where a.个人档案编号 = View_个人信息表.个人档案编号  ) ";

				//chk残疾All
				_strWhere += "and isnull(听力言语残疾随访表,'') = '' and  isnull(肢体残疾随访表,'') = '' and  isnull(智力残疾随访表,'') = '' and  isnull(视力残疾随访表,'') =  '' and isnull(精神疾病信息补充表,'')=''";

				//_strWhere += ") ";
			}
            //重点人群
            if (this.radio重点人群.EditValue != null && this.radio重点人群.EditValue.ToString() != "")
            {
                string age = this.radio重点人群.EditValue.ToString();
                if (age == "3")//儿童0-三岁
                {
                    string date = DateTime.Now.AddYears(-4).ToString("yyyy-MM-dd");
                    _strWhere += "  and  出生日期 >= '" + date + "' ";
                }
                else if (age == "6")//儿童0-6岁
                {
                    string date = DateTime.Now.AddYears(-7).ToString("yyyy-MM-dd");
                    //_strWhere += "  and datediff(year, 出生日期 ,GETDATE())<6 ";
                    _strWhere += "  and  出生日期 >= '" + date + "' ";
                }
                else if (age == "15")//育龄妇女
                {
                    string date1 = DateTime.Now.AddYears(-15).ToString("yyyy-MM-dd");
                    string date2 = DateTime.Now.AddYears(-49).ToString("yyyy-MM-dd");
                    //_strWhere += "  and datediff(year, 出生日期 ,GETDATE())>15 and 性别  = '女' ";
                    _strWhere += "  and 出生日期 <= '" + date1 + "'  and 出生日期 >= '" + date2 + "' and 性别  = '女' ";
                }
                else if (age == "23")//孕产妇
                {
                    _strWhere += "  and 孕产妇基本信息表 is not null  and 孕产妇基本信息表<>''  ";
                    //_strWhere += "  and 个人档案编号 in (select 个人档案编号 from tb_孕妇基本信息 group by 个人档案编号)  and  (怀孕情况 = '已孕未生产' or 怀孕情况 = '已生产随访期内')";
                }
                else if (age == "65")//65岁及以上老年人 
                {
                    string date = DateTime.Now.AddYears(-65).ToString("yyyy-12-31");
                    //_strWhere += "  and datediff(year, 出生日期 ,GETDATE())<6 ";
                    _strWhere += "  and  出生日期 <= '" + date + "' ";
                }
            }
            if (chk高血压.Checked)
            {
                _strWhere += " and 个人档案编号 in  ( select 个人档案编号 from tb_MXB高血压管理卡 where isnull(tb_MXB高血压管理卡.终止管理,'')<>'1') ";
            }
            if (chk2型糖尿病.Checked)
            {
                _strWhere += " and 个人档案编号 in  ( select 个人档案编号 from tb_MXB糖尿病管理卡 where isnull(tb_MXB糖尿病管理卡.终止管理,'')<>'1') ";
            }
            if (chk冠心病.Checked)
            {
                _strWhere += " and  个人档案编号 in  ( select 个人档案编号 from tb_MXB冠心病管理卡 where isnull(tb_MXB冠心病管理卡.终止管理,'')<>'1') ";
            }
            if (chk脑卒中.Checked)
            {
                _strWhere += " and 个人档案编号 in  ( select 个人档案编号 from tb_MXB脑卒中管理卡 where isnull(tb_MXB脑卒中管理卡.终止管理,'')<>'1')  ";
            }
            if (chk肿瘤.Checked)
            {
                _strWhere += " and 是否肿瘤 =  '1' ";
            }
            if (chk慢阻肺.Checked)
            {
                _strWhere += " and 是否慢阻肺 =  '1' ";
            }
            if (chk重症精神病.Checked)
            {
                //_strWhere += " and 精神疾病信息补充表 <>  '' and 精神疾病信息补充表 is not null ";
                _strWhere += " and 个人档案编号 in  ( select distinct 个人档案编号 from tb_精神疾病信息补充表 )  ";
            }
            if (chk高血压高危人群.Checked)
            {
                _strWhere += " and exists ( select 1 from tb_MXB高血压高危人群干预调查与随访记录表 gw where gw.个人档案编号 = View_个人信息表.个人档案编号  ) ";
            }
            if (chk残疾All.Checked)
            {
                _strWhere += " and (isnull(听力言语残疾随访表,'') <> '' or  isnull(肢体残疾随访表,'') <> '' or  isnull(智力残疾随访表,'') <> '' or  isnull(视力残疾随访表,'') <>  ''  ) ";
            }
            if (chk听力残疾.Checked)
            {
                _strWhere += " and isnull(听力言语残疾随访表,'') <>  '' ";
            }
            if (chk言语残疾.Checked)
            {
                _strWhere += " and  isnull(听力言语残疾随访表,'') <>  '' and  残疾情况='3' ";
            }
            if (chk肢体残疾.Checked)
            {
                _strWhere += " and isnull(肢体残疾随访表,'') <>  '' ";
            }
            if (chk智力残疾.Checked)
            {
                _strWhere += " and isnull(智力残疾随访表,'') <>  ''  ";
            }
            if (chk视力残疾.Checked)
            {
                _strWhere += " and isnull(视力残疾随访表,'') <>  ''  ";
            }

            //状态
            if (this.chk是否流动人口.Checked)
            {
                _strWhere += " and ISNULL(是否流动人口,'')<>''  ";
            }
            if (this.chk是否贫困人口.Checked)
            {
                _strWhere += " and ISNULL(是否贫困人口,'')<>''  ";
            }
            if (this.chk是否签约服务.Checked)
            {
                _strWhere += " and ISNULL(是否签约,'')<>''  ";
            }
            //2017年2月8日14:15:55 yufh 添加 - 沙沟
            if (this.chk外出人员.Checked)
            {
                _strWhere += " and ISNULL(外出情况,'')<>''  ";
            }
            if (this.chk已复核.Checked)
            {
                _strWhere += " and ((len(isnull(本人或家属签字,''))>30 and isnull(家庭结构,'')<>'') or 复核标记='1')  ";
            }
            if (this.chk未复核.Checked)
            {
                _strWhere += " and (len(isnull(本人或家属签字,''))<30 or isnull(家庭结构,'')='')  ";
            }
            if (this.chk流入人口.Checked)
            {
                _strWhere += " and (substring(外出情况,1,1)='0')  ";
            }
            if (this.chk二次复核.Checked)
            {
                _strWhere += " and (isnull(二次复核,'')='1')  ";
            }
            if (this.chk未二次复核.Checked)
            {
                _strWhere += " and (isnull(二次复核,'')<>'1')  ";
            }
            if (this.chk参保人数.Checked)
            {
                _strWhere += " and (isnull(医疗保险号,'')<>'')  ";
            }


            //高危人群

            if (chk临界高血压.Checked)
            {
                _strWhere += " and 是否高血压 =  '2' and  not exists  ( select 1 from tb_MXB高血压管理卡 where View_个人信息表.个人档案编号=tb_MXB高血压管理卡.个人档案编号 and isnull(tb_MXB高血压管理卡.终止管理,'')<>'1' ) ";

            }
            if (chk血脂边缘升高.Checked)
            {
                _strWhere += " and 是否血脂 =  '1' ";
            }
            if (chk空腹血糖升高.Checked)
            {
                _strWhere += " and 是否空腹血糖 =  '1' ";
            }
            //餐后2小时血糖，超过正常的7.8mmol/L，但仍未达到11.1mmol/L的糖尿病诊断标准（或空腹血糖升高，未达到糖尿病的诊断标准，即空腹血糖在6.2~7.0之间）称糖耐量异常（或空腹葡萄糖受损）
            if (chk糖耐量异常.Checked)
            {
                _strWhere += " and( 是否餐后血糖 =  '1' or  是否空腹血糖 =  '1'  and 是否糖尿病 <> '1' )";
            }
            if (chk肥胖.Checked)
            {
                _strWhere += " and 是否肥胖 =  '1' ";
            }
            if (chk重度吸烟.Checked)
            {
                _strWhere += " and 是否重度吸烟 =  '1' ";
            }
            if (chk超重且中心型肥胖.Checked)
            {
                _strWhere += " and 是否超重肥胖 =  '1' ";
            }
            if (chk高危因素.Checked)
            {
                _strWhere += "and (是否高血压 =  '2' or 是否血脂 =  '1' or 是否空腹血糖 =  '1' or 是否肥胖 =  '1' or 是否重度吸烟 =  '1' or 是否超重肥胖 =  '1' ) ";
                string date = DateTime.Now.AddYears(-55).ToString("yyyy-12-31");
                //_strWhere += "  and datediff(year, 出生日期 ,GETDATE())<6 ";
                _strWhere += "  and  出生日期 <= '" + date + "' ";
            }

            this.gc个人健康档案.View = "View_个人信息表";
            this.gc个人健康档案.StrWhere = _strWhere;


            DataSet ds = null;
            if (this.cbo排序方式.Text.Trim() != "查询排序方式")
            {
                if (this.cbo排序方式.Text.Trim() == "档案创建时间")
                    ds = this.pagerControl1.GetQueryResult("View_个人信息表", "*", _strWhere, "创建时间", "DESC");
                if (this.cbo排序方式.Text.Trim() == "个人档案编号")
                    ds = this.pagerControl1.GetQueryResult("View_个人信息表", "*", _strWhere, "个人档案编号", "DESC");
                if (this.cbo排序方式.Text.Trim() == "家庭档案编号")
                    ds = this.pagerControl1.GetQueryResultNew("View_个人信息表", "*", _strWhere, "家庭档案编号", "DESC");
            }
            else
            {
                ds = this.pagerControl1.GetQueryResultNew("View_个人信息表", "*", _strWhere, "ID", "DESC");
            }

            //颜色标注 add by wjz 20180808 ▽
            AddColumnsForShow(ds.Tables[0]);
            //颜色标注 add by wjz 20180808 △

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                //ds.Tables[0].Rows[i][tb_健康档案.姓名] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i][tb_健康档案.姓名].ToString());
                ds.Tables[0].Rows[i][tb_健康档案.档案状态] = _BLL.ReturnDis字典显示("rkx_dazt", ds.Tables[0].Rows[i][tb_健康档案.档案状态].ToString());
                ds.Tables[0].Rows[i][tb_健康档案.所属机构] = _BLL.Return机构名称(ds.Tables[0].Rows[i][tb_健康档案.所属机构].ToString());
                ds.Tables[0].Rows[i][tb_健康档案.创建机构] = _BLL.Return机构名称(ds.Tables[0].Rows[i][tb_健康档案.创建机构].ToString());
                ds.Tables[0].Rows[i][tb_健康档案.创建人] = _BLL.Return用户名称(ds.Tables[0].Rows[i][tb_健康档案.创建人].ToString());
                ds.Tables[0].Rows[i][tb_健康档案.居住地址] = ds.Tables[0].Rows[i][tb_健康档案.区].ToString() + ds.Tables[0].Rows[i][tb_健康档案.街道] + ds.Tables[0].Rows[i][tb_健康档案.居委会] + ds.Tables[0].Rows[i][tb_健康档案.居住地址];
            }

            //颜色标注 add by wjz 20180808 ▽
            SetColorImage(ds.Tables[0]);
            //颜色标注 add by wjz 20180808 △

            e.Result = ds;
            //this.gc个人健康档案.DataSource = ds.Tables[0];
            //this.pagerControl1.DrawControl();
            //BindData();
        }
        void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            BindData();
        }
        private void layoutControlGroup4_Click(object sender, EventArgs e)
        {
            if (this.layoutControlGroup4.Expanded)//如果是展开的
            {
                this.groupControl1.Height = panelSize + 110;
            }
            else
            {
                this.groupControl1.Height = panelSize + 10;

            }
        }
        private void btn查询_Click(object sender, EventArgs e)
        {
            if (funCheck())
            {
                try
                {
                    this.btnQuery.Enabled = false;
                    back.RunWorkerAsync();
                }
                catch (Exception ex)
                {
                    Msg.ShowInformation(ex.Message);
                    this.btnQuery.Enabled = true;
                }
            }
        }
        /// <summary>
        /// 绑定数据
        /// </summary>
        private void BindData()
        {
            this.gc个人健康档案.View = "View_个人信息表";
            this.gc个人健康档案.StrWhere = _strWhere;

            DataSet ds = this.pagerControl1.GetQueryResultNew("View_个人信息表", "*", _strWhere, "ID", "DESC");

            //add by wjz 颜色标注 ▽
            AddColumnsForShow(ds.Tables[0]);
            //add by wjz 颜色标注 △

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                //ds.Tables[0].Rows[i][tb_健康档案.姓名] = ds.Tables[0].Rows[i][tb_健康档案.姓名].ToString();
                ds.Tables[0].Rows[i][tb_健康档案.档案状态] = _BLL.ReturnDis字典显示("rkx_dazt", ds.Tables[0].Rows[i][tb_健康档案.档案状态].ToString());
                ds.Tables[0].Rows[i][tb_健康档案.所属机构] = _BLL.Return机构名称(ds.Tables[0].Rows[i][tb_健康档案.所属机构].ToString());
                ds.Tables[0].Rows[i][tb_健康档案.创建机构] = _BLL.Return机构名称(ds.Tables[0].Rows[i][tb_健康档案.创建机构].ToString());
                ds.Tables[0].Rows[i][tb_健康档案.创建人] = _BLL.Return用户名称(ds.Tables[0].Rows[i][tb_健康档案.创建人].ToString());
                ds.Tables[0].Rows[i][tb_健康档案.居住地址] = ds.Tables[0].Rows[i][tb_健康档案.市].ToString() + ds.Tables[0].Rows[i][tb_健康档案.区] + ds.Tables[0].Rows[i][tb_健康档案.街道] + ds.Tables[0].Rows[i][tb_健康档案.居委会] + ds.Tables[0].Rows[i][tb_健康档案.居住地址];
            }

            //add by wjz 颜色标注 ▽
            SetColorImage(ds.Tables[0]);
            //add by wjz 颜色标注 △

            this.gc个人健康档案.DataSource = ds.Tables[0];
            this.pagerControl1.DrawControl();
            this.gv个人健康档案.BestFitColumns();//列自适应宽度

            //add by 颜色标注重点人 by wjz ▽
            SetColorImageWidth();
            //add by 颜色标注重点人 by wjz △

        }
        private void tabSearchCondition_TabIndexChanged(object sender, EventArgs e)
        {

        }
        private void comboBoxEdit镇_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv村 = new DataView(DataDictCache.Cache.t地区信息);
                dv村.RowFilter = "上级编码='" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'";
                //DataBinder.BindingLookupEditDataSource(lookUpEdit村, dv县.ToTable(), "地区名称", "地区编码");
                util.ControlsHelper.BindComboxData(dv村.ToTable(), comboBoxEdit村, "地区编码", "地区名称");
                util.ControlsHelper.SetComboxNullData("", comboBoxEdit村, "请选择居/村委会");
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }
        private bool funCheck()
        {
            if (!string.IsNullOrEmpty(this.dte出生时间1.Text.Trim()) && !string.IsNullOrEmpty(this.dte出生时间2.Text.Trim()) && this.dte出生时间1.DateTime > this.dte出生时间2.DateTime)
            {
                Msg.ShowError("出生日期 结束时间不能小于开始时间，请重新选择！");
                this.dte出生时间2.Text = "";
                this.dte出生时间2.Focus();
                return false;
            }
            if (!string.IsNullOrEmpty(this.dte调查时间1.Text.Trim()) && !string.IsNullOrEmpty(this.dte调查时间2.Text.Trim()) && this.dte调查时间1.DateTime > this.dte调查时间2.DateTime)
            {
                Msg.ShowError("调查时间 结束时间不能小于开始时间，请重新选择！");
                this.dte调查时间2.Text = "";
                this.dte调查时间2.Focus();
                return false;
            }
            if (!string.IsNullOrEmpty(this.dte录入时间1.Text.Trim()) && !string.IsNullOrEmpty(this.dte录入时间2.Text.Trim()) && this.dte录入时间1.DateTime > this.dte录入时间2.DateTime)
            {
                Msg.ShowError("录入时间 结束时间不能小于开始时间，请重新选择！");
                this.dte录入时间2.Text = "";
                this.dte录入时间2.Focus();
                return false;
            }
            if (!string.IsNullOrEmpty(this.dte最近更新时间1.Text.Trim()) && !string.IsNullOrEmpty(this.dte最近更新时间2.Text.Trim()) && this.dte最近更新时间1.DateTime > this.dte最近更新时间2.DateTime)
            {
                Msg.ShowError("最近更新时间 结束时间不能小于开始时间，请重新选择！");
                this.dte最近更新时间2.Text = "";
                this.dte最近更新时间2.Focus();
                return false;
            }
            return true;
        }
        private void simpleButton3_Click(object sender, EventArgs e)
        {
            int[] selectRows = gv个人健康档案.GetSelectedRows();
            if (selectRows.Length != 1) Msg.ShowInformation("请选择一条记录！");

            string 档案编号 = gv个人健康档案.GetRowCellValue(selectRows[0], col个人档案号码).ToString();
            if (!string.IsNullOrEmpty(档案编号))
            {
                frm个人健康 frm = new frm个人健康(true, this.Name, "", "", null);
                frm.Show();
            }
        }
        private void btn删除_Click(object sender, EventArgs e)
        {
            int[] selectRows = gv个人健康档案.GetSelectedRows();
            if (selectRows.Length == 0)
            {
                Msg.ShowInformation("请勾选一条记录进行删除。");
                return;
            }
            if (selectRows.Length > 1)
            {
                Msg.ShowInformation("删除只能选择一条记录。");
                return;
            }
            //2016-01-19 10:54:40 yufh 移动至此，防止在没有勾选的时候点删除提示超出索引
            string grdabh = gv个人健康档案.GetRowCellValue(selectRows[0], "个人档案编号").ToString();
            string grdaxm = gv个人健康档案.GetRowCellValue(selectRows[0], "姓名").ToString();

            if (!Msg.AskQuestion("确定要删除【" + grdaxm + "】的个人档案？该删除不能恢复！请慎重！")) return;

            //判断是否是精神病，精神病需要 与 管理员进行关联
            DataSet ds个人健康特征 = new bll健康档案_个人健康特征().GetBusinessByKey(grdabh, false);
            if (ds个人健康特征 != null && ds个人健康特征.Tables[0] != null && ds个人健康特征.Tables[0].Rows.Count == 1)
            {
                string 精神疾病信息补充表 = ds个人健康特征.Tables[0].Rows[0][tb_健康档案_个人健康特征.精神疾病信息补充表] == null ? "" : ds个人健康特征.Tables[0].Rows[0][tb_健康档案_个人健康特征.精神疾病信息补充表].ToString();
                if (!string.IsNullOrEmpty(精神疾病信息补充表))
                {
                    if (!(Loginer.CurrentUser.IsAdmin() || Loginer.CurrentUser.所属机构.Length == 12))
                    {
                        Msg.ShowInformation("精神病患者的删除功能 只有 管理员才有权限，请联系乡镇的管理员！");
                        return;
                    }
                }
                //return;
            }
            string 与户主关系 = (_BLL as bll健康档案).GetYHZGX(grdabh);//gv个人健康档案.GetRowCellValue(selectRows[0], "与户主关系").ToString();
            if (与户主关系 == "1")
            {
                Msg.Warning("不能删除户主信息");
                return;
            }
            SaveResult result = (_BLL as bll健康档案).DeleteByDocNO(grdabh);
            if (result.Success)
            {
                Msg.ShowInformation("删除个人档案成功！");
                _BLL.WriteLog(grdabh, tb_健康档案.__TableName, grdaxm); //插入删除日志
                //btn查询_Click(null, null);
                BindData();
            }
        }
        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv个人健康档案.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            //_BLL.GetBusinessByKey(个人档案编号, true);//下载一个空业务单据            
            //_BLL.NewBusiness(); //增加一条主表记录
            frm个人健康 frm = new frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, null);
            frm.Show();
            //    if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //{
            //    //BindData();
            //}
        }
        private void btn新建户主档案_Click(object sender, EventArgs e)
        {
            //frm个人基本信息表 frm = new frm个人基本信息表();
            //frm.ShowDialog();
            DoAdd(null);
        }

        #endregion

        #region Private Methods
        private void InitView()
        {
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, cbo性别, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案类别, cbo档案类别, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案状态, cbo档案状态, "P_CODE", "P_DESC");
            cbo档案状态.SelectedIndex = 1;
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t是否, cbo合格档案, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t职业, cbo职业, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t常住类型, cbo常住类型, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t民族, cbo民族, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t婚姻状况, cbo婚姻状况, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t文化程度, cbo文化程度, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t血型, cbo血型, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.tRH, cboRH, "P_CODE", "P_DESC");
            //为“镇”绑定信息
            DataView dv镇 = new DataView(DataDictCache.Cache.t地区信息);
            dv镇.RowFilter = "上级编码=371323";
            //DataBinder.BindingLookupEditDataSource(lookUpEdit镇, dv市.ToTable(), "地区名称", "地区编码");
            util.ControlsHelper.BindComboxData(dv镇.ToTable(), comboBoxEdit镇, "地区编码", "地区名称");

            //为“所属机构"绑定信息
            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                treeListLookUpEdit机构.Properties.ValueMember = "机构编号";
                treeListLookUpEdit机构.Properties.DisplayMember = "机构名称";

                treeListLookUpEdit机构.Properties.TreeList.KeyFieldName = "机构编号";
                treeListLookUpEdit机构.Properties.TreeList.ParentFieldName = "上级机构";
                treeListLookUpEdit机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                treeListLookUpEdit机构.Properties.DataSource = dt所属机构;
                //treeListLookUpEdit机
                //treeListLookUpEdit机构.Properties.TreeList.CollapseAll();

                treeListLookUpEdit机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                treeListLookUpEdit机构.Text = Loginer.CurrentUser.所属机构;
            }
            if (Loginer.CurrentUser.所属机构.Length >= 12)
                treeListLookUpEdit机构.Properties.AutoExpandAllNodes = true;
            else { treeListLookUpEdit机构.Properties.AutoExpandAllNodes = false; }
            //绑定“录入人”绑定信息
            try
            {
                DataTable dt录入人 = new bllUser().GetUserNameAndIDBy机构用户(treeListLookUpEdit机构.EditValue.ToString());
                cbo录入人.Properties.DisplayMember = "UserName";
                cbo录入人.Properties.ValueMember = "用户编码";
                cbo录入人.Properties.DataSource = dt录入人;

                cbo最近修改人.Properties.DisplayMember = "UserName";
                cbo最近修改人.Properties.ValueMember = "用户编码";
                cbo最近修改人.Properties.DataSource = dt录入人.Copy();
            }
            catch
            {
                cbo录入人.Text = "请选择...";
                cbo最近修改人.Text = "请选择...";
            }
        }

        #endregion

        #region Override Methods
        protected override void InitializeForm()
        {
            _BLL = new bll健康档案();// 业务逻辑层实例
            _SummaryView = new DevGridView(gv个人健康档案);//给成员变量赋值.每个业务窗体必需给这个变量赋值.
            _ActiveEditor = txt编码;
            _DetailGroupControl = gcDetailEditor;

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gv个人健康档案);
            //DevStyle.SetGridControlLayout(gc个人健康档案, false);//表格设置   
            //DevStyle.SetSummaryGridViewLayout(gv个人健康档案);
            //gridColumn2.OptionsColumn.AllowEdit = false;
            //_BLL.GetBusinessByKey("-", true);//加载一个空的业务对象.
            //BindingSummarySearchPanel(btnQuery, btnEmpty, panelControl1);

        }

        protected override void ButtonStateChanged(UpdateType currentState)
        {
            base.ButtonStateChanged(currentState);
            txt编码.Enabled = UpdateType.Add == currentState;
        }

        /// <summary>
        /// 绑定输入框
        /// </summary>
        /// <param name="summary"></param>
        protected override void DoBindingSummaryEditor(DataTable summary)
        {
            try
            {
                if (summary == null) return;
                //DataBinder.BindingTextEdit(txt编码, summary, tb_Test._编码);
                //DataBinder.BindingTextEdit(txt名称, summary, tb_Test._名称);
            }
            catch (Exception ex)
            { Msg.ShowException(ex); }
        }


        public override void DoAdd(Interfaces.IButtonInfo sender)
        {
            //base.DoAdd(sender);
            //this._UpdateType = UpdateType.Add; //标记新增记录
            //_BLL.GetBusinessByKey("-", true);//下载一个空业务单据            
            //_BLL.NewBusiness(); //增加一条主表记录
            frm个人基本信息表 frm = new frm个人基本信息表();
            //frm个人基本信息表 frm = new frm个人基本信息表();
            if (frm.ShowDialog() == DialogResult.Yes)
            {
                frm.Close();
                btn查询_Click(null, null);
                Msg.ShowInformation("保存成功!");
            }

        }

        public override void DoDelete(Interfaces.IButtonInfo sender)
        {
            base.DoDelete(sender);
        }

        /// <summary>
        /// 重写保存
        /// </summary>
        /// <param name="sender">自定义按钮接口</param>
        public override void DoSave(Interfaces.IButtonInfo sender)// 保存数据
        {
            this.UpdateLastControl();//更新最后一个输入框的数据


            if (_UpdateType == UpdateType.Modify) _BLL.WriteLog(); //注意:只有修改状态下保存修改日志

            DataSet dsTemplate = _BLL.CreateSaveData(_BLL.CurrentBusiness, _UpdateType); //创建用于保存的临时数据

            SaveResult result = _BLL.Save(dsTemplate);//调用业务逻辑保存数据方法

            if (result.Success) //保存成功, 不需要重新加载数据，更新当前的缓存数据就行．
            {
                //if (_UpdateType == UpdateType.Add) _BLL.DataBindRow[tb_健康档案.__KeyName] = result.DocNo; //更新单据号码
                //if (_UpdateType == UpdateType.Modify) _BLL.NotifyUser();//修改后通知创建人

                //this.UpdateSummaryRow(_BLL.DataBindRow);//刷新表格内当前记录的缓存数据.
                //this.DoBindingSummaryEditor(_BLL.DataBinder); //重新显示数据
                //if (_UpdateType == UpdateType.Add) gv个人健康档案.MoveLast(); //如是新增单据,移动到取后一条记录.
                // base.DoSave(sender); //调用基类的方法. 此行代码应放较后位置.       
                DoSearchSummary();
                btn查询_Click(null, null);
                Msg.ShowInformation("保存成功!");
            }
            else
                Msg.Warning("保存失败!");
        }
        #endregion

        private void btn导出_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "XLS file(*.xls)|*.xls";//"PDF file(*.pdf)|*.pdf";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                DataTable dt = gc个人健康档案.DataSource as DataTable;
                Business.bll机构信息 bll = new Business.bll机构信息();
                DataTable newtable = new AtomEHR.Business.BLL_Base.bllBase().Get全部数据(gc个人健康档案.View, "*", _strWhere, "ID", "DESC");
                for (int i = 0; i < newtable.Rows.Count; i++)
                {
                    //newtable.Rows[i]["姓名"] = newtable.Rows[i]["姓名"].ToString();
                    newtable.Rows[i]["居住地址"] = bll.Return地区名称(newtable.Rows[i]["区"].ToString()) + bll.Return地区名称(newtable.Rows[i]["街道"].ToString()) + bll.Return地区名称(newtable.Rows[i]["居委会"].ToString()) + newtable.Rows[i]["居住地址"].ToString();
                    newtable.Rows[i]["所属机构"] = bll.Return机构名称(newtable.Rows[i]["所属机构"].ToString());
                    newtable.Rows[i]["创建人"] = bll.Return用户名称(newtable.Rows[i]["创建人"].ToString());
                }
                gc个人健康档案.DataSource = newtable;
                gv个人健康档案.ExportToXls(dlg.FileName);
                //gc个人健康档案.DataSource = dt;
                //view.ExportToXls(dlg.FileName);
                //ExportToExcel(table, dlg.FileName);
            }
        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn打印条码_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < this.gv个人健康档案.GetSelectedRows().Length; i++)
            {
                //xtre
                DataRowView dr = this.gv个人健康档案.GetRow(this.gv个人健康档案.GetSelectedRows()[i]) as DataRowView;
                string s档案号 = dr["个人档案编号"].ToString();//this.gridView1.GetRowCellValue(this.gridView1.GetSelectedRows()[0], "档案号码").ToString();
                string s出生日期 = dr["出生日期"].ToString();//this.gridView1.GetRowCellValue(this.gridView1.GetSelectedRows()[0], "出生日期").ToString();
                string s年龄 = (System.DateTime.Now.Year - Convert.ToDateTime(s出生日期).Year).ToString();
                string s姓名 = dr["姓名"].ToString();//this.gridView1.GetRowCellValue(this.gridView1.GetSelectedRows()[0], "姓名").ToString();
                string s性别 = dr["性别"].ToString();
                string s检查 = string.Empty;
                if (this.radio重点人群.EditValue != null)
                {
                    if (this.radio重点人群.EditValue.ToString() == "23")//孕产妇
                    {
                        s检查 = "孕产妇查体";
                    }
                    else if (this.radio重点人群.EditValue.ToString() == "65")//老年人
                    {
                        s检查 = "老年人查体";
                    }
                    else if (this.radio重点人群.EditValue.ToString() == "3" || this.radio重点人群.EditValue.ToString() == "6")
                    {
                        s检查 = "儿童查体";
                    }
                }
                else
                {
                    s检查 = "其他查体";
                }
                TableXReport xtr = new TableXReport();

                Font ft = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                xtr.SetReportTitle("打印时间 " + System.DateTime.Now.ToString(), true, ft, TextAlignment.TopLeft, Size.Empty);
                xtr.SetReportTitle(s姓名 + "/" + s性别 + "/" + s年龄 + "岁 查体", true, ft, TextAlignment.TopLeft, Size.Empty);
                xtr.SetReportTitle(s检查, true, ft, TextAlignment.TopLeft, Size.Empty);
                //xtr.SetReportTitle("测试用下2 ", false, ft, TextAlignment.TopRight, Size.Empty);
                var Code128 = new DevExpress.XtraPrinting.BarCode.Code128Generator() { CharacterSet = DevExpress.XtraPrinting.BarCode.Code128Charset.CharsetAuto };
                //em.CharacterSet = DevExpress.XtraPrinting.BarCode.Code128Charset.CharsetC;
                xtr.SetReportBarCode(s档案号.Substring(6), new Point(5, 135), new Size(400, 125), Code128, true);
                //SetReportMain只能最后设置一次
                //xtr.SetReportMain(100, 12, 2, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(10, 100, 10, 100), System.Drawing.Printing.PaperKind.A4, Size.Empty, 0, 0, "");           
                xtr.SetReportMain(0, 0, 1, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(20, 20, 5, 5), System.Drawing.Printing.PaperKind.Custom, new Size(500, 300), 0, 0, "");
                xtr.Print();

            }

            //xtr.UnitAfterPrint += new XReportUnitAfterPrint(xtr_UnitAfterPrint);

            //xtr.Print("ZDesigner GK888t");
        }

        private void radio重点人群_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void chk残疾All_CheckedChanged(object sender, EventArgs e)
        {
            if (chk残疾All.Checked)
            {
                chk听力残疾.Checked = !chk残疾All.Checked;
                chk言语残疾.Checked = !chk残疾All.Checked;
                chk肢体残疾.Checked = !chk残疾All.Checked;
                chk智力残疾.Checked = !chk残疾All.Checked;
                chk视力残疾.Checked = !chk残疾All.Checked;
            }
            else
            {
                chk听力残疾.Checked = chk残疾All.Checked;
                chk言语残疾.Checked = chk残疾All.Checked;
                chk肢体残疾.Checked = chk残疾All.Checked;
                chk智力残疾.Checked = chk残疾All.Checked;
                chk视力残疾.Checked = chk残疾All.Checked;
            }

            chk听力残疾.Enabled = !chk残疾All.Checked;
            chk言语残疾.Enabled = !chk残疾All.Checked;
            chk肢体残疾.Enabled = !chk残疾All.Checked;
            chk智力残疾.Enabled = !chk残疾All.Checked;
            chk视力残疾.Enabled = !chk残疾All.Checked;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            frm重叠数据向导 frm = new frm重叠数据向导();
            frm.ShowDialog();

            //AtomEHR.公共卫生.Module.frm慢性病重叠数据分析 frm = new frm慢性病重叠数据分析();
            //frm.ShowDialog();
        }

        private void treeListLookUpEdit机构_EditValueChanged(object sender, EventArgs e)
        {
            //绑定“录入人”绑定信息
            try
            {
                DataTable dt录入人 = new bllUser().GetUserNameAndIDBy机构用户(treeListLookUpEdit机构.EditValue.ToString());
                cbo录入人.Properties.DisplayMember = "UserName";
                cbo录入人.Properties.ValueMember = "用户编码";
                cbo录入人.Properties.DataSource = dt录入人;

                cbo最近修改人.Properties.DisplayMember = "UserName";
                cbo最近修改人.Properties.ValueMember = "用户编码";
                cbo最近修改人.Properties.DataSource = dt录入人.Copy();
            }
            catch (Exception ex)
            {
                cbo录入人.Text = "请选择...";
                cbo最近修改人.Text = "请选择...";
            }
        }

        private void btnEmpty_Click(object sender, EventArgs e)
        {
            XtraReport report1 = new XtraReport();
            report1.CreateDocument();
            report1.PrintingSystem.ContinuousPageNumbering = true;
            for (int i = 0; i < this.gv个人健康档案.GetSelectedRows().Length; i++)
            {
                //xtre
                DataRowView dr = this.gv个人健康档案.GetRow(this.gv个人健康档案.GetSelectedRows()[i]) as DataRowView;
                string s档案号 = dr["个人档案编号"].ToString();//this.gridView1.GetRowCellValue(this.gridView1.GetSelectedRows()[0], "档案号码").ToString();
                string s居住地址 = dr[tb_健康档案.居住地址].ToString();
                report个人基本信息表 report = new report个人基本信息表(s档案号, s居住地址);
                report.CreateDocument();
                report1.Pages.AddRange(report.Pages);
            }
            ReportPrintTool pt1 = new ReportPrintTool(report1);
            //pt1.PrintingSystem.StartPrint += new PrintDocumentEventHandler(PrintingSystem_StartPrint);
            //打印报表
            //pt1.PrintDialog(); //显示选择打印的页面
            pt1.ShowPreviewDialog();
        }

        private void btn修改记录_Click(object sender, EventArgs e)
        {
            this.AssertFocusedRow();
            DataRowView dr = this.gv个人健康档案.GetRow(this.gv个人健康档案.GetSelectedRows()[0]) as DataRowView;
            string s档案号 = dr["个人档案编号"].ToString();
            Form form = MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frmModifyLog));
            (form as frmModifyLog).ShowModifyLog("个人档案", s档案号);
        }

        private void btn打印条码New_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < this.gv个人健康档案.GetSelectedRows().Length; i++)
            {
                //xtre
                DataRowView dr = this.gv个人健康档案.GetRow(this.gv个人健康档案.GetSelectedRows()[i]) as DataRowView;
                string s档案号 = dr["个人档案编号"].ToString();//this.gridView1.GetRowCellValue(this.gridView1.GetSelectedRows()[0], "档案号码").ToString();
                string s出生日期 = dr["出生日期"].ToString();//this.gridView1.GetRowCellValue(this.gridView1.GetSelectedRows()[0], "出生日期").ToString();
                string s年龄 = (System.DateTime.Now.Year - Convert.ToDateTime(s出生日期).Year).ToString();
                string s姓名 = dr["姓名"].ToString();//this.gridView1.GetRowCellValue(this.gridView1.GetSelectedRows()[0], "姓名").ToString();
                string s性别 = dr["性别"].ToString();
                string s检查 = string.Empty;
                if (this.radio重点人群.EditValue != null)
                {
                    if (this.radio重点人群.EditValue.ToString() == "23")//孕产妇
                    {
                        s检查 = "孕产妇查体";
                    }
                    else if (this.radio重点人群.EditValue.ToString() == "65")//老年人
                    {
                        s检查 = "老年人查体";
                    }
                    else if (this.radio重点人群.EditValue.ToString() == "3" || this.radio重点人群.EditValue.ToString() == "6")
                    {
                        s检查 = "儿童查体";
                    }
                }
                else
                {
                    s检查 = "其他查体";
                }
                TableXReport xtr = new TableXReport();

                Font ft = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                xtr.SetReportTitle("打印时间 " + System.DateTime.Now.ToString(), true, ft, TextAlignment.TopLeft, Size.Empty);
                xtr.SetReportTitle(s姓名 + "/" + s性别 + "/" + s年龄 + "岁 查体", true, ft, TextAlignment.TopLeft, Size.Empty);
                xtr.SetReportTitle(s检查, true, ft, TextAlignment.TopLeft, Size.Empty);
                //xtr.SetReportTitle("测试用下2 ", false, ft, TextAlignment.TopRight, Size.Empty);
                var Code128 = new DevExpress.XtraPrinting.BarCode.Code128Generator() { CharacterSet = DevExpress.XtraPrinting.BarCode.Code128Charset.CharsetAuto };
                xtr.SetReportBarCode(获取条码by档案号(s档案号), new Point(5, 135), new Size(400, 125), Code128, true);
                //SetReportMain只能最后设置一次
                //xtr.SetReportMain(100, 12, 2, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(10, 100, 10, 100), System.Drawing.Printing.PaperKind.A4, Size.Empty, 0, 0, "");           
                xtr.SetReportMain(0, 0, 1, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(20, 20, 5, 5), System.Drawing.Printing.PaperKind.Custom, new Size(500, 300), 0, 0, "");
                xtr.Print();

            }
        }
        /// <summary>
        /// 2018年3月5日 16点13分 yufh添加--李丹提出城区打印条码的变更
        /// </summary>
        /// <param name="档案编码"></param>
        /// <returns></returns>
        private string 获取条码by档案号(string 档案编码)
        {
            string bar = string.Empty;
            if (档案编码.Length == 17)
            {
                bar = "1" + 档案编码.Substring(2, 档案编码.Length - 2);
            }
            else if (档案编码.Length == 18)
            {
                bar = "1" + 档案编码.Substring(3, 档案编码.Length - 3);
            }

            return bar;
        }
        #region 颜色标注重点人群
        byte[] canImgByte = null;
        byte[] feiImgByte = null;
        byte[] gaoImgByte = null;
        byte[] guanImgByte = null;
        byte[] jingImgByte = null;
        byte[] laoImgByte = null;
        byte[] naoImgByte = null;
        byte[] pinImgByte = null;
        byte[] tangImgByte = null;
        byte[] tmImgByte = null;
        byte[] youImgByte = null;
        byte[] yunImgByte = null;
        private byte[] GetImageByte(ImageType imgtype)
        {
            byte[] arrbyte = GetExistingImageByte(imgtype);
            if (arrbyte == null || arrbyte.Length == 0)
            {
                Image image = GetImage(imgtype);
                arrbyte = GetByteByImage(image);
                SetImageByte(imgtype, arrbyte);
            }
            return arrbyte;
        }

        private Image GetImage(ImageType imgtype)
        {
            Image image = null;
            switch (imgtype)
            {
                case ImageType.can:
                    image = global::AtomEHR.公共卫生.Properties.Resources.ys_can;
                    break;
                case ImageType.fei:
                    image = global::AtomEHR.公共卫生.Properties.Resources.ys_fei;
                    break;
                case ImageType.gao:
                    image = global::AtomEHR.公共卫生.Properties.Resources.ys_gao;
                    break;
                case ImageType.guan:
                    image = global::AtomEHR.公共卫生.Properties.Resources.ys_guan;
                    break;
                case ImageType.jing:
                    image = global::AtomEHR.公共卫生.Properties.Resources.ys_jing;
                    break;
                case ImageType.lao:
                    image = global::AtomEHR.公共卫生.Properties.Resources.ys_lao;
                    break;
                case ImageType.nao:
                    image = global::AtomEHR.公共卫生.Properties.Resources.ys_nao;
                    break;
                case ImageType.pin:
                    image = global::AtomEHR.公共卫生.Properties.Resources.ys_pin;
                    break;
                case ImageType.tang:
                    image = global::AtomEHR.公共卫生.Properties.Resources.ys_tang;
                    break;
                case ImageType.tm:
                    image = global::AtomEHR.公共卫生.Properties.Resources.ys_tm;
                    break;
                case ImageType.you:
                    image = global::AtomEHR.公共卫生.Properties.Resources.ys_you;
                    break;
                case ImageType.yun:
                    image = global::AtomEHR.公共卫生.Properties.Resources.ys_yun;
                    break;
                default:
                    break;
            }
            return image;
        }

        private byte[] GetExistingImageByte(ImageType imgtype)
        {
            byte[] arrbyte = null;
            switch (imgtype)
            {
                case ImageType.can:
                    arrbyte = canImgByte;
                    break;
                case ImageType.fei:
                    arrbyte = feiImgByte;
                    break;
                case ImageType.gao:
                    arrbyte = gaoImgByte;
                    break;
                case ImageType.guan:
                    arrbyte = guanImgByte;
                    break;
                case ImageType.jing:
                    arrbyte = jingImgByte;
                    break;
                case ImageType.lao:
                    arrbyte = laoImgByte;
                    break;
                case ImageType.nao:
                    arrbyte = naoImgByte;
                    break;
                case ImageType.pin:
                    arrbyte = pinImgByte;
                    break;
                case ImageType.tang:
                    arrbyte = tangImgByte;
                    break;
                case ImageType.tm:
                    arrbyte = tmImgByte;
                    break;
                case ImageType.you:
                    arrbyte = youImgByte;
                    break;
                case ImageType.yun:
                    arrbyte = yunImgByte;
                    break;
                default:
                    break;
            }
            return arrbyte;
        }

        private void SetImageByte(ImageType imgType, byte[] arrbyte)
        {
            switch (imgType)
            {
                case ImageType.can:
                    canImgByte = arrbyte;
                    break;
                case ImageType.fei:
                    feiImgByte = arrbyte;
                    break;
                case ImageType.gao:
                    gaoImgByte = arrbyte;
                    break;
                case ImageType.guan:
                    guanImgByte = arrbyte;
                    break;
                case ImageType.jing:
                    jingImgByte = arrbyte;
                    break;
                case ImageType.lao:
                    laoImgByte = arrbyte;
                    break;
                case ImageType.nao:
                    naoImgByte = arrbyte;
                    break;
                case ImageType.pin:
                    pinImgByte = arrbyte;
                    break;
                case ImageType.tang:
                    tangImgByte = arrbyte;
                    break;
                case ImageType.tm:
                    tmImgByte = arrbyte;
                    break;
                case ImageType.you:
                    youImgByte = arrbyte;
                    break;
                case ImageType.yun:
                    yunImgByte = arrbyte;
                    break;
                default:
                    break;
            }
        }

        private byte[] GetByteByImage(Image image)
        {
            byte[] arr1;
            using (System.IO.MemoryStream ms1 = new System.IO.MemoryStream())
            {
                image.Save(ms1, System.Drawing.Imaging.ImageFormat.Png);
                arr1 = new byte[ms1.Length];
                ms1.Position = 0;
                ms1.Read(arr1, 0, (int)ms1.Length);
                ms1.Close();

            }
            return arr1;
        }

        private enum ImageType
        {
            can, //残疾人
            fei, //肺结核
            gao, //高血压
            guan,  //冠心病
            jing,  //精神病
            lao,   //老年人
            nao,   //脑卒中
            pin,   //贫困人口
            tang,  //糖尿病
            tm,    //透明背景，空白
            you,   //幼儿
            yun    //孕产妇

        }

        private void AddColumnsForShow(DataTable dtable)
        {
            dtable.Columns.Add("can", System.Type.GetType("System.Byte[]"));
            dtable.Columns.Add("fei", System.Type.GetType("System.Byte[]"));
            dtable.Columns.Add("gao", System.Type.GetType("System.Byte[]"));
            dtable.Columns.Add("guan", System.Type.GetType("System.Byte[]"));
            dtable.Columns.Add("jing", System.Type.GetType("System.Byte[]"));
            dtable.Columns.Add("lao", System.Type.GetType("System.Byte[]"));
            dtable.Columns.Add("nao", System.Type.GetType("System.Byte[]"));
            dtable.Columns.Add("pin", System.Type.GetType("System.Byte[]"));
            dtable.Columns.Add("tang", System.Type.GetType("System.Byte[]"));
            dtable.Columns.Add("you", System.Type.GetType("System.Byte[]"));
            dtable.Columns.Add("yun", System.Type.GetType("System.Byte[]"));
        }

        private void SetColorImage(DataTable dtable)
        {
            try
            {
                //查询
                List<string> dahlist = new List<string>();
                for (int index = 0; index < dtable.Rows.Count; index++)
                {
                    dahlist.Add(dtable.Rows[index][tb_健康档案.个人档案编号].ToString());
                }

                DataSet ds = ((bll健康档案)_BLL).Get重点人群档案号ByDah(dahlist);
                DataTable dt肺结核 = ds.Tables[0];
                DataTable dt高血压 = ds.Tables[1];
                DataTable dt糖尿病 = ds.Tables[2];
                DataTable dt冠心病 = ds.Tables[3];
                DataTable dt脑卒中 = ds.Tables[4];
                DataTable dt精神病 = ds.Tables[5];

                for (int i = 0; i < dtable.Rows.Count; i++)
                {
                    string dah = dtable.Rows[i][tb_健康档案.个人档案编号].ToString();
                    // and (isnull(听力言语残疾随访表,'') <>  ''  or  isnull(肢体残疾随访表,'') <>  '' or  isnull(智力残疾随访表,'') <>  ''  or  isnull(视力残疾随访表,'') <>  ''  )
                    //根据back_DoWork里的既有程序进行判断
                    if (dtable.Rows[i][tb_健康档案_个人健康特征.听力言语残疾随访表].ToString() != "" || dtable.Rows[i][tb_健康档案_个人健康特征.肢体残疾随访表].ToString() != ""
                        || dtable.Rows[i][tb_健康档案_个人健康特征.智力残疾随访表].ToString() != "" || dtable.Rows[i][tb_健康档案_个人健康特征.视力残疾随访表].ToString() != ""
                       )
                    {
                        dtable.Rows[i]["can"] = GetImageByte(ImageType.can);
                    }
                    else
                    {
                        dtable.Rows[i]["can"] = GetImageByte(ImageType.tm);
                    }

                    DataRow[] drs肺结核 = dt肺结核.Select("个人档案编号='" + dah + "'");
                    if (drs肺结核.Length > 0)
                    {
                        dtable.Rows[i]["fei"] = GetImageByte(ImageType.fei);
                    }
                    else
                    {
                        dtable.Rows[i]["fei"] = GetImageByte(ImageType.tm);
                    }

                    DataRow[] drs高血压 = dt高血压.Select("个人档案编号='" + dah + "'");
                    if (drs高血压.Length > 0)
                    {
                        dtable.Rows[i]["gao"] = GetImageByte(ImageType.gao);//sql
                    }
                    else
                    {
                        dtable.Rows[i]["gao"] = GetImageByte(ImageType.tm);//sql
                    }

                    DataRow[] drs冠心病 = dt冠心病.Select("个人档案编号='" + dah + "'");
                    if (drs冠心病.Length > 0)
                    {
                        dtable.Rows[i]["guan"] = GetImageByte(ImageType.guan);//sql
                    }
                    else
                    {
                        dtable.Rows[i]["guan"] = GetImageByte(ImageType.tm);//sql
                    }

                    DataRow[] drs精神病 = dt精神病.Select("个人档案编号='" + dah + "'");
                    if (drs精神病.Length > 0)
                    {
                        dtable.Rows[i]["jing"] = GetImageByte(ImageType.jing);//sql
                    }
                    else
                    {
                        dtable.Rows[i]["jing"] = GetImageByte(ImageType.tm);//sql
                    }

                    //老年人
                    string birthdate = dtable.Rows[i][tb_健康档案.出生日期].ToString();
                    string serverDate = _BLL.ServiceDateTime;
                    try
                    {
                        string noyear = Convert.ToDateTime(serverDate).AddYears(-65).ToString("yyyy");
                        if (birthdate.Substring(0, 4).CompareTo(noyear) <= 0)
                        {
                            dtable.Rows[i]["lao"] = GetImageByte(ImageType.lao);
                        }
                        else
                        {
                            dtable.Rows[i]["lao"] = GetImageByte(ImageType.tm);
                        }
                    }
                    catch
                    {
                        dtable.Rows[i]["lao"] = GetImageByte(ImageType.tm);
                    }

                    //
                    DataRow[] drs脑卒中 = dt脑卒中.Select("个人档案编号='" + dah + "'");
                    if (drs脑卒中.Length > 0)
                    {
                        dtable.Rows[i]["nao"] = GetImageByte(ImageType.nao);//sql
                    }
                    else
                    {
                        dtable.Rows[i]["nao"] = GetImageByte(ImageType.tm);//sql
                    }

                    //贫困人口
                    string pinkunrenkou = dtable.Rows[i][tb_健康档案.是否贫困人口].ToString();
                    if (string.IsNullOrWhiteSpace(pinkunrenkou))
                    {
                        dtable.Rows[i]["pin"] = GetImageByte(ImageType.tm);
                    }
                    else
                    {
                        dtable.Rows[i]["pin"] = GetImageByte(ImageType.pin);
                    }

                    DataRow[] drs糖尿病 = dt糖尿病.Select("个人档案编号='" + dah + "'");
                    if (drs糖尿病.Length > 0)
                    {
                        dtable.Rows[i]["tang"] = GetImageByte(ImageType.tang);//sql
                    }
                    else
                    {
                        dtable.Rows[i]["tang"] = GetImageByte(ImageType.tm);//sql
                    }

                    //儿童
                    try
                    {
                        if (birthdate.CompareTo(Convert.ToDateTime(serverDate).AddYears(-7).ToString("yyyy-MM-dd")) > 0)
                        {
                            dtable.Rows[i]["you"] = GetImageByte(ImageType.you);
                        }
                        else
                        {
                            dtable.Rows[i]["you"] = GetImageByte(ImageType.tm);
                        }
                    }
                    catch
                    {
                        dtable.Rows[i]["you"] = GetImageByte(ImageType.tm);
                    }


                    string yunchanfu = dtable.Rows[i][tb_健康档案.怀孕情况].ToString();
                    //if (string.IsNullOrWhiteSpace(yunchanfu))
                    if (yunchanfu.Equals("已生产(随访期内)") || yunchanfu.Equals("已生产随访期内") || yunchanfu.Equals("已孕未生产"))
                    {
                        //dtable.Rows[i]["yun"] = GetImageByte(ImageType.tm);
                        dtable.Rows[i]["yun"] = GetImageByte(ImageType.yun);
                    }
                    else
                    {
                        //dtable.Rows[i]["yun"] = GetImageByte(ImageType.yun);
                        dtable.Rows[i]["yun"] = GetImageByte(ImageType.tm);
                    }
                    //颜色标注 add by wjz 20180808 △
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void SetColorImageWidth()
        {
            this.col残疾人.Width = 30;
            this.col肺结核.Width = 30;
            this.col高血压.Width = 30;
            this.col冠心病.Width = 30;
            this.col精神病.Width = 30;
            this.col老年人.Width = 30;
            this.col脑卒中.Width = 30;
            this.col贫困.Width = 30;
            this.col糖尿病.Width = 30;
            this.col儿童.Width = 30;
            this.col孕产妇.Width = 30;
        }
        #endregion

        //Begin WXF 2018-11-01 | 13:37
        //整合打印功能 
        private void btn综合打印_Click(object sender, EventArgs e)
        {
            //DataRowView dr = this.gv个人健康档案.GetRow(this.gv个人健康档案.GetSelectedRows()[0]) as DataRowView;
            string s档案号 = this.gv个人健康档案.GetRowCellValue(this.gv个人健康档案.GetSelectedRows()[0], "个人档案编号").ToString();//dr["个人档案编号"].ToString();
            if (!string.IsNullOrEmpty(s档案号))
            {
                frm_ReportALL repo = new frm_ReportALL(s档案号);
                repo.ShowDialog();
            }
        }
        

    }
}
