﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.健康档案.辖区健康档案
{
    public partial class XtraReport1 : DevExpress.XtraReports.UI.XtraReport
    {
        private List<string> result;

        public XtraReport1()
        {
            InitializeComponent();
        }

        public XtraReport1(System.Collections.Generic.List<string> result)
        {
            InitializeComponent();
            //this.xrPictureBox1.Image = Image.FromFile(Application.StartupPath + "\\Resources\\1111.jpg");
            //this.xrPictureBox2.Image = Image.FromFile(Application.StartupPath + "\\Resources\\2222.jpg");
            this.result = result;
            this.cell姓名.Text = result[0];
            this.cell性别.Text = result[1];
            this.cell年龄.Text = result[2];
            this.cell村庄.Text = result[3];
            this.cell体检医师.Text = result[4];
            this.cell体检单位.Text = result[5];
            this.xrTableCell16.Text = "体检项目：" + result[6];
            this.cell结论.Text = result[7];
            label检查时间.Text = result[8].ToString();
            this.lbl医院.Text = result[9].ToString();

        }

    }
}
