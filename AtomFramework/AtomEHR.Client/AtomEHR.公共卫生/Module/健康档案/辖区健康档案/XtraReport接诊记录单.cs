﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Collections.Generic;

namespace AtomEHR.公共卫生.Module.健康档案.辖区健康档案
{
    public partial class XtraReport接诊记录单 : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReport接诊记录单()
        {
            InitializeComponent();
        }
        List<string> list;
        public XtraReport接诊记录单(DataRow dt, string s姓名, string s性别, string s年龄, string s地址)
        {
            InitializeComponent();
            xrLabel姓名.Text += s姓名;
            xrLabel性别.Text += s性别;
            xrLabel年龄.Text += s年龄;
            xrLabel住址.Text += s地址;
            xrRichText1.Text = dt["主观资料"].ToString();
            xrRichText2.Text = dt["客观资料"].ToString();
            xrRichText3.Text = dt["评估"].ToString();
            xrRichText4.Text = dt["处置计划"].ToString();
            xrLabel签字.Text += dt["接诊医生"].ToString();
            xrLabel日期.Text = dt["接诊时间"].ToString();
        }

        public XtraReport接诊记录单(List<string> list, string s姓名, string s性别, string s年龄, string s地址)
        {
            InitializeComponent();
            xrLabel姓名.Text += s姓名;
            xrLabel性别.Text += s性别;
            xrLabel年龄.Text += s年龄;
            xrLabel住址.Text += s地址;
            xrRichText1.Text = list[2].ToString();
            xrRichText2.Text = list[3].ToString();
            xrRichText3.Text = list[4].ToString();
            xrRichText4.Text = list[5].ToString();
            xrLabel签字.Text += list[1].ToString();
            xrLabel日期.Text = list[0].ToString();
        }
    }
}
