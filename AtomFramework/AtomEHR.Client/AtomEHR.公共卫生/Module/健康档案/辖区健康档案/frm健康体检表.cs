﻿using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.公共卫生.Module.个人健康;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.健康档案.辖区健康档案
{
    public partial class frm健康体检表 : frmBaseBusinessForm
    {
        private string _strWhere;
        Business.bll健康体检 _buBLL = new Business.bll健康体检();
        BackgroundWorker back = new BackgroundWorker();

        public frm健康体检表()
        {
            InitializeComponent();
            frmGridCustomize.RegisterGrid(gv健康体检);
            frmGridCustomize.RegisterGrid(gv健康体检Num);

            back.RunWorkerCompleted += back_RunWorkerCompleted;
            back.DoWork += back_DoWork;
        }

        void back_DoWork(object sender, DoWorkEventArgs e)
        {
            DataTable dt = null;
            #region 查询条件

            _strWhere = string.Empty;
            string pgrid = this.treeListLookUpEdit机构.EditValue.ToString();


            if (this.txt姓名.Text.Trim() != "")
            {
                _strWhere += " and 姓名 LIKE '" + this.txt姓名.Text.Trim() + "%' ";
            }
            if (this.txt档案编号.Text.Trim() != "")
            {
                _strWhere += " and 个人档案编号 ='" + this.txt档案编号.Text.Trim() + "'";
            }
            if (this.txt身份证号.Text.Trim() != "")
            {
                _strWhere += " and 身份证号 ='" + this.txt身份证号.Text.Trim() + "'";
            }
            if (util.ControlsHelper.GetComboxKey(comboBoxEdit镇) != "")
            {
                _strWhere += " and 街道编码 ='" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'";
            }
            if (util.ControlsHelper.GetComboxKey(comboBoxEdit村) != "")
            {
                _strWhere += " and [居委会编码] ='" + util.ControlsHelper.GetComboxKey(comboBoxEdit村) + "'";
            }
            if (this.txt详细地址.Text.Trim() != "")
            {
                _strWhere += " and [居住地址] like '" + this.txt详细地址.Text.Trim() + "%'";
            }

            if (this.dte出生日期1.Text.Trim() != "")
            {
                _strWhere += " and [出生日期] >= '" + this.dte出生日期1.Text.Trim() + " 00:00:00'";
            }
            if (this.dte出生日期2.Text.Trim() != "")
            {
                _strWhere += " and [出生日期] <= '" + this.dte出生日期2.Text.Trim() + " 23:59:59'";
            }
            string str性别 = cbo性别.Text;//util.ControlsHelper.GetComboxKey(cbo性别);
            if (str性别.Trim() != "" && str性别.Trim() != "请选择")
            {
                _strWhere += " and 性别 = '" + str性别 + "'";
            }
            //if (util.ControlsHelper.GetComboxKey(this.cbo性别) != "")
            //{
            //    _strWhere += " and [性别] = '" + util.ControlsHelper.GetComboxKey(cbo性别) + "'";
            //}
            if (util.ControlsHelper.GetComboxKey(this.cbo档案状态) != "")
            {
                _strWhere += " and [档案状态] = '" + util.ControlsHelper.GetComboxKey(this.cbo档案状态) + "'";
            }
            if (this.chk是否流动人口.Checked)
            {
                _strWhere += " and isnull([是否流动人口],'') ='1' ";
            }
            if (this.chk是否贫困人口.Checked)
            {
                _strWhere += " and isnull([是否贫困人口],'') ='1' ";
            }
            if (this.chk是否签约服务.Checked)
            {
                _strWhere += " and isnull([是否签约],'') ='1' ";
            }
            if (this.chk老年人.Checked)
            {
                _strWhere += " and 出生日期 <= convert(varchar(10),year(dateadd(yy,-65,getdate())),120)+'-12-31'  ";
            }
            #endregion
            if (this.cbo人数人次.Text.Trim() == "请选择" || this.cbo人数人次.Text.Trim() == "人次")
            {
                if (this.chk含下属机构.Checked)//包含下属机构
                {
                    if (pgrid.Length == 12)
                    {
                        _strWhere += " and ( [所属机构]=  '" + pgrid + "' or substring( [所属机构],1,7)+'1'+substring( [所属机构],9,7) like '" + pgrid + "%')";
                    }
                    else
                    {
                        _strWhere += " and  [所属机构] like '" + pgrid + "%'";
                    }
                }
                else
                {
                    _strWhere += " and  [所属机构] ='" + pgrid + "'";
                }
                if (this.cbo录入人.Text.Trim() != "")
                {
                    _strWhere += " and  [创建人] = '" + this.cbo录入人.EditValue.ToString() + "'";
                }
                if (this.cbo档案类别.Text.Trim() != "请选择")
                {
                    _strWhere += " and  [档案类别] = '" + util.ControlsHelper.GetComboxKey(cbo档案类别) + "'";
                }
                if (this.dte录入时间1.Text.Trim() != "")
                {
                    _strWhere += " and  [创建时间] >= '" + this.dte录入时间1.Text.Trim() + " 00:00:00'";
                }
                if (this.dte录入时间2.Text.Trim() != "")
                {
                    _strWhere += " and  [创建时间] <= '" + this.dte录入时间2.Text.Trim() + " 23:59:59'";
                }
                if (this.dte体检时间1.Text.Trim() != "")
                {
                    _strWhere += " and  [体检日期] >= '" + this.dte体检时间1.Text.Trim() + "'";
                }
                if (this.dte体检时间2.Text.Trim() != "")
                {
                    _strWhere += " and  [体检日期] <= '" + this.dte体检时间2.Text.Trim() + "'";
                }
                if (util.ControlsHelper.GetComboxKey(this.cbo合格档案) != "")
                {
                    if (util.ControlsHelper.GetComboxKey(this.cbo合格档案) == "1")//合格 ， 缺项为0
                    {
                        _strWhere += " and  [缺项] = '0'";
                    }
                    else
                    {
                        _strWhere += " and  [缺项] <> '0'";
                    }
                }
                //老年人专项查询-2017-01-11 22:00:01 yufh添加
                if ( this.cob辅助检查合格.Text=="合格")
                {
                    string checks = " and (isnull(总胆固醇,'')<>'' and isnull(甘油三酯,'')<>'' and isnull(血清低密度脂蛋白胆固醇,'')<>'' and isnull(血清高密度脂蛋白胆固醇,'')<>'' and isnull(血清谷丙转氨酶,'')<>'' and isnull(血清谷草转氨酶,'')<>'' and isnull(总胆红素,'')<>'' and isnull(血清肌酐,'')<>'' and isnull(血尿素氮,'')<>'' and isnull(B超,'未做')<>'未做' and isnull(心电图,'')<>'' and isnull(血红蛋白,'')<>'' and isnull(白细胞,'')<>'' and isnull(血小板,'')<>'' and isnull(尿蛋白,'')<>'' and isnull(尿糖,'')<>'' and isnull(尿酮体,'')<>'' and isnull(尿潜血,'')<>'' ) ";
                    _strWhere += checks;
                }
                else if (this.cob辅助检查合格.Text == "不合格")
                {
                    string checks = " and (isnull(总胆固醇,'')='' or isnull(甘油三酯,'')='' or isnull(血清低密度脂蛋白胆固醇,'')='' or isnull(血清高密度脂蛋白胆固醇,'')='' or isnull(血清谷丙转氨酶,'')='' or isnull(血清谷草转氨酶,'')='' or isnull(总胆红素,'')='' or isnull(血清肌酐,'')='' or isnull(血尿素氮,'')='' or isnull(B超,'未做')='未做' or isnull(心电图,'')='' or isnull(血红蛋白,'')='' or isnull(白细胞,'')='' or isnull(血小板,'')='' or isnull(尿蛋白,'')='' or isnull(尿糖,'')='' or isnull(尿酮体,'')='' or isnull(尿潜血,'')='' ) ";
                    _strWhere += checks;
                }
                if (chk高血压完整项.Checked)
                {
                    string checks = " and 个人档案编号 in  ( select 个人档案编号 from tb_MXB高血压管理卡 where isnull(tb_MXB高血压管理卡.终止管理,'')<>'1') and (isnull(血压右侧,'')<>'' and isnull(血压左侧,'')<>'' and isnull(吸烟状况,'')<>'' and isnull(饮酒频率,'')<>'' and isnull(体温,0)<>0 and isnull(呼吸,'')<>'' and isnull(身高,0)<>0 and isnull(体重,0)<>0 and isnull(腰围,0)<>0 and isnull(皮肤,'')<>'' and isnull(淋巴结,'')<>'' and isnull(心脏疾病,'')<>'' and isnull(右眼视力,0)<>0 and isnull(左眼视力,0)<>0 and isnull(听力,'')<>'' and isnull(运动功能,'')<>'' ) ";
                    _strWhere += checks;
                }

                this.pagerControl1.InitControl();

                dt = this.pagerControl1.GetQueryResultNew("vw_健康体检", " *", _strWhere, "ID", "DESC").Tables[0];

                //ds = this.pagerControl1.GetQueryResult("vw_健康体检", "*", _strWhere, "创建时间", "DESC");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //dt.Rows[i]["姓名"] = util.DESEncrypt.DES解密(dt.Rows[i]["姓名"].ToString());
                    dt.Rows[i]["创建人"] = _buBLL.Return用户名称(dt.Rows[i]["创建人"].ToString());
                    //dt.Rows[i]["性别"] = _buBLL.ReturnDis字典显示("xb_xingbie", dt.Rows[i]["性别"].ToString());
                    dt.Rows[i]["所属机构"] = _buBLL.Return机构名称(dt.Rows[i]["所属机构"].ToString());
                    dt.Rows[i]["居住地址"] = _buBLL.Return地区名称(dt.Rows[i]["街道"].ToString()) + _buBLL.Return地区名称(dt.Rows[i]["居委会"].ToString()) + dt.Rows[i]["居住地址"];
                }
                //this.pagerControl1.DrawControl();
            }
            else if (this.cbo人数人次.Text.Trim() == "人数")//需要Group by 一下进行统计
            {
                _strWhere += @" and  个人档案编号 in (
		select tj.个人档案编号 from tb_健康体检 tj where 1=1 ";
                if (this.chk含下属机构.Checked)//包含下属机构
                {
                    if (pgrid.Length == 12)
                    {
                        _strWhere += " and (tj.[所属机构]=  '" + pgrid + "' or substring(tj.[所属机构],1,7)+'1'+substring(tj.[所属机构],9,7) like '" + pgrid + "%')";
                    }
                    else
                    {
                        _strWhere += " and tj.[所属机构] like '" + pgrid + "%'";
                    }
                }
                else
                {
                    _strWhere += " and tj.[所属机构] ='" + pgrid + "'";
                }
                if (this.cbo录入人.Text.Trim() != "")
                {
                    _strWhere += " and tj.[创建人] = '" + this.cbo录入人.EditValue.ToString() + "'";
                }
                if (this.cbo档案类别.Text.Trim() != "请选择")
                {
                    _strWhere += " and [档案类别] = '" + util.ControlsHelper.GetComboxKey(cbo档案类别) + "'";
                }
                if (this.dte录入时间1.Text.Trim() != "")
                {
                    _strWhere += " and tj.[创建时间] >= '" + this.dte录入时间1.Text.Trim() + ",  00:00:00'";
                }
                if (this.dte录入时间2.Text.Trim() != "")
                {
                    _strWhere += " and tj.[创建时间] <= '" + this.dte录入时间2.Text.Trim() + ", 23:59:59'";
                }
                if (this.dte体检时间1.Text.Trim() != "")
                {
                    _strWhere += " and tj.[体检日期] >= '" + this.dte体检时间1.Text.Trim() + ",  00:00:00'";
                }
                if (this.dte体检时间2.Text.Trim() != "")
                {
                    _strWhere += " and tj.[体检日期] <= '" + this.dte体检时间2.Text.Trim() + ", 23:59:59'";
                }
                if (util.ControlsHelper.GetComboxKey(this.cbo合格档案) != "")
                {
                    if (util.ControlsHelper.GetComboxKey(this.cbo合格档案) == "1")//合格 ， 缺项为0
                    {
                        _strWhere += " and tj.[缺项] = '0'";
                    }
                    else
                    {
                        _strWhere += " and tj.[缺项] <> '0'";
                    }
                }
                _strWhere += "  group by tj.个人档案编号)";
                this.pagerControl2.InitControl();

                dt = this.pagerControl2.GetQueryResultNew("vw_健康体检",
 " 个人档案编号,姓名,性别,出生日期,街道,居委会,居住地址,身份证号,本人电话", _strWhere, "ID", "DESC").Tables[0];

                //ds = this.pagerControl2.GetQueryResult("View_个人信息表", _strWhere);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //dt.Rows[i]["姓名"] = util.DESEncrypt.DES解密(dt.Rows[i]["姓名"].ToString());
                    //dt.Rows[i]["性别"] = _buBLL.ReturnDis字典显示("xb_xingbie", dt.Rows[i]["性别"].ToString());
                    dt.Rows[i]["性别"] = _buBLL.ReturnDis字典显示("xb_xingbie", dt.Rows[i]["性别"].ToString());
                    dt.Rows[i]["居住地址"] = _buBLL.Return地区名称(dt.Rows[i]["街道"].ToString()) + _buBLL.Return地区名称(dt.Rows[i]["居委会"].ToString()) + dt.Rows[i]["居住地址"];
                }
                //this.gc健康体检Num.DataSource = ds.Tables[0];
                //this.pagerControl2.DrawControl();
                //this.gv健康体检Num.BestFitColumns();//列自适应宽度         
            }
            e.Result = dt;
        }

        void back_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            DataTable dt = e.Result as DataTable;
            if (this.cbo人数人次.Text.Trim() == "请选择" || this.cbo人数人次.Text.Trim() == "人次")
            {
                this.gc健康体检.DataSource = dt;
                this.pagerControl1.DrawControl();
                this.gv健康体检.BestFitColumns();//列自适应宽度         
            }
            else
            {
                this.gc健康体检Num.DataSource = dt;
                this.pagerControl2.DrawControl();
                this.gv健康体检Num.BestFitColumns();//列自适应宽度         
            }

        }
        private void btn查询_Click(object sender, EventArgs e)
        {
            if (funCheck())
            {
                back.RunWorkerAsync();
            }
        }
        private void BindDataNum()
        {
            DataTable dt = this.pagerControl2.GetQueryResultNew("vw_健康体检", "*", _strWhere, "ID", "DESC").Tables[0];
            //DataSet ds = this.pagerControl2.GetQueryResult("View_个人信息表", _strWhere);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //dt.Rows[i]["姓名"] = util.DESEncrypt.DES解密(dt.Rows[i]["姓名"].ToString());
                //dt.Rows[i]["性别"] = _buBLL.ReturnDis字典显示("xb_xingbie", dt.Rows[i]["性别"].ToString());
                dt.Rows[i]["性别"] = _buBLL.ReturnDis字典显示("zflx", dt.Rows[i]["性别"].ToString());
                dt.Rows[i]["居住地址"] = _buBLL.Return地区名称(dt.Rows[i]["街道"].ToString()) + _buBLL.Return地区名称(dt.Rows[i]["居委会"].ToString()) + dt.Rows[i]["居住地址"];
            }
            this.gc健康体检Num.DataSource = dt;
            this.pagerControl2.DrawControl();
            this.gv健康体检Num.BestFitColumns();//列自适应宽度         
        }
        private void BindDataList()
        {
            DataTable dt = this.pagerControl1.GetQueryResultNew("vw_健康体检", "*", _strWhere, "ID", "DESC").Tables[0];
            //DataSet ds = this.pagerControl1.GetQueryResult("vw_健康体检", _strWhere);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //dt.Rows[i]["姓名"] = util.DESEncrypt.DES解密(dt.Rows[i]["姓名"].ToString());
                dt.Rows[i]["创建人"] = _BLL.Return用户名称(dt.Rows[i]["创建人"].ToString());
                dt.Rows[i]["性别"] = _BLL.ReturnDis字典显示("xb_xingbie", dt.Rows[i]["性别"].ToString());
                dt.Rows[i]["所属机构"] = _BLL.Return机构名称(dt.Rows[i]["所属机构"].ToString());
                dt.Rows[i]["居住地址"] = _BLL.Return地区名称(dt.Rows[i]["街道"].ToString()) + _BLL.Return地区名称(dt.Rows[i]["居委会"].ToString()) + dt.Rows[i]["居住地址"];
            }
            this.gc健康体检.DataSource = dt;
            this.pagerControl1.DrawControl();
            this.gv健康体检.BestFitColumns();//列自适应宽度         
        }
        private bool funCheck()
        {
            if (!string.IsNullOrEmpty(dte出生日期2.Text.Trim()) && !string.IsNullOrEmpty(dte出生日期1.Text.Trim()) && this.dte出生日期1.DateTime > this.dte出生日期2.DateTime)
            {
                Msg.Warning("出生日期的结束日期必须大于开始时间！");
                this.dte出生日期2.Text = "";
                this.dte出生日期2.Focus();
                return false;
            }
            if (!string.IsNullOrEmpty(dte录入时间1.Text.Trim()) && !string.IsNullOrEmpty(dte录入时间2.Text.Trim()) && this.dte录入时间1.DateTime > this.dte录入时间2.DateTime)
            {
                Msg.Warning("录入时间的结束日期必须大于开始时间！");
                this.dte录入时间2.Text = "";
                this.dte录入时间2.Focus();
                return false;
            }
            if (!string.IsNullOrEmpty(dte体检时间1.Text.Trim()) && !string.IsNullOrEmpty(dte体检时间2.Text.Trim()) && this.dte体检时间1.DateTime > this.dte体检时间2.DateTime)
            {
                Msg.Warning("体检时间的结束日期必须大于开始时间！");
                this.dte体检时间2.Text = "";
                this.dte体检时间2.Focus();
                return false;
            }
            return true;
        }
        private void BindData()
        {


            DataSet ds = this.pagerControl1.GetQueryResult("vw_健康体检", _strWhere);
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                //ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                //ds.Tables[0].Rows[i]["性别"] = _BLL.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                ds.Tables[0].Rows[i]["性别"] = _BLL.ReturnDis字典显示("zflx", ds.Tables[0].Rows[i]["性别"].ToString());
                ds.Tables[0].Rows[i]["居住地址"] = ds.Tables[0].Rows[i]["街道"].ToString() + ds.Tables[0].Rows[i]["居委会"].ToString() + ds.Tables[0].Rows[i]["居住地址"];
            }
            this.gc健康体检.DataSource = ds.Tables[0];
            this.pagerControl1.DrawControl();
            this.gv健康体检.BestFitColumns();//列自适应宽度         
        }
        private void link档案号_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv健康体检.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            string date = row["创建时间"].ToString();
            //_BLL.GetBusinessByKey(个人档案编号, true);//下载一个空业务单据            
            //_BLL.NewBusiness(); //增加一条主表记录
            frm个人健康 frm = new frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, date);
            frm.Show();
        }
        private void frm健康体检表_Load(object sender, EventArgs e)
        {
            base.InitButtonsBase();
            InitView();
        }
        private void InitView()
        {
            this.page人次.PageVisible = true;
            this.page人数.PageVisible = false;
            this.tabControl.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;

            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, cbo性别, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案状态, cbo档案状态, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案类别, cbo档案类别, "P_CODE", "P_DESC");
            cbo档案状态.SelectedIndex = 1;
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t是否, cbo合格档案, "P_CODE", "P_DESC");
            //为“镇”绑定信息
            DataView dv镇 = new DataView(DataDictCache.Cache.t地区信息);
            dv镇.RowFilter = "上级编码=371323";
            //DataBinder.BindingLookupEditDataSource(lookUpEdit镇, dv市.ToTable(), "地区名称", "地区编码");
            util.ControlsHelper.BindComboxData(dv镇.ToTable(), comboBoxEdit镇, "地区编码", "地区名称");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t是否, cbo合格档案, "P_CODE", "P_DESC");
            //为“所属机构"绑定信息
            if (Loginer.CurrentUser.所属机构.Length >= 12)
                treeListLookUpEdit机构.Properties.AutoExpandAllNodes = true;
            else { treeListLookUpEdit机构.Properties.AutoExpandAllNodes = false; }
            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                treeListLookUpEdit机构.Properties.ValueMember = "机构编号";
                treeListLookUpEdit机构.Properties.DisplayMember = "机构名称";

                treeListLookUpEdit机构.Properties.TreeList.KeyFieldName = "机构编号";
                treeListLookUpEdit机构.Properties.TreeList.ParentFieldName = "上级机构";
                treeListLookUpEdit机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                treeListLookUpEdit机构.Properties.DataSource = dt所属机构;
                //treeListLookUpEdit机构.Properties.TreeList.ExpandAll();
                //treeListLookUpEdit机构.Properties.TreeList.CollapseAll();

                treeListLookUpEdit机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                treeListLookUpEdit机构.Text = Loginer.CurrentUser.所属机构;
            }

            try
            {
                DataTable dt录入人 = _bllUser.GetUserNameAndIDBy机构用户(treeListLookUpEdit机构.EditValue.ToString());
                cbo录入人.Properties.DisplayMember = "UserName";
                cbo录入人.Properties.ValueMember = "用户编码";
                cbo录入人.Properties.DataSource = dt录入人;
            }
            catch (Exception ex)
            {
                cbo录入人.Text = "请选择...";
            }
        }
        private void comboBoxEdit镇_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv村 = new DataView(DataDictCache.Cache.t地区信息);
                dv村.RowFilter = "上级编码='" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'";
                //DataBinder.BindingLookupEditDataSource(lookUpEdit村, dv县.ToTable(), "地区名称", "地区编码");
                util.ControlsHelper.BindComboxData(dv村.ToTable(), comboBoxEdit村, "地区编码", "地区名称");
                util.ControlsHelper.SetComboxNullData("", comboBoxEdit村, "请选择居/村委会");
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }
        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            BindDataList();
        }
        private void cbo人数人次_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cbo人数人次.Text.Trim() == "请选择" || this.cbo人数人次.Text.Trim() == "人次")
            {
                this.page人次.PageVisible = true;
                this.page人数.PageVisible = false;
                this.xtraTabPage1.PageVisible = false;
            }
            else if (this.cbo人数人次.Text.Trim() == "人数")
            {
                this.page人次.PageVisible = false;
                this.page人数.PageVisible = true;
                this.xtraTabPage1.PageVisible = false;
            }
        }
        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv健康体检Num.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            string date = row["创建时间"].ToString();
            //_BLL.GetBusinessByKey(个人档案编号, true);//下载一个空业务单据            
            //_BLL.NewBusiness(); //增加一条主表记录
            frm个人健康 frm = new frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, date);
            frm.Show();
        }
        private void pagerControl2_OnPageChanged(object sender, EventArgs e)
        {
            BindDataNum();
        }
        private void btn导出_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "XLS file(*.xls)|*.xls";//"PDF file(*.pdf)|*.pdf";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                DataTable dt = gc健康体检.DataSource as DataTable;
                Business.bll机构信息 bll = new Business.bll机构信息();
                DataTable newtable = new AtomEHR.Business.BLL_Base.bllBase().Get全部数据("vw_健康体检", "*", _strWhere, "ID", "DESC");
                for (int i = 0; i < newtable.Rows.Count; i++)
                {
                    //newtable.Rows[i]["姓名"] = Common.DESEncrypt.DES解密(newtable.Rows[i]["姓名"].ToString());
                    newtable.Rows[i]["居住地址"] = newtable.Rows[i]["街道"].ToString() + newtable.Rows[i]["居委会"].ToString() + newtable.Rows[i]["居住地址"].ToString();
                    newtable.Rows[i]["所属机构"] = newtable.Rows[i]["所属机构名称"].ToString();
                    newtable.Rows[i]["创建人"] = bll.Return用户名称(newtable.Rows[i]["创建人"].ToString());
                }
                try
                {

                    gridControl1.DataSource = newtable;
                    gridView1.ExportToXls(dlg.FileName);
                }
                catch (Exception ex)
                {
                    Msg.ShowError("导出失败！" + ex.Message);
                }
                finally
                {
                    gridControl1.DataSource = null;
                }

                //gc健康体检.DataSource = newtable;
                //gv健康体检.ExportToXls(dlg.FileName);
                //gc健康体检.DataSource = dt;
                //view.ExportToXls(dlg.FileName);
                //ExportToExcel(table, dlg.FileName);
                //DevExpress.XtraGrid.GridControl lsgrid = new DevExpress.XtraGrid.GridControl();
                //DevExpress.XtraGrid.Views.Grid.GridView gvlsgrid = new DevExpress.XtraGrid.Views.Grid.GridView();
                //lsgrid.MainView = gvlsgrid;
                //lsgrid.DataSource = newtable;
                //lsgrid.ExportToXls(dlg.FileName);
                //gvlsgrid.ExportToXls(dlg.FileName);
                //AtomEHR.Library.frmGridCustomize.ExportToExcel(newtable, dlg.FileName);
            }
        }

        private void treeListLookUpEdit机构_EditValueChanged(object sender, EventArgs e)
        {
            DataTable dt录入人 = _bllUser.GetUserNameAndIDBy机构用户(treeListLookUpEdit机构.EditValue.ToString());
            cbo录入人.Properties.DisplayMember = "UserName";
            cbo录入人.Properties.ValueMember = "用户编码";
            cbo录入人.Properties.DataSource = dt录入人;
        }

        private void btn微信推送_Click(object sender, EventArgs e)
        {
            try
            {
                string rows = "";
                for (int i = 0; i < this.gv健康体检.GetSelectedRows().Length; i++)
                {
                    DataRowView dr = this.gv健康体检.GetRow(this.gv健康体检.GetSelectedRows()[i]) as DataRowView;

                    if (dr == null) return;
                    string 身份证号 = dr["身份证号"] as string;
                    string 体检日期 = dr["体检日期"] as string;
                    string 姓名 = dr["姓名"] as string;
                    string[] keyWord = new string[2];
                    keyWord[0] = "您的体检信息报告已完成！祝您身体健康，生活愉快。";
                    keyWord[1] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    if (!string.IsNullOrEmpty(身份证号))
                        _BLL.SendWxMessage("yFwL5QocGuRn9jknf0_9vNXoAYYnKNxy361Vle0MiWc"
                        , 身份证号, "tj", "尊敬的[" + 姓名 + "]您好！", "点击详情查看", 体检日期, keyWord);
                    rows = (i + 1).ToString();
                }
                Msg.ShowInformation("本次推送" + rows + "条体检数据!");
            }
            catch 
            {

            }
        }

        private void btn综合打印_Click(object sender, EventArgs e)
        {
            //DataRowView dr = this.gv个人健康档案.GetRow(this.gv个人健康档案.GetSelectedRows()[0]) as DataRowView;
            string s档案号 = this.gv健康体检.GetRowCellValue(this.gv健康体检.GetSelectedRows()[0], "个人档案编号").ToString();//dr["个人档案编号"].ToString();
            if (!string.IsNullOrEmpty(s档案号))
            {
                AtomEHR.公共卫生.ReportFrm.frm_ReportALL repo = new AtomEHR.公共卫生.ReportFrm.frm_ReportALL(s档案号);
                repo.ShowDialog();
            }
        }

        private void btn体检报告_Click(object sender, EventArgs e)
        {
            if (this.gv健康体检.SelectedRowsCount == 1)
            {
                DataRow row = this.gv健康体检.GetFocusedDataRow();
                if (row == null) return;
                string docNo = row["个人档案编号"] as string;
                string date = row["体检日期"] as string;
                frm详细打印表单 frm = new frm详细打印表单(row);
                if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {

                }
            }
        }
    }
}
