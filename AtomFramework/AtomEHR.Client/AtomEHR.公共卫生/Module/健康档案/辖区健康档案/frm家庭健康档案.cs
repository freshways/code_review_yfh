﻿using AtomEHR.Common;
using AtomEHR.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.公共卫生.Module.个人健康;
using AtomEHR.Business.Security;
using AtomEHR.Models;

namespace AtomEHR.公共卫生.Module.健康档案.辖区健康档案
{
    public partial class frm家庭健康档案 : AtomEHR.Library.frmBaseBusinessForm
    {
        string _strWhere;
        public frm家庭健康档案()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 初始化窗体
        /// </summary>
        protected override void InitializeForm()
        {
            _BLL = new bll健康档案();// 业务逻辑层实例
            _SummaryView = new DevGridView(gv家庭档案编号);
            _ActiveEditor = txt姓名;
            _DetailGroupControl = groupControl1;
            
            base.InitButtonsBase();
             frmGridCustomize.RegisterGrid(gv家庭档案编号);
            //DevStyle.SetGridControlLayout(gc家庭档案编号, false);//表格设置   
            //DevStyle.SetSummaryGridViewLayout(gv家庭档案编号);

            // BindingSummaryNavigator(controlNavigatorSummary, gc家庭档案编号); //Summary导航条.
            // BindingSummarySearchPanel(btnQuery, btnEmpty, panelControl2);


            //txt_DocDateTo.DateTime = DateTime.Today; //查询条件截止日期

            //DataBinder.BindingLookupEditDataSource(txt创建人, DataDictCache.Cache.User, TUser.UserName, TUser.Account);//绑定弹窗控件

            //_BLL.GetBusinessByKey("-", true);//加载一个空的业务对象.

            // ShowSummaryPage(true); //一切初始化完毕后显示SummaryPage        
        }
        
        private void frm家庭健康档案_Load(object sender, EventArgs e)
        {
            this.tpDetail.PageVisible = false;
            InitView();
            this.InitializeForm(); //这行代码放到初始化变量后最好

        }

        private void InitView()
        {

            //为“镇”绑定信息
            DataView dv镇 = new DataView(DataDictCache.Cache.t地区信息);
            dv镇.RowFilter = "上级编码=371323";
            //DataBinder.BindingLookupEditDataSource(lookUpEdit镇, dv市.ToTable(), "地区名称", "地区编码");
            util.ControlsHelper.BindComboxData(dv镇.ToTable(), comboBoxEdit镇, "地区编码", "地区名称");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t是否, cbo合格档案, "P_CODE", "P_DESC");
            //为“所属机构"绑定信息
            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                treeListLookUpEdit机构.Properties.ValueMember = "机构编号";
                treeListLookUpEdit机构.Properties.DisplayMember = "机构名称";

                treeListLookUpEdit机构.Properties.TreeList.KeyFieldName = "机构编号";
                treeListLookUpEdit机构.Properties.TreeList.ParentFieldName = "上级机构";
                treeListLookUpEdit机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                treeListLookUpEdit机构.Properties.DataSource = dt所属机构;
                //treeListLookUpEdit机构.Properties.TreeList.ExpandAll();
                //treeListLookUpEdit机构.Properties.TreeList.CollapseAll();

                treeListLookUpEdit机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                treeListLookUpEdit机构.Text = Loginer.CurrentUser.所属机构;
            }

            try
            {
                DataTable dt录入人 = new bllUser().GetUserNameAndIDBy机构用户(treeListLookUpEdit机构.EditValue.ToString());
                cbo录入人.Properties.DisplayMember = "UserName";
                cbo录入人.Properties.ValueMember = "用户编码";
                cbo录入人.Properties.DataSource = dt录入人;
            }
            catch (Exception ex)
            {
                cbo录入人.Text = "请选择...";
            }
        }
        private void btnQuery_Click(object sender, EventArgs e)
        {
            if (funCheck())
            {
                _strWhere = string.Empty;
                string pgrid = this.treeListLookUpEdit机构.EditValue.ToString();
                if (this.chk含下属机构.Checked)//包含下属机构
                {
                    if (pgrid.Length == 12)
                    {
                        _strWhere += " and ([所属机构]=  '" + pgrid + "' or substring([所属机构],1,7)+'1'+substring([所属机构],9,7) like '" + pgrid + "%')";
                    }
                    else
                    {
                        _strWhere += " and [所属机构] like '" + pgrid + "%'";
                    }
                }
                else
                {
                    _strWhere += " and [所属机构] ='" + pgrid + "'";
                }

                if (this.txt户主姓名.Text.Trim() != "")
                {
                    _strWhere += " and (姓名 like'" + this.txt户主姓名.Text.Trim() + "%') ";
                }
                if (this.txt户主档案编号.Text.Trim() != "")
                {
                    _strWhere += " and 个人档案编号 ='" + this.txt户主档案编号.Text.Trim() + "'";
                }
                if (this.txt身份证号.Text.Trim() != "")
                {
                    _strWhere += " and 身份证号 ='" + this.txt身份证号.Text.Trim() + "'";
                }
                if (this.txt家庭档案编号.Text.Trim() != "")
                {
                    _strWhere += " and 家庭档案编号 ='" + this.txt家庭档案编号.Text.Trim() + "'";
                }
                if (util.ControlsHelper.GetComboxKey(comboBoxEdit村) != "")
                {
                    _strWhere += " and 街道 ='" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'";
                }
                if (util.ControlsHelper.GetComboxKey(comboBoxEdit村) != "")
                {
                    _strWhere += " and [居委会] ='" + util.ControlsHelper.GetComboxKey(comboBoxEdit村) + "'";
                }
                if (this.txt详细地址.Text.Trim() != "")
                {
                    _strWhere += " and [居住地址] like '" + this.txt详细地址.Text.Trim() + "%'";
                }
                if (this.cbo录入人.Text.Trim() != "")
                {
                    _strWhere += " and [创建人] = '" + this.cbo录入人.EditValue.ToString() + "'";
                }
                if (this.uC时间段1.Dte1.Text.Trim() != "")
                {
                    _strWhere += " and [创建时间] >= '" + this.uC时间段1.Dte1.Text.Trim() + " 00:00:00'";
                }
                if (this.uC时间段1.Dte2.Text.Trim() != "")
                {
                    _strWhere += " and [创建时间] <= '" + this.uC时间段1.Dte2.Text.Trim() + " 23:59:59'";
                }

                if (util.ControlsHelper.GetComboxKey(this.cbo合格档案) != "")
                {
                    if (util.ControlsHelper.GetComboxKey(this.cbo合格档案) == "1")//合格 ， 缺项为0
                    {
                        _strWhere += " and [缺少项] = '0'";
                    }
                    else
                    {
                        _strWhere += " and [缺少项] <> '0'";

                    }
                }
                _strWhere += " and 档案状态 = '1'  ";
            }
            BindData();
        }
        //判断
        private bool funCheck()
        {
            if (this.uC时间段1.Dte1.DateTime < this.uC时间段1.Dte2.DateTime)
            {
                Msg.Warning("录入时间的结束日期必须大于开始时间！");
                this.uC时间段1.Dte2.Text = "";
                this.uC时间段1.Dte2.Focus();
                return false;
            }
            return true;
        }
        private void BindData()
        {
            DataSet ds = this.pagerControl1.GetQueryResult("vw_家庭档案", "*", _strWhere, "ID", "desc");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                //ds.Tables[0].Rows[i]["性别"] = _BLL.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                ds.Tables[0].Rows[i][tb_家庭档案.住房类型] = _BLL.ReturnDis字典显示("zflx", ds.Tables[0].Rows[i][tb_家庭档案.住房类型].ToString());
                ds.Tables[0].Rows[i][tb_家庭档案.厕所类型] = _BLL.ReturnDis字典显示("cslx", ds.Tables[0].Rows[i][tb_家庭档案.厕所类型].ToString());
                ds.Tables[0].Rows[i][tb_家庭档案.是否低保户] = _BLL.ReturnDis字典显示("sf_shifou", ds.Tables[0].Rows[i][tb_家庭档案.是否低保户].ToString());
                ds.Tables[0].Rows[i][tb_家庭档案.所属机构] = _BLL.Return机构名称(ds.Tables[0].Rows[i][tb_家庭档案.所属机构].ToString());
                ds.Tables[0].Rows[i][tb_家庭档案.创建人] = _BLL.Return用户名称(ds.Tables[0].Rows[i][tb_家庭档案.创建人].ToString());
                ds.Tables[0].Rows[i]["居住地址"] = _BLL.Return地区名称(ds.Tables[0].Rows[i]["街道"].ToString()) + _BLL.Return地区名称(ds.Tables[0].Rows[i]["居委会"].ToString()) + ds.Tables[0].Rows[i]["居住地址"];
            }
            this.gc家庭档案编号.DataSource = ds.Tables[0];
            this.pagerControl1.DrawControl();
            this.gv家庭档案编号.BestFitColumns();//列自适应宽度         
        }
        private void link家庭档案编号_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv家庭档案编号.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            //string 个人档案编号 = row["个人档案号码"] as string;
            //_BLL.GetBusinessByKey(个人档案编号, true);//下载一个空业务单据            
            //_BLL.NewBusiness(); //增加一条主表记录
            frm个人健康 frm = new frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, null);
            frm.Show();
        }
        private void layoutControlGroup2_Click(object sender, EventArgs e)
        {
            if (this.layoutControlGroup2.Expanded)//展开
            {
                this.panelControl1.Height = 130;
            }
            else
            {
                this.panelControl1.Height = 30;
            }
        }
        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            BindData();
        }
        private void comboBoxEdit镇_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv村 = new DataView(DataDictCache.Cache.t地区信息);
                dv村.RowFilter = "上级编码='" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'";
                //DataBinder.BindingLookupEditDataSource(lookUpEdit村, dv县.ToTable(), "地区名称", "地区编码");
                util.ControlsHelper.BindComboxData(dv村.ToTable(), comboBoxEdit村, "地区编码", "地区名称");
                util.ControlsHelper.SetComboxNullData("", comboBoxEdit村, "请选择居/村委会");
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }

        private void treeListLookUpEdit机构_EditValueChanged(object sender, EventArgs e)
        {
            DataTable dt录入人 = _bllUser.GetUserNameAndIDBy机构用户(treeListLookUpEdit机构.EditValue.ToString());
            cbo录入人.Properties.DisplayMember = "UserName";
            cbo录入人.Properties.ValueMember = "用户编码";
            cbo录入人.Properties.DataSource = dt录入人;
        }

    }
}
