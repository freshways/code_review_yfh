﻿namespace AtomEHR.公共卫生.Module.健康档案.辖区健康档案
{
    partial class frm详细打印表单
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.groupControl11 = new DevExpress.XtraEditors.GroupControl();
            this.ucTxtLbl1 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLblB超异常 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLblB超 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.groupControl10 = new DevExpress.XtraEditors.GroupControl();
            this.uc特禀质 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc气郁质 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc血瘀质 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc湿热质 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc痰湿质 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc阴虚质 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc阳虚质 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc气虚质 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc平和质 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.groupControl8 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl9 = new DevExpress.XtraEditors.GroupControl();
            this.textEditB超医师 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLbl体质指数 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLbl心电图异常 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLbl心电图 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.ucTxtLbl尿潜血 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLbl尿糖 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLbl尿胴体 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLbl尿蛋白 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.ucTxtLbl结合胆红素 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLbl总胆红素 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLbl白蛋白 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLbl血清谷草转氨酶 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLbl血清谷丙转氨酶 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.ucTxtLbl血钠浓度 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLbl血钾浓度 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLbl血尿素氮 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLbl血清肌酐 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.ucTxtLbl餐后2H血糖 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLbl空腹血糖 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.ucTxtLbl血清高密度脂蛋白胆固醇 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLbl血清低密度脂蛋白胆固醇 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLbl甘油三酯 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLbl总胆固醇 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.ucTxtLbl右侧血压 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLbl左侧血压 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.ucTxtLbl血小板 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLbl白细胞 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLbl血红蛋白 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.textEdit村庄 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit年龄 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit症状 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit体检项目 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit性别 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.btn打印2 = new DevExpress.XtraEditors.SimpleButton();
            this.btn打印 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit姓名 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.cbo体检医师 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo体检单位 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.txt结论建议 = new DevExpress.XtraRichEdit.RichEditControl();
            this.listBoxControl1 = new DevExpress.XtraEditors.ListBoxControl();
            this.panel2 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl11)).BeginInit();
            this.groupControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).BeginInit();
            this.groupControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).BeginInit();
            this.groupControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).BeginInit();
            this.groupControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditB超医师.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit村庄.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit年龄.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit症状.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit体检项目.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo体检医师.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo体检单位.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControl1)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.groupControl11);
            this.layoutControl1.Controls.Add(this.groupControl10);
            this.layoutControl1.Controls.Add(this.groupControl8);
            this.layoutControl1.Controls.Add(this.groupControl7);
            this.layoutControl1.Controls.Add(this.groupControl6);
            this.layoutControl1.Controls.Add(this.groupControl5);
            this.layoutControl1.Controls.Add(this.groupControl4);
            this.layoutControl1.Controls.Add(this.groupControl3);
            this.layoutControl1.Controls.Add(this.groupControl2);
            this.layoutControl1.Controls.Add(this.groupControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(966, 225);
            this.layoutControl1.TabIndex = 11;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // groupControl11
            // 
            this.groupControl11.Controls.Add(this.ucTxtLbl1);
            this.groupControl11.Controls.Add(this.labelControl32);
            this.groupControl11.Controls.Add(this.ucTxtLblB超异常);
            this.groupControl11.Controls.Add(this.labelControl36);
            this.groupControl11.Controls.Add(this.ucTxtLblB超);
            this.groupControl11.Location = new System.Drawing.Point(2, 142);
            this.groupControl11.Name = "groupControl11";
            this.groupControl11.Size = new System.Drawing.Size(111, 81);
            this.groupControl11.TabIndex = 13;
            this.groupControl11.Text = "B超";
            // 
            // ucTxtLbl1
            // 
            this.ucTxtLbl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl1.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl1.Lbl1Text = "";
            this.ucTxtLbl1.Location = new System.Drawing.Point(2, 94);
            this.ucTxtLbl1.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl1.Name = "ucTxtLbl1";
            this.ucTxtLbl1.Size = new System.Drawing.Size(107, 22);
            this.ucTxtLbl1.TabIndex = 8;
            this.ucTxtLbl1.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl32
            // 
            this.labelControl32.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl32.Location = new System.Drawing.Point(2, 80);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(36, 14);
            this.labelControl32.TabIndex = 7;
            this.labelControl32.Text = "血小板";
            // 
            // ucTxtLblB超异常
            // 
            this.ucTxtLblB超异常.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLblB超异常.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLblB超异常.Lbl1Text = "";
            this.ucTxtLblB超异常.Location = new System.Drawing.Point(2, 58);
            this.ucTxtLblB超异常.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLblB超异常.Name = "ucTxtLblB超异常";
            this.ucTxtLblB超异常.Size = new System.Drawing.Size(107, 22);
            this.ucTxtLblB超异常.TabIndex = 6;
            this.ucTxtLblB超异常.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl36
            // 
            this.labelControl36.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl36.Location = new System.Drawing.Point(2, 44);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(24, 14);
            this.labelControl36.TabIndex = 5;
            this.labelControl36.Text = "异常";
            // 
            // ucTxtLblB超
            // 
            this.ucTxtLblB超.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLblB超.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLblB超.Lbl1Text = "";
            this.ucTxtLblB超.Location = new System.Drawing.Point(2, 22);
            this.ucTxtLblB超.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLblB超.Name = "ucTxtLblB超";
            this.ucTxtLblB超.Size = new System.Drawing.Size(107, 22);
            this.ucTxtLblB超.TabIndex = 4;
            this.ucTxtLblB超.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // groupControl10
            // 
            this.groupControl10.Controls.Add(this.uc特禀质);
            this.groupControl10.Controls.Add(this.uc气郁质);
            this.groupControl10.Controls.Add(this.uc血瘀质);
            this.groupControl10.Controls.Add(this.uc湿热质);
            this.groupControl10.Controls.Add(this.uc痰湿质);
            this.groupControl10.Controls.Add(this.uc阴虚质);
            this.groupControl10.Controls.Add(this.uc阳虚质);
            this.groupControl10.Controls.Add(this.uc气虚质);
            this.groupControl10.Controls.Add(this.uc平和质);
            this.groupControl10.Location = new System.Drawing.Point(768, 2);
            this.groupControl10.Name = "groupControl10";
            this.groupControl10.Size = new System.Drawing.Size(196, 221);
            this.groupControl10.TabIndex = 13;
            this.groupControl10.Text = "中医体质辨识";
            // 
            // uc特禀质
            // 
            this.uc特禀质.Dock = System.Windows.Forms.DockStyle.Top;
            this.uc特禀质.Lbl1Size = new System.Drawing.Size(69, 18);
            this.uc特禀质.Lbl1Text = "特禀质";
            this.uc特禀质.Location = new System.Drawing.Point(2, 198);
            this.uc特禀质.Margin = new System.Windows.Forms.Padding(4);
            this.uc特禀质.Name = "uc特禀质";
            this.uc特禀质.Size = new System.Drawing.Size(192, 22);
            this.uc特禀质.TabIndex = 8;
            this.uc特禀质.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // uc气郁质
            // 
            this.uc气郁质.Dock = System.Windows.Forms.DockStyle.Top;
            this.uc气郁质.Lbl1Size = new System.Drawing.Size(69, 18);
            this.uc气郁质.Lbl1Text = "气郁质";
            this.uc气郁质.Location = new System.Drawing.Point(2, 176);
            this.uc气郁质.Margin = new System.Windows.Forms.Padding(4);
            this.uc气郁质.Name = "uc气郁质";
            this.uc气郁质.Size = new System.Drawing.Size(192, 22);
            this.uc气郁质.TabIndex = 7;
            this.uc气郁质.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // uc血瘀质
            // 
            this.uc血瘀质.Dock = System.Windows.Forms.DockStyle.Top;
            this.uc血瘀质.Lbl1Size = new System.Drawing.Size(69, 18);
            this.uc血瘀质.Lbl1Text = "血瘀质";
            this.uc血瘀质.Location = new System.Drawing.Point(2, 154);
            this.uc血瘀质.Margin = new System.Windows.Forms.Padding(4);
            this.uc血瘀质.Name = "uc血瘀质";
            this.uc血瘀质.Size = new System.Drawing.Size(192, 22);
            this.uc血瘀质.TabIndex = 6;
            this.uc血瘀质.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // uc湿热质
            // 
            this.uc湿热质.Dock = System.Windows.Forms.DockStyle.Top;
            this.uc湿热质.Lbl1Size = new System.Drawing.Size(69, 18);
            this.uc湿热质.Lbl1Text = "湿热质";
            this.uc湿热质.Location = new System.Drawing.Point(2, 132);
            this.uc湿热质.Margin = new System.Windows.Forms.Padding(4);
            this.uc湿热质.Name = "uc湿热质";
            this.uc湿热质.Size = new System.Drawing.Size(192, 22);
            this.uc湿热质.TabIndex = 5;
            this.uc湿热质.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // uc痰湿质
            // 
            this.uc痰湿质.Dock = System.Windows.Forms.DockStyle.Top;
            this.uc痰湿质.Lbl1Size = new System.Drawing.Size(69, 18);
            this.uc痰湿质.Lbl1Text = "痰湿质";
            this.uc痰湿质.Location = new System.Drawing.Point(2, 110);
            this.uc痰湿质.Margin = new System.Windows.Forms.Padding(4);
            this.uc痰湿质.Name = "uc痰湿质";
            this.uc痰湿质.Size = new System.Drawing.Size(192, 22);
            this.uc痰湿质.TabIndex = 4;
            this.uc痰湿质.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // uc阴虚质
            // 
            this.uc阴虚质.Dock = System.Windows.Forms.DockStyle.Top;
            this.uc阴虚质.Lbl1Size = new System.Drawing.Size(69, 18);
            this.uc阴虚质.Lbl1Text = "阴虚质";
            this.uc阴虚质.Location = new System.Drawing.Point(2, 88);
            this.uc阴虚质.Margin = new System.Windows.Forms.Padding(4);
            this.uc阴虚质.Name = "uc阴虚质";
            this.uc阴虚质.Size = new System.Drawing.Size(192, 22);
            this.uc阴虚质.TabIndex = 3;
            this.uc阴虚质.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // uc阳虚质
            // 
            this.uc阳虚质.Dock = System.Windows.Forms.DockStyle.Top;
            this.uc阳虚质.Lbl1Size = new System.Drawing.Size(69, 18);
            this.uc阳虚质.Lbl1Text = "阳虚质";
            this.uc阳虚质.Location = new System.Drawing.Point(2, 66);
            this.uc阳虚质.Margin = new System.Windows.Forms.Padding(4);
            this.uc阳虚质.Name = "uc阳虚质";
            this.uc阳虚质.Size = new System.Drawing.Size(192, 22);
            this.uc阳虚质.TabIndex = 2;
            this.uc阳虚质.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // uc气虚质
            // 
            this.uc气虚质.Dock = System.Windows.Forms.DockStyle.Top;
            this.uc气虚质.Lbl1Size = new System.Drawing.Size(69, 18);
            this.uc气虚质.Lbl1Text = "气虚质";
            this.uc气虚质.Location = new System.Drawing.Point(2, 44);
            this.uc气虚质.Margin = new System.Windows.Forms.Padding(4);
            this.uc气虚质.Name = "uc气虚质";
            this.uc气虚质.Size = new System.Drawing.Size(192, 22);
            this.uc气虚质.TabIndex = 1;
            this.uc气虚质.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // uc平和质
            // 
            this.uc平和质.Dock = System.Windows.Forms.DockStyle.Top;
            this.uc平和质.Lbl1Size = new System.Drawing.Size(69, 18);
            this.uc平和质.Lbl1Text = "平和质";
            this.uc平和质.Location = new System.Drawing.Point(2, 22);
            this.uc平和质.Margin = new System.Windows.Forms.Padding(4);
            this.uc平和质.Name = "uc平和质";
            this.uc平和质.Size = new System.Drawing.Size(192, 22);
            this.uc平和质.TabIndex = 0;
            this.uc平和质.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // groupControl8
            // 
            this.groupControl8.Controls.Add(this.groupControl9);
            this.groupControl8.Controls.Add(this.ucTxtLbl心电图异常);
            this.groupControl8.Controls.Add(this.labelControl25);
            this.groupControl8.Controls.Add(this.ucTxtLbl心电图);
            this.groupControl8.Controls.Add(this.labelControl26);
            this.groupControl8.Location = new System.Drawing.Point(664, 2);
            this.groupControl8.Name = "groupControl8";
            this.groupControl8.Size = new System.Drawing.Size(100, 221);
            this.groupControl8.TabIndex = 12;
            this.groupControl8.Text = "心电图";
            // 
            // groupControl9
            // 
            this.groupControl9.Controls.Add(this.textEditB超医师);
            this.groupControl9.Controls.Add(this.labelControl37);
            this.groupControl9.Controls.Add(this.ucTxtLbl体质指数);
            this.groupControl9.Controls.Add(this.labelControl29);
            this.groupControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl9.Location = new System.Drawing.Point(2, 94);
            this.groupControl9.Name = "groupControl9";
            this.groupControl9.Size = new System.Drawing.Size(96, 125);
            this.groupControl9.TabIndex = 1;
            this.groupControl9.Text = "其他";
            // 
            // textEditB超医师
            // 
            this.textEditB超医师.Dock = System.Windows.Forms.DockStyle.Top;
            this.textEditB超医师.Location = new System.Drawing.Point(2, 72);
            this.textEditB超医师.Name = "textEditB超医师";
            this.textEditB超医师.Size = new System.Drawing.Size(92, 20);
            this.textEditB超医师.TabIndex = 27;
            // 
            // labelControl37
            // 
            this.labelControl37.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl37.Location = new System.Drawing.Point(2, 58);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(43, 14);
            this.labelControl37.TabIndex = 28;
            this.labelControl37.Text = "B超医师";
            // 
            // ucTxtLbl体质指数
            // 
            this.ucTxtLbl体质指数.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl体质指数.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl体质指数.Lbl1Text = "";
            this.ucTxtLbl体质指数.Location = new System.Drawing.Point(2, 36);
            this.ucTxtLbl体质指数.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl体质指数.Name = "ucTxtLbl体质指数";
            this.ucTxtLbl体质指数.Size = new System.Drawing.Size(92, 22);
            this.ucTxtLbl体质指数.TabIndex = 26;
            this.ucTxtLbl体质指数.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl29
            // 
            this.labelControl29.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl29.Location = new System.Drawing.Point(2, 22);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(48, 14);
            this.labelControl29.TabIndex = 25;
            this.labelControl29.Text = "体质指数";
            // 
            // ucTxtLbl心电图异常
            // 
            this.ucTxtLbl心电图异常.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl心电图异常.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl心电图异常.Lbl1Text = "";
            this.ucTxtLbl心电图异常.Location = new System.Drawing.Point(2, 72);
            this.ucTxtLbl心电图异常.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl心电图异常.Name = "ucTxtLbl心电图异常";
            this.ucTxtLbl心电图异常.Size = new System.Drawing.Size(96, 22);
            this.ucTxtLbl心电图异常.TabIndex = 17;
            this.ucTxtLbl心电图异常.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl25
            // 
            this.labelControl25.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl25.Location = new System.Drawing.Point(2, 58);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(60, 14);
            this.labelControl25.TabIndex = 16;
            this.labelControl25.Text = "心电图异常";
            // 
            // ucTxtLbl心电图
            // 
            this.ucTxtLbl心电图.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl心电图.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl心电图.Lbl1Text = "";
            this.ucTxtLbl心电图.Location = new System.Drawing.Point(2, 36);
            this.ucTxtLbl心电图.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl心电图.Name = "ucTxtLbl心电图";
            this.ucTxtLbl心电图.Size = new System.Drawing.Size(96, 22);
            this.ucTxtLbl心电图.TabIndex = 15;
            this.ucTxtLbl心电图.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl26
            // 
            this.labelControl26.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl26.Location = new System.Drawing.Point(2, 22);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(36, 14);
            this.labelControl26.TabIndex = 14;
            this.labelControl26.Text = "心电图";
            // 
            // groupControl7
            // 
            this.groupControl7.Controls.Add(this.ucTxtLbl尿潜血);
            this.groupControl7.Controls.Add(this.labelControl7);
            this.groupControl7.Controls.Add(this.ucTxtLbl尿糖);
            this.groupControl7.Controls.Add(this.labelControl4);
            this.groupControl7.Controls.Add(this.ucTxtLbl尿胴体);
            this.groupControl7.Controls.Add(this.labelControl5);
            this.groupControl7.Controls.Add(this.ucTxtLbl尿蛋白);
            this.groupControl7.Controls.Add(this.labelControl6);
            this.groupControl7.Location = new System.Drawing.Point(117, 2);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.Size = new System.Drawing.Size(104, 221);
            this.groupControl7.TabIndex = 10;
            this.groupControl7.Text = "尿常规";
            // 
            // ucTxtLbl尿潜血
            // 
            this.ucTxtLbl尿潜血.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl尿潜血.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl尿潜血.Lbl1Text = "";
            this.ucTxtLbl尿潜血.Location = new System.Drawing.Point(2, 144);
            this.ucTxtLbl尿潜血.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl尿潜血.Name = "ucTxtLbl尿潜血";
            this.ucTxtLbl尿潜血.Size = new System.Drawing.Size(100, 22);
            this.ucTxtLbl尿潜血.TabIndex = 16;
            this.ucTxtLbl尿潜血.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl7
            // 
            this.labelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl7.Location = new System.Drawing.Point(2, 130);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(36, 14);
            this.labelControl7.TabIndex = 15;
            this.labelControl7.Text = "尿潜血";
            // 
            // ucTxtLbl尿糖
            // 
            this.ucTxtLbl尿糖.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl尿糖.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl尿糖.Lbl1Text = "";
            this.ucTxtLbl尿糖.Location = new System.Drawing.Point(2, 108);
            this.ucTxtLbl尿糖.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl尿糖.Name = "ucTxtLbl尿糖";
            this.ucTxtLbl尿糖.Size = new System.Drawing.Size(100, 22);
            this.ucTxtLbl尿糖.TabIndex = 14;
            this.ucTxtLbl尿糖.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl4
            // 
            this.labelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl4.Location = new System.Drawing.Point(2, 94);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(24, 14);
            this.labelControl4.TabIndex = 13;
            this.labelControl4.Text = "尿糖";
            // 
            // ucTxtLbl尿胴体
            // 
            this.ucTxtLbl尿胴体.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl尿胴体.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl尿胴体.Lbl1Text = "";
            this.ucTxtLbl尿胴体.Location = new System.Drawing.Point(2, 72);
            this.ucTxtLbl尿胴体.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl尿胴体.Name = "ucTxtLbl尿胴体";
            this.ucTxtLbl尿胴体.Size = new System.Drawing.Size(100, 22);
            this.ucTxtLbl尿胴体.TabIndex = 12;
            this.ucTxtLbl尿胴体.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl5
            // 
            this.labelControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl5.Location = new System.Drawing.Point(2, 58);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(36, 14);
            this.labelControl5.TabIndex = 11;
            this.labelControl5.Text = "尿胴体";
            // 
            // ucTxtLbl尿蛋白
            // 
            this.ucTxtLbl尿蛋白.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl尿蛋白.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl尿蛋白.Lbl1Text = "";
            this.ucTxtLbl尿蛋白.Location = new System.Drawing.Point(2, 36);
            this.ucTxtLbl尿蛋白.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl尿蛋白.Name = "ucTxtLbl尿蛋白";
            this.ucTxtLbl尿蛋白.Size = new System.Drawing.Size(100, 22);
            this.ucTxtLbl尿蛋白.TabIndex = 10;
            this.ucTxtLbl尿蛋白.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl6
            // 
            this.labelControl6.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl6.Location = new System.Drawing.Point(2, 22);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(36, 14);
            this.labelControl6.TabIndex = 9;
            this.labelControl6.Text = "尿蛋白";
            // 
            // groupControl6
            // 
            this.groupControl6.Controls.Add(this.ucTxtLbl结合胆红素);
            this.groupControl6.Controls.Add(this.labelControl12);
            this.groupControl6.Controls.Add(this.ucTxtLbl总胆红素);
            this.groupControl6.Controls.Add(this.labelControl8);
            this.groupControl6.Controls.Add(this.ucTxtLbl白蛋白);
            this.groupControl6.Controls.Add(this.labelControl9);
            this.groupControl6.Controls.Add(this.ucTxtLbl血清谷草转氨酶);
            this.groupControl6.Controls.Add(this.labelControl10);
            this.groupControl6.Controls.Add(this.ucTxtLbl血清谷丙转氨酶);
            this.groupControl6.Controls.Add(this.labelControl11);
            this.groupControl6.Location = new System.Drawing.Point(225, 2);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(109, 221);
            this.groupControl6.TabIndex = 9;
            this.groupControl6.Text = "肝功";
            // 
            // ucTxtLbl结合胆红素
            // 
            this.ucTxtLbl结合胆红素.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl结合胆红素.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl结合胆红素.Lbl1Text = "";
            this.ucTxtLbl结合胆红素.Location = new System.Drawing.Point(2, 180);
            this.ucTxtLbl结合胆红素.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl结合胆红素.Name = "ucTxtLbl结合胆红素";
            this.ucTxtLbl结合胆红素.Size = new System.Drawing.Size(105, 22);
            this.ucTxtLbl结合胆红素.TabIndex = 26;
            this.ucTxtLbl结合胆红素.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl12
            // 
            this.labelControl12.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl12.Location = new System.Drawing.Point(2, 166);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(60, 14);
            this.labelControl12.TabIndex = 25;
            this.labelControl12.Text = "结合胆红素";
            // 
            // ucTxtLbl总胆红素
            // 
            this.ucTxtLbl总胆红素.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl总胆红素.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl总胆红素.Lbl1Text = "";
            this.ucTxtLbl总胆红素.Location = new System.Drawing.Point(2, 144);
            this.ucTxtLbl总胆红素.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl总胆红素.Name = "ucTxtLbl总胆红素";
            this.ucTxtLbl总胆红素.Size = new System.Drawing.Size(105, 22);
            this.ucTxtLbl总胆红素.TabIndex = 24;
            this.ucTxtLbl总胆红素.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl8
            // 
            this.labelControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl8.Location = new System.Drawing.Point(2, 130);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(48, 14);
            this.labelControl8.TabIndex = 23;
            this.labelControl8.Text = "总胆红素";
            // 
            // ucTxtLbl白蛋白
            // 
            this.ucTxtLbl白蛋白.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl白蛋白.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl白蛋白.Lbl1Text = "";
            this.ucTxtLbl白蛋白.Location = new System.Drawing.Point(2, 108);
            this.ucTxtLbl白蛋白.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl白蛋白.Name = "ucTxtLbl白蛋白";
            this.ucTxtLbl白蛋白.Size = new System.Drawing.Size(105, 22);
            this.ucTxtLbl白蛋白.TabIndex = 22;
            this.ucTxtLbl白蛋白.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl9
            // 
            this.labelControl9.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl9.Location = new System.Drawing.Point(2, 94);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(36, 14);
            this.labelControl9.TabIndex = 21;
            this.labelControl9.Text = "白蛋白";
            // 
            // ucTxtLbl血清谷草转氨酶
            // 
            this.ucTxtLbl血清谷草转氨酶.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl血清谷草转氨酶.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl血清谷草转氨酶.Lbl1Text = "";
            this.ucTxtLbl血清谷草转氨酶.Location = new System.Drawing.Point(2, 72);
            this.ucTxtLbl血清谷草转氨酶.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl血清谷草转氨酶.Name = "ucTxtLbl血清谷草转氨酶";
            this.ucTxtLbl血清谷草转氨酶.Size = new System.Drawing.Size(105, 22);
            this.ucTxtLbl血清谷草转氨酶.TabIndex = 20;
            this.ucTxtLbl血清谷草转氨酶.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl10
            // 
            this.labelControl10.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl10.Location = new System.Drawing.Point(2, 58);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(84, 14);
            this.labelControl10.TabIndex = 19;
            this.labelControl10.Text = "血清谷草转氨酶";
            // 
            // ucTxtLbl血清谷丙转氨酶
            // 
            this.ucTxtLbl血清谷丙转氨酶.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl血清谷丙转氨酶.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl血清谷丙转氨酶.Lbl1Text = "";
            this.ucTxtLbl血清谷丙转氨酶.Location = new System.Drawing.Point(2, 36);
            this.ucTxtLbl血清谷丙转氨酶.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl血清谷丙转氨酶.Name = "ucTxtLbl血清谷丙转氨酶";
            this.ucTxtLbl血清谷丙转氨酶.Size = new System.Drawing.Size(105, 22);
            this.ucTxtLbl血清谷丙转氨酶.TabIndex = 18;
            this.ucTxtLbl血清谷丙转氨酶.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl11
            // 
            this.labelControl11.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl11.Location = new System.Drawing.Point(2, 22);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(84, 14);
            this.labelControl11.TabIndex = 17;
            this.labelControl11.Text = "血清谷丙转氨酶";
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.ucTxtLbl血钠浓度);
            this.groupControl5.Controls.Add(this.labelControl13);
            this.groupControl5.Controls.Add(this.ucTxtLbl血钾浓度);
            this.groupControl5.Controls.Add(this.labelControl14);
            this.groupControl5.Controls.Add(this.ucTxtLbl血尿素氮);
            this.groupControl5.Controls.Add(this.labelControl15);
            this.groupControl5.Controls.Add(this.ucTxtLbl血清肌酐);
            this.groupControl5.Controls.Add(this.labelControl16);
            this.groupControl5.Location = new System.Drawing.Point(338, 2);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(114, 221);
            this.groupControl5.TabIndex = 8;
            this.groupControl5.Text = "肾功";
            // 
            // ucTxtLbl血钠浓度
            // 
            this.ucTxtLbl血钠浓度.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl血钠浓度.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl血钠浓度.Lbl1Text = "";
            this.ucTxtLbl血钠浓度.Location = new System.Drawing.Point(2, 144);
            this.ucTxtLbl血钠浓度.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl血钠浓度.Name = "ucTxtLbl血钠浓度";
            this.ucTxtLbl血钠浓度.Size = new System.Drawing.Size(110, 22);
            this.ucTxtLbl血钠浓度.TabIndex = 24;
            this.ucTxtLbl血钠浓度.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl13
            // 
            this.labelControl13.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl13.Location = new System.Drawing.Point(2, 130);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(48, 14);
            this.labelControl13.TabIndex = 23;
            this.labelControl13.Text = "血钠浓度";
            // 
            // ucTxtLbl血钾浓度
            // 
            this.ucTxtLbl血钾浓度.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl血钾浓度.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl血钾浓度.Lbl1Text = "";
            this.ucTxtLbl血钾浓度.Location = new System.Drawing.Point(2, 108);
            this.ucTxtLbl血钾浓度.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl血钾浓度.Name = "ucTxtLbl血钾浓度";
            this.ucTxtLbl血钾浓度.Size = new System.Drawing.Size(110, 22);
            this.ucTxtLbl血钾浓度.TabIndex = 22;
            this.ucTxtLbl血钾浓度.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl14
            // 
            this.labelControl14.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl14.Location = new System.Drawing.Point(2, 94);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(48, 14);
            this.labelControl14.TabIndex = 21;
            this.labelControl14.Text = "血钾浓度";
            // 
            // ucTxtLbl血尿素氮
            // 
            this.ucTxtLbl血尿素氮.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl血尿素氮.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl血尿素氮.Lbl1Text = "";
            this.ucTxtLbl血尿素氮.Location = new System.Drawing.Point(2, 72);
            this.ucTxtLbl血尿素氮.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl血尿素氮.Name = "ucTxtLbl血尿素氮";
            this.ucTxtLbl血尿素氮.Size = new System.Drawing.Size(110, 22);
            this.ucTxtLbl血尿素氮.TabIndex = 20;
            this.ucTxtLbl血尿素氮.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl15
            // 
            this.labelControl15.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl15.Location = new System.Drawing.Point(2, 58);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(48, 14);
            this.labelControl15.TabIndex = 19;
            this.labelControl15.Text = "血尿素氮";
            // 
            // ucTxtLbl血清肌酐
            // 
            this.ucTxtLbl血清肌酐.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl血清肌酐.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl血清肌酐.Lbl1Text = "";
            this.ucTxtLbl血清肌酐.Location = new System.Drawing.Point(2, 36);
            this.ucTxtLbl血清肌酐.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl血清肌酐.Name = "ucTxtLbl血清肌酐";
            this.ucTxtLbl血清肌酐.Size = new System.Drawing.Size(110, 22);
            this.ucTxtLbl血清肌酐.TabIndex = 18;
            this.ucTxtLbl血清肌酐.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl16
            // 
            this.labelControl16.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl16.Location = new System.Drawing.Point(2, 22);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(48, 14);
            this.labelControl16.TabIndex = 17;
            this.labelControl16.Text = "血清肌酐";
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.ucTxtLbl餐后2H血糖);
            this.groupControl4.Controls.Add(this.labelControl17);
            this.groupControl4.Controls.Add(this.ucTxtLbl空腹血糖);
            this.groupControl4.Controls.Add(this.labelControl18);
            this.groupControl4.Location = new System.Drawing.Point(456, 2);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(100, 108);
            this.groupControl4.TabIndex = 7;
            this.groupControl4.Text = "血糖";
            // 
            // ucTxtLbl餐后2H血糖
            // 
            this.ucTxtLbl餐后2H血糖.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl餐后2H血糖.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl餐后2H血糖.Lbl1Text = "";
            this.ucTxtLbl餐后2H血糖.Location = new System.Drawing.Point(2, 72);
            this.ucTxtLbl餐后2H血糖.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl餐后2H血糖.Name = "ucTxtLbl餐后2H血糖";
            this.ucTxtLbl餐后2H血糖.Size = new System.Drawing.Size(96, 22);
            this.ucTxtLbl餐后2H血糖.TabIndex = 13;
            this.ucTxtLbl餐后2H血糖.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl17
            // 
            this.labelControl17.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl17.Location = new System.Drawing.Point(2, 58);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(63, 14);
            this.labelControl17.TabIndex = 12;
            this.labelControl17.Text = "餐后2H血糖";
            // 
            // ucTxtLbl空腹血糖
            // 
            this.ucTxtLbl空腹血糖.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl空腹血糖.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl空腹血糖.Lbl1Text = "";
            this.ucTxtLbl空腹血糖.Location = new System.Drawing.Point(2, 36);
            this.ucTxtLbl空腹血糖.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl空腹血糖.Name = "ucTxtLbl空腹血糖";
            this.ucTxtLbl空腹血糖.Size = new System.Drawing.Size(96, 22);
            this.ucTxtLbl空腹血糖.TabIndex = 11;
            this.ucTxtLbl空腹血糖.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl18
            // 
            this.labelControl18.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl18.Location = new System.Drawing.Point(2, 22);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(48, 14);
            this.labelControl18.TabIndex = 10;
            this.labelControl18.Text = "空腹血糖";
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.ucTxtLbl血清高密度脂蛋白胆固醇);
            this.groupControl3.Controls.Add(this.labelControl19);
            this.groupControl3.Controls.Add(this.ucTxtLbl血清低密度脂蛋白胆固醇);
            this.groupControl3.Controls.Add(this.labelControl20);
            this.groupControl3.Controls.Add(this.ucTxtLbl甘油三酯);
            this.groupControl3.Controls.Add(this.labelControl21);
            this.groupControl3.Controls.Add(this.ucTxtLbl总胆固醇);
            this.groupControl3.Controls.Add(this.labelControl22);
            this.groupControl3.Location = new System.Drawing.Point(560, 2);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(100, 221);
            this.groupControl3.TabIndex = 6;
            this.groupControl3.Text = "血脂";
            // 
            // ucTxtLbl血清高密度脂蛋白胆固醇
            // 
            this.ucTxtLbl血清高密度脂蛋白胆固醇.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl血清高密度脂蛋白胆固醇.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl血清高密度脂蛋白胆固醇.Lbl1Text = "";
            this.ucTxtLbl血清高密度脂蛋白胆固醇.Location = new System.Drawing.Point(2, 144);
            this.ucTxtLbl血清高密度脂蛋白胆固醇.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl血清高密度脂蛋白胆固醇.Name = "ucTxtLbl血清高密度脂蛋白胆固醇";
            this.ucTxtLbl血清高密度脂蛋白胆固醇.Size = new System.Drawing.Size(96, 22);
            this.ucTxtLbl血清高密度脂蛋白胆固醇.TabIndex = 24;
            this.ucTxtLbl血清高密度脂蛋白胆固醇.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl19
            // 
            this.labelControl19.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl19.Location = new System.Drawing.Point(2, 130);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(132, 14);
            this.labelControl19.TabIndex = 23;
            this.labelControl19.Text = "血清高密度脂蛋白胆固醇";
            // 
            // ucTxtLbl血清低密度脂蛋白胆固醇
            // 
            this.ucTxtLbl血清低密度脂蛋白胆固醇.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl血清低密度脂蛋白胆固醇.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl血清低密度脂蛋白胆固醇.Lbl1Text = "";
            this.ucTxtLbl血清低密度脂蛋白胆固醇.Location = new System.Drawing.Point(2, 108);
            this.ucTxtLbl血清低密度脂蛋白胆固醇.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl血清低密度脂蛋白胆固醇.Name = "ucTxtLbl血清低密度脂蛋白胆固醇";
            this.ucTxtLbl血清低密度脂蛋白胆固醇.Size = new System.Drawing.Size(96, 22);
            this.ucTxtLbl血清低密度脂蛋白胆固醇.TabIndex = 22;
            this.ucTxtLbl血清低密度脂蛋白胆固醇.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl20
            // 
            this.labelControl20.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl20.Location = new System.Drawing.Point(2, 94);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(132, 14);
            this.labelControl20.TabIndex = 21;
            this.labelControl20.Text = "血清低密度脂蛋白胆固醇";
            // 
            // ucTxtLbl甘油三酯
            // 
            this.ucTxtLbl甘油三酯.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl甘油三酯.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl甘油三酯.Lbl1Text = "";
            this.ucTxtLbl甘油三酯.Location = new System.Drawing.Point(2, 72);
            this.ucTxtLbl甘油三酯.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl甘油三酯.Name = "ucTxtLbl甘油三酯";
            this.ucTxtLbl甘油三酯.Size = new System.Drawing.Size(96, 22);
            this.ucTxtLbl甘油三酯.TabIndex = 20;
            this.ucTxtLbl甘油三酯.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl21
            // 
            this.labelControl21.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl21.Location = new System.Drawing.Point(2, 58);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(48, 14);
            this.labelControl21.TabIndex = 19;
            this.labelControl21.Text = "甘油三酯";
            // 
            // ucTxtLbl总胆固醇
            // 
            this.ucTxtLbl总胆固醇.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl总胆固醇.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl总胆固醇.Lbl1Text = "";
            this.ucTxtLbl总胆固醇.Location = new System.Drawing.Point(2, 36);
            this.ucTxtLbl总胆固醇.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl总胆固醇.Name = "ucTxtLbl总胆固醇";
            this.ucTxtLbl总胆固醇.Size = new System.Drawing.Size(96, 22);
            this.ucTxtLbl总胆固醇.TabIndex = 18;
            this.ucTxtLbl总胆固醇.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl22
            // 
            this.labelControl22.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl22.Location = new System.Drawing.Point(2, 22);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(48, 14);
            this.labelControl22.TabIndex = 17;
            this.labelControl22.Text = "总胆固醇";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.ucTxtLbl右侧血压);
            this.groupControl2.Controls.Add(this.labelControl23);
            this.groupControl2.Controls.Add(this.ucTxtLbl左侧血压);
            this.groupControl2.Controls.Add(this.labelControl24);
            this.groupControl2.Location = new System.Drawing.Point(456, 114);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(100, 109);
            this.groupControl2.TabIndex = 5;
            this.groupControl2.Text = "血压";
            // 
            // ucTxtLbl右侧血压
            // 
            this.ucTxtLbl右侧血压.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl右侧血压.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl右侧血压.Lbl1Text = "";
            this.ucTxtLbl右侧血压.Location = new System.Drawing.Point(2, 72);
            this.ucTxtLbl右侧血压.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl右侧血压.Name = "ucTxtLbl右侧血压";
            this.ucTxtLbl右侧血压.Size = new System.Drawing.Size(96, 22);
            this.ucTxtLbl右侧血压.TabIndex = 17;
            this.ucTxtLbl右侧血压.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl23
            // 
            this.labelControl23.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl23.Location = new System.Drawing.Point(2, 58);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(48, 14);
            this.labelControl23.TabIndex = 16;
            this.labelControl23.Text = "右侧血压";
            // 
            // ucTxtLbl左侧血压
            // 
            this.ucTxtLbl左侧血压.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl左侧血压.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl左侧血压.Lbl1Text = "";
            this.ucTxtLbl左侧血压.Location = new System.Drawing.Point(2, 36);
            this.ucTxtLbl左侧血压.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl左侧血压.Name = "ucTxtLbl左侧血压";
            this.ucTxtLbl左侧血压.Size = new System.Drawing.Size(96, 22);
            this.ucTxtLbl左侧血压.TabIndex = 15;
            this.ucTxtLbl左侧血压.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl24
            // 
            this.labelControl24.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl24.Location = new System.Drawing.Point(2, 22);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(48, 14);
            this.labelControl24.TabIndex = 14;
            this.labelControl24.Text = "左侧血压";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.ucTxtLbl血小板);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.ucTxtLbl白细胞);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.ucTxtLbl血红蛋白);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(2, 2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(111, 136);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "血常规";
            // 
            // ucTxtLbl血小板
            // 
            this.ucTxtLbl血小板.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl血小板.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl血小板.Lbl1Text = "";
            this.ucTxtLbl血小板.Location = new System.Drawing.Point(2, 108);
            this.ucTxtLbl血小板.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl血小板.Name = "ucTxtLbl血小板";
            this.ucTxtLbl血小板.Size = new System.Drawing.Size(107, 22);
            this.ucTxtLbl血小板.TabIndex = 8;
            this.ucTxtLbl血小板.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl3
            // 
            this.labelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl3.Location = new System.Drawing.Point(2, 94);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(36, 14);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "血小板";
            // 
            // ucTxtLbl白细胞
            // 
            this.ucTxtLbl白细胞.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl白细胞.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl白细胞.Lbl1Text = "";
            this.ucTxtLbl白细胞.Location = new System.Drawing.Point(2, 72);
            this.ucTxtLbl白细胞.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl白细胞.Name = "ucTxtLbl白细胞";
            this.ucTxtLbl白细胞.Size = new System.Drawing.Size(107, 22);
            this.ucTxtLbl白细胞.TabIndex = 6;
            this.ucTxtLbl白细胞.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl2
            // 
            this.labelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl2.Location = new System.Drawing.Point(2, 58);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(36, 14);
            this.labelControl2.TabIndex = 5;
            this.labelControl2.Text = "白细胞";
            // 
            // ucTxtLbl血红蛋白
            // 
            this.ucTxtLbl血红蛋白.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucTxtLbl血红蛋白.Lbl1Size = new System.Drawing.Size(87, 21);
            this.ucTxtLbl血红蛋白.Lbl1Text = "";
            this.ucTxtLbl血红蛋白.Location = new System.Drawing.Point(2, 36);
            this.ucTxtLbl血红蛋白.Margin = new System.Windows.Forms.Padding(4);
            this.ucTxtLbl血红蛋白.Name = "ucTxtLbl血红蛋白";
            this.ucTxtLbl血红蛋白.Size = new System.Drawing.Size(107, 22);
            this.ucTxtLbl血红蛋白.TabIndex = 4;
            this.ucTxtLbl血红蛋白.Txt1Size = new System.Drawing.Size(117, 20);
            // 
            // labelControl1
            // 
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl1.Location = new System.Drawing.Point(2, 22);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(48, 14);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "血红蛋白";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem2,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(966, 225);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.groupControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 140);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(115, 140);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(115, 140);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.groupControl3;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(558, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(104, 225);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.groupControl4;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(454, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(104, 112);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.groupControl5;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(336, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(118, 225);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.groupControl6;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(223, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(113, 225);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.groupControl7;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(115, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(108, 225);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.groupControl2;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(454, 112);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(104, 113);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.groupControl8;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(662, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(104, 225);
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.groupControl10;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(766, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(200, 225);
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.groupControl11;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 140);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(115, 85);
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.textEdit村庄);
            this.panelControl1.Controls.Add(this.labelControl35);
            this.panelControl1.Controls.Add(this.textEdit年龄);
            this.panelControl1.Controls.Add(this.labelControl34);
            this.panelControl1.Controls.Add(this.textEdit症状);
            this.panelControl1.Controls.Add(this.textEdit体检项目);
            this.panelControl1.Controls.Add(this.labelControl33);
            this.panelControl1.Controls.Add(this.labelControl30);
            this.panelControl1.Controls.Add(this.labelControl31);
            this.panelControl1.Controls.Add(this.textEdit性别);
            this.panelControl1.Controls.Add(this.labelControl28);
            this.panelControl1.Controls.Add(this.btn打印2);
            this.panelControl1.Controls.Add(this.btn打印);
            this.panelControl1.Controls.Add(this.textEdit姓名);
            this.panelControl1.Controls.Add(this.labelControl27);
            this.panelControl1.Controls.Add(this.cbo体检医师);
            this.panelControl1.Controls.Add(this.cbo体检单位);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(966, 115);
            this.panelControl1.TabIndex = 1;
            // 
            // textEdit村庄
            // 
            this.textEdit村庄.EditValue = "";
            this.textEdit村庄.Location = new System.Drawing.Point(406, 10);
            this.textEdit村庄.Name = "textEdit村庄";
            this.textEdit村庄.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.textEdit村庄.Properties.Appearance.Options.UseFont = true;
            this.textEdit村庄.Size = new System.Drawing.Size(199, 26);
            this.textEdit村庄.TabIndex = 16;
            // 
            // labelControl35
            // 
            this.labelControl35.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl35.Location = new System.Drawing.Point(368, 13);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(32, 19);
            this.labelControl35.TabIndex = 15;
            this.labelControl35.Text = "村庄";
            // 
            // textEdit年龄
            // 
            this.textEdit年龄.EditValue = "";
            this.textEdit年龄.Location = new System.Drawing.Point(292, 10);
            this.textEdit年龄.Name = "textEdit年龄";
            this.textEdit年龄.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.textEdit年龄.Properties.Appearance.Options.UseFont = true;
            this.textEdit年龄.Size = new System.Drawing.Size(55, 26);
            this.textEdit年龄.TabIndex = 14;
            // 
            // labelControl34
            // 
            this.labelControl34.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl34.Location = new System.Drawing.Point(254, 13);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(32, 19);
            this.labelControl34.TabIndex = 13;
            this.labelControl34.Text = "年龄";
            // 
            // textEdit症状
            // 
            this.textEdit症状.EditValue = "";
            this.textEdit症状.Location = new System.Drawing.Point(565, 78);
            this.textEdit症状.Name = "textEdit症状";
            this.textEdit症状.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.textEdit症状.Properties.Appearance.Options.UseFont = true;
            this.textEdit症状.Size = new System.Drawing.Size(77, 26);
            this.textEdit症状.TabIndex = 10;
            // 
            // textEdit体检项目
            // 
            this.textEdit体检项目.EditValue = "血常规、尿常规、肝功、肾功、血糖、血脂、血压、心电图";
            this.textEdit体检项目.Location = new System.Drawing.Point(82, 78);
            this.textEdit体检项目.Name = "textEdit体检项目";
            this.textEdit体检项目.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.textEdit体检项目.Properties.Appearance.Options.UseFont = true;
            this.textEdit体检项目.Size = new System.Drawing.Size(477, 26);
            this.textEdit体检项目.TabIndex = 10;
            // 
            // labelControl33
            // 
            this.labelControl33.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl33.Location = new System.Drawing.Point(12, 78);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(64, 19);
            this.labelControl33.TabIndex = 9;
            this.labelControl33.Text = "体检项目";
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl30.Location = new System.Drawing.Point(255, 44);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(64, 19);
            this.labelControl30.TabIndex = 7;
            this.labelControl30.Text = "体检单位";
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl31.Location = new System.Drawing.Point(12, 44);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(64, 19);
            this.labelControl31.TabIndex = 5;
            this.labelControl31.Text = "体检医师";
            // 
            // textEdit性别
            // 
            this.textEdit性别.EditValue = "";
            this.textEdit性别.Location = new System.Drawing.Point(175, 10);
            this.textEdit性别.Name = "textEdit性别";
            this.textEdit性别.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.textEdit性别.Properties.Appearance.Options.UseFont = true;
            this.textEdit性别.Size = new System.Drawing.Size(68, 26);
            this.textEdit性别.TabIndex = 4;
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl28.Location = new System.Drawing.Point(137, 13);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(32, 19);
            this.labelControl28.TabIndex = 3;
            this.labelControl28.Text = "性别";
            // 
            // btn打印2
            // 
            this.btn打印2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn打印2.Appearance.Options.UseFont = true;
            this.btn打印2.Location = new System.Drawing.Point(824, 18);
            this.btn打印2.Name = "btn打印2";
            this.btn打印2.Size = new System.Drawing.Size(130, 77);
            this.btn打印2.TabIndex = 2;
            this.btn打印2.Text = "打印(新)";
            this.btn打印2.Click += new System.EventHandler(this.btn打印2_Click);
            // 
            // btn打印
            // 
            this.btn打印.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn打印.Appearance.Options.UseFont = true;
            this.btn打印.Location = new System.Drawing.Point(669, 20);
            this.btn打印.Name = "btn打印";
            this.btn打印.Size = new System.Drawing.Size(130, 77);
            this.btn打印.TabIndex = 2;
            this.btn打印.Text = "打印";
            this.btn打印.Click += new System.EventHandler(this.btn打印_Click);
            // 
            // textEdit姓名
            // 
            this.textEdit姓名.EditValue = "";
            this.textEdit姓名.Location = new System.Drawing.Point(50, 10);
            this.textEdit姓名.Name = "textEdit姓名";
            this.textEdit姓名.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.textEdit姓名.Properties.Appearance.Options.UseFont = true;
            this.textEdit姓名.Size = new System.Drawing.Size(76, 26);
            this.textEdit姓名.TabIndex = 1;
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl27.Location = new System.Drawing.Point(12, 13);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(32, 19);
            this.labelControl27.TabIndex = 0;
            this.labelControl27.Text = "姓名";
            // 
            // cbo体检医师
            // 
            this.cbo体检医师.EditValue = "孙鹏";
            this.cbo体检医师.Location = new System.Drawing.Point(82, 44);
            this.cbo体检医师.Name = "cbo体检医师";
            this.cbo体检医师.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cbo体检医师.Properties.Appearance.Options.UseFont = true;
            this.cbo体检医师.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo体检医师.Size = new System.Drawing.Size(161, 26);
            this.cbo体检医师.TabIndex = 6;
            // 
            // cbo体检单位
            // 
            this.cbo体检单位.EditValue = "姚店子卫生院";
            this.cbo体检单位.Location = new System.Drawing.Point(325, 44);
            this.cbo体检单位.Name = "cbo体检单位";
            this.cbo体检单位.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cbo体检单位.Properties.Appearance.Options.UseFont = true;
            this.cbo体检单位.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo体检单位.Size = new System.Drawing.Size(234, 26);
            this.cbo体检单位.TabIndex = 8;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 115);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.txt结论建议);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.listBoxControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(966, 295);
            this.splitContainerControl1.SplitterPosition = 629;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // txt结论建议
            // 
            this.txt结论建议.Appearance.Text.Font = new System.Drawing.Font("宋体", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt结论建议.Appearance.Text.Options.UseFont = true;
            this.txt结论建议.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt结论建议.Location = new System.Drawing.Point(0, 0);
            this.txt结论建议.Name = "txt结论建议";
            this.txt结论建议.Options.Fields.UseCurrentCultureDateTimeFormat = false;
            this.txt结论建议.Options.MailMerge.KeepLastParagraph = false;
            this.txt结论建议.Size = new System.Drawing.Size(629, 295);
            this.txt结论建议.TabIndex = 3;
            // 
            // listBoxControl1
            // 
            this.listBoxControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 11F);
            this.listBoxControl1.Appearance.Options.UseFont = true;
            this.listBoxControl1.Appearance.Options.UseTextOptions = true;
            this.listBoxControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.listBoxControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxControl1.Items.AddRange(new object[] {
            "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq" +
                "qqqqqqqqqqqqqqqqqqqqq"});
            this.listBoxControl1.Location = new System.Drawing.Point(0, 0);
            this.listBoxControl1.Name = "listBoxControl1";
            this.listBoxControl1.Size = new System.Drawing.Size(332, 295);
            this.listBoxControl1.TabIndex = 0;
            this.listBoxControl1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxControl1_MouseDoubleClick);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.splitContainerControl1);
            this.panel2.Controls.Add(this.panelControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 225);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(966, 410);
            this.panel2.TabIndex = 12;
            // 
            // frm详细打印表单
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(966, 635);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.layoutControl1);
            this.Name = "frm详细打印表单";
            this.Text = "frm详细打印表单";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frm详细打印表单_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl11)).EndInit();
            this.groupControl11.ResumeLayout(false);
            this.groupControl11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).EndInit();
            this.groupControl10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).EndInit();
            this.groupControl8.ResumeLayout(false);
            this.groupControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).EndInit();
            this.groupControl9.ResumeLayout(false);
            this.groupControl9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditB超医师.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            this.groupControl7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            this.groupControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            this.groupControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit村庄.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit年龄.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit症状.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit体检项目.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo体检医师.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo体检单位.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControl1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.GroupControl groupControl8;
        private Library.UserControls.UCTxtLbl ucTxtLbl心电图异常;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private Library.UserControls.UCTxtLbl ucTxtLbl心电图;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.GroupControl groupControl7;
        private Library.UserControls.UCTxtLbl ucTxtLbl尿潜血;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private Library.UserControls.UCTxtLbl ucTxtLbl尿糖;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private Library.UserControls.UCTxtLbl ucTxtLbl尿胴体;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private Library.UserControls.UCTxtLbl ucTxtLbl尿蛋白;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private Library.UserControls.UCTxtLbl ucTxtLbl结合胆红素;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private Library.UserControls.UCTxtLbl ucTxtLbl总胆红素;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private Library.UserControls.UCTxtLbl ucTxtLbl白蛋白;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private Library.UserControls.UCTxtLbl ucTxtLbl血清谷草转氨酶;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private Library.UserControls.UCTxtLbl ucTxtLbl血清谷丙转氨酶;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private Library.UserControls.UCTxtLbl ucTxtLbl血钠浓度;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private Library.UserControls.UCTxtLbl ucTxtLbl血钾浓度;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private Library.UserControls.UCTxtLbl ucTxtLbl血尿素氮;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private Library.UserControls.UCTxtLbl ucTxtLbl血清肌酐;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private Library.UserControls.UCTxtLbl ucTxtLbl餐后2H血糖;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private Library.UserControls.UCTxtLbl ucTxtLbl空腹血糖;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private Library.UserControls.UCTxtLbl ucTxtLbl血清高密度脂蛋白胆固醇;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private Library.UserControls.UCTxtLbl ucTxtLbl血清低密度脂蛋白胆固醇;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private Library.UserControls.UCTxtLbl ucTxtLbl甘油三酯;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private Library.UserControls.UCTxtLbl ucTxtLbl总胆固醇;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private Library.UserControls.UCTxtLbl ucTxtLbl右侧血压;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private Library.UserControls.UCTxtLbl ucTxtLbl左侧血压;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private Library.UserControls.UCTxtLbl ucTxtLbl血小板;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private Library.UserControls.UCTxtLbl ucTxtLbl白细胞;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private Library.UserControls.UCTxtLbl ucTxtLbl血红蛋白;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.GroupControl groupControl9;
        private Library.UserControls.UCTxtLbl ucTxtLbl体质指数;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.GroupControl groupControl10;
        private Library.UserControls.UCLblTxt uc气郁质;
        private Library.UserControls.UCLblTxt uc血瘀质;
        private Library.UserControls.UCLblTxt uc湿热质;
        private Library.UserControls.UCLblTxt uc痰湿质;
        private Library.UserControls.UCLblTxt uc阴虚质;
        private Library.UserControls.UCLblTxt uc阳虚质;
        private Library.UserControls.UCLblTxt uc气虚质;
        private Library.UserControls.UCLblTxt uc平和质;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private Library.UserControls.UCLblTxt uc特禀质;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TextEdit textEdit村庄;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.TextEdit textEdit年龄;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.TextEdit textEdit体检项目;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.TextEdit textEdit性别;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.SimpleButton btn打印;
        private DevExpress.XtraEditors.TextEdit textEdit姓名;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.ComboBoxEdit cbo体检医师;
        private DevExpress.XtraEditors.ComboBoxEdit cbo体检单位;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraRichEdit.RichEditControl txt结论建议;
        private DevExpress.XtraEditors.ListBoxControl listBoxControl1;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.GroupControl groupControl11;
        private Library.UserControls.UCTxtLbl ucTxtLbl1;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private Library.UserControls.UCTxtLbl ucTxtLblB超异常;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private Library.UserControls.UCTxtLbl ucTxtLblB超;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.SimpleButton btn打印2;
        private DevExpress.XtraEditors.TextEdit textEdit症状;
        private DevExpress.XtraEditors.TextEdit textEditB超医师;
        private DevExpress.XtraEditors.LabelControl labelControl37;


    }
}