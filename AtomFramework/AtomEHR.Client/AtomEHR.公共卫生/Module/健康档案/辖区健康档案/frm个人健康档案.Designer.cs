﻿using AtomEHR.Library.UserControls;
namespace AtomEHR.公共卫生.Module.健康档案.辖区健康档案
{
    partial class frm个人健康档案
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm个人健康档案));
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.tabSearchCondition = new DevExpress.XtraTab.XtraTabControl();
            this.tab_基础 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txt工作单位 = new DevExpress.XtraEditors.TextEdit();
            this.dte调查时间1 = new DevExpress.XtraEditors.DateEdit();
            this.dte调查时间2 = new DevExpress.XtraEditors.DateEdit();
            this.dte录入时间2 = new DevExpress.XtraEditors.DateEdit();
            this.dte录入时间1 = new DevExpress.XtraEditors.DateEdit();
            this.dte出生时间2 = new DevExpress.XtraEditors.DateEdit();
            this.dte出生时间1 = new DevExpress.XtraEditors.DateEdit();
            this.cbo排序方式 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt本人电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit地址 = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEdit村 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit镇 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.checkEdit21 = new DevExpress.XtraEditors.CheckEdit();
            this.treeListLookUpEdit机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.chk户主 = new DevExpress.XtraEditors.CheckEdit();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.txt年龄1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txt年龄2 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.dte最近更新时间1 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.dte最近更新时间2 = new DevExpress.XtraEditors.DateEdit();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.cbo血型 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cboRH = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt新农合号 = new DevExpress.XtraEditors.TextEdit();
            this.txt医疗保险号 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt档案号 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.cbo档案类别 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo档案状态 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo合格档案 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo缺项调查 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo职业 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo婚姻状况 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo民族 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo常住类型 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo性别 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo文化程度 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo最近修改人 = new DevExpress.XtraEditors.LookUpEdit();
            this.cbo录入人 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt联系人电话 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tab_重点 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk普通人群 = new DevExpress.XtraEditors.CheckEdit();
            this.chk重点人群 = new DevExpress.XtraEditors.CheckEdit();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk是否流动人口 = new DevExpress.XtraEditors.CheckEdit();
            this.chk是否签约服务 = new DevExpress.XtraEditors.CheckEdit();
            this.chk是否贫困人口 = new DevExpress.XtraEditors.CheckEdit();
            this.chk外出人员 = new DevExpress.XtraEditors.CheckEdit();
            this.chk流入人口 = new DevExpress.XtraEditors.CheckEdit();
            this.chk已复核 = new DevExpress.XtraEditors.CheckEdit();
            this.chk未复核 = new DevExpress.XtraEditors.CheckEdit();
            this.chk二次复核 = new DevExpress.XtraEditors.CheckEdit();
            this.chk未二次复核 = new DevExpress.XtraEditors.CheckEdit();
            this.chk参保人数 = new DevExpress.XtraEditors.CheckEdit();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk听力残疾 = new DevExpress.XtraEditors.CheckEdit();
            this.chk言语残疾 = new DevExpress.XtraEditors.CheckEdit();
            this.chk肢体残疾 = new DevExpress.XtraEditors.CheckEdit();
            this.chk智力残疾 = new DevExpress.XtraEditors.CheckEdit();
            this.chk视力残疾 = new DevExpress.XtraEditors.CheckEdit();
            this.chk残疾All = new DevExpress.XtraEditors.CheckEdit();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk高血压 = new DevExpress.XtraEditors.CheckEdit();
            this.chk2型糖尿病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk冠心病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk脑卒中 = new DevExpress.XtraEditors.CheckEdit();
            this.chk肿瘤 = new DevExpress.XtraEditors.CheckEdit();
            this.chk慢阻肺 = new DevExpress.XtraEditors.CheckEdit();
            this.chk重症精神病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk高血压高危人群 = new DevExpress.XtraEditors.CheckEdit();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio重点人群 = new DevExpress.XtraEditors.RadioGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tab_高危 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk临界高血压 = new DevExpress.XtraEditors.CheckEdit();
            this.chk血脂边缘升高 = new DevExpress.XtraEditors.CheckEdit();
            this.chk空腹血糖升高 = new DevExpress.XtraEditors.CheckEdit();
            this.chk糖耐量异常 = new DevExpress.XtraEditors.CheckEdit();
            this.chk肥胖 = new DevExpress.XtraEditors.CheckEdit();
            this.chk重度吸烟 = new DevExpress.XtraEditors.CheckEdit();
            this.chk超重且中心型肥胖 = new DevExpress.XtraEditors.CheckEdit();
            this.chk高危因素 = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.btn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.btn导出 = new DevExpress.XtraEditors.SimpleButton();
            this.btn打印条码 = new DevExpress.XtraEditors.SimpleButton();
            this.btn打印条码New = new DevExpress.XtraEditors.SimpleButton();
            this.btn修改记录 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btn综合打印 = new DevExpress.XtraEditors.SimpleButton();
            this.btnEmpty = new DevExpress.XtraEditors.SimpleButton();
            this.gc个人健康档案 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv个人健康档案 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col个人档案号码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.col姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col性别 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col出生日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col身份证号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col居住地址 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col本人电话 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col联系人电话 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col所属机构 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col创建人 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col创建时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col档案状态 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col工作单位 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col残疾人 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col肺结核 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col高血压 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col冠心病 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col精神病 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col老年人 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col脑卒中 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col贫困 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col糖尿病 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col儿童 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col孕产妇 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcDetailEditor = new DevExpress.XtraEditors.GroupControl();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.txt编码 = new DevExpress.XtraEditors.TextEdit();
            this.pagerControl1 = new TActionProject.PagerControl();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            this.tpDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabSearchCondition)).BeginInit();
            this.tabSearchCondition.SuspendLayout();
            this.tab_基础.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt工作单位.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte调查时间1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte调查时间1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte调查时间2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte调查时间2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo排序方式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt本人电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk户主.Properties)).BeginInit();
            this.flowLayoutPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt年龄1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt年龄2.Properties)).BeginInit();
            this.flowLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dte最近更新时间1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte最近更新时间1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte最近更新时间2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte最近更新时间2.Properties)).BeginInit();
            this.flowLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo血型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboRH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt新农合号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医疗保险号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案类别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo合格档案.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo缺项调查.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo职业.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo婚姻状况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo民族.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo常住类型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo文化程度.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo最近修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo录入人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系人电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            this.tab_重点.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk普通人群.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk重点人群.Properties)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk是否流动人口.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk是否签约服务.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk是否贫困人口.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk外出人员.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk流入人口.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk已复核.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk未复核.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk二次复核.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk未二次复核.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk参保人数.Properties)).BeginInit();
            this.flowLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk听力残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk言语残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肢体残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk智力残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk视力残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk残疾All.Properties)).BeginInit();
            this.flowLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk高血压.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2型糖尿病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk冠心病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑卒中.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肿瘤.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk慢阻肺.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk重症精神病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk高血压高危人群.Properties)).BeginInit();
            this.flowLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio重点人群.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            this.tab_高危.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            this.flowLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk临界高血压.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk血脂边缘升高.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk空腹血糖升高.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk糖耐量异常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肥胖.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk重度吸烟.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk超重且中心型肥胖.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk高危因素.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc个人健康档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv个人健康档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetailEditor)).BeginInit();
            this.gcDetailEditor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt编码.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gc个人健康档案);
            this.tpSummary.Controls.Add(this.pagerControl1);
            this.tpSummary.Controls.Add(this.panelControl4);
            this.tpSummary.Controls.Add(this.groupControl1);
            this.tpSummary.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tpSummary.Size = new System.Drawing.Size(1107, 529);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.pnlSummary.Size = new System.Drawing.Size(1113, 535);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tcBusiness.Size = new System.Drawing.Size(1113, 535);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Controls.Add(this.gcDetailEditor);
            this.tpDetail.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tpDetail.Size = new System.Drawing.Size(932, 589);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.gcNavigator.Padding = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.gcNavigator.Size = new System.Drawing.Size(1113, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(910, 4);
            this.controlNavigatorSummary.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.controlNavigatorSummary.Size = new System.Drawing.Size(201, 19);
            // 
            // txtFocusForSave
            // 
            this.txtFocusForSave.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(685, 4);
            this.lblAboutInfo.Size = new System.Drawing.Size(225, 18);
            // 
            // panelControl5
            // 
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(0, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1113, 561);
            this.panelControl5.TabIndex = 3;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.tabSearchCondition);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.ShowCaption = false;
            this.groupControl1.Size = new System.Drawing.Size(1107, 168);
            this.groupControl1.TabIndex = 2;
            this.groupControl1.Text = "信息查询";
            // 
            // tabSearchCondition
            // 
            this.tabSearchCondition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabSearchCondition.Location = new System.Drawing.Point(2, 2);
            this.tabSearchCondition.Name = "tabSearchCondition";
            this.tabSearchCondition.SelectedTabPage = this.tab_基础;
            this.tabSearchCondition.ShowTabHeader = DevExpress.Utils.DefaultBoolean.True;
            this.tabSearchCondition.Size = new System.Drawing.Size(1103, 164);
            this.tabSearchCondition.TabIndex = 0;
            this.tabSearchCondition.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tab_基础,
            this.tab_重点,
            this.tab_高危});
            this.tabSearchCondition.TabIndexChanged += new System.EventHandler(this.tabSearchCondition_TabIndexChanged);
            // 
            // tab_基础
            // 
            this.tab_基础.Controls.Add(this.panelControl1);
            this.tab_基础.Name = "tab_基础";
            this.tab_基础.Size = new System.Drawing.Size(1097, 135);
            this.tab_基础.Text = "基础查询";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.layoutControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1097, 135);
            this.panelControl1.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txt工作单位);
            this.layoutControl1.Controls.Add(this.dte调查时间1);
            this.layoutControl1.Controls.Add(this.dte调查时间2);
            this.layoutControl1.Controls.Add(this.dte录入时间2);
            this.layoutControl1.Controls.Add(this.dte录入时间1);
            this.layoutControl1.Controls.Add(this.dte出生时间2);
            this.layoutControl1.Controls.Add(this.dte出生时间1);
            this.layoutControl1.Controls.Add(this.cbo排序方式);
            this.layoutControl1.Controls.Add(this.txt本人电话);
            this.layoutControl1.Controls.Add(this.textEdit地址);
            this.layoutControl1.Controls.Add(this.comboBoxEdit村);
            this.layoutControl1.Controls.Add(this.comboBoxEdit镇);
            this.layoutControl1.Controls.Add(this.checkEdit21);
            this.layoutControl1.Controls.Add(this.treeListLookUpEdit机构);
            this.layoutControl1.Controls.Add(this.chk户主);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel11);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel10);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel9);
            this.layoutControl1.Controls.Add(this.txt新农合号);
            this.layoutControl1.Controls.Add(this.txt医疗保险号);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt档案号);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.cbo档案类别);
            this.layoutControl1.Controls.Add(this.cbo档案状态);
            this.layoutControl1.Controls.Add(this.cbo合格档案);
            this.layoutControl1.Controls.Add(this.cbo缺项调查);
            this.layoutControl1.Controls.Add(this.cbo职业);
            this.layoutControl1.Controls.Add(this.cbo婚姻状况);
            this.layoutControl1.Controls.Add(this.cbo民族);
            this.layoutControl1.Controls.Add(this.cbo常住类型);
            this.layoutControl1.Controls.Add(this.cbo性别);
            this.layoutControl1.Controls.Add(this.cbo文化程度);
            this.layoutControl1.Controls.Add(this.cbo最近修改人);
            this.layoutControl1.Controls.Add(this.cbo录入人);
            this.layoutControl1.Controls.Add(this.txt联系人电话);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1093, 131);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txt工作单位
            // 
            this.txt工作单位.Location = new System.Drawing.Point(876, 76);
            this.txt工作单位.Name = "txt工作单位";
            this.txt工作单位.Size = new System.Drawing.Size(213, 20);
            this.txt工作单位.StyleController = this.layoutControl1;
            this.txt工作单位.TabIndex = 41;
            // 
            // dte调查时间1
            // 
            this.dte调查时间1.EditValue = null;
            this.dte调查时间1.Location = new System.Drawing.Point(69, 76);
            this.dte调查时间1.Margin = new System.Windows.Forms.Padding(0);
            this.dte调查时间1.Name = "dte调查时间1";
            this.dte调查时间1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte调查时间1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte调查时间1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte调查时间1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte调查时间1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte调查时间1.Size = new System.Drawing.Size(133, 20);
            this.dte调查时间1.StyleController = this.layoutControl1;
            this.dte调查时间1.TabIndex = 6;
            // 
            // dte调查时间2
            // 
            this.dte调查时间2.EditValue = null;
            this.dte调查时间2.Location = new System.Drawing.Point(221, 76);
            this.dte调查时间2.Margin = new System.Windows.Forms.Padding(0);
            this.dte调查时间2.Name = "dte调查时间2";
            this.dte调查时间2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte调查时间2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte调查时间2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte调查时间2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte调查时间2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte调查时间2.Size = new System.Drawing.Size(135, 20);
            this.dte调查时间2.StyleController = this.layoutControl1;
            this.dte调查时间2.TabIndex = 8;
            // 
            // dte录入时间2
            // 
            this.dte录入时间2.EditValue = null;
            this.dte录入时间2.Location = new System.Drawing.Point(221, 52);
            this.dte录入时间2.Margin = new System.Windows.Forms.Padding(0);
            this.dte录入时间2.Name = "dte录入时间2";
            this.dte录入时间2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte录入时间2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte录入时间2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte录入时间2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte录入时间2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte录入时间2.Size = new System.Drawing.Size(135, 20);
            this.dte录入时间2.StyleController = this.layoutControl1;
            this.dte录入时间2.TabIndex = 5;
            // 
            // dte录入时间1
            // 
            this.dte录入时间1.EditValue = null;
            this.dte录入时间1.Location = new System.Drawing.Point(69, 52);
            this.dte录入时间1.Margin = new System.Windows.Forms.Padding(0);
            this.dte录入时间1.Name = "dte录入时间1";
            this.dte录入时间1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte录入时间1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte录入时间1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte录入时间1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte录入时间1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte录入时间1.Size = new System.Drawing.Size(133, 20);
            this.dte录入时间1.StyleController = this.layoutControl1;
            this.dte录入时间1.TabIndex = 3;
            // 
            // dte出生时间2
            // 
            this.dte出生时间2.EditValue = null;
            this.dte出生时间2.Location = new System.Drawing.Point(221, 28);
            this.dte出生时间2.Margin = new System.Windows.Forms.Padding(0);
            this.dte出生时间2.Name = "dte出生时间2";
            this.dte出生时间2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出生时间2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出生时间2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte出生时间2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte出生时间2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte出生时间2.Size = new System.Drawing.Size(135, 20);
            this.dte出生时间2.StyleController = this.layoutControl1;
            this.dte出生时间2.TabIndex = 2;
            // 
            // dte出生时间1
            // 
            this.dte出生时间1.EditValue = null;
            this.dte出生时间1.Location = new System.Drawing.Point(69, 28);
            this.dte出生时间1.Margin = new System.Windows.Forms.Padding(0);
            this.dte出生时间1.Name = "dte出生时间1";
            this.dte出生时间1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出生时间1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出生时间1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte出生时间1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte出生时间1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte出生时间1.Size = new System.Drawing.Size(133, 20);
            this.dte出生时间1.StyleController = this.layoutControl1;
            this.dte出生时间1.TabIndex = 0;
            // 
            // cbo排序方式
            // 
            this.cbo排序方式.EditValue = "查询排序方式";
            this.cbo排序方式.Location = new System.Drawing.Point(661, 76);
            this.cbo排序方式.Name = "cbo排序方式";
            this.cbo排序方式.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo排序方式.Properties.Items.AddRange(new object[] {
            "查询排序方式",
            "档案创建时间",
            "个人档案编号",
            "家庭档案编号"});
            this.cbo排序方式.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo排序方式.Size = new System.Drawing.Size(146, 20);
            this.cbo排序方式.StyleController = this.layoutControl1;
            this.cbo排序方式.TabIndex = 40;
            // 
            // txt本人电话
            // 
            this.txt本人电话.Location = new System.Drawing.Point(875, 28);
            this.txt本人电话.Name = "txt本人电话";
            this.txt本人电话.Size = new System.Drawing.Size(214, 20);
            this.txt本人电话.StyleController = this.layoutControl1;
            this.txt本人电话.TabIndex = 39;
            // 
            // textEdit地址
            // 
            this.textEdit地址.Location = new System.Drawing.Point(731, 52);
            this.textEdit地址.Name = "textEdit地址";
            this.textEdit地址.Size = new System.Drawing.Size(142, 20);
            this.textEdit地址.StyleController = this.layoutControl1;
            this.textEdit地址.TabIndex = 38;
            // 
            // comboBoxEdit村
            // 
            this.comboBoxEdit村.Location = new System.Drawing.Point(571, 52);
            this.comboBoxEdit村.Name = "comboBoxEdit村";
            this.comboBoxEdit村.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit村.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit村.Size = new System.Drawing.Size(156, 20);
            this.comboBoxEdit村.StyleController = this.layoutControl1;
            this.comboBoxEdit村.TabIndex = 37;
            // 
            // comboBoxEdit镇
            // 
            this.comboBoxEdit镇.Location = new System.Drawing.Point(425, 52);
            this.comboBoxEdit镇.Name = "comboBoxEdit镇";
            this.comboBoxEdit镇.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit镇.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit镇.Size = new System.Drawing.Size(142, 20);
            this.comboBoxEdit镇.StyleController = this.layoutControl1;
            this.comboBoxEdit镇.TabIndex = 36;
            this.comboBoxEdit镇.EditValueChanged += new System.EventHandler(this.comboBoxEdit镇_EditValueChanged);
            // 
            // checkEdit21
            // 
            this.checkEdit21.EditValue = true;
            this.checkEdit21.Location = new System.Drawing.Point(225, 4);
            this.checkEdit21.Name = "checkEdit21";
            this.checkEdit21.Properties.Caption = "含下属机构";
            this.checkEdit21.Size = new System.Drawing.Size(131, 19);
            this.checkEdit21.StyleController = this.layoutControl1;
            this.checkEdit21.TabIndex = 35;
            // 
            // treeListLookUpEdit机构
            // 
            this.treeListLookUpEdit机构.Location = new System.Drawing.Point(69, 4);
            this.treeListLookUpEdit机构.Name = "treeListLookUpEdit机构";
            this.treeListLookUpEdit机构.Properties.AutoExpandAllNodes = false;
            this.treeListLookUpEdit机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEdit机构.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.treeListLookUpEdit机构.Size = new System.Drawing.Size(152, 20);
            this.treeListLookUpEdit机构.StyleController = this.layoutControl1;
            this.treeListLookUpEdit机构.TabIndex = 34;
            this.treeListLookUpEdit机构.EditValueChanged += new System.EventHandler(this.treeListLookUpEdit机构_EditValueChanged);
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(0, 0);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // chk户主
            // 
            this.chk户主.Location = new System.Drawing.Point(594, 172);
            this.chk户主.Name = "chk户主";
            this.chk户主.Properties.Caption = "户主";
            this.chk户主.Size = new System.Drawing.Size(472, 19);
            this.chk户主.StyleController = this.layoutControl1;
            this.chk户主.TabIndex = 30;
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.Controls.Add(this.txt年龄1);
            this.flowLayoutPanel11.Controls.Add(this.labelControl6);
            this.flowLayoutPanel11.Controls.Add(this.txt年龄2);
            this.flowLayoutPanel11.Location = new System.Drawing.Point(659, 148);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(407, 20);
            this.flowLayoutPanel11.TabIndex = 27;
            // 
            // txt年龄1
            // 
            this.txt年龄1.Location = new System.Drawing.Point(0, 0);
            this.txt年龄1.Margin = new System.Windows.Forms.Padding(0);
            this.txt年龄1.Name = "txt年龄1";
            this.txt年龄1.Properties.Mask.EditMask = "n0";
            this.txt年龄1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt年龄1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt年龄1.Size = new System.Drawing.Size(100, 20);
            this.txt年龄1.TabIndex = 6;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(100, 0);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(9, 14);
            this.labelControl6.TabIndex = 7;
            this.labelControl6.Text = "~";
            // 
            // txt年龄2
            // 
            this.txt年龄2.Location = new System.Drawing.Point(109, 0);
            this.txt年龄2.Margin = new System.Windows.Forms.Padding(0);
            this.txt年龄2.Name = "txt年龄2";
            this.txt年龄2.Properties.Mask.EditMask = "n0";
            this.txt年龄2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt年龄2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt年龄2.Size = new System.Drawing.Size(100, 20);
            this.txt年龄2.TabIndex = 8;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.Controls.Add(this.dte最近更新时间1);
            this.flowLayoutPanel10.Controls.Add(this.labelControl5);
            this.flowLayoutPanel10.Controls.Add(this.dte最近更新时间2);
            this.flowLayoutPanel10.Location = new System.Drawing.Point(359, 148);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(231, 20);
            this.flowLayoutPanel10.TabIndex = 26;
            // 
            // dte最近更新时间1
            // 
            this.dte最近更新时间1.EditValue = null;
            this.dte最近更新时间1.Location = new System.Drawing.Point(0, 0);
            this.dte最近更新时间1.Margin = new System.Windows.Forms.Padding(0);
            this.dte最近更新时间1.Name = "dte最近更新时间1";
            this.dte最近更新时间1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte最近更新时间1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte最近更新时间1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte最近更新时间1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte最近更新时间1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte最近更新时间1.Size = new System.Drawing.Size(100, 20);
            this.dte最近更新时间1.TabIndex = 3;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(100, 0);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(9, 14);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "~";
            // 
            // dte最近更新时间2
            // 
            this.dte最近更新时间2.EditValue = null;
            this.dte最近更新时间2.Location = new System.Drawing.Point(109, 0);
            this.dte最近更新时间2.Margin = new System.Windows.Forms.Padding(0);
            this.dte最近更新时间2.Name = "dte最近更新时间2";
            this.dte最近更新时间2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte最近更新时间2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte最近更新时间2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte最近更新时间2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte最近更新时间2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte最近更新时间2.Size = new System.Drawing.Size(100, 20);
            this.dte最近更新时间2.TabIndex = 5;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.Controls.Add(this.cbo血型);
            this.flowLayoutPanel9.Controls.Add(this.labelControl4);
            this.flowLayoutPanel9.Controls.Add(this.cboRH);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(79, 148);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(181, 20);
            this.flowLayoutPanel9.TabIndex = 25;
            // 
            // cbo血型
            // 
            this.cbo血型.Location = new System.Drawing.Point(0, 0);
            this.cbo血型.Margin = new System.Windows.Forms.Padding(0);
            this.cbo血型.Name = "cbo血型";
            this.cbo血型.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo血型.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo血型.Size = new System.Drawing.Size(73, 20);
            this.cbo血型.TabIndex = 0;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(73, 0);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(15, 14);
            this.labelControl4.TabIndex = 1;
            this.labelControl4.Text = "RH";
            // 
            // cboRH
            // 
            this.cboRH.Location = new System.Drawing.Point(88, 0);
            this.cboRH.Margin = new System.Windows.Forms.Padding(0);
            this.cboRH.Name = "cboRH";
            this.cboRH.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboRH.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cboRH.Size = new System.Drawing.Size(68, 20);
            this.cboRH.TabIndex = 2;
            // 
            // txt新农合号
            // 
            this.txt新农合号.Location = new System.Drawing.Point(579, 124);
            this.txt新农合号.Name = "txt新农合号";
            this.txt新农合号.Size = new System.Drawing.Size(487, 20);
            this.txt新农合号.StyleController = this.layoutControl1;
            this.txt新农合号.TabIndex = 24;
            // 
            // txt医疗保险号
            // 
            this.txt医疗保险号.Location = new System.Drawing.Point(359, 124);
            this.txt医疗保险号.Name = "txt医疗保险号";
            this.txt医疗保险号.Size = new System.Drawing.Size(151, 20);
            this.txt医疗保险号.StyleController = this.layoutControl1;
            this.txt医疗保险号.TabIndex = 23;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(912, 4);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Size = new System.Drawing.Size(177, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 8;
            // 
            // txt档案号
            // 
            this.txt档案号.Location = new System.Drawing.Point(725, 4);
            this.txt档案号.Name = "txt档案号";
            this.txt档案号.Size = new System.Drawing.Size(118, 20);
            this.txt档案号.StyleController = this.layoutControl1;
            this.txt档案号.TabIndex = 7;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(425, 4);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(81, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 5;
            // 
            // cbo档案类别
            // 
            this.cbo档案类别.Location = new System.Drawing.Point(425, 28);
            this.cbo档案类别.Name = "cbo档案类别";
            this.cbo档案类别.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo档案类别.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo档案类别.Size = new System.Drawing.Size(81, 20);
            this.cbo档案类别.StyleController = this.layoutControl1;
            this.cbo档案类别.TabIndex = 10;
            // 
            // cbo档案状态
            // 
            this.cbo档案状态.Location = new System.Drawing.Point(575, 28);
            this.cbo档案状态.Name = "cbo档案状态";
            this.cbo档案状态.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo档案状态.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo档案状态.Size = new System.Drawing.Size(81, 20);
            this.cbo档案状态.StyleController = this.layoutControl1;
            this.cbo档案状态.TabIndex = 11;
            // 
            // cbo合格档案
            // 
            this.cbo合格档案.Location = new System.Drawing.Point(725, 28);
            this.cbo合格档案.Name = "cbo合格档案";
            this.cbo合格档案.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo合格档案.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo合格档案.Size = new System.Drawing.Size(81, 20);
            this.cbo合格档案.StyleController = this.layoutControl1;
            this.cbo合格档案.TabIndex = 12;
            // 
            // cbo缺项调查
            // 
            this.cbo缺项调查.EditValue = "请选择缺项";
            this.cbo缺项调查.Location = new System.Drawing.Point(425, 76);
            this.cbo缺项调查.Name = "cbo缺项调查";
            this.cbo缺项调查.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo缺项调查.Properties.Items.AddRange(new object[] {
            "请选择缺项",
            "身份证号",
            "本人电话",
            "血型",
            "居住情况"});
            this.cbo缺项调查.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo缺项调查.Size = new System.Drawing.Size(232, 20);
            this.cbo缺项调查.StyleController = this.layoutControl1;
            this.cbo缺项调查.TabIndex = 16;
            // 
            // cbo职业
            // 
            this.cbo职业.Location = new System.Drawing.Point(79, 100);
            this.cbo职业.Name = "cbo职业";
            this.cbo职业.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo职业.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo职业.Size = new System.Drawing.Size(181, 20);
            this.cbo职业.StyleController = this.layoutControl1;
            this.cbo职业.TabIndex = 18;
            // 
            // cbo婚姻状况
            // 
            this.cbo婚姻状况.Location = new System.Drawing.Point(729, 100);
            this.cbo婚姻状况.Name = "cbo婚姻状况";
            this.cbo婚姻状况.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo婚姻状况.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo婚姻状况.Size = new System.Drawing.Size(337, 20);
            this.cbo婚姻状况.StyleController = this.layoutControl1;
            this.cbo婚姻状况.TabIndex = 21;
            // 
            // cbo民族
            // 
            this.cbo民族.Location = new System.Drawing.Point(579, 100);
            this.cbo民族.Name = "cbo民族";
            this.cbo民族.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo民族.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo民族.Size = new System.Drawing.Size(81, 20);
            this.cbo民族.StyleController = this.layoutControl1;
            this.cbo民族.TabIndex = 20;
            // 
            // cbo常住类型
            // 
            this.cbo常住类型.Location = new System.Drawing.Point(359, 100);
            this.cbo常住类型.Name = "cbo常住类型";
            this.cbo常住类型.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo常住类型.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo常住类型.Size = new System.Drawing.Size(151, 20);
            this.cbo常住类型.StyleController = this.layoutControl1;
            this.cbo常住类型.TabIndex = 19;
            // 
            // cbo性别
            // 
            this.cbo性别.Location = new System.Drawing.Point(575, 4);
            this.cbo性别.Name = "cbo性别";
            this.cbo性别.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo性别.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo性别.Size = new System.Drawing.Size(81, 20);
            this.cbo性别.StyleController = this.layoutControl1;
            this.cbo性别.TabIndex = 6;
            // 
            // cbo文化程度
            // 
            this.cbo文化程度.Location = new System.Drawing.Point(79, 124);
            this.cbo文化程度.Name = "cbo文化程度";
            this.cbo文化程度.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo文化程度.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo文化程度.Size = new System.Drawing.Size(181, 20);
            this.cbo文化程度.StyleController = this.layoutControl1;
            this.cbo文化程度.TabIndex = 22;
            // 
            // cbo最近修改人
            // 
            this.cbo最近修改人.Location = new System.Drawing.Point(79, 172);
            this.cbo最近修改人.Name = "cbo最近修改人";
            this.cbo最近修改人.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo最近修改人.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("UserName", "用户名")});
            this.cbo最近修改人.Properties.NullText = "";
            this.cbo最近修改人.Properties.PopupSizeable = false;
            this.cbo最近修改人.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbo最近修改人.Size = new System.Drawing.Size(181, 20);
            this.cbo最近修改人.StyleController = this.layoutControl1;
            this.cbo最近修改人.TabIndex = 28;
            // 
            // cbo录入人
            // 
            this.cbo录入人.Location = new System.Drawing.Point(359, 172);
            this.cbo录入人.Name = "cbo录入人";
            this.cbo录入人.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo录入人.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("UserName", "用户名")});
            this.cbo录入人.Properties.NullText = "";
            this.cbo录入人.Properties.PopupSizeable = false;
            this.cbo录入人.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbo录入人.Size = new System.Drawing.Size(231, 20);
            this.cbo录入人.StyleController = this.layoutControl1;
            this.cbo录入人.TabIndex = 29;
            // 
            // txt联系人电话
            // 
            this.txt联系人电话.EditValue = "";
            this.txt联系人电话.Location = new System.Drawing.Point(952, 52);
            this.txt联系人电话.Name = "txt联系人电话";
            this.txt联系人电话.Size = new System.Drawing.Size(137, 20);
            this.txt联系人电话.StyleController = this.layoutControl1;
            this.txt联系人电话.TabIndex = 33;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem13,
            this.layoutControlGroup4,
            this.layoutControlItem9,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem18,
            this.layoutControlItem32,
            this.layoutControlItem1,
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.layoutControlItem35,
            this.layoutControlItem5,
            this.layoutControlItem11,
            this.layoutControlItem36,
            this.layoutControlItem37,
            this.layoutControlItem38,
            this.layoutControlItem6,
            this.layoutControlItem39,
            this.layoutControlItem10,
            this.layoutControlItem40,
            this.layoutControlItem41});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1093, 131);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem7.Control = this.cbo档案类别;
            this.layoutControlItem7.CustomizationFormText = "档案类别：";
            this.layoutControlItem7.Location = new System.Drawing.Point(356, 24);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "档案类别：";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem8.Control = this.cbo档案状态;
            this.layoutControlItem8.CustomizationFormText = "档案状态：";
            this.layoutControlItem8.Location = new System.Drawing.Point(506, 24);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "档案状态：";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem13.Control = this.cbo缺项调查;
            this.layoutControlItem13.CustomizationFormText = "缺项调查：";
            this.layoutControlItem13.Location = new System.Drawing.Point(356, 72);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(301, 24);
            this.layoutControlItem13.Text = "缺项调查：";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "扩展查询";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.Expanded = false;
            this.layoutControlGroup4.ExpandOnDoubleClick = true;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem31,
            this.layoutControlItem30,
            this.layoutControlItem29,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.layoutControlItem24,
            this.layoutControlItem28,
            this.layoutControlItem25,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem20,
            this.layoutControlItem23,
            this.layoutControlItem19});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 96);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Size = new System.Drawing.Size(1089, 31);
            this.layoutControlGroup4.Text = "扩展查询";
            this.layoutControlGroup4.DoubleClick += new System.EventHandler(this.layoutControlGroup4_Click);
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.chk户主;
            this.layoutControlItem31.CustomizationFormText = "layoutControlItem31";
            this.layoutControlItem31.Location = new System.Drawing.Point(590, 72);
            this.layoutControlItem31.MaxSize = new System.Drawing.Size(100, 23);
            this.layoutControlItem31.MinSize = new System.Drawing.Size(100, 23);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(476, 25);
            this.layoutControlItem31.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem31.Text = "layoutControlItem31";
            this.layoutControlItem31.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem31.TextToControlDistance = 0;
            this.layoutControlItem31.TextVisible = false;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.Control = this.cbo录入人;
            this.layoutControlItem30.CustomizationFormText = "录入人：";
            this.layoutControlItem30.Location = new System.Drawing.Point(260, 72);
            this.layoutControlItem30.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(330, 25);
            this.layoutControlItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem30.Text = "录入人：";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.Control = this.cbo最近修改人;
            this.layoutControlItem29.CustomizationFormText = "最近修改人：";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(260, 25);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Text = "最近修改人：";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.Control = this.flowLayoutPanel9;
            this.layoutControlItem26.CustomizationFormText = "血型：";
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem26.MaxSize = new System.Drawing.Size(260, 0);
            this.layoutControlItem26.MinSize = new System.Drawing.Size(260, 24);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(260, 24);
            this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem26.Text = "血型：";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.Control = this.flowLayoutPanel10;
            this.layoutControlItem27.CustomizationFormText = "最近更新时间：";
            this.layoutControlItem27.Location = new System.Drawing.Point(260, 48);
            this.layoutControlItem27.MaxSize = new System.Drawing.Size(330, 0);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(330, 24);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Text = "最近更新时间：";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem24.Control = this.txt医疗保险号;
            this.layoutControlItem24.CustomizationFormText = "医疗保险号：";
            this.layoutControlItem24.Location = new System.Drawing.Point(260, 24);
            this.layoutControlItem24.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(250, 24);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.Text = "医疗保险号：";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.Control = this.flowLayoutPanel11;
            this.layoutControlItem28.CustomizationFormText = "年龄区间：";
            this.layoutControlItem28.Location = new System.Drawing.Point(590, 48);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(169, 24);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(476, 24);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Text = "年龄区间：";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.Control = this.txt新农合号;
            this.layoutControlItem25.CustomizationFormText = "新农合号：";
            this.layoutControlItem25.Location = new System.Drawing.Point(510, 24);
            this.layoutControlItem25.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(556, 24);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Text = "新农合号：";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem21.Control = this.cbo民族;
            this.layoutControlItem21.CustomizationFormText = "民族：";
            this.layoutControlItem21.Location = new System.Drawing.Point(510, 0);
            this.layoutControlItem21.MaxSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "民族：";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem21.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.cbo婚姻状况;
            this.layoutControlItem22.CustomizationFormText = "婚姻状况：";
            this.layoutControlItem22.Location = new System.Drawing.Point(660, 0);
            this.layoutControlItem22.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(406, 24);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "婚姻状况：";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.Control = this.cbo常住类型;
            this.layoutControlItem20.CustomizationFormText = "常驻类型：";
            this.layoutControlItem20.Location = new System.Drawing.Point(260, 0);
            this.layoutControlItem20.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(250, 24);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "常住类型：";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.Control = this.cbo文化程度;
            this.layoutControlItem23.CustomizationFormText = "文化程度：";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem23.MaxSize = new System.Drawing.Size(260, 24);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(260, 24);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(260, 24);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "文化程度：";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem19.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem19.Control = this.cbo职业;
            this.layoutControlItem19.CustomizationFormText = "职业：";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem19.MaxSize = new System.Drawing.Size(260, 24);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(260, 24);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(260, 24);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "职业：";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem9.Control = this.cbo合格档案;
            this.layoutControlItem9.CustomizationFormText = "合格档案：";
            this.layoutControlItem9.Location = new System.Drawing.Point(656, 24);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "合格档案：";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem2.Control = this.txt姓名;
            this.layoutControlItem2.CustomizationFormText = "姓名：";
            this.layoutControlItem2.Location = new System.Drawing.Point(356, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "姓名：";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem3.Control = this.cbo性别;
            this.layoutControlItem3.CustomizationFormText = "性别：";
            this.layoutControlItem3.Location = new System.Drawing.Point(506, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "性别：";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem4.Control = this.txt档案号;
            this.layoutControlItem4.CustomizationFormText = "档案号：";
            this.layoutControlItem4.Location = new System.Drawing.Point(656, 0);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(119, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(187, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "档案号：";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.txt联系人电话;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(873, 48);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(216, 24);
            this.layoutControlItem18.Text = "联系人电话：";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.treeListLookUpEdit机构;
            this.layoutControlItem32.CustomizationFormText = "所属机构：";
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem32.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem32.MinSize = new System.Drawing.Size(117, 24);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(221, 24);
            this.layoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem32.Text = "所属机构：";
            this.layoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem32.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.checkEdit21;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(221, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(135, 23);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(135, 23);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(135, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.comboBoxEdit镇;
            this.layoutControlItem33.CustomizationFormText = "居住地址：";
            this.layoutControlItem33.Location = new System.Drawing.Point(356, 48);
            this.layoutControlItem33.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem33.MinSize = new System.Drawing.Size(117, 24);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(211, 24);
            this.layoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem33.Text = "居住地址：";
            this.layoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem33.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem33.TextToControlDistance = 5;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.comboBoxEdit村;
            this.layoutControlItem34.CustomizationFormText = "layoutControlItem34";
            this.layoutControlItem34.Location = new System.Drawing.Point(567, 48);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(160, 24);
            this.layoutControlItem34.Text = "layoutControlItem34";
            this.layoutControlItem34.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem34.TextToControlDistance = 0;
            this.layoutControlItem34.TextVisible = false;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.textEdit地址;
            this.layoutControlItem35.CustomizationFormText = "layoutControlItem35";
            this.layoutControlItem35.Location = new System.Drawing.Point(727, 48);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(146, 24);
            this.layoutControlItem35.Text = "layoutControlItem35";
            this.layoutControlItem35.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem35.TextToControlDistance = 0;
            this.layoutControlItem35.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem5.Control = this.txt身份证号;
            this.layoutControlItem5.CustomizationFormText = "身份证号：";
            this.layoutControlItem5.Location = new System.Drawing.Point(843, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(119, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(246, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "身份证号：";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.txt本人电话;
            this.layoutControlItem11.CustomizationFormText = "本人电话：";
            this.layoutControlItem11.Location = new System.Drawing.Point(806, 24);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(119, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(283, 24);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "本人电话：";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.cbo排序方式;
            this.layoutControlItem36.CustomizationFormText = "layoutControlItem36";
            this.layoutControlItem36.Location = new System.Drawing.Point(657, 72);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem36.Text = "layoutControlItem36";
            this.layoutControlItem36.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem36.TextToControlDistance = 0;
            this.layoutControlItem36.TextVisible = false;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.dte出生时间1;
            this.layoutControlItem37.CustomizationFormText = "出生日期：";
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(202, 24);
            this.layoutControlItem37.Text = "出生日期：";
            this.layoutControlItem37.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem37.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem37.TextToControlDistance = 5;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.dte出生时间2;
            this.layoutControlItem38.CustomizationFormText = "~";
            this.layoutControlItem38.Location = new System.Drawing.Point(202, 24);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(154, 24);
            this.layoutControlItem38.Text = "~";
            this.layoutControlItem38.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem38.TextSize = new System.Drawing.Size(10, 20);
            this.layoutControlItem38.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.dte录入时间1;
            this.layoutControlItem6.CustomizationFormText = "录入时间：";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(202, 24);
            this.layoutControlItem6.Text = "录入时间：";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.Control = this.dte录入时间2;
            this.layoutControlItem39.CustomizationFormText = "~";
            this.layoutControlItem39.Location = new System.Drawing.Point(202, 48);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(154, 24);
            this.layoutControlItem39.Text = "~";
            this.layoutControlItem39.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem39.TextSize = new System.Drawing.Size(10, 20);
            this.layoutControlItem39.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.dte调查时间1;
            this.layoutControlItem10.CustomizationFormText = "调查时间：";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(202, 24);
            this.layoutControlItem10.Text = "调查时间：";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.Control = this.dte调查时间2;
            this.layoutControlItem40.CustomizationFormText = "~";
            this.layoutControlItem40.Location = new System.Drawing.Point(202, 72);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(154, 24);
            this.layoutControlItem40.Text = "~";
            this.layoutControlItem40.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem40.TextSize = new System.Drawing.Size(10, 20);
            this.layoutControlItem40.TextToControlDistance = 5;
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.Control = this.txt工作单位;
            this.layoutControlItem41.CustomizationFormText = "工作单位：";
            this.layoutControlItem41.Location = new System.Drawing.Point(807, 72);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(282, 24);
            this.layoutControlItem41.Text = "工作单位：";
            this.layoutControlItem41.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem41.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem41.TextToControlDistance = 5;
            // 
            // tab_重点
            // 
            this.tab_重点.Controls.Add(this.panelControl2);
            this.tab_重点.Name = "tab_重点";
            this.tab_重点.Size = new System.Drawing.Size(922, 135);
            this.tab_重点.Text = "重点人群查询";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.layoutControl2);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(922, 135);
            this.panelControl2.TabIndex = 0;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.flowLayoutPanel3);
            this.layoutControl2.Controls.Add(this.flowLayoutPanel2);
            this.layoutControl2.Controls.Add(this.flowLayoutPanel7);
            this.layoutControl2.Controls.Add(this.flowLayoutPanel6);
            this.layoutControl2.Controls.Add(this.flowLayoutPanel5);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(2, 2);
            this.layoutControl2.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl2.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsView.DrawItemBorders = true;
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(918, 128);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.chk普通人群);
            this.flowLayoutPanel3.Controls.Add(this.chk重点人群);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(745, 4);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(169, 26);
            this.flowLayoutPanel3.TabIndex = 8;
            // 
            // chk普通人群
            // 
            this.chk普通人群.Location = new System.Drawing.Point(3, 3);
            this.chk普通人群.Name = "chk普通人群";
            this.chk普通人群.Properties.Caption = "普通人群";
            this.chk普通人群.Size = new System.Drawing.Size(75, 19);
            this.chk普通人群.TabIndex = 2;
            // 
            // chk重点人群
            // 
            this.chk重点人群.Location = new System.Drawing.Point(84, 3);
            this.chk重点人群.Name = "chk重点人群";
            this.chk重点人群.Properties.Caption = "重点人群";
            this.chk重点人群.Size = new System.Drawing.Size(75, 19);
            this.chk重点人群.TabIndex = 3;
            this.chk重点人群.Visible = false;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.chk是否流动人口);
            this.flowLayoutPanel2.Controls.Add(this.chk是否签约服务);
            this.flowLayoutPanel2.Controls.Add(this.chk是否贫困人口);
            this.flowLayoutPanel2.Controls.Add(this.chk外出人员);
            this.flowLayoutPanel2.Controls.Add(this.chk流入人口);
            this.flowLayoutPanel2.Controls.Add(this.chk已复核);
            this.flowLayoutPanel2.Controls.Add(this.chk未复核);
            this.flowLayoutPanel2.Controls.Add(this.chk二次复核);
            this.flowLayoutPanel2.Controls.Add(this.chk未二次复核);
            this.flowLayoutPanel2.Controls.Add(this.chk参保人数);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(59, 94);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(855, 30);
            this.flowLayoutPanel2.TabIndex = 7;
            // 
            // chk是否流动人口
            // 
            this.chk是否流动人口.Location = new System.Drawing.Point(0, 0);
            this.chk是否流动人口.Margin = new System.Windows.Forms.Padding(0);
            this.chk是否流动人口.Name = "chk是否流动人口";
            this.chk是否流动人口.Properties.Caption = "是否流动人口";
            this.chk是否流动人口.Size = new System.Drawing.Size(101, 19);
            this.chk是否流动人口.TabIndex = 8;
            // 
            // chk是否签约服务
            // 
            this.chk是否签约服务.Location = new System.Drawing.Point(101, 0);
            this.chk是否签约服务.Margin = new System.Windows.Forms.Padding(0);
            this.chk是否签约服务.Name = "chk是否签约服务";
            this.chk是否签约服务.Properties.Caption = "是否签约服务";
            this.chk是否签约服务.Size = new System.Drawing.Size(108, 19);
            this.chk是否签约服务.TabIndex = 9;
            // 
            // chk是否贫困人口
            // 
            this.chk是否贫困人口.Location = new System.Drawing.Point(209, 0);
            this.chk是否贫困人口.Margin = new System.Windows.Forms.Padding(0);
            this.chk是否贫困人口.Name = "chk是否贫困人口";
            this.chk是否贫困人口.Properties.Caption = "是否贫困人口";
            this.chk是否贫困人口.Size = new System.Drawing.Size(102, 19);
            this.chk是否贫困人口.TabIndex = 10;
            // 
            // chk外出人员
            // 
            this.chk外出人员.Location = new System.Drawing.Point(311, 0);
            this.chk外出人员.Margin = new System.Windows.Forms.Padding(0);
            this.chk外出人员.Name = "chk外出人员";
            this.chk外出人员.Properties.Caption = "外出人员";
            this.chk外出人员.Size = new System.Drawing.Size(88, 19);
            this.chk外出人员.TabIndex = 10;
            // 
            // chk流入人口
            // 
            this.chk流入人口.Location = new System.Drawing.Point(399, 0);
            this.chk流入人口.Margin = new System.Windows.Forms.Padding(0);
            this.chk流入人口.Name = "chk流入人口";
            this.chk流入人口.Properties.Caption = "流入人口";
            this.chk流入人口.Size = new System.Drawing.Size(70, 19);
            this.chk流入人口.TabIndex = 10;
            // 
            // chk已复核
            // 
            this.chk已复核.Location = new System.Drawing.Point(469, 0);
            this.chk已复核.Margin = new System.Windows.Forms.Padding(0);
            this.chk已复核.Name = "chk已复核";
            this.chk已复核.Properties.Caption = "已复核";
            this.chk已复核.Size = new System.Drawing.Size(70, 19);
            this.chk已复核.TabIndex = 10;
            // 
            // chk未复核
            // 
            this.chk未复核.Location = new System.Drawing.Point(539, 0);
            this.chk未复核.Margin = new System.Windows.Forms.Padding(0);
            this.chk未复核.Name = "chk未复核";
            this.chk未复核.Properties.Caption = "未复核";
            this.chk未复核.Size = new System.Drawing.Size(70, 19);
            this.chk未复核.TabIndex = 10;
            // 
            // chk二次复核
            // 
            this.chk二次复核.Location = new System.Drawing.Point(609, 0);
            this.chk二次复核.Margin = new System.Windows.Forms.Padding(0);
            this.chk二次复核.Name = "chk二次复核";
            this.chk二次复核.Properties.Caption = "二次复核";
            this.chk二次复核.Size = new System.Drawing.Size(70, 19);
            this.chk二次复核.TabIndex = 10;
            // 
            // chk未二次复核
            // 
            this.chk未二次复核.Location = new System.Drawing.Point(679, 0);
            this.chk未二次复核.Margin = new System.Windows.Forms.Padding(0);
            this.chk未二次复核.Name = "chk未二次复核";
            this.chk未二次复核.Properties.Caption = "未二次复核";
            this.chk未二次复核.Size = new System.Drawing.Size(94, 19);
            this.chk未二次复核.TabIndex = 11;
            // 
            // chk参保人数
            // 
            this.chk参保人数.Location = new System.Drawing.Point(0, 19);
            this.chk参保人数.Margin = new System.Windows.Forms.Padding(0);
            this.chk参保人数.Name = "chk参保人数";
            this.chk参保人数.Properties.Caption = "参保人数";
            this.chk参保人数.Size = new System.Drawing.Size(94, 19);
            this.chk参保人数.TabIndex = 12;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.Controls.Add(this.chk听力残疾);
            this.flowLayoutPanel7.Controls.Add(this.chk言语残疾);
            this.flowLayoutPanel7.Controls.Add(this.chk肢体残疾);
            this.flowLayoutPanel7.Controls.Add(this.chk智力残疾);
            this.flowLayoutPanel7.Controls.Add(this.chk视力残疾);
            this.flowLayoutPanel7.Controls.Add(this.chk残疾All);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(59, 64);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(855, 26);
            this.flowLayoutPanel7.TabIndex = 6;
            // 
            // chk听力残疾
            // 
            this.chk听力残疾.Location = new System.Drawing.Point(0, 0);
            this.chk听力残疾.Margin = new System.Windows.Forms.Padding(0);
            this.chk听力残疾.Name = "chk听力残疾";
            this.chk听力残疾.Properties.Caption = "听力残疾";
            this.chk听力残疾.Size = new System.Drawing.Size(75, 19);
            this.chk听力残疾.TabIndex = 0;
            // 
            // chk言语残疾
            // 
            this.chk言语残疾.Location = new System.Drawing.Point(75, 0);
            this.chk言语残疾.Margin = new System.Windows.Forms.Padding(0);
            this.chk言语残疾.Name = "chk言语残疾";
            this.chk言语残疾.Properties.Caption = "言语残疾";
            this.chk言语残疾.Size = new System.Drawing.Size(75, 19);
            this.chk言语残疾.TabIndex = 1;
            // 
            // chk肢体残疾
            // 
            this.chk肢体残疾.Location = new System.Drawing.Point(150, 0);
            this.chk肢体残疾.Margin = new System.Windows.Forms.Padding(0);
            this.chk肢体残疾.Name = "chk肢体残疾";
            this.chk肢体残疾.Properties.Caption = "肢体残疾";
            this.chk肢体残疾.Size = new System.Drawing.Size(75, 19);
            this.chk肢体残疾.TabIndex = 2;
            // 
            // chk智力残疾
            // 
            this.chk智力残疾.Location = new System.Drawing.Point(225, 0);
            this.chk智力残疾.Margin = new System.Windows.Forms.Padding(0);
            this.chk智力残疾.Name = "chk智力残疾";
            this.chk智力残疾.Properties.Caption = "智力残疾";
            this.chk智力残疾.Size = new System.Drawing.Size(75, 19);
            this.chk智力残疾.TabIndex = 3;
            // 
            // chk视力残疾
            // 
            this.chk视力残疾.Location = new System.Drawing.Point(300, 0);
            this.chk视力残疾.Margin = new System.Windows.Forms.Padding(0);
            this.chk视力残疾.Name = "chk视力残疾";
            this.chk视力残疾.Properties.Caption = "视力残疾";
            this.chk视力残疾.Size = new System.Drawing.Size(75, 19);
            this.chk视力残疾.TabIndex = 7;
            // 
            // chk残疾All
            // 
            this.chk残疾All.Location = new System.Drawing.Point(375, 0);
            this.chk残疾All.Margin = new System.Windows.Forms.Padding(0);
            this.chk残疾All.Name = "chk残疾All";
            this.chk残疾All.Properties.Caption = "全部";
            this.chk残疾All.Size = new System.Drawing.Size(75, 19);
            this.chk残疾All.TabIndex = 1;
            this.chk残疾All.CheckedChanged += new System.EventHandler(this.chk残疾All_CheckedChanged);
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.Controls.Add(this.chk高血压);
            this.flowLayoutPanel6.Controls.Add(this.chk2型糖尿病);
            this.flowLayoutPanel6.Controls.Add(this.chk冠心病);
            this.flowLayoutPanel6.Controls.Add(this.chk脑卒中);
            this.flowLayoutPanel6.Controls.Add(this.chk肿瘤);
            this.flowLayoutPanel6.Controls.Add(this.chk慢阻肺);
            this.flowLayoutPanel6.Controls.Add(this.chk重症精神病);
            this.flowLayoutPanel6.Controls.Add(this.chk高血压高危人群);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(59, 34);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(855, 26);
            this.flowLayoutPanel6.TabIndex = 5;
            // 
            // chk高血压
            // 
            this.chk高血压.Location = new System.Drawing.Point(0, 0);
            this.chk高血压.Margin = new System.Windows.Forms.Padding(0);
            this.chk高血压.Name = "chk高血压";
            this.chk高血压.Properties.Caption = "高血压";
            this.chk高血压.Size = new System.Drawing.Size(75, 19);
            this.chk高血压.TabIndex = 0;
            // 
            // chk2型糖尿病
            // 
            this.chk2型糖尿病.Location = new System.Drawing.Point(75, 0);
            this.chk2型糖尿病.Margin = new System.Windows.Forms.Padding(0);
            this.chk2型糖尿病.Name = "chk2型糖尿病";
            this.chk2型糖尿病.Properties.Caption = "2型糖尿病";
            this.chk2型糖尿病.Size = new System.Drawing.Size(86, 19);
            this.chk2型糖尿病.TabIndex = 1;
            // 
            // chk冠心病
            // 
            this.chk冠心病.Location = new System.Drawing.Point(161, 0);
            this.chk冠心病.Margin = new System.Windows.Forms.Padding(0);
            this.chk冠心病.Name = "chk冠心病";
            this.chk冠心病.Properties.Caption = "冠心病";
            this.chk冠心病.Size = new System.Drawing.Size(75, 19);
            this.chk冠心病.TabIndex = 2;
            // 
            // chk脑卒中
            // 
            this.chk脑卒中.Location = new System.Drawing.Point(236, 0);
            this.chk脑卒中.Margin = new System.Windows.Forms.Padding(0);
            this.chk脑卒中.Name = "chk脑卒中";
            this.chk脑卒中.Properties.Caption = "脑卒中";
            this.chk脑卒中.Size = new System.Drawing.Size(75, 19);
            this.chk脑卒中.TabIndex = 3;
            // 
            // chk肿瘤
            // 
            this.chk肿瘤.Location = new System.Drawing.Point(311, 0);
            this.chk肿瘤.Margin = new System.Windows.Forms.Padding(0);
            this.chk肿瘤.Name = "chk肿瘤";
            this.chk肿瘤.Properties.Caption = "肿瘤";
            this.chk肿瘤.Size = new System.Drawing.Size(64, 19);
            this.chk肿瘤.TabIndex = 4;
            // 
            // chk慢阻肺
            // 
            this.chk慢阻肺.Location = new System.Drawing.Point(375, 0);
            this.chk慢阻肺.Margin = new System.Windows.Forms.Padding(0);
            this.chk慢阻肺.Name = "chk慢阻肺";
            this.chk慢阻肺.Properties.Caption = "慢阻肺";
            this.chk慢阻肺.Size = new System.Drawing.Size(63, 19);
            this.chk慢阻肺.TabIndex = 5;
            // 
            // chk重症精神病
            // 
            this.chk重症精神病.Location = new System.Drawing.Point(438, 0);
            this.chk重症精神病.Margin = new System.Windows.Forms.Padding(0);
            this.chk重症精神病.Name = "chk重症精神病";
            this.chk重症精神病.Properties.Caption = "重症精神病";
            this.chk重症精神病.Size = new System.Drawing.Size(88, 19);
            this.chk重症精神病.TabIndex = 6;
            // 
            // chk高血压高危人群
            // 
            this.chk高血压高危人群.Location = new System.Drawing.Point(526, 0);
            this.chk高血压高危人群.Margin = new System.Windows.Forms.Padding(0);
            this.chk高血压高危人群.Name = "chk高血压高危人群";
            this.chk高血压高危人群.Properties.Caption = "高血压高危人群";
            this.chk高血压高危人群.Size = new System.Drawing.Size(115, 19);
            this.chk高血压高危人群.TabIndex = 7;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Controls.Add(this.radio重点人群);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(59, 4);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(619, 26);
            this.flowLayoutPanel5.TabIndex = 4;
            // 
            // radio重点人群
            // 
            this.radio重点人群.Location = new System.Drawing.Point(0, 0);
            this.radio重点人群.Margin = new System.Windows.Forms.Padding(0);
            this.radio重点人群.Name = "radio重点人群";
            this.radio重点人群.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("", "不限年龄"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "0~3岁幼童"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("6", "0~6岁幼童"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("15", "育龄妇女"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("23", "孕产妇"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("65", "65岁及以上老年人")});
            this.radio重点人群.Size = new System.Drawing.Size(718, 24);
            this.radio重点人群.TabIndex = 0;
            this.radio重点人群.SelectedIndexChanged += new System.EventHandler(this.radio重点人群_SelectedIndexChanged);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem12,
            this.layoutControlItem42});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(918, 128);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.flowLayoutPanel5;
            this.layoutControlItem14.CustomizationFormText = "普通：";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(143, 30);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(678, 30);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "普通：";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(50, 14);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.flowLayoutPanel6;
            this.layoutControlItem15.CustomizationFormText = "患者：";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(143, 30);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(914, 30);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "患者：";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(50, 14);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem16.Control = this.flowLayoutPanel7;
            this.layoutControlItem16.CustomizationFormText = "残疾：";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(143, 30);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(914, 30);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "残疾：";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(50, 14);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.flowLayoutPanel2;
            this.layoutControlItem12.CustomizationFormText = "状态:";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(914, 34);
            this.layoutControlItem12.Text = "状态：";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(50, 14);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.Control = this.flowLayoutPanel3;
            this.layoutControlItem42.CustomizationFormText = "人群类别：";
            this.layoutControlItem42.Location = new System.Drawing.Point(678, 0);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(236, 30);
            this.layoutControlItem42.Text = "人群类别：";
            this.layoutControlItem42.TextSize = new System.Drawing.Size(60, 14);
            // 
            // tab_高危
            // 
            this.tab_高危.Controls.Add(this.panelControl3);
            this.tab_高危.Name = "tab_高危";
            this.tab_高危.Size = new System.Drawing.Size(922, 135);
            this.tab_高危.Text = "高危人群查询";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.layoutControl3);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(922, 135);
            this.panelControl3.TabIndex = 0;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.flowLayoutPanel8);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(2, 2);
            this.layoutControl3.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl3.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.OptionsView.DrawItemBorders = true;
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(918, 131);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.Controls.Add(this.chk临界高血压);
            this.flowLayoutPanel8.Controls.Add(this.chk血脂边缘升高);
            this.flowLayoutPanel8.Controls.Add(this.chk空腹血糖升高);
            this.flowLayoutPanel8.Controls.Add(this.chk糖耐量异常);
            this.flowLayoutPanel8.Controls.Add(this.chk肥胖);
            this.flowLayoutPanel8.Controls.Add(this.chk重度吸烟);
            this.flowLayoutPanel8.Controls.Add(this.chk超重且中心型肥胖);
            this.flowLayoutPanel8.Controls.Add(this.chk高危因素);
            this.flowLayoutPanel8.Location = new System.Drawing.Point(51, 12);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(855, 51);
            this.flowLayoutPanel8.TabIndex = 4;
            // 
            // chk临界高血压
            // 
            this.chk临界高血压.Location = new System.Drawing.Point(3, 3);
            this.chk临界高血压.Name = "chk临界高血压";
            this.chk临界高血压.Properties.Caption = "A: 临界高血压";
            this.chk临界高血压.Size = new System.Drawing.Size(107, 19);
            this.chk临界高血压.TabIndex = 0;
            // 
            // chk血脂边缘升高
            // 
            this.chk血脂边缘升高.Location = new System.Drawing.Point(116, 3);
            this.chk血脂边缘升高.Name = "chk血脂边缘升高";
            this.chk血脂边缘升高.Properties.Caption = "B: 血脂边缘升高";
            this.chk血脂边缘升高.Size = new System.Drawing.Size(120, 19);
            this.chk血脂边缘升高.TabIndex = 1;
            // 
            // chk空腹血糖升高
            // 
            this.chk空腹血糖升高.Location = new System.Drawing.Point(242, 3);
            this.chk空腹血糖升高.Name = "chk空腹血糖升高";
            this.chk空腹血糖升高.Properties.Caption = "C: 空腹血糖升高";
            this.chk空腹血糖升高.Size = new System.Drawing.Size(124, 19);
            this.chk空腹血糖升高.TabIndex = 2;
            // 
            // chk糖耐量异常
            // 
            this.chk糖耐量异常.Location = new System.Drawing.Point(372, 3);
            this.chk糖耐量异常.Name = "chk糖耐量异常";
            this.chk糖耐量异常.Properties.Caption = "D: 糖耐量异常";
            this.chk糖耐量异常.Size = new System.Drawing.Size(109, 19);
            this.chk糖耐量异常.TabIndex = 3;
            // 
            // chk肥胖
            // 
            this.chk肥胖.Location = new System.Drawing.Point(487, 3);
            this.chk肥胖.Name = "chk肥胖";
            this.chk肥胖.Properties.Caption = "E: 肥胖";
            this.chk肥胖.Size = new System.Drawing.Size(75, 19);
            this.chk肥胖.TabIndex = 4;
            // 
            // chk重度吸烟
            // 
            this.chk重度吸烟.Location = new System.Drawing.Point(568, 3);
            this.chk重度吸烟.Name = "chk重度吸烟";
            this.chk重度吸烟.Properties.Caption = "F: 重度吸烟";
            this.chk重度吸烟.Size = new System.Drawing.Size(93, 19);
            this.chk重度吸烟.TabIndex = 5;
            // 
            // chk超重且中心型肥胖
            // 
            this.chk超重且中心型肥胖.Location = new System.Drawing.Point(667, 3);
            this.chk超重且中心型肥胖.Name = "chk超重且中心型肥胖";
            this.chk超重且中心型肥胖.Properties.Caption = "G: 超重且中心型肥胖";
            this.chk超重且中心型肥胖.Size = new System.Drawing.Size(147, 19);
            this.chk超重且中心型肥胖.TabIndex = 6;
            // 
            // chk高危因素
            // 
            this.chk高危因素.Location = new System.Drawing.Point(3, 28);
            this.chk高危因素.Name = "chk高危因素";
            this.chk高危因素.Properties.Caption = "Z: 高危人群因素(根据高血压健康管理规范中六项指标中的任一项判断)";
            this.chk高危因素.Size = new System.Drawing.Size(501, 19);
            this.chk高危因素.TabIndex = 7;
            this.chk高危因素.ToolTip = resources.GetString("chk高危因素.ToolTip");
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem17});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(918, 131);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.flowLayoutPanel8;
            this.layoutControlItem17.CustomizationFormText = "高危：";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem17.MaxSize = new System.Drawing.Size(0, 55);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(145, 55);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(898, 111);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "高危：";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(36, 14);
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.flowLayoutPanel1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(0, 168);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1107, 32);
            this.panelControl4.TabIndex = 3;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnQuery);
            this.flowLayoutPanel1.Controls.Add(this.simpleButton2);
            this.flowLayoutPanel1.Controls.Add(this.btn删除);
            this.flowLayoutPanel1.Controls.Add(this.btn导出);
            this.flowLayoutPanel1.Controls.Add(this.btn打印条码);
            this.flowLayoutPanel1.Controls.Add(this.btn打印条码New);
            this.flowLayoutPanel1.Controls.Add(this.btn修改记录);
            this.flowLayoutPanel1.Controls.Add(this.simpleButton1);
            this.flowLayoutPanel1.Controls.Add(this.btn综合打印);
            this.flowLayoutPanel1.Controls.Add(this.btnEmpty);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1103, 28);
            this.flowLayoutPanel1.TabIndex = 11;
            this.flowLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.flowLayoutPanel1_Paint);
            // 
            // btnQuery
            // 
            this.btnQuery.Image = ((System.Drawing.Image)(resources.GetObject("btnQuery.Image")));
            this.btnQuery.Location = new System.Drawing.Point(3, 0);
            this.btnQuery.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(71, 28);
            this.btnQuery.TabIndex = 5;
            this.btnQuery.Text = "查询";
            this.btnQuery.Click += new System.EventHandler(this.btn查询_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(80, 0);
            this.simpleButton2.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(120, 28);
            this.simpleButton2.TabIndex = 6;
            this.simpleButton2.Text = "新建户主档案";
            this.simpleButton2.Click += new System.EventHandler(this.btn新建户主档案_Click);
            // 
            // btn删除
            // 
            this.btn删除.Image = ((System.Drawing.Image)(resources.GetObject("btn删除.Image")));
            this.btn删除.Location = new System.Drawing.Point(206, 0);
            this.btn删除.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btn删除.Name = "btn删除";
            this.btn删除.Size = new System.Drawing.Size(78, 28);
            this.btn删除.TabIndex = 8;
            this.btn删除.Text = "删除";
            this.btn删除.Click += new System.EventHandler(this.btn删除_Click);
            // 
            // btn导出
            // 
            this.btn导出.Image = ((System.Drawing.Image)(resources.GetObject("btn导出.Image")));
            this.btn导出.Location = new System.Drawing.Point(290, 0);
            this.btn导出.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btn导出.Name = "btn导出";
            this.btn导出.Size = new System.Drawing.Size(75, 28);
            this.btn导出.TabIndex = 10;
            this.btn导出.Text = "导出";
            this.btn导出.Click += new System.EventHandler(this.btn导出_Click);
            // 
            // btn打印条码
            // 
            this.btn打印条码.Image = ((System.Drawing.Image)(resources.GetObject("btn打印条码.Image")));
            this.btn打印条码.Location = new System.Drawing.Point(371, 0);
            this.btn打印条码.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btn打印条码.Name = "btn打印条码";
            this.btn打印条码.Size = new System.Drawing.Size(108, 28);
            this.btn打印条码.TabIndex = 11;
            this.btn打印条码.Text = "打印查体条码";
            this.btn打印条码.Click += new System.EventHandler(this.btn打印条码_Click);
            // 
            // btn打印条码New
            // 
            this.btn打印条码New.Location = new System.Drawing.Point(485, 3);
            this.btn打印条码New.Name = "btn打印条码New";
            this.btn打印条码New.Size = new System.Drawing.Size(85, 23);
            this.btn打印条码New.TabIndex = 14;
            this.btn打印条码New.Text = "打印条码(新)";
            this.btn打印条码New.Click += new System.EventHandler(this.btn打印条码New_Click);
            // 
            // btn修改记录
            // 
            this.btn修改记录.Image = ((System.Drawing.Image)(resources.GetObject("btn修改记录.Image")));
            this.btn修改记录.Location = new System.Drawing.Point(576, 0);
            this.btn修改记录.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btn修改记录.Name = "btn修改记录";
            this.btn修改记录.Size = new System.Drawing.Size(78, 28);
            this.btn修改记录.TabIndex = 7;
            this.btn修改记录.Text = "复核记录";
            this.btn修改记录.Click += new System.EventHandler(this.btn修改记录_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(660, 0);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(118, 28);
            this.simpleButton1.TabIndex = 12;
            this.simpleButton1.Text = "重叠数据分析1";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // btn综合打印
            // 
            this.btn综合打印.Image = ((System.Drawing.Image)(resources.GetObject("btn综合打印.Image")));
            this.btn综合打印.Location = new System.Drawing.Point(784, 0);
            this.btn综合打印.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btn综合打印.Name = "btn综合打印";
            this.btn综合打印.Size = new System.Drawing.Size(87, 28);
            this.btn综合打印.TabIndex = 13;
            this.btn综合打印.Text = "综合打印";
            this.btn综合打印.Click += new System.EventHandler(this.btn综合打印_Click);
            // 
            // btnEmpty
            // 
            this.btnEmpty.Image = ((System.Drawing.Image)(resources.GetObject("btnEmpty.Image")));
            this.btnEmpty.Location = new System.Drawing.Point(877, 0);
            this.btnEmpty.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.Size = new System.Drawing.Size(105, 28);
            this.btnEmpty.TabIndex = 9;
            this.btnEmpty.Text = "批量打印档案";
            this.btnEmpty.Click += new System.EventHandler(this.btnEmpty_Click);
            // 
            // gc个人健康档案
            // 
            this.gc个人健康档案.AllowBandedGridColumnSort = false;
            this.gc个人健康档案.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc个人健康档案.IsBestFitColumns = true;
            this.gc个人健康档案.Location = new System.Drawing.Point(0, 200);
            this.gc个人健康档案.MainView = this.gv个人健康档案;
            this.gc个人健康档案.Name = "gc个人健康档案";
            this.gc个人健康档案.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.gc个人健康档案.ShowContextMenu = true;
            this.gc个人健康档案.Size = new System.Drawing.Size(1107, 260);
            this.gc个人健康档案.StrWhere = "";
            this.gc个人健康档案.TabIndex = 4;
            this.gc个人健康档案.UseCheckBox = true;
            this.gc个人健康档案.View = "";
            this.gc个人健康档案.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv个人健康档案});
            // 
            // gv个人健康档案
            // 
            this.gv个人健康档案.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gv个人健康档案.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gv个人健康档案.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gv个人健康档案.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.DetailTip.Options.UseFont = true;
            this.gv个人健康档案.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.Empty.Options.UseFont = true;
            this.gv个人健康档案.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.EvenRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FilterPanel.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FixedLine.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FocusedCell.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FocusedRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gv个人健康档案.Appearance.FooterPanel.Options.UseFont = true;
            this.gv个人健康档案.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.GroupButton.Options.UseFont = true;
            this.gv个人健康档案.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.GroupFooter.Options.UseFont = true;
            this.gv个人健康档案.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.GroupPanel.Options.UseFont = true;
            this.gv个人健康档案.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.GroupRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.HeaderPanel.Options.UseFont = true;
            this.gv个人健康档案.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.HorzLine.Options.UseFont = true;
            this.gv个人健康档案.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.OddRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.Preview.Options.UseFont = true;
            this.gv个人健康档案.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.Row.Options.UseFont = true;
            this.gv个人健康档案.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.RowSeparator.Options.UseFont = true;
            this.gv个人健康档案.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.SelectedRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.TopNewRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.VertLine.Options.UseFont = true;
            this.gv个人健康档案.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.ViewCaption.Options.UseFont = true;
            this.gv个人健康档案.ColumnPanelRowHeight = 30;
            this.gv个人健康档案.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col个人档案号码,
            this.col姓名,
            this.col性别,
            this.col出生日期,
            this.col身份证号,
            this.col居住地址,
            this.col本人电话,
            this.col联系人电话,
            this.col所属机构,
            this.col创建人,
            this.col创建时间,
            this.col档案状态,
            this.col工作单位,
            this.gridColumn1,
            this.col残疾人,
            this.col肺结核,
            this.col高血压,
            this.col冠心病,
            this.col精神病,
            this.col老年人,
            this.col脑卒中,
            this.col贫困,
            this.col糖尿病,
            this.col儿童,
            this.col孕产妇});
            this.gv个人健康档案.GridControl = this.gc个人健康档案;
            this.gv个人健康档案.GroupPanelText = "DragColumn";
            this.gv个人健康档案.Name = "gv个人健康档案";
            this.gv个人健康档案.OptionsView.ColumnAutoWidth = false;
            this.gv个人健康档案.OptionsView.EnableAppearanceEvenRow = true;
            this.gv个人健康档案.OptionsView.EnableAppearanceOddRow = true;
            this.gv个人健康档案.OptionsView.ShowGroupPanel = false;
            // 
            // col个人档案号码
            // 
            this.col个人档案号码.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.col个人档案号码.AppearanceCell.Options.UseBackColor = true;
            this.col个人档案号码.AppearanceCell.Options.UseTextOptions = true;
            this.col个人档案号码.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col个人档案号码.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col个人档案号码.AppearanceHeader.Options.UseFont = true;
            this.col个人档案号码.AppearanceHeader.Options.UseTextOptions = true;
            this.col个人档案号码.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col个人档案号码.Caption = "档案号";
            this.col个人档案号码.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.col个人档案号码.FieldName = "个人档案编号";
            this.col个人档案号码.Name = "col个人档案号码";
            this.col个人档案号码.OptionsColumn.ReadOnly = true;
            this.col个人档案号码.Visible = true;
            this.col个人档案号码.VisibleIndex = 0;
            this.col个人档案号码.Width = 127;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit1_Click);
            // 
            // col姓名
            // 
            this.col姓名.AppearanceCell.Options.UseTextOptions = true;
            this.col姓名.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col姓名.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col姓名.AppearanceHeader.Options.UseFont = true;
            this.col姓名.AppearanceHeader.Options.UseTextOptions = true;
            this.col姓名.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col姓名.Caption = "姓名";
            this.col姓名.FieldName = "姓名";
            this.col姓名.Name = "col姓名";
            this.col姓名.OptionsColumn.ReadOnly = true;
            this.col姓名.Visible = true;
            this.col姓名.VisibleIndex = 1;
            // 
            // col性别
            // 
            this.col性别.AppearanceCell.Options.UseTextOptions = true;
            this.col性别.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col性别.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col性别.AppearanceHeader.Options.UseFont = true;
            this.col性别.AppearanceHeader.Options.UseTextOptions = true;
            this.col性别.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col性别.Caption = "性别";
            this.col性别.FieldName = "性别";
            this.col性别.Name = "col性别";
            this.col性别.OptionsColumn.ReadOnly = true;
            this.col性别.Visible = true;
            this.col性别.VisibleIndex = 2;
            // 
            // col出生日期
            // 
            this.col出生日期.AppearanceCell.Options.UseTextOptions = true;
            this.col出生日期.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col出生日期.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col出生日期.AppearanceHeader.Options.UseFont = true;
            this.col出生日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col出生日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col出生日期.Caption = "出生日期";
            this.col出生日期.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.col出生日期.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.col出生日期.FieldName = "出生日期";
            this.col出生日期.Name = "col出生日期";
            this.col出生日期.OptionsColumn.ReadOnly = true;
            this.col出生日期.Visible = true;
            this.col出生日期.VisibleIndex = 3;
            // 
            // col身份证号
            // 
            this.col身份证号.AppearanceCell.Options.UseTextOptions = true;
            this.col身份证号.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col身份证号.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col身份证号.AppearanceHeader.Options.UseFont = true;
            this.col身份证号.AppearanceHeader.Options.UseTextOptions = true;
            this.col身份证号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col身份证号.Caption = "身份证号";
            this.col身份证号.FieldName = "身份证号";
            this.col身份证号.Name = "col身份证号";
            this.col身份证号.OptionsColumn.ReadOnly = true;
            this.col身份证号.Visible = true;
            this.col身份证号.VisibleIndex = 4;
            // 
            // col居住地址
            // 
            this.col居住地址.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col居住地址.AppearanceHeader.Options.UseFont = true;
            this.col居住地址.AppearanceHeader.Options.UseTextOptions = true;
            this.col居住地址.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col居住地址.Caption = "居住地址";
            this.col居住地址.FieldName = "居住地址";
            this.col居住地址.Name = "col居住地址";
            this.col居住地址.OptionsColumn.ReadOnly = true;
            this.col居住地址.Visible = true;
            this.col居住地址.VisibleIndex = 5;
            this.col居住地址.Width = 163;
            // 
            // col本人电话
            // 
            this.col本人电话.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col本人电话.AppearanceHeader.Options.UseFont = true;
            this.col本人电话.AppearanceHeader.Options.UseTextOptions = true;
            this.col本人电话.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col本人电话.Caption = "本人电话";
            this.col本人电话.FieldName = "本人电话";
            this.col本人电话.Name = "col本人电话";
            this.col本人电话.OptionsColumn.ReadOnly = true;
            this.col本人电话.Visible = true;
            this.col本人电话.VisibleIndex = 6;
            // 
            // col联系人电话
            // 
            this.col联系人电话.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col联系人电话.AppearanceHeader.Options.UseFont = true;
            this.col联系人电话.AppearanceHeader.Options.UseTextOptions = true;
            this.col联系人电话.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col联系人电话.Caption = "联系人电话";
            this.col联系人电话.FieldName = "联系人电话";
            this.col联系人电话.Name = "col联系人电话";
            this.col联系人电话.OptionsColumn.ReadOnly = true;
            this.col联系人电话.Visible = true;
            this.col联系人电话.VisibleIndex = 7;
            // 
            // col所属机构
            // 
            this.col所属机构.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col所属机构.AppearanceHeader.Options.UseFont = true;
            this.col所属机构.AppearanceHeader.Options.UseTextOptions = true;
            this.col所属机构.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col所属机构.Caption = "当前所属机构";
            this.col所属机构.FieldName = "所属机构";
            this.col所属机构.Name = "col所属机构";
            this.col所属机构.OptionsColumn.ReadOnly = true;
            this.col所属机构.Visible = true;
            this.col所属机构.VisibleIndex = 8;
            this.col所属机构.Width = 93;
            // 
            // col创建人
            // 
            this.col创建人.AppearanceCell.Options.UseTextOptions = true;
            this.col创建人.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col创建人.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col创建人.AppearanceHeader.Options.UseFont = true;
            this.col创建人.AppearanceHeader.Options.UseTextOptions = true;
            this.col创建人.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col创建人.Caption = "录入人";
            this.col创建人.FieldName = "创建人";
            this.col创建人.Name = "col创建人";
            this.col创建人.OptionsColumn.ReadOnly = true;
            this.col创建人.Visible = true;
            this.col创建人.VisibleIndex = 9;
            // 
            // col创建时间
            // 
            this.col创建时间.AppearanceCell.Options.UseTextOptions = true;
            this.col创建时间.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col创建时间.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col创建时间.AppearanceHeader.Options.UseFont = true;
            this.col创建时间.AppearanceHeader.Options.UseTextOptions = true;
            this.col创建时间.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col创建时间.Caption = "录入时间";
            this.col创建时间.DisplayFormat.FormatString = "d";
            this.col创建时间.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.col创建时间.FieldName = "创建时间";
            this.col创建时间.Name = "col创建时间";
            this.col创建时间.OptionsColumn.ReadOnly = true;
            this.col创建时间.Visible = true;
            this.col创建时间.VisibleIndex = 10;
            // 
            // col档案状态
            // 
            this.col档案状态.AppearanceCell.Options.UseTextOptions = true;
            this.col档案状态.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col档案状态.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col档案状态.AppearanceHeader.Options.UseFont = true;
            this.col档案状态.AppearanceHeader.Options.UseTextOptions = true;
            this.col档案状态.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col档案状态.Caption = "档案状态";
            this.col档案状态.FieldName = "档案状态";
            this.col档案状态.Name = "col档案状态";
            this.col档案状态.OptionsColumn.ReadOnly = true;
            this.col档案状态.Visible = true;
            this.col档案状态.VisibleIndex = 11;
            // 
            // col工作单位
            // 
            this.col工作单位.AppearanceCell.Options.UseTextOptions = true;
            this.col工作单位.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col工作单位.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col工作单位.AppearanceHeader.Options.UseFont = true;
            this.col工作单位.AppearanceHeader.Options.UseTextOptions = true;
            this.col工作单位.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col工作单位.Caption = "工作单位";
            this.col工作单位.FieldName = "工作单位";
            this.col工作单位.Name = "col工作单位";
            this.col工作单位.OptionsColumn.ReadOnly = true;
            this.col工作单位.Visible = true;
            this.col工作单位.VisibleIndex = 12;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "档案位置";
            this.gridColumn1.FieldName = "档案位置";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 13;
            // 
            // col残疾人
            // 
            this.col残疾人.Caption = "残疾人";
            this.col残疾人.FieldName = "can";
            this.col残疾人.Name = "col残疾人";
            this.col残疾人.OptionsColumn.ShowCaption = false;
            this.col残疾人.Visible = true;
            this.col残疾人.VisibleIndex = 14;
            // 
            // col肺结核
            // 
            this.col肺结核.Caption = "肺结核";
            this.col肺结核.FieldName = "fei";
            this.col肺结核.Name = "col肺结核";
            this.col肺结核.OptionsColumn.ShowCaption = false;
            this.col肺结核.Visible = true;
            this.col肺结核.VisibleIndex = 15;
            // 
            // col高血压
            // 
            this.col高血压.Caption = "高血压";
            this.col高血压.FieldName = "gao";
            this.col高血压.Name = "col高血压";
            this.col高血压.OptionsColumn.ShowCaption = false;
            this.col高血压.Visible = true;
            this.col高血压.VisibleIndex = 16;
            // 
            // col冠心病
            // 
            this.col冠心病.Caption = "冠心病";
            this.col冠心病.FieldName = "guan";
            this.col冠心病.Name = "col冠心病";
            this.col冠心病.OptionsColumn.ShowCaption = false;
            this.col冠心病.Visible = true;
            this.col冠心病.VisibleIndex = 17;
            // 
            // col精神病
            // 
            this.col精神病.Caption = "精神障碍";
            this.col精神病.FieldName = "jing";
            this.col精神病.Name = "col精神病";
            this.col精神病.OptionsColumn.ShowCaption = false;
            this.col精神病.Visible = true;
            this.col精神病.VisibleIndex = 18;
            // 
            // col老年人
            // 
            this.col老年人.Caption = "老年人";
            this.col老年人.FieldName = "lao";
            this.col老年人.Name = "col老年人";
            this.col老年人.OptionsColumn.ShowCaption = false;
            this.col老年人.Visible = true;
            this.col老年人.VisibleIndex = 19;
            // 
            // col脑卒中
            // 
            this.col脑卒中.Caption = "脑卒中";
            this.col脑卒中.FieldName = "nao";
            this.col脑卒中.Name = "col脑卒中";
            this.col脑卒中.OptionsColumn.ShowCaption = false;
            this.col脑卒中.Visible = true;
            this.col脑卒中.VisibleIndex = 20;
            // 
            // col贫困
            // 
            this.col贫困.Caption = "贫困人口";
            this.col贫困.FieldName = "pin";
            this.col贫困.Name = "col贫困";
            this.col贫困.OptionsColumn.ShowCaption = false;
            this.col贫困.Visible = true;
            this.col贫困.VisibleIndex = 21;
            // 
            // col糖尿病
            // 
            this.col糖尿病.Caption = "糖尿病";
            this.col糖尿病.FieldName = "tang";
            this.col糖尿病.Name = "col糖尿病";
            this.col糖尿病.OptionsColumn.ShowCaption = false;
            this.col糖尿病.Visible = true;
            this.col糖尿病.VisibleIndex = 22;
            // 
            // col儿童
            // 
            this.col儿童.Caption = "儿童";
            this.col儿童.FieldName = "you";
            this.col儿童.Name = "col儿童";
            this.col儿童.OptionsColumn.ShowCaption = false;
            this.col儿童.Visible = true;
            this.col儿童.VisibleIndex = 23;
            // 
            // col孕产妇
            // 
            this.col孕产妇.Caption = "孕产妇";
            this.col孕产妇.FieldName = "yun";
            this.col孕产妇.Name = "col孕产妇";
            this.col孕产妇.OptionsColumn.ShowCaption = false;
            this.col孕产妇.Visible = true;
            this.col孕产妇.VisibleIndex = 24;
            // 
            // gcDetailEditor
            // 
            this.gcDetailEditor.Controls.Add(this.textEdit12);
            this.gcDetailEditor.Controls.Add(this.textEdit11);
            this.gcDetailEditor.Controls.Add(this.txt编码);
            this.gcDetailEditor.Location = new System.Drawing.Point(30, 60);
            this.gcDetailEditor.Name = "gcDetailEditor";
            this.gcDetailEditor.Size = new System.Drawing.Size(587, 274);
            this.gcDetailEditor.TabIndex = 0;
            this.gcDetailEditor.Text = "groupControl2";
            // 
            // textEdit12
            // 
            this.textEdit12.Location = new System.Drawing.Point(109, 155);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Size = new System.Drawing.Size(254, 20);
            this.textEdit12.TabIndex = 2;
            // 
            // textEdit11
            // 
            this.textEdit11.Location = new System.Drawing.Point(109, 114);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Size = new System.Drawing.Size(254, 20);
            this.textEdit11.TabIndex = 1;
            // 
            // txt编码
            // 
            this.txt编码.Location = new System.Drawing.Point(109, 74);
            this.txt编码.Name = "txt编码";
            this.txt编码.Size = new System.Drawing.Size(254, 20);
            this.txt编码.TabIndex = 0;
            // 
            // pagerControl1
            // 
            this.pagerControl1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pagerControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pagerControl1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pagerControl1.JumpText = "跳转";
            this.pagerControl1.Location = new System.Drawing.Point(0, 460);
            this.pagerControl1.Margin = new System.Windows.Forms.Padding(144030, 144030, 144030, 144030);
            this.pagerControl1.Name = "pagerControl1";
            this.pagerControl1.PageIndex = 1;
            this.pagerControl1.PageSize = 10;
            this.pagerControl1.paramters = null;
            this.pagerControl1.RecordCount = 0;
            this.pagerControl1.Size = new System.Drawing.Size(1107, 69);
            this.pagerControl1.SqlQuery = null;
            this.pagerControl1.TabIndex = 5;
            // 
            // frm个人健康档案
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1113, 561);
            this.Controls.Add(this.panelControl5);
            this.Margin = new System.Windows.Forms.Padding(3, 8, 3, 8);
            this.Name = "frm个人健康档案";
            this.Text = "个人健康档案";
            this.Load += new System.EventHandler(this.frm个人健康档案_Load);
            this.Controls.SetChildIndex(this.panelControl5, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.Controls.SetChildIndex(this.gcNavigator, 0);
            this.Controls.SetChildIndex(this.pnlSummary, 0);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            this.tpDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabSearchCondition)).EndInit();
            this.tabSearchCondition.ResumeLayout(false);
            this.tab_基础.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt工作单位.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte调查时间1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte调查时间1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte调查时间2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte调查时间2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo排序方式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt本人电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk户主.Properties)).EndInit();
            this.flowLayoutPanel11.ResumeLayout(false);
            this.flowLayoutPanel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt年龄1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt年龄2.Properties)).EndInit();
            this.flowLayoutPanel10.ResumeLayout(false);
            this.flowLayoutPanel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dte最近更新时间1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte最近更新时间1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte最近更新时间2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte最近更新时间2.Properties)).EndInit();
            this.flowLayoutPanel9.ResumeLayout(false);
            this.flowLayoutPanel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo血型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboRH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt新农合号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医疗保险号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案类别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo合格档案.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo缺项调查.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo职业.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo婚姻状况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo民族.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo常住类型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo文化程度.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo最近修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo录入人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系人电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            this.tab_重点.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk普通人群.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk重点人群.Properties)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk是否流动人口.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk是否签约服务.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk是否贫困人口.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk外出人员.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk流入人口.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk已复核.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk未复核.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk二次复核.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk未二次复核.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk参保人数.Properties)).EndInit();
            this.flowLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk听力残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk言语残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肢体残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk智力残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk视力残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk残疾All.Properties)).EndInit();
            this.flowLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk高血压.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2型糖尿病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk冠心病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑卒中.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肿瘤.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk慢阻肺.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk重症精神病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk高血压高危人群.Properties)).EndInit();
            this.flowLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio重点人群.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            this.tab_高危.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            this.flowLayoutPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk临界高血压.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk血脂边缘升高.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk空腹血糖升高.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk糖耐量异常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肥胖.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk重度吸烟.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk超重且中心型肥胖.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk高危因素.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc个人健康档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv个人健康档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetailEditor)).EndInit();
            this.gcDetailEditor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt编码.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraTab.XtraTabControl tabSearchCondition;
        private DevExpress.XtraTab.XtraTabPage tab_基础;
        private DevExpress.XtraTab.XtraTabPage tab_重点;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private DevExpress.XtraEditors.CheckEdit chk听力残疾;
        private DevExpress.XtraEditors.CheckEdit chk肢体残疾;
        private DevExpress.XtraEditors.CheckEdit chk智力残疾;
        private DevExpress.XtraEditors.CheckEdit chk视力残疾;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private DevExpress.XtraEditors.CheckEdit chk高血压;
        private DevExpress.XtraEditors.CheckEdit chk2型糖尿病;
        private DevExpress.XtraEditors.CheckEdit chk冠心病;
        private DevExpress.XtraEditors.CheckEdit chk脑卒中;
        private DevExpress.XtraEditors.CheckEdit chk肿瘤;
        private DevExpress.XtraEditors.CheckEdit chk慢阻肺;
        private DevExpress.XtraEditors.CheckEdit chk重症精神病;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private DevExpress.XtraEditors.RadioGroup radio重点人群;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraTab.XtraTabPage tab_高危;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private DevExpress.XtraEditors.CheckEdit chk临界高血压;
        private DevExpress.XtraEditors.CheckEdit chk血脂边缘升高;
        private DevExpress.XtraEditors.CheckEdit chk空腹血糖升高;
        private DevExpress.XtraEditors.CheckEdit chk糖耐量异常;
        private DevExpress.XtraEditors.CheckEdit chk肥胖;
        private DevExpress.XtraEditors.CheckEdit chk重度吸烟;
        private DevExpress.XtraEditors.CheckEdit chk超重且中心型肥胖;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DataGridControl gc个人健康档案;
        private DevExpress.XtraGrid.Views.Grid.GridView gv个人健康档案;
        private DevExpress.XtraGrid.Columns.GridColumn col个人档案号码;
        private DevExpress.XtraGrid.Columns.GridColumn col姓名;
        private DevExpress.XtraGrid.Columns.GridColumn col性别;
        private DevExpress.XtraGrid.Columns.GridColumn col出生日期;
        private DevExpress.XtraGrid.Columns.GridColumn col身份证号;
        private DevExpress.XtraGrid.Columns.GridColumn col居住地址;
        private DevExpress.XtraGrid.Columns.GridColumn col本人电话;
        private DevExpress.XtraGrid.Columns.GridColumn col联系人电话;
        private DevExpress.XtraGrid.Columns.GridColumn col所属机构;
        private DevExpress.XtraGrid.Columns.GridColumn col创建人;
        private DevExpress.XtraGrid.Columns.GridColumn col创建时间;
        private DevExpress.XtraGrid.Columns.GridColumn col档案状态;
        private DevExpress.XtraEditors.GroupControl gcDetailEditor;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.TextEdit txt编码;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit textEdit地址;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit村;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit镇;
        private DevExpress.XtraEditors.CheckEdit checkEdit21;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEdit机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraEditors.CheckEdit chk户主;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private DevExpress.XtraEditors.TextEdit txt年龄1;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txt年龄2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private DevExpress.XtraEditors.DateEdit dte最近更新时间1;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.DateEdit dte最近更新时间2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private DevExpress.XtraEditors.ComboBoxEdit cbo血型;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.ComboBoxEdit cboRH;
        private DevExpress.XtraEditors.TextEdit txt新农合号;
        private DevExpress.XtraEditors.TextEdit txt医疗保险号;
        private DevExpress.XtraEditors.DateEdit dte调查时间1;
        private DevExpress.XtraEditors.DateEdit dte调查时间2;
        private DevExpress.XtraEditors.DateEdit dte录入时间1;
        private DevExpress.XtraEditors.DateEdit dte录入时间2;
        private DevExpress.XtraEditors.DateEdit dte出生时间1;
        private DevExpress.XtraEditors.DateEdit dte出生时间2;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt档案号;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.ComboBoxEdit cbo档案类别;
        private DevExpress.XtraEditors.ComboBoxEdit cbo档案状态;
        private DevExpress.XtraEditors.ComboBoxEdit cbo合格档案;
        private DevExpress.XtraEditors.ComboBoxEdit cbo缺项调查;
        private DevExpress.XtraEditors.ComboBoxEdit cbo职业;
        private DevExpress.XtraEditors.ComboBoxEdit cbo婚姻状况;
        private DevExpress.XtraEditors.ComboBoxEdit cbo民族;
        private DevExpress.XtraEditors.ComboBoxEdit cbo常住类型;
        private DevExpress.XtraEditors.ComboBoxEdit cbo性别;
        private DevExpress.XtraEditors.ComboBoxEdit cbo文化程度;
        private DevExpress.XtraEditors.LookUpEdit cbo最近修改人;
        private DevExpress.XtraEditors.LookUpEdit cbo录入人;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        public DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private TActionProject.PagerControl pagerControl1;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraEditors.SimpleButton btnEmpty;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton btn修改记录;
        private DevExpress.XtraEditors.SimpleButton btn删除;
        private DevExpress.XtraEditors.TextEdit txt本人电话;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.TextEdit txt联系人电话;
        private DevExpress.XtraEditors.SimpleButton btn导出;
        private DevExpress.XtraEditors.ComboBoxEdit cbo排序方式;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn打印条码;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.CheckEdit chk是否流动人口;
        private DevExpress.XtraEditors.CheckEdit chk是否签约服务;
        private DevExpress.XtraEditors.CheckEdit chk是否贫困人口;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.CheckEdit chk外出人员;
        private DevExpress.XtraEditors.CheckEdit chk残疾All;
        private DevExpress.XtraEditors.CheckEdit chk言语残疾;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton btn综合打印;
        private DevExpress.XtraEditors.CheckEdit chk已复核;
        private DevExpress.XtraEditors.CheckEdit chk未复核;
        private DevExpress.XtraEditors.CheckEdit chk流入人口;
        private DevExpress.XtraEditors.SimpleButton btn打印条码New;
        private DevExpress.XtraEditors.TextEdit txt工作单位;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraGrid.Columns.GridColumn col工作单位;
        private DevExpress.XtraEditors.CheckEdit chk二次复核;
        private DevExpress.XtraEditors.CheckEdit chk未二次复核;
        private DevExpress.XtraEditors.CheckEdit chk高危因素;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private DevExpress.XtraEditors.CheckEdit chk普通人群;
        private DevExpress.XtraEditors.CheckEdit chk重点人群;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn col残疾人;
        private DevExpress.XtraGrid.Columns.GridColumn col肺结核;
        private DevExpress.XtraGrid.Columns.GridColumn col高血压;
        private DevExpress.XtraGrid.Columns.GridColumn col冠心病;
        private DevExpress.XtraGrid.Columns.GridColumn col精神病;
        private DevExpress.XtraGrid.Columns.GridColumn col老年人;
        private DevExpress.XtraGrid.Columns.GridColumn col脑卒中;
        private DevExpress.XtraGrid.Columns.GridColumn col贫困;
        private DevExpress.XtraGrid.Columns.GridColumn col糖尿病;
        private DevExpress.XtraGrid.Columns.GridColumn col儿童;
        private DevExpress.XtraGrid.Columns.GridColumn col孕产妇;
        private DevExpress.XtraEditors.CheckEdit chk高血压高危人群;
        private DevExpress.XtraEditors.CheckEdit chk参保人数;

    }
}