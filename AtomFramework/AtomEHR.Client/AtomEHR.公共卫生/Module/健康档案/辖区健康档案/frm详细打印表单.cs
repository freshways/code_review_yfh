﻿using AtomEHR.Business;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Common;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.健康档案.辖区健康档案
{
    public partial class frm详细打印表单 : Form
    {
        private DataRow currentrow;
        
        public frm详细打印表单()
        {
            InitializeComponent();
        }

        public List<string> Rresult = new List<string>();

        public frm详细打印表单(DataRow row,bool isPrint=false)
        {
            InitializeComponent();
            this.currentrow = row;
            this.textEdit姓名.Text = currentrow["姓名"].ToString();
            this.textEdit性别.Text = currentrow["性别"].ToString();
            this.textEdit年龄.Text = AtomEHR.公共卫生.util.ControlsHelper.GetAge(currentrow["出生日期"].ToString(), new bll健康体检().ServiceDateTime).ToString();
            this.textEdit村庄.Text = currentrow["居委会"].ToString();
            this.ucTxtLbl血红蛋白.Txt1.Text = row["血红蛋白"].ToString();
            this.ucTxtLbl白细胞.Txt1.Text = row["白细胞"].ToString();
            this.ucTxtLbl血小板.Txt1.Text = row["血小板"].ToString();
            this.ucTxtLbl尿蛋白.Txt1.Text = row["尿蛋白"].ToString();
            this.ucTxtLbl尿胴体.Txt1.Text = row["尿酮体"].ToString();
            this.ucTxtLbl尿糖.Txt1.Text = row["尿糖"].ToString();
            this.ucTxtLbl尿潜血.Txt1.Text = row["尿潜血"].ToString();
            this.ucTxtLbl血清谷丙转氨酶.Txt1.Text = row["血清谷丙转氨酶"].ToString();
            this.ucTxtLbl血清谷草转氨酶.Txt1.Text = row["血清谷草转氨酶"].ToString();
            this.ucTxtLbl白蛋白.Txt1.Text = row["白蛋白"].ToString();
            this.ucTxtLbl总胆红素.Txt1.Text = row["总胆红素"].ToString();
            this.ucTxtLbl结合胆红素.Txt1.Text = row["结合胆红素"].ToString();
            this.ucTxtLbl血清肌酐.Txt1.Text = row["血清肌酐"].ToString();
            this.ucTxtLbl血尿素氮.Txt1.Text = row["血尿素氮"].ToString();
            this.ucTxtLbl血钾浓度.Txt1.Text = row["血钾浓度"].ToString();
            this.ucTxtLbl血钠浓度.Txt1.Text = row["血钠浓度"].ToString();
            this.ucTxtLbl空腹血糖.Txt1.Text = row["空腹血糖"].ToString();
            this.ucTxtLbl餐后2H血糖.Txt1.Text = row["餐后2H血糖"].ToString();
            this.ucTxtLbl左侧血压.Txt1.Text = row["血压左侧"].ToString();
            this.ucTxtLbl右侧血压.Txt1.Text = row["血压右侧"].ToString();
            this.ucTxtLbl总胆固醇.Txt1.Text = row["总胆固醇"].ToString();
            this.ucTxtLbl甘油三酯.Txt1.Text = row["甘油三酯"].ToString();
            this.ucTxtLbl血清低密度脂蛋白胆固醇.Txt1.Text = row["血清低密度脂蛋白胆固醇"].ToString();
            this.ucTxtLbl血清高密度脂蛋白胆固醇.Txt1.Text = row["血清高密度脂蛋白胆固醇"].ToString();
            this.ucTxtLbl心电图.Txt1.Text = row["心电图"].ToString();
            this.ucTxtLbl心电图异常.Txt1.Text = row["心电图异常"].ToString();
            this.ucTxtLbl体质指数.Txt1.Text = row["体重指数"].ToString();
            this.uc平和质.Txt1.Text = row["平和质"].ToString();
            this.uc气虚质.Txt1.Text = row["气虚质"].ToString();
            this.uc阳虚质.Txt1.Text = row["阳虚质"].ToString();
            this.uc阴虚质.Txt1.Text = row["阴虚质"].ToString();
            this.uc痰湿质.Txt1.Text = row["痰湿质"].ToString();
            this.uc湿热质.Txt1.Text = row["湿热质"].ToString();
            this.uc血瘀质.Txt1.Text = row["血瘀质"].ToString();
            this.uc气郁质.Txt1.Text = row["气郁质"].ToString();
            this.uc特禀质.Txt1.Text = row["特禀质"].ToString();
            this.ucTxtLblB超.Txt1.Text = row["B超"].ToString();
            this.ucTxtLblB超异常.Txt1.Text = row["B超其他"].ToString();
            if (row["FIELD2"] != null)
                this.cbo体检医师.Text = row["FIELD2"].ToString();

            Calculate();
            if (isPrint)
            {
                string 体检项目 = string.Empty;
                for (int i = 0; i < DataDictCache.Cache.t体检项目.Rows.Count; i++)
                {
                    体检项目 += DataDictCache.Cache.t体检项目.Rows[i]["NativeName"] + "、";
                }
                if (!string.IsNullOrEmpty(体检项目))
                {
                    this.textEdit体检项目.Text = 体检项目.Remove(体检项目.Length - 1);
                }
                this.listBoxControl1.Items.Clear();
                string 体检建议 = string.Empty;
                for (int i = 0; i < DataDictCache.Cache.t体检建议.Rows.Count; i++)
                {
                    this.listBoxControl1.Items.Add(DataDictCache.Cache.t体检建议.Rows[i]["NativeName"]);
                }
                this.cbo体检单位.Text = Loginer.CurrentUser.所属机构名称;
                List<string> result = new List<string>();
                result.Add(this.textEdit姓名.Text);
                result.Add(this.textEdit性别.Text);
                result.Add(this.textEdit年龄.Text);
                result.Add(this.textEdit村庄.Text);
                result.Add(this.cbo体检医师.Text.Trim());
                result.Add(this.cbo体检单位.Text.Trim());
                result.Add(this.textEdit体检项目.Text.Trim());
                result.Add(this.txt结论建议.Text);
                result.Add(currentrow["体检日期"].ToString());
                result.Add(this.cbo体检单位.Text);
                Rresult = result;
            }
        }

        private void Calculate()
        {
            StringBuilder 症状temp = new StringBuilder();
            StringBuilder 健康指导temp = new StringBuilder();

            #region BMI
            //BMI
            /*
                过轻：低于18.5
                正常：18.5-23.9
                超重：≥24
             * 24～27.9 偏胖
                肥胖：28-32
                非常肥胖, 高于32
             */
            if (!string.IsNullOrEmpty(this.ucTxtLbl体质指数.Txt1.Text))
            {
                double bmi = double.Parse(this.ucTxtLbl体质指数.Txt1.Text);
                if (bmi < 18.5)
                {
                    症状temp.Append("【体重偏轻（体重指数：" + bmi + ")" + ", 】");
                    健康指导temp.AppendLine("体重偏轻（体重指数：" + bmi + ")");
                    健康指导temp.AppendLine("建议：补充营养。");
                }
                else if (bmi >= 24)
                {
                    症状temp.Append("【体重超重（体重指数：" + bmi + "), 】");
                    健康指导temp.AppendLine("体重超重（体重指数：" + bmi + ")");
                    健康指导temp.AppendLine("建议：少食，减盐，控制体重。");
                }
                else if (bmi >= 24 && bmi < 27.9)
                {
                    症状temp.Append("【体重偏胖（体重指数：" + bmi + ")" + ", 】");
                    健康指导temp.AppendLine("体重偏胖（体重指数：" + bmi + ")");
                    健康指导temp.AppendLine("建议：少食，减盐，控制体重。");
                }
                else if (bmi >= 28 && bmi < 32)
                {
                    症状temp.Append("【体重肥胖（体重指数：" + bmi + ")" + ", 】");
                    健康指导temp.AppendLine("体重肥胖（体重指数：" + bmi + ")");
                    健康指导temp.AppendLine("建议：少食，减盐，控制体重。");
                }
                else if (bmi > 32)
                {
                    症状temp.Append("【体重非常肥胖（体重指数：" + bmi + ")" + ", 】");
                    健康指导temp.AppendLine("体重非常肥胖（体重指数：" + bmi + ")");
                    健康指导temp.AppendLine("建议：少食，减盐，控制体重。");
                }
            }

            #endregion

            #region 糖尿病

            if (!string.IsNullOrEmpty(this.ucTxtLbl空腹血糖.Txt1.Text))
            {
                string xuetang = this.ucTxtLbl空腹血糖.Txt1.Text.Trim();
                decimal _xuetang = 0m;
                if (Decimal.TryParse(xuetang, out _xuetang))
                {
                    //判断血糖高低值
                    if (_xuetang != 0m && _xuetang > 6.40m)
                    {
                        症状temp.Append("【血糖偏高(GLU:" + _xuetang + "mmol/l)（3.89-6.40）】");
                        健康指导temp.AppendLine("血糖偏高(GLU:" + _xuetang + "mmol/l)（3.89-6.40）");
                        健康指导temp.AppendLine("血糖偏高，建议：进一步复查。" );
                    }
                    if (_xuetang != 0m && _xuetang < 3.89m)
                    {
                        症状temp.Append("【血糖偏低(GLU:" + _xuetang + "mmol/l)（3.89-6.40）" + "；】");
                        健康指导temp.AppendLine("血糖偏低(GLU:" + _xuetang + "mmol/l)（3.89-6.40）" + "；");
                        健康指导temp.AppendLine("建议复查空腹血糖。糖尿病建议：\n1、糖尿病者积极配合医院规范化治疗。\n2、接受多种形式的健康教育知识。\n3、调整饮食，控制热量的摄入。按医嘱进行自我监测。\n4、成人35岁以后都应每年查血糖和尿糖，早发现早治疗。\n5、改变不良生活方式，忌烟酒，加强运动。\n6、保持心态平衡，克服各种紧张压力，培养高尚情操，多爬山郊游等。\n7、定期复查");
                    }

                }
            }

            #endregion

            #region 血压

            if (!string.IsNullOrEmpty(this.ucTxtLbl右侧血压.Txt1.Text) || !string.IsNullOrEmpty(this.ucTxtLbl左侧血压.Txt1.Text))
            {
                //右侧
                if (!string.IsNullOrEmpty(this.ucTxtLbl右侧血压.Txt1.Text))
                {
                    string xueya = this.ucTxtLbl右侧血压.Txt1.Text.Trim();
                    string you1 = xueya.Split('/')[0];
                    string you2 = xueya.Split('/')[1];
                    int _you1; int _you2;
                    if (int.TryParse(you1, out _you1) && int.TryParse(you2, out _you2))
                    {
                        if (_you1 > 140 || _you2 > 90)
                        {
                            症状temp.Append("【血压偏高（" + xueya + ")" + ", 】");
                            健康指导temp.AppendLine("血压偏高（" + xueya + ")");
                            健康指导temp.AppendLine("建议连续三日测血压以明确诊断。高血压建议：\nA.注意劳逸结合，保持心境平和；\nB.坚持有氧运动；\nC.戒烟限酒；\nD.少吃盐；\nE.控制体重。\n治疗：上述非药物治疗后血压仍高者，应考虑用降压药。定期测量血压。");
                        }
                    }
                }
                //左侧
                //else if (!string.IsNullOrEmpty(this.ucTxtLbl左侧血压.Txt1.Text))
                //{
                //    string xueya = this.ucTxtLbl左侧血压.Txt1.Text.Trim();
                //    string you1 = xueya.Split('/')[0];
                //    string you2 = xueya.Split('/')[1];
                //    int _you1; int _you2;
                //    if (int.TryParse(you1, out _you1) && int.TryParse(you2, out _you2))
                //    {
                //        if (_you1 > 140 || _you2 > 90)
                //        {
                //            症状temp.AppendLine("【血压偏高（" + xueya + ")" + ",】");
                //            健康指导temp.AppendLine("血压偏高（" + xueya + ")");
                //            健康指导temp.AppendLine("建议连续三日测血压以明确诊断。高血压建议：\nA.注意劳逸结合，保持心境平和；\nB.坚持有氧运动；\nC.戒烟限酒；\nD.少吃盐；\nE.控制体重。\n治疗：上述非药物治疗后血压仍高者，应考虑用降压药。定期测量血压。");
                //        }
                //    }
                //}
            }

            #endregion

            #region 血常规
            /*
             白细胞：3.9-9.9
             血红蛋白：116-179
             血小板：98.7-302.9
             */
            if (!string.IsNullOrEmpty(ucTxtLbl白细胞.Txt1.Text) || !string.IsNullOrEmpty(ucTxtLbl血红蛋白.Txt1.Text) || !string.IsNullOrEmpty(ucTxtLbl血小板.Txt1.Text))
            {
                string baixibao = ucTxtLbl白细胞.Txt1.Text.Trim();
                string xuehongdanbai = ucTxtLbl血红蛋白.Txt1.Text.Trim();
                string xuexiaoban = ucTxtLbl血小板.Txt1.Text.Trim();
                decimal _baixibao;
                decimal _xuehongdanbai;
                decimal _xuexiaoban;
                string text = "【血常规异常：";
                if (decimal.TryParse(baixibao, out _baixibao))
                {
                    if (_baixibao > 9.9m || _baixibao < 3.9m)
                    {
                        text += "白细胞:" + _baixibao + "(3.9-9.9)";
                    }
                }
                if (decimal.TryParse(xuehongdanbai, out _xuehongdanbai))
                {
                    if (_xuehongdanbai > 179m || _xuehongdanbai < 116m)
                    {
                        text += "血红蛋白:" + _xuehongdanbai + "(116-179)";
                    }
                }
                if (decimal.TryParse(xuexiaoban, out _xuexiaoban))
                {
                    if (_xuexiaoban > 302.9m || _xuexiaoban < 98.7m)
                    {
                        text += "血小板:" + _xuexiaoban + "(98.7-302.9)";
                    }
                }
                if (text != "【血常规异常：")
                {
                    症状temp.Append(text + ", 】");
                    健康指导temp.AppendLine(text + " 】");
                    健康指导temp.AppendLine("建议：医院复查");
                }
            }

            #endregion

            #region 尿常规
            if (!string.IsNullOrEmpty(ucTxtLbl尿蛋白.Txt1.Text) || !string.IsNullOrEmpty(ucTxtLbl尿胴体.Txt1.Text) || !string.IsNullOrEmpty(ucTxtLbl尿潜血.Txt1.Text) || !string.IsNullOrEmpty(ucTxtLbl尿糖.Txt1.Text))
            {
                string text = "【尿常规异常：";
                if (!string.IsNullOrEmpty(ucTxtLbl尿蛋白.Txt1.Text) && ucTxtLbl尿蛋白.Txt1.Text.Trim() != "-")
                {
                    text += "尿蛋白:" + ucTxtLbl尿蛋白.Txt1.Text;
                }
                if (!string.IsNullOrEmpty(ucTxtLbl尿胴体.Txt1.Text) && ucTxtLbl尿胴体.Txt1.Text.Trim() != "-")
                {
                    text += "尿酮体:" + ucTxtLbl尿胴体.Txt1.Text;
                }
                if (!string.IsNullOrEmpty(ucTxtLbl尿潜血.Txt1.Text) && ucTxtLbl尿潜血.Txt1.Text.Trim() != "-")
                {
                    text += "尿潜血:" + ucTxtLbl尿潜血.Txt1.Text;
                }
                if (!string.IsNullOrEmpty(ucTxtLbl尿糖.Txt1.Text) && ucTxtLbl尿糖.Txt1.Text.Trim() != "-")
                {
                    text += "尿糖:" + ucTxtLbl尿糖.Txt1.Text;
                }
                if (text != "【尿常规异常：")
                {
                    症状temp.Append(text + ", 】");
                    健康指导temp.AppendLine(text + " 】");
                    健康指导temp.AppendLine("建议：请结合临床，进一步复查。");
                }
            }
            #endregion

            #region 肝功
            /*
             血清谷丙转氨酶：0-40
             血清谷草转氨酶：0-41
             总胆红素：2-20.5
             结合胆红素：0-7.0
             */
            if (!string.IsNullOrEmpty(ucTxtLbl血清谷丙转氨酶.Txt1.Text) || !string.IsNullOrEmpty(ucTxtLbl血清谷草转氨酶.Txt1.Text) || !string.IsNullOrEmpty(ucTxtLbl总胆红素.Txt1.Text) || !string.IsNullOrEmpty(ucTxtLbl结合胆红素.Txt1.Text))
            {
                string 血清谷丙转氨酶 = ucTxtLbl血清谷丙转氨酶.Txt1.Text.Trim();
                string 血清谷草转氨酶 = ucTxtLbl血清谷草转氨酶.Txt1.Text.Trim();
                string 总胆红素 = ucTxtLbl总胆红素.Txt1.Text.Trim();
                string 结合胆红素 = ucTxtLbl结合胆红素.Txt1.Text.Trim();
                decimal _血清谷丙转氨酶;
                decimal _血清谷草转氨酶;
                decimal _总胆红素;
                decimal _结合胆红素;
                string text = "【肝功异常：";
                if (decimal.TryParse(血清谷丙转氨酶, out _血清谷丙转氨酶))
                {
                    if (_血清谷丙转氨酶 > 40m || _血清谷丙转氨酶 < 0m)
                    {
                        text += "谷丙转氨酶:" + _血清谷丙转氨酶 + "(0-40);";
                    }
                }
                if (decimal.TryParse(血清谷草转氨酶, out _血清谷草转氨酶))
                {
                    if (_血清谷草转氨酶 > 41m || _血清谷草转氨酶 < 0m)
                    {
                        text += "谷草转氨酶:" + _血清谷草转氨酶 + "(0-41);";
                    }
                }
                if (decimal.TryParse(总胆红素, out _总胆红素))
                {
                    if (_总胆红素 > 20.5m || _总胆红素 < 2m)
                    {
                        text += "总胆红素:" + _总胆红素 + "(2-20.5);";
                    }
                }
                if (decimal.TryParse(结合胆红素, out _结合胆红素))
                {
                    if (_结合胆红素 > 7m || _结合胆红素 < 0m)
                    {
                        text += "结合胆红素:" + _结合胆红素 + "(0-7.0);";
                    }
                }
                if (text != "【肝功异常：")
                {
                    症状temp.Append(text + ", 】");
                    健康指导temp.AppendLine(text + " 】");
                    健康指导temp.AppendLine(@"肝功异常建议：导致增高的原因很多，应定期复查。1、 注意在下次检查前，三天禁烟及动物脂肪等高脂肪食品，后抽血。2、如果仍高，应到医院正规治疗。");
                }
            }
            #endregion

            #region 肾功
            /*
             血清肌酐:53-115
             血尿素氮:1.7-8.7 
             */
            if (!string.IsNullOrEmpty(ucTxtLbl血清肌酐.Txt1.Text) || !string.IsNullOrEmpty(ucTxtLbl血尿素氮.Txt1.Text))
            {
                string 血清肌酐 = ucTxtLbl血清肌酐.Txt1.Text.Trim();
                string 血尿素氮 = ucTxtLbl血尿素氮.Txt1.Text.Trim();
                decimal _血清肌酐;
                decimal _血尿素氮;
                string text = "【肾功异常：";
                if (decimal.TryParse(血清肌酐, out _血清肌酐))
                {
                    if (_血清肌酐 > 115m || _血清肌酐 < 53m)
                    {
                        text += "血清肌酐:" + _血清肌酐 + "(53-115);";
                    }
                }
                if (decimal.TryParse(血尿素氮, out _血尿素氮))
                {
                    if (_血尿素氮 > 8.7m || _血尿素氮 < 1.7m)
                    {
                        text += "血尿素氮:" + _血尿素氮 + "(1.7-8.7 );";
                    }
                }

                if (text != "【肾功异常：")
                {
                    症状temp.Append(text + ", 】 ");
                    健康指导temp.AppendLine(text + " 】");
                    健康指导temp.AppendLine("建议：请结合临床，进一步复查。");
                }
            }
            #endregion

            #region 血脂
            /*
             总胆固醇：2.34-5.20
             甘油三酯：0.7-1.7
             高密度脂蛋白：0.77-2.25
             低密度脂蛋白：0-4.13
             */
            if (!string.IsNullOrEmpty(ucTxtLbl总胆固醇.Txt1.Text) || !string.IsNullOrEmpty(ucTxtLbl甘油三酯.Txt1.Text) || !string.IsNullOrEmpty(ucTxtLbl血清高密度脂蛋白胆固醇.Txt1.Text) || !string.IsNullOrEmpty(ucTxtLbl血清低密度脂蛋白胆固醇.Txt1.Text))
            {
                string 总胆固醇 = ucTxtLbl总胆固醇.Txt1.Text.Trim();
                string 甘油三酯 = ucTxtLbl甘油三酯.Txt1.Text.Trim();
                string 高密度脂蛋白 = ucTxtLbl血清高密度脂蛋白胆固醇.Txt1.Text.Trim();
                string 低密度脂蛋白 = ucTxtLbl血清低密度脂蛋白胆固醇.Txt1.Text.Trim();
                decimal _总胆固醇;
                decimal _甘油三酯;
                decimal _高密度脂蛋白;
                decimal _低密度脂蛋白;
                string text = "【血脂异常：";
                if (decimal.TryParse(总胆固醇, out _总胆固醇))
                {
                    if (_总胆固醇 > 5.2m || _总胆固醇 < 2.34m)
                    {
                        text += "总胆固醇:" + _总胆固醇 + "(2.34-5.20);";
                    }
                }
                if (decimal.TryParse(甘油三酯, out _甘油三酯))
                {
                    if (_甘油三酯 > 1.7m || _甘油三酯 < 0.7m)
                    {
                        text += "甘油三酯:" + _甘油三酯 + "(0.7-1.7);";
                    }
                }
                if (decimal.TryParse(高密度脂蛋白, out _高密度脂蛋白))
                {
                    if (_高密度脂蛋白 > 2.25m || _高密度脂蛋白 < 0.77m)
                    {
                        text += "高密度脂蛋白:" + _高密度脂蛋白 + "(0.77-2.25);";
                    }
                }
                if (decimal.TryParse(低密度脂蛋白, out _低密度脂蛋白))
                {
                    if (_低密度脂蛋白 > 4.13m || _低密度脂蛋白 < 0m)
                    {
                        text += "低密度脂蛋白:" + _低密度脂蛋白 + "(0-4.13);";
                    }
                }
                if (text != "【血脂异常：")
                {
                    症状temp.Append(text + ", 】");
                    健康指导temp.AppendLine(text + " 】");
                    健康指导temp.AppendLine("建议：要注意清淡饮食，不吃高脂餐，控制进食量，适当运动，戒烟限酒。");
                }
            }
            #endregion

            #region 心电图
            if (!string.IsNullOrEmpty(this.ucTxtLbl心电图.Txt1.Text) && !this.ucTxtLbl心电图.Txt1.Text.Contains("正常"))
            {
                症状temp.Append("【心电图异常：" + this.ucTxtLbl心电图.Txt1.Text + ucTxtLbl心电图异常.Txt1.Text.Trim() + ", 】");
                健康指导temp.AppendLine("心电图异常：" + this.ucTxtLbl心电图.Txt1.Text + ucTxtLbl心电图异常.Txt1.Text.Trim() + ";");
                健康指导temp.AppendLine("建议：请结合临床，进一步复查。");
            }
            #endregion

            #region B超
            //if (this.ucTxtLblB超.Txt1.Text != "未做")
            //{
            健康指导temp.AppendLine("B超：" + this.ucTxtLblB超.Txt1.Text+ ";");
            if (!string.IsNullOrEmpty(this.ucTxtLblB超异常.Txt1.Text))
            {
                症状temp.Append("【B超异常：" +this.ucTxtLblB超异常.Txt1.Text+"。】");
                健康指导temp.AppendLine("B超异常，建议：请结合临床，进一步复查。");
            }
            //}
            #endregion

            if (健康指导temp.Length == 0)
            {
                健康指导temp.AppendLine("体检未见明显异常。");
            }
            #region 体质辨识
            if (!string.IsNullOrEmpty(uc平和质.Txt1.Text) || !string.IsNullOrEmpty(uc气虚质.Txt1.Text) || !string.IsNullOrEmpty(uc阳虚质.Txt1.Text) || !string.IsNullOrEmpty(uc阴虚质.Txt1.Text) || !string.IsNullOrEmpty(uc痰湿质.Txt1.Text) || !string.IsNullOrEmpty(uc湿热质.Txt1.Text) || !string.IsNullOrEmpty(uc血瘀质.Txt1.Text) || !string.IsNullOrEmpty(uc气郁质.Txt1.Text) || !string.IsNullOrEmpty(uc特禀质.Txt1.Text))
            {
                健康指导temp.AppendLine("中医体质辨识结果：");
                if (!string.IsNullOrEmpty(uc平和质.Txt1.Text))
                { 
                    健康指导temp.AppendLine("平和质：" + uc平和质.Txt1.Text); 
                    健康指导temp.AppendLine(@"【总体特征】 阴阳气血调和，以体态适中、面色润泽、精力充沛等为主要特征。
【情志调摄】宜保持平和的心态。可根据个人爱好，选择弹琴、下棋、书法、绘画、听音乐、阅读、旅游、种植花草等放松心情。
【饮食调养】饮食宜粗细粮食合理搭配，多吃五谷杂粮、蔬菜瓜果，少食过于油腻及辛辣食品；不要过饥过饱，也不要进食过冷过烫或不干净食物；注意戒烟限酒。
【起居调摄】起居宜规律，睡眠要充足，劳逸相结合，穿戴求自然。
【运动保健】形成良好的运动健身习惯。可根据个人爱好和耐受程度，选择运动健身项目。
【穴位保健】（1）选穴：涌泉、足三里。（2）定位：涌泉位于足底部，卷足时足前部凹陷处，约当足底2、3趾趾缝纹头端与足跟连线的前三分之一与后三分之二交点上。足三里位于小腿前外侧，当犊鼻下3寸，距胫骨前缘一横指处。（3）操作：用大拇指或中指指腹按压穴位，做轻柔缓和的环旋活动，以穴位感到酸胀为度，按揉2～3分钟。每天操作1～2次。"); 
                }
                if (!string.IsNullOrEmpty(uc气虚质.Txt1.Text))
                { 
                    健康指导temp.AppendLine("气虚质：" + uc气虚质.Txt1.Text);
                    健康指导temp.AppendLine(@"【总体特征】元气不足，以疲乏、气短、自汗等表现为主要特征。
【情志调摄】宜保持稳定乐观的心态，不可过度劳神。宜欣赏节奏明快的音乐，如笛子曲《喜相逢》等。
【饮食调养】宜选用性平偏温、健脾益气的食物，如大米、小米、南瓜、胡萝卜、山药、大枣、香菇、莲子、白扁豆、黄豆、豆腐、鸡肉、鸡蛋、鹌鹑（蛋）、牛肉等。尽量少吃或不吃槟榔、生萝卜等耗气的食物。不宜多食生冷苦寒、辛辣燥热的食物。
【起居调摄】提倡劳逸结合，不要过于劳作，以免损伤正气。平时应避免汗出受风。居室环境应采用明亮的暖色调。
【运动保健】宜选择比较柔和的传统健身项目，如八段锦。在做完全套八段锦动作后，将“两手攀足固肾腰”和“攒拳怒目增力气”各加做1～3遍。避免剧烈运动。还可采用提肛法防止脏器下垂，提肛法：全身放松，注意力集中在会阴肛门部。首先吸气收腹，收缩并提升肛门，停顿2～3秒之后，再缓慢放松呼气，如此反复10～15次。
【穴位保健】（1）选穴：气海、关元。（2）定位：气海位于下腹部，前正中线上，当脐中下1.5寸；关元位于下腹部，前正中线上，当脐下3寸（3）操作：用掌根着力于穴位，做轻柔缓和的环旋活动，每个穴位按揉2～3分钟，每天操作1～2次。还可以采用艾条温和灸，增加温阳益气的作用。点燃艾条或借助温灸盒，对穴位进行温灸，每次10分钟。艾条温和灸点燃端要与皮肤保持2～3厘米的距离，不要烫伤皮肤。温和灸可每周操作1次。"); 
                }
                if (!string.IsNullOrEmpty(uc阳虚质.Txt1.Text))
                { 
                    健康指导temp.AppendLine("阳虚质：" + uc阳虚质.Txt1.Text);
                    健康指导temp.AppendLine(@"【总体特征】阳气不足，以畏寒怕冷、手足不温等表现为主要特征。
【情志调摄】宜保持积极向上的心态，正确对待生活中的不利事件，及时调节自己的消极情绪。宜欣赏激昂、高亢、豪迈的音乐，如《黄河大合唱》等。
【饮食调养	】宜选用甘温补脾阳、温肾阳为主的食物，如羊肉、鸡肉、带鱼、黄鳝、虾、刀豆、韭菜、茴香、核桃、栗子、腰果、松子、红茶、生姜等。少食生冷、苦寒、黏腻食物，如田螺、螃蟹、海带、紫菜、芹菜、苦瓜、冬瓜、西瓜、香蕉、柿子、甘蔗、梨、绿豆、蚕豆、绿茶、冷冻饮料等。即使在盛夏也不要过食寒凉之品。
【起居调摄】居住环境以温和的暖色调为宜，不宜在阴暗、潮湿、寒冷的环境下长期工作和生活。平时要注意腰部、背部和下肢保暖。白天保持一定活动量，避免打盹瞌睡。睡觉前尽量不要饮水，睡前将小便排净。
【运动保健】宜在阳光充足的环境下适当进行舒缓柔和的户外活动，尽量避免在大风、大寒、大雪的环境中锻炼。日光浴、空气浴是较好的强身壮阳之法。也可选择八段锦，在完成整套动作后将“五劳七伤往后瞧”和“两手攀足固肾腰”加做1～3遍。
【穴位保健】（1）选穴：关元、命门。（2）定位：关元（位置见气虚质）。命门位于腰部，当后正中线上,第2腰椎棘突下凹陷中。（3）操作：两穴均可采用温和灸（见气虚质）的方法，每周进行1次。关元穴还可采用掌根揉法（见气虚质），按揉每穴2～3分钟，每天1～2次。也可配合摩擦腰肾法温肾助阳，以手掌鱼际、掌根或拳背摩擦两侧腰骶部，每次操作约10分钟，以摩至皮肤温热为度，每天1次。"); 
                }
                if (!string.IsNullOrEmpty(uc阴虚质.Txt1.Text))
                { 
                    健康指导temp.AppendLine("阴虚质：" + uc阴虚质.Txt1.Text);
                    健康指导temp.AppendLine(@"【总体特征】阴液亏少，以口燥咽干、手足心热等表现为主要特征。
【情志调摄】宜加强自我修养、培养自己的耐性，尽量减少与人争执、动怒，不宜参加竞争胜负的活动，可在安静、优雅环境中练习书法、绘画等。有条件者可以选择在环境清新凉爽的海边、山林旅游休假。 宜欣赏曲调轻柔、舒缓的音乐，如舒伯特《小夜曲》等。
【饮食调养】宜选用甘凉滋润的食物，如鸭肉、猪瘦肉、百合、黑芝麻、蜂蜜、荸荠、鳖、海蜇、海参、甘蔗、银耳、燕窝等。少食温燥、辛辣、香浓的食物，如羊肉、韭菜、茴香、辣椒、葱、蒜、葵花子、酒、咖啡、浓茶，以及荔枝、龙眼、樱桃、杏、大枣、核桃、栗子等。
【起居调摄】居住环境宜安静，睡好“子午觉”。避免熬夜及在高温酷暑下工作，不宜洗桑拿、泡温泉。节制房事，勿吸烟。注意防晒，保持皮肤湿润，宜选择选择蚕丝等清凉柔和的衣物。
【运动保健】宜做中小强度的运动项目，控制出汗量，及时补充水分。不宜进行大强度、大运动量的锻炼，避免在炎热的夏天或闷热的环境中运动。可选择八段锦，在做完八段锦整套动作后将“摇头摆尾去心火”和“两手攀足固肾腰”加做1～3遍。也可选择太极拳、太极剑等。
【穴位保健	】（1）选穴：太溪、三阴交。（2）定位：太溪位于足内侧，内踝后方，当内踝尖与跟腱之间的凹陷处；三阴交位于小腿内侧，当足内踝尖上3寸，胫骨内侧缘后方。（3）操作：采用指揉的方法（见平和质），每个穴位按揉2～3分钟，每天操作1～2次。"); 
                }
                if (!string.IsNullOrEmpty(uc痰湿质.Txt1.Text))
                { 
                    健康指导temp.AppendLine("痰湿质：" + uc痰湿质.Txt1.Text);
                    健康指导temp.AppendLine(@"【总体特征】痰湿凝聚，以形体肥胖、腹部肥满、口黏苔腻等表现为主要特征。
【情志调摄】宜多参加社会活动，培养广泛的兴趣爱好。宜欣赏激进、振奋的音乐，如二胡《赛马》等。
【饮食调养】宜选用健脾助运、祛湿化痰的食物，如冬瓜、白萝卜、薏苡仁、赤小豆、荷叶、山楂、生姜、荠菜、紫菜、海带、鲫鱼、鲤鱼、鲈鱼、文蛤等。少食肥、甜、油、黏（腻）的食物。
【起居调摄】居住环境宜干燥，不宜潮湿，穿衣面料以棉、麻、丝等透气散湿的天然纤维为佳，尽量保持宽松，有利于汗液蒸发，祛除体内湿气。晚上睡觉枕头不宜过高，防止打鼾加重；早睡早起，不要过于安逸，勿贪恋沙发和床榻。
【运动保健】坚持长期运动锻炼，强度应根据自身的状况循序渐进。不宜在阴雨季节、天气湿冷的气候条件下运动。可选择快走、武术以及打羽毛球等，使松弛的肌肉逐渐变得结实、致密。如果体重过重、膝盖受损，可选择游泳。
【穴位保健】（1）选穴：丰隆、足三里。（2）定位：足三里（位置见气虚质）。丰隆位于小腿前外侧，当外踝尖上8寸，条口外，距胫骨前缘二横指处。（3）操作：采用指揉法（见平和质）。"); 
                }
                if (!string.IsNullOrEmpty(uc湿热质.Txt1.Text))
                { 
                    健康指导temp.AppendLine("湿热质：" + uc湿热质.Txt1.Text);
                    健康指导temp.AppendLine(@"【总体特征】湿热内蕴，以面垢油光、口苦、苔黄腻等表现为主要特征。
【情志调摄】宜稳定情绪，尽量避免烦恼，可选择不同形式的兴趣爱好。宜欣赏曲调悠扬的乐曲，如古筝《高山流水》等。
【饮食调养】宜选用甘寒或苦寒的清利化湿食物，如绿豆（芽）、绿豆糕、绿茶、芹菜、黄瓜、苦瓜、西瓜、冬瓜、薏苡仁、赤小豆、马齿苋、藕等。少食羊肉、动物内脏等肥厚油腻之品，以及韭菜、生姜、辣椒、胡椒、花椒及火锅、烹炸、烧烤等辛温助热的食物。
【起居调摄】居室宜干燥、通风良好，避免居处潮热，可在室内用除湿器或空调改善湿、热的环境。选择款式宽松，透气性好的天然棉、麻、丝质服装。注意个人卫生，预防皮肤病变。保持充足而有规律的睡眠，睡前半小时不宜思考问题、看书、看情节紧张的电视节目，避免服用兴奋饮料，不宜吸烟饮酒。保持二便通畅，防止湿热积聚。
【运动保健】宜做中长跑、游泳、各种球类、武术等强度较大的锻炼。夏季应避免在烈日下长时间活动，在秋高气爽的季节，经常选择爬山登高，更有助于祛除湿热。也可做八段锦，在完成整套动作后将“双手托天理三焦”和“调理脾胃须单举”加做1～3遍，每日1遍。
【穴位保健】（1）选穴：支沟、阴陵泉。（2）定位：支沟穴位于前臂背侧，当阳池与肘尖的连线上，腕背横纹上3寸，尺骨与桡骨之间（见图7）。阴陵泉位于小腿内侧，当胫骨内侧踝后下凹陷处。（3）操作：采用指揉法（见平和质）。阴陵泉还可以选择刮痧，先涂刮痧油，用刮痧板与皮肤呈45°角在穴位区域从上往下刮，以皮肤潮红或出痧点为度。");             
                }
                if (!string.IsNullOrEmpty(uc血瘀质.Txt1.Text))
                {
                    健康指导temp.AppendLine("血瘀质：" + uc血瘀质.Txt1.Text);
                    健康指导temp.AppendLine(@"【总体特征】血行不畅，以肤色晦黯、舌质紫黯等表现为主要特征。
【情志调摄】遇事宜沉稳，努力克服浮躁情绪。宜欣赏流畅抒情的音乐，如《春江花月夜》等。
【饮食调养】宜选用具有调畅气血作用的食物，如生山楂、醋、玫瑰花、桃仁（花）、黑豆、油菜等。少食收涩、寒凉、冰冻之物，如乌梅、柿子、石榴、苦瓜、花生米，以及高脂肪、高胆固醇、油腻食物，如蛋黄、虾、猪头肉、猪脑、奶酪等。还可少量饮用葡萄酒、糯米甜酒，有助于促进血液运行，但高血压和冠心病等患者不宜饮用。女性月经期间慎用活血类食物。
【起居调摄】居室宜温暖舒适，不宜在阴暗、寒冷的环境中长期工作和生活。衣着宜宽松，注意保暖，保持大便通畅。不宜贪图安逸，宜在阳光充足的时候进行户外活动。避免长时间打麻将、久坐、看电视等。
【运动保健】宜进行有助于促进气血运行的运动项目，持之以恒。如步行健身法，或者八段锦，在完成整套动作后将“左右开弓似射雕”和“背后七颠百病消”加做1～3遍。避免在封闭环境中进行锻炼。锻炼强度视身体情况而定，不宜进行大强度、大负荷运动，以防意外。
【穴位保健】（1）选穴：期门、血海。（2）定位：期门位于胸部，当乳头直下，第6肋间隙，前正中线旁开4寸。血海：屈膝，在大腿内侧，髌底内侧端上2寸，当股四头肌内侧头的隆起处。（3）操作：采用指揉法（见平和质）。");      
                }
                if (!string.IsNullOrEmpty(uc气郁质.Txt1.Text))
                {
                    健康指导temp.AppendLine("气郁质：" + uc气郁质.Txt1.Text);
                    健康指导temp.AppendLine(@"【总体特征】气机郁滞，以神情抑郁、紧张焦虑等表现为主要特征。
【情志调摄】宜乐观开朗，多与他人相处，不苛求自己也不苛求他人。如心境抑郁不能排解时，要积极寻找原因，及时向朋友倾诉。宜欣赏节奏欢快、旋律优美的乐曲如《金蛇狂舞》等，还适宜看喜剧、励志剧，以及轻松愉悦的相声表演。
【饮食调养】宜选用具有理气解郁作用的食物，如黄花菜、菊花、玫瑰花、茉莉花、大麦、金橘、柑橘、柚子等。少食收敛酸涩的食物，如石榴、乌梅、青梅、杨梅、草莓、杨桃、酸枣、李子、柠檬、南瓜、泡菜等。
【起居调摄】尽量增加户外活动和社交，防止一人独处时心生凄凉。居室保持安静，宜宽敞、明亮。平日保持有规律的睡眠，睡前避免饮用茶、咖啡和可可等饮料。衣着宜柔软、透气、舒适。
【运动保健】宜多参加群体性体育运动项目，坚持做较大强度、较大负荷的“发泄式”锻炼，如跑步、登山、游泳。也可参与下棋、打牌等娱乐活动，分散注意力。
【穴位保健】（1）选穴：合谷、太冲穴（2）定位：合谷位于手背，第1、2掌骨间，当第2掌骨桡侧的中点处。太冲位于足背侧，当第1跖骨间隙的后方凹陷处。（3）操作：采用指揉的方法（见平和质）");      
                }
                if (!string.IsNullOrEmpty(uc特禀质.Txt1.Text))
                { 
                    健康指导temp.AppendLine("特禀质：" + uc特禀质.Txt1.Text);
                    健康指导temp.AppendLine(@" 【总体特征】过敏体质者，禀赋不耐、异气外侵，以过敏反应等为主要特征；先天失常者为另一类特禀质，以禀赋异常为主要特征。
【情志调摄】过敏体质的人因对过敏原敏感，容易产生紧张、焦虑等情绪，因此要在尽量避免过敏原的同时，还应避免紧张情绪。
【饮食调养】饮食宜均衡、粗细粮食搭配适当、荤素配伍合理，宜多食益气固表的食物，尽量少食辛辣、腥发食物，不食含致敏物质的食品，如蚕豆、白扁豆、羊肉、鹅肉、鲤鱼、虾、蟹、茄子、辣椒、浓茶、咖啡等。 
【起居调摄】起居要有规律，保持充足的睡眠时间。居室宜通风良好。生活环境中接触的物品如枕头、棉被、床垫、地毯、窗帘、衣橱易附有尘螨，可引起过敏，应经常清洗、日晒。外出也要避免处在花粉及粉刷油漆的空气中，以免刺激而诱发过敏病症。
【运动保健】宜进行慢跑、散步等户外活动，也可选择下棋、瑜珈等室内活动。不宜选择大运动量的活动，避免春天或季节交替时长时间在野外锻炼。运动时注意避风寒，如出现哮喘、憋闷的现象应及时停止运动。
【穴位保健】（1）选穴：神阙、曲池。（2）定位：神阙位于腹中部，脐中央。曲池位于肘横纹外侧端，屈肘，当尺泽与在肘横纹外侧端与肱骨外上髁连线中点。（3）操作：神阙采用温和灸（见气虚质）。曲池采用指揉法（见平和质）。");      
                }
            }

            this.txt结论建议.Text = 健康指导temp.ToString();
            this.textEdit症状.Text = 症状temp.ToString();

            #endregion


        }

        private void frm详细打印表单_Load(object sender, EventArgs e)
        {
            //util.ControlsHelper.BindComboxData(DataDictCache.Cache.体检医师, cbo体检医师, "ISID", "NativeName");
            //cbo体检医师.SelectedIndex = 1;
            string 体检项目 = string.Empty;
            for (int i = 0; i < DataDictCache.Cache.t体检项目.Rows.Count; i++)
            {
                体检项目 += DataDictCache.Cache.t体检项目.Rows[i]["NativeName"] + "、";
            }
            if (!string.IsNullOrEmpty(体检项目))
            {
                this.textEdit体检项目.Text = 体检项目.Remove(体检项目.Length - 1);
            }
            this.listBoxControl1.Items.Clear();
            string 体检建议 = string.Empty;
            for (int i = 0; i < DataDictCache.Cache.t体检建议.Rows.Count; i++)
            {
                this.listBoxControl1.Items.Add(DataDictCache.Cache.t体检建议.Rows[i]["NativeName"]);
            }
            //if (!string.IsNullOrEmpty(体检项目))
            //{
            //    this.textEdit体检项目.Text = 体检项目.Remove(体检项目.Length - 1);
            //}
            this.cbo体检单位.Text = Loginer.CurrentUser.所属机构名称;
        }

        private void btn打印_Click(object sender, EventArgs e)
        {
            string Hospital = this.cbo体检单位.Text;
            string name = this.textEdit姓名.Text;
            string gender = this.textEdit性别.Text;
            string age = this.textEdit年龄.Text;
            string cunzhuang = this.textEdit村庄.Text;
            string yishi = this.cbo体检医师.Text.Trim();
            string danwei = this.cbo体检单位.Text.Trim();
            string xiangmu = this.textEdit体检项目.Text.Trim();
            string text = this.txt结论建议.Text;

            List<string> result = new List<string>();
            result.Add(name);
            result.Add(gender);
            result.Add(age);
            result.Add(cunzhuang);
            result.Add(yishi);
            result.Add(danwei);
            result.Add(xiangmu);
            result.Add(text);
            result.Add(currentrow["体检日期"].ToString());
            result.Add(Hospital);

            XtraReport1 report = new XtraReport1(result);
            ReportPrintTool tool = new ReportPrintTool(report);
            tool.ShowPreviewDialog();
            //if (tool.PrintDialog() == true)
            //{
            //    //tool.Print();
            //    //if (report.ShowPrintStatusDialog == true)
            //    //{
            //    //SetIsPrintNVTrue(orderIDList);
            //    //result = true;
            //    //sbtnSearch_Click(null, null);
            //    //}
            //}
            //else
            //{
            //    //result = false;
            //}
        }

        private void listBoxControl1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            string text = this.listBoxControl1.SelectedItem.ToString();
            this.txt结论建议.Document.AppendText("\r\n" + text);
            //this.txt结论建议.Document.pos.Text.Insert(this.txt结论建议.Text.Length, "\r\n");
            //this.txt结论建议.Text.Insert(this.txt结论建议.Text.Length, text);
        }

        private void btn打印2_Click(object sender, EventArgs e)
        {
            List<string> result = new List<string>();
            result.Add(this.textEdit姓名.Text); //0*
            result.Add(this.textEdit性别.Text);//1
            result.Add(this.textEdit年龄.Text);//2
            result.Add(this.textEdit村庄.Text);//3
            result.Add(this.cbo体检医师.Text.Trim());//4
            result.Add(this.cbo体检单位.Text.Trim());//5
            result.Add(this.textEdit体检项目.Text.Trim());//6
            result.Add(this.txt结论建议.Text);//7*
            result.Add(currentrow["体检日期"].ToString());//8
            result.Add(currentrow["个人档案编号"].ToString());//9
            result.Add(currentrow["身份证号"].ToString());//10
            result.Add(this.textEdit症状.Text);//11*

            体检回执单.回执A4.回执封面1A4 rpta4 = new 体检回执单.回执A4.回执封面1A4(result);
            rpta4.CreateDocument();

            体检回执单.回执A4.回执健康指导 rpta5 = new 体检回执单.回执A4.回执健康指导(result);
            rpta5.CreateDocument();

            体检回执单.回执A4.回执生化 rpta6 = new 体检回执单.回执A4.回执生化(currentrow);
            rpta6.CreateDocument();

            XtraReport[] reports = new XtraReport[] { rpta4, rpta5, rpta6 };

           XtraReport report1 = new XtraReport();
           report1.CreateDocument();
           report1.PrintingSystem.ContinuousPageNumbering = true;
           foreach (XtraReport report in reports)
           {
               //逐一显示报表
               //ReportPrintTool pts = new ReportPrintTool(report);

               ////pts.PrintingSystem.StartPrint += new PrintDocumentEventHandler(reportsStartPrintEventHandler);

               //pts.ShowPreviewDialog();

               //把报表添加导一个里面显示
               report1.Pages.AddRange(report.Pages);
           }

           string ftpxz = currentrow["街道"].ToString();
           string ftprq = currentrow["体检日期"].ToString();
           string ftpdn = currentrow["个人档案编号"].ToString().Substring(2);
           AtomEHR.公共卫生.util.FtpUtil ftpcl = new util.FtpUtil("192.168.10.135", "21", "qzxdt", "qzyy@163.com");
           byte[] ecg = ftpcl.Download(@"ftp://192.168.10.135/心电图/" + ftpxz + "/" + ftprq + "/" + ftpdn + ".JPG");
           if (ecg != null && ecg.Length > 0)
           {
               体检回执单.回执A4.回执心电 rpta7 = new 体检回执单.回执A4.回执心电(ecg);
               rpta7.CreateDocument();
               report1.Pages.AddRange(rpta7.Pages);
           }
           //2018年10月24日 yfh 添加B超打印
           //体检回执单.回执A4.回执B超 rpta8 = new 体检回执单.回执A4.回执B超(currentrow, this.textEditB超医师.Text, ftpxz);

           体检回执单.回执A4.回执B超 rpta8 = new 体检回执单.回执A4.回执B超(currentrow["个人档案编号"].ToString(), ftprq);
           rpta8.CreateDocument();
           report1.Pages.AddRange(rpta8.Pages);

           ReportPrintTool pt1 = new ReportPrintTool(report1);
           //pt1.PrintingSystem.StartPrint += new PrintDocumentEventHandler(PrintingSystem_StartPrint);
           //打印报表
           //pt1.PrintDialog(); //显示选择打印的页面
           pt1.ShowPreviewDialog();
            
        }
    }
}
