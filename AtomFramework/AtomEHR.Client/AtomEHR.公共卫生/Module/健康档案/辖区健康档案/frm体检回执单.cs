﻿using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Interfaces;
using AtomEHR.Library;
using AtomEHR.公共卫生.Module.个人健康;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.健康档案.辖区健康档案
{
    public partial class frm体检回执单 : frmBaseBusinessForm
    {
        private string _strWhere;
        Business.bll健康体检 _BLL = new Business.bll健康体检();
        BackgroundWorker back = new BackgroundWorker();
        private DataRow row;

        public frm体检回执单()
        {
            InitializeComponent();
            back.RunWorkerCompleted += back_RunWorkerCompleted;
            back.DoWork += back_DoWork;
        }

        void back_DoWork(object sender, DoWorkEventArgs e)
        {
            DataTable dt = null;
            #region 查询条件

            _strWhere = string.Empty;
            string pgrid = this.treeListLookUpEdit机构.EditValue.ToString();


            if (this.txt姓名.Text.Trim() != "")
            {
                _strWhere += " and (姓名 like'" + this.txt姓名.Text.Trim() + "%') ";
            }
            if (this.txt档案编号.Text.Trim() != "")
            {
                _strWhere += " and 个人档案编号 ='" + this.txt档案编号.Text.Trim() + "'";
            }
            if (this.txt身份证号.Text.Trim() != "")
            {
                _strWhere += " and 身份证号 ='" + this.txt身份证号.Text.Trim() + "'";
            }
            if (util.ControlsHelper.GetComboxKey(comboBoxEdit村) != "")
            {
                _strWhere += " and 街道编码 ='" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'";
            }
            if (util.ControlsHelper.GetComboxKey(comboBoxEdit村) != "")
            {
                _strWhere += " and [居委会编码] ='" + util.ControlsHelper.GetComboxKey(comboBoxEdit村) + "'";
            }
            if (this.txt详细地址.Text.Trim() != "")
            {
                _strWhere += " and [居住地址] like '" + this.txt详细地址.Text.Trim() + "%'";
            }

            if (this.dte出生日期1.Text.Trim() != "")
            {
                _strWhere += " and [出生日期] >= '" + this.dte出生日期1.Text.Trim() + " 00:00:00'";
            }
            if (this.dte出生日期2.Text.Trim() != "")
            {
                _strWhere += " and [出生日期] <= '" + this.dte出生日期2.Text.Trim() + " 23:59:59'";
            }
            if (util.ControlsHelper.GetComboxKey(this.cbo性别) != "")
            {
                _strWhere += " and [性别] = '" + util.ControlsHelper.GetComboxKey(cbo性别) + "'";
            }
            if (util.ControlsHelper.GetComboxKey(this.cbo档案状态) != "")
            {
                _strWhere += " and [档案状态] = '" + util.ControlsHelper.GetComboxKey(this.cbo档案状态) + "'";
            }
            #endregion

            if (this.chk含下属机构.Checked)//包含下属机构
            {
                if (pgrid.Length == 12)
                {
                    _strWhere += " and ( [所属机构]=  '" + pgrid + "' or substring( [所属机构],1,7)+'1'+substring( [所属机构],9,7) like '" + pgrid + "%')";
                }
                else
                {
                    _strWhere += " and  [所属机构] like '" + pgrid + "%'";
                }
            }
            else
            {
                _strWhere += " and  [所属机构] ='" + pgrid + "'";
            }
            if (this.cbo录入人.Text.Trim() != "")
            {
                _strWhere += " and  [创建人] = '" + this.cbo录入人.EditValue.ToString() + "'";
            }
            if (this.cbo档案类别.Text.Trim() != "请选择")
            {
                _strWhere += " and  [档案类别] = '" + util.ControlsHelper.GetComboxKey(cbo档案类别) + "'";
            }
            if (this.dte录入时间1.Text.Trim() != "")
            {
                _strWhere += " and  [创建时间] >= '" + this.dte录入时间1.Text.Trim() + " 00:00:00'";
            }
            if (this.dte录入时间2.Text.Trim() != "")
            {
                _strWhere += " and  [创建时间] <= '" + this.dte录入时间2.Text.Trim() + " 23:59:59'";
            }
            if (this.dte体检时间1.Text.Trim() != "")
            {
                _strWhere += " and  [体检日期] >= '" + this.dte体检时间1.Text.Trim() + "'";
            }
            if (this.dte体检时间2.Text.Trim() != "")
            {
                _strWhere += " and  [体检日期] <= '" + this.dte体检时间2.Text.Trim() + "'";
            }
            if (util.ControlsHelper.GetComboxKey(this.cbo合格档案) != "")
            {
                if (util.ControlsHelper.GetComboxKey(this.cbo合格档案) == "1")//合格 ， 缺项为0
                {
                    _strWhere += " and  [缺少项] = '0'";
                }
                else
                {
                    _strWhere += " and  [缺少项] <> '0'";
                }
            }
            //状态
            if (this.chk是否贫困人口.Checked)
            {
                _strWhere += " and ISNULL(是否贫困人口,'')<>''  ";
            }
            if (this.chk是否签约服务.Checked)
            {
                _strWhere += " and ISNULL(是否签约,'')<>''  ";
            }

            this.pagerControl1.InitControl();

            dt = this.pagerControl1.GetQueryResultNew("vw_健康体检", " *", _strWhere, "CONVERT(DATETIME, 创建时间)", "DESC").Tables[0];

            //ds = this.pagerControl1.GetQueryResult("vw_健康体检", "*", _strWhere, "创建时间", "DESC");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //dt.Rows[i]["姓名"] = util.DESEncrypt.DES解密(dt.Rows[i]["姓名"].ToString());
                dt.Rows[i]["创建人"] = _BLL.Return用户名称(dt.Rows[i]["创建人"].ToString());
                dt.Rows[i]["性别"] = _BLL.ReturnDis字典显示("xb_xingbie", dt.Rows[i]["性别"].ToString());
                dt.Rows[i]["所属机构"] = _BLL.Return机构名称(dt.Rows[i]["所属机构"].ToString());
                dt.Rows[i]["居住地址"] = _BLL.Return地区名称(dt.Rows[i]["街道"].ToString()) + _BLL.Return地区名称(dt.Rows[i]["居委会"].ToString()) + dt.Rows[i]["居住地址"];
            }
            //this.pagerControl1.DrawControl();

            e.Result = dt;
        }

        void back_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            DataTable dt = e.Result as DataTable;

            this.gc健康体检.DataSource = dt;
            this.pagerControl1.DrawControl();
            this.gv健康体检.BestFitColumns();//列自适应宽度         
        }
        private void btn查询_Click(object sender, EventArgs e)
        {
            if (funCheck() && !back.IsBusy)
            {
                back.RunWorkerAsync();
            }
        }

        private void BindDataList()
        {
            DataTable dt = this.pagerControl1.GetQueryResultNew("vw_健康体检",
        " *",
        _strWhere, "CONVERT(DATETIME, 创建时间)", "DESC").Tables[0];

            //DataSet ds = this.pagerControl1.GetQueryResult("vw_健康体检", _strWhere);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //dt.Rows[i]["姓名"] = util.DESEncrypt.DES解密(dt.Rows[i]["姓名"].ToString());
                dt.Rows[i]["创建人"] = _BLL.Return用户名称(dt.Rows[i]["创建人"].ToString());
                dt.Rows[i]["性别"] = _BLL.ReturnDis字典显示("xb_xingbie", dt.Rows[i]["性别"].ToString());
                dt.Rows[i]["所属机构"] = _BLL.Return机构名称(dt.Rows[i]["所属机构"].ToString());
                dt.Rows[i]["居住地址"] = _BLL.Return地区名称(dt.Rows[i]["街道"].ToString()) + _BLL.Return地区名称(dt.Rows[i]["居委会"].ToString()) + dt.Rows[i]["居住地址"];
            }
            this.gc健康体检.DataSource = dt;
            this.pagerControl1.DrawControl();
            this.gv健康体检.BestFitColumns();//列自适应宽度         
        }
        private bool funCheck()
        {
            if (!string.IsNullOrEmpty(dte出生日期2.Text.Trim()) && !string.IsNullOrEmpty(dte出生日期1.Text.Trim()) && this.dte出生日期1.DateTime > this.dte出生日期2.DateTime)
            {
                Msg.Warning("出生日期的结束日期必须大于开始时间！");
                this.dte出生日期2.Text = "";
                this.dte出生日期2.Focus();
                return false;
            }
            if (!string.IsNullOrEmpty(dte录入时间1.Text.Trim()) && !string.IsNullOrEmpty(dte录入时间2.Text.Trim()) && this.dte录入时间1.DateTime > this.dte录入时间2.DateTime)
            {
                Msg.Warning("录入时间的结束日期必须大于开始时间！");
                this.dte录入时间2.Text = "";
                this.dte录入时间2.Focus();
                return false;
            }
            if (!string.IsNullOrEmpty(dte体检时间1.Text.Trim()) && !string.IsNullOrEmpty(dte体检时间2.Text.Trim()) && this.dte体检时间1.DateTime > this.dte体检时间2.DateTime)
            {
                Msg.Warning("体检时间的结束日期必须大于开始时间！");
                this.dte体检时间2.Text = "";
                this.dte体检时间2.Focus();
                return false;
            }
            return true;
        }
        private void BindData()
        {
            DataSet ds = this.pagerControl1.GetQueryResult("vw_健康体检", _strWhere);
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                //ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                //ds.Tables[0].Rows[i]["性别"] = _BLL.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                ds.Tables[0].Rows[i]["性别"] = _BLL.ReturnDis字典显示("zflx", ds.Tables[0].Rows[i]["性别"].ToString());
                ds.Tables[0].Rows[i]["居住地址"] = ds.Tables[0].Rows[i]["街道"].ToString() + ds.Tables[0].Rows[i]["居委会"].ToString() + ds.Tables[0].Rows[i]["居住地址"];
            }
            this.gc健康体检.DataSource = ds.Tables[0];
            this.pagerControl1.DrawControl();
            this.gv健康体检.BestFitColumns();//列自适应宽度         
        }
        private void link档案号_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv健康体检.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            string date = row["创建时间"] as string;
            //_BLL.GetBusinessByKey(个人档案编号, true);//下载一个空业务单据            
            //_BLL.NewBusiness(); //增加一条主表记录
            frm个人健康 frm = new frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, date);
            frm.Show();
        }
        private void frm健康体检表_Load(object sender, EventArgs e)
        {
            base.InitButtonsBase();
            InitView();
        }
        private void InitView()
        {
            this.page人次.PageVisible = true;
            this.tabControl.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;

            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, cbo性别, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案状态, cbo档案状态, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案类别, cbo档案类别, "P_CODE", "P_DESC");
            cbo档案状态.SelectedIndex = 1;
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t是否, cbo合格档案, "P_CODE", "P_DESC");
            //为“镇”绑定信息
            DataView dv镇 = new DataView(DataDictCache.Cache.t地区信息);
            dv镇.RowFilter = "上级编码=371323";
            //DataBinder.BindingLookupEditDataSource(lookUpEdit镇, dv市.ToTable(), "地区名称", "地区编码");
            util.ControlsHelper.BindComboxData(dv镇.ToTable(), comboBoxEdit镇, "地区编码", "地区名称");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t是否, cbo合格档案, "P_CODE", "P_DESC");
            //为“所属机构"绑定信息
            if (Loginer.CurrentUser.所属机构.Length >= 12)
                treeListLookUpEdit机构.Properties.AutoExpandAllNodes = true;
            else { treeListLookUpEdit机构.Properties.AutoExpandAllNodes = false; }
            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                treeListLookUpEdit机构.Properties.ValueMember = "机构编号";
                treeListLookUpEdit机构.Properties.DisplayMember = "机构名称";

                treeListLookUpEdit机构.Properties.TreeList.KeyFieldName = "机构编号";
                treeListLookUpEdit机构.Properties.TreeList.ParentFieldName = "上级机构";
                treeListLookUpEdit机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                treeListLookUpEdit机构.Properties.DataSource = dt所属机构;
                //treeListLookUpEdit机构.Properties.TreeList.ExpandAll();
                //treeListLookUpEdit机构.Properties.TreeList.CollapseAll();

                treeListLookUpEdit机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                treeListLookUpEdit机构.Text = Loginer.CurrentUser.所属机构;
            }

            try
            {
                DataTable dt录入人 = _bllUser.GetUserNameAndIDBy机构用户(treeListLookUpEdit机构.EditValue.ToString());
                cbo录入人.Properties.DisplayMember = "UserName";
                cbo录入人.Properties.ValueMember = "用户编码";
                cbo录入人.Properties.DataSource = dt录入人;
            }
            catch (Exception ex)
            {
                cbo录入人.Text = "请选择...";
            }
        }
        private void comboBoxEdit镇_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv村 = new DataView(DataDictCache.Cache.t地区信息);
                dv村.RowFilter = "上级编码='" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'";
                //DataBinder.BindingLookupEditDataSource(lookUpEdit村, dv县.ToTable(), "地区名称", "地区编码");
                util.ControlsHelper.BindComboxData(dv村.ToTable(), comboBoxEdit村, "地区编码", "地区名称");
                util.ControlsHelper.SetComboxNullData("", comboBoxEdit村, "请选择居/村委会");
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }
        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            BindDataList();
        }
        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv健康体检.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            //string date = row["创建时间"] as string;
            //_BLL.GetBusinessByKey(个人档案编号, true);//下载一个空业务单据            
            //_BLL.NewBusiness(); //增加一条主表记录
            frm个人健康 frm = new frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, "");
            frm.Show();
        }
     
        private void gc健康体检_Click(object sender, EventArgs e)
        {

        }

        private void btn打印_Click(object sender, EventArgs e)
        {
            //2017-07-04 10:52:38 yufh 修改：批量打印-城区医院提出
            if (this.gv健康体检.SelectedRowsCount == 1)
            {
                DataRow row = this.gv健康体检.GetFocusedDataRow();
                if (row == null) return;
                string docNo = row["个人档案编号"] as string;
                string date = row["体检日期"] as string;
                frm详细打印表单 frm = new frm详细打印表单(row);
                if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {

                }
            }
            else
            {
                //Msg.ShowInformation("每次只能打印一个人的回执单，请确认！");
                if(!Msg.AskQuestion("是否批量打印！"))
                    return;
                XtraReport report1 = new XtraReport();
                report1.CreateDocument();
                report1.PrintingSystem.ContinuousPageNumbering = true;
                for (int i = 0; i < gv健康体检.GetSelectedRows().Length;i++ )
                {
                    DataRowView dr = this.gv健康体检.GetRow(this.gv健康体检.GetSelectedRows()[i]) as DataRowView;
                    frm详细打印表单 frm = new frm详细打印表单(dr.Row,true);
                    if (frm.Rresult != null && frm.Rresult.Count>0)
                    {
                        XtraReport1 report = new XtraReport1(frm.Rresult);
                        report.CreateDocument();
                        report1.Pages.AddRange(report.Pages);
                    }
                }
                ReportPrintTool pt1 = new ReportPrintTool(report1);
                //pt1.PrintingSystem.StartPrint += new PrintDocumentEventHandler(PrintingSystem_StartPrint);
                //打印报表
                //pt1.PrintDialog(); //显示选择打印的页面
                pt1.ShowPreviewDialog();
            }
            
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            //MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm公共字典定义));
        }

        private void treeListLookUpEdit机构_EditValueChanged(object sender, EventArgs e)
        {
            DataTable dt录入人 = _bllUser.GetUserNameAndIDBy机构用户(treeListLookUpEdit机构.EditValue.ToString());
            cbo录入人.Properties.DisplayMember = "UserName";
            cbo录入人.Properties.ValueMember = "用户编码";
            cbo录入人.Properties.DataSource = dt录入人;
        }
    }
}
