﻿namespace AtomEHR.公共卫生.Module.健康档案.辖区健康档案
{
    partial class report体检1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.lbl饮酒种类3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl饮酒种类2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl放射物质 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl近一年内是否醉酒 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl饮酒种类其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl开始饮酒年龄 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl饮酒种类1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl粉尘 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl戒烟年龄 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl开始吸烟年龄 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl日饮酒量 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl戒酒年龄 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl是否戒酒 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl饮酒频率 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel坚持锻炼时间 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl日吸烟量 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel锻炼方式 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl饮食习惯1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl饮食习惯2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl饮食习惯3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl吸烟状况 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel体重 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel腰围 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel体质指数 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel老年人健康撞他自我评估 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel老年人生活自理能力自我评估 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel老年人认知功能分 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel老年人认知功能 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel老年人情感状态分 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel老年人情感状态 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel锻炼频率 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel每次锻炼时间 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel体检日期年 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel体检日期月 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel责任医生 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel体检日期日 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel体温 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel呼吸频率 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel脉率 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血压左侧1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血压左侧2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血压右侧1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血压右侧2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel身高 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel尿胴体 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel尿潜血 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel尿常规其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel空腹血糖1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel空腹血糖2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl饮酒种类4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel78 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel大便潜血 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel症状其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel宫体异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel附件异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel外阴 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl放射物质有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl职业病有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl从业时间 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl工种 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel查体其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血红蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel白细胞 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血小板 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血常规其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel尿蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel尿糖 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel移动性浊音有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel乳腺1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel乳腺2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel乳腺3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel乳腺其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel外阴异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel阴道异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel宫颈异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel心率 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel眼底异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel症状8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel症状9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel巩膜异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel症状10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel淋巴结其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel呼吸音异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl住院史原因2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl病案号1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl住院史入院时间2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl住院史出院时间2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl医疗机构名称2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl病案号2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel症状5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel症状4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel症状6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel症状7 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl住院史出院时间1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel缺齿4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel龋齿1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl医疗机构名称1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl住院史原因1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl住院史入院时间1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel症状3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel症状2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.lbl粉尘措施 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl粉尘有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl物理防护 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl物理因素 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl化学防护 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl化学物质 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl职业病其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl职业病其他防护 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl物理因素有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl化学物质有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel杂音有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel罗音其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel压痛有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel脾大有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel肝大有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel包块有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel肛门指诊其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel心律 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel杂音 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel压痛 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel包块 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel肝大 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel脾大 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel移动性浊音 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel下肢水肿 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel足背动脉 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel肛门指诊 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel乳腺4 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.BorderWidth = 0F;
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel32,
            this.xrLabel33,
            this.xrLabel34,
            this.xrLabel35,
            this.xrLabel36,
            this.xrLabel27,
            this.xrLabel28,
            this.xrLabel29,
            this.xrLabel30,
            this.xrLabel31,
            this.xrLabel22,
            this.xrLabel23,
            this.xrLabel24,
            this.xrLabel25,
            this.xrLabel26,
            this.xrLabel17,
            this.xrLabel18,
            this.xrLabel19,
            this.xrLabel20,
            this.xrLabel21,
            this.xrLabel12,
            this.xrLabel13,
            this.xrLabel14,
            this.xrLabel15,
            this.xrLabel16,
            this.xrLabel2,
            this.xrLabel3,
            this.xrLabel4,
            this.xrLabel5,
            this.xrLabel6,
            this.xrLabel7,
            this.xrLabel8,
            this.xrLabel9,
            this.xrLabel10,
            this.xrLabel11,
            this.lbl化学物质有无,
            this.lbl物理因素有无,
            this.lbl职业病其他,
            this.lbl职业病其他防护,
            this.lbl化学防护,
            this.lbl化学物质,
            this.lbl物理防护,
            this.lbl物理因素,
            this.xrLabel1,
            this.lbl粉尘有无,
            this.lbl粉尘措施,
            this.lbl饮酒种类3,
            this.lbl饮酒种类2,
            this.lbl放射物质,
            this.lbl近一年内是否醉酒,
            this.lbl饮酒种类其他,
            this.lbl开始饮酒年龄,
            this.lbl饮酒种类1,
            this.lbl粉尘,
            this.lbl戒烟年龄,
            this.lbl开始吸烟年龄,
            this.lbl日饮酒量,
            this.lbl戒酒年龄,
            this.lbl是否戒酒,
            this.lbl饮酒频率,
            this.xrLabel坚持锻炼时间,
            this.lbl日吸烟量,
            this.xrLabel锻炼方式,
            this.lbl饮食习惯1,
            this.lbl饮食习惯2,
            this.lbl饮食习惯3,
            this.lbl吸烟状况,
            this.xrLabel体重,
            this.xrLabel腰围,
            this.xrLabel体质指数,
            this.xrLabel老年人健康撞他自我评估,
            this.xrLabel老年人生活自理能力自我评估,
            this.xrLabel老年人认知功能分,
            this.xrLabel老年人认知功能,
            this.xrLabel老年人情感状态分,
            this.xrLabel老年人情感状态,
            this.xrLabel锻炼频率,
            this.xrLabel每次锻炼时间,
            this.xrLabel体检日期年,
            this.xrLabel体检日期月,
            this.xrLabel责任医生,
            this.xrLabel体检日期日,
            this.xrLabel体温,
            this.xrLabel呼吸频率,
            this.xrLabel脉率,
            this.xrLabel血压左侧1,
            this.xrLabel血压左侧2,
            this.xrLabel血压右侧1,
            this.xrLabel血压右侧2,
            this.xrLabel身高,
            this.xrLabel尿胴体,
            this.xrLabel尿潜血,
            this.xrLabel尿常规其他,
            this.xrLabel空腹血糖1,
            this.xrLabel空腹血糖2,
            this.lbl饮酒种类4,
            this.xrLabel78,
            this.xrLabel大便潜血,
            this.xrLabel姓名,
            this.xrLabel症状其他,
            this.xrLabel宫体异常,
            this.xrLabel附件异常,
            this.xrLabel外阴,
            this.lbl放射物质有无,
            this.lbl职业病有无,
            this.lbl从业时间,
            this.lbl工种,
            this.xrLabel查体其他,
            this.xrLabel血红蛋白,
            this.xrLabel白细胞,
            this.xrLabel血小板,
            this.xrLabel血常规其他,
            this.xrLabel尿蛋白,
            this.xrLabel尿糖,
            this.xrLabel压痛,
            this.xrLabel包块,
            this.xrLabel肝大,
            this.xrLabel脾大,
            this.xrLabel移动性浊音有,
            this.xrLabel移动性浊音,
            this.xrLabel下肢水肿,
            this.xrLabel足背动脉,
            this.xrLabel肛门指诊,
            this.xrLabel肛门指诊其他,
            this.xrLabel乳腺1,
            this.xrLabel乳腺2,
            this.xrLabel乳腺4,
            this.xrLabel乳腺3,
            this.xrLabel乳腺其他,
            this.xrLabel外阴异常,
            this.xrLabel阴道异常,
            this.xrLabel宫颈异常,
            this.xrLabel心率,
            this.xrLabel39,
            this.xrLabel罗音其他,
            this.xrLabel心律,
            this.xrLabel杂音,
            this.xrLabel杂音有,
            this.xrLabel压痛有,
            this.xrLabel包块有,
            this.xrLabel肝大有,
            this.xrLabel脾大有,
            this.xrLabel眼底异常,
            this.xrLabel症状8,
            this.xrLabel症状9,
            this.xrLabel巩膜异常,
            this.xrLabel症状10,
            this.xrLabel淋巴结其他,
            this.xrLabel呼吸音异常,
            this.lbl住院史原因2,
            this.lbl病案号1,
            this.lbl住院史入院时间2,
            this.lbl住院史出院时间2,
            this.lbl医疗机构名称2,
            this.lbl病案号2,
            this.xrLabel症状5,
            this.xrLabel症状4,
            this.xrLabel症状6,
            this.xrLabel症状7,
            this.lbl住院史出院时间1,
            this.xrLabel缺齿4,
            this.xrLabel龋齿1,
            this.lbl医疗机构名称1,
            this.lbl住院史原因1,
            this.lbl住院史入院时间1,
            this.xrLabel症状3,
            this.xrLabel症状2,
            this.xrPictureBox1});
            this.Detail.HeightF = 1429.5F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseBorderWidth = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl饮酒种类3
            // 
            this.lbl饮酒种类3.LocationFloat = new DevExpress.Utils.PointFloat(1530F, 840F);
            this.lbl饮酒种类3.Name = "lbl饮酒种类3";
            this.lbl饮酒种类3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl饮酒种类3.SizeF = new System.Drawing.SizeF(16.66663F, 13.625F);
            this.lbl饮酒种类3.Text = "1";
            // 
            // lbl饮酒种类2
            // 
            this.lbl饮酒种类2.LocationFloat = new DevExpress.Utils.PointFloat(1510F, 840F);
            this.lbl饮酒种类2.Name = "lbl饮酒种类2";
            this.lbl饮酒种类2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl饮酒种类2.SizeF = new System.Drawing.SizeF(15.625F, 13.625F);
            this.lbl饮酒种类2.Text = "1";
            // 
            // lbl放射物质
            // 
            this.lbl放射物质.LocationFloat = new DevExpress.Utils.PointFloat(1232.291F, 895F);
            this.lbl放射物质.Name = "lbl放射物质";
            this.lbl放射物质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl放射物质.SizeF = new System.Drawing.SizeF(107.2921F, 13.625F);
            this.lbl放射物质.Text = "放射物质";
            // 
            // lbl近一年内是否醉酒
            // 
            this.lbl近一年内是否醉酒.LocationFloat = new DevExpress.Utils.PointFloat(1560F, 823.9583F);
            this.lbl近一年内是否醉酒.Name = "lbl近一年内是否醉酒";
            this.lbl近一年内是否醉酒.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl近一年内是否醉酒.SizeF = new System.Drawing.SizeF(13.54163F, 13.625F);
            this.lbl近一年内是否醉酒.Text = "3";
            // 
            // lbl饮酒种类其他
            // 
            this.lbl饮酒种类其他.LocationFloat = new DevExpress.Utils.PointFloat(1441.917F, 839.9999F);
            this.lbl饮酒种类其他.Name = "lbl饮酒种类其他";
            this.lbl饮酒种类其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl饮酒种类其他.SizeF = new System.Drawing.SizeF(41.96106F, 13.62506F);
            this.lbl饮酒种类其他.Text = "其他";
            // 
            // lbl开始饮酒年龄
            // 
            this.lbl开始饮酒年龄.LocationFloat = new DevExpress.Utils.PointFloat(1194.792F, 823.9583F);
            this.lbl开始饮酒年龄.Name = "lbl开始饮酒年龄";
            this.lbl开始饮酒年龄.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl开始饮酒年龄.SizeF = new System.Drawing.SizeF(111.4583F, 13.62506F);
            this.lbl开始饮酒年龄.Text = "开始饮酒年龄";
            // 
            // lbl饮酒种类1
            // 
            this.lbl饮酒种类1.LocationFloat = new DevExpress.Utils.PointFloat(1490F, 840F);
            this.lbl饮酒种类1.Name = "lbl饮酒种类1";
            this.lbl饮酒种类1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl饮酒种类1.SizeF = new System.Drawing.SizeF(15.625F, 13.625F);
            this.lbl饮酒种类1.Text = "1";
            // 
            // lbl粉尘
            // 
            this.lbl粉尘.LocationFloat = new DevExpress.Utils.PointFloat(1210.416F, 880F);
            this.lbl粉尘.Name = "lbl粉尘";
            this.lbl粉尘.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl粉尘.SizeF = new System.Drawing.SizeF(79.16724F, 13.625F);
            this.lbl粉尘.Text = "粉尘";
            // 
            // lbl戒烟年龄
            // 
            this.lbl戒烟年龄.LocationFloat = new DevExpress.Utils.PointFloat(1441.917F, 735F);
            this.lbl戒烟年龄.Name = "lbl戒烟年龄";
            this.lbl戒烟年龄.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl戒烟年龄.SizeF = new System.Drawing.SizeF(117.7083F, 13.625F);
            this.lbl戒烟年龄.Text = "戒烟年龄";
            // 
            // lbl开始吸烟年龄
            // 
            this.lbl开始吸烟年龄.LocationFloat = new DevExpress.Utils.PointFloat(1210.416F, 735F);
            this.lbl开始吸烟年龄.Name = "lbl开始吸烟年龄";
            this.lbl开始吸烟年龄.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl开始吸烟年龄.SizeF = new System.Drawing.SizeF(107.2921F, 13.625F);
            this.lbl开始吸烟年龄.Text = "开始吸烟年龄";
            // 
            // lbl日饮酒量
            // 
            this.lbl日饮酒量.LocationFloat = new DevExpress.Utils.PointFloat(1223.958F, 772.9999F);
            this.lbl日饮酒量.Name = "lbl日饮酒量";
            this.lbl日饮酒量.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl日饮酒量.SizeF = new System.Drawing.SizeF(82.2915F, 13.62494F);
            this.lbl日饮酒量.Text = "日饮酒量";
            // 
            // lbl戒酒年龄
            // 
            this.lbl戒酒年龄.LocationFloat = new DevExpress.Utils.PointFloat(1425F, 805.2917F);
            this.lbl戒酒年龄.Name = "lbl戒酒年龄";
            this.lbl戒酒年龄.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl戒酒年龄.SizeF = new System.Drawing.SizeF(83.62671F, 13.625F);
            this.lbl戒酒年龄.Text = "戒酒年龄";
            // 
            // lbl是否戒酒
            // 
            this.lbl是否戒酒.LocationFloat = new DevExpress.Utils.PointFloat(1554.999F, 792.7083F);
            this.lbl是否戒酒.Name = "lbl是否戒酒";
            this.lbl是否戒酒.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl是否戒酒.SizeF = new System.Drawing.SizeF(11.45825F, 13.625F);
            this.lbl是否戒酒.Text = "4";
            // 
            // lbl饮酒频率
            // 
            this.lbl饮酒频率.LocationFloat = new DevExpress.Utils.PointFloat(1557.083F, 755F);
            this.lbl饮酒频率.Name = "lbl饮酒频率";
            this.lbl饮酒频率.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl饮酒频率.SizeF = new System.Drawing.SizeF(13.54163F, 13.625F);
            this.lbl饮酒频率.Text = "5";
            // 
            // xrLabel坚持锻炼时间
            // 
            this.xrLabel坚持锻炼时间.LocationFloat = new DevExpress.Utils.PointFloat(1491.96F, 630F);
            this.xrLabel坚持锻炼时间.Name = "xrLabel坚持锻炼时间";
            this.xrLabel坚持锻炼时间.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel坚持锻炼时间.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel坚持锻炼时间.Text = "20";
            // 
            // lbl日吸烟量
            // 
            this.lbl日吸烟量.LocationFloat = new DevExpress.Utils.PointFloat(1223.958F, 710F);
            this.lbl日吸烟量.Name = "lbl日吸烟量";
            this.lbl日吸烟量.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl日吸烟量.SizeF = new System.Drawing.SizeF(82.29163F, 13.625F);
            this.lbl日吸烟量.Text = "日吸烟量";
            // 
            // xrLabel锻炼方式
            // 
            this.xrLabel锻炼方式.LocationFloat = new DevExpress.Utils.PointFloat(1194.792F, 645F);
            this.xrLabel锻炼方式.Name = "xrLabel锻炼方式";
            this.xrLabel锻炼方式.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel锻炼方式.SizeF = new System.Drawing.SizeF(360.6666F, 24.04169F);
            this.xrLabel锻炼方式.StylePriority.UseTextAlignment = false;
            this.xrLabel锻炼方式.Text = "锻炼方式";
            this.xrLabel锻炼方式.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl饮食习惯1
            // 
            this.lbl饮食习惯1.LocationFloat = new DevExpress.Utils.PointFloat(1505F, 672F);
            this.lbl饮食习惯1.Name = "lbl饮食习惯1";
            this.lbl饮食习惯1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl饮食习惯1.SizeF = new System.Drawing.SizeF(14.58313F, 13.625F);
            this.lbl饮食习惯1.Text = "2";
            // 
            // lbl饮食习惯2
            // 
            this.lbl饮食习惯2.LocationFloat = new DevExpress.Utils.PointFloat(1535F, 672F);
            this.lbl饮食习惯2.Name = "lbl饮食习惯2";
            this.lbl饮食习惯2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl饮食习惯2.SizeF = new System.Drawing.SizeF(13.54163F, 13.625F);
            this.lbl饮食习惯2.Text = "3";
            // 
            // lbl饮食习惯3
            // 
            this.lbl饮食习惯3.LocationFloat = new DevExpress.Utils.PointFloat(1560F, 672F);
            this.lbl饮食习惯3.Name = "lbl饮食习惯3";
            this.lbl饮食习惯3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl饮食习惯3.SizeF = new System.Drawing.SizeF(11.45825F, 13.625F);
            this.lbl饮食习惯3.Text = "4";
            // 
            // lbl吸烟状况
            // 
            this.lbl吸烟状况.LocationFloat = new DevExpress.Utils.PointFloat(1557.083F, 695F);
            this.lbl吸烟状况.Name = "lbl吸烟状况";
            this.lbl吸烟状况.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl吸烟状况.SizeF = new System.Drawing.SizeF(13.54163F, 13.625F);
            this.lbl吸烟状况.Text = "5";
            // 
            // xrLabel体重
            // 
            this.xrLabel体重.LocationFloat = new DevExpress.Utils.PointFloat(1469.835F, 380F);
            this.xrLabel体重.Name = "xrLabel体重";
            this.xrLabel体重.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel体重.SizeF = new System.Drawing.SizeF(57.49927F, 13.625F);
            this.xrLabel体重.Text = "70";
            // 
            // xrLabel腰围
            // 
            this.xrLabel腰围.LocationFloat = new DevExpress.Utils.PointFloat(1164.627F, 400F);
            this.xrLabel腰围.Name = "xrLabel腰围";
            this.xrLabel腰围.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel腰围.SizeF = new System.Drawing.SizeF(44.79163F, 13.625F);
            this.xrLabel腰围.Text = "60";
            // 
            // xrLabel体质指数
            // 
            this.xrLabel体质指数.LocationFloat = new DevExpress.Utils.PointFloat(1447.917F, 400F);
            this.xrLabel体质指数.Name = "xrLabel体质指数";
            this.xrLabel体质指数.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel体质指数.SizeF = new System.Drawing.SizeF(61.75232F, 13.625F);
            this.xrLabel体质指数.Text = "23";
            // 
            // xrLabel老年人健康撞他自我评估
            // 
            this.xrLabel老年人健康撞他自我评估.LocationFloat = new DevExpress.Utils.PointFloat(1555F, 445F);
            this.xrLabel老年人健康撞他自我评估.Name = "xrLabel老年人健康撞他自我评估";
            this.xrLabel老年人健康撞他自我评估.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel老年人健康撞他自我评估.SizeF = new System.Drawing.SizeF(16.6665F, 13.625F);
            this.xrLabel老年人健康撞他自我评估.Text = "1";
            // 
            // xrLabel老年人生活自理能力自我评估
            // 
            this.xrLabel老年人生活自理能力自我评估.LocationFloat = new DevExpress.Utils.PointFloat(1555F, 490F);
            this.xrLabel老年人生活自理能力自我评估.Name = "xrLabel老年人生活自理能力自我评估";
            this.xrLabel老年人生活自理能力自我评估.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel老年人生活自理能力自我评估.SizeF = new System.Drawing.SizeF(16.66663F, 13.625F);
            this.xrLabel老年人生活自理能力自我评估.Text = "2";
            // 
            // xrLabel老年人认知功能分
            // 
            this.xrLabel老年人认知功能分.LocationFloat = new DevExpress.Utils.PointFloat(1358.334F, 550F);
            this.xrLabel老年人认知功能分.Name = "xrLabel老年人认知功能分";
            this.xrLabel老年人认知功能分.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel老年人认知功能分.SizeF = new System.Drawing.SizeF(55.20837F, 13.625F);
            this.xrLabel老年人认知功能分.Text = "3";
            // 
            // xrLabel老年人认知功能
            // 
            this.xrLabel老年人认知功能.LocationFloat = new DevExpress.Utils.PointFloat(1555F, 540F);
            this.xrLabel老年人认知功能.Name = "xrLabel老年人认知功能";
            this.xrLabel老年人认知功能.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel老年人认知功能.SizeF = new System.Drawing.SizeF(16.66626F, 13.625F);
            this.xrLabel老年人认知功能.Text = "4";
            // 
            // xrLabel老年人情感状态分
            // 
            this.xrLabel老年人情感状态分.LocationFloat = new DevExpress.Utils.PointFloat(1380.208F, 585F);
            this.xrLabel老年人情感状态分.Name = "xrLabel老年人情感状态分";
            this.xrLabel老年人情感状态分.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel老年人情感状态分.SizeF = new System.Drawing.SizeF(51.0415F, 13.625F);
            this.xrLabel老年人情感状态分.Text = "5";
            // 
            // xrLabel老年人情感状态
            // 
            this.xrLabel老年人情感状态.LocationFloat = new DevExpress.Utils.PointFloat(1554.999F, 570F);
            this.xrLabel老年人情感状态.Name = "xrLabel老年人情感状态";
            this.xrLabel老年人情感状态.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel老年人情感状态.SizeF = new System.Drawing.SizeF(16.66687F, 13.625F);
            this.xrLabel老年人情感状态.Text = "6";
            // 
            // xrLabel锻炼频率
            // 
            this.xrLabel锻炼频率.LocationFloat = new DevExpress.Utils.PointFloat(1554.999F, 610F);
            this.xrLabel锻炼频率.Name = "xrLabel锻炼频率";
            this.xrLabel锻炼频率.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel锻炼频率.SizeF = new System.Drawing.SizeF(15.62561F, 13.625F);
            this.xrLabel锻炼频率.Text = "7";
            // 
            // xrLabel每次锻炼时间
            // 
            this.xrLabel每次锻炼时间.LocationFloat = new DevExpress.Utils.PointFloat(1194.792F, 630F);
            this.xrLabel每次锻炼时间.Name = "xrLabel每次锻炼时间";
            this.xrLabel每次锻炼时间.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel每次锻炼时间.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel每次锻炼时间.Text = "60";
            // 
            // xrLabel体检日期年
            // 
            this.xrLabel体检日期年.LocationFloat = new DevExpress.Utils.PointFloat(1020.833F, 180.83F);
            this.xrLabel体检日期年.Name = "xrLabel体检日期年";
            this.xrLabel体检日期年.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel体检日期年.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel体检日期年.Text = "2016";
            // 
            // xrLabel体检日期月
            // 
            this.xrLabel体检日期月.LocationFloat = new DevExpress.Utils.PointFloat(1086.458F, 180.83F);
            this.xrLabel体检日期月.Name = "xrLabel体检日期月";
            this.xrLabel体检日期月.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel体检日期月.SizeF = new System.Drawing.SizeF(32.29163F, 13.625F);
            this.xrLabel体检日期月.Text = "3";
            // 
            // xrLabel责任医生
            // 
            this.xrLabel责任医生.LocationFloat = new DevExpress.Utils.PointFloat(1381.25F, 180.83F);
            this.xrLabel责任医生.Name = "xrLabel责任医生";
            this.xrLabel责任医生.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel责任医生.SizeF = new System.Drawing.SizeF(122.045F, 20F);
            this.xrLabel责任医生.StylePriority.UseTextAlignment = false;
            this.xrLabel责任医生.Text = "责任医生";
            this.xrLabel责任医生.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel体检日期日
            // 
            this.xrLabel体检日期日.LocationFloat = new DevExpress.Utils.PointFloat(1144.792F, 180.83F);
            this.xrLabel体检日期日.Name = "xrLabel体检日期日";
            this.xrLabel体检日期日.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel体检日期日.SizeF = new System.Drawing.SizeF(34.37488F, 13.625F);
            this.xrLabel体检日期日.Text = "1";
            // 
            // xrLabel体温
            // 
            this.xrLabel体温.LocationFloat = new DevExpress.Utils.PointFloat(1134.375F, 320F);
            this.xrLabel体温.Name = "xrLabel体温";
            this.xrLabel体温.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel体温.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel体温.Text = "28";
            // 
            // xrLabel呼吸频率
            // 
            this.xrLabel呼吸频率.LocationFloat = new DevExpress.Utils.PointFloat(1114.627F, 350.4166F);
            this.xrLabel呼吸频率.Name = "xrLabel呼吸频率";
            this.xrLabel呼吸频率.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel呼吸频率.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel呼吸频率.Text = "40";
            // 
            // xrLabel脉率
            // 
            this.xrLabel脉率.LocationFloat = new DevExpress.Utils.PointFloat(1446.875F, 320F);
            this.xrLabel脉率.Name = "xrLabel脉率";
            this.xrLabel脉率.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel脉率.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel脉率.Text = "39";
            // 
            // xrLabel血压左侧1
            // 
            this.xrLabel血压左侧1.LocationFloat = new DevExpress.Utils.PointFloat(1414.627F, 340F);
            this.xrLabel血压左侧1.Name = "xrLabel血压左侧1";
            this.xrLabel血压左侧1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血压左侧1.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel血压左侧1.Text = "120";
            // 
            // xrLabel血压左侧2
            // 
            this.xrLabel血压左侧2.LocationFloat = new DevExpress.Utils.PointFloat(1473.958F, 340F);
            this.xrLabel血压左侧2.Name = "xrLabel血压左侧2";
            this.xrLabel血压左侧2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血压左侧2.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel血压左侧2.Text = "90";
            // 
            // xrLabel血压右侧1
            // 
            this.xrLabel血压右侧1.LocationFloat = new DevExpress.Utils.PointFloat(1414.627F, 365F);
            this.xrLabel血压右侧1.Name = "xrLabel血压右侧1";
            this.xrLabel血压右侧1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血压右侧1.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel血压右侧1.Text = "130";
            // 
            // xrLabel血压右侧2
            // 
            this.xrLabel血压右侧2.LocationFloat = new DevExpress.Utils.PointFloat(1473.958F, 365F);
            this.xrLabel血压右侧2.Name = "xrLabel血压右侧2";
            this.xrLabel血压右侧2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血压右侧2.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel血压右侧2.Text = "100";
            // 
            // xrLabel身高
            // 
            this.xrLabel身高.LocationFloat = new DevExpress.Utils.PointFloat(1164.627F, 380F);
            this.xrLabel身高.Name = "xrLabel身高";
            this.xrLabel身高.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel身高.SizeF = new System.Drawing.SizeF(44.79166F, 13.625F);
            this.xrLabel身高.Text = "180";
            // 
            // xrLabel尿胴体
            // 
            this.xrLabel尿胴体.LocationFloat = new DevExpress.Utils.PointFloat(554.1666F, 860.4999F);
            this.xrLabel尿胴体.Name = "xrLabel尿胴体";
            this.xrLabel尿胴体.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel尿胴体.SizeF = new System.Drawing.SizeF(67.29187F, 13.625F);
            this.xrLabel尿胴体.Text = "尿胴体";
            // 
            // xrLabel尿潜血
            // 
            this.xrLabel尿潜血.LocationFloat = new DevExpress.Utils.PointFloat(677.0833F, 860.4999F);
            this.xrLabel尿潜血.Name = "xrLabel尿潜血";
            this.xrLabel尿潜血.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel尿潜血.SizeF = new System.Drawing.SizeF(64.5835F, 13.625F);
            this.xrLabel尿潜血.Text = "尿潜血";
            // 
            // xrLabel尿常规其他
            // 
            this.xrLabel尿常规其他.LocationFloat = new DevExpress.Utils.PointFloat(311.4583F, 884.4579F);
            this.xrLabel尿常规其他.Name = "xrLabel尿常规其他";
            this.xrLabel尿常规其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel尿常规其他.SizeF = new System.Drawing.SizeF(224.9999F, 13.625F);
            this.xrLabel尿常规其他.Text = "尿常规其他";
            // 
            // xrLabel空腹血糖1
            // 
            this.xrLabel空腹血糖1.LocationFloat = new DevExpress.Utils.PointFloat(279.7914F, 903.125F);
            this.xrLabel空腹血糖1.Name = "xrLabel空腹血糖1";
            this.xrLabel空腹血糖1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel空腹血糖1.SizeF = new System.Drawing.SizeF(87.5F, 13.625F);
            this.xrLabel空腹血糖1.Text = "空腹血糖1";
            // 
            // xrLabel空腹血糖2
            // 
            this.xrLabel空腹血糖2.LocationFloat = new DevExpress.Utils.PointFloat(501.4583F, 903.125F);
            this.xrLabel空腹血糖2.Name = "xrLabel空腹血糖2";
            this.xrLabel空腹血糖2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel空腹血糖2.SizeF = new System.Drawing.SizeF(90.62476F, 13.625F);
            this.xrLabel空腹血糖2.Text = "空腹血糖2";
            // 
            // lbl饮酒种类4
            // 
            this.lbl饮酒种类4.LocationFloat = new DevExpress.Utils.PointFloat(1553.958F, 840F);
            this.lbl饮酒种类4.Name = "lbl饮酒种类4";
            this.lbl饮酒种类4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl饮酒种类4.SizeF = new System.Drawing.SizeF(16.66663F, 13.625F);
            this.lbl饮酒种类4.Text = "1";
            // 
            // xrLabel78
            // 
            this.xrLabel78.LocationFloat = new DevExpress.Utils.PointFloat(377.7082F, 931.25F);
            this.xrLabel78.Name = "xrLabel78";
            this.xrLabel78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel78.SizeF = new System.Drawing.SizeF(227.0834F, 13.625F);
            this.xrLabel78.Text = "心电图异常";
            // 
            // xrLabel大便潜血
            // 
            this.xrLabel大便潜血.LocationFloat = new DevExpress.Utils.PointFloat(1322.916F, 303.75F);
            this.xrLabel大便潜血.Name = "xrLabel大便潜血";
            this.xrLabel大便潜血.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel大便潜血.SizeF = new System.Drawing.SizeF(16.66663F, 13.625F);
            this.xrLabel大便潜血.Text = "1";
            // 
            // xrLabel姓名
            // 
            this.xrLabel姓名.LocationFloat = new DevExpress.Utils.PointFloat(963.5417F, 154.455F);
            this.xrLabel姓名.Name = "xrLabel姓名";
            this.xrLabel姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel姓名.SizeF = new System.Drawing.SizeF(84.375F, 20F);
            this.xrLabel姓名.StylePriority.UseTextAlignment = false;
            this.xrLabel姓名.Text = "姓名";
            this.xrLabel姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel姓名.WordWrap = false;
            // 
            // xrLabel症状其他
            // 
            this.xrLabel症状其他.LocationFloat = new DevExpress.Utils.PointFloat(1445.833F, 277.9183F);
            this.xrLabel症状其他.Name = "xrLabel症状其他";
            this.xrLabel症状其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel症状其他.SizeF = new System.Drawing.SizeF(109.6252F, 20F);
            this.xrLabel症状其他.StylePriority.UseTextAlignment = false;
            this.xrLabel症状其他.Text = "体检其他";
            this.xrLabel症状其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel宫体异常
            // 
            this.xrLabel宫体异常.LocationFloat = new DevExpress.Utils.PointFloat(417.7084F, 730F);
            this.xrLabel宫体异常.Name = "xrLabel宫体异常";
            this.xrLabel宫体异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel宫体异常.SizeF = new System.Drawing.SizeF(186.4584F, 13.625F);
            this.xrLabel宫体异常.Text = "宫体异常";
            // 
            // xrLabel附件异常
            // 
            this.xrLabel附件异常.LocationFloat = new DevExpress.Utils.PointFloat(417.7084F, 755F);
            this.xrLabel附件异常.Name = "xrLabel附件异常";
            this.xrLabel附件异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel附件异常.SizeF = new System.Drawing.SizeF(186.4584F, 13.625F);
            this.xrLabel附件异常.Text = "附件异常";
            // 
            // xrLabel外阴
            // 
            this.xrLabel外阴.LocationFloat = new DevExpress.Utils.PointFloat(745.9597F, 660F);
            this.xrLabel外阴.Name = "xrLabel外阴";
            this.xrLabel外阴.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel外阴.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel外阴.Text = "1";
            // 
            // lbl放射物质有无
            // 
            this.lbl放射物质有无.LocationFloat = new DevExpress.Utils.PointFloat(1556.875F, 905F);
            this.lbl放射物质有无.Name = "lbl放射物质有无";
            this.lbl放射物质有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl放射物质有无.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.lbl放射物质有无.Text = "1";
            // 
            // lbl职业病有无
            // 
            this.lbl职业病有无.LocationFloat = new DevExpress.Utils.PointFloat(1556.875F, 860.4998F);
            this.lbl职业病有无.Name = "lbl职业病有无";
            this.lbl职业病有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl职业病有无.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.lbl职业病有无.Text = "1";
            // 
            // lbl从业时间
            // 
            this.lbl从业时间.LocationFloat = new DevExpress.Utils.PointFloat(1322.916F, 860.4999F);
            this.lbl从业时间.Name = "lbl从业时间";
            this.lbl从业时间.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl从业时间.SizeF = new System.Drawing.SizeF(38.5415F, 13.625F);
            this.lbl从业时间.Text = "1";
            // 
            // lbl工种
            // 
            this.lbl工种.LocationFloat = new DevExpress.Utils.PointFloat(1192.752F, 860.4998F);
            this.lbl工种.Name = "lbl工种";
            this.lbl工种.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl工种.SizeF = new System.Drawing.SizeF(46.8313F, 13.625F);
            this.lbl工种.Text = "工种";
            // 
            // xrLabel查体其他
            // 
            this.xrLabel查体其他.LocationFloat = new DevExpress.Utils.PointFloat(279.1666F, 775F);
            this.xrLabel查体其他.Name = "xrLabel查体其他";
            this.xrLabel查体其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel查体其他.SizeF = new System.Drawing.SizeF(370.8333F, 31.33331F);
            this.xrLabel查体其他.Text = "查体其他";
            // 
            // xrLabel血红蛋白
            // 
            this.xrLabel血红蛋白.LocationFloat = new DevExpress.Utils.PointFloat(339.5833F, 823.9583F);
            this.xrLabel血红蛋白.Name = "xrLabel血红蛋白";
            this.xrLabel血红蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血红蛋白.SizeF = new System.Drawing.SizeF(64.58334F, 13.625F);
            this.xrLabel血红蛋白.Text = "血红蛋白";
            // 
            // xrLabel白细胞
            // 
            this.xrLabel白细胞.LocationFloat = new DevExpress.Utils.PointFloat(501.4583F, 823.9583F);
            this.xrLabel白细胞.Name = "xrLabel白细胞";
            this.xrLabel白细胞.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel白细胞.SizeF = new System.Drawing.SizeF(75.00009F, 13.625F);
            this.xrLabel白细胞.Text = "白细胞";
            // 
            // xrLabel血小板
            // 
            this.xrLabel血小板.LocationFloat = new DevExpress.Utils.PointFloat(661.4583F, 823.9583F);
            this.xrLabel血小板.Name = "xrLabel血小板";
            this.xrLabel血小板.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血小板.SizeF = new System.Drawing.SizeF(75.20837F, 13.625F);
            this.xrLabel血小板.Text = "血小板";
            // 
            // xrLabel血常规其他
            // 
            this.xrLabel血常规其他.LocationFloat = new DevExpress.Utils.PointFloat(306.2499F, 840F);
            this.xrLabel血常规其他.Name = "xrLabel血常规其他";
            this.xrLabel血常规其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血常规其他.SizeF = new System.Drawing.SizeF(246.8751F, 13.625F);
            this.xrLabel血常规其他.Text = "血常规其他";
            // 
            // xrLabel尿蛋白
            // 
            this.xrLabel尿蛋白.LocationFloat = new DevExpress.Utils.PointFloat(329.1666F, 860.4999F);
            this.xrLabel尿蛋白.Name = "xrLabel尿蛋白";
            this.xrLabel尿蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel尿蛋白.SizeF = new System.Drawing.SizeF(55.2084F, 13.625F);
            this.xrLabel尿蛋白.Text = "尿蛋白";
            // 
            // xrLabel尿糖
            // 
            this.xrLabel尿糖.LocationFloat = new DevExpress.Utils.PointFloat(431.2499F, 860.4999F);
            this.xrLabel尿糖.Name = "xrLabel尿糖";
            this.xrLabel尿糖.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel尿糖.SizeF = new System.Drawing.SizeF(76.04166F, 13.625F);
            this.xrLabel尿糖.Text = "尿糖";
            // 
            // xrLabel移动性浊音有
            // 
            this.xrLabel移动性浊音有.LocationFloat = new DevExpress.Utils.PointFloat(440F, 560F);
            this.xrLabel移动性浊音有.Name = "xrLabel移动性浊音有";
            this.xrLabel移动性浊音有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel移动性浊音有.SizeF = new System.Drawing.SizeF(96.45847F, 13.625F);
            this.xrLabel移动性浊音有.StylePriority.UseTextAlignment = false;
            this.xrLabel移动性浊音有.Text = "移动性浊音";
            // 
            // xrLabel乳腺1
            // 
            this.xrLabel乳腺1.LocationFloat = new DevExpress.Utils.PointFloat(675F, 644.7917F);
            this.xrLabel乳腺1.Name = "xrLabel乳腺1";
            this.xrLabel乳腺1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel乳腺1.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel乳腺1.Text = "1";
            // 
            // xrLabel乳腺2
            // 
            this.xrLabel乳腺2.LocationFloat = new DevExpress.Utils.PointFloat(698.125F, 644.7917F);
            this.xrLabel乳腺2.Name = "xrLabel乳腺2";
            this.xrLabel乳腺2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel乳腺2.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel乳腺2.Text = "1";
            // 
            // xrLabel乳腺3
            // 
            this.xrLabel乳腺3.LocationFloat = new DevExpress.Utils.PointFloat(723.125F, 644.7917F);
            this.xrLabel乳腺3.Name = "xrLabel乳腺3";
            this.xrLabel乳腺3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel乳腺3.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel乳腺3.Text = "1";
            // 
            // xrLabel乳腺其他
            // 
            this.xrLabel乳腺其他.LocationFloat = new DevExpress.Utils.PointFloat(635.4167F, 644.7917F);
            this.xrLabel乳腺其他.Name = "xrLabel乳腺其他";
            this.xrLabel乳腺其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel乳腺其他.SizeF = new System.Drawing.SizeF(39.58331F, 13.625F);
            this.xrLabel乳腺其他.Text = "乳腺其他";
            this.xrLabel乳腺其他.WordWrap = false;
            // 
            // xrLabel外阴异常
            // 
            this.xrLabel外阴异常.LocationFloat = new DevExpress.Utils.PointFloat(417.7084F, 660F);
            this.xrLabel外阴异常.Name = "xrLabel外阴异常";
            this.xrLabel外阴异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel外阴异常.SizeF = new System.Drawing.SizeF(186.4584F, 13.625F);
            this.xrLabel外阴异常.Text = "外阴异常";
            // 
            // xrLabel阴道异常
            // 
            this.xrLabel阴道异常.LocationFloat = new DevExpress.Utils.PointFloat(417.7083F, 683F);
            this.xrLabel阴道异常.Name = "xrLabel阴道异常";
            this.xrLabel阴道异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel阴道异常.SizeF = new System.Drawing.SizeF(186.4584F, 13.625F);
            this.xrLabel阴道异常.Text = "阴道异常";
            // 
            // xrLabel宫颈异常
            // 
            this.xrLabel宫颈异常.LocationFloat = new DevExpress.Utils.PointFloat(417.7084F, 708F);
            this.xrLabel宫颈异常.Name = "xrLabel宫颈异常";
            this.xrLabel宫颈异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel宫颈异常.SizeF = new System.Drawing.SizeF(186.4584F, 13.625F);
            this.xrLabel宫颈异常.Text = "宫颈异常";
            // 
            // xrLabel心率
            // 
            this.xrLabel心率.LocationFloat = new DevExpress.Utils.PointFloat(279.7914F, 570F);
            this.xrLabel心率.Name = "xrLabel心率";
            this.xrLabel心率.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel心率.SizeF = new System.Drawing.SizeF(44.79172F, 13.625F);
            this.xrLabel心率.StylePriority.UseTextAlignment = false;
            this.xrLabel心率.Text = "心率";
            // 
            // xrLabel眼底异常
            // 
            this.xrLabel眼底异常.LocationFloat = new DevExpress.Utils.PointFloat(263.5417F, 329.5832F);
            this.xrLabel眼底异常.Name = "xrLabel眼底异常";
            this.xrLabel眼底异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel眼底异常.SizeF = new System.Drawing.SizeF(120.8334F, 24.04169F);
            this.xrLabel眼底异常.StylePriority.UseTextAlignment = false;
            this.xrLabel眼底异常.Text = "1";
            this.xrLabel眼底异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel症状8
            // 
            this.xrLabel症状8.LocationFloat = new DevExpress.Utils.PointFloat(1491.96F, 303.7501F);
            this.xrLabel症状8.Name = "xrLabel症状8";
            this.xrLabel症状8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel症状8.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel症状8.Text = "1";
            // 
            // xrLabel症状9
            // 
            this.xrLabel症状9.LocationFloat = new DevExpress.Utils.PointFloat(1518.958F, 303.7501F);
            this.xrLabel症状9.Name = "xrLabel症状9";
            this.xrLabel症状9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel症状9.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel症状9.Text = "1";
            // 
            // xrLabel巩膜异常
            // 
            this.xrLabel巩膜异常.LocationFloat = new DevExpress.Utils.PointFloat(473.5417F, 329.5834F);
            this.xrLabel巩膜异常.Name = "xrLabel巩膜异常";
            this.xrLabel巩膜异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel巩膜异常.SizeF = new System.Drawing.SizeF(79.58334F, 24.04163F);
            this.xrLabel巩膜异常.StylePriority.UseTextAlignment = false;
            this.xrLabel巩膜异常.Text = "巩膜";
            this.xrLabel巩膜异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel症状10
            // 
            this.xrLabel症状10.LocationFloat = new DevExpress.Utils.PointFloat(1542.958F, 303.7501F);
            this.xrLabel症状10.Name = "xrLabel症状10";
            this.xrLabel症状10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel症状10.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel症状10.Text = "1";
            // 
            // xrLabel淋巴结其他
            // 
            this.xrLabel淋巴结其他.LocationFloat = new DevExpress.Utils.PointFloat(599.7917F, 329.5834F);
            this.xrLabel淋巴结其他.Name = "xrLabel淋巴结其他";
            this.xrLabel淋巴结其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel淋巴结其他.SizeF = new System.Drawing.SizeF(115F, 24.04166F);
            this.xrLabel淋巴结其他.StylePriority.UseTextAlignment = false;
            this.xrLabel淋巴结其他.Text = "1";
            this.xrLabel淋巴结其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel呼吸音异常
            // 
            this.xrLabel呼吸音异常.LocationFloat = new DevExpress.Utils.PointFloat(141.6667F, 570F);
            this.xrLabel呼吸音异常.Name = "xrLabel呼吸音异常";
            this.xrLabel呼吸音异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel呼吸音异常.SizeF = new System.Drawing.SizeF(91.66669F, 13.625F);
            this.xrLabel呼吸音异常.StylePriority.UseTextAlignment = false;
            this.xrLabel呼吸音异常.Text = "呼吸音";
            // 
            // lbl住院史原因2
            // 
            this.lbl住院史原因2.LocationFloat = new DevExpress.Utils.PointFloat(440F, 180.83F);
            this.lbl住院史原因2.Name = "lbl住院史原因2";
            this.lbl住院史原因2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl住院史原因2.SizeF = new System.Drawing.SizeF(79.79166F, 13.62498F);
            this.lbl住院史原因2.Text = "1";
            // 
            // lbl病案号1
            // 
            this.lbl病案号1.LocationFloat = new DevExpress.Utils.PointFloat(658.3333F, 154.455F);
            this.lbl病案号1.Name = "lbl病案号1";
            this.lbl病案号1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl病案号1.SizeF = new System.Drawing.SizeF(71.66669F, 13.62498F);
            this.lbl病案号1.Text = "1";
            // 
            // lbl住院史入院时间2
            // 
            this.lbl住院史入院时间2.LocationFloat = new DevExpress.Utils.PointFloat(263.5416F, 180.83F);
            this.lbl住院史入院时间2.Name = "lbl住院史入院时间2";
            this.lbl住院史入院时间2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.lbl住院史入院时间2.SizeF = new System.Drawing.SizeF(70.83337F, 13.62498F);
            this.lbl住院史入院时间2.StylePriority.UsePadding = false;
            this.lbl住院史入院时间2.Text = "1";
            // 
            // lbl住院史出院时间2
            // 
            this.lbl住院史出院时间2.LocationFloat = new DevExpress.Utils.PointFloat(350.6248F, 180.83F);
            this.lbl住院史出院时间2.Name = "lbl住院史出院时间2";
            this.lbl住院史出院时间2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl住院史出院时间2.SizeF = new System.Drawing.SizeF(73.95837F, 13.62498F);
            this.lbl住院史出院时间2.Text = "1";
            // 
            // lbl医疗机构名称2
            // 
            this.lbl医疗机构名称2.LocationFloat = new DevExpress.Utils.PointFloat(539.375F, 180.83F);
            this.lbl医疗机构名称2.Name = "lbl医疗机构名称2";
            this.lbl医疗机构名称2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl医疗机构名称2.SizeF = new System.Drawing.SizeF(98.12512F, 13.62498F);
            this.lbl医疗机构名称2.Text = "1";
            // 
            // lbl病案号2
            // 
            this.lbl病案号2.LocationFloat = new DevExpress.Utils.PointFloat(658.3333F, 180.83F);
            this.lbl病案号2.Name = "lbl病案号2";
            this.lbl病案号2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl病案号2.SizeF = new System.Drawing.SizeF(71.66669F, 13.62498F);
            this.lbl病案号2.Text = "1";
            // 
            // xrLabel症状5
            // 
            this.xrLabel症状5.LocationFloat = new DevExpress.Utils.PointFloat(1425F, 303.7501F);
            this.xrLabel症状5.Name = "xrLabel症状5";
            this.xrLabel症状5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel症状5.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel症状5.Text = "1";
            // 
            // xrLabel症状4
            // 
            this.xrLabel症状4.LocationFloat = new DevExpress.Utils.PointFloat(1396.875F, 303.7501F);
            this.xrLabel症状4.Name = "xrLabel症状4";
            this.xrLabel症状4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel症状4.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel症状4.Text = "1";
            // 
            // xrLabel症状6
            // 
            this.xrLabel症状6.LocationFloat = new DevExpress.Utils.PointFloat(1446.875F, 303.7501F);
            this.xrLabel症状6.Name = "xrLabel症状6";
            this.xrLabel症状6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel症状6.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel症状6.Text = "1";
            // 
            // xrLabel症状7
            // 
            this.xrLabel症状7.LocationFloat = new DevExpress.Utils.PointFloat(1473.958F, 303.75F);
            this.xrLabel症状7.Name = "xrLabel症状7";
            this.xrLabel症状7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel症状7.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel症状7.Text = "1";
            // 
            // lbl住院史出院时间1
            // 
            this.lbl住院史出院时间1.LocationFloat = new DevExpress.Utils.PointFloat(350.6248F, 154.455F);
            this.lbl住院史出院时间1.Name = "lbl住院史出院时间1";
            this.lbl住院史出院时间1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.lbl住院史出院时间1.SizeF = new System.Drawing.SizeF(73.95837F, 13.62498F);
            this.lbl住院史出院时间1.StylePriority.UsePadding = false;
            this.lbl住院史出院时间1.Text = "1";
            // 
            // xrLabel缺齿4
            // 
            this.xrLabel缺齿4.LocationFloat = new DevExpress.Utils.PointFloat(159.3751F, 329.5833F);
            this.xrLabel缺齿4.Name = "xrLabel缺齿4";
            this.xrLabel缺齿4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel缺齿4.SizeF = new System.Drawing.SizeF(73.95834F, 24.0416F);
            this.xrLabel缺齿4.StylePriority.UseTextAlignment = false;
            this.xrLabel缺齿4.Text = "1";
            this.xrLabel缺齿4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel龋齿1
            // 
            this.xrLabel龋齿1.LocationFloat = new DevExpress.Utils.PointFloat(407.9165F, 329.5833F);
            this.xrLabel龋齿1.Name = "xrLabel龋齿1";
            this.xrLabel龋齿1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel龋齿1.SizeF = new System.Drawing.SizeF(52.08334F, 24.04169F);
            this.xrLabel龋齿1.StylePriority.UseTextAlignment = false;
            this.xrLabel龋齿1.Text = "1";
            this.xrLabel龋齿1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl医疗机构名称1
            // 
            this.lbl医疗机构名称1.LocationFloat = new DevExpress.Utils.PointFloat(539.375F, 154.455F);
            this.lbl医疗机构名称1.Name = "lbl医疗机构名称1";
            this.lbl医疗机构名称1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl医疗机构名称1.SizeF = new System.Drawing.SizeF(98.12512F, 13.62498F);
            this.lbl医疗机构名称1.Text = "1";
            // 
            // lbl住院史原因1
            // 
            this.lbl住院史原因1.LocationFloat = new DevExpress.Utils.PointFloat(440F, 154.455F);
            this.lbl住院史原因1.Name = "lbl住院史原因1";
            this.lbl住院史原因1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl住院史原因1.SizeF = new System.Drawing.SizeF(79.79166F, 13.62498F);
            this.lbl住院史原因1.Text = "1";
            // 
            // lbl住院史入院时间1
            // 
            this.lbl住院史入院时间1.LocationFloat = new DevExpress.Utils.PointFloat(263.5416F, 154.455F);
            this.lbl住院史入院时间1.Name = "lbl住院史入院时间1";
            this.lbl住院史入院时间1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl住院史入院时间1.SizeF = new System.Drawing.SizeF(70.83334F, 13.62498F);
            this.lbl住院史入院时间1.Text = "1";
            // 
            // xrLabel症状3
            // 
            this.xrLabel症状3.LocationFloat = new DevExpress.Utils.PointFloat(1380.208F, 303.75F);
            this.xrLabel症状3.Name = "xrLabel症状3";
            this.xrLabel症状3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel症状3.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel症状3.Text = "1";
            // 
            // xrLabel症状2
            // 
            this.xrLabel症状2.LocationFloat = new DevExpress.Utils.PointFloat(1345F, 303.7501F);
            this.xrLabel症状2.Name = "xrLabel症状2";
            this.xrLabel症状2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel症状2.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel症状2.Text = "1";
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.ImageUrl = "C:\\Users\\wangm\\Desktop\\001\\002 - 副本.JPG";
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(1654F, 1169F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.BottomMargin.BorderWidth = 0F;
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.StylePriority.UseBorders = false;
            this.BottomMargin.StylePriority.UseBorderWidth = false;
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl粉尘措施
            // 
            this.lbl粉尘措施.LocationFloat = new DevExpress.Utils.PointFloat(1454.541F, 880F);
            this.lbl粉尘措施.Name = "lbl粉尘措施";
            this.lbl粉尘措施.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl粉尘措施.SizeF = new System.Drawing.SizeF(55.12805F, 13.625F);
            this.lbl粉尘措施.Text = "粉尘";
            // 
            // lbl粉尘有无
            // 
            this.lbl粉尘有无.LocationFloat = new DevExpress.Utils.PointFloat(1557.083F, 884.4579F);
            this.lbl粉尘有无.Name = "lbl粉尘有无";
            this.lbl粉尘有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl粉尘有无.SizeF = new System.Drawing.SizeF(13.54163F, 13.625F);
            this.lbl粉尘有无.Text = "1";
            // 
            // xrLabel1
            // 
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(1454.541F, 895F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(55.12805F, 13.625F);
            this.xrLabel1.Text = "粉尘";
            // 
            // lbl物理防护
            // 
            this.lbl物理防护.LocationFloat = new DevExpress.Utils.PointFloat(1454.541F, 910F);
            this.lbl物理防护.Name = "lbl物理防护";
            this.lbl物理防护.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl物理防护.SizeF = new System.Drawing.SizeF(55.12805F, 13.625F);
            this.lbl物理防护.Text = "粉尘";
            // 
            // lbl物理因素
            // 
            this.lbl物理因素.LocationFloat = new DevExpress.Utils.PointFloat(1232.291F, 910F);
            this.lbl物理因素.Name = "lbl物理因素";
            this.lbl物理因素.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl物理因素.SizeF = new System.Drawing.SizeF(107.2921F, 13.625F);
            this.lbl物理因素.Text = "物理因素";
            // 
            // lbl化学防护
            // 
            this.lbl化学防护.LocationFloat = new DevExpress.Utils.PointFloat(1454.541F, 925F);
            this.lbl化学防护.Name = "lbl化学防护";
            this.lbl化学防护.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl化学防护.SizeF = new System.Drawing.SizeF(55.12805F, 13.625F);
            this.lbl化学防护.Text = "粉尘";
            // 
            // lbl化学物质
            // 
            this.lbl化学物质.LocationFloat = new DevExpress.Utils.PointFloat(1232.291F, 925F);
            this.lbl化学物质.Name = "lbl化学物质";
            this.lbl化学物质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl化学物质.SizeF = new System.Drawing.SizeF(107.2921F, 13.625F);
            this.lbl化学物质.Text = "化学物质";
            // 
            // lbl职业病其他
            // 
            this.lbl职业病其他.LocationFloat = new DevExpress.Utils.PointFloat(1210.416F, 940F);
            this.lbl职业病其他.Name = "lbl职业病其他";
            this.lbl职业病其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl职业病其他.SizeF = new System.Drawing.SizeF(79.16724F, 13.625F);
            this.lbl职业病其他.Text = "其他";
            // 
            // lbl职业病其他防护
            // 
            this.lbl职业病其他防护.LocationFloat = new DevExpress.Utils.PointFloat(1454.541F, 940F);
            this.lbl职业病其他防护.Name = "lbl职业病其他防护";
            this.lbl职业病其他防护.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl职业病其他防护.SizeF = new System.Drawing.SizeF(55.12805F, 13.625F);
            this.lbl职业病其他防护.Text = "粉尘";
            // 
            // lbl物理因素有无
            // 
            this.lbl物理因素有无.LocationFloat = new DevExpress.Utils.PointFloat(1557.083F, 920F);
            this.lbl物理因素有无.Name = "lbl物理因素有无";
            this.lbl物理因素有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl物理因素有无.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.lbl物理因素有无.Text = "1";
            // 
            // lbl化学物质有无
            // 
            this.lbl化学物质有无.LocationFloat = new DevExpress.Utils.PointFloat(1557.083F, 940F);
            this.lbl化学物质有无.Name = "lbl化学物质有无";
            this.lbl化学物质有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl化学物质有无.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.lbl化学物质有无.Text = "1";
            // 
            // xrLabel2
            // 
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(263.5416F, 232.0834F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(70.83334F, 13.62498F);
            this.xrLabel2.Text = "1";
            // 
            // xrLabel3
            // 
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(350.6248F, 232.0834F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(73.95837F, 13.62498F);
            this.xrLabel3.StylePriority.UsePadding = false;
            this.xrLabel3.Text = "1";
            // 
            // xrLabel4
            // 
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(350.6248F, 258.4584F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(73.95837F, 13.62498F);
            this.xrLabel4.Text = "1";
            // 
            // xrLabel5
            // 
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(263.5416F, 258.4584F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(70.83337F, 13.62498F);
            this.xrLabel5.StylePriority.UsePadding = false;
            this.xrLabel5.Text = "1";
            // 
            // xrLabel6
            // 
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(440.0001F, 232.0834F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(79.79166F, 13.62498F);
            this.xrLabel6.Text = "1";
            // 
            // xrLabel7
            // 
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(440.0001F, 258.4584F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(79.79166F, 13.62498F);
            this.xrLabel7.Text = "1";
            // 
            // xrLabel8
            // 
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(539.3751F, 258.4584F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(98.12512F, 13.62498F);
            this.xrLabel8.Text = "1";
            // 
            // xrLabel9
            // 
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(539.3751F, 232.0834F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(98.12512F, 13.62498F);
            this.xrLabel9.Text = "1";
            // 
            // xrLabel10
            // 
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(658.3333F, 232.0834F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(71.66669F, 13.62498F);
            this.xrLabel10.Text = "1";
            // 
            // xrLabel11
            // 
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(658.3333F, 258.4584F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(71.66669F, 13.62498F);
            this.xrLabel11.Text = "1";
            // 
            // xrLabel12
            // 
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(599.7917F, 354.5833F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(115F, 24.04166F);
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "1";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(473.5417F, 354.5833F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(79.58334F, 24.04163F);
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "巩膜";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(407.9164F, 354.5832F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(52.08334F, 24.04169F);
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "1";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(263.5416F, 354.5832F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(120.8334F, 24.04169F);
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "1";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(159.375F, 354.5833F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(73.95834F, 24.0416F);
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "1";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel17
            // 
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(599.7916F, 380F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(115F, 24.04166F);
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "1";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel18
            // 
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(159.3748F, 380F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(73.95834F, 24.0416F);
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "1";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel19
            // 
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(407.9163F, 379.9998F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(52.08334F, 24.04169F);
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "1";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel20
            // 
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(473.5415F, 380F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(79.58334F, 24.04163F);
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "巩膜";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel21
            // 
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(263.5415F, 379.9998F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(120.8334F, 24.04169F);
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "1";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel22
            // 
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(263.5417F, 409.5833F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(120.8334F, 24.04169F);
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = "1";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel23
            // 
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(159.3749F, 409.5834F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(73.95834F, 24.0416F);
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "1";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel24
            // 
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(407.9164F, 409.5833F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(52.08334F, 24.04169F);
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "1";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel25
            // 
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(473.5416F, 409.5834F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(79.58334F, 24.04163F);
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "巩膜";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel26
            // 
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(599.7918F, 409.5834F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(115F, 24.04166F);
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "1";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel27
            // 
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(263.5416F, 439.9999F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(120.8334F, 24.04169F);
            this.xrLabel27.StylePriority.UseTextAlignment = false;
            this.xrLabel27.Text = "1";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel28
            // 
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(159.3749F, 440F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(73.95834F, 24.0416F);
            this.xrLabel28.StylePriority.UseTextAlignment = false;
            this.xrLabel28.Text = "1";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel29
            // 
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(407.9163F, 439.9999F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(52.08334F, 24.04169F);
            this.xrLabel29.StylePriority.UseTextAlignment = false;
            this.xrLabel29.Text = "1";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel30
            // 
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(473.5415F, 440F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(79.58334F, 24.04163F);
            this.xrLabel30.StylePriority.UseTextAlignment = false;
            this.xrLabel30.Text = "巩膜";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel31
            // 
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(599.7918F, 440F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(115F, 24.04166F);
            this.xrLabel31.StylePriority.UseTextAlignment = false;
            this.xrLabel31.Text = "1";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel32
            // 
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(263.5416F, 469.0416F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(120.8334F, 24.04169F);
            this.xrLabel32.StylePriority.UseTextAlignment = false;
            this.xrLabel32.Text = "1";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel33
            // 
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(159.3748F, 469.0417F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(73.95834F, 24.0416F);
            this.xrLabel33.StylePriority.UseTextAlignment = false;
            this.xrLabel33.Text = "1";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel34
            // 
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(407.9162F, 469.0416F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(52.08334F, 24.04169F);
            this.xrLabel34.StylePriority.UseTextAlignment = false;
            this.xrLabel34.Text = "1";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel35
            // 
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(473.5414F, 469.0417F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(79.58334F, 24.04163F);
            this.xrLabel35.StylePriority.UseTextAlignment = false;
            this.xrLabel35.Text = "巩膜";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel36
            // 
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(599.7917F, 469.0417F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(115F, 24.04166F);
            this.xrLabel36.StylePriority.UseTextAlignment = false;
            this.xrLabel36.Text = "1";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel杂音有
            // 
            this.xrLabel杂音有.LocationFloat = new DevExpress.Utils.PointFloat(155.2084F, 600F);
            this.xrLabel杂音有.Name = "xrLabel杂音有";
            this.xrLabel杂音有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel杂音有.SizeF = new System.Drawing.SizeF(78.12503F, 13.625F);
            this.xrLabel杂音有.Text = "杂音有";
            // 
            // xrLabel罗音其他
            // 
            this.xrLabel罗音其他.LocationFloat = new DevExpress.Utils.PointFloat(279.1666F, 600F);
            this.xrLabel罗音其他.Name = "xrLabel罗音其他";
            this.xrLabel罗音其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel罗音其他.SizeF = new System.Drawing.SizeF(57.29175F, 13.625F);
            this.xrLabel罗音其他.Text = "罗音";
            // 
            // xrLabel压痛有
            // 
            this.xrLabel压痛有.LocationFloat = new DevExpress.Utils.PointFloat(419.7916F, 600F);
            this.xrLabel压痛有.Name = "xrLabel压痛有";
            this.xrLabel压痛有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel压痛有.SizeF = new System.Drawing.SizeF(87.5F, 13.625F);
            this.xrLabel压痛有.Text = "压痛";
            // 
            // xrLabel脾大有
            // 
            this.xrLabel脾大有.LocationFloat = new DevExpress.Utils.PointFloat(388.5413F, 540F);
            this.xrLabel脾大有.Name = "xrLabel脾大有";
            this.xrLabel脾大有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel脾大有.SizeF = new System.Drawing.SizeF(87.5F, 13.625F);
            this.xrLabel脾大有.Text = "脾大";
            // 
            // xrLabel肝大有
            // 
            this.xrLabel肝大有.LocationFloat = new DevExpress.Utils.PointFloat(550.0001F, 600F);
            this.xrLabel肝大有.Name = "xrLabel肝大有";
            this.xrLabel肝大有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel肝大有.SizeF = new System.Drawing.SizeF(87.49997F, 13.625F);
            this.xrLabel肝大有.Text = "肝大";
            // 
            // xrLabel包块有
            // 
            this.xrLabel包块有.LocationFloat = new DevExpress.Utils.PointFloat(562.4999F, 560.0001F);
            this.xrLabel包块有.Name = "xrLabel包块有";
            this.xrLabel包块有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel包块有.SizeF = new System.Drawing.SizeF(87.5F, 13.625F);
            this.xrLabel包块有.Text = "包块";
            // 
            // xrLabel肛门指诊其他
            // 
            this.xrLabel肛门指诊其他.LocationFloat = new DevExpress.Utils.PointFloat(661.4583F, 620F);
            this.xrLabel肛门指诊其他.Name = "xrLabel肛门指诊其他";
            this.xrLabel肛门指诊其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel肛门指诊其他.SizeF = new System.Drawing.SizeF(75.20837F, 13.625F);
            this.xrLabel肛门指诊其他.Text = "肛门指诊";
            // 
            // xrLabel39
            // 
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(748.9583F, 420F);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel39.Text = "1";
            // 
            // xrLabel心律
            // 
            this.xrLabel心律.LocationFloat = new DevExpress.Utils.PointFloat(745.96F, 440F);
            this.xrLabel心律.Name = "xrLabel心律";
            this.xrLabel心律.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel心律.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel心律.Text = "1";
            // 
            // xrLabel杂音
            // 
            this.xrLabel杂音.LocationFloat = new DevExpress.Utils.PointFloat(745.96F, 458.75F);
            this.xrLabel杂音.Name = "xrLabel杂音";
            this.xrLabel杂音.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel杂音.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel杂音.Text = "1";
            // 
            // xrLabel压痛
            // 
            this.xrLabel压痛.LocationFloat = new DevExpress.Utils.PointFloat(745.9597F, 482.7917F);
            this.xrLabel压痛.Name = "xrLabel压痛";
            this.xrLabel压痛.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel压痛.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel压痛.Text = "1";
            // 
            // xrLabel包块
            // 
            this.xrLabel包块.LocationFloat = new DevExpress.Utils.PointFloat(745.9597F, 500F);
            this.xrLabel包块.Name = "xrLabel包块";
            this.xrLabel包块.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel包块.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel包块.Text = "1";
            // 
            // xrLabel肝大
            // 
            this.xrLabel肝大.LocationFloat = new DevExpress.Utils.PointFloat(745.9595F, 520F);
            this.xrLabel肝大.Name = "xrLabel肝大";
            this.xrLabel肝大.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel肝大.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel肝大.Text = "1";
            // 
            // xrLabel脾大
            // 
            this.xrLabel脾大.LocationFloat = new DevExpress.Utils.PointFloat(745.9595F, 540F);
            this.xrLabel脾大.Name = "xrLabel脾大";
            this.xrLabel脾大.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel脾大.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel脾大.Text = "1";
            // 
            // xrLabel移动性浊音
            // 
            this.xrLabel移动性浊音.LocationFloat = new DevExpress.Utils.PointFloat(745.9597F, 560F);
            this.xrLabel移动性浊音.Name = "xrLabel移动性浊音";
            this.xrLabel移动性浊音.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel移动性浊音.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel移动性浊音.Text = "1";
            // 
            // xrLabel下肢水肿
            // 
            this.xrLabel下肢水肿.LocationFloat = new DevExpress.Utils.PointFloat(745.9598F, 580F);
            this.xrLabel下肢水肿.Name = "xrLabel下肢水肿";
            this.xrLabel下肢水肿.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel下肢水肿.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel下肢水肿.Text = "1";
            // 
            // xrLabel足背动脉
            // 
            this.xrLabel足背动脉.LocationFloat = new DevExpress.Utils.PointFloat(745.9597F, 600F);
            this.xrLabel足背动脉.Name = "xrLabel足背动脉";
            this.xrLabel足背动脉.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel足背动脉.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel足背动脉.Text = "1";
            // 
            // xrLabel肛门指诊
            // 
            this.xrLabel肛门指诊.LocationFloat = new DevExpress.Utils.PointFloat(745.9597F, 620F);
            this.xrLabel肛门指诊.Name = "xrLabel肛门指诊";
            this.xrLabel肛门指诊.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel肛门指诊.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel肛门指诊.Text = "1";
            // 
            // xrLabel乳腺4
            // 
            this.xrLabel乳腺4.LocationFloat = new DevExpress.Utils.PointFloat(749.0847F, 644.7917F);
            this.xrLabel乳腺4.Name = "xrLabel乳腺4";
            this.xrLabel乳腺4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel乳腺4.SizeF = new System.Drawing.SizeF(16.66669F, 13.62498F);
            this.xrLabel乳腺4.Text = "1";
            // 
            // report体检1
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.BorderWidth = 0F;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
            this.PageHeight = 1169;
            this.PageWidth = 1654;
            this.PaperKind = System.Drawing.Printing.PaperKind.A3;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel症状2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel移动性浊音有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel乳腺1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel乳腺2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel乳腺3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel乳腺其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel外阴异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel阴道异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel宫颈异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel心率;
        private DevExpress.XtraReports.UI.XRLabel xrLabel眼底异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel症状8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel症状9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel巩膜异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel症状10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel淋巴结其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel呼吸音异常;
        private DevExpress.XtraReports.UI.XRLabel lbl住院史原因2;
        private DevExpress.XtraReports.UI.XRLabel lbl病案号1;
        private DevExpress.XtraReports.UI.XRLabel lbl住院史入院时间2;
        private DevExpress.XtraReports.UI.XRLabel lbl住院史出院时间2;
        private DevExpress.XtraReports.UI.XRLabel lbl医疗机构名称2;
        private DevExpress.XtraReports.UI.XRLabel lbl病案号2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel症状5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel症状4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel症状6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel症状7;
        private DevExpress.XtraReports.UI.XRLabel lbl住院史出院时间1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel缺齿4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel龋齿1;
        private DevExpress.XtraReports.UI.XRLabel lbl医疗机构名称1;
        private DevExpress.XtraReports.UI.XRLabel lbl住院史原因1;
        private DevExpress.XtraReports.UI.XRLabel lbl住院史入院时间1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel症状3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel尿胴体;
        private DevExpress.XtraReports.UI.XRLabel xrLabel尿潜血;
        private DevExpress.XtraReports.UI.XRLabel xrLabel尿常规其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel空腹血糖1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel空腹血糖2;
        private DevExpress.XtraReports.UI.XRLabel lbl饮酒种类4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel78;
        private DevExpress.XtraReports.UI.XRLabel xrLabel大便潜血;
        private DevExpress.XtraReports.UI.XRLabel xrLabel姓名;
        private DevExpress.XtraReports.UI.XRLabel xrLabel症状其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel宫体异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel附件异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel外阴;
        private DevExpress.XtraReports.UI.XRLabel lbl放射物质有无;
        private DevExpress.XtraReports.UI.XRLabel lbl职业病有无;
        private DevExpress.XtraReports.UI.XRLabel lbl从业时间;
        private DevExpress.XtraReports.UI.XRLabel lbl工种;
        private DevExpress.XtraReports.UI.XRLabel xrLabel查体其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血红蛋白;
        private DevExpress.XtraReports.UI.XRLabel xrLabel白细胞;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血小板;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血常规其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel尿蛋白;
        private DevExpress.XtraReports.UI.XRLabel xrLabel尿糖;
        private DevExpress.XtraReports.UI.XRLabel xrLabel体检日期年;
        private DevExpress.XtraReports.UI.XRLabel xrLabel体检日期月;
        private DevExpress.XtraReports.UI.XRLabel xrLabel责任医生;
        private DevExpress.XtraReports.UI.XRLabel xrLabel体检日期日;
        private DevExpress.XtraReports.UI.XRLabel xrLabel体温;
        private DevExpress.XtraReports.UI.XRLabel xrLabel呼吸频率;
        private DevExpress.XtraReports.UI.XRLabel xrLabel脉率;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血压左侧1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血压左侧2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血压右侧1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血压右侧2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel身高;
        private DevExpress.XtraReports.UI.XRLabel xrLabel体重;
        private DevExpress.XtraReports.UI.XRLabel xrLabel腰围;
        private DevExpress.XtraReports.UI.XRLabel xrLabel体质指数;
        private DevExpress.XtraReports.UI.XRLabel xrLabel老年人健康撞他自我评估;
        private DevExpress.XtraReports.UI.XRLabel xrLabel老年人生活自理能力自我评估;
        private DevExpress.XtraReports.UI.XRLabel xrLabel老年人认知功能分;
        private DevExpress.XtraReports.UI.XRLabel xrLabel老年人认知功能;
        private DevExpress.XtraReports.UI.XRLabel xrLabel老年人情感状态分;
        private DevExpress.XtraReports.UI.XRLabel xrLabel老年人情感状态;
        private DevExpress.XtraReports.UI.XRLabel xrLabel锻炼频率;
        private DevExpress.XtraReports.UI.XRLabel xrLabel每次锻炼时间;
        private DevExpress.XtraReports.UI.XRLabel xrLabel坚持锻炼时间;
        private DevExpress.XtraReports.UI.XRLabel lbl日吸烟量;
        private DevExpress.XtraReports.UI.XRLabel xrLabel锻炼方式;
        private DevExpress.XtraReports.UI.XRLabel lbl饮食习惯1;
        private DevExpress.XtraReports.UI.XRLabel lbl饮食习惯2;
        private DevExpress.XtraReports.UI.XRLabel lbl饮食习惯3;
        private DevExpress.XtraReports.UI.XRLabel lbl吸烟状况;
        private DevExpress.XtraReports.UI.XRLabel lbl开始吸烟年龄;
        private DevExpress.XtraReports.UI.XRLabel lbl日饮酒量;
        private DevExpress.XtraReports.UI.XRLabel lbl戒酒年龄;
        private DevExpress.XtraReports.UI.XRLabel lbl是否戒酒;
        private DevExpress.XtraReports.UI.XRLabel lbl饮酒频率;
        private DevExpress.XtraReports.UI.XRLabel lbl戒烟年龄;
        private DevExpress.XtraReports.UI.XRLabel lbl开始饮酒年龄;
        private DevExpress.XtraReports.UI.XRLabel lbl饮酒种类1;
        private DevExpress.XtraReports.UI.XRLabel lbl粉尘;
        private DevExpress.XtraReports.UI.XRLabel lbl近一年内是否醉酒;
        private DevExpress.XtraReports.UI.XRLabel lbl饮酒种类其他;
        private DevExpress.XtraReports.UI.XRLabel lbl饮酒种类2;
        private DevExpress.XtraReports.UI.XRLabel lbl放射物质;
        private DevExpress.XtraReports.UI.XRLabel lbl饮酒种类3;
        private DevExpress.XtraReports.UI.XRLabel lbl粉尘措施;
        private DevExpress.XtraReports.UI.XRLabel lbl粉尘有无;
        private DevExpress.XtraReports.UI.XRLabel lbl化学防护;
        private DevExpress.XtraReports.UI.XRLabel lbl化学物质;
        private DevExpress.XtraReports.UI.XRLabel lbl物理防护;
        private DevExpress.XtraReports.UI.XRLabel lbl物理因素;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lbl职业病其他;
        private DevExpress.XtraReports.UI.XRLabel lbl职业病其他防护;
        private DevExpress.XtraReports.UI.XRLabel lbl物理因素有无;
        private DevExpress.XtraReports.UI.XRLabel lbl化学物质有无;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel肛门指诊其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel罗音其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel杂音有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel压痛有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel包块有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel肝大有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel脾大有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel压痛;
        private DevExpress.XtraReports.UI.XRLabel xrLabel包块;
        private DevExpress.XtraReports.UI.XRLabel xrLabel肝大;
        private DevExpress.XtraReports.UI.XRLabel xrLabel脾大;
        private DevExpress.XtraReports.UI.XRLabel xrLabel移动性浊音;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel xrLabel心律;
        private DevExpress.XtraReports.UI.XRLabel xrLabel杂音;
        private DevExpress.XtraReports.UI.XRLabel xrLabel下肢水肿;
        private DevExpress.XtraReports.UI.XRLabel xrLabel足背动脉;
        private DevExpress.XtraReports.UI.XRLabel xrLabel肛门指诊;
        private DevExpress.XtraReports.UI.XRLabel xrLabel乳腺4;
    }
}
