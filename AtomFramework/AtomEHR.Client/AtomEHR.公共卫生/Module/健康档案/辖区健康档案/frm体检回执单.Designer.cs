﻿using AtomEHR.Library.UserControls;
namespace AtomEHR.公共卫生.Module.健康档案.辖区健康档案
{
    partial class frm体检回执单
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm体检回执单));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk是否签约服务 = new DevExpress.XtraEditors.CheckEdit();
            this.chk是否贫困人口 = new DevExpress.XtraEditors.CheckEdit();
            this.cbo档案类别 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dte体检时间2 = new DevExpress.XtraEditors.DateEdit();
            this.dte体检时间1 = new DevExpress.XtraEditors.DateEdit();
            this.dte录入时间2 = new DevExpress.XtraEditors.DateEdit();
            this.dte录入时间1 = new DevExpress.XtraEditors.DateEdit();
            this.dte出生日期2 = new DevExpress.XtraEditors.DateEdit();
            this.dte出生日期1 = new DevExpress.XtraEditors.DateEdit();
            this.txt详细地址 = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEdit村 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit镇 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.chk含下属机构 = new DevExpress.XtraEditors.CheckEdit();
            this.treeListLookUpEdit机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt档案编号 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.cbo档案状态 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo合格档案 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo性别 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo录入人 = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.gc健康体检 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv健康体检 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col体检日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.link档案号 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col创建日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col血红蛋白 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col白细胞 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col血小板 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col尿蛋白 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col尿糖 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col尿酮体 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col尿潜血 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col血清谷丙转氨酶 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col血清谷草转氨酶 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col白蛋白 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col总胆红素 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col结合胆红素 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col血清肌酐 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col血尿素氮 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col血钾浓度 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col血钠浓度 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col总胆固醇 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col甘油三酯 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col血清低密度脂蛋白胆固醇 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col血清高密度脂蛋白胆固醇 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col空腹血糖 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col餐后2H血糖 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col血压右侧1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col血压右侧2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col血压左侧1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col血压左侧2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col心电图 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col心电图异常 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn查询 = new DevExpress.XtraEditors.SimpleButton();
            this.btn重置 = new DevExpress.XtraEditors.SimpleButton();
            this.btn打印 = new DevExpress.XtraEditors.SimpleButton();
            this.btn设置 = new DevExpress.XtraEditors.SimpleButton();
            this.tabControl = new DevExpress.XtraTab.XtraTabControl();
            this.page人次 = new DevExpress.XtraTab.XtraTabPage();
            this.pagerControl1 = new TActionProject.PagerControl();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk是否签约服务.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk是否贫困人口.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案类别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte体检时间2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte体检时间2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte体检时间1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte体检时间1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生日期2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生日期2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生日期1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生日期1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt详细地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk含下属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo合格档案.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo录入人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc健康体检)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv健康体检)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link档案号)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.page人次.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.tabControl);
            this.tpSummary.Controls.Add(this.panelControl1);
            this.tpSummary.Controls.Add(this.layoutControl1);
            this.tpSummary.Size = new System.Drawing.Size(1115, 516);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 29);
            this.pnlSummary.Size = new System.Drawing.Size(1121, 522);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Size = new System.Drawing.Size(1121, 522);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(932, 589);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Location = new System.Drawing.Point(0, 3);
            this.gcNavigator.Size = new System.Drawing.Size(1121, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(943, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(746, 2);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.flowLayoutPanel2);
            this.layoutControl1.Controls.Add(this.cbo档案类别);
            this.layoutControl1.Controls.Add(this.dte体检时间2);
            this.layoutControl1.Controls.Add(this.dte体检时间1);
            this.layoutControl1.Controls.Add(this.dte录入时间2);
            this.layoutControl1.Controls.Add(this.dte录入时间1);
            this.layoutControl1.Controls.Add(this.dte出生日期2);
            this.layoutControl1.Controls.Add(this.dte出生日期1);
            this.layoutControl1.Controls.Add(this.txt详细地址);
            this.layoutControl1.Controls.Add(this.comboBoxEdit村);
            this.layoutControl1.Controls.Add(this.comboBoxEdit镇);
            this.layoutControl1.Controls.Add(this.chk含下属机构);
            this.layoutControl1.Controls.Add(this.treeListLookUpEdit机构);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt档案编号);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.cbo档案状态);
            this.layoutControl1.Controls.Add(this.cbo合格档案);
            this.layoutControl1.Controls.Add(this.cbo性别);
            this.layoutControl1.Controls.Add(this.cbo录入人);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1115, 176);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.chk是否签约服务);
            this.flowLayoutPanel2.Controls.Add(this.chk是否贫困人口);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(100, 145);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1010, 26);
            this.flowLayoutPanel2.TabIndex = 8;
            // 
            // chk是否签约服务
            // 
            this.chk是否签约服务.Location = new System.Drawing.Point(0, 0);
            this.chk是否签约服务.Margin = new System.Windows.Forms.Padding(0);
            this.chk是否签约服务.Name = "chk是否签约服务";
            this.chk是否签约服务.Properties.Caption = "是否签约服务";
            this.chk是否签约服务.Size = new System.Drawing.Size(108, 19);
            this.chk是否签约服务.TabIndex = 9;
            // 
            // chk是否贫困人口
            // 
            this.chk是否贫困人口.Location = new System.Drawing.Point(108, 0);
            this.chk是否贫困人口.Margin = new System.Windows.Forms.Padding(0);
            this.chk是否贫困人口.Name = "chk是否贫困人口";
            this.chk是否贫困人口.Properties.Caption = "是否贫困人口";
            this.chk是否贫困人口.Size = new System.Drawing.Size(102, 19);
            this.chk是否贫困人口.TabIndex = 10;
            // 
            // cbo档案类别
            // 
            this.cbo档案类别.Location = new System.Drawing.Point(800, 97);
            this.cbo档案类别.Name = "cbo档案类别";
            this.cbo档案类别.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo档案类别.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo档案类别.Size = new System.Drawing.Size(201, 20);
            this.cbo档案类别.StyleController = this.layoutControl1;
            this.cbo档案类别.TabIndex = 28;
            // 
            // dte体检时间2
            // 
            this.dte体检时间2.EditValue = null;
            this.dte体检时间2.Location = new System.Drawing.Point(275, 97);
            this.dte体检时间2.Name = "dte体检时间2";
            this.dte体检时间2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte体检时间2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte体检时间2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte体检时间2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte体检时间2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte体检时间2.Size = new System.Drawing.Size(126, 20);
            this.dte体检时间2.StyleController = this.layoutControl1;
            this.dte体检时间2.TabIndex = 27;
            // 
            // dte体检时间1
            // 
            this.dte体检时间1.EditValue = null;
            this.dte体检时间1.Location = new System.Drawing.Point(100, 97);
            this.dte体检时间1.Name = "dte体检时间1";
            this.dte体检时间1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte体检时间1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte体检时间1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte体检时间1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte体检时间1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte体检时间1.Size = new System.Drawing.Size(151, 20);
            this.dte体检时间1.StyleController = this.layoutControl1;
            this.dte体检时间1.TabIndex = 26;
            // 
            // dte录入时间2
            // 
            this.dte录入时间2.EditValue = null;
            this.dte录入时间2.Location = new System.Drawing.Point(275, 73);
            this.dte录入时间2.Name = "dte录入时间2";
            this.dte录入时间2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte录入时间2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte录入时间2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte录入时间2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte录入时间2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte录入时间2.Size = new System.Drawing.Size(126, 20);
            this.dte录入时间2.StyleController = this.layoutControl1;
            this.dte录入时间2.TabIndex = 25;
            // 
            // dte录入时间1
            // 
            this.dte录入时间1.EditValue = null;
            this.dte录入时间1.Location = new System.Drawing.Point(100, 73);
            this.dte录入时间1.Name = "dte录入时间1";
            this.dte录入时间1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte录入时间1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte录入时间1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte录入时间1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte录入时间1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte录入时间1.Size = new System.Drawing.Size(151, 20);
            this.dte录入时间1.StyleController = this.layoutControl1;
            this.dte录入时间1.TabIndex = 24;
            // 
            // dte出生日期2
            // 
            this.dte出生日期2.EditValue = null;
            this.dte出生日期2.Location = new System.Drawing.Point(275, 49);
            this.dte出生日期2.Name = "dte出生日期2";
            this.dte出生日期2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出生日期2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出生日期2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte出生日期2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte出生日期2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte出生日期2.Size = new System.Drawing.Size(126, 20);
            this.dte出生日期2.StyleController = this.layoutControl1;
            this.dte出生日期2.TabIndex = 23;
            // 
            // dte出生日期1
            // 
            this.dte出生日期1.EditValue = null;
            this.dte出生日期1.Location = new System.Drawing.Point(100, 49);
            this.dte出生日期1.Name = "dte出生日期1";
            this.dte出生日期1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出生日期1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出生日期1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte出生日期1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte出生日期1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte出生日期1.Size = new System.Drawing.Size(151, 20);
            this.dte出生日期1.StyleController = this.layoutControl1;
            this.dte出生日期1.TabIndex = 22;
            // 
            // txt详细地址
            // 
            this.txt详细地址.Location = new System.Drawing.Point(495, 121);
            this.txt详细地址.Name = "txt详细地址";
            this.txt详细地址.Size = new System.Drawing.Size(206, 20);
            this.txt详细地址.StyleController = this.layoutControl1;
            this.txt详细地址.TabIndex = 21;
            // 
            // comboBoxEdit村
            // 
            this.comboBoxEdit村.Location = new System.Drawing.Point(245, 121);
            this.comboBoxEdit村.Name = "comboBoxEdit村";
            this.comboBoxEdit村.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit村.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit村.Size = new System.Drawing.Size(246, 20);
            this.comboBoxEdit村.StyleController = this.layoutControl1;
            this.comboBoxEdit村.TabIndex = 20;
            // 
            // comboBoxEdit镇
            // 
            this.comboBoxEdit镇.Location = new System.Drawing.Point(100, 121);
            this.comboBoxEdit镇.Name = "comboBoxEdit镇";
            this.comboBoxEdit镇.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit镇.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit镇.Size = new System.Drawing.Size(141, 20);
            this.comboBoxEdit镇.StyleController = this.layoutControl1;
            this.comboBoxEdit镇.TabIndex = 19;
            this.comboBoxEdit镇.EditValueChanged += new System.EventHandler(this.comboBoxEdit镇_EditValueChanged);
            // 
            // chk含下属机构
            // 
            this.chk含下属机构.EditValue = true;
            this.chk含下属机构.Location = new System.Drawing.Point(255, 25);
            this.chk含下属机构.Name = "chk含下属机构";
            this.chk含下属机构.Properties.Caption = "含下属机构";
            this.chk含下属机构.Size = new System.Drawing.Size(146, 19);
            this.chk含下属机构.StyleController = this.layoutControl1;
            this.chk含下属机构.TabIndex = 18;
            // 
            // treeListLookUpEdit机构
            // 
            this.treeListLookUpEdit机构.EditValue = "";
            this.treeListLookUpEdit机构.Location = new System.Drawing.Point(100, 25);
            this.treeListLookUpEdit机构.Name = "treeListLookUpEdit机构";
            this.treeListLookUpEdit机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEdit机构.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.treeListLookUpEdit机构.Size = new System.Drawing.Size(151, 20);
            this.treeListLookUpEdit机构.StyleController = this.layoutControl1;
            this.treeListLookUpEdit机构.TabIndex = 17;
            this.treeListLookUpEdit机构.EditValueChanged += new System.EventHandler(this.treeListLookUpEdit机构_EditValueChanged);
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(0, 0);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(500, 73);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Size = new System.Drawing.Size(201, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 12;
            // 
            // txt档案编号
            // 
            this.txt档案编号.Location = new System.Drawing.Point(500, 49);
            this.txt档案编号.Name = "txt档案编号";
            this.txt档案编号.Size = new System.Drawing.Size(201, 20);
            this.txt档案编号.StyleController = this.layoutControl1;
            this.txt档案编号.TabIndex = 8;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(500, 25);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(201, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 5;
            // 
            // cbo档案状态
            // 
            this.cbo档案状态.Location = new System.Drawing.Point(500, 97);
            this.cbo档案状态.Name = "cbo档案状态";
            this.cbo档案状态.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo档案状态.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo档案状态.Size = new System.Drawing.Size(201, 20);
            this.cbo档案状态.StyleController = this.layoutControl1;
            this.cbo档案状态.TabIndex = 13;
            // 
            // cbo合格档案
            // 
            this.cbo合格档案.Location = new System.Drawing.Point(800, 73);
            this.cbo合格档案.Name = "cbo合格档案";
            this.cbo合格档案.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo合格档案.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo合格档案.Size = new System.Drawing.Size(201, 20);
            this.cbo合格档案.StyleController = this.layoutControl1;
            this.cbo合格档案.TabIndex = 14;
            // 
            // cbo性别
            // 
            this.cbo性别.Location = new System.Drawing.Point(800, 25);
            this.cbo性别.Name = "cbo性别";
            this.cbo性别.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo性别.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo性别.Size = new System.Drawing.Size(201, 20);
            this.cbo性别.StyleController = this.layoutControl1;
            this.cbo性别.TabIndex = 6;
            // 
            // cbo录入人
            // 
            this.cbo录入人.Location = new System.Drawing.Point(800, 49);
            this.cbo录入人.Name = "cbo录入人";
            this.cbo录入人.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo录入人.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("UserName", "用户名")});
            this.cbo录入人.Properties.NullText = "";
            this.cbo录入人.Properties.PopupSizeable = false;
            this.cbo录入人.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbo录入人.Size = new System.Drawing.Size(201, 20);
            this.cbo录入人.StyleController = this.layoutControl1;
            this.cbo录入人.TabIndex = 9;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.ExpandButtonVisible = true;
            this.layoutControlGroup1.ExpandOnDoubleClick = true;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10,
            this.layoutControlItem2,
            this.layoutControlItem6,
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.layoutControlItem9,
            this.layoutControlItem11,
            this.layoutControlItem14,
            this.layoutControlItem1,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem13,
            this.layoutControlItem18,
            this.layoutControlItem4,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem7,
            this.layoutControlItem8});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1115, 176);
            this.layoutControlGroup1.Text = "查询条件";
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.cbo档案状态;
            this.layoutControlItem10.CustomizationFormText = "档案状态：";
            this.layoutControlItem10.Location = new System.Drawing.Point(400, 72);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(300, 24);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "档案状态：";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.txt姓名;
            this.layoutControlItem2.CustomizationFormText = "姓名：";
            this.layoutControlItem2.Location = new System.Drawing.Point(400, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(300, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "姓名：";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.cbo录入人;
            this.layoutControlItem6.CustomizationFormText = "录入人：";
            this.layoutControlItem6.Location = new System.Drawing.Point(700, 24);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(409, 24);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "录入人：";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.cbo性别;
            this.layoutControlItem3.CustomizationFormText = "性别：";
            this.layoutControlItem3.Location = new System.Drawing.Point(700, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(409, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "性别：";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt档案编号;
            this.layoutControlItem5.CustomizationFormText = "档案号：";
            this.layoutControlItem5.Location = new System.Drawing.Point(400, 24);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(300, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "档案号：";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.txt身份证号;
            this.layoutControlItem9.CustomizationFormText = "身份证号：";
            this.layoutControlItem9.Location = new System.Drawing.Point(400, 48);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(300, 24);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "身份证号：";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.cbo合格档案;
            this.layoutControlItem11.CustomizationFormText = "合格档案：";
            this.layoutControlItem11.Location = new System.Drawing.Point(700, 48);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(409, 24);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "合格档案：";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.treeListLookUpEdit机构;
            this.layoutControlItem14.CustomizationFormText = "所属机构：";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(250, 24);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "所属机构：";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.chk含下属机构;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(250, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(150, 23);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(150, 23);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.comboBoxEdit镇;
            this.layoutControlItem15.CustomizationFormText = "居住地址：";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(240, 24);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(240, 24);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(240, 24);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "居住地址：";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.comboBoxEdit村;
            this.layoutControlItem16.CustomizationFormText = "layoutControlItem16";
            this.layoutControlItem16.Location = new System.Drawing.Point(240, 96);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(250, 24);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "layoutControlItem16";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextToControlDistance = 0;
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.txt详细地址;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(490, 96);
            this.layoutControlItem17.MaxSize = new System.Drawing.Size(210, 24);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(210, 24);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(210, 24);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.dte出生日期1;
            this.layoutControlItem13.CustomizationFormText = "出生日期：";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(149, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(250, 24);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "出生日期：";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.dte出生日期2;
            this.layoutControlItem18.CustomizationFormText = "~";
            this.layoutControlItem18.Location = new System.Drawing.Point(250, 24);
            this.layoutControlItem18.MaxSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(74, 24);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "~";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(15, 20);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.dte录入时间1;
            this.layoutControlItem4.CustomizationFormText = "录入时间：";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(149, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(250, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "录入时间：";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.dte录入时间2;
            this.layoutControlItem19.CustomizationFormText = "~";
            this.layoutControlItem19.Location = new System.Drawing.Point(250, 48);
            this.layoutControlItem19.MaxSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(74, 24);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "~";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(15, 20);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.Control = this.dte体检时间1;
            this.layoutControlItem20.CustomizationFormText = "体检时间：";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem20.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(149, 24);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(250, 24);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "体检时间：";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.dte体检时间2;
            this.layoutControlItem21.CustomizationFormText = "~";
            this.layoutControlItem21.Location = new System.Drawing.Point(250, 72);
            this.layoutControlItem21.MaxSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(74, 24);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "~";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(15, 20);
            this.layoutControlItem21.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.cbo档案类别;
            this.layoutControlItem7.CustomizationFormText = "档案类别：";
            this.layoutControlItem7.Location = new System.Drawing.Point(700, 72);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(409, 48);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "档案类别：";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem8.Control = this.flowLayoutPanel2;
            this.layoutControlItem8.CustomizationFormText = "状态：";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(1109, 30);
            this.layoutControlItem8.Text = "状态：";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1121, 3);
            this.panelControl2.TabIndex = 1;
            // 
            // panelControl3
            // 
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(0, 3);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1121, 548);
            this.panelControl3.TabIndex = 2;
            // 
            // gc健康体检
            // 
            this.gc健康体检.AllowBandedGridColumnSort = false;
            this.gc健康体检.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc健康体检.IsBestFitColumns = true;
            this.gc健康体检.Location = new System.Drawing.Point(0, 0);
            this.gc健康体检.MainView = this.gv健康体检;
            this.gc健康体检.Name = "gc健康体检";
            this.gc健康体检.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.link档案号});
            this.gc健康体检.ShowContextMenu = false;
            this.gc健康体检.Size = new System.Drawing.Size(1109, 241);
            this.gc健康体检.StrWhere = "";
            this.gc健康体检.TabIndex = 1;
            this.gc健康体检.UseCheckBox = false;
            this.gc健康体检.View = "";
            this.gc健康体检.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv健康体检});
            this.gc健康体检.Click += new System.EventHandler(this.gc健康体检_Click);
            // 
            // gv健康体检
            // 
            this.gv健康体检.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gv健康体检.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gv健康体检.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gv健康体检.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.DetailTip.Options.UseFont = true;
            this.gv健康体检.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.Empty.Options.UseFont = true;
            this.gv健康体检.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.EvenRow.Options.UseFont = true;
            this.gv健康体检.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gv健康体检.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.FilterPanel.Options.UseFont = true;
            this.gv健康体检.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.FixedLine.Options.UseFont = true;
            this.gv健康体检.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.FocusedCell.Options.UseFont = true;
            this.gv健康体检.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.FocusedRow.Options.UseFont = true;
            this.gv健康体检.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gv健康体检.Appearance.FooterPanel.Options.UseFont = true;
            this.gv健康体检.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.GroupButton.Options.UseFont = true;
            this.gv健康体检.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.GroupFooter.Options.UseFont = true;
            this.gv健康体检.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.GroupPanel.Options.UseFont = true;
            this.gv健康体检.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.GroupRow.Options.UseFont = true;
            this.gv健康体检.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.HeaderPanel.Options.UseFont = true;
            this.gv健康体检.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gv健康体检.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.HorzLine.Options.UseFont = true;
            this.gv健康体检.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.OddRow.Options.UseFont = true;
            this.gv健康体检.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.Preview.Options.UseFont = true;
            this.gv健康体检.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.Row.Options.UseFont = true;
            this.gv健康体检.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.RowSeparator.Options.UseFont = true;
            this.gv健康体检.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.SelectedRow.Options.UseFont = true;
            this.gv健康体检.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.TopNewRow.Options.UseFont = true;
            this.gv健康体检.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.VertLine.Options.UseFont = true;
            this.gv健康体检.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gv健康体检.Appearance.ViewCaption.Options.UseFont = true;
            this.gv健康体检.ColumnPanelRowHeight = 30;
            this.gv健康体检.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col体检日期,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.col创建日期,
            this.col血红蛋白,
            this.col白细胞,
            this.col血小板,
            this.col尿蛋白,
            this.col尿糖,
            this.col尿酮体,
            this.col尿潜血,
            this.col血清谷丙转氨酶,
            this.col血清谷草转氨酶,
            this.col白蛋白,
            this.col总胆红素,
            this.col结合胆红素,
            this.col血清肌酐,
            this.col血尿素氮,
            this.col血钾浓度,
            this.col血钠浓度,
            this.col总胆固醇,
            this.col甘油三酯,
            this.col血清低密度脂蛋白胆固醇,
            this.col血清高密度脂蛋白胆固醇,
            this.col空腹血糖,
            this.col餐后2H血糖,
            this.col血压右侧1,
            this.col血压右侧2,
            this.col血压左侧1,
            this.col血压左侧2,
            this.col心电图,
            this.col心电图异常});
            this.gv健康体检.GridControl = this.gc健康体检;
            this.gv健康体检.GroupPanelText = "DragColumn";
            this.gv健康体检.Name = "gv健康体检";
            this.gv健康体检.OptionsSelection.MultiSelect = true;
            this.gv健康体检.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gv健康体检.OptionsView.ColumnAutoWidth = false;
            this.gv健康体检.OptionsView.EnableAppearanceEvenRow = true;
            this.gv健康体检.OptionsView.EnableAppearanceOddRow = true;
            this.gv健康体检.OptionsView.ShowGroupPanel = false;
            // 
            // col体检日期
            // 
            this.col体检日期.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col体检日期.AppearanceHeader.Options.UseFont = true;
            this.col体检日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col体检日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col体检日期.Caption = "体检日期";
            this.col体检日期.FieldName = "体检日期";
            this.col体检日期.Name = "col体检日期";
            this.col体检日期.Visible = true;
            this.col体检日期.VisibleIndex = 1;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "档案号";
            this.gridColumn1.ColumnEdit = this.link档案号;
            this.gridColumn1.FieldName = "个人档案编号";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 2;
            // 
            // link档案号
            // 
            this.link档案号.AutoHeight = false;
            this.link档案号.Name = "link档案号";
            this.link档案号.Click += new System.EventHandler(this.link档案号_Click);
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "姓名";
            this.gridColumn2.FieldName = "姓名";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 3;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "性别";
            this.gridColumn3.FieldName = "性别";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 4;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "出生日期";
            this.gridColumn4.FieldName = "出生日期";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 5;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn5.AppearanceHeader.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "居住地址";
            this.gridColumn5.FieldName = "居住地址";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 6;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn6.AppearanceHeader.Options.UseFont = true;
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "身份证号";
            this.gridColumn6.FieldName = "身份证号";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 7;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn7.AppearanceHeader.Options.UseFont = true;
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.Caption = "联系电话";
            this.gridColumn7.FieldName = "本人电话";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 8;
            // 
            // col创建日期
            // 
            this.col创建日期.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col创建日期.AppearanceHeader.Options.UseFont = true;
            this.col创建日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col创建日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col创建日期.Caption = "创建日期";
            this.col创建日期.FieldName = "创建时间";
            this.col创建日期.Name = "col创建日期";
            this.col创建日期.Visible = true;
            this.col创建日期.VisibleIndex = 9;
            // 
            // col血红蛋白
            // 
            this.col血红蛋白.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col血红蛋白.AppearanceHeader.Options.UseFont = true;
            this.col血红蛋白.Caption = "血红蛋白";
            this.col血红蛋白.FieldName = "血红蛋白";
            this.col血红蛋白.Name = "col血红蛋白";
            // 
            // col白细胞
            // 
            this.col白细胞.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col白细胞.AppearanceHeader.Options.UseFont = true;
            this.col白细胞.Caption = "白细胞";
            this.col白细胞.FieldName = "白细胞";
            this.col白细胞.Name = "col白细胞";
            // 
            // col血小板
            // 
            this.col血小板.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col血小板.AppearanceHeader.Options.UseFont = true;
            this.col血小板.Caption = "血小板";
            this.col血小板.FieldName = "血小板";
            this.col血小板.Name = "col血小板";
            // 
            // col尿蛋白
            // 
            this.col尿蛋白.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col尿蛋白.AppearanceHeader.Options.UseFont = true;
            this.col尿蛋白.Caption = "尿蛋白";
            this.col尿蛋白.FieldName = "尿蛋白";
            this.col尿蛋白.Name = "col尿蛋白";
            // 
            // col尿糖
            // 
            this.col尿糖.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col尿糖.AppearanceHeader.Options.UseFont = true;
            this.col尿糖.Caption = "尿糖";
            this.col尿糖.FieldName = "尿糖";
            this.col尿糖.Name = "col尿糖";
            // 
            // col尿酮体
            // 
            this.col尿酮体.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col尿酮体.AppearanceHeader.Options.UseFont = true;
            this.col尿酮体.Caption = "尿酮体";
            this.col尿酮体.FieldName = "尿酮体";
            this.col尿酮体.Name = "col尿酮体";
            // 
            // col尿潜血
            // 
            this.col尿潜血.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col尿潜血.AppearanceHeader.Options.UseFont = true;
            this.col尿潜血.Caption = "尿潜血";
            this.col尿潜血.FieldName = "尿潜血";
            this.col尿潜血.Name = "col尿潜血";
            // 
            // col血清谷丙转氨酶
            // 
            this.col血清谷丙转氨酶.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col血清谷丙转氨酶.AppearanceHeader.Options.UseFont = true;
            this.col血清谷丙转氨酶.Caption = "血清谷丙转氨酶";
            this.col血清谷丙转氨酶.FieldName = "血清谷丙转氨酶";
            this.col血清谷丙转氨酶.Name = "col血清谷丙转氨酶";
            // 
            // col血清谷草转氨酶
            // 
            this.col血清谷草转氨酶.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col血清谷草转氨酶.AppearanceHeader.Options.UseFont = true;
            this.col血清谷草转氨酶.Caption = "血清谷草转氨酶";
            this.col血清谷草转氨酶.FieldName = "血清谷草转氨酶";
            this.col血清谷草转氨酶.Name = "col血清谷草转氨酶";
            // 
            // col白蛋白
            // 
            this.col白蛋白.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col白蛋白.AppearanceHeader.Options.UseFont = true;
            this.col白蛋白.Caption = "白蛋白";
            this.col白蛋白.FieldName = "白蛋白";
            this.col白蛋白.Name = "col白蛋白";
            // 
            // col总胆红素
            // 
            this.col总胆红素.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col总胆红素.AppearanceHeader.Options.UseFont = true;
            this.col总胆红素.Caption = "总胆红素";
            this.col总胆红素.FieldName = "总胆红素";
            this.col总胆红素.Name = "col总胆红素";
            // 
            // col结合胆红素
            // 
            this.col结合胆红素.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col结合胆红素.AppearanceHeader.Options.UseFont = true;
            this.col结合胆红素.Caption = "结合胆红素";
            this.col结合胆红素.FieldName = "结合胆红素";
            this.col结合胆红素.Name = "col结合胆红素";
            // 
            // col血清肌酐
            // 
            this.col血清肌酐.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col血清肌酐.AppearanceHeader.Options.UseFont = true;
            this.col血清肌酐.Caption = "血清肌酐";
            this.col血清肌酐.FieldName = "血清肌酐";
            this.col血清肌酐.Name = "col血清肌酐";
            // 
            // col血尿素氮
            // 
            this.col血尿素氮.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col血尿素氮.AppearanceHeader.Options.UseFont = true;
            this.col血尿素氮.Caption = "血尿素氮";
            this.col血尿素氮.FieldName = "血尿素氮";
            this.col血尿素氮.Name = "col血尿素氮";
            // 
            // col血钾浓度
            // 
            this.col血钾浓度.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col血钾浓度.AppearanceHeader.Options.UseFont = true;
            this.col血钾浓度.Caption = "血钾浓度";
            this.col血钾浓度.FieldName = "血钾浓度";
            this.col血钾浓度.Name = "col血钾浓度";
            // 
            // col血钠浓度
            // 
            this.col血钠浓度.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col血钠浓度.AppearanceHeader.Options.UseFont = true;
            this.col血钠浓度.Caption = "血钠浓度";
            this.col血钠浓度.FieldName = "血钠浓度";
            this.col血钠浓度.Name = "col血钠浓度";
            // 
            // col总胆固醇
            // 
            this.col总胆固醇.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col总胆固醇.AppearanceHeader.Options.UseFont = true;
            this.col总胆固醇.Caption = "总胆固醇";
            this.col总胆固醇.FieldName = "总胆固醇";
            this.col总胆固醇.Name = "col总胆固醇";
            // 
            // col甘油三酯
            // 
            this.col甘油三酯.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col甘油三酯.AppearanceHeader.Options.UseFont = true;
            this.col甘油三酯.Caption = "甘油三酯";
            this.col甘油三酯.FieldName = "甘油三酯";
            this.col甘油三酯.Name = "col甘油三酯";
            // 
            // col血清低密度脂蛋白胆固醇
            // 
            this.col血清低密度脂蛋白胆固醇.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col血清低密度脂蛋白胆固醇.AppearanceHeader.Options.UseFont = true;
            this.col血清低密度脂蛋白胆固醇.Caption = "血清低密度脂蛋白胆固醇";
            this.col血清低密度脂蛋白胆固醇.FieldName = "血清低密度脂蛋白胆固醇";
            this.col血清低密度脂蛋白胆固醇.Name = "col血清低密度脂蛋白胆固醇";
            // 
            // col血清高密度脂蛋白胆固醇
            // 
            this.col血清高密度脂蛋白胆固醇.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col血清高密度脂蛋白胆固醇.AppearanceHeader.Options.UseFont = true;
            this.col血清高密度脂蛋白胆固醇.Caption = "血清高密度脂蛋白胆固醇";
            this.col血清高密度脂蛋白胆固醇.FieldName = "血清高密度脂蛋白胆固醇";
            this.col血清高密度脂蛋白胆固醇.Name = "col血清高密度脂蛋白胆固醇";
            // 
            // col空腹血糖
            // 
            this.col空腹血糖.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col空腹血糖.AppearanceHeader.Options.UseFont = true;
            this.col空腹血糖.Caption = "空腹血糖";
            this.col空腹血糖.FieldName = "空腹血糖";
            this.col空腹血糖.Name = "col空腹血糖";
            // 
            // col餐后2H血糖
            // 
            this.col餐后2H血糖.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col餐后2H血糖.AppearanceHeader.Options.UseFont = true;
            this.col餐后2H血糖.Caption = "餐后2H血糖";
            this.col餐后2H血糖.FieldName = "餐后2H血糖";
            this.col餐后2H血糖.Name = "col餐后2H血糖";
            // 
            // col血压右侧1
            // 
            this.col血压右侧1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col血压右侧1.AppearanceHeader.Options.UseFont = true;
            this.col血压右侧1.Caption = "血压右侧1";
            this.col血压右侧1.FieldName = "血压右侧1";
            this.col血压右侧1.Name = "col血压右侧1";
            // 
            // col血压右侧2
            // 
            this.col血压右侧2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col血压右侧2.AppearanceHeader.Options.UseFont = true;
            this.col血压右侧2.Caption = "血压右侧2";
            this.col血压右侧2.FieldName = "血压右侧2";
            this.col血压右侧2.Name = "col血压右侧2";
            // 
            // col血压左侧1
            // 
            this.col血压左侧1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col血压左侧1.AppearanceHeader.Options.UseFont = true;
            this.col血压左侧1.Caption = "血压左侧1";
            this.col血压左侧1.FieldName = "血压左侧1";
            this.col血压左侧1.Name = "col血压左侧1";
            // 
            // col血压左侧2
            // 
            this.col血压左侧2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col血压左侧2.AppearanceHeader.Options.UseFont = true;
            this.col血压左侧2.Caption = "血压左侧2";
            this.col血压左侧2.FieldName = "血压左侧2";
            this.col血压左侧2.Name = "col血压左侧2";
            // 
            // col心电图
            // 
            this.col心电图.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col心电图.AppearanceHeader.Options.UseFont = true;
            this.col心电图.Caption = "心电图";
            this.col心电图.FieldName = "心电图";
            this.col心电图.Name = "col心电图";
            // 
            // col心电图异常
            // 
            this.col心电图异常.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col心电图异常.AppearanceHeader.Options.UseFont = true;
            this.col心电图异常.Caption = "心电图异常";
            this.col心电图异常.FieldName = "心电图异常";
            this.col心电图异常.Name = "col心电图异常";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 176);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1115, 36);
            this.panelControl1.TabIndex = 17;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn查询);
            this.flowLayoutPanel1.Controls.Add(this.btn重置);
            this.flowLayoutPanel1.Controls.Add(this.btn打印);
            this.flowLayoutPanel1.Controls.Add(this.btn设置);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1111, 32);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn查询
            // 
            this.btn查询.Image = ((System.Drawing.Image)(resources.GetObject("btn查询.Image")));
            this.btn查询.Location = new System.Drawing.Point(3, 3);
            this.btn查询.Name = "btn查询";
            this.btn查询.Size = new System.Drawing.Size(75, 23);
            this.btn查询.TabIndex = 0;
            this.btn查询.Text = "查询";
            this.btn查询.Click += new System.EventHandler(this.btn查询_Click);
            // 
            // btn重置
            // 
            this.btn重置.Location = new System.Drawing.Point(84, 3);
            this.btn重置.Name = "btn重置";
            this.btn重置.Size = new System.Drawing.Size(75, 23);
            this.btn重置.TabIndex = 1;
            this.btn重置.Text = "重置";
            // 
            // btn打印
            // 
            this.btn打印.Image = ((System.Drawing.Image)(resources.GetObject("btn打印.Image")));
            this.btn打印.Location = new System.Drawing.Point(165, 3);
            this.btn打印.Name = "btn打印";
            this.btn打印.Size = new System.Drawing.Size(75, 23);
            this.btn打印.TabIndex = 3;
            this.btn打印.Text = "打印";
            this.btn打印.Click += new System.EventHandler(this.btn打印_Click);
            // 
            // btn设置
            // 
            this.btn设置.Image = ((System.Drawing.Image)(resources.GetObject("btn设置.Image")));
            this.btn设置.Location = new System.Drawing.Point(246, 3);
            this.btn设置.Name = "btn设置";
            this.btn设置.Size = new System.Drawing.Size(75, 23);
            this.btn设置.TabIndex = 4;
            this.btn设置.Text = "设置";
            this.btn设置.Visible = false;
            this.btn设置.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // tabControl
            // 
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 212);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedTabPage = this.page人次;
            this.tabControl.ShowTabHeader = DevExpress.Utils.DefaultBoolean.True;
            this.tabControl.Size = new System.Drawing.Size(1115, 304);
            this.tabControl.TabIndex = 20;
            this.tabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.page人次});
            // 
            // page人次
            // 
            this.page人次.Controls.Add(this.gc健康体检);
            this.page人次.Controls.Add(this.pagerControl1);
            this.page人次.Name = "page人次";
            this.page人次.Size = new System.Drawing.Size(1109, 275);
            this.page人次.Text = "xtraTabPage1";
            // 
            // pagerControl1
            // 
            this.pagerControl1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pagerControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pagerControl1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pagerControl1.JumpText = "跳转";
            this.pagerControl1.Location = new System.Drawing.Point(0, 241);
            this.pagerControl1.Name = "pagerControl1";
            this.pagerControl1.PageIndex = 1;
            this.pagerControl1.PageSize = 100;
            this.pagerControl1.paramters = null;
            this.pagerControl1.RecordCount = 0;
            this.pagerControl1.Size = new System.Drawing.Size(1109, 34);
            this.pagerControl1.SqlQuery = null;
            this.pagerControl1.TabIndex = 19;
            this.pagerControl1.OnPageChanged += new System.EventHandler(this.pagerControl1_OnPageChanged);
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit1_Click);
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn11.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn11.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn11.AppearanceHeader.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "档案号";
            this.gridColumn11.FieldName = "个人档案编号";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 0;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn12.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn12.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn12.AppearanceHeader.Options.UseFont = true;
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.Caption = "姓名";
            this.gridColumn12.FieldName = "姓名";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 1;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn13.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn13.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn13.AppearanceHeader.Options.UseFont = true;
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.Caption = "性别";
            this.gridColumn13.FieldName = "性别";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 2;
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn14.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn14.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn14.AppearanceHeader.Options.UseFont = true;
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.Caption = "出生日期";
            this.gridColumn14.FieldName = "出生日期";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 3;
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn15.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn15.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn15.AppearanceHeader.Options.UseFont = true;
            this.gridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.Caption = "居住地址";
            this.gridColumn15.FieldName = "居住地址";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 4;
            // 
            // gridColumn16
            // 
            this.gridColumn16.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn16.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn16.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn16.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn16.AppearanceHeader.Options.UseFont = true;
            this.gridColumn16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn16.Caption = "身份证号";
            this.gridColumn16.FieldName = "身份证号";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 5;
            // 
            // gridColumn17
            // 
            this.gridColumn17.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn17.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn17.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn17.AppearanceHeader.Options.UseFont = true;
            this.gridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.Caption = "联系电话";
            this.gridColumn17.FieldName = "本人电话";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 6;
            // 
            // frm体检回执单
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1121, 551);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl2);
            this.Name = "frm体检回执单";
            this.Text = "体检回执单(体检报告)";
            this.Load += new System.EventHandler(this.frm健康体检表_Load);
            this.Controls.SetChildIndex(this.panelControl2, 0);
            this.Controls.SetChildIndex(this.panelControl3, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.Controls.SetChildIndex(this.gcNavigator, 0);
            this.Controls.SetChildIndex(this.pnlSummary, 0);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk是否签约服务.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk是否贫困人口.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案类别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte体检时间2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte体检时间2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte体检时间1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte体检时间1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生日期2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生日期2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生日期1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生日期1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt详细地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk含下属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo合格档案.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo录入人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc健康体检)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv健康体检)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link档案号)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.page人次.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt档案编号;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.ComboBoxEdit cbo档案状态;
        private DevExpress.XtraEditors.ComboBoxEdit cbo合格档案;
        private DevExpress.XtraEditors.ComboBoxEdit cbo性别;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DataGridControl gc健康体检;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn查询;
        private DevExpress.XtraEditors.SimpleButton btn重置;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link档案号;
        private DevExpress.XtraEditors.CheckEdit chk含下属机构;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEdit机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit txt详细地址;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit村;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit镇;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraEditors.LookUpEdit cbo录入人;
        private DevExpress.XtraTab.XtraTabControl tabControl;
        private DevExpress.XtraTab.XtraTabPage page人次;
        private TActionProject.PagerControl pagerControl1;
        private DevExpress.XtraEditors.DateEdit dte出生日期2;
        private DevExpress.XtraEditors.DateEdit dte出生日期1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraEditors.DateEdit dte体检时间2;
        private DevExpress.XtraEditors.DateEdit dte体检时间1;
        private DevExpress.XtraEditors.DateEdit dte录入时间2;
        private DevExpress.XtraEditors.DateEdit dte录入时间1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraEditors.ComboBoxEdit cbo档案类别;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraEditors.SimpleButton btn打印;
        private DevExpress.XtraGrid.Views.Grid.GridView gv健康体检;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn col血红蛋白;
        private DevExpress.XtraGrid.Columns.GridColumn col白细胞;
        private DevExpress.XtraGrid.Columns.GridColumn col血小板;
        private DevExpress.XtraGrid.Columns.GridColumn col尿蛋白;
        private DevExpress.XtraGrid.Columns.GridColumn col尿糖;
        private DevExpress.XtraGrid.Columns.GridColumn col尿酮体;
        private DevExpress.XtraGrid.Columns.GridColumn col尿潜血;
        private DevExpress.XtraGrid.Columns.GridColumn col血清谷丙转氨酶;
        private DevExpress.XtraGrid.Columns.GridColumn col血清谷草转氨酶;
        private DevExpress.XtraGrid.Columns.GridColumn col白蛋白;
        private DevExpress.XtraGrid.Columns.GridColumn col总胆红素;
        private DevExpress.XtraGrid.Columns.GridColumn col结合胆红素;
        private DevExpress.XtraGrid.Columns.GridColumn col血清肌酐;
        private DevExpress.XtraGrid.Columns.GridColumn col血尿素氮;
        private DevExpress.XtraGrid.Columns.GridColumn col血钾浓度;
        private DevExpress.XtraGrid.Columns.GridColumn col血钠浓度;
        private DevExpress.XtraGrid.Columns.GridColumn col总胆固醇;
        private DevExpress.XtraGrid.Columns.GridColumn col甘油三酯;
        private DevExpress.XtraGrid.Columns.GridColumn col血清低密度脂蛋白胆固醇;
        private DevExpress.XtraGrid.Columns.GridColumn col血清高密度脂蛋白胆固醇;
        private DevExpress.XtraGrid.Columns.GridColumn col空腹血糖;
        private DevExpress.XtraGrid.Columns.GridColumn col餐后2H血糖;
        private DevExpress.XtraGrid.Columns.GridColumn col血压右侧1;
        private DevExpress.XtraGrid.Columns.GridColumn col血压右侧2;
        private DevExpress.XtraGrid.Columns.GridColumn col血压左侧1;
        private DevExpress.XtraGrid.Columns.GridColumn col血压左侧2;
        private DevExpress.XtraGrid.Columns.GridColumn col心电图;
        private DevExpress.XtraGrid.Columns.GridColumn col心电图异常;
        private DevExpress.XtraGrid.Columns.GridColumn col体检日期;
        private DevExpress.XtraGrid.Columns.GridColumn col创建日期;
        private DevExpress.XtraEditors.SimpleButton btn设置;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.CheckEdit chk是否签约服务;
        private DevExpress.XtraEditors.CheckEdit chk是否贫困人口;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
    }
}