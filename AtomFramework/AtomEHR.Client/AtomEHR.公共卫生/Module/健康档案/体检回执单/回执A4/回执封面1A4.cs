﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
//using cn.net.sunshine.physicalExam.modules.patient;
//using cn.net.sunshine.physicalExam;
using System.Collections.Generic;
//using cn.net.sunshine.tools;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Reflection;
using System.Text;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Common;
//using cn.net.sunshine.controls;

namespace AtomEHR.公共卫生.Module.健康档案.体检回执单.回执A4
{
    public partial class 回执封面1A4 : DevExpress.XtraReports.UI.XtraReport
    {
        string docNo;
        string date;
        string name;
        string 条码or照片名 ="";
        
        public 回执封面1A4()
        {
            InitializeComponent();
        }

        public 回执封面1A4(System.Collections.Generic.List<string> result)
        {
            InitializeComponent();

            this.docNo = result[9];//已建档的档案和未建档的档案和条码
            this.date = result[8];
            this.name = result[0];

            this.txt编号.Text = docNo;
            this.txt姓名.Text = name;
            this.txt性别.Text = result[1];
            this.txt身份证号.Text = result[10];
            this.txt户籍地址.Text = result[3];
            this.txt单位.Text = result[5];
            txt前言称谓.Text = "尊敬的" + name + "：";
            this.txt体检日期.Text = date;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_docNo">个人档案号或者为建档条码</param>
        /// <param name="_date"></param>
        public 回执封面1A4(string _docNo, string _date, string _name)
        {
            InitializeComponent();

            this.docNo = _docNo;//已建档的档案和未建档的档案和条码
            this.date = _date;
            this.name = _name;

            txt前言称谓.Text = "尊敬的" + name + "：";

            //1.已建档
            if (!string.Equals("0", _docNo.Substring(0, 1)))
            {
                //this.条码or照片名 = 健康档案服务.获取条码by档案号(_docNo);

                //1.1 健康档案 获取基本信息并绑定
                //tb_健康档案 健康档案bean = new tb_健康档案();
                //健康档案bean.个人档案编号 = docNo;
                //List<tb_健康档案> tb_健康档案List = 健康档案服务.获取健康档案(健康档案bean);

                //1.1.1 获取到健康档案
                //if (tb_健康档案List.Count > 0)
                //{
                //    bindUserInfo(tb_健康档案List[0]);
                //}
                ////1.1.2 未获取到健康档案  按照未建档处理
                //else 
                //{
                //    bindUserInfoNoDoc(条码or照片名);
                //}
            }
            //2.未建档
            else
            {
                条码or照片名 = _docNo;

                //2.1 个人信息
                bindUserInfoNoDoc(条码or照片名);                
            }
        }
        
        private void bindPic() 
        {
            //#region 获取照片路径
            //string 照片路径 = "";
            //// 配置的图片路径
            //string photoBasePath = Util.GetConfigValue("photoBasePath", Application.ProductName + ".exe");

            //// 默认路径
            //if ("default".Equals(photoBasePath))
            //{
            //    照片路径 = Application.StartupPath + "\\photo\\" + 条码or照片名 + ".jpeg";
            //}
            //// 自定义路径
            //else
            //{
            //    照片路径 = photoBasePath + "\\" + 条码or照片名;
            //}
            //#endregion

            //this.pic照片.ImageUrl = 照片路径;
        }

        /// <summary>
        /// 无档案 个人信息
        /// </summary>
        private void bindUserInfoNoDoc(string  条码)
        {
            bindPic();

            //tb_移动查体记录 bean = new tb_移动查体记录();
            //bean.条码 = 条码;
            //List<tb_移动查体记录> tb_移动查体记录List = 健康档案服务.获取tb_移动查体记录根据条码(bean);

            //if (tb_移动查体记录List.Count > 0)
            //{
            //    tb_移动查体记录 查体bean = tb_移动查体记录List[0];
            //    this.txt编号.Text = docNo;
            //    this.txt姓名.Text = name;//健康档案bean.姓名;//是否需要解密
            //    this.txt性别.Text = 查体bean.性别;
            //    this.txt身份证号.Text = 查体bean.身份证号;
            //    this.txt户籍地址.Text = 查体bean.住址;

            //    //txt体检单位.Text = "沂南县清驼卫生院";
            //    //txt体检日期.Text = this.date;
            //}
        }
  
        /// <summary>
        /// 有档案 个人信息
        /// </summary>
        //private void bindUserInfo(tb_健康档案 健康档案bean)
        //{
        //    bindPic();

        //    this.txt编号.Text = docNo;
        //    this.txt姓名.Text = name;//健康档案bean.姓名;//是否需要解密
        //    this.txt性别.Text = 健康档案bean.性别 == "1" ? "男" : "女";
        //    this.txt身份证号.Text = 健康档案bean.身份证号;
        //    this.txt户籍地址.Text = 健康档案bean.居住地址;//是否拼成正规的户籍地址

        //    //this.txt联系电话1.Text = 健康档案bean.本人电话;
        //    //this.txt联系电话2.Text = 健康档案bean.联系人电话;

        //    //txt体检单位.Text = "沂南县清驼卫生院";
        //    //txt体检日期.Text = this.date;
        //}


        //Begin WXF 2018-11-04 | 01:43
        //根据档案号绑定数据 
        public 回执封面1A4(string str_recordNum)
        {
            InitializeComponent();

            bll健康档案 bll_健康档案 = new bll健康档案();

            DataSet ds_健康档案 = bll_健康档案.Get_健康档案封面(str_recordNum);
            DataRow dr_健康档案 = ds_健康档案.Tables[tb_健康档案.__TableName].Rows[0];
            DataTable dt_图像采集 = ds_健康档案.Tables["tb_健康体检_图像采集"];            

            txt编号.Text = dr_健康档案[tb_健康档案.个人档案编号].ToString();
            txt姓名.Text = dr_健康档案[tb_健康档案.姓名].ToString();
            txt性别.Text = dr_健康档案[tb_健康档案.性别].ToString();
            txt身份证号.Text = dr_健康档案[tb_健康档案.身份证号].ToString();
            txt户籍地址.Text = dr_健康档案[tb_健康档案.区].ToString() + dr_健康档案[tb_健康档案.街道].ToString() + dr_健康档案[tb_健康档案.居委会].ToString() + dr_健康档案[tb_健康档案.居住地址].ToString();
            txt前言称谓.Text = dr_健康档案[tb_健康档案.姓名].ToString();
            txt单位.Text = dr_健康档案[tb_健康档案.所属机构].ToString();
            if (ds_健康档案.Tables[tb_健康体检.__TableName] != null && ds_健康档案.Tables[tb_健康体检.__TableName].Rows.Count >= 1)
            {
                txt体检日期.Text = ds_健康档案.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.体检日期].ToString();
            }

            if (dt_图像采集 != null && dt_图像采集.Rows.Count >= 1)
            {
                var img = dt_图像采集.Rows[0]["图像"];
                if (img != null)
                {
                    System.Drawing.Image img健康评价 = (Image)ZipTools.DecompressionObject((byte[])img);
                    this.pic照片.Image = img健康评价;
                }
            }
        }
        //End
											 

    }
}
