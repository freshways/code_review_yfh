﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
//using cn.net.sunshine.physicalExam.modules.patient;
//using cn.net.sunshine.physicalExam;
using System.Collections.Generic;
//using cn.net.sunshine.physicalExam.modules.ecg;
using System.Xml;
//using cn.net.sunshine.tools;
using System.IO;
using System.Windows.Forms;

//using System.ComponentModel;

using System.Data;
using System.Text;
using System.Linq;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using DevExpress.XtraPrinting;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Common;
//using cn.net.sunshine.physicalExam.modules.common;

namespace AtomEHR.公共卫生.Module.健康档案.体检回执单.回执A4
{
    public partial class 回执B超 : DevExpress.XtraReports.UI.XtraReport
    {
        public 回执B超()
        {
            InitializeComponent();
        }

        public 回执B超(DataRow dr,string yishi,string yiyuan)
        {
            InitializeComponent();

            this.txt卫生院.Text = string.Format("{0}卫生院", yiyuan);
            this.txt档案号.Text = dr["个人档案编号"].ToString();
            this.txt姓名.Text = dr["姓名"].ToString();
            this.txt性别.Text = dr["性别"].ToString();
            int age = 0;
            try
            {
                TimeSpan sp = DateTime.Now.Subtract(Convert.ToDateTime(dr["出生日期"].ToString()));
                age = sp.Days / 365;
            }
            catch
            {
                
            }
            this.txt年龄.Text = age.ToString();
            this.txt身份证.Text = dr["身份证号"].ToString();
            this.txt体检日期.Text = dr["体检日期"].ToString();
            this.txt医师.Text = yishi;
            
        }


        //Begin WXF 2018-11-04 | 02:33
        //根据档案号与体检日期打印
        public 回执B超(string str_recordNum, string str_CreateDT)
        {
            InitializeComponent();

            DataRow dr_健康档案 = new bll健康档案().Get_健康档案封面(str_recordNum).Tables[tb_健康档案.__TableName].Rows[0];
            
            
            this.txt卫生院.Text = string.Format("{0}卫生院", dr_健康档案[tb_健康档案.街道]);
            this.txt档案号.Text = str_recordNum;
            this.txt姓名.Text = dr_健康档案[tb_健康档案.姓名].ToString();
            this.txt性别.Text = dr_健康档案[tb_健康档案.性别].ToString();
            int age = 0;
            try
            {
                TimeSpan sp = DateTime.Now.Subtract(Convert.ToDateTime(dr_健康档案[tb_健康档案.出生日期]));
                age = sp.Days / 365;
            }
            catch
            {

            }
            DataTable dt_B超报告 = new bll健康体检().Get_B超报告(str_recordNum, str_CreateDT);
            if (dt_B超报告 != null && dt_B超报告.Rows.Count > 0)
            {
                this.txt超声所见.Text = string.Format(txt超声所见.Text, dt_B超报告.Rows[0]["超声所见"]);
                this.txt超声提示.Text = string.Format(txt超声提示.Text, dt_B超报告.Rows[0]["超声提示"]);

            }
            else
            {
                this.txt超声所见.Text = string.Format(txt超声所见.Text, @"肝脏形态大小正常，被膜光整，实质内回声均匀，门脉及肝内外

   胆管无扩张。

	胆囊形态大小正常，壁光滑，内透声好。

	胰腺形态大小正常，实质内回声均匀，主胰管无扩张。

	脾脏形态大小正常，被膜光整，实质内回声均匀。");
                this.txt超声提示.Text = string.Format(txt超声提示.Text, @"肝胆胰脾未见明显异常");
            }
            
            this.txt年龄.Text = age.ToString();
            this.txt身份证.Text = dr_健康档案[tb_健康档案.身份证号].ToString();
            //this.txt医师.Text = "";//目前统合打印不能绑定B超医师
            this.txt体检日期.Text = str_CreateDT;
            try
            {
                bll医生信息 bll手签 = new bll医生信息();
                //string RGID = ds体检.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.所属机构].ToString();
                DataTable dt = bll手签.GetSummaryData(false);

                System.Drawing.Image imgB超 = (Image)ZipTools.DecompressionObject((byte[])dt.Select("c查体模块='B超' ")[0]["s手签"]);
                this.xrPictureBoxB超医生.Image = imgB超;

            }
            catch (Exception ex)
            {
            }
        }
        //End
											 
        
    }
}
