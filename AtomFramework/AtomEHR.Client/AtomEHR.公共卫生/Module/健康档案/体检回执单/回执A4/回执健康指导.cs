﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
//using cn.net.sunshine.physicalExam.modules.patient;
//using cn.net.sunshine.physicalExam;
using System.Collections.Generic;
//using cn.net.sunshine.tools;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Reflection;
using System.Text;
//using cn.net.sunshine.controls;

namespace AtomEHR.公共卫生.Module.健康档案.体检回执单.回执A4
{
    public partial class 回执健康指导 : DevExpress.XtraReports.UI.XtraReport
    {
        string docNo;
        string date;
        string name;
        string 条码or照片名 ="";
        
        public 回执健康指导()
        {
            InitializeComponent();
        }

        public 回执健康指导(System.Collections.Generic.List<string> result)
        {
            InitializeComponent();
            lbName.Text = result[0];
            this.txt症状.Text = result[11];
            this.txt健康指导.Text = result[7];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_docNo">个人档案号或者为建档条码</param>
        /// <param name="_date"></param>
        public 回执健康指导(string _docNo, string _date, string _name)
        {
            InitializeComponent();

            this.docNo = _docNo;//已建档的档案和未建档的档案和条码
            this.date = _date;
            this.name = _name;

            lbName.Text = _name;

            ////1.已建档
            //if (!string.Equals("0", _docNo.Substring(0, 1)))
            //{
            //    this.条码or照片名 = 健康档案服务.获取条码by档案号(_docNo);

            //    //1.1 健康档案 获取基本信息并绑定
            //    tb_健康档案 健康档案bean = new tb_健康档案();
            //    健康档案bean.个人档案编号 = docNo;
            //    List<tb_健康档案> tb_健康档案List = 健康档案服务.获取健康档案(健康档案bean);

            //    //1.1.1 获取到健康档案
            //    if (tb_健康档案List.Count > 0)
            //    {
            //        //bindUserInfo(tb_健康档案List[0]);
            //    }
            //    //1.1.2 未获取到健康档案  按照未建档处理
            //    else 
            //    {
            //        //bindUserInfoNoDoc(条码or照片名);
            //    }

            //    //1.2体检视图 获取体检并绑定
            //    vw_健康体检 bean = new vw_健康体检();
            //    bean.个人档案编号 = _docNo;
            //    bean.体检日期 = _date;
            //    List<vw_健康体检> vw_健康体检List = 健康档案服务.获取健康体检(bean);

            //    //1.2.1 获取到体检视图
            //    if (vw_健康体检List.Count > 0)
            //    {
            //        vw_健康体检 体检bean = vw_健康体检List[0];
            //        bind体检信息(体检bean);
            //    }
            //    else //1.2.2 未获取到体检视图
            //    {
            //        bind体检信息NoDoc(条码or照片名);
            //    }
            //}
            ////2.未建档
            //else
            //{
            //    条码or照片名 = _docNo;

            //    //2.1 个人信息
            //    //bindUserInfoNoDoc(条码or照片名);                

            //    //2.2 体检信息
            //    bind体检信息NoDoc(条码or照片名);  
            //}
        }

        //private vw_健康体检 getBeanByRow(DataRow row)
        //{
        //    vw_健康体检 体检bean = new vw_健康体检();
        //    //this.currentrow = row;
        //    //this.textEdit姓名.Text = currentrow["姓名"].ToString();
        //    //this.textEdit性别.Text = currentrow["性别"].ToString();
        //    //this.textEdit年龄.Text = AtomEHR.公共卫生.util.ControlsHelper.GetAge(currentrow["出生日期"].ToString(), new bll健康体检().ServiceDateTime).ToString();
        //    //this.textEdit村庄.Text = currentrow["居委会"].ToString();
        //    体检bean.血红蛋白 = row["血红蛋白"].ToString();
        //    体检bean.白细胞 = row["白细胞"].ToString();
        //    体检bean.血小板 = row["血小板"].ToString();
        //    体检bean.尿蛋白= row["尿蛋白"].ToString();
        //    体检bean .尿酮体= row["尿酮体"].ToString();
        //    体检bean .尿糖= row["尿糖"].ToString();
        //    体检bean.尿潜血 = row["尿潜血"].ToString();
        //    体检bean.血清谷丙转氨酶 = row["血清谷丙转氨酶"].ToString();
        //    体检bean.血清谷草转氨酶 = row["血清谷草转氨酶"].ToString();
        //    体检bean .白蛋白= row["白蛋白"].ToString();
        //    体检bean .总胆红素= row["总胆红素"].ToString();
        //    体检bean.结合胆红素 = row["结合胆红素"].ToString();
        //    体检bean .血清肌酐= row["血清肌酐"].ToString();
        //    体检bean.血尿素氮 = row["血尿素氮"].ToString();
        //    体检bean .血钾浓度= row["血钾浓度"].ToString();
        //    体检bean.血钠浓度 = row["血钠浓度"].ToString();
        //    体检bean.空腹血糖 = row["空腹血糖"].ToString();
        //    体检bean .餐后2H血糖= row["餐后2H血糖"].ToString();
        //    体检bean.血压左侧= row["血压左侧"].ToString();
        //    体检bean.血压右侧 = row["血压右侧"].ToString();
        //    体检bean.总胆固醇 = row["总胆固醇"].ToString();
        //    体检bean.甘油三酯 = row["甘油三酯"].ToString();
        //    体检bean.血清低密度脂蛋白胆固醇 = row["血清低密度脂蛋白胆固醇"].ToString();
        //    体检bean.血清高密度脂蛋白胆固醇  = row["血清高密度脂蛋白胆固醇"].ToString();
        //    体检bean.心电图 = row["心电图"].ToString();
        //    体检bean.心电图异常 = row["心电图异常"].ToString();
        //    体检bean.体重指数 = row["体重指数"].ToString();
        //    体检bean.平和质  = row["平和质"].ToString();
        //    体检bean.气虚质  = row["气虚质"].ToString();
        //    体检bean.阳虚质 = row["阳虚质"].ToString();
        //    体检bean.阴虚质  = row["阴虚质"].ToString();
        //    体检bean.痰湿质  = row["痰湿质"].ToString();
        //    体检bean.湿热质 = row["湿热质"].ToString();
        //    体检bean.血瘀质  = row["血瘀质"].ToString();
        //    体检bean.气郁质  = row["气郁质"].ToString();
        //    体检bean.特禀质  = row["特禀质"].ToString();
        //    体检bean.B超  = row["B超"].ToString();
        //    体检bean.B超其他 = row["B超其他"].ToString();

        //    return 体检bean;
        //}

        //private void Calculate(vw_健康体检 体检bean)
        //{
        //    StringBuilder 症状temp = new StringBuilder();
        //    StringBuilder 健康指导temp = new StringBuilder();

        //    #region BMI
        //    //BMI
        //    /*
        //        过轻：低于18.5
        //        正常：18.5-23.9
        //        超重：≥24
        //     * 24～27.9 偏胖
        //        肥胖：28-32
        //        非常肥胖, 高于32
        //     */
        //    if (!string.IsNullOrEmpty(体检bean.体重指数))
        //    {
        //        double bmi = double.Parse(体检bean.体重指数);
        //        if (bmi < 18.5)
        //        {
        //            症状temp.Append("【体重偏轻（体重指数：" + bmi + ")" + ", 】");
        //            健康指导temp.Append( "体重偏轻异常，建议：补充营养。\n");
        //            //builder2.AppendLine("体重偏轻异常，建议：");
        //        }
        //        else if (bmi >= 24)
        //        {
        //            症状temp.Append("【体重超重（体重指数：" + bmi + ")" + ", 】");
        //            健康指导temp.Append("体重超重异常，建议：少食，减盐，常称体重。\n");
        //            //builder2.AppendLine("体重超重异常，建议：");
        //        }
        //        else if (bmi >= 24 && bmi < 27.9)
        //        {
        //            症状temp.Append("【体重偏胖（体重指数：" + bmi + ")" + ", 】");
        //            健康指导temp.Append("体重偏胖异常，建议：少食，减盐，常称体重。\n");
        //            //builder2.AppendLine("体重偏胖异常，建议：");
        //        }
        //        else if (bmi >= 28 && bmi < 32)
        //        {
        //            症状temp.Append("【体重肥胖（体重指数：" + bmi + ")" + ", 】");
        //            健康指导temp.Append("体重肥胖异常，建议：少食，减盐，常称体重。\n");
        //            //builder2.AppendLine("体重肥胖异常，建议：");
        //        }
        //        else if (bmi > 32)
        //        {
        //            症状temp.Append("【体重非常肥胖（体重指数：" + bmi + ")" + ", 】");
        //            健康指导temp.Append("体重非常肥胖异常，建议：少食，减盐，常称体重。\n");
        //            //builder2.AppendLine("体重非常肥胖异常，建议：");
        //        }
        //    }

        //    #endregion

        //    #region 糖尿病

        //    if (!string.IsNullOrEmpty(体检bean.空腹血糖))
        //    {
        //        string xuetang = 体检bean.空腹血糖;
        //        decimal _xuetang = 0m;
        //        if (Decimal.TryParse(xuetang, out _xuetang))
        //        {
        //            if (_xuetang != 0m && (_xuetang > 6.40m || _xuetang < 3.89m))
        //            {
        //                症状temp.Append("【糖尿病(GLU:" + _xuetang + "mmol/l)（3.89-6.40）" + ", 】");
        //                健康指导temp.Append("空腹血糖偏高，建议：尽早到内分泌专科进一步检查确认治疗。并注意低糖、低盐、低脂饮食，控制体重，防止并发症的发生。建议做葡萄糖检验，以确认有无糖尿病或糖尿量正常。可取生茅根60-90g水煎代茶饮每日一剂，连服10日。\n");
        //            }

        //        }
        //    }

        //    #endregion

        //    #region 血压

        //    if (!string.IsNullOrEmpty(体检bean.血压右侧) || !string.IsNullOrEmpty(体检bean.血压左侧 ))
        //    {
        //        //右侧
        //        if (!string.IsNullOrEmpty(体检bean.血压右侧))
        //        {
        //            string xueya = 体检bean.血压右侧.Trim();
        //            string you1 = xueya.Split('/')[0];
        //            string you2 = xueya.Split('/')[1];
        //            int _you1; int _you2;
        //            if (int.TryParse(you1, out _you1) && int.TryParse(you2, out _you2))
        //            {
        //                if (_you1 > 140 || _you2 > 90)
        //                {
        //                    症状temp.Append("【高血压（" + xueya + ")" + ", 】");
        //                    健康指导temp.Append("血压高偏高，建议：①减轻并控制体重。②减少钠盐摄入。③补充钙和钾盐。④减少脂肪摄入。⑤增加运动。⑥戒烟、限制饮酒。⑦减轻精神压力，保持心理平衡。可取金银花、菊花各26g，每日一剂，分4份用沸水冲泡代饮，头晕明显者可加桑叶12g，若动脉硬化、血脂高者加山楂24-30g，冲泡2次弃掉另换。\n");
        //                }
        //            }
        //        }
        //        //左侧
        //        else if (!string.IsNullOrEmpty(体检bean.血压左侧))
        //        {
        //            string xueya = 体检bean.血压左侧.Trim();
        //            string you1 = xueya.Split('/')[0];
        //            string you2 = xueya.Split('/')[1];
        //            int _you1; int _you2;
        //            if (int.TryParse(you1, out _you1) && int.TryParse(you2, out _you2))
        //            {
        //                if (_you1 > 140 || _you2 > 90)
        //                {
        //                    症状temp.Append("【高血压（" + xueya + ")" + ", 】");
        //                    // this.txt结论建议.Document.AppendText("血压高偏高，建议：①减轻并控制体重。②减少钠盐摄入。③补充钙和钾盐。④减少脂肪摄入。⑤增加运动。⑥戒烟、限制饮酒。⑦减轻精神压力，保持心理平衡。可取金银花、菊花各26g，每日一剂，分4份用沸水冲泡代饮，头晕明显者可加桑叶12g，若动脉硬化、血脂高者加山楂24-30g，冲泡2次弃掉另换。");
        //                }
        //            }
        //        }
        //    }

        //    #endregion

        //    #region 血常规
        //    /*
        //     白细胞：3.9-9.9
        //     血红蛋白：116-179
        //     血小板：98.7-302.9
        //     */
        //    if (!string.IsNullOrEmpty(体检bean.白细胞 ) || !string.IsNullOrEmpty(体检bean.血红蛋白 ) || !string.IsNullOrEmpty(体检bean.血小板 ))
        //    {
        //        string baixibao = 体检bean.白细胞 .Trim();
        //        string xuehongdanbai = 体检bean.血红蛋白 .Trim();
        //        string xuexiaoban = 体检bean.血小板 .Trim();
        //        decimal _baixibao;
        //        decimal _xuehongdanbai;
        //        decimal _xuexiaoban;
        //        string text = "【血常规异常：";
        //        if (decimal.TryParse(baixibao, out _baixibao))
        //        {
        //            if (_baixibao > 9.9m || _baixibao < 3.9m)
        //            {
        //                text += "白细胞:" + _baixibao + "(3.9-9.9)" + ", ";
        //            }
        //        }
        //        if (decimal.TryParse(xuehongdanbai, out _xuehongdanbai))
        //        {
        //            if (_xuehongdanbai > 179m || _xuehongdanbai < 116m)
        //            {
        //                text += "血红蛋白:" + _xuehongdanbai + "(116-179)" + ", ";
        //            }
        //        }
        //        if (decimal.TryParse(xuexiaoban, out _xuexiaoban))
        //        {
        //            if (_xuexiaoban > 302.9m || _xuexiaoban < 98.7m)
        //            {
        //                text += "血小板:" + _xuexiaoban + "(98.7-302.9)" + ", ";
        //            }
        //        }
        //        if (text != "血常规异常：")
        //        {
        //            症状temp.Append(text + ", 】");
        //            健康指导temp.Append("血常规异常，建议：医院复查。\n");
        //        }
        //    }

        //    #endregion

        //    #region 尿常规
        //    if (!string.IsNullOrEmpty(体检bean.尿蛋白 ) || !string.IsNullOrEmpty(体检bean.尿酮体 ) || !string.IsNullOrEmpty(体检bean.尿潜血 ) || !string.IsNullOrEmpty(体检bean.尿糖 ))
        //    {
        //        string text = "【尿常规异常：";
        //        if (!string.IsNullOrEmpty(体检bean.尿蛋白 ) && 体检bean.尿蛋白 .Trim() != "-")
        //        {
        //            text += "尿蛋白:" + 体检bean.尿蛋白  + ", ";
        //        }
        //        if (!string.IsNullOrEmpty(体检bean.尿酮体 ) && 体检bean.尿酮体 .Trim() != "-")
        //        {
        //            text += "尿酮体:" + 体检bean.尿酮体  + ", ";
        //        }
        //        if (!string.IsNullOrEmpty(体检bean.尿潜血 ) && 体检bean.尿潜血 .Trim() != "-")
        //        {
        //            text += "尿潜血:" + 体检bean.尿潜血  + ", ";
        //        }
        //        if (!string.IsNullOrEmpty(体检bean.尿糖 ) && 体检bean.尿糖 .Trim() != "-")
        //        {
        //            text += "尿糖:" + 体检bean.尿糖  + ", ";
        //        }
        //        if (text != "尿常规异常：")
        //        {
        //            症状temp.Append(text + ", 】");
        //            健康指导temp.Append("尿常规异常，建议：医院复查。\n");
        //        }
        //    }
        //    #endregion

        //    #region 肝功
        //    /*
        //     血清谷丙转氨酶：0-40
        //     血清谷草转氨酶：0-41
        //     总胆红素：2-20.5
        //     结合胆红素：0-7.0
        //     */
        //    if (!string.IsNullOrEmpty(体检bean.血清谷丙转氨酶 ) || !string.IsNullOrEmpty(体检bean.血清谷草转氨酶 ) || !string.IsNullOrEmpty(体检bean.总胆红素 ) || !string.IsNullOrEmpty(体检bean.结合胆红素 ))
        //    {
        //        string 血清谷丙转氨酶 = 体检bean.血清谷丙转氨酶 .Trim();
        //        string 血清谷草转氨酶 = 体检bean.血清谷草转氨酶 .Trim();
        //        string 总胆红素 = 体检bean.总胆红素 .Trim();
        //        string 结合胆红素 = 体检bean.结合胆红素 .Trim();
        //        decimal _血清谷丙转氨酶;
        //        decimal _血清谷草转氨酶;
        //        decimal _总胆红素;
        //        decimal _结合胆红素;
        //        string text = "【肝功异常：";
        //        if (decimal.TryParse(血清谷丙转氨酶, out _血清谷丙转氨酶))
        //        {
        //            if (_血清谷丙转氨酶 > 40m || _血清谷丙转氨酶 < 0m)
        //            {
        //                text += "谷丙转氨酶:" + _血清谷丙转氨酶 + "(0-40)" + ", ";
        //            }
        //        }
        //        if (decimal.TryParse(血清谷草转氨酶, out _血清谷草转氨酶))
        //        {
        //            if (_血清谷草转氨酶 > 41m || _血清谷草转氨酶 < 0m)
        //            {
        //                text += "谷草转氨酶:" + _血清谷草转氨酶 + "(0-41)" + ", ";
        //            }
        //        }
        //        if (decimal.TryParse(总胆红素, out _总胆红素))
        //        {
        //            if (_总胆红素 > 20.5m || _总胆红素 < 2m)
        //            {
        //                text += "总胆红素:" + _总胆红素 + "(2-20.5)" + ", ";
        //            }
        //        }
        //        if (decimal.TryParse(结合胆红素, out _结合胆红素))
        //        {
        //            if (_结合胆红素 > 7m || _结合胆红素 < 0m)
        //            {
        //                text += "结合胆红素:" + _结合胆红素 + "(0-7.0)" + ", ";
        //            }
        //        }
        //        if (text != "肝功异常：")
        //        {
        //            症状temp.Append(text + ", 】");
        //            健康指导temp.Append(@"肝功异常，建议：1、适当运动，在体力允许的情况下，可做些散步，慢跑等运动；2、低脂肪低胆固醇饮食，多吃新鲜蔬菜水果，少吃高脂食物，如肥肉、动物内脏、油炸食品，粗细粮搭配；3、戒烟限酒；4、半年复查肝脏超声。\n");
        //        }
        //    }
        //    #endregion

        //    #region 肾功
        //    /*
        //     血清肌酐:53-115
        //     血尿素氮:1.7-8.7 
        //     */
        //    if (!string.IsNullOrEmpty(体检bean.血清肌酐 ) || !string.IsNullOrEmpty(体检bean.血尿素氮 ))
        //    {
        //        string 血清肌酐 = 体检bean.血清肌酐 .Trim();
        //        string 血尿素氮 = 体检bean.血尿素氮 .Trim();
        //        decimal _血清肌酐;
        //        decimal _血尿素氮;
        //        string text = "【肾功异常：";
        //        if (decimal.TryParse(血清肌酐, out _血清肌酐))
        //        {
        //            if (_血清肌酐 > 115m || _血清肌酐 < 53m)
        //            {
        //                text += "血清肌酐:" + _血清肌酐 + "(53-115)" + ", ";
        //            }
        //        }
        //        if (decimal.TryParse(血尿素氮, out _血尿素氮))
        //        {
        //            if (_血尿素氮 > 8.7m || _血尿素氮 < 1.7m)
        //            {
        //                text += "血尿素氮:" + _血尿素氮 + "(1.7-8.7 )" + ", ";
        //            }
        //        }

        //        if (text != "肾功异常：")
        //        {
        //            症状temp.Append(text + ", 】 ");
        //            健康指导temp.Append("肾功异常，建议：医院复查。\n");
        //        }
        //    }
        //    #endregion

        //    #region 血脂
        //    /*
        //     总胆固醇：2.34-5.20
        //     甘油三酯：0.7-1.7
        //     高密度脂蛋白：0.77-2.25
        //     低密度脂蛋白：0-4.13
        //     */
        //    if (!string.IsNullOrEmpty(体检bean.总胆固醇 ) || !string.IsNullOrEmpty(体检bean.甘油三酯 ) || !string.IsNullOrEmpty(体检bean.血清高密度脂蛋白胆固醇 ) || !string.IsNullOrEmpty(体检bean.血清低密度脂蛋白胆固醇 ))
        //    {
        //        string 总胆固醇 = 体检bean.总胆固醇 .Trim();
        //        string 甘油三酯 = 体检bean.甘油三酯 .Trim();
        //        string 高密度脂蛋白 = 体检bean.血清高密度脂蛋白胆固醇 .Trim();
        //        string 低密度脂蛋白 = 体检bean.血清低密度脂蛋白胆固醇 .Trim();
        //        decimal _总胆固醇;
        //        decimal _甘油三酯;
        //        decimal _高密度脂蛋白;
        //        decimal _低密度脂蛋白;
        //        string text = "【血脂异常：";
        //        if (decimal.TryParse(总胆固醇, out _总胆固醇))
        //        {
        //            if (_总胆固醇 > 5.2m || _总胆固醇 < 2.34m)
        //            {
        //                text += "总胆固醇:" + _总胆固醇 + "(2.34-5.20)" + ", ";
        //            }
        //        }
        //        if (decimal.TryParse(甘油三酯, out _甘油三酯))
        //        {
        //            if (_甘油三酯 > 1.7m || _甘油三酯 < 0.7m)
        //            {
        //                text += "甘油三酯:" + _甘油三酯 + "(0.7-1.7)" + ", ";
        //            }
        //        }
        //        if (decimal.TryParse(高密度脂蛋白, out _高密度脂蛋白))
        //        {
        //            if (_高密度脂蛋白 > 2.25m || _高密度脂蛋白 < 0.77m)
        //            {
        //                text += "高密度脂蛋白:" + _高密度脂蛋白 + "(0.77-2.25)" + ", ";
        //            }
        //        }
        //        if (decimal.TryParse(低密度脂蛋白, out _低密度脂蛋白))
        //        {
        //            if (_低密度脂蛋白 > 4.13m || _低密度脂蛋白 < 0m)
        //            {
        //                text += "低密度脂蛋白:" + _低密度脂蛋白 + "(0-4.13)" + ", ";
        //            }
        //        }
        //        if (text != "血脂异常：")
        //        {
        //            症状temp.Append(text + ", 】");
        //            健康指导temp.Append("血常规异常，建议：要注意清淡饮食，不吃高脂餐，控制进食量，适当运动，戒烟限酒。\n");
        //        }
        //    }
        //    #endregion

        //    #region 心电图
        //    if (!string.IsNullOrEmpty(体检bean.心电图 ) && 体检bean.心电图 .Trim() != "正常")
        //    {
        //        症状temp.Append("【心电图异常：" + 体检bean.心电图异常 .Trim() + ", 】");
        //        健康指导temp.Append("心电图异常，建议：医院复查。\n");
        //    }
        //    #endregion

        //    #region B超
        //    //if (this.体检bean.B超  != "未做")
        //    //{
        //    症状temp.Append("【B超：" + 体检bean.B超  + ",】 ");
        //    if (!string.IsNullOrEmpty(体检bean.B超其他 ))
        //    {
        //        症状temp.Append(体检bean.B超其他  + ", ");
        //    }
        //    //}
        //    #endregion

        //    if (症状temp.Length == 0)
        //    {
        //        症状temp.Append(@"体检未见明显异常" + "。 ");
        //    }
        //    #region 体质辨识
        //    if (!string.IsNullOrEmpty(体检bean.平和质 ) || !string.IsNullOrEmpty(体检bean.气虚质 ) || !string.IsNullOrEmpty(体检bean.阳虚质 ) || !string.IsNullOrEmpty(体检bean.阴虚质 ) || !string.IsNullOrEmpty(体检bean.痰湿质 ) || !string.IsNullOrEmpty(体检bean.湿热质 ) || !string.IsNullOrEmpty(体检bean.血瘀质 ) || !string.IsNullOrEmpty(体检bean.气郁质 ) || !string.IsNullOrEmpty(体检bean.特禀质 ))
        //    {
        //        症状temp.Append("【中医体质辨识结果：");
        //        if (!string.IsNullOrEmpty(体检bean.平和质 ))
        //        { 症状temp.Append("平和质：" + 体检bean.平和质  + ", "); }
        //        if (!string.IsNullOrEmpty(体检bean.气虚质 ))
        //        { 症状temp.Append("气虚质：" + 体检bean.气虚质  + ", "); }
        //        if (!string.IsNullOrEmpty(体检bean.阳虚质 ))
        //        { 症状temp.Append("阳虚质：" + 体检bean.阳虚质  + ", "); }
        //        if (!string.IsNullOrEmpty(体检bean.阴虚质 ))
        //        { 症状temp.Append("阴虚质：" + 体检bean.阴虚质  + ", "); }
        //        if (!string.IsNullOrEmpty(体检bean.痰湿质 ))
        //        { 症状temp.Append("痰湿质：" + 体检bean.痰湿质  + ", "); }
        //        if (!string.IsNullOrEmpty(体检bean.湿热质 ))
        //        { 症状temp.Append("湿热质：" + 体检bean.湿热质  + ", "); }
        //        if (!string.IsNullOrEmpty(体检bean.血瘀质 ))
        //        { 症状temp.Append("血瘀质：" + 体检bean.血瘀质  + ", "); }
        //        if (!string.IsNullOrEmpty(体检bean.气郁质 ))
        //        { 症状temp.Append("气郁质：" + 体检bean.气郁质  + ", "); }
        //        if (!string.IsNullOrEmpty(体检bean.特禀质 ))
        //        { 症状temp.Append("特禀质：" + 体检bean.特禀质  + ", "); }
        //        症状temp.Append("】");
        //    }

        //    #endregion

        //    try
        //    {

        //        //this.richTextBox异常情况.Text = 症状temp.ToString();
        //        this.txt症状.Text = 症状temp.ToString();
        //        this.txt健康指导.Text = 健康指导temp.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        //Msg.ShowError(ex.Message);
        //    }
        //}

        //private void CalculateNoDocNo(tb_健康体检 体检bean) 
        //{
        //    StringBuilder 症状temp = new StringBuilder();
        //    StringBuilder 健康指导temp = new StringBuilder();

        //    #region BMI
        //    //BMI
        //    /*
        //        过轻：低于18.5
        //        正常：18.5-23.9
        //        超重：≥24
        //     * 24～27.9 偏胖
        //        肥胖：28-32
        //        非常肥胖, 高于32
        //     */
        //    if (!string.IsNullOrEmpty(体检bean.体重指数))
        //    {
        //        double bmi = double.Parse(体检bean.体重指数);
        //        if (bmi < 18.5)
        //        {
        //            症状temp.Append("【体重偏轻（体重指数：" + bmi + ")" + ", 】");
        //            健康指导temp.Append("体重偏轻异常，建议：补充营养。\n");
        //            //builder2.AppendLine("体重偏轻异常，建议：");
        //        }
        //        else if (bmi >= 24)
        //        {
        //            症状temp.Append("【体重超重（体重指数：" + bmi + ")" + ", 】");
        //            健康指导temp.Append("体重超重异常，建议：少食，减盐，常称体重。\n");
        //            //builder2.AppendLine("体重超重异常，建议：");
        //        }
        //        else if (bmi >= 24 && bmi < 27.9)
        //        {
        //            症状temp.Append("【体重偏胖（体重指数：" + bmi + ")" + ", 】");
        //            健康指导temp.Append("体重偏胖异常，建议：少食，减盐，常称体重。\n");
        //            //builder2.AppendLine("体重偏胖异常，建议：");
        //        }
        //        else if (bmi >= 28 && bmi < 32)
        //        {
        //            症状temp.Append("【体重肥胖（体重指数：" + bmi + ")" + ", 】");
        //            健康指导temp.Append("体重肥胖异常，建议：少食，减盐，常称体重。\n");
        //            //builder2.AppendLine("体重肥胖异常，建议：");
        //        }
        //        else if (bmi > 32)
        //        {
        //            症状temp.Append("【体重非常肥胖（体重指数：" + bmi + ")" + ", 】");
        //            健康指导temp.Append("体重非常肥胖异常，建议：少食，减盐，常称体重。\n");
        //            //builder2.AppendLine("体重非常肥胖异常，建议：");
        //        }
        //    }

        //    #endregion

        //    #region 糖尿病

        //    if (!string.IsNullOrEmpty(体检bean.空腹血糖))
        //    {
        //        string xuetang = 体检bean.空腹血糖;
        //        decimal _xuetang = 0m;
        //        if (Decimal.TryParse(xuetang, out _xuetang))
        //        {
        //            if (_xuetang != 0m && (_xuetang > 6.40m || _xuetang < 3.89m))
        //            {
        //                症状temp.Append("【糖尿病(GLU:" + _xuetang + "mmol/l)（3.89-6.40）" + ", 】");
        //                健康指导temp.Append("空腹血糖偏高，建议：尽早到内分泌专科进一步检查确认治疗。并注意低糖、低盐、低脂饮食，控制体重，防止并发症的发生。建议做葡萄糖检验，以确认有无糖尿病或糖尿量正常。可取生茅根60-90g水煎代茶饮每日一剂，连服10日。\n");
        //            }

        //        }
        //    }

        //    #endregion

        //    #region 血压


        //    string 血压右侧 = 体检bean.血压右侧1 + "/" + 体检bean.血压右侧2;
        //    string 血压左侧 = 体检bean.血压左侧1 + "/" + 体检bean.血压左侧2;

        //    if (!string.IsNullOrEmpty(血压右侧) || !string.IsNullOrEmpty(血压左侧))
        //    {
        //        //右侧
        //        if (!string.IsNullOrEmpty(血压右侧))
        //        {
        //            string xueya = 血压右侧.Trim();
        //            string you1 = xueya.Split('/')[0];
        //            string you2 = xueya.Split('/')[1];
        //            int _you1; int _you2;
        //            if (int.TryParse(you1, out _you1) && int.TryParse(you2, out _you2))
        //            {
        //                if (_you1 > 140 || _you2 > 90)
        //                {
        //                    症状temp.Append("【高血压（" + xueya + ")" + ", 】");
        //                    健康指导temp.Append("血压高偏高，建议：①减轻并控制体重。②减少钠盐摄入。③补充钙和钾盐。④减少脂肪摄入。⑤增加运动。⑥戒烟、限制饮酒。⑦减轻精神压力，保持心理平衡。可取金银花、菊花各26g，每日一剂，分4份用沸水冲泡代饮，头晕明显者可加桑叶12g，若动脉硬化、血脂高者加山楂24-30g，冲泡2次弃掉另换。\n");
        //                }
        //            }
        //        }
        //        //左侧
        //        else if (!string.IsNullOrEmpty(血压左侧))
        //        {
        //            string xueya = 血压左侧.Trim();
        //            string you1 = xueya.Split('/')[0];
        //            string you2 = xueya.Split('/')[1];
        //            int _you1; int _you2;
        //            if (int.TryParse(you1, out _you1) && int.TryParse(you2, out _you2))
        //            {
        //                if (_you1 > 140 || _you2 > 90)
        //                {
        //                    症状temp.Append("【高血压（" + xueya + ")" + ", 】");
        //                    // this.txt结论建议.Document.AppendText("血压高偏高，建议：①减轻并控制体重。②减少钠盐摄入。③补充钙和钾盐。④减少脂肪摄入。⑤增加运动。⑥戒烟、限制饮酒。⑦减轻精神压力，保持心理平衡。可取金银花、菊花各26g，每日一剂，分4份用沸水冲泡代饮，头晕明显者可加桑叶12g，若动脉硬化、血脂高者加山楂24-30g，冲泡2次弃掉另换。");
        //                }
        //            }
        //        }
        //    }

        //    #endregion

        //    #region 血常规
        //    /*
        //     白细胞：3.9-9.9
        //     血红蛋白：116-179
        //     血小板：98.7-302.9
        //     */
        //    if (!string.IsNullOrEmpty(体检bean.白细胞) || !string.IsNullOrEmpty(体检bean.血红蛋白) || !string.IsNullOrEmpty(体检bean.血小板))
        //    {
        //        string baixibao = 体检bean.白细胞.Trim();
        //        string xuehongdanbai = 体检bean.血红蛋白.Trim();
        //        string xuexiaoban = 体检bean.血小板.Trim();
        //        decimal _baixibao;
        //        decimal _xuehongdanbai;
        //        decimal _xuexiaoban;
        //        string text = "【血常规异常：";
        //        if (decimal.TryParse(baixibao, out _baixibao))
        //        {
        //            if (_baixibao > 9.9m || _baixibao < 3.9m)
        //            {
        //                text += "白细胞:" + _baixibao + "(3.9-9.9)" + ", ";
        //            }
        //        }
        //        if (decimal.TryParse(xuehongdanbai, out _xuehongdanbai))
        //        {
        //            if (_xuehongdanbai > 179m || _xuehongdanbai < 116m)
        //            {
        //                text += "血红蛋白:" + _xuehongdanbai + "(116-179)" + ", ";
        //            }
        //        }
        //        if (decimal.TryParse(xuexiaoban, out _xuexiaoban))
        //        {
        //            if (_xuexiaoban > 302.9m || _xuexiaoban < 98.7m)
        //            {
        //                text += "血小板:" + _xuexiaoban + "(98.7-302.9)" + ", ";
        //            }
        //        }
        //        if (text != "血常规异常：")
        //        {
        //            症状temp.Append(text + ", 】");
        //            健康指导temp.Append("血常规异常，建议：医院复查。\n");
        //        }
        //    }

        //    #endregion

        //    #region 尿常规
        //    if (!string.IsNullOrEmpty(体检bean.尿蛋白) || !string.IsNullOrEmpty(体检bean.尿酮体) || !string.IsNullOrEmpty(体检bean.尿潜血) || !string.IsNullOrEmpty(体检bean.尿糖))
        //    {
        //        string text = "【尿常规异常：";
        //        if (!string.IsNullOrEmpty(体检bean.尿蛋白) && 体检bean.尿蛋白.Trim() != "-")
        //        {
        //            text += "尿蛋白:" + 体检bean.尿蛋白 + ", ";
        //        }
        //        if (!string.IsNullOrEmpty(体检bean.尿酮体) && 体检bean.尿酮体.Trim() != "-")
        //        {
        //            text += "尿酮体:" + 体检bean.尿酮体 + ", ";
        //        }
        //        if (!string.IsNullOrEmpty(体检bean.尿潜血) && 体检bean.尿潜血.Trim() != "-")
        //        {
        //            text += "尿潜血:" + 体检bean.尿潜血 + ", ";
        //        }
        //        if (!string.IsNullOrEmpty(体检bean.尿糖) && 体检bean.尿糖.Trim() != "-")
        //        {
        //            text += "尿糖:" + 体检bean.尿糖 + ", ";
        //        }
        //        if (text != "尿常规异常：")
        //        {
        //            症状temp.Append(text + ", 】");
        //            健康指导temp.Append("尿常规异常，建议：医院复查。\n");
        //        }
        //    }
        //    #endregion

        //    #region 肝功
        //    /*
        //     血清谷丙转氨酶：0-40
        //     血清谷草转氨酶：0-41
        //     总胆红素：2-20.5
        //     结合胆红素：0-7.0
        //     */
        //    if (!string.IsNullOrEmpty(体检bean.血清谷丙转氨酶) || !string.IsNullOrEmpty(体检bean.血清谷草转氨酶) || !string.IsNullOrEmpty(体检bean.总胆红素) || !string.IsNullOrEmpty(体检bean.结合胆红素))
        //    {
        //        string 血清谷丙转氨酶 = 体检bean.血清谷丙转氨酶.Trim();
        //        string 血清谷草转氨酶 = 体检bean.血清谷草转氨酶.Trim();
        //        string 总胆红素 = 体检bean.总胆红素.Trim();
        //        string 结合胆红素 = 体检bean.结合胆红素.Trim();
        //        decimal _血清谷丙转氨酶;
        //        decimal _血清谷草转氨酶;
        //        decimal _总胆红素;
        //        decimal _结合胆红素;
        //        string text = "【肝功异常：";
        //        if (decimal.TryParse(血清谷丙转氨酶, out _血清谷丙转氨酶))
        //        {
        //            if (_血清谷丙转氨酶 > 40m || _血清谷丙转氨酶 < 0m)
        //            {
        //                text += "谷丙转氨酶:" + _血清谷丙转氨酶 + "(0-40)" + ", ";
        //            }
        //        }
        //        if (decimal.TryParse(血清谷草转氨酶, out _血清谷草转氨酶))
        //        {
        //            if (_血清谷草转氨酶 > 41m || _血清谷草转氨酶 < 0m)
        //            {
        //                text += "谷草转氨酶:" + _血清谷草转氨酶 + "(0-41)" + ", ";
        //            }
        //        }
        //        if (decimal.TryParse(总胆红素, out _总胆红素))
        //        {
        //            if (_总胆红素 > 20.5m || _总胆红素 < 2m)
        //            {
        //                text += "总胆红素:" + _总胆红素 + "(2-20.5)" + ", ";
        //            }
        //        }
        //        if (decimal.TryParse(结合胆红素, out _结合胆红素))
        //        {
        //            if (_结合胆红素 > 7m || _结合胆红素 < 0m)
        //            {
        //                text += "结合胆红素:" + _结合胆红素 + "(0-7.0)" + ", ";
        //            }
        //        }
        //        if (text != "肝功异常：")
        //        {
        //            症状temp.Append(text + ", 】");
        //            健康指导temp.Append(@"肝功异常，建议：1、适当运动，在体力允许的情况下，可做些散步，慢跑等运动；2、低脂肪低胆固醇饮食，多吃新鲜蔬菜水果，少吃高脂食物，如肥肉、动物内脏、油炸食品，粗细粮搭配；3、戒烟限酒；4、半年复查肝脏超声。\n");
        //        }
        //    }
        //    #endregion

        //    #region 肾功
        //    /*
        //     血清肌酐:53-115
        //     血尿素氮:1.7-8.7 
        //     */
        //    if (!string.IsNullOrEmpty(体检bean.血清肌酐) || !string.IsNullOrEmpty(体检bean.血尿素氮))
        //    {
        //        string 血清肌酐 = 体检bean.血清肌酐.Trim();
        //        string 血尿素氮 = 体检bean.血尿素氮.Trim();
        //        decimal _血清肌酐;
        //        decimal _血尿素氮;
        //        string text = "【肾功异常：";
        //        if (decimal.TryParse(血清肌酐, out _血清肌酐))
        //        {
        //            if (_血清肌酐 > 115m || _血清肌酐 < 53m)
        //            {
        //                text += "血清肌酐:" + _血清肌酐 + "(53-115)" + ", ";
        //            }
        //        }
        //        if (decimal.TryParse(血尿素氮, out _血尿素氮))
        //        {
        //            if (_血尿素氮 > 8.7m || _血尿素氮 < 1.7m)
        //            {
        //                text += "血尿素氮:" + _血尿素氮 + "(1.7-8.7 )" + ", ";
        //            }
        //        }

        //        if (text != "肾功异常：")
        //        {
        //            症状temp.Append(text + ", 】 ");
        //            健康指导temp.Append("肾功异常，建议：医院复查。\n");
        //        }
        //    }
        //    #endregion

        //    #region 血脂
        //    /*
        //     总胆固醇：2.34-5.20
        //     甘油三酯：0.7-1.7
        //     高密度脂蛋白：0.77-2.25
        //     低密度脂蛋白：0-4.13
        //     */
        //    if (!string.IsNullOrEmpty(体检bean.总胆固醇) || !string.IsNullOrEmpty(体检bean.甘油三酯) || !string.IsNullOrEmpty(体检bean.血清高密度脂蛋白胆固醇) || !string.IsNullOrEmpty(体检bean.血清低密度脂蛋白胆固醇))
        //    {
        //        string 总胆固醇 = 体检bean.总胆固醇.Trim();
        //        string 甘油三酯 = 体检bean.甘油三酯.Trim();
        //        string 高密度脂蛋白 = 体检bean.血清高密度脂蛋白胆固醇.Trim();
        //        string 低密度脂蛋白 = 体检bean.血清低密度脂蛋白胆固醇.Trim();
        //        decimal _总胆固醇;
        //        decimal _甘油三酯;
        //        decimal _高密度脂蛋白;
        //        decimal _低密度脂蛋白;
        //        string text = "【血脂异常：";
        //        if (decimal.TryParse(总胆固醇, out _总胆固醇))
        //        {
        //            if (_总胆固醇 > 5.2m || _总胆固醇 < 2.34m)
        //            {
        //                text += "总胆固醇:" + _总胆固醇 + "(2.34-5.20)" + ", ";
        //            }
        //        }
        //        if (decimal.TryParse(甘油三酯, out _甘油三酯))
        //        {
        //            if (_甘油三酯 > 1.7m || _甘油三酯 < 0.7m)
        //            {
        //                text += "甘油三酯:" + _甘油三酯 + "(0.7-1.7)" + ", ";
        //            }
        //        }
        //        if (decimal.TryParse(高密度脂蛋白, out _高密度脂蛋白))
        //        {
        //            if (_高密度脂蛋白 > 2.25m || _高密度脂蛋白 < 0.77m)
        //            {
        //                text += "高密度脂蛋白:" + _高密度脂蛋白 + "(0.77-2.25)" + ", ";
        //            }
        //        }
        //        if (decimal.TryParse(低密度脂蛋白, out _低密度脂蛋白))
        //        {
        //            if (_低密度脂蛋白 > 4.13m || _低密度脂蛋白 < 0m)
        //            {
        //                text += "低密度脂蛋白:" + _低密度脂蛋白 + "(0-4.13)" + ", ";
        //            }
        //        }
        //        if (text != "血脂异常：")
        //        {
        //            症状temp.Append(text + ", 】");
        //            健康指导temp.Append("血常规异常，建议：要注意清淡饮食，不吃高脂餐，控制进食量，适当运动，戒烟限酒。\n");
        //        }
        //    }
        //    #endregion

        //    #region 心电图
        //    if (!string.IsNullOrEmpty(体检bean.心电图) && 体检bean.心电图.Trim() != "1")
        //    {
        //        症状temp.Append("【心电图异常：" + 体检bean.心电图异常.Trim() + ", 】");
        //        健康指导temp.Append("心电图异常，建议：医院复查。\n");
        //    }
        //    #endregion

        //    #region B超
        //    //if (this.体检bean.B超  != "未做")
        //    //{
        //    string B超desc = string.Equals(体检bean.B超, "1") ? "正常" : string.Equals(体检bean.B超, "2") ? "异常" : "未做";

        //    症状temp.Append("【B超：" + B超desc + ",】 ");
        //    if (!string.IsNullOrEmpty(体检bean.B超其他))
        //    {
        //        症状temp.Append(体检bean.B超其他 + ", ");
        //    }
        //    //}
        //    #endregion

        //    if (症状temp.Length == 0)
        //    {
        //        症状temp.Append(@"体检未见明显异常" + "。 ");
        //    }
        //    #region 体质辨识
        //    if (!string.IsNullOrEmpty(体检bean.平和质) || !string.IsNullOrEmpty(体检bean.气虚质) || !string.IsNullOrEmpty(体检bean.阳虚质) || !string.IsNullOrEmpty(体检bean.阴虚质) || !string.IsNullOrEmpty(体检bean.痰湿质) || !string.IsNullOrEmpty(体检bean.湿热质) || !string.IsNullOrEmpty(体检bean.血瘀质) || !string.IsNullOrEmpty(体检bean.气郁质) || !string.IsNullOrEmpty(体检bean.特禀质))
        //    {
        //        症状temp.Append("【中医体质辨识结果：");
        //        string 平和质desc = string.Equals(体检bean.平和质, "1") ? "是" : string.Equals(体检bean.平和质, "2") ? "基本是" : "";
        //        string 气虚质desc = string.Equals(体检bean.气虚质, "1") ? "是" : string.Equals(体检bean.气虚质, "2") ? "倾向是" : "";
        //        string 阳虚质desc = string.Equals(体检bean.阳虚质, "1") ? "是" : string.Equals(体检bean.阳虚质, "2") ? "倾向是" : "";
        //        string 阴虚质desc = string.Equals(体检bean.阴虚质, "1") ? "是" : string.Equals(体检bean.阴虚质, "2") ? "倾向是" : "";
        //        string 痰湿质desc = string.Equals(体检bean.痰湿质, "1") ? "是" : string.Equals(体检bean.痰湿质, "2") ? "倾向是" : "";
        //        string 湿热质desc = string.Equals(体检bean.湿热质, "1") ? "是" : string.Equals(体检bean.湿热质, "2") ? "倾向是" : "";
        //        string 血瘀质desc = string.Equals(体检bean.血瘀质, "1") ? "是" : string.Equals(体检bean.血瘀质, "2") ? "倾向是" : "";
        //        string 气郁质desc = string.Equals(体检bean.气郁质, "1") ? "是" : string.Equals(体检bean.气郁质, "2") ? "倾向是" : "";
        //        string 特禀质desc = string.Equals(体检bean.特禀质, "1") ? "是" : string.Equals(体检bean.特禀质, "2") ? "倾向是" : "";

        //        if (!string.IsNullOrEmpty(体检bean.平和质))
        //        { 症状temp.Append("平和质：" + 平和质desc + ", "); } 

        //        if (!string.IsNullOrEmpty(体检bean.气虚质))
        //        { 症状temp.Append("气虚质：" + 气虚质desc + ", "); }

        //        if (!string.IsNullOrEmpty(体检bean.阳虚质))
        //        { 症状temp.Append("阳虚质：" + 阳虚质desc + ", "); }

        //        if (!string.IsNullOrEmpty(体检bean.阴虚质))
        //        { 症状temp.Append("阴虚质：" + 阴虚质desc + ", "); }

        //        if (!string.IsNullOrEmpty(体检bean.痰湿质))
        //        { 症状temp.Append("痰湿质：" + 痰湿质desc + ", "); }

        //        if (!string.IsNullOrEmpty(体检bean.湿热质))
        //        { 症状temp.Append("湿热质：" + 湿热质desc + ", "); }

        //        if (!string.IsNullOrEmpty(体检bean.血瘀质))
        //        { 症状temp.Append("血瘀质：" + 血瘀质desc + ", "); }

        //        if (!string.IsNullOrEmpty(体检bean.气郁质))
        //        { 症状temp.Append("气郁质：" + 气郁质desc + ", "); }

        //        if (!string.IsNullOrEmpty(体检bean.特禀质))
        //        { 症状temp.Append("特禀质：" + 特禀质desc + ", "); }

        //        症状temp.Append("】");
        //    }

        //    #endregion

        //    try
        //    {

        //        //this.richTextBox异常情况.Text = 症状temp.ToString();
        //        this.txt症状.Text = 症状temp.ToString();
        //        this.txt健康指导.Text = 健康指导temp.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        //Msg.ShowError(ex.Message);
        //    }
        //}

       
       


    
  
        /// <summary>
        /// 无视图 体检信息
        /// </summary>
        private void bind体检信息NoDoc(string 条码)
        {
            
            //tb_健康体检 健康体检bean = new tb_健康体检();
            //健康体检bean.个人档案编号 = 条码;
            //List<tb_健康体检> 健康体检List = 健康档案服务.获取tb_健康体检(健康体检bean);

            //if (健康体检List.Count > 0)
            //{
            //    tb_健康体检 体检bean = 健康体检List[0];
            //    //计算分析 并绑定
            //    CalculateNoDocNo(体检bean);
            //}
        }


        /// <summary>
        /// 有视图体检信息
        /// </summary>
        //private void bind体检信息(vw_健康体检 健康体检bean)
        //{
        //    //额外的个人信息
        //    //this.txt家庭医生.Text = 健康体检bean.FIELD2;

        //    //计算分析 并绑定
        //    Calculate(健康体检bean);

        //}

    }
}
