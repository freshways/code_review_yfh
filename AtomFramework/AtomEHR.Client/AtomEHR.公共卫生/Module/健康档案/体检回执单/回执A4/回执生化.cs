﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
//using cn.net.sunshine.physicalExam.modules.patient;
//using cn.net.sunshine.physicalExam;
using System.Collections.Generic;
//using cn.net.sunshine.physicalExam.modules.ecg;
using System.Xml;
//using cn.net.sunshine.tools;
using System.IO;
using System.Windows.Forms;

//using System.ComponentModel;

using System.Data;
using System.Text;
using System.Linq;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using DevExpress.XtraPrinting;
using AtomEHR.Business;
//using cn.net.sunshine.physicalExam.modules.common;

namespace AtomEHR.公共卫生.Module.健康档案.体检回执单.回执A4
{
    public partial class 回执生化 : DevExpress.XtraReports.UI.XtraReport
    {

        string docNo;
        string date;
        string name;
        string 条码;
        //private Dictionary<string, Exam> _examDic = new Dictionary<string, Exam>();

        //string SqlServerConnString = @"Server=172.12.12.107;uid=readonlyforqt;pwd=readonlyforqt123?!@#;database=SYSXTQT";
        //如果泉庄和青驼不同则要判断
        //string SqlServerConnString = @"Server=" + PUB_PARAM.LIS_HostIp + ";uid=" + PUB_PARAM.LIS_DBUser + ";pwd=" + PUB_PARAM.LIS_DBPass + ";database=" + PUB_PARAM.LIS_DBName;
        string SqlServerConnString = @"Server=localhost;uid=sa;pwd=sasa;database=LIS";

        //坐标
        private float xOfFirst = 5F;//810F;
        private float yOfFirst = 215F;
        private float yOfFirst大生化 = 653F;

        private float yOfFirst尿检 = 950F;

        //尺寸
        private float widthOfLb1 = 180F;
        private float widthOfLb2 = 80F;
        private float widthOfLb3 = 120F;
        private float widthOfLb4 = 70F;
        private float widthOfLb5 = 70F;
        private float widthOfLb6 = 70F;
        private float widthOfLb7 = 155F;

        private float heightOfLb = 15F;
        //边框
        private BorderSide BorderSideOfLb = DevExpress.XtraPrinting.BorderSide.None;
        //字体
        private Font fontOfLb = new System.Drawing.Font("宋体", 8F);
        //private Font fontOfLbTemp = new System.Drawing.Font("黑体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        //对齐
        private TextAlignment textAlignmentOfLb = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;

        public 回执生化()
        {
            InitializeComponent();

            ////血常规
            //bind血常规(getLIs数据("血常规", "17051403003122000000"));
            ////尿常规
            //bind尿常规(getLIs数据("尿常规", "17051403003122000000"));
        }

        public 回执生化(DataRow dr)
        {
            InitializeComponent();
            this.txt条码号.Text = dr["个人档案编号"].ToString().Substring(2);
            this.txt地址.Text = dr["居住地址"].ToString();
            this.txt姓名.Text = dr["姓名"].ToString();
            this.txt性别.Text = dr["性别"].ToString();
            this.txt生日.Text = Convert.ToDateTime(dr["出生日期"].ToString()).ToShortDateString();
            this.txt身份证.Text = dr["身份证号"].ToString();

            this.txt身高.Text = dr["身高"].ToString();
            this.txt体重.Text = dr["体重"].ToString();
            this.txtBMI.Text = dr["体重指数"].ToString();
            if (dr.Table.Columns.Contains("腰围"))
                this.txt腰围.Text = dr["腰围"].ToString();
            else
                this.txt腰围.Text = "";
            if (dr.Table.Columns.Contains("体温"))
                this.txt体温.Text = dr["体温"].ToString();
            else
                this.txt体温.Text = "";
            if (dr.Table.Columns.Contains("呼吸"))
                this.txt呼吸频率.Text = dr["呼吸"].ToString();
            else
                this.txt呼吸频率.Text = "";
            this.txt左侧高压.Text = dr["血压左侧"].ToString();
            //this.txt左侧低压.Text = dr["姓名"].ToString();
            this.txt右侧高压.Text = dr["血压右侧"].ToString();
            //this.txt右侧低压.Text = dr["姓名"].ToString();

            bind尿常规(get尿(dr));
            try
            {
                date = dr["体检日期"].ToString();
                条码 = this.txt身份证.Text;
                //血常规
                DataTable dt血常规 = getLIs数据("血常规", 条码, date);
                if(dt血常规!=null && dt血常规.Rows.Count>0)
                    bind血常规(dt血常规);
                else
                    bind血常规(get血常规(dr));
                //大生化
                DataTable dt大生化 = getLIs数据("大生化", 条码, date);
                if (dt大生化 != null && dt大生化.Rows.Count > 0)
                    bind生化(dt大生化);
                else
                    bind生化(get大生化(dr));
            }
            catch
            {
                
            }
        }

        public 回执生化(string _docNo, string _date, string _name)
        {
            InitializeComponent();
        }

        private DataTable get尿(DataRow drs)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("项目名称", System.Type.GetType("System.String"));
            dt.Columns.Add("项目代码", System.Type.GetType("System.String"));
            dt.Columns.Add("仪器法", System.Type.GetType("System.String"));
            dt.Columns.Add("标记", System.Type.GetType("System.String"));
            dt.Columns.Add("项目值", System.Type.GetType("System.String"));
            dt.Columns.Add("参考值", System.Type.GetType("System.String"));
            dt.Columns.Add("项目单位", System.Type.GetType("System.String"));

            DataRow dr = dt.NewRow();
            dr["项目名称"] = "尿蛋白质";
            dr["项目代码"] = "PRO";
            dr["仪器法"] = "";
            dr["标记"] = "";
            dr["项目值"] = drs["尿蛋白"].ToString();//体检bean.尿蛋白;
            dr["参考值"] = "";
            dr["项目单位"] = "";
            dt.Rows.Add(dr);

            DataRow dr1 = dt.NewRow();
            dr1["项目名称"] = "尿糖";
            dr1["项目代码"] = "GLU";
            dr1["仪器法"] = "";
            dr1["标记"] = "";
            dr1["项目值"] = drs["尿糖"].ToString();//体检bean.尿糖;
            dr1["参考值"] = "";
            dr1["项目单位"] = "";
            dt.Rows.Add(dr1);

            DataRow dr2 = dt.NewRow();
            dr2["项目名称"] = "尿酮体";
            dr2["项目代码"] = "KET";
            dr2["仪器法"] = "";
            dr2["标记"] = "";
            dr2["项目值"] = drs["尿酮体"].ToString();//体检bean.尿酮体;
            dr2["参考值"] = "";
            dr2["项目单位"] = "";
            dt.Rows.Add(dr2);

            DataRow dr3 = dt.NewRow();
            dr3["项目名称"] = "尿潜血";
            dr3["项目代码"] = "BLO";
            dr3["仪器法"] = "";
            dr3["标记"] = "";
            dr3["项目值"] = drs["尿潜血"].ToString();//体检bean.尿潜血;
            dr3["参考值"] = "";
            dr3["项目单位"] = "";
            dt.Rows.Add(dr3);

            DataRow dr4 = dt.NewRow();
            dr4["项目名称"] = "尿蛋白质";
            dr4["项目代码"] = "PRO";
            dr4["仪器法"] = "";
            dr4["标记"] = "";
            dr4["项目值"] = drs["尿蛋白"].ToString();//体检bean.尿蛋白;
            dr4["参考值"] = "";
            dr4["项目单位"] = "";
            dt.Rows.Add(dr4);

            return dt;
        }

        private DataTable get血常规(DataRow drs)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("项目名称", System.Type.GetType("System.String"));
            dt.Columns.Add("项目代码", System.Type.GetType("System.String"));
            dt.Columns.Add("仪器法", System.Type.GetType("System.String"));
            dt.Columns.Add("标记", System.Type.GetType("System.String"));
            dt.Columns.Add("项目值", System.Type.GetType("System.String"));
            dt.Columns.Add("参考值", System.Type.GetType("System.String"));
            dt.Columns.Add("项目单位", System.Type.GetType("System.String"));

            DataRow dr = dt.NewRow();
            dr["项目名称"] = "血红蛋白";
            dr["项目代码"] = "PRO";
            dr["仪器法"] = "";
            dr["标记"] = "";
            dr["项目值"] = drs["血红蛋白"].ToString();//体检bean.尿蛋白;
            dr["参考值"] = "110--165";
            dr["项目单位"] = "g/L";
            dt.Rows.Add(dr);

            DataRow dr1 = dt.NewRow();
            dr1["项目名称"] = "白细胞";
            dr1["项目代码"] = "GLU";
            dr1["仪器法"] = "";
            dr1["标记"] = "";
            dr1["项目值"] = drs["白细胞"].ToString();//体检bean.尿糖;
            dr1["参考值"] = "4--10";
            dr1["项目单位"] = "10^9/L";
            dt.Rows.Add(dr1);

            DataRow dr2 = dt.NewRow();
            dr2["项目名称"] = "血小板";
            dr2["项目代码"] = "KET";
            dr2["仪器法"] = "";
            dr2["标记"] = "";
            dr2["项目值"] = drs["血小板"].ToString();//体检bean.尿酮体;
            dr2["参考值"] = "100--300";
            dr2["项目单位"] = "10^9/L";
            dt.Rows.Add(dr2);

            return dt;
        }

        private DataTable get大生化(DataRow drs)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("项目名称", System.Type.GetType("System.String"));
            dt.Columns.Add("项目代码", System.Type.GetType("System.String"));
            dt.Columns.Add("仪器法", System.Type.GetType("System.String"));
            dt.Columns.Add("标记", System.Type.GetType("System.String"));
            dt.Columns.Add("项目值", System.Type.GetType("System.String"));
            dt.Columns.Add("参考值", System.Type.GetType("System.String"));
            dt.Columns.Add("项目单位", System.Type.GetType("System.String"));

            #region 肝功
            //肝功
            DataRow dr = dt.NewRow();
            dr["项目名称"] = "血清谷丙转氨酶";
            dr["项目代码"] = "ALT";
            dr["仪器法"] = "";
            dr["标记"] = "";
            dr["项目值"] = drs["血清谷丙转氨酶"].ToString();//体检bean.尿蛋白;
            dr["参考值"] = "0--40";
            dr["项目单位"] = "U/L";
            dt.Rows.Add(dr);

            DataRow dr1 = dt.NewRow();
            dr1["项目名称"] = "血清谷草转氨酶";
            dr1["项目代码"] = "AST";
            dr1["仪器法"] = "";
            dr1["标记"] = "";
            dr1["项目值"] = drs["血清谷草转氨酶"].ToString();//体检bean.尿糖;
            dr1["参考值"] = "0--40";
            dr1["项目单位"] = "U/L";
            dt.Rows.Add(dr1);

            DataRow dr2 = dt.NewRow();
            dr2["项目名称"] = "白蛋白";
            dr2["项目代码"] = "ALB";
            dr2["仪器法"] = "";
            dr2["标记"] = "";
            dr2["项目值"] = drs["白蛋白"].ToString();//体检bean.尿酮体;
            dr2["参考值"] = "35--55";
            dr2["项目单位"] = "g/L";
            dt.Rows.Add(dr2);

            DataRow dr3 = dt.NewRow();
            dr2["项目名称"] = "总胆红素";
            dr2["项目代码"] = "TBIL";
            dr2["仪器法"] = "";
            dr2["标记"] = "";
            dr2["项目值"] = drs["总胆红素"].ToString();//体检bean.尿酮体;
            dr2["参考值"] = "2--20";
            dr2["项目单位"] = "μmol/l";
            dt.Rows.Add(dr3);

            DataRow dr4 = dt.NewRow();
            dr2["项目名称"] = "直接胆红素";
            dr2["项目代码"] = "DBIL";
            dr2["仪器法"] = "";
            dr2["标记"] = "";
            dr2["项目值"] = drs["结合胆红素"].ToString();//体检bean.尿酮体;
            dr2["参考值"] = "0--8";
            dr2["项目单位"] = "μmol/l";
            dt.Rows.Add(dr4); 
            #endregion

            #region 肾功
            //肾功
            DataRow dr5 = dt.NewRow();
            dr["项目名称"] = "血清肌酐";
            dr["项目代码"] = "ALT";
            dr["仪器法"] = "";
            dr["标记"] = "";
            dr["项目值"] = drs["血清肌酐"].ToString();//体检bean.尿蛋白;
            dr["参考值"] = "50--106";
            dr["项目单位"] = "μmol/l";
            dt.Rows.Add(dr5);

            DataRow dr6 = dt.NewRow();
            dr1["项目名称"] = "血尿素氮";
            dr1["项目代码"] = "AST";
            dr1["仪器法"] = "";
            dr1["标记"] = "";
            dr1["项目值"] = drs["血尿素氮"].ToString();//体检bean.尿糖;
            dr1["参考值"] = "2.5--6.3";
            dr1["项目单位"] = "MMOL/L";
            dt.Rows.Add(dr6);

            DataRow dr7 = dt.NewRow();
            dr2["项目名称"] = "血钾浓度";
            dr2["项目代码"] = "K";
            dr2["仪器法"] = "";
            dr2["标记"] = "";
            dr2["项目值"] = drs["血钾浓度"].ToString();//体检bean.尿酮体;
            dr2["参考值"] = "3.5--5.5";
            dr2["项目单位"] = "mmol/L";
            dt.Rows.Add(dr7);

            DataRow dr8 = dt.NewRow();
            dr2["项目名称"] = "血钠浓度";
            dr2["项目代码"] = "Na";
            dr2["仪器法"] = "";
            dr2["标记"] = "";
            dr2["项目值"] = drs["血钠浓度"].ToString();//体检bean.尿酮体;
            dr2["参考值"] = "135--145";
            dr2["项目单位"] = "mmol/L";
            dt.Rows.Add(dr8);
            #endregion

            #region 血脂
            //肾功
            DataRow dr9 = dt.NewRow();
            dr["项目名称"] = "总胆固醇";
            dr["项目代码"] = "CHOL";
            dr["仪器法"] = "";
            dr["标记"] = "";
            dr["项目值"] = drs["总胆固醇"].ToString();//体检bean.尿蛋白;
            dr["参考值"] = "3.6--6.5";
            dr["项目单位"] = "mmol/L";
            dt.Rows.Add(dr9);

            DataRow dr10 = dt.NewRow();
            dr1["项目名称"] = "甘油三酯";
            dr1["项目代码"] = "TG";
            dr1["仪器法"] = "";
            dr1["标记"] = "";
            dr1["项目值"] = drs["甘油三酯"].ToString();//体检bean.尿糖;
            dr1["参考值"] = "0--1.71";
            dr1["项目单位"] = "mmol/L";
            dt.Rows.Add(dr10);

            DataRow dr11 = dt.NewRow();
            dr2["项目名称"] = "血清低密度脂蛋白胆固醇";
            dr2["项目代码"] = "LDL_C";
            dr2["仪器法"] = "";
            dr2["标记"] = "";
            dr2["项目值"] = drs["血清低密度脂蛋白胆固醇"].ToString();//体检bean.尿酮体;
            dr2["参考值"] = "2.07--3.1";
            dr2["项目单位"] = "mmol/L";
            dt.Rows.Add(dr11);

            DataRow dr12 = dt.NewRow();
            dr2["项目名称"] = "血清高密度脂蛋白胆固醇";
            dr2["项目代码"] = "HDL_C";
            dr2["仪器法"] = "";
            dr2["标记"] = "";
            dr2["项目值"] = drs["血清高密度脂蛋白胆固醇"].ToString();//体检bean.尿酮体;
            dr2["参考值"] = "0.83--1.96";
            dr2["项目单位"] = "mmol/L";
            dt.Rows.Add(dr12);
            #endregion

            //血糖
            DataRow dr13 = dt.NewRow();
            dr2["项目名称"] = "葡萄糖";
            dr2["项目代码"] = "GLU";
            dr2["仪器法"] = "";
            dr2["标记"] = "";
            dr2["项目值"] = drs["空腹血糖"].ToString();//体检bean.尿酮体;
            dr2["参考值"] = "3.9--6.1";
            dr2["项目单位"] = "mmol/L";
            dt.Rows.Add(dr13);
            
            return dt;
        }

        private DataTable getLIs数据(string 项目, string 条码, string 体检日期)
        {
            try
            {
                //string sql = @"select * from View_GWCT where 化验仪器 ='" + 项目 + "' and 条码 = '" + 条码 + "' and 日期 ='" + 体检日期 + "'";
                //DataTable dt = GetSqlInfo(sql);
                DataTable dt = bll获取化验信息.getLIs数据(项目, 条码, 体检日期);
                return dt;
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// 普通控件属性设置
        /// </summary>
        /// <param name="xrLabel"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="font"></param>
        /// <param name="textAlignment"></param>
        /// <param name="borderSide"></param>
        /// <param name="text"></param>
        private void setLabelProperty(XRLabel xrLabel, float width, float height, float x, float y, Font font, TextAlignment textAlignment, BorderSide borderSide, string text)
        {
            //坐标
            xrLabel.LocationFloat = new DevExpress.Utils.PointFloat(x, y);
            //大小
            xrLabel.SizeF = new System.Drawing.SizeF(width, height);
            //字体大小
            xrLabel.Font = font;
            //对齐 label右对齐,内容左对齐
            xrLabel.TextAlignment = textAlignment;
            //边框
            xrLabel.Borders = borderSide;
            //内容
            xrLabel.Text = text;
        }

        private void bind血常规(DataTable dt)
        {
            int count = 0;

            foreach (DataRow dr in dt.Rows)
            {
                //本行纵坐标 始终不变
                float yOfThisRow = yOfFirst + heightOfLb * count;
                count += 1;

                XRLabel lb1 = new XRLabel();
                XRLabel lb2 = new XRLabel();
                XRLabel lb3 = new XRLabel();
                XRLabel lb4 = new XRLabel();
                XRLabel lb5 = new XRLabel();
                XRLabel lb6 = new XRLabel();
                XRLabel lb7 = new XRLabel();

                float xOfRow = xOfFirst;
                setLabelProperty(lb1, widthOfLb1, heightOfLb, xOfRow, yOfThisRow, fontOfLb, textAlignmentOfLb, BorderSide.None, dr["项目名称"].ToString());

                xOfRow = xOfRow + widthOfLb1;
                setLabelProperty(lb2, widthOfLb2, heightOfLb, xOfRow, yOfThisRow, fontOfLb, textAlignmentOfLb, BorderSide.None, dr["项目代码"].ToString());

                xOfRow = xOfRow + widthOfLb2;
                setLabelProperty(lb3, widthOfLb3, heightOfLb, xOfRow, yOfThisRow, fontOfLb, textAlignmentOfLb, BorderSide.None, "仪器法");

                xOfRow = xOfRow + widthOfLb3;
                setLabelProperty(lb4, widthOfLb4, heightOfLb, xOfRow, yOfThisRow, fontOfLb, textAlignmentOfLb, BorderSide.None, dr["标记"].ToString());

                xOfRow = xOfRow + widthOfLb4;
                setLabelProperty(lb5, widthOfLb5, heightOfLb, xOfRow, yOfThisRow, fontOfLb, textAlignmentOfLb, BorderSide.None, dr["项目值"].ToString());

                xOfRow = xOfRow + widthOfLb5;
                setLabelProperty(lb6, widthOfLb6, heightOfLb, xOfRow, yOfThisRow, fontOfLb, textAlignmentOfLb, BorderSide.None, dr["项目单位"].ToString());

                xOfRow = xOfRow + widthOfLb6;
                setLabelProperty(lb7, widthOfLb7, heightOfLb, xOfRow, yOfThisRow, fontOfLb, textAlignmentOfLb, BorderSide.None, dr["参考值"].ToString());

                this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            lb1,lb2,lb3,lb4,lb5,lb6,lb7});
            }
        }

        private void bind尿常规(DataTable dt)
        {
            int count = 0;

            foreach (DataRow dr in dt.Rows)
            {
                //本行纵坐标 始终不变
                float yOfThisRow = yOfFirst尿检 + heightOfLb * count;
                count += 1;

                XRLabel lb1 = new XRLabel();
                XRLabel lb2 = new XRLabel();
                XRLabel lb3 = new XRLabel();
                XRLabel lb4 = new XRLabel();
                XRLabel lb5 = new XRLabel();
                XRLabel lb6 = new XRLabel();
                XRLabel lb7 = new XRLabel();

                float xOfRow = xOfFirst;
                setLabelProperty(lb1, widthOfLb1, heightOfLb, xOfRow, yOfThisRow, fontOfLb, textAlignmentOfLb, BorderSide.None, dr["项目名称"].ToString());

                xOfRow = xOfRow + widthOfLb1;
                setLabelProperty(lb2, widthOfLb2, heightOfLb, xOfRow, yOfThisRow, fontOfLb, textAlignmentOfLb, BorderSide.None, dr["项目代码"].ToString());

                xOfRow = xOfRow + widthOfLb2;
                setLabelProperty(lb3, widthOfLb3, heightOfLb, xOfRow, yOfThisRow, fontOfLb, textAlignmentOfLb, BorderSide.None, dr["仪器法"].ToString());

                xOfRow = xOfRow + widthOfLb3;
                setLabelProperty(lb4, widthOfLb4, heightOfLb, xOfRow, yOfThisRow, fontOfLb, textAlignmentOfLb, BorderSide.None, dr["标记"].ToString());

                xOfRow = xOfRow + widthOfLb4;
                setLabelProperty(lb5, widthOfLb5, heightOfLb, xOfRow, yOfThisRow, fontOfLb, textAlignmentOfLb, BorderSide.None, dr["项目值"].ToString());

                xOfRow = xOfRow + widthOfLb5;
                setLabelProperty(lb6, widthOfLb6, heightOfLb, xOfRow, yOfThisRow, fontOfLb, textAlignmentOfLb, BorderSide.None, dr["项目单位"].ToString());

                xOfRow = xOfRow + widthOfLb6;
                setLabelProperty(lb7, widthOfLb7, heightOfLb, xOfRow, yOfThisRow, fontOfLb, textAlignmentOfLb, BorderSide.None, dr["参考值"].ToString());

                this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            lb1,lb2,lb3,lb4,lb5,lb6,lb7});
            }
        }

        private void bind生化(DataTable dt)
        {
            //this.xtc项目.DataBindings.Add("Text", DataSource, "项目");
            //this.xtc简称.DataBindings.Add("Text", DataSource, "仪器");
            //this.xtc结果.DataBindings.Add("Text", DataSource, "数值");

            //this.xtc检验方法.DataBindings.Add("Text", DataSource, "检验方法");
            //this.xtc提示.DataBindings.Add("Text", DataSource, "提示");
            //this.xtc单位.DataBindings.Add("Text", DataSource, "单位");
            //this.xtc参考范围.DataBindings.Add("Text", DataSource, "参考范围");

            //this.DataSource = dt;

            int count = 0;

            foreach (DataRow dr in dt.Rows)
            {
                //本行纵坐标 始终不变
                float yOfThisRow = yOfFirst大生化 + heightOfLb * count;
                count += 1;

                XRLabel lb1 = new XRLabel();
                XRLabel lb2 = new XRLabel();
                XRLabel lb3 = new XRLabel();
                XRLabel lb4 = new XRLabel();
                XRLabel lb5 = new XRLabel();
                XRLabel lb6 = new XRLabel();
                XRLabel lb7 = new XRLabel();


                float xOfRow = xOfFirst;
                setLabelProperty(lb1, widthOfLb1, heightOfLb, xOfRow, yOfThisRow, fontOfLb, textAlignmentOfLb, BorderSide.None, dr["项目名称"].ToString());

                xOfRow = xOfRow + widthOfLb1;
                setLabelProperty(lb2, widthOfLb2, heightOfLb, xOfRow, yOfThisRow, fontOfLb, textAlignmentOfLb, BorderSide.None, dr["项目代码"].ToString());

                xOfRow = xOfRow + widthOfLb2;
                setLabelProperty(lb3, widthOfLb3, heightOfLb, xOfRow, yOfThisRow, fontOfLb, textAlignmentOfLb, BorderSide.None, getWayByCode(dr["项目代码"].ToString()));

                xOfRow = xOfRow + widthOfLb3;
                setLabelProperty(lb4, widthOfLb4, heightOfLb, xOfRow, yOfThisRow, fontOfLb, textAlignmentOfLb, BorderSide.None, dr["标记"].ToString());

                xOfRow = xOfRow + widthOfLb4;
                setLabelProperty(lb5, widthOfLb5, heightOfLb, xOfRow, yOfThisRow, fontOfLb, textAlignmentOfLb, BorderSide.None, dr["项目值"].ToString());

                xOfRow = xOfRow + widthOfLb5;
                setLabelProperty(lb6, widthOfLb6, heightOfLb, xOfRow, yOfThisRow, fontOfLb, textAlignmentOfLb, BorderSide.None, dr["项目单位"].ToString());

                xOfRow = xOfRow + widthOfLb6;
                setLabelProperty(lb7, widthOfLb7, heightOfLb, xOfRow, yOfThisRow, fontOfLb, textAlignmentOfLb, BorderSide.None, dr["参考值"].ToString());

                this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            lb1,lb2,lb3,lb4,lb5,lb6,lb7});
            }
        }

        private string getWayByCode(string code)
        {

            //            --ALT AST  速率法
            //--TBIL  钒酸盐法
            //--CHO  HDL-C LDL-C  TG  化学法 
            //--UREA  GLU  酶法   
            //--CRE a  苦味酸法

            if (string.Equals(code, "ALT") || string.Equals(code, "AST"))
            {
                return "速率法";
            }

            if (string.Equals(code, "TBIL"))
            {
                return "钒酸盐法";
            }

            if (string.Equals(code, "CHO") || string.Equals(code, "HDL-C") || string.Equals(code, "LDL-C") || string.Equals(code, "TG"))
            {
                return "化学法";
            }

            if (string.Equals(code, "UREA") || string.Equals(code, "GLU"))
            {
                return "酶法";
            }

            if (string.Equals(code, "CRE"))
            {
                return "苦味酸法";
            }

            return "";
        }


        public DataTable GetSqlInfo(string sql)
        {
            DataSet ds = new DataSet();
            DataTable table = null;
            SqlConnection conn = new SqlConnection(SqlServerConnString);
            try
            {
                conn.Open();
                SqlCommand command = new SqlCommand();
                SqlDataAdapter dr = new SqlDataAdapter(sql, conn);
                dr.Fill(ds);
                table = ds.Tables[0];
                dr.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

            }
            finally
            {
                conn.Close();
            }
            return table;
        }



        /// <summary>
        /// 加载所有心电文件
        /// </summary>
        //private void load() 
        //{
        //    string basePath = getBasePath();

        //    var xmlFilePathes = Directory.GetFiles(basePath, ".", SearchOption.AllDirectories).Where(s => s.EndsWith(".XML"));


        //    Exam exam;
        //    string fileName;
        //    foreach (string xmlPath in xmlFilePathes)
        //    {
        //        exam = readXmlFile(xmlPath);
        //        fileName = Path.GetFileNameWithoutExtension(xmlPath);
        //        exam.ImageName = Path.Combine(basePath, fileName + ".JPG");

        //        //已有key 不添加
        //        if (!_examDic.ContainsKey(exam.ExamInfo.BarCode))
        //        {
        //            _examDic.Add(exam.ExamInfo.BarCode, exam);
        //        }
        //        else //更新
        //        {
        //            _examDic[exam.ExamInfo.BarCode] = exam;
        //        }
        //    }
        //}

        //private void getData()
        //{
        //    string basePath = getBasePath();

        //    string barCode = 条码;
        //    string photoAllName = basePath + "\\" + barCode + ".jpeg";
        //    //Image photo = Image.FromFile(photoAllName);
        //    //this.pePhoto.Image = photo;
        //    if (_examDic.ContainsKey(barCode))
        //    {
        //        string imageAllName = _examDic[barCode].ImageName;
        //        Image image = Image.FromFile(imageAllName);
        //        Bitmap bmp = (Bitmap)image;
        //        bmp.RotateFlip(RotateFlipType.Rotate270FlipNone);
        //        //this.xrpic心电.Image = bmp;
        //    }
        //    else
        //    {
        //        //XtraMessageBox.Show("取心电图数据异常!");
        //    }
        //}


        /// <summary>
        /// 获取基础路径
        /// </summary>
        /// <returns></returns>
        //private string getBasePath()
        //{
        //    string basePath = string.Empty;

        //    // 配置的图片路径
        //    string photoBasePath = Util.GetConfigValue("photoBasePath", Application.ProductName + ".exe");

        //    // 默认路径
        //    if ("default".Equals(photoBasePath))
        //    {
        //        basePath = Application.StartupPath + "\\photo\\";
        //    }
        //    // 自定义路径
        //    else
        //    {
        //        basePath = photoBasePath;
        //    }

        //    return basePath;
        //}

        //private Exam readXmlFile(string fileName)
        //{
        //    Exam exam = new Exam();
        //    ExamInfo examInfo;
        //    ExamItem examItem;

        //    XmlDocument doc = new XmlDocument();
        //    doc.Load(fileName);

        //    XmlNodeList examList = doc.GetElementsByTagName("exam");
        //    XmlNode examNode = examList.Item(0);
        //    XmlNode examInfoNode = examNode.ChildNodes.Item(0);

        //    examInfo = new ExamInfo();
        //    examInfo.Name = examInfoNode.SelectNodes("name").Item(0).InnerText;
        //    examInfo.EcgId = examInfoNode.SelectNodes("ecgid").Item(0).InnerText;
        //    examInfo.BarCode = examInfoNode.SelectNodes("barcode").Item(0).InnerText;
        //    examInfo.ExamDate = Convert.ToDateTime(examInfoNode.SelectNodes("ExamDate").Item(0).InnerText);

        //    exam.ExamInfo = examInfo;

        //    exam.ExamItems = new List<ExamItem>();
        //    XmlNode examItemNode;
        //    for (int i = 1; i < examNode.ChildNodes.Count; i++)
        //    {
        //        examItem = new ExamItem();
        //        examItemNode = examNode.ChildNodes.Item(i);
        //        examItem.EName = examItemNode.SelectNodes("ename").Item(0).InnerText;
        //        examItem.CName = examItemNode.SelectNodes("cname").Item(0).InnerText;

        //        examItem.Result = examItemNode.SelectNodes("result").Item(0).InnerText;
        //        examItem.Unit = examItemNode.SelectNodes("unit").Item(0).InnerText;
        //        examItem.Range = examItemNode.SelectNodes("range").Item(0).InnerText;
        //        examItem.Prompt = examItemNode.SelectNodes("prompt").Item(0).InnerText;

        //        exam.ExamItems.Add(examItem);
        //    }

        //    return exam;
        //}
    }
}
