﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.IO;
using AtomEHR.Business;
using AtomEHR.Models;

namespace AtomEHR.公共卫生.Module.健康档案.体检回执单.回执A4
{
    public partial class 回执心电 : DevExpress.XtraReports.UI.XtraReport
    {
        public 回执心电()
        {
            InitializeComponent();
        }

        public 回执心电(byte[] obj)
        {
            InitializeComponent();
            this.xrPictureBox1.Image = byteArrayToImage(obj);
        }

        public 回执心电(string str_recordNum,string str_Date)
        {
            InitializeComponent();
            string ftpxz = new bll健康档案().Get_健康档案封面(str_recordNum).Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.街道].ToString();
            string ftprq = str_Date;
            string ftpdn = str_recordNum.Substring(2);
            AtomEHR.公共卫生.util.FtpUtil ftpcl = new util.FtpUtil("192.168.10.135", "21", "qzxdt", "qzyy@163.com");
            byte[] ecg = ftpcl.Download(@"ftp://192.168.10.135/心电图/" + ftpxz + "/" + ftprq + "/" + ftpdn + ".JPG");
            if (ecg != null && ecg.Length > 0)
            {
                this.xrPictureBox1.Image = byteArrayToImage(ecg);
            }
        }

        /// <summary>
        /// 字节数组生成图片
        /// </summary>
        /// <param name="Bytes">字节数组</param>
        /// <returns>图片</returns>
        private Image byteArrayToImage(byte[] Bytes)
        {
            //Bitmap img = new Bitmap(0,0);
            try
            {
                using (MemoryStream ms = new MemoryStream(Bytes))
                {
                    Image outputImg = Image.FromStream(ms);
                    outputImg.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    return outputImg;
                }
            }
            catch 
            {
                return null;
            }
        }

    }
}
