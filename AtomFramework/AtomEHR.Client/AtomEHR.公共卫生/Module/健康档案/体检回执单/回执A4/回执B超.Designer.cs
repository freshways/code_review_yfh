﻿namespace AtomEHR.公共卫生.Module.健康档案.体检回执单.回执A4
{
    partial class 回执B超
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrPictureBoxB超医生 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.txt体检日期 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt医师 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt超声提示 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt超声所见 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt档案号 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt姓名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt性别 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt身份证 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt年龄 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt部位 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt卫生院 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBoxB超医生,
            this.txt体检日期,
            this.txt医师,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel5,
            this.txt超声提示,
            this.txt超声所见,
            this.xrLine3,
            this.xrLabel1,
            this.xrLine4,
            this.xrLine1,
            this.xrTable1,
            this.txt卫生院});
            this.Detail.Font = new System.Drawing.Font("宋体", 9.75F);
            this.Detail.HeightF = 969F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPictureBoxB超医生
            // 
            this.xrPictureBoxB超医生.LocationFloat = new DevExpress.Utils.PointFloat(514.9402F, 759.46F);
            this.xrPictureBoxB超医生.Name = "xrPictureBoxB超医生";
            this.xrPictureBoxB超医生.SizeF = new System.Drawing.SizeF(193.071F, 46.45862F);
            this.xrPictureBoxB超医生.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // txt体检日期
            // 
            this.txt体检日期.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt体检日期.LocationFloat = new DevExpress.Utils.PointFloat(514.9401F, 818.8334F);
            this.txt体检日期.Name = "txt体检日期";
            this.txt体检日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt体检日期.SizeF = new System.Drawing.SizeF(193.071F, 27.16669F);
            this.txt体检日期.StylePriority.UseFont = false;
            this.txt体检日期.StylePriority.UseTextAlignment = false;
            this.txt体检日期.Text = "2018年10月24日";
            this.txt体检日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt医师
            // 
            this.txt医师.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt医师.LocationFloat = new DevExpress.Utils.PointFloat(514.9401F, 768.7501F);
            this.txt医师.Name = "txt医师";
            this.txt医师.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt医师.SizeF = new System.Drawing.SizeF(193.0711F, 27.16669F);
            this.txt医师.StylePriority.UseFont = false;
            this.txt医师.StylePriority.UseTextAlignment = false;
            this.txt医师.Text = "检查医生";
            this.txt医师.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("宋体", 10F);
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(152.0833F, 871.8751F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(281.2562F, 27.16669F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "本次诊断结果，仅供参考";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(351.0356F, 818.8334F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(163.9044F, 27.16669F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "检查日期：";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(351.0356F, 768.7501F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(163.9044F, 27.16669F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "检查医生：";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // txt超声提示
            // 
            this.txt超声提示.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt超声提示.LocationFloat = new DevExpress.Utils.PointFloat(62.81201F, 658.7499F);
            this.txt超声提示.Multiline = true;
            this.txt超声提示.Name = "txt超声提示";
            this.txt超声提示.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt超声提示.SizeF = new System.Drawing.SizeF(667.5212F, 90.70831F);
            this.txt超声提示.StylePriority.UseFont = false;
            this.txt超声提示.StylePriority.UseTextAlignment = false;
            this.txt超声提示.Text = "    超声提示：\r\n\r\n   \t{0}\r\n";
            this.txt超声提示.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // txt超声所见
            // 
            this.txt超声所见.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt超声所见.LocationFloat = new DevExpress.Utils.PointFloat(62.81201F, 217.4167F);
            this.txt超声所见.Multiline = true;
            this.txt超声所见.Name = "txt超声所见";
            this.txt超声所见.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt超声所见.SizeF = new System.Drawing.SizeF(667.5212F, 418.8334F);
            this.txt超声所见.StylePriority.UseFont = false;
            this.txt超声所见.StylePriority.UseTextAlignment = false;
            this.txt超声所见.Text = "\t\r\n    超声所见：\r\n\r\n    {0}\r\n\r\n";
            this.txt超声所见.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine3
            // 
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(10.00012F, 97.62503F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(745.2344F, 10F);
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(10.00012F, 70.45835F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(745.2343F, 27.16668F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "腹部超声报告单";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine4
            // 
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 636.2501F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(745.2299F, 10F);
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(9.99558F, 207.4167F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(745.2343F, 10F);
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("宋体", 12F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(27.07721F, 110F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow4});
            this.xrTable1.SizeF = new System.Drawing.SizeF(703.256F, 97.41665F);
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.txt档案号,
            this.xrTableCell3,
            this.txt姓名,
            this.xrTableCell2,
            this.txt性别});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.711111111111111D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "档 案 号：";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell1.Weight = 1.1426281736870918D;
            // 
            // txt档案号
            // 
            this.txt档案号.Name = "txt档案号";
            this.txt档案号.StylePriority.UseTextAlignment = false;
            this.txt档案号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt档案号.Weight = 1.9407067311211692D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "姓名：";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell3.Weight = 1.0166656029743948D;
            // 
            // txt姓名
            // 
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.StylePriority.UseTextAlignment = false;
            this.txt姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt姓名.Weight = 1.2354174478628306D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "性别：";
            this.xrTableCell2.Weight = 0.73957730169278557D;
            // 
            // txt性别
            // 
            this.txt性别.Name = "txt性别";
            this.txt性别.StylePriority.UseTextAlignment = false;
            this.txt性别.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt性别.Weight = 1.3773493984935685D;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.txt身份证,
            this.xrTableCell13,
            this.txt年龄,
            this.xrTableCell15});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 0.711111111111111D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "身份证号：";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell9.Weight = 1.1426281736870918D;
            // 
            // txt身份证
            // 
            this.txt身份证.Name = "txt身份证";
            this.txt身份证.StylePriority.UseTextAlignment = false;
            this.txt身份证.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt身份证.Weight = 1.94070654989175D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "年龄：";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell13.Weight = 1.016665996127522D;
            // 
            // txt年龄
            // 
            this.txt年龄.Name = "txt年龄";
            this.txt年龄.StylePriority.UseTextAlignment = false;
            this.txt年龄.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt年龄.Weight = 0.55950513143008052D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = " 岁";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell15.Weight = 2.7928388046953949D;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.txt部位});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 0.71111111111111092D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseTextAlignment = false;
            this.xrTableCell25.Text = "部    位：";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell25.Weight = 1.1426280973931309D;
            // 
            // txt部位
            // 
            this.txt部位.Name = "txt部位";
            this.txt部位.StylePriority.UseTextAlignment = false;
            this.txt部位.Text = " 腹部";
            this.txt部位.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt部位.Weight = 6.3097165584387094D;
            // 
            // txt卫生院
            // 
            this.txt卫生院.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Bold);
            this.txt卫生院.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 26.625F);
            this.txt卫生院.Name = "txt卫生院";
            this.txt卫生院.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt卫生院.SizeF = new System.Drawing.SizeF(745.2344F, 43.83335F);
            this.txt卫生院.StylePriority.UseFont = false;
            this.txt卫生院.StylePriority.UseTextAlignment = false;
            this.txt卫生院.Text = "X X X 卫 生 院";
            this.txt卫生院.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 47F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 40.04167F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // 回执B超
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.DesignerOptions.ShowExportWarnings = false;
            this.DesignerOptions.ShowPrintingWarnings = false;
            this.Margins = new System.Drawing.Printing.Margins(30, 30, 47, 40);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell txt档案号;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell txt姓名;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell txt身份证;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell txt年龄;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell txt部位;
        private DevExpress.XtraReports.UI.XRLabel txt卫生院;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel txt超声提示;
        private DevExpress.XtraReports.UI.XRLabel txt超声所见;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell txt性别;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel txt体检日期;
        private DevExpress.XtraReports.UI.XRLabel txt医师;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBoxB超医生;
    }
}
