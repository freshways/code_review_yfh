﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Text;
using System.Data;
using AtomEHR.Models;
using AtomEHR.Business;

namespace AtomEHR.公共卫生.Module.健康档案.体检回执单.回执A4
{
    public partial class report异常结果及健康指导 : DevExpress.XtraReports.UI.XtraReport
    {
        StringBuilder sb_症状 = new StringBuilder();
        StringBuilder sb_指导 = new StringBuilder();

        bll常用字典 bll_常用字典 = new bll常用字典();

        /// <summary>
        /// recordNum 个人档案编号
        /// examinationDate 体检日期
        /// identificationDate 辨识日期
        /// 如果不是老年人(没做体质辨识)的identificationDate传""或null
        /// </summary>
        /// <param name="recordNum"></param>
        /// <param name="examinationDate"></param>
        /// <param name="identificationDate"></param>
        public report异常结果及健康指导(string recordNum, string examinationDate, string identificationDate)
        {
            InitializeComponent();

            bll健康体检 bll_健康体检 = new bll健康体检();
            DataRow dr_健康体检 = bll_健康体检.Get_One体检(recordNum, examinationDate);
            Analyse_PhysicalExamination(dr_健康体检);

            if (!string.IsNullOrEmpty(identificationDate))
            {
                bll老年人中医药特征管理 bll_体质辨识 = new bll老年人中医药特征管理();
                DataRow dr_体质辨识 = bll_体质辨识.Get_One中医药管理(recordNum, identificationDate);
                Analyse_PhysicalIdentification(dr_体质辨识);
            }

            lab_姓名.Text = new bll健康档案().GetRowInfoByKey(recordNum).Tables[0].Rows[0][tb_健康档案.姓名].ToString();
            lab_异常症状.Text = sb_症状.ToString();
            Rich_指导.Text = sb_指导.ToString();
        }

        /// <summary>
        /// 健康体检指导
        /// </summary>
        /// <param name="dr"></param>
        private void Analyse_PhysicalExamination(DataRow dr)
        {
            //BMI
            float fl_BMI;
            if (float.TryParse(dr[tb_健康体检.体重指数].ToString(), out fl_BMI))
            {
                if (fl_BMI < 18.5)
                {
                    sb_症状.Append("【体重偏轻（体重指数：" + fl_BMI + ")" + ", 】");
                    sb_指导.AppendLine("体重偏轻（体重指数：" + fl_BMI + ")");
                    sb_指导.AppendLine("建议：补充营养。");
                }
                else if (fl_BMI >= 24)
                {
                    sb_症状.Append("【体重超重（体重指数：" + fl_BMI + "), 】");
                    sb_指导.AppendLine("体重超重（体重指数：" + fl_BMI + ")");
                    sb_指导.AppendLine("建议：少食，减盐，控制体重。");
                }
                else if (fl_BMI >= 24 && fl_BMI < 27.9)
                {
                    sb_症状.Append("【体重偏胖（体重指数：" + fl_BMI + ")" + ", 】");
                    sb_指导.AppendLine("体重偏胖（体重指数：" + fl_BMI + ")");
                    sb_指导.AppendLine("建议：少食，减盐，控制体重。");
                }
                else if (fl_BMI >= 28 && fl_BMI < 32)
                {
                    sb_症状.Append("【体重肥胖（体重指数：" + fl_BMI + ")" + ", 】");
                    sb_指导.AppendLine("体重肥胖（体重指数：" + fl_BMI + ")");
                    sb_指导.AppendLine("建议：少食，减盐，控制体重。");
                }
                else if (fl_BMI > 32)
                {
                    sb_症状.Append("【体重非常肥胖（体重指数：" + fl_BMI + ")" + ", 】");
                    sb_指导.AppendLine("体重非常肥胖（体重指数：" + fl_BMI + ")");
                    sb_指导.AppendLine("建议：少食，减盐，控制体重。");
                }
            }

            //高血压
            //应泉庄指导,目前只判断右侧血压......
            int i_Right_SBP, i_Right_DBP;
            if (int.TryParse(dr[tb_健康体检.血压右侧1].ToString(), out i_Right_SBP) && int.TryParse(dr[tb_健康体检.血压右侧2].ToString(), out i_Right_DBP))
            {
                if (i_Right_SBP >= 140 || i_Right_DBP >= 90)
                {
                    sb_症状.Append("【血压偏高（" + i_Right_SBP + "/" + i_Right_DBP + ")" + ", 】");
                    sb_指导.AppendLine("血压偏高（" + i_Right_SBP + "/" + i_Right_DBP + ")");
                    sb_指导.AppendLine("建议连续三日测血压以明确诊断。高血压建议：\nA.注意劳逸结合，保持心境平和；\nB.坚持有氧运动；\nC.戒烟限酒；\nD.少吃盐；\nE.控制体重。\n治疗：上述非药物治疗后血压仍高者，应考虑用降压药。定期测量血压。");
                }
            }


            //糖尿病
            float fl_GLU;
            if (float.TryParse(dr[tb_健康体检.空腹血糖].ToString(), out fl_GLU))
            {
                if (fl_GLU != 0 && fl_GLU > 6.40)
                {
                    sb_症状.Append("【血糖偏高(GLU:" + fl_GLU + "mmol/l)（3.89-6.40）】");
                    sb_指导.AppendLine("血糖偏高(GLU:" + fl_GLU + "mmol/l)（3.89-6.40）");
                    sb_指导.AppendLine("血糖偏高，建议：进一步复查。");
                }

                if (fl_GLU != 0 && fl_GLU < 3.89)
                {
                    sb_症状.Append("【血糖偏低(GLU:" + fl_GLU + "mmol/l)（3.89-6.40）" + "；】");
                    sb_指导.AppendLine("血糖偏低(GLU:" + fl_GLU + "mmol/l)（3.89-6.40）" + "；");
                    sb_指导.AppendLine("建议复查空腹血糖。糖尿病建议：\n1、糖尿病者积极配合医院规范化治疗。\n2、接受多种形式的健康教育知识。\n3、调整饮食，控制热量的摄入。按医嘱进行自我监测。\n4、成人35岁以后都应每年查血糖和尿糖，早发现早治疗。\n5、改变不良生活方式，忌烟酒，加强运动。\n6、保持心态平衡，克服各种紧张压力，培养高尚情操，多爬山郊游等。\n7、定期复查");
                }
            }

            //血常规
            float fl_HGB, fl_WBC, fl_BPC;
            string str_BR = "【血常规异常：";
            if (float.TryParse(dr[tb_健康体检.白细胞].ToString(), out fl_HGB))
            {
                if (fl_HGB > 9.9 || fl_HGB < 3.9)
                {
                    str_BR += "白细胞:" + fl_HGB + "(3.9-9.9)";
                }
            }

            if (float.TryParse(dr[tb_健康体检.血红蛋白].ToString(), out fl_WBC))
            {
                if (fl_WBC > 179 || fl_WBC < 116)
                {
                    str_BR += "血红蛋白:" + fl_WBC + "(116-179)";
                }
            }

            if (float.TryParse(dr[tb_健康体检.血小板].ToString(), out fl_BPC))
            {
                if (fl_BPC > 302.9 || fl_BPC < 98.7)
                {
                    str_BR += "血小板:" + fl_BPC + "(98.7-302.9)";
                }
            }

            if (!str_BR.Equals("【血常规异常："))
            {
                sb_症状.Append(str_BR + ", 】");
                sb_指导.AppendLine(str_BR + " 】");
                sb_指导.AppendLine("建议：医院复查");
            }

            //尿常规
            string str_PRO, str_UGLU, str_KET, str_BLD;
            string str_RU = "【尿常规异常：";
            str_PRO = dr[tb_健康体检.尿蛋白].ToString().Trim();
            str_UGLU = dr[tb_健康体检.尿糖].ToString().Trim();
            str_KET = dr[tb_健康体检.尿酮体].ToString().Trim();
            str_BLD = dr[tb_健康体检.尿潜血].ToString().Trim();
            if (!string.IsNullOrEmpty(str_PRO) && "-".Equals(str_PRO))
            {
                str_RU += "尿蛋白:" + str_PRO;
            }

            if (!string.IsNullOrEmpty(str_UGLU) && "-".Equals(str_UGLU))
            {
                str_RU += "尿酮体:" + str_UGLU;
            }

            if (!string.IsNullOrEmpty(str_KET) && "-".Equals(str_KET))
            {
                str_RU += "尿潜血:" + str_KET;
            }

            if (!string.IsNullOrEmpty(str_BLD) && "-".Equals(str_BLD))
            {
                str_RU += "尿糖:" + str_BLD;
            }

            if (!str_RU.Equals("【尿常规异常："))
            {
                sb_症状.Append(str_RU + ", 】");
                sb_指导.AppendLine(str_RU + " 】");
                sb_指导.AppendLine("建议：请结合临床，进一步复查。");
            }

            //肝功能
            float fl_SGPT, fl_SGOT, fl_ALB, fl_TBil, fl_DBil;
            string str_Liver = "【肝功异常：";
            if (float.TryParse(dr[tb_健康体检.血清谷丙转氨酶].ToString(), out fl_SGPT))
            {
                if (fl_SGPT > 40 || fl_SGPT < 0)
                {
                    str_Liver += "血清谷丙转氨酶" + fl_SGPT + "(0-40);";
                }
            }

            if (float.TryParse(dr[tb_健康体检.血清谷草转氨酶].ToString(), out fl_SGOT))
            {
                if (fl_SGOT > 41 || fl_SGOT < 0)
                {
                    str_Liver += "血清谷草转氨酶" + fl_SGOT + "(0-41);";
                }
            }

            if (float.TryParse(dr[tb_健康体检.白蛋白].ToString(), out fl_ALB))
            {
                if (true)
                {
                }
            }

            if (float.TryParse(dr[tb_健康体检.总胆红素].ToString(), out fl_TBil))
            {
                if (fl_TBil > 20.5 || fl_TBil < 2)
                {
                    str_Liver += "总胆红素" + fl_TBil + "(2-20.5);";
                }
            }

            if (float.TryParse(dr[tb_健康体检.结合胆红素].ToString(), out fl_DBil))
            {
                if (fl_DBil > 7 || fl_DBil < 0)
                {
                    str_Liver += "结合胆红素" + fl_DBil + "(0-7.0);";
                }
            }

            if (!str_Liver.Equals("【肝功异常："))
            {
                sb_症状.Append(str_Liver + ", 】");
                sb_指导.AppendLine(str_Liver + " 】");
                sb_指导.AppendLine(@"肝功异常建议：导致增高的原因很多，应定期复查。1、 注意在下次检查前，三天禁烟及动物脂肪等高脂肪食品，后抽血。2、如果仍高，应到医院正规治疗。");
            }

            //肾功能
            float fl_SCr, fl_BUN, fl_SP, fl_SS;
            string str_Renal = "【肾功异常：";
            if (float.TryParse(dr[tb_健康体检.血清肌酐].ToString(), out fl_SCr))
            {
                if (fl_SCr > 115 || fl_SCr < 53)
                {
                    str_Renal += "血清肌酐:" + fl_SCr + "(53-115);";
                }
            }

            if (float.TryParse(dr[tb_健康体检.血尿素氮].ToString(), out fl_BUN))
            {
                if (fl_BUN > 8.7 || fl_BUN < 1.7)
                {
                    str_Renal += "血尿素氮:" + fl_BUN + "(1.7-8.7);";
                }
            }

            if (float.TryParse(dr[tb_健康体检.血钾浓度].ToString(), out fl_SP))
            {
                if (true)
                {
                }
            }

            if (float.TryParse(dr[tb_健康体检.血钠浓度].ToString(), out fl_SS))
            {
                if (true)
                {
                }
            }

            if (!str_Renal.Equals("【肾功异常："))
            {
                sb_症状.Append(str_Renal + ", 】 ");
                sb_指导.AppendLine(str_Renal + " 】");
                sb_指导.AppendLine("建议：请结合临床，进一步复查。");
            }

            //血脂
            float fl_TC, fl_TG, fl_LDL, fl_HDL;
            string str_bloodFat = "【血脂异常：";
            if (float.TryParse(dr[tb_健康体检.总胆固醇].ToString(), out fl_TC))
            {
                str_bloodFat += "总胆固醇:" + fl_TC + "(2.34-5.20);";
            }

            if (float.TryParse(dr[tb_健康体检.甘油三酯].ToString(), out fl_TG))
            {
                str_bloodFat += "甘油三酯:" + fl_TG + "(0.7-1.7);";
            }

            if (float.TryParse(dr[tb_健康体检.血清低密度脂蛋白胆固醇].ToString(), out fl_LDL))
            {
                str_bloodFat += "低密度脂蛋白:" + fl_LDL + "(0-4.13);";
            }

            if (float.TryParse(dr[tb_健康体检.血清高密度脂蛋白胆固醇].ToString(), out fl_HDL))
            {
                str_bloodFat += "高密度脂蛋白:" + fl_HDL + "(0.77-2.25);";
            }

            if (!str_bloodFat.Equals("【血脂异常："))
            {
                sb_症状.Append(str_bloodFat + ", 】");
                sb_指导.AppendLine(str_bloodFat + " 】");
                sb_指导.AppendLine("建议：要注意清淡饮食，不吃高脂餐，控制进食量，适当运动，戒烟限酒。");
            }

            //心电图
            string str_ECG = dr[tb_健康体检.心电图].ToString();
            if (!string.IsNullOrEmpty(str_ECG) && !"1".Equals(str_ECG))
            {
                string Abnormal = bll_常用字典.DicMap("jktj_xdt", str_ECG, ',') + dr[tb_健康体检.心电图异常].ToString();
                sb_症状.Append("【心电图异常：" + Abnormal + ", 】");
                sb_指导.AppendLine("心电图异常：" + Abnormal + ";");
                sb_指导.AppendLine("建议：请结合临床，进一步复查。");
            }

            //B超
            if ("2".Equals(dr[tb_健康体检.B超].ToString()))
            {
                sb_症状.Append("【B超异常：" + dr[tb_健康体检.B超其他].ToString() + "。】");
                sb_指导.AppendLine("B超异常，建议：请结合临床，进一步复查。");
            }
            else
            {
                sb_指导.AppendLine("B超：" + bll_常用字典.DicMap("zcyc", dr[tb_健康体检.B超].ToString()) + ";");
            }
        }

        /// <summary>
        /// 体质辨识指导
        /// </summary>
        /// <param name="dr"></param>
        private void Analyse_PhysicalIdentification(DataRow dr)
        {
            sb_指导.AppendLine("中医体质辨识结果：");

            //气虚质辨识
            if (!string.IsNullOrEmpty(dr[tb_老年人中医药特征管理.气虚质辨识].ToString()))
            {
                sb_指导.AppendLine("气虚质：" + bll_常用字典.DicMap("zytzbs", dr[tb_老年人中医药特征管理.气虚质辨识].ToString()));
                sb_指导.AppendLine(@"【总体特征】元气不足，以疲乏、气短、自汗等表现为主要特征。
【情志调摄】宜保持稳定乐观的心态，不可过度劳神。宜欣赏节奏明快的音乐，如笛子曲《喜相逢》等。
【饮食调养】宜选用性平偏温、健脾益气的食物，如大米、小米、南瓜、胡萝卜、山药、大枣、香菇、莲子、白扁豆、黄豆、豆腐、鸡肉、鸡蛋、鹌鹑（蛋）、牛肉等。尽量少吃或不吃槟榔、生萝卜等耗气的食物。不宜多食生冷苦寒、辛辣燥热的食物。
【起居调摄】提倡劳逸结合，不要过于劳作，以免损伤正气。平时应避免汗出受风。居室环境应采用明亮的暖色调。
【运动保健】宜选择比较柔和的传统健身项目，如八段锦。在做完全套八段锦动作后，将“两手攀足固肾腰”和“攒拳怒目增力气”各加做1～3遍。避免剧烈运动。还可采用提肛法防止脏器下垂，提肛法：全身放松，注意力集中在会阴肛门部。首先吸气收腹，收缩并提升肛门，停顿2～3秒之后，再缓慢放松呼气，如此反复10～15次。
【穴位保健】（1）选穴：气海、关元。（2）定位：气海位于下腹部，前正中线上，当脐中下1.5寸；关元位于下腹部，前正中线上，当脐下3寸（3）操作：用掌根着力于穴位，做轻柔缓和的环旋活动，每个穴位按揉2～3分钟，每天操作1～2次。还可以采用艾条温和灸，增加温阳益气的作用。点燃艾条或借助温灸盒，对穴位进行温灸，每次10分钟。艾条温和灸点燃端要与皮肤保持2～3厘米的距离，不要烫伤皮肤。温和灸可每周操作1次。");
            }

            //阳虚质辨识
            if (!string.IsNullOrEmpty(dr[tb_老年人中医药特征管理.阳虚质辨识].ToString()))
            {
                sb_指导.AppendLine("阳虚质：" + bll_常用字典.DicMap("zytzbs", dr[tb_老年人中医药特征管理.阳虚质辨识].ToString()));
                sb_指导.AppendLine(@"【总体特征】阳气不足，以畏寒怕冷、手足不温等表现为主要特征。
【情志调摄】宜保持积极向上的心态，正确对待生活中的不利事件，及时调节自己的消极情绪。宜欣赏激昂、高亢、豪迈的音乐，如《黄河大合唱》等。
【饮食调养】宜选用甘温补脾阳、温肾阳为主的食物，如羊肉、鸡肉、带鱼、黄鳝、虾、刀豆、韭菜、茴香、核桃、栗子、腰果、松子、红茶、生姜等。少食生冷、苦寒、黏腻食物，如田螺、螃蟹、海带、紫菜、芹菜、苦瓜、冬瓜、西瓜、香蕉、柿子、甘蔗、梨、绿豆、蚕豆、绿茶、冷冻饮料等。即使在盛夏也不要过食寒凉之品。
【起居调摄】居住环境以温和的暖色调为宜，不宜在阴暗、潮湿、寒冷的环境下长期工作和生活。平时要注意腰部、背部和下肢保暖。白天保持一定活动量，避免打盹瞌睡。睡觉前尽量不要饮水，睡前将小便排净。
【运动保健】宜在阳光充足的环境下适当进行舒缓柔和的户外活动，尽量避免在大风、大寒、大雪的环境中锻炼。日光浴、空气浴是较好的强身壮阳之法。也可选择八段锦，在完成整套动作后将“五劳七伤往后瞧”和“两手攀足固肾腰”加做1～3遍。
【穴位保健】（1）选穴：关元、命门。（2）定位：关元（位置见气虚质）。命门位于腰部，当后正中线上,第2腰椎棘突下凹陷中。（3）操作：两穴均可采用温和灸（见气虚质）的方法，每周进行1次。关元穴还可采用掌根揉法（见气虚质），按揉每穴2～3分钟，每天1～2次。也可配合摩擦腰肾法温肾助阳，以手掌鱼际、掌根或拳背摩擦两侧腰骶部，每次操作约10分钟，以摩至皮肤温热为度，每天1次。");
            }

            //阴虚质辨识
            if (!string.IsNullOrEmpty(dr[tb_老年人中医药特征管理.阴虚质辨识].ToString()))
            {
                sb_指导.AppendLine("阴虚质：" + bll_常用字典.DicMap("zytzbs", dr[tb_老年人中医药特征管理.阴虚质辨识].ToString()));
                sb_指导.AppendLine(@"【总体特征】阴液亏少，以口燥咽干、手足心热等表现为主要特征。
【情志调摄】宜加强自我修养、培养自己的耐性，尽量减少与人争执、动怒，不宜参加竞争胜负的活动，可在安静、优雅环境中练习书法、绘画等。有条件者可以选择在环境清新凉爽的海边、山林旅游休假。 宜欣赏曲调轻柔、舒缓的音乐，如舒伯特《小夜曲》等。
【饮食调养】宜选用甘凉滋润的食物，如鸭肉、猪瘦肉、百合、黑芝麻、蜂蜜、荸荠、鳖、海蜇、海参、甘蔗、银耳、燕窝等。少食温燥、辛辣、香浓的食物，如羊肉、韭菜、茴香、辣椒、葱、蒜、葵花子、酒、咖啡、浓茶，以及荔枝、龙眼、樱桃、杏、大枣、核桃、栗子等。
【起居调摄】居住环境宜安静，睡好“子午觉”。避免熬夜及在高温酷暑下工作，不宜洗桑拿、泡温泉。节制房事，勿吸烟。注意防晒，保持皮肤湿润，宜选择选择蚕丝等清凉柔和的衣物。
【运动保健】宜做中小强度的运动项目，控制出汗量，及时补充水分。不宜进行大强度、大运动量的锻炼，避免在炎热的夏天或闷热的环境中运动。可选择八段锦，在做完八段锦整套动作后将“摇头摆尾去心火”和“两手攀足固肾腰”加做1～3遍。也可选择太极拳、太极剑等。
【穴位保健	】（1）选穴：太溪、三阴交。（2）定位：太溪位于足内侧，内踝后方，当内踝尖与跟腱之间的凹陷处；三阴交位于小腿内侧，当足内踝尖上3寸，胫骨内侧缘后方。（3）操作：采用指揉的方法（见平和质），每个穴位按揉2～3分钟，每天操作1～2次。");
            }

            //痰湿质辨识
            if (!string.IsNullOrEmpty(dr[tb_老年人中医药特征管理.痰湿质辨识].ToString()))
            {
                sb_指导.AppendLine("痰湿质：" + bll_常用字典.DicMap("zytzbs", dr[tb_老年人中医药特征管理.痰湿质辨识].ToString()));
                sb_指导.AppendLine(@"【总体特征】痰湿凝聚，以形体肥胖、腹部肥满、口黏苔腻等表现为主要特征。
【情志调摄】宜多参加社会活动，培养广泛的兴趣爱好。宜欣赏激进、振奋的音乐，如二胡《赛马》等。
【饮食调养】宜选用健脾助运、祛湿化痰的食物，如冬瓜、白萝卜、薏苡仁、赤小豆、荷叶、山楂、生姜、荠菜、紫菜、海带、鲫鱼、鲤鱼、鲈鱼、文蛤等。少食肥、甜、油、黏（腻）的食物。
【起居调摄】居住环境宜干燥，不宜潮湿，穿衣面料以棉、麻、丝等透气散湿的天然纤维为佳，尽量保持宽松，有利于汗液蒸发，祛除体内湿气。晚上睡觉枕头不宜过高，防止打鼾加重；早睡早起，不要过于安逸，勿贪恋沙发和床榻。
【运动保健】坚持长期运动锻炼，强度应根据自身的状况循序渐进。不宜在阴雨季节、天气湿冷的气候条件下运动。可选择快走、武术以及打羽毛球等，使松弛的肌肉逐渐变得结实、致密。如果体重过重、膝盖受损，可选择游泳。
【穴位保健】（1）选穴：丰隆、足三里。（2）定位：足三里（位置见气虚质）。丰隆位于小腿前外侧，当外踝尖上8寸，条口外，距胫骨前缘二横指处。（3）操作：采用指揉法（见平和质）。");
            }

            //湿热质辨识
            if (!string.IsNullOrEmpty(dr[tb_老年人中医药特征管理.湿热质辨识].ToString()))
            {
                sb_指导.AppendLine("湿热质：" + bll_常用字典.DicMap("zytzbs", dr[tb_老年人中医药特征管理.湿热质辨识].ToString()));
                sb_指导.AppendLine(@"【总体特征】湿热内蕴，以面垢油光、口苦、苔黄腻等表现为主要特征。
【情志调摄】宜稳定情绪，尽量避免烦恼，可选择不同形式的兴趣爱好。宜欣赏曲调悠扬的乐曲，如古筝《高山流水》等。
【饮食调养】宜选用甘寒或苦寒的清利化湿食物，如绿豆（芽）、绿豆糕、绿茶、芹菜、黄瓜、苦瓜、西瓜、冬瓜、薏苡仁、赤小豆、马齿苋、藕等。少食羊肉、动物内脏等肥厚油腻之品，以及韭菜、生姜、辣椒、胡椒、花椒及火锅、烹炸、烧烤等辛温助热的食物。
【起居调摄】居室宜干燥、通风良好，避免居处潮热，可在室内用除湿器或空调改善湿、热的环境。选择款式宽松，透气性好的天然棉、麻、丝质服装。注意个人卫生，预防皮肤病变。保持充足而有规律的睡眠，睡前半小时不宜思考问题、看书、看情节紧张的电视节目，避免服用兴奋饮料，不宜吸烟饮酒。保持二便通畅，防止湿热积聚。
【运动保健】宜做中长跑、游泳、各种球类、武术等强度较大的锻炼。夏季应避免在烈日下长时间活动，在秋高气爽的季节，经常选择爬山登高，更有助于祛除湿热。也可做八段锦，在完成整套动作后将“双手托天理三焦”和“调理脾胃须单举”加做1～3遍，每日1遍。
【穴位保健】（1）选穴：支沟、阴陵泉。（2）定位：支沟穴位于前臂背侧，当阳池与肘尖的连线上，腕背横纹上3寸，尺骨与桡骨之间（见图7）。阴陵泉位于小腿内侧，当胫骨内侧踝后下凹陷处。（3）操作：采用指揉法（见平和质）。阴陵泉还可以选择刮痧，先涂刮痧油，用刮痧板与皮肤呈45°角在穴位区域从上往下刮，以皮肤潮红或出痧点为度。");
            }

            //血瘀质辨识
            if (!string.IsNullOrEmpty(dr[tb_老年人中医药特征管理.血瘀质辨识].ToString()))
            {
                sb_指导.AppendLine("血瘀质：" + bll_常用字典.DicMap("zytzbs", dr[tb_老年人中医药特征管理.血瘀质辨识].ToString()));
                sb_指导.AppendLine(@"【总体特征】血行不畅，以肤色晦黯、舌质紫黯等表现为主要特征。
【情志调摄】遇事宜沉稳，努力克服浮躁情绪。宜欣赏流畅抒情的音乐，如《春江花月夜》等。
【饮食调养】宜选用具有调畅气血作用的食物，如生山楂、醋、玫瑰花、桃仁（花）、黑豆、油菜等。少食收涩、寒凉、冰冻之物，如乌梅、柿子、石榴、苦瓜、花生米，以及高脂肪、高胆固醇、油腻食物，如蛋黄、虾、猪头肉、猪脑、奶酪等。还可少量饮用葡萄酒、糯米甜酒，有助于促进血液运行，但高血压和冠心病等患者不宜饮用。女性月经期间慎用活血类食物。
【起居调摄】居室宜温暖舒适，不宜在阴暗、寒冷的环境中长期工作和生活。衣着宜宽松，注意保暖，保持大便通畅。不宜贪图安逸，宜在阳光充足的时候进行户外活动。避免长时间打麻将、久坐、看电视等。
【运动保健】宜进行有助于促进气血运行的运动项目，持之以恒。如步行健身法，或者八段锦，在完成整套动作后将“左右开弓似射雕”和“背后七颠百病消”加做1～3遍。避免在封闭环境中进行锻炼。锻炼强度视身体情况而定，不宜进行大强度、大负荷运动，以防意外。
【穴位保健】（1）选穴：期门、血海。（2）定位：期门位于胸部，当乳头直下，第6肋间隙，前正中线旁开4寸。血海：屈膝，在大腿内侧，髌底内侧端上2寸，当股四头肌内侧头的隆起处。（3）操作：采用指揉法（见平和质）。");
            }

            //气郁质辨识
            if (!string.IsNullOrEmpty(dr[tb_老年人中医药特征管理.气郁质辨识].ToString()))
            {
                sb_指导.AppendLine("气郁质：" + bll_常用字典.DicMap("zytzbs", dr[tb_老年人中医药特征管理.气郁质辨识].ToString()));
                sb_指导.AppendLine(@"【总体特征】气机郁滞，以神情抑郁、紧张焦虑等表现为主要特征。
【情志调摄】宜乐观开朗，多与他人相处，不苛求自己也不苛求他人。如心境抑郁不能排解时，要积极寻找原因，及时向朋友倾诉。宜欣赏节奏欢快、旋律优美的乐曲如《金蛇狂舞》等，还适宜看喜剧、励志剧，以及轻松愉悦的相声表演。
【饮食调养】宜选用具有理气解郁作用的食物，如黄花菜、菊花、玫瑰花、茉莉花、大麦、金橘、柑橘、柚子等。少食收敛酸涩的食物，如石榴、乌梅、青梅、杨梅、草莓、杨桃、酸枣、李子、柠檬、南瓜、泡菜等。
【起居调摄】尽量增加户外活动和社交，防止一人独处时心生凄凉。居室保持安静，宜宽敞、明亮。平日保持有规律的睡眠，睡前避免饮用茶、咖啡和可可等饮料。衣着宜柔软、透气、舒适。
【运动保健】宜多参加群体性体育运动项目，坚持做较大强度、较大负荷的“发泄式”锻炼，如跑步、登山、游泳。也可参与下棋、打牌等娱乐活动，分散注意力。
【穴位保健】（1）选穴：合谷、太冲穴（2）定位：合谷位于手背，第1、2掌骨间，当第2掌骨桡侧的中点处。太冲位于足背侧，当第1跖骨间隙的后方凹陷处。（3）操作：采用指揉的方法（见平和质）");
            }

            //特禀质辨识
            if (!string.IsNullOrEmpty(dr[tb_老年人中医药特征管理.特禀质辨识].ToString()))
            {
                sb_指导.AppendLine("特禀质：" + bll_常用字典.DicMap("zytzbs", dr[tb_老年人中医药特征管理.特禀质辨识].ToString()));
                sb_指导.AppendLine(@" 【总体特征】过敏体质者，禀赋不耐、异气外侵，以过敏反应等为主要特征；先天失常者为另一类特禀质，以禀赋异常为主要特征。
【情志调摄】过敏体质的人因对过敏原敏感，容易产生紧张、焦虑等情绪，因此要在尽量避免过敏原的同时，还应避免紧张情绪。
【饮食调养】饮食宜均衡、粗细粮食搭配适当、荤素配伍合理，宜多食益气固表的食物，尽量少食辛辣、腥发食物，不食含致敏物质的食品，如蚕豆、白扁豆、羊肉、鹅肉、鲤鱼、虾、蟹、茄子、辣椒、浓茶、咖啡等。 
【起居调摄】起居要有规律，保持充足的睡眠时间。居室宜通风良好。生活环境中接触的物品如枕头、棉被、床垫、地毯、窗帘、衣橱易附有尘螨，可引起过敏，应经常清洗、日晒。外出也要避免处在花粉及粉刷油漆的空气中，以免刺激而诱发过敏病症。
【运动保健】宜进行慢跑、散步等户外活动，也可选择下棋、瑜珈等室内活动。不宜选择大运动量的活动，避免春天或季节交替时长时间在野外锻炼。运动时注意避风寒，如出现哮喘、憋闷的现象应及时停止运动。
【穴位保健】（1）选穴：神阙、曲池。（2）定位：神阙位于腹中部，脐中央。曲池位于肘横纹外侧端，屈肘，当尺泽与在肘横纹外侧端与肱骨外上髁连线中点。（3）操作：神阙采用温和灸（见气虚质）。曲池采用指揉法（见平和质）。");
            }

            //平和质辨识
            if (!string.IsNullOrEmpty(dr[tb_老年人中医药特征管理.平和质辨识].ToString()))
            {
                sb_指导.AppendLine("平和质：" + bll_常用字典.DicMap("zytzbs", dr[tb_老年人中医药特征管理.平和质辨识].ToString()));
                sb_指导.AppendLine(@"【总体特征】 阴阳气血调和，以体态适中、面色润泽、精力充沛等为主要特征。
【情志调摄】宜保持平和的心态。可根据个人爱好，选择弹琴、下棋、书法、绘画、听音乐、阅读、旅游、种植花草等放松心情。
【饮食调养】饮食宜粗细粮食合理搭配，多吃五谷杂粮、蔬菜瓜果，少食过于油腻及辛辣食品；不要过饥过饱，也不要进食过冷过烫或不干净食物；注意戒烟限酒。
【起居调摄】起居宜规律，睡眠要充足，劳逸相结合，穿戴求自然。
【运动保健】形成良好的运动健身习惯。可根据个人爱好和耐受程度，选择运动健身项目。
【穴位保健】（1）选穴：涌泉、足三里。（2）定位：涌泉位于足底部，卷足时足前部凹陷处，约当足底2、3趾趾缝纹头端与足跟连线的前三分之一与后三分之二交点上。足三里位于小腿前外侧，当犊鼻下3寸，距胫骨前缘一横指处。（3）操作：用大拇指或中指指腹按压穴位，做轻柔缓和的环旋活动，以穴位感到酸胀为度，按揉2～3分钟。每天操作1～2次。");
            }
        }
    }
}
