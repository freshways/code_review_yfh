﻿namespace AtomEHR.公共卫生.Module.健康档案.体检回执单.回执A4
{
    partial class 回执封面1A4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(回执封面1A4));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.txt前言称谓 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt单位 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.txt体检日期 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt户籍地址 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt身份证号 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt性别 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.pic照片 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.txt姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt编号 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt前言称谓,
            this.xrLabel3,
            this.xrLabel1,
            this.xrLabel7,
            this.xrLabel4,
            this.xrLabel11,
            this.xrLabel5,
            this.xrLabel9,
            this.xrLabel13,
            this.xrLabel15,
            this.xrLabel16,
            this.txt单位,
            this.xrPanel1,
            this.xrLabel2});
            this.Detail.Font = new System.Drawing.Font("宋体", 9.75F);
            this.Detail.HeightF = 1169F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // txt前言称谓
            // 
            this.txt前言称谓.Font = new System.Drawing.Font("新宋体", 12F);
            this.txt前言称谓.LocationFloat = new DevExpress.Utils.PointFloat(33.95824F, 480.1249F);
            this.txt前言称谓.Name = "txt前言称谓";
            this.txt前言称谓.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt前言称谓.SizeF = new System.Drawing.SizeF(172.9167F, 23F);
            this.txt前言称谓.StylePriority.UseFont = false;
            this.txt前言称谓.StylePriority.UseTextAlignment = false;
            this.txt前言称谓.Text = "尊敬的用户：";
            this.txt前言称谓.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("宋体", 12F);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(68.95822F, 535.125F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(625F, 23F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "您好，欢迎您参加本年度国家基本公共卫生服务老年人健康体检项目。";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("宋体", 12F);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(68.95822F, 760.1249F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(625F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "我们希望与您建立长期的联系，并为你建立健康档案，以便对您的健康状况进行长期";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("宋体", 12F);
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(33.95824F, 715.1249F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(660F, 23F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "联系。";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("宋体", 12F);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(68.95822F, 670.1249F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(625F, 23F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "希望您仔细阅读并妥善保管这份体检报告，给予充分的重视。如有疑问，请与我中心";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("宋体", 12F);
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(33.95824F, 625.125F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(659.9999F, 23F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "检测。定期的健康体检可以帮助您及早发现健康状态下的危机，为您的健康保驾护航。";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("宋体", 12F);
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(68.95822F, 580.1249F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(625F, 23F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "健康体检是在自我感觉健康的情况下，通过医学手段对身体各脏器的功能状态进行的";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Font = new System.Drawing.Font("宋体", 12F);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(33.95824F, 805.1249F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(659.9999F, 23F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "追踪，为您的健康提供长期的帮助和服务。";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Font = new System.Drawing.Font("宋体", 12F);
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(68.95822F, 850.1249F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(625F, 23F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "由于体检项目和检查手段所限，一次体检不能全部检出身体潜在的隐患，一旦身体出";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Font = new System.Drawing.Font("宋体", 12F);
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(33.95824F, 900.1249F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(659.9999F, 23F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "现不适，请您及时到上级医院就诊。";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(451.4584F, 974.1249F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(216.4581F, 23F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "定期体检，尊享健康！";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt单位
            // 
            this.txt单位.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold);
            this.txt单位.LocationFloat = new DevExpress.Utils.PointFloat(451.4584F, 1036.708F);
            this.txt单位.Name = "txt单位";
            this.txt单位.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt单位.SizeF = new System.Drawing.SizeF(216.458F, 23F);
            this.txt单位.StylePriority.UseFont = false;
            this.txt单位.StylePriority.UseTextAlignment = false;
            this.txt单位.Text = "xx卫生院";
            this.txt单位.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrPanel1
            // 
            this.xrPanel1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt体检日期,
            this.xrLabel17,
            this.txt户籍地址,
            this.xrLabel14,
            this.txt身份证号,
            this.xrLabel12,
            this.txt性别,
            this.xrLabel10,
            this.pic照片,
            this.txt姓名,
            this.xrLabel8,
            this.txt编号,
            this.xrLabel6});
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(21.45831F, 160F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.SizeF = new System.Drawing.SizeF(699.9999F, 304.7917F);
            this.xrPanel1.StylePriority.UseBorders = false;
            // 
            // txt体检日期
            // 
            this.txt体检日期.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt体检日期.Font = new System.Drawing.Font("宋体", 14F);
            this.txt体检日期.LocationFloat = new DevExpress.Utils.PointFloat(234.1667F, 209.0104F);
            this.txt体检日期.Name = "txt体检日期";
            this.txt体检日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt体检日期.SizeF = new System.Drawing.SizeF(182.2916F, 23F);
            this.txt体检日期.StylePriority.UseBorders = false;
            this.txt体检日期.StylePriority.UseFont = false;
            this.txt体检日期.StylePriority.UseTextAlignment = false;
            this.txt体检日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel17.Font = new System.Drawing.Font("宋体", 14F);
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(121.1667F, 209.0104F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(113F, 23F);
            this.xrLabel17.StylePriority.UseBorders = false;
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "体检日期：";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt户籍地址
            // 
            this.txt户籍地址.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt户籍地址.Font = new System.Drawing.Font("宋体", 12F);
            this.txt户籍地址.LocationFloat = new DevExpress.Utils.PointFloat(234.1667F, 250.2083F);
            this.txt户籍地址.Name = "txt户籍地址";
            this.txt户籍地址.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt户籍地址.SizeF = new System.Drawing.SizeF(397.4165F, 23F);
            this.txt户籍地址.StylePriority.UseBorders = false;
            this.txt户籍地址.StylePriority.UseFont = false;
            this.txt户籍地址.StylePriority.UseTextAlignment = false;
            this.txt户籍地址.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel14.Font = new System.Drawing.Font("宋体", 14F);
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(121.1667F, 250.2083F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(113F, 23F);
            this.xrLabel14.StylePriority.UseBorders = false;
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "地    址：";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt身份证号.Font = new System.Drawing.Font("宋体", 14F);
            this.txt身份证号.LocationFloat = new DevExpress.Utils.PointFloat(234.1667F, 167.8125F);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt身份证号.SizeF = new System.Drawing.SizeF(182.2916F, 23F);
            this.txt身份证号.StylePriority.UseBorders = false;
            this.txt身份证号.StylePriority.UseFont = false;
            this.txt身份证号.StylePriority.UseTextAlignment = false;
            this.txt身份证号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel12.Font = new System.Drawing.Font("宋体", 14F);
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(121.1667F, 167.8125F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(113F, 23F);
            this.xrLabel12.StylePriority.UseBorders = false;
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "身份证号：";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt性别
            // 
            this.txt性别.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt性别.Font = new System.Drawing.Font("宋体", 14F);
            this.txt性别.LocationFloat = new DevExpress.Utils.PointFloat(234.1667F, 126.6145F);
            this.txt性别.Name = "txt性别";
            this.txt性别.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt性别.SizeF = new System.Drawing.SizeF(182.2916F, 23F);
            this.txt性别.StylePriority.UseBorders = false;
            this.txt性别.StylePriority.UseFont = false;
            this.txt性别.StylePriority.UseTextAlignment = false;
            this.txt性别.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel10.Font = new System.Drawing.Font("宋体", 14F);
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(121.1667F, 126.6146F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(113F, 23F);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "性    别：";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pic照片
            // 
            this.pic照片.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.pic照片.Image = ((System.Drawing.Image)(resources.GetObject("pic照片.Image")));
            this.pic照片.LocationFloat = new DevExpress.Utils.PointFloat(481.1667F, 85.41666F);
            this.pic照片.Name = "pic照片";
            this.pic照片.SizeF = new System.Drawing.SizeF(146.59F, 146.5938F);
            this.pic照片.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            this.pic照片.StylePriority.UseBorders = false;
            // 
            // txt姓名
            // 
            this.txt姓名.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt姓名.Font = new System.Drawing.Font("宋体", 14F);
            this.txt姓名.LocationFloat = new DevExpress.Utils.PointFloat(234.1667F, 85.41666F);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt姓名.SizeF = new System.Drawing.SizeF(182.2916F, 23F);
            this.txt姓名.StylePriority.UseBorders = false;
            this.txt姓名.StylePriority.UseFont = false;
            this.txt姓名.StylePriority.UseTextAlignment = false;
            this.txt姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel8.Font = new System.Drawing.Font("宋体", 14F);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(121.1667F, 85.41666F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(113F, 23F);
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "姓    名：";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt编号
            // 
            this.txt编号.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt编号.Font = new System.Drawing.Font("宋体", 14F);
            this.txt编号.LocationFloat = new DevExpress.Utils.PointFloat(430.0001F, 9.999954F);
            this.txt编号.Name = "txt编号";
            this.txt编号.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt编号.SizeF = new System.Drawing.SizeF(235.4998F, 23F);
            this.txt编号.StylePriority.UseBorders = false;
            this.txt编号.StylePriority.UseFont = false;
            this.txt编号.StylePriority.UseTextAlignment = false;
            this.txt编号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel6.Font = new System.Drawing.Font("宋体", 14F);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(370F, 10F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(60F, 23F);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "编号:";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("宋体", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 60.00001F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(726.9999F, 35F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "健康体检反馈单";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // 回执封面1A4
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.DesignerOptions.ShowExportWarnings = false;
            this.DesignerOptions.ShowPrintingWarnings = false;
            this.Margins = new System.Drawing.Printing.Margins(40, 40, 0, 0);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRPanel xrPanel1;
        private DevExpress.XtraReports.UI.XRLabel txt户籍地址;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel txt身份证号;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel txt性别;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRPictureBox pic照片;
        private DevExpress.XtraReports.UI.XRLabel txt姓名;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel txt编号;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel txt前言称谓;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel txt单位;
        private DevExpress.XtraReports.UI.XRLabel txt体检日期;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
    }
}
