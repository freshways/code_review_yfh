﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraNavBar;
using AtomEHR.Common;

namespace AtomEHR.公共卫生.Module
{
    public partial class UserControlBaseNavBar : UserControlBase
    {
        public UserControlBaseNavBar()
        {
            InitializeComponent();
        }

        #region NavBarControl
        /// <summary>
        /// 创建导航组件的组按钮(NavBarControl.NavBarGroup)
        /// 2015-10-09 16:10:32 注释原CreateNavBarButton，添加到本页面
        /// </summary>
        /// <param name="navBar">NavBarControl对象</param>
        /// <param name="moduleMenu">要处理的数据</param>
        /// <param name="moduleDisplayName">年</param>
        /// <param name="s划分时间字段"></param>
        public virtual void CreateNavBarButton_new(DataTable moduleMenu, string s划分时间字段)
        {
            navBarControl1.Groups.Clear();
            string odlyaer = "";
            for (int i = 0; i < moduleMenu.Rows.Count; i++)
            {
                string yaer = Convert.ToDateTime(moduleMenu.Rows[i][s划分时间字段]).Year.ToString();
                if (yaer != odlyaer)
                {
                    odlyaer = yaer;
                    //绑定事件,当组按钮的Index改变时触发
                    navBarControl1.MouseClick += new MouseEventHandler(this.OnNavBar_MouseClick);

                    NavBarGroup group = navBarControl1.Groups.Add();//增加一个Button组.
                    group.Caption = yaer; //模块的显示名称
                    group.LargeImageIndex = 0;

                    for (int j = 0; j < moduleMenu.Rows.Count; j++)
                    {
                        string yaers = moduleMenu.Rows[j][s划分时间字段].ToString();
                        if (Convert.ToDateTime(yaers).Year.ToString() == yaer)
                        {
                            NavBarItem newItem = new NavBarItem();
                            newItem.Caption = Convert.ToDateTime(yaers).ToString("yyyy-MM-dd");
                            newItem.Tag = moduleMenu.Rows[j];
                            navBarControl1.Items.Add(newItem);
                            group.ItemLinks.Add(newItem);
                        }
                    }
                }
            }
            for (int i = 0; i < this.navBarControl1.Groups.Count; i++)
            {
                this.navBarControl1.Groups[i].Expanded = true;
            }
        }

        string Yaer = "";
        object _Datarow;
        /// <summary>
        /// 点击导航分组按钮时触发该事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void OnNavBar_MouseClick(object sender, MouseEventArgs e)
        {
            NavBarControl nav = (sender as NavBarControl);//取到NavBarControl对象引用
            NavBarHitInfo hit = nav.CalcHitInfo(e.Location);//计算点击区域的对象

            if (hit.InGroup && hit.InGroupCaption)//点击导航分组按钮
            {
                try
                {
                    //changed by wjz 20151028 ▽
                    //nav.ActiveGroup = hit.Group; //立即设置为激活的组
                    //hit.Group.Expanded = true;
                    if (nav.ActiveGroup == hit.Group)
                    {

                        hit.Group.Expanded = !(hit.Group.Expanded);
                    }
                    else
                    {
                        nav.ActiveGroup = hit.Group; //立即设置为激活的组
                        hit.Group.Expanded = true;
                    }
                    //changed by wjz 20151028 △
                }
                catch (Exception ex)
                { Msg.ShowException(ex); }
            }
            else if (hit.Link != null && hit.Link.Item.Tag != null)
            {
                //add by wjz 所有项目的字体颜色设置为黑色 20151028 ▽
                //将字体颜色重新设置为黑色
                for (int index = 0; index < nav.Items.Count; index++)
                {
                    nav.Items[index].Appearance.ForeColor = Color.Black;
                    //nav.Items[index].Appearance.Font.Underline 
                }
                //add by wjz 所有项目的字体颜色设置为黑色 20151028 △
                try
                {
                    //add by wjz 字体颜色设置为红色 20151028 ▽
                    hit.Link.Item.Appearance.ForeColor = Color.Red;
                    //add by wjz 字体颜色设置为红色 20151028 △
                    if (!hit.Link.Item.Caption.Equals(Yaer) ||
                        !hit.Link.Item.Tag.Equals(_Datarow))
                    {
                        Yaer = hit.Link.Item.Caption;
                        _Datarow = hit.Link.Item.Tag;
                        DoBindingSummaryEditor(_Datarow);
                    }
                }
                catch (Exception ex)
                { Msg.ShowException(ex); }
            }
        }

        /// <summary>
        /// 记录的id
        /// </summary>
        /// <param name="id"></param>
        protected void SetItemColorToRed(string id)
        {
            for(int index =0; index < this.navBarControl1.Items.Count; index++)
            {
                try
                {
                    DataRow dr = this.navBarControl1.Items[index].Tag as DataRow;
                    string strID = dr["ID"].ToString();
                    if(id == strID)
                    {
                        this.navBarControl1.Items[index].Appearance.ForeColor = Color.Red;
                        //navBarControl1.Groups[0].Expanded = true; //获取group 并展开-还未实现
                        return;
                    }
                }
                catch//异常时没有进行任何处理
                {}
            }
        }
        #endregion
    }
}
