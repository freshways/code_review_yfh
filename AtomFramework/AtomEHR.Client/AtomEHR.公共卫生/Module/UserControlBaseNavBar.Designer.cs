﻿namespace AtomEHR.公共卫生.Module
{
    partial class UserControlBaseNavBar
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControlNavbar = new DevExpress.XtraEditors.PanelControl();
            this.navBarControl1 = new DevExpress.XtraNavBar.NavBarControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).BeginInit();
            this.panelControlNavbar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControlNavbar
            // 
            this.panelControlNavbar.Controls.Add(this.navBarControl1);
            this.panelControlNavbar.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControlNavbar.Location = new System.Drawing.Point(0, 0);
            this.panelControlNavbar.Name = "panelControlNavbar";
            this.panelControlNavbar.Size = new System.Drawing.Size(136, 266);
            this.panelControlNavbar.TabIndex = 2;
            // 
            // navBarControl1
            // 
            this.navBarControl1.ActiveGroup = null;
            this.navBarControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navBarControl1.Location = new System.Drawing.Point(2, 2);
            this.navBarControl1.Name = "navBarControl1";
            this.navBarControl1.OptionsNavPane.ExpandedWidth = 132;
            this.navBarControl1.Size = new System.Drawing.Size(132, 262);
            this.navBarControl1.TabIndex = 5;
            this.navBarControl1.Text = "navBarControl1";
            // 
            // UserControlBaseNavBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControlNavbar);
            this.Name = "UserControlBaseNavBar";
            this.Size = new System.Drawing.Size(432, 266);
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).EndInit();
            this.panelControlNavbar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.PanelControl panelControlNavbar;
        public DevExpress.XtraNavBar.NavBarControl navBarControl1;

    }
}
