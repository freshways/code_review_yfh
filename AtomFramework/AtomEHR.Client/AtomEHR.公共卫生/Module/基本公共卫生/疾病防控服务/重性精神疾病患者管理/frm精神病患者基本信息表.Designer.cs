﻿using AtomEHR.Library.UserControls;
namespace AtomEHR.公共卫生.Module.基本公共卫生.重性精神疾病患者管理
{
    partial class frm精神病患者基本信息表 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm精神病患者基本信息表));
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txt详细地址 = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEdit村 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit镇 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.treeListLookUpEdit机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit2TreeList = new DevExpress.XtraTreeList.TreeList();
            this.chk含下属机构 = new DevExpress.XtraEditors.CheckEdit();
            this.cbo性别 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt档案编号 = new DevExpress.XtraEditors.TextEdit();
            this.uc年龄区间 = new AtomEHR.Library.UserControls.UCTxtToTxt();
            this.uC录入时间 = new AtomEHR.Library.UserControls.UCDtpToDtp();
            this.uC出生日期 = new AtomEHR.Library.UserControls.UCDtpToDtp();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.cbo档案状态 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo录入人 = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.btn修改 = new DevExpress.XtraEditors.SimpleButton();
            this.btn重置 = new DevExpress.XtraEditors.SimpleButton();
            this.btn查询 = new DevExpress.XtraEditors.SimpleButton();
            this.gc人次 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv人次 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pagerControl1 = new TActionProject.PagerControl();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt详细地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit2TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk含下属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo录入人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc人次)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv人次)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gc人次);
            this.tpSummary.Controls.Add(this.pagerControl1);
            this.tpSummary.Controls.Add(this.panelControl1);
            this.tpSummary.Controls.Add(this.layoutControl1);
            this.tpSummary.Size = new System.Drawing.Size(868, 430);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Size = new System.Drawing.Size(874, 436);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Size = new System.Drawing.Size(874, 436);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(932, 589);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(874, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(696, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(499, 2);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 1;
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(880, 176);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // panelControl2
            // 
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(874, 462);
            this.panelControl2.TabIndex = 3;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txt详细地址);
            this.layoutControl1.Controls.Add(this.comboBoxEdit村);
            this.layoutControl1.Controls.Add(this.comboBoxEdit镇);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel1);
            this.layoutControl1.Controls.Add(this.cbo性别);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt档案编号);
            this.layoutControl1.Controls.Add(this.uc年龄区间);
            this.layoutControl1.Controls.Add(this.uC录入时间);
            this.layoutControl1.Controls.Add(this.uC出生日期);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.cbo档案状态);
            this.layoutControl1.Controls.Add(this.cbo录入人);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.Root = this.layoutControlGroup2;
            this.layoutControl1.Size = new System.Drawing.Size(868, 126);
            this.layoutControl1.TabIndex = 2;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txt详细地址
            // 
            this.txt详细地址.Location = new System.Drawing.Point(411, 97);
            this.txt详细地址.Name = "txt详细地址";
            this.txt详细地址.Size = new System.Drawing.Size(231, 20);
            this.txt详细地址.StyleController = this.layoutControl1;
            this.txt详细地址.TabIndex = 24;
            // 
            // comboBoxEdit村
            // 
            this.comboBoxEdit村.Location = new System.Drawing.Point(255, 97);
            this.comboBoxEdit村.Name = "comboBoxEdit村";
            this.comboBoxEdit村.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit村.Size = new System.Drawing.Size(152, 20);
            this.comboBoxEdit村.StyleController = this.layoutControl1;
            this.comboBoxEdit村.TabIndex = 23;
            // 
            // comboBoxEdit镇
            // 
            this.comboBoxEdit镇.Location = new System.Drawing.Point(110, 97);
            this.comboBoxEdit镇.Name = "comboBoxEdit镇";
            this.comboBoxEdit镇.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit镇.Size = new System.Drawing.Size(141, 20);
            this.comboBoxEdit镇.StyleController = this.layoutControl1;
            this.comboBoxEdit镇.TabIndex = 22;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.treeListLookUpEdit机构);
            this.flowLayoutPanel1.Controls.Add(this.chk含下属机构);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(110, 25);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(291, 20);
            this.flowLayoutPanel1.TabIndex = 21;
            // 
            // treeListLookUpEdit机构
            // 
            this.treeListLookUpEdit机构.EditValue = "含下属机构";
            this.treeListLookUpEdit机构.Location = new System.Drawing.Point(0, 0);
            this.treeListLookUpEdit机构.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.treeListLookUpEdit机构.Name = "treeListLookUpEdit机构";
            this.treeListLookUpEdit机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEdit机构.Properties.TreeList = this.treeListLookUpEdit2TreeList;
            this.treeListLookUpEdit机构.Size = new System.Drawing.Size(180, 20);
            this.treeListLookUpEdit机构.TabIndex = 0;
            this.treeListLookUpEdit机构.EditValueChanged += new System.EventHandler(this.treeListLookUpEdit机构_EditValueChanged);
            // 
            // treeListLookUpEdit2TreeList
            // 
            this.treeListLookUpEdit2TreeList.Location = new System.Drawing.Point(0, 95);
            this.treeListLookUpEdit2TreeList.Name = "treeListLookUpEdit2TreeList";
            this.treeListLookUpEdit2TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit2TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit2TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit2TreeList.TabIndex = 0;
            // 
            // chk含下属机构
            // 
            this.chk含下属机构.EditValue = true;
            this.chk含下属机构.Location = new System.Drawing.Point(186, 0);
            this.chk含下属机构.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk含下属机构.Name = "chk含下属机构";
            this.chk含下属机构.Properties.Caption = "含下属机构";
            this.chk含下属机构.Size = new System.Drawing.Size(101, 19);
            this.chk含下属机构.TabIndex = 1;
            // 
            // cbo性别
            // 
            this.cbo性别.EditValue = "";
            this.cbo性别.Location = new System.Drawing.Point(720, 25);
            this.cbo性别.Name = "cbo性别";
            this.cbo性别.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo性别.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo性别.Size = new System.Drawing.Size(143, 20);
            this.cbo性别.StyleController = this.layoutControl1;
            this.cbo性别.TabIndex = 18;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(720, 73);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Size = new System.Drawing.Size(143, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 14;
            // 
            // txt档案编号
            // 
            this.txt档案编号.Location = new System.Drawing.Point(480, 73);
            this.txt档案编号.Name = "txt档案编号";
            this.txt档案编号.Size = new System.Drawing.Size(161, 20);
            this.txt档案编号.StyleController = this.layoutControl1;
            this.txt档案编号.TabIndex = 11;
            // 
            // uc年龄区间
            // 
            this.uc年龄区间.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.uc年龄区间.Location = new System.Drawing.Point(480, 49);
            this.uc年龄区间.Name = "uc年龄区间";
            this.uc年龄区间.Size = new System.Drawing.Size(161, 20);
            this.uc年龄区间.TabIndex = 9;
            this.uc年龄区间.Txt1Size = new System.Drawing.Size(314, 20);
            this.uc年龄区间.Txt2Size = new System.Drawing.Size(314, 20);
            // 
            // uC录入时间
            // 
            this.uC录入时间.Dte1Size = new System.Drawing.Size(117, 20);
            this.uC录入时间.Dte2Size = new System.Drawing.Size(117, 20);
            this.uC录入时间.Location = new System.Drawing.Point(110, 73);
            this.uC录入时间.Margin = new System.Windows.Forms.Padding(0);
            this.uC录入时间.Name = "uC录入时间";
            this.uC录入时间.Size = new System.Drawing.Size(291, 20);
            this.uC录入时间.TabIndex = 8;
            // 
            // uC出生日期
            // 
            this.uC出生日期.Dte1Size = new System.Drawing.Size(117, 20);
            this.uC出生日期.Dte2Size = new System.Drawing.Size(117, 20);
            this.uC出生日期.Location = new System.Drawing.Point(110, 49);
            this.uC出生日期.Margin = new System.Windows.Forms.Padding(0);
            this.uC出生日期.Name = "uC出生日期";
            this.uC出生日期.Size = new System.Drawing.Size(291, 20);
            this.uC出生日期.TabIndex = 7;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(480, 25);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(161, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 5;
            // 
            // cbo档案状态
            // 
            this.cbo档案状态.Location = new System.Drawing.Point(720, 49);
            this.cbo档案状态.Name = "cbo档案状态";
            this.cbo档案状态.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo档案状态.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo档案状态.Size = new System.Drawing.Size(143, 20);
            this.cbo档案状态.StyleController = this.layoutControl1;
            this.cbo档案状态.TabIndex = 10;
            // 
            // cbo录入人
            // 
            this.cbo录入人.Location = new System.Drawing.Point(721, 97);
            this.cbo录入人.Name = "cbo录入人";
            this.cbo录入人.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo录入人.Properties.NullText = "";
            this.cbo录入人.Properties.PopupSizeable = false;
            this.cbo录入人.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbo录入人.Size = new System.Drawing.Size(142, 20);
            this.cbo录入人.StyleController = this.layoutControl1;
            this.cbo录入人.TabIndex = 13;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup1";
            this.layoutControlGroup2.OptionsItemText.TextToControlDistance = 1;
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(868, 126);
            this.layoutControlGroup2.Text = "layoutControlGroup1";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "查询条件";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem5,
            this.layoutControlItem8,
            this.layoutControlItem1,
            this.layoutControlItem9,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem12,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem11,
            this.layoutControlItem10});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup2";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(868, 126);
            this.layoutControlGroup3.Text = "查询条件";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.txt姓名;
            this.layoutControlItem2.CustomizationFormText = "姓名：";
            this.layoutControlItem2.Location = new System.Drawing.Point(400, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(240, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(240, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(240, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "姓名：";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.uC录入时间;
            this.layoutControlItem5.CustomizationFormText = "录入时间：";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(400, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(400, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(400, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "录入时间：";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.txt档案编号;
            this.layoutControlItem8.CustomizationFormText = "档案编号：";
            this.layoutControlItem8.Location = new System.Drawing.Point(400, 48);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(240, 24);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(240, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(240, 24);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "档案编号：";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.flowLayoutPanel1;
            this.layoutControlItem1.CustomizationFormText = "所属机构：";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(400, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(400, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(400, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "所属机构：";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.comboBoxEdit镇;
            this.layoutControlItem9.CustomizationFormText = "居住地址：";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(250, 28);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "居住地址：";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.comboBoxEdit村;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(250, 72);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(156, 24);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(156, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(156, 28);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "layoutControlItem13";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.txt详细地址;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(406, 72);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(235, 24);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(235, 24);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(235, 28);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "layoutControlItem14";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextToControlDistance = 0;
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.cbo性别;
            this.layoutControlItem12.CustomizationFormText = "人数/人次：";
            this.layoutControlItem12.Location = new System.Drawing.Point(640, 0);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(119, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(222, 24);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "性别：";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.uC出生日期;
            this.layoutControlItem4.CustomizationFormText = "出生日期：";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(400, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(400, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(400, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "出生日期：";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.uc年龄区间;
            this.layoutControlItem6.CustomizationFormText = "年龄区间：";
            this.layoutControlItem6.Location = new System.Drawing.Point(400, 24);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(240, 24);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(240, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(240, 24);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "年龄区间：";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.cbo档案状态;
            this.layoutControlItem7.CustomizationFormText = "档案状态：";
            this.layoutControlItem7.Location = new System.Drawing.Point(640, 24);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(119, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(222, 24);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "档案状态：";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.txt身份证号;
            this.layoutControlItem11.CustomizationFormText = "身份证号：";
            this.layoutControlItem11.Location = new System.Drawing.Point(640, 48);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(119, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(222, 24);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "身份证号：";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.cbo录入人;
            this.layoutControlItem10.CustomizationFormText = "录入人：";
            this.layoutControlItem10.Location = new System.Drawing.Point(641, 72);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(250, 25);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(221, 28);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "录入人：";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btn删除);
            this.panelControl1.Controls.Add(this.btn修改);
            this.panelControl1.Controls.Add(this.btn重置);
            this.panelControl1.Controls.Add(this.btn查询);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 126);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(868, 39);
            this.panelControl1.TabIndex = 3;
            // 
            // btn删除
            // 
            this.btn删除.Image = ((System.Drawing.Image)(resources.GetObject("btn删除.Image")));
            this.btn删除.Location = new System.Drawing.Point(376, 2);
            this.btn删除.Name = "btn删除";
            this.btn删除.Size = new System.Drawing.Size(75, 33);
            this.btn删除.TabIndex = 3;
            this.btn删除.Text = "删除";
            this.btn删除.Click += new System.EventHandler(this.btn删除_Click);
            // 
            // btn修改
            // 
            this.btn修改.Image = ((System.Drawing.Image)(resources.GetObject("btn修改.Image")));
            this.btn修改.Location = new System.Drawing.Point(261, 2);
            this.btn修改.Name = "btn修改";
            this.btn修改.Size = new System.Drawing.Size(75, 33);
            this.btn修改.TabIndex = 2;
            this.btn修改.Text = "修改";
            // 
            // btn重置
            // 
            this.btn重置.Image = ((System.Drawing.Image)(resources.GetObject("btn重置.Image")));
            this.btn重置.Location = new System.Drawing.Point(146, 2);
            this.btn重置.Name = "btn重置";
            this.btn重置.Size = new System.Drawing.Size(75, 33);
            this.btn重置.TabIndex = 1;
            this.btn重置.Text = "重置";
            // 
            // btn查询
            // 
            this.btn查询.Image = ((System.Drawing.Image)(resources.GetObject("btn查询.Image")));
            this.btn查询.Location = new System.Drawing.Point(31, 2);
            this.btn查询.Name = "btn查询";
            this.btn查询.Size = new System.Drawing.Size(75, 33);
            this.btn查询.TabIndex = 0;
            this.btn查询.Text = "查询";
            this.btn查询.Click += new System.EventHandler(this.btn查询_Click);
            // 
            // gc人次
            // 
            this.gc人次.AllowBandedGridColumnSort = false;
            this.gc人次.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc人次.IsBestFitColumns = true;
            this.gc人次.Location = new System.Drawing.Point(0, 165);
            this.gc人次.MainView = this.gv人次;
            this.gc人次.Name = "gc人次";
            this.gc人次.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.gc人次.ShowContextMenu = false;
            this.gc人次.Size = new System.Drawing.Size(868, 200);
            this.gc人次.StrWhere = "";
            this.gc人次.TabIndex = 5;
            this.gc人次.UseCheckBox = false;
            this.gc人次.View = "";
            this.gc人次.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv人次});
            // 
            // gv人次
            // 
            this.gv人次.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gv人次.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gv人次.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gv人次.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.DetailTip.Options.UseFont = true;
            this.gv人次.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.Empty.Options.UseFont = true;
            this.gv人次.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.EvenRow.Options.UseFont = true;
            this.gv人次.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gv人次.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.FilterPanel.Options.UseFont = true;
            this.gv人次.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.FixedLine.Options.UseFont = true;
            this.gv人次.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.FocusedCell.Options.UseFont = true;
            this.gv人次.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.FocusedRow.Options.UseFont = true;
            this.gv人次.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gv人次.Appearance.FooterPanel.Options.UseFont = true;
            this.gv人次.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.GroupButton.Options.UseFont = true;
            this.gv人次.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.GroupFooter.Options.UseFont = true;
            this.gv人次.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.GroupPanel.Options.UseFont = true;
            this.gv人次.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.GroupRow.Options.UseFont = true;
            this.gv人次.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.HeaderPanel.Options.UseFont = true;
            this.gv人次.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gv人次.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.HorzLine.Options.UseFont = true;
            this.gv人次.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.OddRow.Options.UseFont = true;
            this.gv人次.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.Preview.Options.UseFont = true;
            this.gv人次.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.Row.Options.UseFont = true;
            this.gv人次.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.RowSeparator.Options.UseFont = true;
            this.gv人次.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.SelectedRow.Options.UseFont = true;
            this.gv人次.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.TopNewRow.Options.UseFont = true;
            this.gv人次.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.VertLine.Options.UseFont = true;
            this.gv人次.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.ViewCaption.Options.UseFont = true;
            this.gv人次.ColumnPanelRowHeight = 30;
            this.gv人次.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn6,
            this.gridColumn4,
            this.gridColumn7,
            this.gridColumn3,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11});
            this.gv人次.GridControl = this.gc人次;
            this.gv人次.GroupPanelText = "DragColumn";
            this.gv人次.Name = "gv人次";
            this.gv人次.OptionsView.ColumnAutoWidth = false;
            this.gv人次.OptionsView.EnableAppearanceEvenRow = true;
            this.gv人次.OptionsView.EnableAppearanceOddRow = true;
            this.gv人次.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn1.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "档案号";
            this.gridColumn1.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.gridColumn1.FieldName = "个人档案编号";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit1_Click);
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn2.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "姓名";
            this.gridColumn2.FieldName = "姓名";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn6.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn6.AppearanceHeader.Options.UseFont = true;
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "性别";
            this.gridColumn6.FieldName = "性别";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn4.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "出生日期";
            this.gridColumn4.FieldName = "出生日期";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 2;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn7.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn7.AppearanceHeader.Options.UseFont = true;
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.Caption = "居住地址";
            this.gridColumn7.FieldName = "居住地址";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 3;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn3.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "身份证号";
            this.gridColumn3.FieldName = "身份证号";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 4;
            this.gridColumn3.Width = 86;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn8.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn8.AppearanceHeader.Options.UseFont = true;
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.Caption = "联系电话";
            this.gridColumn8.FieldName = "联系电话";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 5;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn9.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn9.AppearanceHeader.Options.UseFont = true;
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.Caption = "当前所属机构";
            this.gridColumn9.FieldName = "所属机构";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 6;
            this.gridColumn9.Width = 113;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn10.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn10.AppearanceHeader.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.Caption = "录入人";
            this.gridColumn10.FieldName = "创建人";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 7;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn11.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn11.AppearanceHeader.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "录入时间";
            this.gridColumn11.FieldName = "创建时间";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 8;
            // 
            // pagerControl1
            // 
            this.pagerControl1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pagerControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pagerControl1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pagerControl1.JumpText = "跳转";
            this.pagerControl1.Location = new System.Drawing.Point(0, 365);
            this.pagerControl1.MaximumSize = new System.Drawing.Size(0, 65);
            this.pagerControl1.Name = "pagerControl1";
            this.pagerControl1.PageIndex = 1;
            this.pagerControl1.PageSize = 100;
            this.pagerControl1.paramters = null;
            this.pagerControl1.RecordCount = 0;
            this.pagerControl1.Size = new System.Drawing.Size(868, 65);
            this.pagerControl1.SqlQuery = null;
            this.pagerControl1.TabIndex = 6;
            this.pagerControl1.OnPageChanged += new System.EventHandler(this.pagerControl1_OnPageChanged);
            // 
            // frm精神病患者基本信息表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 462);
            this.Controls.Add(this.panelControl2);
            this.Name = "frm精神病患者基本信息表";
            this.Text = "精神病患者基本信息表";
            this.Load += new System.EventHandler(this.frm精神病患者基本信息表_Load);
            this.Controls.SetChildIndex(this.panelControl2, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.Controls.SetChildIndex(this.gcNavigator, 0);
            this.Controls.SetChildIndex(this.pnlSummary, 0);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt详细地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit2TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk含下属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo录入人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc人次)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv人次)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btn删除;
        private DevExpress.XtraEditors.SimpleButton btn修改;
        private DevExpress.XtraEditors.SimpleButton btn重置;
        private DevExpress.XtraEditors.SimpleButton btn查询;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txt详细地址;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit村;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit镇;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEdit机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit2TreeList;
        private DevExpress.XtraEditors.CheckEdit chk含下属机构;
        private DevExpress.XtraEditors.ComboBoxEdit cbo性别;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt档案编号;
        private UCTxtToTxt uc年龄区间;
        private UCDtpToDtp uC录入时间;
        private UCDtpToDtp uC出生日期;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.ComboBoxEdit cbo档案状态;
        private DevExpress.XtraEditors.LookUpEdit cbo录入人;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DataGridControl gc人次;
        private DevExpress.XtraGrid.Views.Grid.GridView gv人次;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private TActionProject.PagerControl pagerControl1;
    }
}