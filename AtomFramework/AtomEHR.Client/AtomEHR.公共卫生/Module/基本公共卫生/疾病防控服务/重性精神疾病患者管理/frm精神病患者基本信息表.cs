﻿using AtomEHR.Business;
using AtomEHR.Business.Security;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.Models;
using AtomEHR.公共卫生.Module.个人健康;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.基本公共卫生.重性精神疾病患者管理
{
    public partial class frm精神病患者基本信息表 : frmBaseBusinessForm
    {
        string _strWhere;
        public frm精神病患者基本信息表()
        {
            InitializeComponent();
        }
        private void frm精神病患者基本信息表_Load(object sender, EventArgs e)
        {
            InitializeForm();
            InitView();
        }
        /// <summary>
        /// 初始化页面
        /// </summary>
        private void InitView()
        {
            this.pagerControl1.Height = 35;
            this.uc年龄区间.Txt1.Width = 60;
            this.uc年龄区间.Txt2.Width = 60;
            this.uC出生日期.Dte1.Width = 90;
            this.uC出生日期.Dte2.Width = 90;
            this.uC录入时间.Dte1.Width = 90;
            this.uC录入时间.Dte2.Width = 90;

            this.uc年龄区间.Txt1.Properties.Mask.EditMask = "###";
            this.uc年龄区间.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.uc年龄区间.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.uc年龄区间.Txt2.Properties.Mask.EditMask = "###"; ;
            this.uc年龄区间.Txt2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.uc年龄区间.Txt2.Properties.Mask.UseMaskAsDisplayFormat = true;


            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案状态, cbo档案状态, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, cbo性别, "P_CODE", "P_DESC");
            //cbo性别.SelectedIndex = 1;
            //为“镇”绑定信息
            DataView dv镇 = new DataView(DataDictCache.Cache.t地区信息);
            dv镇.RowFilter = "上级编码=371323";
            //DataBinder.BindingLookupEditDataSource(lookUpEdit镇, dv市.ToTable(), "地区名称", "地区编码");
            util.ControlsHelper.BindComboxData(dv镇.ToTable(), comboBoxEdit镇, "地区编码", "地区名称");

            //为“所属机构"绑定信息
            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                treeListLookUpEdit机构.Properties.ValueMember = "机构编号";
                treeListLookUpEdit机构.Properties.DisplayMember = "机构名称";

                treeListLookUpEdit机构.Properties.TreeList.KeyFieldName = "机构编号";
                treeListLookUpEdit机构.Properties.TreeList.ParentFieldName = "上级机构";
                treeListLookUpEdit机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                treeListLookUpEdit机构.Properties.DataSource = dt所属机构;
                //treeListLookUpEdit机构.Properties.TreeList.ExpandAll();
                //treeListLookUpEdit机构.Properties.TreeList.CollapseAll();

                treeListLookUpEdit机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                treeListLookUpEdit机构.Text = Loginer.CurrentUser.所属机构;
            }
            if (Loginer.CurrentUser.所属机构.Length >= 12)
                treeListLookUpEdit机构.Properties.AutoExpandAllNodes = true;
            else { treeListLookUpEdit机构.Properties.AutoExpandAllNodes = false; }
            //绑定“录入人”绑定信息
            try
            {
                DataTable dt录入人 = new bllUser().GetUserNameAndIDBy机构用户(treeListLookUpEdit机构.EditValue.ToString());
                cbo录入人.Properties.DisplayMember = "UserName";
                cbo录入人.Properties.ValueMember = "用户编码";
                cbo录入人.Properties.DataSource = dt录入人;
            }
            catch (Exception ex)
            {
                cbo录入人.Text = "请选择...";
            }
        }
        private void btn查询_Click(object sender, EventArgs e)
        {
            if (!funCheck()) return;

            this.pagerControl1.InitControl();
            #region 查询条件
            _strWhere = string.Empty;
            string pgrid = this.treeListLookUpEdit机构.EditValue.ToString();

            if (this.txt姓名.Text.Trim() != "")
            {
                _strWhere += " and (姓名 like'" + this.txt姓名.Text.Trim() + "%') ";
            }
            if (this.txt档案编号.Text.Trim() != "")
            {
                _strWhere += " and 个人档案编号 ='" + this.txt档案编号.Text.Trim() + "'";
            }
            if (this.txt身份证号.Text.Trim() != "")
            {
                _strWhere += " and 身份证号 ='" + this.txt身份证号.Text.Trim() + "'";
            }
            if (util.ControlsHelper.GetComboxKey(comboBoxEdit镇) != "")
            {
                _strWhere += " and 街道编码 ='" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'";
            }
            if (util.ControlsHelper.GetComboxKey(comboBoxEdit村) != "")
            {
                _strWhere += " and [居委会编码] ='" + util.ControlsHelper.GetComboxKey(comboBoxEdit村) + "'";
            }
            if (this.txt详细地址.Text.Trim() != "")
            {
                _strWhere += " and [居住地址] like '" + this.txt详细地址.Text.Trim() + "%'";
            }

            if (this.uC出生日期.Dte1.Text.Trim() != "")
            {
                _strWhere += " and [出生日期] >= '" + this.uC出生日期.Dte1.Text.Trim() + ",  00:00:00'";
            }
            if (this.uC出生日期.Dte2.Text.Trim() != "")
            {
                _strWhere += " and [出生日期] <= '" + this.uC出生日期.Dte2.Text.Trim() + ", 23:59:59'";
            }
            if (util.ControlsHelper.GetComboxKey(this.cbo档案状态) != "")
            {
                _strWhere += " and [档案状态] = '" + util.ControlsHelper.GetComboxKey(this.cbo档案状态) + "'";
            }
            #endregion
            if (this.chk含下属机构.Checked)//包含下属机构
            {
                if (pgrid.Length == 12)
                {
                    _strWhere += " and ([所属机构]=  '" + pgrid + "' or substring([所属机构],1,7)+'1'+substring([所属机构],9,7) like '" + pgrid + "%')";
                }
                else
                {
                    _strWhere += " and [所属机构] like '" + pgrid + "%'";
                }
            }
            else
            {
                _strWhere += " and [所属机构] ='" + pgrid + "'";
            }
            if (this.cbo录入人.Text.Trim() != "")
            {
                _strWhere += " and [创建人] = '" + this.cbo录入人.EditValue.ToString() + "'";
            }
            if (this.uC录入时间.Dte1.Text.Trim() != "")
            {
                _strWhere += " and [创建时间] >= '" + this.uC录入时间.Dte1.Text.Trim() + ",  00:00:00'";
            }
            if (this.uC录入时间.Dte2.Text.Trim() != "")
            {
                _strWhere += " and [创建时间] <= '" + this.uC录入时间.Dte2.Text.Trim() + ", 23:59:59'";
            }

            BindDataList();

        }

        private bool funCheck()
        {
            if (this.uC出生日期.Dte1.DateTime > this.uC出生日期.Dte2.DateTime)
            {
                Msg.Warning("出生日期 起始日期不能大于结束日期");
                this.uC出生日期.Dte1.Focus();
                return false;
            }
            if (this.uC录入时间.Dte1.DateTime > this.uC录入时间.Dte2.DateTime)
            {
                Msg.Warning("录入时间 起始日期不能大于结束日期");
                this.uC出生日期.Dte1.Focus();
                return false;
            }
            if (!string.IsNullOrEmpty(this.uc年龄区间.Txt1.Text.Trim()) && !string.IsNullOrEmpty(this.uc年龄区间.Txt2.Text.Trim()))
            {
                int a = Convert.ToInt32(this.uc年龄区间.Txt1.Text);
                int b = Convert.ToInt32(this.uc年龄区间.Txt2.Text);
                if (a > b)
                {
                    Msg.Warning("年龄区间 起始年龄不能大于结束年龄");
                    this.uc年龄区间.Txt1.Focus();
                    return false;
                }
            }
            return true;
        }
        private void BindDataList()
        {
            this.gc人次.StrWhere = _strWhere;
            this.gc人次.View = "vw_精神病基本信息";
            DataSet ds = this.pagerControl1.GetQueryResult("vw_精神病基本信息", _strWhere);
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                ds.Tables[0].Rows[i][Models.tb_精神疾病信息补充表.创建人] = _BLL.Return用户名称(ds.Tables[0].Rows[i][Models.tb_精神疾病信息补充表.创建人].ToString());
                ds.Tables[0].Rows[i]["所属机构"] = _BLL.Return机构名称(ds.Tables[0].Rows[i][tb_精神疾病信息补充表.所属机构].ToString());
                ds.Tables[0].Rows[i]["居住地址"] = _BLL.Return地区名称(ds.Tables[0].Rows[i]["街道"].ToString()) + _BLL.Return地区名称(ds.Tables[0].Rows[i]["居委会"].ToString()) + ds.Tables[0].Rows[i]["居住地址"];
            }
            this.gc人次.DataSource = ds.Tables[0];
            this.pagerControl1.DrawControl();
            this.gv人次.BestFitColumns();//列自适应宽度         
        }
        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv人次.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            string id = row["ID"].ToString();
            //_BLL.GetBusinessByKey(个人档案编号, true);//下载一个空业务单据            
            //_BLL.NewBusiness(); //增加一条主表记录
            frm个人健康 frm = new frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, id);
            frm.Show();
        }
        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            BindDataList();
        }
        private void quAge1()
        {

            string age1 = this.uc年龄区间.Txt1.Text.Trim();
            string age2 = this.uc年龄区间.Txt2.Text.Trim();
            if (age1.Length >= 3)
            {
                Msg.Warning("年龄所输入的数字不能超过3位！");
                this.uc年龄区间.Txt1.Text = "";
                age1 = "";
                this.uC出生日期.Dte2.Text = "";
            }
            if (age2.Length >= 3)
            {
                Msg.Warning("年龄所输入的数字不能超过3位！");
                this.uc年龄区间.Txt2.Text = "";
                age2 = "";
                this.uC出生日期.Dte1.Text = "";
            }
            //var re=new RegExp("^\\d+$");

            string nowTime = _BLL.ServiceDateTime;
            string start = this.uC出生日期.Dte1.Text;
            string end = this.uC出生日期.Dte2.Text;


        }
        protected override void InitializeForm()
        {
            base.InitializeForm();
            frmGridCustomize.RegisterGrid(gv人次);
        }

        private void comboBoxEdit镇_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv村 = new DataView(DataDictCache.Cache.t地区信息);
                dv村.RowFilter = "上级编码='" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'";
                //DataBinder.BindingLookupEditDataSource(lookUpEdit村, dv县.ToTable(), "地区名称", "地区编码");
                util.ControlsHelper.BindComboxData(dv村.ToTable(), comboBoxEdit村, "地区编码", "地区名称");
                util.ControlsHelper.SetComboxNullData("", comboBoxEdit村, "请选择居/村委会");
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }

        private void treeListLookUpEdit机构_EditValueChanged(object sender, EventArgs e)
        {
            DataTable dt录入人 = _bllUser.GetUserNameAndIDBy机构用户(treeListLookUpEdit机构.EditValue.ToString());
            cbo录入人.Properties.DisplayMember = "UserName";
            cbo录入人.Properties.ValueMember = "用户编码";
            cbo录入人.Properties.DataSource = dt录入人;
        }

        private void btn删除_Click(object sender, EventArgs e)
        {

        }

    }
}
