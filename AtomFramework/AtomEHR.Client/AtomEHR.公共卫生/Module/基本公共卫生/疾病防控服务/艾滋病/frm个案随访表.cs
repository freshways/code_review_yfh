﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Interfaces;

namespace AtomEHR.公共卫生.Module.基本公共卫生.疾病防控服务
{
    public partial class frm个案随访表 : AtomEHR.Library.frmBaseBusinessForm
    {
        public frm个案随访表()
        {
            InitializeComponent();
        }

        private void frm个案随访表_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
            this.pagerControl1.Height = 35;
        }

        protected override void InitializeForm()
        {
            _BLL = new bll个案随访表();// 业务逻辑层实例
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gvSummary);
            BindingSummarySearchPanel(btnQuery, btnEmpty, gcFindGroup);

            //绑定相关缓存数据
            DataView dv县 = new DataView(DataDictCache.Cache.t地区信息);
            dv县.RowFilter = "上级编码='" + Loginer.CurrentUser.单位代码 + "' ";
            //DataBinder.BindingComboEdit(txt镇, dv县.ToTable(), "地区名称", "地区编码");
            util.ControlsHelper.BindComboxData(dv县.ToTable(), txt镇, "地区编码", "地区名称");
            txt镇.EditValueChanged += txt镇_EditValueChanged;
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, txt性别, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t职业, txt职业, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案状态, txt档案状态, "P_CODE", "P_DESC");
            txt档案状态.SelectedIndex = 1;
            util.ControlsHelper.BindComboxData(BLL.ConvertRowsToTable(DataDictCache.Cache.User.Select("所属机构='" + Loginer.CurrentUser.所属机构 + "'")),
                txt录入人, "用户编码", "UserName");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t是否, txt合格档案, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t是否随访, cbo是否随访, "P_CODE", "P_DESC");
            //cbo是否随访.SelectedIndex = 1;//默认已随访
            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                txt机构.Properties.ValueMember = "机构编号";
                txt机构.Properties.DisplayMember = "机构名称";

                txt机构.Properties.TreeList.KeyFieldName = "机构编号";
                txt机构.Properties.TreeList.ParentFieldName = "上级机构";
                txt机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                txt机构.Properties.DataSource = dt所属机构;
                if (Loginer.CurrentUser.所属机构.Length >= 12)
                    txt机构.Properties.AutoExpandAllNodes = true;
                else { txt机构.Properties.AutoExpandAllNodes = false; }

                txt机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                txt机构.Text = Loginer.CurrentUser.所属机构;
            }

            _BLL.GetBusinessByKey("-", true);//加载一个空的业务对象.

            ShowSummaryPage(true); //一切初始化完毕后显示SummaryPage        
            this.pagerControl1.OnPageChanged += pagerControl1_OnPageChanged;
        }

        private void txt镇_EditValueChanged(object sender, EventArgs e)
        {
            DataView dv村 = new DataView(DataDictCache.Cache.t地区信息);
            dv村.RowFilter = "上级编码='" + util.ControlsHelper.GetComboxKey(txt镇) + "'";
            //DataBinder.BindingLookupEditDataSource(lookUpEdit村, dv县.ToTable(), "地区名称", "地区编码");
            util.ControlsHelper.BindComboxData(dv村.ToTable(), txt村, "地区编码", "地区名称");
            util.ControlsHelper.SetComboxNullData("", txt村, "请选择居/村委会");
        }

        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            DoSearchSummary();
        }

        private void link档案号_Click(object sender, EventArgs e)
        {
            DataRow row = this.gvSummary.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            string date = row["创建时间"] as string;
            Module.个人健康.frm个人健康 frm = new Module.个人健康.frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, date);
            frm.Show();
        }

        /// <summary>
        /// 绑定列表数据
        /// </summary>
        /// <returns></returns>
        protected override bool DoSearchSummary()
        {
            //DataTable dt = (_BLL as bll接诊记录).GetSummaryByParam("and 1=1");
            string _strWhere = "";
            if (txt机构.Text != "" && txt机构.EditValue != null)
            {
                string pgrid = txt机构.EditValue.ToString();
                if (ck含下属机构.Checked)
                {
                    if (pgrid.Length == 12)
                        _strWhere += " and (所属机构='" + pgrid + "' or 上级机构编号='" + pgrid + "')";
                        //_strWhere += " AND [所属机构] IN (select 机构编号 from tb_机构信息 where 上级机构='" + pgrid + "' or 机构编号='"+pgrid+"' ) ";
                       // _strWhere += " AND ([所属机构]=  '" + pgrid + "' OR SUBSTRING([所属机构],1,7)+'1'+SUBSTRING([所属机构],9,7) LIKE '" + pgrid + "%')";
                    else
                        _strWhere += " AND [所属机构] LIKE '" + pgrid + "%'";
                }
                else
                    _strWhere += " AND 所属机构='" + pgrid + "'";
            }
            if (txt姓名.Text != "")
                _strWhere += " AND (姓名 like'" + this.txt姓名.Text.Trim() + "%') ";
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt性别)))
                _strWhere += " AND 性别='" + txt性别.Text.ToString() + "'";
            if (txt档案编号.Text != "")
                _strWhere += " AND 个人档案编号='" + txt档案编号.Text.ToString() + "'";
            if (txt出生日期1.Text != "")
                _strWhere += " AND 出生日期>='" + txt出生日期1.Text.ToString() + "'";
            if (txt出生日期2.Text != "")
                _strWhere += " AND 出生日期<='" + txt出生日期2.Text.ToString() + "'";
            //if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt管理组别)))
            //{
            //    _strWhere += " AND 个人档案编号 IN ( SELECT 个人档案编号 FROM tb_MXB高血压管理卡 WHERE 管理组别='" + util.ControlsHelper.GetComboxKey(txt管理组别) + "')  ";
            //}
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt档案状态)))
            {
                _strWhere += " AND 档案状态 ='" + util.ControlsHelper.GetComboxKey(txt档案状态) + "'  ";
            }
            if (txt身份证.Text != "")
                _strWhere += " AND 身份证号='" + txt身份证.Text.ToString() + "'";
            if (txt录入时间1.Text != "")
                _strWhere += " AND CONVERT(DATETIME,创建时间)>='" + txt录入时间1.Text.ToString() + "'";
            if (txt录入时间2.Text != "")
                _strWhere += " AND CONVERT(DATETIME,创建时间)<='" + txt录入时间2.Text.ToString() + " 23:59:59'";
            //if (txt随访时间1.Text != "")
            //    _strWhere += " AND 发生时间>='" + txt随访时间1.Text.ToString() + "'";
            //if (txt随访时间2.Text != "")
            //    _strWhere += " AND 发生时间<='" + txt随访时间2.Text.ToString() + " 23:59:59'";
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt录入人)))
            {
                _strWhere += " AND 创建人='" + util.ControlsHelper.GetComboxKey(txt录入人) + "'";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt合格档案)))
            {
                if (util.ControlsHelper.GetComboxKey(txt合格档案) == "2")//不合格档案
                {
                    _strWhere += " AND 完整度 <> 100 ";
                }
                if (util.ControlsHelper.GetComboxKey(txt合格档案) == "1")//合格档案
                {
                    _strWhere += " AND 完整度 = 100 ";
                }
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt职业)))
                _strWhere += " AND 职业='" + util.ControlsHelper.GetComboxKey(txt职业) + "'";
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt镇)))
                _strWhere += " AND 街道编码='" + util.ControlsHelper.GetComboxKey(txt镇) + "'";
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt村)))
                _strWhere += " AND 居委会编码='" + util.ControlsHelper.GetComboxKey(txt村) + "'";
            if (txt地址.Text != "")
                _strWhere += " AND 居住地址='" + txt地址.Text.ToString() + "'";
            //添加是否随访判断条件
            if (this.cbo是否随访.Text == null || this.cbo是否随访.Text == "请选择")
            {
                if (this.txt随访时间1.Text != "")
                    _strWhere += " AND 随访日期>='" + this.txt随访时间1.Text.ToString() + "'";
                if (this.txt随访时间2.Text != "")
                    _strWhere += " AND 随访日期<='" + this.txt随访时间2.Text.ToString() + " 23:59:59'";
            }
            else if (this.cbo是否随访.Text == "已随访")
            {
                _strWhere = " and ID in (select MAX(ID) as ID from vw_个案随访表 where 1=1 " + _strWhere
                                     + " and 随访日期 between '" + this.txt随访时间1.Text.ToString() + "' and '" + this.txt随访时间2.Text.ToString()+ "' group by 个人档案编号 )";
            }
            else if (this.cbo是否随访.Text == "未随访")
            {
                _strWhere = " and ID in (select MAX(ID) as ID from vw_个案随访表 where 1=1 " + _strWhere
                                     + " and 个人档案编号 not in (select 个人档案编号 from tb_个案随访表 where 随访日期 between '" + this.txt随访时间1.Text.ToString() + "' and '" + this.txt随访时间2.Text.ToString() + "') group by 个人档案编号 )";
            }
            else { }

            gcSummary.View = "vw_个案随访表";
            gcSummary.StrWhere = _strWhere;

            DataTable dt = this.pagerControl1.GetQueryResultNew("vw_个案随访表", "*",
                _strWhere, "CONVERT(DATETIME,创建时间)", "DESC").Tables[0]; //((bll个案随访表)_BLL).GetSummaryByParam(_strWhere);
            this.pagerControl1.DrawControl();

            foreach (DataRow dr in dt.Rows)
            {
                dr[tb_健康档案.姓名] = util.DESEncrypt.DES解密(dr[tb_健康档案.姓名].ToString());
                //dr[tb_健康档案.性别] = _BLL.ReturnDis字典显示("xb_xingbie", dr[tb_健康档案.性别].ToString());
                //dr[tb_健康档案.所属机构] = _BLL.Return机构名称(dr[tb_健康档案.所属机构].ToString());
                //dr[tb_健康档案.创建人] = _BLL.Return用户名称(dr[tb_健康档案.创建人].ToString());
                dr[tb_健康档案.居住地址] =  _BLL.Return地区名称(dr[tb_健康档案.街道].ToString()) + _BLL.Return地区名称(dr[tb_健康档案.居委会].ToString()) + dr[tb_健康档案.居住地址].ToString();
            }
            DoBindingSummaryGrid(dt); //绑定主表的Grid
            ShowSummaryPage(true); //显示Summary页面.   

            return dt != null && dt.Rows.Count > 0;
        }

        #region button
        private void btnQuery_Click(object sender, EventArgs e)
        {
            pagerControl1.InitControl();
        }
        private void btn修改_Click(object sender, EventArgs e)
        {
            link档案号_Click(sender, e);
            //this.AssertFocusedRow();
            //Form form = MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frmModifyLog));
            //(form as frmModifyLog).ShowModifyLog(this.Name);
        }
        private void btn删除_Click(object sender, EventArgs e)
        {
            Msg.ShowInformation("请到修改页面确认后删除！");
        }
        #endregion

    }
}
