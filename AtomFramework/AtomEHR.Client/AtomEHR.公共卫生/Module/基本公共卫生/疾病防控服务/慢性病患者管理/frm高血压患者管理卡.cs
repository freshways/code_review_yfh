﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Interfaces;

namespace AtomEHR.公共卫生.Module.基本公共卫生.疾病防控服务
{
    public partial class frm高血压患者管理卡 : AtomEHR.Library.frmBaseBusinessForm
    {
        public frm高血压患者管理卡()
        {
            InitializeComponent();
        }

        private void frm高血压患者管理卡_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
            this.pagerControl1.Height = 35;
        }

        #region Init初始化
        protected override void InitializeForm()
        {
            _BLL = new bllMXB高血压管理卡();// 业务逻辑层实例
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gvSummary);
            BindingSummarySearchPanel(btnQuery, btnEmpty, gcFindGroup);

            //绑定相关缓存数据
            //DataBinder.BindingLookupEditDataSource(txt机构级别, DataDictCache.Cache.t机构级别, "NativeName", "DataCode");
            DataView dv县 = new DataView(DataDictCache.Cache.t地区信息);
            dv县.RowFilter = "上级编码=371323";
            util.ControlsHelper.BindComboxData(dv县.ToTable(), txt镇, "地区编码", "地区名称");
            txt镇.EditValueChanged += txt镇_EditValueChanged;
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, txt性别, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t职业, txt职业, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(_BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='gxyglzb' ")), txt管理组别, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案状态, txt档案状态, "P_CODE", "P_DESC");
            txt档案状态.SelectedIndex = 1;
            util.ControlsHelper.BindComboxData(BLL.ConvertRowsToTable(DataDictCache.Cache.User.Select("所属机构='" + Loginer.CurrentUser.所属机构 + "'")),
                txt录入人, "用户编码", "UserName");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t是否, txt合格档案, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t是否, txt终止管理, "P_CODE", "P_DESC");
            util.ControlsHelper.SetComboxData("2", txt终止管理);//默认为未终止管理的
            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                txt机构.Properties.ValueMember = "机构编号";
                txt机构.Properties.DisplayMember = "机构名称";

                txt机构.Properties.TreeList.KeyFieldName = "机构编号";
                txt机构.Properties.TreeList.ParentFieldName = "上级机构";
                txt机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                txt机构.Properties.DataSource = dt所属机构;
                if (Loginer.CurrentUser.所属机构.Length >= 12)
                    txt机构.Properties.AutoExpandAllNodes = true;
                else { txt机构.Properties.AutoExpandAllNodes = false; }

                txt机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                txt机构.Text = Loginer.CurrentUser.所属机构;
            }

            _BLL.GetBusinessByKey("-", true);//加载一个空的业务对象.

            ShowSummaryPage(true); //一切初始化完毕后显示SummaryPage        
            this.pagerControl1.OnPageChanged += pagerControl1_OnPageChanged;
        }

        private void txt镇_EditValueChanged(object sender, EventArgs e)
        {
            DataView dv村 = new DataView(DataDictCache.Cache.t地区信息);
            dv村.RowFilter = "上级编码='" + util.ControlsHelper.GetComboxKey(txt镇) + "'";
            util.ControlsHelper.BindComboxData(dv村.ToTable(), txt村, "地区编码", "地区名称");
            util.ControlsHelper.SetComboxNullData("", txt村, "请选择居/村委会");
        }

        #endregion

        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            DoSearchSummary();
        }

        private void link档案号_Click(object sender, EventArgs e)
        {
            DataRow row = this.gvSummary.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            string date = row["创建时间"] as string;
            Module.个人健康.frm个人健康 frm = new Module.个人健康.frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, date);
            frm.Show();
        }

        /// <summary>
        /// 绑定列表数据
        /// </summary>
        /// <returns></returns>
        protected override bool DoSearchSummary()
        {
            //DataTable dt = (_BLL as bll接诊记录).GetSummaryByParam("and 1=1");
            string _strWhere = "";
            if (txt机构.Text != "" && txt机构.EditValue != null)
            {
                string pgrid = txt机构.EditValue.ToString();
                if (ck含下属机构.Checked)
                {
                    if (pgrid.Length == 12)
                        _strWhere += " and ([所属机构]=  '" + pgrid + "' or substring([所属机构],1,7)+'1'+substring([所属机构],9,7) like '" + pgrid + "%')";
                    else
                        _strWhere += " and [所属机构] like '" + pgrid + "%'";
                }
                else
                    _strWhere += " and 所属机构='" + pgrid + "'";
            }
            if (txt姓名.Text != "")
                _strWhere += " and (姓名 like'" + this.txt姓名.Text.Trim() + "%') ";
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt性别)))
                _strWhere += " and 性别='" + txt性别.Text.ToString() + "'";
            if (txt档案编号.Text != "")
                _strWhere += " and 个人档案编号='" + txt档案编号.Text.ToString() + "'";
            if (txt出生日期1.Text != "")
                _strWhere += " and 出生日期>='" + txt出生日期1.Text.ToString() + "'";
            if (txt出生日期2.Text != "")
                _strWhere += " and 出生日期<='" + txt出生日期2.Text.ToString() + "'";
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt管理组别)))
            {
                _strWhere += " and  管理组别='" + util.ControlsHelper.GetComboxKey(txt管理组别) + "'  ";
            }
            if (!string.IsNullOrEmpty(txt分级管理.Text) && !txt分级管理.Text.Equals("请选择"))
            {
                string s管理组别 = string.Empty;
                switch (txt分级管理.Text)
                {
                    case "三级":
                        s管理组别 = "1";
                        break;
                    case "二级":
                        s管理组别 = "2";
                        break;
                    case "一级":
                        s管理组别 = "3";
                        break;
                    default:
                        break;
                }
                _strWhere += " and  管理组别='" + s管理组别 + "'  ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt档案状态)))
            {
                _strWhere += " and 档案状态 ='" + util.ControlsHelper.GetComboxKey(txt档案状态) + "'  ";
            }
            if (txt身份证.Text != "")
                _strWhere += " and 身份证号='" + txt身份证.Text.ToString() + "'";
            if (txt录入时间1.Text != "")
                _strWhere += " and 创建时间>='" + txt录入时间1.Text.ToString() + "'";
            if (txt录入时间2.Text != "")
                _strWhere += " and 创建时间<='" + txt录入时间2.Text.ToString() + " 23:59:59'";
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt终止管理)))
            {
                if (util.ControlsHelper.GetComboxKey(txt终止管理) == "1")
                    _strWhere += " and 终止管理=1 ";
                else
                    _strWhere += " and isnull(终止管理,'')<> 1 ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt录入人)))
            {
                _strWhere += " and 创建人='" + util.ControlsHelper.GetComboxKey(txt录入人) + "'";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt合格档案)))
            {
                if (util.ControlsHelper.GetComboxKey(txt合格档案) == "2")//不合格档案
                {
                    _strWhere += " and 完整度 <> 100 ";
                }
                if (util.ControlsHelper.GetComboxKey(txt合格档案) == "1")//合格档案
                {
                    _strWhere += " and 完整度 = 100 ";
                }
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt职业)))
                _strWhere += " and 职业='" + util.ControlsHelper.GetComboxKey(txt职业) + "'";
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt镇)))
                _strWhere += " and 街道编码='" + util.ControlsHelper.GetComboxKey(txt镇) + "'";
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt村)))
                _strWhere += " and 居委会编码='" + util.ControlsHelper.GetComboxKey(txt村) + "'";
            if (txt地址.Text != "")
                _strWhere += " and 居住地址='" + txt地址.Text.ToString() + "'";
            if(!string.IsNullOrEmpty(txt随访次数.Text))
                _strWhere += " and 随访次数<'" + txt随访次数.Text.ToString() + "'";
            if (ck控制不满意.Checked)
                _strWhere += "  and  exists (select 1 from tb_MXB高血压随访表 b where b.个人档案编号 =vw_mxb高血压管理卡.个人档案编号 and PATINDEX('2%',本次随访分类)>0 ) ";

            gcSummary.View = "vw_mxb高血压管理卡";
            gcSummary.StrWhere = _strWhere;

            DataTable dt = this.pagerControl1.GetQueryResult("vw_mxb高血压管理卡", _strWhere).Tables[0];
            this.pagerControl1.DrawControl();

            foreach (DataRow dr in dt.Rows)
            {
                dr[tb_健康档案.姓名] = util.DESEncrypt.DES解密(dr[tb_健康档案.姓名].ToString());
                dr[tb_健康档案.居住地址] = _BLL.Return地区名称(dr[tb_健康档案.街道].ToString()) + _BLL.Return地区名称(dr[tb_健康档案.居委会].ToString()) + dr[tb_健康档案.居住地址].ToString();
            }
            DoBindingSummaryGrid(dt); //绑定主表的Grid
            ShowSummaryPage(true); //显示Summary页面.   

            return dt != null && dt.Rows.Count > 0;
        }


        #region ButtonClick
        private void btnQuery_Click(object sender, EventArgs e)
        {
            pagerControl1.InitControl();
        }

        //这个是打开修改记录
        private void btn修改_Click(object sender, EventArgs e)
        {
            link档案号_Click(sender, e);
            //this.AssertFocusedRow();
            //Form form = MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frmModifyLog));
            //(form as frmModifyLog).ShowModifyLog(this.Name);
        }


        private void btn删除_Click(object sender, EventArgs e)
        {
            Msg.ShowInformation("请到修改页面确认后删除！");
        }
        #endregion

    }
}
