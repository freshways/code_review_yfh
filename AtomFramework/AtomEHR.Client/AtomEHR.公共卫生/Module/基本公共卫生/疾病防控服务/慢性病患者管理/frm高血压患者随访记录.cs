﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Interfaces;
using AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息;

namespace AtomEHR.公共卫生.Module.基本公共卫生.疾病防控服务
{
    public partial class frm高血压患者随访记录 : AtomEHR.Library.frmBaseBusinessForm
    {
        public frm高血压患者随访记录()
        {
            InitializeComponent();
        }

        private void frm高血压患者管理卡_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
            this.pagerControl1.Height = 35;
        }

        protected override void InitializeForm()
        {
            _BLL = new bllMXB高血压随访表();// 业务逻辑层实例
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gvSummary);
            BindingSummarySearchPanel(btnQuery, btnEmpty, gcFindGroup);

            //绑定相关缓存数据
            DataView dv县 = new DataView(DataDictCache.Cache.t地区信息);
            dv县.RowFilter = "上级编码=371323";
            //DataBinder.BindingComboEdit(txt镇, dv县.ToTable(), "地区名称", "地区编码");
            util.ControlsHelper.BindComboxData(dv县.ToTable(), txt镇, "地区编码", "地区名称");
            txt镇.EditValueChanged += txt镇_EditValueChanged;
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, txt性别, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t职业, txt职业, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(_BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='gxyglzb' ")), txt管理组别, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案状态, txt档案状态, "P_CODE", "P_DESC");
            txt档案状态.SelectedIndex = 1;
            util.ControlsHelper.BindComboxData(BLL.ConvertRowsToTable(DataDictCache.Cache.User.Select("所属机构='" + Loginer.CurrentUser.所属机构 + "'")),
                txt录入人, "用户编码", "UserName");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t是否, txt合格档案, "P_CODE", "P_DESC");
            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                txt机构.Properties.ValueMember = "机构编号";
                txt机构.Properties.DisplayMember = "机构名称";

                txt机构.Properties.TreeList.KeyFieldName = "机构编号";
                txt机构.Properties.TreeList.ParentFieldName = "上级机构";
                txt机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                txt机构.Properties.DataSource = dt所属机构;
                if (Loginer.CurrentUser.所属机构.Length >= 12)
                    txt机构.Properties.AutoExpandAllNodes = true;
                else { txt机构.Properties.AutoExpandAllNodes = false; }

                txt机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                txt机构.Text = Loginer.CurrentUser.所属机构;
            }

            _BLL.GetBusinessByKey("-", true);//加载一个空的业务对象.

            ShowSummaryPage(true); //一切初始化完毕后显示SummaryPage        
            this.pagerControl1.OnPageChanged += pagerControl1_OnPageChanged;
        }

        private void txt镇_EditValueChanged(object sender, EventArgs e)
        {
            DataView dv村 = new DataView(DataDictCache.Cache.t地区信息);
            dv村.RowFilter = "上级编码='" + util.ControlsHelper.GetComboxKey(txt镇) + "'";
            //DataBinder.BindingLookupEditDataSource(lookUpEdit村, dv县.ToTable(), "地区名称", "地区编码");
            util.ControlsHelper.BindComboxData(dv村.ToTable(), txt村, "地区编码", "地区名称");
            util.ControlsHelper.SetComboxNullData("", txt村, "请选择居/村委会");
        }

        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            DoSearchSummary();
        }

        private void link档案号_Click(object sender, EventArgs e)
        {
            DataRow row = this.gvSummary.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            string date = row["创建时间"] as string;
            Module.个人健康.frm个人健康 frm = new Module.个人健康.frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, date);
            frm.Show();
        }

        string _strWhere = "";
        /// <summary>
        /// 绑定列表数据
        /// </summary>
        /// <returns></returns>
        protected override bool DoSearchSummary()
        {
            //DataTable dt = (_BLL as bll接诊记录).GetSummaryByParam("and 1=1");
            _strWhere = "";
            if (txt机构.Text != "" && txt机构.EditValue != null)
            {
                string pgrid = txt机构.EditValue.ToString();
                if (ck含下属机构.Checked)
                {
                    if (pgrid.Length == 12)
                        _strWhere += " AND ([所属机构]=  '" + pgrid + "' OR SUBSTRING([所属机构],1,7)+'1'+SUBSTRING([所属机构],9,7) LIKE '" + pgrid + "%')";
                    else
                        _strWhere += " AND [所属机构] LIKE '" + pgrid + "%'";
                }
                else
                    _strWhere += " AND 所属机构='" + pgrid + "'";
            }
            if (txt姓名.Text != "")
                _strWhere += " AND (姓名 like'" + this.txt姓名.Text.Trim() + "%') ";
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt性别)))
                _strWhere += " AND 性别='" + txt性别.Text.ToString() + "'";
            if (txt档案编号.Text != "")
                _strWhere += " AND 个人档案编号='" + txt档案编号.Text.ToString() + "'";
            if (txt出生日期1.Text != "")
                _strWhere += " AND 出生日期>='" + txt出生日期1.Text.ToString() + "'";
            if (txt出生日期2.Text != "")
                _strWhere += " AND 出生日期<='" + txt出生日期2.Text.ToString() + "'";
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt管理组别)))
            {
                _strWhere += " AND 个人档案编号 IN ( SELECT 个人档案编号 FROM tb_MXB高血压管理卡 WHERE 管理组别='" + util.ControlsHelper.GetComboxKey(txt管理组别) + "')  ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt档案状态)))
            {
                _strWhere += " AND 档案状态 ='" + util.ControlsHelper.GetComboxKey(txt档案状态) + "'  ";
            }
            if (txt身份证.Text != "")
                _strWhere += " AND 身份证号='" + txt身份证.Text.ToString() + "'";
            if (txt录入时间1.Text != "")
                _strWhere += " AND CONVERT(DATETIME,创建时间)>='" + txt录入时间1.Text.ToString() + "'";
            if (txt录入时间2.Text != "")
                _strWhere += " AND CONVERT(DATETIME,创建时间)<='" + txt录入时间2.Text.ToString() + " 23:59:59'";
            if (txt随访时间1.Text != "")
                _strWhere += " AND 发生时间>='" + txt随访时间1.Text.ToString() + "'";
            if (txt随访时间2.Text != "")
                _strWhere += " AND 发生时间<='" + txt随访时间2.Text.ToString() + " 23:59:59'";
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt录入人)))
            {
                _strWhere += " AND 创建人='" + util.ControlsHelper.GetComboxKey(txt录入人) + "'";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt合格档案)))
            {
                if (util.ControlsHelper.GetComboxKey(txt合格档案) == "2")//不合格档案
                {
                    _strWhere += " AND 完整度 <> 100 ";
                }
                if (util.ControlsHelper.GetComboxKey(txt合格档案) == "1")//合格档案
                {
                    _strWhere += " AND 完整度 = 100 ";
                }
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt职业)))
                _strWhere += " AND 职业='" + util.ControlsHelper.GetComboxKey(txt职业) + "'";
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt镇)))
                _strWhere += " AND 街道编码='" + util.ControlsHelper.GetComboxKey(txt镇) + "'";
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(txt村)))
                _strWhere += " AND 居委会编码='" + util.ControlsHelper.GetComboxKey(txt村) + "'";
            if (txt地址.Text != "")
                _strWhere += " AND 居住地址='" + txt地址.Text.ToString() + "'";
            //if ((!string.IsNullOrEmpty(txt收缩压.Text) && decimal.Parse(txt收缩压.Text) > 0) 
            //    && (!string.IsNullOrEmpty(txt舒张压.Text) && decimal.Parse(txt舒张压.Text) > 0))
            //    _strWhere += " AND (收缩压='" + txt收缩压.Text.ToString() + "' or 舒张压>='" + txt舒张压.Text.ToString() + "') ";
            if (!string.IsNullOrEmpty(txt收缩压.Text) && decimal.Parse(txt收缩压.Text) > 0)
                _strWhere += " AND 收缩压>='" + txt收缩压.Text.ToString() + "'";
            if (!string.IsNullOrEmpty(txt舒张压.Text) && decimal.Parse(txt舒张压.Text) > 0)
                _strWhere += " AND 舒张压>='" + txt舒张压.Text.ToString() + "'";

            gcSummary.View = "vw_mxb高血压随访记录";
            gcSummary.StrWhere = _strWhere;

            DataTable dt = this.pagerControl1.GetQueryResultNew("vw_mxb高血压随访记录", "*",
                _strWhere, "CONVERT(DATETIME,创建时间)", "DESC").Tables[0]; //((bllMXB高血压随访表)_BLL).GetSummaryByParam(_strWhere);
            this.pagerControl1.DrawControl();

            foreach (DataRow dr in dt.Rows)
            {
                //dr[tb_健康档案.姓名] = util.DESEncrypt.DES解密(dr[tb_健康档案.姓名].ToString());
                //dr[tb_健康档案.性别] = _BLL.ReturnDis字典显示("xb_xingbie", dr[tb_健康档案.性别].ToString());
                //dr[tb_健康档案.所属机构] = _BLL.Return机构名称(dr[tb_健康档案.所属机构].ToString());
                //dr[tb_健康档案.创建人] = _BLL.Return用户名称(dr[tb_健康档案.创建人].ToString());
                dr[tb_健康档案.居住地址] = _BLL.Return地区名称(dr[tb_健康档案.街道].ToString()) + _BLL.Return地区名称(dr[tb_健康档案.居委会].ToString()) + dr[tb_健康档案.居住地址].ToString();
            }
            DoBindingSummaryGrid(dt); //绑定主表的Grid
            ShowSummaryPage(true); //显示Summary页面.   

            return dt != null && dt.Rows.Count > 0;
        }

        #region button
        private void btnQuery_Click(object sender, EventArgs e)
        {
            pagerControl1.InitControl();
        }
        private void btn修改_Click(object sender, EventArgs e)
        {
            link档案号_Click(sender, e);
            //this.AssertFocusedRow();
            //Form form = MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frmModifyLog));
            //(form as frmModifyLog).ShowModifyLog(this.Name);
        }
        private void btn删除_Click(object sender, EventArgs e)
        {
            Msg.ShowInformation("请到修改页面确认后删除！");
        }
        #endregion

        private void btn微信推送_Click(object sender, EventArgs e)
        {
            try
            {
                string rows = "";
                for (int i = 0; i < this.gvSummary.GetSelectedRows().Length; i++)
                {
                    DataRowView dr = this.gvSummary.GetRow(this.gvSummary.GetSelectedRows()[i]) as DataRowView;

                    if (dr == null) return;
                    string 身份证号 = dr["身份证号"] as string;
                    string 体检日期 = dr["发生时间"] as string;
                    string 姓名 = dr["姓名"] as string;
                    string[] keyWord = new string[2];
                    keyWord[0] = "您的高血压随访已完成！祝您身体健康，生活愉快。";
                    keyWord[1] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    if (!string.IsNullOrEmpty(身份证号))
                    {
                        string reslut = _BLL.SendWxMessage("yFwL5QocGuRn9jknf0_9vNXoAYYnKNxy361Vle0MiWc"
                        , 身份证号, "gaoXy%2fgxy", "尊敬的[" + 姓名 + "]您好！", "点击详情查看", 体检日期, keyWord);
                    }
                    rows = (i+1).ToString();
                }
                Msg.ShowInformation("本次推送" + rows + "条随访数据!");
            }
            catch
            {

            }
        }

        private void btn导出_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "XLS file(*.xls)|*.xls";//"PDF file(*.pdf)|*.pdf";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                DataTable dt = gcSummary.DataSource as DataTable;
                Business.bll机构信息 bll = new Business.bll机构信息();
                DataTable newtable = new AtomEHR.Business.BLL_Base.bllBase().Get全部数据("vw_高血压随访详细", "*", _strWhere, "ID", "DESC");
                //for (int i = 0; i < newtable.Rows.Count; i++)
                //{
                //    //newtable.Rows[i]["姓名"] = Common.DESEncrypt.DES解密(newtable.Rows[i]["姓名"].ToString());
                //    newtable.Rows[i]["居住地址"] = newtable.Rows[i]["街道"].ToString() + newtable.Rows[i]["居委会"].ToString() + newtable.Rows[i]["居住地址"].ToString();
                //    newtable.Rows[i]["所属机构"] = newtable.Rows[i]["所属机构名称"].ToString();
                //    newtable.Rows[i]["创建人"] = bll.Return用户名称(newtable.Rows[i]["创建人"].ToString());
                //}
                try
                {

                    gridControl1.DataSource = newtable;
                    gridView1.ExportToXls(dlg.FileName);
                }
                catch (Exception ex)
                {
                    Msg.ShowError("导出失败！" + ex.Message);
                }
                finally
                {
                    gridControl1.DataSource = null;
                }

            }
        }

        private void btn新增_Click(object sender, EventArgs e)
        {
            foreach (Form frms in Application.OpenForms)
            {
                if (frms.Text == "高血压患者分级管理-新增")
                {
                    frms.Activate();
                    frms.WindowState = FormWindowState.Normal;
                    return;
                }
            }
            DataRow row = this.gvSummary.GetFocusedDataRow();
            Form frm = new Form();
            UC高血压患者分级管理 uc = new UC高血压患者分级管理(row, AtomEHR.Common.UpdateType.Add, "");
            frm.Controls.Add(uc);
            uc.Dock = DockStyle.Fill;
            frm.WindowState = FormWindowState.Normal;
            frm.Size = new System.Drawing.Size(750, 690);
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.ShowIcon = false;
            frm.MaximizeBox = false;
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm.Text = "高血压患者分级管理-新增";
            frm.Show();
        }

    }
}
