﻿using AtomEHR.Library.UserControls;
namespace AtomEHR.公共卫生.Module.基本公共卫生.疾病防控服务
{
    partial class frm冠心病患者管理卡 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm冠心病患者管理卡));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.btnEmpty = new DevExpress.XtraEditors.SimpleButton();
            this.btn修改 = new DevExpress.XtraEditors.SimpleButton();
            this.btn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.gcSummary = new AtomEHR.Library.UserControls.DataGridControl();
            this.gvSummary = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.hlink档案编号 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn添加随访 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gcFindGroup = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txt地址 = new DevExpress.XtraEditors.TextEdit();
            this.txt录入时间2 = new DevExpress.XtraEditors.DateEdit();
            this.txt录入时间1 = new DevExpress.XtraEditors.DateEdit();
            this.txt出生日期2 = new DevExpress.XtraEditors.DateEdit();
            this.txt出生日期1 = new DevExpress.XtraEditors.DateEdit();
            this.ck含下属机构 = new DevExpress.XtraEditors.CheckEdit();
            this.txt机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.txt录入人 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt管理组别 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit3 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt身份证 = new DevExpress.XtraEditors.TextEdit();
            this.txt档案编号 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt职业 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt性别 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.txt档案状态 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt镇 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt村 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt合格档案 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt终止管理 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem71 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem72 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem73 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.pagerControl1 = new TActionProject.PagerControl();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hlink档案编号)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn添加随访)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFindGroup)).BeginInit();
            this.gcFindGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入时间2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入时间2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入时间1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入时间1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck含下属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt管理组别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt镇.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt村.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt合格档案.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止管理.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gcSummary);
            this.tpSummary.Controls.Add(this.flowLayoutPanel1);
            this.tpSummary.Controls.Add(this.gcFindGroup);
            this.tpSummary.Controls.Add(this.pagerControl1);
            this.tpSummary.Size = new System.Drawing.Size(937, 430);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Size = new System.Drawing.Size(943, 436);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Size = new System.Drawing.Size(943, 436);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(932, 589);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(943, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(765, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(568, 2);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 1;
            this.layoutControlGroup1.Size = new System.Drawing.Size(880, 176);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnQuery);
            this.flowLayoutPanel1.Controls.Add(this.btn修改);
            this.flowLayoutPanel1.Controls.Add(this.btn删除);
            this.flowLayoutPanel1.Controls.Add(this.btnEmpty);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 176);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(100, 0, 0, 0);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(937, 29);
            this.flowLayoutPanel1.TabIndex = 4;
            // 
            // btnQuery
            // 
            this.btnQuery.Image = ((System.Drawing.Image)(resources.GetObject("btnQuery.Image")));
            this.btnQuery.Location = new System.Drawing.Point(103, 3);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(75, 23);
            this.btnQuery.TabIndex = 0;
            this.btnQuery.Text = "查询";
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnEmpty
            // 
            this.btnEmpty.Image = ((System.Drawing.Image)(resources.GetObject("btnEmpty.Image")));
            this.btnEmpty.Location = new System.Drawing.Point(346, 3);
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.Size = new System.Drawing.Size(75, 23);
            this.btnEmpty.TabIndex = 1;
            this.btnEmpty.Text = "重置";
            this.btnEmpty.Visible = false;
            // 
            // btn修改
            // 
            this.btn修改.Image = ((System.Drawing.Image)(resources.GetObject("btn修改.Image")));
            this.btn修改.Location = new System.Drawing.Point(184, 3);
            this.btn修改.Name = "btn修改";
            this.btn修改.Size = new System.Drawing.Size(75, 23);
            this.btn修改.TabIndex = 2;
            this.btn修改.Text = "修改";
            this.btn修改.Click += new System.EventHandler(this.btn修改_Click);
            // 
            // btn删除
            // 
            this.btn删除.Image = ((System.Drawing.Image)(resources.GetObject("btn删除.Image")));
            this.btn删除.Location = new System.Drawing.Point(265, 3);
            this.btn删除.Name = "btn删除";
            this.btn删除.Size = new System.Drawing.Size(75, 23);
            this.btn删除.TabIndex = 3;
            this.btn删除.Text = "删除";
            this.btn删除.Click += new System.EventHandler(this.btn删除_Click);
            // 
            // gcSummary
            // 
            this.gcSummary.AllowBandedGridColumnSort = false;
            this.gcSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSummary.IsBestFitColumns = true;
            this.gcSummary.Location = new System.Drawing.Point(0, 205);
            this.gcSummary.MainView = this.gvSummary;
            this.gcSummary.Name = "gcSummary";
            this.gcSummary.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.hlink档案编号,
            this.btn添加随访});
            this.gcSummary.ShowContextMenu = false;
            this.gcSummary.Size = new System.Drawing.Size(937, 188);
            this.gcSummary.StrWhere = "";
            this.gcSummary.TabIndex = 4;
            this.gcSummary.UseCheckBox = false;
            this.gcSummary.View = "";
            this.gcSummary.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSummary});
            // 
            // gvSummary
            // 
            this.gvSummary.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gvSummary.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gvSummary.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gvSummary.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.DetailTip.Options.UseFont = true;
            this.gvSummary.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Empty.Options.UseFont = true;
            this.gvSummary.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.EvenRow.Options.UseFont = true;
            this.gvSummary.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gvSummary.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FilterPanel.Options.UseFont = true;
            this.gvSummary.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FixedLine.Options.UseFont = true;
            this.gvSummary.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FocusedCell.Options.UseFont = true;
            this.gvSummary.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FocusedRow.Options.UseFont = true;
            this.gvSummary.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gvSummary.Appearance.FooterPanel.Options.UseFont = true;
            this.gvSummary.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupButton.Options.UseFont = true;
            this.gvSummary.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupFooter.Options.UseFont = true;
            this.gvSummary.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupPanel.Options.UseFont = true;
            this.gvSummary.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupRow.Options.UseFont = true;
            this.gvSummary.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvSummary.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gvSummary.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HorzLine.Options.UseFont = true;
            this.gvSummary.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.OddRow.Options.UseFont = true;
            this.gvSummary.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Preview.Options.UseFont = true;
            this.gvSummary.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Row.Options.UseFont = true;
            this.gvSummary.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.RowSeparator.Options.UseFont = true;
            this.gvSummary.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.SelectedRow.Options.UseFont = true;
            this.gvSummary.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.TopNewRow.Options.UseFont = true;
            this.gvSummary.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.VertLine.Options.UseFont = true;
            this.gvSummary.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ViewCaption.Options.UseFont = true;
            this.gvSummary.ColumnPanelRowHeight = 30;
            this.gvSummary.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn6,
            this.gridColumn4,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn3,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11});
            this.gvSummary.GridControl = this.gcSummary;
            this.gvSummary.GroupPanelText = "DragColumn";
            this.gvSummary.Name = "gvSummary";
            this.gvSummary.OptionsView.ColumnAutoWidth = false;
            this.gvSummary.OptionsView.EnableAppearanceEvenRow = true;
            this.gvSummary.OptionsView.EnableAppearanceOddRow = true;
            this.gvSummary.OptionsView.ShowGroupPanel = false;
            this.gvSummary.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn11, DevExpress.Data.ColumnSortOrder.Descending)});
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn1.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "档案号";
            this.gridColumn1.ColumnEdit = this.hlink档案编号;
            this.gridColumn1.FieldName = "个人档案编号";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 114;
            // 
            // hlink档案编号
            // 
            this.hlink档案编号.AutoHeight = false;
            this.hlink档案编号.Name = "hlink档案编号";
            this.hlink档案编号.Click += new System.EventHandler(this.hlink档案编号_Click);
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn2.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "姓名";
            this.gridColumn2.FieldName = "姓名";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 95;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn6.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn6.AppearanceHeader.Options.UseFont = true;
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "性别";
            this.gridColumn6.FieldName = "性别";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 2;
            this.gridColumn6.Width = 86;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn4.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "出生日期";
            this.gridColumn4.FieldName = "出生日期";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn7.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn7.AppearanceHeader.Options.UseFont = true;
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.Caption = "居住地址";
            this.gridColumn7.FieldName = "居住地址";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 4;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn8.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn8.AppearanceHeader.Options.UseFont = true;
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.Caption = "联系电话";
            this.gridColumn8.FieldName = "本人电话";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 5;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn3.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "身份证号";
            this.gridColumn3.FieldName = "身份证号";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 6;
            this.gridColumn3.Width = 86;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn9.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn9.AppearanceHeader.Options.UseFont = true;
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.Caption = "当前所属机构";
            this.gridColumn9.FieldName = "所属机构名称";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 7;
            this.gridColumn9.Width = 113;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn10.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn10.AppearanceHeader.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.Caption = "录入人";
            this.gridColumn10.FieldName = "创建人姓名";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 8;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn11.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn11.AppearanceHeader.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "录入时间";
            this.gridColumn11.FieldName = "创建时间";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 9;
            // 
            // btn添加随访
            // 
            this.btn添加随访.AutoHeight = false;
            this.btn添加随访.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btn添加随访.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::AtomEHR.公共卫生.Properties.Resources.add_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.btn添加随访.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btn添加随访.Name = "btn添加随访";
            this.btn添加随访.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btn添加随访.Click += new System.EventHandler(this.btn添加随访_Click);
            // 
            // gcFindGroup
            // 
            this.gcFindGroup.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.gcFindGroup.Appearance.Options.UseBackColor = true;
            this.gcFindGroup.Controls.Add(this.layoutControl1);
            this.gcFindGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.gcFindGroup.Location = new System.Drawing.Point(0, 0);
            this.gcFindGroup.Name = "gcFindGroup";
            this.gcFindGroup.Size = new System.Drawing.Size(937, 176);
            this.gcFindGroup.TabIndex = 5;
            // 
            // layoutControl1
            // 
            this.layoutControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.layoutControl1.Controls.Add(this.txt地址);
            this.layoutControl1.Controls.Add(this.txt录入时间2);
            this.layoutControl1.Controls.Add(this.txt录入时间1);
            this.layoutControl1.Controls.Add(this.txt出生日期2);
            this.layoutControl1.Controls.Add(this.txt出生日期1);
            this.layoutControl1.Controls.Add(this.ck含下属机构);
            this.layoutControl1.Controls.Add(this.txt机构);
            this.layoutControl1.Controls.Add(this.pictureBox3);
            this.layoutControl1.Controls.Add(this.txt录入人);
            this.layoutControl1.Controls.Add(this.txt管理组别);
            this.layoutControl1.Controls.Add(this.comboBoxEdit3);
            this.layoutControl1.Controls.Add(this.txt身份证);
            this.layoutControl1.Controls.Add(this.txt档案编号);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.txt职业);
            this.layoutControl1.Controls.Add(this.txt性别);
            this.layoutControl1.Controls.Add(this.comboBoxEdit5);
            this.layoutControl1.Controls.Add(this.txt档案状态);
            this.layoutControl1.Controls.Add(this.txt镇);
            this.layoutControl1.Controls.Add(this.txt村);
            this.layoutControl1.Controls.Add(this.txt合格档案);
            this.layoutControl1.Controls.Add(this.txt终止管理);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(276, 280, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup2;
            this.layoutControl1.Size = new System.Drawing.Size(933, 172);
            this.layoutControl1.TabIndex = 2;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txt地址
            // 
            this.txt地址.Location = new System.Drawing.Point(732, 123);
            this.txt地址.Name = "txt地址";
            this.txt地址.Size = new System.Drawing.Size(196, 20);
            this.txt地址.StyleController = this.layoutControl1;
            this.txt地址.TabIndex = 133;
            // 
            // txt录入时间2
            // 
            this.txt录入时间2.EditValue = null;
            this.txt录入时间2.Location = new System.Drawing.Point(266, 122);
            this.txt录入时间2.Name = "txt录入时间2";
            this.txt录入时间2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt录入时间2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt录入时间2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt录入时间2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt录入时间2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt录入时间2.Size = new System.Drawing.Size(110, 20);
            this.txt录入时间2.StyleController = this.layoutControl1;
            this.txt录入时间2.TabIndex = 136;
            // 
            // txt录入时间1
            // 
            this.txt录入时间1.EditValue = null;
            this.txt录入时间1.Location = new System.Drawing.Point(141, 122);
            this.txt录入时间1.Name = "txt录入时间1";
            this.txt录入时间1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt录入时间1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt录入时间1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt录入时间1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt录入时间1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt录入时间1.Size = new System.Drawing.Size(107, 20);
            this.txt录入时间1.StyleController = this.layoutControl1;
            this.txt录入时间1.TabIndex = 135;
            // 
            // txt出生日期2
            // 
            this.txt出生日期2.EditValue = null;
            this.txt出生日期2.Location = new System.Drawing.Point(266, 98);
            this.txt出生日期2.Name = "txt出生日期2";
            this.txt出生日期2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt出生日期2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt出生日期2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt出生日期2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt出生日期2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt出生日期2.Size = new System.Drawing.Size(110, 20);
            this.txt出生日期2.StyleController = this.layoutControl1;
            this.txt出生日期2.TabIndex = 134;
            // 
            // txt出生日期1
            // 
            this.txt出生日期1.EditValue = null;
            this.txt出生日期1.Location = new System.Drawing.Point(141, 98);
            this.txt出生日期1.Name = "txt出生日期1";
            this.txt出生日期1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt出生日期1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt出生日期1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt出生日期1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt出生日期1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt出生日期1.Size = new System.Drawing.Size(107, 20);
            this.txt出生日期1.StyleController = this.layoutControl1;
            this.txt出生日期1.TabIndex = 133;
            // 
            // ck含下属机构
            // 
            this.ck含下属机构.EditValue = true;
            this.ck含下属机构.Location = new System.Drawing.Point(292, 25);
            this.ck含下属机构.Name = "ck含下属机构";
            this.ck含下属机构.Properties.Caption = "含下属机构";
            this.ck含下属机构.Size = new System.Drawing.Size(84, 19);
            this.ck含下属机构.StyleController = this.layoutControl1;
            this.ck含下属机构.TabIndex = 132;
            // 
            // txt机构
            // 
            this.txt机构.Location = new System.Drawing.Point(141, 25);
            this.txt机构.Name = "txt机构";
            this.txt机构.Properties.AutoExpandAllNodes = false;
            this.txt机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt机构.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.txt机构.Size = new System.Drawing.Size(147, 20);
            this.txt机构.StyleController = this.layoutControl1;
            this.txt机构.TabIndex = 131;
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(0, 0);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = global::AtomEHR.公共卫生.Properties.Resources.zoom_in;
            this.pictureBox3.Location = new System.Drawing.Point(5, 25);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(67, 142);
            this.pictureBox3.TabIndex = 130;
            this.pictureBox3.TabStop = false;
            // 
            // txt录入人
            // 
            this.txt录入人.Location = new System.Drawing.Point(680, 98);
            this.txt录入人.Name = "txt录入人";
            this.txt录入人.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt录入人.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txt录入人.Size = new System.Drawing.Size(248, 20);
            this.txt录入人.StyleController = this.layoutControl1;
            this.txt录入人.TabIndex = 23;
            // 
            // txt管理组别
            // 
            this.txt管理组别.Location = new System.Drawing.Point(680, 73);
            this.txt管理组别.Name = "txt管理组别";
            this.txt管理组别.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt管理组别.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txt管理组别.Size = new System.Drawing.Size(248, 20);
            this.txt管理组别.StyleController = this.layoutControl1;
            this.txt管理组别.TabIndex = 22;
            // 
            // comboBoxEdit3
            // 
            this.comboBoxEdit3.Location = new System.Drawing.Point(680, 49);
            this.comboBoxEdit3.Name = "comboBoxEdit3";
            this.comboBoxEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit3.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit3.Size = new System.Drawing.Size(248, 20);
            this.comboBoxEdit3.StyleController = this.layoutControl1;
            this.comboBoxEdit3.TabIndex = 19;
            // 
            // txt身份证
            // 
            this.txt身份证.Location = new System.Drawing.Point(445, 49);
            this.txt身份证.Name = "txt身份证";
            this.txt身份证.Size = new System.Drawing.Size(166, 20);
            this.txt身份证.StyleController = this.layoutControl1;
            this.txt身份证.TabIndex = 14;
            // 
            // txt档案编号
            // 
            this.txt档案编号.Location = new System.Drawing.Point(141, 49);
            this.txt档案编号.Name = "txt档案编号";
            this.txt档案编号.Size = new System.Drawing.Size(235, 20);
            this.txt档案编号.StyleController = this.layoutControl1;
            this.txt档案编号.TabIndex = 11;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(445, 25);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(166, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 5;
            // 
            // txt职业
            // 
            this.txt职业.Location = new System.Drawing.Point(141, 73);
            this.txt职业.Name = "txt职业";
            this.txt职业.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt职业.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txt职业.Size = new System.Drawing.Size(235, 20);
            this.txt职业.StyleController = this.layoutControl1;
            this.txt职业.TabIndex = 20;
            // 
            // txt性别
            // 
            this.txt性别.Location = new System.Drawing.Point(680, 25);
            this.txt性别.Name = "txt性别";
            this.txt性别.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txt性别.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt性别.Properties.NullText = "请选择";
            this.txt性别.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txt性别.Size = new System.Drawing.Size(248, 20);
            this.txt性别.StyleController = this.layoutControl1;
            this.txt性别.TabIndex = 124;
            // 
            // comboBoxEdit5
            // 
            this.comboBoxEdit5.Location = new System.Drawing.Point(445, 73);
            this.comboBoxEdit5.Name = "comboBoxEdit5";
            this.comboBoxEdit5.Size = new System.Drawing.Size(166, 20);
            this.comboBoxEdit5.StyleController = this.layoutControl1;
            this.comboBoxEdit5.TabIndex = 21;
            // 
            // txt档案状态
            // 
            this.txt档案状态.Location = new System.Drawing.Point(445, 98);
            this.txt档案状态.Name = "txt档案状态";
            this.txt档案状态.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt档案状态.Size = new System.Drawing.Size(166, 20);
            this.txt档案状态.StyleController = this.layoutControl1;
            this.txt档案状态.TabIndex = 10;
            // 
            // txt镇
            // 
            this.txt镇.Location = new System.Drawing.Point(445, 123);
            this.txt镇.Name = "txt镇";
            this.txt镇.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txt镇.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt镇.Properties.NullText = "请选择";
            this.txt镇.Properties.PopupSizeable = true;
            this.txt镇.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txt镇.Size = new System.Drawing.Size(116, 20);
            this.txt镇.StyleController = this.layoutControl1;
            this.txt镇.TabIndex = 131;
            // 
            // txt村
            // 
            this.txt村.Location = new System.Drawing.Point(565, 123);
            this.txt村.Name = "txt村";
            this.txt村.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt村.Properties.NullText = "请选择居/村委会";
            this.txt村.Properties.PopupSizeable = true;
            this.txt村.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txt村.Size = new System.Drawing.Size(114, 20);
            this.txt村.StyleController = this.layoutControl1;
            this.txt村.TabIndex = 132;
            // 
            // txt合格档案
            // 
            this.txt合格档案.Location = new System.Drawing.Point(445, 147);
            this.txt合格档案.Name = "txt合格档案";
            this.txt合格档案.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txt合格档案.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt合格档案.Properties.NullText = "请选择...";
            this.txt合格档案.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txt合格档案.Size = new System.Drawing.Size(167, 20);
            this.txt合格档案.StyleController = this.layoutControl1;
            this.txt合格档案.TabIndex = 125;
            // 
            // txt终止管理
            // 
            this.txt终止管理.Location = new System.Drawing.Point(141, 146);
            this.txt终止管理.Name = "txt终止管理";
            this.txt终止管理.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txt终止管理.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt终止管理.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txt终止管理.Size = new System.Drawing.Size(235, 20);
            this.txt终止管理.StyleController = this.layoutControl1;
            this.txt终止管理.TabIndex = 124;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.OptionsItemText.TextToControlDistance = 1;
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(933, 172);
            this.layoutControlGroup2.Text = "Root";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "查询条件";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem11,
            this.layoutControlItem8,
            this.layoutControlItem10,
            this.layoutControlItem12,
            this.layoutControlItem14,
            this.layoutControlItem9,
            this.layoutControlItem7,
            this.layoutControlItem15,
            this.layoutControlItem71,
            this.layoutControlItem6,
            this.layoutControlItem1,
            this.layoutControlItem72,
            this.layoutControlItem4,
            this.layoutControlItem73,
            this.layoutControlItem16,
            this.emptySpaceItem1,
            this.layoutControlItem13,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem21,
            this.layoutControlItem22});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup2";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(933, 172);
            this.layoutControlGroup3.Text = "查询条件";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem2.Control = this.txt姓名;
            this.layoutControlItem2.CustomizationFormText = "姓名:";
            this.layoutControlItem2.Location = new System.Drawing.Point(375, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(235, 24);
            this.layoutControlItem2.Text = "姓名:";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem11.Control = this.txt身份证;
            this.layoutControlItem11.CustomizationFormText = "身份证号:";
            this.layoutControlItem11.Location = new System.Drawing.Point(375, 24);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(119, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(235, 24);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "身份证号:";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem8.Control = this.txt档案编号;
            this.layoutControlItem8.CustomizationFormText = "档案编号:";
            this.layoutControlItem8.Location = new System.Drawing.Point(71, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem8.Text = "档案编号:";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem10.Control = this.comboBoxEdit3;
            this.layoutControlItem10.CustomizationFormText = "片区：";
            this.layoutControlItem10.Location = new System.Drawing.Point(610, 24);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(119, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(317, 24);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "片区:";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem12.Control = this.txt职业;
            this.layoutControlItem12.CustomizationFormText = "职业:";
            this.layoutControlItem12.Location = new System.Drawing.Point(71, 48);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(304, 25);
            this.layoutControlItem12.Text = "职业:";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem14.Control = this.comboBoxEdit5;
            this.layoutControlItem14.CustomizationFormText = "医保号：";
            this.layoutControlItem14.Location = new System.Drawing.Point(375, 48);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(235, 25);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "医保号:";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem9.Control = this.txt管理组别;
            this.layoutControlItem9.CustomizationFormText = "管理组别：";
            this.layoutControlItem9.Location = new System.Drawing.Point(610, 48);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(119, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(317, 25);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "管理组别:";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem7.Control = this.txt档案状态;
            this.layoutControlItem7.CustomizationFormText = "档案状态：";
            this.layoutControlItem7.Location = new System.Drawing.Point(375, 73);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(235, 25);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "档案状态:";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem15.Control = this.txt录入人;
            this.layoutControlItem15.CustomizationFormText = "录入人：";
            this.layoutControlItem15.Location = new System.Drawing.Point(610, 73);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(119, 24);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(317, 25);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "录入人:";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem71
            // 
            this.layoutControlItem71.Control = this.pictureBox3;
            this.layoutControlItem71.CustomizationFormText = "layoutControlItem71";
            this.layoutControlItem71.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem71.MinSize = new System.Drawing.Size(66, 24);
            this.layoutControlItem71.Name = "layoutControlItem71";
            this.layoutControlItem71.Size = new System.Drawing.Size(71, 146);
            this.layoutControlItem71.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem71.Text = "layoutControlItem71";
            this.layoutControlItem71.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem71.TextToControlDistance = 0;
            this.layoutControlItem71.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem6.Control = this.txt机构;
            this.layoutControlItem6.CustomizationFormText = "区域:";
            this.layoutControlItem6.Location = new System.Drawing.Point(71, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(216, 24);
            this.layoutControlItem6.Text = "区域:";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.ck含下属机构;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(287, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 23);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(1, 23);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(88, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "包含下属机构";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem72
            // 
            this.layoutControlItem72.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem72.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem72.Control = this.txt出生日期1;
            this.layoutControlItem72.CustomizationFormText = "出生日期:";
            this.layoutControlItem72.Location = new System.Drawing.Point(71, 73);
            this.layoutControlItem72.Name = "layoutControlItem72";
            this.layoutControlItem72.Size = new System.Drawing.Size(176, 24);
            this.layoutControlItem72.Text = "出生日期:";
            this.layoutControlItem72.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem72.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem72.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txt出生日期2;
            this.layoutControlItem4.CustomizationFormText = "~";
            this.layoutControlItem4.Location = new System.Drawing.Point(247, 73);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(128, 24);
            this.layoutControlItem4.Text = "~";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(9, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem73
            // 
            this.layoutControlItem73.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem73.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem73.Control = this.txt录入时间1;
            this.layoutControlItem73.CustomizationFormText = "录入时间:";
            this.layoutControlItem73.Location = new System.Drawing.Point(71, 97);
            this.layoutControlItem73.Name = "layoutControlItem73";
            this.layoutControlItem73.Size = new System.Drawing.Size(176, 24);
            this.layoutControlItem73.Text = "录入时间:";
            this.layoutControlItem73.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem73.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem73.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.txt录入时间2;
            this.layoutControlItem16.CustomizationFormText = "~";
            this.layoutControlItem16.Location = new System.Drawing.Point(247, 97);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(128, 24);
            this.layoutControlItem16.Text = "~";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(9, 14);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(611, 122);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(316, 24);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem13.Control = this.txt镇;
            this.layoutControlItem13.CustomizationFormText = "居住地址:";
            this.layoutControlItem13.Location = new System.Drawing.Point(375, 98);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(185, 24);
            this.layoutControlItem13.Text = "居住地址:";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.txt村;
            this.layoutControlItem17.CustomizationFormText = "居委会";
            this.layoutControlItem17.Location = new System.Drawing.Point(560, 98);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(118, 24);
            this.layoutControlItem17.Text = "居委会";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.txt地址;
            this.layoutControlItem18.CustomizationFormText = "详细地址";
            this.layoutControlItem18.Location = new System.Drawing.Point(678, 98);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(249, 24);
            this.layoutControlItem18.Text = "详细地址";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(48, 14);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem19.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem19.Control = this.txt终止管理;
            this.layoutControlItem19.CustomizationFormText = "终止管理:";
            this.layoutControlItem19.Location = new System.Drawing.Point(71, 121);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(304, 25);
            this.layoutControlItem19.Text = "终止管理:";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem21.Control = this.txt合格档案;
            this.layoutControlItem21.CustomizationFormText = "合格档案:";
            this.layoutControlItem21.Location = new System.Drawing.Point(375, 122);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(236, 24);
            this.layoutControlItem21.Text = "合格档案:";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem21.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem22.Control = this.txt性别;
            this.layoutControlItem22.CustomizationFormText = "性 别:";
            this.layoutControlItem22.Location = new System.Drawing.Point(610, 0);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(317, 24);
            this.layoutControlItem22.Text = "性 别:";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // pagerControl1
            // 
            this.pagerControl1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pagerControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pagerControl1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pagerControl1.JumpText = "跳转";
            this.pagerControl1.Location = new System.Drawing.Point(0, 393);
            this.pagerControl1.MaximumSize = new System.Drawing.Size(0, 353);
            this.pagerControl1.Name = "pagerControl1";
            this.pagerControl1.PageIndex = 1;
            this.pagerControl1.PageSize = 15;
            this.pagerControl1.paramters = null;
            this.pagerControl1.RecordCount = 0;
            this.pagerControl1.Size = new System.Drawing.Size(937, 37);
            this.pagerControl1.SqlQuery = null;
            this.pagerControl1.TabIndex = 128;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem5.CustomizationFormText = "合格档案:";
            this.layoutControlItem5.Location = new System.Drawing.Point(372, 120);
            this.layoutControlItem5.Name = "layoutControlItem1";
            this.layoutControlItem5.Size = new System.Drawing.Size(253, 27);
            this.layoutControlItem5.Text = "合格档案:";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem20.CustomizationFormText = "合格档案:";
            this.layoutControlItem20.Location = new System.Drawing.Point(372, 120);
            this.layoutControlItem20.Name = "layoutControlItem1";
            this.layoutControlItem20.Size = new System.Drawing.Size(253, 27);
            this.layoutControlItem20.Text = "合格档案:";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // frm冠心病患者管理卡
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(943, 462);
            this.Name = "frm冠心病患者管理卡";
            this.Text = "冠心病患者管理卡";
            this.Load += new System.EventHandler(this.frm冠心病患者管理卡_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hlink档案编号)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn添加随访)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFindGroup)).EndInit();
            this.gcFindGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入时间2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入时间2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入时间1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入时间1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck含下属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt管理组别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt镇.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt村.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt合格档案.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止管理.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DataGridControl gcSummary;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSummary;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.SimpleButton btn删除;
        private DevExpress.XtraEditors.SimpleButton btn修改;
        private DevExpress.XtraEditors.SimpleButton btnEmpty;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit hlink档案编号;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btn添加随访;
        private DevExpress.XtraEditors.PanelControl gcFindGroup;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private DevExpress.XtraEditors.ComboBoxEdit txt录入人;
        private DevExpress.XtraEditors.ComboBoxEdit txt管理组别;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit3;
        private DevExpress.XtraEditors.TextEdit txt身份证;
        private DevExpress.XtraEditors.TextEdit txt档案编号;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem71;
        private TActionProject.PagerControl pagerControl1;
        private DevExpress.XtraEditors.TreeListLookUpEdit txt机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.CheckEdit ck含下属机构;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.DateEdit txt录入时间2;
        private DevExpress.XtraEditors.DateEdit txt录入时间1;
        private DevExpress.XtraEditors.DateEdit txt出生日期2;
        private DevExpress.XtraEditors.DateEdit txt出生日期1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem72;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem73;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraEditors.TextEdit txt地址;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraEditors.ComboBoxEdit txt职业;
        private DevExpress.XtraEditors.ComboBoxEdit txt性别;
        private DevExpress.XtraEditors.TextEdit comboBoxEdit5;
        private DevExpress.XtraEditors.ComboBoxEdit txt档案状态;
        private DevExpress.XtraEditors.ComboBoxEdit txt镇;
        private DevExpress.XtraEditors.ComboBoxEdit txt村;
        private DevExpress.XtraEditors.ComboBoxEdit txt合格档案;
        private DevExpress.XtraEditors.ComboBoxEdit txt终止管理;
    }
}