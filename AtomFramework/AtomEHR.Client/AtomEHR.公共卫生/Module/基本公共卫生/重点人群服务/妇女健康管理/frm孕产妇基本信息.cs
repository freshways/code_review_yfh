﻿using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.公共卫生.Module.个人健康;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.基本公共卫生.妇女健康管理
{
    public partial class frm孕产妇基本信息 : frmBaseBusinessForm
    {
        private string _strWhere = null;

        public frm孕产妇基本信息()
        {
            InitializeComponent();
        }

        private void frm妇女保健检查表_Load(object sender, EventArgs e)
        {
            _SummaryView = new DevGridView(gv孕妇一次随访);//每个业务窗体必需给这个变量赋值.
            frmGridCustomize.RegisterGrid(gv孕妇一次随访);
            InitView();
        }
        /// <summary>
        /// 初始化页面
        /// </summary>
        private void InitView()
        {
            this.pagerControl1.Height = 35;
            this.uc年龄区间.Txt1.Width = 60;
            this.uc年龄区间.Txt2.Width = 60;
            this.uc年龄区间.Txt1.Width = 60;
            this.uc年龄区间.Txt2.Width = 60;
            //util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, cbo性别, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案状态, cbo档案状态, "P_CODE", "P_DESC");
            cbo档案状态.SelectedIndex = 1;
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t是否, cbo合格档案, "P_CODE", "P_DESC");
            //为“镇”绑定信息
            DataView dv镇 = new DataView(DataDictCache.Cache.t地区信息);
            dv镇.RowFilter = "上级编码=371323";
            //DataBinder.BindingLookupEditDataSource(lookUpEdit镇, dv市.ToTable(), "地区名称", "地区编码");
            util.ControlsHelper.BindComboxData(dv镇.ToTable(), comboBoxEdit镇, "地区编码", "地区名称");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t是否, cbo合格档案, "P_CODE", "P_DESC");
            frmGridCustomize.RegisterGrid(gv孕妇一次随访);
            //为“所属机构"绑定信息
            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                treeListLookUpEdit机构.Properties.ValueMember = "机构编号";
                treeListLookUpEdit机构.Properties.DisplayMember = "机构名称";

                treeListLookUpEdit机构.Properties.TreeList.KeyFieldName = "机构编号";
                treeListLookUpEdit机构.Properties.TreeList.ParentFieldName = "上级机构";
                treeListLookUpEdit机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                treeListLookUpEdit机构.Properties.DataSource = dt所属机构;
                //treeListLookUpEdit机构.Properties.TreeList.ExpandAll();
                //treeListLookUpEdit机构.Properties.TreeList.CollapseAll();

                treeListLookUpEdit机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                treeListLookUpEdit机构.Text = Loginer.CurrentUser.所属机构;
            }
            if (Loginer.CurrentUser.所属机构.Length >= 12)
                treeListLookUpEdit机构.Properties.AutoExpandAllNodes = true;
            else { treeListLookUpEdit机构.Properties.AutoExpandAllNodes = false; }
            try
            {
                DataTable dt录入人 = _bllUser.GetUserNameAndIDBy机构用户(treeListLookUpEdit机构.EditValue.ToString());
                cbo录入人.Properties.DisplayMember = "UserName";
                cbo录入人.Properties.ValueMember = "用户编码";
                cbo录入人.Properties.DataSource = dt录入人;
            }
            catch
            {
                cbo录入人.Text = "请选择...";
            }
        }

        private void btn查询_Click(object sender, EventArgs e)
        {
            if (funCheck())
            {
                this.pagerControl1.InitControl();
                #region 查询条件

                _strWhere = string.Empty;
                string pgrid = this.treeListLookUpEdit机构.EditValue.ToString();
                if (this.txt姓名.Text.Trim() != "")
                {
                    _strWhere += " and (姓名 like '" + this.txt姓名.Text.Trim() + "%') ";
                }
                if (this.txt档案编号.Text.Trim() != "")
                {
                    _strWhere += " and 个人档案编号 ='" + this.txt档案编号.Text.Trim() + "'";
                }
                if (this.txt身份证号.Text.Trim() != "")
                {
                    _strWhere += " and 身份证号 ='" + this.txt身份证号.Text.Trim() + "'";
                }
                if (util.ControlsHelper.GetComboxKey(comboBoxEdit镇) != "")
                {
                    _strWhere += " and 街道 ='" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'";
                }
                if (util.ControlsHelper.GetComboxKey(comboBoxEdit村) != "")
                {
                    _strWhere += " and 居委会 ='" + util.ControlsHelper.GetComboxKey(comboBoxEdit村) + "'";
                }
                if (this.txt详细地址.Text.Trim() != "")
                {
                    _strWhere += " and [居住地址] like '" + this.txt详细地址.Text.Trim() + "%'";
                }

                if (this.uC出生日期.Dte1.Text.Trim() != "")
                {
                    _strWhere += " and [出生日期] >= '" + this.uC出生日期.Dte1.Text.Trim() + ",  00:00:00'";
                }
                if (this.uC出生日期.Dte2.Text.Trim() != "")
                {
                    _strWhere += " and [出生日期] <= '" + this.uC出生日期.Dte2.Text.Trim() + ", 23:59:59'";
                }

                if (util.ControlsHelper.GetComboxKey(this.cbo档案状态) != "")
                {
                    _strWhere += " and [档案状态] = '" + util.ControlsHelper.GetComboxKey(this.cbo档案状态) + "'";
                }
                #endregion

                if (this.chk含下属机构.Checked)//包含下属机构
                {
                    if (pgrid.Length == 12)
                    {
                        _strWhere += " and ([所属机构]=  '" + pgrid + "' or substring([所属机构],1,7)+'1'+substring([所属机构],9,7) like '" + pgrid + "%')";
                    }
                    else
                    {
                        _strWhere += " and [所属机构] like '" + pgrid + "%'";
                    }
                }
                else
                {
                    _strWhere += " and [所属机构] ='" + pgrid + "'";
                }
                if (this.cbo录入人.Text.Trim() != "" && this.cbo录入人.Text.Trim() != "请选择")
                {
                    _strWhere += " and [创建人] = '" + this.cbo录入人.EditValue.ToString() + "'";
                }
                if (this.uC录入时间.Dte1.Text.Trim() != "")
                {
                    _strWhere += " and [创建时间] >= '" + this.uC录入时间.Dte1.Text.Trim() + " 00:00:00'";
                }
                if (this.uC录入时间.Dte2.Text.Trim() != "")
                {
                    _strWhere += " and [创建时间] <= '" + this.uC录入时间.Dte2.Text.Trim() + " 23:59:59'";
                }

                if (util.ControlsHelper.GetComboxKey(this.cbo合格档案) != "")
                {
                    if (util.ControlsHelper.GetComboxKey(this.cbo合格档案) == "1")//合格 ， 缺项为0
                    {
                        _strWhere += " and [缺项] = '0'";
                    }
                    else
                    {
                        _strWhere += " and [缺项] <> '0'";
                    }
                }
                BindDataList();
            }
        }

        private void BindDataList()
        {
            if (string.IsNullOrWhiteSpace(_strWhere))
            {
                return;
            }
            this.gc孕妇一次随访.View = "vw_孕妇基本信息";
            this.gc孕妇一次随访.StrWhere = _strWhere;
            DataSet ds = this.pagerControl1.GetQueryResult("vw_孕妇基本信息", _strWhere);
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                ds.Tables[0].Rows[i]["所属机构"] = _BLL.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                ds.Tables[0].Rows[i]["创建人"] = _BLL.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                ds.Tables[0].Rows[i]["居住地址"] = _BLL.Return地区名称(ds.Tables[0].Rows[i]["街道"].ToString()) + _BLL.Return地区名称(ds.Tables[0].Rows[i]["居委会"].ToString()) + ds.Tables[0].Rows[i]["居住地址"];
            }
            this.gc孕妇一次随访.DataSource = ds.Tables[0];
            this.pagerControl1.DrawControl();
            this.gv孕妇一次随访.BestFitColumns();//列自适应宽度         
        }
        private bool funCheck()
        {
            if (this.uC出生日期.Dte1.DateTime > this.uC出生日期.Dte2.DateTime)
            {
                Msg.Warning("出生日期的结束日期必须大于开始时间！");
                this.uC出生日期.Dte2.Text = "";
                this.uC出生日期.Dte2.Focus();
                return false;
            }
            if (this.uC录入时间.Dte1.DateTime > this.uC录入时间.Dte2.DateTime)
            {
                Msg.Warning("录入时间的结束日期必须大于开始时间！");
                this.uC录入时间.Dte2.Text = "";
                this.uC录入时间.Dte2.Focus();
                return false;
            }
            if (this.uc年龄区间.Txt1.Text.Trim() != "" && this.uc年龄区间.Txt2.Text.Trim() != "")
            {
                if (Convert.ToInt32(this.uc年龄区间.Txt1.Text) < Convert.ToInt32(this.uc年龄区间.Txt2.Text))
                {
                    Msg.Warning("体检时间的结束日期必须大于开始时间！");
                    this.uc年龄区间.Txt2.Text = "";
                    this.uc年龄区间.Txt2.Focus();
                    return false;
                }
            }
            return true;
        }

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv孕妇一次随访.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            string id = row["ID"].ToString();
            frm个人健康 frm = new frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, id);
            frm.ShowDialog();
            //  frm.Show();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {

        }

        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            BindDataList();
        }

        private void treeListLookUpEdit机构_EditValueChanged(object sender, EventArgs e)
        {
            //绑定“录入人”绑定信息
            try
            {
                DataTable dt录入人 = _bllUser.GetUserNameAndIDBy机构用户(treeListLookUpEdit机构.EditValue.ToString());
                cbo录入人.Properties.DisplayMember = "UserName";
                cbo录入人.Properties.ValueMember = "用户编码";
                cbo录入人.Properties.DataSource = dt录入人;
            }
            catch
            {
                cbo录入人.Text = "请选择...";
            }
        }
    }
}
