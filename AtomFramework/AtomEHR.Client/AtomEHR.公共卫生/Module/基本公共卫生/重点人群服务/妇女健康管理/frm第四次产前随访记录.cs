﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Business.Security;
using AtomEHR.Models;
using AtomEHR.公共卫生.Module.个人健康;
using AtomEHR.Library;

namespace AtomEHR.公共卫生.Module.基本公共卫生.妇女健康管理
{
    public partial class frm第四次产前随访记录 : AtomEHR.Library.frmBaseBusinessForm
    {
        private string strWhere = null;
        public frm第四次产前随访记录()
        {
            InitializeComponent();
            this.pagerControl1.Height = 48;
        }

        private void textEdit年龄开始_EditValueChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textEdit年龄开始.Text))
            {
                dateEdit出生时间截止.Text = "";
                return;
            }

            DateTime dtNow = DateTime.Now;
            int age开始 = Convert.ToInt32(textEdit年龄开始.Text);

            if (string.IsNullOrWhiteSpace(textEdit年龄截止.Text))
            { }
            else
            {
                int age截止 = Convert.ToInt32(textEdit年龄截止.Text);
                if (age开始 > age截止)
                {
                    Msg.Warning("起始年龄应小于结束年龄");
                    textEdit年龄截止.Text = textEdit年龄开始.Text;
                    dateEdit出生时间开始.Text = DateTime.Now.AddYears(-1 * age开始 - 1).ToString("yyyy-MM-dd");
                }
            }

            dateEdit出生时间截止.Text = dtNow.AddYears(age开始 * -1).ToString("yyyy-MM-dd");
        }

        private void textEdit年龄截止_EditValueChanged(object sender, EventArgs e)
        {
            //如果截止年龄设置为空，则将出生日期的开始时间也设置为空
            if (string.IsNullOrWhiteSpace(textEdit年龄截止.Text))
            {
                dateEdit出生时间开始.Text = "";
                return;
            }

            DateTime dtNow = DateTime.Now;
            int age截止 = Convert.ToInt32(textEdit年龄截止.Text);

            //进行年龄范围的合法性验证：开始年龄<=截止年龄
            #region
            if (string.IsNullOrWhiteSpace(textEdit年龄开始.Text))
            {
            }
            else
            {
                //开始年龄不为空的情况下
                int age开始 = Convert.ToInt32(textEdit年龄开始.Text);
                if (age开始 > age截止)
                {
                    Msg.Warning("起始年龄应小于结束年龄");
                    textEdit年龄截止.Text = textEdit年龄开始.Text;
                    dateEdit出生时间开始.Text = DateTime.Now.AddYears(-1 * age开始 - 1).ToString("yyyy-MM-dd");
                    return;
                }
            }
            #endregion

            dateEdit出生时间开始.Text = dtNow.AddYears(age截止 * (-1) - 1).AddDays(1).ToString("yyyy-MM-dd");
        }

        private void dateEdit出生时间开始_EditValueChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(dateEdit出生时间开始.Text))//出生日期为空时，截止年龄设置为空
            {
                //this.textEdit年龄截止.Text = "";
                return;
            }

            DateTime dt出生日期开始 = Convert.ToDateTime(this.dateEdit出生时间开始.Text);
            DateTime dtNow = DateTime.Now;
            TimeSpan timeSpan时间差 = dtNow - dt出生日期开始;
            //如果所选时间大于当前时间（运行此程序的电脑的系统时间），则显示提示信息
            if (timeSpan时间差.TotalDays < 0)
            {
                Msg.Warning("起始时间不能大于当前时间");
                dateEdit出生时间开始.Text = dtNow.ToString("yyyy-MM-dd");
                //this.textEdit年龄截止.Text = "0";
                return; //必须返回，词句不可缺少
            }

            if (string.IsNullOrWhiteSpace(dateEdit出生时间截止.Text))
            {
                //计算截止年龄
            }
            else
            {
                //如果截止时间不是空的，则需要判断时间范围：
                DateTime dt截止时间 = Convert.ToDateTime(dateEdit出生时间截止.Text);
                TimeSpan timespan = dt出生日期开始 - dt截止时间;
                if (timespan.TotalDays > 0)
                {
                    dateEdit出生时间截止.Text = DateTime.Now.ToString("yyyy-MM-dd");
                    //textEdit年龄开始.Text = "0";
                    //然后，计算截止年龄
                }
                else
                {
                    //计算截止年龄
                }
            }

            //所选时间小于等于当前时间（运行此程序的电脑的系统时间）的情况下，则根据日期修改年龄的截止范围
            //double dYear = Math.Floor(timeSpan时间差.TotalDays / 365);

            ////判断日期是否满足  当前日期-（dYear+1）*1年 < 设定的出生开始日期 <= 当前日期-dYear*1年
            //DateTime dt1 = dtNow.AddYears(-1 * (int)dYear);
            //DateTime dt2 = dtNow.AddYears(-1 * (int)dYear -1);
            //if(DateTime.Compare(dt2,dt出生日期开始)< 0 && DateTime.Compare(dt出生日期开始,dt1)<=0 )
            //{
            //    textEdit年龄截止.Text = dYear.ToString();
            //}
            //else
            //{
            //    textEdit年龄截止.Text = (dYear - 1).ToString();
            //}
        }

        private void dateEdit出生时间截止_EditValueChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(dateEdit出生时间截止.Text))
            {
                //textEdit年龄开始.Text = "";
                return;
            }

            DateTime dt截止时间 = Convert.ToDateTime(dateEdit出生时间截止.Text);
            if (DateTime.Compare(dt截止时间, DateTime.Now) > 0)
            {
                Msg.Warning("截止时间不能大于当前日期");
                dateEdit出生时间截止.Text = DateTime.Now.ToString("yyyy-MM-dd");
                //textEdit年龄开始.Text = "0";
                return;
            }

            if (string.IsNullOrWhiteSpace(dateEdit出生时间开始.Text))
            {
                //直接设定年龄开始
                //GOTO: 下方开始年龄的设定
            }
            else
            {
                DateTime dt开始时间 = Convert.ToDateTime(dateEdit出生时间开始.Text);
                if (DateTime.Compare(dt开始时间, dt截止时间) <= 0)
                {
                    //出生日期的开始日期小于等于截止日期，直接设定开始年龄
                    //GOTO:下方开始年龄的设定
                }
                else
                {
                    //截止日期小于出生日期的开始日期
                    Msg.Warning("截止日期不能小于开始日期");
                    dateEdit出生时间截止.Text = DateTime.Now.ToString("yyyy-MM-dd");
                    //textEdit年龄开始.Text = "0";
                    return;
                }
            }

            //下方开始年龄的设定
            //判断日期是否满足  当前日期-（dYear+1）*1年 < 设定的出生截止日期 <= 当前日期-dYear*1年
            //TimeSpan timespan = DateTime.Now - dt截止时间;

            //double dYear = timespan.TotalDays / 365;

            //DateTime dt1 = DateTime.Now.AddYears(-1 * (int)dYear);
            //DateTime dt2 = DateTime.Now.AddYears(-1 * (int)dYear - 1);
            //if (DateTime.Compare(dt2, dt截止时间) < 0 && DateTime.Compare(dt截止时间, dt1) <= 0)
            //{
            //    textEdit年龄开始.Text = dYear.ToString();
            //}
            //else
            //{
            //    textEdit年龄开始.Text = (dYear - 1).ToString();
            //}
        }

        private void lookUpEdit镇_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv村 = new DataView(DataDictCache.Cache.t地区信息);
                dv村.RowFilter = "上级编码='" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'";
                //DataBinder.BindingLookupEditDataSource(lookUpEdit村, dv县.ToTable(), "地区名称", "地区编码");
                util.ControlsHelper.BindComboxData(dv村.ToTable(), comboBoxEdit村, "地区编码", "地区名称");
                util.ControlsHelper.SetComboxNullData("", comboBoxEdit村, "请选择居/村委会");
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }

        private void sbtn重置_Click(object sender, EventArgs e)
        {
            //暂时不对“含下属机构”做修改
            //checkEdit含下属机构.Checked = true;

            treeListLookUpEdit机构.EditValue = Loginer.CurrentUser.所属机构;

            textEdit姓名.Text = "";
            //comboBoxEdit性别.EditValue = null;

            dateEdit出生时间开始.Text = "";
            dateEdit出生时间截止.Text = "";
            combBoxEdit档案状态.Text = "活动";
            lookUpEdit录入人.EditValue = "";

            dateEdit录入时间开始.Text = "";
            dateEdit录入时间截止.Text = "";
            textEdit档案编号.Text = "";
            textEdit身份证号.Text = "";

            textEdit年龄开始.Text = "";
            textEdit年龄截止.Text = "";
            comboBoxEdit是否合格.Text = "请选择";

            comboBoxEdit镇.EditValue = null;
        }

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            DataRow row = this.gridView1.GetFocusedDataRow();
            if (row == null)
            {
                Msg.ShowInformation("没有选择任何行");
                return;
            }
            else
            {
                string 家庭档案编号 = row["家庭档案编号"] as string;
                string 个人档案编号 = row["个人档案编号"] as string;
                string ID = row["ID"] as string;
                //_BLL.GetBusinessByKey(个人档案编号, true);//下载一个空业务单据            
                //_BLL.NewBusiness(); //增加一条主表记录
                frm个人健康 frm = new frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, ID);
                frm.ShowDialog();
            }
        }

        private void textEdit档案编号_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textEdit档案编号.Text))
            {
                return;
            }

            int len = this.textEdit档案编号.Text.Length;
            if (len < 17)
            {
                Msg.ShowInformation("请输入一个长度最少是17位的编号");
                textEdit档案编号.Focus();
            }
            else if (len > 18)
            {
                Msg.ShowInformation("请输入一个长度最多是18位的编号");
                textEdit档案编号.Focus();
            }

        }

        private void textEdit身份证号_Leave(object sender, EventArgs e)
        {
            //身份证验证
            if (string.IsNullOrWhiteSpace(textEdit身份证号.Text))
            {
            }
            else
            {
                bool bRet = util.ControlsHelper.Check身份证号(textEdit身份证号.Text);
                if (bRet == false)
                {
                    Msg.ShowInformation("请输入一个合法的身份证号！");
                    textEdit身份证号.Focus();
                }
            }
        }

        private void dateEdit录入时间开始_EditValueChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(dateEdit录入时间开始.Text))
            { }
            else
            {
                if (string.IsNullOrWhiteSpace(dateEdit录入时间截止.Text))
                { }
                else
                {
                    try
                    {
                        DateTime dtime录入开始 = Convert.ToDateTime(dateEdit录入时间开始.Text);
                        DateTime dtime录入截止 = Convert.ToDateTime(dateEdit录入时间截止.Text);
                        if ((dtime录入开始 - dtime录入截止).TotalDays > 0)
                        {
                            Msg.Warning("输入的日期大于当前日期！请重新输入！");
                            dateEdit录入时间开始.Text = "";
                        }
                    }
                    catch
                    {

                    }
                }
            }
        }

        private void dateEdit录入时间截止_EditValueChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(dateEdit录入时间截止.Text))
            { }
            else
            {
                if (string.IsNullOrWhiteSpace(dateEdit录入时间开始.Text))
                { }
                else
                {
                    //录入时间的结束日期必须大于录入时间的开始日期！
                    try
                    {
                        DateTime dtime录入开始 = Convert.ToDateTime(dateEdit录入时间开始.Text);
                        DateTime dtime录入截止 = Convert.ToDateTime(dateEdit录入时间截止.Text);
                        if ((dtime录入开始 - dtime录入截止).TotalDays > 0)
                        {
                            Msg.Warning("录入时间的结束日期必须大于录入时间的开始日期！");
                            dateEdit录入时间截止.Text = "";
                        }
                    }
                    catch
                    {

                    }
                }
            }
        }

        private void frm第四次产前随访记录_Load(object sender, EventArgs e)
        {
            base.InitButtonsBase();
            comboBoxEdit是否合格.EditValue = null;
            this.pagerControl1.Height = 35;
            //为“镇”绑定信息
            DataView dv镇 = new DataView(DataDictCache.Cache.t地区信息);
            dv镇.RowFilter = "上级编码=371323";
            //DataBinder.BindingLookupEditDataSource(lookUpEdit镇, dv市.ToTable(), "地区名称", "地区编码");
            util.ControlsHelper.BindComboxData(dv镇.ToTable(), comboBoxEdit镇, "地区编码", "地区名称");
            frmGridCustomize.RegisterGrid(gridView1);
            //为“所属机构"绑定信息
            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                treeListLookUpEdit机构.Properties.ValueMember = "机构编号";
                treeListLookUpEdit机构.Properties.DisplayMember = "机构名称";

                treeListLookUpEdit机构.Properties.TreeList.KeyFieldName = "机构编号";
                treeListLookUpEdit机构.Properties.TreeList.ParentFieldName = "上级机构";
                treeListLookUpEdit机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                treeListLookUpEdit机构.Properties.DataSource = dt所属机构;
                //treeListLookUpEdit机构.Properties.TreeList.ExpandAll();
                //treeListLookUpEdit机构.Properties.TreeList.CollapseAll();

                treeListLookUpEdit机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                treeListLookUpEdit机构.Text = Loginer.CurrentUser.所属机构;
            }

            if (Loginer.CurrentUser.所属机构.Length >= 12)
            {
                treeListLookUpEdit机构.Properties.AutoExpandAllNodes = true;
            }
            else
            {
                treeListLookUpEdit机构.Properties.AutoExpandAllNodes = false;
            }

            //绑定“录入人”绑定信息
            try
            {
                DataTable dt录入人 = new bllUser().GetUserNameAndIDBy机构用户(treeListLookUpEdit机构.EditValue.ToString());
                lookUpEdit录入人.Properties.DisplayMember = "UserName";
                lookUpEdit录入人.Properties.ValueMember = "用户编码";
                lookUpEdit录入人.Properties.DataSource = dt录入人;
            }
            catch
            {
                lookUpEdit录入人.Text = "请选择...";
            }
        }

        private void sbtn查询_Click(object sender, EventArgs e)
        {
            this.pagerControl1.InitControl();
            strWhere = GetSqlWhere();
            BindDataList();
        }

        private string GetSqlWhere()
        {
            string _strWhere = "";

            if (this.textEdit姓名.Text.Trim() != "")
            {
                _strWhere += " and (姓名 like '" + this.textEdit姓名.Text.Trim() + "%') ";
            }
            if (this.textEdit档案编号.Text.Trim() != "")
            {
                _strWhere += " and 个人档案编号 ='" + this.textEdit档案编号.Text.Trim() + "'";
            }
            if (this.textEdit身份证号.Text.Trim() != "")
            {
                _strWhere += " and 身份证号 ='" + this.textEdit身份证号.Text.Trim() + "'";
            }

            string str镇 = util.ControlsHelper.GetComboxKey(comboBoxEdit镇);
            if (string.IsNullOrWhiteSpace(str镇))
            { }
            else
            {
                _strWhere += " and 街道 ='" + str镇 + "'";
            }
            string str村 = util.ControlsHelper.GetComboxKey(comboBoxEdit村);
            if (string.IsNullOrWhiteSpace(str村))
            { }
            else
            {
                _strWhere += " and [居委会] ='" + str村 + "'";
            }
            if (this.textEdit详细地址.Text.Trim() != "")
            {
                _strWhere += " and [居住地址] like '" + this.textEdit详细地址.Text.Trim() + "%'";
            }

            if (this.dateEdit出生时间开始.Text != "")
            {
                _strWhere += " and [出生日期] >= '" + this.dateEdit出生时间开始.Text.Trim() + " 00:00:00'";
            }
            if (this.dateEdit出生时间截止.Text.Trim() != "")
            {
                _strWhere += " and [出生日期] <= '" + this.dateEdit出生时间截止.Text.Trim() + " 23:59:59'";
            }

            string str档案状态 = this.combBoxEdit档案状态.Text;
            if ("活动".Equals(str档案状态))
            {
                _strWhere += " and [档案状态] = '1' ";
            }

            string pgrid = this.treeListLookUpEdit机构.EditValue.ToString();
            if (this.checkEdit含下属机构.Checked)//包含下属机构
            {
                if (pgrid.Length == 12)
                {
                    _strWhere += " and ([所属机构]=  '" + pgrid + "' or substring([所属机构],1,7)+'1'+substring([所属机构],9,7) like '" + pgrid + "%')";
                }
                else
                {
                    _strWhere += " and [所属机构] like '" + pgrid + "%'";
                }
            }
            else
            {
                _strWhere += " and [所属机构] ='" + pgrid + "'";
            }


            if (this.lookUpEdit录入人.EditValue == null || string.IsNullOrWhiteSpace(this.lookUpEdit录入人.EditValue.ToString()))
            { }
            else
            {
                _strWhere += " and [创建人] = '" + this.lookUpEdit录入人.EditValue.ToString() + "'";
            }
            if (this.dateEdit录入时间开始.Text.Trim() != "")
            {
                _strWhere += " and [创建时间] >= '" + this.dateEdit录入时间开始.Text.Trim() + " 00:00:00'";
            }
            if (this.dateEdit录入时间截止.Text.Trim() != "")
            {
                _strWhere += " and [创建时间] <= '" + this.dateEdit录入时间截止.Text.Trim() + " 23:59:59'";
            }

            string str是否合格 = this.comboBoxEdit是否合格.Text;
            if (str是否合格 != "" && str是否合格 != "请选择")
            {
                if (str是否合格 == "是")//合格 ， 缺项为0
                {
                    _strWhere += " and [缺项] = '0'";
                }
                else if (str是否合格 == "否")
                {
                    _strWhere += " and [缺项] <> '0'";
                }
            }

            if (string.IsNullOrWhiteSpace(cbo怀孕状态.Text))
            {
            }
            else
            {
                _strWhere += " and 怀孕状态='" + cbo怀孕状态.Text + "'";
            }

            if (!string.IsNullOrWhiteSpace(txt孕周Begin.Text) && !string.IsNullOrWhiteSpace(txt孕周End.Text))
            {
                _strWhere += " and exists(SELECT 1 FROM [dbo].[tb_孕妇_产前随访1次] yc1c where yc1c.个人档案编号=vw_孕妇产前4次随访.个人档案编号 and yc1c.孕次<>'' and cast(yc1c.孕次 as int) = cast(vw_孕妇产前4次随访.孕次 as int) and yc1c.孕周<>'' and cast(yc1c.孕周 as int) >= " + txt孕周Begin.Text + " and cast(yc1c.孕周 as int) < " + txt孕周End.Text + ")";
            }
            else if (!string.IsNullOrWhiteSpace(txt孕周Begin.Text))
            {
                _strWhere += " and exists(SELECT 1 FROM [dbo].[tb_孕妇_产前随访1次] yc1c where yc1c.个人档案编号=vw_孕妇产前4次随访.个人档案编号 and yc1c.孕次<>'' and cast(yc1c.孕次 as int) = cast(vw_孕妇产前4次随访.孕次 as int) and yc1c.孕周<>'' and cast(yc1c.孕周 as int) >= " + txt孕周Begin.Text + ")";
            }
            else if (!string.IsNullOrWhiteSpace(txt孕周End.Text))
            {
                _strWhere += " and exists(SELECT 1 FROM [dbo].[tb_孕妇_产前随访1次] yc1c where yc1c.个人档案编号=vw_孕妇产前4次随访.个人档案编号 and yc1c.孕次<>'' and cast(yc1c.孕次 as int) = cast(vw_孕妇产前4次随访.孕次 as int) and yc1c.孕周<>'' and cast(yc1c.孕周 as int) < " + txt孕周End.Text + ")";
            }
            else
            {
            }

            return _strWhere;
        }

        private void BindDataList()
        {
            if (string.IsNullOrWhiteSpace(strWhere))
            {
                return;
            }
            DataSet ds = this.pagerControl1.GetQueryResult("vw_孕妇产前4次随访", strWhere);
            this.dataGridControl1.StrWhere = strWhere;
            this.dataGridControl1.View = "vw_孕妇产前4次随访";
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                ds.Tables[0].Rows[i]["所属机构名称"] = _BLL.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                ds.Tables[0].Rows[i]["创建人"] = _BLL.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                ds.Tables[0].Rows[i]["居住地址"] = _BLL.Return地区名称(ds.Tables[0].Rows[i]["街道"].ToString()) + _BLL.Return地区名称(ds.Tables[0].Rows[i]["居委会"].ToString()) + ds.Tables[0].Rows[i]["居住地址"];
            }
            this.dataGridControl1.DataSource = ds.Tables[0];
            this.pagerControl1.DrawControl();
            this.gridView1.BestFitColumns();//列自适应宽度
        }

        private void sbtn修改_Click(object sender, EventArgs e)
        {

        }

        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            BindDataList();
        }

        private void treeListLookUpEdit机构_EditValueChanged(object sender, EventArgs e)
        {
            //绑定“录入人”绑定信息
            try
            {
                DataTable dt录入人 = new bllUser().GetUserNameAndIDBy机构用户(treeListLookUpEdit机构.EditValue.ToString());
                lookUpEdit录入人.Properties.DisplayMember = "UserName";
                lookUpEdit录入人.Properties.ValueMember = "用户编码";
                lookUpEdit录入人.Properties.DataSource = dt录入人;
            }
            catch
            {
                lookUpEdit录入人.Text = "请选择...";
            }
        }

        private void btn微信推送_Click(object sender, EventArgs e)
        {
            try
            {
                string rows = "";
                for (int i = 0; i < this.gridView1.GetSelectedRows().Length; i++)
                {
                    DataRowView dr = this.gridView1.GetRow(this.gridView1.GetSelectedRows()[i]) as DataRowView;

                    if (dr == null) return;
                    string 身份证号 = dr["身份证号"] as string;
                    string 体检日期 = dr["填表日期"] as string;
                    string 姓名 = dr["姓名"] as string;
                    string[] keyWord = new string[2];
                    keyWord[0] = "您的第4次产检已完成！祝您身体健康，生活愉快。";
                    keyWord[1] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    if (!string.IsNullOrEmpty(身份证号))
                        _BLL.SendWxMessage("yFwL5QocGuRn9jknf0_9vNXoAYYnKNxy361Vle0MiWc"
                        , 身份证号, "ycfSearch%2fcq4", "尊敬的[" + 姓名 + "]您好！", "点击详情查看", 体检日期, keyWord);
                    rows = (i + 1).ToString();
                }
                Msg.ShowInformation("本次推送" + rows + "条体检数据!");
            }
            catch
            {

            }
        }
    }
}
