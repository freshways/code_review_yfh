﻿using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.公共卫生.Module.个人健康;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.基本公共卫生.妇女健康管理
{
    public partial class frm产后访视情况 : frmBaseBusinessForm
    {
        private string _strWhere;

        public frm产后访视情况()
        {
            InitializeComponent();
        }

        private void frm产后访视情况_Load(object sender, EventArgs e)
        {
            InitView();
            this.pagerControl1.Height = 35;
        }
        /// <summary>
        /// 初始化页面
        /// </summary>
        private void InitView()
        {
            this.pagerControl1.Height = 35;
            this.uc年龄区间.Txt1.Width = 60;
            this.uc年龄区间.Txt2.Width = 60;
            this.uc年龄区间.Txt1.Width = 60;
            this.uc年龄区间.Txt2.Width = 60;
            //util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, cbo性别, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案类别, cbo档案类别, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案状态, cbo档案状态, "P_CODE", "P_DESC");
            cbo档案状态.SelectedIndex = 1;
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t是否, cbo合格档案, "P_CODE", "P_DESC");
            //为“镇”绑定信息
            DataView dv镇 = new DataView(DataDictCache.Cache.t地区信息);
            dv镇.RowFilter = "上级编码='" + Loginer.CurrentUser.单位代码 + "' ";
            //DataBinder.BindingLookupEditDataSource(lookUpEdit镇, dv市.ToTable(), "地区名称", "地区编码");
            util.ControlsHelper.BindComboxData(dv镇.ToTable(), comboBoxEdit镇, "地区编码", "地区名称");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t是否, cbo合格档案, "P_CODE", "P_DESC");
            frmGridCustomize.RegisterGrid(gv产后访视);
            //为“所属机构"绑定信息
            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                treeListLookUpEdit机构.Properties.ValueMember = "机构编号";
                treeListLookUpEdit机构.Properties.DisplayMember = "机构名称";

                treeListLookUpEdit机构.Properties.TreeList.KeyFieldName = "机构编号";
                treeListLookUpEdit机构.Properties.TreeList.ParentFieldName = "上级机构";
                treeListLookUpEdit机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                treeListLookUpEdit机构.Properties.DataSource = dt所属机构;
                //treeListLookUpEdit机构.Properties.TreeList.ExpandAll();
                //treeListLookUpEdit机构.Properties.TreeList.CollapseAll();

                treeListLookUpEdit机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                treeListLookUpEdit机构.Text = Loginer.CurrentUser.所属机构;
            }
            if (Loginer.CurrentUser.所属机构.Length >= 12)
                treeListLookUpEdit机构.Properties.AutoExpandAllNodes = true;
            else { treeListLookUpEdit机构.Properties.AutoExpandAllNodes = false; }
            try
            {
                DataTable dt录入人 = _bllUser.GetUserNameAndIDBy机构用户(treeListLookUpEdit机构.EditValue.ToString());
                cbo录入人.Properties.DisplayMember = "UserName";
                cbo录入人.Properties.ValueMember = "用户编码";
                cbo录入人.Properties.DataSource = dt录入人;
            }
            catch
            {
                cbo录入人.Text = "请选择...";
            }
        }

        private void btn查询_Click(object sender, EventArgs e)
        {
            if (funCheck())
            {
                this.pagerControl1.InitControl();
                #region 查询条件

                _strWhere = string.Empty;
                string pgrid = this.treeListLookUpEdit机构.EditValue.ToString();
                if (this.txt姓名.Text.Trim() != "")
                {
                    _strWhere += " and (姓名 like '" + this.txt姓名.Text.Trim() + "%')";
                }
                if (this.txt档案编号.Text.Trim() != "")
                {
                    _strWhere += " and 个人档案编号 ='" + this.txt档案编号.Text.Trim() + "'";
                }
                if (this.txt身份证号.Text.Trim() != "")
                {
                    _strWhere += " and 身份证号 ='" + this.txt身份证号.Text.Trim() + "'";
                }
                if (util.ControlsHelper.GetComboxKey(comboBoxEdit镇) != "")
                {
                    _strWhere += " and 街道 ='" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'";
                }
                if (util.ControlsHelper.GetComboxKey(comboBoxEdit村) != "")
                {
                    _strWhere += " and 居委会 ='" + util.ControlsHelper.GetComboxKey(comboBoxEdit村) + "'";
                }
                if (this.txt详细地址.Text.Trim() != "")
                {
                    _strWhere += " and [居住地址] like '" + this.txt详细地址.Text.Trim() + "%'";
                }

                if (this.uC随访日期.Dte1.Text.Trim() != "")
                {
                    _strWhere += " and [随访日期] >= '" + this.uC随访日期.Dte1.Text.Trim() + "'";
                }
                if (this.uC随访日期.Dte2.Text.Trim() != "")
                {
                    _strWhere += " and [随访日期] <= '" + this.uC随访日期.Dte2.Text.Trim() + "'";
                }

                if (this.uc分娩日期.Dte1.Text.Trim() != "")
                {
                    _strWhere += " and [分娩日期] >= '" + this.uc分娩日期.Dte1.Text.Trim() + "'";
                }
                if (this.uc分娩日期.Dte2.Text.Trim() != "")
                {
                    _strWhere += " and [分娩日期] <= '" + this.uc分娩日期.Dte2.Text.Trim() + "'";
                }
                if (chk失访.Checked)
                {
                    _strWhere += " and [失访情况] = '1'";
                }
                if (chk未失访.Checked)
                {
                    _strWhere += " and isnull([失访情况],'') <> '1'";                    
                }
                if (util.ControlsHelper.GetComboxKey(this.cbo档案状态) != "")
                {
                    _strWhere += " and [档案状态] = '" + util.ControlsHelper.GetComboxKey(this.cbo档案状态) + "'";
                }
                if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(cbo档案类别)))
                {
                    _strWhere += " and 档案类别 ='" + util.ControlsHelper.GetComboxKey(cbo档案类别) + "'  ";
                }
                #endregion

                if (this.chk含下属机构.Checked)//包含下属机构
                {
                    if (pgrid.Length == 12)
                    {
                        _strWhere += " and ([所属机构]=  '" + pgrid + "' or substring([所属机构],1,7)+'1'+substring([所属机构],9,7) like '" + pgrid + "%')";
                    }
                    else
                    {
                        _strWhere += " and [所属机构] like '" + pgrid + "%'";
                    }
                }
                else
                {
                    _strWhere += " and [所属机构] ='" + pgrid + "'";
                }
                if (this.cbo录入人.Text.Trim() != "")
                {
                    _strWhere += " and [创建人] = '" + this.cbo录入人.EditValue.ToString() + "'";
                }
                if (this.uC录入时间.Dte1.Text.Trim() != "")
                {
                    _strWhere += " and [创建时间] >= '" + this.uC录入时间.Dte1.Text.Trim() + " 00:00:00'";
                }
                if (this.uC录入时间.Dte2.Text.Trim() != "")
                {
                    _strWhere += " and [创建时间] <= '" + this.uC录入时间.Dte2.Text.Trim() + " 23:59:59'";
                }

                if (util.ControlsHelper.GetComboxKey(this.cbo合格档案) != "")
                {
                    if (util.ControlsHelper.GetComboxKey(this.cbo合格档案) == "1")//合格 ， 缺项为0
                    {
                        _strWhere += " and [缺项] = '0'";
                    }
                    else
                    {
                        _strWhere += " and [缺项] <> '0'";
                    }
                }

                if(!string.IsNullOrWhiteSpace(txt孕周Begin.Text) && !string.IsNullOrWhiteSpace(txt孕周End.Text))
                {
                    _strWhere += " and exists(SELECT 1 FROM [dbo].[tb_孕妇_产前随访1次] yc1c where yc1c.个人档案编号=vw_孕妇产后访视情况表.个人档案编号 and yc1c.孕次<>'' and cast(yc1c.孕次 as int) = cast(vw_孕妇产后访视情况表.孕次 as int) and yc1c.孕周<>'' and cast(yc1c.孕周 as int) >= " + txt孕周Begin.Text + " and cast(yc1c.孕周 as int) < " + txt孕周End.Text + ")";
                }
                else if (!string.IsNullOrWhiteSpace(txt孕周Begin.Text))
                {
                    _strWhere += " and exists(SELECT 1 FROM [dbo].[tb_孕妇_产前随访1次] yc1c where yc1c.个人档案编号=vw_孕妇产后访视情况表.个人档案编号 and yc1c.孕次<>'' and cast(yc1c.孕次 as int) = cast(vw_孕妇产后访视情况表.孕次 as int) and yc1c.孕周<>'' and cast(yc1c.孕周 as int) >= " + txt孕周Begin.Text + ")";
                }
                else if (!string.IsNullOrWhiteSpace(txt孕周End.Text))
                {
                    _strWhere += " and exists(SELECT 1 FROM [dbo].[tb_孕妇_产前随访1次] yc1c where yc1c.个人档案编号=vw_孕妇产后访视情况表.个人档案编号 and yc1c.孕次<>'' and cast(yc1c.孕次 as int) = cast(vw_孕妇产后访视情况表.孕次 as int) and yc1c.孕周<>'' and cast(yc1c.孕周 as int) < " + txt孕周End.Text + ")";
                }
                else{
                }

                BindDataList();
            }
        }

        private void BindDataList()
        {
            this.gc产后访视.View = "vw_孕妇产后访视情况表";
            this.gc产后访视.StrWhere = _strWhere;
            DataSet ds = this.pagerControl1.GetQueryResult("vw_孕妇产后访视情况表", _strWhere);
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                ds.Tables[0].Rows[i]["所属机构"] = _BLL.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                ds.Tables[0].Rows[i]["创建人"] = _BLL.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                ds.Tables[0].Rows[i]["居住地址"] = _BLL.Return地区名称(ds.Tables[0].Rows[i]["街道"].ToString()) + _BLL.Return地区名称(ds.Tables[0].Rows[i]["居委会"].ToString()) + ds.Tables[0].Rows[i]["居住地址"];
            }
            this.gc产后访视.DataSource = ds.Tables[0];
            this.pagerControl1.DrawControl();
            this.gv产后访视.BestFitColumns();//列自适应宽度         
        }
        private bool funCheck()
        {
            if (this.uC随访日期.Dte1.DateTime > this.uC随访日期.Dte2.DateTime)
            {
                Msg.Warning("出生日期的结束日期必须大于开始时间！");
                this.uC随访日期.Dte2.Text = "";
                this.uC随访日期.Dte2.Focus();
                return false;
            }
            if (this.uC录入时间.Dte1.DateTime > this.uC录入时间.Dte2.DateTime)
            {
                Msg.Warning("录入时间的结束日期必须大于开始时间！");
                this.uC录入时间.Dte2.Text = "";
                this.uC录入时间.Dte2.Focus();
                return false;
            }
            if (this.uc年龄区间.Txt1.Text.Trim() != "" && this.uc年龄区间.Txt2.Text.Trim() != "")
            {
                if (Convert.ToInt32(this.uc年龄区间.Txt1.Text) < Convert.ToInt32(this.uc年龄区间.Txt2.Text))
                {
                    Msg.Warning("体检时间的结束日期必须大于开始时间！");
                    this.uc年龄区间.Txt2.Text = "";
                    this.uc年龄区间.Txt2.Focus();
                    return false;
                }
            }
            return true;
        }

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv产后访视.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            string id = row["ID"].ToString();
            frm个人健康 frm = new frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, id);
            frm.ShowDialog();
        }

        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            BindDataList();
        }

        private void comboBoxEdit镇_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv村 = new DataView(DataDictCache.Cache.t地区信息);
                dv村.RowFilter = "上级编码='" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'";
                //DataBinder.BindingLookupEditDataSource(lookUpEdit村, dv县.ToTable(), "地区名称", "地区编码");
                util.ControlsHelper.BindComboxData(dv村.ToTable(), comboBoxEdit村, "地区编码", "地区名称");
                util.ControlsHelper.SetComboxNullData("", comboBoxEdit村, "请选择居/村委会");
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }

        private void treeListLookUpEdit机构_EditValueChanged(object sender, EventArgs e)
        {
            //绑定“录入人”绑定信息
            try
            {
                DataTable dt录入人 = _bllUser.GetUserNameAndIDBy机构用户(treeListLookUpEdit机构.EditValue.ToString());
                cbo录入人.Properties.DisplayMember = "UserName";
                cbo录入人.Properties.ValueMember = "用户编码";
                cbo录入人.Properties.DataSource = dt录入人;
            }
            catch (Exception ex)
            {
                cbo录入人.Text = "请选择...";
            }
        }

        private void btn微信推送_Click(object sender, EventArgs e)
        {
            try
            {
                string rows = "";
                for (int i = 0; i < this.gv产后访视.GetSelectedRows().Length; i++)
                {
                    DataRowView dr = this.gv产后访视.GetRow(this.gv产后访视.GetSelectedRows()[i]) as DataRowView;

                    if (dr == null) return;
                    string 身份证号 = dr["身份证号"] as string;
                    string 体检日期 = dr["填表日期"] as string;
                    string 姓名 = dr["姓名"] as string;
                    string[] keyWord = new string[2];
                    keyWord[0] = "您的产后访视信息已完成！祝您身体健康，生活愉快。";
                    keyWord[1] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    if (!string.IsNullOrEmpty(身份证号))
                        _BLL.SendWxMessage("yFwL5QocGuRn9jknf0_9vNXoAYYnKNxy361Vle0MiWc"
                        , 身份证号, "ycfCh%2fch", "尊敬的[" + 姓名 + "]您好！", "点击详情查看", 体检日期, keyWord);
                    rows = (i + 1).ToString();
                }
                Msg.ShowInformation("本次推送" + rows + "条体检数据!");
            }
            catch
            {

            }
        }
    }
}
