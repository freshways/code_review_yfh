﻿using AtomEHR.Library.UserControls;
namespace AtomEHR.公共卫生.Module.基本公共卫生.妇女健康管理
{
    partial class frm第二次产前随访记录
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm第二次产前随访记录));
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.txt孕周End = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txt孕周Begin = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.cbo怀孕状态 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.treeListLookUpEdit机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.checkEdit含下属机构 = new DevExpress.XtraEditors.CheckEdit();
            this.dateEdit出生时间截止 = new DevExpress.XtraEditors.DateEdit();
            this.dateEdit出生时间开始 = new DevExpress.XtraEditors.DateEdit();
            this.dateEdit录入时间截止 = new DevExpress.XtraEditors.DateEdit();
            this.dateEdit录入时间开始 = new DevExpress.XtraEditors.DateEdit();
            this.lookUpEdit录入人 = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit年龄截止 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit年龄开始 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit详细地址 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit档案编号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit姓名 = new DevExpress.XtraEditors.TextEdit();
            this.combBoxEdit档案状态 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit是否合格 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit镇 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit村 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btn微信推送 = new DevExpress.XtraEditors.SimpleButton();
            this.sbtn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.sbtn修改 = new DevExpress.XtraEditors.SimpleButton();
            this.sbtn重置 = new DevExpress.XtraEditors.SimpleButton();
            this.sbtn查询 = new DevExpress.XtraEditors.SimpleButton();
            this.dataGridControl1 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pagerControl1 = new TActionProject.PagerControl();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt孕周End.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt孕周Begin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo怀孕状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit含下属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit出生时间截止.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit出生时间截止.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit出生时间开始.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit出生时间开始.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit录入时间截止.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit录入时间截止.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit录入时间开始.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit录入时间开始.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit录入人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit年龄截止.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit年龄开始.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit详细地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit档案编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.combBoxEdit档案状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit是否合格.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.dataGridControl1);
            this.tpSummary.Controls.Add(this.panelControl1);
            this.tpSummary.Controls.Add(this.layoutControl1);
            this.tpSummary.Controls.Add(this.pagerControl1);
            this.tpSummary.Size = new System.Drawing.Size(868, 413);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Size = new System.Drawing.Size(874, 419);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Size = new System.Drawing.Size(874, 419);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(932, 589);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(874, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(696, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(499, 2);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 1;
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(880, 176);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.panelControl3);
            this.layoutControl1.Controls.Add(this.cbo怀孕状态);
            this.layoutControl1.Controls.Add(this.treeListLookUpEdit机构);
            this.layoutControl1.Controls.Add(this.checkEdit含下属机构);
            this.layoutControl1.Controls.Add(this.dateEdit出生时间截止);
            this.layoutControl1.Controls.Add(this.dateEdit出生时间开始);
            this.layoutControl1.Controls.Add(this.dateEdit录入时间截止);
            this.layoutControl1.Controls.Add(this.dateEdit录入时间开始);
            this.layoutControl1.Controls.Add(this.lookUpEdit录入人);
            this.layoutControl1.Controls.Add(this.textEdit年龄截止);
            this.layoutControl1.Controls.Add(this.textEdit年龄开始);
            this.layoutControl1.Controls.Add(this.textEdit详细地址);
            this.layoutControl1.Controls.Add(this.textEdit身份证号);
            this.layoutControl1.Controls.Add(this.textEdit档案编号);
            this.layoutControl1.Controls.Add(this.textEdit姓名);
            this.layoutControl1.Controls.Add(this.combBoxEdit档案状态);
            this.layoutControl1.Controls.Add(this.comboBoxEdit是否合格);
            this.layoutControl1.Controls.Add(this.comboBoxEdit镇);
            this.layoutControl1.Controls.Add(this.comboBoxEdit村);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(525, 4, 546, 489);
            this.layoutControl1.Padding = new System.Windows.Forms.Padding(1);
            this.layoutControl1.Root = this.layoutControlGroup2;
            this.layoutControl1.Size = new System.Drawing.Size(868, 172);
            this.layoutControl1.TabIndex = 2;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.txt孕周End);
            this.panelControl3.Controls.Add(this.labelControl3);
            this.panelControl3.Controls.Add(this.labelControl2);
            this.panelControl3.Controls.Add(this.txt孕周Begin);
            this.panelControl3.Controls.Add(this.labelControl1);
            this.panelControl3.Location = new System.Drawing.Point(321, 133);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(530, 22);
            this.panelControl3.TabIndex = 129;
            // 
            // txt孕周End
            // 
            this.txt孕周End.Location = new System.Drawing.Point(164, 1);
            this.txt孕周End.Name = "txt孕周End";
            this.txt孕周End.Properties.Mask.EditMask = "\\d{0,2}";
            this.txt孕周End.Properties.Mask.IgnoreMaskBlank = false;
            this.txt孕周End.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txt孕周End.Properties.Mask.ShowPlaceHolders = false;
            this.txt孕周End.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt孕周End.Size = new System.Drawing.Size(50, 20);
            this.txt孕周End.StyleController = this.layoutControl1;
            this.txt孕周End.TabIndex = 129;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(220, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(12, 14);
            this.labelControl3.TabIndex = 128;
            this.labelControl3.Text = "周";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(112, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(44, 14);
            this.labelControl2.TabIndex = 128;
            this.labelControl2.Text = "周  小于";
            // 
            // txt孕周Begin
            // 
            this.txt孕周Begin.Location = new System.Drawing.Point(56, 1);
            this.txt孕周Begin.Name = "txt孕周Begin";
            this.txt孕周Begin.Properties.Mask.EditMask = "\\d{0,2}";
            this.txt孕周Begin.Properties.Mask.IgnoreMaskBlank = false;
            this.txt孕周Begin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txt孕周Begin.Properties.Mask.ShowPlaceHolders = false;
            this.txt孕周Begin.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt孕周Begin.Size = new System.Drawing.Size(50, 20);
            this.txt孕周Begin.StyleController = this.layoutControl1;
            this.txt孕周Begin.TabIndex = 127;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(5, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(48, 14);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "大于等于";
            // 
            // cbo怀孕状态
            // 
            this.cbo怀孕状态.Location = new System.Drawing.Point(82, 133);
            this.cbo怀孕状态.Name = "cbo怀孕状态";
            this.cbo怀孕状态.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo怀孕状态.Properties.Items.AddRange(new object[] {
            "",
            "在孕",
            "流/引产"});
            this.cbo怀孕状态.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo怀孕状态.Size = new System.Drawing.Size(126, 20);
            this.cbo怀孕状态.StyleController = this.layoutControl1;
            this.cbo怀孕状态.TabIndex = 32;
            // 
            // treeListLookUpEdit机构
            // 
            this.treeListLookUpEdit机构.Location = new System.Drawing.Point(82, 37);
            this.treeListLookUpEdit机构.Name = "treeListLookUpEdit机构";
            this.treeListLookUpEdit机构.Properties.AutoExpandAllNodes = false;
            this.treeListLookUpEdit机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEdit机构.Properties.DisplayMember = "机构名称";
            this.treeListLookUpEdit机构.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.treeListLookUpEdit机构.Properties.ValueMember = "机构编号";
            this.treeListLookUpEdit机构.Size = new System.Drawing.Size(163, 20);
            this.treeListLookUpEdit机构.StyleController = this.layoutControl1;
            this.treeListLookUpEdit机构.TabIndex = 30;
            this.treeListLookUpEdit机构.EditValueChanged += new System.EventHandler(this.treeListLookUpEdit机构_EditValueChanged);
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1,
            this.treeListColumn2});
            this.treeListLookUpEdit1TreeList.KeyFieldName = "机构编号";
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(0, 106);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.ParentFieldName = "机构编号";
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "机构编号";
            this.treeListColumn1.FieldName = "机构编号";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            // 
            // treeListColumn2
            // 
            this.treeListColumn2.Caption = "机构名称";
            this.treeListColumn2.FieldName = "机构名称";
            this.treeListColumn2.Name = "treeListColumn2";
            this.treeListColumn2.Visible = true;
            this.treeListColumn2.VisibleIndex = 1;
            // 
            // checkEdit含下属机构
            // 
            this.checkEdit含下属机构.EditValue = true;
            this.checkEdit含下属机构.Location = new System.Drawing.Point(249, 37);
            this.checkEdit含下属机构.Name = "checkEdit含下属机构";
            this.checkEdit含下属机构.Properties.Caption = "含下属机构";
            this.checkEdit含下属机构.Size = new System.Drawing.Size(171, 19);
            this.checkEdit含下属机构.StyleController = this.layoutControl1;
            this.checkEdit含下属机构.TabIndex = 29;
            // 
            // dateEdit出生时间截止
            // 
            this.dateEdit出生时间截止.EditValue = null;
            this.dateEdit出生时间截止.Location = new System.Drawing.Point(263, 85);
            this.dateEdit出生时间截止.Name = "dateEdit出生时间截止";
            this.dateEdit出生时间截止.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit出生时间截止.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit出生时间截止.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit出生时间截止.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit出生时间截止.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit出生时间截止.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit出生时间截止.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEdit出生时间截止.Size = new System.Drawing.Size(157, 20);
            this.dateEdit出生时间截止.StyleController = this.layoutControl1;
            this.dateEdit出生时间截止.TabIndex = 26;
            this.dateEdit出生时间截止.EditValueChanged += new System.EventHandler(this.dateEdit出生时间截止_EditValueChanged);
            // 
            // dateEdit出生时间开始
            // 
            this.dateEdit出生时间开始.EditValue = null;
            this.dateEdit出生时间开始.Location = new System.Drawing.Point(82, 85);
            this.dateEdit出生时间开始.Name = "dateEdit出生时间开始";
            this.dateEdit出生时间开始.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit出生时间开始.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit出生时间开始.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit出生时间开始.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit出生时间开始.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit出生时间开始.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit出生时间开始.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEdit出生时间开始.Size = new System.Drawing.Size(163, 20);
            this.dateEdit出生时间开始.StyleController = this.layoutControl1;
            this.dateEdit出生时间开始.TabIndex = 25;
            this.dateEdit出生时间开始.EditValueChanged += new System.EventHandler(this.dateEdit出生时间开始_EditValueChanged);
            // 
            // dateEdit录入时间截止
            // 
            this.dateEdit录入时间截止.EditValue = null;
            this.dateEdit录入时间截止.Location = new System.Drawing.Point(263, 61);
            this.dateEdit录入时间截止.Name = "dateEdit录入时间截止";
            this.dateEdit录入时间截止.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit录入时间截止.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit录入时间截止.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit录入时间截止.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit录入时间截止.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit录入时间截止.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit录入时间截止.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEdit录入时间截止.Size = new System.Drawing.Size(157, 20);
            this.dateEdit录入时间截止.StyleController = this.layoutControl1;
            this.dateEdit录入时间截止.TabIndex = 24;
            this.dateEdit录入时间截止.EditValueChanged += new System.EventHandler(this.dateEdit录入时间截止_EditValueChanged);
            // 
            // dateEdit录入时间开始
            // 
            this.dateEdit录入时间开始.EditValue = null;
            this.dateEdit录入时间开始.Location = new System.Drawing.Point(82, 61);
            this.dateEdit录入时间开始.Name = "dateEdit录入时间开始";
            this.dateEdit录入时间开始.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit录入时间开始.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit录入时间开始.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit录入时间开始.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit录入时间开始.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit录入时间开始.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit录入时间开始.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEdit录入时间开始.Size = new System.Drawing.Size(163, 20);
            this.dateEdit录入时间开始.StyleController = this.layoutControl1;
            this.dateEdit录入时间开始.TabIndex = 23;
            this.dateEdit录入时间开始.EditValueChanged += new System.EventHandler(this.dateEdit录入时间开始_EditValueChanged);
            // 
            // lookUpEdit录入人
            // 
            this.lookUpEdit录入人.Location = new System.Drawing.Point(710, 61);
            this.lookUpEdit录入人.Name = "lookUpEdit录入人";
            this.lookUpEdit录入人.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit录入人.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("UserName", "用户名")});
            this.lookUpEdit录入人.Properties.NullText = "请选择";
            this.lookUpEdit录入人.Properties.ShowHeader = false;
            this.lookUpEdit录入人.Size = new System.Drawing.Size(141, 20);
            this.lookUpEdit录入人.StyleController = this.layoutControl1;
            this.lookUpEdit录入人.TabIndex = 22;
            // 
            // textEdit年龄截止
            // 
            this.textEdit年龄截止.Location = new System.Drawing.Point(571, 85);
            this.textEdit年龄截止.Name = "textEdit年龄截止";
            this.textEdit年龄截止.Properties.Mask.EditMask = "\\d+";
            this.textEdit年龄截止.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.textEdit年龄截止.Properties.Mask.ShowPlaceHolders = false;
            this.textEdit年龄截止.Size = new System.Drawing.Size(70, 20);
            this.textEdit年龄截止.StyleController = this.layoutControl1;
            this.textEdit年龄截止.TabIndex = 21;
            this.textEdit年龄截止.EditValueChanged += new System.EventHandler(this.textEdit年龄截止_EditValueChanged);
            // 
            // textEdit年龄开始
            // 
            this.textEdit年龄开始.Location = new System.Drawing.Point(489, 85);
            this.textEdit年龄开始.Name = "textEdit年龄开始";
            this.textEdit年龄开始.Properties.Mask.EditMask = "\\d+";
            this.textEdit年龄开始.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.textEdit年龄开始.Properties.Mask.ShowPlaceHolders = false;
            this.textEdit年龄开始.Size = new System.Drawing.Size(64, 20);
            this.textEdit年龄开始.StyleController = this.layoutControl1;
            this.textEdit年龄开始.TabIndex = 20;
            this.textEdit年龄开始.EditValueChanged += new System.EventHandler(this.textEdit年龄开始_EditValueChanged);
            // 
            // textEdit详细地址
            // 
            this.textEdit详细地址.Location = new System.Drawing.Point(400, 109);
            this.textEdit详细地址.Name = "textEdit详细地址";
            this.textEdit详细地址.Size = new System.Drawing.Size(241, 20);
            this.textEdit详细地址.StyleController = this.layoutControl1;
            this.textEdit详细地址.TabIndex = 19;
            // 
            // textEdit身份证号
            // 
            this.textEdit身份证号.Location = new System.Drawing.Point(710, 37);
            this.textEdit身份证号.Name = "textEdit身份证号";
            this.textEdit身份证号.Size = new System.Drawing.Size(141, 20);
            this.textEdit身份证号.StyleController = this.layoutControl1;
            this.textEdit身份证号.TabIndex = 14;
            this.textEdit身份证号.Leave += new System.EventHandler(this.textEdit身份证号_Leave);
            // 
            // textEdit档案编号
            // 
            this.textEdit档案编号.Location = new System.Drawing.Point(489, 61);
            this.textEdit档案编号.Name = "textEdit档案编号";
            this.textEdit档案编号.Size = new System.Drawing.Size(152, 20);
            this.textEdit档案编号.StyleController = this.layoutControl1;
            this.textEdit档案编号.TabIndex = 11;
            this.textEdit档案编号.Leave += new System.EventHandler(this.textEdit档案编号_Leave);
            // 
            // textEdit姓名
            // 
            this.textEdit姓名.Location = new System.Drawing.Point(489, 37);
            this.textEdit姓名.Name = "textEdit姓名";
            this.textEdit姓名.Size = new System.Drawing.Size(152, 20);
            this.textEdit姓名.StyleController = this.layoutControl1;
            this.textEdit姓名.TabIndex = 5;
            // 
            // combBoxEdit档案状态
            // 
            this.combBoxEdit档案状态.EditValue = "活动";
            this.combBoxEdit档案状态.Location = new System.Drawing.Point(710, 85);
            this.combBoxEdit档案状态.Name = "combBoxEdit档案状态";
            this.combBoxEdit档案状态.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.combBoxEdit档案状态.Properties.Items.AddRange(new object[] {
            "请选择",
            "活动",
            "非活动"});
            this.combBoxEdit档案状态.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.combBoxEdit档案状态.Size = new System.Drawing.Size(141, 20);
            this.combBoxEdit档案状态.StyleController = this.layoutControl1;
            this.combBoxEdit档案状态.TabIndex = 10;
            // 
            // comboBoxEdit是否合格
            // 
            this.comboBoxEdit是否合格.EditValue = "";
            this.comboBoxEdit是否合格.Location = new System.Drawing.Point(710, 109);
            this.comboBoxEdit是否合格.Name = "comboBoxEdit是否合格";
            this.comboBoxEdit是否合格.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit是否合格.Properties.Items.AddRange(new object[] {
            "请选择",
            "是",
            "否"});
            this.comboBoxEdit是否合格.Properties.NullText = "请选择";
            this.comboBoxEdit是否合格.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit是否合格.Size = new System.Drawing.Size(141, 20);
            this.comboBoxEdit是否合格.StyleController = this.layoutControl1;
            this.comboBoxEdit是否合格.TabIndex = 31;
            // 
            // comboBoxEdit镇
            // 
            this.comboBoxEdit镇.Location = new System.Drawing.Point(82, 109);
            this.comboBoxEdit镇.Name = "comboBoxEdit镇";
            this.comboBoxEdit镇.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit镇.Properties.NullText = "请选择";
            this.comboBoxEdit镇.Properties.PopupSizeable = true;
            this.comboBoxEdit镇.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit镇.Size = new System.Drawing.Size(126, 20);
            this.comboBoxEdit镇.StyleController = this.layoutControl1;
            this.comboBoxEdit镇.TabIndex = 17;
            this.comboBoxEdit镇.EditValueChanged += new System.EventHandler(this.lookUpEdit镇_EditValueChanged);
            // 
            // comboBoxEdit村
            // 
            this.comboBoxEdit村.Location = new System.Drawing.Point(212, 109);
            this.comboBoxEdit村.Name = "comboBoxEdit村";
            this.comboBoxEdit村.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit村.Properties.NullText = "请选择居/村委会";
            this.comboBoxEdit村.Properties.PopupSizeable = true;
            this.comboBoxEdit村.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit村.Size = new System.Drawing.Size(184, 20);
            this.comboBoxEdit村.StyleController = this.layoutControl1;
            this.comboBoxEdit村.TabIndex = 18;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup1";
            this.layoutControlGroup2.OptionsItemText.TextToControlDistance = 1;
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup2.Size = new System.Drawing.Size(868, 172);
            this.layoutControlGroup2.Text = "layoutControlGroup1";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "查询条件";
            this.layoutControlGroup3.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem2,
            this.layoutControlItem19,
            this.layoutControlItem1,
            this.layoutControlItem11,
            this.layoutControlItem10,
            this.layoutControlItem8,
            this.layoutControlItem5,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem7,
            this.layoutControlItem6,
            this.layoutControlItem15,
            this.layoutControlItem4,
            this.layoutControlItem9,
            this.layoutControlItem3,
            this.layoutControlItem18});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup2";
            this.layoutControlGroup3.Size = new System.Drawing.Size(862, 166);
            this.layoutControlGroup3.Text = "查询条件";
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.comboBoxEdit镇;
            this.layoutControlItem12.CustomizationFormText = "居住地址：";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(195, 24);
            this.layoutControlItem12.Text = "居住地址：";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.textEdit详细地址;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(383, 72);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(245, 24);
            this.layoutControlItem13.Text = "layoutControlItem13";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.comboBoxEdit村;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(195, 72);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(188, 24);
            this.layoutControlItem14.Text = "layoutControlItem14";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextToControlDistance = 0;
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem2.Control = this.textEdit姓名;
            this.layoutControlItem2.CustomizationFormText = "姓名：";
            this.layoutControlItem2.Location = new System.Drawing.Point(407, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(221, 24);
            this.layoutControlItem2.Text = "姓名：";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.checkEdit含下属机构;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(232, 0);
            this.layoutControlItem19.MaxSize = new System.Drawing.Size(200, 23);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(150, 23);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(175, 24);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "layoutControlItem19";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextToControlDistance = 0;
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.treeListLookUpEdit机构;
            this.layoutControlItem1.CustomizationFormText = "所属机构：";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(232, 24);
            this.layoutControlItem1.Text = "所属机构：";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem11.Control = this.textEdit身份证号;
            this.layoutControlItem11.CustomizationFormText = "身份证号：";
            this.layoutControlItem11.Location = new System.Drawing.Point(628, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(210, 24);
            this.layoutControlItem11.Text = "身份证号：";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.dateEdit录入时间开始;
            this.layoutControlItem10.CustomizationFormText = "录入时间：";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(232, 24);
            this.layoutControlItem10.Text = "录入时间：";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem8.Control = this.textEdit档案编号;
            this.layoutControlItem8.CustomizationFormText = "档案编号：";
            this.layoutControlItem8.Location = new System.Drawing.Point(407, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(221, 24);
            this.layoutControlItem8.Text = "档案编号：";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.dateEdit录入时间截止;
            this.layoutControlItem5.CustomizationFormText = "~";
            this.layoutControlItem5.Location = new System.Drawing.Point(232, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(175, 24);
            this.layoutControlItem5.Text = "~";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(9, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem16.Control = this.lookUpEdit录入人;
            this.layoutControlItem16.CustomizationFormText = "录入人：";
            this.layoutControlItem16.Location = new System.Drawing.Point(628, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(210, 24);
            this.layoutControlItem16.Text = "录入人：";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.dateEdit出生时间开始;
            this.layoutControlItem17.CustomizationFormText = "出生日期：";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(232, 24);
            this.layoutControlItem17.Text = "出生日期：";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem7.Control = this.combBoxEdit档案状态;
            this.layoutControlItem7.CustomizationFormText = "档案状态：";
            this.layoutControlItem7.Location = new System.Drawing.Point(628, 48);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(210, 24);
            this.layoutControlItem7.Text = "档案状态：";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEdit年龄开始;
            this.layoutControlItem6.CustomizationFormText = "年龄区间：";
            this.layoutControlItem6.Location = new System.Drawing.Point(407, 48);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(133, 24);
            this.layoutControlItem6.Text = "年龄区间：";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.textEdit年龄截止;
            this.layoutControlItem15.CustomizationFormText = "~";
            this.layoutControlItem15.Location = new System.Drawing.Point(540, 48);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(88, 24);
            this.layoutControlItem15.Text = "~";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(9, 14);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.dateEdit出生时间截止;
            this.layoutControlItem4.CustomizationFormText = "~";
            this.layoutControlItem4.Location = new System.Drawing.Point(232, 48);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(175, 24);
            this.layoutControlItem4.Text = "~";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(9, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem9.Control = this.comboBoxEdit是否合格;
            this.layoutControlItem9.CustomizationFormText = "是否合格：";
            this.layoutControlItem9.Location = new System.Drawing.Point(628, 72);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(210, 24);
            this.layoutControlItem9.Text = "是否合格：";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.cbo怀孕状态;
            this.layoutControlItem3.CustomizationFormText = "怀孕状态：";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(195, 26);
            this.layoutControlItem3.Text = "怀孕状态：";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.panelControl3;
            this.layoutControlItem18.CustomizationFormText = "早孕建册孕周范围：";
            this.layoutControlItem18.Location = new System.Drawing.Point(195, 96);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(373, 26);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(643, 26);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "早孕建册孕周范围：";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(108, 14);
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btn微信推送);
            this.panelControl1.Controls.Add(this.sbtn删除);
            this.panelControl1.Controls.Add(this.sbtn修改);
            this.panelControl1.Controls.Add(this.sbtn重置);
            this.panelControl1.Controls.Add(this.sbtn查询);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 172);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(868, 43);
            this.panelControl1.TabIndex = 3;
            // 
            // btn微信推送
            // 
            this.btn微信推送.Image = ((System.Drawing.Image)(resources.GetObject("btn微信推送.Image")));
            this.btn微信推送.Location = new System.Drawing.Point(591, 5);
            this.btn微信推送.Name = "btn微信推送";
            this.btn微信推送.Size = new System.Drawing.Size(75, 27);
            this.btn微信推送.TabIndex = 5;
            this.btn微信推送.Text = "微信推送";
            this.btn微信推送.Click += new System.EventHandler(this.btn微信推送_Click);
            // 
            // sbtn删除
            // 
            this.sbtn删除.Image = ((System.Drawing.Image)(resources.GetObject("sbtn删除.Image")));
            this.sbtn删除.Location = new System.Drawing.Point(483, 7);
            this.sbtn删除.Name = "sbtn删除";
            this.sbtn删除.Size = new System.Drawing.Size(75, 23);
            this.sbtn删除.TabIndex = 3;
            this.sbtn删除.Text = "删除";
            this.sbtn删除.Visible = false;
            // 
            // sbtn修改
            // 
            this.sbtn修改.Image = ((System.Drawing.Image)(resources.GetObject("sbtn修改.Image")));
            this.sbtn修改.Location = new System.Drawing.Point(368, 7);
            this.sbtn修改.Name = "sbtn修改";
            this.sbtn修改.Size = new System.Drawing.Size(75, 23);
            this.sbtn修改.TabIndex = 2;
            this.sbtn修改.Text = "修改";
            this.sbtn修改.Visible = false;
            this.sbtn修改.Click += new System.EventHandler(this.sbtn修改_Click);
            // 
            // sbtn重置
            // 
            this.sbtn重置.Image = ((System.Drawing.Image)(resources.GetObject("sbtn重置.Image")));
            this.sbtn重置.Location = new System.Drawing.Point(253, 7);
            this.sbtn重置.Name = "sbtn重置";
            this.sbtn重置.Size = new System.Drawing.Size(75, 23);
            this.sbtn重置.TabIndex = 1;
            this.sbtn重置.Text = "重置";
            this.sbtn重置.Click += new System.EventHandler(this.sbtn重置_Click);
            // 
            // sbtn查询
            // 
            this.sbtn查询.Image = ((System.Drawing.Image)(resources.GetObject("sbtn查询.Image")));
            this.sbtn查询.Location = new System.Drawing.Point(138, 7);
            this.sbtn查询.Name = "sbtn查询";
            this.sbtn查询.Size = new System.Drawing.Size(75, 23);
            this.sbtn查询.TabIndex = 0;
            this.sbtn查询.Text = "查询";
            this.sbtn查询.Click += new System.EventHandler(this.sbtn查询_Click);
            // 
            // dataGridControl1
            // 
            this.dataGridControl1.AllowBandedGridColumnSort = false;
            this.dataGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridControl1.IsBestFitColumns = true;
            this.dataGridControl1.Location = new System.Drawing.Point(0, 215);
            this.dataGridControl1.MainView = this.gridView1;
            this.dataGridControl1.Name = "dataGridControl1";
            this.dataGridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.dataGridControl1.ShowContextMenu = false;
            this.dataGridControl1.Size = new System.Drawing.Size(868, 149);
            this.dataGridControl1.StrWhere = "";
            this.dataGridControl1.TabIndex = 131;
            this.dataGridControl1.UseCheckBox = true;
            this.dataGridControl1.View = "";
            this.dataGridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gridView1.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gridView1.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.DetailTip.Options.UseFont = true;
            this.gridView1.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.Empty.Options.UseFont = true;
            this.gridView1.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.EvenRow.Options.UseFont = true;
            this.gridView1.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gridView1.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.FilterPanel.Options.UseFont = true;
            this.gridView1.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.FixedLine.Options.UseFont = true;
            this.gridView1.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.FocusedCell.Options.UseFont = true;
            this.gridView1.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.GroupButton.Options.UseFont = true;
            this.gridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView1.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gridView1.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.HorzLine.Options.UseFont = true;
            this.gridView1.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.OddRow.Options.UseFont = true;
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.RowSeparator.Options.UseFont = true;
            this.gridView1.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.SelectedRow.Options.UseFont = true;
            this.gridView1.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.TopNewRow.Options.UseFont = true;
            this.gridView1.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.VertLine.Options.UseFont = true;
            this.gridView1.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.ViewCaption.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn4,
            this.gridColumn7,
            this.gridColumn6,
            this.gridColumn3,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14});
            this.gridView1.GridControl = this.dataGridControl1;
            this.gridView1.GroupPanelText = "DragColumn";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "档案号";
            this.gridColumn1.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.gridColumn1.FieldName = "个人档案编号";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 120;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit1_Click);
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "姓名";
            this.gridColumn2.FieldName = "姓名";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 60;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "出生日期";
            this.gridColumn4.FieldName = "出生日期";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 2;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn7.AppearanceHeader.Options.UseFont = true;
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.Caption = "居住地址";
            this.gridColumn7.FieldName = "居住地址";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 6;
            this.gridColumn7.Width = 170;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn6.AppearanceHeader.Options.UseFont = true;
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "身份证号";
            this.gridColumn6.FieldName = "身份证号";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 3;
            this.gridColumn6.Width = 120;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn8.AppearanceHeader.Options.UseFont = true;
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.Caption = "丈夫电话";
            this.gridColumn8.FieldName = "联系电话";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 5;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn9.AppearanceHeader.Options.UseFont = true;
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.Caption = "当前所属机构";
            this.gridColumn9.FieldName = "所属机构名称";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 7;
            this.gridColumn9.Width = 150;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn10.AppearanceHeader.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.Caption = "录入人";
            this.gridColumn10.FieldName = "创建人";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 8;
            this.gridColumn10.Width = 60;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn11.AppearanceHeader.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "录入时间";
            this.gridColumn11.FieldName = "创建时间";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 9;
            this.gridColumn11.Width = 125;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "家庭档案编号";
            this.gridColumn12.FieldName = "家庭档案编号";
            this.gridColumn12.Name = "gridColumn12";
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "所属机构";
            this.gridColumn13.FieldName = "所属机构";
            this.gridColumn13.Name = "gridColumn13";
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "ID";
            this.gridColumn14.FieldName = "ID";
            this.gridColumn14.Name = "gridColumn14";
            // 
            // pagerControl1
            // 
            this.pagerControl1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pagerControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pagerControl1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pagerControl1.JumpText = "跳转";
            this.pagerControl1.Location = new System.Drawing.Point(0, 364);
            this.pagerControl1.MaximumSize = new System.Drawing.Size(0, 191);
            this.pagerControl1.Name = "pagerControl1";
            this.pagerControl1.PageIndex = 1;
            this.pagerControl1.PageSize = 100;
            this.pagerControl1.paramters = null;
            this.pagerControl1.RecordCount = 0;
            this.pagerControl1.Size = new System.Drawing.Size(868, 49);
            this.pagerControl1.SqlQuery = null;
            this.pagerControl1.TabIndex = 132;
            this.pagerControl1.OnPageChanged += new System.EventHandler(this.pagerControl1_OnPageChanged);
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "本人电话";
            this.gridColumn3.FieldName = "本人电话";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 4;
            // 
            // frm第二次产前随访记录
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 445);
            this.Name = "frm第二次产前随访记录";
            this.Text = "第二次产前随访服务记录表";
            this.Load += new System.EventHandler(this.frm第二次产前随访记录_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt孕周End.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt孕周Begin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo怀孕状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit含下属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit出生时间截止.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit出生时间截止.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit出生时间开始.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit出生时间开始.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit录入时间截止.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit录入时间截止.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit录入时间开始.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit录入时间开始.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit录入人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit年龄截止.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit年龄开始.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit详细地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit档案编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.combBoxEdit档案状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit是否合格.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEdit机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
        private DevExpress.XtraEditors.CheckEdit checkEdit含下属机构;
        private DevExpress.XtraEditors.DateEdit dateEdit出生时间截止;
        private DevExpress.XtraEditors.DateEdit dateEdit出生时间开始;
        private DevExpress.XtraEditors.DateEdit dateEdit录入时间截止;
        private DevExpress.XtraEditors.DateEdit dateEdit录入时间开始;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit录入人;
        private DevExpress.XtraEditors.TextEdit textEdit年龄截止;
        private DevExpress.XtraEditors.TextEdit textEdit年龄开始;
        private DevExpress.XtraEditors.TextEdit textEdit详细地址;
        private DevExpress.XtraEditors.TextEdit textEdit身份证号;
        private DevExpress.XtraEditors.TextEdit textEdit档案编号;
        private DevExpress.XtraEditors.TextEdit textEdit姓名;
        private DevExpress.XtraEditors.ComboBoxEdit combBoxEdit档案状态;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit是否合格;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit镇;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit村;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton sbtn删除;
        private DevExpress.XtraEditors.SimpleButton sbtn修改;
        private DevExpress.XtraEditors.SimpleButton sbtn重置;
        private DevExpress.XtraEditors.SimpleButton sbtn查询;
        private DataGridControl dataGridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private TActionProject.PagerControl pagerControl1;
        private DevExpress.XtraEditors.SimpleButton btn微信推送;
        private DevExpress.XtraEditors.ComboBoxEdit cbo怀孕状态;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.TextEdit txt孕周End;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txt孕周Begin;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
    }
}