﻿using AtomEHR.Library.UserControls;
namespace AtomEHR.公共卫生.Module.基本公共卫生.老年人健康管理
{
    partial class frm老年人随访服务记录表 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm老年人随访服务记录表));
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.cbo档案类别 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dte录入时间2 = new DevExpress.XtraEditors.DateEdit();
            this.dte录入时间1 = new DevExpress.XtraEditors.DateEdit();
            this.dte出生日期2 = new DevExpress.XtraEditors.DateEdit();
            this.dte出生日期1 = new DevExpress.XtraEditors.DateEdit();
            this.dte随访时间1 = new DevExpress.XtraEditors.DateEdit();
            this.dte随访时间2 = new DevExpress.XtraEditors.DateEdit();
            this.txt详细地址 = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEdit村 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit镇 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.chk含下属机构 = new DevExpress.XtraEditors.CheckEdit();
            this.treeListLookUpEdit机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.cbo人数人次 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo性别 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt档案编号 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.cbo档案状态 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo合格档案 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo录入人 = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.删除 = new DevExpress.XtraEditors.SimpleButton();
            this.修改 = new DevExpress.XtraEditors.SimpleButton();
            this.重置 = new DevExpress.XtraEditors.SimpleButton();
            this.btn查询 = new DevExpress.XtraEditors.SimpleButton();
            this.gc老年人 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv老年人 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.link随访时间 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.link档案编号 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gc老年人Num = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv老年人Num = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.link档案号2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pageControl = new DevExpress.XtraTab.XtraTabControl();
            this.page人次 = new DevExpress.XtraTab.XtraTabPage();
            this.pagerControl1 = new TActionProject.PagerControl();
            this.page人数 = new DevExpress.XtraTab.XtraTabPage();
            this.pagerControl2 = new TActionProject.PagerControl();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案类别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生日期2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生日期2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生日期1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生日期1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt详细地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk含下属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo人数人次.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo合格档案.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo录入人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc老年人)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv老年人)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link随访时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link档案编号)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc老年人Num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv老年人Num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link档案号2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageControl)).BeginInit();
            this.pageControl.SuspendLayout();
            this.page人次.SuspendLayout();
            this.page人数.SuspendLayout();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.pageControl);
            this.tpSummary.Controls.Add(this.panelControl1);
            this.tpSummary.Controls.Add(this.layoutControl1);
            this.tpSummary.Size = new System.Drawing.Size(929, 430);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Size = new System.Drawing.Size(935, 436);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Size = new System.Drawing.Size(935, 436);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(932, 589);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(935, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(757, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(560, 2);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 1;
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(880, 176);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // panelControl2
            // 
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(935, 462);
            this.panelControl2.TabIndex = 3;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.cbo档案类别);
            this.layoutControl1.Controls.Add(this.dte录入时间2);
            this.layoutControl1.Controls.Add(this.dte录入时间1);
            this.layoutControl1.Controls.Add(this.dte出生日期2);
            this.layoutControl1.Controls.Add(this.dte出生日期1);
            this.layoutControl1.Controls.Add(this.dte随访时间1);
            this.layoutControl1.Controls.Add(this.dte随访时间2);
            this.layoutControl1.Controls.Add(this.txt详细地址);
            this.layoutControl1.Controls.Add(this.comboBoxEdit村);
            this.layoutControl1.Controls.Add(this.comboBoxEdit镇);
            this.layoutControl1.Controls.Add(this.chk含下属机构);
            this.layoutControl1.Controls.Add(this.treeListLookUpEdit机构);
            this.layoutControl1.Controls.Add(this.cbo人数人次);
            this.layoutControl1.Controls.Add(this.cbo性别);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt档案编号);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.cbo档案状态);
            this.layoutControl1.Controls.Add(this.cbo合格档案);
            this.layoutControl1.Controls.Add(this.cbo录入人);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.Root = this.layoutControlGroup2;
            this.layoutControl1.Size = new System.Drawing.Size(929, 148);
            this.layoutControl1.TabIndex = 2;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // cbo档案类别
            // 
            this.cbo档案类别.Location = new System.Drawing.Point(762, 123);
            this.cbo档案类别.Name = "cbo档案类别";
            this.cbo档案类别.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo档案类别.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo档案类别.Size = new System.Drawing.Size(101, 20);
            this.cbo档案类别.StyleController = this.layoutControl1;
            this.cbo档案类别.TabIndex = 30;
            // 
            // dte录入时间2
            // 
            this.dte录入时间2.EditValue = null;
            this.dte录入时间2.Location = new System.Drawing.Point(552, 27);
            this.dte录入时间2.Name = "dte录入时间2";
            this.dte录入时间2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte录入时间2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte录入时间2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte录入时间2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte录入时间2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte录入时间2.Size = new System.Drawing.Size(111, 20);
            this.dte录入时间2.StyleController = this.layoutControl1;
            this.dte录入时间2.TabIndex = 29;
            // 
            // dte录入时间1
            // 
            this.dte录入时间1.EditValue = null;
            this.dte录入时间1.Location = new System.Drawing.Point(422, 27);
            this.dte录入时间1.Name = "dte录入时间1";
            this.dte录入时间1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte录入时间1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte录入时间1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte录入时间1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte录入时间1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte录入时间1.Size = new System.Drawing.Size(111, 20);
            this.dte录入时间1.StyleController = this.layoutControl1;
            this.dte录入时间1.TabIndex = 28;
            // 
            // dte出生日期2
            // 
            this.dte出生日期2.EditValue = null;
            this.dte出生日期2.Location = new System.Drawing.Point(552, 51);
            this.dte出生日期2.Name = "dte出生日期2";
            this.dte出生日期2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出生日期2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出生日期2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte出生日期2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte出生日期2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte出生日期2.Size = new System.Drawing.Size(111, 20);
            this.dte出生日期2.StyleController = this.layoutControl1;
            this.dte出生日期2.TabIndex = 27;
            // 
            // dte出生日期1
            // 
            this.dte出生日期1.EditValue = null;
            this.dte出生日期1.Location = new System.Drawing.Point(422, 51);
            this.dte出生日期1.Name = "dte出生日期1";
            this.dte出生日期1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出生日期1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出生日期1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte出生日期1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte出生日期1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte出生日期1.Size = new System.Drawing.Size(111, 20);
            this.dte出生日期1.StyleController = this.layoutControl1;
            this.dte出生日期1.TabIndex = 26;
            // 
            // dte随访时间1
            // 
            this.dte随访时间1.EditValue = null;
            this.dte随访时间1.Location = new System.Drawing.Point(422, 75);
            this.dte随访时间1.Name = "dte随访时间1";
            this.dte随访时间1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte随访时间1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte随访时间1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte随访时间1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte随访时间1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte随访时间1.Size = new System.Drawing.Size(111, 20);
            this.dte随访时间1.StyleController = this.layoutControl1;
            this.dte随访时间1.TabIndex = 25;
            // 
            // dte随访时间2
            // 
            this.dte随访时间2.EditValue = null;
            this.dte随访时间2.Location = new System.Drawing.Point(552, 75);
            this.dte随访时间2.Name = "dte随访时间2";
            this.dte随访时间2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte随访时间2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte随访时间2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte随访时间2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte随访时间2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte随访时间2.Size = new System.Drawing.Size(111, 20);
            this.dte随访时间2.StyleController = this.layoutControl1;
            this.dte随访时间2.TabIndex = 24;
            // 
            // txt详细地址
            // 
            this.txt详细地址.Location = new System.Drawing.Point(457, 123);
            this.txt详细地址.Name = "txt详细地址";
            this.txt详细地址.Size = new System.Drawing.Size(206, 20);
            this.txt详细地址.StyleController = this.layoutControl1;
            this.txt详细地址.TabIndex = 23;
            // 
            // comboBoxEdit村
            // 
            this.comboBoxEdit村.Location = new System.Drawing.Point(257, 123);
            this.comboBoxEdit村.Name = "comboBoxEdit村";
            this.comboBoxEdit村.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit村.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit村.Size = new System.Drawing.Size(196, 20);
            this.comboBoxEdit村.StyleController = this.layoutControl1;
            this.comboBoxEdit村.TabIndex = 22;
            // 
            // comboBoxEdit镇
            // 
            this.comboBoxEdit镇.Location = new System.Drawing.Point(102, 123);
            this.comboBoxEdit镇.Name = "comboBoxEdit镇";
            this.comboBoxEdit镇.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit镇.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit镇.Size = new System.Drawing.Size(151, 20);
            this.comboBoxEdit镇.StyleController = this.layoutControl1;
            this.comboBoxEdit镇.TabIndex = 21;
            this.comboBoxEdit镇.EditValueChanged += new System.EventHandler(this.comboBoxEdit镇_EditValueChanged);
            // 
            // chk含下属机构
            // 
            this.chk含下属机构.EditValue = true;
            this.chk含下属机构.Location = new System.Drawing.Point(227, 27);
            this.chk含下属机构.Name = "chk含下属机构";
            this.chk含下属机构.Properties.Caption = "含下属机构";
            this.chk含下属机构.Size = new System.Drawing.Size(96, 19);
            this.chk含下属机构.StyleController = this.layoutControl1;
            this.chk含下属机构.TabIndex = 20;
            // 
            // treeListLookUpEdit机构
            // 
            this.treeListLookUpEdit机构.EditValue = "";
            this.treeListLookUpEdit机构.Location = new System.Drawing.Point(102, 27);
            this.treeListLookUpEdit机构.Name = "treeListLookUpEdit机构";
            this.treeListLookUpEdit机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEdit机构.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.treeListLookUpEdit机构.Size = new System.Drawing.Size(121, 20);
            this.treeListLookUpEdit机构.StyleController = this.layoutControl1;
            this.treeListLookUpEdit机构.TabIndex = 19;
            this.treeListLookUpEdit机构.EditValueChanged += new System.EventHandler(this.treeListLookUpEdit机构_EditValueChanged);
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(0, 0);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // cbo人数人次
            // 
            this.cbo人数人次.EditValue = "请选择";
            this.cbo人数人次.Location = new System.Drawing.Point(762, 99);
            this.cbo人数人次.Name = "cbo人数人次";
            this.cbo人数人次.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo人数人次.Properties.Items.AddRange(new object[] {
            "请选择",
            "人数",
            "人次"});
            this.cbo人数人次.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo人数人次.Size = new System.Drawing.Size(101, 20);
            this.cbo人数人次.StyleController = this.layoutControl1;
            this.cbo人数人次.TabIndex = 18;
            this.cbo人数人次.SelectedIndexChanged += new System.EventHandler(this.cbo人数人次_SelectedIndexChanged);
            // 
            // cbo性别
            // 
            this.cbo性别.Location = new System.Drawing.Point(102, 99);
            this.cbo性别.Name = "cbo性别";
            this.cbo性别.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo性别.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo性别.Size = new System.Drawing.Size(221, 20);
            this.cbo性别.StyleController = this.layoutControl1;
            this.cbo性别.TabIndex = 17;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(422, 99);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Size = new System.Drawing.Size(241, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 14;
            // 
            // txt档案编号
            // 
            this.txt档案编号.Location = new System.Drawing.Point(102, 75);
            this.txt档案编号.Name = "txt档案编号";
            this.txt档案编号.Size = new System.Drawing.Size(221, 20);
            this.txt档案编号.StyleController = this.layoutControl1;
            this.txt档案编号.TabIndex = 11;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(102, 51);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(221, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 5;
            // 
            // cbo档案状态
            // 
            this.cbo档案状态.Location = new System.Drawing.Point(762, 51);
            this.cbo档案状态.Name = "cbo档案状态";
            this.cbo档案状态.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo档案状态.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo档案状态.Size = new System.Drawing.Size(101, 20);
            this.cbo档案状态.StyleController = this.layoutControl1;
            this.cbo档案状态.TabIndex = 10;
            // 
            // cbo合格档案
            // 
            this.cbo合格档案.Location = new System.Drawing.Point(762, 27);
            this.cbo合格档案.Name = "cbo合格档案";
            this.cbo合格档案.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo合格档案.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo合格档案.Size = new System.Drawing.Size(101, 20);
            this.cbo合格档案.StyleController = this.layoutControl1;
            this.cbo合格档案.TabIndex = 12;
            // 
            // cbo录入人
            // 
            this.cbo录入人.Location = new System.Drawing.Point(762, 75);
            this.cbo录入人.Name = "cbo录入人";
            this.cbo录入人.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo录入人.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("UserName", "用户名")});
            this.cbo录入人.Properties.NullText = "";
            this.cbo录入人.Properties.PopupSizeable = false;
            this.cbo录入人.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbo录入人.Size = new System.Drawing.Size(101, 20);
            this.cbo录入人.StyleController = this.layoutControl1;
            this.cbo录入人.TabIndex = 13;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup1";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(912, 150);
            this.layoutControlGroup2.Text = "layoutControlGroup1";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "查询条件";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem9,
            this.layoutControlItem7,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem14,
            this.layoutControlItem1,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem8,
            this.layoutControlItem3,
            this.layoutControlItem12,
            this.layoutControlItem6,
            this.layoutControlItem13,
            this.layoutControlItem18,
            this.layoutControlItem4,
            this.layoutControlItem19,
            this.layoutControlItem5,
            this.layoutControlItem20});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup2";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup3.Size = new System.Drawing.Size(912, 150);
            this.layoutControlGroup3.Text = "查询条件";
            this.layoutControlGroup3.Click += new System.EventHandler(this.layoutControlGroup3_Click);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.txt姓名;
            this.layoutControlItem2.CustomizationFormText = "姓名：";
            this.layoutControlItem2.FillControlToClientArea = false;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(320, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(320, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(320, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "姓名：";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(90, 24);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.cbo合格档案;
            this.layoutControlItem9.CustomizationFormText = "是否合格：";
            this.layoutControlItem9.Location = new System.Drawing.Point(660, 0);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(242, 24);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "是否合格：";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.cbo档案状态;
            this.layoutControlItem7.CustomizationFormText = "档案状态：";
            this.layoutControlItem7.Location = new System.Drawing.Point(660, 24);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(242, 24);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "档案状态：";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.cbo录入人;
            this.layoutControlItem10.CustomizationFormText = "录入人：";
            this.layoutControlItem10.Location = new System.Drawing.Point(660, 48);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(242, 24);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "录入人：";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.txt身份证号;
            this.layoutControlItem11.CustomizationFormText = "身份证号：";
            this.layoutControlItem11.Location = new System.Drawing.Point(320, 72);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(340, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(340, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(340, 24);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "身份证号：";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.treeListLookUpEdit机构;
            this.layoutControlItem14.CustomizationFormText = "所属机构：";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(220, 24);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(220, 24);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(220, 24);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "所属机构：";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(90, 24);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.chk含下属机构;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(220, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(100, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.comboBoxEdit镇;
            this.layoutControlItem15.CustomizationFormText = "居住地址：";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(250, 24);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "居住地址：";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(90, 24);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.comboBoxEdit村;
            this.layoutControlItem16.CustomizationFormText = "layoutControlItem16";
            this.layoutControlItem16.Location = new System.Drawing.Point(250, 96);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(200, 24);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "layoutControlItem16";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextToControlDistance = 0;
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.txt详细地址;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(450, 96);
            this.layoutControlItem17.MaxSize = new System.Drawing.Size(210, 24);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(210, 24);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.txt档案编号;
            this.layoutControlItem8.CustomizationFormText = "档案编号：";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(320, 24);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(320, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(320, 24);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "档案编号：";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(90, 24);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.cbo性别;
            this.layoutControlItem3.CustomizationFormText = "性 别： ";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(320, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(320, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(320, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "性 别： ";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(90, 24);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.cbo人数人次;
            this.layoutControlItem12.CustomizationFormText = "人数/人次：";
            this.layoutControlItem12.Location = new System.Drawing.Point(660, 72);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(242, 24);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "人数/人次：";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.dte随访时间1;
            this.layoutControlItem6.CustomizationFormText = "随访时间：";
            this.layoutControlItem6.Location = new System.Drawing.Point(320, 48);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(210, 24);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(210, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(210, 24);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "随访时间：";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.dte随访时间2;
            this.layoutControlItem13.CustomizationFormText = "~";
            this.layoutControlItem13.Location = new System.Drawing.Point(530, 48);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(130, 24);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "~";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(10, 20);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem18.Control = this.dte出生日期1;
            this.layoutControlItem18.CustomizationFormText = "出生日期：";
            this.layoutControlItem18.Location = new System.Drawing.Point(320, 24);
            this.layoutControlItem18.MaxSize = new System.Drawing.Size(210, 24);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(210, 24);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(210, 24);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "出生日期：";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.dte出生日期2;
            this.layoutControlItem4.CustomizationFormText = "~";
            this.layoutControlItem4.Location = new System.Drawing.Point(530, 24);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(130, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "~";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(10, 20);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem19.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem19.Control = this.dte录入时间1;
            this.layoutControlItem19.CustomizationFormText = "录入时间：";
            this.layoutControlItem19.Location = new System.Drawing.Point(320, 0);
            this.layoutControlItem19.MaxSize = new System.Drawing.Size(210, 24);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(210, 24);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(210, 24);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "录入时间：";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.dte录入时间2;
            this.layoutControlItem5.CustomizationFormText = "~";
            this.layoutControlItem5.Location = new System.Drawing.Point(530, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(130, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "~";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(10, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.Control = this.cbo档案类别;
            this.layoutControlItem20.CustomizationFormText = "档案类别：";
            this.layoutControlItem20.Location = new System.Drawing.Point(660, 96);
            this.layoutControlItem20.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(242, 24);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "档案类别：";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.删除);
            this.panelControl1.Controls.Add(this.修改);
            this.panelControl1.Controls.Add(this.重置);
            this.panelControl1.Controls.Add(this.btn查询);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 148);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(929, 39);
            this.panelControl1.TabIndex = 3;
            // 
            // 删除
            // 
            this.删除.Image = ((System.Drawing.Image)(resources.GetObject("删除.Image")));
            this.删除.Location = new System.Drawing.Point(483, 7);
            this.删除.Name = "删除";
            this.删除.Size = new System.Drawing.Size(75, 23);
            this.删除.TabIndex = 3;
            this.删除.Text = "删除";
            this.删除.Visible = false;
            // 
            // 修改
            // 
            this.修改.Image = ((System.Drawing.Image)(resources.GetObject("修改.Image")));
            this.修改.Location = new System.Drawing.Point(368, 7);
            this.修改.Name = "修改";
            this.修改.Size = new System.Drawing.Size(75, 23);
            this.修改.TabIndex = 2;
            this.修改.Text = "导出";
            this.修改.Click += new System.EventHandler(this.导出_Click);
            // 
            // 重置
            // 
            this.重置.Image = ((System.Drawing.Image)(resources.GetObject("重置.Image")));
            this.重置.Location = new System.Drawing.Point(253, 7);
            this.重置.Name = "重置";
            this.重置.Size = new System.Drawing.Size(75, 23);
            this.重置.TabIndex = 1;
            this.重置.Text = "重置";
            // 
            // btn查询
            // 
            this.btn查询.Image = ((System.Drawing.Image)(resources.GetObject("btn查询.Image")));
            this.btn查询.Location = new System.Drawing.Point(138, 7);
            this.btn查询.Name = "btn查询";
            this.btn查询.Size = new System.Drawing.Size(75, 23);
            this.btn查询.TabIndex = 0;
            this.btn查询.Text = "查询";
            this.btn查询.Click += new System.EventHandler(this.btn查询_Click);
            // 
            // gc老年人
            // 
            this.gc老年人.AllowBandedGridColumnSort = false;
            this.gc老年人.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc老年人.IsBestFitColumns = true;
            this.gc老年人.Location = new System.Drawing.Point(0, 0);
            this.gc老年人.MainView = this.gv老年人;
            this.gc老年人.Name = "gc老年人";
            this.gc老年人.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.link随访时间,
            this.link档案编号});
            this.gc老年人.ShowContextMenu = false;
            this.gc老年人.Size = new System.Drawing.Size(923, 178);
            this.gc老年人.StrWhere = "";
            this.gc老年人.TabIndex = 4;
            this.gc老年人.UseCheckBox = false;
            this.gc老年人.View = "";
            this.gc老年人.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv老年人});
            // 
            // gv老年人
            // 
            this.gv老年人.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gv老年人.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gv老年人.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gv老年人.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.DetailTip.Options.UseFont = true;
            this.gv老年人.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.Empty.Options.UseFont = true;
            this.gv老年人.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.EvenRow.Options.UseFont = true;
            this.gv老年人.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gv老年人.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.FilterPanel.Options.UseFont = true;
            this.gv老年人.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.FixedLine.Options.UseFont = true;
            this.gv老年人.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.FocusedCell.Options.UseFont = true;
            this.gv老年人.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.FocusedRow.Options.UseFont = true;
            this.gv老年人.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gv老年人.Appearance.FooterPanel.Options.UseFont = true;
            this.gv老年人.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.GroupButton.Options.UseFont = true;
            this.gv老年人.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.GroupFooter.Options.UseFont = true;
            this.gv老年人.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.GroupPanel.Options.UseFont = true;
            this.gv老年人.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.GroupRow.Options.UseFont = true;
            this.gv老年人.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.HeaderPanel.Options.UseFont = true;
            this.gv老年人.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gv老年人.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.HorzLine.Options.UseFont = true;
            this.gv老年人.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.OddRow.Options.UseFont = true;
            this.gv老年人.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.Preview.Options.UseFont = true;
            this.gv老年人.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.Row.Options.UseFont = true;
            this.gv老年人.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.RowSeparator.Options.UseFont = true;
            this.gv老年人.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.SelectedRow.Options.UseFont = true;
            this.gv老年人.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.TopNewRow.Options.UseFont = true;
            this.gv老年人.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.VertLine.Options.UseFont = true;
            this.gv老年人.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人.Appearance.ViewCaption.Options.UseFont = true;
            this.gv老年人.ColumnPanelRowHeight = 30;
            this.gv老年人.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn6,
            this.gridColumn4,
            this.gridColumn7,
            this.gridColumn3,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11});
            this.gv老年人.GridControl = this.gc老年人;
            this.gv老年人.GroupPanelText = "DragColumn";
            this.gv老年人.Name = "gv老年人";
            this.gv老年人.OptionsView.ColumnAutoWidth = false;
            this.gv老年人.OptionsView.EnableAppearanceEvenRow = true;
            this.gv老年人.OptionsView.EnableAppearanceOddRow = true;
            this.gv老年人.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn5.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn5.AppearanceHeader.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "随访时间";
            this.gridColumn5.ColumnEdit = this.link随访时间;
            this.gridColumn5.FieldName = "随访日期";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            // 
            // link随访时间
            // 
            this.link随访时间.AutoHeight = false;
            this.link随访时间.Name = "link随访时间";
            this.link随访时间.Click += new System.EventHandler(this.link随访时间_Click);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn1.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "档案号";
            this.gridColumn1.ColumnEdit = this.link档案编号;
            this.gridColumn1.FieldName = "个人档案编号";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            // 
            // link档案编号
            // 
            this.link档案编号.AutoHeight = false;
            this.link档案编号.Name = "link档案编号";
            this.link档案编号.Click += new System.EventHandler(this.link档案编号_Click);
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn2.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "姓名";
            this.gridColumn2.FieldName = "姓名";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn6.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn6.AppearanceHeader.Options.UseFont = true;
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "性别";
            this.gridColumn6.FieldName = "性别";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 3;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn4.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "出生日期";
            this.gridColumn4.FieldName = "出生日期";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 4;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn7.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn7.AppearanceHeader.Options.UseFont = true;
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.Caption = "居住地址";
            this.gridColumn7.FieldName = "居住地址";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 5;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn3.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "身份证号";
            this.gridColumn3.FieldName = "身份证号";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 6;
            this.gridColumn3.Width = 86;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn8.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn8.AppearanceHeader.Options.UseFont = true;
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.Caption = "联系电话";
            this.gridColumn8.FieldName = "本人电话";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 7;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn9.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn9.AppearanceHeader.Options.UseFont = true;
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.Caption = "当前所属机构";
            this.gridColumn9.FieldName = "所属机构";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 8;
            this.gridColumn9.Width = 113;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn10.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn10.AppearanceHeader.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.Caption = "录入人";
            this.gridColumn10.FieldName = "创建人";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 9;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn11.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn11.AppearanceHeader.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "录入时间";
            this.gridColumn11.FieldName = "创建时间";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 10;
            // 
            // gc老年人Num
            // 
            this.gc老年人Num.AllowBandedGridColumnSort = false;
            this.gc老年人Num.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc老年人Num.IsBestFitColumns = true;
            this.gc老年人Num.Location = new System.Drawing.Point(0, 0);
            this.gc老年人Num.MainView = this.gv老年人Num;
            this.gc老年人Num.Name = "gc老年人Num";
            this.gc老年人Num.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.link档案号2});
            this.gc老年人Num.ShowContextMenu = false;
            this.gc老年人Num.Size = new System.Drawing.Size(926, 340);
            this.gc老年人Num.StrWhere = "";
            this.gc老年人Num.TabIndex = 5;
            this.gc老年人Num.UseCheckBox = false;
            this.gc老年人Num.View = "";
            this.gc老年人Num.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv老年人Num});
            // 
            // gv老年人Num
            // 
            this.gv老年人Num.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gv老年人Num.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gv老年人Num.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gv老年人Num.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.DetailTip.Options.UseFont = true;
            this.gv老年人Num.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.Empty.Options.UseFont = true;
            this.gv老年人Num.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.EvenRow.Options.UseFont = true;
            this.gv老年人Num.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gv老年人Num.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.FilterPanel.Options.UseFont = true;
            this.gv老年人Num.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.FixedLine.Options.UseFont = true;
            this.gv老年人Num.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.FocusedCell.Options.UseFont = true;
            this.gv老年人Num.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.FocusedRow.Options.UseFont = true;
            this.gv老年人Num.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gv老年人Num.Appearance.FooterPanel.Options.UseFont = true;
            this.gv老年人Num.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.GroupButton.Options.UseFont = true;
            this.gv老年人Num.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.GroupFooter.Options.UseFont = true;
            this.gv老年人Num.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.GroupPanel.Options.UseFont = true;
            this.gv老年人Num.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.GroupRow.Options.UseFont = true;
            this.gv老年人Num.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.HeaderPanel.Options.UseFont = true;
            this.gv老年人Num.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gv老年人Num.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.HorzLine.Options.UseFont = true;
            this.gv老年人Num.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.OddRow.Options.UseFont = true;
            this.gv老年人Num.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.Preview.Options.UseFont = true;
            this.gv老年人Num.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.Row.Options.UseFont = true;
            this.gv老年人Num.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.RowSeparator.Options.UseFont = true;
            this.gv老年人Num.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.SelectedRow.Options.UseFont = true;
            this.gv老年人Num.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.TopNewRow.Options.UseFont = true;
            this.gv老年人Num.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.VertLine.Options.UseFont = true;
            this.gv老年人Num.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gv老年人Num.Appearance.ViewCaption.Options.UseFont = true;
            this.gv老年人Num.ColumnPanelRowHeight = 30;
            this.gv老年人Num.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19});
            this.gv老年人Num.GridControl = this.gc老年人Num;
            this.gv老年人Num.GroupPanelText = "DragColumn";
            this.gv老年人Num.Name = "gv老年人Num";
            this.gv老年人Num.OptionsView.ColumnAutoWidth = false;
            this.gv老年人Num.OptionsView.EnableAppearanceEvenRow = true;
            this.gv老年人Num.OptionsView.EnableAppearanceOddRow = true;
            this.gv老年人Num.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn13.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn13.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn13.AppearanceHeader.Options.UseFont = true;
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.Caption = "档案号";
            this.gridColumn13.ColumnEdit = this.link档案号2;
            this.gridColumn13.FieldName = "个人档案编号";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 0;
            // 
            // link档案号2
            // 
            this.link档案号2.AutoHeight = false;
            this.link档案号2.Name = "link档案号2";
            this.link档案号2.Click += new System.EventHandler(this.link档案号2_Click);
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn14.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn14.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn14.AppearanceHeader.Options.UseFont = true;
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.Caption = "姓名";
            this.gridColumn14.FieldName = "姓名";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 1;
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn15.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn15.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn15.AppearanceHeader.Options.UseFont = true;
            this.gridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.Caption = "性别";
            this.gridColumn15.FieldName = "性别";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 2;
            // 
            // gridColumn16
            // 
            this.gridColumn16.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn16.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn16.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn16.AppearanceHeader.Options.UseFont = true;
            this.gridColumn16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn16.Caption = "出生日期";
            this.gridColumn16.FieldName = "出生日期";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 3;
            // 
            // gridColumn17
            // 
            this.gridColumn17.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn17.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn17.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn17.AppearanceHeader.Options.UseFont = true;
            this.gridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.Caption = "居住地址";
            this.gridColumn17.FieldName = "居住地址";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 4;
            // 
            // gridColumn18
            // 
            this.gridColumn18.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn18.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn18.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn18.AppearanceHeader.Options.UseFont = true;
            this.gridColumn18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn18.Caption = "身份证号";
            this.gridColumn18.FieldName = "身份证号";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 5;
            this.gridColumn18.Width = 86;
            // 
            // gridColumn19
            // 
            this.gridColumn19.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn19.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn19.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn19.AppearanceHeader.Options.UseFont = true;
            this.gridColumn19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn19.Caption = "联系电话";
            this.gridColumn19.FieldName = "本人电话";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 6;
            // 
            // pageControl
            // 
            this.pageControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pageControl.Location = new System.Drawing.Point(0, 187);
            this.pageControl.Name = "pageControl";
            this.pageControl.SelectedTabPage = this.page人次;
            this.pageControl.Size = new System.Drawing.Size(929, 243);
            this.pageControl.TabIndex = 7;
            this.pageControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.page人次,
            this.page人数});
            // 
            // page人次
            // 
            this.page人次.Controls.Add(this.gc老年人);
            this.page人次.Controls.Add(this.pagerControl1);
            this.page人次.Name = "page人次";
            this.page人次.Size = new System.Drawing.Size(923, 214);
            this.page人次.Text = "人次";
            // 
            // pagerControl1
            // 
            this.pagerControl1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pagerControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pagerControl1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pagerControl1.JumpText = "跳转";
            this.pagerControl1.Location = new System.Drawing.Point(0, 178);
            this.pagerControl1.Name = "pagerControl1";
            this.pagerControl1.PageIndex = 1;
            this.pagerControl1.PageSize = 100;
            this.pagerControl1.paramters = null;
            this.pagerControl1.RecordCount = 0;
            this.pagerControl1.Size = new System.Drawing.Size(923, 36);
            this.pagerControl1.SqlQuery = null;
            this.pagerControl1.TabIndex = 7;
            this.pagerControl1.OnPageChanged += new System.EventHandler(this.pagerControl1_OnPageChanged);
            // 
            // page人数
            // 
            this.page人数.Controls.Add(this.gc老年人Num);
            this.page人数.Controls.Add(this.pagerControl2);
            this.page人数.Name = "page人数";
            this.page人数.Size = new System.Drawing.Size(926, 373);
            this.page人数.Text = "人数";
            // 
            // pagerControl2
            // 
            this.pagerControl2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pagerControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pagerControl2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pagerControl2.JumpText = "跳转";
            this.pagerControl2.Location = new System.Drawing.Point(0, 340);
            this.pagerControl2.Name = "pagerControl2";
            this.pagerControl2.PageIndex = 1;
            this.pagerControl2.PageSize = 100;
            this.pagerControl2.paramters = null;
            this.pagerControl2.RecordCount = 0;
            this.pagerControl2.Size = new System.Drawing.Size(926, 33);
            this.pagerControl2.SqlQuery = null;
            this.pagerControl2.TabIndex = 6;
            this.pagerControl2.OnPageChanged += new System.EventHandler(this.pagerControl2_OnPageChanged);
            // 
            // frm老年人随访服务记录表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(935, 462);
            this.Controls.Add(this.panelControl2);
            this.Name = "frm老年人随访服务记录表";
            this.Text = "老年人随访服务记录表";
            this.Load += new System.EventHandler(this.frm老年人随访服务记录表_Load);
            this.Controls.SetChildIndex(this.panelControl2, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.Controls.SetChildIndex(this.gcNavigator, 0);
            this.Controls.SetChildIndex(this.pnlSummary, 0);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案类别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生日期2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生日期2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生日期1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生日期1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt详细地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk含下属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo人数人次.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo合格档案.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo录入人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc老年人)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv老年人)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link随访时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link档案编号)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc老年人Num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv老年人Num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link档案号2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageControl)).EndInit();
            this.pageControl.ResumeLayout(false);
            this.page人次.ResumeLayout(false);
            this.page人数.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.ComboBoxEdit cbo人数人次;
        private DevExpress.XtraEditors.ComboBoxEdit cbo性别;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt档案编号;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.ComboBoxEdit cbo档案状态;
        private DevExpress.XtraEditors.ComboBoxEdit cbo合格档案;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton 删除;
        private DevExpress.XtraEditors.SimpleButton 修改;
        private DevExpress.XtraEditors.SimpleButton 重置;
        private DevExpress.XtraEditors.SimpleButton btn查询;
        private DataGridControl gc老年人Num;
        private DevExpress.XtraGrid.Views.Grid.GridView gv老年人Num;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DataGridControl gc老年人;
        private DevExpress.XtraGrid.Views.Grid.GridView gv老年人;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.CheckEdit chk含下属机构;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEdit机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit txt详细地址;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit村;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit镇;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraEditors.LookUpEdit cbo录入人;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link随访时间;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link档案编号;
        private DevExpress.XtraTab.XtraTabControl pageControl;
        private DevExpress.XtraTab.XtraTabPage page人次;
        private TActionProject.PagerControl pagerControl1;
        private DevExpress.XtraTab.XtraTabPage page人数;
        private TActionProject.PagerControl pagerControl2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link档案号2;
        private DevExpress.XtraEditors.DateEdit dte随访时间1;
        private DevExpress.XtraEditors.DateEdit dte随访时间2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.DateEdit dte出生日期2;
        private DevExpress.XtraEditors.DateEdit dte出生日期1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.DateEdit dte录入时间2;
        private DevExpress.XtraEditors.DateEdit dte录入时间1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.ComboBoxEdit cbo档案类别;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
    }
}