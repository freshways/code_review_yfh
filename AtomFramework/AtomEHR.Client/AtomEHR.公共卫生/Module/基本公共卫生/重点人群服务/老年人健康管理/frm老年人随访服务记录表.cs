﻿using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.Models;
using AtomEHR.公共卫生.Module.个人健康;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.基本公共卫生.老年人健康管理
{
    public partial class frm老年人随访服务记录表 : AtomEHR.Library.frmBaseBusinessForm
    {
        private string _strWhere;
        public frm老年人随访服务记录表()
        {
            InitializeComponent();
        }

        private void frm老年人随访服务记录表_Load(object sender, EventArgs e)
        {
            this.page人次.PageVisible = true;
            this.page人数.PageVisible = false;
            this.pageControl.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            frmGridCustomize.RegisterGrid(gv老年人);
            frmGridCustomize.RegisterGrid(gv老年人Num);
            InitView();
        }
        /// <summary>
        /// 初始化页面
        /// </summary>
        private void InitView()
        {
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, cbo性别, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案状态, cbo档案状态, "P_CODE", "P_DESC");
            cbo档案状态.SelectedIndex = 1;
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案类别, cbo档案类别, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t是否, cbo合格档案, "P_CODE", "P_DESC");
            //为“镇”绑定信息
            DataView dv镇 = new DataView(DataDictCache.Cache.t地区信息);
            dv镇.RowFilter = "上级编码=371323";
            //DataBinder.BindingLookupEditDataSource(lookUpEdit镇, dv市.ToTable(), "地区名称", "地区编码");
            util.ControlsHelper.BindComboxData(dv镇.ToTable(), comboBoxEdit镇, "地区编码", "地区名称");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t是否, cbo合格档案, "P_CODE", "P_DESC");
            //为“所属机构"绑定信息
            if (Loginer.CurrentUser.所属机构.Length >= 12)
                treeListLookUpEdit机构.Properties.AutoExpandAllNodes = true;
            else { treeListLookUpEdit机构.Properties.AutoExpandAllNodes = false; }
            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                treeListLookUpEdit机构.Properties.ValueMember = "机构编号";
                treeListLookUpEdit机构.Properties.DisplayMember = "机构名称";

                treeListLookUpEdit机构.Properties.TreeList.KeyFieldName = "机构编号";
                treeListLookUpEdit机构.Properties.TreeList.ParentFieldName = "上级机构";
                treeListLookUpEdit机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                treeListLookUpEdit机构.Properties.DataSource = dt所属机构;
                //treeListLookUpEdit机构.Properties.TreeList.ExpandAll();
                //treeListLookUpEdit机构.Properties.TreeList.CollapseAll();

                treeListLookUpEdit机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                treeListLookUpEdit机构.Text = Loginer.CurrentUser.所属机构;
            }
            try
            {
                DataTable dt录入人 = _bllUser.GetUserNameAndIDBy机构用户(treeListLookUpEdit机构.EditValue.ToString());
                cbo录入人.Properties.DisplayMember = "UserName";
                cbo录入人.Properties.ValueMember = "用户编码";
                cbo录入人.Properties.DataSource = dt录入人;
            }
            catch
            {
                cbo录入人.Text = "请选择...";
            }
        }
        private void comboBoxEdit镇_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv村 = new DataView(DataDictCache.Cache.t地区信息);
                dv村.RowFilter = "上级编码='" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'";
                //DataBinder.BindingLookupEditDataSource(lookUpEdit村, dv县.ToTable(), "地区名称", "地区编码");
                util.ControlsHelper.BindComboxData(dv村.ToTable(), comboBoxEdit村, "地区编码", "地区名称");
                util.ControlsHelper.SetComboxNullData("", comboBoxEdit村, "请选择居/村委会");
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }
        private void layoutControlGroup3_Click(object sender, EventArgs e)
        {

        }
        private void btn查询_Click(object sender, EventArgs e)
        {
            if (funCheck())
            {
                this.pagerControl1.InitControl();
                this.pagerControl2.InitControl();
                #region 查询条件

                _strWhere = string.Empty;
                string pgrid = this.treeListLookUpEdit机构.EditValue.ToString();


                if (this.txt姓名.Text.Trim() != "")
                {
                    _strWhere += " and (姓名 like '" + this.txt姓名.Text.Trim() + "%') ";
                }
                if (this.txt档案编号.Text.Trim() != "")
                {
                    _strWhere += " and 个人档案编号 ='" + this.txt档案编号.Text.Trim() + "'";
                }
                if (this.txt身份证号.Text.Trim() != "")
                {
                    _strWhere += " and 身份证号 ='" + this.txt身份证号.Text.Trim() + "'";
                }
                if (util.ControlsHelper.GetComboxKey(comboBoxEdit镇) != "")
                {
                    _strWhere += " and 街道编码 ='" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'";
                }
                if (util.ControlsHelper.GetComboxKey(comboBoxEdit村) != "")
                {
                    _strWhere += " and [居委会编码] ='" + util.ControlsHelper.GetComboxKey(comboBoxEdit村) + "'";
                }
                if (this.txt详细地址.Text.Trim() != "")
                {
                    _strWhere += " and [居住地址] like '" + this.txt详细地址.Text.Trim() + "%'";
                }

                if (this.dte出生日期1.Text.Trim() != "")
                {
                    _strWhere += " and [出生日期] >= '" + this.dte出生日期1.Text.Trim() + " 00:00:00'";
                }
                if (this.dte出生日期2.Text.Trim() != "")
                {
                    _strWhere += " and [出生日期] <= '" + this.dte出生日期2.Text.Trim() + " 23:59:59'";
                }
                string str性别 = cbo性别.Text;//util.ControlsHelper.GetComboxKey(cbo性别);
                if (str性别.Trim() != "" && str性别.Trim() != "请选择")
                {
                    _strWhere += " and 性别 = '" + str性别 + "'";
                }
                //if (util.ControlsHelper.GetComboxKey(this.cbo性别) != "")
                //{
                //    _strWhere += " and [性别] = '" + util.ControlsHelper.GetComboxKey(cbo性别) + "'";
                //}
                if (util.ControlsHelper.GetComboxKey(this.cbo档案状态) != "")
                {
                    _strWhere += " and [档案状态] = '" + util.ControlsHelper.GetComboxKey(this.cbo档案状态) + "'";
                }
                if (util.ControlsHelper.GetComboxKey(this.cbo档案类别) != "")
                {
                    _strWhere += " and [档案类别] = '" + util.ControlsHelper.GetComboxKey(this.cbo档案类别) + "'";
                }
                #endregion
                if (this.cbo人数人次.Text.Trim() == "请选择" || this.cbo人数人次.Text.Trim() == "人次")
                {
                    if (this.chk含下属机构.Checked)//包含下属机构
                    {
                        if (pgrid.Length == 12)
                        {
                            _strWhere += " and ([所属机构]=  '" + pgrid + "' or substring([所属机构],1,7)+'1'+substring([所属机构],9,7) like '" + pgrid + "%')";
                        }
                        else
                        {
                            _strWhere += " and [所属机构] like '" + pgrid + "%'";
                        }
                    }
                    else
                    {
                        _strWhere += " and [所属机构] ='" + pgrid + "'";
                    }
                    if (this.cbo录入人.Text.Trim() != "")
                    {
                        _strWhere += " and [创建人] = '" + this.cbo录入人.EditValue.ToString() + "'";
                    }
                    if (this.dte录入时间1.Text.Trim() != "")
                    {
                        _strWhere += " and [创建时间] >= '" + this.dte录入时间1.Text.Trim() + " 00:00:00'";
                    }
                    if (this.dte录入时间2.Text.Trim() != "")
                    {
                        _strWhere += " and [创建时间] <= '" + this.dte录入时间2.Text.Trim() + " 23:59:59'";
                    }
                    if (this.dte随访时间1.Text.Trim() != "")
                    {
                        _strWhere += " and  [随访日期] >= '" + this.dte随访时间1.Text.Trim() + "'";
                    }
                    if (this.dte随访时间2.Text.Trim() != "")
                    {
                        _strWhere += " and  [随访日期] <= '" + this.dte随访时间2.Text.Trim() + "'";
                    }
                    if (util.ControlsHelper.GetComboxKey(this.cbo合格档案) != "")
                    {
                        if (util.ControlsHelper.GetComboxKey(this.cbo合格档案) == "1")//合格 ， 缺项为0
                        {
                            _strWhere += " and  [缺项] = '0'";
                        }
                        else
                        {
                            _strWhere += " and  [缺项] <> '0'";
                        }
                    }
                    //_strWhere += " order by 随访日期 desc";
                    BindDataList();
                }
                else if (this.cbo人数人次.Text.Trim() == "人数")//需要Group by 一下进行统计
                {
                    _strWhere += @" and  个人档案编号 in (
		select tj.个人档案编号 from tb_老年人随访 tj where 1=1 ";
                    if (this.chk含下属机构.Checked)//包含下属机构
                    {
                        if (pgrid.Length == 12)
                        {
                            _strWhere += " and (tj.[所属机构]=  '" + pgrid + "' or substring(tj.[所属机构],1,7)+'1'+substring(tj.[所属机构],9,7) like '" + pgrid + "%')";
                        }
                        else
                        {
                            _strWhere += " and tj.[所属机构] like '" + pgrid + "%'";
                        }
                    }
                    else
                    {
                        _strWhere += " and tj.[所属机构] ='" + pgrid + "'";
                    }
                    if (this.cbo录入人.Text.Trim() != "")
                    {
                        _strWhere += " and tj.[创建人] = '" + this.cbo录入人.EditValue.ToString() + "'";
                    }
                    if (this.dte录入时间1.Text.Trim() != "")
                    {
                        _strWhere += " and [创建时间] >= '" + this.dte录入时间1.Text.Trim() + " 00:00:00'";
                    }
                    if (this.dte录入时间2.Text.Trim() != "")
                    {
                        _strWhere += " and [创建时间] <= '" + this.dte录入时间2.Text.Trim() + " 23:59:59'";
                    }
                    if (this.dte随访时间1.Text.Trim() != "")
                    {
                        _strWhere += " and [随访日期] >= '" + this.dte随访时间1.Text.Trim() + "'";
                    }
                    if (this.dte随访时间2.Text.Trim() != "")
                    {
                        _strWhere += " and [随访日期] <= '" + this.dte随访时间2.Text.Trim() + "'";
                    }

                    if (util.ControlsHelper.GetComboxKey(this.cbo合格档案) != "")
                    {
                        if (util.ControlsHelper.GetComboxKey(this.cbo合格档案) == "1")//合格 ， 缺项为0
                        {
                            _strWhere += " and tj.[缺项] = '0'";
                        }
                        else
                        {
                            _strWhere += " and tj.[缺项] <> '0'";
                        }
                    }
                    _strWhere += "  group by tj.个人档案编号)";
                    BindDataNum();
                }
            }
        }
        private bool funCheck()
        {
            if (!string.IsNullOrEmpty(this.dte出生日期1.Text.Trim()) && !string.IsNullOrEmpty(this.dte出生日期2.Text.Trim()) && this.dte出生日期1.DateTime > this.dte出生日期2.DateTime)
            {
                Msg.Warning("出生日期的结束日期必须大于开始时间！");
                this.dte出生日期2.Text = "";
                this.dte出生日期2.Focus();
                return false;
            }
            if (!string.IsNullOrEmpty(this.dte录入时间1.Text.Trim()) && !string.IsNullOrEmpty(this.dte录入时间2.Text.Trim()) && this.dte录入时间1.DateTime > this.dte录入时间2.DateTime)
            {
                Msg.Warning("录入时间的结束日期必须大于开始时间！");
                this.dte录入时间2.Text = "";
                this.dte录入时间2.Focus();
                return false;
            }
            if (!string.IsNullOrEmpty(this.dte随访时间1.Text.Trim()) && !string.IsNullOrEmpty(this.dte随访时间2.Text.Trim()) && this.dte随访时间1.DateTime > this.dte随访时间2.DateTime)
            {
                Msg.Warning("随访时间的结束日期必须大于开始时间！");
                this.dte随访时间2.Text = "";
                this.dte随访时间2.Focus();
                return false;
            }
            return true;
        }
        private void BindDataNum()
        {
            DataTable dt = this.pagerControl2.GetQueryResultNew("vw_老年人随访", "*", _strWhere, "CONVERT(DATETIME, 创建时间)", "DESC").Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["姓名"] = util.DESEncrypt.DES解密(dt.Rows[i]["姓名"].ToString());
                dt.Rows[i]["创建人"] = _BLL.Return用户名称(dt.Rows[i]["创建人"].ToString());
                dt.Rows[i]["性别"] = _BLL.ReturnDis字典显示("xb_xingbie", dt.Rows[i]["性别"].ToString());
                dt.Rows[i]["所属机构"] = _BLL.Return机构名称(dt.Rows[i]["所属机构"].ToString());
                dt.Rows[i]["居住地址"] = _BLL.Return地区名称(dt.Rows[i]["街道"].ToString()) + _BLL.Return地区名称(dt.Rows[i]["居委会"].ToString()) + dt.Rows[i]["居住地址"];
            }
            this.gc老年人Num.DataSource = dt;
            this.pagerControl2.DrawControl();
            this.gv老年人Num.BestFitColumns();//列自适应宽度         
        }
        private void BindDataList()
        {
            DataTable dt = this.pagerControl1.GetQueryResultNew("vw_老年人随访", "*", _strWhere, "CONVERT(DATETIME, 创建时间)", "DESC").Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["姓名"] = util.DESEncrypt.DES解密(dt.Rows[i]["姓名"].ToString());
                dt.Rows[i]["创建人"] = _BLL.Return用户名称(dt.Rows[i]["创建人"].ToString());
                dt.Rows[i]["性别"] = _BLL.ReturnDis字典显示("xb_xingbie", dt.Rows[i]["性别"].ToString());
                dt.Rows[i]["所属机构"] = _BLL.Return机构名称(dt.Rows[i]["所属机构"].ToString());
                dt.Rows[i]["居住地址"] = _BLL.Return地区名称(dt.Rows[i]["街道"].ToString()) + _BLL.Return地区名称(dt.Rows[i]["居委会"].ToString()) + dt.Rows[i]["居住地址"];
            }
            //DataSet ds = this.pagerControl1.GetQueryResult("vw_老年人随访", _strWhere);
            //for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            //{
            //    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
            //    ds.Tables[0].Rows[i][Models.tb_老年人随访.创建人] = _BLL.Return用户名称(ds.Tables[0].Rows[i][Models.tb_老年人随访.创建人].ToString());
            //    ds.Tables[0].Rows[i]["居住地址"] = ds.Tables[0].Rows[i]["街道"].ToString() + ds.Tables[0].Rows[i]["居委会"].ToString() + ds.Tables[0].Rows[i]["居住地址"];
            //}
            this.gc老年人.DataSource = dt;
            this.pagerControl1.DrawControl();
            this.gv老年人.BestFitColumns();//列自适应宽度         
        }
        private void cbo人数人次_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cbo人数人次.Text.Trim() == "请选择" || this.cbo人数人次.Text.Trim() == "人次")
            {
                this.page人次.PageVisible = true;
                this.page人数.PageVisible = false;
            }
            else if (this.cbo人数人次.Text.Trim() == "人数")
            {
                this.page人次.PageVisible = false;
                this.page人数.PageVisible = true;
            }
        }
        private void link随访时间_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv老年人.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            string id = row["ID"].ToString();
            //_BLL.GetBusinessByKey(个人档案编号, true);//下载一个空业务单据            
            //_BLL.NewBusiness(); //增加一条主表记录
            frm个人健康 frm = new frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, id);
            frm.Show();
        }
        private void link档案编号_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv老年人.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            //string id = row["ID"] as string;
            frm个人健康 frm = new frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, "");
            frm.Show();
        }
        private void pagerControl2_OnPageChanged(object sender, EventArgs e)
        {
            BindDataNum();
        }
        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            BindDataList();
        }
        private void link档案号2_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv老年人.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            //string id = row["ID"] as string;
            //_BLL.GetBusinessByKey(个人档案编号, true);//下载一个空业务单据            
            //_BLL.NewBusiness(); //增加一条主表记录
            frm个人健康 frm = new frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, "");
            frm.Show();
        }

        private void 导出_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "XLS file(*.xls)|*.xls";//"PDF file(*.pdf)|*.pdf";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                DataTable dt = gc老年人.DataSource as DataTable;
                Business.bll机构信息 bll = new Business.bll机构信息();
                DataTable newtable = new AtomEHR.Business.BLL_Base.bllBase().Get全部数据("vw_老年人随访", "*", _strWhere, "ID", "DESC");
                for (int i = 0; i < newtable.Rows.Count; i++)
                {
                    newtable.Rows[i]["姓名"] = Common.DESEncrypt.DES解密(newtable.Rows[i]["姓名"].ToString());
                    newtable.Rows[i]["居住地址"] = bll.Return地区名称(newtable.Rows[i]["区"].ToString()) + bll.Return地区名称(newtable.Rows[i]["街道"].ToString()) + bll.Return地区名称(newtable.Rows[i]["居委会"].ToString()) + newtable.Rows[i]["居住地址"].ToString();
                    newtable.Rows[i]["所属机构"] = bll.Return机构名称(newtable.Rows[i]["所属机构"].ToString());
                    newtable.Rows[i]["创建人"] = bll.Return用户名称(newtable.Rows[i]["创建人"].ToString());
                }
                gc老年人.DataSource = newtable;
                gv老年人.ExportToXls(dlg.FileName);
                gc老年人.DataSource = dt;
                //view.ExportToXls(dlg.FileName);
                //ExportToExcel(table, dlg.FileName);
            }
        }

        private void treeListLookUpEdit机构_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dt录入人 = _bllUser.GetUserNameAndIDBy机构用户(treeListLookUpEdit机构.EditValue.ToString());
                cbo录入人.Properties.DisplayMember = "UserName";
                cbo录入人.Properties.ValueMember = "用户编码";
                cbo录入人.Properties.DataSource = dt录入人;
            }
            catch
            {
                cbo录入人.Text = "请选择...";
            }
        }
    }
}
