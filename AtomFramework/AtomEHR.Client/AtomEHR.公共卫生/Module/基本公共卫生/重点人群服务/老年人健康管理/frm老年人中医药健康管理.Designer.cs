﻿using AtomEHR.Library.UserControls;
namespace AtomEHR.公共卫生.Module.基本公共卫生.老年人健康管理
{
    partial class frm老年人中医药健康管理
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm老年人中医药健康管理));
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.cbo档案类别 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dte随访时间2 = new DevExpress.XtraEditors.DateEdit();
            this.dte随访时间1 = new DevExpress.XtraEditors.DateEdit();
            this.txt详细地址 = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEdit村 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit镇 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.treeListLookUpEdit机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit2TreeList = new DevExpress.XtraTreeList.TreeList();
            this.chk含下属机构 = new DevExpress.XtraEditors.CheckEdit();
            this.cbo人数人次 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo是否做中医 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt档案编号 = new DevExpress.XtraEditors.TextEdit();
            this.uC录入时间 = new AtomEHR.Library.UserControls.UCDtpToDtp();
            this.uC出生日期 = new AtomEHR.Library.UserControls.UCDtpToDtp();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.cbo档案状态 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo录入人 = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.btn导出 = new DevExpress.XtraEditors.SimpleButton();
            this.btn重置 = new DevExpress.XtraEditors.SimpleButton();
            this.btn查询 = new DevExpress.XtraEditors.SimpleButton();
            this.gc人数 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv人数 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gc人次 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv人次 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabControl = new DevExpress.XtraTab.XtraTabControl();
            this.page人次 = new DevExpress.XtraTab.XtraTabPage();
            this.pagerControl1 = new TActionProject.PagerControl();
            this.page人数 = new DevExpress.XtraTab.XtraTabPage();
            this.pagerControl2 = new TActionProject.PagerControl();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案类别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt详细地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit2TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk含下属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo人数人次.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo是否做中医.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo录入人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc人数)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv人数)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc人次)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv人次)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.page人次.SuspendLayout();
            this.page人数.SuspendLayout();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.tabControl);
            this.tpSummary.Controls.Add(this.panelControl1);
            this.tpSummary.Controls.Add(this.layoutControl1);
            this.tpSummary.Size = new System.Drawing.Size(929, 430);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Size = new System.Drawing.Size(935, 436);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Size = new System.Drawing.Size(935, 436);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(932, 589);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(935, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(757, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(560, 2);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 1;
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(880, 176);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // panelControl2
            // 
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(935, 462);
            this.panelControl2.TabIndex = 3;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.cbo档案类别);
            this.layoutControl1.Controls.Add(this.dte随访时间2);
            this.layoutControl1.Controls.Add(this.dte随访时间1);
            this.layoutControl1.Controls.Add(this.txt详细地址);
            this.layoutControl1.Controls.Add(this.comboBoxEdit村);
            this.layoutControl1.Controls.Add(this.comboBoxEdit镇);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel1);
            this.layoutControl1.Controls.Add(this.cbo人数人次);
            this.layoutControl1.Controls.Add(this.cbo是否做中医);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt档案编号);
            this.layoutControl1.Controls.Add(this.uC录入时间);
            this.layoutControl1.Controls.Add(this.uC出生日期);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.cbo档案状态);
            this.layoutControl1.Controls.Add(this.cbo录入人);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.Root = this.layoutControlGroup2;
            this.layoutControl1.Size = new System.Drawing.Size(929, 149);
            this.layoutControl1.TabIndex = 2;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // cbo档案类别
            // 
            this.cbo档案类别.Location = new System.Drawing.Point(342, 122);
            this.cbo档案类别.Name = "cbo档案类别";
            this.cbo档案类别.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo档案类别.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo档案类别.Size = new System.Drawing.Size(101, 20);
            this.cbo档案类别.StyleController = this.layoutControl1;
            this.cbo档案类别.TabIndex = 27;
            // 
            // dte随访时间2
            // 
            this.dte随访时间2.EditValue = null;
            this.dte随访时间2.Location = new System.Drawing.Point(630, 73);
            this.dte随访时间2.Name = "dte随访时间2";
            this.dte随访时间2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte随访时间2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte随访时间2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte随访时间2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte随访时间2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte随访时间2.Size = new System.Drawing.Size(111, 20);
            this.dte随访时间2.StyleController = this.layoutControl1;
            this.dte随访时间2.TabIndex = 26;
            // 
            // dte随访时间1
            // 
            this.dte随访时间1.EditValue = null;
            this.dte随访时间1.Location = new System.Drawing.Point(480, 73);
            this.dte随访时间1.Name = "dte随访时间1";
            this.dte随访时间1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte随访时间1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte随访时间1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte随访时间1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte随访时间1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte随访时间1.Size = new System.Drawing.Size(131, 20);
            this.dte随访时间1.StyleController = this.layoutControl1;
            this.dte随访时间1.TabIndex = 25;
            // 
            // txt详细地址
            // 
            this.txt详细地址.Location = new System.Drawing.Point(411, 98);
            this.txt详细地址.Name = "txt详细地址";
            this.txt详细地址.Size = new System.Drawing.Size(231, 20);
            this.txt详细地址.StyleController = this.layoutControl1;
            this.txt详细地址.TabIndex = 24;
            // 
            // comboBoxEdit村
            // 
            this.comboBoxEdit村.Location = new System.Drawing.Point(255, 98);
            this.comboBoxEdit村.Name = "comboBoxEdit村";
            this.comboBoxEdit村.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit村.Size = new System.Drawing.Size(152, 20);
            this.comboBoxEdit村.StyleController = this.layoutControl1;
            this.comboBoxEdit村.TabIndex = 23;
            // 
            // comboBoxEdit镇
            // 
            this.comboBoxEdit镇.Location = new System.Drawing.Point(110, 98);
            this.comboBoxEdit镇.Name = "comboBoxEdit镇";
            this.comboBoxEdit镇.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit镇.Size = new System.Drawing.Size(141, 20);
            this.comboBoxEdit镇.StyleController = this.layoutControl1;
            this.comboBoxEdit镇.TabIndex = 22;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.treeListLookUpEdit机构);
            this.flowLayoutPanel1.Controls.Add(this.chk含下属机构);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(110, 25);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(291, 20);
            this.flowLayoutPanel1.TabIndex = 21;
            // 
            // treeListLookUpEdit机构
            // 
            this.treeListLookUpEdit机构.EditValue = "含下属机构";
            this.treeListLookUpEdit机构.Location = new System.Drawing.Point(0, 0);
            this.treeListLookUpEdit机构.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.treeListLookUpEdit机构.Name = "treeListLookUpEdit机构";
            this.treeListLookUpEdit机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEdit机构.Properties.TreeList = this.treeListLookUpEdit2TreeList;
            this.treeListLookUpEdit机构.Size = new System.Drawing.Size(180, 20);
            this.treeListLookUpEdit机构.TabIndex = 0;
            this.treeListLookUpEdit机构.EditValueChanged += new System.EventHandler(this.treeListLookUpEdit机构_EditValueChanged);
            // 
            // treeListLookUpEdit2TreeList
            // 
            this.treeListLookUpEdit2TreeList.Location = new System.Drawing.Point(0, 95);
            this.treeListLookUpEdit2TreeList.Name = "treeListLookUpEdit2TreeList";
            this.treeListLookUpEdit2TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit2TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit2TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit2TreeList.TabIndex = 0;
            // 
            // chk含下属机构
            // 
            this.chk含下属机构.EditValue = true;
            this.chk含下属机构.Location = new System.Drawing.Point(186, 0);
            this.chk含下属机构.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk含下属机构.Name = "chk含下属机构";
            this.chk含下属机构.Properties.Caption = "含下属机构";
            this.chk含下属机构.Size = new System.Drawing.Size(101, 19);
            this.chk含下属机构.TabIndex = 1;
            // 
            // cbo人数人次
            // 
            this.cbo人数人次.EditValue = "请选择";
            this.cbo人数人次.Location = new System.Drawing.Point(721, 98);
            this.cbo人数人次.Name = "cbo人数人次";
            this.cbo人数人次.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo人数人次.Properties.Items.AddRange(new object[] {
            "请选择",
            "人数",
            "人次"});
            this.cbo人数人次.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo人数人次.Size = new System.Drawing.Size(171, 20);
            this.cbo人数人次.StyleController = this.layoutControl1;
            this.cbo人数人次.TabIndex = 18;
            this.cbo人数人次.SelectedIndexChanged += new System.EventHandler(this.cbo人数人次_SelectedIndexChanged);
            // 
            // cbo是否做中医
            // 
            this.cbo是否做中医.EditValue = "已作";
            this.cbo是否做中医.Location = new System.Drawing.Point(110, 122);
            this.cbo是否做中医.Name = "cbo是否做中医";
            this.cbo是否做中医.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo是否做中医.Properties.Items.AddRange(new object[] {
            "已作",
            "本机构本年未作",
            "本机构未作",
            "从未作过"});
            this.cbo是否做中医.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo是否做中医.Size = new System.Drawing.Size(133, 20);
            this.cbo是否做中医.StyleController = this.layoutControl1;
            this.cbo是否做中医.TabIndex = 17;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(820, 25);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Size = new System.Drawing.Size(104, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 14;
            // 
            // txt档案编号
            // 
            this.txt档案编号.Location = new System.Drawing.Point(480, 49);
            this.txt档案编号.Name = "txt档案编号";
            this.txt档案编号.Size = new System.Drawing.Size(261, 20);
            this.txt档案编号.StyleController = this.layoutControl1;
            this.txt档案编号.TabIndex = 11;
            // 
            // uC录入时间
            // 
            this.uC录入时间.Dte1Size = new System.Drawing.Size(117, 20);
            this.uC录入时间.Dte2Size = new System.Drawing.Size(117, 20);
            this.uC录入时间.Location = new System.Drawing.Point(110, 49);
            this.uC录入时间.Margin = new System.Windows.Forms.Padding(0);
            this.uC录入时间.Name = "uC录入时间";
            this.uC录入时间.Size = new System.Drawing.Size(291, 20);
            this.uC录入时间.TabIndex = 8;
            // 
            // uC出生日期
            // 
            this.uC出生日期.Dte1Size = new System.Drawing.Size(117, 20);
            this.uC出生日期.Dte2Size = new System.Drawing.Size(117, 20);
            this.uC出生日期.Location = new System.Drawing.Point(110, 73);
            this.uC出生日期.Margin = new System.Windows.Forms.Padding(0);
            this.uC出生日期.Name = "uC出生日期";
            this.uC出生日期.Size = new System.Drawing.Size(291, 20);
            this.uC出生日期.TabIndex = 7;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(480, 25);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(261, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 5;
            // 
            // cbo档案状态
            // 
            this.cbo档案状态.Location = new System.Drawing.Point(820, 49);
            this.cbo档案状态.Name = "cbo档案状态";
            this.cbo档案状态.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo档案状态.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo档案状态.Size = new System.Drawing.Size(104, 20);
            this.cbo档案状态.StyleController = this.layoutControl1;
            this.cbo档案状态.TabIndex = 10;
            // 
            // cbo录入人
            // 
            this.cbo录入人.Location = new System.Drawing.Point(820, 73);
            this.cbo录入人.Name = "cbo录入人";
            this.cbo录入人.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo录入人.Properties.NullText = "";
            this.cbo录入人.Properties.PopupSizeable = false;
            this.cbo录入人.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbo录入人.Size = new System.Drawing.Size(104, 20);
            this.cbo录入人.StyleController = this.layoutControl1;
            this.cbo录入人.TabIndex = 13;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup1";
            this.layoutControlGroup2.OptionsItemText.TextToControlDistance = 1;
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(929, 149);
            this.layoutControlGroup2.Text = "layoutControlGroup1";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "查询条件";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem12,
            this.layoutControlItem2,
            this.layoutControlItem11,
            this.layoutControlItem5,
            this.layoutControlItem8,
            this.layoutControlItem7,
            this.layoutControlItem4,
            this.layoutControlItem10,
            this.layoutControlItem3,
            this.emptySpaceItem1,
            this.layoutControlItem1,
            this.layoutControlItem9,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem6,
            this.layoutControlItem15,
            this.layoutControlItem16});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup2";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(929, 149);
            this.layoutControlGroup3.Text = "查询条件";
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.cbo人数人次;
            this.layoutControlItem12.CustomizationFormText = "人数/人次：";
            this.layoutControlItem12.Location = new System.Drawing.Point(641, 73);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(119, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(282, 24);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "人数/人次：";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.txt姓名;
            this.layoutControlItem2.CustomizationFormText = "姓名：";
            this.layoutControlItem2.Location = new System.Drawing.Point(400, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(340, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(340, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(340, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "姓名：";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.txt身份证号;
            this.layoutControlItem11.CustomizationFormText = "身份证号：";
            this.layoutControlItem11.Location = new System.Drawing.Point(740, 0);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(119, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(183, 24);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "身份证号：";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.uC录入时间;
            this.layoutControlItem5.CustomizationFormText = "录入时间：";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(400, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(400, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(400, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "录入时间：";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.txt档案编号;
            this.layoutControlItem8.CustomizationFormText = "档案编号：";
            this.layoutControlItem8.Location = new System.Drawing.Point(400, 24);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(340, 24);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(340, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(340, 24);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "档案编号：";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.cbo档案状态;
            this.layoutControlItem7.CustomizationFormText = "档案状态：";
            this.layoutControlItem7.Location = new System.Drawing.Point(740, 24);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(119, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(183, 24);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "档案状态：";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.uC出生日期;
            this.layoutControlItem4.CustomizationFormText = "出生日期：";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(400, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(400, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(400, 25);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "出生日期：";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.cbo录入人;
            this.layoutControlItem10.CustomizationFormText = "录入人：";
            this.layoutControlItem10.Location = new System.Drawing.Point(740, 48);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(250, 25);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(183, 25);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "录入人：";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.cbo是否做中医;
            this.layoutControlItem3.CustomizationFormText = "性 别： ";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 97);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(119, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(242, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "是否做中医体质：";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(442, 97);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(481, 26);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.flowLayoutPanel1;
            this.layoutControlItem1.CustomizationFormText = "所属机构：";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(400, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(400, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(400, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "所属机构：";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.comboBoxEdit镇;
            this.layoutControlItem9.CustomizationFormText = "居住地址：";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 73);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(250, 24);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "居住地址：";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.comboBoxEdit村;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(250, 73);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(156, 24);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(156, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(156, 24);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "layoutControlItem13";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.txt详细地址;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(406, 73);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(235, 24);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(235, 24);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(235, 24);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "layoutControlItem14";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextToControlDistance = 0;
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.dte随访时间1;
            this.layoutControlItem6.CustomizationFormText = "随访时间：";
            this.layoutControlItem6.Location = new System.Drawing.Point(400, 48);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(210, 24);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(210, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(210, 25);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "随访时间：";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.dte随访时间2;
            this.layoutControlItem15.CustomizationFormText = "~";
            this.layoutControlItem15.Location = new System.Drawing.Point(610, 48);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(130, 25);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "~";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(10, 20);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem16.Control = this.cbo档案类别;
            this.layoutControlItem16.CustomizationFormText = "档案类别：";
            this.layoutControlItem16.Location = new System.Drawing.Point(242, 97);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(200, 26);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "档案类别：";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btn删除);
            this.panelControl1.Controls.Add(this.btn导出);
            this.panelControl1.Controls.Add(this.btn重置);
            this.panelControl1.Controls.Add(this.btn查询);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 149);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(929, 39);
            this.panelControl1.TabIndex = 3;
            // 
            // btn删除
            // 
            this.btn删除.Image = ((System.Drawing.Image)(resources.GetObject("btn删除.Image")));
            this.btn删除.Location = new System.Drawing.Point(376, 6);
            this.btn删除.Name = "btn删除";
            this.btn删除.Size = new System.Drawing.Size(75, 23);
            this.btn删除.TabIndex = 3;
            this.btn删除.Text = "删除";
            this.btn删除.Visible = false;
            // 
            // btn导出
            // 
            this.btn导出.Image = ((System.Drawing.Image)(resources.GetObject("btn导出.Image")));
            this.btn导出.Location = new System.Drawing.Point(261, 6);
            this.btn导出.Name = "btn导出";
            this.btn导出.Size = new System.Drawing.Size(75, 23);
            this.btn导出.TabIndex = 2;
            this.btn导出.Text = "导出";
            this.btn导出.Click += new System.EventHandler(this.btn导出_Click);
            // 
            // btn重置
            // 
            this.btn重置.Image = ((System.Drawing.Image)(resources.GetObject("btn重置.Image")));
            this.btn重置.Location = new System.Drawing.Point(146, 6);
            this.btn重置.Name = "btn重置";
            this.btn重置.Size = new System.Drawing.Size(75, 23);
            this.btn重置.TabIndex = 1;
            this.btn重置.Text = "重置";
            // 
            // btn查询
            // 
            this.btn查询.Image = ((System.Drawing.Image)(resources.GetObject("btn查询.Image")));
            this.btn查询.Location = new System.Drawing.Point(31, 6);
            this.btn查询.Name = "btn查询";
            this.btn查询.Size = new System.Drawing.Size(75, 23);
            this.btn查询.TabIndex = 0;
            this.btn查询.Text = "查询";
            this.btn查询.Click += new System.EventHandler(this.btn查询_Click);
            // 
            // gc人数
            // 
            this.gc人数.AllowBandedGridColumnSort = false;
            this.gc人数.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc人数.IsBestFitColumns = true;
            this.gc人数.Location = new System.Drawing.Point(0, 0);
            this.gc人数.MainView = this.gv人数;
            this.gc人数.Name = "gc人数";
            this.gc人数.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit2});
            this.gc人数.ShowContextMenu = false;
            this.gc人数.Size = new System.Drawing.Size(926, 339);
            this.gc人数.StrWhere = "";
            this.gc人数.TabIndex = 4;
            this.gc人数.UseCheckBox = false;
            this.gc人数.View = "";
            this.gc人数.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv人数});
            // 
            // gv人数
            // 
            this.gv人数.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gv人数.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gv人数.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gv人数.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.DetailTip.Options.UseFont = true;
            this.gv人数.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.Empty.Options.UseFont = true;
            this.gv人数.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.EvenRow.Options.UseFont = true;
            this.gv人数.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gv人数.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.FilterPanel.Options.UseFont = true;
            this.gv人数.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.FixedLine.Options.UseFont = true;
            this.gv人数.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.FocusedCell.Options.UseFont = true;
            this.gv人数.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.FocusedRow.Options.UseFont = true;
            this.gv人数.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gv人数.Appearance.FooterPanel.Options.UseFont = true;
            this.gv人数.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.GroupButton.Options.UseFont = true;
            this.gv人数.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.GroupFooter.Options.UseFont = true;
            this.gv人数.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.GroupPanel.Options.UseFont = true;
            this.gv人数.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.GroupRow.Options.UseFont = true;
            this.gv人数.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.HeaderPanel.Options.UseFont = true;
            this.gv人数.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gv人数.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.HorzLine.Options.UseFont = true;
            this.gv人数.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.OddRow.Options.UseFont = true;
            this.gv人数.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.Preview.Options.UseFont = true;
            this.gv人数.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.Row.Options.UseFont = true;
            this.gv人数.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.RowSeparator.Options.UseFont = true;
            this.gv人数.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.SelectedRow.Options.UseFont = true;
            this.gv人数.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.TopNewRow.Options.UseFont = true;
            this.gv人数.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.VertLine.Options.UseFont = true;
            this.gv人数.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人数.Appearance.ViewCaption.Options.UseFont = true;
            this.gv人数.ColumnPanelRowHeight = 30;
            this.gv人数.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19});
            this.gv人数.GridControl = this.gc人数;
            this.gv人数.GroupPanelText = "DragColumn";
            this.gv人数.Name = "gv人数";
            this.gv人数.OptionsView.ColumnAutoWidth = false;
            this.gv人数.OptionsView.EnableAppearanceEvenRow = true;
            this.gv人数.OptionsView.EnableAppearanceOddRow = true;
            this.gv人数.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn13.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn13.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn13.AppearanceHeader.Options.UseFont = true;
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.Caption = "档案号";
            this.gridColumn13.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.gridColumn13.FieldName = "个人档案编号";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 0;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit2_Click);
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn14.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn14.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn14.AppearanceHeader.Options.UseFont = true;
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.Caption = "姓名";
            this.gridColumn14.FieldName = "姓名";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 1;
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn15.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn15.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn15.AppearanceHeader.Options.UseFont = true;
            this.gridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.Caption = "性别";
            this.gridColumn15.FieldName = "性别";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 2;
            // 
            // gridColumn16
            // 
            this.gridColumn16.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn16.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn16.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn16.AppearanceHeader.Options.UseFont = true;
            this.gridColumn16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn16.Caption = "出生日期";
            this.gridColumn16.FieldName = "出生日期";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 3;
            // 
            // gridColumn17
            // 
            this.gridColumn17.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn17.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn17.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn17.AppearanceHeader.Options.UseFont = true;
            this.gridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.Caption = "居住地址";
            this.gridColumn17.FieldName = "居住地址";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 4;
            // 
            // gridColumn18
            // 
            this.gridColumn18.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn18.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn18.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn18.AppearanceHeader.Options.UseFont = true;
            this.gridColumn18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn18.Caption = "身份证号";
            this.gridColumn18.FieldName = "身份证号";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 5;
            this.gridColumn18.Width = 86;
            // 
            // gridColumn19
            // 
            this.gridColumn19.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn19.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn19.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn19.AppearanceHeader.Options.UseFont = true;
            this.gridColumn19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn19.Caption = "联系电话";
            this.gridColumn19.FieldName = "本人电话";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 6;
            // 
            // gc人次
            // 
            this.gc人次.AllowBandedGridColumnSort = false;
            this.gc人次.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc人次.IsBestFitColumns = true;
            this.gc人次.Location = new System.Drawing.Point(0, 0);
            this.gc人次.MainView = this.gv人次;
            this.gc人次.Name = "gc人次";
            this.gc人次.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.gc人次.ShowContextMenu = false;
            this.gc人次.Size = new System.Drawing.Size(923, 180);
            this.gc人次.StrWhere = "";
            this.gc人次.TabIndex = 5;
            this.gc人次.UseCheckBox = false;
            this.gc人次.View = "";
            this.gc人次.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv人次});
            // 
            // gv人次
            // 
            this.gv人次.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gv人次.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gv人次.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gv人次.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.DetailTip.Options.UseFont = true;
            this.gv人次.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.Empty.Options.UseFont = true;
            this.gv人次.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.EvenRow.Options.UseFont = true;
            this.gv人次.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gv人次.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.FilterPanel.Options.UseFont = true;
            this.gv人次.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.FixedLine.Options.UseFont = true;
            this.gv人次.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.FocusedCell.Options.UseFont = true;
            this.gv人次.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.FocusedRow.Options.UseFont = true;
            this.gv人次.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gv人次.Appearance.FooterPanel.Options.UseFont = true;
            this.gv人次.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.GroupButton.Options.UseFont = true;
            this.gv人次.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.GroupFooter.Options.UseFont = true;
            this.gv人次.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.GroupPanel.Options.UseFont = true;
            this.gv人次.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.GroupRow.Options.UseFont = true;
            this.gv人次.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.HeaderPanel.Options.UseFont = true;
            this.gv人次.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gv人次.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.HorzLine.Options.UseFont = true;
            this.gv人次.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.OddRow.Options.UseFont = true;
            this.gv人次.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.Preview.Options.UseFont = true;
            this.gv人次.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.Row.Options.UseFont = true;
            this.gv人次.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.RowSeparator.Options.UseFont = true;
            this.gv人次.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.SelectedRow.Options.UseFont = true;
            this.gv人次.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.TopNewRow.Options.UseFont = true;
            this.gv人次.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.VertLine.Options.UseFont = true;
            this.gv人次.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gv人次.Appearance.ViewCaption.Options.UseFont = true;
            this.gv人次.ColumnPanelRowHeight = 30;
            this.gv人次.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn6,
            this.gridColumn4,
            this.gridColumn7,
            this.gridColumn3,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11});
            this.gv人次.GridControl = this.gc人次;
            this.gv人次.GroupPanelText = "DragColumn";
            this.gv人次.Name = "gv人次";
            this.gv人次.OptionsView.ColumnAutoWidth = false;
            this.gv人次.OptionsView.EnableAppearanceEvenRow = true;
            this.gv人次.OptionsView.EnableAppearanceOddRow = true;
            this.gv人次.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn1.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "档案号";
            this.gridColumn1.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.gridColumn1.FieldName = "个人档案编号";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit1_Click);
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn2.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "姓名";
            this.gridColumn2.FieldName = "姓名";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn6.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn6.AppearanceHeader.Options.UseFont = true;
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "性别";
            this.gridColumn6.FieldName = "性别";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn4.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "出生日期";
            this.gridColumn4.FieldName = "出生日期";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 2;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn7.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn7.AppearanceHeader.Options.UseFont = true;
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.Caption = "居住地址";
            this.gridColumn7.FieldName = "居住地址";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 3;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn3.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "身份证号";
            this.gridColumn3.FieldName = "身份证号";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 4;
            this.gridColumn3.Width = 86;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn8.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn8.AppearanceHeader.Options.UseFont = true;
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.Caption = "联系电话";
            this.gridColumn8.FieldName = "本人电话";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 5;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn9.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn9.AppearanceHeader.Options.UseFont = true;
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.Caption = "当前所属机构";
            this.gridColumn9.FieldName = "所属机构";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 6;
            this.gridColumn9.Width = 113;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn10.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn10.AppearanceHeader.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.Caption = "录入人";
            this.gridColumn10.FieldName = "创建人";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 7;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn11.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn11.AppearanceHeader.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "录入时间";
            this.gridColumn11.FieldName = "创建时间";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 8;
            // 
            // tabControl
            // 
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 188);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedTabPage = this.page人次;
            this.tabControl.Size = new System.Drawing.Size(929, 242);
            this.tabControl.TabIndex = 6;
            this.tabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.page人次,
            this.page人数});
            // 
            // page人次
            // 
            this.page人次.Controls.Add(this.gc人次);
            this.page人次.Controls.Add(this.pagerControl1);
            this.page人次.Name = "page人次";
            this.page人次.Size = new System.Drawing.Size(923, 213);
            this.page人次.Text = "人次";
            // 
            // pagerControl1
            // 
            this.pagerControl1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pagerControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pagerControl1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pagerControl1.JumpText = "跳转";
            this.pagerControl1.Location = new System.Drawing.Point(0, 180);
            this.pagerControl1.Name = "pagerControl1";
            this.pagerControl1.PageIndex = 1;
            this.pagerControl1.PageSize = 100;
            this.pagerControl1.paramters = null;
            this.pagerControl1.RecordCount = 0;
            this.pagerControl1.Size = new System.Drawing.Size(923, 33);
            this.pagerControl1.SqlQuery = null;
            this.pagerControl1.TabIndex = 6;
            this.pagerControl1.OnPageChanged += new System.EventHandler(this.pagerControl1_OnPageChanged);
            // 
            // page人数
            // 
            this.page人数.Controls.Add(this.gc人数);
            this.page人数.Controls.Add(this.pagerControl2);
            this.page人数.Name = "page人数";
            this.page人数.Size = new System.Drawing.Size(926, 372);
            this.page人数.Text = "人数";
            // 
            // pagerControl2
            // 
            this.pagerControl2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pagerControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pagerControl2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pagerControl2.JumpText = "跳转";
            this.pagerControl2.Location = new System.Drawing.Point(0, 339);
            this.pagerControl2.Name = "pagerControl2";
            this.pagerControl2.PageIndex = 1;
            this.pagerControl2.PageSize = 100;
            this.pagerControl2.paramters = null;
            this.pagerControl2.RecordCount = 0;
            this.pagerControl2.Size = new System.Drawing.Size(926, 33);
            this.pagerControl2.SqlQuery = null;
            this.pagerControl2.TabIndex = 5;
            this.pagerControl2.OnPageChanged += new System.EventHandler(this.pagerControl2_OnPageChanged);
            // 
            // frm老年人中医药健康管理
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(935, 462);
            this.Controls.Add(this.panelControl2);
            this.Name = "frm老年人中医药健康管理";
            this.Text = "老年人中医药健康管理";
            this.Load += new System.EventHandler(this.frm老年人中医药健康管理_Load);
            this.Controls.SetChildIndex(this.panelControl2, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.Controls.SetChildIndex(this.gcNavigator, 0);
            this.Controls.SetChildIndex(this.pnlSummary, 0);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案类别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt详细地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit2TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk含下属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo人数人次.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo是否做中医.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo录入人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc人数)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv人数)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc人次)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv人次)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.page人次.ResumeLayout(false);
            this.page人数.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btn删除;
        private DevExpress.XtraEditors.SimpleButton btn导出;
        private DevExpress.XtraEditors.SimpleButton btn重置;
        private DevExpress.XtraEditors.SimpleButton btn查询;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txt详细地址;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit村;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit镇;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEdit机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit2TreeList;
        private DevExpress.XtraEditors.CheckEdit chk含下属机构;
        private DevExpress.XtraEditors.ComboBoxEdit cbo人数人次;
        private DevExpress.XtraEditors.ComboBoxEdit cbo是否做中医;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt档案编号;
        private UCDtpToDtp uC录入时间;
        private UCDtpToDtp uC出生日期;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.ComboBoxEdit cbo档案状态;
        private DevExpress.XtraEditors.LookUpEdit cbo录入人;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DataGridControl gc人次;
        private DevExpress.XtraGrid.Views.Grid.GridView gv人次;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DataGridControl gc人数;
        private DevExpress.XtraGrid.Views.Grid.GridView gv人数;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraTab.XtraTabControl tabControl;
        private DevExpress.XtraTab.XtraTabPage page人次;
        private DevExpress.XtraTab.XtraTabPage page人数;
        private TActionProject.PagerControl pagerControl1;
        private TActionProject.PagerControl pagerControl2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraEditors.DateEdit dte随访时间2;
        private DevExpress.XtraEditors.DateEdit dte随访时间1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.ComboBoxEdit cbo档案类别;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
    }
}