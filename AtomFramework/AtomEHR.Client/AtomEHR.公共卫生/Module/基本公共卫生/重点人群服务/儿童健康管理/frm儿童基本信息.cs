﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Business.Security;
using AtomEHR.Library;
using AtomEHR.公共卫生.Module.个人健康;
using AtomEHR.Models;


namespace AtomEHR.公共卫生.Module.基本公共卫生.儿童健康管理
{
    public partial class frm儿童基本信息 : AtomEHR.Library.frmBaseBusinessForm
    {
        private int size = 100;
        private string strWhere = null;
        public frm儿童基本信息()
        {
            InitializeComponent();
        }


        private void textEdit年龄开始_EditValueChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textEdit年龄开始.Text))
            {
                dateEdit出生时间截止.Text = "";
                return;
            }

            DateTime dtNow = DateTime.Now;
            int age开始 = Convert.ToInt32(textEdit年龄开始.Text);

            if (string.IsNullOrWhiteSpace(textEdit年龄截止.Text))
            { }
            else
            {
                int age截止 = Convert.ToInt32(textEdit年龄截止.Text);
                if (age开始 > age截止)
                {
                    Msg.Warning("起始年龄应小于结束年龄");
                    textEdit年龄截止.Text = textEdit年龄开始.Text;
                    dateEdit出生时间开始.Text = DateTime.Now.AddYears(-1 * age开始 - 1).ToString("yyyy-MM-dd");
                }
            }

            dateEdit出生时间截止.Text = dtNow.AddYears(age开始 * -1).ToString("yyyy-MM-dd");
        }

        private void textEdit年龄截止_EditValueChanged(object sender, EventArgs e)
        {
            //如果截止年龄设置为空，则将出生日期的开始时间也设置为空
            if (string.IsNullOrWhiteSpace(textEdit年龄截止.Text))
            {
                dateEdit出生时间开始.Text = "";
                return;
            }

            DateTime dtNow = DateTime.Now;
            int age截止 = Convert.ToInt32(textEdit年龄截止.Text);

            //进行年龄范围的合法性验证：开始年龄<=截止年龄
            #region
            if (string.IsNullOrWhiteSpace(textEdit年龄开始.Text))
            {
            }
            else
            {
                //开始年龄不为空的情况下
                int age开始 = Convert.ToInt32(textEdit年龄开始.Text);
                if (age开始 > age截止)
                {
                    Msg.Warning("起始年龄应小于结束年龄");
                    textEdit年龄截止.Text = textEdit年龄开始.Text;
                    dateEdit出生时间开始.Text = DateTime.Now.AddYears(-1 * age开始 - 1).ToString("yyyy-MM-dd");
                    return;
                }
            }
            #endregion

            dateEdit出生时间开始.Text = dtNow.AddYears(age截止 * (-1) - 1).AddDays(1).ToString("yyyy-MM-dd");
        }

        private void dateEdit出生时间开始_EditValueChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(dateEdit出生时间开始.Text))//出生日期为空时，截止年龄设置为空
            {
                //this.textEdit年龄截止.Text = "";
                return;
            }

            DateTime dt出生日期开始 = Convert.ToDateTime(this.dateEdit出生时间开始.Text);
            DateTime dtNow = DateTime.Now;
            TimeSpan timeSpan时间差 = dtNow - dt出生日期开始;
            //如果所选时间大于当前时间（运行此程序的电脑的系统时间），则显示提示信息
            if (timeSpan时间差.TotalDays < 0)
            {
                Msg.Warning("起始时间不能大于当前时间");
                dateEdit出生时间开始.Text = dtNow.ToString("yyyy-MM-dd");
                //this.textEdit年龄截止.Text = "0";
                return; //必须返回，词句不可缺少
            }

            if (string.IsNullOrWhiteSpace(dateEdit出生时间截止.Text))
            {
                //计算截止年龄
            }
            else
            {
                //如果截止时间不是空的，则需要判断时间范围：
                DateTime dt截止时间 = Convert.ToDateTime(dateEdit出生时间截止.Text);
                TimeSpan timespan = dt出生日期开始 - dt截止时间;
                if (timespan.TotalDays > 0)
                {
                    dateEdit出生时间截止.Text = DateTime.Now.ToString("yyyy-MM-dd");
                    //textEdit年龄开始.Text = "0";
                    //然后，计算截止年龄
                }
                else
                {
                    //计算截止年龄
                }
            }

            //所选时间小于等于当前时间（运行此程序的电脑的系统时间）的情况下，则根据日期修改年龄的截止范围
            //double dYear = Math.Floor(timeSpan时间差.TotalDays / 365);

            ////判断日期是否满足  当前日期-（dYear+1）*1年 < 设定的出生开始日期 <= 当前日期-dYear*1年
            //DateTime dt1 = dtNow.AddYears(-1 * (int)dYear);
            //DateTime dt2 = dtNow.AddYears(-1 * (int)dYear -1);
            //if(DateTime.Compare(dt2,dt出生日期开始)< 0 && DateTime.Compare(dt出生日期开始,dt1)<=0 )
            //{
            //    textEdit年龄截止.Text = dYear.ToString();
            //}
            //else
            //{
            //    textEdit年龄截止.Text = (dYear - 1).ToString();
            //}
        }

        private void dateEdit出生时间截止_EditValueChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(dateEdit出生时间截止.Text))
            {
                //textEdit年龄开始.Text = "";
                return;
            }

            DateTime dt截止时间 = Convert.ToDateTime(dateEdit出生时间截止.Text);
            if (DateTime.Compare(dt截止时间, DateTime.Now) > 0)
            {
                Msg.Warning("截止时间不能大于当前日期");
                dateEdit出生时间截止.Text = DateTime.Now.ToString("yyyy-MM-dd");
                //textEdit年龄开始.Text = "0";
                return;
            }

            if (string.IsNullOrWhiteSpace(dateEdit出生时间开始.Text))
            {
                //直接设定年龄开始
                //GOTO: 下方开始年龄的设定
            }
            else
            {
                DateTime dt开始时间 = Convert.ToDateTime(dateEdit出生时间开始.Text);
                if (DateTime.Compare(dt开始时间, dt截止时间) <= 0)
                {
                    //出生日期的开始日期小于等于截止日期，直接设定开始年龄
                    //GOTO:下方开始年龄的设定
                }
                else
                {
                    //截止日期小于出生日期的开始日期
                    Msg.Warning("截止日期不能小于开始日期");
                    dateEdit出生时间截止.Text = DateTime.Now.ToString("yyyy-MM-dd");
                    //textEdit年龄开始.Text = "0";
                    return;
                }
            }

            //下方开始年龄的设定
            //判断日期是否满足  当前日期-（dYear+1）*1年 < 设定的出生截止日期 <= 当前日期-dYear*1年
            //TimeSpan timespan = DateTime.Now - dt截止时间;

            //double dYear = timespan.TotalDays / 365;

            //DateTime dt1 = DateTime.Now.AddYears(-1 * (int)dYear);
            //DateTime dt2 = DateTime.Now.AddYears(-1 * (int)dYear - 1);
            //if (DateTime.Compare(dt2, dt截止时间) < 0 && DateTime.Compare(dt截止时间, dt1) <= 0)
            //{
            //    textEdit年龄开始.Text = dYear.ToString();
            //}
            //else
            //{
            //    textEdit年龄开始.Text = (dYear - 1).ToString();
            //}
        }

        private void frm儿童基本信息_Load(object sender, EventArgs e)
        {
            size = layoutControl1.Height;
            base.InitButtonsBase();
            comboBoxEdit是否合格.EditValue = null;
            this.pagerControl1.Height = 35;
            //为性别绑定信息
            //DataBinder.BindingLookupEditDataSource(lookUpEdit性别, DataDictCache.Cache.t性别, "P_DESC", "P_CODE");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, comboBoxEdit性别, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案类别, com档案类型, "P_CODE", "P_DESC");
            comboBoxEdit性别.EditValue = null;
            frmGridCustomize.RegisterGrid(gridView1);

            //为“镇”绑定信息
            DataView dv镇 = new DataView(DataDictCache.Cache.t地区信息);
            dv镇.RowFilter = "上级编码=371323";
            //DataBinder.BindingLookupEditDataSource(lookUpEdit镇, dv市.ToTable(), "地区名称", "地区编码");
            util.ControlsHelper.BindComboxData(dv镇.ToTable(), comboBoxEdit镇, "地区编码", "地区名称");

            //为“所属机构"绑定信息
            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                treeListLookUpEdit机构.Properties.ValueMember = "机构编号";
                treeListLookUpEdit机构.Properties.DisplayMember = "机构名称";

                treeListLookUpEdit机构.Properties.TreeList.KeyFieldName = "机构编号";
                treeListLookUpEdit机构.Properties.TreeList.ParentFieldName = "上级机构";
                treeListLookUpEdit机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                treeListLookUpEdit机构.Properties.DataSource = dt所属机构;
                //treeListLookUpEdit机构.Properties.TreeList.ExpandAll();
                //treeListLookUpEdit机构.Properties.TreeList.CollapseAll();

                treeListLookUpEdit机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                treeListLookUpEdit机构.Text = Loginer.CurrentUser.所属机构;
            }

            if (Loginer.CurrentUser.所属机构.Length >= 12)
            {
                treeListLookUpEdit机构.Properties.AutoExpandAllNodes = true;
            }
            else
            {
                treeListLookUpEdit机构.Properties.AutoExpandAllNodes = false;
            }

            //绑定“录入人”绑定信息
            try
            {
                DataTable dt录入人 = new bllUser().GetUserNameAndIDBy机构用户(treeListLookUpEdit机构.EditValue.ToString());
                lookUpEdit录入人.Properties.DisplayMember = "UserName";
                lookUpEdit录入人.Properties.ValueMember = "用户编码";
                lookUpEdit录入人.Properties.DataSource = dt录入人;
            }
            catch
            {
                lookUpEdit录入人.Text = "请选择...";
            }

            BindDataForComboBox("wyfs", comboBoxEdit喂养方式);
        }

        private void BindDataForComboBox(string p_fun_code, DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit)
        {
            DataView dv = new DataView(DataDictCache.Cache.t常用字典);
            dv.RowFilter = "P_FUN_CODE = '" + p_fun_code + "'";
            dv.Sort = "P_CODE";
            util.ControlsHelper.BindComboxDataNull(dv.ToTable(), comboBoxEdit, "P_CODE", "P_DESC");
        }

        private void lookUpEdit镇_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv村 = new DataView(DataDictCache.Cache.t地区信息);
                dv村.RowFilter = "上级编码='" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'";
                //DataBinder.BindingLookupEditDataSource(lookUpEdit村, dv县.ToTable(), "地区名称", "地区编码");
                util.ControlsHelper.BindComboxData(dv村.ToTable(), comboBoxEdit村, "地区编码", "地区名称");
                util.ControlsHelper.SetComboxNullData("", comboBoxEdit村, "请选择居/村委会");
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }

        private void sbtn查询_Click(object sender, EventArgs e)
        {
            #region 旧的查询方式注释掉
            //bll儿童基本信息 bll儿童 = new bll儿童基本信息();
            //try
            //{
            //    string str录入人 = null;
            //    if(lookUpEdit录入人.EditValue != null)
            //    {
            //        str录入人 = lookUpEdit录入人.EditValue.ToString();
            //    }

            //    string str是否合格 = null;
            //    if(comboBoxEdit是否合格.Text.Equals("是"))
            //    {
            //        str是否合格 = "1";
            //    }
            //    else if(comboBoxEdit是否合格.Text.Equals("否"))
            //    {
            //        str是否合格 = "2";
            //    }
            //    else
            //    {
            //        str是否合格 = null;
            //    }

            //    string str镇 = null;
            //    if (comboBoxEdit镇.EditValue != null)
            //    {
            //        str镇 = util.ControlsHelper.GetComboxKey(comboBoxEdit镇);
            //    }

            //    string str村 = null;
            //    if(comboBoxEdit村.EditValue != null)
            //    {
            //        str村 = util.ControlsHelper.GetComboxKey(comboBoxEdit村);
            //    }

            //    string str档案状态 = null;
            //    if (combBoxEdit档案状态.Text.Equals("活动"))
            //    {
            //        str档案状态 = "1";
            //    }
            //    else if(combBoxEdit档案状态.Text.Equals("非活动"))
            //    {
            //        str档案状态 = "2";
            //    }

            //    string str性别 = null;
            //    if (comboBoxEdit性别.EditValue != null)
            //    {
            //        str性别 = util.ControlsHelper.GetComboxKey(comboBoxEdit性别);
            //    }

            //    DataTable dt = bll儿童.GetSummaryByParam(treeListLookUpEdit机构.EditValue.ToString(), checkEdit含下属机构.Checked, util.DESEncrypt.DES加密(textEdit姓名.Text), str性别
            //                                            , dateEdit出生时间开始.Text, dateEdit出生时间截止.Text, str档案状态, str录入人
            //                                            ,dateEdit录入时间开始.Text, dateEdit录入时间截止.Text,textEdit档案编号.Text, textEdit身份证号.Text
            //                                            , str是否合格, str镇, str村, textEdit地址.Text);

            //    //解密姓名
            //    if(dt!=null && dt.Rows.Count >0)
            //    {
            //        for(int index=0; index < dt.Rows.Count; index++)
            //        {
            //            dt.Rows[index]["姓名"] = util.DESEncrypt.DES解密(dt.Rows[index]["姓名"].ToString());
            //        }
            //    }

            //    this.dataGridControl1.DataSource = dt;

            //}
            //catch(Exception ex)
            //{
            //    Msg.ShowException(ex);
            //}
            #endregion
            try
            {
                strWhere = "";

                string str所属机构 = treeListLookUpEdit机构.EditValue.ToString();
                this.pagerControl1.InitControl();
                if (checkEdit含下属机构.Checked)
                {
                    if (str所属机构.Length == 12)
                    {
                        strWhere += "and ( 所属机构='" + str所属机构 + "' or substring(所属机构,1,7)+'1'+substring(所属机构,9,7) like '" + str所属机构 + "%' )";
                    }
                    else
                    {
                        strWhere += " and 所属机构 like '" + str所属机构 + "%'";
                    }

                }
                else
                {
                    strWhere += " and 所属机构='" + str所属机构 + "'";
                }

                if (string.IsNullOrWhiteSpace(textEdit姓名.Text))
                { }
                else
                {
                    strWhere += " and (姓名 like '" + this.textEdit姓名.Text.Trim() + "%')";
                }

                string str性别 = null;
                if (comboBoxEdit性别.EditValue != null)
                {
                    str性别 = util.ControlsHelper.GetComboxKey(comboBoxEdit性别);
                    if (string.IsNullOrWhiteSpace(str性别))
                    { }
                    else
                    {
                        strWhere += " and 性别='" + str性别 + "'";
                    }

                }

                if (!(string.IsNullOrWhiteSpace(dateEdit出生时间开始.Text) || string.IsNullOrWhiteSpace(dateEdit出生时间截止.Text)))
                {
                    strWhere += " and 出生日期 between '" + dateEdit出生时间开始.Text + "' and '" + dateEdit出生时间截止.Text + " 23:59:59' ";
                }
                else if (!(string.IsNullOrWhiteSpace(dateEdit出生时间开始.Text)))
                {
                    strWhere += " and 出生日期 >= '" + dateEdit出生时间开始.Text + "'";
                }
                else if (!(string.IsNullOrWhiteSpace(dateEdit出生时间截止.Text)))
                {
                    strWhere += " and 出生日期 <= '" + dateEdit出生时间截止.Text + " 23:59:59'";
                }

                string str档案状态 = null;
                if (combBoxEdit档案状态.Text.Equals("活动"))
                {
                    str档案状态 = "1";
                    strWhere += " and 档案状态='" + str档案状态 + "'";
                }
                else if (combBoxEdit档案状态.Text.Equals("非活动"))
                {
                    str档案状态 = "2";
                    strWhere += " and 档案状态='" + str档案状态 + "'";
                }
                if (!string.IsNullOrEmpty(this.com档案类型.Text.Trim()) && this.com档案类型.Text.Trim() != "请选择")
                {
                    strWhere += " and 档案类别='" + util.ControlsHelper.GetComboxKey(com档案类型) + "'";
                }
                if (lookUpEdit录入人.EditValue == null || string.IsNullOrWhiteSpace(lookUpEdit录入人.EditValue.ToString()))
                { }
                else
                {
                    //str录入人 = lookUpEdit录入人.EditValue.ToString();
                    strWhere += " and 创建人='" + lookUpEdit录入人.EditValue.ToString() + "'";
                }

                if (!(string.IsNullOrWhiteSpace(dateEdit随访时间开始.Text)))
                {
                    strWhere += " and 随访时间 >= '" + dateEdit随访时间开始.Text + "'";
                }
                else if (!(string.IsNullOrWhiteSpace(dateEdit随访时间截止.Text)))
                {
                    strWhere += " and 随访时间 <= '" + dateEdit随访时间截止.Text + " 23:59:59'";
                }
                //2018年11月7日 yfh 筛查当年有体检的儿童
                string notwhere = string.Empty;
                if (chk未体检.Checked)
                {
                    notwhere = " not in ";
                }
                else
                {
                    notwhere = " in ";
                }

                string str血红蛋白where = "";

                if(!string.IsNullOrWhiteSpace(txt血红蛋白1.Text) || !string.IsNullOrWhiteSpace(txt血红蛋白2.Text))
                {
                    str血红蛋白where += " and isnumeric(血红蛋白值)=1 ";
                }
                if (!string.IsNullOrWhiteSpace(txt血红蛋白1.Text))
                {
                    str血红蛋白where += " and cast(血红蛋白值 as numeric(10,3)) >= " + txt血红蛋白1.Text;
                }

                if (!string.IsNullOrWhiteSpace(txt血红蛋白2.Text))
                {
                    str血红蛋白where += " and cast(血红蛋白值 as numeric(10,3)) <= " + txt血红蛋白2.Text;
                }
               
                if (!(string.IsNullOrWhiteSpace(dateEdit查体时间1.Text)) && !(string.IsNullOrWhiteSpace(dateEdit查体时间2.Text)))
                    strWhere += " and 个人档案编号 " + notwhere + " (select distinct 个人档案编号 from vw_儿童13次随访 where 随访时间>='" + dateEdit查体时间1.Text + "' and  随访时间<='" + dateEdit查体时间2.Text + " 23:59:59' " + str血红蛋白where + " )";
                
                if (string.IsNullOrWhiteSpace(textEdit档案编号.Text))
                { }
                else
                {
                    strWhere += " and 个人档案编号 = '" + textEdit档案编号.Text + "'";
                }

                if (string.IsNullOrWhiteSpace(textEdit身份证号.Text))
                { }
                else
                {
                    strWhere += " and 身份证号 = '" + textEdit身份证号.Text + "'";
                }

                //string str是否合格 = null;
                if (comboBoxEdit是否合格.Text.Equals("是"))
                {
                    //str是否合格 = "1";
                    strWhere += " and 缺项='0'";
                }
                else if (comboBoxEdit是否合格.Text.Equals("否"))
                {
                    //str是否合格 = "2";
                    strWhere += " and (缺项<> '0' or 缺项 is null)";
                }
                else
                {
                    //str是否合格 = null;
                }

                //添加儿童中医药健康指导查询条件
                if (cbo中医药健康指导.EditValue == null || cbo中医药健康指导.EditValue.ToString().Contains("请选择"))
                {
                }
                if (cbo中医药健康指导.EditValue != null && cbo中医药健康指导.EditValue.ToString() == "已做")
                {
                    strWhere +="and 个人档案编号 not in (select 个人档案编号 from vw_儿童 where isnull(六月中医药管理,'')='' "
                                        +" and isnull(十二月中医药管理,'')='' "
                                        +" and isnull(十八月中医药管理,'')='' "
                                        +" and isnull(二十四月中医药管理,'')=''"
                                        +" and isnull(三十月中医药管理,'')='' "
                                        +" and isnull(三岁中医药管理,'')='') ";
                }
                else if (cbo中医药健康指导.EditValue != null && cbo中医药健康指导.EditValue.ToString() == "未做")
                {
                    strWhere += " and isnull(六月中医药管理,'')='' "
                                        +" and isnull(十二月中医药管理,'')='' "
                                        +" and isnull(十八月中医药管理,'')='' "
                                        +" and isnull(二十四月中医药管理,'')=''"
                                        +" and isnull(三十月中医药管理,'')='' "
                                        +" and isnull(三岁中医药管理,'')='' ";
                }
                else
                { }
                string str镇 = util.ControlsHelper.GetComboxKey(comboBoxEdit镇);
                if (string.IsNullOrWhiteSpace(str镇))
                { }
                else
                {
                    strWhere += " and 街道='" + str镇 + "' ";
                }

                string str村 = util.ControlsHelper.GetComboxKey(comboBoxEdit村);
                if (string.IsNullOrWhiteSpace(str村))
                { }
                else
                {
                    strWhere += " and 居委会='" + str村 + "' ";
                }

                if (string.IsNullOrWhiteSpace(textEdit地址.Text))
                { }
                else
                {
                    strWhere += " and 居住地址 like '%'+'" + textEdit地址.Text + "'+'%' ";
                }

                string str喂养方式 = util.ControlsHelper.GetComboxKey(comboBoxEdit喂养方式);
                
                if(!string.IsNullOrWhiteSpace(str喂养方式))
                {
                    strWhere += " and 个人档案编号 in (select 个人档案编号 from [dbo].[tb_儿童新生儿访视记录] where [喂养方式]='"+str喂养方式+"')"; 
                }

                BindDataList();
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }

        private void BindDataList()
        {
            //this.pagerControl1.InitControl();
            if (string.IsNullOrWhiteSpace(strWhere))
            {
                return;
            }
            DataSet ds = this.pagerControl1.GetQueryResult("vw_儿童", strWhere);
            this.dataGridControl1.View = "vw_儿童";
            this.dataGridControl1.StrWhere = strWhere;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                ds.Tables[0].Rows[i]["性别"] = _bll地区.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
            }
            this.dataGridControl1.DataSource = ds.Tables[0];
            this.pagerControl1.DrawControl();
            this.gridView1.BestFitColumns();//列自适应宽度
        }

        private void sbtn重置_Click(object sender, EventArgs e)
        {
            //暂时不对“含下属机构”做修改
            //checkEdit含下属机构.Checked = true;

            treeListLookUpEdit机构.EditValue = Loginer.CurrentUser.所属机构;

            textEdit姓名.Text = "";
            comboBoxEdit性别.EditValue = null;

            dateEdit出生时间开始.Text = "";
            dateEdit出生时间截止.Text = "";
            combBoxEdit档案状态.Text = "活动";
            lookUpEdit录入人.EditValue = "";

            dateEdit随访时间开始.Text = "";
            dateEdit随访时间截止.Text = "";
            textEdit档案编号.Text = "";
            textEdit身份证号.Text = "";

            textEdit年龄开始.Text = "";
            textEdit年龄截止.Text = "";
            comboBoxEdit是否合格.Text = "请选择";

            comboBoxEdit镇.EditValue = null;
        }

        private void sbtn修改_Click(object sender, EventArgs e)
        {
            int[] indexs = this.gridView1.GetSelectedRows();

            if (indexs.Length > 1)
            {
                Msg.ShowInformation("只能选择一条记录！");
                return;
            }

            if (indexs.Length == 0)
            {
                Msg.ShowInformation("请选择一条记录！");
                return;
            }

            DataRow row = this.gridView1.GetFocusedDataRow();
            if (row == null)
            {
                Msg.ShowInformation("没有选择任何行");
                return;
            }
            else
            {
                string 家庭档案编号 = row["家庭档案编号"] as string;
                string 个人档案编号 = row["个人档案编号"] as string;
                //_BLL.GetBusinessByKey(个人档案编号, true);//下载一个空业务单据            
                //_BLL.NewBusiness(); //增加一条主表记录
                frm个人健康 frm = new frm个人健康(true, this.Name, 家庭档案编号, 个人档案编号, "SaveAndClose");
                frm.ShowDialog();
            }
        }

        private void sbtn删除_Click(object sender, EventArgs e)
        {

            int[] rowIndexs = this.gridView1.GetSelectedRows();

            if (rowIndexs.Length == 0)
            {
                Msg.ShowInformation("请至少选择一条记录！");
                return;
            }

            //跨机构修改
            //bool isallow = DataDictCache.Cache.IsAllow跨机构修改();
            //if(isallow)
            {
                for (int inner = 0; inner < rowIndexs.Length; inner++)
                {
                    string prgid = this.gridView1.GetRowCellValue(rowIndexs[inner], tb_儿童基本信息.所属机构).ToString();
                    if (prgid != Loginer.CurrentUser.所属机构)
                    {
                        Msg.ShowInformation("对不起，您只能对本机构的业务数据进行此操作！");
                        return;
                    }
                }
            }

            //获取服务器时间,判断是否所有要删除的儿童年龄是否有小于等于6岁的情况，若有，停止删除操作
            string strDateTime = new bllCom().GetDateTimeFromDBServer();
            for (int inner = 0; inner < rowIndexs.Length; inner++)
            {
                string strBirth = this.gridView1.GetRowCellValue(rowIndexs[inner], tb_儿童基本信息.出生日期).ToString();
                if (util.ControlsHelper.GetAge(strBirth, strDateTime) <= 6)
                {
                    Msg.ShowInformation("对不起，儿童时不能删除儿童基本信息表");
                    return;
                }
            }

            //根据个人档案编号删除儿童基本信息
            //string selectedGrdabh = "";
            string msg = "";
            bll儿童基本信息 bll儿童 = new bll儿童基本信息();
            for (int index = 0; index < rowIndexs.Length; index++)
            {
                string delGrdabh = this.gridView1.GetRowCellValue(index, tb_儿童基本信息.个人档案编号).ToString();
                string delid = this.gridView1.GetRowCellValue(index, tb_儿童基本信息.ID).ToString();
                bool bsuccess = bll儿童.DeleteByGrdabh(delid, delGrdabh);
                if (bsuccess == false)
                {
                    msg += "档案号为" + delGrdabh + "的档案删除成功！\n";
                }
                else
                {
                    msg += "档案号为" + delGrdabh + "的档案删除失败！\n";
                }
            }

            Msg.ShowInformation(msg);

            sbtn查询_Click(null, null);
        }

        private void comboBoxEdit镇_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            DataRow row = this.gridView1.GetFocusedDataRow();
            if (row == null)
            {
                Msg.ShowInformation("没有选择任何行");
                return;
            }
            else
            {
                string 家庭档案编号 = row["家庭档案编号"] as string;
                string 个人档案编号 = row["个人档案编号"] as string;
                //_BLL.GetBusinessByKey(个人档案编号, true);//下载一个空业务单据            
                //_BLL.NewBusiness(); //增加一条主表记录
                frm个人健康 frm = new frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, null);
                frm.ShowDialog();
            }
        }

        private void textEdit档案编号_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textEdit档案编号.Text))
            {
                return;
            }

            int len = this.textEdit档案编号.Text.Length;
            if (len < 17)
            {
                Msg.ShowInformation("请输入一个长度最少是17位的编号");
                textEdit档案编号.Focus();
            }
            else if (len > 18)
            {
                Msg.ShowInformation("请输入一个长度最多是18位的编号");
                textEdit档案编号.Focus();
            }

        }

        private void textEdit身份证号_Leave(object sender, EventArgs e)
        {
            //身份证验证
            if (string.IsNullOrWhiteSpace(textEdit身份证号.Text))
            {
            }
            else
            {
                bool bRet = util.ControlsHelper.Check身份证号(textEdit身份证号.Text);
                if (bRet == false)
                {
                    Msg.ShowInformation("请输入一个合法的身份证号！");
                    textEdit身份证号.Focus();
                }
            }
        }

        private void dateEdit录入时间开始_EditValueChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(dateEdit随访时间开始.Text))
            { }
            else
            {
                if (string.IsNullOrWhiteSpace(dateEdit随访时间截止.Text))
                { }
                else
                {
                    try
                    {
                        DateTime dtime录入开始 = Convert.ToDateTime(dateEdit随访时间开始.Text);
                        DateTime dtime录入截止 = Convert.ToDateTime(dateEdit随访时间截止.Text);
                        if ((dtime录入开始 - dtime录入截止).TotalDays > 0)
                        {
                            Msg.Warning("输入的日期大于当前日期！请重新输入！");
                            dateEdit随访时间开始.Text = "";
                        }
                    }
                    catch
                    {

                    }
                }
            }
        }

        private void dateEdit录入时间截止_EditValueChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(dateEdit随访时间截止.Text))
            { }
            else
            {
                if (string.IsNullOrWhiteSpace(dateEdit随访时间开始.Text))
                { }
                else
                {
                    //录入时间的结束日期必须大于录入时间的开始日期！
                    try
                    {
                        DateTime dtime录入开始 = Convert.ToDateTime(dateEdit随访时间开始.Text);
                        DateTime dtime录入截止 = Convert.ToDateTime(dateEdit随访时间截止.Text);
                        if ((dtime录入开始 - dtime录入截止).TotalDays > 0)
                        {
                            Msg.Warning("录入时间的结束日期必须大于录入时间的开始日期！");
                            dateEdit随访时间截止.Text = "";
                        }
                    }
                    catch
                    {

                    }
                }
            }
        }

        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            BindDataList();
        }

        private void treeListLookUpEdit机构_EditValueChanged(object sender, EventArgs e)
        {
            //绑定“录入人”绑定信息
            try
            {
                DataTable dt录入人 = new bllUser().GetUserNameAndIDBy机构用户(treeListLookUpEdit机构.EditValue.ToString());
                lookUpEdit录入人.Properties.DisplayMember = "UserName";
                lookUpEdit录入人.Properties.ValueMember = "用户编码";
                lookUpEdit录入人.Properties.DataSource = dt录入人;
            }
            catch
            {
                lookUpEdit录入人.Text = "请选择...";
            }
        }

        private void sbtn生长发育监测图_Click(object sender, EventArgs e)
        {
            int[] indexs = this.gridView1.GetSelectedRows();

            if (indexs.Length > 1)
            {
                Msg.ShowInformation("只能选择一条记录！");
                return;
            }

            if (indexs.Length == 0)
            {
                Msg.ShowInformation("请选择一条记录！");
                return;
            }

            DataRow row = this.gridView1.GetFocusedDataRow();
            if (row == null)
            {
                Msg.ShowInformation("没有选择任何行");
                return;
            }
            else
            {
                string 家庭档案编号 = row["家庭档案编号"] as string;
                string 个人档案编号 = row["个人档案编号"] as string;
                string name = row["姓名"] as string;
                string sex = row["性别"] as string;

                DataTable dtBase = Get儿童基础数据(sex);
                DataTable dtGrow = Get儿童发育数据(个人档案编号);

                List<个人健康.儿童健康信息.ImagePoint> list身高 = new List<个人健康.儿童健康信息.ImagePoint>();
                List<个人健康.儿童健康信息.ImagePoint> list体重 = new List<个人健康.儿童健康信息.ImagePoint>();
                for(int index = 0; index < dtGrow.Rows.Count; index++)
                {
                    decimal d身高 = 0m;
                    try
                    {
                        d身高 = Convert.ToDecimal(dtGrow.Rows[index]["CM"].ToString());
                    }
                    catch
                    { }
                    decimal d体重 = 0m;
                    try
                    {
                        d体重 = Convert.ToDecimal(dtGrow.Rows[index]["KG"].ToString());
                    }
                    catch
                    {}

                    list身高.Add(new 个人健康.儿童健康信息.ImagePoint((int)(dtGrow.Rows[index]["Month"]), d身高));
                    list体重.Add(new 个人健康.儿童健康信息.ImagePoint((int)(dtGrow.Rows[index]["Month"]), d体重));
                }

                个人健康.儿童健康信息.Frm儿童生长发育图 frm
                    = new 个人健康.儿童健康信息.Frm儿童生长发育图(dtBase, name, 个人档案编号, sex, list体重, list身高);
                frm.ShowDialog();
            }
        }

        private DataTable m_男孩基础数据 = null;
        private DataTable m_女孩基础数据 = null;
        bll儿童基本信息 bll儿童2 = new bll儿童基本信息();
        private DataTable Get儿童基础数据(string sex)
        {
            if (sex == "女")
            {
                if (m_女孩基础数据 == null)
                {
                    m_女孩基础数据 = bll儿童2.GetBaseData(sex);
                }
                return m_女孩基础数据;
            }
            else
            {
                if (m_男孩基础数据 == null)
                {
                    m_男孩基础数据 = bll儿童2.GetBaseData("男");
                }
                return m_男孩基础数据;
            }
        }

        private DataTable Get儿童发育数据(string dah)
        {
            DataTable dtGrownData = bll儿童2.GetGrownData(dah);
            return dtGrownData;
        }
    }
}
