﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.基本公共卫生.儿童健康管理
{
    public partial class frm儿童入托检查表 : DevExpress.XtraEditors.XtraForm
    {
        public frm儿童入托检查表()
        {
            InitializeComponent();
        }

        private void layoutControlGroup1_Click(object sender, EventArgs e)
        {

        }

        private void frm儿童基本信息_Load(object sender, EventArgs e)
        {
            InitView();
        }
        /// <summary>
        /// 初始化页面样式
        /// </summary>
        private void InitView()
        {
            //this.uC居住地址1.Cbo1.Width = 100;
            //this.uC居住地址1.Cbo2.Width = 200;
            //this.uC居住地址1.Txt1.Width = 100;

            this.ucTxtToTxt1.Txt1.Width = 100;
            this.ucTxtToTxt1.Txt2.Width = 100;
            this.pagerControl1.Height = 38;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.pagerControl1.InitControl();

            this.gridView1.BestFitColumns();//列自适应宽度
        }
    }
}
