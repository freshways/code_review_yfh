﻿namespace AtomEHR.公共卫生.Module.基本公共卫生.重点人群服务.儿童健康管理
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkEdit67 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit66 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit64 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit63 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit65 = new DevExpress.XtraEditors.CheckEdit();
            this.flowLayoutPanel31 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit68 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit69 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit39 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel30 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit58 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit59 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit60 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit61 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit62 = new DevExpress.XtraEditors.CheckEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.textEdit38 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit37 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl80 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl79 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit36 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel25 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit38 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit39 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit40 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit41 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit31 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel23 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit30 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit31 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit32 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit33 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit27 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel28 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit50 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit51 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit52 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit53 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit34 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel29 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit54 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit55 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit56 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit57 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit35 = new DevExpress.XtraEditors.TextEdit();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.lbl儿童姓名 = new DevExpress.XtraEditors.LabelControl();
            this.lbl性别 = new DevExpress.XtraEditors.LabelControl();
            this.lbl住址 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.lbl儿童档案号 = new DevExpress.XtraEditors.LabelControl();
            this.lbl身份证号 = new DevExpress.XtraEditors.LabelControl();
            this.lbl出生日期 = new DevExpress.XtraEditors.LabelControl();
            this.lbl联系电话 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.lbl父亲姓名 = new DevExpress.XtraEditors.LabelControl();
            this.lbl母亲姓名 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.lbl父亲出生日期 = new DevExpress.XtraEditors.LabelControl();
            this.lbl母亲出生日期 = new DevExpress.XtraEditors.LabelControl();
            this.lablecontrol = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.lbl父亲职业 = new DevExpress.XtraEditors.LabelControl();
            this.lbl母亲职业 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.dateEdit3 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl72 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl71 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl70 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl69 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl68 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl67 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit28 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl66 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl65 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl64 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel21 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit21 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit26 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl63 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl62 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl61 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl60 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel20 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit20 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit25 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel19 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit19 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit24 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel18 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit18 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit23 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel17 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit17 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit22 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl59 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl58 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl57 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl56 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel16 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit16 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit21 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel15 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit15 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit20 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl55 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl54 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel13 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit14 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit18 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit13 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit17 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit12 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit16 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl52 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl51 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit11 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit15 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit10 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit14 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl49 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit9 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl46 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLbl8 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.ucTxtLbl7 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit5 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit6 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit22 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.ucTxtLbl6 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.comboBoxEdit7 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit6 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLbl5 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.ucTxtLbl4 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.comboBoxEdit5 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit4 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit15 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit16 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit17 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit18 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit19 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit20 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit8 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit9 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit10 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit11 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit12 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit13 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit14 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEdit3 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.ucTxtLbl2 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.ucTxtLbl3 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.ucTxtLbl1 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.ucTxtLbl9 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.dateEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl73 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl74 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl75 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl76 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl77 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl78 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit8 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.flowLayoutPanel14 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit7 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit21 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit23 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit24 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit19 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel24 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit34 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit35 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit36 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit37 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit30 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel26 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit42 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit43 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit44 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit45 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit32 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel27 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit46 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit47 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit48 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit49 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit33 = new DevExpress.XtraEditors.TextEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.ucLblTxt2 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.ucLblTxt1 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.flowLayoutPanel22 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit25 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit26 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit27 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit28 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit29 = new DevExpress.XtraEditors.CheckEdit();
            this.layoutConverter1 = new DevExpress.XtraLayout.Converter.LayoutConverter(this.components);
            this.xtraScrollableControl1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit67.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit66.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit64.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit63.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit65.Properties)).BeginInit();
            this.flowLayoutPanel31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit68.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit69.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit39.Properties)).BeginInit();
            this.flowLayoutPanel30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit58.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit59.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit60.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit61.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit62.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit38.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit37.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit36.Properties)).BeginInit();
            this.flowLayoutPanel25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit38.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit39.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit40.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit41.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit31.Properties)).BeginInit();
            this.flowLayoutPanel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit30.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit32.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit33.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit27.Properties)).BeginInit();
            this.flowLayoutPanel28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit50.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit51.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit52.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit53.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit34.Properties)).BeginInit();
            this.flowLayoutPanel29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit54.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit55.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit56.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit57.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit35.Properties)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit28.Properties)).BeginInit();
            this.flowLayoutPanel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit26.Properties)).BeginInit();
            this.flowLayoutPanel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit25.Properties)).BeginInit();
            this.flowLayoutPanel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit24.Properties)).BeginInit();
            this.flowLayoutPanel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit23.Properties)).BeginInit();
            this.flowLayoutPanel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit22.Properties)).BeginInit();
            this.flowLayoutPanel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit21.Properties)).BeginInit();
            this.flowLayoutPanel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit20.Properties)).BeginInit();
            this.flowLayoutPanel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).BeginInit();
            this.flowLayoutPanel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit17.Properties)).BeginInit();
            this.flowLayoutPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).BeginInit();
            this.flowLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).BeginInit();
            this.flowLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            this.flowLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            this.flowLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            this.flowLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit4.Properties)).BeginInit();
            this.flowLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            this.flowLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit8.Properties)).BeginInit();
            this.flowLayoutPanel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit19.Properties)).BeginInit();
            this.flowLayoutPanel24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit34.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit35.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit36.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit37.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit30.Properties)).BeginInit();
            this.flowLayoutPanel26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit43.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit44.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit45.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit32.Properties)).BeginInit();
            this.flowLayoutPanel27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit46.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit47.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit48.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit49.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit33.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.flowLayoutPanel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit27.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit28.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit29.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.Controls.Add(this.panel1);
            this.xtraScrollableControl1.Controls.Add(this.flowLayoutPanel31);
            this.xtraScrollableControl1.Controls.Add(this.flowLayoutPanel30);
            this.xtraScrollableControl1.Controls.Add(this.panelControl1);
            this.xtraScrollableControl1.Controls.Add(this.flowLayoutPanel25);
            this.xtraScrollableControl1.Controls.Add(this.flowLayoutPanel23);
            this.xtraScrollableControl1.Controls.Add(this.flowLayoutPanel28);
            this.xtraScrollableControl1.Controls.Add(this.tableLayoutPanel1);
            this.xtraScrollableControl1.Controls.Add(this.tableLayoutPanel2);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(1122, 458);
            this.xtraScrollableControl1.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.checkEdit67);
            this.panel1.Controls.Add(this.checkEdit66);
            this.panel1.Controls.Add(this.checkEdit64);
            this.panel1.Controls.Add(this.checkEdit63);
            this.panel1.Controls.Add(this.checkEdit65);
            this.panel1.Location = new System.Drawing.Point(758, 85);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(323, 45);
            this.panel1.TabIndex = 81;
            // 
            // checkEdit67
            // 
            this.checkEdit67.Location = new System.Drawing.Point(209, 1);
            this.checkEdit67.Name = "checkEdit67";
            this.checkEdit67.Properties.Caption = "双多胎";
            this.checkEdit67.Size = new System.Drawing.Size(63, 19);
            this.checkEdit67.TabIndex = 4;
            // 
            // checkEdit66
            // 
            this.checkEdit66.Location = new System.Drawing.Point(159, 1);
            this.checkEdit66.Name = "checkEdit66";
            this.checkEdit66.Properties.Caption = "剖宫";
            this.checkEdit66.Size = new System.Drawing.Size(44, 19);
            this.checkEdit66.TabIndex = 3;
            // 
            // checkEdit64
            // 
            this.checkEdit64.Location = new System.Drawing.Point(49, 1);
            this.checkEdit64.Name = "checkEdit64";
            this.checkEdit64.Properties.Caption = "头吸";
            this.checkEdit64.Size = new System.Drawing.Size(50, 19);
            this.checkEdit64.TabIndex = 1;
            // 
            // checkEdit63
            // 
            this.checkEdit63.Location = new System.Drawing.Point(1, 1);
            this.checkEdit63.Name = "checkEdit63";
            this.checkEdit63.Properties.Caption = "顺产";
            this.checkEdit63.Size = new System.Drawing.Size(50, 19);
            this.checkEdit63.TabIndex = 0;
            // 
            // checkEdit65
            // 
            this.checkEdit65.Location = new System.Drawing.Point(106, 1);
            this.checkEdit65.Name = "checkEdit65";
            this.checkEdit65.Properties.Caption = "产钳";
            this.checkEdit65.Size = new System.Drawing.Size(48, 19);
            this.checkEdit65.TabIndex = 2;
            // 
            // flowLayoutPanel31
            // 
            this.flowLayoutPanel31.Controls.Add(this.checkEdit68);
            this.flowLayoutPanel31.Controls.Add(this.checkEdit69);
            this.flowLayoutPanel31.Controls.Add(this.textEdit39);
            this.flowLayoutPanel31.Location = new System.Drawing.Point(758, 25);
            this.flowLayoutPanel31.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel31.Name = "flowLayoutPanel31";
            this.flowLayoutPanel31.Size = new System.Drawing.Size(414, 51);
            this.flowLayoutPanel31.TabIndex = 80;
            // 
            // checkEdit68
            // 
            this.checkEdit68.Location = new System.Drawing.Point(3, 3);
            this.checkEdit68.Name = "checkEdit68";
            this.checkEdit68.Properties.Caption = "臀位";
            this.checkEdit68.Size = new System.Drawing.Size(51, 19);
            this.checkEdit68.TabIndex = 5;
            // 
            // checkEdit69
            // 
            this.checkEdit69.Location = new System.Drawing.Point(60, 3);
            this.checkEdit69.Name = "checkEdit69";
            this.checkEdit69.Properties.Caption = "其他";
            this.checkEdit69.Size = new System.Drawing.Size(53, 19);
            this.checkEdit69.TabIndex = 6;
            // 
            // textEdit39
            // 
            this.textEdit39.Location = new System.Drawing.Point(119, 3);
            this.textEdit39.Name = "textEdit39";
            this.textEdit39.Size = new System.Drawing.Size(100, 20);
            this.textEdit39.TabIndex = 14;
            // 
            // flowLayoutPanel30
            // 
            this.flowLayoutPanel30.Controls.Add(this.checkEdit58);
            this.flowLayoutPanel30.Controls.Add(this.checkEdit59);
            this.flowLayoutPanel30.Controls.Add(this.checkEdit60);
            this.flowLayoutPanel30.Controls.Add(this.checkEdit61);
            this.flowLayoutPanel30.Controls.Add(this.checkEdit62);
            this.flowLayoutPanel30.Location = new System.Drawing.Point(765, 591);
            this.flowLayoutPanel30.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel30.Name = "flowLayoutPanel30";
            this.flowLayoutPanel30.Size = new System.Drawing.Size(455, 25);
            this.flowLayoutPanel30.TabIndex = 79;
            // 
            // checkEdit58
            // 
            this.checkEdit58.Location = new System.Drawing.Point(3, 3);
            this.checkEdit58.Name = "checkEdit58";
            this.checkEdit58.Properties.Caption = "科学喂养";
            this.checkEdit58.Size = new System.Drawing.Size(79, 19);
            this.checkEdit58.TabIndex = 0;
            // 
            // checkEdit59
            // 
            this.checkEdit59.Location = new System.Drawing.Point(88, 3);
            this.checkEdit59.Name = "checkEdit59";
            this.checkEdit59.Properties.Caption = "生长发育";
            this.checkEdit59.Size = new System.Drawing.Size(72, 19);
            this.checkEdit59.TabIndex = 1;
            // 
            // checkEdit60
            // 
            this.checkEdit60.Location = new System.Drawing.Point(166, 3);
            this.checkEdit60.Name = "checkEdit60";
            this.checkEdit60.Properties.Caption = "疾病预防";
            this.checkEdit60.Size = new System.Drawing.Size(75, 19);
            this.checkEdit60.TabIndex = 2;
            // 
            // checkEdit61
            // 
            this.checkEdit61.Location = new System.Drawing.Point(247, 3);
            this.checkEdit61.Name = "checkEdit61";
            this.checkEdit61.Properties.Caption = "预防意外伤害";
            this.checkEdit61.Size = new System.Drawing.Size(99, 19);
            this.checkEdit61.TabIndex = 5;
            // 
            // checkEdit62
            // 
            this.checkEdit62.Location = new System.Drawing.Point(352, 3);
            this.checkEdit62.Name = "checkEdit62";
            this.checkEdit62.Properties.Caption = "口腔保健";
            this.checkEdit62.Size = new System.Drawing.Size(83, 19);
            this.checkEdit62.TabIndex = 6;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.textEdit38);
            this.panelControl1.Controls.Add(this.textEdit37);
            this.panelControl1.Controls.Add(this.labelControl80);
            this.panelControl1.Controls.Add(this.labelControl79);
            this.panelControl1.Controls.Add(this.textEdit36);
            this.panelControl1.Location = new System.Drawing.Point(740, 237);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(240, 73);
            this.panelControl1.TabIndex = 78;
            // 
            // textEdit38
            // 
            this.textEdit38.Location = new System.Drawing.Point(82, 50);
            this.textEdit38.Name = "textEdit38";
            this.textEdit38.Size = new System.Drawing.Size(114, 20);
            this.textEdit38.TabIndex = 3;
            // 
            // textEdit37
            // 
            this.textEdit37.Location = new System.Drawing.Point(82, 27);
            this.textEdit37.Name = "textEdit37";
            this.textEdit37.Size = new System.Drawing.Size(114, 20);
            this.textEdit37.TabIndex = 2;
            // 
            // labelControl80
            // 
            this.labelControl80.Location = new System.Drawing.Point(4, 53);
            this.labelControl80.Name = "labelControl80";
            this.labelControl80.Size = new System.Drawing.Size(72, 14);
            this.labelControl80.TabIndex = 1;
            this.labelControl80.Text = "机构及科室：";
            // 
            // labelControl79
            // 
            this.labelControl79.Location = new System.Drawing.Point(4, 30);
            this.labelControl79.Name = "labelControl79";
            this.labelControl79.Size = new System.Drawing.Size(72, 14);
            this.labelControl79.TabIndex = 1;
            this.labelControl79.Text = "原　　　因：";
            // 
            // textEdit36
            // 
            this.textEdit36.Location = new System.Drawing.Point(3, 3);
            this.textEdit36.Name = "textEdit36";
            this.textEdit36.Size = new System.Drawing.Size(115, 20);
            this.textEdit36.TabIndex = 0;
            // 
            // flowLayoutPanel25
            // 
            this.flowLayoutPanel25.Controls.Add(this.checkEdit38);
            this.flowLayoutPanel25.Controls.Add(this.checkEdit39);
            this.flowLayoutPanel25.Controls.Add(this.checkEdit40);
            this.flowLayoutPanel25.Controls.Add(this.checkEdit41);
            this.flowLayoutPanel25.Controls.Add(this.textEdit31);
            this.flowLayoutPanel25.Location = new System.Drawing.Point(0, -189);
            this.flowLayoutPanel25.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel25.Name = "flowLayoutPanel25";
            this.flowLayoutPanel25.Size = new System.Drawing.Size(414, 26);
            this.flowLayoutPanel25.TabIndex = 19;
            // 
            // checkEdit38
            // 
            this.checkEdit38.Location = new System.Drawing.Point(3, 3);
            this.checkEdit38.Name = "checkEdit38";
            this.checkEdit38.Properties.Caption = "未见异常";
            this.checkEdit38.Size = new System.Drawing.Size(79, 19);
            this.checkEdit38.TabIndex = 0;
            // 
            // checkEdit39
            // 
            this.checkEdit39.Location = new System.Drawing.Point(88, 3);
            this.checkEdit39.Name = "checkEdit39";
            this.checkEdit39.Properties.Caption = "湿疹";
            this.checkEdit39.Size = new System.Drawing.Size(52, 19);
            this.checkEdit39.TabIndex = 1;
            // 
            // checkEdit40
            // 
            this.checkEdit40.Location = new System.Drawing.Point(146, 3);
            this.checkEdit40.Name = "checkEdit40";
            this.checkEdit40.Properties.Caption = "糜烂";
            this.checkEdit40.Size = new System.Drawing.Size(57, 19);
            this.checkEdit40.TabIndex = 2;
            // 
            // checkEdit41
            // 
            this.checkEdit41.Location = new System.Drawing.Point(209, 3);
            this.checkEdit41.Name = "checkEdit41";
            this.checkEdit41.Properties.Caption = "其他";
            this.checkEdit41.Size = new System.Drawing.Size(54, 19);
            this.checkEdit41.TabIndex = 5;
            // 
            // textEdit31
            // 
            this.textEdit31.Location = new System.Drawing.Point(269, 3);
            this.textEdit31.Name = "textEdit31";
            this.textEdit31.Size = new System.Drawing.Size(100, 20);
            this.textEdit31.TabIndex = 14;
            // 
            // flowLayoutPanel23
            // 
            this.flowLayoutPanel23.Controls.Add(this.checkEdit30);
            this.flowLayoutPanel23.Controls.Add(this.checkEdit31);
            this.flowLayoutPanel23.Controls.Add(this.checkEdit32);
            this.flowLayoutPanel23.Controls.Add(this.checkEdit33);
            this.flowLayoutPanel23.Controls.Add(this.textEdit27);
            this.flowLayoutPanel23.Location = new System.Drawing.Point(758, 193);
            this.flowLayoutPanel23.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel23.Name = "flowLayoutPanel23";
            this.flowLayoutPanel23.Size = new System.Drawing.Size(192, 42);
            this.flowLayoutPanel23.TabIndex = 17;
            // 
            // checkEdit30
            // 
            this.checkEdit30.Location = new System.Drawing.Point(0, 0);
            this.checkEdit30.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit30.Name = "checkEdit30";
            this.checkEdit30.Properties.Caption = "未检";
            this.checkEdit30.Size = new System.Drawing.Size(51, 19);
            this.checkEdit30.TabIndex = 0;
            // 
            // checkEdit31
            // 
            this.checkEdit31.Location = new System.Drawing.Point(51, 0);
            this.checkEdit31.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit31.Name = "checkEdit31";
            this.checkEdit31.Properties.Caption = "红润";
            this.checkEdit31.Size = new System.Drawing.Size(52, 19);
            this.checkEdit31.TabIndex = 1;
            // 
            // checkEdit32
            // 
            this.checkEdit32.Location = new System.Drawing.Point(103, 0);
            this.checkEdit32.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit32.Name = "checkEdit32";
            this.checkEdit32.Properties.Caption = "黄染";
            this.checkEdit32.Size = new System.Drawing.Size(84, 19);
            this.checkEdit32.TabIndex = 2;
            // 
            // checkEdit33
            // 
            this.checkEdit33.Location = new System.Drawing.Point(0, 19);
            this.checkEdit33.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit33.Name = "checkEdit33";
            this.checkEdit33.Properties.Caption = "其他";
            this.checkEdit33.Size = new System.Drawing.Size(54, 19);
            this.checkEdit33.TabIndex = 5;
            // 
            // textEdit27
            // 
            this.textEdit27.Location = new System.Drawing.Point(54, 19);
            this.textEdit27.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit27.Name = "textEdit27";
            this.textEdit27.Size = new System.Drawing.Size(100, 20);
            this.textEdit27.TabIndex = 14;
            // 
            // flowLayoutPanel28
            // 
            this.flowLayoutPanel28.Controls.Add(this.checkEdit50);
            this.flowLayoutPanel28.Controls.Add(this.checkEdit51);
            this.flowLayoutPanel28.Controls.Add(this.checkEdit52);
            this.flowLayoutPanel28.Controls.Add(this.checkEdit53);
            this.flowLayoutPanel28.Controls.Add(this.textEdit34);
            this.flowLayoutPanel28.Controls.Add(this.flowLayoutPanel29);
            this.flowLayoutPanel28.Location = new System.Drawing.Point(758, 142);
            this.flowLayoutPanel28.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel28.Name = "flowLayoutPanel28";
            this.flowLayoutPanel28.Size = new System.Drawing.Size(378, 26);
            this.flowLayoutPanel28.TabIndex = 20;
            // 
            // checkEdit50
            // 
            this.checkEdit50.Location = new System.Drawing.Point(3, 3);
            this.checkEdit50.Name = "checkEdit50";
            this.checkEdit50.Properties.Caption = "未见异常";
            this.checkEdit50.Size = new System.Drawing.Size(79, 19);
            this.checkEdit50.TabIndex = 0;
            // 
            // checkEdit51
            // 
            this.checkEdit51.Location = new System.Drawing.Point(88, 3);
            this.checkEdit51.Name = "checkEdit51";
            this.checkEdit51.Properties.Caption = "湿疹";
            this.checkEdit51.Size = new System.Drawing.Size(52, 19);
            this.checkEdit51.TabIndex = 1;
            // 
            // checkEdit52
            // 
            this.checkEdit52.Location = new System.Drawing.Point(146, 3);
            this.checkEdit52.Name = "checkEdit52";
            this.checkEdit52.Properties.Caption = "糜烂";
            this.checkEdit52.Size = new System.Drawing.Size(57, 19);
            this.checkEdit52.TabIndex = 2;
            // 
            // checkEdit53
            // 
            this.checkEdit53.Location = new System.Drawing.Point(209, 3);
            this.checkEdit53.Name = "checkEdit53";
            this.checkEdit53.Properties.Caption = "其他";
            this.checkEdit53.Size = new System.Drawing.Size(54, 19);
            this.checkEdit53.TabIndex = 5;
            // 
            // textEdit34
            // 
            this.textEdit34.Location = new System.Drawing.Point(269, 3);
            this.textEdit34.Name = "textEdit34";
            this.textEdit34.Size = new System.Drawing.Size(100, 20);
            this.textEdit34.TabIndex = 14;
            // 
            // flowLayoutPanel29
            // 
            this.flowLayoutPanel29.Controls.Add(this.checkEdit54);
            this.flowLayoutPanel29.Controls.Add(this.checkEdit55);
            this.flowLayoutPanel29.Controls.Add(this.checkEdit56);
            this.flowLayoutPanel29.Controls.Add(this.checkEdit57);
            this.flowLayoutPanel29.Controls.Add(this.textEdit35);
            this.flowLayoutPanel29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel29.Location = new System.Drawing.Point(0, 26);
            this.flowLayoutPanel29.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel29.Name = "flowLayoutPanel29";
            this.flowLayoutPanel29.Size = new System.Drawing.Size(414, 0);
            this.flowLayoutPanel29.TabIndex = 18;
            // 
            // checkEdit54
            // 
            this.checkEdit54.Location = new System.Drawing.Point(3, 3);
            this.checkEdit54.Name = "checkEdit54";
            this.checkEdit54.Properties.Caption = "未见异常";
            this.checkEdit54.Size = new System.Drawing.Size(79, 19);
            this.checkEdit54.TabIndex = 0;
            // 
            // checkEdit55
            // 
            this.checkEdit55.Location = new System.Drawing.Point(88, 3);
            this.checkEdit55.Name = "checkEdit55";
            this.checkEdit55.Properties.Caption = "湿疹";
            this.checkEdit55.Size = new System.Drawing.Size(52, 19);
            this.checkEdit55.TabIndex = 1;
            // 
            // checkEdit56
            // 
            this.checkEdit56.Location = new System.Drawing.Point(146, 3);
            this.checkEdit56.Name = "checkEdit56";
            this.checkEdit56.Properties.Caption = "糜烂";
            this.checkEdit56.Size = new System.Drawing.Size(57, 19);
            this.checkEdit56.TabIndex = 2;
            // 
            // checkEdit57
            // 
            this.checkEdit57.Location = new System.Drawing.Point(209, 3);
            this.checkEdit57.Name = "checkEdit57";
            this.checkEdit57.Properties.Caption = "其他";
            this.checkEdit57.Size = new System.Drawing.Size(54, 19);
            this.checkEdit57.TabIndex = 5;
            // 
            // textEdit35
            // 
            this.textEdit35.Location = new System.Drawing.Point(269, 3);
            this.textEdit35.Name = "textEdit35";
            this.textEdit35.Size = new System.Drawing.Size(100, 20);
            this.textEdit35.TabIndex = 14;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 8;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 73F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 96F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 87F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.labelControl1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelControl2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelControl3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelControl4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelControl5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lbl儿童姓名, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl性别, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbl住址, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.labelControl14, 4, 4);
            this.tableLayoutPanel1.Controls.Add(this.labelControl15, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelControl16, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelControl17, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl儿童档案号, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl身份证号, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl出生日期, 5, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbl联系电话, 5, 4);
            this.tableLayoutPanel1.Controls.Add(this.labelControl27, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.labelControl28, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.lbl父亲姓名, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.lbl母亲姓名, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.labelControl31, 4, 5);
            this.tableLayoutPanel1.Controls.Add(this.labelControl32, 4, 6);
            this.tableLayoutPanel1.Controls.Add(this.labelControl33, 5, 5);
            this.tableLayoutPanel1.Controls.Add(this.labelControl34, 5, 6);
            this.tableLayoutPanel1.Controls.Add(this.lbl父亲出生日期, 7, 5);
            this.tableLayoutPanel1.Controls.Add(this.lbl母亲出生日期, 7, 6);
            this.tableLayoutPanel1.Controls.Add(this.lablecontrol, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.labelControl10, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.lbl父亲职业, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.lbl母亲职业, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.labelControl25, 6, 5);
            this.tableLayoutPanel1.Controls.Add(this.labelControl24, 6, 6);
            this.tableLayoutPanel1.Controls.Add(this.textEdit1, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, -567);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(730, 188);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.tableLayoutPanel1.SetColumnSpan(this.labelControl1, 8);
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl1.Location = new System.Drawing.Point(1, 1);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(728, 27);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "新生儿家庭访视记录表 ";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl2.Location = new System.Drawing.Point(1, 29);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(73, 21);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "卡号:";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl3.Location = new System.Drawing.Point(1, 51);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(73, 22);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "儿童姓名:";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl4.Location = new System.Drawing.Point(1, 74);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(73, 20);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "性别:";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl5.Location = new System.Drawing.Point(1, 95);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(73, 35);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "住址:";
            // 
            // lbl儿童姓名
            // 
            this.lbl儿童姓名.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl儿童姓名.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.tableLayoutPanel1.SetColumnSpan(this.lbl儿童姓名, 3);
            this.lbl儿童姓名.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl儿童姓名.Location = new System.Drawing.Point(78, 54);
            this.lbl儿童姓名.Name = "lbl儿童姓名";
            this.lbl儿童姓名.Size = new System.Drawing.Size(256, 16);
            this.lbl儿童姓名.TabIndex = 6;
            this.lbl儿童姓名.Text = "牛茗卉 ";
            // 
            // lbl性别
            // 
            this.lbl性别.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl性别.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.tableLayoutPanel1.SetColumnSpan(this.lbl性别, 3);
            this.lbl性别.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl性别.Location = new System.Drawing.Point(78, 77);
            this.lbl性别.Name = "lbl性别";
            this.lbl性别.Size = new System.Drawing.Size(256, 14);
            this.lbl性别.TabIndex = 7;
            this.lbl性别.Text = "女 ";
            // 
            // lbl住址
            // 
            this.lbl住址.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl住址.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl住址.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.tableLayoutPanel1.SetColumnSpan(this.lbl住址, 3);
            this.lbl住址.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl住址.Location = new System.Drawing.Point(78, 98);
            this.lbl住址.Name = "lbl住址";
            this.lbl住址.Size = new System.Drawing.Size(256, 29);
            this.lbl住址.TabIndex = 8;
            this.lbl住址.Text = "山东省 临沂市 沂水县 姚店子镇 赵庄子村委会 姚店子镇赵庄子村       ";
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl14.Location = new System.Drawing.Point(338, 95);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(96, 35);
            this.labelControl14.TabIndex = 13;
            this.labelControl14.Text = "联系电话:";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl15.Location = new System.Drawing.Point(338, 74);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(96, 20);
            this.labelControl15.TabIndex = 14;
            this.labelControl15.Text = "出生日期:";
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl16.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl16.Location = new System.Drawing.Point(338, 51);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(96, 22);
            this.labelControl16.TabIndex = 15;
            this.labelControl16.Text = "身份证号:";
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl17.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl17.Location = new System.Drawing.Point(338, 29);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(96, 21);
            this.labelControl17.TabIndex = 16;
            this.labelControl17.Text = "儿童档案号:";
            // 
            // lbl儿童档案号
            // 
            this.lbl儿童档案号.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl儿童档案号.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.tableLayoutPanel1.SetColumnSpan(this.lbl儿童档案号, 3);
            this.lbl儿童档案号.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl儿童档案号.Location = new System.Drawing.Point(438, 32);
            this.lbl儿童档案号.Name = "lbl儿童档案号";
            this.lbl儿童档案号.Size = new System.Drawing.Size(288, 15);
            this.lbl儿童档案号.TabIndex = 19;
            this.lbl儿童档案号.Text = "371323190270002003  ";
            // 
            // lbl身份证号
            // 
            this.lbl身份证号.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl身份证号.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.tableLayoutPanel1.SetColumnSpan(this.lbl身份证号, 3);
            this.lbl身份证号.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl身份证号.Location = new System.Drawing.Point(438, 54);
            this.lbl身份证号.Name = "lbl身份证号";
            this.lbl身份证号.Size = new System.Drawing.Size(288, 16);
            this.lbl身份证号.TabIndex = 20;
            this.lbl身份证号.Text = "371323201411274142";
            // 
            // lbl出生日期
            // 
            this.lbl出生日期.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl出生日期.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.tableLayoutPanel1.SetColumnSpan(this.lbl出生日期, 3);
            this.lbl出生日期.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl出生日期.Location = new System.Drawing.Point(438, 77);
            this.lbl出生日期.Name = "lbl出生日期";
            this.lbl出生日期.Size = new System.Drawing.Size(288, 14);
            this.lbl出生日期.TabIndex = 21;
            this.lbl出生日期.Text = "2014-11-27";
            // 
            // lbl联系电话
            // 
            this.lbl联系电话.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl联系电话.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.tableLayoutPanel1.SetColumnSpan(this.lbl联系电话, 3);
            this.lbl联系电话.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl联系电话.Location = new System.Drawing.Point(438, 98);
            this.lbl联系电话.Name = "lbl联系电话";
            this.lbl联系电话.Size = new System.Drawing.Size(288, 29);
            this.lbl联系电话.TabIndex = 22;
            this.lbl联系电话.Text = "1529998907 ";
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl27.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl27.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl27.Location = new System.Drawing.Point(1, 131);
            this.labelControl27.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(73, 26);
            this.labelControl27.TabIndex = 26;
            this.labelControl27.Text = "父亲姓名:";
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl28.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl28.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl28.Location = new System.Drawing.Point(1, 158);
            this.labelControl28.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(73, 29);
            this.labelControl28.TabIndex = 27;
            this.labelControl28.Text = "母亲姓名:";
            // 
            // lbl父亲姓名
            // 
            this.lbl父亲姓名.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl父亲姓名.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl父亲姓名.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl父亲姓名.Location = new System.Drawing.Point(78, 134);
            this.lbl父亲姓名.Name = "lbl父亲姓名";
            this.lbl父亲姓名.Size = new System.Drawing.Size(74, 20);
            this.lbl父亲姓名.TabIndex = 28;
            this.lbl父亲姓名.Text = "牛纪山  ";
            // 
            // lbl母亲姓名
            // 
            this.lbl母亲姓名.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl母亲姓名.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl母亲姓名.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl母亲姓名.Location = new System.Drawing.Point(78, 161);
            this.lbl母亲姓名.Name = "lbl母亲姓名";
            this.lbl母亲姓名.Size = new System.Drawing.Size(74, 23);
            this.lbl母亲姓名.TabIndex = 29;
            this.lbl母亲姓名.Text = "武玉艳";
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl31.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl31.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl31.Location = new System.Drawing.Point(338, 131);
            this.labelControl31.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(96, 26);
            this.labelControl31.TabIndex = 30;
            this.labelControl31.Text = "联系电话:";
            // 
            // labelControl32
            // 
            this.labelControl32.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl32.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl32.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl32.Location = new System.Drawing.Point(338, 158);
            this.labelControl32.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(96, 29);
            this.labelControl32.TabIndex = 31;
            this.labelControl32.Text = "联系电话:";
            // 
            // labelControl33
            // 
            this.labelControl33.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl33.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl33.Location = new System.Drawing.Point(438, 134);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(81, 20);
            this.labelControl33.TabIndex = 32;
            this.labelControl33.Text = "1529998907 ";
            // 
            // labelControl34
            // 
            this.labelControl34.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl34.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl34.Location = new System.Drawing.Point(438, 161);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(81, 23);
            this.labelControl34.TabIndex = 33;
            this.labelControl34.Text = "1529998907 ";
            // 
            // lbl父亲出生日期
            // 
            this.lbl父亲出生日期.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl父亲出生日期.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl父亲出生日期.Location = new System.Drawing.Point(621, 134);
            this.lbl父亲出生日期.Name = "lbl父亲出生日期";
            this.lbl父亲出生日期.Size = new System.Drawing.Size(105, 20);
            this.lbl父亲出生日期.TabIndex = 34;
            this.lbl父亲出生日期.Text = "1988-08-14";
            // 
            // lbl母亲出生日期
            // 
            this.lbl母亲出生日期.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl母亲出生日期.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl母亲出生日期.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl母亲出生日期.Location = new System.Drawing.Point(621, 161);
            this.lbl母亲出生日期.Name = "lbl母亲出生日期";
            this.lbl母亲出生日期.Size = new System.Drawing.Size(105, 23);
            this.lbl母亲出生日期.TabIndex = 35;
            this.lbl母亲出生日期.Text = "1988-08-14";
            // 
            // lablecontrol
            // 
            this.lablecontrol.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.lablecontrol.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lablecontrol.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lablecontrol.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lablecontrol.Location = new System.Drawing.Point(156, 131);
            this.lablecontrol.Margin = new System.Windows.Forms.Padding(0);
            this.lablecontrol.Name = "lablecontrol";
            this.lablecontrol.Size = new System.Drawing.Size(60, 26);
            this.lablecontrol.TabIndex = 10;
            this.lablecontrol.Text = "职业:";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl10.Location = new System.Drawing.Point(156, 158);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(60, 29);
            this.labelControl10.TabIndex = 9;
            this.labelControl10.Text = "职业:";
            // 
            // lbl父亲职业
            // 
            this.lbl父亲职业.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl父亲职业.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl父亲职业.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl父亲职业.Location = new System.Drawing.Point(220, 134);
            this.lbl父亲职业.Name = "lbl父亲职业";
            this.lbl父亲职业.Size = new System.Drawing.Size(114, 20);
            this.lbl父亲职业.TabIndex = 11;
            this.lbl父亲职业.Text = "农 ";
            // 
            // lbl母亲职业
            // 
            this.lbl母亲职业.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl母亲职业.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl母亲职业.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl母亲职业.Location = new System.Drawing.Point(220, 161);
            this.lbl母亲职业.Name = "lbl母亲职业";
            this.lbl母亲职业.Size = new System.Drawing.Size(114, 23);
            this.lbl母亲职业.TabIndex = 12;
            this.lbl母亲职业.Text = "农 ";
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl25.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl25.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl25.Location = new System.Drawing.Point(523, 131);
            this.labelControl25.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(94, 26);
            this.labelControl25.TabIndex = 24;
            this.labelControl25.Text = "出生日期:";
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl24.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl24.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl24.Location = new System.Drawing.Point(523, 158);
            this.labelControl24.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(94, 29);
            this.labelControl24.TabIndex = 23;
            this.labelControl24.Text = "出生日期:";
            // 
            // textEdit1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.textEdit1, 3);
            this.textEdit1.Location = new System.Drawing.Point(75, 29);
            this.textEdit1.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.textEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.textEdit1.Size = new System.Drawing.Size(246, 20);
            this.textEdit1.TabIndex = 36;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 6;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 188F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 208F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 59F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.dateEdit3, 1, 20);
            this.tableLayoutPanel2.Controls.Add(this.labelControl72, 0, 22);
            this.tableLayoutPanel2.Controls.Add(this.labelControl71, 4, 21);
            this.tableLayoutPanel2.Controls.Add(this.labelControl70, 4, 20);
            this.tableLayoutPanel2.Controls.Add(this.labelControl69, 2, 21);
            this.tableLayoutPanel2.Controls.Add(this.labelControl68, 2, 20);
            this.tableLayoutPanel2.Controls.Add(this.labelControl67, 0, 21);
            this.tableLayoutPanel2.Controls.Add(this.textEdit28, 5, 19);
            this.tableLayoutPanel2.Controls.Add(this.labelControl66, 4, 19);
            this.tableLayoutPanel2.Controls.Add(this.labelControl65, 2, 19);
            this.tableLayoutPanel2.Controls.Add(this.labelControl64, 0, 19);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel21, 1, 16);
            this.tableLayoutPanel2.Controls.Add(this.labelControl63, 0, 20);
            this.tableLayoutPanel2.Controls.Add(this.labelControl62, 0, 16);
            this.tableLayoutPanel2.Controls.Add(this.labelControl61, 0, 18);
            this.tableLayoutPanel2.Controls.Add(this.labelControl60, 0, 17);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel20, 3, 15);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel19, 1, 15);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel18, 3, 14);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel17, 1, 14);
            this.tableLayoutPanel2.Controls.Add(this.labelControl59, 2, 15);
            this.tableLayoutPanel2.Controls.Add(this.labelControl58, 2, 14);
            this.tableLayoutPanel2.Controls.Add(this.labelControl57, 0, 15);
            this.tableLayoutPanel2.Controls.Add(this.labelControl56, 0, 14);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel16, 3, 13);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel15, 1, 13);
            this.tableLayoutPanel2.Controls.Add(this.labelControl55, 2, 13);
            this.tableLayoutPanel2.Controls.Add(this.labelControl54, 0, 13);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel13, 1, 12);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel12, 3, 11);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel11, 1, 11);
            this.tableLayoutPanel2.Controls.Add(this.labelControl53, 0, 12);
            this.tableLayoutPanel2.Controls.Add(this.labelControl52, 2, 11);
            this.tableLayoutPanel2.Controls.Add(this.labelControl51, 2, 12);
            this.tableLayoutPanel2.Controls.Add(this.labelControl50, 0, 11);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel10, 3, 10);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel9, 1, 10);
            this.tableLayoutPanel2.Controls.Add(this.labelControl49, 2, 10);
            this.tableLayoutPanel2.Controls.Add(this.labelControl48, 0, 10);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel8, 3, 9);
            this.tableLayoutPanel2.Controls.Add(this.labelControl45, 2, 9);
            this.tableLayoutPanel2.Controls.Add(this.labelControl44, 0, 9);
            this.tableLayoutPanel2.Controls.Add(this.labelControl43, 0, 8);
            this.tableLayoutPanel2.Controls.Add(this.labelControl42, 2, 8);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel7, 1, 9);
            this.tableLayoutPanel2.Controls.Add(this.labelControl41, 4, 8);
            this.tableLayoutPanel2.Controls.Add(this.ucTxtLbl8, 3, 7);
            this.tableLayoutPanel2.Controls.Add(this.ucTxtLbl7, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.labelControl40, 2, 7);
            this.tableLayoutPanel2.Controls.Add(this.labelControl39, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel6, 3, 8);
            this.tableLayoutPanel2.Controls.Add(this.ucTxtLbl6, 5, 6);
            this.tableLayoutPanel2.Controls.Add(this.comboBoxEdit7, 3, 6);
            this.tableLayoutPanel2.Controls.Add(this.comboBoxEdit6, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.labelControl38, 4, 6);
            this.tableLayoutPanel2.Controls.Add(this.labelControl37, 2, 6);
            this.tableLayoutPanel2.Controls.Add(this.labelControl36, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.ucTxtLbl5, 5, 5);
            this.tableLayoutPanel2.Controls.Add(this.ucTxtLbl4, 3, 5);
            this.tableLayoutPanel2.Controls.Add(this.comboBoxEdit5, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.labelControl35, 4, 5);
            this.tableLayoutPanel2.Controls.Add(this.labelControl30, 2, 5);
            this.tableLayoutPanel2.Controls.Add(this.labelControl26, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.comboBoxEdit4, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel5, 3, 3);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel4, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.textEdit5, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel2, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.comboBoxEdit3, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.labelControl6, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelControl7, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelControl8, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelControl9, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.labelControl11, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.labelControl12, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.labelControl13, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.labelControl18, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.labelControl19, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.textEdit2, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel1, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel3, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.labelControl22, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.labelControl23, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.labelControl29, 4, 4);
            this.tableLayoutPanel2.Controls.Add(this.ucTxtLbl2, 3, 4);
            this.tableLayoutPanel2.Controls.Add(this.ucTxtLbl3, 5, 4);
            this.tableLayoutPanel2.Controls.Add(this.ucTxtLbl1, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.ucTxtLbl9, 1, 8);
            this.tableLayoutPanel2.Controls.Add(this.dateEdit1, 1, 19);
            this.tableLayoutPanel2.Controls.Add(this.dateEdit2, 3, 19);
            this.tableLayoutPanel2.Controls.Add(this.labelControl73, 3, 20);
            this.tableLayoutPanel2.Controls.Add(this.labelControl74, 5, 21);
            this.tableLayoutPanel2.Controls.Add(this.labelControl75, 5, 20);
            this.tableLayoutPanel2.Controls.Add(this.labelControl76, 3, 21);
            this.tableLayoutPanel2.Controls.Add(this.labelControl77, 1, 21);
            this.tableLayoutPanel2.Controls.Add(this.labelControl78, 1, 22);
            this.tableLayoutPanel2.Controls.Add(this.comboBoxEdit8, 5, 8);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel14, 3, 12);
            this.tableLayoutPanel2.Controls.Add(this.panelControl2, 1, 17);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel22, 1, 18);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 23;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(730, 837);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // dateEdit3
            // 
            this.dateEdit3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateEdit3.EditValue = null;
            this.dateEdit3.Location = new System.Drawing.Point(67, 743);
            this.dateEdit3.Name = "dateEdit3";
            this.dateEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit3.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit3.Size = new System.Drawing.Size(182, 20);
            this.dateEdit3.TabIndex = 91;
            // 
            // labelControl72
            // 
            this.labelControl72.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl72.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl72.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl72.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl72.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl72.Location = new System.Drawing.Point(1, 800);
            this.labelControl72.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl72.Name = "labelControl72";
            this.labelControl72.Size = new System.Drawing.Size(62, 38);
            this.labelControl72.TabIndex = 90;
            this.labelControl72.Text = "当前所属机构:";
            // 
            // labelControl71
            // 
            this.labelControl71.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl71.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl71.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl71.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl71.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl71.Location = new System.Drawing.Point(524, 769);
            this.labelControl71.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl71.Name = "labelControl71";
            this.labelControl71.Size = new System.Drawing.Size(59, 30);
            this.labelControl71.TabIndex = 89;
            this.labelControl71.Text = "最近修改人:";
            // 
            // labelControl70
            // 
            this.labelControl70.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl70.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl70.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl70.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl70.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl70.Location = new System.Drawing.Point(524, 740);
            this.labelControl70.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl70.Name = "labelControl70";
            this.labelControl70.Size = new System.Drawing.Size(59, 28);
            this.labelControl70.TabIndex = 88;
            this.labelControl70.Text = "最近更新时间:";
            // 
            // labelControl69
            // 
            this.labelControl69.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl69.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl69.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl69.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl69.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl69.Location = new System.Drawing.Point(253, 769);
            this.labelControl69.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl69.Name = "labelControl69";
            this.labelControl69.Size = new System.Drawing.Size(61, 30);
            this.labelControl69.TabIndex = 87;
            this.labelControl69.Text = "录入人:";
            // 
            // labelControl68
            // 
            this.labelControl68.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl68.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl68.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl68.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl68.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl68.Location = new System.Drawing.Point(253, 740);
            this.labelControl68.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl68.Name = "labelControl68";
            this.labelControl68.Size = new System.Drawing.Size(61, 28);
            this.labelControl68.TabIndex = 86;
            this.labelControl68.Text = "录入时间:";
            // 
            // labelControl67
            // 
            this.labelControl67.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl67.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl67.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl67.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl67.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl67.Location = new System.Drawing.Point(1, 769);
            this.labelControl67.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl67.Name = "labelControl67";
            this.labelControl67.Size = new System.Drawing.Size(62, 30);
            this.labelControl67.TabIndex = 85;
            this.labelControl67.Text = "创建机构:";
            // 
            // textEdit28
            // 
            this.textEdit28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textEdit28.Location = new System.Drawing.Point(585, 705);
            this.textEdit28.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.textEdit28.Name = "textEdit28";
            this.textEdit28.Size = new System.Drawing.Size(143, 20);
            this.textEdit28.TabIndex = 84;
            // 
            // labelControl66
            // 
            this.labelControl66.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl66.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl66.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl66.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl66.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl66.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl66.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl66.Location = new System.Drawing.Point(524, 702);
            this.labelControl66.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl66.Name = "labelControl66";
            this.labelControl66.Size = new System.Drawing.Size(59, 37);
            this.labelControl66.TabIndex = 81;
            this.labelControl66.Text = "随访医生签名:";
            // 
            // labelControl65
            // 
            this.labelControl65.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl65.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl65.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl65.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl65.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl65.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl65.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl65.Location = new System.Drawing.Point(253, 702);
            this.labelControl65.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl65.Name = "labelControl65";
            this.labelControl65.Size = new System.Drawing.Size(61, 37);
            this.labelControl65.TabIndex = 80;
            this.labelControl65.Text = "下次随访地点:";
            // 
            // labelControl64
            // 
            this.labelControl64.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl64.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl64.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl64.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl64.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl64.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl64.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl64.Location = new System.Drawing.Point(1, 702);
            this.labelControl64.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl64.Name = "labelControl64";
            this.labelControl64.Size = new System.Drawing.Size(62, 37);
            this.labelControl64.TabIndex = 79;
            this.labelControl64.Text = "下次随访日期:";
            // 
            // flowLayoutPanel21
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel21, 5);
            this.flowLayoutPanel21.Controls.Add(this.comboBoxEdit21);
            this.flowLayoutPanel21.Controls.Add(this.textEdit26);
            this.flowLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel21.Location = new System.Drawing.Point(64, 575);
            this.flowLayoutPanel21.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel21.Name = "flowLayoutPanel21";
            this.flowLayoutPanel21.Size = new System.Drawing.Size(665, 25);
            this.flowLayoutPanel21.TabIndex = 76;
            // 
            // comboBoxEdit21
            // 
            this.comboBoxEdit21.Location = new System.Drawing.Point(1, 3);
            this.comboBoxEdit21.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.comboBoxEdit21.Name = "comboBoxEdit21";
            this.comboBoxEdit21.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit21.Size = new System.Drawing.Size(81, 20);
            this.comboBoxEdit21.TabIndex = 0;
            // 
            // textEdit26
            // 
            this.textEdit26.Location = new System.Drawing.Point(84, 3);
            this.textEdit26.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.textEdit26.Name = "textEdit26";
            this.textEdit26.Size = new System.Drawing.Size(100, 20);
            this.textEdit26.TabIndex = 1;
            // 
            // labelControl63
            // 
            this.labelControl63.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl63.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl63.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl63.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl63.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl63.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl63.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl63.Location = new System.Drawing.Point(1, 740);
            this.labelControl63.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl63.Name = "labelControl63";
            this.labelControl63.Size = new System.Drawing.Size(62, 28);
            this.labelControl63.TabIndex = 75;
            this.labelControl63.Text = "随访日期:";
            // 
            // labelControl62
            // 
            this.labelControl62.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl62.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl62.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl62.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl62.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl62.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl62.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl62.Location = new System.Drawing.Point(1, 575);
            this.labelControl62.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl62.Name = "labelControl62";
            this.labelControl62.Size = new System.Drawing.Size(62, 25);
            this.labelControl62.TabIndex = 74;
            this.labelControl62.Text = "脐带:";
            // 
            // labelControl61
            // 
            this.labelControl61.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl61.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl61.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl61.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl61.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl61.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl61.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl61.Location = new System.Drawing.Point(1, 676);
            this.labelControl61.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl61.Name = "labelControl61";
            this.labelControl61.Size = new System.Drawing.Size(62, 25);
            this.labelControl61.TabIndex = 73;
            this.labelControl61.Text = "指导:";
            // 
            // labelControl60
            // 
            this.labelControl60.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl60.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl60.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl60.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl60.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl60.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl60.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl60.Location = new System.Drawing.Point(1, 601);
            this.labelControl60.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl60.Name = "labelControl60";
            this.labelControl60.Size = new System.Drawing.Size(62, 74);
            this.labelControl60.TabIndex = 72;
            this.labelControl60.Text = "转诊:";
            // 
            // flowLayoutPanel20
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel20, 3);
            this.flowLayoutPanel20.Controls.Add(this.comboBoxEdit20);
            this.flowLayoutPanel20.Controls.Add(this.textEdit25);
            this.flowLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel20.Location = new System.Drawing.Point(315, 548);
            this.flowLayoutPanel20.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel20.Name = "flowLayoutPanel20";
            this.flowLayoutPanel20.Size = new System.Drawing.Size(414, 26);
            this.flowLayoutPanel20.TabIndex = 71;
            // 
            // comboBoxEdit20
            // 
            this.comboBoxEdit20.Location = new System.Drawing.Point(1, 3);
            this.comboBoxEdit20.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.comboBoxEdit20.Name = "comboBoxEdit20";
            this.comboBoxEdit20.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit20.Size = new System.Drawing.Size(81, 20);
            this.comboBoxEdit20.TabIndex = 0;
            // 
            // textEdit25
            // 
            this.textEdit25.Location = new System.Drawing.Point(84, 3);
            this.textEdit25.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.textEdit25.Name = "textEdit25";
            this.textEdit25.Size = new System.Drawing.Size(100, 20);
            this.textEdit25.TabIndex = 1;
            // 
            // flowLayoutPanel19
            // 
            this.flowLayoutPanel19.Controls.Add(this.comboBoxEdit19);
            this.flowLayoutPanel19.Controls.Add(this.textEdit24);
            this.flowLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel19.Location = new System.Drawing.Point(64, 548);
            this.flowLayoutPanel19.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel19.Name = "flowLayoutPanel19";
            this.flowLayoutPanel19.Size = new System.Drawing.Size(188, 26);
            this.flowLayoutPanel19.TabIndex = 70;
            // 
            // comboBoxEdit19
            // 
            this.comboBoxEdit19.Location = new System.Drawing.Point(1, 3);
            this.comboBoxEdit19.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.comboBoxEdit19.Name = "comboBoxEdit19";
            this.comboBoxEdit19.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit19.Size = new System.Drawing.Size(81, 20);
            this.comboBoxEdit19.TabIndex = 0;
            // 
            // textEdit24
            // 
            this.textEdit24.Location = new System.Drawing.Point(84, 3);
            this.textEdit24.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.textEdit24.Name = "textEdit24";
            this.textEdit24.Size = new System.Drawing.Size(100, 20);
            this.textEdit24.TabIndex = 1;
            // 
            // flowLayoutPanel18
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel18, 3);
            this.flowLayoutPanel18.Controls.Add(this.comboBoxEdit18);
            this.flowLayoutPanel18.Controls.Add(this.textEdit23);
            this.flowLayoutPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel18.Location = new System.Drawing.Point(315, 512);
            this.flowLayoutPanel18.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel18.Name = "flowLayoutPanel18";
            this.flowLayoutPanel18.Size = new System.Drawing.Size(414, 35);
            this.flowLayoutPanel18.TabIndex = 69;
            // 
            // comboBoxEdit18
            // 
            this.comboBoxEdit18.Location = new System.Drawing.Point(1, 3);
            this.comboBoxEdit18.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.comboBoxEdit18.Name = "comboBoxEdit18";
            this.comboBoxEdit18.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit18.Size = new System.Drawing.Size(81, 20);
            this.comboBoxEdit18.TabIndex = 0;
            // 
            // textEdit23
            // 
            this.textEdit23.Location = new System.Drawing.Point(84, 3);
            this.textEdit23.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.textEdit23.Name = "textEdit23";
            this.textEdit23.Size = new System.Drawing.Size(100, 20);
            this.textEdit23.TabIndex = 1;
            // 
            // flowLayoutPanel17
            // 
            this.flowLayoutPanel17.Controls.Add(this.comboBoxEdit17);
            this.flowLayoutPanel17.Controls.Add(this.textEdit22);
            this.flowLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel17.Location = new System.Drawing.Point(64, 512);
            this.flowLayoutPanel17.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel17.Name = "flowLayoutPanel17";
            this.flowLayoutPanel17.Size = new System.Drawing.Size(188, 35);
            this.flowLayoutPanel17.TabIndex = 68;
            // 
            // comboBoxEdit17
            // 
            this.comboBoxEdit17.Location = new System.Drawing.Point(1, 3);
            this.comboBoxEdit17.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.comboBoxEdit17.Name = "comboBoxEdit17";
            this.comboBoxEdit17.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit17.Size = new System.Drawing.Size(81, 20);
            this.comboBoxEdit17.TabIndex = 0;
            // 
            // textEdit22
            // 
            this.textEdit22.Location = new System.Drawing.Point(84, 3);
            this.textEdit22.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.textEdit22.Name = "textEdit22";
            this.textEdit22.Size = new System.Drawing.Size(100, 20);
            this.textEdit22.TabIndex = 1;
            // 
            // labelControl59
            // 
            this.labelControl59.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl59.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl59.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl59.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl59.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl59.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl59.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl59.Location = new System.Drawing.Point(253, 548);
            this.labelControl59.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl59.Name = "labelControl59";
            this.labelControl59.Size = new System.Drawing.Size(61, 26);
            this.labelControl59.TabIndex = 67;
            this.labelControl59.Text = "脊柱:";
            // 
            // labelControl58
            // 
            this.labelControl58.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl58.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl58.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl58.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl58.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl58.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl58.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl58.Location = new System.Drawing.Point(253, 512);
            this.labelControl58.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl58.Name = "labelControl58";
            this.labelControl58.Size = new System.Drawing.Size(61, 35);
            this.labelControl58.TabIndex = 66;
            this.labelControl58.Text = "外生殖器官:";
            // 
            // labelControl57
            // 
            this.labelControl57.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl57.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl57.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl57.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl57.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl57.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl57.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl57.Location = new System.Drawing.Point(1, 548);
            this.labelControl57.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl57.Name = "labelControl57";
            this.labelControl57.Size = new System.Drawing.Size(62, 26);
            this.labelControl57.TabIndex = 65;
            this.labelControl57.Text = "腹部:";
            // 
            // labelControl56
            // 
            this.labelControl56.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl56.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl56.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl56.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl56.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl56.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl56.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl56.Location = new System.Drawing.Point(1, 512);
            this.labelControl56.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl56.Name = "labelControl56";
            this.labelControl56.Size = new System.Drawing.Size(62, 35);
            this.labelControl56.TabIndex = 64;
            this.labelControl56.Text = "心肺:";
            // 
            // flowLayoutPanel16
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel16, 3);
            this.flowLayoutPanel16.Controls.Add(this.comboBoxEdit16);
            this.flowLayoutPanel16.Controls.Add(this.textEdit21);
            this.flowLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel16.Location = new System.Drawing.Point(315, 485);
            this.flowLayoutPanel16.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel16.Name = "flowLayoutPanel16";
            this.flowLayoutPanel16.Size = new System.Drawing.Size(414, 26);
            this.flowLayoutPanel16.TabIndex = 63;
            // 
            // comboBoxEdit16
            // 
            this.comboBoxEdit16.Location = new System.Drawing.Point(1, 3);
            this.comboBoxEdit16.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.comboBoxEdit16.Name = "comboBoxEdit16";
            this.comboBoxEdit16.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit16.Size = new System.Drawing.Size(81, 20);
            this.comboBoxEdit16.TabIndex = 0;
            // 
            // textEdit21
            // 
            this.textEdit21.Location = new System.Drawing.Point(84, 3);
            this.textEdit21.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.textEdit21.Name = "textEdit21";
            this.textEdit21.Size = new System.Drawing.Size(100, 20);
            this.textEdit21.TabIndex = 1;
            // 
            // flowLayoutPanel15
            // 
            this.flowLayoutPanel15.Controls.Add(this.comboBoxEdit15);
            this.flowLayoutPanel15.Controls.Add(this.textEdit20);
            this.flowLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel15.Location = new System.Drawing.Point(64, 485);
            this.flowLayoutPanel15.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel15.Name = "flowLayoutPanel15";
            this.flowLayoutPanel15.Size = new System.Drawing.Size(188, 26);
            this.flowLayoutPanel15.TabIndex = 62;
            // 
            // comboBoxEdit15
            // 
            this.comboBoxEdit15.Location = new System.Drawing.Point(1, 3);
            this.comboBoxEdit15.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.comboBoxEdit15.Name = "comboBoxEdit15";
            this.comboBoxEdit15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit15.Size = new System.Drawing.Size(81, 20);
            this.comboBoxEdit15.TabIndex = 0;
            // 
            // textEdit20
            // 
            this.textEdit20.Location = new System.Drawing.Point(84, 3);
            this.textEdit20.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.textEdit20.Name = "textEdit20";
            this.textEdit20.Size = new System.Drawing.Size(100, 20);
            this.textEdit20.TabIndex = 1;
            // 
            // labelControl55
            // 
            this.labelControl55.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl55.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl55.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl55.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl55.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl55.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl55.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl55.Location = new System.Drawing.Point(253, 485);
            this.labelControl55.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl55.Name = "labelControl55";
            this.labelControl55.Size = new System.Drawing.Size(61, 26);
            this.labelControl55.TabIndex = 61;
            this.labelControl55.Text = "肛门:";
            // 
            // labelControl54
            // 
            this.labelControl54.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl54.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl54.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl54.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl54.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl54.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl54.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl54.Location = new System.Drawing.Point(1, 485);
            this.labelControl54.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl54.Name = "labelControl54";
            this.labelControl54.Size = new System.Drawing.Size(62, 26);
            this.labelControl54.TabIndex = 60;
            this.labelControl54.Text = "口腔:";
            // 
            // flowLayoutPanel13
            // 
            this.flowLayoutPanel13.Controls.Add(this.comboBoxEdit14);
            this.flowLayoutPanel13.Controls.Add(this.textEdit18);
            this.flowLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel13.Location = new System.Drawing.Point(64, 458);
            this.flowLayoutPanel13.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel13.Name = "flowLayoutPanel13";
            this.flowLayoutPanel13.Size = new System.Drawing.Size(188, 26);
            this.flowLayoutPanel13.TabIndex = 59;
            // 
            // comboBoxEdit14
            // 
            this.comboBoxEdit14.Location = new System.Drawing.Point(1, 3);
            this.comboBoxEdit14.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.comboBoxEdit14.Name = "comboBoxEdit14";
            this.comboBoxEdit14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit14.Size = new System.Drawing.Size(81, 20);
            this.comboBoxEdit14.TabIndex = 0;
            // 
            // textEdit18
            // 
            this.textEdit18.Location = new System.Drawing.Point(84, 3);
            this.textEdit18.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.textEdit18.Name = "textEdit18";
            this.textEdit18.Size = new System.Drawing.Size(100, 20);
            this.textEdit18.TabIndex = 1;
            // 
            // flowLayoutPanel12
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel12, 3);
            this.flowLayoutPanel12.Controls.Add(this.comboBoxEdit13);
            this.flowLayoutPanel12.Controls.Add(this.textEdit17);
            this.flowLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel12.Location = new System.Drawing.Point(315, 431);
            this.flowLayoutPanel12.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            this.flowLayoutPanel12.Size = new System.Drawing.Size(414, 26);
            this.flowLayoutPanel12.TabIndex = 58;
            // 
            // comboBoxEdit13
            // 
            this.comboBoxEdit13.Location = new System.Drawing.Point(1, 3);
            this.comboBoxEdit13.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.comboBoxEdit13.Name = "comboBoxEdit13";
            this.comboBoxEdit13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit13.Size = new System.Drawing.Size(81, 20);
            this.comboBoxEdit13.TabIndex = 0;
            // 
            // textEdit17
            // 
            this.textEdit17.Location = new System.Drawing.Point(84, 3);
            this.textEdit17.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.textEdit17.Name = "textEdit17";
            this.textEdit17.Size = new System.Drawing.Size(100, 20);
            this.textEdit17.TabIndex = 1;
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.Controls.Add(this.comboBoxEdit12);
            this.flowLayoutPanel11.Controls.Add(this.textEdit16);
            this.flowLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel11.Location = new System.Drawing.Point(64, 431);
            this.flowLayoutPanel11.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(188, 26);
            this.flowLayoutPanel11.TabIndex = 57;
            // 
            // comboBoxEdit12
            // 
            this.comboBoxEdit12.Location = new System.Drawing.Point(1, 3);
            this.comboBoxEdit12.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.comboBoxEdit12.Name = "comboBoxEdit12";
            this.comboBoxEdit12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit12.Size = new System.Drawing.Size(81, 20);
            this.comboBoxEdit12.TabIndex = 0;
            // 
            // textEdit16
            // 
            this.textEdit16.Location = new System.Drawing.Point(84, 3);
            this.textEdit16.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.textEdit16.Name = "textEdit16";
            this.textEdit16.Size = new System.Drawing.Size(100, 20);
            this.textEdit16.TabIndex = 1;
            // 
            // labelControl53
            // 
            this.labelControl53.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl53.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl53.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl53.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl53.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl53.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl53.Location = new System.Drawing.Point(1, 458);
            this.labelControl53.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(62, 26);
            this.labelControl53.TabIndex = 56;
            this.labelControl53.Text = "鼻:";
            // 
            // labelControl52
            // 
            this.labelControl52.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl52.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl52.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl52.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl52.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl52.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl52.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl52.Location = new System.Drawing.Point(253, 431);
            this.labelControl52.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl52.Name = "labelControl52";
            this.labelControl52.Size = new System.Drawing.Size(61, 26);
            this.labelControl52.TabIndex = 55;
            this.labelControl52.Text = "颈部包块:";
            // 
            // labelControl51
            // 
            this.labelControl51.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl51.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl51.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl51.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl51.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl51.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl51.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl51.Location = new System.Drawing.Point(253, 458);
            this.labelControl51.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl51.Name = "labelControl51";
            this.labelControl51.Size = new System.Drawing.Size(61, 26);
            this.labelControl51.TabIndex = 54;
            this.labelControl51.Text = "皮肤:";
            // 
            // labelControl50
            // 
            this.labelControl50.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl50.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl50.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl50.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl50.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl50.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl50.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl50.Location = new System.Drawing.Point(1, 431);
            this.labelControl50.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(62, 26);
            this.labelControl50.TabIndex = 53;
            this.labelControl50.Text = "耳外观:";
            // 
            // flowLayoutPanel10
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel10, 3);
            this.flowLayoutPanel10.Controls.Add(this.comboBoxEdit11);
            this.flowLayoutPanel10.Controls.Add(this.textEdit15);
            this.flowLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(315, 396);
            this.flowLayoutPanel10.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(414, 34);
            this.flowLayoutPanel10.TabIndex = 52;
            // 
            // comboBoxEdit11
            // 
            this.comboBoxEdit11.Location = new System.Drawing.Point(1, 3);
            this.comboBoxEdit11.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.comboBoxEdit11.Name = "comboBoxEdit11";
            this.comboBoxEdit11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit11.Size = new System.Drawing.Size(81, 20);
            this.comboBoxEdit11.TabIndex = 0;
            // 
            // textEdit15
            // 
            this.textEdit15.Location = new System.Drawing.Point(84, 3);
            this.textEdit15.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.textEdit15.Name = "textEdit15";
            this.textEdit15.Size = new System.Drawing.Size(100, 20);
            this.textEdit15.TabIndex = 1;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.Controls.Add(this.comboBoxEdit10);
            this.flowLayoutPanel9.Controls.Add(this.textEdit14);
            this.flowLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(64, 396);
            this.flowLayoutPanel9.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(188, 34);
            this.flowLayoutPanel9.TabIndex = 51;
            // 
            // comboBoxEdit10
            // 
            this.comboBoxEdit10.Location = new System.Drawing.Point(1, 3);
            this.comboBoxEdit10.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.comboBoxEdit10.Name = "comboBoxEdit10";
            this.comboBoxEdit10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit10.Size = new System.Drawing.Size(81, 20);
            this.comboBoxEdit10.TabIndex = 0;
            // 
            // textEdit14
            // 
            this.textEdit14.Location = new System.Drawing.Point(84, 3);
            this.textEdit14.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Size = new System.Drawing.Size(100, 20);
            this.textEdit14.TabIndex = 1;
            // 
            // labelControl49
            // 
            this.labelControl49.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl49.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl49.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl49.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl49.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl49.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl49.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl49.Location = new System.Drawing.Point(253, 396);
            this.labelControl49.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl49.Name = "labelControl49";
            this.labelControl49.Size = new System.Drawing.Size(61, 34);
            this.labelControl49.TabIndex = 50;
            this.labelControl49.Text = "四肢活动度:";
            // 
            // labelControl48
            // 
            this.labelControl48.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl48.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl48.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl48.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl48.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl48.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl48.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl48.Location = new System.Drawing.Point(1, 396);
            this.labelControl48.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(62, 34);
            this.labelControl48.TabIndex = 49;
            this.labelControl48.Text = "眼外观:";
            // 
            // flowLayoutPanel8
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel8, 3);
            this.flowLayoutPanel8.Controls.Add(this.comboBoxEdit9);
            this.flowLayoutPanel8.Controls.Add(this.textEdit13);
            this.flowLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(315, 370);
            this.flowLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(414, 25);
            this.flowLayoutPanel8.TabIndex = 48;
            // 
            // comboBoxEdit9
            // 
            this.comboBoxEdit9.Location = new System.Drawing.Point(1, 3);
            this.comboBoxEdit9.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.comboBoxEdit9.Name = "comboBoxEdit9";
            this.comboBoxEdit9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit9.Size = new System.Drawing.Size(100, 20);
            this.comboBoxEdit9.TabIndex = 0;
            // 
            // textEdit13
            // 
            this.textEdit13.Location = new System.Drawing.Point(103, 3);
            this.textEdit13.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Size = new System.Drawing.Size(100, 20);
            this.textEdit13.TabIndex = 1;
            // 
            // labelControl45
            // 
            this.labelControl45.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl45.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl45.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl45.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl45.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl45.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl45.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl45.Location = new System.Drawing.Point(253, 370);
            this.labelControl45.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(61, 25);
            this.labelControl45.TabIndex = 47;
            this.labelControl45.Text = "前囟情况:";
            // 
            // labelControl44
            // 
            this.labelControl44.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl44.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl44.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl44.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl44.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl44.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl44.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl44.Location = new System.Drawing.Point(1, 370);
            this.labelControl44.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(62, 25);
            this.labelControl44.TabIndex = 46;
            this.labelControl44.Text = "前囟:";
            // 
            // labelControl43
            // 
            this.labelControl43.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl43.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl43.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl43.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl43.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl43.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl43.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl43.Location = new System.Drawing.Point(1, 319);
            this.labelControl43.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(62, 50);
            this.labelControl43.TabIndex = 43;
            this.labelControl43.Text = "脉率:";
            // 
            // labelControl42
            // 
            this.labelControl42.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl42.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl42.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl42.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl42.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl42.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl42.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl42.Location = new System.Drawing.Point(253, 319);
            this.labelControl42.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(61, 50);
            this.labelControl42.TabIndex = 42;
            this.labelControl42.Text = "面色:";
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.Controls.Add(this.textEdit11);
            this.flowLayoutPanel7.Controls.Add(this.labelControl47);
            this.flowLayoutPanel7.Controls.Add(this.textEdit12);
            this.flowLayoutPanel7.Controls.Add(this.labelControl46);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(64, 370);
            this.flowLayoutPanel7.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(188, 25);
            this.flowLayoutPanel7.TabIndex = 17;
            // 
            // textEdit11
            // 
            this.textEdit11.Location = new System.Drawing.Point(0, 3);
            this.textEdit11.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Size = new System.Drawing.Size(59, 20);
            this.textEdit11.TabIndex = 14;
            // 
            // labelControl47
            // 
            this.labelControl47.Location = new System.Drawing.Point(59, 3);
            this.labelControl47.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(23, 14);
            this.labelControl47.TabIndex = 17;
            this.labelControl47.Text = "cm*";
            // 
            // textEdit12
            // 
            this.textEdit12.Location = new System.Drawing.Point(82, 3);
            this.textEdit12.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Size = new System.Drawing.Size(59, 20);
            this.textEdit12.TabIndex = 15;
            // 
            // labelControl46
            // 
            this.labelControl46.Location = new System.Drawing.Point(144, 3);
            this.labelControl46.Name = "labelControl46";
            this.labelControl46.Size = new System.Drawing.Size(16, 14);
            this.labelControl46.TabIndex = 16;
            this.labelControl46.Text = "cm";
            // 
            // labelControl41
            // 
            this.labelControl41.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl41.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl41.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl41.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl41.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl41.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl41.Location = new System.Drawing.Point(524, 319);
            this.labelControl41.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(59, 50);
            this.labelControl41.TabIndex = 41;
            this.labelControl41.Text = "黄疸部位:";
            // 
            // ucTxtLbl8
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.ucTxtLbl8, 3);
            this.ucTxtLbl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTxtLbl8.Lbl1Size = new System.Drawing.Size(40, 18);
            this.ucTxtLbl8.Lbl1Text = "次/分钟";
            this.ucTxtLbl8.Location = new System.Drawing.Point(318, 293);
            this.ucTxtLbl8.Name = "ucTxtLbl8";
            this.ucTxtLbl8.Size = new System.Drawing.Size(408, 22);
            this.ucTxtLbl8.TabIndex = 40;
            this.ucTxtLbl8.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // ucTxtLbl7
            // 
            this.ucTxtLbl7.Lbl1Size = new System.Drawing.Size(20, 18);
            this.ucTxtLbl7.Lbl1Text = "℃";
            this.ucTxtLbl7.Location = new System.Drawing.Point(67, 293);
            this.ucTxtLbl7.Name = "ucTxtLbl7";
            this.ucTxtLbl7.Size = new System.Drawing.Size(148, 19);
            this.ucTxtLbl7.TabIndex = 39;
            this.ucTxtLbl7.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // labelControl40
            // 
            this.labelControl40.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl40.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl40.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl40.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl40.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl40.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl40.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl40.Location = new System.Drawing.Point(253, 290);
            this.labelControl40.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(61, 28);
            this.labelControl40.TabIndex = 38;
            this.labelControl40.Text = "呼吸频率:";
            // 
            // labelControl39
            // 
            this.labelControl39.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl39.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl39.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl39.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl39.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl39.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl39.Location = new System.Drawing.Point(1, 290);
            this.labelControl39.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(62, 28);
            this.labelControl39.TabIndex = 37;
            this.labelControl39.Text = "体温:";
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.Controls.Add(this.checkEdit2);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit5);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit6);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit22);
            this.flowLayoutPanel6.Controls.Add(this.textEdit10);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(315, 319);
            this.flowLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(208, 50);
            this.flowLayoutPanel6.TabIndex = 16;
            // 
            // checkEdit2
            // 
            this.checkEdit2.Location = new System.Drawing.Point(3, 3);
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "未检";
            this.checkEdit2.Size = new System.Drawing.Size(51, 19);
            this.checkEdit2.TabIndex = 0;
            // 
            // checkEdit5
            // 
            this.checkEdit5.Location = new System.Drawing.Point(60, 3);
            this.checkEdit5.Name = "checkEdit5";
            this.checkEdit5.Properties.Caption = "红润";
            this.checkEdit5.Size = new System.Drawing.Size(52, 19);
            this.checkEdit5.TabIndex = 1;
            // 
            // checkEdit6
            // 
            this.checkEdit6.Location = new System.Drawing.Point(118, 3);
            this.checkEdit6.Name = "checkEdit6";
            this.checkEdit6.Properties.Caption = "黄染";
            this.checkEdit6.Size = new System.Drawing.Size(84, 19);
            this.checkEdit6.TabIndex = 2;
            // 
            // checkEdit22
            // 
            this.checkEdit22.Location = new System.Drawing.Point(3, 28);
            this.checkEdit22.Name = "checkEdit22";
            this.checkEdit22.Properties.Caption = "其他";
            this.checkEdit22.Size = new System.Drawing.Size(54, 19);
            this.checkEdit22.TabIndex = 5;
            // 
            // textEdit10
            // 
            this.textEdit10.Location = new System.Drawing.Point(63, 28);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Size = new System.Drawing.Size(100, 20);
            this.textEdit10.TabIndex = 14;
            // 
            // ucTxtLbl6
            // 
            this.ucTxtLbl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTxtLbl6.Lbl1Size = new System.Drawing.Size(40, 18);
            this.ucTxtLbl6.Lbl1Text = "次/每日";
            this.ucTxtLbl6.Location = new System.Drawing.Point(587, 267);
            this.ucTxtLbl6.Name = "ucTxtLbl6";
            this.ucTxtLbl6.Size = new System.Drawing.Size(139, 19);
            this.ucTxtLbl6.TabIndex = 36;
            this.ucTxtLbl6.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // comboBoxEdit7
            // 
            this.comboBoxEdit7.Location = new System.Drawing.Point(316, 267);
            this.comboBoxEdit7.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.comboBoxEdit7.Name = "comboBoxEdit7";
            this.comboBoxEdit7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit7.Size = new System.Drawing.Size(69, 20);
            this.comboBoxEdit7.TabIndex = 35;
            // 
            // comboBoxEdit6
            // 
            this.comboBoxEdit6.Location = new System.Drawing.Point(65, 267);
            this.comboBoxEdit6.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.comboBoxEdit6.Name = "comboBoxEdit6";
            this.comboBoxEdit6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit6.Size = new System.Drawing.Size(83, 20);
            this.comboBoxEdit6.TabIndex = 34;
            // 
            // labelControl38
            // 
            this.labelControl38.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl38.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl38.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl38.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl38.Location = new System.Drawing.Point(524, 264);
            this.labelControl38.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(59, 25);
            this.labelControl38.TabIndex = 33;
            this.labelControl38.Text = "大便次数:";
            // 
            // labelControl37
            // 
            this.labelControl37.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl37.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl37.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl37.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl37.Location = new System.Drawing.Point(253, 264);
            this.labelControl37.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(61, 25);
            this.labelControl37.TabIndex = 32;
            this.labelControl37.Text = "大便:";
            // 
            // labelControl36
            // 
            this.labelControl36.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl36.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl36.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl36.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl36.Location = new System.Drawing.Point(1, 264);
            this.labelControl36.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(62, 25);
            this.labelControl36.TabIndex = 31;
            this.labelControl36.Text = "呕吐:";
            // 
            // ucTxtLbl5
            // 
            this.ucTxtLbl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTxtLbl5.Lbl1Size = new System.Drawing.Size(40, 18);
            this.ucTxtLbl5.Lbl1Text = "次/每日";
            this.ucTxtLbl5.Location = new System.Drawing.Point(587, 240);
            this.ucTxtLbl5.Name = "ucTxtLbl5";
            this.ucTxtLbl5.Size = new System.Drawing.Size(139, 20);
            this.ucTxtLbl5.TabIndex = 30;
            this.ucTxtLbl5.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // ucTxtLbl4
            // 
            this.ucTxtLbl4.Lbl1Size = new System.Drawing.Size(69, 18);
            this.ucTxtLbl4.Lbl1Text = "ml/次";
            this.ucTxtLbl4.Location = new System.Drawing.Point(318, 240);
            this.ucTxtLbl4.Name = "ucTxtLbl4";
            this.ucTxtLbl4.Size = new System.Drawing.Size(181, 20);
            this.ucTxtLbl4.TabIndex = 29;
            this.ucTxtLbl4.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // comboBoxEdit5
            // 
            this.comboBoxEdit5.Location = new System.Drawing.Point(65, 240);
            this.comboBoxEdit5.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.comboBoxEdit5.Name = "comboBoxEdit5";
            this.comboBoxEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit5.Size = new System.Drawing.Size(83, 20);
            this.comboBoxEdit5.TabIndex = 28;
            // 
            // labelControl35
            // 
            this.labelControl35.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl35.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl35.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl35.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl35.Location = new System.Drawing.Point(524, 237);
            this.labelControl35.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(59, 26);
            this.labelControl35.TabIndex = 27;
            this.labelControl35.Text = "吃奶次数:";
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl30.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl30.Appearance.ForeColor = System.Drawing.SystemColors.InfoText;
            this.labelControl30.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl30.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl30.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl30.Location = new System.Drawing.Point(253, 237);
            this.labelControl30.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(61, 26);
            this.labelControl30.TabIndex = 26;
            this.labelControl30.Text = "吃奶量:";
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl26.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl26.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl26.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl26.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl26.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl26.Location = new System.Drawing.Point(1, 237);
            this.labelControl26.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(62, 26);
            this.labelControl26.TabIndex = 25;
            this.labelControl26.Text = "喂养方式:";
            // 
            // comboBoxEdit4
            // 
            this.comboBoxEdit4.Location = new System.Drawing.Point(65, 161);
            this.comboBoxEdit4.Margin = new System.Windows.Forms.Padding(1, 15, 1, 3);
            this.comboBoxEdit4.Name = "comboBoxEdit4";
            this.comboBoxEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit4.Size = new System.Drawing.Size(112, 20);
            this.comboBoxEdit4.TabIndex = 16;
            // 
            // flowLayoutPanel5
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel5, 3);
            this.flowLayoutPanel5.Controls.Add(this.checkEdit15);
            this.flowLayoutPanel5.Controls.Add(this.checkEdit16);
            this.flowLayoutPanel5.Controls.Add(this.checkEdit17);
            this.flowLayoutPanel5.Controls.Add(this.checkEdit18);
            this.flowLayoutPanel5.Controls.Add(this.checkEdit19);
            this.flowLayoutPanel5.Controls.Add(this.checkEdit20);
            this.flowLayoutPanel5.Controls.Add(this.textEdit8);
            this.flowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(315, 146);
            this.flowLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(414, 50);
            this.flowLayoutPanel5.TabIndex = 15;
            // 
            // checkEdit15
            // 
            this.checkEdit15.Location = new System.Drawing.Point(3, 3);
            this.checkEdit15.Name = "checkEdit15";
            this.checkEdit15.Properties.Caption = "无";
            this.checkEdit15.Size = new System.Drawing.Size(40, 19);
            this.checkEdit15.TabIndex = 0;
            // 
            // checkEdit16
            // 
            this.checkEdit16.Location = new System.Drawing.Point(49, 3);
            this.checkEdit16.Name = "checkEdit16";
            this.checkEdit16.Properties.Caption = "甲低";
            this.checkEdit16.Size = new System.Drawing.Size(52, 19);
            this.checkEdit16.TabIndex = 1;
            // 
            // checkEdit17
            // 
            this.checkEdit17.Location = new System.Drawing.Point(107, 3);
            this.checkEdit17.Name = "checkEdit17";
            this.checkEdit17.Properties.Caption = "苯丙酮尿症";
            this.checkEdit17.Size = new System.Drawing.Size(84, 19);
            this.checkEdit17.TabIndex = 2;
            // 
            // checkEdit18
            // 
            this.checkEdit18.Location = new System.Drawing.Point(197, 3);
            this.checkEdit18.Name = "checkEdit18";
            this.checkEdit18.Properties.Caption = "先天性肾上腺皮质激素增生症";
            this.checkEdit18.Size = new System.Drawing.Size(177, 19);
            this.checkEdit18.TabIndex = 3;
            // 
            // checkEdit19
            // 
            this.checkEdit19.Location = new System.Drawing.Point(3, 28);
            this.checkEdit19.Name = "checkEdit19";
            this.checkEdit19.Properties.Caption = "葡萄糖-6-磷酸脱氢酶缺乏症";
            this.checkEdit19.Size = new System.Drawing.Size(172, 19);
            this.checkEdit19.TabIndex = 4;
            // 
            // checkEdit20
            // 
            this.checkEdit20.Location = new System.Drawing.Point(181, 28);
            this.checkEdit20.Name = "checkEdit20";
            this.checkEdit20.Properties.Caption = "其他遗传代谢病";
            this.checkEdit20.Size = new System.Drawing.Size(109, 19);
            this.checkEdit20.TabIndex = 5;
            // 
            // textEdit8
            // 
            this.textEdit8.Location = new System.Drawing.Point(296, 28);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Size = new System.Drawing.Size(100, 20);
            this.textEdit8.TabIndex = 14;
            // 
            // flowLayoutPanel4
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel4, 3);
            this.flowLayoutPanel4.Controls.Add(this.checkEdit8);
            this.flowLayoutPanel4.Controls.Add(this.checkEdit9);
            this.flowLayoutPanel4.Controls.Add(this.checkEdit10);
            this.flowLayoutPanel4.Controls.Add(this.checkEdit11);
            this.flowLayoutPanel4.Controls.Add(this.checkEdit12);
            this.flowLayoutPanel4.Controls.Add(this.checkEdit13);
            this.flowLayoutPanel4.Controls.Add(this.checkEdit14);
            this.flowLayoutPanel4.Controls.Add(this.textEdit7);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(315, 54);
            this.flowLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(414, 51);
            this.flowLayoutPanel4.TabIndex = 14;
            // 
            // checkEdit8
            // 
            this.checkEdit8.Location = new System.Drawing.Point(3, 3);
            this.checkEdit8.Name = "checkEdit8";
            this.checkEdit8.Properties.Caption = "顺产";
            this.checkEdit8.Size = new System.Drawing.Size(53, 19);
            this.checkEdit8.TabIndex = 0;
            // 
            // checkEdit9
            // 
            this.checkEdit9.Location = new System.Drawing.Point(62, 3);
            this.checkEdit9.Name = "checkEdit9";
            this.checkEdit9.Properties.Caption = "头吸";
            this.checkEdit9.Size = new System.Drawing.Size(54, 19);
            this.checkEdit9.TabIndex = 1;
            // 
            // checkEdit10
            // 
            this.checkEdit10.Location = new System.Drawing.Point(122, 3);
            this.checkEdit10.Name = "checkEdit10";
            this.checkEdit10.Properties.Caption = "产钳";
            this.checkEdit10.Size = new System.Drawing.Size(53, 19);
            this.checkEdit10.TabIndex = 2;
            // 
            // checkEdit11
            // 
            this.checkEdit11.Location = new System.Drawing.Point(181, 3);
            this.checkEdit11.Name = "checkEdit11";
            this.checkEdit11.Properties.Caption = "剖宫";
            this.checkEdit11.Size = new System.Drawing.Size(51, 19);
            this.checkEdit11.TabIndex = 3;
            // 
            // checkEdit12
            // 
            this.checkEdit12.Location = new System.Drawing.Point(238, 3);
            this.checkEdit12.Name = "checkEdit12";
            this.checkEdit12.Properties.Caption = "双多胎";
            this.checkEdit12.Size = new System.Drawing.Size(63, 19);
            this.checkEdit12.TabIndex = 4;
            // 
            // checkEdit13
            // 
            this.checkEdit13.Location = new System.Drawing.Point(307, 3);
            this.checkEdit13.Name = "checkEdit13";
            this.checkEdit13.Properties.Caption = "臀位";
            this.checkEdit13.Size = new System.Drawing.Size(51, 19);
            this.checkEdit13.TabIndex = 5;
            // 
            // checkEdit14
            // 
            this.checkEdit14.Location = new System.Drawing.Point(3, 28);
            this.checkEdit14.Name = "checkEdit14";
            this.checkEdit14.Properties.Caption = "其他";
            this.checkEdit14.Size = new System.Drawing.Size(53, 19);
            this.checkEdit14.TabIndex = 6;
            // 
            // textEdit7
            // 
            this.textEdit7.Location = new System.Drawing.Point(62, 28);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Size = new System.Drawing.Size(100, 20);
            this.textEdit7.TabIndex = 14;
            // 
            // textEdit5
            // 
            this.textEdit5.Location = new System.Drawing.Point(67, 69);
            this.textEdit5.Margin = new System.Windows.Forms.Padding(3, 15, 3, 3);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Size = new System.Drawing.Size(182, 20);
            this.textEdit5.TabIndex = 12;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.comboBoxEdit2);
            this.flowLayoutPanel2.Controls.Add(this.textEdit4);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(584, 1);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(145, 52);
            this.flowLayoutPanel2.TabIndex = 11;
            // 
            // comboBoxEdit2
            // 
            this.comboBoxEdit2.Location = new System.Drawing.Point(1, 15);
            this.comboBoxEdit2.Margin = new System.Windows.Forms.Padding(1, 15, 1, 3);
            this.comboBoxEdit2.Name = "comboBoxEdit2";
            this.comboBoxEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit2.Size = new System.Drawing.Size(69, 20);
            this.comboBoxEdit2.TabIndex = 0;
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(1, 53);
            this.textEdit4.Margin = new System.Windows.Forms.Padding(1, 15, 1, 3);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Size = new System.Drawing.Size(82, 20);
            this.textEdit4.TabIndex = 1;
            // 
            // comboBoxEdit3
            // 
            this.comboBoxEdit3.Location = new System.Drawing.Point(65, 116);
            this.comboBoxEdit3.Margin = new System.Windows.Forms.Padding(1, 10, 1, 3);
            this.comboBoxEdit3.Name = "comboBoxEdit3";
            this.comboBoxEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit3.Size = new System.Drawing.Size(112, 20);
            this.comboBoxEdit3.TabIndex = 2;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl6.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl6.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl6.Location = new System.Drawing.Point(1, 1);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(62, 52);
            this.labelControl6.TabIndex = 0;
            this.labelControl6.Text = "出生孕周:";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl7.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl7.Location = new System.Drawing.Point(253, 1);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(61, 52);
            this.labelControl7.TabIndex = 1;
            this.labelControl7.Text = "母亲妊娠期患病疾病情况:";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl8.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl8.Location = new System.Drawing.Point(524, 1);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(59, 52);
            this.labelControl8.TabIndex = 2;
            this.labelControl8.Text = "是否有畸型:";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl9.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl9.Location = new System.Drawing.Point(253, 54);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(61, 51);
            this.labelControl9.TabIndex = 3;
            this.labelControl9.Text = "出生情况:";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl11.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl11.Location = new System.Drawing.Point(253, 106);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(61, 39);
            this.labelControl11.TabIndex = 4;
            this.labelControl11.Text = "Apgar评分:";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl12.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl12.Location = new System.Drawing.Point(253, 146);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(61, 50);
            this.labelControl12.TabIndex = 5;
            this.labelControl12.Text = "新生儿疾病筛查:";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl13.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl13.Location = new System.Drawing.Point(1, 146);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(62, 50);
            this.labelControl13.TabIndex = 6;
            this.labelControl13.Text = "新生儿听力筛查:";
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl18.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl18.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl18.Location = new System.Drawing.Point(1, 106);
            this.labelControl18.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(62, 39);
            this.labelControl18.TabIndex = 7;
            this.labelControl18.Text = "新生儿窒息:";
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl19.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl19.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl19.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl19.Location = new System.Drawing.Point(1, 54);
            this.labelControl19.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(62, 51);
            this.labelControl19.TabIndex = 8;
            this.labelControl19.Text = "助产机构名称:";
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(67, 16);
            this.textEdit2.Margin = new System.Windows.Forms.Padding(3, 15, 3, 3);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(100, 20);
            this.textEdit2.TabIndex = 9;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.comboBoxEdit1);
            this.flowLayoutPanel1.Controls.Add(this.textEdit3);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(315, 1);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(208, 52);
            this.flowLayoutPanel1.TabIndex = 10;
            // 
            // comboBoxEdit1
            // 
            this.comboBoxEdit1.Location = new System.Drawing.Point(1, 15);
            this.comboBoxEdit1.Margin = new System.Windows.Forms.Padding(1, 15, 1, 3);
            this.comboBoxEdit1.Name = "comboBoxEdit1";
            this.comboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit1.Size = new System.Drawing.Size(100, 20);
            this.comboBoxEdit1.TabIndex = 0;
            // 
            // textEdit3
            // 
            this.textEdit3.Location = new System.Drawing.Point(103, 15);
            this.textEdit3.Margin = new System.Windows.Forms.Padding(1, 15, 1, 3);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Size = new System.Drawing.Size(100, 20);
            this.textEdit3.TabIndex = 1;
            // 
            // flowLayoutPanel3
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel3, 3);
            this.flowLayoutPanel3.Controls.Add(this.checkEdit1);
            this.flowLayoutPanel3.Controls.Add(this.textEdit6);
            this.flowLayoutPanel3.Controls.Add(this.labelControl21);
            this.flowLayoutPanel3.Controls.Add(this.checkEdit3);
            this.flowLayoutPanel3.Controls.Add(this.textEdit9);
            this.flowLayoutPanel3.Controls.Add(this.labelControl20);
            this.flowLayoutPanel3.Controls.Add(this.checkEdit4);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(315, 106);
            this.flowLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(414, 39);
            this.flowLayoutPanel3.TabIndex = 13;
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(1, 10);
            this.checkEdit1.Margin = new System.Windows.Forms.Padding(1, 10, 3, 3);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "1分钟";
            this.checkEdit1.Size = new System.Drawing.Size(53, 19);
            this.checkEdit1.TabIndex = 0;
            // 
            // textEdit6
            // 
            this.textEdit6.Location = new System.Drawing.Point(58, 10);
            this.textEdit6.Margin = new System.Windows.Forms.Padding(1, 10, 3, 3);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Size = new System.Drawing.Size(100, 20);
            this.textEdit6.TabIndex = 4;
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(162, 10);
            this.labelControl21.Margin = new System.Windows.Forms.Padding(1, 10, 3, 3);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(12, 14);
            this.labelControl21.TabIndex = 7;
            this.labelControl21.Text = "分";
            // 
            // checkEdit3
            // 
            this.checkEdit3.Location = new System.Drawing.Point(178, 10);
            this.checkEdit3.Margin = new System.Windows.Forms.Padding(1, 10, 3, 3);
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "5分钟";
            this.checkEdit3.Size = new System.Drawing.Size(53, 19);
            this.checkEdit3.TabIndex = 2;
            // 
            // textEdit9
            // 
            this.textEdit9.Location = new System.Drawing.Point(235, 10);
            this.textEdit9.Margin = new System.Windows.Forms.Padding(1, 10, 3, 3);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Size = new System.Drawing.Size(100, 20);
            this.textEdit9.TabIndex = 5;
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(339, 10);
            this.labelControl20.Margin = new System.Windows.Forms.Padding(1, 10, 3, 3);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(12, 14);
            this.labelControl20.TabIndex = 6;
            this.labelControl20.Text = "分";
            // 
            // checkEdit4
            // 
            this.checkEdit4.Location = new System.Drawing.Point(355, 10);
            this.checkEdit4.Margin = new System.Windows.Forms.Padding(1, 10, 3, 3);
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.Caption = "不详";
            this.checkEdit4.Size = new System.Drawing.Size(51, 19);
            this.checkEdit4.TabIndex = 3;
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl22.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl22.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl22.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl22.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl22.Location = new System.Drawing.Point(1, 197);
            this.labelControl22.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(62, 39);
            this.labelControl22.TabIndex = 17;
            this.labelControl22.Text = "新生儿出生体重:";
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl23.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl23.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl23.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl23.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl23.Location = new System.Drawing.Point(253, 197);
            this.labelControl23.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(61, 39);
            this.labelControl23.TabIndex = 18;
            this.labelControl23.Text = "目前体重:";
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl29.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl29.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl29.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl29.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl29.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl29.Location = new System.Drawing.Point(524, 197);
            this.labelControl29.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(59, 39);
            this.labelControl29.TabIndex = 20;
            this.labelControl29.Text = "出生身长:";
            // 
            // ucTxtLbl2
            // 
            this.ucTxtLbl2.Lbl1Size = new System.Drawing.Size(69, 18);
            this.ucTxtLbl2.Lbl1Text = "kg";
            this.ucTxtLbl2.Location = new System.Drawing.Point(318, 209);
            this.ucTxtLbl2.Margin = new System.Windows.Forms.Padding(3, 12, 3, 3);
            this.ucTxtLbl2.Name = "ucTxtLbl2";
            this.ucTxtLbl2.Size = new System.Drawing.Size(181, 22);
            this.ucTxtLbl2.TabIndex = 22;
            this.ucTxtLbl2.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // ucTxtLbl3
            // 
            this.ucTxtLbl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTxtLbl3.Lbl1Size = new System.Drawing.Size(16, 18);
            this.ucTxtLbl3.Lbl1Text = "cm";
            this.ucTxtLbl3.Location = new System.Drawing.Point(587, 209);
            this.ucTxtLbl3.Margin = new System.Windows.Forms.Padding(3, 12, 3, 3);
            this.ucTxtLbl3.Name = "ucTxtLbl3";
            this.ucTxtLbl3.Size = new System.Drawing.Size(139, 24);
            this.ucTxtLbl3.TabIndex = 23;
            this.ucTxtLbl3.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // ucTxtLbl1
            // 
            this.ucTxtLbl1.Lbl1Size = new System.Drawing.Size(15, 18);
            this.ucTxtLbl1.Lbl1Text = "kg";
            this.ucTxtLbl1.Location = new System.Drawing.Point(67, 209);
            this.ucTxtLbl1.Margin = new System.Windows.Forms.Padding(3, 12, 3, 3);
            this.ucTxtLbl1.Name = "ucTxtLbl1";
            this.ucTxtLbl1.Size = new System.Drawing.Size(181, 22);
            this.ucTxtLbl1.TabIndex = 24;
            this.ucTxtLbl1.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // ucTxtLbl9
            // 
            this.ucTxtLbl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTxtLbl9.Lbl1Size = new System.Drawing.Size(40, 18);
            this.ucTxtLbl9.Lbl1Text = "次/分钟";
            this.ucTxtLbl9.Location = new System.Drawing.Point(67, 334);
            this.ucTxtLbl9.Margin = new System.Windows.Forms.Padding(3, 15, 3, 3);
            this.ucTxtLbl9.Name = "ucTxtLbl9";
            this.ucTxtLbl9.Size = new System.Drawing.Size(182, 32);
            this.ucTxtLbl9.TabIndex = 44;
            this.ucTxtLbl9.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // dateEdit1
            // 
            this.dateEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(67, 705);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Size = new System.Drawing.Size(182, 20);
            this.dateEdit1.TabIndex = 82;
            // 
            // dateEdit2
            // 
            this.dateEdit2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateEdit2.Location = new System.Drawing.Point(318, 705);
            this.dateEdit2.Name = "dateEdit2";
            this.dateEdit2.Properties.DisplayFormat.FormatString = "d";
            this.dateEdit2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit2.Properties.EditFormat.FormatString = "d";
            this.dateEdit2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit2.Properties.Mask.EditMask = "d";
            this.dateEdit2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.dateEdit2.Size = new System.Drawing.Size(202, 20);
            this.dateEdit2.TabIndex = 83;
            // 
            // labelControl73
            // 
            this.labelControl73.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl73.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl73.Location = new System.Drawing.Point(318, 743);
            this.labelControl73.Name = "labelControl73";
            this.labelControl73.Size = new System.Drawing.Size(202, 22);
            this.labelControl73.TabIndex = 92;
            this.labelControl73.Text = "2015-07-29 08:17:24";
            // 
            // labelControl74
            // 
            this.labelControl74.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl74.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl74.Location = new System.Drawing.Point(587, 772);
            this.labelControl74.Name = "labelControl74";
            this.labelControl74.Size = new System.Drawing.Size(139, 24);
            this.labelControl74.TabIndex = 93;
            this.labelControl74.Text = "孙鹏";
            // 
            // labelControl75
            // 
            this.labelControl75.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl75.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl75.Location = new System.Drawing.Point(587, 743);
            this.labelControl75.Name = "labelControl75";
            this.labelControl75.Size = new System.Drawing.Size(139, 22);
            this.labelControl75.TabIndex = 94;
            this.labelControl75.Text = "2015-07-29 08:17:24";
            // 
            // labelControl76
            // 
            this.labelControl76.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl76.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl76.Location = new System.Drawing.Point(318, 772);
            this.labelControl76.Name = "labelControl76";
            this.labelControl76.Size = new System.Drawing.Size(202, 24);
            this.labelControl76.TabIndex = 95;
            this.labelControl76.Text = "孙鹏";
            // 
            // labelControl77
            // 
            this.labelControl77.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl77.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl77.Location = new System.Drawing.Point(67, 772);
            this.labelControl77.Name = "labelControl77";
            this.labelControl77.Size = new System.Drawing.Size(182, 24);
            this.labelControl77.TabIndex = 96;
            this.labelControl77.Text = "姚店子镇中心卫生院";
            // 
            // labelControl78
            // 
            this.labelControl78.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.tableLayoutPanel2.SetColumnSpan(this.labelControl78, 5);
            this.labelControl78.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl78.Location = new System.Drawing.Point(67, 803);
            this.labelControl78.Name = "labelControl78";
            this.labelControl78.Size = new System.Drawing.Size(659, 32);
            this.labelControl78.TabIndex = 97;
            this.labelControl78.Text = "姚店子镇中心卫生院";
            // 
            // comboBoxEdit8
            // 
            this.comboBoxEdit8.Location = new System.Drawing.Point(585, 334);
            this.comboBoxEdit8.Margin = new System.Windows.Forms.Padding(1, 15, 1, 3);
            this.comboBoxEdit8.Name = "comboBoxEdit8";
            this.comboBoxEdit8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit8.Size = new System.Drawing.Size(97, 20);
            this.comboBoxEdit8.TabIndex = 45;
            // 
            // flowLayoutPanel14
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel14, 3);
            this.flowLayoutPanel14.Controls.Add(this.checkEdit7);
            this.flowLayoutPanel14.Controls.Add(this.checkEdit21);
            this.flowLayoutPanel14.Controls.Add(this.checkEdit23);
            this.flowLayoutPanel14.Controls.Add(this.checkEdit24);
            this.flowLayoutPanel14.Controls.Add(this.textEdit19);
            this.flowLayoutPanel14.Controls.Add(this.flowLayoutPanel24);
            this.flowLayoutPanel14.Controls.Add(this.flowLayoutPanel26);
            this.flowLayoutPanel14.Location = new System.Drawing.Point(315, 458);
            this.flowLayoutPanel14.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel14.Name = "flowLayoutPanel14";
            this.flowLayoutPanel14.Size = new System.Drawing.Size(414, 26);
            this.flowLayoutPanel14.TabIndex = 17;
            // 
            // checkEdit7
            // 
            this.checkEdit7.Location = new System.Drawing.Point(3, 3);
            this.checkEdit7.Name = "checkEdit7";
            this.checkEdit7.Properties.Caption = "未见异常";
            this.checkEdit7.Size = new System.Drawing.Size(79, 19);
            this.checkEdit7.TabIndex = 0;
            // 
            // checkEdit21
            // 
            this.checkEdit21.Location = new System.Drawing.Point(88, 3);
            this.checkEdit21.Name = "checkEdit21";
            this.checkEdit21.Properties.Caption = "湿疹";
            this.checkEdit21.Size = new System.Drawing.Size(52, 19);
            this.checkEdit21.TabIndex = 1;
            // 
            // checkEdit23
            // 
            this.checkEdit23.Location = new System.Drawing.Point(146, 3);
            this.checkEdit23.Name = "checkEdit23";
            this.checkEdit23.Properties.Caption = "糜烂";
            this.checkEdit23.Size = new System.Drawing.Size(57, 19);
            this.checkEdit23.TabIndex = 2;
            // 
            // checkEdit24
            // 
            this.checkEdit24.Location = new System.Drawing.Point(209, 3);
            this.checkEdit24.Name = "checkEdit24";
            this.checkEdit24.Properties.Caption = "其他";
            this.checkEdit24.Size = new System.Drawing.Size(54, 19);
            this.checkEdit24.TabIndex = 5;
            // 
            // textEdit19
            // 
            this.textEdit19.Location = new System.Drawing.Point(269, 3);
            this.textEdit19.Name = "textEdit19";
            this.textEdit19.Size = new System.Drawing.Size(100, 20);
            this.textEdit19.TabIndex = 14;
            // 
            // flowLayoutPanel24
            // 
            this.flowLayoutPanel24.Controls.Add(this.checkEdit34);
            this.flowLayoutPanel24.Controls.Add(this.checkEdit35);
            this.flowLayoutPanel24.Controls.Add(this.checkEdit36);
            this.flowLayoutPanel24.Controls.Add(this.checkEdit37);
            this.flowLayoutPanel24.Controls.Add(this.textEdit30);
            this.flowLayoutPanel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel24.Location = new System.Drawing.Point(0, 26);
            this.flowLayoutPanel24.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel24.Name = "flowLayoutPanel24";
            this.flowLayoutPanel24.Size = new System.Drawing.Size(414, 0);
            this.flowLayoutPanel24.TabIndex = 18;
            // 
            // checkEdit34
            // 
            this.checkEdit34.Location = new System.Drawing.Point(3, 3);
            this.checkEdit34.Name = "checkEdit34";
            this.checkEdit34.Properties.Caption = "未见异常";
            this.checkEdit34.Size = new System.Drawing.Size(79, 19);
            this.checkEdit34.TabIndex = 0;
            // 
            // checkEdit35
            // 
            this.checkEdit35.Location = new System.Drawing.Point(88, 3);
            this.checkEdit35.Name = "checkEdit35";
            this.checkEdit35.Properties.Caption = "湿疹";
            this.checkEdit35.Size = new System.Drawing.Size(52, 19);
            this.checkEdit35.TabIndex = 1;
            // 
            // checkEdit36
            // 
            this.checkEdit36.Location = new System.Drawing.Point(146, 3);
            this.checkEdit36.Name = "checkEdit36";
            this.checkEdit36.Properties.Caption = "糜烂";
            this.checkEdit36.Size = new System.Drawing.Size(57, 19);
            this.checkEdit36.TabIndex = 2;
            // 
            // checkEdit37
            // 
            this.checkEdit37.Location = new System.Drawing.Point(209, 3);
            this.checkEdit37.Name = "checkEdit37";
            this.checkEdit37.Properties.Caption = "其他";
            this.checkEdit37.Size = new System.Drawing.Size(54, 19);
            this.checkEdit37.TabIndex = 5;
            // 
            // textEdit30
            // 
            this.textEdit30.Location = new System.Drawing.Point(269, 3);
            this.textEdit30.Name = "textEdit30";
            this.textEdit30.Size = new System.Drawing.Size(100, 20);
            this.textEdit30.TabIndex = 14;
            // 
            // flowLayoutPanel26
            // 
            this.flowLayoutPanel26.Controls.Add(this.checkEdit42);
            this.flowLayoutPanel26.Controls.Add(this.checkEdit43);
            this.flowLayoutPanel26.Controls.Add(this.checkEdit44);
            this.flowLayoutPanel26.Controls.Add(this.checkEdit45);
            this.flowLayoutPanel26.Controls.Add(this.textEdit32);
            this.flowLayoutPanel26.Controls.Add(this.flowLayoutPanel27);
            this.flowLayoutPanel26.Location = new System.Drawing.Point(0, 26);
            this.flowLayoutPanel26.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel26.Name = "flowLayoutPanel26";
            this.flowLayoutPanel26.Size = new System.Drawing.Size(414, 26);
            this.flowLayoutPanel26.TabIndex = 19;
            // 
            // checkEdit42
            // 
            this.checkEdit42.Location = new System.Drawing.Point(3, 3);
            this.checkEdit42.Name = "checkEdit42";
            this.checkEdit42.Properties.Caption = "未见异常";
            this.checkEdit42.Size = new System.Drawing.Size(79, 19);
            this.checkEdit42.TabIndex = 0;
            // 
            // checkEdit43
            // 
            this.checkEdit43.Location = new System.Drawing.Point(88, 3);
            this.checkEdit43.Name = "checkEdit43";
            this.checkEdit43.Properties.Caption = "湿疹";
            this.checkEdit43.Size = new System.Drawing.Size(52, 19);
            this.checkEdit43.TabIndex = 1;
            // 
            // checkEdit44
            // 
            this.checkEdit44.Location = new System.Drawing.Point(146, 3);
            this.checkEdit44.Name = "checkEdit44";
            this.checkEdit44.Properties.Caption = "糜烂";
            this.checkEdit44.Size = new System.Drawing.Size(57, 19);
            this.checkEdit44.TabIndex = 2;
            // 
            // checkEdit45
            // 
            this.checkEdit45.Location = new System.Drawing.Point(209, 3);
            this.checkEdit45.Name = "checkEdit45";
            this.checkEdit45.Properties.Caption = "其他";
            this.checkEdit45.Size = new System.Drawing.Size(54, 19);
            this.checkEdit45.TabIndex = 5;
            // 
            // textEdit32
            // 
            this.textEdit32.Location = new System.Drawing.Point(269, 3);
            this.textEdit32.Name = "textEdit32";
            this.textEdit32.Size = new System.Drawing.Size(100, 20);
            this.textEdit32.TabIndex = 14;
            // 
            // flowLayoutPanel27
            // 
            this.flowLayoutPanel27.Controls.Add(this.checkEdit46);
            this.flowLayoutPanel27.Controls.Add(this.checkEdit47);
            this.flowLayoutPanel27.Controls.Add(this.checkEdit48);
            this.flowLayoutPanel27.Controls.Add(this.checkEdit49);
            this.flowLayoutPanel27.Controls.Add(this.textEdit33);
            this.flowLayoutPanel27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel27.Location = new System.Drawing.Point(0, 26);
            this.flowLayoutPanel27.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel27.Name = "flowLayoutPanel27";
            this.flowLayoutPanel27.Size = new System.Drawing.Size(414, 0);
            this.flowLayoutPanel27.TabIndex = 18;
            // 
            // checkEdit46
            // 
            this.checkEdit46.Location = new System.Drawing.Point(3, 3);
            this.checkEdit46.Name = "checkEdit46";
            this.checkEdit46.Properties.Caption = "未见异常";
            this.checkEdit46.Size = new System.Drawing.Size(79, 19);
            this.checkEdit46.TabIndex = 0;
            // 
            // checkEdit47
            // 
            this.checkEdit47.Location = new System.Drawing.Point(88, 3);
            this.checkEdit47.Name = "checkEdit47";
            this.checkEdit47.Properties.Caption = "湿疹";
            this.checkEdit47.Size = new System.Drawing.Size(52, 19);
            this.checkEdit47.TabIndex = 1;
            // 
            // checkEdit48
            // 
            this.checkEdit48.Location = new System.Drawing.Point(146, 3);
            this.checkEdit48.Name = "checkEdit48";
            this.checkEdit48.Properties.Caption = "糜烂";
            this.checkEdit48.Size = new System.Drawing.Size(57, 19);
            this.checkEdit48.TabIndex = 2;
            // 
            // checkEdit49
            // 
            this.checkEdit49.Location = new System.Drawing.Point(209, 3);
            this.checkEdit49.Name = "checkEdit49";
            this.checkEdit49.Properties.Caption = "其他";
            this.checkEdit49.Size = new System.Drawing.Size(54, 19);
            this.checkEdit49.TabIndex = 5;
            // 
            // textEdit33
            // 
            this.textEdit33.Location = new System.Drawing.Point(269, 3);
            this.textEdit33.Name = "textEdit33";
            this.textEdit33.Size = new System.Drawing.Size(100, 20);
            this.textEdit33.TabIndex = 14;
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.tableLayoutPanel2.SetColumnSpan(this.panelControl2, 5);
            this.panelControl2.Controls.Add(this.ucLblTxt2);
            this.panelControl2.Controls.Add(this.ucLblTxt1);
            this.panelControl2.Location = new System.Drawing.Point(64, 601);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(665, 74);
            this.panelControl2.TabIndex = 77;
            // 
            // ucLblTxt2
            // 
            this.ucLblTxt2.Lbl1Size = new System.Drawing.Size(69, 18);
            this.ucLblTxt2.Lbl1Text = "机构及科室：";
            this.ucLblTxt2.Location = new System.Drawing.Point(7, 46);
            this.ucLblTxt2.Name = "ucLblTxt2";
            this.ucLblTxt2.Size = new System.Drawing.Size(384, 22);
            this.ucLblTxt2.TabIndex = 1;
            this.ucLblTxt2.Txt1Size = new System.Drawing.Size(300, 20);
            // 
            // ucLblTxt1
            // 
            this.ucLblTxt1.Lbl1Size = new System.Drawing.Size(69, 18);
            this.ucLblTxt1.Lbl1Text = "原    因：";
            this.ucLblTxt1.Location = new System.Drawing.Point(7, 24);
            this.ucLblTxt1.Name = "ucLblTxt1";
            this.ucLblTxt1.Size = new System.Drawing.Size(388, 22);
            this.ucLblTxt1.TabIndex = 0;
            this.ucLblTxt1.Txt1Size = new System.Drawing.Size(300, 20);
            // 
            // flowLayoutPanel22
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel22, 5);
            this.flowLayoutPanel22.Controls.Add(this.checkEdit25);
            this.flowLayoutPanel22.Controls.Add(this.checkEdit26);
            this.flowLayoutPanel22.Controls.Add(this.checkEdit27);
            this.flowLayoutPanel22.Controls.Add(this.checkEdit28);
            this.flowLayoutPanel22.Controls.Add(this.checkEdit29);
            this.flowLayoutPanel22.Location = new System.Drawing.Point(64, 676);
            this.flowLayoutPanel22.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel22.Name = "flowLayoutPanel22";
            this.flowLayoutPanel22.Size = new System.Drawing.Size(665, 25);
            this.flowLayoutPanel22.TabIndex = 78;
            // 
            // checkEdit25
            // 
            this.checkEdit25.Location = new System.Drawing.Point(3, 3);
            this.checkEdit25.Name = "checkEdit25";
            this.checkEdit25.Properties.Caption = "科学喂养";
            this.checkEdit25.Size = new System.Drawing.Size(79, 19);
            this.checkEdit25.TabIndex = 0;
            // 
            // checkEdit26
            // 
            this.checkEdit26.Location = new System.Drawing.Point(88, 3);
            this.checkEdit26.Name = "checkEdit26";
            this.checkEdit26.Properties.Caption = "生长发育";
            this.checkEdit26.Size = new System.Drawing.Size(72, 19);
            this.checkEdit26.TabIndex = 1;
            // 
            // checkEdit27
            // 
            this.checkEdit27.Location = new System.Drawing.Point(166, 3);
            this.checkEdit27.Name = "checkEdit27";
            this.checkEdit27.Properties.Caption = "疾病预防";
            this.checkEdit27.Size = new System.Drawing.Size(75, 19);
            this.checkEdit27.TabIndex = 2;
            // 
            // checkEdit28
            // 
            this.checkEdit28.Location = new System.Drawing.Point(247, 3);
            this.checkEdit28.Name = "checkEdit28";
            this.checkEdit28.Properties.Caption = "预防意外伤害";
            this.checkEdit28.Size = new System.Drawing.Size(99, 19);
            this.checkEdit28.TabIndex = 5;
            // 
            // checkEdit29
            // 
            this.checkEdit29.Location = new System.Drawing.Point(352, 3);
            this.checkEdit29.Name = "checkEdit29";
            this.checkEdit29.Properties.Caption = "口腔保健";
            this.checkEdit29.Size = new System.Drawing.Size(83, 19);
            this.checkEdit29.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1122, 483);
            this.Controls.Add(this.xtraScrollableControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.xtraScrollableControl1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit67.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit66.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit64.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit63.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit65.Properties)).EndInit();
            this.flowLayoutPanel31.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit68.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit69.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit39.Properties)).EndInit();
            this.flowLayoutPanel30.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit58.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit59.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit60.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit61.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit62.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit38.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit37.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit36.Properties)).EndInit();
            this.flowLayoutPanel25.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit38.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit39.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit40.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit41.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit31.Properties)).EndInit();
            this.flowLayoutPanel23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit30.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit32.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit33.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit27.Properties)).EndInit();
            this.flowLayoutPanel28.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit50.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit51.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit52.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit53.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit34.Properties)).EndInit();
            this.flowLayoutPanel29.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit54.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit55.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit56.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit57.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit35.Properties)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit28.Properties)).EndInit();
            this.flowLayoutPanel21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit26.Properties)).EndInit();
            this.flowLayoutPanel20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit25.Properties)).EndInit();
            this.flowLayoutPanel19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit24.Properties)).EndInit();
            this.flowLayoutPanel18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit23.Properties)).EndInit();
            this.flowLayoutPanel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit22.Properties)).EndInit();
            this.flowLayoutPanel16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit21.Properties)).EndInit();
            this.flowLayoutPanel15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit20.Properties)).EndInit();
            this.flowLayoutPanel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).EndInit();
            this.flowLayoutPanel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit17.Properties)).EndInit();
            this.flowLayoutPanel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).EndInit();
            this.flowLayoutPanel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).EndInit();
            this.flowLayoutPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            this.flowLayoutPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            this.flowLayoutPanel7.ResumeLayout(false);
            this.flowLayoutPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            this.flowLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit4.Properties)).EndInit();
            this.flowLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            this.flowLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit8.Properties)).EndInit();
            this.flowLayoutPanel14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit19.Properties)).EndInit();
            this.flowLayoutPanel24.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit34.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit35.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit36.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit37.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit30.Properties)).EndInit();
            this.flowLayoutPanel26.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit43.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit44.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit45.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit32.Properties)).EndInit();
            this.flowLayoutPanel27.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit46.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit47.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit48.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit49.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit33.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.flowLayoutPanel22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit27.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit28.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit29.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl lbl儿童姓名;
        private DevExpress.XtraEditors.LabelControl lbl性别;
        private DevExpress.XtraEditors.LabelControl lbl住址;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl lbl儿童档案号;
        private DevExpress.XtraEditors.LabelControl lbl身份证号;
        private DevExpress.XtraEditors.LabelControl lbl出生日期;
        private DevExpress.XtraEditors.LabelControl lbl联系电话;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.LabelControl lbl父亲姓名;
        private DevExpress.XtraEditors.LabelControl lbl母亲姓名;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.LabelControl lbl父亲出生日期;
        private DevExpress.XtraEditors.LabelControl lbl母亲出生日期;
        private DevExpress.XtraEditors.LabelControl lablecontrol;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl lbl父亲职业;
        private DevExpress.XtraEditors.LabelControl lbl母亲职业;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private DevExpress.XtraEditors.DateEdit dateEdit3;
        private DevExpress.XtraEditors.LabelControl labelControl72;
        private DevExpress.XtraEditors.LabelControl labelControl71;
        private DevExpress.XtraEditors.LabelControl labelControl70;
        private DevExpress.XtraEditors.LabelControl labelControl69;
        private DevExpress.XtraEditors.LabelControl labelControl68;
        private DevExpress.XtraEditors.LabelControl labelControl67;
        private DevExpress.XtraEditors.TextEdit textEdit28;
        private DevExpress.XtraEditors.LabelControl labelControl66;
        private DevExpress.XtraEditors.LabelControl labelControl65;
        private DevExpress.XtraEditors.LabelControl labelControl64;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel22;
        private DevExpress.XtraEditors.CheckEdit checkEdit25;
        private DevExpress.XtraEditors.CheckEdit checkEdit26;
        private DevExpress.XtraEditors.CheckEdit checkEdit27;
        private DevExpress.XtraEditors.CheckEdit checkEdit28;
        private DevExpress.XtraEditors.CheckEdit checkEdit29;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel21;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit21;
        private DevExpress.XtraEditors.TextEdit textEdit26;
        private DevExpress.XtraEditors.LabelControl labelControl63;
        private DevExpress.XtraEditors.LabelControl labelControl62;
        private DevExpress.XtraEditors.LabelControl labelControl61;
        private DevExpress.XtraEditors.LabelControl labelControl60;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel20;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit20;
        private DevExpress.XtraEditors.TextEdit textEdit25;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel19;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit19;
        private DevExpress.XtraEditors.TextEdit textEdit24;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel18;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit18;
        private DevExpress.XtraEditors.TextEdit textEdit23;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel17;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit17;
        private DevExpress.XtraEditors.TextEdit textEdit22;
        private DevExpress.XtraEditors.LabelControl labelControl59;
        private DevExpress.XtraEditors.LabelControl labelControl58;
        private DevExpress.XtraEditors.LabelControl labelControl57;
        private DevExpress.XtraEditors.LabelControl labelControl56;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel16;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit16;
        private DevExpress.XtraEditors.TextEdit textEdit21;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel15;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit15;
        private DevExpress.XtraEditors.TextEdit textEdit20;
        private DevExpress.XtraEditors.LabelControl labelControl55;
        private DevExpress.XtraEditors.LabelControl labelControl54;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel13;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit14;
        private DevExpress.XtraEditors.TextEdit textEdit18;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit13;
        private DevExpress.XtraEditors.TextEdit textEdit17;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit12;
        private DevExpress.XtraEditors.TextEdit textEdit16;
        private DevExpress.XtraEditors.LabelControl labelControl53;
        private DevExpress.XtraEditors.LabelControl labelControl52;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel14;
        private DevExpress.XtraEditors.CheckEdit checkEdit7;
        private DevExpress.XtraEditors.CheckEdit checkEdit21;
        private DevExpress.XtraEditors.CheckEdit checkEdit23;
        private DevExpress.XtraEditors.CheckEdit checkEdit24;
        private DevExpress.XtraEditors.TextEdit textEdit19;
        private DevExpress.XtraEditors.LabelControl labelControl51;
        private DevExpress.XtraEditors.LabelControl labelControl50;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit11;
        private DevExpress.XtraEditors.TextEdit textEdit15;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit10;
        private DevExpress.XtraEditors.TextEdit textEdit14;
        private DevExpress.XtraEditors.LabelControl labelControl49;
        private DevExpress.XtraEditors.LabelControl labelControl48;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit9;
        private DevExpress.XtraEditors.TextEdit textEdit13;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.LabelControl labelControl44;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit8;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private DevExpress.XtraEditors.LabelControl labelControl46;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private Library.UserControls.UCTxtLbl ucTxtLbl8;
        private Library.UserControls.UCTxtLbl ucTxtLbl7;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEdit5;
        private DevExpress.XtraEditors.CheckEdit checkEdit6;
        private DevExpress.XtraEditors.CheckEdit checkEdit22;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private Library.UserControls.UCTxtLbl ucTxtLbl6;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit7;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit6;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private Library.UserControls.UCTxtLbl ucTxtLbl5;
        private Library.UserControls.UCTxtLbl ucTxtLbl4;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit5;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private DevExpress.XtraEditors.CheckEdit checkEdit15;
        private DevExpress.XtraEditors.CheckEdit checkEdit16;
        private DevExpress.XtraEditors.CheckEdit checkEdit17;
        private DevExpress.XtraEditors.CheckEdit checkEdit18;
        private DevExpress.XtraEditors.CheckEdit checkEdit19;
        private DevExpress.XtraEditors.CheckEdit checkEdit20;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private DevExpress.XtraEditors.CheckEdit checkEdit8;
        private DevExpress.XtraEditors.CheckEdit checkEdit9;
        private DevExpress.XtraEditors.CheckEdit checkEdit10;
        private DevExpress.XtraEditors.CheckEdit checkEdit11;
        private DevExpress.XtraEditors.CheckEdit checkEdit12;
        private DevExpress.XtraEditors.CheckEdit checkEdit13;
        private DevExpress.XtraEditors.CheckEdit checkEdit14;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit3;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit1;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private Library.UserControls.UCTxtLbl ucTxtLbl2;
        private Library.UserControls.UCTxtLbl ucTxtLbl3;
        private Library.UserControls.UCTxtLbl ucTxtLbl1;
        private Library.UserControls.UCTxtLbl ucTxtLbl9;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private Library.UserControls.UCLblTxt ucLblTxt2;
        private Library.UserControls.UCLblTxt ucLblTxt1;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.TextEdit dateEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl73;
        private DevExpress.XtraEditors.LabelControl labelControl74;
        private DevExpress.XtraEditors.LabelControl labelControl75;
        private DevExpress.XtraEditors.LabelControl labelControl76;
        private DevExpress.XtraEditors.LabelControl labelControl77;
        private DevExpress.XtraEditors.LabelControl labelControl78;
        private DevExpress.XtraLayout.Converter.LayoutConverter layoutConverter1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel23;
        private DevExpress.XtraEditors.CheckEdit checkEdit30;
        private DevExpress.XtraEditors.CheckEdit checkEdit31;
        private DevExpress.XtraEditors.CheckEdit checkEdit32;
        private DevExpress.XtraEditors.CheckEdit checkEdit33;
        private DevExpress.XtraEditors.TextEdit textEdit27;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel25;
        private DevExpress.XtraEditors.CheckEdit checkEdit38;
        private DevExpress.XtraEditors.CheckEdit checkEdit39;
        private DevExpress.XtraEditors.CheckEdit checkEdit40;
        private DevExpress.XtraEditors.CheckEdit checkEdit41;
        private DevExpress.XtraEditors.TextEdit textEdit31;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel28;
        private DevExpress.XtraEditors.CheckEdit checkEdit50;
        private DevExpress.XtraEditors.CheckEdit checkEdit51;
        private DevExpress.XtraEditors.CheckEdit checkEdit52;
        private DevExpress.XtraEditors.CheckEdit checkEdit53;
        private DevExpress.XtraEditors.TextEdit textEdit34;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel29;
        private DevExpress.XtraEditors.CheckEdit checkEdit54;
        private DevExpress.XtraEditors.CheckEdit checkEdit55;
        private DevExpress.XtraEditors.CheckEdit checkEdit56;
        private DevExpress.XtraEditors.CheckEdit checkEdit57;
        private DevExpress.XtraEditors.TextEdit textEdit35;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel24;
        private DevExpress.XtraEditors.CheckEdit checkEdit34;
        private DevExpress.XtraEditors.CheckEdit checkEdit35;
        private DevExpress.XtraEditors.CheckEdit checkEdit36;
        private DevExpress.XtraEditors.CheckEdit checkEdit37;
        private DevExpress.XtraEditors.TextEdit textEdit30;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel26;
        private DevExpress.XtraEditors.CheckEdit checkEdit42;
        private DevExpress.XtraEditors.CheckEdit checkEdit43;
        private DevExpress.XtraEditors.CheckEdit checkEdit44;
        private DevExpress.XtraEditors.CheckEdit checkEdit45;
        private DevExpress.XtraEditors.TextEdit textEdit32;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel27;
        private DevExpress.XtraEditors.CheckEdit checkEdit46;
        private DevExpress.XtraEditors.CheckEdit checkEdit47;
        private DevExpress.XtraEditors.CheckEdit checkEdit48;
        private DevExpress.XtraEditors.CheckEdit checkEdit49;
        private DevExpress.XtraEditors.TextEdit textEdit33;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TextEdit textEdit36;
        private DevExpress.XtraEditors.TextEdit textEdit38;
        private DevExpress.XtraEditors.TextEdit textEdit37;
        private DevExpress.XtraEditors.LabelControl labelControl80;
        private DevExpress.XtraEditors.LabelControl labelControl79;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel30;
        private DevExpress.XtraEditors.CheckEdit checkEdit58;
        private DevExpress.XtraEditors.CheckEdit checkEdit59;
        private DevExpress.XtraEditors.CheckEdit checkEdit60;
        private DevExpress.XtraEditors.CheckEdit checkEdit61;
        private DevExpress.XtraEditors.CheckEdit checkEdit62;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.CheckEdit checkEdit67;
        private DevExpress.XtraEditors.CheckEdit checkEdit66;
        private DevExpress.XtraEditors.CheckEdit checkEdit64;
        private DevExpress.XtraEditors.CheckEdit checkEdit63;
        private DevExpress.XtraEditors.CheckEdit checkEdit65;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel31;
        private DevExpress.XtraEditors.CheckEdit checkEdit68;
        private DevExpress.XtraEditors.CheckEdit checkEdit69;
        private DevExpress.XtraEditors.TextEdit textEdit39;

    }
}