﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using AtomEHR.Library;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Common;

namespace AtomEHR.公共卫生.Module.健康教育
{
    public partial class frm健康教育活动列表 : AtomEHR.Library.frmBaseBusinessForm
    {
        public frm健康教育活动列表()
        {
            InitializeComponent();
        }

        private void frm健康教育活动列表_Load(object sender, EventArgs e)
        {
            InitializeForm();
            //活动时间3.EditValue = DateTime.Now.ToString("yyyy-MM-dd");
            //活动时间4.EditValue = DateTime.Now.ToString("yyyy-MM-dd");
        }
        protected override void InitializeForm()
        {
            _BLL = new bll健康教育();// 业务逻辑层实例
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.

            //size = layoutControl1.Height;
            base.InitButtonsBase();
            this.pagerControl1.Height = 35;

            //BindingSummarySearchPanel(btnQuery, null, null);
            btnQuery.Click += new EventHandler(OnSummarySearchClick);


            //为“镇”绑定信息
            DataView dv镇 = new DataView(DataDictCache.Cache.t地区信息);
            dv镇.RowFilter = "上级编码='" + Loginer.CurrentUser.单位代码 + "' ";
            //DataBinder.BindingLookupEditDataSource(lookUpEdit镇, dv市.ToTable(), "地区名称", "地区编码");
            //util.ControlsHelper.BindComboxData(dv镇.ToTable(), comboBoxEdit镇, "地区编码", "地区名称");

            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                treeListLookUpEdit机构.Properties.ValueMember = "机构编号";
                treeListLookUpEdit机构.Properties.DisplayMember = "机构名称";

                treeListLookUpEdit机构.Properties.TreeList.KeyFieldName = "机构编号";
                treeListLookUpEdit机构.Properties.TreeList.ParentFieldName = "上级机构";
                treeListLookUpEdit机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                treeListLookUpEdit机构.Properties.DataSource = dt所属机构;
                //treeListLookUpEdit机构.Properties.TreeList.ExpandAll();
                //treeListLookUpEdit机构.Properties.TreeList.CollapseAll();

                treeListLookUpEdit机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                treeListLookUpEdit机构.Text = Loginer.CurrentUser.所属机构;
            }

            if (Loginer.CurrentUser.所属机构.Length >= 12)
            {
                treeListLookUpEdit机构.Properties.AutoExpandAllNodes = true;
            }
            else
            {
                treeListLookUpEdit机构.Properties.AutoExpandAllNodes = false;
            }

            //BindingSummarySearchPanel(btnQuery, btn删除);

            //string strJGTemp = Loginer.CurrentUser.所属机构.Length >= 6 ? Loginer.CurrentUser.所属机构.Substring(0, 6) : "";
            ////DataTable dt所属机构 = _bll机构信息.Get机构("371321");
            //DataTable dt所属机构 = _bll机构信息.Get机构(strJGTemp);
            //util.ControlsHelper.BindComboxData(dt所属机构, cbo机构, "机构编号", "机构名称");
            //if (Loginer.CurrentUser.所属机构.Length > 6)
            //{
            //    util.ControlsHelper.SetComboxData(Loginer.CurrentUser.所属机构, cbo机构);
            //    cbo机构.Enabled = false;
            //}
        }
        protected override bool DoSearchSummary()
        {
            string _strWhere = " and 是否有效=1 ";//" and " + tb_JTYS医生信息.是否有效 + "=1";

            //string str机构 = util.ControlsHelper.GetComboxKey(textEdit2.Text.Trim());
            this.pagerControl1.InitControl();
            string pgrid = this.treeListLookUpEdit机构.EditValue.ToString();
            if (this.checkEdit21.Checked)//包含下属机构
            {
                if (pgrid.Length == 12)
                {
                    #region  所属机构关联（未用）
                    //_strWhere += " and ([所属机构]=  '" + pgrid + "' or substring([所属机构],1,7)+'D'+substring([所属机构],9,7) like '" + pgrid + "%')";
                    //_strWhere += " and ( [所属机构] in (select 机构编号 from dbo.fn_GetRGID('" + pgrid + "') ))";
                    //_strWhere += " and ([所属机构]=  '" + pgrid + "' or [所属机构] in (select 机构编号 from tb_机构信息 where 上级机构='" + pgrid + "' ) )";
                    //_strWhere += "and ([所属机构]='" + pgrid + "' or [所属机构] in (select 机构编号 from tb_机构信息 where 所属机构 in (select 机构编号 from tb_机构信息 where 上级机构='" + pgrid + "')))";//多级查询
                    #endregion
                    _strWhere += " AND [创建机构] IN (select 机构编号 from tb_机构信息 where 上级机构='" + pgrid + "' or 机构编号='" + pgrid + "' ) ";
                }
                else
                {
                    _strWhere += " and [创建机构] like '" + pgrid + "%'";
                }
            }
            else
            {
                _strWhere += " and [创建机构] ='" + pgrid + "'";
            }
            //if (!string.IsNullOrWhiteSpace(treeListLookUpEdit机构.Text))
            //{
            //    _strWhere = " and " + tb_健康教育.创建机构 + "='" + treeListLookUpEdit机构.Text.Trim() + "' ";
            //}

            if (!string.IsNullOrWhiteSpace(txt主题.Text))
            {
                _strWhere += " and " + tb_健康教育.主题 + " like '%" + txt主题.Text.Trim() + "%' ";
            }
            if (!string.IsNullOrWhiteSpace(txt负责人.Text))
            {
                _strWhere += " and " + tb_健康教育.负责人 + " like '%" + txt负责人.Text.Trim() + "%' ";
            }
            if (!string.IsNullOrWhiteSpace(活动时间3.Text))
            {
                _strWhere += " and " + tb_健康教育.活动日期 + " >= '" + 活动时间3.Text.Trim() + "' ";
            }
            if (!string.IsNullOrWhiteSpace(活动时间4.Text))
            {
                _strWhere += " and " + tb_健康教育.活动日期 + " <='" + 活动时间4.Text.Trim() + "' ";
            }
            if (!string.IsNullOrWhiteSpace(txt活动形式.Text))
            {
                _strWhere += " and " + tb_健康教育.活动形式 + " like '%" + txt活动形式.Text.Trim() + "%' ";
            }

            DataTable dt = this.pagerControl1.GetQueryResultNew("vw_健康教育", "*", _strWhere, tb_健康教育.ID, "DESC").Tables[0];
            this.pagerControl1.DrawControl();

            DoBindingSummaryGrid(dt); //绑定主表的Grid
            ShowSummaryPage(true); //显示Summary页面.   
            gvSummary.BestFitColumns();

            return dt != null && dt.Rows.Count > 0;
        }
        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            DoSearchSummary();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            pagerControl1.InitControl();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            frm新建健康教育活动 frm = new frm新建健康教育活动(UpdateType.Add, "");
            frm.ShowDialog();
        }

        private void sbtnEdit_Click(object sender, EventArgs e)
        {
            int[] selectdIndex = gvSummary.GetSelectedRows();
            if (selectdIndex.Length == 0)
            {
                Msg.ShowInformation("请选择需要修改的行");
                return;
            }
            else if (selectdIndex.Length > 1)
            {
                Msg.ShowInformation("每次只能修改一条数据");
                return;
            }

            string itemid = gvSummary.GetRowCellValue(selectdIndex[0], tb_健康教育.ID).ToString();
            frm新建健康教育活动 frm = new frm新建健康教育活动(UpdateType.Modify, itemid);
            frm.ShowDialog();
        }

        private void btn删除_Click(object sender, EventArgs e)
        {
            try
            {
                int[] selectdIndex = gvSummary.GetSelectedRows();
                if (selectdIndex.Length == 0)
                {
                    Msg.ShowInformation("请选择要修改的行");
                    return;
                }
                else if (selectdIndex.Length > 1)
                {
                    Msg.ShowInformation("每次只能修改一条数据");
                    return;
                }

                string id = gvSummary.GetRowCellValue(selectdIndex[0], tb_健康教育.ID).ToString();
                //string serobj = gvSummary.GetRowCellValue(selectdIndex[0], "签约对象名称").ToString();
                string spname = gvSummary.GetRowCellValue(selectdIndex[0], "活动编号").ToString();

                if (!Msg.AskQuestion("确定要删除活动编号为" + spname + "的记录吗？")) return;

                int count = ((bll健康教育)_BLL).Update是否禁用(id, false);
                 

                if (count > 0)
                {
                    DoSearchSummary();
                }
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }

    }
}