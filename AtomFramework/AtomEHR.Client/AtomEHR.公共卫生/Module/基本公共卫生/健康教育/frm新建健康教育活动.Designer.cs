﻿namespace AtomEHR.公共卫生.Module.健康教育
{
    partial class frm新建健康教育活动
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.chk书面材料 = new DevExpress.XtraEditors.CheckEdit();
            this.chk图片材料 = new DevExpress.XtraEditors.CheckEdit();
            this.chk印刷材料 = new DevExpress.XtraEditors.CheckEdit();
            this.chk影音材料 = new DevExpress.XtraEditors.CheckEdit();
            this.chk签到表 = new DevExpress.XtraEditors.CheckEdit();
            this.chk其他材料 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnClose = new DevExpress.XtraEditors.SimpleButton();
            this.BtnSave = new DevExpress.XtraEditors.SimpleButton();
            this.txt负责人 = new DevExpress.XtraEditors.TextEdit();
            this.txt填表人 = new DevExpress.XtraEditors.TextEdit();
            this.txt资料数 = new DevExpress.XtraEditors.TextEdit();
            this.txt资料类 = new DevExpress.XtraEditors.TextEdit();
            this.txt人数 = new DevExpress.XtraEditors.TextEdit();
            this.txt人员类别 = new DevExpress.XtraEditors.TextEdit();
            this.txt主讲单位 = new DevExpress.XtraEditors.TextEdit();
            this.txt职称 = new DevExpress.XtraEditors.TextEdit();
            this.txt主讲 = new DevExpress.XtraEditors.TextEdit();
            this.txt组织者 = new DevExpress.XtraEditors.TextEdit();
            this.txt主题 = new DevExpress.XtraEditors.TextEdit();
            this.txt活动形式 = new DevExpress.XtraEditors.TextEdit();
            this.txtAddrs = new DevExpress.XtraEditors.TextEdit();
            this.txtNumber = new DevExpress.XtraEditors.TextEdit();
            this.txt内容 = new DevExpress.XtraEditors.MemoEdit();
            this.txt评价 = new DevExpress.XtraEditors.MemoEdit();
            this.txt活动时间 = new DevExpress.XtraEditors.DateEdit();
            this.txt填表时间 = new DevExpress.XtraEditors.DateEdit();
            this.txt教育项目 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk书面材料.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk图片材料.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk印刷材料.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk影音材料.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk签到表.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他材料.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt负责人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt填表人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt资料数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt资料类.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt人数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt人员类别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt主讲单位.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职称.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt主讲.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt组织者.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt主题.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt活动形式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddrs.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt内容.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt评价.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt活动时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt活动时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt填表时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt填表时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt教育项目.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.BtnClose);
            this.layoutControl1.Controls.Add(this.BtnSave);
            this.layoutControl1.Controls.Add(this.txt负责人);
            this.layoutControl1.Controls.Add(this.txt填表人);
            this.layoutControl1.Controls.Add(this.txt资料数);
            this.layoutControl1.Controls.Add(this.txt资料类);
            this.layoutControl1.Controls.Add(this.txt人数);
            this.layoutControl1.Controls.Add(this.txt人员类别);
            this.layoutControl1.Controls.Add(this.txt主讲单位);
            this.layoutControl1.Controls.Add(this.txt职称);
            this.layoutControl1.Controls.Add(this.txt主讲);
            this.layoutControl1.Controls.Add(this.txt组织者);
            this.layoutControl1.Controls.Add(this.txt主题);
            this.layoutControl1.Controls.Add(this.txt活动形式);
            this.layoutControl1.Controls.Add(this.txtAddrs);
            this.layoutControl1.Controls.Add(this.txtNumber);
            this.layoutControl1.Controls.Add(this.txt内容);
            this.layoutControl1.Controls.Add(this.txt评价);
            this.layoutControl1.Controls.Add(this.txt活动时间);
            this.layoutControl1.Controls.Add(this.txt填表时间);
            this.layoutControl1.Controls.Add(this.txt教育项目);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(841, 391, 554, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(779, 536);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panelControl1.Controls.Add(this.chk书面材料);
            this.panelControl1.Controls.Add(this.chk图片材料);
            this.panelControl1.Controls.Add(this.chk印刷材料);
            this.panelControl1.Controls.Add(this.chk影音材料);
            this.panelControl1.Controls.Add(this.chk签到表);
            this.panelControl1.Controls.Add(this.chk其他材料);
            this.panelControl1.Location = new System.Drawing.Point(153, 405);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(602, 33);
            this.panelControl1.TabIndex = 32;
            // 
            // chk书面材料
            // 
            this.chk书面材料.Location = new System.Drawing.Point(18, 5);
            this.chk书面材料.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chk书面材料.Name = "chk书面材料";
            this.chk书面材料.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.chk书面材料.Properties.Caption = "书面材料";
            this.chk书面材料.Size = new System.Drawing.Size(88, 19);
            this.chk书面材料.TabIndex = 0;
            // 
            // chk图片材料
            // 
            this.chk图片材料.Location = new System.Drawing.Point(112, 5);
            this.chk图片材料.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chk图片材料.Name = "chk图片材料";
            this.chk图片材料.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.chk图片材料.Properties.Caption = "图片材料";
            this.chk图片材料.Size = new System.Drawing.Size(88, 19);
            this.chk图片材料.TabIndex = 1;
            // 
            // chk印刷材料
            // 
            this.chk印刷材料.Location = new System.Drawing.Point(205, 5);
            this.chk印刷材料.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chk印刷材料.Name = "chk印刷材料";
            this.chk印刷材料.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.chk印刷材料.Properties.Caption = "印刷材料";
            this.chk印刷材料.Size = new System.Drawing.Size(88, 19);
            this.chk印刷材料.TabIndex = 2;
            // 
            // chk影音材料
            // 
            this.chk影音材料.Location = new System.Drawing.Point(298, 5);
            this.chk影音材料.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chk影音材料.Name = "chk影音材料";
            this.chk影音材料.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.chk影音材料.Properties.Caption = "影音材料";
            this.chk影音材料.Size = new System.Drawing.Size(88, 19);
            this.chk影音材料.TabIndex = 3;
            // 
            // chk签到表
            // 
            this.chk签到表.Location = new System.Drawing.Point(390, 5);
            this.chk签到表.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chk签到表.Name = "chk签到表";
            this.chk签到表.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.chk签到表.Properties.Caption = "签到表";
            this.chk签到表.Size = new System.Drawing.Size(88, 19);
            this.chk签到表.TabIndex = 4;
            // 
            // chk其他材料
            // 
            this.chk其他材料.Location = new System.Drawing.Point(483, 5);
            this.chk其他材料.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chk其他材料.Name = "chk其他材料";
            this.chk其他材料.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.chk其他材料.Properties.Caption = "其他材料";
            this.chk其他材料.Size = new System.Drawing.Size(88, 19);
            this.chk其他材料.TabIndex = 5;
            // 
            // BtnClose
            // 
            this.BtnClose.Location = new System.Drawing.Point(573, 490);
            this.BtnClose.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnClose.Name = "BtnClose";
            this.BtnClose.Size = new System.Drawing.Size(78, 22);
            this.BtnClose.StyleController = this.layoutControl1;
            this.BtnClose.TabIndex = 25;
            this.BtnClose.Text = "关闭";
            this.BtnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(437, 490);
            this.BtnSave.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(85, 22);
            this.BtnSave.StyleController = this.layoutControl1;
            this.BtnSave.TabIndex = 24;
            this.BtnSave.Text = "保存";
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // txt负责人
            // 
            this.txt负责人.Location = new System.Drawing.Point(153, 442);
            this.txt负责人.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt负责人.Name = "txt负责人";
            this.txt负责人.Size = new System.Drawing.Size(224, 20);
            this.txt负责人.StyleController = this.layoutControl1;
            this.txt负责人.TabIndex = 22;
            // 
            // txt填表人
            // 
            this.txt填表人.Location = new System.Drawing.Point(510, 442);
            this.txt填表人.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt填表人.Name = "txt填表人";
            this.txt填表人.Properties.ReadOnly = true;
            this.txt填表人.Size = new System.Drawing.Size(245, 20);
            this.txt填表人.StyleController = this.layoutControl1;
            this.txt填表人.TabIndex = 21;
            // 
            // txt资料数
            // 
            this.txt资料数.Location = new System.Drawing.Point(510, 243);
            this.txt资料数.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt资料数.Name = "txt资料数";
            this.txt资料数.Size = new System.Drawing.Size(245, 20);
            this.txt资料数.StyleController = this.layoutControl1;
            this.txt资料数.TabIndex = 17;
            // 
            // txt资料类
            // 
            this.txt资料类.Location = new System.Drawing.Point(153, 243);
            this.txt资料类.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt资料类.Name = "txt资料类";
            this.txt资料类.Size = new System.Drawing.Size(224, 20);
            this.txt资料类.StyleController = this.layoutControl1;
            this.txt资料类.TabIndex = 16;
            // 
            // txt人数
            // 
            this.txt人数.Location = new System.Drawing.Point(510, 219);
            this.txt人数.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt人数.Name = "txt人数";
            this.txt人数.Size = new System.Drawing.Size(245, 20);
            this.txt人数.StyleController = this.layoutControl1;
            this.txt人数.TabIndex = 15;
            // 
            // txt人员类别
            // 
            this.txt人员类别.Location = new System.Drawing.Point(153, 219);
            this.txt人员类别.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt人员类别.Name = "txt人员类别";
            this.txt人员类别.Size = new System.Drawing.Size(224, 20);
            this.txt人员类别.StyleController = this.layoutControl1;
            this.txt人员类别.TabIndex = 14;
            // 
            // txt主讲单位
            // 
            this.txt主讲单位.Location = new System.Drawing.Point(153, 195);
            this.txt主讲单位.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt主讲单位.Name = "txt主讲单位";
            this.txt主讲单位.Size = new System.Drawing.Size(602, 20);
            this.txt主讲单位.StyleController = this.layoutControl1;
            this.txt主讲单位.TabIndex = 13;
            // 
            // txt职称
            // 
            this.txt职称.Location = new System.Drawing.Point(509, 171);
            this.txt职称.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt职称.Name = "txt职称";
            this.txt职称.Size = new System.Drawing.Size(246, 20);
            this.txt职称.StyleController = this.layoutControl1;
            this.txt职称.TabIndex = 12;
            // 
            // txt主讲
            // 
            this.txt主讲.Location = new System.Drawing.Point(153, 171);
            this.txt主讲.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt主讲.Name = "txt主讲";
            this.txt主讲.Size = new System.Drawing.Size(223, 20);
            this.txt主讲.StyleController = this.layoutControl1;
            this.txt主讲.TabIndex = 11;
            // 
            // txt组织者
            // 
            this.txt组织者.Location = new System.Drawing.Point(153, 147);
            this.txt组织者.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt组织者.Name = "txt组织者";
            this.txt组织者.Size = new System.Drawing.Size(602, 20);
            this.txt组织者.StyleController = this.layoutControl1;
            this.txt组织者.TabIndex = 10;
            // 
            // txt主题
            // 
            this.txt主题.Location = new System.Drawing.Point(153, 123);
            this.txt主题.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt主题.Name = "txt主题";
            this.txt主题.Size = new System.Drawing.Size(602, 20);
            this.txt主题.StyleController = this.layoutControl1;
            this.txt主题.TabIndex = 9;
            // 
            // txt活动形式
            // 
            this.txt活动形式.Location = new System.Drawing.Point(508, 99);
            this.txt活动形式.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt活动形式.Name = "txt活动形式";
            this.txt活动形式.Size = new System.Drawing.Size(247, 20);
            this.txt活动形式.StyleController = this.layoutControl1;
            this.txt活动形式.TabIndex = 8;
            // 
            // txtAddrs
            // 
            this.txtAddrs.Location = new System.Drawing.Point(153, 99);
            this.txtAddrs.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtAddrs.Name = "txtAddrs";
            this.txtAddrs.Size = new System.Drawing.Size(222, 20);
            this.txtAddrs.StyleController = this.layoutControl1;
            this.txtAddrs.TabIndex = 7;
            // 
            // txtNumber
            // 
            this.txtNumber.Location = new System.Drawing.Point(153, 75);
            this.txtNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new System.Drawing.Size(222, 20);
            this.txtNumber.StyleController = this.layoutControl1;
            this.txtNumber.TabIndex = 5;
            // 
            // txt内容
            // 
            this.txt内容.Location = new System.Drawing.Point(153, 267);
            this.txt内容.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt内容.Name = "txt内容";
            this.txt内容.Size = new System.Drawing.Size(224, 134);
            this.txt内容.StyleController = this.layoutControl1;
            this.txt内容.TabIndex = 18;
            this.txt内容.UseOptimizedRendering = true;
            // 
            // txt评价
            // 
            this.txt评价.Location = new System.Drawing.Point(510, 267);
            this.txt评价.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt评价.Name = "txt评价";
            this.txt评价.Size = new System.Drawing.Size(245, 134);
            this.txt评价.StyleController = this.layoutControl1;
            this.txt评价.TabIndex = 19;
            this.txt评价.UseOptimizedRendering = true;
            // 
            // txt活动时间
            // 
            this.txt活动时间.EditValue = null;
            this.txt活动时间.Location = new System.Drawing.Point(508, 75);
            this.txt活动时间.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt活动时间.Name = "txt活动时间";
            this.txt活动时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt活动时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt活动时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt活动时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt活动时间.Size = new System.Drawing.Size(247, 20);
            this.txt活动时间.StyleController = this.layoutControl1;
            this.txt活动时间.TabIndex = 6;
            // 
            // txt填表时间
            // 
            this.txt填表时间.EditValue = null;
            this.txt填表时间.Location = new System.Drawing.Point(153, 466);
            this.txt填表时间.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt填表时间.Name = "txt填表时间";
            this.txt填表时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt填表时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt填表时间.Properties.Mask.EditMask = "yyyy-MM-dd HH:mm:ss";
            this.txt填表时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt填表时间.Properties.ReadOnly = true;
            this.txt填表时间.Size = new System.Drawing.Size(602, 20);
            this.txt填表时间.StyleController = this.layoutControl1;
            this.txt填表时间.TabIndex = 23;
            // 
            // txt教育项目
            // 
            this.txt教育项目.EditValue = "请选择";
            this.txt教育项目.Location = new System.Drawing.Point(153, 51);
            this.txt教育项目.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt教育项目.Name = "txt教育项目";
            this.txt教育项目.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt教育项目.Properties.Items.AddRange(new object[] {
            "",
            "发放、播放宣传资料",
            "设置健康教育宣传栏",
            "开展公众健康咨询",
            "举办健康知识讲座"});
            this.txt教育项目.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txt教育项目.Size = new System.Drawing.Size(602, 20);
            this.txt教育项目.StyleController = this.layoutControl1;
            this.txt教育项目.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(779, 536);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F);
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup2.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup2.CustomizationFormText = "国家基本公共卫生服务项目健康教育活动记录表";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem3,
            this.layoutControlItem6,
            this.layoutControlItem5,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem10,
            this.layoutControlItem9,
            this.layoutControlItem11,
            this.layoutControlItem13,
            this.layoutControlItem12,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem14,
            this.layoutControlItem20,
            this.layoutControlItem19,
            this.layoutControlItem18,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem29,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.emptySpaceItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(759, 516);
            this.layoutControlGroup2.Text = "国家基本公共卫生服务项目健康教育活动记录表";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txt教育项目;
            this.layoutControlItem1.CustomizationFormText = "健康教育项目:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(735, 24);
            this.layoutControlItem1.Text = "健康教育项目:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(126, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtNumber;
            this.layoutControlItem2.CustomizationFormText = "活动编号:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(355, 24);
            this.layoutControlItem2.Text = "活动编号:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(126, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtAddrs;
            this.layoutControlItem4.CustomizationFormText = "活动地点:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(355, 24);
            this.layoutControlItem4.Text = "活动地点:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(126, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txt活动时间;
            this.layoutControlItem3.CustomizationFormText = "活动时间:";
            this.layoutControlItem3.Location = new System.Drawing.Point(355, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(380, 24);
            this.layoutControlItem3.Text = "活动时间:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(126, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txt主题;
            this.layoutControlItem6.CustomizationFormText = "活动主题:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(735, 24);
            this.layoutControlItem6.Text = "活动主题:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(126, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txt活动形式;
            this.layoutControlItem5.CustomizationFormText = "活动形式:";
            this.layoutControlItem5.Location = new System.Drawing.Point(355, 48);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(380, 24);
            this.layoutControlItem5.Text = "活动形式:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(126, 14);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txt组织者;
            this.layoutControlItem7.CustomizationFormText = "组织者";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(735, 24);
            this.layoutControlItem7.Text = "组织者";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(126, 14);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txt主讲;
            this.layoutControlItem8.CustomizationFormText = "主讲人姓名:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(356, 24);
            this.layoutControlItem8.Text = "主讲人姓名:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(126, 14);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txt主讲单位;
            this.layoutControlItem10.CustomizationFormText = "主讲人单位:";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(735, 24);
            this.layoutControlItem10.Text = "主讲人单位:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(126, 14);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txt职称;
            this.layoutControlItem9.CustomizationFormText = "职称:";
            this.layoutControlItem9.Location = new System.Drawing.Point(356, 120);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(379, 24);
            this.layoutControlItem9.Text = "职称:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(126, 14);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.txt人员类别;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 168);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(357, 24);
            this.layoutControlItem11.Text = "接受健康教育人员类别:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(126, 14);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.txt资料类;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 192);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(357, 24);
            this.layoutControlItem13.Text = "健康教育资料发放种类:";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(126, 14);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.txt人数;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(357, 168);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem12.Text = "接受健康教育人数:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(126, 14);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.txt内容;
            this.layoutControlItem15.CustomizationFormText = "活动内容(字数少于2000字):";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 216);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(357, 138);
            this.layoutControlItem15.Text = "活动内容(少于2000字):";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(126, 14);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.txt评价;
            this.layoutControlItem16.CustomizationFormText = "活动总评价:";
            this.layoutControlItem16.Location = new System.Drawing.Point(357, 216);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(378, 138);
            this.layoutControlItem16.Text = "活动总评价:";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(126, 14);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.txt资料数;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(357, 192);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem14.Text = "健康教育资料发放数量:";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(126, 14);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.txt填表时间;
            this.layoutControlItem20.CustomizationFormText = "填表时间:";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 415);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(735, 24);
            this.layoutControlItem20.Text = "填表时间:";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(126, 14);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.txt负责人;
            this.layoutControlItem19.CustomizationFormText = "负责人签字:";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 391);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(357, 24);
            this.layoutControlItem19.Text = "负责人:";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(126, 14);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.txt填表人;
            this.layoutControlItem18.CustomizationFormText = "填表人签字:";
            this.layoutControlItem18.Location = new System.Drawing.Point(357, 391);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem18.Text = "填表人:";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(126, 14);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.BtnSave;
            this.layoutControlItem21.CustomizationFormText = "layoutControlItem21";
            this.layoutControlItem21.Location = new System.Drawing.Point(413, 439);
            this.layoutControlItem21.MaxSize = new System.Drawing.Size(89, 26);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(89, 26);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(89, 26);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "layoutControlItem21";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextToControlDistance = 0;
            this.layoutControlItem21.TextVisible = false;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.BtnClose;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(549, 439);
            this.layoutControlItem22.MaxSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(82, 26);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "layoutControlItem22";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextToControlDistance = 0;
            this.layoutControlItem22.TextVisible = false;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.panelControl1;
            this.layoutControlItem29.CustomizationFormText = "layoutControlItem29";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 354);
            this.layoutControlItem29.MaxSize = new System.Drawing.Size(0, 37);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(257, 37);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(735, 37);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Text = "存档材料请附后:";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(126, 14);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 439);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(413, 26);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(502, 439);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(47, 26);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(47, 26);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(47, 26);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(631, 439);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(104, 26);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // frm新建健康教育活动
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 536);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frm新建健康教育活动";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "编辑健康教育活动";
            this.Load += new System.EventHandler(this.frm新建健康教育活动_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk书面材料.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk图片材料.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk印刷材料.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk影音材料.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk签到表.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他材料.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt负责人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt填表人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt资料数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt资料类.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt人数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt人员类别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt主讲单位.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职称.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt主讲.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt组织者.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt主题.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt活动形式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddrs.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt内容.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt评价.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt活动时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt活动时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt填表时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt填表时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt教育项目.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.TextEdit txt资料数;
        private DevExpress.XtraEditors.TextEdit txt资料类;
        private DevExpress.XtraEditors.TextEdit txt人数;
        private DevExpress.XtraEditors.TextEdit txt人员类别;
        private DevExpress.XtraEditors.TextEdit txt主讲单位;
        private DevExpress.XtraEditors.TextEdit txt职称;
        private DevExpress.XtraEditors.TextEdit txt主讲;
        private DevExpress.XtraEditors.TextEdit txt组织者;
        private DevExpress.XtraEditors.TextEdit txt主题;
        private DevExpress.XtraEditors.TextEdit txt活动形式;
        private DevExpress.XtraEditors.TextEdit txtAddrs;
        private DevExpress.XtraEditors.TextEdit txtNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.TextEdit txt负责人;
        private DevExpress.XtraEditors.TextEdit txt填表人;
        private DevExpress.XtraEditors.MemoEdit txt内容;
        private DevExpress.XtraEditors.MemoEdit txt评价;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckEdit chk书面材料;
        private DevExpress.XtraEditors.CheckEdit chk图片材料;
        private DevExpress.XtraEditors.CheckEdit chk印刷材料;
        private DevExpress.XtraEditors.CheckEdit chk影音材料;
        private DevExpress.XtraEditors.CheckEdit chk签到表;
        private DevExpress.XtraEditors.CheckEdit chk其他材料;
        private DevExpress.XtraEditors.SimpleButton BtnClose;
        private DevExpress.XtraEditors.SimpleButton BtnSave;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraEditors.DateEdit txt活动时间;
        private DevExpress.XtraEditors.DateEdit txt填表时间;
        private DevExpress.XtraEditors.ComboBoxEdit txt教育项目;



    }
}