﻿namespace AtomEHR.公共卫生.Module.健康教育
{
    partial class frm健康教育活动列表
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm健康教育活动列表));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.checkEdit21 = new DevExpress.XtraEditors.CheckEdit();
            this.treeListLookUpEdit机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.txt活动形式 = new DevExpress.XtraEditors.TextEdit();
            this.txt负责人 = new DevExpress.XtraEditors.TextEdit();
            this.txt主题 = new DevExpress.XtraEditors.TextEdit();
            this.活动时间3 = new DevExpress.XtraEditors.DateEdit();
            this.活动时间4 = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.btn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.gcSummary = new AtomEHR.Library.UserControls.DataGridControl();
            this.gvSummary = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pagerControl1 = new TActionProject.PagerControl();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt活动形式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt负责人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt主题.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.活动时间3.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.活动时间3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.活动时间4.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.活动时间4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gcSummary);
            this.tpSummary.Controls.Add(this.pagerControl1);
            this.tpSummary.Controls.Add(this.flowLayoutPanel1);
            this.tpSummary.Controls.Add(this.layoutControl1);
            this.tpSummary.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tpSummary.Size = new System.Drawing.Size(842, 440);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlSummary.Size = new System.Drawing.Size(848, 446);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tcBusiness.Size = new System.Drawing.Size(848, 446);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tpDetail.Size = new System.Drawing.Size(932, 589);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gcNavigator.Size = new System.Drawing.Size(848, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(670, 2);
            this.controlNavigatorSummary.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            // 
            // txtFocusForSave
            // 
            this.txtFocusForSave.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(473, 2);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.checkEdit21);
            this.layoutControl1.Controls.Add(this.treeListLookUpEdit机构);
            this.layoutControl1.Controls.Add(this.txt活动形式);
            this.layoutControl1.Controls.Add(this.txt负责人);
            this.layoutControl1.Controls.Add(this.txt主题);
            this.layoutControl1.Controls.Add(this.活动时间3);
            this.layoutControl1.Controls.Add(this.活动时间4);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(68, 204, 528, 509);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(842, 121);
            this.layoutControl1.TabIndex = 16;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // checkEdit21
            // 
            this.checkEdit21.EditValue = true;
            this.checkEdit21.Location = new System.Drawing.Point(248, 44);
            this.checkEdit21.Name = "checkEdit21";
            this.checkEdit21.Properties.Caption = "含下属机构";
            this.checkEdit21.Size = new System.Drawing.Size(167, 19);
            this.checkEdit21.StyleController = this.layoutControl1;
            this.checkEdit21.TabIndex = 135;
            // 
            // treeListLookUpEdit机构
            // 
            this.treeListLookUpEdit机构.Location = new System.Drawing.Point(75, 44);
            this.treeListLookUpEdit机构.Name = "treeListLookUpEdit机构";
            this.treeListLookUpEdit机构.Properties.AutoExpandAllNodes = false;
            this.treeListLookUpEdit机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEdit机构.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.treeListLookUpEdit机构.Size = new System.Drawing.Size(169, 20);
            this.treeListLookUpEdit机构.StyleController = this.layoutControl1;
            this.treeListLookUpEdit机构.TabIndex = 124;
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(284, 251);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // txt活动形式
            // 
            this.txt活动形式.Location = new System.Drawing.Point(470, 68);
            this.txt活动形式.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt活动形式.Name = "txt活动形式";
            this.txt活动形式.Size = new System.Drawing.Size(155, 20);
            this.txt活动形式.StyleController = this.layoutControl1;
            this.txt活动形式.TabIndex = 9;
            // 
            // txt负责人
            // 
            this.txt负责人.Location = new System.Drawing.Point(680, 44);
            this.txt负责人.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt负责人.Name = "txt负责人";
            this.txt负责人.Size = new System.Drawing.Size(138, 20);
            this.txt负责人.StyleController = this.layoutControl1;
            this.txt负责人.TabIndex = 6;
            // 
            // txt主题
            // 
            this.txt主题.Location = new System.Drawing.Point(470, 44);
            this.txt主题.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt主题.Name = "txt主题";
            this.txt主题.Size = new System.Drawing.Size(155, 20);
            this.txt主题.StyleController = this.layoutControl1;
            this.txt主题.TabIndex = 4;
            // 
            // 活动时间3
            // 
            this.活动时间3.EditValue = null;
            this.活动时间3.Location = new System.Drawing.Point(75, 68);
            this.活动时间3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.活动时间3.Name = "活动时间3";
            this.活动时间3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.活动时间3.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.活动时间3.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.活动时间3.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.活动时间3.Size = new System.Drawing.Size(169, 20);
            this.活动时间3.StyleController = this.layoutControl1;
            this.活动时间3.TabIndex = 7;
            // 
            // 活动时间4
            // 
            this.活动时间4.EditValue = null;
            this.活动时间4.Location = new System.Drawing.Point(262, 68);
            this.活动时间4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.活动时间4.Name = "活动时间4";
            this.活动时间4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.活动时间4.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.活动时间4.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.活动时间4.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.活动时间4.Size = new System.Drawing.Size(153, 20);
            this.活动时间4.StyleController = this.layoutControl1;
            this.活动时间4.TabIndex = 8;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(842, 121);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "查询条件";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem2,
            this.emptySpaceItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(822, 101);
            this.layoutControlGroup2.Text = "查询条件";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txt主题;
            this.layoutControlItem1.CustomizationFormText = "活动主题";
            this.layoutControlItem1.Location = new System.Drawing.Point(395, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(210, 24);
            this.layoutControlItem1.Text = "活动主题";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(48, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txt负责人;
            this.layoutControlItem3.CustomizationFormText = "负责人";
            this.layoutControlItem3.Location = new System.Drawing.Point(605, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(193, 24);
            this.layoutControlItem3.Text = "负责人";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(48, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.活动时间3;
            this.layoutControlItem4.CustomizationFormText = "活动时间";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(224, 33);
            this.layoutControlItem4.Text = "活动时间";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(48, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.活动时间4;
            this.layoutControlItem5.CustomizationFormText = "~";
            this.layoutControlItem5.Location = new System.Drawing.Point(224, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(171, 33);
            this.layoutControlItem5.Text = "~";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(9, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txt活动形式;
            this.layoutControlItem6.CustomizationFormText = "活动形式";
            this.layoutControlItem6.Location = new System.Drawing.Point(395, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(210, 33);
            this.layoutControlItem6.Text = "活动形式";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(48, 14);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.treeListLookUpEdit机构;
            this.layoutControlItem7.CustomizationFormText = "机构";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(224, 24);
            this.layoutControlItem7.Text = "机构";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(48, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.checkEdit21;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(224, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(171, 24);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(605, 24);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(193, 33);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnQuery);
            this.flowLayoutPanel1.Controls.Add(this.btnAdd);
            this.flowLayoutPanel1.Controls.Add(this.sbtnEdit);
            this.flowLayoutPanel1.Controls.Add(this.btn删除);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 121);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(100, 0, 0, 0);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(842, 34);
            this.flowLayoutPanel1.TabIndex = 132;
            // 
            // btnQuery
            // 
            this.btnQuery.Image = ((System.Drawing.Image)(resources.GetObject("btnQuery.Image")));
            this.btnQuery.Location = new System.Drawing.Point(103, 3);
            this.btnQuery.MinimumSize = new System.Drawing.Size(75, 22);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(75, 22);
            this.btnQuery.TabIndex = 0;
            this.btnQuery.Text = "查询";
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Location = new System.Drawing.Point(184, 3);
            this.btnAdd.MinimumSize = new System.Drawing.Size(75, 22);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 22);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "添加";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // sbtnEdit
            // 
            this.sbtnEdit.Image = ((System.Drawing.Image)(resources.GetObject("sbtnEdit.Image")));
            this.sbtnEdit.Location = new System.Drawing.Point(265, 3);
            this.sbtnEdit.MinimumSize = new System.Drawing.Size(75, 22);
            this.sbtnEdit.Name = "sbtnEdit";
            this.sbtnEdit.Size = new System.Drawing.Size(75, 22);
            this.sbtnEdit.TabIndex = 7;
            this.sbtnEdit.Text = "修改";
            this.sbtnEdit.Click += new System.EventHandler(this.sbtnEdit_Click);
            // 
            // btn删除
            // 
            this.btn删除.Image = ((System.Drawing.Image)(resources.GetObject("btn删除.Image")));
            this.btn删除.Location = new System.Drawing.Point(346, 3);
            this.btn删除.MinimumSize = new System.Drawing.Size(75, 22);
            this.btn删除.Name = "btn删除";
            this.btn删除.Size = new System.Drawing.Size(75, 22);
            this.btn删除.TabIndex = 3;
            this.btn删除.Text = "删除";
            this.btn删除.Click += new System.EventHandler(this.btn删除_Click);
            // 
            // gcSummary
            // 
            this.gcSummary.AllowBandedGridColumnSort = false;
            this.gcSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSummary.IsBestFitColumns = true;
            this.gcSummary.Location = new System.Drawing.Point(0, 155);
            this.gcSummary.MainView = this.gvSummary;
            this.gcSummary.Name = "gcSummary";
            this.gcSummary.ShowContextMenu = false;
            this.gcSummary.Size = new System.Drawing.Size(842, 247);
            this.gcSummary.StrWhere = "";
            this.gcSummary.TabIndex = 133;
            this.gcSummary.UseCheckBox = false;
            this.gcSummary.View = "";
            this.gcSummary.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSummary});
            // 
            // gvSummary
            // 
            this.gvSummary.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gvSummary.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gvSummary.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gvSummary.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.DetailTip.Options.UseFont = true;
            this.gvSummary.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Empty.Options.UseFont = true;
            this.gvSummary.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.EvenRow.Options.UseFont = true;
            this.gvSummary.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gvSummary.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FilterPanel.Options.UseFont = true;
            this.gvSummary.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FixedLine.Options.UseFont = true;
            this.gvSummary.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FocusedCell.Options.UseFont = true;
            this.gvSummary.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FocusedRow.Options.UseFont = true;
            this.gvSummary.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gvSummary.Appearance.FooterPanel.Options.UseFont = true;
            this.gvSummary.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupButton.Options.UseFont = true;
            this.gvSummary.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupFooter.Options.UseFont = true;
            this.gvSummary.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupPanel.Options.UseFont = true;
            this.gvSummary.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupRow.Options.UseFont = true;
            this.gvSummary.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvSummary.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gvSummary.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HorzLine.Options.UseFont = true;
            this.gvSummary.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.OddRow.Options.UseFont = true;
            this.gvSummary.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Preview.Options.UseFont = true;
            this.gvSummary.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Row.Options.UseFont = true;
            this.gvSummary.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.RowSeparator.Options.UseFont = true;
            this.gvSummary.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.SelectedRow.Options.UseFont = true;
            this.gvSummary.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.TopNewRow.Options.UseFont = true;
            this.gvSummary.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.VertLine.Options.UseFont = true;
            this.gvSummary.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ViewCaption.Options.UseFont = true;
            this.gvSummary.ColumnPanelRowHeight = 30;
            this.gvSummary.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn9,
            this.gridColumn8,
            this.gridColumn6,
            this.gridColumn7});
            this.gvSummary.GridControl = this.gcSummary;
            this.gvSummary.GroupPanelText = "DragColumn";
            this.gvSummary.Name = "gvSummary";
            this.gvSummary.OptionsBehavior.Editable = false;
            this.gvSummary.OptionsView.ColumnAutoWidth = false;
            this.gvSummary.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "活动编号";
            this.gridColumn2.FieldName = "活动编号";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "活动时间";
            this.gridColumn3.FieldName = "活动日期";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "活动地点";
            this.gridColumn4.FieldName = "活动地点";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "活动主题";
            this.gridColumn5.FieldName = "主题";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "创建人";
            this.gridColumn6.FieldName = "创建人姓名";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 7;
            // 
            // pagerControl1
            // 
            this.pagerControl1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pagerControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pagerControl1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pagerControl1.JumpText = "跳转";
            this.pagerControl1.Location = new System.Drawing.Point(0, 402);
            this.pagerControl1.Margin = new System.Windows.Forms.Padding(6, 16, 6, 16);
            this.pagerControl1.MaximumSize = new System.Drawing.Size(0, 36512);
            this.pagerControl1.Name = "pagerControl1";
            this.pagerControl1.PageIndex = 1;
            this.pagerControl1.PageSize = 15;
            this.pagerControl1.paramters = null;
            this.pagerControl1.RecordCount = 0;
            this.pagerControl1.Size = new System.Drawing.Size(842, 38);
            this.pagerControl1.SqlQuery = null;
            this.pagerControl1.TabIndex = 134;
            this.pagerControl1.OnPageChanged += new System.EventHandler(this.pagerControl1_OnPageChanged);
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "负责人";
            this.gridColumn8.FieldName = "负责人";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 5;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "活动形式";
            this.gridColumn9.FieldName = "活动形式";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 4;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "创建时间";
            this.gridColumn7.FieldName = "创建日期";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 7;
            // 
            // frm健康教育活动列表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(848, 472);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frm健康教育活动列表";
            this.Text = "健康教育活动列表";
            this.Load += new System.EventHandler(this.frm健康教育活动列表_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt活动形式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt负责人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt主题.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.活动时间3.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.活动时间3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.活动时间4.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.活动时间4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txt活动形式;
        private DevExpress.XtraEditors.TextEdit txt负责人;
        private DevExpress.XtraEditors.TextEdit txt主题;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.SimpleButton sbtnEdit;
        private DevExpress.XtraEditors.SimpleButton btn删除;
        private Library.UserControls.DataGridControl gcSummary;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSummary;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private TActionProject.PagerControl pagerControl1;
        private DevExpress.XtraEditors.DateEdit 活动时间3;
        private DevExpress.XtraEditors.DateEdit 活动时间4;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEdit机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.CheckEdit checkEdit21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
    }
}