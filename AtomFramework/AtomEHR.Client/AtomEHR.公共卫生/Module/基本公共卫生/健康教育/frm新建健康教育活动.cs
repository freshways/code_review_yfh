﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using AtomEHR.Library;
using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Models;

namespace AtomEHR.公共卫生.Module.健康教育
{
    public partial class frm新建健康教育活动 : frmBase
    {
        bll健康教育 bll = new bll健康教育();
        string m_ID;
        UpdateType m_UpdateType;
        public frm新建健康教育活动(UpdateType updatetype, string itemid)
        {
            InitializeComponent();
            m_UpdateType = updatetype;
            m_ID = itemid;
            
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
           
            if (txt教育项目.Text == "请选择" || string.IsNullOrWhiteSpace(txt教育项目.Text))
            {
                MessageBox.Show("请选择健康教育项目！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (string.IsNullOrWhiteSpace(txtNumber.Text))
            {
                MessageBox.Show("请填写活动编号！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (string.IsNullOrWhiteSpace(txt活动时间.Text))
            {
                MessageBox.Show("请选择活动时间！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (string.IsNullOrWhiteSpace(txtAddrs.Text))
            {
                MessageBox.Show("请填写活动地点！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (string.IsNullOrWhiteSpace(txt活动形式.Text))
            {
                MessageBox.Show("请填写活动形式！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (string.IsNullOrWhiteSpace(txt主题.Text))
            {
                MessageBox.Show("请填写活动主题！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (string.IsNullOrWhiteSpace(txt组织者.Text))
            {
                MessageBox.Show("请填写组织者！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (string.IsNullOrWhiteSpace(txt主讲.Text))
            {
                MessageBox.Show("请填写主讲人姓名！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (string.IsNullOrWhiteSpace(txt职称.Text))
            {
                MessageBox.Show("请填写职称！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (string.IsNullOrWhiteSpace(txt主讲单位.Text))
            {
                MessageBox.Show("请填写主讲人单位！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            } 
            if (string.IsNullOrWhiteSpace(txt人员类别.Text))
            {
                MessageBox.Show("请填写接受健康教育人员类别！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (string.IsNullOrWhiteSpace(txt人数.Text))
            {
                MessageBox.Show("请填写接受健康教育人数！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (string.IsNullOrWhiteSpace(txt资料类.Text))
            {
                MessageBox.Show("请填写健康资料资料发放种类！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (string.IsNullOrWhiteSpace(txt资料数.Text))
            {
                MessageBox.Show("请填写健康教育资料发放数量！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (string.IsNullOrWhiteSpace(txt内容.Text))
            {
                MessageBox.Show("请填写活动内容！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (string.IsNullOrWhiteSpace(txt评价.Text))
            {
                MessageBox.Show("请填写活动总评价！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (!chk书面材料.Checked && !chk图片材料.Checked && !chk印刷材料.Checked 
                && !chk影音材料.Checked && !chk签到表.Checked && !chk其他材料.Checked)
            {
                MessageBox.Show("存档材料请附后必选一项！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (string.IsNullOrWhiteSpace(txt负责人.Text))
            {
                MessageBox.Show("负责人签字不能为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (string.IsNullOrWhiteSpace(txt填表人.Text))
            {
                MessageBox.Show("填表人签字不能为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (string.IsNullOrWhiteSpace(txt填表时间.Text))
            {
                MessageBox.Show("填表时间不能为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }


            string str = string.Empty;
            if (chk书面材料.Checked)
            {
                str += "书面材料,";
            }
            if (chk图片材料.Checked)
            {
                str += "图片材料,";
            }
            if (chk印刷材料.Checked)
            {
                str += "印刷材料,";
            }
            if (chk影音材料.Checked)
            {
                str += "影音材料,";
            }
            if (chk签到表.Checked)
            {
                str += "签到表,";
            }
            if (chk其他材料.Checked)
            {
                str += "其他材料,";
            }

            if (m_UpdateType == UpdateType.Add)
            {
                bll.GetBusinessByKey("-1", true);
                bll.NewBusiness();
                //保存概要信息
                txt活动时间.EditValue = DateTime.Now.ToString("yyyy-MM-dd");
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.教育项目] = txt教育项目.EditValue.ToString();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.活动编号] = txtNumber.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.活动日期] = txt活动时间.EditValue;
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.活动地点] = txtAddrs.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.活动形式] = txt活动形式.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.主题] = txt主题.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.组织者] = txt组织者.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.主讲人姓名] = txt主讲.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.主讲人职称] = txt职称.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.主讲人单位] = txt主讲单位.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.受教育人类别] = txt人员类别.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.受教育人数] = txt人数.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.资料类型] = txt资料类.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.资料数量] = txt资料数.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.活动内容] = txt内容.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.总结评价] = txt评价.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.是否有效] = true;

                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.存档资料] = str;
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.负责人] = txt负责人.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.创建人] = Loginer.CurrentUser.用户编码;
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.创建机构] = Loginer.CurrentUser.所属机构;
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.创建日期] = bll.ServiceDateTime;
                
                bll.Save(bll.CurrentBusiness);
            }
            else if (m_UpdateType == UpdateType.Modify)
            {
                bll.GetBusinessByKey(m_ID, true);

                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.存档资料] = str;

                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.教育项目] = txt教育项目.EditValue.ToString();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.活动编号] = txtNumber.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.活动日期] = txt活动时间.Text;
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.活动地点] = txtAddrs.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.活动形式] = txt活动形式.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.主题] = txt主题.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.组织者] = txt组织者.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.主讲人姓名] = txt主讲.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.主讲人职称] = txt职称.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.主讲人单位] = txt主讲单位.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.受教育人类别] = txt人员类别.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.受教育人数] = txt人数.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.资料类型] = txt资料类.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.资料数量] = txt资料数.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.活动内容] = txt内容.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.总结评价] = txt评价.Text.Trim();
                //bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.ID] = m_ID;
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.修改人] = Loginer.CurrentUser.用户编码;
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.修改日期] = bll.ServiceDateTime;
                bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.修改机构] = Loginer.CurrentUser.所属机构;

                DataSet dsTemplate = bll.CreateSaveData(bll.CurrentBusiness, m_UpdateType); //创建用于保存的临时数据
                bll.Save(dsTemplate);//调用业务逻辑保存数据方法
            }
            //bll.Save(bll.CurrentBusiness);

            this.DialogResult = System.Windows.Forms.DialogResult.OK;


        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frm新建健康教育活动_Load(object sender, EventArgs e)
        {
            if (m_UpdateType == UpdateType.Add)
            {
                txt填表人.Text = Loginer.CurrentUser.AccountName;
                txt填表时间.Text = bll.ServiceDateTime;
                txt活动时间.DateTime = DateTime.Now;
            }
            else if (m_UpdateType == UpdateType.Modify)
            {
                //lookUp机构.Enabled = false;
                DataSet ds = bll.GetBusinessByKey2(m_ID);

                //txtName.Text = m_ds项目.Tables[0].Rows[0][tb_JTYS服务包.名称].ToString();
                //txtUnit.Text = m_ds项目.Tables[0].Rows[0][tb_JTYS服务包.适宜对象].ToString();
                //numPrice.Value = Convert.ToDecimal(m_ds项目.Tables[0].Rows[0][tb_JTYS服务包.年收费标准]);

                txt教育项目.Text = ds.Tables[0].Rows[0][tb_健康教育.教育项目].ToString();
                txtNumber.Text = ds.Tables[0].Rows[0][tb_健康教育.活动编号].ToString();
                txt活动时间.Text = ds.Tables[0].Rows[0][tb_健康教育.活动日期].ToString();
                txtAddrs.Text = ds.Tables[0].Rows[0][tb_健康教育.活动地点].ToString();
                txt活动形式.Text = ds.Tables[0].Rows[0][tb_健康教育.活动形式].ToString();
                txt主题.Text = ds.Tables[0].Rows[0][tb_健康教育.主题].ToString();
                txt组织者.Text = ds.Tables[0].Rows[0][tb_健康教育.组织者].ToString();
                txt主讲.Text = ds.Tables[0].Rows[0][tb_健康教育.主讲人姓名].ToString();
                txt职称.Text = ds.Tables[0].Rows[0][tb_健康教育.主讲人职称].ToString();
                txt主讲单位.Text = ds.Tables[0].Rows[0][tb_健康教育.主讲人单位].ToString();
                txt人员类别.Text = ds.Tables[0].Rows[0][tb_健康教育.受教育人类别].ToString();
                txt人数.Text = ds.Tables[0].Rows[0][tb_健康教育.受教育人数].ToString();
                txt资料类.Text = ds.Tables[0].Rows[0][tb_健康教育.资料类型].ToString();
                txt资料数.Text = ds.Tables[0].Rows[0][tb_健康教育.资料数量].ToString();
                txt内容.Text = ds.Tables[0].Rows[0][tb_健康教育.活动内容].ToString();
                txt评价.Text = ds.Tables[0].Rows[0][tb_健康教育.总结评价].ToString();
                

                //DataBinder.BindingTextEdit(txt教育项目, bll.CurrentBusiness.Tables[0], tb_健康教育.教育项目);
                //DataBinder.BindingTextEdit(txtNumber, bll.CurrentBusiness.Tables[0], tb_健康教育.活动编号);
                //DataBinder.BindingTextEdit(txt活动时间, bll.CurrentBusiness.Tables[0], tb_健康教育.活动日期);
                //DataBinder.BindingTextEdit(txtAddrs, bll.CurrentBusiness.Tables[0], tb_健康教育.活动地点);
                //DataBinder.BindingTextEdit(txt活动形式, bll.CurrentBusiness.Tables[0], tb_健康教育.活动形式);
                //DataBinder.BindingTextEdit(txt主题, bll.CurrentBusiness.Tables[0], tb_健康教育.主题);
                //DataBinder.BindingTextEdit(txt组织者, bll.CurrentBusiness.Tables[0], tb_健康教育.组织者);
                //DataBinder.BindingTextEdit(txt主讲, bll.CurrentBusiness.Tables[0], tb_健康教育.主讲人姓名);
                //DataBinder.BindingTextEdit(txt职称, bll.CurrentBusiness.Tables[0], tb_健康教育.主讲人职称);
                //DataBinder.BindingTextEdit(txt主讲单位, bll.CurrentBusiness.Tables[0], tb_健康教育.主讲人单位);
                //DataBinder.BindingTextEdit(txt人员类别, bll.CurrentBusiness.Tables[0], tb_健康教育.受教育人类别);
                //DataBinder.BindingTextEdit(txt人数, bll.CurrentBusiness.Tables[0], tb_健康教育.受教育人数);
                //DataBinder.BindingTextEdit(txt资料类, bll.CurrentBusiness.Tables[0], tb_健康教育.资料类型);
                //DataBinder.BindingTextEdit(txt资料数, bll.CurrentBusiness.Tables[0], tb_健康教育.资料数量);
                //DataBinder.BindingTextEdit(txt内容, bll.CurrentBusiness.Tables[0], tb_健康教育.活动内容);
                //DataBinder.BindingTextEdit(txt评价, bll.CurrentBusiness.Tables[0], tb_健康教育.总结评价);

                //string sArray=bll.CurrentBusiness.Tables[0].Rows[0][tb_健康教育.存档资料].ToString();

                string sArray = ds.Tables[0].Rows[0][tb_健康教育.存档资料].ToString();
                if(sArray.Contains("书面材料"))
                {
                    chk书面材料.Checked=true;
                }
                else
                {
                    chk书面材料.Checked = false;
                }
                if(sArray.Contains("图片材料"))
                {
                    chk图片材料.EditValue=true;
                }
                else
                {
                    chk图片材料.EditValue = false;
                }
                if(sArray.Contains("印刷材料"))
                {
                    chk印刷材料.EditValue=true;
                }
                else
                {
                    chk印刷材料.EditValue = false;
                }
                if(sArray.Contains("影音材料"))
                {
                    chk影音材料.EditValue=true;
                }
                else
                {
                    chk影音材料.EditValue = false;
                }
                if(sArray.Contains("签到表"))
                {
                    chk签到表.EditValue=true;
                }
                else
                {
                    chk签到表.EditValue = false;
                }
                if (sArray.Contains("其他材料"))
                {
                    chk其他材料.EditValue=true;
                }
                else
                {
                    chk其他材料.EditValue = false;
                }

                string csss = ds.Tables[0].Rows[0][tb_健康教育.负责人].ToString();

                //txt负责人.Text = ds.Tables[0].Rows[0][tb_健康教育.负责人].ToString();

                txt负责人.Text = csss;

                txt填表人.Text = ds.Tables[0].Rows[0]["创建人姓名"].ToString();
                txt填表时间.DateTime = Convert.ToDateTime(ds.Tables[0].Rows[0][tb_健康教育.创建日期]);
                //DataBinder.BindingTextEdit(txt负责人, bll.CurrentBusiness.Tables[0], tb_健康教育.负责人);
                //DataBinder.BindingTextEdit(txt填表人, bll.CurrentBusiness.Tables[0], tb_健康教育.创建人);
                //DataBinder.BindingTextEdit(txt填表时间, bll.CurrentBusiness.Tables[0], tb_健康教育.创建日期);

                //bll健康教育 bll教育 = new bll健康教育();
                //bll教育.ServiceDateTime

            }
        }
    }
}