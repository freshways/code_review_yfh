﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Interfaces;
using AtomEHR.公共卫生.Module.个人健康;
using DevExpress.XtraEditors.Repository;

namespace AtomEHR.公共卫生.Module
{
    public partial class frm健康体检表_老年人 : AtomEHR.Library.frmBaseDataForm
    {
        //定义此类型，用来判断是点击那个链接跳转进档案grid的
        private string _type = "1";
        public frm健康体检表_老年人()
        {
            InitializeComponent();
        }

        private void frm健康体检表_老年人_Load(object sender, EventArgs e)
        {
            if (Loginer.CurrentUser.所属机构.Length >= 12)
                txt机构.Properties.AutoExpandAllNodes = true;
            else { txt机构.Properties.AutoExpandAllNodes = false; }
            this.InitializeForm();
            string m_ServerDate = _bll.ServiceDateTime;
            try
            {
                this.dte开始1.Text = Convert.ToDateTime(m_ServerDate).ToString("yyyy-MM-01");
                this.dte结束2.Text = Convert.ToDateTime(m_ServerDate).ToString("yyyy-MM-dd");
            }
            catch
            {
                this.dte开始1.Text = DateTime.Now.ToString("yyyy-MM-01");
                this.dte结束2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }

            this.gcSummary.Dock = DockStyle.Fill;
            this.gcSummary.BringToFront();
            this.gc档案.Dock = DockStyle.Fill;

        }

        protected override void InitializeForm()
        {
            //_BLL = new bllMXB高血压管理卡();// 业务逻辑层实例
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gvSummary);
            frmGridCustomize.RegisterGrid(gv档案);
            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                txt机构.Properties.ValueMember = "机构编号";
                txt机构.Properties.DisplayMember = "机构名称";

                txt机构.Properties.TreeList.KeyFieldName = "机构编号";
                txt机构.Properties.TreeList.ParentFieldName = "上级机构";
                txt机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                txt机构.Properties.DataSource = dt所属机构;

                txt机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                txt机构.Text = Loginer.CurrentUser.所属机构;
            }
        }

        bll健康体检 _bll = new bll健康体检();
        private void btnQuery_Click(object sender, EventArgs e)
        {
            string searchBy = this.cbo查询标准.Text.Trim() == "录入时间" ? "1" : "2";
            DataSet ds = _bll.GetReportDataDetail(txt机构.EditValue.ToString(), txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "1", "2", searchBy);
            if (ds != null && ds.Tables.Count > 0)
            {
                gcSummary.DataSource = ds.Tables[0];
                this.gcSummary.BringToFront();
                this.gvSummary.BestFitColumns();
            }
        }

        private void link区域名称_Click(object sender, EventArgs e)
        {
            string searchBy = this.cbo查询标准.Text.Trim() == "录入时间" ? "1" : "2";
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();
            DataSet ds = _bll.GetReportDataDetail(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "1", "2", searchBy);
            if (ds != null && ds.Tables.Count > 0)
            {
                gcSummary.DataSource = ds.Tables[0];
                this.gvSummary.BestFitColumns();
                this.gcSummary.BringToFront();
            }
        }

        private void link总数_Click(object sender, EventArgs e)
        {
            string searchBy = this.cbo查询标准.Text.Trim() == "录入时间" ? "1" : "2";
            _type = "1";
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();
            DataSet ds = _bll.GetReportDataDetail(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "2", "1", searchBy);
            if (ds != null && ds.Tables.Count > 0)
            {
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        private void link未体检_Click(object sender, EventArgs e)
        {
            _type = "2";
            string searchBy = this.cbo查询标准.Text.Trim() == "录入时间" ? "1" : "2";
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();
            DataSet ds = _bll.GetReportDataDetail(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "6", "", searchBy);
            if (ds != null && ds.Tables.Count > 0)
            {
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        private void link已体检_Click(object sender, EventArgs e)
        {
            _type = "1";
            string searchBy = this.cbo查询标准.Text.Trim() == "录入时间" ? "1" : "2";
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();
            DataSet ds = _bll.GetReportDataDetail(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "5", "", searchBy);
            if (ds != null && ds.Tables.Count > 0)
            {
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        private void link健康管理数_Click(object sender, EventArgs e)
        {

        }

        private void link中医体质辨识数_Click(object sender, EventArgs e)
        {

        }

        private void gvSummary_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            DataRowView row = gvSummary.GetRow(e.RowHandle) as DataRowView;
            if (row == null) return;
            if (e.Column == col电子档案数)
            {
                string zongshu = row["电子档案数"].ToString();
                if (!string.IsNullOrEmpty(zongshu) && zongshu != "0")
                {
                    RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
                    link.Click += link总数_Click;
                    e.RepositoryItem = link;
                }
            }

            if (e.Column == col老年人已体检)
            {
                string count = row["老年人已体检"].ToString();
                if (!string.IsNullOrEmpty(count) && count != "0")
                {
                    RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
                    link.Click += link已体检_Click;
                    e.RepositoryItem = link;
                }
            }
            if (e.Column == col老年人未体检)
            {
                string count = row["老年人未体检"].ToString();
                if (!string.IsNullOrEmpty(count) && count != "0")
                {
                    RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
                    link.Click += link未体检_Click;
                    e.RepositoryItem = link;
                }
            }

            if (e.Column == col健康管理数)
            {
                string count = row["健康管理数"].ToString();
                if (!string.IsNullOrEmpty(count) && count != "0")
                {
                    RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
                    link.Click += link健康管理数_Click;
                    e.RepositoryItem = link;
                }
            }

            if (e.Column == col中医体质辨识人数)
            {
                string count = row["中医体质辨识人数"].ToString();
                if (!string.IsNullOrEmpty(count) && count != "0")
                {
                    RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
                    link.Click += link中医体质辨识数_Click;
                    e.RepositoryItem = link;
                }
            }
        }
        

        private int iAll = 1;
        private int iHege = 0;
        private void gvSummary_CustomDrawFooterCell(object sender, DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventArgs e)
        {
            if (e.Column == col电子档案数)
            {
                iAll = 1;
                try
                {
                    int temp = Convert.ToInt32(e.Column.SummaryText.Replace(",", ""));
                    iAll = (temp == 0) ? 1 : temp;
                }
                catch
                { }
            }
            else if (e.Column == col健康管理数)
            {
                iHege = 1;
                try
                {
                    int temp = Convert.ToInt32(e.Column.SummaryText.Replace(",", ""));
                    iHege = temp;
                }
                catch
                { }
            }
            else
            { }
        }

        private void btnEmpty_Click(object sender, EventArgs e)
        {
            this.gcSummary.BringToFront();
        }

    }
}
