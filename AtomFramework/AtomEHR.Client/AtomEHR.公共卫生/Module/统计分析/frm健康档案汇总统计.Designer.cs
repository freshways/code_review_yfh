﻿namespace AtomEHR.公共卫生.Module
{
    partial class frm健康档案汇总统计
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm健康档案汇总统计));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnEmpty = new DevExpress.XtraEditors.SimpleButton();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.cbo查询标准 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dte结束2 = new DevExpress.XtraEditors.DateEdit();
            this.dte开始1 = new DevExpress.XtraEditors.DateEdit();
            this.txt机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.gcSummary = new AtomEHR.Library.UserControls.DataGridControl();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.link区域名称 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridBand33 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand34 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand36 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand30 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand31 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand32 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand35 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand37 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand38 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand39 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand40 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand41 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand16 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand42 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand44 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand18 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand43 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand17 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand19 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand47 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand45 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand46 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand48 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand20 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn13 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand49 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn14 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand50 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn15 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand51 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn16 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand52 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn13 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand53 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn14 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand54 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn15 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand21 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn16 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand22 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn17 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand23 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn18 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand55 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn19 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand56 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn20 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand57 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn21 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand24 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn17 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand25 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn18 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand26 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn19 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand27 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn20 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand68 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand72 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn31 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand70 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn29 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand71 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn22 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand28 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand60 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn30 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand61 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn22 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand29 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand62 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn23 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand63 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn24 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand58 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand64 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn25 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand65 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn26 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand59 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand66 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn27 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand67 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn28 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.link总数 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo查询标准.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte结束2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte结束2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte开始1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte开始1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link区域名称)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link总数)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gcSummary);
            this.tpSummary.Controls.Add(this.groupControl1);
            this.tpSummary.Size = new System.Drawing.Size(1033, 460);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Size = new System.Drawing.Size(1039, 466);
            // 
            // tcBusiness
            // 
            this.tcBusiness.SelectedTabPage = this.tpSummary;
            this.tcBusiness.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            this.tcBusiness.Size = new System.Drawing.Size(1039, 466);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(776, 507);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(1039, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(861, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(664, 2);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.layoutControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1033, 109);
            this.groupControl1.TabIndex = 2;
            this.groupControl1.Text = "查询信息";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnEmpty);
            this.layoutControl1.Controls.Add(this.btnQuery);
            this.layoutControl1.Controls.Add(this.cbo查询标准);
            this.layoutControl1.Controls.Add(this.dte结束2);
            this.layoutControl1.Controls.Add(this.dte开始1);
            this.layoutControl1.Controls.Add(this.txt机构);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 22);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(431, 278, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1029, 85);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnEmpty
            // 
            this.btnEmpty.Image = ((System.Drawing.Image)(resources.GetObject("btnEmpty.Image")));
            this.btnEmpty.Location = new System.Drawing.Point(468, 52);
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.Size = new System.Drawing.Size(60, 22);
            this.btnEmpty.StyleController = this.layoutControl1;
            this.btnEmpty.TabIndex = 10;
            this.btnEmpty.Text = "后退";
            this.btnEmpty.Visible = false;
            // 
            // btnQuery
            // 
            this.btnQuery.Image = ((System.Drawing.Image)(resources.GetObject("btnQuery.Image")));
            this.btnQuery.Location = new System.Drawing.Point(404, 52);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(60, 22);
            this.btnQuery.StyleController = this.layoutControl1;
            this.btnQuery.TabIndex = 6;
            this.btnQuery.Text = "查询";
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // cbo查询标准
            // 
            this.cbo查询标准.EditValue = "录入时间";
            this.cbo查询标准.Location = new System.Drawing.Point(459, 28);
            this.cbo查询标准.Name = "cbo查询标准";
            this.cbo查询标准.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo查询标准.Properties.Items.AddRange(new object[] {
            "录入时间",
            "调查时间"});
            this.cbo查询标准.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo查询标准.Size = new System.Drawing.Size(338, 20);
            this.cbo查询标准.StyleController = this.layoutControl1;
            this.cbo查询标准.TabIndex = 12;
            // 
            // dte结束2
            // 
            this.dte结束2.EditValue = null;
            this.dte结束2.Location = new System.Drawing.Point(268, 28);
            this.dte结束2.Margin = new System.Windows.Forms.Padding(0);
            this.dte结束2.Name = "dte结束2";
            this.dte结束2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte结束2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte结束2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte结束2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte结束2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte结束2.Size = new System.Drawing.Size(112, 20);
            this.dte结束2.StyleController = this.layoutControl1;
            this.dte结束2.TabIndex = 3;
            // 
            // dte开始1
            // 
            this.dte开始1.EditValue = null;
            this.dte开始1.Location = new System.Drawing.Point(79, 28);
            this.dte开始1.Margin = new System.Windows.Forms.Padding(0);
            this.dte开始1.Name = "dte开始1";
            this.dte开始1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte开始1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte开始1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte开始1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte开始1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte开始1.Size = new System.Drawing.Size(168, 20);
            this.dte开始1.StyleController = this.layoutControl1;
            this.dte开始1.TabIndex = 1;
            // 
            // txt机构
            // 
            this.txt机构.Location = new System.Drawing.Point(79, 4);
            this.txt机构.Name = "txt机构";
            this.txt机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt机构.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.txt机构.Size = new System.Drawing.Size(302, 20);
            this.txt机构.StyleController = this.layoutControl1;
            this.txt机构.TabIndex = 35;
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(212, 256);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.emptySpaceItem1,
            this.emptySpaceItem3,
            this.emptySpaceItem2,
            this.emptySpaceItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1029, 85);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txt机构;
            this.layoutControlItem1.CustomizationFormText = "机构:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(381, 24);
            this.layoutControlItem1.Text = "机构:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dte开始1;
            this.layoutControlItem2.CustomizationFormText = "统计周期:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(247, 24);
            this.layoutControlItem2.Text = "统计周期:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.dte结束2;
            this.layoutControlItem3.CustomizationFormText = "至";
            this.layoutControlItem3.Location = new System.Drawing.Point(247, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(133, 24);
            this.layoutControlItem3.Text = "至";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(12, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.cbo查询标准;
            this.layoutControlItem5.CustomizationFormText = "%      查询标准:";
            this.layoutControlItem5.Location = new System.Drawing.Point(380, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(417, 24);
            this.layoutControlItem5.Text = "     查询标准:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnQuery;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(400, 48);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(64, 33);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.btnEmpty;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(464, 48);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(64, 33);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(381, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(644, 24);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(528, 48);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(497, 33);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(797, 24);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(228, 24);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(400, 33);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // gcSummary
            // 
            this.gcSummary.AllowBandedGridColumnSort = false;
            this.gcSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSummary.IsBestFitColumns = true;
            this.gcSummary.Location = new System.Drawing.Point(0, 109);
            this.gcSummary.MainView = this.bandedGridView1;
            this.gcSummary.Name = "gcSummary";
            this.gcSummary.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.link区域名称,
            this.link总数});
            this.gcSummary.ShowContextMenu = false;
            this.gcSummary.Size = new System.Drawing.Size(1033, 351);
            this.gcSummary.StrWhere = "";
            this.gcSummary.TabIndex = 7;
            this.gcSummary.UseCheckBox = false;
            this.gcSummary.View = "";
            this.gcSummary.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1});
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.bandedGridView1.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.bandedGridView1.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.DetailTip.Options.UseFont = true;
            this.bandedGridView1.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.Empty.Options.UseFont = true;
            this.bandedGridView1.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.EvenRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FilterCloseButton.Options.UseFont = true;
            this.bandedGridView1.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FilterPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FixedLine.Options.UseFont = true;
            this.bandedGridView1.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FocusedCell.Options.UseFont = true;
            this.bandedGridView1.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FocusedRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.bandedGridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.GroupButton.Options.UseFont = true;
            this.bandedGridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.bandedGridView1.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.GroupRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.HideSelectionRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.HorzLine.Options.UseFont = true;
            this.bandedGridView1.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.OddRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.Preview.Options.UseFont = true;
            this.bandedGridView1.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.Row.Options.UseFont = true;
            this.bandedGridView1.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.RowSeparator.Options.UseFont = true;
            this.bandedGridView1.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.SelectedRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.TopNewRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.VertLine.Options.UseFont = true;
            this.bandedGridView1.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.ViewCaption.Options.UseFont = true;
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1});
            this.bandedGridView1.ColumnPanelRowHeight = 30;
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn22,
            this.bandedGridColumn1,
            this.bandedGridColumn2,
            this.bandedGridColumn3,
            this.bandedGridColumn4,
            this.bandedGridColumn5,
            this.bandedGridColumn6,
            this.bandedGridColumn7,
            this.bandedGridColumn8,
            this.bandedGridColumn9,
            this.bandedGridColumn10,
            this.bandedGridColumn11,
            this.bandedGridColumn12,
            this.bandedGridColumn13,
            this.bandedGridColumn14,
            this.bandedGridColumn15,
            this.bandedGridColumn16,
            this.bandedGridColumn17,
            this.bandedGridColumn18,
            this.bandedGridColumn19,
            this.bandedGridColumn20,
            this.bandedGridColumn21,
            this.bandedGridColumn22,
            this.bandedGridColumn23,
            this.bandedGridColumn24,
            this.bandedGridColumn25,
            this.bandedGridColumn26,
            this.bandedGridColumn27,
            this.bandedGridColumn28,
            this.bandedGridColumn29,
            this.bandedGridColumn30,
            this.bandedGridColumn31});
            this.bandedGridView1.GridControl = this.gcSummary;
            this.bandedGridView1.GroupPanelText = "DragColumn";
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsBehavior.ReadOnly = true;
            this.bandedGridView1.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.bandedGridView1.OptionsView.EnableAppearanceOddRow = true;
            this.bandedGridView1.OptionsView.ShowColumnHeaders = false;
            this.bandedGridView1.OptionsView.ShowFooter = true;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand1.Caption = "健康档案汇总统计";
            this.gridBand1.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand3,
            this.gridBand4,
            this.gridBand33,
            this.gridBand5,
            this.gridBand7,
            this.gridBand8,
            this.gridBand47,
            this.gridBand9,
            this.gridBand10,
            this.gridBand68,
            this.gridBand11});
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 3297;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand3.AppearanceHeader.Options.UseFont = true;
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand3.Caption = "机构编码";
            this.gridBand3.Columns.Add(this.gridColumn1);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 0;
            this.gridBand3.Width = 108;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "区域编码";
            this.gridColumn1.FieldName = "机构编号";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "区域编码", "合计")});
            this.gridColumn1.Visible = true;
            this.gridColumn1.Width = 108;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand4.AppearanceHeader.Options.UseFont = true;
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand4.Caption = "机构名称";
            this.gridBand4.Columns.Add(this.gridColumn2);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 1;
            this.gridBand4.Width = 180;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "区域名称";
            this.gridColumn2.ColumnEdit = this.link区域名称;
            this.gridColumn2.FieldName = "机构名称";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.Width = 180;
            // 
            // link区域名称
            // 
            this.link区域名称.AutoHeight = false;
            this.link区域名称.Name = "link区域名称";
            // 
            // gridBand33
            // 
            this.gridBand33.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand33.AppearanceHeader.Options.UseFont = true;
            this.gridBand33.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand33.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand33.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand33.Caption = "辖区人口数";
            this.gridBand33.Columns.Add(this.gridColumn3);
            this.gridBand33.Name = "gridBand33";
            this.gridBand33.VisibleIndex = 2;
            this.gridBand33.Width = 58;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "辖区人口数";
            this.gridColumn3.FieldName = "人口数";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "人口数", "{0:n0}")});
            this.gridColumn3.Visible = true;
            this.gridColumn3.Width = 58;
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand5.AppearanceHeader.Options.UseFont = true;
            this.gridBand5.Caption = "三、建立居民健康档案";
            this.gridBand5.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand34,
            this.gridBand35});
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 3;
            this.gridBand5.Width = 360;
            // 
            // gridBand34
            // 
            this.gridBand34.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand34.AppearanceHeader.Options.UseFont = true;
            this.gridBand34.Caption = "1.建立健康档案";
            this.gridBand34.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand36,
            this.gridBand6,
            this.gridBand12});
            this.gridBand34.Name = "gridBand34";
            this.gridBand34.RowCount = 2;
            this.gridBand34.VisibleIndex = 0;
            this.gridBand34.Width = 300;
            // 
            // gridBand36
            // 
            this.gridBand36.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand36.AppearanceHeader.Options.UseFont = true;
            this.gridBand36.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand36.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand36.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand36.Caption = "季末累计新建健康档案数(份)（6）";
            this.gridBand36.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand30,
            this.gridBand31,
            this.gridBand32});
            this.gridBand36.Name = "gridBand36";
            this.gridBand36.RowCount = 2;
            this.gridBand36.VisibleIndex = 0;
            this.gridBand36.Width = 180;
            // 
            // gridBand30
            // 
            this.gridBand30.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand30.AppearanceHeader.Options.UseFont = true;
            this.gridBand30.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand30.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand30.Caption = "合计";
            this.gridBand30.Columns.Add(this.bandedGridColumn1);
            this.gridBand30.Name = "gridBand30";
            this.gridBand30.RowCount = 4;
            this.gridBand30.VisibleIndex = 0;
            this.gridBand30.Width = 60;
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.Caption = "季末累计建档数";
            this.bandedGridColumn1.FieldName = "季末累计建档数";
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            this.bandedGridColumn1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.bandedGridColumn1.Visible = true;
            this.bandedGridColumn1.Width = 60;
            // 
            // gridBand31
            // 
            this.gridBand31.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand31.AppearanceHeader.Options.UseFont = true;
            this.gridBand31.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand31.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand31.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand31.Caption = "本地户籍";
            this.gridBand31.Columns.Add(this.bandedGridColumn2);
            this.gridBand31.Name = "gridBand31";
            this.gridBand31.VisibleIndex = 1;
            this.gridBand31.Width = 60;
            // 
            // bandedGridColumn2
            // 
            this.bandedGridColumn2.Caption = "本地户籍数";
            this.bandedGridColumn2.FieldName = "季末累计建档数";
            this.bandedGridColumn2.Name = "bandedGridColumn2";
            this.bandedGridColumn2.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.bandedGridColumn2.Visible = true;
            this.bandedGridColumn2.Width = 60;
            // 
            // gridBand32
            // 
            this.gridBand32.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand32.AppearanceHeader.Options.UseFont = true;
            this.gridBand32.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand32.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand32.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand32.Caption = "居住6个月以上非本地户籍居民";
            this.gridBand32.Columns.Add(this.bandedGridColumn4);
            this.gridBand32.Name = "gridBand32";
            this.gridBand32.VisibleIndex = 2;
            this.gridBand32.Width = 60;
            // 
            // bandedGridColumn4
            // 
            this.bandedGridColumn4.Caption = "居住6个月以上非本地户籍居民";
            this.bandedGridColumn4.FieldName = "零";
            this.bandedGridColumn4.Name = "bandedGridColumn4";
            this.bandedGridColumn4.Visible = true;
            this.bandedGridColumn4.Width = 60;
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand6.AppearanceHeader.Options.UseFont = true;
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand6.Caption = "累计建立健康档案数(份)（7）";
            this.gridBand6.Columns.Add(this.bandedGridColumn3);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 1;
            this.gridBand6.Width = 60;
            // 
            // bandedGridColumn3
            // 
            this.bandedGridColumn3.Caption = "累计建立健康档案数";
            this.bandedGridColumn3.FieldName = "总管理数";
            this.bandedGridColumn3.Name = "bandedGridColumn3";
            this.bandedGridColumn3.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.bandedGridColumn3.Visible = true;
            this.bandedGridColumn3.Width = 60;
            // 
            // gridBand12
            // 
            this.gridBand12.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand12.AppearanceHeader.Options.UseFont = true;
            this.gridBand12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand12.Caption = "其中建立电子健康档案数(份）（8）";
            this.gridBand12.Columns.Add(this.gridColumn4);
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.VisibleIndex = 2;
            this.gridBand12.Width = 60;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "电子档案数";
            this.gridColumn4.FieldName = "总管理数";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "总管理数", "{0:n0}")});
            this.gridColumn4.Visible = true;
            this.gridColumn4.Width = 60;
            // 
            // gridBand35
            // 
            this.gridBand35.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand35.AppearanceHeader.Options.UseFont = true;
            this.gridBand35.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand35.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand35.Caption = "2.健康档案维护管理";
            this.gridBand35.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand13});
            this.gridBand35.Name = "gridBand35";
            this.gridBand35.RowCount = 2;
            this.gridBand35.VisibleIndex = 1;
            this.gridBand35.Width = 60;
            // 
            // gridBand13
            // 
            this.gridBand13.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand13.AppearanceHeader.Options.UseFont = true;
            this.gridBand13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand13.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand13.Caption = "季末累计有动态记录的健康档案数（份）（9）";
            this.gridBand13.Columns.Add(this.gridColumn5);
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.VisibleIndex = 0;
            this.gridBand13.Width = 60;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "动态档案数";
            this.gridColumn5.FieldName = "动态使用数";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "动态使用数", "{0:n0}")});
            this.gridColumn5.Visible = true;
            this.gridColumn5.Width = 60;
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand7.AppearanceHeader.Options.UseFont = true;
            this.gridBand7.Caption = "五、孕产妇健康管理";
            this.gridBand7.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand37,
            this.gridBand38,
            this.gridBand14,
            this.gridBand15,
            this.gridBand16});
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 4;
            this.gridBand7.Width = 415;
            // 
            // gridBand37
            // 
            this.gridBand37.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand37.AppearanceHeader.Options.UseFont = true;
            this.gridBand37.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand37.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand37.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand37.Caption = "孕产妇数（人）(23)";
            this.gridBand37.Columns.Add(this.bandedGridColumn7);
            this.gridBand37.Name = "gridBand37";
            this.gridBand37.VisibleIndex = 0;
            this.gridBand37.Width = 60;
            // 
            // bandedGridColumn7
            // 
            this.bandedGridColumn7.Caption = "孕产妇规范管理数";
            this.bandedGridColumn7.FieldName = "孕产妇规范管理数";
            this.bandedGridColumn7.Name = "bandedGridColumn7";
            this.bandedGridColumn7.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.bandedGridColumn7.Visible = true;
            this.bandedGridColumn7.Width = 60;
            // 
            // gridBand38
            // 
            this.gridBand38.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand38.AppearanceHeader.Options.UseFont = true;
            this.gridBand38.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand38.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand38.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand38.Caption = "早孕(孕13周之前)建册人数(人)（24）";
            this.gridBand38.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand39,
            this.gridBand40,
            this.gridBand41});
            this.gridBand38.Name = "gridBand38";
            this.gridBand38.RowCount = 2;
            this.gridBand38.VisibleIndex = 1;
            this.gridBand38.Width = 176;
            // 
            // gridBand39
            // 
            this.gridBand39.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand39.AppearanceHeader.Options.UseFont = true;
            this.gridBand39.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand39.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand39.Caption = "合计";
            this.gridBand39.Columns.Add(this.bandedGridColumn5);
            this.gridBand39.Name = "gridBand39";
            this.gridBand39.VisibleIndex = 0;
            this.gridBand39.Width = 58;
            // 
            // bandedGridColumn5
            // 
            this.bandedGridColumn5.Caption = "早孕建册数合计";
            this.bandedGridColumn5.FieldName = "早孕建册数";
            this.bandedGridColumn5.Name = "bandedGridColumn5";
            this.bandedGridColumn5.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.bandedGridColumn5.Visible = true;
            this.bandedGridColumn5.Width = 58;
            // 
            // gridBand40
            // 
            this.gridBand40.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand40.AppearanceHeader.Options.UseFont = true;
            this.gridBand40.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand40.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand40.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand40.Caption = "在基层医疗卫生机构建册人数（人）";
            this.gridBand40.Columns.Add(this.gridColumn6);
            this.gridBand40.Name = "gridBand40";
            this.gridBand40.VisibleIndex = 1;
            this.gridBand40.Width = 61;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "早孕建册数";
            this.gridColumn6.FieldName = "早孕建册数";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "早孕建册数", "{0:n0}")});
            this.gridColumn6.Visible = true;
            this.gridColumn6.Width = 61;
            // 
            // gridBand41
            // 
            this.gridBand41.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand41.AppearanceHeader.Options.UseFont = true;
            this.gridBand41.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand41.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand41.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand41.Caption = "在妇幼机构或综合机构建册人数（人）";
            this.gridBand41.Columns.Add(this.bandedGridColumn6);
            this.gridBand41.Name = "gridBand41";
            this.gridBand41.VisibleIndex = 2;
            this.gridBand41.Width = 57;
            // 
            // bandedGridColumn6
            // 
            this.bandedGridColumn6.Caption = "在妇幼机构或综合机构建册人数";
            this.bandedGridColumn6.FieldName = "零";
            this.bandedGridColumn6.Name = "bandedGridColumn6";
            this.bandedGridColumn6.Visible = true;
            this.bandedGridColumn6.Width = 57;
            // 
            // gridBand14
            // 
            this.gridBand14.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand14.AppearanceHeader.Options.UseFont = true;
            this.gridBand14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand14.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand14.Caption = "产前接受5次及以上产前随访服务孕妇数(人)（25）";
            this.gridBand14.Columns.Add(this.bandedGridColumn8);
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.VisibleIndex = 2;
            this.gridBand14.Width = 59;
            // 
            // bandedGridColumn8
            // 
            this.bandedGridColumn8.Caption = "产前随访数";
            this.bandedGridColumn8.FieldName = "产前随访数";
            this.bandedGridColumn8.Name = "bandedGridColumn8";
            this.bandedGridColumn8.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.bandedGridColumn8.Visible = true;
            this.bandedGridColumn8.Width = 59;
            // 
            // gridBand15
            // 
            this.gridBand15.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand15.AppearanceHeader.Options.UseFont = true;
            this.gridBand15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand15.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand15.Caption = "产妇出院后28天内接受产后访视产妇数(人)（26）";
            this.gridBand15.Columns.Add(this.gridColumn7);
            this.gridBand15.Name = "gridBand15";
            this.gridBand15.VisibleIndex = 3;
            this.gridBand15.Width = 60;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.Caption = "产后访视数";
            this.gridColumn7.FieldName = "产后访视数";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "产后访视数", "{0:n0}")});
            this.gridColumn7.Visible = true;
            this.gridColumn7.Width = 60;
            // 
            // gridBand16
            // 
            this.gridBand16.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand16.AppearanceHeader.Options.UseFont = true;
            this.gridBand16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand16.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand16.Caption = "按规范频次要求管理的孕产妇数（人）（27）";
            this.gridBand16.Columns.Add(this.gridColumn8);
            this.gridBand16.Name = "gridBand16";
            this.gridBand16.VisibleIndex = 4;
            this.gridBand16.Width = 60;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.Caption = "孕产妇规范管理数";
            this.gridColumn8.FieldName = "孕产妇规范管理数";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.gridColumn8.Visible = true;
            this.gridColumn8.Width = 60;
            // 
            // gridBand8
            // 
            this.gridBand8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand8.AppearanceHeader.Options.UseFont = true;
            this.gridBand8.Caption = "六、儿童健康管理";
            this.gridBand8.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand42,
            this.gridBand43});
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = 5;
            this.gridBand8.Width = 240;
            // 
            // gridBand42
            // 
            this.gridBand42.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand42.AppearanceHeader.Options.UseFont = true;
            this.gridBand42.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand42.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand42.Caption = "1、新生儿健康管理";
            this.gridBand42.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand44,
            this.gridBand18});
            this.gridBand42.Name = "gridBand42";
            this.gridBand42.RowCount = 2;
            this.gridBand42.VisibleIndex = 0;
            this.gridBand42.Width = 120;
            // 
            // gridBand44
            // 
            this.gridBand44.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand44.AppearanceHeader.Options.UseFont = true;
            this.gridBand44.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand44.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand44.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand44.Caption = "季末累计活产数(人)（28）";
            this.gridBand44.Columns.Add(this.bandedGridColumn10);
            this.gridBand44.Name = "gridBand44";
            this.gridBand44.VisibleIndex = 0;
            this.gridBand44.Width = 60;
            // 
            // bandedGridColumn10
            // 
            this.bandedGridColumn10.Caption = "季末累计儿童活产数";
            this.bandedGridColumn10.FieldName = "零";
            this.bandedGridColumn10.Name = "bandedGridColumn10";
            this.bandedGridColumn10.Visible = true;
            this.bandedGridColumn10.Width = 60;
            // 
            // gridBand18
            // 
            this.gridBand18.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand18.AppearanceHeader.Options.UseFont = true;
            this.gridBand18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand18.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand18.Caption = "季末累计接受1次及以上访视新生儿数(人)（29）";
            this.gridBand18.Columns.Add(this.gridColumn10);
            this.gridBand18.Name = "gridBand18";
            this.gridBand18.VisibleIndex = 1;
            this.gridBand18.Width = 60;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.Caption = "新生儿访视数";
            this.gridColumn10.FieldName = "新生儿访视数";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.gridColumn10.Visible = true;
            this.gridColumn10.Width = 60;
            // 
            // gridBand43
            // 
            this.gridBand43.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand43.AppearanceHeader.Options.UseFont = true;
            this.gridBand43.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand43.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand43.Caption = "2、0-6岁儿童健康管理";
            this.gridBand43.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand17,
            this.gridBand19});
            this.gridBand43.Name = "gridBand43";
            this.gridBand43.RowCount = 2;
            this.gridBand43.VisibleIndex = 1;
            this.gridBand43.Width = 120;
            // 
            // gridBand17
            // 
            this.gridBand17.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand17.AppearanceHeader.Options.UseFont = true;
            this.gridBand17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand17.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand17.Caption = "辖区0-6岁儿童数(人)（30）";
            this.gridBand17.Columns.Add(this.gridColumn9);
            this.gridBand17.Name = "gridBand17";
            this.gridBand17.VisibleIndex = 0;
            this.gridBand17.Width = 60;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.Caption = "儿童档案数";
            this.gridColumn9.FieldName = "儿童档案数";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.gridColumn9.Visible = true;
            this.gridColumn9.Width = 60;
            // 
            // gridBand19
            // 
            this.gridBand19.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand19.AppearanceHeader.Options.UseFont = true;
            this.gridBand19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand19.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand19.Caption = "辖区接受1次及以上随访的0-6岁儿童数(人)（31）";
            this.gridBand19.Columns.Add(this.gridColumn11);
            this.gridBand19.Name = "gridBand19";
            this.gridBand19.VisibleIndex = 1;
            this.gridBand19.Width = 60;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "1次及以上随访的0-6岁儿童";
            this.gridColumn11.FieldName = "一次及以上随访的儿童";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "一次及以上随访的儿童", "{0:n0}")});
            this.gridColumn11.Visible = true;
            this.gridColumn11.Width = 60;
            // 
            // gridBand47
            // 
            this.gridBand47.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand47.AppearanceHeader.Options.UseFont = true;
            this.gridBand47.Caption = "八、老年人健康管理";
            this.gridBand47.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand45,
            this.gridBand2});
            this.gridBand47.Name = "gridBand47";
            this.gridBand47.VisibleIndex = 6;
            this.gridBand47.Width = 120;
            // 
            // gridBand45
            // 
            this.gridBand45.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand45.AppearanceHeader.Options.UseFont = true;
            this.gridBand45.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand45.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand45.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand45.Caption = "65岁及以上常住居民数(人)（36）";
            this.gridBand45.Columns.Add(this.gridColumn12);
            this.gridBand45.Name = "gridBand45";
            this.gridBand45.VisibleIndex = 0;
            this.gridBand45.Width = 60;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.Caption = "老年人档案数";
            this.gridColumn12.FieldName = "老年人档案数";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "老年人档案数", "{0:n0}")});
            this.gridColumn12.Visible = true;
            this.gridColumn12.Width = 60;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand2.Caption = "季末累计65岁及以上老年人健康管理人数(人)（37）";
            this.gridBand2.Columns.Add(this.bandedGridColumn9);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 1;
            this.gridBand2.Width = 60;
            // 
            // bandedGridColumn9
            // 
            this.bandedGridColumn9.Caption = "季末累计老年人健康管理人数";
            this.bandedGridColumn9.FieldName = "季末累计老年人健康管理人数";
            this.bandedGridColumn9.Name = "bandedGridColumn9";
            this.bandedGridColumn9.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.bandedGridColumn9.Visible = true;
            this.bandedGridColumn9.Width = 60;
            // 
            // gridBand9
            // 
            this.gridBand9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand9.AppearanceHeader.Options.UseFont = true;
            this.gridBand9.Caption = "九、高血压患者健康管理";
            this.gridBand9.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand46,
            this.gridBand48,
            this.gridBand20,
            this.gridBand49,
            this.gridBand50,
            this.gridBand51,
            this.gridBand52,
            this.gridBand53,
            this.gridBand54,
            this.gridBand21,
            this.gridBand22,
            this.gridBand23,
            this.gridBand55});
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.VisibleIndex = 7;
            this.gridBand9.Width = 780;
            // 
            // gridBand46
            // 
            this.gridBand46.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand46.AppearanceHeader.Options.UseFont = true;
            this.gridBand46.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand46.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand46.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand46.Caption = "估算高血压患者总人数(人)（38）";
            this.gridBand46.Columns.Add(this.bandedGridColumn11);
            this.gridBand46.Name = "gridBand46";
            this.gridBand46.VisibleIndex = 0;
            this.gridBand46.Width = 60;
            // 
            // bandedGridColumn11
            // 
            this.bandedGridColumn11.Caption = "估算高血压患者总人数";
            this.bandedGridColumn11.FieldName = "估算高血压患者总人数";
            this.bandedGridColumn11.Name = "bandedGridColumn11";
            this.bandedGridColumn11.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.bandedGridColumn11.Visible = true;
            this.bandedGridColumn11.Width = 60;
            // 
            // gridBand48
            // 
            this.gridBand48.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand48.AppearanceHeader.Options.UseFont = true;
            this.gridBand48.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand48.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand48.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand48.Caption = "季末累计新发现高血压患者数(人)（39）";
            this.gridBand48.Columns.Add(this.bandedGridColumn12);
            this.gridBand48.Name = "gridBand48";
            this.gridBand48.VisibleIndex = 1;
            this.gridBand48.Width = 60;
            // 
            // bandedGridColumn12
            // 
            this.bandedGridColumn12.Caption = "季末累计新发现高血压患者数";
            this.bandedGridColumn12.FieldName = "季末累计新发现高血压患者数";
            this.bandedGridColumn12.Name = "bandedGridColumn12";
            this.bandedGridColumn12.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.bandedGridColumn12.Visible = true;
            this.bandedGridColumn12.Width = 60;
            // 
            // gridBand20
            // 
            this.gridBand20.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand20.AppearanceHeader.Options.UseFont = true;
            this.gridBand20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand20.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand20.Caption = "季末累计管理高血压患者数(人)（40）";
            this.gridBand20.Columns.Add(this.gridColumn13);
            this.gridBand20.Name = "gridBand20";
            this.gridBand20.VisibleIndex = 2;
            this.gridBand20.Width = 60;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.Caption = "已管理的高血压患者人数";
            this.gridColumn13.FieldName = "已管理的高血压患者人数";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "已管理的高血压患者人数", "{0:n0}")});
            this.gridColumn13.Visible = true;
            this.gridColumn13.Width = 60;
            // 
            // gridBand49
            // 
            this.gridBand49.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand49.AppearanceHeader.Options.UseFont = true;
            this.gridBand49.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand49.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand49.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand49.Caption = "规范管理高血压患者数(人)（41）";
            this.gridBand49.Columns.Add(this.gridColumn14);
            this.gridBand49.Name = "gridBand49";
            this.gridBand49.VisibleIndex = 3;
            this.gridBand49.Width = 60;
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.Caption = "高血压规范管理数";
            this.gridColumn14.FieldName = "高血压规范管理数";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "高血压规范管理数", "{0:n0}")});
            this.gridColumn14.Visible = true;
            this.gridColumn14.Width = 60;
            // 
            // gridBand50
            // 
            this.gridBand50.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand50.AppearanceHeader.Options.UseFont = true;
            this.gridBand50.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand50.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand50.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand50.Caption = "最近一次随访血压达标患者数(人)（42）";
            this.gridBand50.Columns.Add(this.gridColumn15);
            this.gridBand50.Name = "gridBand50";
            this.gridBand50.VisibleIndex = 4;
            this.gridBand50.Width = 60;
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.Caption = "最近一次随访血压达标人数";
            this.gridColumn15.FieldName = "最近一次随访血压达标人数";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "最近一次随访血压达标人数", "{0:n0}")});
            this.gridColumn15.Visible = true;
            this.gridColumn15.Width = 60;
            // 
            // gridBand51
            // 
            this.gridBand51.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand51.AppearanceHeader.Options.UseFont = true;
            this.gridBand51.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand51.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand51.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand51.Caption = "最近一次随访高血压患者服药数（43）";
            this.gridBand51.Columns.Add(this.gridColumn16);
            this.gridBand51.Name = "gridBand51";
            this.gridBand51.VisibleIndex = 5;
            this.gridBand51.Width = 60;
            // 
            // gridColumn16
            // 
            this.gridColumn16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn16.Caption = "最近一次随访血压服药数";
            this.gridColumn16.FieldName = "最近一次随访血压服药数";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "最近一次随访血压服药数", "{0:n2}")});
            this.gridColumn16.Visible = true;
            this.gridColumn16.Width = 60;
            // 
            // gridBand52
            // 
            this.gridBand52.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand52.AppearanceHeader.Options.UseFont = true;
            this.gridBand52.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand52.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand52.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand52.Caption = "在管高血压患者家庭医生签约服务人数（人）（44）";
            this.gridBand52.Columns.Add(this.bandedGridColumn13);
            this.gridBand52.Name = "gridBand52";
            this.gridBand52.VisibleIndex = 6;
            this.gridBand52.Width = 60;
            // 
            // bandedGridColumn13
            // 
            this.bandedGridColumn13.Caption = "在管高血压患者家庭医生签约服务人数";
            this.bandedGridColumn13.FieldName = "零";
            this.bandedGridColumn13.Name = "bandedGridColumn13";
            this.bandedGridColumn13.Visible = true;
            this.bandedGridColumn13.Width = 60;
            // 
            // gridBand53
            // 
            this.gridBand53.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand53.AppearanceHeader.Options.UseFont = true;
            this.gridBand53.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand53.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand53.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand53.Caption = "累计开展高血压与食盐摄入量相关因素调查人数(人)（45）";
            this.gridBand53.Columns.Add(this.bandedGridColumn14);
            this.gridBand53.Name = "gridBand53";
            this.gridBand53.VisibleIndex = 7;
            this.gridBand53.Width = 60;
            // 
            // bandedGridColumn14
            // 
            this.bandedGridColumn14.Caption = "累计开展高血压与食盐摄入量相关因素调查人数";
            this.bandedGridColumn14.FieldName = "零";
            this.bandedGridColumn14.Name = "bandedGridColumn14";
            this.bandedGridColumn14.Visible = true;
            this.bandedGridColumn14.Width = 60;
            // 
            // gridBand54
            // 
            this.gridBand54.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand54.AppearanceHeader.Options.UseFont = true;
            this.gridBand54.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand54.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand54.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand54.Caption = "季末累计开展高血压患者低盐膳食干预人数(人)（46）";
            this.gridBand54.Columns.Add(this.bandedGridColumn15);
            this.gridBand54.Name = "gridBand54";
            this.gridBand54.VisibleIndex = 8;
            this.gridBand54.Width = 60;
            // 
            // bandedGridColumn15
            // 
            this.bandedGridColumn15.Caption = "季末累计开展高血压患者低盐膳食干预人数";
            this.bandedGridColumn15.FieldName = "零";
            this.bandedGridColumn15.Name = "bandedGridColumn15";
            this.bandedGridColumn15.Visible = true;
            this.bandedGridColumn15.Width = 60;
            // 
            // gridBand21
            // 
            this.gridBand21.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand21.AppearanceHeader.Options.UseFont = true;
            this.gridBand21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand21.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand21.Caption = "发现高血压高危人群人数（人）（47）";
            this.gridBand21.Columns.Add(this.bandedGridColumn16);
            this.gridBand21.Name = "gridBand21";
            this.gridBand21.VisibleIndex = 9;
            this.gridBand21.Width = 60;
            // 
            // bandedGridColumn16
            // 
            this.bandedGridColumn16.Caption = "发现高血压高危人群人数";
            this.bandedGridColumn16.FieldName = "零";
            this.bandedGridColumn16.Name = "bandedGridColumn16";
            this.bandedGridColumn16.Visible = true;
            this.bandedGridColumn16.Width = 60;
            // 
            // gridBand22
            // 
            this.gridBand22.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand22.AppearanceHeader.Options.UseFont = true;
            this.gridBand22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand22.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand22.Caption = "季末累计开展高血压高危人群低盐膳食干预人数（人）（48）";
            this.gridBand22.Columns.Add(this.bandedGridColumn17);
            this.gridBand22.Name = "gridBand22";
            this.gridBand22.VisibleIndex = 10;
            this.gridBand22.Width = 60;
            // 
            // bandedGridColumn17
            // 
            this.bandedGridColumn17.Caption = "季末累计开展高血压高危人群低盐膳食干预人数";
            this.bandedGridColumn17.FieldName = "零";
            this.bandedGridColumn17.Name = "bandedGridColumn17";
            this.bandedGridColumn17.Visible = true;
            this.bandedGridColumn17.Width = 60;
            // 
            // gridBand23
            // 
            this.gridBand23.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand23.AppearanceHeader.Options.UseFont = true;
            this.gridBand23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand23.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand23.Caption = "辖区内小型餐饮单位数（个）（49）";
            this.gridBand23.Columns.Add(this.bandedGridColumn18);
            this.gridBand23.Name = "gridBand23";
            this.gridBand23.VisibleIndex = 11;
            this.gridBand23.Width = 60;
            // 
            // bandedGridColumn18
            // 
            this.bandedGridColumn18.Caption = "辖区内小型餐饮单位数";
            this.bandedGridColumn18.FieldName = "零";
            this.bandedGridColumn18.Name = "bandedGridColumn18";
            this.bandedGridColumn18.Visible = true;
            this.bandedGridColumn18.Width = 60;
            // 
            // gridBand55
            // 
            this.gridBand55.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand55.AppearanceHeader.Options.UseFont = true;
            this.gridBand55.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand55.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand55.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand55.Caption = "季末累计开展小型餐饮单位减盐指导数(个)（50";
            this.gridBand55.Columns.Add(this.bandedGridColumn19);
            this.gridBand55.Name = "gridBand55";
            this.gridBand55.VisibleIndex = 12;
            this.gridBand55.Width = 60;
            // 
            // bandedGridColumn19
            // 
            this.bandedGridColumn19.Caption = "季末累计开展小型餐饮单位减盐指导数";
            this.bandedGridColumn19.FieldName = "零";
            this.bandedGridColumn19.Name = "bandedGridColumn19";
            this.bandedGridColumn19.Visible = true;
            this.bandedGridColumn19.Width = 60;
            // 
            // gridBand10
            // 
            this.gridBand10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand10.AppearanceHeader.Options.UseFont = true;
            this.gridBand10.Caption = "十、2型糖尿病患者健康管理";
            this.gridBand10.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand56,
            this.gridBand57,
            this.gridBand24,
            this.gridBand25,
            this.gridBand26,
            this.gridBand27});
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.VisibleIndex = 8;
            this.gridBand10.Width = 361;
            // 
            // gridBand56
            // 
            this.gridBand56.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand56.AppearanceHeader.Options.UseFont = true;
            this.gridBand56.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand56.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand56.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand56.Caption = "估算2型糖尿病患者总人数(人)（51）";
            this.gridBand56.Columns.Add(this.bandedGridColumn20);
            this.gridBand56.Name = "gridBand56";
            this.gridBand56.VisibleIndex = 0;
            this.gridBand56.Width = 60;
            // 
            // bandedGridColumn20
            // 
            this.bandedGridColumn20.Caption = "估算2型糖尿病患者总人数";
            this.bandedGridColumn20.FieldName = "估算糖尿病患者总人数";
            this.bandedGridColumn20.Name = "bandedGridColumn20";
            this.bandedGridColumn20.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.bandedGridColumn20.Visible = true;
            this.bandedGridColumn20.Width = 60;
            // 
            // gridBand57
            // 
            this.gridBand57.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand57.AppearanceHeader.Options.UseFont = true;
            this.gridBand57.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand57.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand57.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand57.Caption = "季末累计新发现2型糖尿病患者数(人)（52）";
            this.gridBand57.Columns.Add(this.bandedGridColumn21);
            this.gridBand57.Name = "gridBand57";
            this.gridBand57.VisibleIndex = 1;
            this.gridBand57.Width = 60;
            // 
            // bandedGridColumn21
            // 
            this.bandedGridColumn21.Caption = "季末累计新发现2型糖尿病患者数";
            this.bandedGridColumn21.FieldName = "季末累计新发现糖尿病患者数";
            this.bandedGridColumn21.Name = "bandedGridColumn21";
            this.bandedGridColumn21.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.bandedGridColumn21.Visible = true;
            this.bandedGridColumn21.Width = 60;
            // 
            // gridBand24
            // 
            this.gridBand24.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand24.AppearanceHeader.Options.UseFont = true;
            this.gridBand24.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand24.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand24.Caption = "季末累计管理2型糖尿病患者数(人)（53）";
            this.gridBand24.Columns.Add(this.gridColumn17);
            this.gridBand24.Name = "gridBand24";
            this.gridBand24.VisibleIndex = 2;
            this.gridBand24.Width = 60;
            // 
            // gridColumn17
            // 
            this.gridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.Caption = "已管理的2型糖尿病患者人数";
            this.gridColumn17.FieldName = "已管理的2型糖尿病患者人数";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "已管理的2型糖尿病患者人数", "{0:n2}")});
            this.gridColumn17.Visible = true;
            this.gridColumn17.Width = 60;
            // 
            // gridBand25
            // 
            this.gridBand25.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand25.AppearanceHeader.Options.UseFont = true;
            this.gridBand25.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand25.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand25.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand25.Caption = "规范管理2型糖尿病患者数(人)（54）";
            this.gridBand25.Columns.Add(this.gridColumn18);
            this.gridBand25.Name = "gridBand25";
            this.gridBand25.VisibleIndex = 3;
            this.gridBand25.Width = 60;
            // 
            // gridColumn18
            // 
            this.gridColumn18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn18.Caption = "糖尿病档案规范管理数";
            this.gridColumn18.FieldName = "糖尿病档案规范管理数";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "糖尿病档案规范管理数", "{0:n0}")});
            this.gridColumn18.Visible = true;
            this.gridColumn18.Width = 60;
            // 
            // gridBand26
            // 
            this.gridBand26.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand26.AppearanceHeader.Options.UseFont = true;
            this.gridBand26.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand26.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand26.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand26.Caption = "最近一次随访血糖达标患者数(人)（55）";
            this.gridBand26.Columns.Add(this.gridColumn19);
            this.gridBand26.Name = "gridBand26";
            this.gridBand26.VisibleIndex = 4;
            this.gridBand26.Width = 61;
            // 
            // gridColumn19
            // 
            this.gridColumn19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn19.Caption = "血糖达标人数";
            this.gridColumn19.FieldName = "血糖达标人数";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "血糖达标人数", "{0:n0}")});
            this.gridColumn19.Visible = true;
            this.gridColumn19.Width = 61;
            // 
            // gridBand27
            // 
            this.gridBand27.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand27.AppearanceHeader.Options.UseFont = true;
            this.gridBand27.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand27.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand27.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand27.Caption = "最近一次随访糖尿病患者服药数(人)";
            this.gridBand27.Columns.Add(this.gridColumn20);
            this.gridBand27.Name = "gridBand27";
            this.gridBand27.VisibleIndex = 5;
            this.gridBand27.Width = 60;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "最后一次随访糖尿病服药数";
            this.gridColumn20.FieldName = "最后一次随访糖尿病服药数";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.gridColumn20.Visible = true;
            this.gridColumn20.Width = 60;
            // 
            // gridBand68
            // 
            this.gridBand68.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand68.AppearanceHeader.Options.UseFont = true;
            this.gridBand68.Caption = "十三、中医药健康管理";
            this.gridBand68.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand72,
            this.gridBand70,
            this.gridBand71});
            this.gridBand68.Name = "gridBand68";
            this.gridBand68.VisibleIndex = 9;
            this.gridBand68.Width = 180;
            // 
            // gridBand72
            // 
            this.gridBand72.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand72.AppearanceHeader.Options.UseFont = true;
            this.gridBand72.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand72.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand72.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand72.Caption = "季末累计接受中医药健康服务的65岁及以上居民数（人）（77）";
            this.gridBand72.Columns.Add(this.bandedGridColumn31);
            this.gridBand72.Name = "gridBand72";
            this.gridBand72.VisibleIndex = 0;
            this.gridBand72.Width = 60;
            // 
            // bandedGridColumn31
            // 
            this.bandedGridColumn31.Caption = "老年人中医药管理数";
            this.bandedGridColumn31.FieldName = "老年人中医药管理数";
            this.bandedGridColumn31.Name = "bandedGridColumn31";
            this.bandedGridColumn31.OptionsColumn.ReadOnly = true;
            this.bandedGridColumn31.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.bandedGridColumn31.Visible = true;
            this.bandedGridColumn31.Width = 60;
            // 
            // gridBand70
            // 
            this.gridBand70.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand70.AppearanceHeader.Options.UseFont = true;
            this.gridBand70.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand70.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand70.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand70.Caption = "辖区应管理0～36个月儿童数（78）";
            this.gridBand70.Columns.Add(this.bandedGridColumn29);
            this.gridBand70.Name = "gridBand70";
            this.gridBand70.VisibleIndex = 1;
            this.gridBand70.Width = 60;
            // 
            // bandedGridColumn29
            // 
            this.bandedGridColumn29.Caption = "36个月儿童数";
            this.bandedGridColumn29.FieldName = "儿童档案数三岁";
            this.bandedGridColumn29.Name = "bandedGridColumn29";
            this.bandedGridColumn29.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.bandedGridColumn29.Visible = true;
            this.bandedGridColumn29.Width = 60;
            // 
            // gridBand71
            // 
            this.gridBand71.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand71.AppearanceHeader.Options.UseFont = true;
            this.gridBand71.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand71.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand71.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand71.Caption = "季末累计按照月龄接受中医药健康管理服务的0～36个月儿童数（人）（79）";
            this.gridBand71.Columns.Add(this.gridColumn22);
            this.gridBand71.Name = "gridBand71";
            this.gridBand71.VisibleIndex = 2;
            this.gridBand71.Width = 60;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "儿童中医药管理数";
            this.gridColumn22.FieldName = "儿童中医药管理数";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.gridColumn22.Visible = true;
            this.gridColumn22.Width = 60;
            // 
            // gridBand11
            // 
            this.gridBand11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand11.AppearanceHeader.Options.UseFont = true;
            this.gridBand11.Caption = "十六、省级地方项目";
            this.gridBand11.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand28,
            this.gridBand29,
            this.gridBand58,
            this.gridBand59});
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.VisibleIndex = 10;
            this.gridBand11.Width = 495;
            // 
            // gridBand28
            // 
            this.gridBand28.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand28.AppearanceHeader.Options.UseFont = true;
            this.gridBand28.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand28.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand28.Caption = "1.妇女保健";
            this.gridBand28.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand60,
            this.gridBand61});
            this.gridBand28.Name = "gridBand28";
            this.gridBand28.VisibleIndex = 0;
            this.gridBand28.Width = 135;
            // 
            // gridBand60
            // 
            this.gridBand60.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand60.AppearanceHeader.Options.UseFont = true;
            this.gridBand60.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand60.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand60.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand60.Caption = "常住15-49岁妇女数（人）（100）";
            this.gridBand60.Columns.Add(this.bandedGridColumn30);
            this.gridBand60.Name = "gridBand60";
            this.gridBand60.VisibleIndex = 0;
            this.gridBand60.Width = 75;
            // 
            // bandedGridColumn30
            // 
            this.bandedGridColumn30.Caption = "常住15-49岁妇女数";
            this.bandedGridColumn30.FieldName = "零";
            this.bandedGridColumn30.Name = "bandedGridColumn30";
            this.bandedGridColumn30.Visible = true;
            // 
            // gridBand61
            // 
            this.gridBand61.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand61.AppearanceHeader.Options.UseFont = true;
            this.gridBand61.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand61.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand61.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand61.Caption = "季末累计妇女保健健康指导人数（人）（101）";
            this.gridBand61.Columns.Add(this.bandedGridColumn22);
            this.gridBand61.Name = "gridBand61";
            this.gridBand61.VisibleIndex = 1;
            this.gridBand61.Width = 60;
            // 
            // bandedGridColumn22
            // 
            this.bandedGridColumn22.Caption = "季末累计妇女保健健康指导人数";
            this.bandedGridColumn22.FieldName = "零";
            this.bandedGridColumn22.Name = "bandedGridColumn22";
            this.bandedGridColumn22.Visible = true;
            this.bandedGridColumn22.Width = 60;
            // 
            // gridBand29
            // 
            this.gridBand29.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand29.AppearanceHeader.Options.UseFont = true;
            this.gridBand29.Caption = "2.冠心病患者系统管理";
            this.gridBand29.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand62,
            this.gridBand63});
            this.gridBand29.Name = "gridBand29";
            this.gridBand29.VisibleIndex = 1;
            this.gridBand29.Width = 120;
            // 
            // gridBand62
            // 
            this.gridBand62.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand62.AppearanceHeader.Options.UseFont = true;
            this.gridBand62.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand62.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand62.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand62.Caption = "辖区内冠心病患者数（人）（102）";
            this.gridBand62.Columns.Add(this.bandedGridColumn23);
            this.gridBand62.Name = "gridBand62";
            this.gridBand62.VisibleIndex = 0;
            this.gridBand62.Width = 60;
            // 
            // bandedGridColumn23
            // 
            this.bandedGridColumn23.Caption = "辖区内冠心病患者数";
            this.bandedGridColumn23.FieldName = "估算冠心病患者总人数";
            this.bandedGridColumn23.Name = "bandedGridColumn23";
            this.bandedGridColumn23.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.bandedGridColumn23.Visible = true;
            this.bandedGridColumn23.Width = 60;
            // 
            // gridBand63
            // 
            this.gridBand63.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand63.AppearanceHeader.Options.UseFont = true;
            this.gridBand63.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand63.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand63.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand63.Caption = "季末累计冠心病患者管理人数（人）（103）";
            this.gridBand63.Columns.Add(this.bandedGridColumn24);
            this.gridBand63.Name = "gridBand63";
            this.gridBand63.VisibleIndex = 1;
            this.gridBand63.Width = 60;
            // 
            // bandedGridColumn24
            // 
            this.bandedGridColumn24.Caption = "季末累计冠心病患者管理人数";
            this.bandedGridColumn24.FieldName = "季末累计冠心病患者管理人数";
            this.bandedGridColumn24.Name = "bandedGridColumn24";
            this.bandedGridColumn24.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.bandedGridColumn24.Visible = true;
            this.bandedGridColumn24.Width = 60;
            // 
            // gridBand58
            // 
            this.gridBand58.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand58.AppearanceHeader.Options.UseFont = true;
            this.gridBand58.Caption = "3.脑卒中患者系统管理";
            this.gridBand58.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand64,
            this.gridBand65});
            this.gridBand58.Name = "gridBand58";
            this.gridBand58.VisibleIndex = 2;
            this.gridBand58.Width = 120;
            // 
            // gridBand64
            // 
            this.gridBand64.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand64.AppearanceHeader.Options.UseFont = true;
            this.gridBand64.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand64.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand64.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand64.Caption = "辖区内脑卒中患者数（人）（104）";
            this.gridBand64.Columns.Add(this.bandedGridColumn25);
            this.gridBand64.Name = "gridBand64";
            this.gridBand64.VisibleIndex = 0;
            this.gridBand64.Width = 60;
            // 
            // bandedGridColumn25
            // 
            this.bandedGridColumn25.Caption = "辖区内脑卒中患者数";
            this.bandedGridColumn25.FieldName = "估算脑卒中患者总人数";
            this.bandedGridColumn25.Name = "bandedGridColumn25";
            this.bandedGridColumn25.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.bandedGridColumn25.Visible = true;
            this.bandedGridColumn25.Width = 60;
            // 
            // gridBand65
            // 
            this.gridBand65.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand65.AppearanceHeader.Options.UseFont = true;
            this.gridBand65.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand65.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand65.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand65.Caption = "季末累计脑卒中患者管理人数（人）（105）";
            this.gridBand65.Columns.Add(this.bandedGridColumn26);
            this.gridBand65.Name = "gridBand65";
            this.gridBand65.VisibleIndex = 1;
            this.gridBand65.Width = 60;
            // 
            // bandedGridColumn26
            // 
            this.bandedGridColumn26.Caption = "季末累计脑卒中患者管理人数";
            this.bandedGridColumn26.FieldName = "季末累计脑卒中患者管理人数";
            this.bandedGridColumn26.Name = "bandedGridColumn26";
            this.bandedGridColumn26.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.bandedGridColumn26.Visible = true;
            this.bandedGridColumn26.Width = 60;
            // 
            // gridBand59
            // 
            this.gridBand59.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand59.AppearanceHeader.Options.UseFont = true;
            this.gridBand59.Caption = "4.残疾人系统管理";
            this.gridBand59.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand66,
            this.gridBand67});
            this.gridBand59.Name = "gridBand59";
            this.gridBand59.VisibleIndex = 3;
            this.gridBand59.Width = 120;
            // 
            // gridBand66
            // 
            this.gridBand66.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand66.AppearanceHeader.Options.UseFont = true;
            this.gridBand66.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand66.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand66.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand66.Caption = "辖区内残疾人数（人）（106）";
            this.gridBand66.Columns.Add(this.bandedGridColumn27);
            this.gridBand66.Name = "gridBand66";
            this.gridBand66.VisibleIndex = 0;
            this.gridBand66.Width = 60;
            // 
            // bandedGridColumn27
            // 
            this.bandedGridColumn27.Caption = "辖区内残疾人数";
            this.bandedGridColumn27.FieldName = "估算残疾人总人数";
            this.bandedGridColumn27.Name = "bandedGridColumn27";
            this.bandedGridColumn27.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.bandedGridColumn27.Visible = true;
            this.bandedGridColumn27.Width = 60;
            // 
            // gridBand67
            // 
            this.gridBand67.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand67.AppearanceHeader.Options.UseFont = true;
            this.gridBand67.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand67.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand67.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand67.Caption = "季末累计接受康复指导残疾人数（人）（107）";
            this.gridBand67.Columns.Add(this.bandedGridColumn28);
            this.gridBand67.Name = "gridBand67";
            this.gridBand67.VisibleIndex = 1;
            this.gridBand67.Width = 60;
            // 
            // bandedGridColumn28
            // 
            this.bandedGridColumn28.Caption = "季末累计接受康复指导残疾人数";
            this.bandedGridColumn28.FieldName = "零";
            this.bandedGridColumn28.Name = "bandedGridColumn28";
            this.bandedGridColumn28.Visible = true;
            this.bandedGridColumn28.Width = 60;
            // 
            // link总数
            // 
            this.link总数.AutoHeight = false;
            this.link总数.Name = "link总数";
            // 
            // frm健康档案汇总统计
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1039, 492);
            this.Name = "frm健康档案汇总统计";
            this.Text = "健康档案汇总统计";
            this.Load += new System.EventHandler(this.frm健康档案汇总统计_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbo查询标准.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte结束2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte结束2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte开始1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte开始1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link区域名称)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link总数)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btnEmpty;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraEditors.ComboBoxEdit cbo查询标准;
        private DevExpress.XtraEditors.DateEdit dte结束2;
        private DevExpress.XtraEditors.DateEdit dte开始1;
        private DevExpress.XtraEditors.TreeListLookUpEdit txt机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private Library.UserControls.DataGridControl gcSummary;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link区域名称;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link总数;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn15;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn16;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn14;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn18;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn19;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn20;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn22;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn14;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn15;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn16;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn18;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn19;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn20;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn21;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn22;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn23;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn24;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn25;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn26;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn27;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn28;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn29;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn30;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn31;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand33;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand34;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand36;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand30;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand31;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand32;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand35;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand37;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand38;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand39;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand40;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand41;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand16;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand42;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand44;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand18;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand43;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand17;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand19;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand47;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand45;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand46;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand48;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand20;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand49;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand50;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand51;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand52;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand53;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand54;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand21;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand22;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand23;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand55;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand56;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand57;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand24;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand25;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand26;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand27;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand68;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand72;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand70;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand71;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand28;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand60;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand61;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand29;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand62;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand63;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand58;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand64;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand65;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand59;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand66;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand67;
    }
}