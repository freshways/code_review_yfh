﻿using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using DevExpress.XtraEditors.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


/*
 早孕建册数是对产妇的信息进行统计，以产后访视为基准，判断条件为孕周（4-13周）
 * 此表前期设计的目的是想把孕妇信息一起统计进去的
 * 暂时停用此表
 */

namespace AtomEHR.公共卫生.Module
{
    public partial class frm高危人群达标率统计 : AtomEHR.Library.frmBaseDataForm
    {
        public frm高危人群达标率统计()
        {
            InitializeComponent();
        }


        protected override void InitializeForm()
        {
            //_BLL = new bllMXB高血压管理卡();// 业务逻辑层实例
            //_SummaryView = new DevGridView(gv产妇);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gv高危人群);
            try
            {
                this.txt机构.Properties.AutoExpandAllNodes = false;
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                txt机构.Properties.ValueMember = "机构编号";
                txt机构.Properties.DisplayMember = "机构名称";

                txt机构.Properties.TreeList.KeyFieldName = "机构编号";
                txt机构.Properties.TreeList.ParentFieldName = "上级机构";
                txt机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                txt机构.Properties.DataSource = dt所属机构;

                txt机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                txt机构.Text = Loginer.CurrentUser.所属机构;
            }
        }

        bllMXB高血压高危人群干预调查与随访记录表 _bll = new bllMXB高血压高危人群干预调查与随访记录表();
        private void frm高危人群达标率统计_Load(object sender, EventArgs e)
        {
            if (Loginer.CurrentUser.所属机构.Length >= 12)
                txt机构.Properties.AutoExpandAllNodes = true;
            else { txt机构.Properties.AutoExpandAllNodes = false; }
            this.InitializeForm();
            string m_ServerDate = _bll.ServiceDateTime;
            try
            {
                this.dte开始1.Text = Convert.ToDateTime(m_ServerDate).AddMonths(-4).ToString("yyyy-MM-01");
                this.dte结束2.Text = Convert.ToDateTime(m_ServerDate).ToString("yyyy-MM-dd");
            }
            catch
            {
                this.dte开始1.Text = DateTime.Now.ToString("yyyy-MM-01");
                this.dte结束2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }

            this.gc高危人群.Dock = DockStyle.Fill;
            this.gc高危人群.BringToFront();
            this.gc档案.Dock = DockStyle.Fill;
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;

            string searchBy = this.cbo查询标准.Text.Trim() == "随访日期" ? "1" : "2";

            DataSet ds = _bll.GetReportDataForDBL(txt机构.EditValue.ToString(), dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), searchBy, "1");
            if (ds != null && ds.Tables.Count > 0)
            {
                gc高危人群.DataSource = ds.Tables[0];
                this.gc高危人群.BringToFront();
                this.gv高危人群.BestFitColumns();
            }

            DateTime dt2 = DateTime.Now;
            TimeSpan ts = dt2 - dt;
            double Mill = ts.TotalMilliseconds;
            //MessageBox.Show(Math.Round(Mill / 1000, 3).ToString() + "毫秒");
            //this.Invoke((EventHandler)(delegate { lblPrompt.Text = "查询完成！" + Math.Round(Mill / 1000, 3).ToString() + "毫秒"; }));            
        }
        
        private void gv产妇_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            //DataRowView row = gv产妇.GetRow(e.RowHandle) as DataRowView;
            //if (row == null) return;
            //if (e.Column == gridColumn3)
            //{
            //    string zongshu = row["总数"].ToString();
            //    if (!string.IsNullOrEmpty(zongshu) && zongshu != "0")
            //    {
            //        RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
            //        link.Click += link产后访视数_Click;
            //        e.RepositoryItem = link;
            //    }
            //}

            //if (e.Column == gridColumn4)
            //{
            //    string count = row["达标人数"].ToString();
            //    if (!string.IsNullOrEmpty(count) && count != "0")
            //    {
            //        RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
            //        link.Click += link达标人数_Click;
            //        e.RepositoryItem = link;
            //    }
            //}
        }

        private void link达标人数_Click(object sender, EventArgs e)
        {
            string searchBy = this.cbo查询标准.Text.Trim() == "产后随访日期" ? "1" : "2";

            string jigoubianma = this.gv高危人群.GetFocusedRowCellValue("区域编码").ToString();
            DataSet ds = _bll.GetReportDataForDBL(jigoubianma, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), searchBy, "3");
            if (ds != null && ds.Tables.Count > 0)
            {
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        private void link产后访视数_Click(object sender, EventArgs e)
        {
            string searchBy = this.cbo查询标准.Text.Trim() == "产后随访日期" ? "1" : "2";

            string jigoubianma = this.gv高危人群.GetFocusedRowCellValue("区域编码").ToString();
            DataSet ds = _bll.GetReportDataForDBL(jigoubianma, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), searchBy, "2");
            if (ds != null && ds.Tables.Count > 0)
            {
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        private void link区域名称_Click(object sender, EventArgs e)
        {
            string searchBy = this.cbo查询标准.Text.Trim() == "产后随访日期" ? "1" : "2";

            string jigoubianma = this.gv高危人群.GetFocusedRowCellValue("区域编码").ToString();
            DataSet ds = _bll.GetReportDataForDBL(jigoubianma, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), searchBy, "1");
            if (ds != null && ds.Tables.Count > 0)
            {
                gc高危人群.DataSource = ds.Tables[0];
                this.gc高危人群.BringToFront();
                this.gv高危人群.BestFitColumns();
            }
        }

    }
}
