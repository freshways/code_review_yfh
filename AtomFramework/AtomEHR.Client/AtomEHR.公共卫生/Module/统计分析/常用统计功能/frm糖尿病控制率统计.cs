﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Interfaces;
using AtomEHR.公共卫生.Module.个人健康;
using DevExpress.XtraEditors.Repository;

namespace AtomEHR.公共卫生.Module.统计分析.常用统计功能
{
    public partial class frm糖尿病控制率统计 : AtomEHR.Library.frmBaseDataForm
    {
        DataSet ds = null;
        public frm糖尿病控制率统计()
        {
            InitializeComponent();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            ds = GetData(txt机构.EditValue.ToString(), dateTimePickerBegin.Value.ToString("yyyy-MM-dd"), dateTimePickerEnd.Value.ToString("yyyy-MM-dd"));
            if (ds != null && ds.Tables.Count > 0)
            {
                gcSummary.DataSource = ds.Tables[0];
            }
            else
            {
                gcSummary.DataSource = null;
            }
        }
        private DataSet GetData(string prgid, string begindate, string enddate)
        {
            //DataSet ds = new bllCom().Get管理率By机构ID(txt机构.EditValue.ToString());
            DataSet ds = new bllMXB糖尿病随访表().Get糖尿病控制率(prgid, begindate, enddate);
            return ds;
        }

        private void frm糖尿病控制率统计_Load(object sender, EventArgs e)
        {
            if (Loginer.CurrentUser.所属机构.Length >= 12)
            {
                txt机构.Properties.AutoExpandAllNodes = true;
            }
            else
            {
                txt机构.Properties.AutoExpandAllNodes = false;
            }
            this.InitializeForm();
        }

        protected override void InitializeForm()
        {
            //_BLL = new bllMXB高血压管理卡();// 业务逻辑层实例
            _SummaryView = new DevGridView(gridView1);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gridView1);

            try
            {
                this.txt机构.Properties.AutoExpandAllNodes = false;
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                txt机构.Properties.ValueMember = "机构编号";
                txt机构.Properties.DisplayMember = "机构名称";

                txt机构.Properties.TreeList.KeyFieldName = "机构编号";
                txt机构.Properties.TreeList.ParentFieldName = "上级机构";
                txt机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                txt机构.Properties.DataSource = dt所属机构;

                txt机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                txt机构.Text = Loginer.CurrentUser.所属机构;
            }
        }

        int i总管理数 = 0;
        int i控制数 = 0;

        private void gridView1_CustomDrawFooterCell(object sender, DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventArgs e)
        {
            try
            {
                if (e.Column == col总管理数)
                {
                    i总管理数 = Convert.ToInt32(e.Column.SummaryText.Replace(",", ""));
                }
                else if (e.Column == col总控制数)
                {
                    i控制数 = Convert.ToInt32(e.Column.SummaryText.Replace(",", ""));
                }
                else if(e.Column==col血糖控制率)
                {
                    e.Info.DisplayText = (i控制数 == 0) ? "0" : (i控制数 * 100.0 / i总管理数).ToString("n2") + "%";
                }
            }
            catch
            { }
        }

        private void sbtnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog dig = new SaveFileDialog();
            dig.Filter = "EXCEL 2007 表格|*.xlsx";
            DialogResult result = dig.ShowDialog();
            if (result == DialogResult.OK)
            {
                if (dig.FileName[dig.FileName.Length - 1] == 'x')
                {
                    this.gridView1.ExportToXlsx(dig.FileName);
                }
                else
                {
                    this.gridView1.ExportToXls(dig.FileName);
                }
            }
        }
    }
}
