﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Interfaces;
using AtomEHR.公共卫生.Module.个人健康;
using DevExpress.XtraEditors.Repository;

namespace AtomEHR.公共卫生.Module
{
    public partial class frm管理率统计 : AtomEHR.Library.frmBaseDataForm
    {
        DataSet ds = null;
        private string strWhere = null;
        public frm管理率统计()
        {
            InitializeComponent();
        }

        private void 孕产妇系统管理情况登记簿_Load(object sender, EventArgs e)
        {
            if (Loginer.CurrentUser.所属机构.Length >= 12)
            {
                txt机构.Properties.AutoExpandAllNodes = true;
            }
            else
            {
                txt机构.Properties.AutoExpandAllNodes = false;
            }
            this.InitializeForm();
        }

        protected override void InitializeForm()
        {
            //_BLL = new bllMXB高血压管理卡();// 业务逻辑层实例
            _SummaryView = new DevGridView(gridView1);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gridView1);

            try
            {
                this.txt机构.Properties.AutoExpandAllNodes = false;
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                txt机构.Properties.ValueMember = "机构编号";
                txt机构.Properties.DisplayMember = "机构名称";

                txt机构.Properties.TreeList.KeyFieldName = "机构编号";
                txt机构.Properties.TreeList.ParentFieldName = "上级机构";
                txt机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                txt机构.Properties.DataSource = dt所属机构;

                txt机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                txt机构.Text = Loginer.CurrentUser.所属机构;
            }
        }


        //查询按钮
        private void btnQuery_Click(object sender, EventArgs e)
        {
            ds = GetData(txt机构.EditValue.ToString());
            if (ds != null && ds.Tables.Count > 0)
            {
                gcSummary.DataSource = ds.Tables[0];
            }
            else
            {
                gcSummary.DataSource = null;
            }
        }
        private DataSet GetData(string prgid)
        {
            DataSet ds = new bllCom().Get管理率By机构ID(prgid);
            return ds;
        }
    }
}
