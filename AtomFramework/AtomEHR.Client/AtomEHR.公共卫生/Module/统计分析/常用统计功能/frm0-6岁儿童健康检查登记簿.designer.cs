﻿namespace AtomEHR.公共卫生.Module
{
    partial class frm儿童健康检查登记簿
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm儿童健康检查登记簿));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtDAH = new DevExpress.XtraEditors.TextEdit();
            this.txtSFZH = new DevExpress.XtraEditors.TextEdit();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.btnEmpty = new DevExpress.XtraEditors.SimpleButton();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.dte结束2 = new DevExpress.XtraEditors.DateEdit();
            this.dte开始1 = new DevExpress.XtraEditors.DateEdit();
            this.txt机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gcSummary = new AtomEHR.Library.UserControls.DataGridControl();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.个人档案编号 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col个人档案编号 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col村庄 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col姓名 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col性别 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col出生日期 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand35 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col母亲联系电话 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand36 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col父亲联系电话 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand39 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col母亲姓名 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand40 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col母亲身份证号 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col建册日期 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand17 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col0岁检查一次 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand18 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col0岁检查二次 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand19 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col0岁检查三次 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand20 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col0岁检查四次 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col0岁评价 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand16 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand21 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col纯母乳 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand22 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col部分母乳 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand23 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col混合 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand24 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col人工 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand25 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand27 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col1岁检查一次 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand28 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col1岁检查二次 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand26 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col1岁评价 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand29 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand31 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col2岁检查一次 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand32 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col2岁检查二次 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand30 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col2岁评价 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand33 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col3岁检查一次 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand34 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col3岁评价 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand37 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col4岁检查一次 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand38 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col4岁评价 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand41 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col5岁检查一次 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand42 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col5岁评价 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand45 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col6岁检查一次 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand46 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col6岁评价 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.link区域名称 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gvSummary = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gc档案 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv档案 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.link档案号 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sbtn导出 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDAH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSFZH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte结束2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte结束2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte开始1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte开始1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link区域名称)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link档案号)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gcSummary);
            this.tpSummary.Controls.Add(this.groupControl1);
            this.tpSummary.Controls.Add(this.gc档案);
            this.tpSummary.Size = new System.Drawing.Size(939, 400);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Size = new System.Drawing.Size(945, 406);
            // 
            // tcBusiness
            // 
            this.tcBusiness.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            this.tcBusiness.Size = new System.Drawing.Size(945, 406);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(776, 507);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(945, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(767, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(570, 2);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.layoutControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(939, 103);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "查询信息";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.sbtn导出);
            this.layoutControl1.Controls.Add(this.txtDAH);
            this.layoutControl1.Controls.Add(this.txtSFZH);
            this.layoutControl1.Controls.Add(this.txtName);
            this.layoutControl1.Controls.Add(this.btnEmpty);
            this.layoutControl1.Controls.Add(this.btnQuery);
            this.layoutControl1.Controls.Add(this.dte结束2);
            this.layoutControl1.Controls.Add(this.dte开始1);
            this.layoutControl1.Controls.Add(this.txt机构);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 22);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(431, 278, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(935, 79);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtDAH
            // 
            this.txtDAH.Location = new System.Drawing.Point(413, 28);
            this.txtDAH.Name = "txtDAH";
            this.txtDAH.Size = new System.Drawing.Size(158, 20);
            this.txtDAH.StyleController = this.layoutControl1;
            this.txtDAH.TabIndex = 38;
            // 
            // txtSFZH
            // 
            this.txtSFZH.Location = new System.Drawing.Point(413, 4);
            this.txtSFZH.Name = "txtSFZH";
            this.txtSFZH.Size = new System.Drawing.Size(158, 20);
            this.txtSFZH.StyleController = this.layoutControl1;
            this.txtSFZH.TabIndex = 37;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(638, 4);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(115, 20);
            this.txtName.StyleController = this.layoutControl1;
            this.txtName.TabIndex = 36;
            // 
            // btnEmpty
            // 
            this.btnEmpty.Image = ((System.Drawing.Image)(resources.GetObject("btnEmpty.Image")));
            this.btnEmpty.Location = new System.Drawing.Point(439, 52);
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.Size = new System.Drawing.Size(55, 22);
            this.btnEmpty.StyleController = this.layoutControl1;
            this.btnEmpty.TabIndex = 10;
            this.btnEmpty.Text = "取消";
            this.btnEmpty.Visible = false;
            this.btnEmpty.Click += new System.EventHandler(this.btnEmpty_Click);
            // 
            // btnQuery
            // 
            this.btnQuery.Image = ((System.Drawing.Image)(resources.GetObject("btnQuery.Image")));
            this.btnQuery.Location = new System.Drawing.Point(344, 52);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(55, 22);
            this.btnQuery.StyleController = this.layoutControl1;
            this.btnQuery.TabIndex = 6;
            this.btnQuery.Text = "查询";
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // dte结束2
            // 
            this.dte结束2.EditValue = null;
            this.dte结束2.Location = new System.Drawing.Point(246, 28);
            this.dte结束2.Margin = new System.Windows.Forms.Padding(0);
            this.dte结束2.Name = "dte结束2";
            this.dte结束2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte结束2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte结束2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte结束2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte结束2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte结束2.Size = new System.Drawing.Size(100, 20);
            this.dte结束2.StyleController = this.layoutControl1;
            this.dte结束2.TabIndex = 3;
            // 
            // dte开始1
            // 
            this.dte开始1.EditValue = null;
            this.dte开始1.Location = new System.Drawing.Point(67, 28);
            this.dte开始1.Margin = new System.Windows.Forms.Padding(0);
            this.dte开始1.Name = "dte开始1";
            this.dte开始1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte开始1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte开始1.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dte开始1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dte开始1.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dte开始1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dte开始1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte开始1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte开始1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte开始1.Size = new System.Drawing.Size(158, 20);
            this.dte开始1.StyleController = this.layoutControl1;
            this.dte开始1.TabIndex = 1;
            // 
            // txt机构
            // 
            this.txt机构.Location = new System.Drawing.Point(67, 4);
            this.txt机构.Name = "txt机构";
            this.txt机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt机构.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.txt机构.Size = new System.Drawing.Size(279, 20);
            this.txt机构.StyleController = this.layoutControl1;
            this.txt机构.TabIndex = 35;
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(212, 146);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.emptySpaceItem1,
            this.emptySpaceItem3,
            this.emptySpaceItem2,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.emptySpaceItem6});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(935, 79);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txt机构;
            this.layoutControlItem1.CustomizationFormText = "机构:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(346, 24);
            this.layoutControlItem1.Text = "机构:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(60, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dte开始1;
            this.layoutControlItem2.CustomizationFormText = "统计周期:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(225, 24);
            this.layoutControlItem2.Text = "出生日期:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(60, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.dte结束2;
            this.layoutControlItem3.CustomizationFormText = "至";
            this.layoutControlItem3.Location = new System.Drawing.Point(225, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(121, 24);
            this.layoutControlItem3.Text = "至";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(12, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnQuery;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(340, 48);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(59, 27);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.btnEmpty;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(435, 48);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(59, 27);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(753, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(178, 24);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(622, 48);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(309, 27);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(571, 24);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(360, 24);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(340, 27);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(399, 48);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(36, 27);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtName;
            this.layoutControlItem4.CustomizationFormText = "姓名：";
            this.layoutControlItem4.Location = new System.Drawing.Point(571, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(182, 24);
            this.layoutControlItem4.Text = "姓名：";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(60, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtSFZH;
            this.layoutControlItem5.CustomizationFormText = "身份证号：";
            this.layoutControlItem5.Location = new System.Drawing.Point(346, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(225, 24);
            this.layoutControlItem5.Text = "身份证号：";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(60, 14);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtDAH;
            this.layoutControlItem8.CustomizationFormText = "档案编号：";
            this.layoutControlItem8.Location = new System.Drawing.Point(346, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(225, 24);
            this.layoutControlItem8.Text = "档案编号：";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(60, 14);
            // 
            // gcSummary
            // 
            this.gcSummary.AllowBandedGridColumnSort = false;
            this.gcSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSummary.IsBestFitColumns = true;
            this.gcSummary.Location = new System.Drawing.Point(0, 103);
            this.gcSummary.MainView = this.bandedGridView1;
            this.gcSummary.Name = "gcSummary";
            this.gcSummary.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.link区域名称,
            this.repositoryItemHyperLinkEdit1});
            this.gcSummary.ShowContextMenu = false;
            this.gcSummary.Size = new System.Drawing.Size(939, 297);
            this.gcSummary.StrWhere = "";
            this.gcSummary.TabIndex = 6;
            this.gcSummary.UseCheckBox = false;
            this.gcSummary.View = "";
            this.gcSummary.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1,
            this.gvSummary});
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.bandedGridView1.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.bandedGridView1.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.DetailTip.Options.UseFont = true;
            this.bandedGridView1.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.Empty.Options.UseFont = true;
            this.bandedGridView1.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.EvenRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FilterCloseButton.Options.UseFont = true;
            this.bandedGridView1.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FilterPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FixedLine.Options.UseFont = true;
            this.bandedGridView1.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FocusedCell.Options.UseFont = true;
            this.bandedGridView1.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FocusedRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.bandedGridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.GroupButton.Options.UseFont = true;
            this.bandedGridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.bandedGridView1.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.GroupRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.bandedGridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridView1.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.HideSelectionRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.HorzLine.Options.UseFont = true;
            this.bandedGridView1.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.OddRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.Preview.Options.UseFont = true;
            this.bandedGridView1.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.Row.Options.UseFont = true;
            this.bandedGridView1.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.RowSeparator.Options.UseFont = true;
            this.bandedGridView1.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.SelectedRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.TopNewRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.VertLine.Options.UseFont = true;
            this.bandedGridView1.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.ViewCaption.Options.UseFont = true;
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1});
            this.bandedGridView1.ColumnPanelRowHeight = 30;
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.col个人档案编号,
            this.col村庄,
            this.col姓名,
            this.col性别,
            this.col出生日期,
            this.col建册日期,
            this.col0岁检查一次,
            this.col0岁检查二次,
            this.col0岁检查三次,
            this.col0岁检查四次,
            this.col0岁评价,
            this.col纯母乳,
            this.col部分母乳,
            this.col混合,
            this.col人工,
            this.col1岁检查一次,
            this.col1岁检查二次,
            this.col1岁评价,
            this.col2岁检查一次,
            this.col2岁检查二次,
            this.col2岁评价,
            this.col3岁检查一次,
            this.col3岁评价,
            this.col4岁检查一次,
            this.col4岁评价,
            this.col5岁检查一次,
            this.col5岁评价,
            this.col6岁检查一次,
            this.col6岁评价,
            this.col母亲联系电话,
            this.col父亲联系电话,
            this.col母亲姓名,
            this.col母亲身份证号});
            this.bandedGridView1.GridControl = this.gcSummary;
            this.bandedGridView1.GroupPanelText = "DragColumn";
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsBehavior.ReadOnly = true;
            this.bandedGridView1.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.bandedGridView1.OptionsView.EnableAppearanceOddRow = true;
            this.bandedGridView1.OptionsView.ShowColumnHeaders = false;
            this.bandedGridView1.OptionsView.ShowFooter = true;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 15F);
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "0-6岁 儿 童 健 康 检 查 登 记 簿  （2015年度）";
            this.gridBand1.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.个人档案编号,
            this.gridBand2,
            this.gridBand3,
            this.gridBand4,
            this.gridBand5,
            this.gridBand35,
            this.gridBand36,
            this.gridBand39,
            this.gridBand40,
            this.gridBand6,
            this.gridBand7,
            this.gridBand8,
            this.gridBand9,
            this.gridBand10,
            this.gridBand11,
            this.gridBand12,
            this.gridBand13});
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 3126;
            // 
            // 个人档案编号
            // 
            this.个人档案编号.AppearanceHeader.Options.UseTextOptions = true;
            this.个人档案编号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.个人档案编号.Caption = "个人档案编号";
            this.个人档案编号.Columns.Add(this.col个人档案编号);
            this.个人档案编号.Name = "个人档案编号";
            this.个人档案编号.VisibleIndex = 0;
            this.个人档案编号.Width = 133;
            // 
            // col个人档案编号
            // 
            this.col个人档案编号.Caption = "个人档案编号";
            this.col个人档案编号.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.col个人档案编号.FieldName = "个人档案编号";
            this.col个人档案编号.Name = "col个人档案编号";
            this.col个人档案编号.OptionsColumn.ReadOnly = true;
            this.col个人档案编号.Visible = true;
            this.col个人档案编号.Width = 133;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit1_Click);
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "村庄";
            this.gridBand2.Columns.Add(this.col村庄);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 1;
            this.gridBand2.Width = 75;
            // 
            // col村庄
            // 
            this.col村庄.AppearanceHeader.Options.UseTextOptions = true;
            this.col村庄.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col村庄.Caption = "村庄";
            this.col村庄.FieldName = "村庄";
            this.col村庄.Name = "col村庄";
            this.col村庄.OptionsColumn.AllowEdit = false;
            this.col村庄.Visible = true;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "姓名";
            this.gridBand3.Columns.Add(this.col姓名);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 2;
            this.gridBand3.Width = 93;
            // 
            // col姓名
            // 
            this.col姓名.AppearanceHeader.Options.UseTextOptions = true;
            this.col姓名.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col姓名.Caption = "姓名";
            this.col姓名.FieldName = "姓名";
            this.col姓名.Name = "col姓名";
            this.col姓名.OptionsColumn.AllowEdit = false;
            this.col姓名.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "姓名", "合计：{0}")});
            this.col姓名.Visible = true;
            this.col姓名.Width = 93;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "性别";
            this.gridBand4.Columns.Add(this.col性别);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 3;
            this.gridBand4.Width = 75;
            // 
            // col性别
            // 
            this.col性别.AppearanceHeader.Options.UseTextOptions = true;
            this.col性别.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col性别.Caption = "性别";
            this.col性别.FieldName = "性别";
            this.col性别.Name = "col性别";
            this.col性别.OptionsColumn.AllowEdit = false;
            this.col性别.Visible = true;
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "出生日期";
            this.gridBand5.Columns.Add(this.col出生日期);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 4;
            this.gridBand5.Width = 75;
            // 
            // col出生日期
            // 
            this.col出生日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col出生日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col出生日期.Caption = "出生日期";
            this.col出生日期.FieldName = "出生日期";
            this.col出生日期.Name = "col出生日期";
            this.col出生日期.OptionsColumn.AllowEdit = false;
            this.col出生日期.Visible = true;
            // 
            // gridBand35
            // 
            this.gridBand35.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand35.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand35.Caption = "母亲联系电话";
            this.gridBand35.Columns.Add(this.col母亲联系电话);
            this.gridBand35.Name = "gridBand35";
            this.gridBand35.VisibleIndex = 5;
            this.gridBand35.Width = 96;
            // 
            // col母亲联系电话
            // 
            this.col母亲联系电话.Caption = "母亲联系电话";
            this.col母亲联系电话.FieldName = "母亲联系电话";
            this.col母亲联系电话.Name = "col母亲联系电话";
            this.col母亲联系电话.OptionsColumn.AllowEdit = false;
            this.col母亲联系电话.Visible = true;
            this.col母亲联系电话.Width = 96;
            // 
            // gridBand36
            // 
            this.gridBand36.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand36.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand36.Caption = "父亲联系电话";
            this.gridBand36.Columns.Add(this.col父亲联系电话);
            this.gridBand36.Name = "gridBand36";
            this.gridBand36.VisibleIndex = 6;
            this.gridBand36.Width = 104;
            // 
            // col父亲联系电话
            // 
            this.col父亲联系电话.Caption = "父亲联系电话";
            this.col父亲联系电话.FieldName = "父亲联系电话";
            this.col父亲联系电话.Name = "col父亲联系电话";
            this.col父亲联系电话.OptionsColumn.AllowEdit = false;
            this.col父亲联系电话.Visible = true;
            this.col父亲联系电话.Width = 104;
            // 
            // gridBand39
            // 
            this.gridBand39.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand39.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand39.Caption = "母亲姓名";
            this.gridBand39.Columns.Add(this.col母亲姓名);
            this.gridBand39.Name = "gridBand39";
            this.gridBand39.VisibleIndex = 7;
            this.gridBand39.Width = 75;
            // 
            // col母亲姓名
            // 
            this.col母亲姓名.Caption = "母亲姓名";
            this.col母亲姓名.FieldName = "母亲姓名";
            this.col母亲姓名.Name = "col母亲姓名";
            this.col母亲姓名.OptionsColumn.AllowEdit = false;
            this.col母亲姓名.Visible = true;
            // 
            // gridBand40
            // 
            this.gridBand40.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand40.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand40.Caption = "母亲身份证号";
            this.gridBand40.Columns.Add(this.col母亲身份证号);
            this.gridBand40.Name = "gridBand40";
            this.gridBand40.VisibleIndex = 8;
            this.gridBand40.Width = 137;
            // 
            // col母亲身份证号
            // 
            this.col母亲身份证号.Caption = "母亲身份证号";
            this.col母亲身份证号.FieldName = "母亲身份证号";
            this.col母亲身份证号.Name = "col母亲身份证号";
            this.col母亲身份证号.OptionsColumn.AllowEdit = false;
            this.col母亲身份证号.Visible = true;
            this.col母亲身份证号.Width = 137;
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand6.Caption = "建册日期\n(新生儿访视日期)";
            this.gridBand6.Columns.Add(this.col建册日期);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 9;
            this.gridBand6.Width = 75;
            // 
            // col建册日期
            // 
            this.col建册日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col建册日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col建册日期.Caption = "建册日期";
            this.col建册日期.FieldName = "建册日期";
            this.col建册日期.Name = "col建册日期";
            this.col建册日期.OptionsColumn.AllowEdit = false;
            this.col建册日期.Visible = true;
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand7.Caption = "0岁";
            this.gridBand7.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand14,
            this.gridBand15,
            this.gridBand16});
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 10;
            this.gridBand7.Width = 757;
            // 
            // gridBand14
            // 
            this.gridBand14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand14.Caption = "检查日期";
            this.gridBand14.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand17,
            this.gridBand18,
            this.gridBand19,
            this.gridBand20});
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.VisibleIndex = 0;
            this.gridBand14.Width = 318;
            // 
            // gridBand17
            // 
            this.gridBand17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand17.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand17.Caption = "一次 (满月)";
            this.gridBand17.Columns.Add(this.col0岁检查一次);
            this.gridBand17.Name = "gridBand17";
            this.gridBand17.VisibleIndex = 0;
            this.gridBand17.Width = 75;
            // 
            // col0岁检查一次
            // 
            this.col0岁检查一次.AppearanceHeader.Options.UseTextOptions = true;
            this.col0岁检查一次.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col0岁检查一次.Caption = "一次";
            this.col0岁检查一次.FieldName = "0岁检查一次";
            this.col0岁检查一次.Name = "col0岁检查一次";
            this.col0岁检查一次.OptionsColumn.AllowEdit = false;
            this.col0岁检查一次.Visible = true;
            // 
            // gridBand18
            // 
            this.gridBand18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand18.Caption = "二次 (3月龄)";
            this.gridBand18.Columns.Add(this.col0岁检查二次);
            this.gridBand18.Name = "gridBand18";
            this.gridBand18.VisibleIndex = 1;
            this.gridBand18.Width = 80;
            // 
            // col0岁检查二次
            // 
            this.col0岁检查二次.AppearanceHeader.Options.UseTextOptions = true;
            this.col0岁检查二次.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col0岁检查二次.Caption = "二次";
            this.col0岁检查二次.FieldName = "0岁检查二次";
            this.col0岁检查二次.Name = "col0岁检查二次";
            this.col0岁检查二次.OptionsColumn.AllowEdit = false;
            this.col0岁检查二次.Visible = true;
            this.col0岁检查二次.Width = 80;
            // 
            // gridBand19
            // 
            this.gridBand19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand19.Caption = "三次 (6月龄)";
            this.gridBand19.Columns.Add(this.col0岁检查三次);
            this.gridBand19.Name = "gridBand19";
            this.gridBand19.VisibleIndex = 2;
            this.gridBand19.Width = 82;
            // 
            // col0岁检查三次
            // 
            this.col0岁检查三次.Caption = "三次";
            this.col0岁检查三次.FieldName = "0岁检查三次";
            this.col0岁检查三次.Name = "col0岁检查三次";
            this.col0岁检查三次.OptionsColumn.AllowEdit = false;
            this.col0岁检查三次.Visible = true;
            this.col0岁检查三次.Width = 82;
            // 
            // gridBand20
            // 
            this.gridBand20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand20.Caption = "四次 (8月龄)";
            this.gridBand20.Columns.Add(this.col0岁检查四次);
            this.gridBand20.Name = "gridBand20";
            this.gridBand20.VisibleIndex = 3;
            this.gridBand20.Width = 81;
            // 
            // col0岁检查四次
            // 
            this.col0岁检查四次.Caption = "四次";
            this.col0岁检查四次.FieldName = "0岁检查四次";
            this.col0岁检查四次.Name = "col0岁检查四次";
            this.col0岁检查四次.OptionsColumn.AllowEdit = false;
            this.col0岁检查四次.Visible = true;
            this.col0岁检查四次.Width = 81;
            // 
            // gridBand15
            // 
            this.gridBand15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand15.Caption = "评价（以体重为标注）";
            this.gridBand15.Columns.Add(this.col0岁评价);
            this.gridBand15.Name = "gridBand15";
            this.gridBand15.VisibleIndex = 1;
            this.gridBand15.Width = 139;
            // 
            // col0岁评价
            // 
            this.col0岁评价.Caption = "评价";
            this.col0岁评价.FieldName = "0岁检查评价";
            this.col0岁评价.Name = "col0岁评价";
            this.col0岁评价.OptionsColumn.AllowEdit = false;
            this.col0岁评价.Visible = true;
            this.col0岁评价.Width = 139;
            // 
            // gridBand16
            // 
            this.gridBand16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand16.Caption = "4个月母乳喂养情况";
            this.gridBand16.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand21,
            this.gridBand22,
            this.gridBand23,
            this.gridBand24});
            this.gridBand16.Name = "gridBand16";
            this.gridBand16.VisibleIndex = 2;
            this.gridBand16.Width = 300;
            // 
            // gridBand21
            // 
            this.gridBand21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand21.Caption = "纯母乳";
            this.gridBand21.Columns.Add(this.col纯母乳);
            this.gridBand21.Name = "gridBand21";
            this.gridBand21.VisibleIndex = 0;
            this.gridBand21.Width = 75;
            // 
            // col纯母乳
            // 
            this.col纯母乳.Caption = "纯母乳";
            this.col纯母乳.FieldName = "纯母乳";
            this.col纯母乳.Name = "col纯母乳";
            this.col纯母乳.OptionsColumn.AllowEdit = false;
            this.col纯母乳.Visible = true;
            // 
            // gridBand22
            // 
            this.gridBand22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand22.Caption = "部分母乳";
            this.gridBand22.Columns.Add(this.col部分母乳);
            this.gridBand22.Name = "gridBand22";
            this.gridBand22.VisibleIndex = 1;
            this.gridBand22.Width = 75;
            // 
            // col部分母乳
            // 
            this.col部分母乳.Caption = "部分母乳";
            this.col部分母乳.FieldName = "部分母乳";
            this.col部分母乳.Name = "col部分母乳";
            this.col部分母乳.OptionsColumn.AllowEdit = false;
            this.col部分母乳.Visible = true;
            // 
            // gridBand23
            // 
            this.gridBand23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand23.Caption = "混合";
            this.gridBand23.Columns.Add(this.col混合);
            this.gridBand23.Name = "gridBand23";
            this.gridBand23.VisibleIndex = 2;
            this.gridBand23.Width = 75;
            // 
            // col混合
            // 
            this.col混合.Caption = "混合";
            this.col混合.FieldName = "混合";
            this.col混合.Name = "col混合";
            this.col混合.OptionsColumn.AllowEdit = false;
            this.col混合.Visible = true;
            // 
            // gridBand24
            // 
            this.gridBand24.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand24.Caption = "人工";
            this.gridBand24.Columns.Add(this.col人工);
            this.gridBand24.Name = "gridBand24";
            this.gridBand24.VisibleIndex = 3;
            this.gridBand24.Width = 75;
            // 
            // col人工
            // 
            this.col人工.Caption = "人工";
            this.col人工.FieldName = "人工";
            this.col人工.Name = "col人工";
            this.col人工.OptionsColumn.AllowEdit = false;
            this.col人工.Visible = true;
            // 
            // gridBand8
            // 
            this.gridBand8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand8.Caption = "1岁";
            this.gridBand8.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand25,
            this.gridBand26});
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = 11;
            this.gridBand8.Width = 298;
            // 
            // gridBand25
            // 
            this.gridBand25.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand25.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand25.Caption = "检查日期";
            this.gridBand25.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand27,
            this.gridBand28});
            this.gridBand25.Name = "gridBand25";
            this.gridBand25.VisibleIndex = 0;
            this.gridBand25.Width = 171;
            // 
            // gridBand27
            // 
            this.gridBand27.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand27.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand27.Caption = "一次 (12月龄)";
            this.gridBand27.Columns.Add(this.col1岁检查一次);
            this.gridBand27.Name = "gridBand27";
            this.gridBand27.VisibleIndex = 0;
            this.gridBand27.Width = 85;
            // 
            // col1岁检查一次
            // 
            this.col1岁检查一次.Caption = "一次";
            this.col1岁检查一次.FieldName = "1岁检查一次";
            this.col1岁检查一次.Name = "col1岁检查一次";
            this.col1岁检查一次.OptionsColumn.AllowEdit = false;
            this.col1岁检查一次.Visible = true;
            this.col1岁检查一次.Width = 85;
            // 
            // gridBand28
            // 
            this.gridBand28.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand28.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand28.Caption = "二次 (18月龄)";
            this.gridBand28.Columns.Add(this.col1岁检查二次);
            this.gridBand28.Name = "gridBand28";
            this.gridBand28.VisibleIndex = 1;
            this.gridBand28.Width = 86;
            // 
            // col1岁检查二次
            // 
            this.col1岁检查二次.Caption = "二次";
            this.col1岁检查二次.FieldName = "1岁检查二次";
            this.col1岁检查二次.Name = "col1岁检查二次";
            this.col1岁检查二次.OptionsColumn.AllowEdit = false;
            this.col1岁检查二次.Visible = true;
            this.col1岁检查二次.Width = 86;
            // 
            // gridBand26
            // 
            this.gridBand26.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand26.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand26.Caption = "评价（以体重为标注）";
            this.gridBand26.Columns.Add(this.col1岁评价);
            this.gridBand26.Name = "gridBand26";
            this.gridBand26.VisibleIndex = 1;
            this.gridBand26.Width = 127;
            // 
            // col1岁评价
            // 
            this.col1岁评价.Caption = "评价";
            this.col1岁评价.FieldName = "1岁检查评价";
            this.col1岁评价.Name = "col1岁评价";
            this.col1岁评价.OptionsColumn.AllowEdit = false;
            this.col1岁评价.Visible = true;
            this.col1岁评价.Width = 127;
            // 
            // gridBand9
            // 
            this.gridBand9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand9.Caption = "2岁";
            this.gridBand9.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand29,
            this.gridBand30});
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.VisibleIndex = 12;
            this.gridBand9.Width = 296;
            // 
            // gridBand29
            // 
            this.gridBand29.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand29.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand29.Caption = "检查日期";
            this.gridBand29.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand31,
            this.gridBand32});
            this.gridBand29.Name = "gridBand29";
            this.gridBand29.VisibleIndex = 0;
            this.gridBand29.Width = 165;
            // 
            // gridBand31
            // 
            this.gridBand31.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand31.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand31.Caption = "一次(24月龄)";
            this.gridBand31.Columns.Add(this.col2岁检查一次);
            this.gridBand31.Name = "gridBand31";
            this.gridBand31.VisibleIndex = 0;
            this.gridBand31.Width = 84;
            // 
            // col2岁检查一次
            // 
            this.col2岁检查一次.Caption = "一次";
            this.col2岁检查一次.FieldName = "2岁检查一次";
            this.col2岁检查一次.Name = "col2岁检查一次";
            this.col2岁检查一次.OptionsColumn.AllowEdit = false;
            this.col2岁检查一次.Visible = true;
            this.col2岁检查一次.Width = 84;
            // 
            // gridBand32
            // 
            this.gridBand32.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand32.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand32.Caption = "二次(30月龄)";
            this.gridBand32.Columns.Add(this.col2岁检查二次);
            this.gridBand32.Name = "gridBand32";
            this.gridBand32.VisibleIndex = 1;
            this.gridBand32.Width = 81;
            // 
            // col2岁检查二次
            // 
            this.col2岁检查二次.Caption = "二次";
            this.col2岁检查二次.FieldName = "2岁检查二次";
            this.col2岁检查二次.Name = "col2岁检查二次";
            this.col2岁检查二次.OptionsColumn.AllowEdit = false;
            this.col2岁检查二次.Visible = true;
            this.col2岁检查二次.Width = 81;
            // 
            // gridBand30
            // 
            this.gridBand30.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand30.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand30.Caption = "评价（以体重为标注）";
            this.gridBand30.Columns.Add(this.col2岁评价);
            this.gridBand30.Name = "gridBand30";
            this.gridBand30.VisibleIndex = 1;
            this.gridBand30.Width = 131;
            // 
            // col2岁评价
            // 
            this.col2岁评价.Caption = "评价";
            this.col2岁评价.FieldName = "2岁检查评价";
            this.col2岁评价.Name = "col2岁评价";
            this.col2岁评价.OptionsColumn.AllowEdit = false;
            this.col2岁评价.Visible = true;
            this.col2岁评价.Width = 131;
            // 
            // gridBand10
            // 
            this.gridBand10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand10.Caption = "3岁";
            this.gridBand10.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand33,
            this.gridBand34});
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.VisibleIndex = 13;
            this.gridBand10.Width = 205;
            // 
            // gridBand33
            // 
            this.gridBand33.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand33.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand33.Caption = "检查日期";
            this.gridBand33.Columns.Add(this.col3岁检查一次);
            this.gridBand33.Name = "gridBand33";
            this.gridBand33.VisibleIndex = 0;
            this.gridBand33.Width = 75;
            // 
            // col3岁检查一次
            // 
            this.col3岁检查一次.Caption = "一次";
            this.col3岁检查一次.FieldName = "3岁检查一次";
            this.col3岁检查一次.Name = "col3岁检查一次";
            this.col3岁检查一次.OptionsColumn.AllowEdit = false;
            this.col3岁检查一次.Visible = true;
            // 
            // gridBand34
            // 
            this.gridBand34.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand34.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand34.Caption = "评价（以体重为标注）";
            this.gridBand34.Columns.Add(this.col3岁评价);
            this.gridBand34.Name = "gridBand34";
            this.gridBand34.VisibleIndex = 1;
            this.gridBand34.Width = 130;
            // 
            // col3岁评价
            // 
            this.col3岁评价.Caption = "评价";
            this.col3岁评价.FieldName = "3岁检查评价";
            this.col3岁评价.Name = "col3岁评价";
            this.col3岁评价.OptionsColumn.AllowEdit = false;
            this.col3岁评价.Visible = true;
            this.col3岁评价.Width = 130;
            // 
            // gridBand11
            // 
            this.gridBand11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand11.Caption = "4岁";
            this.gridBand11.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand37,
            this.gridBand38});
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.VisibleIndex = 14;
            this.gridBand11.Width = 206;
            // 
            // gridBand37
            // 
            this.gridBand37.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand37.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand37.Caption = "检查日期";
            this.gridBand37.Columns.Add(this.col4岁检查一次);
            this.gridBand37.Name = "gridBand37";
            this.gridBand37.VisibleIndex = 0;
            this.gridBand37.Width = 75;
            // 
            // col4岁检查一次
            // 
            this.col4岁检查一次.Caption = "一次";
            this.col4岁检查一次.FieldName = "4岁检查一次";
            this.col4岁检查一次.Name = "col4岁检查一次";
            this.col4岁检查一次.OptionsColumn.AllowEdit = false;
            this.col4岁检查一次.Visible = true;
            // 
            // gridBand38
            // 
            this.gridBand38.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand38.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand38.Caption = "评价（以体重为标注）";
            this.gridBand38.Columns.Add(this.col4岁评价);
            this.gridBand38.Name = "gridBand38";
            this.gridBand38.VisibleIndex = 1;
            this.gridBand38.Width = 131;
            // 
            // col4岁评价
            // 
            this.col4岁评价.Caption = "评价";
            this.col4岁评价.FieldName = "4岁检查评价";
            this.col4岁评价.Name = "col4岁评价";
            this.col4岁评价.OptionsColumn.AllowEdit = false;
            this.col4岁评价.Visible = true;
            this.col4岁评价.Width = 131;
            // 
            // gridBand12
            // 
            this.gridBand12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand12.Caption = "5岁";
            this.gridBand12.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand41,
            this.gridBand42});
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.VisibleIndex = 15;
            this.gridBand12.Width = 212;
            // 
            // gridBand41
            // 
            this.gridBand41.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand41.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand41.Caption = "检查日期";
            this.gridBand41.Columns.Add(this.col5岁检查一次);
            this.gridBand41.Name = "gridBand41";
            this.gridBand41.VisibleIndex = 0;
            this.gridBand41.Width = 75;
            // 
            // col5岁检查一次
            // 
            this.col5岁检查一次.Caption = "一次";
            this.col5岁检查一次.FieldName = "5岁检查一次";
            this.col5岁检查一次.Name = "col5岁检查一次";
            this.col5岁检查一次.OptionsColumn.AllowEdit = false;
            this.col5岁检查一次.Visible = true;
            // 
            // gridBand42
            // 
            this.gridBand42.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand42.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand42.Caption = "评价（以体重为标注）";
            this.gridBand42.Columns.Add(this.col5岁评价);
            this.gridBand42.Name = "gridBand42";
            this.gridBand42.VisibleIndex = 1;
            this.gridBand42.Width = 137;
            // 
            // col5岁评价
            // 
            this.col5岁评价.Caption = "评价";
            this.col5岁评价.FieldName = "5岁检查评价";
            this.col5岁评价.Name = "col5岁评价";
            this.col5岁评价.OptionsColumn.AllowEdit = false;
            this.col5岁评价.Visible = true;
            this.col5岁评价.Width = 137;
            // 
            // gridBand13
            // 
            this.gridBand13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand13.Caption = "6岁";
            this.gridBand13.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand45,
            this.gridBand46});
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.VisibleIndex = 16;
            this.gridBand13.Width = 214;
            // 
            // gridBand45
            // 
            this.gridBand45.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand45.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand45.Caption = "检查日期";
            this.gridBand45.Columns.Add(this.col6岁检查一次);
            this.gridBand45.Name = "gridBand45";
            this.gridBand45.VisibleIndex = 0;
            this.gridBand45.Width = 75;
            // 
            // col6岁检查一次
            // 
            this.col6岁检查一次.Caption = "一次";
            this.col6岁检查一次.FieldName = "6岁检查一次";
            this.col6岁检查一次.Name = "col6岁检查一次";
            this.col6岁检查一次.OptionsColumn.AllowEdit = false;
            this.col6岁检查一次.Visible = true;
            // 
            // gridBand46
            // 
            this.gridBand46.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand46.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand46.Caption = "评价（以体重为标注）";
            this.gridBand46.Columns.Add(this.col6岁评价);
            this.gridBand46.Name = "gridBand46";
            this.gridBand46.VisibleIndex = 1;
            this.gridBand46.Width = 139;
            // 
            // col6岁评价
            // 
            this.col6岁评价.Caption = "评价";
            this.col6岁评价.FieldName = "6岁检查评价";
            this.col6岁评价.Name = "col6岁评价";
            this.col6岁评价.OptionsColumn.AllowEdit = false;
            this.col6岁评价.Visible = true;
            this.col6岁评价.Width = 139;
            // 
            // link区域名称
            // 
            this.link区域名称.AutoHeight = false;
            this.link区域名称.Name = "link区域名称";
            this.link区域名称.Click += new System.EventHandler(this.link区域名称_Click);
            // 
            // gvSummary
            // 
            this.gvSummary.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gvSummary.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gvSummary.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gvSummary.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.DetailTip.Options.UseFont = true;
            this.gvSummary.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Empty.Options.UseFont = true;
            this.gvSummary.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.EvenRow.Options.UseFont = true;
            this.gvSummary.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gvSummary.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FilterPanel.Options.UseFont = true;
            this.gvSummary.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FixedLine.Options.UseFont = true;
            this.gvSummary.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FocusedCell.Options.UseFont = true;
            this.gvSummary.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FocusedRow.Options.UseFont = true;
            this.gvSummary.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gvSummary.Appearance.FooterPanel.Options.UseFont = true;
            this.gvSummary.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupButton.Options.UseFont = true;
            this.gvSummary.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupFooter.Options.UseFont = true;
            this.gvSummary.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupPanel.Options.UseFont = true;
            this.gvSummary.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupRow.Options.UseFont = true;
            this.gvSummary.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvSummary.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gvSummary.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HorzLine.Options.UseFont = true;
            this.gvSummary.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.OddRow.Options.UseFont = true;
            this.gvSummary.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Preview.Options.UseFont = true;
            this.gvSummary.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Row.Options.UseFont = true;
            this.gvSummary.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.RowSeparator.Options.UseFont = true;
            this.gvSummary.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.SelectedRow.Options.UseFont = true;
            this.gvSummary.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.TopNewRow.Options.UseFont = true;
            this.gvSummary.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.VertLine.Options.UseFont = true;
            this.gvSummary.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ViewCaption.Options.UseFont = true;
            this.gvSummary.ColumnPanelRowHeight = 30;
            this.gvSummary.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn19});
            this.gvSummary.GridControl = this.gcSummary;
            this.gvSummary.GroupPanelText = "DragColumn";
            this.gvSummary.Name = "gvSummary";
            this.gvSummary.OptionsView.ColumnAutoWidth = false;
            this.gvSummary.OptionsView.EnableAppearanceEvenRow = true;
            this.gvSummary.OptionsView.EnableAppearanceOddRow = true;
            this.gvSummary.OptionsView.ShowFooter = true;
            this.gvSummary.OptionsView.ShowGroupPanel = false;
            this.gvSummary.CustomDrawFooterCell += new DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventHandler(this.gvSummary_CustomDrawFooterCell);
            this.gvSummary.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gvSummary_CustomRowCellEdit);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "区域编码";
            this.gridColumn1.FieldName = "区域编码";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "区域编码", "合计")});
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "区域名称";
            this.gridColumn2.ColumnEdit = this.link区域名称;
            this.gridColumn2.FieldName = "区域名称";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "总数";
            this.gridColumn6.FieldName = "总数";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "总数", "{0:n0}")});
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 2;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.Caption = "合格数";
            this.gridColumn7.FieldName = "合格数";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "合格数", "{0:n0}")});
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 3;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.Caption = "不合格数";
            this.gridColumn8.FieldName = "不合格数";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "不合格数", "{0:n0}")});
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 4;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.Caption = "合格率";
            this.gridColumn9.DisplayFormat.FormatString = "{0:n2}%";
            this.gridColumn9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn9.FieldName = "合格率";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Average, "合格率", "{0:n2}%")});
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 5;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.Caption = "平均完整度";
            this.gridColumn10.DisplayFormat.FormatString = "{0:n2}%";
            this.gridColumn10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn10.FieldName = "平均完整度";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Average, "平均完整度", "{0:n2}%")});
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 6;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "转入档案数";
            this.gridColumn11.FieldName = "转入档案数";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "转入档案数", "{0:n0}")});
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 7;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.Caption = "转出档案数";
            this.gridColumn12.FieldName = "转出档案数";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "转出档案数", "{0:n0}")});
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 8;
            // 
            // gridColumn19
            // 
            this.gridColumn19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn19.Caption = "非活动档案数";
            this.gridColumn19.FieldName = "非活动档案数";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "非活动档案数", "{0:n0}")});
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 9;
            // 
            // gc档案
            // 
            this.gc档案.AllowBandedGridColumnSort = false;
            this.gc档案.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc档案.IsBestFitColumns = true;
            this.gc档案.Location = new System.Drawing.Point(0, 0);
            this.gc档案.MainView = this.gv档案;
            this.gc档案.Name = "gc档案";
            this.gc档案.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.link档案号});
            this.gc档案.ShowContextMenu = false;
            this.gc档案.Size = new System.Drawing.Size(939, 400);
            this.gc档案.StrWhere = "";
            this.gc档案.TabIndex = 7;
            this.gc档案.UseCheckBox = false;
            this.gc档案.View = "";
            this.gc档案.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv档案});
            // 
            // gv档案
            // 
            this.gv档案.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gv档案.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gv档案.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gv档案.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.DetailTip.Options.UseFont = true;
            this.gv档案.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.Empty.Options.UseFont = true;
            this.gv档案.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.EvenRow.Options.UseFont = true;
            this.gv档案.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gv档案.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.FilterPanel.Options.UseFont = true;
            this.gv档案.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.FixedLine.Options.UseFont = true;
            this.gv档案.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.FocusedCell.Options.UseFont = true;
            this.gv档案.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.FocusedRow.Options.UseFont = true;
            this.gv档案.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gv档案.Appearance.FooterPanel.Options.UseFont = true;
            this.gv档案.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.GroupButton.Options.UseFont = true;
            this.gv档案.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.GroupFooter.Options.UseFont = true;
            this.gv档案.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.GroupPanel.Options.UseFont = true;
            this.gv档案.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.GroupRow.Options.UseFont = true;
            this.gv档案.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.HeaderPanel.Options.UseFont = true;
            this.gv档案.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gv档案.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.HorzLine.Options.UseFont = true;
            this.gv档案.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.OddRow.Options.UseFont = true;
            this.gv档案.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.Preview.Options.UseFont = true;
            this.gv档案.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.Row.Options.UseFont = true;
            this.gv档案.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.RowSeparator.Options.UseFont = true;
            this.gv档案.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.SelectedRow.Options.UseFont = true;
            this.gv档案.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.TopNewRow.Options.UseFont = true;
            this.gv档案.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.VertLine.Options.UseFont = true;
            this.gv档案.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.ViewCaption.Options.UseFont = true;
            this.gv档案.ColumnPanelRowHeight = 30;
            this.gv档案.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29});
            this.gv档案.GridControl = this.gc档案;
            this.gv档案.GroupPanelText = "DragColumn";
            this.gv档案.Name = "gv档案";
            this.gv档案.OptionsView.ColumnAutoWidth = false;
            this.gv档案.OptionsView.EnableAppearanceEvenRow = true;
            this.gv档案.OptionsView.EnableAppearanceOddRow = true;
            this.gv档案.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn20
            // 
            this.gridColumn20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn20.Caption = "档案号";
            this.gridColumn20.ColumnEdit = this.link档案号;
            this.gridColumn20.FieldName = "个人档案编号";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 0;
            // 
            // link档案号
            // 
            this.link档案号.AutoHeight = false;
            this.link档案号.Name = "link档案号";
            this.link档案号.Click += new System.EventHandler(this.link档案号_Click);
            // 
            // gridColumn21
            // 
            this.gridColumn21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn21.Caption = "姓名";
            this.gridColumn21.FieldName = "姓名";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 1;
            // 
            // gridColumn22
            // 
            this.gridColumn22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn22.Caption = "性别";
            this.gridColumn22.FieldName = "性别";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 2;
            // 
            // gridColumn23
            // 
            this.gridColumn23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn23.Caption = "出生日期";
            this.gridColumn23.FieldName = "出生日期";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 3;
            // 
            // gridColumn24
            // 
            this.gridColumn24.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn24.Caption = "身份证号";
            this.gridColumn24.FieldName = "身份证号";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 4;
            // 
            // gridColumn25
            // 
            this.gridColumn25.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn25.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn25.Caption = "联系电话";
            this.gridColumn25.FieldName = "联系人电话";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 5;
            // 
            // gridColumn26
            // 
            this.gridColumn26.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn26.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn26.Caption = "当前所属机构";
            this.gridColumn26.FieldName = "所属机构";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 6;
            this.gridColumn26.Width = 105;
            // 
            // gridColumn27
            // 
            this.gridColumn27.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn27.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn27.Caption = "录入人";
            this.gridColumn27.FieldName = "创建人";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 7;
            // 
            // gridColumn28
            // 
            this.gridColumn28.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn28.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn28.Caption = "录入时间";
            this.gridColumn28.FieldName = "创建时间";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 8;
            // 
            // gridColumn29
            // 
            this.gridColumn29.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn29.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn29.Caption = "家庭档案编号";
            this.gridColumn29.FieldName = "家庭档案编号";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 9;
            this.gridColumn29.Width = 102;
            // 
            // sbtn导出
            // 
            this.sbtn导出.Image = ((System.Drawing.Image)(resources.GetObject("sbtn导出.Image")));
            this.sbtn导出.Location = new System.Drawing.Point(543, 52);
            this.sbtn导出.Name = "sbtn导出";
            this.sbtn导出.Size = new System.Drawing.Size(79, 22);
            this.sbtn导出.StyleController = this.layoutControl1;
            this.sbtn导出.TabIndex = 39;
            this.sbtn导出.Text = "导出";
            this.sbtn导出.Click += new System.EventHandler(this.sbtn导出_Click);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.sbtn导出;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(539, 48);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(83, 27);
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(494, 48);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(45, 27);
            this.emptySpaceItem6.Text = "emptySpaceItem6";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // frm儿童健康检查登记簿
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(945, 432);
            this.Name = "frm儿童健康检查登记簿";
            this.Text = "儿童健康检查登记薄";
            this.Load += new System.EventHandler(this.儿童健康检查登记簿_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDAH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSFZH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte结束2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte结束2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte开始1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte开始1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link区域名称)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link档案号)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Library.UserControls.DataGridControl gcSummary;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSummary;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btnEmpty;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraEditors.DateEdit dte结束2;
        private DevExpress.XtraEditors.DateEdit dte开始1;
        private DevExpress.XtraEditors.TreeListLookUpEdit txt机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link区域名称;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private Library.UserControls.DataGridControl gc档案;
        private DevExpress.XtraGrid.Views.Grid.GridView gv档案;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link档案号;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col村庄;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col姓名;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col性别;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col出生日期;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col建册日期;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col0岁检查一次;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col0岁检查二次;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col0岁检查三次;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col0岁检查四次;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col0岁评价;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col纯母乳;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col部分母乳;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col混合;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col人工;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col1岁检查一次;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col1岁检查二次;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col1岁评价;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col2岁检查一次;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col2岁检查二次;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col2岁评价;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col3岁检查一次;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col3岁评价;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col4岁检查一次;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col4岁评价;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col5岁检查一次;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col5岁评价;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col6岁检查一次;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col6岁评价;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit txtSFZH;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.TextEdit txtDAH;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col个人档案编号;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col母亲联系电话;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col父亲联系电话;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col母亲姓名;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col母亲身份证号;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand 个人档案编号;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand35;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand36;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand39;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand40;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand17;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand18;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand19;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand20;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand16;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand21;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand22;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand23;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand24;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand25;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand27;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand28;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand26;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand29;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand31;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand32;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand30;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand33;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand34;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand37;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand38;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand41;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand42;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand45;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand46;
        private DevExpress.XtraEditors.SimpleButton sbtn导出;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;

    }
}