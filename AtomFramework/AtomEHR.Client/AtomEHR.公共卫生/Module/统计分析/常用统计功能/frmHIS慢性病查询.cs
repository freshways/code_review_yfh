﻿using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.统计分析.常用统计功能
{
    public partial class frmHIS慢性病查询 : AtomEHR.Library.frmBaseDataForm
    {
        public frmHIS慢性病查询()
        {
            InitializeComponent();
        }

        private void frmHIS慢性病查询_Load(object sender, EventArgs e)
        {
            if (Loginer.CurrentUser.所属机构.Length >= 12)
            {
                txt机构.Properties.AutoExpandAllNodes = true;
            }
            else
            {
                txt机构.Properties.AutoExpandAllNodes = false;
            }
            this.InitializeForm();
        }
        protected override void InitializeForm()
        {
            //_BLL = new bllMXB高血压管理卡();// 业务逻辑层实例
            _SummaryView = new DevGridView(gridView1);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gridView1);

            try
            {
                this.txt机构.Properties.AutoExpandAllNodes = false;
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                txt机构.Properties.ValueMember = "机构编号";
                txt机构.Properties.DisplayMember = "机构名称";

                txt机构.Properties.TreeList.KeyFieldName = "机构编号";
                txt机构.Properties.TreeList.ParentFieldName = "上级机构";
                txt机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                txt机构.Properties.DataSource = dt所属机构;

                txt机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                txt机构.Text = Loginer.CurrentUser.所属机构;
            }
        }

        DataSet ds = null;
        private void btnQuery_Click(object sender, EventArgs e)
        {            
            string jbType = "";
            string rgid = "";
            if (!string.IsNullOrEmpty(cbo查询标准.Text))
            {
                jbType = cbo查询标准.Text;
            }
            if (txt机构.EditValue.ToString().Length >= 6)
            {
                rgid = "1" + txt机构.EditValue.ToString().Substring(10, 2);
            }
            ds = GetData(jbType, rgid);
            if (ds != null && ds.Tables.Count > 0)
            {
                gcSummary.DataSource = ds.Tables[0];
            }
            else
            {
                gcSummary.DataSource = null;
            }
        }

        private DataSet GetData(string jbType, string rgid)
        {
            DataSet ds = new bllMXB高血压随访表().GetHIS慢性病(jbType, rgid);
            
            return ds;
        }

    }
}
