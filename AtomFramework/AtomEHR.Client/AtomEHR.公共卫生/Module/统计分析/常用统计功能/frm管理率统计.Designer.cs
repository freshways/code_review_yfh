﻿namespace AtomEHR.公共卫生.Module
{
    partial class frm管理率统计 

    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm管理率统计));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.txt机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gcSummary = new AtomEHR.Library.UserControls.DataGridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col管理人群 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col总人口数 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col占总人口比率 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col理论管理数 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col实际管理数 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col健康管理率 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col规范管理率 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col体检表完整率 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col中医药健康管理率 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col血糖血压控制率 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gcSummary);
            this.tpSummary.Controls.Add(this.groupControl1);
            this.tpSummary.Size = new System.Drawing.Size(908, 387);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Size = new System.Drawing.Size(914, 393);
            // 
            // tcBusiness
            // 
            this.tcBusiness.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            this.tcBusiness.Size = new System.Drawing.Size(914, 393);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(776, 507);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(914, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(736, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(539, 2);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.layoutControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(908, 64);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "查询信息";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnQuery);
            this.layoutControl1.Controls.Add(this.txt机构);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 22);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(431, 278, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(904, 40);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnQuery
            // 
            this.btnQuery.Image = ((System.Drawing.Image)(resources.GetObject("btnQuery.Image")));
            this.btnQuery.Location = new System.Drawing.Point(405, 4);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(62, 22);
            this.btnQuery.StyleController = this.layoutControl1;
            this.btnQuery.TabIndex = 6;
            this.btnQuery.Text = "查询";
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // txt机构
            // 
            this.txt机构.Location = new System.Drawing.Point(35, 4);
            this.txt机构.Name = "txt机构";
            this.txt机构.Properties.AutoExpandAllNodes = false;
            this.txt机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt机构.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.txt机构.Size = new System.Drawing.Size(299, 20);
            this.txt机构.StyleController = this.layoutControl1;
            this.txt机构.TabIndex = 35;
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(212, 146);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.layoutControlItem6});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(904, 40);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txt机构;
            this.layoutControlItem1.CustomizationFormText = "机构:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(334, 26);
            this.layoutControlItem1.Text = "机构:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(28, 14);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(467, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(433, 26);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 26);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(900, 10);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(334, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(67, 26);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnQuery;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(401, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(66, 26);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // gcSummary
            // 
            this.gcSummary.AllowBandedGridColumnSort = false;
            this.gcSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSummary.IsBestFitColumns = true;
            this.gcSummary.Location = new System.Drawing.Point(0, 64);
            this.gcSummary.MainView = this.gridView1;
            this.gcSummary.Name = "gcSummary";
            this.gcSummary.ShowContextMenu = false;
            this.gcSummary.Size = new System.Drawing.Size(908, 323);
            this.gcSummary.StrWhere = "";
            this.gcSummary.TabIndex = 6;
            this.gcSummary.UseCheckBox = false;
            this.gcSummary.View = "";
            this.gcSummary.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gridView1.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gridView1.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.DetailTip.Options.UseFont = true;
            this.gridView1.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.Empty.Options.UseFont = true;
            this.gridView1.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.EvenRow.Options.UseFont = true;
            this.gridView1.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gridView1.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.FilterPanel.Options.UseFont = true;
            this.gridView1.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.FixedLine.Options.UseFont = true;
            this.gridView1.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.FocusedCell.Options.UseFont = true;
            this.gridView1.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.GroupButton.Options.UseFont = true;
            this.gridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView1.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gridView1.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.HorzLine.Options.UseFont = true;
            this.gridView1.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.OddRow.Options.UseFont = true;
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.RowSeparator.Options.UseFont = true;
            this.gridView1.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.SelectedRow.Options.UseFont = true;
            this.gridView1.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.TopNewRow.Options.UseFont = true;
            this.gridView1.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.VertLine.Options.UseFont = true;
            this.gridView1.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.ViewCaption.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col管理人群,
            this.col总人口数,
            this.col占总人口比率,
            this.col理论管理数,
            this.col实际管理数,
            this.col健康管理率,
            this.col规范管理率,
            this.col体检表完整率,
            this.col中医药健康管理率,
            this.col血糖血压控制率});
            this.gridView1.GridControl = this.gcSummary;
            this.gridView1.GroupPanelText = "DragColumn";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsBehavior.ReadOnly = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // col管理人群
            // 
            this.col管理人群.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col管理人群.AppearanceHeader.Options.UseFont = true;
            this.col管理人群.Caption = "管理人群";
            this.col管理人群.FieldName = "管理人群";
            this.col管理人群.Name = "col管理人群";
            this.col管理人群.Visible = true;
            this.col管理人群.VisibleIndex = 0;
            this.col管理人群.Width = 116;
            // 
            // col总人口数
            // 
            this.col总人口数.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col总人口数.AppearanceHeader.Options.UseFont = true;
            this.col总人口数.Caption = "总人口数";
            this.col总人口数.FieldName = "总人口数";
            this.col总人口数.Name = "col总人口数";
            this.col总人口数.Visible = true;
            this.col总人口数.VisibleIndex = 1;
            this.col总人口数.Width = 93;
            // 
            // col占总人口比率
            // 
            this.col占总人口比率.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col占总人口比率.AppearanceHeader.Options.UseFont = true;
            this.col占总人口比率.AppearanceHeader.Options.UseTextOptions = true;
            this.col占总人口比率.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col占总人口比率.Caption = "占总人口比率";
            this.col占总人口比率.FieldName = "占总人口比率";
            this.col占总人口比率.Name = "col占总人口比率";
            this.col占总人口比率.OptionsColumn.ReadOnly = true;
            this.col占总人口比率.Visible = true;
            this.col占总人口比率.VisibleIndex = 2;
            this.col占总人口比率.Width = 98;
            // 
            // col理论管理数
            // 
            this.col理论管理数.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col理论管理数.AppearanceHeader.Options.UseFont = true;
            this.col理论管理数.AppearanceHeader.Options.UseTextOptions = true;
            this.col理论管理数.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col理论管理数.Caption = "理论管理数";
            this.col理论管理数.FieldName = "理论管理数";
            this.col理论管理数.Name = "col理论管理数";
            this.col理论管理数.OptionsColumn.ReadOnly = true;
            this.col理论管理数.Visible = true;
            this.col理论管理数.VisibleIndex = 3;
            this.col理论管理数.Width = 78;
            // 
            // col实际管理数
            // 
            this.col实际管理数.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col实际管理数.AppearanceHeader.Options.UseFont = true;
            this.col实际管理数.AppearanceHeader.Options.UseTextOptions = true;
            this.col实际管理数.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col实际管理数.Caption = "实际管理数";
            this.col实际管理数.FieldName = "实际管理数";
            this.col实际管理数.Name = "col实际管理数";
            this.col实际管理数.OptionsColumn.ReadOnly = true;
            this.col实际管理数.Visible = true;
            this.col实际管理数.VisibleIndex = 4;
            this.col实际管理数.Width = 76;
            // 
            // col健康管理率
            // 
            this.col健康管理率.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col健康管理率.AppearanceHeader.Options.UseFont = true;
            this.col健康管理率.AppearanceHeader.Options.UseTextOptions = true;
            this.col健康管理率.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col健康管理率.Caption = "健康管理率";
            this.col健康管理率.DisplayFormat.FormatString = "{0:n2}%";
            this.col健康管理率.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col健康管理率.FieldName = "管理率";
            this.col健康管理率.Name = "col健康管理率";
            this.col健康管理率.OptionsColumn.ReadOnly = true;
            this.col健康管理率.Visible = true;
            this.col健康管理率.VisibleIndex = 5;
            this.col健康管理率.Width = 77;
            // 
            // col规范管理率
            // 
            this.col规范管理率.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col规范管理率.AppearanceHeader.Options.UseFont = true;
            this.col规范管理率.AppearanceHeader.Options.UseTextOptions = true;
            this.col规范管理率.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col规范管理率.Caption = "规范管理率";
            this.col规范管理率.Name = "col规范管理率";
            this.col规范管理率.OptionsColumn.ReadOnly = true;
            this.col规范管理率.Width = 82;
            // 
            // col体检表完整率
            // 
            this.col体检表完整率.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col体检表完整率.AppearanceHeader.Options.UseFont = true;
            this.col体检表完整率.AppearanceHeader.Options.UseTextOptions = true;
            this.col体检表完整率.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col体检表完整率.Caption = "体检表完整率";
            this.col体检表完整率.Name = "col体检表完整率";
            this.col体检表完整率.OptionsColumn.ReadOnly = true;
            this.col体检表完整率.Width = 97;
            // 
            // col中医药健康管理率
            // 
            this.col中医药健康管理率.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col中医药健康管理率.AppearanceHeader.Options.UseFont = true;
            this.col中医药健康管理率.AppearanceHeader.Options.UseTextOptions = true;
            this.col中医药健康管理率.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col中医药健康管理率.Caption = "中医药健康管理率";
            this.col中医药健康管理率.Name = "col中医药健康管理率";
            this.col中医药健康管理率.OptionsColumn.ReadOnly = true;
            this.col中医药健康管理率.Width = 119;
            // 
            // col血糖血压控制率
            // 
            this.col血糖血压控制率.Caption = "血糖(血压)控制率";
            this.col血糖血压控制率.Name = "col血糖血压控制率";
            this.col血糖血压控制率.Width = 109;
            // 
            // frm管理率统计
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(914, 419);
            this.Name = "frm管理率统计";
            this.Text = "管理率统计";
            this.Load += new System.EventHandler(this.孕产妇系统管理情况登记簿_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Library.UserControls.DataGridControl gcSummary;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraEditors.TreeListLookUpEdit txt机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn col管理人群;
        private DevExpress.XtraGrid.Columns.GridColumn col总人口数;
        private DevExpress.XtraGrid.Columns.GridColumn col占总人口比率;
        private DevExpress.XtraGrid.Columns.GridColumn col理论管理数;
        private DevExpress.XtraGrid.Columns.GridColumn col实际管理数;
        private DevExpress.XtraGrid.Columns.GridColumn col健康管理率;
        private DevExpress.XtraGrid.Columns.GridColumn col规范管理率;
        private DevExpress.XtraGrid.Columns.GridColumn col体检表完整率;
        private DevExpress.XtraGrid.Columns.GridColumn col中医药健康管理率;
        private DevExpress.XtraGrid.Columns.GridColumn col血糖血压控制率;

    }
}