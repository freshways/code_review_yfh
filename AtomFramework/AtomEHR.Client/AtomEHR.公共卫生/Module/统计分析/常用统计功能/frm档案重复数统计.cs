﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Interfaces;
using AtomEHR.公共卫生.Module.个人健康;
using DevExpress.XtraEditors.Repository;
using System.Threading.Tasks;

namespace AtomEHR.公共卫生.Module
{
    public partial class frm档案重复数统计 : AtomEHR.Library.frmBaseDataForm
    {
        DataSet ds = null;
        private string strWhere = null;
        bll健康体检 _bll = new bll健康体检();
        BackgroundWorker bgInvoke = new BackgroundWorker();
        public frm档案重复数统计()
        {
            InitializeComponent();
        }

        private void 档案动态使用率统计_Load(object sender, EventArgs e)
        {
            if (Loginer.CurrentUser.所属机构.Length >= 12)
            {
                txt机构.Properties.AutoExpandAllNodes = true;
            }
            else
            {
                txt机构.Properties.AutoExpandAllNodes = false;
            }
            this.InitializeForm();
            string m_ServerDate = _bll.ServiceDateTime;
            try
            {
                this.dte开始1.Text = Convert.ToDateTime(m_ServerDate).ToString("yyyy-MM-01");
                this.dte结束2.Text = Convert.ToDateTime(m_ServerDate).ToString("yyyy-MM-dd");
            }
            catch
            {
                this.dte开始1.Text = DateTime.Now.ToString("yyyy-MM-01");
                this.dte结束2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
            bgInvoke.DoWork += bgInvoke_DoWork;
            bgInvoke.RunWorkerCompleted += bgInvoke_RunWorkerCompleted;

            this.gvSummary.CustomRowCellEdit += gridView1_CustomRowCellEdit;
        }

        void gridView1_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            //throw new NotImplementedException();
            DataRowView row = gvSummary.GetRow(e.RowHandle) as DataRowView;
            if (row == null) return;
            string zongshu = row["联系人电话重复总数"].ToString();
            string hegeshu = row["空联系人电话总数"].ToString();
            string buhegeshu = row["空联系人姓名总数"].ToString();
            string shenfenzheng = row["身份证号重复总数"].ToString();
            if (e.Column == gridColumn3)
            {
                if (!string.IsNullOrEmpty(zongshu) && zongshu != "0")
                {
                    RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
                    link.Click += link联系人重复总数_Click;
                    e.RepositoryItem = link;
                }
            }

            if (e.Column == gridColumn4)
            {
                if (!string.IsNullOrEmpty(hegeshu) && hegeshu != "0")
                {
                    RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
                    link.Click += link空联系人总数_Click;
                    e.RepositoryItem = link;
                }

            }

            if (e.Column == gridColumn5)
            {
                if (!string.IsNullOrEmpty(buhegeshu) && buhegeshu != "0")
                {
                    RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
                    link.Click += link空联系人姓名总数_Click;
                    e.RepositoryItem = link;
                }

            }

            if (e.Column == gridColumn6)
            {
                if (!string.IsNullOrEmpty(shenfenzheng) && shenfenzheng != "0")
                {
                    RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
                    link.Click += link身份证号重复总数_Click;
                    e.RepositoryItem = link;
                }

            }
        }

        void link联系人重复总数_Click(object sender, EventArgs e)
        {
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();

            DataSet ds = new bll常用统计分析().Get重复统计(jigoubianma, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "2");

            if (ds != null && ds.Tables.Count > 0)
            {
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        void link空联系人总数_Click(object sender, EventArgs e)
        {
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();

            DataSet ds = new bll常用统计分析().Get重复统计(jigoubianma, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "3");

            if (ds != null && ds.Tables.Count > 0)
            {
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        void link空联系人姓名总数_Click(object sender, EventArgs e)
        {
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();

            DataSet ds = new bll常用统计分析().Get重复统计(jigoubianma, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "4");

            if (ds != null && ds.Tables.Count > 0)
            {
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        void link身份证号重复总数_Click(object sender, EventArgs e)
        {
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();

            DataSet ds = new bll常用统计分析().Get重复统计(jigoubianma, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "5");

            if (ds != null && ds.Tables.Count > 0)
            {
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        void bgInvoke_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();

            DataSet ds = e.Result as DataSet;
            if (ds != null && ds.Tables.Count > 0)
            {
                gcSummary.DataSource = ds.Tables[0];
                this.gvSummary.BestFitColumns();
                this.gcSummary.BringToFront();
            }
            else
            {
                gcSummary.DataSource = null;
            }
            this.btnQuery.Enabled = true;
        }

        void bgInvoke_DoWork(object sender, DoWorkEventArgs e)
        {
            DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(typeof(WaitForm1));

            try
            {
                DataSet ds = new bll常用统计分析().Get重复统计(txt机构.EditValue.ToString(), dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"),"1");

                e.Result = ds;
            }
            catch
            {
                e.Result = null;
            }
        }

        protected override void InitializeForm()
        {
            //_BLL = new bllMXB高血压管理卡();// 业务逻辑层实例
            //_SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gvSummary);
            frmGridCustomize.RegisterGrid(gv档案);

            try
            {
                this.txt机构.Properties.AutoExpandAllNodes = false;
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                txt机构.Properties.ValueMember = "机构编号";
                txt机构.Properties.DisplayMember = "机构名称";

                txt机构.Properties.TreeList.KeyFieldName = "机构编号";
                txt机构.Properties.TreeList.ParentFieldName = "上级机构";
                txt机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                txt机构.Properties.DataSource = dt所属机构;

                txt机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                txt机构.Text = Loginer.CurrentUser.所属机构;
            }
        }


        //查询按钮
        private void btnQuery_Click(object sender, EventArgs e)
        {
            //GetData2(txt机构.EditValue.ToString(), dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"));
            if (bgInvoke != null && !bgInvoke.IsBusy)
            { 
                bgInvoke.RunWorkerAsync();
                this.btnQuery.Enabled = false;
            }
              
        }

        private void btnEmpty_Click(object sender, EventArgs e)
        {
            this.gcSummary.BringToFront();
        }
    }
}
