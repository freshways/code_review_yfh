﻿using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using DevExpress.XtraEditors.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.统计分析.常用统计功能
{
    public partial class frm0_6岁儿童系统管理率 : AtomEHR.Library.frmBaseDataForm
    {
        public frm0_6岁儿童系统管理率()
        {
            InitializeComponent();
        }

        private string m_ServerDate = DateTime.Now.ToString("yyyy-MM-dd");
        DataSet ds = null;

        protected override void InitializeForm()
        {
            //_BLL = new bllMXB高血压管理卡();// 业务逻辑层实例
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gvSummary);

            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                txt机构.Properties.ValueMember = "机构编号";
                txt机构.Properties.DisplayMember = "机构名称";

                txt机构.Properties.TreeList.KeyFieldName = "机构编号";
                txt机构.Properties.TreeList.ParentFieldName = "上级机构";
                txt机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                txt机构.Properties.DataSource = dt所属机构;

                txt机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                txt机构.Text = Loginer.CurrentUser.所属机构;
            }
        }
        bll儿童基本信息 _bllChild = new bll儿童基本信息();
        private void frm0_6岁儿童系统管理率_Load(object sender, EventArgs e)
        {
            if (Loginer.CurrentUser.所属机构.Length >= 12)
                txt机构.Properties.AutoExpandAllNodes = true;
            else { txt机构.Properties.AutoExpandAllNodes = false; }
            this.InitializeForm();
            m_ServerDate = _bllChild.ServiceDateTime;
            try
            {
                this.dte开始1.Text = Convert.ToDateTime(m_ServerDate).AddYears(-6).ToString("yyyy-MM-01");
                this.dte结束2.Text = Convert.ToDateTime(m_ServerDate).ToString("yyyy-MM-dd");
            }
            catch
            {
                this.dte开始1.Text = DateTime.Now.ToString("yyyy-MM-01");
                this.dte结束2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
            this.gcSummary.Dock = DockStyle.Fill;
            this.gcSummary.BringToFront();
            this.gc档案.Dock = DockStyle.Fill;
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            ds = GetData(txt机构.EditValue.ToString(), "1");
            if (ds != null && ds.Tables.Count > 0)
            {
                gcSummary.DataSource = ds.Tables[0];
                this.gcSummary.BringToFront();
                this.gvSummary.BestFitColumns();
            }
        }

        private DataSet GetData(string prgid, string type)
        {
            string searchBy = this.cbo查询标准.Text.Trim() == "出生日期" ? "1" : "2";
            ds = _bllChild.GetChildGLL(txt机构.EditValue.ToString(), dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), searchBy, "1");
            return ds;
        }

        private void gvSummary_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            DataRowView row = gvSummary.GetRow(e.RowHandle) as DataRowView;
            if (row == null) return;
            if (e.Column == gc新生儿访视数)
            {
                string zongshu = row["新生儿访视数"].ToString();
                if (!string.IsNullOrEmpty(zongshu) && zongshu != "0")
                {
                    RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
                    link.Click += link新生儿访视数_Click;
                    e.RepositoryItem = link;
                }
            }

            if (e.Column == gc3岁管理人数)
            {
                string count = row["3岁管理人数"].ToString();
                if (!string.IsNullOrEmpty(count) && count != "0")
                {
                    RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
                    link.Click += link3岁管理数_Click;
                    e.RepositoryItem = link;
                }
            }

            if (e.Column == gc6岁管理人数)
            {
                string count = row["6岁管理人数"].ToString();
                if (!string.IsNullOrEmpty(count) && count != "0")
                {
                    RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
                    link.Click += link6岁管理数_Click;
                    e.RepositoryItem = link;
                }
            }
        }

        #region link_Click
        private void link区域名称_Click(object sender, EventArgs e)
        {
            string searchBy = this.cbo查询标准.Text.Trim() == "出生日期" ? "1" : "2";

            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();
            DataSet ds = _bllChild.GetChildGLL(jigoubianma, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), searchBy,"1");
            if (ds != null && ds.Tables.Count > 0)
            {
                gcSummary.DataSource = ds.Tables[0];
                this.gcSummary.BringToFront();
                this.gvSummary.BestFitColumns();
            }
        }

        private void link新生儿访视数_Click(object sender, EventArgs e)
        {
            string searchBy = this.cbo查询标准.Text.Trim() == "出生日期" ? "1" : "2";

            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();
            DataSet ds = _bllChild.GetChildGLL(jigoubianma, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), searchBy, "2");
            if (ds != null && ds.Tables.Count > 0)
            {
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        private void link3岁管理数_Click(object sender, EventArgs e)
        {
            string searchBy = this.cbo查询标准.Text.Trim() == "出生日期" ? "1" : "2";

            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();
            DataSet ds = _bllChild.GetChildGLL(jigoubianma, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), searchBy, "3");
            if (ds != null && ds.Tables.Count > 0)
            {
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        private void link6岁管理数_Click(object sender, EventArgs e)
        {
            string searchBy = this.cbo查询标准.Text.Trim() == "出生日期" ? "1" : "2";

            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();
            DataSet ds = _bllChild.GetChildGLL(jigoubianma, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), searchBy, "4");
            if (ds != null && ds.Tables.Count > 0)
            {
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        } 
        #endregion

    }
}
