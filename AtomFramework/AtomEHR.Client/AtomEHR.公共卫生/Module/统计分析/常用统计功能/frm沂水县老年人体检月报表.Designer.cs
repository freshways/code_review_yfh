﻿namespace AtomEHR.公共卫生.Module
{
    partial class frm沂水县老年人体检月报表
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm沂水县老年人体检月报表));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnEmpty = new DevExpress.XtraEditors.SimpleButton();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.cbo档案状态 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt完整度 = new DevExpress.XtraEditors.TextEdit();
            this.dte结束2 = new DevExpress.XtraEditors.DateEdit();
            this.dte开始1 = new DevExpress.XtraEditors.DateEdit();
            this.txt机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.gcSummary = new AtomEHR.Library.UserControls.DataGridControl();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col居住地址 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col65岁及以上常住居民数 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col月末累计65岁及以上老年人体检人数 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.link65以上老年人体检人数 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col月末累计中医体质辨识人数 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col65岁到70岁 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col71岁到80岁 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col80岁以上 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col月末累计健康老年人数 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col生活不能自理老年人数 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col血压异常 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col体质指数 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col血常规异常 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand16 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col尿常规异常 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand17 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col血糖异常 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand18 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col心电图异常 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand19 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col肝功能异常 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand20 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col肾功能异常 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand21 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col血脂异常 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand22 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colB超异常 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand23 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col中医体质辨识异常 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand24 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand25 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col脑血管疾病 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand26 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col肾脏疾病 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand27 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col心脏疾病 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand28 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col血管疾病 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand29 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col眼部疾病 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand30 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col神经系统疾病 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand31 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col高血压 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand32 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand33 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col恶性肿瘤 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand34 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col慢性阻塞性肺病 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand35 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col现存糖尿病 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand36 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col其他 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand37 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand38 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col现存高血压 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand39 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col糖尿病 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand40 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col冠心病 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand41 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col脑卒中 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colprgid = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.link区域名称 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.link总数 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.link合格数 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.link不合格数 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gvSummary = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gc档案 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv档案 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.link档案号 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt完整度.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte结束2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte结束2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte开始1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte开始1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link65以上老年人体检人数)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link区域名称)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link总数)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link合格数)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link不合格数)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link档案号)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gcSummary);
            this.tpSummary.Controls.Add(this.groupControl1);
            this.tpSummary.Controls.Add(this.gc档案);
            this.tpSummary.Size = new System.Drawing.Size(939, 400);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Size = new System.Drawing.Size(945, 406);
            // 
            // tcBusiness
            // 
            this.tcBusiness.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            this.tcBusiness.Size = new System.Drawing.Size(945, 406);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(776, 507);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(945, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(767, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(570, 2);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.layoutControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(939, 109);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "查询信息";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnEmpty);
            this.layoutControl1.Controls.Add(this.btnQuery);
            this.layoutControl1.Controls.Add(this.cbo档案状态);
            this.layoutControl1.Controls.Add(this.txt完整度);
            this.layoutControl1.Controls.Add(this.dte结束2);
            this.layoutControl1.Controls.Add(this.dte开始1);
            this.layoutControl1.Controls.Add(this.txt机构);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 22);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(431, 278, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(935, 85);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnEmpty
            // 
            this.btnEmpty.Image = ((System.Drawing.Image)(resources.GetObject("btnEmpty.Image")));
            this.btnEmpty.Location = new System.Drawing.Point(344, 52);
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.Size = new System.Drawing.Size(54, 22);
            this.btnEmpty.StyleController = this.layoutControl1;
            this.btnEmpty.TabIndex = 10;
            this.btnEmpty.Text = "后退";
            this.btnEmpty.Visible = false;
            this.btnEmpty.Click += new System.EventHandler(this.btnEmpty_Click);
            // 
            // btnQuery
            // 
            this.btnQuery.Image = ((System.Drawing.Image)(resources.GetObject("btnQuery.Image")));
            this.btnQuery.Location = new System.Drawing.Point(286, 52);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(54, 22);
            this.btnQuery.StyleController = this.layoutControl1;
            this.btnQuery.TabIndex = 6;
            this.btnQuery.Text = "查询";
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // cbo档案状态
            // 
            this.cbo档案状态.EditValue = "录入时间";
            this.cbo档案状态.Location = new System.Drawing.Point(636, 28);
            this.cbo档案状态.Name = "cbo档案状态";
            this.cbo档案状态.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo档案状态.Properties.Items.AddRange(new object[] {
            "录入时间",
            "调查时间"});
            this.cbo档案状态.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo档案状态.Size = new System.Drawing.Size(88, 20);
            this.cbo档案状态.StyleController = this.layoutControl1;
            this.cbo档案状态.TabIndex = 12;
            // 
            // txt完整度
            // 
            this.txt完整度.EditValue = "100";
            this.txt完整度.Location = new System.Drawing.Point(453, 28);
            this.txt完整度.Name = "txt完整度";
            this.txt完整度.Size = new System.Drawing.Size(76, 20);
            this.txt完整度.StyleController = this.layoutControl1;
            this.txt完整度.TabIndex = 6;
            // 
            // dte结束2
            // 
            this.dte结束2.EditValue = null;
            this.dte结束2.Location = new System.Drawing.Point(246, 28);
            this.dte结束2.Margin = new System.Windows.Forms.Padding(0);
            this.dte结束2.Name = "dte结束2";
            this.dte结束2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte结束2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte结束2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte结束2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte结束2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte结束2.Size = new System.Drawing.Size(100, 20);
            this.dte结束2.StyleController = this.layoutControl1;
            this.dte结束2.TabIndex = 3;
            // 
            // dte开始1
            // 
            this.dte开始1.EditValue = null;
            this.dte开始1.Location = new System.Drawing.Point(107, 28);
            this.dte开始1.Margin = new System.Windows.Forms.Padding(0);
            this.dte开始1.Name = "dte开始1";
            this.dte开始1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte开始1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte开始1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte开始1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte开始1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte开始1.Size = new System.Drawing.Size(118, 20);
            this.dte开始1.StyleController = this.layoutControl1;
            this.dte开始1.TabIndex = 1;
            // 
            // txt机构
            // 
            this.txt机构.Location = new System.Drawing.Point(107, 4);
            this.txt机构.Name = "txt机构";
            this.txt机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt机构.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.txt机构.Size = new System.Drawing.Size(239, 20);
            this.txt机构.StyleController = this.layoutControl1;
            this.txt机构.TabIndex = 35;
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(212, 146);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.emptySpaceItem1,
            this.emptySpaceItem3,
            this.emptySpaceItem2,
            this.emptySpaceItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(935, 85);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txt机构;
            this.layoutControlItem1.CustomizationFormText = "机构:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(346, 24);
            this.layoutControlItem1.Text = "机构:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(100, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dte开始1;
            this.layoutControlItem2.CustomizationFormText = "统计周期:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(225, 24);
            this.layoutControlItem2.Text = "统计周期:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(100, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.dte结束2;
            this.layoutControlItem3.CustomizationFormText = "至";
            this.layoutControlItem3.Location = new System.Drawing.Point(225, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(121, 24);
            this.layoutControlItem3.Text = "至";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(12, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txt完整度;
            this.layoutControlItem4.CustomizationFormText = "单份档案合格标准:";
            this.layoutControlItem4.Location = new System.Drawing.Point(346, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(183, 24);
            this.layoutControlItem4.Text = "单份档案合格标准:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(100, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.cbo档案状态;
            this.layoutControlItem5.CustomizationFormText = "%      查询标准:";
            this.layoutControlItem5.Location = new System.Drawing.Point(529, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(195, 24);
            this.layoutControlItem5.Text = "%      查询标准:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(100, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnQuery;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(282, 48);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(58, 33);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.btnEmpty;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(340, 48);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(58, 33);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(346, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(585, 24);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(398, 48);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(533, 33);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(724, 24);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(207, 24);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(282, 33);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // gcSummary
            // 
            this.gcSummary.AllowBandedGridColumnSort = false;
            this.gcSummary.IsBestFitColumns = true;
            this.gcSummary.Location = new System.Drawing.Point(-16, 143);
            this.gcSummary.MainView = this.bandedGridView1;
            this.gcSummary.Name = "gcSummary";
            this.gcSummary.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.link区域名称,
            this.link总数,
            this.link合格数,
            this.link不合格数,
            this.link65以上老年人体检人数});
            this.gcSummary.ShowContextMenu = false;
            this.gcSummary.Size = new System.Drawing.Size(564, 189);
            this.gcSummary.StrWhere = "";
            this.gcSummary.TabIndex = 6;
            this.gcSummary.UseCheckBox = false;
            this.gcSummary.View = "";
            this.gcSummary.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1,
            this.gvSummary});
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.bandedGridView1.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.bandedGridView1.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.DetailTip.Options.UseFont = true;
            this.bandedGridView1.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.Empty.Options.UseFont = true;
            this.bandedGridView1.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.EvenRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FilterCloseButton.Options.UseFont = true;
            this.bandedGridView1.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FilterPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FixedLine.Options.UseFont = true;
            this.bandedGridView1.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FocusedCell.Options.UseFont = true;
            this.bandedGridView1.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FocusedRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.bandedGridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.GroupButton.Options.UseFont = true;
            this.bandedGridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.bandedGridView1.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.GroupRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.bandedGridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridView1.Appearance.HeaderPanelBackground.Options.UseTextOptions = true;
            this.bandedGridView1.Appearance.HeaderPanelBackground.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridView1.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.HideSelectionRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.HorzLine.Options.UseFont = true;
            this.bandedGridView1.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.OddRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.Preview.Options.UseFont = true;
            this.bandedGridView1.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.Row.Options.UseFont = true;
            this.bandedGridView1.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.RowSeparator.Options.UseFont = true;
            this.bandedGridView1.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.SelectedRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.TopNewRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.VertLine.Options.UseFont = true;
            this.bandedGridView1.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.ViewCaption.Options.UseFont = true;
            this.bandedGridView1.AppearancePrint.HeaderPanel.Options.UseTextOptions = true;
            this.bandedGridView1.AppearancePrint.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1});
            this.bandedGridView1.ColumnPanelRowHeight = 30;
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.col居住地址,
            this.col65岁及以上常住居民数,
            this.col月末累计65岁及以上老年人体检人数,
            this.col月末累计中医体质辨识人数,
            this.col65岁到70岁,
            this.col71岁到80岁,
            this.col80岁以上,
            this.col月末累计健康老年人数,
            this.col生活不能自理老年人数,
            this.col血压异常,
            this.col体质指数,
            this.col血常规异常,
            this.col尿常规异常,
            this.col血糖异常,
            this.col心电图异常,
            this.col肝功能异常,
            this.col肾功能异常,
            this.col血脂异常,
            this.colB超异常,
            this.col中医体质辨识异常,
            this.col脑血管疾病,
            this.col肾脏疾病,
            this.col心脏疾病,
            this.col血管疾病,
            this.col眼部疾病,
            this.col神经系统疾病,
            this.col现存高血压,
            this.col恶性肿瘤,
            this.col慢性阻塞性肺病,
            this.col现存糖尿病,
            this.col其他,
            this.col高血压,
            this.col糖尿病,
            this.col冠心病,
            this.col脑卒中,
            this.colprgid});
            this.bandedGridView1.GridControl = this.gcSummary;
            this.bandedGridView1.GroupPanelText = "DragColumn";
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsBehavior.Editable = false;
            this.bandedGridView1.OptionsBehavior.ReadOnly = true;
            this.bandedGridView1.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.bandedGridView1.OptionsView.EnableAppearanceOddRow = true;
            this.bandedGridView1.OptionsView.ShowColumnHeaders = false;
            this.bandedGridView1.OptionsView.ShowFooter = true;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            this.bandedGridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.bandedGridView1_CustomDrawCell);
            this.bandedGridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.bandedGridView1_CustomRowCellEdit);
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 15F);
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "65 岁 及 以 上 老 年 人 健 康 查 体 情 况 月 报 表 ";
            this.gridBand1.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2,
            this.gridBand3,
            this.gridBand4,
            this.gridBand5,
            this.gridBand6,
            this.gridBand10,
            this.gridBand11,
            this.gridBand24,
            this.gridBand37});
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 2890;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "市、县（市、区）、镇街名称 ";
            this.gridBand2.Columns.Add(this.col居住地址);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 0;
            this.gridBand2.Width = 172;
            // 
            // col居住地址
            // 
            this.col居住地址.Caption = "市、县（市、区）、镇街名称 ";
            this.col居住地址.FieldName = "所属机构";
            this.col居住地址.Name = "col居住地址";
            this.col居住地址.OptionsColumn.FixedWidth = true;
            this.col居住地址.Visible = true;
            this.col居住地址.Width = 172;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "65岁及以上\r\n常住居民数（人）";
            this.gridBand3.Columns.Add(this.col65岁及以上常住居民数);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 1;
            this.gridBand3.Width = 110;
            // 
            // col65岁及以上常住居民数
            // 
            this.col65岁及以上常住居民数.AppearanceCell.Options.UseTextOptions = true;
            this.col65岁及以上常住居民数.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col65岁及以上常住居民数.AppearanceHeader.Options.UseTextOptions = true;
            this.col65岁及以上常住居民数.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col65岁及以上常住居民数.Caption = "65岁及以上常住居民数";
            this.col65岁及以上常住居民数.FieldName = "65岁及以上常住居民数";
            this.col65岁及以上常住居民数.Name = "col65岁及以上常住居民数";
            this.col65岁及以上常住居民数.OptionsColumn.FixedWidth = true;
            this.col65岁及以上常住居民数.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col65岁及以上常住居民数.Visible = true;
            this.col65岁及以上常住居民数.Width = 110;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "月末累计65岁及以上\r\n老年人体检人数（人）";
            this.gridBand4.Columns.Add(this.col月末累计65岁及以上老年人体检人数);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 2;
            this.gridBand4.Width = 129;
            // 
            // col月末累计65岁及以上老年人体检人数
            // 
            this.col月末累计65岁及以上老年人体检人数.AppearanceCell.Options.UseTextOptions = true;
            this.col月末累计65岁及以上老年人体检人数.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col月末累计65岁及以上老年人体检人数.AppearanceHeader.Options.UseTextOptions = true;
            this.col月末累计65岁及以上老年人体检人数.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col月末累计65岁及以上老年人体检人数.Caption = "月末累计65岁及以上老年人体检人数";
            this.col月末累计65岁及以上老年人体检人数.ColumnEdit = this.link65以上老年人体检人数;
            this.col月末累计65岁及以上老年人体检人数.FieldName = "月末累计65岁及以上老年人体检人数";
            this.col月末累计65岁及以上老年人体检人数.Name = "col月末累计65岁及以上老年人体检人数";
            this.col月末累计65岁及以上老年人体检人数.OptionsColumn.FixedWidth = true;
            this.col月末累计65岁及以上老年人体检人数.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col月末累计65岁及以上老年人体检人数.Visible = true;
            this.col月末累计65岁及以上老年人体检人数.Width = 129;
            // 
            // link65以上老年人体检人数
            // 
            this.link65以上老年人体检人数.AutoHeight = false;
            this.link65以上老年人体检人数.Name = "link65以上老年人体检人数";
            this.link65以上老年人体检人数.Click += new System.EventHandler(this.link65以上老年人体检人数_Click);
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "月末累计\r\n中医体质辨识人数（人）";
            this.gridBand5.Columns.Add(this.col月末累计中医体质辨识人数);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 3;
            this.gridBand5.Width = 143;
            // 
            // col月末累计中医体质辨识人数
            // 
            this.col月末累计中医体质辨识人数.AppearanceCell.Options.UseTextOptions = true;
            this.col月末累计中医体质辨识人数.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col月末累计中医体质辨识人数.Caption = "月末累计中医体质辨识人数";
            this.col月末累计中医体质辨识人数.FieldName = "月末累计中医体质辨识人数";
            this.col月末累计中医体质辨识人数.Name = "col月末累计中医体质辨识人数";
            this.col月末累计中医体质辨识人数.OptionsColumn.FixedWidth = true;
            this.col月末累计中医体质辨识人数.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col月末累计中医体质辨识人数.Visible = true;
            this.col月末累计中医体质辨识人数.Width = 143;
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.Caption = "65岁以上老年人年龄分布";
            this.gridBand6.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand7,
            this.gridBand8,
            this.gridBand9});
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 4;
            this.gridBand6.Width = 225;
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand7.Caption = "65岁-70岁";
            this.gridBand7.Columns.Add(this.col65岁到70岁);
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 0;
            this.gridBand7.Width = 75;
            // 
            // col65岁到70岁
            // 
            this.col65岁到70岁.Caption = "65岁-70岁";
            this.col65岁到70岁.FieldName = "65岁-70岁";
            this.col65岁到70岁.Name = "col65岁到70岁";
            this.col65岁到70岁.OptionsColumn.AllowEdit = false;
            this.col65岁到70岁.OptionsColumn.FixedWidth = true;
            this.col65岁到70岁.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col65岁到70岁.Visible = true;
            // 
            // gridBand8
            // 
            this.gridBand8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand8.Caption = "71岁-80岁";
            this.gridBand8.Columns.Add(this.col71岁到80岁);
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = 1;
            this.gridBand8.Width = 75;
            // 
            // col71岁到80岁
            // 
            this.col71岁到80岁.Caption = "71岁-80岁";
            this.col71岁到80岁.FieldName = "71岁-80岁";
            this.col71岁到80岁.Name = "col71岁到80岁";
            this.col71岁到80岁.OptionsColumn.FixedWidth = true;
            this.col71岁到80岁.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col71岁到80岁.Visible = true;
            // 
            // gridBand9
            // 
            this.gridBand9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand9.Caption = "80岁以上";
            this.gridBand9.Columns.Add(this.col80岁以上);
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.VisibleIndex = 2;
            this.gridBand9.Width = 75;
            // 
            // col80岁以上
            // 
            this.col80岁以上.Caption = "80岁以上";
            this.col80岁以上.FieldName = "80岁以上";
            this.col80岁以上.Name = "col80岁以上";
            this.col80岁以上.OptionsColumn.FixedWidth = true;
            this.col80岁以上.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col80岁以上.Visible = true;
            // 
            // gridBand10
            // 
            this.gridBand10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand10.Caption = "月末累计\r\n健康老年人数（体检无异常）";
            this.gridBand10.Columns.Add(this.col月末累计健康老年人数);
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.VisibleIndex = 5;
            this.gridBand10.Width = 165;
            // 
            // col月末累计健康老年人数
            // 
            this.col月末累计健康老年人数.Caption = "月末累计健康老年人数";
            this.col月末累计健康老年人数.FieldName = "月末累计健康老年人数";
            this.col月末累计健康老年人数.Name = "col月末累计健康老年人数";
            this.col月末累计健康老年人数.OptionsColumn.FixedWidth = true;
            this.col月末累计健康老年人数.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col月末累计健康老年人数.Visible = true;
            this.col月末累计健康老年人数.Width = 165;
            // 
            // gridBand11
            // 
            this.gridBand11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand11.Caption = "其中月末累计体检结果异常人数（人）";
            this.gridBand11.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand12,
            this.gridBand13,
            this.gridBand14,
            this.gridBand15,
            this.gridBand16,
            this.gridBand17,
            this.gridBand18,
            this.gridBand19,
            this.gridBand20,
            this.gridBand21,
            this.gridBand22,
            this.gridBand23});
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.VisibleIndex = 6;
            this.gridBand11.Width = 962;
            // 
            // gridBand12
            // 
            this.gridBand12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand12.Caption = "生活不能自理老年人数\r\n（自理能力评估≥19分）";
            this.gridBand12.Columns.Add(this.col生活不能自理老年人数);
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.VisibleIndex = 0;
            this.gridBand12.Width = 148;
            // 
            // col生活不能自理老年人数
            // 
            this.col生活不能自理老年人数.Caption = "生活不能自理老年人数";
            this.col生活不能自理老年人数.FieldName = "生活不能自理老年人数";
            this.col生活不能自理老年人数.Name = "col生活不能自理老年人数";
            this.col生活不能自理老年人数.OptionsColumn.FixedWidth = true;
            this.col生活不能自理老年人数.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col生活不能自理老年人数.Visible = true;
            this.col生活不能自理老年人数.Width = 148;
            // 
            // gridBand13
            // 
            this.gridBand13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand13.Caption = "血压异常";
            this.gridBand13.Columns.Add(this.col血压异常);
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.VisibleIndex = 1;
            // 
            // col血压异常
            // 
            this.col血压异常.Caption = "血压异常";
            this.col血压异常.FieldName = "血压异常";
            this.col血压异常.Name = "col血压异常";
            this.col血压异常.OptionsColumn.FixedWidth = true;
            this.col血压异常.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col血压异常.Visible = true;
            this.col血压异常.Width = 70;
            // 
            // gridBand14
            // 
            this.gridBand14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand14.Caption = "体质指数\r\n（BMI≥28)";
            this.gridBand14.Columns.Add(this.col体质指数);
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.VisibleIndex = 2;
            this.gridBand14.Width = 82;
            // 
            // col体质指数
            // 
            this.col体质指数.Caption = "体质指数";
            this.col体质指数.FieldName = "体质指数";
            this.col体质指数.Name = "col体质指数";
            this.col体质指数.OptionsColumn.FixedWidth = true;
            this.col体质指数.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col体质指数.Visible = true;
            this.col体质指数.Width = 82;
            // 
            // gridBand15
            // 
            this.gridBand15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand15.Caption = "血常规异常";
            this.gridBand15.Columns.Add(this.col血常规异常);
            this.gridBand15.Name = "gridBand15";
            this.gridBand15.VisibleIndex = 3;
            this.gridBand15.Width = 75;
            // 
            // col血常规异常
            // 
            this.col血常规异常.Caption = "血常规异常";
            this.col血常规异常.FieldName = "血常规异常";
            this.col血常规异常.Name = "col血常规异常";
            this.col血常规异常.OptionsColumn.FixedWidth = true;
            this.col血常规异常.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col血常规异常.Visible = true;
            // 
            // gridBand16
            // 
            this.gridBand16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand16.Caption = "尿常规异常";
            this.gridBand16.Columns.Add(this.col尿常规异常);
            this.gridBand16.Name = "gridBand16";
            this.gridBand16.VisibleIndex = 4;
            this.gridBand16.Width = 75;
            // 
            // col尿常规异常
            // 
            this.col尿常规异常.Caption = "尿常规异常";
            this.col尿常规异常.FieldName = "尿常规异常";
            this.col尿常规异常.Name = "col尿常规异常";
            this.col尿常规异常.OptionsColumn.FixedWidth = true;
            this.col尿常规异常.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col尿常规异常.Visible = true;
            // 
            // gridBand17
            // 
            this.gridBand17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand17.Caption = "血糖异常";
            this.gridBand17.Columns.Add(this.col血糖异常);
            this.gridBand17.Name = "gridBand17";
            this.gridBand17.VisibleIndex = 5;
            this.gridBand17.Width = 75;
            // 
            // col血糖异常
            // 
            this.col血糖异常.Caption = "血糖异常";
            this.col血糖异常.FieldName = "血糖异常";
            this.col血糖异常.Name = "col血糖异常";
            this.col血糖异常.OptionsColumn.FixedWidth = true;
            this.col血糖异常.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col血糖异常.Visible = true;
            // 
            // gridBand18
            // 
            this.gridBand18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand18.Caption = "心电图\r\n异常";
            this.gridBand18.Columns.Add(this.col心电图异常);
            this.gridBand18.Name = "gridBand18";
            this.gridBand18.VisibleIndex = 6;
            this.gridBand18.Width = 50;
            // 
            // col心电图异常
            // 
            this.col心电图异常.Caption = "心电图异常";
            this.col心电图异常.FieldName = "心电图异常";
            this.col心电图异常.Name = "col心电图异常";
            this.col心电图异常.OptionsColumn.FixedWidth = true;
            this.col心电图异常.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col心电图异常.Visible = true;
            this.col心电图异常.Width = 50;
            // 
            // gridBand19
            // 
            this.gridBand19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand19.Caption = "肝功能\r\n异常";
            this.gridBand19.Columns.Add(this.col肝功能异常);
            this.gridBand19.Name = "gridBand19";
            this.gridBand19.VisibleIndex = 7;
            this.gridBand19.Width = 55;
            // 
            // col肝功能异常
            // 
            this.col肝功能异常.Caption = "肝功能异常";
            this.col肝功能异常.FieldName = "肝功能异常";
            this.col肝功能异常.Name = "col肝功能异常";
            this.col肝功能异常.OptionsColumn.FixedWidth = true;
            this.col肝功能异常.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col肝功能异常.Visible = true;
            this.col肝功能异常.Width = 55;
            // 
            // gridBand20
            // 
            this.gridBand20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand20.Caption = "肾功能\r\n异常";
            this.gridBand20.Columns.Add(this.col肾功能异常);
            this.gridBand20.Name = "gridBand20";
            this.gridBand20.VisibleIndex = 8;
            this.gridBand20.Width = 62;
            // 
            // col肾功能异常
            // 
            this.col肾功能异常.Caption = "肾功能异常";
            this.col肾功能异常.FieldName = "肾功能异常";
            this.col肾功能异常.Name = "col肾功能异常";
            this.col肾功能异常.OptionsColumn.FixedWidth = true;
            this.col肾功能异常.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col肾功能异常.Visible = true;
            this.col肾功能异常.Width = 62;
            // 
            // gridBand21
            // 
            this.gridBand21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand21.Caption = "血脂异常";
            this.gridBand21.Columns.Add(this.col血脂异常);
            this.gridBand21.Name = "gridBand21";
            this.gridBand21.VisibleIndex = 9;
            this.gridBand21.Width = 75;
            // 
            // col血脂异常
            // 
            this.col血脂异常.Caption = "血脂异常";
            this.col血脂异常.FieldName = "血脂异常";
            this.col血脂异常.Name = "col血脂异常";
            this.col血脂异常.OptionsColumn.FixedWidth = true;
            this.col血脂异常.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col血脂异常.Visible = true;
            // 
            // gridBand22
            // 
            this.gridBand22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand22.Caption = "B超异常";
            this.gridBand22.Columns.Add(this.colB超异常);
            this.gridBand22.Name = "gridBand22";
            this.gridBand22.VisibleIndex = 10;
            this.gridBand22.Width = 75;
            // 
            // colB超异常
            // 
            this.colB超异常.Caption = "B超异常";
            this.colB超异常.FieldName = "B超异常";
            this.colB超异常.Name = "colB超异常";
            this.colB超异常.OptionsColumn.FixedWidth = true;
            this.colB超异常.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colB超异常.Visible = true;
            // 
            // gridBand23
            // 
            this.gridBand23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand23.Caption = "中医体质辨识异常\r\n（非平和质）";
            this.gridBand23.Columns.Add(this.col中医体质辨识异常);
            this.gridBand23.Name = "gridBand23";
            this.gridBand23.VisibleIndex = 11;
            this.gridBand23.Width = 120;
            // 
            // col中医体质辨识异常
            // 
            this.col中医体质辨识异常.Caption = "中医体质辨识异常";
            this.col中医体质辨识异常.FieldName = "中医体质辨识异常";
            this.col中医体质辨识异常.Name = "col中医体质辨识异常";
            this.col中医体质辨识异常.OptionsColumn.FixedWidth = true;
            this.col中医体质辨识异常.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col中医体质辨识异常.Visible = true;
            this.col中医体质辨识异常.Width = 120;
            // 
            // gridBand24
            // 
            this.gridBand24.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand24.Caption = "月末累计现存主要健康问题异常人数（人）";
            this.gridBand24.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand25,
            this.gridBand26,
            this.gridBand27,
            this.gridBand28,
            this.gridBand29,
            this.gridBand30,
            this.gridBand31,
            this.gridBand32});
            this.gridBand24.Name = "gridBand24";
            this.gridBand24.VisibleIndex = 7;
            this.gridBand24.Width = 759;
            // 
            // gridBand25
            // 
            this.gridBand25.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand25.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand25.Caption = "脑血管\r\n疾病";
            this.gridBand25.Columns.Add(this.col脑血管疾病);
            this.gridBand25.Name = "gridBand25";
            this.gridBand25.VisibleIndex = 0;
            this.gridBand25.Width = 62;
            // 
            // col脑血管疾病
            // 
            this.col脑血管疾病.Caption = "脑血管疾病";
            this.col脑血管疾病.FieldName = "脑血管疾病";
            this.col脑血管疾病.Name = "col脑血管疾病";
            this.col脑血管疾病.OptionsColumn.FixedWidth = true;
            this.col脑血管疾病.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col脑血管疾病.Visible = true;
            this.col脑血管疾病.Width = 62;
            // 
            // gridBand26
            // 
            this.gridBand26.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand26.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand26.Caption = "肾脏\r\n疾病";
            this.gridBand26.Columns.Add(this.col肾脏疾病);
            this.gridBand26.Name = "gridBand26";
            this.gridBand26.VisibleIndex = 1;
            this.gridBand26.Width = 57;
            // 
            // col肾脏疾病
            // 
            this.col肾脏疾病.Caption = "肾脏疾病";
            this.col肾脏疾病.FieldName = "肾脏疾病";
            this.col肾脏疾病.Name = "col肾脏疾病";
            this.col肾脏疾病.OptionsColumn.FixedWidth = true;
            this.col肾脏疾病.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col肾脏疾病.Visible = true;
            this.col肾脏疾病.Width = 57;
            // 
            // gridBand27
            // 
            this.gridBand27.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand27.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand27.Caption = "心脏\r\n疾病";
            this.gridBand27.Columns.Add(this.col心脏疾病);
            this.gridBand27.Name = "gridBand27";
            this.gridBand27.VisibleIndex = 2;
            this.gridBand27.Width = 63;
            // 
            // col心脏疾病
            // 
            this.col心脏疾病.Caption = "心脏疾病";
            this.col心脏疾病.FieldName = "心脏疾病";
            this.col心脏疾病.Name = "col心脏疾病";
            this.col心脏疾病.OptionsColumn.FixedWidth = true;
            this.col心脏疾病.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col心脏疾病.Visible = true;
            this.col心脏疾病.Width = 63;
            // 
            // gridBand28
            // 
            this.gridBand28.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand28.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand28.Caption = "血管\r\n疾病";
            this.gridBand28.Columns.Add(this.col血管疾病);
            this.gridBand28.Name = "gridBand28";
            this.gridBand28.VisibleIndex = 3;
            this.gridBand28.Width = 51;
            // 
            // col血管疾病
            // 
            this.col血管疾病.Caption = "血管疾病";
            this.col血管疾病.FieldName = "血管疾病";
            this.col血管疾病.Name = "col血管疾病";
            this.col血管疾病.OptionsColumn.FixedWidth = true;
            this.col血管疾病.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col血管疾病.Visible = true;
            this.col血管疾病.Width = 51;
            // 
            // gridBand29
            // 
            this.gridBand29.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand29.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand29.Caption = "眼部\r\n疾病";
            this.gridBand29.Columns.Add(this.col眼部疾病);
            this.gridBand29.Name = "gridBand29";
            this.gridBand29.VisibleIndex = 4;
            this.gridBand29.Width = 59;
            // 
            // col眼部疾病
            // 
            this.col眼部疾病.Caption = "眼部疾病";
            this.col眼部疾病.FieldName = "眼部疾病";
            this.col眼部疾病.Name = "col眼部疾病";
            this.col眼部疾病.OptionsColumn.FixedWidth = true;
            this.col眼部疾病.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col眼部疾病.Visible = true;
            this.col眼部疾病.Width = 59;
            // 
            // gridBand30
            // 
            this.gridBand30.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand30.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand30.Caption = "神经系统\r\n疾病";
            this.gridBand30.Columns.Add(this.col神经系统疾病);
            this.gridBand30.Name = "gridBand30";
            this.gridBand30.VisibleIndex = 5;
            this.gridBand30.Width = 61;
            // 
            // col神经系统疾病
            // 
            this.col神经系统疾病.Caption = "神经系统疾病";
            this.col神经系统疾病.FieldName = "神经系统疾病";
            this.col神经系统疾病.Name = "col神经系统疾病";
            this.col神经系统疾病.OptionsColumn.FixedWidth = true;
            this.col神经系统疾病.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col神经系统疾病.Visible = true;
            this.col神经系统疾病.Width = 61;
            // 
            // gridBand31
            // 
            this.gridBand31.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand31.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand31.Caption = "高血压";
            this.gridBand31.Columns.Add(this.col高血压);
            this.gridBand31.Name = "gridBand31";
            this.gridBand31.VisibleIndex = 6;
            this.gridBand31.Width = 75;
            // 
            // col高血压
            // 
            this.col高血压.Caption = "高血压";
            this.col高血压.FieldName = "高血压";
            this.col高血压.Name = "col高血压";
            this.col高血压.OptionsColumn.FixedWidth = true;
            this.col高血压.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col高血压.Visible = true;
            // 
            // gridBand32
            // 
            this.gridBand32.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand32.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand32.Caption = "其他\r\n疾病";
            this.gridBand32.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand33,
            this.gridBand34,
            this.gridBand35,
            this.gridBand36});
            this.gridBand32.Name = "gridBand32";
            this.gridBand32.VisibleIndex = 7;
            this.gridBand32.Width = 331;
            // 
            // gridBand33
            // 
            this.gridBand33.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand33.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand33.Caption = "恶性肿瘤";
            this.gridBand33.Columns.Add(this.col恶性肿瘤);
            this.gridBand33.Name = "gridBand33";
            this.gridBand33.VisibleIndex = 0;
            this.gridBand33.Width = 75;
            // 
            // col恶性肿瘤
            // 
            this.col恶性肿瘤.Caption = "恶性肿瘤";
            this.col恶性肿瘤.FieldName = "恶性肿瘤";
            this.col恶性肿瘤.Name = "col恶性肿瘤";
            this.col恶性肿瘤.OptionsColumn.FixedWidth = true;
            this.col恶性肿瘤.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col恶性肿瘤.Visible = true;
            // 
            // gridBand34
            // 
            this.gridBand34.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand34.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand34.Caption = "慢性\r\n阻塞性肺病";
            this.gridBand34.Columns.Add(this.col慢性阻塞性肺病);
            this.gridBand34.Name = "gridBand34";
            this.gridBand34.VisibleIndex = 1;
            this.gridBand34.Width = 106;
            // 
            // col慢性阻塞性肺病
            // 
            this.col慢性阻塞性肺病.Caption = "慢性阻塞性肺病";
            this.col慢性阻塞性肺病.FieldName = "慢性阻塞性肺病";
            this.col慢性阻塞性肺病.Name = "col慢性阻塞性肺病";
            this.col慢性阻塞性肺病.OptionsColumn.FixedWidth = true;
            this.col慢性阻塞性肺病.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col慢性阻塞性肺病.Visible = true;
            this.col慢性阻塞性肺病.Width = 106;
            // 
            // gridBand35
            // 
            this.gridBand35.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand35.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand35.Caption = "糖尿病";
            this.gridBand35.Columns.Add(this.col现存糖尿病);
            this.gridBand35.Name = "gridBand35";
            this.gridBand35.VisibleIndex = 2;
            this.gridBand35.Width = 75;
            // 
            // col现存糖尿病
            // 
            this.col现存糖尿病.Caption = "糖尿病";
            this.col现存糖尿病.FieldName = "现存糖尿病";
            this.col现存糖尿病.Name = "col现存糖尿病";
            this.col现存糖尿病.OptionsColumn.FixedWidth = true;
            this.col现存糖尿病.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col现存糖尿病.Visible = true;
            // 
            // gridBand36
            // 
            this.gridBand36.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand36.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand36.Caption = "其他";
            this.gridBand36.Columns.Add(this.col其他);
            this.gridBand36.Name = "gridBand36";
            this.gridBand36.VisibleIndex = 3;
            this.gridBand36.Width = 75;
            // 
            // col其他
            // 
            this.col其他.Caption = "其他";
            this.col其他.FieldName = "其他";
            this.col其他.Name = "col其他";
            this.col其他.OptionsColumn.FixedWidth = true;
            this.col其他.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col其他.Visible = true;
            // 
            // gridBand37
            // 
            this.gridBand37.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand37.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand37.Caption = "已纳入重点疾病患者管理人数（人）";
            this.gridBand37.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand38,
            this.gridBand39,
            this.gridBand40,
            this.gridBand41});
            this.gridBand37.Name = "gridBand37";
            this.gridBand37.VisibleIndex = 8;
            this.gridBand37.Width = 225;
            // 
            // gridBand38
            // 
            this.gridBand38.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand38.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand38.Caption = "高血压";
            this.gridBand38.Columns.Add(this.col现存高血压);
            this.gridBand38.Name = "gridBand38";
            this.gridBand38.VisibleIndex = 0;
            this.gridBand38.Width = 49;
            // 
            // col现存高血压
            // 
            this.col现存高血压.Caption = "高血压";
            this.col现存高血压.FieldName = "现存高血压";
            this.col现存高血压.Name = "col现存高血压";
            this.col现存高血压.OptionsColumn.FixedWidth = true;
            this.col现存高血压.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col现存高血压.Visible = true;
            this.col现存高血压.Width = 49;
            // 
            // gridBand39
            // 
            this.gridBand39.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand39.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand39.Caption = "糖尿病";
            this.gridBand39.Columns.Add(this.col糖尿病);
            this.gridBand39.Name = "gridBand39";
            this.gridBand39.VisibleIndex = 1;
            this.gridBand39.Width = 48;
            // 
            // col糖尿病
            // 
            this.col糖尿病.Caption = "糖尿病";
            this.col糖尿病.FieldName = "糖尿病";
            this.col糖尿病.Name = "col糖尿病";
            this.col糖尿病.OptionsColumn.FixedWidth = true;
            this.col糖尿病.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col糖尿病.Visible = true;
            this.col糖尿病.Width = 48;
            // 
            // gridBand40
            // 
            this.gridBand40.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand40.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand40.Caption = "冠心病";
            this.gridBand40.Columns.Add(this.col冠心病);
            this.gridBand40.Name = "gridBand40";
            this.gridBand40.VisibleIndex = 2;
            this.gridBand40.Width = 53;
            // 
            // col冠心病
            // 
            this.col冠心病.Caption = "冠心病";
            this.col冠心病.FieldName = "冠心病";
            this.col冠心病.Name = "col冠心病";
            this.col冠心病.OptionsColumn.FixedWidth = true;
            this.col冠心病.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col冠心病.Visible = true;
            this.col冠心病.Width = 53;
            // 
            // gridBand41
            // 
            this.gridBand41.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand41.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand41.Caption = "脑卒中";
            this.gridBand41.Columns.Add(this.col脑卒中);
            this.gridBand41.Name = "gridBand41";
            this.gridBand41.VisibleIndex = 3;
            this.gridBand41.Width = 75;
            // 
            // col脑卒中
            // 
            this.col脑卒中.Caption = "脑卒中";
            this.col脑卒中.FieldName = "脑卒中";
            this.col脑卒中.Name = "col脑卒中";
            this.col脑卒中.OptionsColumn.FixedWidth = true;
            this.col脑卒中.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col脑卒中.Visible = true;
            // 
            // colprgid
            // 
            this.colprgid.Caption = "所属机构编号";
            this.colprgid.FieldName = "prgid";
            this.colprgid.Name = "colprgid";
            this.colprgid.OptionsColumn.FixedWidth = true;
            this.colprgid.Visible = true;
            // 
            // link区域名称
            // 
            this.link区域名称.AutoHeight = false;
            this.link区域名称.Name = "link区域名称";
            this.link区域名称.Click += new System.EventHandler(this.link区域名称_Click);
            // 
            // link总数
            // 
            this.link总数.AutoHeight = false;
            this.link总数.Name = "link总数";
            this.link总数.Click += new System.EventHandler(this.link总数_Click);
            // 
            // link合格数
            // 
            this.link合格数.AutoHeight = false;
            this.link合格数.Name = "link合格数";
            this.link合格数.Click += new System.EventHandler(this.link合格数_Click);
            // 
            // link不合格数
            // 
            this.link不合格数.AutoHeight = false;
            this.link不合格数.Name = "link不合格数";
            this.link不合格数.Click += new System.EventHandler(this.link不合格数_Click);
            // 
            // gvSummary
            // 
            this.gvSummary.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gvSummary.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gvSummary.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gvSummary.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.DetailTip.Options.UseFont = true;
            this.gvSummary.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Empty.Options.UseFont = true;
            this.gvSummary.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.EvenRow.Options.UseFont = true;
            this.gvSummary.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gvSummary.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FilterPanel.Options.UseFont = true;
            this.gvSummary.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FixedLine.Options.UseFont = true;
            this.gvSummary.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FocusedCell.Options.UseFont = true;
            this.gvSummary.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FocusedRow.Options.UseFont = true;
            this.gvSummary.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gvSummary.Appearance.FooterPanel.Options.UseFont = true;
            this.gvSummary.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupButton.Options.UseFont = true;
            this.gvSummary.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupFooter.Options.UseFont = true;
            this.gvSummary.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupPanel.Options.UseFont = true;
            this.gvSummary.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupRow.Options.UseFont = true;
            this.gvSummary.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvSummary.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gvSummary.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HorzLine.Options.UseFont = true;
            this.gvSummary.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.OddRow.Options.UseFont = true;
            this.gvSummary.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Preview.Options.UseFont = true;
            this.gvSummary.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Row.Options.UseFont = true;
            this.gvSummary.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.RowSeparator.Options.UseFont = true;
            this.gvSummary.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.SelectedRow.Options.UseFont = true;
            this.gvSummary.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.TopNewRow.Options.UseFont = true;
            this.gvSummary.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.VertLine.Options.UseFont = true;
            this.gvSummary.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ViewCaption.Options.UseFont = true;
            this.gvSummary.ColumnPanelRowHeight = 30;
            this.gvSummary.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn19});
            this.gvSummary.GridControl = this.gcSummary;
            this.gvSummary.GroupPanelText = "DragColumn";
            this.gvSummary.Name = "gvSummary";
            this.gvSummary.OptionsView.EnableAppearanceEvenRow = true;
            this.gvSummary.OptionsView.EnableAppearanceOddRow = true;
            this.gvSummary.OptionsView.ShowFooter = true;
            this.gvSummary.OptionsView.ShowGroupPanel = false;
            this.gvSummary.CustomDrawFooterCell += new DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventHandler(this.gvSummary_CustomDrawFooterCell);
            this.gvSummary.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gvSummary_CustomRowCellEdit);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "区域编码";
            this.gridColumn1.FieldName = "区域编码";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "区域编码", "合计")});
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "区域名称";
            this.gridColumn2.ColumnEdit = this.link区域名称;
            this.gridColumn2.FieldName = "区域名称";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "总数";
            this.gridColumn6.FieldName = "总数";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "总数", "{0:n0}")});
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 2;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.Caption = "合格数";
            this.gridColumn7.FieldName = "合格数";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "合格数", "{0:n0}")});
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 3;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.Caption = "不合格数";
            this.gridColumn8.FieldName = "不合格数";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "不合格数", "{0:n0}")});
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 4;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.Caption = "合格率";
            this.gridColumn9.DisplayFormat.FormatString = "{0:n2}%";
            this.gridColumn9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn9.FieldName = "合格率";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Average, "合格率", "{0:n2}%")});
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 5;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.Caption = "平均完整度";
            this.gridColumn10.DisplayFormat.FormatString = "{0:n2}%";
            this.gridColumn10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn10.FieldName = "平均完整度";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Average, "平均完整度", "{0:n2}%")});
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 6;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "转入档案数";
            this.gridColumn11.FieldName = "转入档案数";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "转入档案数", "{0:n0}")});
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 7;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.Caption = "转出档案数";
            this.gridColumn12.FieldName = "转出档案数";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "转出档案数", "{0:n0}")});
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 8;
            // 
            // gridColumn19
            // 
            this.gridColumn19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn19.Caption = "非活动档案数";
            this.gridColumn19.FieldName = "非活动档案数";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "非活动档案数", "{0:n0}")});
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 9;
            // 
            // gc档案
            // 
            this.gc档案.AllowBandedGridColumnSort = false;
            this.gc档案.IsBestFitColumns = true;
            this.gc档案.Location = new System.Drawing.Point(535, 132);
            this.gc档案.MainView = this.gv档案;
            this.gc档案.Name = "gc档案";
            this.gc档案.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.link档案号});
            this.gc档案.ShowContextMenu = false;
            this.gc档案.Size = new System.Drawing.Size(404, 268);
            this.gc档案.StrWhere = "";
            this.gc档案.TabIndex = 7;
            this.gc档案.UseCheckBox = false;
            this.gc档案.View = "";
            this.gc档案.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv档案});
            // 
            // gv档案
            // 
            this.gv档案.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gv档案.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gv档案.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gv档案.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.DetailTip.Options.UseFont = true;
            this.gv档案.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.Empty.Options.UseFont = true;
            this.gv档案.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.EvenRow.Options.UseFont = true;
            this.gv档案.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gv档案.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.FilterPanel.Options.UseFont = true;
            this.gv档案.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.FixedLine.Options.UseFont = true;
            this.gv档案.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.FocusedCell.Options.UseFont = true;
            this.gv档案.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.FocusedRow.Options.UseFont = true;
            this.gv档案.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gv档案.Appearance.FooterPanel.Options.UseFont = true;
            this.gv档案.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.GroupButton.Options.UseFont = true;
            this.gv档案.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.GroupFooter.Options.UseFont = true;
            this.gv档案.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.GroupPanel.Options.UseFont = true;
            this.gv档案.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.GroupRow.Options.UseFont = true;
            this.gv档案.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.HeaderPanel.Options.UseFont = true;
            this.gv档案.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gv档案.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.HorzLine.Options.UseFont = true;
            this.gv档案.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.OddRow.Options.UseFont = true;
            this.gv档案.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.Preview.Options.UseFont = true;
            this.gv档案.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.Row.Options.UseFont = true;
            this.gv档案.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.RowSeparator.Options.UseFont = true;
            this.gv档案.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.SelectedRow.Options.UseFont = true;
            this.gv档案.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.TopNewRow.Options.UseFont = true;
            this.gv档案.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.VertLine.Options.UseFont = true;
            this.gv档案.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.ViewCaption.Options.UseFont = true;
            this.gv档案.ColumnPanelRowHeight = 30;
            this.gv档案.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29});
            this.gv档案.GridControl = this.gc档案;
            this.gv档案.GroupPanelText = "DragColumn";
            this.gv档案.Name = "gv档案";
            this.gv档案.OptionsView.ColumnAutoWidth = false;
            this.gv档案.OptionsView.EnableAppearanceEvenRow = true;
            this.gv档案.OptionsView.EnableAppearanceOddRow = true;
            this.gv档案.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn20
            // 
            this.gridColumn20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn20.Caption = "档案号";
            this.gridColumn20.ColumnEdit = this.link档案号;
            this.gridColumn20.FieldName = "个人档案编号";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 0;
            // 
            // link档案号
            // 
            this.link档案号.AutoHeight = false;
            this.link档案号.Name = "link档案号";
            this.link档案号.Click += new System.EventHandler(this.link档案号_Click);
            // 
            // gridColumn21
            // 
            this.gridColumn21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn21.Caption = "姓名";
            this.gridColumn21.FieldName = "姓名";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 1;
            // 
            // gridColumn22
            // 
            this.gridColumn22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn22.Caption = "性别";
            this.gridColumn22.FieldName = "性别";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 2;
            // 
            // gridColumn23
            // 
            this.gridColumn23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn23.Caption = "出生日期";
            this.gridColumn23.FieldName = "出生日期";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 3;
            // 
            // gridColumn24
            // 
            this.gridColumn24.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn24.Caption = "身份证号";
            this.gridColumn24.FieldName = "身份证号";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 4;
            // 
            // gridColumn25
            // 
            this.gridColumn25.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn25.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn25.Caption = "联系电话";
            this.gridColumn25.FieldName = "联系人电话";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 5;
            // 
            // gridColumn26
            // 
            this.gridColumn26.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn26.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn26.Caption = "当前所属机构";
            this.gridColumn26.FieldName = "所属机构";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 6;
            this.gridColumn26.Width = 105;
            // 
            // gridColumn27
            // 
            this.gridColumn27.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn27.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn27.Caption = "录入人";
            this.gridColumn27.FieldName = "创建人";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 7;
            // 
            // gridColumn28
            // 
            this.gridColumn28.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn28.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn28.Caption = "录入时间";
            this.gridColumn28.FieldName = "创建时间";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 8;
            // 
            // gridColumn29
            // 
            this.gridColumn29.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn29.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn29.Caption = "家庭档案编号";
            this.gridColumn29.FieldName = "家庭档案编号";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 9;
            this.gridColumn29.Width = 102;
            // 
            // frm沂水县老年人体检月报表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(945, 432);
            this.Name = "frm沂水县老年人体检月报表";
            this.Text = "老年人体检月报表";
            this.Load += new System.EventHandler(this.老年人体检月报表_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt完整度.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte结束2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte结束2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte开始1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte开始1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link65以上老年人体检人数)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link区域名称)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link总数)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link合格数)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link不合格数)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link档案号)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Library.UserControls.DataGridControl gcSummary;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSummary;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btnEmpty;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraEditors.ComboBoxEdit cbo档案状态;
        private DevExpress.XtraEditors.TextEdit txt完整度;
        private DevExpress.XtraEditors.DateEdit dte结束2;
        private DevExpress.XtraEditors.DateEdit dte开始1;
        private DevExpress.XtraEditors.TreeListLookUpEdit txt机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link区域名称;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link总数;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link合格数;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link不合格数;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private Library.UserControls.DataGridControl gc档案;
        private DevExpress.XtraGrid.Views.Grid.GridView gv档案;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link档案号;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col居住地址;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col65岁及以上常住居民数;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col月末累计65岁及以上老年人体检人数;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col月末累计中医体质辨识人数;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col65岁到70岁;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col71岁到80岁;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col80岁以上;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col月末累计健康老年人数;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col生活不能自理老年人数;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col血压异常;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col体质指数;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col血常规异常;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col尿常规异常;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col血糖异常;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col心电图异常;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col肝功能异常;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col肾功能异常;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col血脂异常;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colB超异常;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col中医体质辨识异常;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col脑血管疾病;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col肾脏疾病;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col心脏疾病;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col血管疾病;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col眼部疾病;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col神经系统疾病;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col高血压;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col恶性肿瘤;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col慢性阻塞性肺病;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col现存糖尿病;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col其他;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col现存高血压;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col糖尿病;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col冠心病;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col脑卒中;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colprgid;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand16;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand17;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand18;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand19;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand20;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand21;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand22;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand23;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand24;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand25;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand26;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand27;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand28;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand29;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand30;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand31;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand32;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand33;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand34;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand35;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand36;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand37;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand38;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand39;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand40;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand41;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link65以上老年人体检人数;

    }
}