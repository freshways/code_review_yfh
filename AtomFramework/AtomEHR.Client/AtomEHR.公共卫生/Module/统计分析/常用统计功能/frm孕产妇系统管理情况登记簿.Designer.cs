﻿namespace AtomEHR.公共卫生.Module
{
    partial class frm孕产妇系统管理情况登记簿 

    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm孕产妇系统管理情况登记簿));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.dte随访日期2 = new DevExpress.XtraEditors.DateEdit();
            this.dte随访日期1 = new DevExpress.XtraEditors.DateEdit();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.txt孕周End = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txt孕周Begin = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txt档案编号 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.btnEmpty = new DevExpress.XtraEditors.SimpleButton();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.dte结束2 = new DevExpress.XtraEditors.DateEdit();
            this.dte开始1 = new DevExpress.XtraEditors.DateEdit();
            this.txt机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gcSummary = new AtomEHR.Library.UserControls.DataGridControl();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand64 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col个人档案编号 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col孕产妇姓名 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col户口所在地 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col住址 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col年龄 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col孕次 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand16 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col产次 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand17 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col末次月经 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand18 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col预产期 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand19 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col建册时间 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col8周 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand20 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col12周 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand21 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col16周 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand22 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col20周 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand23 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col24周 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand24 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col28周 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand25 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col32周 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand26 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col34周 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand27 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col38周 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand28 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col39周 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand29 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col总次数 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand30 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col高危因素 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand31 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col接受高危管理 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand32 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand33 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand36 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col临产前治愈 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand37 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col临产前未治愈 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand34 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand38 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col产时治愈 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand39 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col产时未治愈 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand35 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand40 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col产褥期治愈 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand41 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col产褥期未治愈 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand47 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand42 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col分娩时间 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand43 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col孕周 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand44 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col分娩地点 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand45 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col分娩方式 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand46 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col非住院分娩新法接生 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand48 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col活产 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand49 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col死胎 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand50 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col死产 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand51 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col新生儿死亡 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand52 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col产后访视次数 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand53 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col产后42到56天检查 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand54 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand55 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col性别 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand56 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col出生身高 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand57 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col出生体重 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand58 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col阿氏评分1分钟 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand59 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col访视次数 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand60 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col患破伤风 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand61 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand62 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand63 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gc档案 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv档案 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.link档案号 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访日期2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访日期2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访日期1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访日期1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt孕周End.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt孕周Begin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte结束2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte结束2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte开始1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte开始1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link档案号)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gcSummary);
            this.tpSummary.Controls.Add(this.groupControl1);
            this.tpSummary.Controls.Add(this.gc档案);
            this.tpSummary.Size = new System.Drawing.Size(1044, 434);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Size = new System.Drawing.Size(1050, 440);
            // 
            // tcBusiness
            // 
            this.tcBusiness.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            this.tcBusiness.Size = new System.Drawing.Size(1050, 440);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(776, 507);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(1050, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(872, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(675, 2);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.layoutControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1044, 156);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "查询信息";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.dte随访日期2);
            this.layoutControl1.Controls.Add(this.dte随访日期1);
            this.layoutControl1.Controls.Add(this.panelControl3);
            this.layoutControl1.Controls.Add(this.txt档案编号);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.btnEmpty);
            this.layoutControl1.Controls.Add(this.btnQuery);
            this.layoutControl1.Controls.Add(this.dte结束2);
            this.layoutControl1.Controls.Add(this.dte开始1);
            this.layoutControl1.Controls.Add(this.txt机构);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 22);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(431, 278, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1040, 132);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // dte随访日期2
            // 
            this.dte随访日期2.EditValue = null;
            this.dte随访日期2.Location = new System.Drawing.Point(245, 52);
            this.dte随访日期2.Name = "dte随访日期2";
            this.dte随访日期2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte随访日期2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte随访日期2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte随访日期2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte随访日期2.Size = new System.Drawing.Size(140, 20);
            this.dte随访日期2.StyleController = this.layoutControl1;
            this.dte随访日期2.TabIndex = 132;
            // 
            // dte随访日期1
            // 
            this.dte随访日期1.EditValue = null;
            this.dte随访日期1.Location = new System.Drawing.Point(93, 52);
            this.dte随访日期1.Name = "dte随访日期1";
            this.dte随访日期1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte随访日期1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte随访日期1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte随访日期1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte随访日期1.Size = new System.Drawing.Size(131, 20);
            this.dte随访日期1.StyleController = this.layoutControl1;
            this.dte随访日期1.TabIndex = 131;
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.txt孕周End);
            this.panelControl3.Controls.Add(this.labelControl3);
            this.panelControl3.Controls.Add(this.labelControl2);
            this.panelControl3.Controls.Add(this.txt孕周Begin);
            this.panelControl3.Controls.Add(this.labelControl1);
            this.panelControl3.Location = new System.Drawing.Point(129, 76);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(907, 26);
            this.panelControl3.TabIndex = 130;
            // 
            // txt孕周End
            // 
            this.txt孕周End.Location = new System.Drawing.Point(164, 1);
            this.txt孕周End.Name = "txt孕周End";
            this.txt孕周End.Properties.Mask.EditMask = "\\d{0,2}";
            this.txt孕周End.Properties.Mask.IgnoreMaskBlank = false;
            this.txt孕周End.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txt孕周End.Properties.Mask.ShowPlaceHolders = false;
            this.txt孕周End.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt孕周End.Size = new System.Drawing.Size(50, 20);
            this.txt孕周End.StyleController = this.layoutControl1;
            this.txt孕周End.TabIndex = 129;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(220, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(12, 14);
            this.labelControl3.TabIndex = 128;
            this.labelControl3.Text = "周";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(112, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(44, 14);
            this.labelControl2.TabIndex = 128;
            this.labelControl2.Text = "周  小于";
            // 
            // txt孕周Begin
            // 
            this.txt孕周Begin.Location = new System.Drawing.Point(56, 1);
            this.txt孕周Begin.Name = "txt孕周Begin";
            this.txt孕周Begin.Properties.Mask.EditMask = "\\d{0,2}";
            this.txt孕周Begin.Properties.Mask.IgnoreMaskBlank = false;
            this.txt孕周Begin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txt孕周Begin.Properties.Mask.ShowPlaceHolders = false;
            this.txt孕周Begin.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt孕周Begin.Size = new System.Drawing.Size(50, 20);
            this.txt孕周Begin.StyleController = this.layoutControl1;
            this.txt孕周Begin.TabIndex = 127;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(5, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(48, 14);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "大于等于";
            // 
            // txt档案编号
            // 
            this.txt档案编号.Location = new System.Drawing.Point(454, 52);
            this.txt档案编号.Name = "txt档案编号";
            this.txt档案编号.Size = new System.Drawing.Size(206, 20);
            this.txt档案编号.StyleController = this.layoutControl1;
            this.txt档案编号.TabIndex = 124;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(454, 28);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Size = new System.Drawing.Size(206, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 124;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(697, 28);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(123, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 124;
            // 
            // btnEmpty
            // 
            this.btnEmpty.Image = ((System.Drawing.Image)(resources.GetObject("btnEmpty.Image")));
            this.btnEmpty.Location = new System.Drawing.Point(421, 106);
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.Size = new System.Drawing.Size(60, 22);
            this.btnEmpty.StyleController = this.layoutControl1;
            this.btnEmpty.TabIndex = 10;
            this.btnEmpty.Text = "后退";
            this.btnEmpty.Visible = false;
            this.btnEmpty.Click += new System.EventHandler(this.btnEmpty_Click);
            // 
            // btnQuery
            // 
            this.btnQuery.Image = ((System.Drawing.Image)(resources.GetObject("btnQuery.Image")));
            this.btnQuery.Location = new System.Drawing.Point(356, 106);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(61, 22);
            this.btnQuery.StyleController = this.layoutControl1;
            this.btnQuery.TabIndex = 6;
            this.btnQuery.Text = "查询";
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // dte结束2
            // 
            this.dte结束2.EditValue = null;
            this.dte结束2.Location = new System.Drawing.Point(245, 28);
            this.dte结束2.Margin = new System.Windows.Forms.Padding(0);
            this.dte结束2.Name = "dte结束2";
            this.dte结束2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte结束2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte结束2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte结束2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte结束2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte结束2.Size = new System.Drawing.Size(140, 20);
            this.dte结束2.StyleController = this.layoutControl1;
            this.dte结束2.TabIndex = 3;
            // 
            // dte开始1
            // 
            this.dte开始1.EditValue = null;
            this.dte开始1.Location = new System.Drawing.Point(93, 28);
            this.dte开始1.Margin = new System.Windows.Forms.Padding(0);
            this.dte开始1.Name = "dte开始1";
            this.dte开始1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte开始1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte开始1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte开始1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte开始1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte开始1.Size = new System.Drawing.Size(131, 20);
            this.dte开始1.StyleController = this.layoutControl1;
            this.dte开始1.TabIndex = 1;
            // 
            // txt机构
            // 
            this.txt机构.Location = new System.Drawing.Point(93, 4);
            this.txt机构.Name = "txt机构";
            this.txt机构.Properties.AutoExpandAllNodes = false;
            this.txt机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt机构.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.txt机构.Size = new System.Drawing.Size(292, 20);
            this.txt机构.StyleController = this.layoutControl1;
            this.txt机构.TabIndex = 35;
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(212, 146);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.emptySpaceItem1,
            this.emptySpaceItem3,
            this.emptySpaceItem2,
            this.emptySpaceItem4,
            this.layoutControlItem11,
            this.layoutControlItem9,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem10,
            this.layoutControlItem8});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1040, 132);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txt机构;
            this.layoutControlItem1.CustomizationFormText = "机构:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(385, 24);
            this.layoutControlItem1.Text = "机构:";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(84, 14);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dte开始1;
            this.layoutControlItem2.CustomizationFormText = "统计周期:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(224, 24);
            this.layoutControlItem2.Text = "分娩日期:";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(84, 14);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.dte结束2;
            this.layoutControlItem3.CustomizationFormText = "至";
            this.layoutControlItem3.Location = new System.Drawing.Point(224, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(161, 24);
            this.layoutControlItem3.Text = "至";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(12, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnQuery;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(352, 102);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(65, 26);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.btnEmpty;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(417, 102);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(64, 26);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(385, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(651, 24);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(481, 102);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(555, 26);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(820, 24);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(216, 48);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 102);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(352, 26);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.panelControl3;
            this.layoutControlItem11.CustomizationFormText = "早孕建档时孕周范围：";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(229, 30);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(1036, 30);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "早孕建档时孕周范围：";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(120, 14);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txt身份证号;
            this.layoutControlItem9.CustomizationFormText = "身份证号：";
            this.layoutControlItem9.Location = new System.Drawing.Point(385, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(275, 24);
            this.layoutControlItem9.Text = "身份证号：";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.dte随访日期1;
            this.layoutControlItem4.CustomizationFormText = "产后随访日期：";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(224, 24);
            this.layoutControlItem4.Text = "产后随访日期：";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(84, 20);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.dte随访日期2;
            this.layoutControlItem5.CustomizationFormText = "至";
            this.layoutControlItem5.Location = new System.Drawing.Point(224, 48);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(161, 24);
            this.layoutControlItem5.Text = "至";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(12, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txt档案编号;
            this.layoutControlItem10.CustomizationFormText = "档案编号：";
            this.layoutControlItem10.Location = new System.Drawing.Point(385, 48);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(275, 24);
            this.layoutControlItem10.Text = "档案编号：";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txt姓名;
            this.layoutControlItem8.CustomizationFormText = "姓名:";
            this.layoutControlItem8.Location = new System.Drawing.Point(660, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(160, 48);
            this.layoutControlItem8.Text = "姓名:";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(28, 14);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // gcSummary
            // 
            this.gcSummary.AllowBandedGridColumnSort = false;
            this.gcSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSummary.IsBestFitColumns = true;
            gridLevelNode1.RelationName = "Level1";
            this.gcSummary.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gcSummary.Location = new System.Drawing.Point(0, 156);
            this.gcSummary.MainView = this.bandedGridView1;
            this.gcSummary.Name = "gcSummary";
            this.gcSummary.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.gcSummary.ShowContextMenu = false;
            this.gcSummary.Size = new System.Drawing.Size(1044, 278);
            this.gcSummary.StrWhere = "";
            this.gcSummary.TabIndex = 6;
            this.gcSummary.UseCheckBox = false;
            this.gcSummary.View = "";
            this.gcSummary.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1});
            this.gcSummary.Click += new System.EventHandler(this.gcSummary_Click);
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Appearance.BandPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.BandPanelBackground.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.bandedGridView1.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.bandedGridView1.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.DetailTip.Options.UseFont = true;
            this.bandedGridView1.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.Empty.Options.UseFont = true;
            this.bandedGridView1.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.EvenRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FilterCloseButton.Options.UseFont = true;
            this.bandedGridView1.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FilterPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FixedLine.Options.UseFont = true;
            this.bandedGridView1.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FocusedCell.Options.UseFont = true;
            this.bandedGridView1.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FocusedRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.bandedGridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.GroupButton.Options.UseFont = true;
            this.bandedGridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.bandedGridView1.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.GroupRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.bandedGridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridView1.Appearance.HeaderPanelBackground.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.HideSelectionRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.HorzLine.Options.UseFont = true;
            this.bandedGridView1.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.OddRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.Preview.Options.UseFont = true;
            this.bandedGridView1.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.Row.Options.UseFont = true;
            this.bandedGridView1.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.RowSeparator.Options.UseFont = true;
            this.bandedGridView1.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.SelectedRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.TopNewRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.VertLine.Options.UseFont = true;
            this.bandedGridView1.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.ViewCaption.Options.UseFont = true;
            this.bandedGridView1.BandPanelRowHeight = 30;
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand8});
            this.bandedGridView1.ColumnPanelRowHeight = 30;
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.col个人档案编号,
            this.col孕产妇姓名,
            this.col户口所在地,
            this.col住址,
            this.col年龄,
            this.col孕次,
            this.col产次,
            this.col末次月经,
            this.col预产期,
            this.col建册时间,
            this.col8周,
            this.col12周,
            this.col16周,
            this.col20周,
            this.col24周,
            this.col28周,
            this.col32周,
            this.col34周,
            this.col38周,
            this.col39周,
            this.col总次数,
            this.col高危因素,
            this.col接受高危管理,
            this.col临产前治愈,
            this.col临产前未治愈,
            this.col产时治愈,
            this.col产时未治愈,
            this.col产褥期治愈,
            this.col产褥期未治愈,
            this.col分娩时间,
            this.col孕周,
            this.col分娩地点,
            this.col分娩方式,
            this.col非住院分娩新法接生,
            this.col活产,
            this.col死胎,
            this.col死产,
            this.col新生儿死亡,
            this.col产后访视次数,
            this.col产后42到56天检查,
            this.col性别,
            this.col出生身高,
            this.col出生体重,
            this.col阿氏评分1分钟,
            this.col访视次数,
            this.col患破伤风});
            this.bandedGridView1.GridControl = this.gcSummary;
            this.bandedGridView1.GroupPanelText = "DragColumn";
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsBehavior.ReadOnly = true;
            this.bandedGridView1.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.bandedGridView1.OptionsView.EnableAppearanceOddRow = true;
            this.bandedGridView1.OptionsView.ShowColumnHeaders = false;
            this.bandedGridView1.OptionsView.ShowFooter = true;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand8
            // 
            this.gridBand8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.gridBand8.AppearanceHeader.Options.UseFont = true;
            this.gridBand8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand8.Caption = "孕  产  妇  系  统  管  理  情  况  登  记  簿";
            this.gridBand8.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand64,
            this.gridBand9,
            this.gridBand10,
            this.gridBand13,
            this.gridBand14,
            this.gridBand15,
            this.gridBand16,
            this.gridBand17,
            this.gridBand18,
            this.gridBand19,
            this.gridBand11,
            this.gridBand30,
            this.gridBand31,
            this.gridBand32,
            this.gridBand47,
            this.gridBand52,
            this.gridBand53,
            this.gridBand54});
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = 0;
            this.gridBand8.Width = 3551;
            // 
            // gridBand64
            // 
            this.gridBand64.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand64.AppearanceHeader.Options.UseFont = true;
            this.gridBand64.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand64.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand64.Caption = "个人档案编号";
            this.gridBand64.Columns.Add(this.col个人档案编号);
            this.gridBand64.Name = "gridBand64";
            this.gridBand64.VisibleIndex = 0;
            this.gridBand64.Width = 123;
            // 
            // col个人档案编号
            // 
            this.col个人档案编号.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col个人档案编号.AppearanceHeader.Options.UseFont = true;
            this.col个人档案编号.Caption = "个人档案编号";
            this.col个人档案编号.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.col个人档案编号.FieldName = "个人档案编号";
            this.col个人档案编号.Name = "col个人档案编号";
            this.col个人档案编号.OptionsColumn.ReadOnly = true;
            this.col个人档案编号.Visible = true;
            this.col个人档案编号.Width = 123;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit1_Click);
            // 
            // gridBand9
            // 
            this.gridBand9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand9.AppearanceHeader.Options.UseFont = true;
            this.gridBand9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand9.Caption = "孕产妇姓名";
            this.gridBand9.Columns.Add(this.col孕产妇姓名);
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.VisibleIndex = 1;
            this.gridBand9.Width = 76;
            // 
            // col孕产妇姓名
            // 
            this.col孕产妇姓名.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col孕产妇姓名.AppearanceHeader.Options.UseFont = true;
            this.col孕产妇姓名.Caption = "孕产妇姓名";
            this.col孕产妇姓名.FieldName = "孕产妇姓名";
            this.col孕产妇姓名.Name = "col孕产妇姓名";
            this.col孕产妇姓名.OptionsColumn.AllowEdit = false;
            this.col孕产妇姓名.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "孕产妇姓名", "合计：{0}")});
            this.col孕产妇姓名.Visible = true;
            this.col孕产妇姓名.Width = 76;
            // 
            // gridBand10
            // 
            this.gridBand10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand10.AppearanceHeader.Options.UseFont = true;
            this.gridBand10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand10.Caption = "户口所在地";
            this.gridBand10.Columns.Add(this.col户口所在地);
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.VisibleIndex = 2;
            this.gridBand10.Width = 76;
            // 
            // col户口所在地
            // 
            this.col户口所在地.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col户口所在地.AppearanceHeader.Options.UseFont = true;
            this.col户口所在地.Caption = "户口所在地";
            this.col户口所在地.FieldName = "户口所在地";
            this.col户口所在地.Name = "col户口所在地";
            this.col户口所在地.OptionsColumn.AllowEdit = false;
            this.col户口所在地.Visible = true;
            this.col户口所在地.Width = 76;
            // 
            // gridBand13
            // 
            this.gridBand13.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand13.AppearanceHeader.Options.UseFont = true;
            this.gridBand13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand13.Caption = "住址";
            this.gridBand13.Columns.Add(this.col住址);
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.VisibleIndex = 3;
            this.gridBand13.Width = 134;
            // 
            // col住址
            // 
            this.col住址.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col住址.AppearanceHeader.Options.UseFont = true;
            this.col住址.AppearanceHeader.Options.UseTextOptions = true;
            this.col住址.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col住址.Caption = "住址";
            this.col住址.FieldName = "住址";
            this.col住址.Name = "col住址";
            this.col住址.OptionsColumn.AllowEdit = false;
            this.col住址.OptionsColumn.ReadOnly = true;
            this.col住址.Visible = true;
            this.col住址.Width = 134;
            // 
            // gridBand14
            // 
            this.gridBand14.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand14.AppearanceHeader.Options.UseFont = true;
            this.gridBand14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand14.Caption = "年龄";
            this.gridBand14.Columns.Add(this.col年龄);
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.VisibleIndex = 4;
            this.gridBand14.Width = 59;
            // 
            // col年龄
            // 
            this.col年龄.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col年龄.AppearanceHeader.Options.UseFont = true;
            this.col年龄.AppearanceHeader.Options.UseTextOptions = true;
            this.col年龄.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col年龄.Caption = "年龄";
            this.col年龄.FieldName = "年龄";
            this.col年龄.Name = "col年龄";
            this.col年龄.OptionsColumn.AllowEdit = false;
            this.col年龄.OptionsColumn.ReadOnly = true;
            this.col年龄.Visible = true;
            this.col年龄.Width = 59;
            // 
            // gridBand15
            // 
            this.gridBand15.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand15.AppearanceHeader.Options.UseFont = true;
            this.gridBand15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand15.Caption = "孕次";
            this.gridBand15.Columns.Add(this.col孕次);
            this.gridBand15.Name = "gridBand15";
            this.gridBand15.VisibleIndex = 5;
            this.gridBand15.Width = 43;
            // 
            // col孕次
            // 
            this.col孕次.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col孕次.AppearanceHeader.Options.UseFont = true;
            this.col孕次.AppearanceHeader.Options.UseTextOptions = true;
            this.col孕次.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col孕次.Caption = "孕次";
            this.col孕次.FieldName = "孕次";
            this.col孕次.Name = "col孕次";
            this.col孕次.OptionsColumn.AllowEdit = false;
            this.col孕次.OptionsColumn.ReadOnly = true;
            this.col孕次.Visible = true;
            this.col孕次.Width = 43;
            // 
            // gridBand16
            // 
            this.gridBand16.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand16.AppearanceHeader.Options.UseFont = true;
            this.gridBand16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand16.Caption = "产次";
            this.gridBand16.Columns.Add(this.col产次);
            this.gridBand16.Name = "gridBand16";
            this.gridBand16.VisibleIndex = 6;
            this.gridBand16.Width = 45;
            // 
            // col产次
            // 
            this.col产次.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col产次.AppearanceHeader.Options.UseFont = true;
            this.col产次.AppearanceHeader.Options.UseTextOptions = true;
            this.col产次.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col产次.Caption = "产次";
            this.col产次.FieldName = "产次";
            this.col产次.Name = "col产次";
            this.col产次.OptionsColumn.AllowEdit = false;
            this.col产次.OptionsColumn.ReadOnly = true;
            this.col产次.Visible = true;
            this.col产次.Width = 45;
            // 
            // gridBand17
            // 
            this.gridBand17.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand17.AppearanceHeader.Options.UseFont = true;
            this.gridBand17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand17.Caption = "末次月经";
            this.gridBand17.Columns.Add(this.col末次月经);
            this.gridBand17.Name = "gridBand17";
            this.gridBand17.VisibleIndex = 7;
            this.gridBand17.Width = 68;
            // 
            // col末次月经
            // 
            this.col末次月经.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col末次月经.AppearanceHeader.Options.UseFont = true;
            this.col末次月经.AppearanceHeader.Options.UseTextOptions = true;
            this.col末次月经.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col末次月经.Caption = "末次月经";
            this.col末次月经.FieldName = "末次月经";
            this.col末次月经.Name = "col末次月经";
            this.col末次月经.OptionsColumn.AllowEdit = false;
            this.col末次月经.OptionsColumn.ReadOnly = true;
            this.col末次月经.Visible = true;
            this.col末次月经.Width = 68;
            // 
            // gridBand18
            // 
            this.gridBand18.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand18.AppearanceHeader.Options.UseFont = true;
            this.gridBand18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand18.Caption = "预产期";
            this.gridBand18.Columns.Add(this.col预产期);
            this.gridBand18.Name = "gridBand18";
            this.gridBand18.VisibleIndex = 8;
            this.gridBand18.Width = 90;
            // 
            // col预产期
            // 
            this.col预产期.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col预产期.AppearanceHeader.Options.UseFont = true;
            this.col预产期.AppearanceHeader.Options.UseTextOptions = true;
            this.col预产期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col预产期.Caption = "预产期";
            this.col预产期.FieldName = "预产期";
            this.col预产期.Name = "col预产期";
            this.col预产期.OptionsColumn.AllowEdit = false;
            this.col预产期.OptionsColumn.ReadOnly = true;
            this.col预产期.Visible = true;
            this.col预产期.Width = 90;
            // 
            // gridBand19
            // 
            this.gridBand19.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand19.AppearanceHeader.Options.UseFont = true;
            this.gridBand19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand19.Caption = "建册时间";
            this.gridBand19.Columns.Add(this.col建册时间);
            this.gridBand19.Name = "gridBand19";
            this.gridBand19.VisibleIndex = 9;
            this.gridBand19.Width = 89;
            // 
            // col建册时间
            // 
            this.col建册时间.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col建册时间.AppearanceHeader.Options.UseFont = true;
            this.col建册时间.AppearanceHeader.Options.UseTextOptions = true;
            this.col建册时间.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col建册时间.Caption = "建册时间";
            this.col建册时间.FieldName = "建册时间";
            this.col建册时间.Name = "col建册时间";
            this.col建册时间.OptionsColumn.AllowEdit = false;
            this.col建册时间.OptionsColumn.ReadOnly = true;
            this.col建册时间.Visible = true;
            this.col建册时间.Width = 89;
            // 
            // gridBand11
            // 
            this.gridBand11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand11.AppearanceHeader.Options.UseFont = true;
            this.gridBand11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand11.Caption = "产前检查情况";
            this.gridBand11.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand12,
            this.gridBand20,
            this.gridBand21,
            this.gridBand22,
            this.gridBand23,
            this.gridBand24,
            this.gridBand25,
            this.gridBand26,
            this.gridBand27,
            this.gridBand28,
            this.gridBand29});
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.VisibleIndex = 10;
            this.gridBand11.Width = 770;
            // 
            // gridBand12
            // 
            this.gridBand12.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand12.AppearanceHeader.Options.UseFont = true;
            this.gridBand12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand12.Caption = "8周~";
            this.gridBand12.Columns.Add(this.col8周);
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.VisibleIndex = 0;
            // 
            // col8周
            // 
            this.col8周.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col8周.AppearanceHeader.Options.UseFont = true;
            this.col8周.AppearanceHeader.Options.UseTextOptions = true;
            this.col8周.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col8周.Caption = "8周~";
            this.col8周.FieldName = "8周";
            this.col8周.Name = "col8周";
            this.col8周.OptionsColumn.AllowEdit = false;
            this.col8周.OptionsColumn.ReadOnly = true;
            this.col8周.Visible = true;
            this.col8周.Width = 70;
            // 
            // gridBand20
            // 
            this.gridBand20.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand20.AppearanceHeader.Options.UseFont = true;
            this.gridBand20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand20.Caption = "12周~";
            this.gridBand20.Columns.Add(this.col12周);
            this.gridBand20.Name = "gridBand20";
            this.gridBand20.VisibleIndex = 1;
            // 
            // col12周
            // 
            this.col12周.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col12周.AppearanceHeader.Options.UseFont = true;
            this.col12周.AppearanceHeader.Options.UseTextOptions = true;
            this.col12周.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col12周.Caption = "12周~";
            this.col12周.FieldName = "12周";
            this.col12周.Name = "col12周";
            this.col12周.OptionsColumn.AllowEdit = false;
            this.col12周.OptionsColumn.ReadOnly = true;
            this.col12周.Visible = true;
            this.col12周.Width = 70;
            // 
            // gridBand21
            // 
            this.gridBand21.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand21.AppearanceHeader.Options.UseFont = true;
            this.gridBand21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand21.Caption = "16周~";
            this.gridBand21.Columns.Add(this.col16周);
            this.gridBand21.Name = "gridBand21";
            this.gridBand21.VisibleIndex = 2;
            // 
            // col16周
            // 
            this.col16周.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col16周.AppearanceHeader.Options.UseFont = true;
            this.col16周.AppearanceHeader.Options.UseTextOptions = true;
            this.col16周.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col16周.Caption = "16周~";
            this.col16周.FieldName = "16周";
            this.col16周.Name = "col16周";
            this.col16周.OptionsColumn.AllowEdit = false;
            this.col16周.OptionsColumn.ReadOnly = true;
            this.col16周.Visible = true;
            this.col16周.Width = 70;
            // 
            // gridBand22
            // 
            this.gridBand22.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand22.AppearanceHeader.Options.UseFont = true;
            this.gridBand22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand22.Caption = "20周~";
            this.gridBand22.Columns.Add(this.col20周);
            this.gridBand22.Name = "gridBand22";
            this.gridBand22.VisibleIndex = 3;
            // 
            // col20周
            // 
            this.col20周.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col20周.AppearanceHeader.Options.UseFont = true;
            this.col20周.AppearanceHeader.Options.UseTextOptions = true;
            this.col20周.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col20周.Caption = "20周~";
            this.col20周.FieldName = "20周";
            this.col20周.Name = "col20周";
            this.col20周.OptionsColumn.AllowEdit = false;
            this.col20周.Visible = true;
            this.col20周.Width = 70;
            // 
            // gridBand23
            // 
            this.gridBand23.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand23.AppearanceHeader.Options.UseFont = true;
            this.gridBand23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand23.Caption = "24周~";
            this.gridBand23.Columns.Add(this.col24周);
            this.gridBand23.Name = "gridBand23";
            this.gridBand23.VisibleIndex = 4;
            // 
            // col24周
            // 
            this.col24周.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col24周.AppearanceHeader.Options.UseFont = true;
            this.col24周.AppearanceHeader.Options.UseTextOptions = true;
            this.col24周.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col24周.Caption = "24周~";
            this.col24周.FieldName = "24周";
            this.col24周.Name = "col24周";
            this.col24周.OptionsColumn.AllowEdit = false;
            this.col24周.Visible = true;
            this.col24周.Width = 70;
            // 
            // gridBand24
            // 
            this.gridBand24.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand24.AppearanceHeader.Options.UseFont = true;
            this.gridBand24.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand24.Caption = "28周~";
            this.gridBand24.Columns.Add(this.col28周);
            this.gridBand24.Name = "gridBand24";
            this.gridBand24.VisibleIndex = 5;
            // 
            // col28周
            // 
            this.col28周.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col28周.AppearanceHeader.Options.UseFont = true;
            this.col28周.AppearanceHeader.Options.UseTextOptions = true;
            this.col28周.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col28周.Caption = "28周~";
            this.col28周.FieldName = "28周";
            this.col28周.Name = "col28周";
            this.col28周.OptionsColumn.AllowEdit = false;
            this.col28周.Visible = true;
            this.col28周.Width = 70;
            // 
            // gridBand25
            // 
            this.gridBand25.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand25.AppearanceHeader.Options.UseFont = true;
            this.gridBand25.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand25.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand25.Caption = "32周~";
            this.gridBand25.Columns.Add(this.col32周);
            this.gridBand25.Name = "gridBand25";
            this.gridBand25.VisibleIndex = 6;
            // 
            // col32周
            // 
            this.col32周.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col32周.AppearanceHeader.Options.UseFont = true;
            this.col32周.AppearanceHeader.Options.UseTextOptions = true;
            this.col32周.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col32周.Caption = "32周~";
            this.col32周.FieldName = "32周";
            this.col32周.Name = "col32周";
            this.col32周.OptionsColumn.AllowEdit = false;
            this.col32周.Visible = true;
            this.col32周.Width = 70;
            // 
            // gridBand26
            // 
            this.gridBand26.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand26.AppearanceHeader.Options.UseFont = true;
            this.gridBand26.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand26.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand26.Caption = "34周~";
            this.gridBand26.Columns.Add(this.col34周);
            this.gridBand26.Name = "gridBand26";
            this.gridBand26.VisibleIndex = 7;
            // 
            // col34周
            // 
            this.col34周.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col34周.AppearanceHeader.Options.UseFont = true;
            this.col34周.AppearanceHeader.Options.UseTextOptions = true;
            this.col34周.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col34周.Caption = "34周~";
            this.col34周.FieldName = "34周";
            this.col34周.Name = "col34周";
            this.col34周.OptionsColumn.AllowEdit = false;
            this.col34周.Visible = true;
            this.col34周.Width = 70;
            // 
            // gridBand27
            // 
            this.gridBand27.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand27.AppearanceHeader.Options.UseFont = true;
            this.gridBand27.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand27.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand27.Caption = "38周~";
            this.gridBand27.Columns.Add(this.col38周);
            this.gridBand27.Name = "gridBand27";
            this.gridBand27.VisibleIndex = 8;
            // 
            // col38周
            // 
            this.col38周.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col38周.AppearanceHeader.Options.UseFont = true;
            this.col38周.AppearanceHeader.Options.UseTextOptions = true;
            this.col38周.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col38周.Caption = "38周~";
            this.col38周.FieldName = "38周";
            this.col38周.Name = "col38周";
            this.col38周.OptionsColumn.AllowEdit = false;
            this.col38周.Visible = true;
            this.col38周.Width = 70;
            // 
            // gridBand28
            // 
            this.gridBand28.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand28.AppearanceHeader.Options.UseFont = true;
            this.gridBand28.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand28.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand28.Caption = "39周~";
            this.gridBand28.Columns.Add(this.col39周);
            this.gridBand28.Name = "gridBand28";
            this.gridBand28.VisibleIndex = 9;
            // 
            // col39周
            // 
            this.col39周.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col39周.AppearanceHeader.Options.UseFont = true;
            this.col39周.AppearanceHeader.Options.UseTextOptions = true;
            this.col39周.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col39周.Caption = "39周~";
            this.col39周.FieldName = "39周";
            this.col39周.Name = "col39周";
            this.col39周.OptionsColumn.AllowEdit = false;
            this.col39周.Visible = true;
            this.col39周.Width = 70;
            // 
            // gridBand29
            // 
            this.gridBand29.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand29.AppearanceHeader.Options.UseFont = true;
            this.gridBand29.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand29.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand29.Caption = "总次数";
            this.gridBand29.Columns.Add(this.col总次数);
            this.gridBand29.Name = "gridBand29";
            this.gridBand29.VisibleIndex = 10;
            // 
            // col总次数
            // 
            this.col总次数.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col总次数.AppearanceHeader.Options.UseFont = true;
            this.col总次数.AppearanceHeader.Options.UseTextOptions = true;
            this.col总次数.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col总次数.Caption = "总次数";
            this.col总次数.FieldName = "总次数";
            this.col总次数.Name = "col总次数";
            this.col总次数.OptionsColumn.AllowEdit = false;
            this.col总次数.Visible = true;
            this.col总次数.Width = 70;
            // 
            // gridBand30
            // 
            this.gridBand30.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand30.AppearanceHeader.Options.UseFont = true;
            this.gridBand30.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand30.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand30.Caption = "高危因素";
            this.gridBand30.Columns.Add(this.col高危因素);
            this.gridBand30.Name = "gridBand30";
            this.gridBand30.VisibleIndex = 11;
            this.gridBand30.Width = 75;
            // 
            // col高危因素
            // 
            this.col高危因素.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col高危因素.AppearanceHeader.Options.UseFont = true;
            this.col高危因素.AppearanceHeader.Options.UseTextOptions = true;
            this.col高危因素.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col高危因素.Caption = "高危因素";
            this.col高危因素.FieldName = "高危因素";
            this.col高危因素.Name = "col高危因素";
            this.col高危因素.OptionsColumn.AllowEdit = false;
            this.col高危因素.Visible = true;
            // 
            // gridBand31
            // 
            this.gridBand31.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand31.AppearanceHeader.Options.UseFont = true;
            this.gridBand31.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand31.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand31.Caption = "接受高危管理";
            this.gridBand31.Columns.Add(this.col接受高危管理);
            this.gridBand31.Name = "gridBand31";
            this.gridBand31.VisibleIndex = 12;
            this.gridBand31.Width = 98;
            // 
            // col接受高危管理
            // 
            this.col接受高危管理.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col接受高危管理.AppearanceHeader.Options.UseFont = true;
            this.col接受高危管理.Caption = "接受高危管理";
            this.col接受高危管理.FieldName = "接受高危管理";
            this.col接受高危管理.Name = "col接受高危管理";
            this.col接受高危管理.OptionsColumn.AllowEdit = false;
            this.col接受高危管理.Visible = true;
            this.col接受高危管理.Width = 98;
            // 
            // gridBand32
            // 
            this.gridBand32.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand32.AppearanceHeader.Options.UseFont = true;
            this.gridBand32.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand32.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand32.Caption = "高危妊娠";
            this.gridBand32.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand33,
            this.gridBand34,
            this.gridBand35});
            this.gridBand32.Name = "gridBand32";
            this.gridBand32.VisibleIndex = 13;
            this.gridBand32.Width = 420;
            // 
            // gridBand33
            // 
            this.gridBand33.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand33.AppearanceHeader.Options.UseFont = true;
            this.gridBand33.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand33.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand33.Caption = "临产前";
            this.gridBand33.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand36,
            this.gridBand37});
            this.gridBand33.Name = "gridBand33";
            this.gridBand33.VisibleIndex = 0;
            this.gridBand33.Width = 140;
            // 
            // gridBand36
            // 
            this.gridBand36.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand36.AppearanceHeader.Options.UseFont = true;
            this.gridBand36.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand36.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand36.Caption = "治愈";
            this.gridBand36.Columns.Add(this.col临产前治愈);
            this.gridBand36.Name = "gridBand36";
            this.gridBand36.VisibleIndex = 0;
            // 
            // col临产前治愈
            // 
            this.col临产前治愈.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col临产前治愈.AppearanceHeader.Options.UseFont = true;
            this.col临产前治愈.Caption = "治愈";
            this.col临产前治愈.FieldName = "治愈";
            this.col临产前治愈.Name = "col临产前治愈";
            this.col临产前治愈.OptionsColumn.AllowEdit = false;
            this.col临产前治愈.Visible = true;
            this.col临产前治愈.Width = 70;
            // 
            // gridBand37
            // 
            this.gridBand37.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand37.AppearanceHeader.Options.UseFont = true;
            this.gridBand37.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand37.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand37.Caption = "未治愈";
            this.gridBand37.Columns.Add(this.col临产前未治愈);
            this.gridBand37.Name = "gridBand37";
            this.gridBand37.VisibleIndex = 1;
            // 
            // col临产前未治愈
            // 
            this.col临产前未治愈.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col临产前未治愈.AppearanceHeader.Options.UseFont = true;
            this.col临产前未治愈.Caption = "未治愈";
            this.col临产前未治愈.FieldName = "未治愈";
            this.col临产前未治愈.Name = "col临产前未治愈";
            this.col临产前未治愈.OptionsColumn.AllowEdit = false;
            this.col临产前未治愈.Visible = true;
            this.col临产前未治愈.Width = 70;
            // 
            // gridBand34
            // 
            this.gridBand34.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand34.AppearanceHeader.Options.UseFont = true;
            this.gridBand34.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand34.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand34.Caption = "产时";
            this.gridBand34.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand38,
            this.gridBand39});
            this.gridBand34.Name = "gridBand34";
            this.gridBand34.VisibleIndex = 1;
            this.gridBand34.Width = 140;
            // 
            // gridBand38
            // 
            this.gridBand38.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand38.AppearanceHeader.Options.UseFont = true;
            this.gridBand38.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand38.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand38.Caption = "治愈";
            this.gridBand38.Columns.Add(this.col产时治愈);
            this.gridBand38.Name = "gridBand38";
            this.gridBand38.VisibleIndex = 0;
            // 
            // col产时治愈
            // 
            this.col产时治愈.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col产时治愈.AppearanceHeader.Options.UseFont = true;
            this.col产时治愈.Caption = "治愈";
            this.col产时治愈.FieldName = "治愈";
            this.col产时治愈.Name = "col产时治愈";
            this.col产时治愈.OptionsColumn.AllowEdit = false;
            this.col产时治愈.Visible = true;
            this.col产时治愈.Width = 70;
            // 
            // gridBand39
            // 
            this.gridBand39.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand39.AppearanceHeader.Options.UseFont = true;
            this.gridBand39.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand39.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand39.Caption = "未治愈";
            this.gridBand39.Columns.Add(this.col产时未治愈);
            this.gridBand39.Name = "gridBand39";
            this.gridBand39.VisibleIndex = 1;
            // 
            // col产时未治愈
            // 
            this.col产时未治愈.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col产时未治愈.AppearanceHeader.Options.UseFont = true;
            this.col产时未治愈.Caption = "未治愈";
            this.col产时未治愈.FieldName = "未治愈";
            this.col产时未治愈.Name = "col产时未治愈";
            this.col产时未治愈.OptionsColumn.AllowEdit = false;
            this.col产时未治愈.Visible = true;
            this.col产时未治愈.Width = 70;
            // 
            // gridBand35
            // 
            this.gridBand35.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand35.AppearanceHeader.Options.UseFont = true;
            this.gridBand35.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand35.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand35.Caption = "产褥期";
            this.gridBand35.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand40,
            this.gridBand41});
            this.gridBand35.Name = "gridBand35";
            this.gridBand35.VisibleIndex = 2;
            this.gridBand35.Width = 140;
            // 
            // gridBand40
            // 
            this.gridBand40.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand40.AppearanceHeader.Options.UseFont = true;
            this.gridBand40.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand40.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand40.Caption = "治愈";
            this.gridBand40.Columns.Add(this.col产褥期治愈);
            this.gridBand40.Name = "gridBand40";
            this.gridBand40.VisibleIndex = 0;
            // 
            // col产褥期治愈
            // 
            this.col产褥期治愈.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col产褥期治愈.AppearanceHeader.Options.UseFont = true;
            this.col产褥期治愈.Caption = "治愈";
            this.col产褥期治愈.FieldName = "治愈";
            this.col产褥期治愈.Name = "col产褥期治愈";
            this.col产褥期治愈.OptionsColumn.AllowEdit = false;
            this.col产褥期治愈.Visible = true;
            this.col产褥期治愈.Width = 70;
            // 
            // gridBand41
            // 
            this.gridBand41.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand41.AppearanceHeader.Options.UseFont = true;
            this.gridBand41.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand41.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand41.Caption = "未治愈";
            this.gridBand41.Columns.Add(this.col产褥期未治愈);
            this.gridBand41.Name = "gridBand41";
            this.gridBand41.VisibleIndex = 1;
            // 
            // col产褥期未治愈
            // 
            this.col产褥期未治愈.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col产褥期未治愈.AppearanceHeader.Options.UseFont = true;
            this.col产褥期未治愈.Caption = "未治愈";
            this.col产褥期未治愈.FieldName = "未治愈";
            this.col产褥期未治愈.Name = "col产褥期未治愈";
            this.col产褥期未治愈.OptionsColumn.AllowEdit = false;
            this.col产褥期未治愈.Visible = true;
            this.col产褥期未治愈.Width = 70;
            // 
            // gridBand47
            // 
            this.gridBand47.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand47.AppearanceHeader.Options.UseFont = true;
            this.gridBand47.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand47.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand47.Caption = "分娩情况";
            this.gridBand47.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand42,
            this.gridBand43,
            this.gridBand44,
            this.gridBand45,
            this.gridBand46,
            this.gridBand48,
            this.gridBand49,
            this.gridBand50,
            this.gridBand51});
            this.gridBand47.Name = "gridBand47";
            this.gridBand47.VisibleIndex = 14;
            this.gridBand47.Width = 716;
            // 
            // gridBand42
            // 
            this.gridBand42.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand42.AppearanceHeader.Options.UseFont = true;
            this.gridBand42.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand42.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand42.Caption = "分娩时间";
            this.gridBand42.Columns.Add(this.col分娩时间);
            this.gridBand42.Name = "gridBand42";
            this.gridBand42.VisibleIndex = 0;
            this.gridBand42.Width = 75;
            // 
            // col分娩时间
            // 
            this.col分娩时间.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col分娩时间.AppearanceHeader.Options.UseFont = true;
            this.col分娩时间.Caption = "分娩时间";
            this.col分娩时间.FieldName = "分娩时间";
            this.col分娩时间.Name = "col分娩时间";
            this.col分娩时间.OptionsColumn.AllowEdit = false;
            this.col分娩时间.Visible = true;
            // 
            // gridBand43
            // 
            this.gridBand43.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand43.AppearanceHeader.Options.UseFont = true;
            this.gridBand43.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand43.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand43.Caption = "孕周";
            this.gridBand43.Columns.Add(this.col孕周);
            this.gridBand43.Name = "gridBand43";
            this.gridBand43.VisibleIndex = 1;
            // 
            // col孕周
            // 
            this.col孕周.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col孕周.AppearanceHeader.Options.UseFont = true;
            this.col孕周.Caption = "孕周";
            this.col孕周.FieldName = "孕周";
            this.col孕周.Name = "col孕周";
            this.col孕周.OptionsColumn.AllowEdit = false;
            this.col孕周.Visible = true;
            this.col孕周.Width = 70;
            // 
            // gridBand44
            // 
            this.gridBand44.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand44.AppearanceHeader.Options.UseFont = true;
            this.gridBand44.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand44.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand44.Caption = "分娩地点";
            this.gridBand44.Columns.Add(this.col分娩地点);
            this.gridBand44.Name = "gridBand44";
            this.gridBand44.VisibleIndex = 2;
            this.gridBand44.Width = 75;
            // 
            // col分娩地点
            // 
            this.col分娩地点.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col分娩地点.AppearanceHeader.Options.UseFont = true;
            this.col分娩地点.Caption = "分娩地点";
            this.col分娩地点.FieldName = "分娩地点";
            this.col分娩地点.Name = "col分娩地点";
            this.col分娩地点.OptionsColumn.AllowEdit = false;
            this.col分娩地点.Visible = true;
            // 
            // gridBand45
            // 
            this.gridBand45.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand45.AppearanceHeader.Options.UseFont = true;
            this.gridBand45.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand45.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand45.Caption = "分娩方式";
            this.gridBand45.Columns.Add(this.col分娩方式);
            this.gridBand45.Name = "gridBand45";
            this.gridBand45.VisibleIndex = 3;
            this.gridBand45.Width = 75;
            // 
            // col分娩方式
            // 
            this.col分娩方式.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col分娩方式.AppearanceHeader.Options.UseFont = true;
            this.col分娩方式.Caption = "分娩方式";
            this.col分娩方式.FieldName = "分娩方式";
            this.col分娩方式.Name = "col分娩方式";
            this.col分娩方式.OptionsColumn.AllowEdit = false;
            this.col分娩方式.Visible = true;
            // 
            // gridBand46
            // 
            this.gridBand46.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand46.AppearanceHeader.Options.UseFont = true;
            this.gridBand46.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand46.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand46.Caption = "非住院分娩新法接生";
            this.gridBand46.Columns.Add(this.col非住院分娩新法接生);
            this.gridBand46.Name = "gridBand46";
            this.gridBand46.VisibleIndex = 4;
            this.gridBand46.Width = 136;
            // 
            // col非住院分娩新法接生
            // 
            this.col非住院分娩新法接生.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col非住院分娩新法接生.AppearanceHeader.Options.UseFont = true;
            this.col非住院分娩新法接生.Caption = "非住院分娩新法接生";
            this.col非住院分娩新法接生.FieldName = "非住院分娩新法接生";
            this.col非住院分娩新法接生.Name = "col非住院分娩新法接生";
            this.col非住院分娩新法接生.OptionsColumn.AllowEdit = false;
            this.col非住院分娩新法接生.Visible = true;
            this.col非住院分娩新法接生.Width = 136;
            // 
            // gridBand48
            // 
            this.gridBand48.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand48.AppearanceHeader.Options.UseFont = true;
            this.gridBand48.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand48.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand48.Caption = "活产";
            this.gridBand48.Columns.Add(this.col活产);
            this.gridBand48.Name = "gridBand48";
            this.gridBand48.VisibleIndex = 5;
            // 
            // col活产
            // 
            this.col活产.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col活产.AppearanceHeader.Options.UseFont = true;
            this.col活产.Caption = "活产";
            this.col活产.FieldName = "活产";
            this.col活产.Name = "col活产";
            this.col活产.OptionsColumn.AllowEdit = false;
            this.col活产.Visible = true;
            this.col活产.Width = 70;
            // 
            // gridBand49
            // 
            this.gridBand49.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand49.AppearanceHeader.Options.UseFont = true;
            this.gridBand49.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand49.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand49.Caption = "死胎";
            this.gridBand49.Columns.Add(this.col死胎);
            this.gridBand49.Name = "gridBand49";
            this.gridBand49.VisibleIndex = 6;
            // 
            // col死胎
            // 
            this.col死胎.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col死胎.AppearanceHeader.Options.UseFont = true;
            this.col死胎.Caption = "死胎";
            this.col死胎.FieldName = "死胎";
            this.col死胎.Name = "col死胎";
            this.col死胎.OptionsColumn.AllowEdit = false;
            this.col死胎.Visible = true;
            this.col死胎.Width = 70;
            // 
            // gridBand50
            // 
            this.gridBand50.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand50.AppearanceHeader.Options.UseFont = true;
            this.gridBand50.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand50.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand50.Caption = "死产";
            this.gridBand50.Columns.Add(this.col死产);
            this.gridBand50.Name = "gridBand50";
            this.gridBand50.VisibleIndex = 7;
            // 
            // col死产
            // 
            this.col死产.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col死产.AppearanceHeader.Options.UseFont = true;
            this.col死产.Caption = "死产";
            this.col死产.FieldName = "死产";
            this.col死产.Name = "col死产";
            this.col死产.OptionsColumn.AllowEdit = false;
            this.col死产.Visible = true;
            this.col死产.Width = 70;
            // 
            // gridBand51
            // 
            this.gridBand51.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand51.AppearanceHeader.Options.UseFont = true;
            this.gridBand51.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand51.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand51.Caption = "新生儿死亡";
            this.gridBand51.Columns.Add(this.col新生儿死亡);
            this.gridBand51.Name = "gridBand51";
            this.gridBand51.VisibleIndex = 8;
            this.gridBand51.Width = 75;
            // 
            // col新生儿死亡
            // 
            this.col新生儿死亡.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col新生儿死亡.AppearanceHeader.Options.UseFont = true;
            this.col新生儿死亡.Caption = "新生儿死亡";
            this.col新生儿死亡.FieldName = "新生儿死亡";
            this.col新生儿死亡.Name = "col新生儿死亡";
            this.col新生儿死亡.OptionsColumn.AllowEdit = false;
            this.col新生儿死亡.Visible = true;
            // 
            // gridBand52
            // 
            this.gridBand52.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand52.AppearanceHeader.Options.UseFont = true;
            this.gridBand52.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand52.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand52.Caption = "产后访视次数";
            this.gridBand52.Columns.Add(this.col产后访视次数);
            this.gridBand52.Name = "gridBand52";
            this.gridBand52.VisibleIndex = 15;
            this.gridBand52.Width = 100;
            // 
            // col产后访视次数
            // 
            this.col产后访视次数.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col产后访视次数.AppearanceHeader.Options.UseFont = true;
            this.col产后访视次数.Caption = "产后访视次数";
            this.col产后访视次数.FieldName = "产后访视次数";
            this.col产后访视次数.Name = "col产后访视次数";
            this.col产后访视次数.OptionsColumn.AllowEdit = false;
            this.col产后访视次数.Visible = true;
            this.col产后访视次数.Width = 100;
            // 
            // gridBand53
            // 
            this.gridBand53.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand53.AppearanceHeader.Options.UseFont = true;
            this.gridBand53.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand53.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand53.Caption = "产后42-56天检查";
            this.gridBand53.Columns.Add(this.col产后42到56天检查);
            this.gridBand53.Name = "gridBand53";
            this.gridBand53.VisibleIndex = 16;
            this.gridBand53.Width = 124;
            // 
            // col产后42到56天检查
            // 
            this.col产后42到56天检查.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col产后42到56天检查.AppearanceHeader.Options.UseFont = true;
            this.col产后42到56天检查.Caption = "产后42天检查";
            this.col产后42到56天检查.FieldName = "产后42天检查";
            this.col产后42到56天检查.Name = "col产后42到56天检查";
            this.col产后42到56天检查.OptionsColumn.AllowEdit = false;
            this.col产后42到56天检查.Visible = true;
            this.col产后42到56天检查.Width = 124;
            // 
            // gridBand54
            // 
            this.gridBand54.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand54.AppearanceHeader.Options.UseFont = true;
            this.gridBand54.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand54.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand54.Caption = "新生儿情况";
            this.gridBand54.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand55,
            this.gridBand56,
            this.gridBand57,
            this.gridBand58,
            this.gridBand59,
            this.gridBand60});
            this.gridBand54.Name = "gridBand54";
            this.gridBand54.VisibleIndex = 17;
            this.gridBand54.Width = 445;
            // 
            // gridBand55
            // 
            this.gridBand55.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand55.AppearanceHeader.Options.UseFont = true;
            this.gridBand55.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand55.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand55.Caption = "性别";
            this.gridBand55.Columns.Add(this.col性别);
            this.gridBand55.Name = "gridBand55";
            this.gridBand55.VisibleIndex = 0;
            this.gridBand55.Width = 50;
            // 
            // col性别
            // 
            this.col性别.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col性别.AppearanceHeader.Options.UseFont = true;
            this.col性别.Caption = "性别";
            this.col性别.FieldName = "性别";
            this.col性别.Name = "col性别";
            this.col性别.OptionsColumn.AllowEdit = false;
            this.col性别.Visible = true;
            this.col性别.Width = 50;
            // 
            // gridBand56
            // 
            this.gridBand56.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand56.AppearanceHeader.Options.UseFont = true;
            this.gridBand56.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand56.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand56.Caption = "出生身高cm";
            this.gridBand56.Columns.Add(this.col出生身高);
            this.gridBand56.Name = "gridBand56";
            this.gridBand56.VisibleIndex = 1;
            this.gridBand56.Width = 80;
            // 
            // col出生身高
            // 
            this.col出生身高.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col出生身高.AppearanceHeader.Options.UseFont = true;
            this.col出生身高.Caption = "出生身高";
            this.col出生身高.FieldName = "出生身高";
            this.col出生身高.Name = "col出生身高";
            this.col出生身高.OptionsColumn.AllowEdit = false;
            this.col出生身高.Visible = true;
            this.col出生身高.Width = 80;
            // 
            // gridBand57
            // 
            this.gridBand57.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand57.AppearanceHeader.Options.UseFont = true;
            this.gridBand57.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand57.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand57.Caption = "出生体重g";
            this.gridBand57.Columns.Add(this.col出生体重);
            this.gridBand57.Name = "gridBand57";
            this.gridBand57.VisibleIndex = 2;
            this.gridBand57.Width = 75;
            // 
            // col出生体重
            // 
            this.col出生体重.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col出生体重.AppearanceHeader.Options.UseFont = true;
            this.col出生体重.Caption = "出生体重";
            this.col出生体重.FieldName = "出生体重";
            this.col出生体重.Name = "col出生体重";
            this.col出生体重.OptionsColumn.AllowEdit = false;
            this.col出生体重.Visible = true;
            // 
            // gridBand58
            // 
            this.gridBand58.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand58.AppearanceHeader.Options.UseFont = true;
            this.gridBand58.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand58.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand58.Caption = "阿氏评分1分钟";
            this.gridBand58.Columns.Add(this.col阿氏评分1分钟);
            this.gridBand58.Name = "gridBand58";
            this.gridBand58.VisibleIndex = 3;
            this.gridBand58.Width = 100;
            // 
            // col阿氏评分1分钟
            // 
            this.col阿氏评分1分钟.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col阿氏评分1分钟.AppearanceHeader.Options.UseFont = true;
            this.col阿氏评分1分钟.Caption = "阿氏评分1分钟";
            this.col阿氏评分1分钟.FieldName = "阿氏评分1分钟";
            this.col阿氏评分1分钟.Name = "col阿氏评分1分钟";
            this.col阿氏评分1分钟.OptionsColumn.AllowEdit = false;
            this.col阿氏评分1分钟.Visible = true;
            this.col阿氏评分1分钟.Width = 100;
            // 
            // gridBand59
            // 
            this.gridBand59.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand59.AppearanceHeader.Options.UseFont = true;
            this.gridBand59.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand59.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand59.Caption = "访视次数";
            this.gridBand59.Columns.Add(this.col访视次数);
            this.gridBand59.Name = "gridBand59";
            this.gridBand59.VisibleIndex = 4;
            // 
            // col访视次数
            // 
            this.col访视次数.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col访视次数.AppearanceHeader.Options.UseFont = true;
            this.col访视次数.Caption = "访视次数";
            this.col访视次数.FieldName = "访视次数";
            this.col访视次数.Name = "col访视次数";
            this.col访视次数.OptionsColumn.AllowEdit = false;
            this.col访视次数.Visible = true;
            this.col访视次数.Width = 70;
            // 
            // gridBand60
            // 
            this.gridBand60.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand60.AppearanceHeader.Options.UseFont = true;
            this.gridBand60.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand60.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand60.Caption = "患破伤风";
            this.gridBand60.Columns.Add(this.col患破伤风);
            this.gridBand60.Name = "gridBand60";
            this.gridBand60.VisibleIndex = 5;
            // 
            // col患破伤风
            // 
            this.col患破伤风.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col患破伤风.AppearanceHeader.Options.UseFont = true;
            this.col患破伤风.Caption = "患破伤风";
            this.col患破伤风.FieldName = "患破伤风";
            this.col患破伤风.Name = "col患破伤风";
            this.col患破伤风.OptionsColumn.AllowEdit = false;
            this.col患破伤风.Visible = true;
            this.col患破伤风.Width = 70;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "孕  产  妇  系  统  管  理  情  况  登  记  簿";
            this.gridBand1.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2,
            this.gridBand3,
            this.gridBand4});
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = -1;
            this.gridBand1.Width = 673;
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "孕产妇姓名";
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 0;
            this.gridBand2.Width = 93;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "户口所在地";
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 1;
            this.gridBand3.Width = 147;
            // 
            // gridBand4
            // 
            this.gridBand4.Caption = "产前检查情况";
            this.gridBand4.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand5,
            this.gridBand7,
            this.gridBand61,
            this.gridBand6,
            this.gridBand62});
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 2;
            this.gridBand4.Width = 433;
            // 
            // gridBand5
            // 
            this.gridBand5.Caption = "8周~";
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 0;
            this.gridBand5.Width = 96;
            // 
            // gridBand7
            // 
            this.gridBand7.Caption = "gridBand7";
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 1;
            this.gridBand7.Width = 95;
            // 
            // gridBand61
            // 
            this.gridBand61.Caption = "gridBand61";
            this.gridBand61.Name = "gridBand61";
            this.gridBand61.VisibleIndex = 2;
            this.gridBand61.Width = 75;
            // 
            // gridBand6
            // 
            this.gridBand6.Caption = "gridBand6";
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 3;
            this.gridBand6.Width = 97;
            // 
            // gridBand62
            // 
            this.gridBand62.Caption = "gridBand62";
            this.gridBand62.Name = "gridBand62";
            this.gridBand62.VisibleIndex = 4;
            // 
            // gridBand63
            // 
            this.gridBand63.Caption = "gridBand63";
            this.gridBand63.Name = "gridBand63";
            this.gridBand63.VisibleIndex = -1;
            // 
            // gc档案
            // 
            this.gc档案.AllowBandedGridColumnSort = false;
            this.gc档案.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc档案.IsBestFitColumns = true;
            this.gc档案.Location = new System.Drawing.Point(0, 0);
            this.gc档案.MainView = this.gv档案;
            this.gc档案.Name = "gc档案";
            this.gc档案.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.link档案号});
            this.gc档案.ShowContextMenu = false;
            this.gc档案.Size = new System.Drawing.Size(1044, 434);
            this.gc档案.StrWhere = "";
            this.gc档案.TabIndex = 7;
            this.gc档案.UseCheckBox = false;
            this.gc档案.View = "";
            this.gc档案.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv档案});
            // 
            // gv档案
            // 
            this.gv档案.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gv档案.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gv档案.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gv档案.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.DetailTip.Options.UseFont = true;
            this.gv档案.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.Empty.Options.UseFont = true;
            this.gv档案.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.EvenRow.Options.UseFont = true;
            this.gv档案.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gv档案.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.FilterPanel.Options.UseFont = true;
            this.gv档案.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.FixedLine.Options.UseFont = true;
            this.gv档案.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.FocusedCell.Options.UseFont = true;
            this.gv档案.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.FocusedRow.Options.UseFont = true;
            this.gv档案.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gv档案.Appearance.FooterPanel.Options.UseFont = true;
            this.gv档案.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.GroupButton.Options.UseFont = true;
            this.gv档案.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.GroupFooter.Options.UseFont = true;
            this.gv档案.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.GroupPanel.Options.UseFont = true;
            this.gv档案.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.GroupRow.Options.UseFont = true;
            this.gv档案.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.HeaderPanel.Options.UseFont = true;
            this.gv档案.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gv档案.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.HorzLine.Options.UseFont = true;
            this.gv档案.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.OddRow.Options.UseFont = true;
            this.gv档案.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.Preview.Options.UseFont = true;
            this.gv档案.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.Row.Options.UseFont = true;
            this.gv档案.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.RowSeparator.Options.UseFont = true;
            this.gv档案.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.SelectedRow.Options.UseFont = true;
            this.gv档案.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.TopNewRow.Options.UseFont = true;
            this.gv档案.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.VertLine.Options.UseFont = true;
            this.gv档案.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.ViewCaption.Options.UseFont = true;
            this.gv档案.ColumnPanelRowHeight = 30;
            this.gv档案.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29});
            this.gv档案.GridControl = this.gc档案;
            this.gv档案.GroupPanelText = "DragColumn";
            this.gv档案.Name = "gv档案";
            this.gv档案.OptionsView.ColumnAutoWidth = false;
            this.gv档案.OptionsView.EnableAppearanceEvenRow = true;
            this.gv档案.OptionsView.EnableAppearanceOddRow = true;
            this.gv档案.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn20
            // 
            this.gridColumn20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn20.Caption = "档案号";
            this.gridColumn20.ColumnEdit = this.link档案号;
            this.gridColumn20.FieldName = "个人档案编号";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 0;
            // 
            // link档案号
            // 
            this.link档案号.AutoHeight = false;
            this.link档案号.Name = "link档案号";
            this.link档案号.Click += new System.EventHandler(this.link档案号_Click);
            // 
            // gridColumn21
            // 
            this.gridColumn21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn21.Caption = "姓名";
            this.gridColumn21.FieldName = "姓名";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 1;
            // 
            // gridColumn22
            // 
            this.gridColumn22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn22.Caption = "性别";
            this.gridColumn22.FieldName = "性别";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 2;
            // 
            // gridColumn23
            // 
            this.gridColumn23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn23.Caption = "出生日期";
            this.gridColumn23.FieldName = "出生日期";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 3;
            // 
            // gridColumn24
            // 
            this.gridColumn24.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn24.Caption = "身份证号";
            this.gridColumn24.FieldName = "身份证号";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 4;
            // 
            // gridColumn25
            // 
            this.gridColumn25.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn25.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn25.Caption = "联系电话";
            this.gridColumn25.FieldName = "联系人电话";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 5;
            // 
            // gridColumn26
            // 
            this.gridColumn26.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn26.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn26.Caption = "当前所属机构";
            this.gridColumn26.FieldName = "所属机构";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 6;
            this.gridColumn26.Width = 105;
            // 
            // gridColumn27
            // 
            this.gridColumn27.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn27.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn27.Caption = "录入人";
            this.gridColumn27.FieldName = "创建人";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 7;
            // 
            // gridColumn28
            // 
            this.gridColumn28.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn28.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn28.Caption = "录入时间";
            this.gridColumn28.FieldName = "创建时间";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 8;
            // 
            // gridColumn29
            // 
            this.gridColumn29.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn29.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn29.Caption = "家庭档案编号";
            this.gridColumn29.FieldName = "家庭档案编号";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 9;
            this.gridColumn29.Width = 102;
            // 
            // frm孕产妇系统管理情况登记簿
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1050, 466);
            this.Name = "frm孕产妇系统管理情况登记簿";
            this.Text = "孕产妇系统管理情况登记簿";
            this.Load += new System.EventHandler(this.孕产妇系统管理情况登记簿_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dte随访日期2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访日期2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访日期1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访日期1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt孕周End.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt孕周Begin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte结束2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte结束2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte开始1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte开始1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link档案号)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Library.UserControls.DataGridControl gcSummary;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btnEmpty;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraEditors.DateEdit dte结束2;
        private DevExpress.XtraEditors.DateEdit dte开始1;
        private DevExpress.XtraEditors.TreeListLookUpEdit txt机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private Library.UserControls.DataGridControl gc档案;
        private DevExpress.XtraGrid.Views.Grid.GridView gv档案;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link档案号;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col孕产妇姓名;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col户口所在地;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col住址;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col年龄;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col孕次;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col产次;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col末次月经;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col预产期;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col建册时间;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col8周;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col12周;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col16周;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand61;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand62;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand63;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col20周;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col24周;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col28周;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col32周;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col34周;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col38周;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col39周;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col总次数;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col高危因素;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col接受高危管理;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col临产前治愈;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col临产前未治愈;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col产时治愈;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col产时未治愈;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col产褥期治愈;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col产褥期未治愈;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col分娩时间;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col孕周;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col分娩地点;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col分娩方式;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col非住院分娩新法接生;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col活产;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col死胎;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col死产;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col新生儿死亡;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col产后访视次数;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col产后42到56天检查;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col性别;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col出生身高;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col出生体重;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col阿氏评分1分钟;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col访视次数;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col患破伤风;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand64;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col个人档案编号;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand16;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand17;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand18;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand19;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand20;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand21;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand22;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand23;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand24;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand25;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand26;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand27;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand28;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand29;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand30;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand31;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand32;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand33;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand36;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand37;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand34;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand38;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand39;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand35;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand40;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand41;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand47;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand42;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand43;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand44;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand45;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand46;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand48;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand49;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand50;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand51;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand52;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand53;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand54;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand55;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand56;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand57;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand58;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand59;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand60;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.TextEdit txt档案编号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.TextEdit txt孕周End;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txt孕周Begin;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.DateEdit dte随访日期2;
        private DevExpress.XtraEditors.DateEdit dte随访日期1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;

    }
}