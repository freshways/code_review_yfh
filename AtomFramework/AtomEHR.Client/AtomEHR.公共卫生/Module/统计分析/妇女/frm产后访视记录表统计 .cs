﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Interfaces;
using AtomEHR.公共卫生.Module.个人健康;
using DevExpress.XtraEditors.Repository;

namespace AtomEHR.公共卫生.Module
{
    public partial class frm产后访视记录表统计 : AtomEHR.Library.frmBaseDataForm
    {
        DataSet ds = null;
        public frm产后访视记录表统计()
        {
            InitializeComponent();
        }

        private void frm产后访视记录表统计_Load(object sender, EventArgs e)
        {
            if (Loginer.CurrentUser.所属机构.Length >= 12)
                txt机构.Properties.AutoExpandAllNodes = true;
            else { txt机构.Properties.AutoExpandAllNodes = false; }
            this.InitializeForm();
            string m_ServerDate = _bll.ServiceDateTime;
            try
            {
                this.dte开始1.Text = Convert.ToDateTime(m_ServerDate).ToString("yyyy-MM-01");
                this.dte结束2.Text = Convert.ToDateTime(m_ServerDate).ToString("yyyy-MM-dd");
            }
            catch
            {
                this.dte开始1.Text = DateTime.Now.ToString("yyyy-MM-01");
                this.dte结束2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
        }

        protected override void InitializeForm()
        {
            //_BLL = new bllMXB高血压管理卡();// 业务逻辑层实例
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gvSummary);
            frmGridCustomize.RegisterGrid(gv档案);

            try
            {
                this.txt机构.Properties.AutoExpandAllNodes = false;
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                txt机构.Properties.ValueMember = "机构编号";
                txt机构.Properties.DisplayMember = "机构名称";

                txt机构.Properties.TreeList.KeyFieldName = "机构编号";
                txt机构.Properties.TreeList.ParentFieldName = "上级机构";
                txt机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                txt机构.Properties.DataSource = dt所属机构;

                txt机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                txt机构.Text = Loginer.CurrentUser.所属机构;
            }
        }

        bll孕妇_产后访视情况 _bll = new bll孕妇_产后访视情况();
        private void btnQuery_Click(object sender, EventArgs e)
        {
            if (this.cbo档案状态.Text.Trim() == "录入时间")
            {
                ds = _bll.GetReportDataBYLRSJ(txt机构.EditValue.ToString(), txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "1");
            }
            else if (this.cbo档案状态.Text.Trim() == "调查时间")
            {
                ds = _bll.GetReportDataBYDCSJ(txt机构.EditValue.ToString(), txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "1");
            }
            if (ds != null && ds.Tables.Count > 0)
            {
                gcSummary.DataSource = ds.Tables[0];
                this.gcSummary.BringToFront();
                this.gvSummary.BestFitColumns();
            }
        }

        private void link区域名称_Click(object sender, EventArgs e)
        {
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();

            if (this.cbo档案状态.Text.Trim() == "录入时间")
            {
                ds = _bll.GetReportDataBYLRSJ(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "1");
            }
            else if (this.cbo档案状态.Text.Trim() == "调查时间")
            {
                ds = _bll.GetReportDataBYDCSJ(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "1");
            }
            if (ds != null && ds.Tables.Count > 0)
            {
                gcSummary.DataSource = ds.Tables[0];
                this.gvSummary.BestFitColumns();
                this.gcSummary.BringToFront();
            }
        }

        private void link总数_Click(object sender, EventArgs e)
        {
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();

            if (this.cbo档案状态.Text.Trim() == "录入时间")
            {
                ds = _bll.GetReportDataBYLRSJ(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "2");
            }
            else if (this.cbo档案状态.Text.Trim() == "调查时间")
            {
                ds = _bll.GetReportDataBYDCSJ(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "2");
            }
            if (ds != null && ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    ds.Tables[0].Rows[i]["性别"] = _bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    ds.Tables[0].Rows[i]["创建人"] = _bll.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                    ds.Tables[0].Rows[i]["创建机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["创建机构"].ToString());
                    ds.Tables[0].Rows[i]["所属机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                }
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        private void link不合格数_Click(object sender, EventArgs e)
        {
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();

            if (this.cbo档案状态.Text.Trim() == "录入时间")
            {
                ds = _bll.GetReportDataBYLRSJ(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "4");
            }
            else if (this.cbo档案状态.Text.Trim() == "调查时间")
            {
                ds = _bll.GetReportDataBYDCSJ(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "4");
            }
            if (ds != null && ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    ds.Tables[0].Rows[i]["性别"] = _bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    ds.Tables[0].Rows[i]["创建人"] = _bll.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                    ds.Tables[0].Rows[i]["创建机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["创建机构"].ToString());
                    ds.Tables[0].Rows[i]["所属机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                }
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        private void link合格数_Click(object sender, EventArgs e)
        {
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();

            if (this.cbo档案状态.Text.Trim() == "录入时间")
            {
                ds = _bll.GetReportDataBYLRSJ(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "3");
            }
            else if (this.cbo档案状态.Text.Trim() == "调查时间")
            {
                ds = _bll.GetReportDataBYDCSJ(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "3");
            }

            if (ds != null && ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    ds.Tables[0].Rows[i]["性别"] = _bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    ds.Tables[0].Rows[i]["创建人"] = _bll.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                    ds.Tables[0].Rows[i]["创建机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["创建机构"].ToString());
                    ds.Tables[0].Rows[i]["所属机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                }
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        private void link档案号_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv档案.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            string id = row["ID"].ToString();
            //_BLL.GetBusinessByKey(个人档案编号, true);//下载一个空业务单据            
            //_BLL.NewBusiness(); //增加一条主表记录
            frm个人健康 frm = new frm个人健康(false, "frm产后访视情况", 家庭档案编号, 个人档案编号, id);
            frm.Show();
        }

        private int iAllcount = 1;
        private int iHegecount = 0;
        private void gvSummary_CustomDrawFooterCell(object sender, DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventArgs e)
        {

            if (e.Column == gridColumn9)//合格率
            {
                e.Info.DisplayText = (iHegecount * 100.0 / iAllcount).ToString("N2") + "%";
            }
            else if (e.Column == gridColumn6)//总数
            {
                try
                {
                    int temp = Convert.ToInt32(e.Column.SummaryText.Replace(",", ""));
                    iAllcount = (temp == 0) ? 1 : temp;
                }
                catch
                {
                    iAllcount = 1;
                }
            }
            else if (e.Column == gridColumn7)//合格数
            {
                try
                {
                    int temp = Convert.ToInt32(e.Column.SummaryText.Replace(",", ""));
                    iHegecount = temp;
                }
                catch
                {
                    iHegecount = 0;
                }
            }
            else
            { }
        }

        private void gvSummary_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            DataRowView row = gvSummary.GetRow(e.RowHandle) as DataRowView;
            if (row == null) return;
            string zongshu = row["总数"].ToString();
            string hegeshu = row["合格数"].ToString();
            string buhegeshu = row["不合格数"].ToString();
            if (e.Column == gridColumn6)
            {
                if (!string.IsNullOrEmpty(zongshu) && zongshu != "0")
                {
                    RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
                    link.Click += link总数_Click;
                    e.RepositoryItem = link;
                }
            }

            if (e.Column == gridColumn7)
            {
                if (!string.IsNullOrEmpty(hegeshu) && hegeshu != "0")
                {
                    RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
                    link.Click += link合格数_Click;
                    e.RepositoryItem = link;
                }
            }
            if (e.Column == gridColumn8)
            {
                if (!string.IsNullOrEmpty(buhegeshu) && buhegeshu != "0")
                {
                    RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
                    link.Click += link不合格数_Click;
                    e.RepositoryItem = link;
                }
            }
        }

        private void btnEmpty_Click(object sender, EventArgs e)
        {
            this.gc档案.BringToFront();
        }
    }
}
