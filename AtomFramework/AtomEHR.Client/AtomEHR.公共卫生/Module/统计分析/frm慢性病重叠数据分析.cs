﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraCharts;
using AtomEHR.Business;

namespace AtomEHR.公共卫生.Module
{
    public partial class frm慢性病重叠数据分析 : Form
    {
        public frm慢性病重叠数据分析()
        {
            InitializeComponent();
        }

        private void frm慢性病重叠数据分析_Load(object sender, EventArgs e)
        {
            //绑定统计图类型
            this.comboBoxEdit1.Properties.Items.Clear();
            this.comboBoxEdit1.Properties.Items.Add("直方图");
            this.comboBoxEdit1.Properties.Items.Add("3D直方图");
            this.comboBoxEdit1.Properties.Items.Add("圆饼图");
            this.comboBoxEdit1.Properties.Items.Add("3D圆饼图");
            this.comboBoxEdit1.Properties.Items.Add("线图");
            this.comboBoxEdit1.Properties.Items.Add("点图");
            this.comboBoxEdit1.SelectedIndex = 2;

            //设置图表标题  
            ChartTitle ct = new ChartTitle();
            ct.Text = "慢性病人群统计分析图";
            ct.TextColor = Color.Black;//颜色  
            ct.Font = new Font("Tahoma", 12);//字体  
            ct.Dock = ChartTitleDockStyle.Top;//停靠在上方  
            ct.Alignment = StringAlignment.Center;//居中显示  
            this.chartControl1.Titles.Add(ct);

            LoadChartInfo();
        }

        /// <summary>
        /// 统计图
        /// </summary>
        private Series mySeries;
        bll慢性病统计分析 bll = new bll慢性病统计分析();
        /// <summary>
        /// 手动加载统计图信息
        /// </summary>
        private void LoadChartInfo()
        {
            this.chartControl1.Series.Clear();

            //Legend显示样式
            chartControl1.Legend.AlignmentHorizontal = LegendAlignmentHorizontal.Left; //居左显示
            chartControl1.Legend.AlignmentVertical = LegendAlignmentVertical.BottomOutside; //显示在下方

            #region 遍历图表显示方式
            //新建Series
            switch (comboBoxEdit1.Text)
            {
                case "直方图":
                    mySeries = new Series("直方图", ViewType.Bar);
                    break;
                case "3D直方图":
                    mySeries = new Series("3D直方图", ViewType.Bar3D);
                    break;
                case "圆饼图":
                    mySeries = new Series("圆饼图", ViewType.Pie);
                    ((PiePointOptions)mySeries.LegendPointOptions).PointView = PointView.ArgumentAndValues;
                    ((PieSeriesLabel)mySeries.Label).Position = PieSeriesLabelPosition.TwoColumns;

                    //设置爆炸方式
                    ((PieSeriesView)mySeries.View).ExplodeMode = PieExplodeMode.UsePoints;
                    break;
                case "3D圆饼图":
                    mySeries = new Series("3D圆饼图", ViewType.Pie3D);
                    //设置爆炸方式
                    ((Pie3DSeriesView)mySeries.View).ExplodeMode = PieExplodeMode.All;
                    break;
                case "线图":
                    mySeries = new Series("线图", ViewType.Line);
                    ((PointSeriesView)mySeries.View).PointMarkerOptions.Kind = MarkerKind.Triangle;
                    break;
                case "点图":
                    mySeries = new Series("点图", ViewType.Point);
                    ((PointSeriesView)mySeries.View).PointMarkerOptions.Kind = MarkerKind.Triangle;
                    break;
                default:
                    mySeries = new Series("直方图", ViewType.Bar);
                    break;
            }
            #endregion

            //设置Series样式  
            mySeries.ArgumentScaleType = ScaleType.Auto;//.Qualitative;//定性的
            mySeries.ValueScaleType = ScaleType.Numerical;//数字类型  
            mySeries.Label.PointOptions.PointView = PointView.ArgumentAndValues;//图表显示标题和值

            mySeries.LegendPointOptions.PointView = PointView.ArgumentAndValues;//显示表示的信息和数据  
            mySeries.LegendPointOptions.ValueNumericOptions.Format = NumericFormat.Number;//NumericFormat.Percent;//用百分比表示
            //mySeries.PointOptions.ValueNumbericOptions.Precision = 0;//百分号前面的数字不跟小数点


            // 获取到的数据
            DataTable datTable = new DataTable();
            datTable = GetBindingMain();

            if (comboBoxEdit1.Text == "圆饼图" || comboBoxEdit1.Text == "3D圆饼图")
            {
                this.chartControl1.Legend.Visible = false;
                //设置显示方式右上角
                ((PiePointOptions)mySeries.LegendPointOptions).PercentOptions.ValueAsPercent = false;
                ((PiePointOptions)mySeries.LegendPointOptions).ValueNumericOptions.Format = NumericFormat.Number;
                ((PiePointOptions)mySeries.LegendPointOptions).ValueNumericOptions.Precision = 0;

                double sum = 0; //总人数
                double sum重叠 = 0;//重叠人数
                double sum单纯 = 0;//单病人数
                for (int i = 0; i < datTable.Rows.Count; i++)
                {
                    string strType = datTable.Rows[i]["类型"].ToString();
                    double dtCount = Convert.ToDouble(datTable.Rows[i]["人数"].ToString());
                    //只显示勾选的分子
                    for (int fz = 0; fz < txt分子.Properties.Items.Count; fz++)
                    {
                        if (txt分子.Properties.Items[fz].CheckState == CheckState.Checked && txt分子.Properties.Items[fz].Description == strType)
                        {
                            mySeries.Points.Add(new SeriesPoint(strType + "(" + dtCount.ToString() + ")", new double[] { dtCount }));
                        }
                    }
                    if (strType.Equals(txt分母.Text.Trim()))
                        sum单纯 += dtCount;
                    else
                        sum重叠 += dtCount;
                    sum += dtCount;
                }

                this.txt总人数.Text = sum.ToString();
                if (txt分母.Text.Equals("65岁以上老年人") || txt分母.Text.Equals("65岁以下人群"))
                    this.txt重叠.Text = "0";
                else
                    this.txt重叠.Text = sum重叠.ToString();
                this.txt单纯.Text = sum单纯.ToString();
                //这个是爆炸扩展
                chartControl1.SetPieExplode(mySeries, PieExplodeMode.MinValue, 5, true);
                //// Specify how series points are located in a pie.  
                //mySeries.SeriesPointsSorting = SortingMode.Ascending;
                //mySeries.SeriesPointsSortingKey = SeriesPointKey.Value_1;
                //((PieSeriesView)mySeries.View).Rotation = 90;
            }
            else if (comboBoxEdit1.Text == "线图")
            {
                mySeries.ArgumentScaleType = ScaleType.Qualitative;//设置Series样式  
                for (int i = 0; i < datTable.Rows.Count; i++)
                {
                    string strType = datTable.Rows[i]["类型"].ToString();
                    double dtJE = Convert.ToDouble(datTable.Rows[i]["人数"].ToString());
                    mySeries.Points.Add(new SeriesPoint(strType, new double[] { dtJE }));
                }
                ((PointSeriesView)mySeries.View).PointMarkerOptions.Kind = MarkerKind.Triangle;
            }
            else
            {
                mySeries.DataSource = datTable.DefaultView;//绑定数据源  
                mySeries.ArgumentDataMember = "类型";//绑定的文字信息（名称）
                mySeries.ValueDataMembers[0] = "人数";//绑定的值（数据） 
            }

            //添加到统计图上
            this.chartControl1.Series.Clear();
            this.chartControl1.Series.Add(mySeries);

            if (comboBoxEdit1.Text == "线图" || comboBoxEdit1.Text == "点图")
            {
                this.chartControl1.Legend.Visible = true;
            }

            //图例设置
            SimpleDiagram3D diagram = new SimpleDiagram3D();
            diagram.RuntimeRotation = true;
            diagram.RuntimeScrolling = true;
            diagram.RuntimeZooming = true;
        }

        private DataTable GetBindingMain()
        {
            switch (txt分母.Text)
            {
                case "高血压":
                    this.chartControl1.Titles[0].Text = "高血压中" + txt人群.Text + "人群重叠数据分析";
                    label5.Text = "单纯-高血压人数";
                    return bll.Get高血压人群分析(txt分子.Text, txt人群.Text);
                case "糖尿病":
                    this.chartControl1.Titles[0].Text = "糖尿病中" + txt人群.Text + "人群重叠数据分析";
                    label5.Text = "单纯-糖尿病人数";
                    return bll.Get糖尿病人群分析(txt分子.Text, txt人群.Text);
                case "冠心病":
                    this.chartControl1.Titles[0].Text = "冠心病人群中重叠数据分析";
                    label5.Text = "单纯-冠心病人数";
                    return bll.Get冠心病人群分析(txt分子.Text, txt人群.Text);
                case "脑卒中":
                    this.chartControl1.Titles[0].Text = "脑卒中人群中重叠数据分析";
                    label5.Text = "单纯-脑卒中人数";
                    return bll.Get脑卒中人群分析(txt分子.Text, txt人群.Text);
                case "65岁以上老年人":
                    this.chartControl1.Titles[0].Text = "65岁以上老年人人群中单纯慢性病数据";
                    return bll.GetMain("65岁及以上老年人");
                case "65岁以下人群":
                    this.chartControl1.Titles[0].Text = "65岁以下人群中单纯慢性病数据";
                    return bll.GetMain("65岁以下人群");
                default:
                    return bll.GetMain("65岁以上老年人");
            }


        }

        /// <summary>
        /// 更换显示方式
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxEdit1_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadChartInfo();
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBinding_Click(object sender, EventArgs e)
        {
            LoadChartInfo();
        }

        private void txt分母_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < txt分子.Properties.Items.Count; i++)
            {
                if (!this.txt分子.Properties.Items[i].Description.Equals(txt分母.Text))
                    this.txt分子.Properties.Items[i].CheckState = CheckState.Checked;
                else
                    this.txt分子.Properties.Items[i].CheckState = CheckState.Unchecked;
            }
        }


    }
}
