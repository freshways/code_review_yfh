﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Interfaces;
using AtomEHR.公共卫生.Module.个人健康;
using DevExpress.XtraEditors.Repository;

namespace AtomEHR.公共卫生.Module
{
    public partial class frm健康人已体检 : AtomEHR.Library.frmBaseDataForm
    {
        //定义此类型，用来判断是点击那个链接跳转进档案grid的
        private string _type = "1";
        public frm健康人已体检()
        {
            InitializeComponent();
        }

        private void frm健康体检表统计_Load(object sender, EventArgs e)
        {
            if (Loginer.CurrentUser.所属机构.Length >= 12)
                txt机构.Properties.AutoExpandAllNodes = true;
            else { txt机构.Properties.AutoExpandAllNodes = false; }
            this.InitializeForm();
            string m_ServerDate = _bll.ServiceDateTime;
            try
            {
                this.dte开始1.Text = Convert.ToDateTime(m_ServerDate).ToString("yyyy-MM-01");
                this.dte结束2.Text = Convert.ToDateTime(m_ServerDate).ToString("yyyy-MM-dd");
            }
            catch
            {
                this.dte开始1.Text = DateTime.Now.ToString("yyyy-MM-01");
                this.dte结束2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
        }

        protected override void InitializeForm()
        {
            //_BLL = new bllMXB高血压管理卡();// 业务逻辑层实例
            _SummaryView = new DevGridView(gv档案);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gv档案);
            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                txt机构.Properties.ValueMember = "机构编号";
                txt机构.Properties.DisplayMember = "机构名称";

                txt机构.Properties.TreeList.KeyFieldName = "机构编号";
                txt机构.Properties.TreeList.ParentFieldName = "上级机构";
                txt机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                txt机构.Properties.DataSource = dt所属机构;

                txt机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                txt机构.Text = Loginer.CurrentUser.所属机构;
            }
        }

        bll健康体检 _bll = new bll健康体检();
        private void btnQuery_Click(object sender, EventArgs e)
        {
            string searchBy = this.cbo查询标准.Text.Trim() == "录入时间" ? "1" : "2";
            _type = "1";
            string jigoubianma = this.txt机构.EditValue.ToString();
            DataSet ds = _bll.GetReportDataDetail(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "11","", searchBy);
            if (ds != null && ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    //ds.Tables[0].Rows[i]["性别"] = _bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    //ds.Tables[0].Rows[i]["创建人"] = _bll.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                    //////ds.Tables[0].Rows[i]["创建机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["创建机构"].ToString());
                    //ds.Tables[0].Rows[i]["所属机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                    //ds.Tables[0].Rows[i]["居住地址"] = _bll.Return地区名称(ds.Tables[0].Rows[i]["街道"].ToString()) + _bll.Return地区名称(ds.Tables[0].Rows[i]["居委会"].ToString()) + ds.Tables[0].Rows[i]["居住地址"].ToString();
                }
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }


        private void link档案号_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv档案.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            string id = row["创建时间"].ToString();
            //_BLL.GetBusinessByKey(个人档案编号, true);//下载一个空业务单据            
            //_BLL.NewBusiness(); //增加一条主表记录
            frm个人健康 frm = null;
            if (_type == "1")
            {
                frm = new frm个人健康(false, "frm健康体检表", 家庭档案编号, 个人档案编号, id);
            }
            else if (_type == "2")
            {
                frm = new frm个人健康(false, "", 家庭档案编号, 个人档案编号, id);
            }
            frm.ShowDialog();
        }
    }
}
