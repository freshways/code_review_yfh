﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Interfaces;
using AtomEHR.公共卫生.Module.个人健康;
using DevExpress.XtraEditors.Repository;

namespace AtomEHR.公共卫生.Module
{
    public partial class frm健康体检表_健康人 : AtomEHR.Library.frmBaseDataForm
    {
        //定义此类型，用来判断是点击那个链接跳转进档案grid的
        private string _type = "1";
        public frm健康体检表_健康人()
        {
            InitializeComponent();
        }

        private void frm健康体检表_健康人_Load(object sender, EventArgs e)
        {
            if (Loginer.CurrentUser.所属机构.Length >= 12)
                txt机构.Properties.AutoExpandAllNodes = true;
            else { txt机构.Properties.AutoExpandAllNodes = false; }
            this.InitializeForm();
            string m_ServerDate = _bll.ServiceDateTime;
            try
            {
                this.dte开始1.Text = Convert.ToDateTime(m_ServerDate).ToString("yyyy-MM-01");
                this.dte结束2.Text = Convert.ToDateTime(m_ServerDate).ToString("yyyy-MM-dd");
            }
            catch
            {
                this.dte开始1.Text = DateTime.Now.ToString("yyyy-MM-01");
                this.dte结束2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }

            this.gcSummary.Dock = DockStyle.Fill;
            this.gcSummary.BringToFront();
            this.gc档案.Dock = DockStyle.Fill;

        }

        protected override void InitializeForm()
        {
            //_BLL = new bllMXB高血压管理卡();// 业务逻辑层实例
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gvSummary);
            frmGridCustomize.RegisterGrid(gv档案);
            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                txt机构.Properties.ValueMember = "机构编号";
                txt机构.Properties.DisplayMember = "机构名称";

                txt机构.Properties.TreeList.KeyFieldName = "机构编号";
                txt机构.Properties.TreeList.ParentFieldName = "上级机构";
                txt机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                txt机构.Properties.DataSource = dt所属机构;

                txt机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                txt机构.Text = Loginer.CurrentUser.所属机构;
            }
        }

        bll健康体检 _bll = new bll健康体检();
        private void btnQuery_Click(object sender, EventArgs e)
        {
            string searchBy = this.cbo查询标准.Text.Trim() == "录入时间" ? "1" : "2";
            DataSet ds = _bll.GetReportDataDetail(txt机构.EditValue.ToString(), txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "1", "3", searchBy);
            if (ds != null && ds.Tables.Count > 0)
            {
                gcSummary.DataSource = ds.Tables[0];
                this.gcSummary.BringToFront();
                this.gvSummary.BestFitColumns();
            }
        }

        private void link区域名称_Click(object sender, EventArgs e)
        {
            string searchBy = this.cbo查询标准.Text.Trim() == "录入时间" ? "1" : "2";
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();
            DataSet ds = _bll.GetReportDataDetail(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "1", "1", searchBy);
            if (ds != null && ds.Tables.Count > 0)
            {
                gcSummary.DataSource = ds.Tables[0];
                this.gvSummary.BestFitColumns();
                this.gcSummary.BringToFront();
            }
        }

        private void link总数_Click(object sender, EventArgs e)
        {
            string searchBy = this.cbo查询标准.Text.Trim() == "录入时间" ? "1" : "2";
            _type = "1";
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();
            DataSet ds = _bll.GetReportDataDetail(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "2", "1", searchBy);
            if (ds != null && ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    //ds.Tables[0].Rows[i]["性别"] = _bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    //ds.Tables[0].Rows[i]["创建人"] = _bll.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                    ////ds.Tables[0].Rows[i]["创建机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["创建机构"].ToString());
                    //ds.Tables[0].Rows[i]["所属机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                }
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        private void link不合格数_Click(object sender, EventArgs e)
        {
            string searchBy = this.cbo查询标准.Text.Trim() == "录入时间" ? "1" : "2";
            _type = "1";
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();
            DataSet ds = _bll.GetReportDataDetail(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "4", "1", searchBy);
            if (ds != null && ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    //ds.Tables[0].Rows[i]["性别"] = _bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    //ds.Tables[0].Rows[i]["创建人"] = _bll.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                    ////ds.Tables[0].Rows[i]["创建机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["创建机构"].ToString());
                    //ds.Tables[0].Rows[i]["所属机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                }
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        private void link合格数_Click(object sender, EventArgs e)
        {
            string searchBy = this.cbo查询标准.Text.Trim() == "录入时间" ? "1" : "2";
            _type = "1";
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();
            DataSet ds = _bll.GetReportDataDetail(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "3", "1", searchBy);
            if (ds != null && ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    //ds.Tables[0].Rows[i]["性别"] = _bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    //ds.Tables[0].Rows[i]["创建人"] = _bll.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                    ////ds.Tables[0].Rows[i]["创建机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["创建机构"].ToString());
                    //ds.Tables[0].Rows[i]["所属机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                }
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        private void link档案号_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv档案.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            string id = row["创建时间"].ToString();
            //_BLL.GetBusinessByKey(个人档案编号, true);//下载一个空业务单据            
            //_BLL.NewBusiness(); //增加一条主表记录
            frm个人健康 frm = null;
            if (_type == "1")
            {
                frm = new frm个人健康(false, "frm健康体检表", 家庭档案编号, 个人档案编号, id);
            }
            else if (_type == "2")
            {
                frm = new frm个人健康(false, "", 家庭档案编号, 个人档案编号, id);
            }
            frm.ShowDialog();
        }

        private void gvSummary_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            DataRowView row = gvSummary.GetRow(e.RowHandle) as DataRowView;
            if (row == null) return;
            //if (e.Column == col总数)
            //{
            //    string zongshu = row["总数"].ToString();
            //    if (!string.IsNullOrEmpty(zongshu) && zongshu != "0")
            //    {
            //        RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
            //        link.Click += link总数_Click;
            //        e.RepositoryItem = link;
            //    }
            //}

            //if (e.Column == col合格数)
            //{
            //    string hegeshu = row["合格数"].ToString();
            //    if (!string.IsNullOrEmpty(hegeshu) && hegeshu != "0")
            //    {
            //        RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
            //        link.Click += link合格数_Click;
            //        e.RepositoryItem = link;
            //    }
            //}
            //if (e.Column == col不合格数)
            //{
            //    string buhegeshu = row["不合格数"].ToString();
            //    if (!string.IsNullOrEmpty(buhegeshu) && buhegeshu != "0")
            //    {
            //        RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
            //        link.Click += link不合格数_Click;
            //        e.RepositoryItem = link;
            //    }
            //}
            //if (e.Column == col健康人已体检)
            //{
            //    string count = row["健康人已体检"].ToString();
            //    if (!string.IsNullOrEmpty(count) && count != "0")
            //    {
            //        RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
            //        link.Click += link健康人已体检_Click;
            //        e.RepositoryItem = link;
            //    }
            //}
            //if (e.Column == col健康人未体检)
            //{
            //    string count = row["健康人未体检"].ToString();
            //    if (!string.IsNullOrEmpty(count) && count != "0")
            //    {
            //        RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
            //        link.Click += link健康人未体检_Click;
            //        e.RepositoryItem = link;
            //    }
            //}
        }

        private void link健康人未体检_Click(object sender, EventArgs e)
        {
            string searchBy = this.cbo查询标准.Text.Trim() == "录入时间" ? "1" : "2";
            _type = "2";
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();
            DataSet ds = _bll.GetReportDataDetail(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "12", "", searchBy);
            if (ds != null && ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    //ds.Tables[0].Rows[i]["性别"] = _bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    //ds.Tables[0].Rows[i]["创建人"] = _bll.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                    //////ds.Tables[0].Rows[i]["创建机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["创建机构"].ToString());
                    //ds.Tables[0].Rows[i]["所属机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                    //ds.Tables[0].Rows[i]["居住地址"] = _bll.Return地区名称(ds.Tables[0].Rows[i]["街道"].ToString()) + _bll.Return地区名称(ds.Tables[0].Rows[i]["居委会"].ToString()) + ds.Tables[0].Rows[i]["居住地址"].ToString();
                }
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        private void link健康人已体检_Click(object sender, EventArgs e)
        {
            string searchBy = this.cbo查询标准.Text.Trim() == "录入时间" ? "1" : "2";
            _type = "1";
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();
            DataSet ds = _bll.GetReportDataDetail(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "11", "", searchBy);
            if (ds != null && ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    //ds.Tables[0].Rows[i]["性别"] = _bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    //ds.Tables[0].Rows[i]["创建人"] = _bll.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                    //////ds.Tables[0].Rows[i]["创建机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["创建机构"].ToString());
                    //ds.Tables[0].Rows[i]["所属机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                    //ds.Tables[0].Rows[i]["居住地址"] = _bll.Return地区名称(ds.Tables[0].Rows[i]["街道"].ToString()) + _bll.Return地区名称(ds.Tables[0].Rows[i]["居委会"].ToString()) + ds.Tables[0].Rows[i]["居住地址"].ToString();
                }
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        private void link糖尿病已体检_Click(object sender, EventArgs e)
        {
            _type = "1";
            string searchBy = this.cbo查询标准.Text.Trim() == "录入时间" ? "1" : "2";
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();
            DataSet ds = _bll.GetReportDataDetail(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "9", "", searchBy);
            if (ds != null && ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    //ds.Tables[0].Rows[i]["性别"] = _bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    //ds.Tables[0].Rows[i]["创建人"] = _bll.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                    ////ds.Tables[0].Rows[i]["创建机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["创建机构"].ToString());
                    //ds.Tables[0].Rows[i]["所属机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                }
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        private void link糖尿病未体检_Click(object sender, EventArgs e)
        {
            _type = "2";
            string searchBy = this.cbo查询标准.Text.Trim() == "录入时间" ? "1" : "2";
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();
            DataSet ds = _bll.GetReportDataDetail(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "10", "", searchBy);
            if (ds != null && ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    //ds.Tables[0].Rows[i]["性别"] = _bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    //ds.Tables[0].Rows[i]["创建人"] = _bll.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                    ////ds.Tables[0].Rows[i]["创建机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["创建机构"].ToString());
                    //ds.Tables[0].Rows[i]["所属机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                }
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        private void link高血压已体检_Click(object sender, EventArgs e)
        {
            _type = "1";
            string searchBy = this.cbo查询标准.Text.Trim() == "录入时间" ? "1" : "2";
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();
            DataSet ds = _bll.GetReportDataDetail(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "7", "", searchBy);
            if (ds != null && ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    //ds.Tables[0].Rows[i]["性别"] = _bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    //ds.Tables[0].Rows[i]["创建人"] = _bll.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                    ////ds.Tables[0].Rows[i]["创建机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["创建机构"].ToString());
                    //ds.Tables[0].Rows[i]["所属机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                }
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        private void link高血压未体检_Click(object sender, EventArgs e)
        {
            _type = "2";
            string searchBy = this.cbo查询标准.Text.Trim() == "录入时间" ? "1" : "2";
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();
            DataSet ds = _bll.GetReportDataDetail(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "8", "", searchBy);
            if (ds != null && ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    //ds.Tables[0].Rows[i]["性别"] = _bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    //ds.Tables[0].Rows[i]["创建人"] = _bll.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                    ////ds.Tables[0].Rows[i]["创建机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["创建机构"].ToString());
                    //ds.Tables[0].Rows[i]["所属机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                }
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        private void link老年人已体检_Click(object sender, EventArgs e)
        {
            _type = "1";
            string searchBy = this.cbo查询标准.Text.Trim() == "录入时间" ? "1" : "2";
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();
            DataSet ds = _bll.GetReportDataDetail(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "5", "", searchBy);
            if (ds != null && ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    //ds.Tables[0].Rows[i]["性别"] = _bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    //ds.Tables[0].Rows[i]["创建人"] = _bll.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                    ////ds.Tables[0].Rows[i]["创建机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["创建机构"].ToString());
                    //ds.Tables[0].Rows[i]["所属机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                }
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        void link老年人未体检_Click(object sender, EventArgs e)
        {
            _type = "2";
            string searchBy = this.cbo查询标准.Text.Trim() == "录入时间" ? "1" : "2";
            string jigoubianma = this.gvSummary.GetFocusedRowCellValue("区域编码").ToString();
            DataSet ds = _bll.GetReportDataDetail(jigoubianma, txt完整度.Text, dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"), "6", "", searchBy);
            if (ds != null && ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                    //ds.Tables[0].Rows[i]["性别"] = _bll.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
                    //ds.Tables[0].Rows[i]["创建人"] = _bll.Return用户名称(ds.Tables[0].Rows[i]["创建人"].ToString());
                    ////ds.Tables[0].Rows[i]["创建机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["创建机构"].ToString());
                    //ds.Tables[0].Rows[i]["所属机构"] = _bll.Return机构名称(ds.Tables[0].Rows[i]["所属机构"].ToString());
                }
                gc档案.DataSource = ds.Tables[0];
                this.gv档案.BestFitColumns();
                this.gc档案.BringToFront();
            }
        }

        private int iAll = 1;
        private int iHege = 0;
        private void gvSummary_CustomDrawFooterCell(object sender, DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventArgs e)
        {
            //if (e.Column == col总数)
            //{
            //    iAll = 1;
            //    try
            //    {
            //        int temp = Convert.ToInt32(e.Column.SummaryText.Replace(",", ""));
            //        iAll = (temp == 0) ? 1 : temp;
            //    }
            //    catch
            //    { }
            //}
            //else if (e.Column == col合格数)
            //{
            //    iHege = 1;
            //    try
            //    {
            //        int temp = Convert.ToInt32(e.Column.SummaryText.Replace(",", ""));
            //        iHege = temp;
            //    }
            //    catch
            //    { }
            //}
            //else
            //{ }
        }

        private void btnEmpty_Click(object sender, EventArgs e)
        {
            this.gcSummary.BringToFront();
        }

    }
}
