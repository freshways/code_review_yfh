﻿using AtomEHR.Business.Security;
using AtomEHR.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module
{
    //本窗口仅对局级账号开放权限
    public partial class Frm妇保院工作量统计 : AtomEHR.Library.frmBaseDataForm
    {
        public Frm妇保院工作量统计()
        {
            InitializeComponent();
        }

        AtomEHR.Business.bll孕妇_产前随访2次 bll2次 = new Business.bll孕妇_产前随访2次();
        private void btnQuery_Click(object sender, EventArgs e)
        {
            if(cbo录入人.EditValue==null || string.IsNullOrWhiteSpace(cbo录入人.EditValue.ToString()))
            {
                Msg.ShowInformation("请选择创建人");
                return;
            }

            try
            {
                DataSet ds = bll2次.Get妇保院工作量(txt姓名.Text.Trim(), txt身份证号.Text.Trim(), txt档案编号.Text.Trim(), cbo录入人.EditValue.ToString(), dteSf1.Text, dteSf2.Text, dteMcyj1.Text, dteMcyj2.Text);
                this.gcDetails.DataSource = ds.Tables[0];
                this.gcSummary.DataSource = ds.Tables[1];

                if (gvDetails.DataRowCount > 0)
                {
                    if (((DevExpress.XtraGrid.GridSummaryItemCollection)(gvDetails.Columns[0].Summary)).ActiveCount == 0)
                    {
                        gvDetails.Columns[0].Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "个人档案编号", "人次合计：{0}")});
                        //this.gcDetails.Refresh();
                    }
                }

                if (gvSummary.DataRowCount > 0)
                {
                    if (((DevExpress.XtraGrid.GridSummaryItemCollection)(gvSummary.Columns[0].Summary)).ActiveCount == 0)
                    {
                        gvSummary.Columns[0].Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
                            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "个人档案编号", "人数合计：{0}")});

                        //if (ds.Tables[1].Columns.Contains("随访次数"))
                        //{
                        //    gvSummary.Columns["随访次数"].Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
                        //        new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "随访次数", "随访次数：{0}")
                        //    });
                        //}
                        //this.gcSummary.Refresh();
                    }
                }

                //gvDetails.PopulateColumns();
                //gvSummary.PopulateColumns();
            }
            catch(Exception ex)
            {
                Msg.ShowInformation(ex.Message);
            }
        }

        private void Frm妇保院工作量统计_Load(object sender, EventArgs e)
        {
            try
            {
                DataTable dt录入人 = new bllUser().GetUserNameAndIDBy机构用户(Loginer.CurrentUser.所属机构);
                cbo录入人.Properties.DisplayMember = "UserName";
                cbo录入人.Properties.ValueMember = "用户编码";
                cbo录入人.Properties.DataSource = dt录入人;

                cbo录入人.EditValue = "3713230054";
            }
            catch (Exception ex)
            {
                cbo录入人.Text = "请选择...";
            }
        }

        private void gvSummary_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator && e.RowHandle > -1)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        private void gvDetails_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator && e.RowHandle > -1)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        SaveFileDialog sfd = new SaveFileDialog();
        private void sbtn导出汇总_Click(object sender, EventArgs e)
        {
            sfd.Filter = "电子表格|*.xls";
            sfd.FileName = "";
            if(sfd.ShowDialog()== System.Windows.Forms.DialogResult.OK)
            {
                this.gvSummary.ExportToXls(sfd.FileName);
            }
        }

        private void sbtn导出明细_Click(object sender, EventArgs e)
        {
            sfd.Filter = "电子表格|*.xls";
            sfd.FileName = "";
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.gvDetails.ExportToXls(sfd.FileName);
            }
        }
    }
}
