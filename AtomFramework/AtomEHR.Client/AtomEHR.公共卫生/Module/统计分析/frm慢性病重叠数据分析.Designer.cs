﻿namespace AtomEHR.公共卫生.Module
{
    partial class frm慢性病重叠数据分析
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraCharts.SimpleDiagram simpleDiagram1 = new DevExpress.XtraCharts.SimpleDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PieSeriesLabel pieSeriesLabel1 = new DevExpress.XtraCharts.PieSeriesLabel();
            DevExpress.XtraCharts.PiePointOptions piePointOptions1 = new DevExpress.XtraCharts.PiePointOptions();
            DevExpress.XtraCharts.PiePointOptions piePointOptions2 = new DevExpress.XtraCharts.PiePointOptions();
            DevExpress.XtraCharts.SeriesPoint seriesPoint1 = new DevExpress.XtraCharts.SeriesPoint("AA", new object[] {
            ((object)(12D))}, 0);
            DevExpress.XtraCharts.SeriesPoint seriesPoint2 = new DevExpress.XtraCharts.SeriesPoint("BB", new object[] {
            ((object)(13D))}, 1);
            DevExpress.XtraCharts.SeriesPoint seriesPoint3 = new DevExpress.XtraCharts.SeriesPoint("CC", new object[] {
            ((object)(21D))}, 2);
            DevExpress.XtraCharts.SeriesPoint seriesPoint4 = new DevExpress.XtraCharts.SeriesPoint("DD", new object[] {
            ((object)(31D))}, 3);
            DevExpress.XtraCharts.PieSeriesView pieSeriesView1 = new DevExpress.XtraCharts.PieSeriesView(new int[] {
            3,
            2,
            1,
            0});
            DevExpress.XtraCharts.SeriesTitle seriesTitle1 = new DevExpress.XtraCharts.SeriesTitle();
            DevExpress.XtraCharts.PieSeriesLabel pieSeriesLabel2 = new DevExpress.XtraCharts.PieSeriesLabel();
            DevExpress.XtraCharts.PiePointOptions piePointOptions3 = new DevExpress.XtraCharts.PiePointOptions();
            DevExpress.XtraCharts.PiePointOptions piePointOptions4 = new DevExpress.XtraCharts.PiePointOptions();
            DevExpress.XtraCharts.PieSeriesView pieSeriesView2 = new DevExpress.XtraCharts.PieSeriesView();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm慢性病重叠数据分析));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt人群 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt分母 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt单纯 = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.txt总人数 = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.txt重叠 = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.txt分子 = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnBinding = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt人群.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt分母.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt单纯.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt总人数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt重叠.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt分子.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.groupControl3);
            this.groupControl1.Controls.Add(this.groupControl2);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(825, 561);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "公共卫生>统计分析";
            // 
            // groupControl3
            // 
            this.groupControl3.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl3.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupControl3.Controls.Add(this.chartControl1);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(202, 22);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(621, 537);
            this.groupControl3.TabIndex = 1;
            this.groupControl3.Text = "统计图表";
            // 
            // chartControl1
            // 
            simpleDiagram1.EqualPieSize = false;
            this.chartControl1.Diagram = simpleDiagram1;
            this.chartControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl1.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Left;
            this.chartControl1.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.BottomOutside;
            this.chartControl1.Legend.Direction = DevExpress.XtraCharts.LegendDirection.LeftToRight;
            this.chartControl1.Location = new System.Drawing.Point(2, 22);
            this.chartControl1.Name = "chartControl1";
            pieSeriesLabel1.Antialiasing = true;
            piePointOptions1.PointView = DevExpress.XtraCharts.PointView.Argument;
            piePointOptions1.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Number;
            pieSeriesLabel1.PointOptions = piePointOptions1;
            series1.Label = pieSeriesLabel1;
            piePointOptions2.ArgumentNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Number;
            piePointOptions2.PercentOptions.ValueAsPercent = false;
            piePointOptions2.PointView = DevExpress.XtraCharts.PointView.ArgumentAndValues;
            piePointOptions2.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Number;
            piePointOptions2.ValueNumericOptions.Precision = 0;
            series1.LegendPointOptions = piePointOptions2;
            series1.Name = "Series 1";
            series1.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint1,
            seriesPoint2,
            seriesPoint3,
            seriesPoint4});
            series1.SynchronizePointOptions = false;
            pieSeriesView1.ExplodeMode = DevExpress.XtraCharts.PieExplodeMode.UsePoints;
            pieSeriesView1.RuntimeExploding = false;
            seriesTitle1.Text = "标题";
            pieSeriesView1.Titles.AddRange(new DevExpress.XtraCharts.SeriesTitle[] {
            seriesTitle1});
            series1.View = pieSeriesView1;
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1};
            piePointOptions3.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.General;
            pieSeriesLabel2.PointOptions = piePointOptions3;
            this.chartControl1.SeriesTemplate.Label = pieSeriesLabel2;
            piePointOptions4.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Number;
            this.chartControl1.SeriesTemplate.LegendPointOptions = piePointOptions4;
            this.chartControl1.SeriesTemplate.SynchronizePointOptions = false;
            pieSeriesView2.RuntimeExploding = false;
            this.chartControl1.SeriesTemplate.View = pieSeriesView2;
            this.chartControl1.Size = new System.Drawing.Size(617, 513);
            this.chartControl1.TabIndex = 0;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.label4);
            this.groupControl2.Controls.Add(this.label7);
            this.groupControl2.Controls.Add(this.label3);
            this.groupControl2.Controls.Add(this.txt人群);
            this.groupControl2.Controls.Add(this.txt分母);
            this.groupControl2.Controls.Add(this.txt单纯);
            this.groupControl2.Controls.Add(this.label5);
            this.groupControl2.Controls.Add(this.txt总人数);
            this.groupControl2.Controls.Add(this.label6);
            this.groupControl2.Controls.Add(this.txt重叠);
            this.groupControl2.Controls.Add(this.label2);
            this.groupControl2.Controls.Add(this.txt分子);
            this.groupControl2.Controls.Add(this.label1);
            this.groupControl2.Controls.Add(this.comboBoxEdit1);
            this.groupControl2.Controls.Add(this.btnBinding);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl2.Location = new System.Drawing.Point(2, 22);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(200, 537);
            this.groupControl2.TabIndex = 0;
            this.groupControl2.Text = "统计条件";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label4.Location = new System.Drawing.Point(9, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "分子：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label7.Location = new System.Drawing.Point(7, 117);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 17);
            this.label7.TabIndex = 9;
            this.label7.Text = "人群：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label3.Location = new System.Drawing.Point(9, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "分母：";
            // 
            // txt人群
            // 
            this.txt人群.EditValue = "不限年龄";
            this.txt人群.Location = new System.Drawing.Point(62, 114);
            this.txt人群.Name = "txt人群";
            this.txt人群.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txt人群.Properties.Appearance.Options.UseFont = true;
            this.txt人群.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt人群.Properties.Items.AddRange(new object[] {
            "不限年龄",
            "65岁以上老年人",
            "65岁以下人群"});
            this.txt人群.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txt人群.Size = new System.Drawing.Size(125, 22);
            this.txt人群.TabIndex = 8;
            // 
            // txt分母
            // 
            this.txt分母.Location = new System.Drawing.Point(62, 46);
            this.txt分母.Name = "txt分母";
            this.txt分母.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txt分母.Properties.Appearance.Options.UseFont = true;
            this.txt分母.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt分母.Properties.Items.AddRange(new object[] {
            "65岁以上老年人",
            "65岁以下人群",
            "高血压",
            "糖尿病",
            "冠心病",
            "脑卒中"});
            this.txt分母.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txt分母.Size = new System.Drawing.Size(125, 22);
            this.txt分母.TabIndex = 8;
            this.txt分母.SelectedIndexChanged += new System.EventHandler(this.txt分母_SelectedIndexChanged);
            // 
            // txt单纯
            // 
            this.txt单纯.Location = new System.Drawing.Point(60, 382);
            this.txt单纯.Name = "txt单纯";
            this.txt单纯.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.txt单纯.Properties.Appearance.ForeColor = System.Drawing.Color.Green;
            this.txt单纯.Properties.Appearance.Options.UseFont = true;
            this.txt单纯.Properties.Appearance.Options.UseForeColor = true;
            this.txt单纯.Size = new System.Drawing.Size(125, 22);
            this.txt单纯.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label5.ForeColor = System.Drawing.Color.Green;
            this.label5.Location = new System.Drawing.Point(9, 362);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "{0}人数:";
            // 
            // txt总人数
            // 
            this.txt总人数.Location = new System.Drawing.Point(60, 270);
            this.txt总人数.Name = "txt总人数";
            this.txt总人数.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.txt总人数.Properties.Appearance.ForeColor = System.Drawing.Color.Green;
            this.txt总人数.Properties.Appearance.Options.UseFont = true;
            this.txt总人数.Properties.Appearance.Options.UseForeColor = true;
            this.txt总人数.Size = new System.Drawing.Size(125, 22);
            this.txt总人数.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label6.ForeColor = System.Drawing.Color.Green;
            this.label6.Location = new System.Drawing.Point(9, 250);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "总人数:";
            // 
            // txt重叠
            // 
            this.txt重叠.Location = new System.Drawing.Point(60, 327);
            this.txt重叠.Name = "txt重叠";
            this.txt重叠.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.txt重叠.Properties.Appearance.ForeColor = System.Drawing.Color.Green;
            this.txt重叠.Properties.Appearance.Options.UseFont = true;
            this.txt重叠.Properties.Appearance.Options.UseForeColor = true;
            this.txt重叠.Size = new System.Drawing.Size(125, 22);
            this.txt重叠.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label2.ForeColor = System.Drawing.Color.Green;
            this.label2.Location = new System.Drawing.Point(9, 307);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "重叠人数:";
            // 
            // txt分子
            // 
            this.txt分子.EditValue = "1; 2; 3; 4";
            this.txt分子.Location = new System.Drawing.Point(62, 80);
            this.txt分子.Name = "txt分子";
            this.txt分子.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txt分子.Properties.Appearance.Options.UseFont = true;
            this.txt分子.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt分子.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.CheckedListBoxItem[] {
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("1", "高血压", System.Windows.Forms.CheckState.Checked),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("2", "糖尿病", System.Windows.Forms.CheckState.Checked),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("3", "冠心病", System.Windows.Forms.CheckState.Checked),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("4", "脑卒中", System.Windows.Forms.CheckState.Checked)});
            this.txt分子.Properties.SelectAllItemCaption = "(全选)";
            this.txt分子.Properties.SeparatorChar = ';';
            this.txt分子.Size = new System.Drawing.Size(125, 22);
            this.txt分子.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label1.Location = new System.Drawing.Point(5, 217);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 14);
            this.label1.TabIndex = 4;
            this.label1.Text = "显示方式：";
            // 
            // comboBoxEdit1
            // 
            this.comboBoxEdit1.Location = new System.Drawing.Point(78, 215);
            this.comboBoxEdit1.Name = "comboBoxEdit1";
            this.comboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit1.Size = new System.Drawing.Size(109, 20);
            this.comboBoxEdit1.TabIndex = 3;
            this.comboBoxEdit1.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit1_SelectedIndexChanged);
            // 
            // btnBinding
            // 
            this.btnBinding.Image = ((System.Drawing.Image)(resources.GetObject("btnBinding.Image")));
            this.btnBinding.Location = new System.Drawing.Point(60, 153);
            this.btnBinding.Name = "btnBinding";
            this.btnBinding.Size = new System.Drawing.Size(81, 28);
            this.btnBinding.TabIndex = 0;
            this.btnBinding.Text = "统计查询";
            this.btnBinding.Click += new System.EventHandler(this.btnBinding_Click);
            // 
            // frm慢性病重叠数据分析
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 561);
            this.Controls.Add(this.groupControl1);
            this.Name = "frm慢性病重叠数据分析";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "慢性病重叠数据分析";
            this.Load += new System.EventHandler(this.frm慢性病重叠数据分析_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt人群.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt分母.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt单纯.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt总人数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt重叠.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt分子.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraCharts.ChartControl chartControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.ComboBoxEdit txt人群;
        private DevExpress.XtraEditors.ComboBoxEdit txt分母;
        private DevExpress.XtraEditors.TextEdit txt单纯;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.TextEdit txt总人数;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit txt重叠;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.CheckedComboBoxEdit txt分子;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit1;
        private DevExpress.XtraEditors.SimpleButton btnBinding;
    }
}