﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Interfaces;
using AtomEHR.公共卫生.Module.个人健康;
using DevExpress.XtraEditors.Repository;

namespace AtomEHR.公共卫生.Module
{
    public partial class Frm孕产妇公卫管理率 : AtomEHR.Library.frmBaseDataForm
    {
        DataSet ds = null;
        private string strWhere = null;
        public Frm孕产妇公卫管理率()
        {
            InitializeComponent();
        }

        private void 孕产妇系统管理情况登记簿_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
        }

        protected override void InitializeForm()
        {
            //_BLL = new bllMXB高血压管理卡();// 业务逻辑层实例
            _SummaryView = new DevGridView(bandedGridView1);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(bandedGridView1);
        }


        //查询按钮
        private void btnQuery_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection("Data Source=192.168.10.57;Initial Catalog=JTK;User ID=yggsuser;Password=yggsuser;");
                try
                {
                    System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "Pro_GWvsWis_WomenVS";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@begin1", dateTimePickerYBegin.Value.ToString("yyyy-MM-dd")));
                    cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@end1", dateTimePickerYEnd.Value.ToString("yyyy-MM-dd")));
                    cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@begin2", dateTimePickerEBegin.Value.ToString("yyyy-MM-dd")));
                    cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@end2", dateTimePickerEEnd.Value.ToString("yyyy-MM-dd")));
                    System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter(cmd);
                    DataSet dsTemp = new DataSet();
                    adapter.Fill(dsTemp);
                    ds = dsTemp;

                    if (ds != null && ds.Tables.Count > 0)
                    {
                        if (!ds.Tables[0].Columns.Contains("公卫管理率"))
                        {
                            ds.Tables[0].Columns.Add("公卫管理率");
                            for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                            {
                                string wis = ds.Tables[0].Rows[index]["WIS总数"].ToString();
                                int wiscount = wis == "" ? 0 : Convert.ToInt32(wis);
                                string gw = ds.Tables[0].Rows[index]["公卫管理数"].ToString();
                                int gwcount = gw == "" ? 0 : Convert.ToInt32(gw);

                                ds.Tables[0].Rows[index]["公卫管理率"] = gwcount == 0 ? "0.00%" : (gwcount * 100.0 / wiscount).ToString("N2")+"%";
                            }
                        }

                        gcSummary.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        gcSummary.DataSource = null;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (conn.State != ConnectionState.Closed)
                    {
                        conn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "异常提示");
            }
        }
        private DataSet GetData(string prgid, string type)
        {
            return ds;
        }

        private int iwiscount = 1;
        private int igwcount = 0;
        private void bandedGridView1_CustomDrawFooterCell(object sender, DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventArgs e)
        {
            if (e.Column == col公卫管理率)
            {
                e.Info.DisplayText = (igwcount * 100.0 / iwiscount).ToString("N2") + "%";
            }
            else if (e.Column == colwis总数)
            {
                iwiscount = 1;
                try
                {
                    int temp = Convert.ToInt32(e.Column.SummaryText.Replace(",", ""));
                    iwiscount = (temp == 0) ? 1 : temp;
                }
                catch
                { }
            }
            else if (e.Column == col公卫管理数)
            {
                igwcount = 0;
                try
                {
                    int temp = Convert.ToInt32(e.Column.SummaryText.Replace(",", ""));
                    igwcount = temp;
                }
                catch
                { }
            }
            else
            { }
        }

        private void sbtnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog dig = new SaveFileDialog();
            dig.Filter = "EXCEL 2007 表格|*.xlsx|EXCEL 2003 表格|*.xls";
            DialogResult result = dig.ShowDialog();
            if (result == DialogResult.OK)
            {
                if (dig.FileName[dig.FileName.Length - 1] == 'x')
                {
                    this.bandedGridView1.ExportToXlsx(dig.FileName);
                }
                else
                {
                    this.bandedGridView1.ExportToXls(dig.FileName);
                }
            }
        }
    }
}
