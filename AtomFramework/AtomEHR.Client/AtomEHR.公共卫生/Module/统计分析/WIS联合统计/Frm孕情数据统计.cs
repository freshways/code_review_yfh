﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using AtomEHR.Library;

namespace AtomEHR.公共卫生.Module.统计分析.WIS联合统计
{
    public partial class Frm孕情数据统计 : AtomEHR.Library.frmBaseDataForm
    {
        string connstr = "Data Source=192.168.10.57;Initial Catalog=JTK;User ID=yggsuser;Password=yggsuser;";
        public Frm孕情数据统计()
        {
            InitializeComponent();

            frmGridCustomize.RegisterGrid(gridView1);
        }

        DataSet ds = new DataSet();
        public void BindXZList()
        {
            ds = new DataSet();
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connstr))
            {
                conn.Open();
                SqlDataAdapter ad = new SqlDataAdapter("SELECT SUBSTRING([dwdm],1,9) dwdm,[dwmc] FROM [dbo].[乡镇代码] ORDER BY dwdm ",conn);
                ad.Fill(ds);
            }

            this.lookUpEditXZ.Properties.ValueMember = "dwdm";
            this.lookUpEditXZ.Properties.DisplayMember = "dwmc";
            this.lookUpEditXZ.Properties.DataSource = ds.Tables[0];
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            if (this.lookUpEditXZ.EditValue == null)
            {
                MessageBox.Show("请选择乡镇！");
                return;
            }
            string xz = this.lookUpEditXZ.EditValue.ToString();
            string name = this.txtName.Text.Trim();
            string sfzh = this.txtSfzh.Text.Trim();
            string mcjybegin = this.dateTimePickerYBegin.Value.ToString("yyyy-MM-dd");
            string mcjyend = this.dateTimePickerYEnd.Value.AddDays(1).ToString("yyyy-MM-dd");
            
            try
            {
                DataTable dt = GetYunJianBiDuiJieGuo(mcjybegin, mcjyend, "1", xz, name, sfzh);
                this.gcSummary.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FrmWis孕产妇信息查询_Load(object sender, EventArgs e)
        {
            BindXZList();
        }


        public DataTable GetYunJianBiDuiJieGuo(string dateBegin, string dateEnd, string bdjg, string dw, string xm, string sfzh)
        {
            StringBuilder sql = new StringBuilder();
            #region sql语句
            sql.Append(@"select '' bh,aa.*,cc.*,''yz,''bz1,--''bdjg,
       bb.[姓名] xm
      ,bb.[身份证号] sfzh
      ,bb.[家庭地址] jtdz
      ,bb.[门诊单据号] mzdjh
      ,convert(varchar(20),bb.[问诊末次月经日期],23) wzmcyjrq
      ,bb.[门诊医师] mzys
      ,bb.[B超诊断结果] bczdjg
      ,bb.[B超诊断推算孕周] bczdtsyz
      ,bb.[B超报告单号] bcbbdh
      ,convert(varchar(20),bb.[B超诊断时间],23) bczdsj
      ,bb.[B超医师] bcys
      ,bb.[备注] bz2
      ,dd.单位名称 ctyy
      --,bb.[比对结果] bdjg
      ,ff.bdjgname bdjg
      ,ee.dwmc
      ,bb.乡镇
 from
(
select [姓名]
      ,[性别]
      ,[身份证号]
      ,[家庭地址]
      ,[门诊单据号]
      ,[问诊末次月经日期]
      ,[门诊医师]
      ,[B超诊断结果]
      ,[B超诊断推算孕周]
      ,[B超报告单号]
      ,[B超诊断时间]
      ,[B超医师]
      ,[备注]
      ,[单位编码]
      ,[比对结果]
      ,[乡镇]
from view孕检数据 where [B超诊断时间] >= @dateBegin and [B超诊断时间] < @dateEnd --and ([身份证号] !='' and [身份证号] is not null)
) bb 
left outer join
(
	SELECT fnbm,dwdm dw,dwmc cj,fnxm nvfxm,fnsfhm nvfsfzh,fnhjd nvfhjd,zfxm nanfxm,zfsfhm nanfsfzh,zfhjd nanfhjd--,''预产期,''孕周,''上报时间,''备注
	FROM  qhry
	where tcrq is null
) aa on (aa.nvfsfzh = bb.[身份证号] and bb.[身份证号]!='')
left outer join 
(
	SELECT id rsid,fnbm rsfnbm,convert(varchar(20),mcyjrq,23) mcyj,convert(varchar(20),ycq,23) ycq,convert(varchar(20),sbrq,23) sbsj
	FROM  rs
	where rszzrq is null
) cc on aa.fnbm = cc.rsfnbm
left outer join pub_单位编码 dd on bb.[单位编码] = dd.单位编码
left outer join 乡镇代码 ee on substring(aa.dw,1,9) = SUBSTRING(ee.[dwdm],1,9)
left outer join 比对结果字典 ff on bb.比对结果 = ff.bdjgcode

where --aa.nvfsfzh != '' and bb.身份证号 !='' 
1=1 ");
            #endregion
            List<SqlParameter> paramList = new List<SqlParameter>();

            paramList.Add(new SqlParameter("@dateBegin", dateBegin));
            paramList.Add(new SqlParameter("@dateEnd", dateEnd));

            if (string.IsNullOrWhiteSpace(sfzh))
            { }
            else
            {
                sql.Append(" and (nvfsfzh =@sfzh or bb.[身份证号]=@sfzh) ");
                paramList.Add(new SqlParameter("@sfzh", sfzh));
            }

            if (string.IsNullOrWhiteSpace(xm))
            { }
            else
            {
                sql.Append(" and (nvfxm = @xm or bb.[姓名]=@xm)");
                paramList.Add(new SqlParameter("@xm", xm));
            }

            if (string.IsNullOrWhiteSpace(dw))
            { }
            else
            {
                sql.Append(" and bb.乡镇 like @dw+'%' ");
                paramList.Add(new SqlParameter("@dw", dw));
            }

            if (string.IsNullOrWhiteSpace(bdjg))
            { }
            else
            {
                paramList.Add(new SqlParameter("@bdjg", bdjg));
                sql.Append(" and bb.[比对结果]=@bdjg");
            }

            DataSet ds = new DataSet();
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connstr))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sql.ToString();
                for (int index = 0; index < paramList.Count; index++)
                {
                    cmd.Parameters.Add(paramList[index]);
                }
                SqlDataAdapter ad = new SqlDataAdapter(cmd);
                ad.Fill(ds);
            }


            return ds.Tables[0];
        }

    }
}
