﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.统计分析.WIS联合统计
{
    public partial class Frm外出标记 : DevExpress.XtraEditors.XtraForm
    {
        public Frm外出标记()
        {
            InitializeComponent();
        }
        public string _外出类型, _外出地点, _外出原因;
        public Frm外出标记(string s_姓名,string s_身份证,ref string _外出类型,ref string _外出地点,ref string _外出原因)
        {
            InitializeComponent();

            this.txt姓名.Text = s_姓名;
            this.txt身份证.Text = s_身份证;
        }

        private void btn确定_Click(object sender, EventArgs e)
        {
            if (Check())
            {
                _外出类型 = this.txt外出类型.Text;
                _外出地点 = this.txt外出地点.Text;
                _外出原因 = this.txt外出原因.Text;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private bool Check()
        {
            if (string.IsNullOrEmpty(txt外出类型.Text))
            {
                MessageBox.Show("【外出类型】不能为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (string.IsNullOrEmpty(txt外出地点.Text))
            {
                MessageBox.Show("【外出地点】不能为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (string.IsNullOrEmpty(txt外出原因.Text))
            {
                MessageBox.Show("【外出原因】不能为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }
    }
}
