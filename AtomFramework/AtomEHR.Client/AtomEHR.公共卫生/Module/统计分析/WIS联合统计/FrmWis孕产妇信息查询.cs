﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using AtomEHR.Library;

namespace AtomEHR.公共卫生.Module.统计分析.WIS联合统计
{
    public partial class FrmWis孕产妇信息查询 : AtomEHR.Library.frmBaseDataForm
    {
        string connstr = "Data Source=192.168.10.57;Initial Catalog=JTK;User ID=yggsuser;Password=yggsuser;";
        public FrmWis孕产妇信息查询()
        {
            InitializeComponent();

            frmGridCustomize.RegisterGrid(gridView1);
        }

        DataSet ds = new DataSet();
        public void BindXZList()
        {
            ds = new DataSet();
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connstr))
            {
                conn.Open();
                SqlDataAdapter ad = new SqlDataAdapter("SELECT SUBSTRING([dwdm],1,9) dwdm,[dwmc] FROM [dbo].[乡镇代码] ORDER BY dwdm ",conn);
                ad.Fill(ds);
            }

            this.lookUpEditXZ.Properties.ValueMember = "dwdm";
            this.lookUpEditXZ.Properties.DisplayMember = "dwmc";
            this.lookUpEditXZ.Properties.DataSource = ds.Tables[0];
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            if (this.lookUpEditXZ.EditValue == null)
            {
                MessageBox.Show("请选择乡镇！");
                return;
            }
            string xz = this.lookUpEditXZ.EditValue.ToString();
            string name = this.txtName.Text.Trim();
            string sfzh = this.txtSfzh.Text.Trim();
            string mcjybegin = this.dateTimePickerYBegin.Value.ToString("yyyy-MM-dd");
            string mcjyend = this.dateTimePickerYEnd.Value.AddDays(1).ToString("yyyy-MM-dd");
            string zfxm = txtManName.Text.Trim();//丈夫姓名
            string zncsrqBegin = this.dateTimePickerznBegin.Value.ToString("yyyy-MM-dd");
            string zncsrqEnd = this.dateTimePickerznEnd.Value.ToString("yyyy-MM-dd");

            #region old code --已注释
//            string sql = @"SELECT TOP 100 fnxm,aa.fnsfhm,fnxjd,aa.dwdm,CONVERT(VARCHAR(20),mcyjrq,23) mcyjrq,zfxm,zfxjd
//FROM  qhry aa INNER join rs bb 
//			on (aa.tcrq is null) and (aa.fnbm = bb.fnbm) and (bb.rszzrq is null)
//where 1=1 ";
//            List<SqlParameter> paramList = new List<SqlParameter>();
//            if (!string.IsNullOrWhiteSpace(xz))
//            {
//                sql += " and aa.dwdm like @xz+'%' ";
//                paramList.Add(new SqlParameter("@xz",xz));
//            }
//            if (!string.IsNullOrWhiteSpace(name))
//            {
//                sql += " and aa.fnxm like @name+'%' ";
//                paramList.Add(new SqlParameter("@name",name));
//            }
//            if (!string.IsNullOrWhiteSpace(sfzh))
//            {
//                sql += " and aa.fnsfhm = @sfzh ";
//                paramList.Add(new SqlParameter("@sfzh",sfzh));
//            }
//            if (!string.IsNullOrWhiteSpace(date1))
//            {
//                sql += " and mcyjrq >= @DATE1 ";
//                paramList.Add(new SqlParameter("@DATE1",date1));
//            }
//            if (!string.IsNullOrWhiteSpace(date2))
//            {
//                sql += " and mcyjrq < @DATE2 ";
//                paramList.Add(new SqlParameter("@DATE2",date2));
//            }

            
//            try
//            {
//                DataSet ds = new DataSet();
//                using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connstr))
//                {
//                    conn.Open();
//                    SqlCommand cmd = conn.CreateCommand();
//                    cmd.CommandText = sql;
//                    for (int index = 0; index < paramList.Count; index++)
//                    {
//                        cmd.Parameters.Add(paramList[index]);
//                    }
//                    SqlDataAdapter ad = new SqlDataAdapter(cmd);
//                    ad.Fill(ds);
//                }
//                this.gcSummary.DataSource = ds.Tables[0];
//            }
//            catch(Exception ex)
//            {
//                MessageBox.Show(ex.Message);
//            }
            #endregion

            try
            {
                DataSet ds = new DataSet();
                string sql = "Pro_GETWisgw未管理孕产妇信息";
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@mcyjbegin",mcjybegin));
                paramList.Add(new SqlParameter("@mcjyend",mcjyend));
                paramList.Add(new SqlParameter("@nvcsrqbegin",zncsrqBegin));
                paramList.Add(new SqlParameter("@nvcsrqend",zncsrqEnd));
                paramList.Add(new SqlParameter("@xz",xz));
                paramList.Add(new SqlParameter("@fnname",name));
                paramList.Add(new SqlParameter("@fnsfzh",sfzh));
                paramList.Add(new SqlParameter("@zfxm",zfxm));


                using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connstr))
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = sql;
                    for (int index = 0; index < paramList.Count; index++)
                    {
                        cmd.Parameters.Add(paramList[index]);
                    }
                    SqlDataAdapter ad = new SqlDataAdapter(cmd);
                    ad.Fill(ds);
                }
                this.gcSummary.DataSource = ds.Tables[0];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FrmWis孕产妇信息查询_Load(object sender, EventArgs e)
        {
            BindXZList();
        }

        private void btn标记_Click(object sender, EventArgs e)
        {
            DataRow dr = gridView1.GetFocusedDataRow();
            if (dr != null)
            {
                string _外出类型 = "",_外出地点="",_外出原因="";
                Frm外出标记 frm = new Frm外出标记(dr["fnxm"].ToString(), dr["fnsfhm"].ToString(),
                    ref _外出类型, ref _外出地点, ref _外出原因);
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;

                string sql = "insert into 外出孕产妇(姓名,身份证号,外出类型,县域内流入地址,外出原因) " +
                    "values('" + dr["fnxm"].ToString() + "','" + dr["fnsfhm"].ToString() + "','" + frm._外出类型 + "','" + frm._外出地点 + "','" + frm._外出原因 + "')";
                //执行插入
                using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connstr))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    int irow = cmd.ExecuteNonQuery();
                    if (irow > 0)
                        MessageBox.Show("保存成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.None);
                }
            }
        }
    }
}
