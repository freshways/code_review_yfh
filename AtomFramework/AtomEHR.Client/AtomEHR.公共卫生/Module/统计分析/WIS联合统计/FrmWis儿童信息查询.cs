﻿using AtomEHR.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.统计分析.WIS联合统计
{
    public partial class FrmWis儿童信息查询 : AtomEHR.Library.frmBaseDataForm
    {
        string connstr = "Data Source=192.168.10.57;Initial Catalog=JTK;User ID=yggsuser;Password=yggsuser;";
        public FrmWis儿童信息查询()
        {
            InitializeComponent();
        }

        private void FrmWis儿童信息查询_Load(object sender, EventArgs e)
        {
            frmGridCustomize.RegisterGrid(gridView1);
            BindXZList();
        }

        DataSet ds = new DataSet();
        public void BindXZList()
        {
            ds = new DataSet();
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connstr))
            {
                conn.Open();
                SqlDataAdapter ad = new SqlDataAdapter("SELECT SUBSTRING([dwdm],1,9) dwdm,[dwmc] FROM [dbo].[乡镇代码] ORDER BY dwdm ", conn);
                ad.Fill(ds);
            }

            this.lookUpEditXZ.Properties.ValueMember = "dwdm";
            this.lookUpEditXZ.Properties.DisplayMember = "dwmc";
            this.lookUpEditXZ.Properties.DataSource = ds.Tables[0];
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            if(this.lookUpEditXZ.EditValue==null)
            {
                MessageBox.Show("请选择乡镇！");
                return;
            }
            string xz = this.lookUpEditXZ.EditValue.ToString();
            string mname = this.txtMName.Text.Trim();
            string msfzh = this.txtMSfzh.Text.Trim();
            string fname = this.txtFname.Text.Trim();
            string fsfzh = this.txtFsfzh.Text.Trim();
            string cname = this.txtCname.Text.Trim();
            string date1 = this.dateTimePickerCBegin.Value.ToString("yyyy-MM-dd");
            string date2 = this.dateTimePickerCEnd.Value.AddDays(1).ToString("yyyy-MM-dd");

            #region old code 已注释
//            string sql = @"select a.znxm 子女姓名,a.znxb 子女性别,a.znsfhm 子女身份证号,CONVERT(VARCHAR(20),a.zncsrq,23) 子女出生日期,
//	a.znxjd 子女现居地,replace(a.smxm,'*','') 生母,replace(a.sfxm,'*','') 生父,
//	b.fnxm 母亲,b.fnsfhm 母亲身份证号,b.fnxjd 母亲现居地,b.zfxm 父亲,b.zfsfhm 父亲身份证号,b.zfxjd 父亲现居地,a.dwdm 上报单位,
//	--b.dwdm 妇女单位,
//	b.dwmc 单位
//from syjjtzn a
//	inner join qhry b on a.fnbm = b.fnbm
//	left join 乡镇代码 c on substring(b.dwdm,1,9) = substring(c.dwdm,1,9)
//where not exists (select 母亲身份证 from 外出儿童 where 外出类型 in('已录入省流管平台','县域内流出','其他') and b.fnsfhm = 母亲身份证)";
//            List<SqlParameter> paramList = new List<SqlParameter>();
//            if (!string.IsNullOrWhiteSpace(xz))
//            {
//                sql += " and a.dwdm like @xz+'%' ";
//                paramList.Add(new SqlParameter("@xz", xz));
//            }
//            if (!string.IsNullOrWhiteSpace(mname))
//            {
//                sql += " AND (smxm LIKE @mqname+'%' OR fnxm LIKE @mqname+'%') ";
//                paramList.Add(new SqlParameter("@mqname", mname));
//            }
//            if (!string.IsNullOrWhiteSpace(msfzh))
//            {
//                sql += " AND (a.fnsfhm=@msfzh OR b.fnsfhm=@msfzh) ";
//                paramList.Add(new SqlParameter("@msfzh", msfzh));
//            }
//            if (!string.IsNullOrWhiteSpace(fname))
//            {
//                sql += " AND (a.sfxm LIKE @fname+'%' OR b.zfxm LIKE @fname+'%') ";
//                paramList.Add(new SqlParameter("@fname", fname));
//            }
//            if (!string.IsNullOrWhiteSpace(fsfzh))
//            {
//                sql += " AND (a.sfsfzh=@fsfzh OR b.zfsfhm=@fsfzh) ";
//                paramList.Add(new SqlParameter("@fsfzh", fsfzh));
//            }
//            if (!string.IsNullOrWhiteSpace(cname))
//            {
//                sql += " AND a.znxm LIKE @cname+'%' ";
//                paramList.Add(new SqlParameter("@cname", cname));
//            }

//            sql += " and zncsrq>=@DATE1 and zncsrq<@DATE2 ";
//            paramList.Add(new SqlParameter("@DATE1", date1));
//            paramList.Add(new SqlParameter("@DATE2", date2));
            


//            try
//            {
//                DataSet ds = new DataSet();
//                using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connstr))
//                {
//                    conn.Open();
//                    SqlCommand cmd = conn.CreateCommand();
//                    cmd.CommandText = sql;
//                    for (int index = 0; index < paramList.Count; index++)
//                    {
//                        cmd.Parameters.Add(paramList[index]);
//                    }
//                    SqlDataAdapter ad = new SqlDataAdapter(cmd);
//                    ad.Fill(ds);
//                }
//                this.gcSummary.DataSource = ds.Tables[0];
//            }
//            catch (Exception ex)
//            {
//                MessageBox.Show(ex.Message);
//            }
            #endregion

            string sql = "Pro_GWisGw未管理儿童信息";
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@sDate2",  date1));
            paramList.Add(new SqlParameter("@eDate2",  date2));
            paramList.Add(new SqlParameter("@xz",  xz));
            paramList.Add(new SqlParameter("@mqname",  mname));
            paramList.Add(new SqlParameter("@msfzh",  msfzh));
            paramList.Add(new SqlParameter("@fname",  fname));
            paramList.Add(new SqlParameter("@fsfzh",  fsfzh));
            paramList.Add(new SqlParameter("@cname",  cname));


            try
            {
                DataSet ds = new DataSet();
                using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connstr))
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = sql;
                    cmd.CommandType = CommandType.StoredProcedure;
                    for (int index = 0; index < paramList.Count; index++)
                    {
                        cmd.Parameters.Add(paramList[index]);
                    }
                    SqlDataAdapter ad = new SqlDataAdapter(cmd);
                    ad.Fill(ds);
                    
                    conn.Close();
                }
                this.gcSummary.DataSource = ds.Tables[0];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn外出标记_Click(object sender, EventArgs e)
        {
            DataRow dr = gridView1.GetFocusedDataRow();
            if (dr != null)
            {
                string _外出类型 = "", _外出地点 = "", _外出原因 = "";
                Frm外出标记 frm = new Frm外出标记(dr["母亲"].ToString(), dr["母亲身份证号"].ToString(),
                    ref _外出类型, ref _外出地点, ref _外出原因);
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;

                string sql = "insert into 外出儿童(母亲,母亲身份证,外出类型,县域内流入地址,外出原因) " +
                    "values('" + dr["母亲"].ToString() + "','" + dr["母亲身份证号"].ToString() + "','" + frm._外出类型 + "','" + frm._外出地点 + "','" + frm._外出原因 + "')";
                //执行插入
                using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connstr))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    int irow = cmd.ExecuteNonQuery();
                    if (irow > 0)
                        MessageBox.Show("保存成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.None);
                }
            }
        }
    }
}
