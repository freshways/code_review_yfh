﻿using AtomEHR.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.统计分析.WIS联合统计
{
    public partial class frm出生数据统计 : AtomEHR.Library.frmBaseDataForm
    {
        string connstr = "Data Source=192.168.10.57;Initial Catalog=JTK;User ID=yggsuser;Password=yggsuser;";
        public frm出生数据统计()
        {
            InitializeComponent();
        }

        private void frm出生数据统计_Load(object sender, EventArgs e)
        {
            frmGridCustomize.RegisterGrid(gridView1);
            BindXZList();
        }

        DataSet ds = new DataSet();
        public void BindXZList()
        {
            ds = new DataSet();
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connstr))
            {
                conn.Open();
                SqlDataAdapter ad = new SqlDataAdapter("SELECT SUBSTRING([dwdm],1,9) dwdm,[dwmc] FROM [dbo].[乡镇代码] ORDER BY dwdm ", conn);
                ad.Fill(ds);
            }

            this.lookUpEditXZ.Properties.ValueMember = "dwdm";
            this.lookUpEditXZ.Properties.DisplayMember = "dwmc";
            this.lookUpEditXZ.Properties.DataSource = ds.Tables[0];
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            if (this.lookUpEditXZ.EditValue == null)
            {
                MessageBox.Show("请选择乡镇！");
                return;
            }
            string xz = this.lookUpEditXZ.EditValue.ToString();
            string mname = this.txt母亲姓名.Text.Trim();
            string msfzh = this.txt母亲身份证.Text.Trim();
            string date1 = this.dt儿童出生日期1.Value.ToString("yyyy-MM-01");
            string date2 = this.dt儿童出生日期2.Value.AddDays(1).ToString("yyyy-MM-dd");

            #region old code 已注释
            #endregion

            string sql = "select * from  view出生数据 where 1=1 ";
            if (!string.IsNullOrEmpty(xz))
            {
                sql += " and 乡镇 like'" + xz + "%' ";
            }
            if (!string.IsNullOrEmpty(mname))
            {
                sql += " and 姓名 like'" + mname + "%' ";
            }
            if (!string.IsNullOrEmpty(msfzh))
            {
                sql += " and 身份证号 ='" + msfzh + "' ";
            }
            if (!string.IsNullOrEmpty(date1))
            {
                sql += " and 子女出生日期 >='" + date1 + "' ";
            }
            if (!string.IsNullOrEmpty(date2))
            {
                sql += " and 子女出生日期 <='" + date2 + "' ";
            }

            try
            {
                DataSet ds = new DataSet();
                using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connstr))
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = sql;
                    cmd.CommandType = CommandType.Text;
                    //for (int index = 0; index < paramList.Count; index++)
                    //{
                    //    cmd.Parameters.Add(paramList[index]);
                    //}
                    SqlDataAdapter ad = new SqlDataAdapter(cmd);
                    ad.Fill(ds);

                    conn.Close();
                }
                this.gcSummary.DataSource = ds.Tables[0];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
