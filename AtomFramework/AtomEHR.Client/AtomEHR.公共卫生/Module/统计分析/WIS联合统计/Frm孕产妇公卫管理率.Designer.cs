﻿namespace AtomEHR.公共卫生.Module
{
    partial class Frm孕产妇公卫管理率 

    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm孕产妇公卫管理率));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.dateTimePickerEEnd = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerEBegin = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerYEnd = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerYBegin = new System.Windows.Forms.DateTimePicker();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.gcSummary = new AtomEHR.Library.UserControls.DataGridControl();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.col乡镇编码 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.col乡镇名称 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colwis总数 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.col孕妇 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.col产妇 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.col公卫管理数 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colgwy孕妇 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colgwy产妇 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.col公卫未管理数 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colgww孕妇 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colgww产妇 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.col公卫管理率 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.sbtnExport = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gcSummary);
            this.tpSummary.Controls.Add(this.groupControl1);
            this.tpSummary.Size = new System.Drawing.Size(908, 387);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Size = new System.Drawing.Size(914, 393);
            // 
            // tcBusiness
            // 
            this.tcBusiness.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            this.tcBusiness.Size = new System.Drawing.Size(914, 393);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(776, 507);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(914, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(736, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(539, 2);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.layoutControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(908, 80);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "查询信息";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.sbtnExport);
            this.layoutControl1.Controls.Add(this.dateTimePickerEEnd);
            this.layoutControl1.Controls.Add(this.dateTimePickerEBegin);
            this.layoutControl1.Controls.Add(this.dateTimePickerYEnd);
            this.layoutControl1.Controls.Add(this.dateTimePickerYBegin);
            this.layoutControl1.Controls.Add(this.btnQuery);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 22);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(431, 278, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(904, 56);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // dateTimePickerEEnd
            // 
            this.dateTimePickerEEnd.Location = new System.Drawing.Point(233, 28);
            this.dateTimePickerEEnd.Name = "dateTimePickerEEnd";
            this.dateTimePickerEEnd.Size = new System.Drawing.Size(104, 22);
            this.dateTimePickerEEnd.TabIndex = 10;
            // 
            // dateTimePickerEBegin
            // 
            this.dateTimePickerEBegin.Location = new System.Drawing.Point(115, 28);
            this.dateTimePickerEBegin.Name = "dateTimePickerEBegin";
            this.dateTimePickerEBegin.Size = new System.Drawing.Size(97, 22);
            this.dateTimePickerEBegin.TabIndex = 9;
            // 
            // dateTimePickerYEnd
            // 
            this.dateTimePickerYEnd.Location = new System.Drawing.Point(233, 4);
            this.dateTimePickerYEnd.Name = "dateTimePickerYEnd";
            this.dateTimePickerYEnd.Size = new System.Drawing.Size(104, 22);
            this.dateTimePickerYEnd.TabIndex = 8;
            // 
            // dateTimePickerYBegin
            // 
            this.dateTimePickerYBegin.Location = new System.Drawing.Point(115, 4);
            this.dateTimePickerYBegin.Name = "dateTimePickerYBegin";
            this.dateTimePickerYBegin.Size = new System.Drawing.Size(97, 22);
            this.dateTimePickerYBegin.TabIndex = 7;
            // 
            // btnQuery
            // 
            this.btnQuery.Image = ((System.Drawing.Image)(resources.GetObject("btnQuery.Image")));
            this.btnQuery.Location = new System.Drawing.Point(409, 28);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(77, 22);
            this.btnQuery.StyleController = this.layoutControl1;
            this.btnQuery.TabIndex = 6;
            this.btnQuery.Text = "查询";
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.emptySpaceItem3,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.emptySpaceItem4,
            this.layoutControlItem5,
            this.emptySpaceItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(904, 56);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(337, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(563, 24);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dateTimePickerYBegin;
            this.layoutControlItem1.CustomizationFormText = "孕妇末次月经日期：";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(212, 24);
            this.layoutControlItem1.Text = "孕妇末次月经日期：";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dateTimePickerYEnd;
            this.layoutControlItem2.CustomizationFormText = "至";
            this.layoutControlItem2.Location = new System.Drawing.Point(212, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(125, 24);
            this.layoutControlItem2.Text = "至";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(12, 14);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.dateTimePickerEBegin;
            this.layoutControlItem3.CustomizationFormText = "儿童出生日期：";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(212, 28);
            this.layoutControlItem3.Text = "儿童出生日期：";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(108, 14);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(574, 24);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(326, 28);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.dateTimePickerEEnd;
            this.layoutControlItem4.CustomizationFormText = "至";
            this.layoutControlItem4.Location = new System.Drawing.Point(212, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(125, 28);
            this.layoutControlItem4.Text = "至";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(12, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnQuery;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(405, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(81, 28);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(337, 24);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(68, 28);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // gcSummary
            // 
            this.gcSummary.AllowBandedGridColumnSort = false;
            this.gcSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSummary.IsBestFitColumns = true;
            this.gcSummary.Location = new System.Drawing.Point(0, 80);
            this.gcSummary.MainView = this.bandedGridView1;
            this.gcSummary.Name = "gcSummary";
            this.gcSummary.ShowContextMenu = false;
            this.gcSummary.Size = new System.Drawing.Size(908, 307);
            this.gcSummary.StrWhere = "";
            this.gcSummary.TabIndex = 6;
            this.gcSummary.UseCheckBox = false;
            this.gcSummary.View = "";
            this.gcSummary.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1});
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Appearance.BandPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.BandPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.BandPanelBackground.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.BandPanelBackground.Options.UseFont = true;
            this.bandedGridView1.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.bandedGridView1.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.bandedGridView1.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.DetailTip.Options.UseFont = true;
            this.bandedGridView1.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.Empty.Options.UseFont = true;
            this.bandedGridView1.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.EvenRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FilterCloseButton.Options.UseFont = true;
            this.bandedGridView1.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FilterPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FixedLine.Options.UseFont = true;
            this.bandedGridView1.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FocusedCell.Options.UseFont = true;
            this.bandedGridView1.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.FocusedRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.bandedGridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.GroupButton.Options.UseFont = true;
            this.bandedGridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.bandedGridView1.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.GroupRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.bandedGridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridView1.Appearance.HeaderPanelBackground.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.HeaderPanelBackground.Options.UseFont = true;
            this.bandedGridView1.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.HideSelectionRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.HorzLine.Options.UseFont = true;
            this.bandedGridView1.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.OddRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.Preview.Options.UseFont = true;
            this.bandedGridView1.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.Row.Options.UseFont = true;
            this.bandedGridView1.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.RowSeparator.Options.UseFont = true;
            this.bandedGridView1.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.SelectedRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.TopNewRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.VertLine.Options.UseFont = true;
            this.bandedGridView1.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.bandedGridView1.Appearance.ViewCaption.Options.UseFont = true;
            this.bandedGridView1.BandPanelRowHeight = 30;
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand5,
            this.gridBand2,
            this.gridBand3,
            this.gridBand4});
            this.bandedGridView1.ColumnPanelRowHeight = 30;
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.col乡镇编码,
            this.col乡镇名称,
            this.colwis总数,
            this.col孕妇,
            this.col产妇,
            this.col公卫管理数,
            this.colgwy孕妇,
            this.colgwy产妇,
            this.col公卫未管理数,
            this.colgww孕妇,
            this.colgww产妇,
            this.col公卫管理率});
            this.bandedGridView1.GridControl = this.gcSummary;
            this.bandedGridView1.GroupPanelText = "DragColumn";
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsBehavior.Editable = false;
            this.bandedGridView1.OptionsBehavior.ReadOnly = true;
            this.bandedGridView1.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.bandedGridView1.OptionsView.EnableAppearanceOddRow = true;
            this.bandedGridView1.OptionsView.ShowColumnHeaders = false;
            this.bandedGridView1.OptionsView.ShowFooter = true;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            this.bandedGridView1.CustomDrawFooterCell += new DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventHandler(this.bandedGridView1_CustomDrawFooterCell);
            // 
            // col乡镇编码
            // 
            this.col乡镇编码.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col乡镇编码.AppearanceHeader.Options.UseFont = true;
            this.col乡镇编码.Caption = "乡镇编码";
            this.col乡镇编码.FieldName = "dwdm";
            this.col乡镇编码.Name = "col乡镇编码";
            this.col乡镇编码.OptionsColumn.ReadOnly = true;
            this.col乡镇编码.Visible = true;
            this.col乡镇编码.Width = 100;
            // 
            // col乡镇名称
            // 
            this.col乡镇名称.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col乡镇名称.AppearanceHeader.Options.UseFont = true;
            this.col乡镇名称.Caption = "乡镇名称";
            this.col乡镇名称.FieldName = "乡镇";
            this.col乡镇名称.Name = "col乡镇名称";
            this.col乡镇名称.OptionsColumn.ReadOnly = true;
            this.col乡镇名称.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "乡镇", "合计")});
            this.col乡镇名称.Visible = true;
            this.col乡镇名称.Width = 116;
            // 
            // colwis总数
            // 
            this.colwis总数.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colwis总数.AppearanceHeader.Options.UseFont = true;
            this.colwis总数.Caption = "wis总数";
            this.colwis总数.FieldName = "wis总数";
            this.colwis总数.Name = "colwis总数";
            this.colwis总数.OptionsColumn.ReadOnly = true;
            this.colwis总数.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colwis总数.Visible = true;
            this.colwis总数.Width = 76;
            // 
            // col孕妇
            // 
            this.col孕妇.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col孕妇.AppearanceHeader.Options.UseFont = true;
            this.col孕妇.AppearanceHeader.Options.UseTextOptions = true;
            this.col孕妇.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col孕妇.Caption = "孕妇";
            this.col孕妇.FieldName = "孕妇";
            this.col孕妇.Name = "col孕妇";
            this.col孕妇.OptionsColumn.ReadOnly = true;
            this.col孕妇.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col孕妇.Visible = true;
            this.col孕妇.Width = 77;
            // 
            // col产妇
            // 
            this.col产妇.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col产妇.AppearanceHeader.Options.UseFont = true;
            this.col产妇.AppearanceHeader.Options.UseTextOptions = true;
            this.col产妇.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col产妇.Caption = "产妇";
            this.col产妇.FieldName = "产妇";
            this.col产妇.Name = "col产妇";
            this.col产妇.OptionsColumn.ReadOnly = true;
            this.col产妇.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col产妇.Visible = true;
            this.col产妇.Width = 78;
            // 
            // col公卫管理数
            // 
            this.col公卫管理数.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col公卫管理数.AppearanceHeader.Options.UseFont = true;
            this.col公卫管理数.AppearanceHeader.Options.UseTextOptions = true;
            this.col公卫管理数.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col公卫管理数.Caption = "公卫管理数";
            this.col公卫管理数.FieldName = "公卫管理数";
            this.col公卫管理数.Name = "col公卫管理数";
            this.col公卫管理数.OptionsColumn.ReadOnly = true;
            this.col公卫管理数.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col公卫管理数.Visible = true;
            this.col公卫管理数.Width = 76;
            // 
            // colgwy孕妇
            // 
            this.colgwy孕妇.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colgwy孕妇.AppearanceHeader.Options.UseFont = true;
            this.colgwy孕妇.AppearanceHeader.Options.UseTextOptions = true;
            this.colgwy孕妇.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colgwy孕妇.Caption = "gwy孕妇";
            this.colgwy孕妇.FieldName = "gwy孕妇";
            this.colgwy孕妇.Name = "colgwy孕妇";
            this.colgwy孕妇.OptionsColumn.ReadOnly = true;
            this.colgwy孕妇.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colgwy孕妇.Visible = true;
            this.colgwy孕妇.Width = 77;
            // 
            // colgwy产妇
            // 
            this.colgwy产妇.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colgwy产妇.AppearanceHeader.Options.UseFont = true;
            this.colgwy产妇.AppearanceHeader.Options.UseTextOptions = true;
            this.colgwy产妇.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colgwy产妇.Caption = "gwy产妇";
            this.colgwy产妇.FieldName = "gwy产妇";
            this.colgwy产妇.Name = "colgwy产妇";
            this.colgwy产妇.OptionsColumn.ReadOnly = true;
            this.colgwy产妇.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colgwy产妇.Visible = true;
            this.colgwy产妇.Width = 70;
            // 
            // col公卫未管理数
            // 
            this.col公卫未管理数.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col公卫未管理数.AppearanceHeader.Options.UseFont = true;
            this.col公卫未管理数.AppearanceHeader.Options.UseTextOptions = true;
            this.col公卫未管理数.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col公卫未管理数.Caption = "公卫未管理数";
            this.col公卫未管理数.FieldName = "公卫未管理数";
            this.col公卫未管理数.Name = "col公卫未管理数";
            this.col公卫未管理数.OptionsColumn.ReadOnly = true;
            this.col公卫未管理数.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.col公卫未管理数.Visible = true;
            this.col公卫未管理数.Width = 97;
            // 
            // colgww孕妇
            // 
            this.colgww孕妇.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colgww孕妇.AppearanceHeader.Options.UseFont = true;
            this.colgww孕妇.AppearanceHeader.Options.UseTextOptions = true;
            this.colgww孕妇.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colgww孕妇.Caption = "gww孕妇";
            this.colgww孕妇.FieldName = "gww孕妇";
            this.colgww孕妇.Name = "colgww孕妇";
            this.colgww孕妇.OptionsColumn.ReadOnly = true;
            this.colgww孕妇.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colgww孕妇.Visible = true;
            this.colgww孕妇.Width = 74;
            // 
            // colgww产妇
            // 
            this.colgww产妇.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colgww产妇.AppearanceHeader.Options.UseFont = true;
            this.colgww产妇.Caption = "gww产妇";
            this.colgww产妇.FieldName = "gww产妇";
            this.colgww产妇.Name = "colgww产妇";
            this.colgww产妇.OptionsColumn.ReadOnly = true;
            this.colgww产妇.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colgww产妇.Visible = true;
            this.colgww产妇.Width = 69;
            // 
            // col公卫管理率
            // 
            this.col公卫管理率.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col公卫管理率.AppearanceHeader.Options.UseFont = true;
            this.col公卫管理率.Caption = "公卫管理率";
            this.col公卫管理率.FieldName = "公卫管理率";
            this.col公卫管理率.Name = "col公卫管理率";
            this.col公卫管理率.OptionsColumn.ReadOnly = true;
            this.col公卫管理率.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom)});
            this.col公卫管理率.Visible = true;
            this.col公卫管理率.Width = 84;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "乡镇编码";
            this.gridBand1.Columns.Add(this.col乡镇编码);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 100;
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand5.AppearanceHeader.Options.UseFont = true;
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "乡镇名称";
            this.gridBand5.Columns.Add(this.col乡镇名称);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 1;
            this.gridBand5.Width = 116;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "WIS";
            this.gridBand2.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand6,
            this.gridBand7,
            this.gridBand8});
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 2;
            this.gridBand2.Width = 231;
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand6.AppearanceHeader.Options.UseFont = true;
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.Caption = "WIS总数";
            this.gridBand6.Columns.Add(this.colwis总数);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 0;
            this.gridBand6.Width = 76;
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand7.AppearanceHeader.Options.UseFont = true;
            this.gridBand7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand7.Caption = "孕妇";
            this.gridBand7.Columns.Add(this.col孕妇);
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 1;
            this.gridBand7.Width = 77;
            // 
            // gridBand8
            // 
            this.gridBand8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand8.AppearanceHeader.Options.UseFont = true;
            this.gridBand8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand8.Caption = "产妇";
            this.gridBand8.Columns.Add(this.col产妇);
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = 2;
            this.gridBand8.Width = 78;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand3.AppearanceHeader.Options.UseFont = true;
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "公卫";
            this.gridBand3.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand9,
            this.gridBand10,
            this.gridBand11,
            this.gridBand12,
            this.gridBand13,
            this.gridBand14});
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 3;
            this.gridBand3.Width = 463;
            // 
            // gridBand9
            // 
            this.gridBand9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand9.AppearanceHeader.Options.UseFont = true;
            this.gridBand9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand9.Caption = "公卫管理数";
            this.gridBand9.Columns.Add(this.col公卫管理数);
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.VisibleIndex = 0;
            this.gridBand9.Width = 76;
            // 
            // gridBand10
            // 
            this.gridBand10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand10.AppearanceHeader.Options.UseFont = true;
            this.gridBand10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand10.Caption = "孕妇";
            this.gridBand10.Columns.Add(this.colgwy孕妇);
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.VisibleIndex = 1;
            this.gridBand10.Width = 77;
            // 
            // gridBand11
            // 
            this.gridBand11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand11.AppearanceHeader.Options.UseFont = true;
            this.gridBand11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand11.Caption = "产妇";
            this.gridBand11.Columns.Add(this.colgwy产妇);
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.VisibleIndex = 2;
            // 
            // gridBand12
            // 
            this.gridBand12.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand12.AppearanceHeader.Options.UseFont = true;
            this.gridBand12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand12.Caption = "公卫未管理数";
            this.gridBand12.Columns.Add(this.col公卫未管理数);
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.VisibleIndex = 3;
            this.gridBand12.Width = 97;
            // 
            // gridBand13
            // 
            this.gridBand13.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand13.AppearanceHeader.Options.UseFont = true;
            this.gridBand13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand13.Caption = "孕妇";
            this.gridBand13.Columns.Add(this.colgww孕妇);
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.VisibleIndex = 4;
            this.gridBand13.Width = 74;
            // 
            // gridBand14
            // 
            this.gridBand14.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand14.AppearanceHeader.Options.UseFont = true;
            this.gridBand14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand14.Caption = "产妇";
            this.gridBand14.Columns.Add(this.colgww产妇);
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.VisibleIndex = 5;
            this.gridBand14.Width = 69;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand4.AppearanceHeader.Options.UseFont = true;
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "公卫管理率";
            this.gridBand4.Columns.Add(this.col公卫管理率);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 4;
            this.gridBand4.Width = 84;
            // 
            // sbtnExport
            // 
            this.sbtnExport.Location = new System.Drawing.Point(500, 28);
            this.sbtnExport.Name = "sbtnExport";
            this.sbtnExport.Size = new System.Drawing.Size(74, 22);
            this.sbtnExport.StyleController = this.layoutControl1;
            this.sbtnExport.TabIndex = 11;
            this.sbtnExport.Text = "导出";
            this.sbtnExport.Click += new System.EventHandler(this.sbtnExport_Click);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.sbtnExport;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(496, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(78, 28);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(486, 24);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(10, 28);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // Frm孕产妇公卫管理率
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(914, 419);
            this.Name = "Frm孕产妇公卫管理率";
            this.Text = "WIS 孕产妇公卫系统管理率";
            this.Load += new System.EventHandler(this.孕产妇系统管理情况登记簿_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Library.UserControls.DataGridControl gcSummary;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private System.Windows.Forms.DateTimePicker dateTimePickerYEnd;
        private System.Windows.Forms.DateTimePicker dateTimePickerYBegin;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.DateTimePicker dateTimePickerEEnd;
        private System.Windows.Forms.DateTimePicker dateTimePickerEBegin;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col乡镇编码;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col乡镇名称;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colwis总数;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col孕妇;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col产妇;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col公卫管理数;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colgwy孕妇;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colgwy产妇;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col公卫未管理数;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colgww孕妇;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colgww产妇;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col公卫管理率;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraEditors.SimpleButton sbtnExport;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;

    }
}