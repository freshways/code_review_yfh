﻿namespace AtomEHR.公共卫生.Module.统计分析.WIS联合统计
{
    partial class FrmWis儿童信息查询
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmWis儿童信息查询));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtFsfzh = new DevExpress.XtraEditors.TextEdit();
            this.txtFname = new DevExpress.XtraEditors.TextEdit();
            this.txtCname = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEditXZ = new DevExpress.XtraEditors.LookUpEdit();
            this.txtMSfzh = new DevExpress.XtraEditors.TextEdit();
            this.txtMName = new DevExpress.XtraEditors.TextEdit();
            this.dateTimePickerCEnd = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerCBegin = new System.Windows.Forms.DateTimePicker();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gcSummary = new AtomEHR.Library.UserControls.DataGridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col子女姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col子女性别 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col子女身份证号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col子女出生日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col子女现居地 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col生母 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col生父 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col母亲 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col母亲身份证号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col母亲现居地 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col父亲 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col父亲身份证号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col父亲现居地 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col上报单位 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col单位 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col提示信息 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn外出标记 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFsfzh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFname.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCname.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditXZ.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMSfzh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gcSummary);
            this.tpSummary.Controls.Add(this.groupControl1);
            this.tpSummary.Size = new System.Drawing.Size(947, 473);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Size = new System.Drawing.Size(953, 479);
            // 
            // tcBusiness
            // 
            this.tcBusiness.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            this.tcBusiness.Size = new System.Drawing.Size(953, 479);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(776, 507);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(953, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(775, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(578, 2);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.layoutControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(947, 103);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "查询信息";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btn外出标记);
            this.layoutControl1.Controls.Add(this.txtFsfzh);
            this.layoutControl1.Controls.Add(this.txtFname);
            this.layoutControl1.Controls.Add(this.txtCname);
            this.layoutControl1.Controls.Add(this.lookUpEditXZ);
            this.layoutControl1.Controls.Add(this.txtMSfzh);
            this.layoutControl1.Controls.Add(this.txtMName);
            this.layoutControl1.Controls.Add(this.dateTimePickerCEnd);
            this.layoutControl1.Controls.Add(this.dateTimePickerCBegin);
            this.layoutControl1.Controls.Add(this.btnQuery);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 22);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(431, 278, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(943, 79);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtFsfzh
            // 
            this.txtFsfzh.Location = new System.Drawing.Point(632, 4);
            this.txtFsfzh.Name = "txtFsfzh";
            this.txtFsfzh.Size = new System.Drawing.Size(135, 20);
            this.txtFsfzh.StyleController = this.layoutControl1;
            this.txtFsfzh.TabIndex = 18;
            // 
            // txtFname
            // 
            this.txtFname.Location = new System.Drawing.Point(69, 28);
            this.txtFname.Name = "txtFname";
            this.txtFname.Size = new System.Drawing.Size(94, 20);
            this.txtFname.StyleController = this.layoutControl1;
            this.txtFname.TabIndex = 17;
            // 
            // txtCname
            // 
            this.txtCname.Location = new System.Drawing.Point(232, 28);
            this.txtCname.Name = "txtCname";
            this.txtCname.Size = new System.Drawing.Size(83, 20);
            this.txtCname.StyleController = this.layoutControl1;
            this.txtCname.TabIndex = 16;
            // 
            // lookUpEditXZ
            // 
            this.lookUpEditXZ.Location = new System.Drawing.Point(45, 4);
            this.lookUpEditXZ.Name = "lookUpEditXZ";
            this.lookUpEditXZ.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditXZ.Properties.DisplayMember = "dwmc";
            this.lookUpEditXZ.Properties.NullText = "[请选择乡镇]";
            this.lookUpEditXZ.Properties.ValueMember = "dwdm";
            this.lookUpEditXZ.Size = new System.Drawing.Size(118, 20);
            this.lookUpEditXZ.StyleController = this.layoutControl1;
            this.lookUpEditXZ.TabIndex = 15;
            // 
            // txtMSfzh
            // 
            this.txtMSfzh.Location = new System.Drawing.Point(408, 4);
            this.txtMSfzh.Name = "txtMSfzh";
            this.txtMSfzh.Size = new System.Drawing.Size(131, 20);
            this.txtMSfzh.StyleController = this.layoutControl1;
            this.txtMSfzh.TabIndex = 14;
            // 
            // txtMName
            // 
            this.txtMName.Location = new System.Drawing.Point(232, 4);
            this.txtMName.Name = "txtMName";
            this.txtMName.Size = new System.Drawing.Size(83, 20);
            this.txtMName.StyleController = this.layoutControl1;
            this.txtMName.TabIndex = 13;
            // 
            // dateTimePickerCEnd
            // 
            this.dateTimePickerCEnd.Location = new System.Drawing.Point(540, 28);
            this.dateTimePickerCEnd.Name = "dateTimePickerCEnd";
            this.dateTimePickerCEnd.Size = new System.Drawing.Size(105, 22);
            this.dateTimePickerCEnd.TabIndex = 8;
            // 
            // dateTimePickerCBegin
            // 
            this.dateTimePickerCBegin.Location = new System.Drawing.Point(408, 28);
            this.dateTimePickerCBegin.Name = "dateTimePickerCBegin";
            this.dateTimePickerCBegin.Size = new System.Drawing.Size(111, 22);
            this.dateTimePickerCBegin.TabIndex = 7;
            // 
            // btnQuery
            // 
            this.btnQuery.Image = ((System.Drawing.Image)(resources.GetObject("btnQuery.Image")));
            this.btnQuery.Location = new System.Drawing.Point(315, 52);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(76, 22);
            this.btnQuery.StyleController = this.layoutControl1;
            this.btnQuery.TabIndex = 6;
            this.btnQuery.Text = "查询";
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem3,
            this.layoutControlItem6,
            this.emptySpaceItem4,
            this.layoutControlItem4,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.emptySpaceItem2,
            this.layoutControlItem9,
            this.layoutControlItem5,
            this.layoutControlItem10});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(943, 79);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(767, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(172, 24);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(484, 48);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(455, 27);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnQuery;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(311, 48);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(80, 27);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(311, 27);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtMName;
            this.layoutControlItem4.CustomizationFormText = "孕妇姓名：";
            this.layoutControlItem4.Location = new System.Drawing.Point(163, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(152, 24);
            this.layoutControlItem4.Text = "母亲姓名：";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtMSfzh;
            this.layoutControlItem7.CustomizationFormText = "身份证号：";
            this.layoutControlItem7.Location = new System.Drawing.Point(315, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(224, 24);
            this.layoutControlItem7.Text = "母亲身份证号：";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(84, 14);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.lookUpEditXZ;
            this.layoutControlItem8.CustomizationFormText = "乡镇：";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(163, 24);
            this.layoutControlItem8.Text = "乡镇：";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(36, 14);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dateTimePickerCBegin;
            this.layoutControlItem1.CustomizationFormText = "孕妇末次月经日期：";
            this.layoutControlItem1.Location = new System.Drawing.Point(315, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(204, 24);
            this.layoutControlItem1.Text = "儿童出生日期：";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(84, 14);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dateTimePickerCEnd;
            this.layoutControlItem2.CustomizationFormText = "至";
            this.layoutControlItem2.Location = new System.Drawing.Point(519, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(126, 24);
            this.layoutControlItem2.Text = "至";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(12, 14);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtCname;
            this.layoutControlItem3.CustomizationFormText = "儿童姓名：";
            this.layoutControlItem3.Location = new System.Drawing.Point(163, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(152, 24);
            this.layoutControlItem3.Text = "儿童姓名：";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(645, 24);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(294, 24);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txtFsfzh;
            this.layoutControlItem9.CustomizationFormText = "父亲身份证号：";
            this.layoutControlItem9.Location = new System.Drawing.Point(539, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(228, 24);
            this.layoutControlItem9.Text = "父亲身份证号：";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(84, 14);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtFname;
            this.layoutControlItem5.CustomizationFormText = "父亲姓名：";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(163, 24);
            this.layoutControlItem5.Text = "父亲姓名：";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // gcSummary
            // 
            this.gcSummary.AllowBandedGridColumnSort = false;
            this.gcSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSummary.IsBestFitColumns = true;
            this.gcSummary.Location = new System.Drawing.Point(0, 103);
            this.gcSummary.MainView = this.gridView1;
            this.gcSummary.Name = "gcSummary";
            this.gcSummary.ShowContextMenu = false;
            this.gcSummary.Size = new System.Drawing.Size(947, 370);
            this.gcSummary.StrWhere = "";
            this.gcSummary.TabIndex = 8;
            this.gcSummary.UseCheckBox = false;
            this.gcSummary.View = "";
            this.gcSummary.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gridView1.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gridView1.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.DetailTip.Options.UseFont = true;
            this.gridView1.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.Empty.Options.UseFont = true;
            this.gridView1.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.EvenRow.Options.UseFont = true;
            this.gridView1.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gridView1.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.FilterPanel.Options.UseFont = true;
            this.gridView1.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.FixedLine.Options.UseFont = true;
            this.gridView1.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.FocusedCell.Options.UseFont = true;
            this.gridView1.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.GroupButton.Options.UseFont = true;
            this.gridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView1.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gridView1.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.HorzLine.Options.UseFont = true;
            this.gridView1.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.OddRow.Options.UseFont = true;
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.RowSeparator.Options.UseFont = true;
            this.gridView1.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.SelectedRow.Options.UseFont = true;
            this.gridView1.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.TopNewRow.Options.UseFont = true;
            this.gridView1.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.VertLine.Options.UseFont = true;
            this.gridView1.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView1.Appearance.ViewCaption.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col子女姓名,
            this.col子女性别,
            this.col子女身份证号,
            this.col子女出生日期,
            this.col子女现居地,
            this.col生母,
            this.col生父,
            this.col母亲,
            this.col母亲身份证号,
            this.col母亲现居地,
            this.col父亲,
            this.col父亲身份证号,
            this.col父亲现居地,
            this.col上报单位,
            this.col单位,
            this.col提示信息,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.GridControl = this.gcSummary;
            this.gridView1.GroupPanelText = "DragColumn";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsBehavior.ReadOnly = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // col子女姓名
            // 
            this.col子女姓名.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col子女姓名.AppearanceHeader.Options.UseFont = true;
            this.col子女姓名.Caption = "子女姓名";
            this.col子女姓名.FieldName = "子女姓名";
            this.col子女姓名.Name = "col子女姓名";
            this.col子女姓名.Visible = true;
            this.col子女姓名.VisibleIndex = 0;
            // 
            // col子女性别
            // 
            this.col子女性别.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col子女性别.AppearanceHeader.Options.UseFont = true;
            this.col子女性别.Caption = "子女性别";
            this.col子女性别.FieldName = "子女性别";
            this.col子女性别.Name = "col子女性别";
            this.col子女性别.Visible = true;
            this.col子女性别.VisibleIndex = 1;
            // 
            // col子女身份证号
            // 
            this.col子女身份证号.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col子女身份证号.AppearanceHeader.Options.UseFont = true;
            this.col子女身份证号.Caption = "子女身份证号";
            this.col子女身份证号.FieldName = "子女身份证号";
            this.col子女身份证号.Name = "col子女身份证号";
            this.col子女身份证号.Visible = true;
            this.col子女身份证号.VisibleIndex = 2;
            // 
            // col子女出生日期
            // 
            this.col子女出生日期.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col子女出生日期.AppearanceHeader.Options.UseFont = true;
            this.col子女出生日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col子女出生日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col子女出生日期.Caption = "子女出生日期";
            this.col子女出生日期.FieldName = "子女出生日期";
            this.col子女出生日期.Name = "col子女出生日期";
            this.col子女出生日期.OptionsColumn.ReadOnly = true;
            this.col子女出生日期.Visible = true;
            this.col子女出生日期.VisibleIndex = 3;
            // 
            // col子女现居地
            // 
            this.col子女现居地.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col子女现居地.AppearanceHeader.Options.UseFont = true;
            this.col子女现居地.AppearanceHeader.Options.UseTextOptions = true;
            this.col子女现居地.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col子女现居地.Caption = "子女现居地";
            this.col子女现居地.FieldName = "子女现居地";
            this.col子女现居地.Name = "col子女现居地";
            this.col子女现居地.Visible = true;
            this.col子女现居地.VisibleIndex = 4;
            // 
            // col生母
            // 
            this.col生母.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col生母.AppearanceHeader.Options.UseFont = true;
            this.col生母.AppearanceHeader.Options.UseTextOptions = true;
            this.col生母.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col生母.Caption = "生母";
            this.col生母.FieldName = "生母";
            this.col生母.Name = "col生母";
            this.col生母.Visible = true;
            this.col生母.VisibleIndex = 5;
            // 
            // col生父
            // 
            this.col生父.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col生父.AppearanceHeader.Options.UseFont = true;
            this.col生父.AppearanceHeader.Options.UseTextOptions = true;
            this.col生父.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col生父.Caption = "生父";
            this.col生父.FieldName = "生父";
            this.col生父.Name = "col生父";
            this.col生父.OptionsColumn.ReadOnly = true;
            this.col生父.Visible = true;
            this.col生父.VisibleIndex = 6;
            // 
            // col母亲
            // 
            this.col母亲.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col母亲.AppearanceHeader.Options.UseFont = true;
            this.col母亲.AppearanceHeader.Options.UseTextOptions = true;
            this.col母亲.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col母亲.Caption = "母亲";
            this.col母亲.FieldName = "母亲";
            this.col母亲.Name = "col母亲";
            this.col母亲.Visible = true;
            this.col母亲.VisibleIndex = 7;
            // 
            // col母亲身份证号
            // 
            this.col母亲身份证号.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col母亲身份证号.AppearanceHeader.Options.UseFont = true;
            this.col母亲身份证号.AppearanceHeader.Options.UseTextOptions = true;
            this.col母亲身份证号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col母亲身份证号.Caption = "母亲身份证号";
            this.col母亲身份证号.FieldName = "母亲身份证号";
            this.col母亲身份证号.Name = "col母亲身份证号";
            this.col母亲身份证号.Visible = true;
            this.col母亲身份证号.VisibleIndex = 8;
            // 
            // col母亲现居地
            // 
            this.col母亲现居地.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col母亲现居地.AppearanceHeader.Options.UseFont = true;
            this.col母亲现居地.AppearanceHeader.Options.UseTextOptions = true;
            this.col母亲现居地.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col母亲现居地.Caption = "母亲现居地";
            this.col母亲现居地.FieldName = "母亲现居地";
            this.col母亲现居地.Name = "col母亲现居地";
            this.col母亲现居地.Visible = true;
            this.col母亲现居地.VisibleIndex = 9;
            // 
            // col父亲
            // 
            this.col父亲.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col父亲.AppearanceHeader.Options.UseFont = true;
            this.col父亲.AppearanceHeader.Options.UseTextOptions = true;
            this.col父亲.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col父亲.Caption = "父亲";
            this.col父亲.FieldName = "父亲";
            this.col父亲.Name = "col父亲";
            this.col父亲.Visible = true;
            this.col父亲.VisibleIndex = 10;
            // 
            // col父亲身份证号
            // 
            this.col父亲身份证号.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col父亲身份证号.AppearanceHeader.Options.UseFont = true;
            this.col父亲身份证号.AppearanceHeader.Options.UseTextOptions = true;
            this.col父亲身份证号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col父亲身份证号.Caption = "父亲身份证号";
            this.col父亲身份证号.FieldName = "父亲身份证号";
            this.col父亲身份证号.Name = "col父亲身份证号";
            this.col父亲身份证号.Visible = true;
            this.col父亲身份证号.VisibleIndex = 11;
            // 
            // col父亲现居地
            // 
            this.col父亲现居地.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col父亲现居地.AppearanceHeader.Options.UseFont = true;
            this.col父亲现居地.AppearanceHeader.Options.UseTextOptions = true;
            this.col父亲现居地.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col父亲现居地.Caption = "父亲现居地";
            this.col父亲现居地.FieldName = "父亲现居地";
            this.col父亲现居地.Name = "col父亲现居地";
            this.col父亲现居地.Visible = true;
            this.col父亲现居地.VisibleIndex = 12;
            // 
            // col上报单位
            // 
            this.col上报单位.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col上报单位.AppearanceHeader.Options.UseFont = true;
            this.col上报单位.Caption = "上报单位编码";
            this.col上报单位.FieldName = "上报单位";
            this.col上报单位.Name = "col上报单位";
            this.col上报单位.Visible = true;
            this.col上报单位.VisibleIndex = 13;
            // 
            // col单位
            // 
            this.col单位.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col单位.AppearanceHeader.Options.UseFont = true;
            this.col单位.AppearanceHeader.Options.UseTextOptions = true;
            this.col单位.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col单位.Caption = "单位";
            this.col单位.FieldName = "单位";
            this.col单位.Name = "col单位";
            this.col单位.Visible = true;
            this.col单位.VisibleIndex = 14;
            // 
            // col提示信息
            // 
            this.col提示信息.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col提示信息.AppearanceHeader.Options.UseFont = true;
            this.col提示信息.Caption = "提示信息";
            this.col提示信息.FieldName = "提示信息";
            this.col提示信息.Name = "col提示信息";
            this.col提示信息.ToolTip = "点击进行儿童建档";
            this.col提示信息.Visible = true;
            this.col提示信息.VisibleIndex = 15;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "外出原因";
            this.gridColumn1.FieldName = "外出原因";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 16;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "外出地址";
            this.gridColumn2.FieldName = "外出地址";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 17;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "外出类型";
            this.gridColumn3.FieldName = "外出类型";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 18;
            // 
            // btn外出标记
            // 
            this.btn外出标记.Location = new System.Drawing.Point(395, 52);
            this.btn外出标记.Name = "btn外出标记";
            this.btn外出标记.Size = new System.Drawing.Size(89, 22);
            this.btn外出标记.StyleController = this.layoutControl1;
            this.btn外出标记.TabIndex = 19;
            this.btn外出标记.Text = "外出标记";
            this.btn外出标记.Click += new System.EventHandler(this.btn外出标记_Click);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.btn外出标记;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(391, 48);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(93, 27);
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // FrmWis儿童信息查询
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(953, 505);
            this.Name = "FrmWis儿童信息查询";
            this.Text = "Wis儿童信息查询";
            this.Load += new System.EventHandler(this.FrmWis儿童信息查询_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtFsfzh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFname.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCname.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditXZ.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMSfzh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditXZ;
        private DevExpress.XtraEditors.TextEdit txtMSfzh;
        private DevExpress.XtraEditors.TextEdit txtMName;
        private System.Windows.Forms.DateTimePicker dateTimePickerCEnd;
        private System.Windows.Forms.DateTimePicker dateTimePickerCBegin;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.TextEdit txtCname;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.TextEdit txtFname;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.TextEdit txtFsfzh;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private Library.UserControls.DataGridControl gcSummary;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn col子女姓名;
        private DevExpress.XtraGrid.Columns.GridColumn col子女性别;
        private DevExpress.XtraGrid.Columns.GridColumn col子女身份证号;
        private DevExpress.XtraGrid.Columns.GridColumn col子女出生日期;
        private DevExpress.XtraGrid.Columns.GridColumn col子女现居地;
        private DevExpress.XtraGrid.Columns.GridColumn col生母;
        private DevExpress.XtraGrid.Columns.GridColumn col生父;
        private DevExpress.XtraGrid.Columns.GridColumn col母亲;
        private DevExpress.XtraGrid.Columns.GridColumn col母亲身份证号;
        private DevExpress.XtraGrid.Columns.GridColumn col母亲现居地;
        private DevExpress.XtraGrid.Columns.GridColumn col父亲;
        private DevExpress.XtraGrid.Columns.GridColumn col父亲身份证号;
        private DevExpress.XtraGrid.Columns.GridColumn col父亲现居地;
        private DevExpress.XtraGrid.Columns.GridColumn col上报单位;
        private DevExpress.XtraGrid.Columns.GridColumn col单位;
        private DevExpress.XtraGrid.Columns.GridColumn col提示信息;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.SimpleButton btn外出标记;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
    }
}