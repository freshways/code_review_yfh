﻿using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.公共卫生.Module.个人健康;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module
{
    public partial class frm健康档案汇总统计 : AtomEHR.Library.frmBaseDataForm
    {
        DataSet ds = null;
        private string strWhere = null;
        bll健康体检 _bll = new bll健康体检();
        BackgroundWorker bgInvoke = new BackgroundWorker();

        public frm健康档案汇总统计()
        {
            InitializeComponent();
        }

        private void frm健康档案汇总统计_Load(object sender, EventArgs e)
        {
            if (Loginer.CurrentUser.所属机构.Length >= 12)
            {
                txt机构.Properties.AutoExpandAllNodes = true;
            }
            else
            {
                txt机构.Properties.AutoExpandAllNodes = false;
            }
            this.InitializeForm();
            string m_ServerDate = _bll.ServiceDateTime;
            try
            {
                this.dte开始1.Text = Convert.ToDateTime(m_ServerDate).ToString("yyyy-MM-01");
                this.dte结束2.Text = Convert.ToDateTime(m_ServerDate).ToString("yyyy-MM-dd");
            }
            catch
            {
                this.dte开始1.Text = DateTime.Now.ToString("yyyy-MM-01");
                this.dte结束2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
            bgInvoke.DoWork += bgInvoke_DoWork;
            bgInvoke.RunWorkerCompleted += bgInvoke_RunWorkerCompleted;
        }

        private void 档案动态使用率统计_Load(object sender, EventArgs e)
        {
            if (Loginer.CurrentUser.所属机构.Length >= 12)
            {
                txt机构.Properties.AutoExpandAllNodes = true;
            }
            else
            {
                txt机构.Properties.AutoExpandAllNodes = false;
            }
            this.InitializeForm();
            string m_ServerDate = _bll.ServiceDateTime;
            try
            {
                this.dte开始1.Text = Convert.ToDateTime(m_ServerDate).ToString("yyyy-MM-01");
                this.dte结束2.Text = Convert.ToDateTime(m_ServerDate).ToString("yyyy-MM-dd");
            }
            catch
            {
                this.dte开始1.Text = DateTime.Now.ToString("yyyy-MM-01");
                this.dte结束2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
            bgInvoke.DoWork += bgInvoke_DoWork;
            bgInvoke.RunWorkerCompleted += bgInvoke_RunWorkerCompleted;
        }

        void bgInvoke_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();

            DataSet ds = e.Result as DataSet;
            if (ds != null && ds.Tables.Count > 0)
            {
                gcSummary.DataSource = ds.Tables[0];
                //this.bandedGridView1.BestFitColumns();
            }
            else
            {
                gcSummary.DataSource = null;
            }
            this.btnQuery.Enabled = true;
        }

        void bgInvoke_DoWork(object sender, DoWorkEventArgs e)
        {
            DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(typeof(WaitForm1));

            try
            {
                DataSet ds = new bllCom().Get档案汇总统计(txt机构.EditValue.ToString(), dte开始1.DateTime.ToString("yyyy-MM-dd"), dte结束2.DateTime.ToString("yyyy-MM-dd"));

                e.Result = ds;
            }
            catch
            {
                e.Result = null;
            }
        }

        protected override void InitializeForm()
        {
            //_BLL = new bllMXB高血压管理卡();// 业务逻辑层实例
            _SummaryView = new DevGridView(bandedGridView1);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(bandedGridView1);

            try
            {
                this.txt机构.Properties.AutoExpandAllNodes = false;
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                txt机构.Properties.ValueMember = "机构编号";
                txt机构.Properties.DisplayMember = "机构名称";

                txt机构.Properties.TreeList.KeyFieldName = "机构编号";
                txt机构.Properties.TreeList.ParentFieldName = "上级机构";
                txt机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                txt机构.Properties.DataSource = dt所属机构;

                txt机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                txt机构.Text = Loginer.CurrentUser.所属机构;
            }
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            if (bgInvoke != null && !bgInvoke.IsBusy)
            {
                bgInvoke.RunWorkerAsync();
                this.btnQuery.Enabled = false;
            }
        }
    }
}
