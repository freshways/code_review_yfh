﻿namespace AtomEHR.公共卫生.Module
{
    partial class frm接诊记录统计
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm接诊记录统计));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnEmpty = new DevExpress.XtraEditors.SimpleButton();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.cbo查询标准 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt完整度 = new DevExpress.XtraEditors.TextEdit();
            this.dte结束2 = new DevExpress.XtraEditors.DateEdit();
            this.dte开始1 = new DevExpress.XtraEditors.DateEdit();
            this.txt机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.gcSummary = new DevExpress.XtraGrid.GridControl();
            this.gvSummary = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.link区域名称 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.col总数 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col合格数 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col不合格数 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.link总数 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.link合格数 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.link不合格数 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gc档案 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv档案 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.link档案号 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col接诊时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo查询标准.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt完整度.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte结束2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte结束2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte开始1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte开始1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link区域名称)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link总数)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link合格数)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link不合格数)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link档案号)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gcSummary);
            this.tpSummary.Controls.Add(this.groupControl1);
            this.tpSummary.Controls.Add(this.gc档案);
            this.tpSummary.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tpSummary.Size = new System.Drawing.Size(1074, 516);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 33);
            this.pnlSummary.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.pnlSummary.Size = new System.Drawing.Size(1080, 522);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tcBusiness.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            this.tcBusiness.Size = new System.Drawing.Size(1080, 522);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tpDetail.Size = new System.Drawing.Size(1016, 852);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.gcNavigator.Padding = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.gcNavigator.Size = new System.Drawing.Size(1080, 33);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(877, 4);
            this.controlNavigatorSummary.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.controlNavigatorSummary.Size = new System.Drawing.Size(201, 25);
            // 
            // txtFocusForSave
            // 
            this.txtFocusForSave.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(620, 4);
            this.lblAboutInfo.Size = new System.Drawing.Size(257, 25);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.layoutControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1074, 140);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "查询信息";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnEmpty);
            this.layoutControl1.Controls.Add(this.btnQuery);
            this.layoutControl1.Controls.Add(this.cbo查询标准);
            this.layoutControl1.Controls.Add(this.txt完整度);
            this.layoutControl1.Controls.Add(this.dte结束2);
            this.layoutControl1.Controls.Add(this.dte开始1);
            this.layoutControl1.Controls.Add(this.txt机构);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 26);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(431, 278, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1070, 112);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnEmpty
            // 
            this.btnEmpty.Image = ((System.Drawing.Image)(resources.GetObject("btnEmpty.Image")));
            this.btnEmpty.Location = new System.Drawing.Point(484, 60);
            this.btnEmpty.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.Size = new System.Drawing.Size(63, 25);
            this.btnEmpty.StyleController = this.layoutControl1;
            this.btnEmpty.TabIndex = 10;
            this.btnEmpty.Text = "后退";
            this.btnEmpty.Visible = false;
            this.btnEmpty.Click += new System.EventHandler(this.btnEmpty_Click);
            // 
            // btnQuery
            // 
            this.btnQuery.Image = ((System.Drawing.Image)(resources.GetObject("btnQuery.Image")));
            this.btnQuery.Location = new System.Drawing.Point(418, 60);
            this.btnQuery.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(62, 25);
            this.btnQuery.StyleController = this.layoutControl1;
            this.btnQuery.TabIndex = 6;
            this.btnQuery.Text = "查询";
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // cbo查询标准
            // 
            this.cbo查询标准.EditValue = "录入时间";
            this.cbo查询标准.Location = new System.Drawing.Point(738, 32);
            this.cbo查询标准.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbo查询标准.Name = "cbo查询标准";
            this.cbo查询标准.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo查询标准.Properties.Items.AddRange(new object[] {
            "录入时间",
            "接诊时间"});
            this.cbo查询标准.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo查询标准.Size = new System.Drawing.Size(91, 24);
            this.cbo查询标准.StyleController = this.layoutControl1;
            this.cbo查询标准.TabIndex = 12;
            // 
            // txt完整度
            // 
            this.txt完整度.EditValue = "100";
            this.txt完整度.Location = new System.Drawing.Point(528, 32);
            this.txt完整度.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt完整度.Name = "txt完整度";
            this.txt完整度.Size = new System.Drawing.Size(78, 24);
            this.txt完整度.StyleController = this.layoutControl1;
            this.txt完整度.TabIndex = 6;
            // 
            // dte结束2
            // 
            this.dte结束2.EditValue = null;
            this.dte结束2.Location = new System.Drawing.Point(282, 32);
            this.dte结束2.Margin = new System.Windows.Forms.Padding(0);
            this.dte结束2.Name = "dte结束2";
            this.dte结束2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte结束2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte结束2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte结束2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte结束2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte结束2.Size = new System.Drawing.Size(114, 24);
            this.dte结束2.StyleController = this.layoutControl1;
            this.dte结束2.TabIndex = 3;
            // 
            // dte开始1
            // 
            this.dte开始1.EditValue = null;
            this.dte开始1.Location = new System.Drawing.Point(132, 32);
            this.dte开始1.Margin = new System.Windows.Forms.Padding(0);
            this.dte开始1.Name = "dte开始1";
            this.dte开始1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte开始1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte开始1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte开始1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte开始1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte开始1.Size = new System.Drawing.Size(126, 24);
            this.dte开始1.StyleController = this.layoutControl1;
            this.dte开始1.TabIndex = 1;
            // 
            // txt机构
            // 
            this.txt机构.Location = new System.Drawing.Point(132, 4);
            this.txt机构.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt机构.Name = "txt机构";
            this.txt机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt机构.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.txt机构.Size = new System.Drawing.Size(264, 24);
            this.txt机构.StyleController = this.layoutControl1;
            this.txt机构.TabIndex = 35;
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(212, 146);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.emptySpaceItem1,
            this.emptySpaceItem3,
            this.emptySpaceItem2,
            this.emptySpaceItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1070, 112);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txt机构;
            this.layoutControlItem1.CustomizationFormText = "机构:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(396, 28);
            this.layoutControlItem1.Text = "机构:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(125, 18);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dte开始1;
            this.layoutControlItem2.CustomizationFormText = "统计周期:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(258, 28);
            this.layoutControlItem2.Text = "统计周期:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(125, 18);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.dte结束2;
            this.layoutControlItem3.CustomizationFormText = "至";
            this.layoutControlItem3.Location = new System.Drawing.Point(258, 28);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(138, 28);
            this.layoutControlItem3.Text = "至";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(15, 18);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txt完整度;
            this.layoutControlItem4.CustomizationFormText = "单份档案合格标准:";
            this.layoutControlItem4.Location = new System.Drawing.Point(396, 28);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(210, 28);
            this.layoutControlItem4.Text = "单份档案合格标准:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(125, 18);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.cbo查询标准;
            this.layoutControlItem5.CustomizationFormText = "%      查询标准:";
            this.layoutControlItem5.Location = new System.Drawing.Point(606, 28);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(223, 28);
            this.layoutControlItem5.Text = "%      查询标准:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(125, 18);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnQuery;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(414, 56);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(66, 52);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.btnEmpty;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(480, 56);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(67, 52);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(396, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(670, 28);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(547, 56);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(519, 52);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(829, 28);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(237, 28);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 56);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(414, 52);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // gcSummary
            // 
            this.gcSummary.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcSummary.Location = new System.Drawing.Point(746, 145);
            this.gcSummary.MainView = this.gvSummary;
            this.gcSummary.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcSummary.Name = "gcSummary";
            this.gcSummary.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.link区域名称,
            this.link总数,
            this.link合格数,
            this.link不合格数,
            this.repositoryItemTextEdit1});
            this.gcSummary.Size = new System.Drawing.Size(286, 360);
            this.gcSummary.TabIndex = 6;
            this.gcSummary.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSummary});
            // 
            // gvSummary
            // 
            this.gvSummary.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gvSummary.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gvSummary.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gvSummary.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.DetailTip.Options.UseFont = true;
            this.gvSummary.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Empty.Options.UseFont = true;
            this.gvSummary.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.EvenRow.Options.UseFont = true;
            this.gvSummary.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gvSummary.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FilterPanel.Options.UseFont = true;
            this.gvSummary.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FixedLine.Options.UseFont = true;
            this.gvSummary.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FocusedCell.Options.UseFont = true;
            this.gvSummary.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FocusedRow.Options.UseFont = true;
            this.gvSummary.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gvSummary.Appearance.FooterPanel.Options.UseFont = true;
            this.gvSummary.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupButton.Options.UseFont = true;
            this.gvSummary.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupFooter.Options.UseFont = true;
            this.gvSummary.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupPanel.Options.UseFont = true;
            this.gvSummary.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupRow.Options.UseFont = true;
            this.gvSummary.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvSummary.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gvSummary.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HorzLine.Options.UseFont = true;
            this.gvSummary.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.OddRow.Options.UseFont = true;
            this.gvSummary.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Preview.Options.UseFont = true;
            this.gvSummary.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Row.Options.UseFont = true;
            this.gvSummary.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.RowSeparator.Options.UseFont = true;
            this.gvSummary.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.SelectedRow.Options.UseFont = true;
            this.gvSummary.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.TopNewRow.Options.UseFont = true;
            this.gvSummary.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.VertLine.Options.UseFont = true;
            this.gvSummary.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ViewCaption.Options.UseFont = true;
            this.gvSummary.ColumnPanelRowHeight = 40;
            this.gvSummary.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.col总数,
            this.col合格数,
            this.col不合格数,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn19});
            this.gvSummary.GridControl = this.gcSummary;
            this.gvSummary.GroupPanelText = "DragColumn";
            this.gvSummary.Name = "gvSummary";
            this.gvSummary.OptionsView.ColumnAutoWidth = false;
            this.gvSummary.OptionsView.EnableAppearanceEvenRow = true;
            this.gvSummary.OptionsView.EnableAppearanceOddRow = true;
            this.gvSummary.OptionsView.ShowFooter = true;
            this.gvSummary.OptionsView.ShowGroupPanel = false;
            this.gvSummary.CustomDrawFooterCell += new DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventHandler(this.gvSummary_CustomDrawFooterCell);
            this.gvSummary.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gvSummary_CustomRowCellEdit);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "区域编码";
            this.gridColumn1.FieldName = "区域编码";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "区域编码", "合计")});
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 76;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "区域名称";
            this.gridColumn2.ColumnEdit = this.link区域名称;
            this.gridColumn2.FieldName = "区域名称";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // link区域名称
            // 
            this.link区域名称.AutoHeight = false;
            this.link区域名称.Name = "link区域名称";
            this.link区域名称.Click += new System.EventHandler(this.link区域名称_Click);
            // 
            // col总数
            // 
            this.col总数.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col总数.AppearanceHeader.Options.UseFont = true;
            this.col总数.AppearanceHeader.Options.UseTextOptions = true;
            this.col总数.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col总数.Caption = "总数";
            this.col总数.FieldName = "总数";
            this.col总数.Name = "col总数";
            this.col总数.OptionsColumn.ReadOnly = true;
            this.col总数.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "总数", "{0:n0}")});
            this.col总数.Visible = true;
            this.col总数.VisibleIndex = 2;
            this.col总数.Width = 43;
            // 
            // col合格数
            // 
            this.col合格数.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col合格数.AppearanceHeader.Options.UseFont = true;
            this.col合格数.AppearanceHeader.Options.UseTextOptions = true;
            this.col合格数.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col合格数.Caption = "合格数";
            this.col合格数.FieldName = "合格数";
            this.col合格数.Name = "col合格数";
            this.col合格数.OptionsColumn.ReadOnly = true;
            this.col合格数.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "合格数", "{0:n0}")});
            this.col合格数.Visible = true;
            this.col合格数.VisibleIndex = 3;
            this.col合格数.Width = 54;
            // 
            // col不合格数
            // 
            this.col不合格数.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col不合格数.AppearanceHeader.Options.UseFont = true;
            this.col不合格数.AppearanceHeader.Options.UseTextOptions = true;
            this.col不合格数.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col不合格数.Caption = "不合格数";
            this.col不合格数.FieldName = "不合格数";
            this.col不合格数.Name = "col不合格数";
            this.col不合格数.OptionsColumn.ReadOnly = true;
            this.col不合格数.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "不合格数", "{0:n0}")});
            this.col不合格数.Visible = true;
            this.col不合格数.VisibleIndex = 4;
            this.col不合格数.Width = 61;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn9.AppearanceHeader.Options.UseFont = true;
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.Caption = "合格率";
            this.gridColumn9.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn9.DisplayFormat.FormatString = "{0:n2}%";
            this.gridColumn9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn9.FieldName = "合格率";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Average, "合格率", "{0:n2}%")});
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 5;
            this.gridColumn9.Width = 55;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "n0";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn10.AppearanceHeader.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.Caption = "平均\r\n完整度";
            this.gridColumn10.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn10.DisplayFormat.FormatString = "{0:n2}%";
            this.gridColumn10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn10.FieldName = "平均完整度";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Average, "平均完整度", "{0:n2}%")});
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 6;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn11.AppearanceHeader.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "转入\r\n档案数";
            this.gridColumn11.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn11.FieldName = "转入档案数";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "转入档案数", "{0:n0}")});
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 7;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn12.AppearanceHeader.Options.UseFont = true;
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.Caption = "转出\r\n档案数";
            this.gridColumn12.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn12.FieldName = "转出档案数";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "转出档案数", "{0:n0}")});
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 8;
            // 
            // gridColumn19
            // 
            this.gridColumn19.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn19.AppearanceHeader.Options.UseFont = true;
            this.gridColumn19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn19.Caption = "非活动\r\n档案数";
            this.gridColumn19.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn19.FieldName = "非活动档案数";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "非活动档案数", "{0:n0}")});
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 9;
            // 
            // link总数
            // 
            this.link总数.AutoHeight = false;
            this.link总数.Name = "link总数";
            this.link总数.Click += new System.EventHandler(this.link总数_Click);
            // 
            // link合格数
            // 
            this.link合格数.AutoHeight = false;
            this.link合格数.Name = "link合格数";
            this.link合格数.Click += new System.EventHandler(this.link合格数_Click);
            // 
            // link不合格数
            // 
            this.link不合格数.AutoHeight = false;
            this.link不合格数.Name = "link不合格数";
            this.link不合格数.Click += new System.EventHandler(this.link不合格数_Click);
            // 
            // gc档案
            // 
            this.gc档案.AllowBandedGridColumnSort = false;
            this.gc档案.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gc档案.IsBestFitColumns = true;
            this.gc档案.Location = new System.Drawing.Point(13, 145);
            this.gc档案.MainView = this.gv档案;
            this.gc档案.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gc档案.Name = "gc档案";
            this.gc档案.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.link档案号});
            this.gc档案.ShowContextMenu = false;
            this.gc档案.Size = new System.Drawing.Size(727, 316);
            this.gc档案.StrWhere = "";
            this.gc档案.TabIndex = 7;
            this.gc档案.UseCheckBox = false;
            this.gc档案.View = "";
            this.gc档案.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv档案});
            // 
            // gv档案
            // 
            this.gv档案.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gv档案.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gv档案.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gv档案.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.DetailTip.Options.UseFont = true;
            this.gv档案.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.Empty.Options.UseFont = true;
            this.gv档案.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.EvenRow.Options.UseFont = true;
            this.gv档案.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gv档案.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.FilterPanel.Options.UseFont = true;
            this.gv档案.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.FixedLine.Options.UseFont = true;
            this.gv档案.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.FocusedCell.Options.UseFont = true;
            this.gv档案.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.FocusedRow.Options.UseFont = true;
            this.gv档案.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gv档案.Appearance.FooterPanel.Options.UseFont = true;
            this.gv档案.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.GroupButton.Options.UseFont = true;
            this.gv档案.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.GroupFooter.Options.UseFont = true;
            this.gv档案.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.GroupPanel.Options.UseFont = true;
            this.gv档案.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.GroupRow.Options.UseFont = true;
            this.gv档案.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.HeaderPanel.Options.UseFont = true;
            this.gv档案.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gv档案.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.HorzLine.Options.UseFont = true;
            this.gv档案.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.OddRow.Options.UseFont = true;
            this.gv档案.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.Preview.Options.UseFont = true;
            this.gv档案.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.Row.Options.UseFont = true;
            this.gv档案.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.RowSeparator.Options.UseFont = true;
            this.gv档案.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.SelectedRow.Options.UseFont = true;
            this.gv档案.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.TopNewRow.Options.UseFont = true;
            this.gv档案.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.VertLine.Options.UseFont = true;
            this.gv档案.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gv档案.Appearance.ViewCaption.Options.UseFont = true;
            this.gv档案.ColumnPanelRowHeight = 30;
            this.gv档案.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.col接诊时间,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn3});
            this.gv档案.GridControl = this.gc档案;
            this.gv档案.GroupPanelText = "DragColumn";
            this.gv档案.Name = "gv档案";
            this.gv档案.OptionsView.ColumnAutoWidth = false;
            this.gv档案.OptionsView.EnableAppearanceEvenRow = true;
            this.gv档案.OptionsView.EnableAppearanceOddRow = true;
            this.gv档案.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn20
            // 
            this.gridColumn20.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn20.AppearanceHeader.Options.UseFont = true;
            this.gridColumn20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn20.Caption = "档案号";
            this.gridColumn20.ColumnEdit = this.link档案号;
            this.gridColumn20.FieldName = "个人档案编号";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 0;
            // 
            // link档案号
            // 
            this.link档案号.AutoHeight = false;
            this.link档案号.Name = "link档案号";
            this.link档案号.Click += new System.EventHandler(this.link档案号_Click);
            // 
            // gridColumn21
            // 
            this.gridColumn21.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn21.AppearanceHeader.Options.UseFont = true;
            this.gridColumn21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn21.Caption = "姓名";
            this.gridColumn21.FieldName = "姓名";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 1;
            // 
            // gridColumn22
            // 
            this.gridColumn22.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn22.AppearanceHeader.Options.UseFont = true;
            this.gridColumn22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn22.Caption = "性别";
            this.gridColumn22.FieldName = "性别";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 2;
            // 
            // gridColumn23
            // 
            this.gridColumn23.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn23.AppearanceHeader.Options.UseFont = true;
            this.gridColumn23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn23.Caption = "出生日期";
            this.gridColumn23.FieldName = "出生日期";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 3;
            // 
            // gridColumn24
            // 
            this.gridColumn24.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn24.AppearanceHeader.Options.UseFont = true;
            this.gridColumn24.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn24.Caption = "身份证号";
            this.gridColumn24.FieldName = "身份证号";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 4;
            // 
            // gridColumn25
            // 
            this.gridColumn25.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn25.AppearanceHeader.Options.UseFont = true;
            this.gridColumn25.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn25.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn25.Caption = "联系电话";
            this.gridColumn25.FieldName = "联系人电话";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 5;
            // 
            // gridColumn26
            // 
            this.gridColumn26.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn26.AppearanceHeader.Options.UseFont = true;
            this.gridColumn26.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn26.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn26.Caption = "当前所属机构";
            this.gridColumn26.FieldName = "所属机构";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 6;
            this.gridColumn26.Width = 105;
            // 
            // gridColumn27
            // 
            this.gridColumn27.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn27.AppearanceHeader.Options.UseFont = true;
            this.gridColumn27.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn27.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn27.Caption = "录入人";
            this.gridColumn27.FieldName = "创建人";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 7;
            // 
            // col接诊时间
            // 
            this.col接诊时间.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col接诊时间.AppearanceHeader.Options.UseFont = true;
            this.col接诊时间.AppearanceHeader.Options.UseTextOptions = true;
            this.col接诊时间.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col接诊时间.Caption = "接诊时间";
            this.col接诊时间.FieldName = "接诊时间";
            this.col接诊时间.Name = "col接诊时间";
            this.col接诊时间.OptionsColumn.ReadOnly = true;
            this.col接诊时间.Visible = true;
            this.col接诊时间.VisibleIndex = 8;
            // 
            // gridColumn28
            // 
            this.gridColumn28.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn28.AppearanceHeader.Options.UseFont = true;
            this.gridColumn28.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn28.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn28.Caption = "录入时间";
            this.gridColumn28.FieldName = "创建时间";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 9;
            // 
            // gridColumn29
            // 
            this.gridColumn29.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn29.AppearanceHeader.Options.UseFont = true;
            this.gridColumn29.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn29.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn29.Caption = "家庭档案编号";
            this.gridColumn29.FieldName = "家庭档案编号";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 10;
            this.gridColumn29.Width = 102;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "接诊医生";
            this.gridColumn3.FieldName = "接诊医生";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 11;
            // 
            // frm接诊记录统计
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1080, 555);
            this.Margin = new System.Windows.Forms.Padding(3, 8, 3, 8);
            this.Name = "frm接诊记录统计";
            this.Text = "接诊记录统计";
            this.Load += new System.EventHandler(this.frm接诊记录统计_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbo查询标准.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt完整度.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte结束2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte结束2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte开始1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte开始1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link区域名称)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link总数)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link合格数)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link不合格数)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link档案号)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcSummary;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSummary;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btnEmpty;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraEditors.ComboBoxEdit cbo查询标准;
        private DevExpress.XtraEditors.TextEdit txt完整度;
        private DevExpress.XtraEditors.DateEdit dte结束2;
        private DevExpress.XtraEditors.DateEdit dte开始1;
        private DevExpress.XtraEditors.TreeListLookUpEdit txt机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link区域名称;
        private DevExpress.XtraGrid.Columns.GridColumn col总数;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link总数;
        private DevExpress.XtraGrid.Columns.GridColumn col合格数;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link合格数;
        private DevExpress.XtraGrid.Columns.GridColumn col不合格数;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link不合格数;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private Library.UserControls.DataGridControl gc档案;
        private DevExpress.XtraGrid.Views.Grid.GridView gv档案;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link档案号;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraGrid.Columns.GridColumn col接诊时间;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;

    }
}