﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Interfaces;
using AtomEHR.公共卫生.Module.个人健康;
using DevExpress.XtraEditors.Repository;

namespace AtomEHR.公共卫生.Module
{
    public partial class frm档案复核升级统计 : AtomEHR.Library.frmBaseDataForm
    {
        //定义此类型，用来判断是点击那个链接跳转进档案grid的
        private string _type = "1";
        DataSet ds = null;
        BackgroundWorker bgInvoke = new BackgroundWorker();
        public frm档案复核升级统计()
        {
            InitializeComponent();
        }

        private void frm档案复核升级统计_Load(object sender, EventArgs e)
        {
            if (Loginer.CurrentUser.所属机构.Length >= 12)
                txt机构.Properties.AutoExpandAllNodes = true;
            else { txt机构.Properties.AutoExpandAllNodes = false; }
            this.InitializeForm();
            string m_ServerDate = _bll.ServiceDateTime;
            try
            {
                this.dte开始1.Text = Convert.ToDateTime(m_ServerDate).ToString("yyyy-MM-01");
                this.dte结束2.Text = Convert.ToDateTime(m_ServerDate).ToString("yyyy-MM-dd");
            }
            catch
            {
                this.dte开始1.Text = DateTime.Now.ToString("yyyy-MM-01");
                this.dte结束2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
            bgInvoke.DoWork += bgInvoke_DoWork;
            bgInvoke.RunWorkerCompleted += bgInvoke_RunWorkerCompleted;
        }

        void bgInvoke_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //throw new NotImplementedException();
            DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();

            DataSet ds = e.Result as DataSet;
            if (ds != null && ds.Tables.Count > 0)
            {
                gcSummary.DataSource = ds.Tables[0];
                this.gvSummary.BestFitColumns();
            }
            else
            {
                gcSummary.DataSource = null;
            }
            this.btnQuery.Enabled = true;
        }

        void bgInvoke_DoWork(object sender, DoWorkEventArgs e)
        {
            //throw new NotImplementedException();
            DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(typeof(WaitForm1));

            try
            {
                DataSet ds = _bll.GetReportDataByFH(txt机构.EditValue.ToString(), dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"));

                e.Result = ds;
            }
            catch
            {
                e.Result = null;
            }
        }

        protected override void InitializeForm()
        {
            //_BLL = new bllMXB高血压管理卡();// 业务逻辑层实例
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gvSummary);

            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                txt机构.Properties.ValueMember = "机构编号";
                txt机构.Properties.DisplayMember = "机构名称";

                txt机构.Properties.TreeList.KeyFieldName = "机构编号";
                txt机构.Properties.TreeList.ParentFieldName = "上级机构";
                txt机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                txt机构.Properties.DataSource = dt所属机构;

                txt机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                txt机构.Text = Loginer.CurrentUser.所属机构;
            }
        }

        bll健康体检 _bll = new bll健康体检();
        private void btnQuery_Click(object sender, EventArgs e)
        {
            if (bgInvoke != null && !bgInvoke.IsBusy)
            {
                bgInvoke.RunWorkerAsync();
                this.btnQuery.Enabled = false;
            }

            //try
            //{
            //    //WaitForm1 frm = new WaitForm1();
            //    //frm.Show();
            //    //DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(typeof(WaitForm1));
            //    Func<DataSet> func = new Func<DataSet>(() =>
            //    {
            //        return _bll.GetReportDataByFH(txt机构.EditValue.ToString(), dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"));
            //    });
            //    func.BeginInvoke(new AsyncCallback((ar) =>
            //    {
            //        Func<DataSet> tmp = (Func<DataSet>)ar.AsyncState;
            //        ds = tmp.EndInvoke(ar);
            //        this.Invoke(new Action(() =>
            //        {
            //            if (ds != null && ds.Tables.Count > 0)
            //            {
            //                gcSummary.DataSource = ds.Tables[0];
            //                this.gcSummary.BringToFront();
            //                this.gvSummary.BestFitColumns();
            //                //DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
            //            }
            //        }));
            //    }), func);
            //}
            //catch (Exception)
            //{
            //    //DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
            //}
            ////ds = _bll.GetReportDataByFH(txt机构.EditValue.ToString(), dte开始1.DateTime.ToString("yyyy-MM-dd 0:00:00"), dte结束2.DateTime.ToString("yyyy-MM-dd 23:59:59"));           
        }
        
        private int iAll = 1;
        private int iHege = 0;
        private void gvSummary_CustomDrawFooterCell(object sender, DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventArgs e)
        {

            if (e.Column == gridColumn9)
            {
                e.Info.DisplayText = (iHege * 100.0 / iAll).ToString("N2") + "%";
            }
            else if (e.Column == col总数)
            {
                iAll = 1;
                try
                {
                    int temp = Convert.ToInt32(e.Column.SummaryText.Replace(",", ""));
                    iAll = (temp == 0) ? 1 : temp;
                }
                catch
                { }
            }
            else if (e.Column == col已复核总人数)
            {
                iHege = 1;
                try
                {
                    int temp = Convert.ToInt32(e.Column.SummaryText.Replace(",", ""));
                    iHege = temp;
                }
                catch
                { }
            }
            else
            { }
        }

        private void btnEmpty_Click(object sender, EventArgs e)
        {
            this.gcSummary.BringToFront();
        }

    }
}
